<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'myblades');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'root');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^:g*iV^B?S/Y|YRDx/sI:47%TgodnrT;AX*j(~guG<lu;v]iCU5y]p$fXcz]o,,U');
define('SECURE_AUTH_KEY',  '?#7A&Z{f{CQ5RuHJI?f]q373N!gZ?|/Ed.xL@i5rbCE1*O#woq[<)J<_g9]b<&|)');
define('LOGGED_IN_KEY',    'Lb5aCCPx%{s=WXbW< e8SH5HZ3VG1xn2P{pCJz$YCgXez4{G)@c%-9Xo=0/TUOhs');
define('NONCE_KEY',        '>DbO5RT,Amxj4raWR&l!7An}m:/xdEVT,!&+F1RByx;<4g#a`dVfoO:>d{{0tw9O');
define('AUTH_SALT',        'WpUdLIQJ!GP)cXs)4XX<k.2&N;$hN!Om.hq2o/3<##$1eF,?a}44h#GBZtO$QA7W');
define('SECURE_AUTH_SALT', '^#SB81s;f.ei~y&+|t(PS7K^LS~LHB#ft*]+}cQ:3`2ow?5;Mq@<3iW2k6}re+$*');
define('LOGGED_IN_SALT',   'wtOg(|2FvGWc43ZR=`12JKgvj+?:Q4HGF O4]z!1_o$w}%)([uG|JS^oQ{xn=?i&');
define('NONCE_SALT',       'PG1R*LEVoXvu284 +7[A(w}D~LZv,j0ndAMhvE<j2Qk&U<nti5YmBd&oEn2F7WuG');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
