<?php 
/*
  Shipping Name: Модуль доставки EMS-отправлений
 */
class USAM_Shipping_emspost extends USAM_Shipping
{	
	public function __construct( $id )
	{
		parent::__construct( $id );
		$this->API_URL = "https://tracking.russianpost.ru/rtm34?wsdl";		
		$default = array( 'login' => 'hzEdTuKLPFCVof', 'pass' => 'jKwlC3wiIsyi' );
		$this->setting = array_merge ($default, $this->setting);
	}		
	
	public function get_history_mail( $barcode )
	{ 
		if ( empty($this->setting['login']) ||  empty($this->setting['pass']) )
		{ 
			$this->set_error( __('Не указан логин или пароль.','usam') );
			return array();
		}
		if ( empty($barcode) )
		{
			$this->set_error( __('Не указан номер почтового отправления.','usam') );
			return array();
		}		
		$barcode = mb_strtoupper($barcode);
		$client2 = new SoapClient($this->API_URL, array('trace' => 1, 'soap_version' => SOAP_1_2));	
		$params = array ('OperationHistoryRequest' => array ('Barcode' => $barcode, 'MessageType' => '0','Language' => 'RUS'),
						  'AuthorizationHeader' => array ('login'=> $this->setting['login'], 'password'=> $this->setting['pass'] ));

		$result = $client2->getOperationHistory(new SoapParam($params,'OperationHistoryRequest'));

		$history = array();
		foreach ($result->OperationHistoryData->historyRecord as $record) 
		{
			$date = str_replace('T',' ', $record->OperationParameters->OperDate);
			$history['operations'][] = array( 'date' => usam_local_date($date), 'description' => $record->AddressParameters->OperationAddress->Description, 'name' => $record->OperationParameters->OperAttr->Name, 'type_name' => $record->ItemParameters->ComplexItemName );		
		}
		return $history;
	}
	
	function send_request( $params )
	{			
		$query = http_build_query($params);  
		$data = wp_remote_get('http://emspost.ru/api/rest/?'.$query, array('sslverify' => false));
		
		if (is_wp_error($data))
			return $data->get_error_message();

		$resp = json_decode($data['body'],true);	
		if ( isset( $resp['error'] ) ) 
		{			
			$error = sprintf( __('Приложение %s вызвало ошибку №%s. Текст ошибки: %s'), $resp['error']['request_params'][1]['value'], $resp['error']['error_code'], $resp['error']['error_msg']);
			$this->set_error( $error );	
			return false;
		}			
		return $resp['rsp'];		
	}
	
	public function calculate_shipping( $from, $to, $weight )
	{
		if ( empty($weight) )
			return false;
		
		if ( empty($from) )
			return false;
		
		if ( empty($to) )
			return false;
		
		$weight = substr($weight, 0, -3).'.'.substr($weight, -3 );	
		$params = array( 'method' => 'ems.calculate', 'from' => $from, 'to' => $to, 'weight' => $weight );	
		
		$result = $this->send_request( $params );		
		return array( 'price' => $result['price'], 'max' => $result['term']['max'], 'min' => $result['term']['min'] ) ;
	}
	
	
	public function get_shipping_order( $order_id )
	{
		$purchase_log = new USAM_Order( $order_id );	
		$customer_location = $purchase_log->get_item_customer_data( 'shippinglocation' );
		$to_location = usam_get_location( $customer_location['value'] );	
							
		$current_location_id = get_option( 'usam_shop_location' );
		$from_location = usam_get_location( $current_location_id );			
		
		$params = array( 'method' => 'ems.get.locations', 'type' => 'cities', 'plain' => 'true' );
		$result = $this->send_request( $params );
	
		$str = mb_strtoupper($from_location['name']);		
		foreach ( $result['locations'] as $item ) 
		{			
			if ( $str == $item['name'] )
			{
				$from = $item['value'];
				break;
			}
		}		
		$str = mb_strtoupper($to_location['name']);
		foreach ( $result['locations'] as $item ) 		
			if ( $str == $item['name'] )
			{
				$to = $item['value'];
				break;
			}			
		
		$data = $purchase_log->get_calculated_data();		
		
		$result = $this->calculate_shipping( $from, $to, $weight );	
		return $result;
	}
	
	
	public function get_shipping_cost( $args )
	{		
		return false;
		if ( empty($args['weight']) )
		{
			$this->set_error( __('Невозможно расчитать доставку. Не указан вес товаров.','usam') );
			return false;
		}	
		if ( empty($args['location']) )
		{
			$this->set_error( __('Невозможно расчитать доставку. Не указано местоположение.','usam') );
			return false;
		}			
		$to_location = usam_get_location( $args['location'] );		
		
		$current_location_id = get_option( 'usam_shop_location' );	
		$from_location = usam_get_location( $current_location_id );			
		
		$params = array( 'method' => 'ems.get.locations', 'type' => 'cities', 'plain' => 'true' );		
		$result = $this->send_request( $params );

	
		$str = mb_strtoupper($from_location['name']);		
		foreach ( $result['locations'] as $item ) 
		{			
			if ( $str == $item['name'] )
			{
				$from = $item['value'];
				break;
			}
		}		
		$str = mb_strtoupper($to_location['name']);
		foreach ( $result['locations'] as $item ) 		
			if ( $str == $item['name'] )
			{
				$to = $item['value'];
				break;
			}			
			
		$result = $this->calculate_shipping( $from, $to, $args['weight'] );	
		return $result;
	}
	
	
	function get_form( ) 
	{				
		?>
		<tr>							
			<td class ="name"><?php _e( 'Логин', 'usam' ); ?>:</td>
			<td class ="row_option"><input type="text" name="handler_setting[login]" value="<?php echo $this->setting['login']; ?>"/></td>
		</tr>
		<tr>							
			<td class ="name"><?php _e( 'Пароль', 'usam' ); ?>:</td>
			<td class ="row_option"><input type="text" name="handler_setting[pass]" value="<?php echo $this->setting['pass']; ?>"/></td>
		</tr>
		<?php
	}	
}
?>