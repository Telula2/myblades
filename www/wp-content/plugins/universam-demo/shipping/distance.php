<?php 
/*
  Shipping Name: Модуль расчитывает стоимость по расстоянию
 */
class USAM_Shipping_distance extends USAM_Shipping
{	
	public function __construct( $id )
	{
		parent::__construct( $id );
		$default = array( 'storage' => 0, 'price' => 0 );
		$this->setting = array_merge ($default, $this->setting);
	}	
	
	private function set_error( $error )
	{		
		$this->error  =  sprintf( __('Приложение %s вызвало ошибку №%s. Текст ошибки: %s'), $error['request_params'][1]['value'], $error['error_code'], $error['error_msg']);
	}
		
	public function calculate_shipping( $from, $to, $weight )
	{
		
	}	
	
	public function get_shipping_order( $document_id )
	{
		
	}	
	
	public function get_shipping_cost( $args )
	{		
		if ( empty($args['location']) )
		{
			$this->error = __('Невозможно расчитать доставку. Не указано местоположение.','usam');
			return false;
		}				
		if ( empty($this->setting['storage']) )
		{			
			return false;
		}	
		$name = usam_get_full_locations_name( $args['location'] );
		
		$address = usam_get_customer_profile( 'shippingaddress' );
		$to = $name.','.$address;
		
		$storage = usam_get_storage( $this->setting['storage'] );		
		$name = usam_get_full_locations_name( $storage['location_id'] );
		$from = $name.','.$storage['address'];		
		
		$distance = usam_get_distance_by_name( $from, $to );
		if ( empty($distance) )
		{			
			return false;
		}					
		$price = round($distance/1000*$this->setting['price'],2);	
		return $price;
	}
	
	
	function get_form( ) 
	{				
		?>
		<tr>							
			<td class ="name"><?php _e( 'Склад отгрузки', 'usam' ); ?>:</td>
			<td class ="row_option"><?php usam_get_storage_dropdown( $this->setting['storage'], array( 'name' => 'handler_setting[storage]' ) ); ?>	</td>
		</tr>	
		<tr>							
			<td class ="name"><?php _e( 'Стоимость за километр', 'usam' ); ?>:</td>
			<td class ="row_option"><input type="text" name="handler_setting[price]" value="<?php echo $this->setting['price']; ?>"/></td>
		</tr>		
		<?php
	}	
}
?>