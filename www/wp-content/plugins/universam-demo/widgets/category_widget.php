<?php
/**
 * Класс виджета "категории продуктов"
 * @since 3.7.1
 */
class USAM_Widget_Product_Categories extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array(
			'classname' => 'widget_usam_categorisation',
			'description' => __( 'Виджет категорий товаров', 'usam' )
		);
		parent::__construct( 'usam_categorisation', __( 'Категории товаров', 'usam' ), $widget_ops );
	}

	function widget( $args, $instance )
	{
		extract( $args );

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'] );

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;

		$show_thumbnails = $instance['image'];
		
		if ( isset($instance['width'] ) )
			$width = $instance['width'];

		if ( isset( $instance['height'] ) )
			$height = $instance['height'];

		if ( !isset( $instance['categories'] ) )
		{
			$instance_categories = get_terms( 'usam-category', 'hide_empty=0&parent=0');
			if(!empty($instance_categories)){
				foreach($instance_categories as $categories){

					$instance['categories'][$categories->term_id] = 'on';
				}
			}		
		}	
		global $wp_query;		
		
		$current_default   = get_option( 'usam_default_menu_category', 0 );	
		
		$args_terms = array( "hide_empty" => 0, 'child_of' => $current_default, 'update_term_meta_cache' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sorting_menu_categories' );
		if ( empty($terms)	)
			$terms = get_terms( 'usam-category', "parent=$current_default&update_term_meta_cache=0" );
		

		if ( !empty($wp_query->query['usam-category']))
		{			
			$term_data = get_term_by( 'slug', $wp_query->query['usam-category'] , 'usam-category' );		
			$active_category = $term_data->term_id;	
			
			$terms = get_ancestors( $term_data->term_id, 'usam-category' );
			if ( !empty($terms))	
				foreach ($terms as $term_id) 
				{							
					if ( $term_id == $current_default)
						break;					
					$active_category = $term_id;	
				}
		}		
		else
			$active_category = '';		
		$args_list = array(	'taxonomy' => 'usam-category', 'select' => array( $active_category ) );	
		
		echo '<div class="usam_categories_list">'.usam_get_walker_terms_list( $args_terms, $args_list ).'</div>';	
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title']      = strip_tags( $new_instance['title'] );
		$instance['image']      = $new_instance['image'] ? 1 : 0;
		$instance['categories'] = $new_instance['categories'];
		$instance['grid']       = $new_instance['grid'] ? 1 : 0;
		$instance['height']     = (int)$new_instance['height'];
		$instance['width']      = (int)$new_instance['width'];
		$instance['show_name']	= (bool)$new_instance['show_name'];	
		return $instance;
	}

	function form( $instance ) 
	{	
		$instance = wp_parse_args((array) $instance, array(
			'title' => __( 'Категории товаров', 'usam'  ),
			'width' => 45,
			'height' => 45,
			'image' => false,		
			'show_name' => false,
		));	
		$title    = esc_attr( $instance['title'] );
		$image    = (bool) $instance['image'];
		$width    = (int) $instance['width'];
		$height   = (int) $instance['height'];	
		$show_name= (bool) $instance['show_name'];	
		 ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>		
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>"<?php checked( $image ); ?> onclick="jQuery('.usam_category_image').toggle()" />
			<label for="<?php echo $this->get_field_id('image'); ?>"><?php _e('Показать миниатюры', 'usam'); ?></label>
		</p>

		<div class="usam_category_image"<?php if( !checked( $image ) ) { echo ' style="display:none;"'; } ?>>
			<p>
				
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_name'); ?>" name="<?php echo $this->get_field_name('show_name'); ?>"<?php checked( $show_name ); ?> /><label for="<?php echo $this->get_field_id('show_name'); ?>"><?php _e(' Показать "Нет данных", когда нет доступных изображений', 'usam'); ?></label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Ширина:', 'usam'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" value="<?php echo $width ; ?>" size="3" />
				<label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Высота:', 'usam'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" value="<?php echo $height ; ?>" size="3" />
			</p>
		</div>
<?php
	}
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Product_Categories");' ) );