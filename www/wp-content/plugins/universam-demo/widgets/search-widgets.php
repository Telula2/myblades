<?php
/**
  * Виджет
 */
class USAM_Search_Widgets extends WP_Widget
{	
	function __construct()
	{
		$widget_ops = array('classname' => 'widget_products_search', 'description' => __( "Пользователь видит не все результаты поиска, поскольку они в выпадающем окне - ссылка 'Все результаты поиска', отправляет на циклическую загрузку.", 'usam') );
		parent::__construct('products_predictive_search', __('Умный поиск', 'usam'), $widget_ops);
	}

	function widget( $args, $instance )
	{
		extract($args);		
		$title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
		if(empty($instance['number_items']) || is_array($instance['number_items']) || $instance['number_items'] <= 0) 
			$number_items = 5; 
		else 
			$number_items = $instance['number_items'];
		if( empty($instance['text_lenght']) || $instance['text_lenght'] < 0 ) 
			$text_lenght = 100; 
		else 
			$text_lenght = $instance['text_lenght'];
		$search_global = empty($instance['search_global']) ? 0 : $instance['search_global'];
		$search_box_text = ( isset($instance['search_box_text']) ? $instance['search_box_text'] : '' );
		if (trim($search_box_text) == '') 
			$search_box_text = get_option('usam_search_box_text');
		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;		
		echo $this->results_search_form($widget_id, $number_items, $text_lenght, '',$search_global, $search_box_text);
		echo $after_widget;
	}
	
	public static function results_search_form($widget_id, $number_items = 5, $text_lenght = 100, $style='', $search_global = 0, $search_box_text = '')
	{			
		global $wp_query;	
		
		//Добавить Ajax Search сценарий и стиль в нижний колонтитул
		add_action('wp_footer',array('USAM_Search_Hook_Filter','add_frontend_script_style'));
		$id = str_replace('products_predictive_search-','',$widget_id);
		$cat_slug = '';
		$tag_slug = '';
		$row = 5;		
		if ( $number_items > 0  ) 
			$row = $number_items;		
		ob_start();	
		if (isset($wp_query->query_vars['keyword'])) 
			$search_keyword = stripslashes( strip_tags( urldecode( $wp_query->query_vars['keyword'] ) ) );
		elseif (isset($_REQUEST['rs']) && trim($_REQUEST['rs']) != '') 
			$search_keyword = stripslashes( strip_tags( $_REQUEST['rs'] ) );
		else 
			$search_keyword = '';
		
		$url = add_query_arg( array( 'action' => 'autocomplete', 'get' => 'search_keyword', 'security' => wp_create_nonce( 'search_keyword' ) ), admin_url( 'admin-ajax.php', 'relative' ) );	
		$url = add_query_arg( array( 'row' => $row, 'text_lenght' => $text_lenght ), $url );	
		
		if( $cat_slug != '')
			$url = add_query_arg( array( 'scat' => $cat_slug), $url );	
		
		if( $tag_slug != '')
			$url = add_query_arg( array( 'stag' => $tag_slug), $url );		
		?>
<script type="text/javascript">
jQuery(document).ready(function() 
{		
	jQuery(document).on("click", "#bt_pp_search_<?php echo $id;?>", function()
	{		
		if (jQuery("#pp_course_<?php echo $id;?>").val() != '' && jQuery("#pp_course_<?php echo $id;?>").val() != '<?php echo $search_box_text; ?>') 
		{			
			<?php if (get_option('permalink_structure') == '') { ?>jQuery("#fr_pp_search_widget_<?php echo $id;?>").submit();<?php } else { ?>var pp_search_url_<?php echo $id;?> = '<?php echo rtrim( usam_get_url_system_page( 'search' ), '/' );?>/keyword/'+jQuery("#pp_course_<?php echo $id;?>").val();
			<?php if ($cat_slug != '') { ?> pp_search_url_<?php echo $id;?> += '/scat/<?php echo $cat_slug; ?>';
			<?php } elseif ($tag_slug != '') { ?> pp_search_url_<?php echo $id;?> += '/stag/<?php echo $tag_slug; ?>'; <?php } ?>
			window.location = pp_search_url_<?php echo $id;?>;
		<?php } ?>
		}		
	});	
	jQuery("#fr_pp_search_widget_<?php echo $id;?>").bind("keypress", function(e) 
	{
		if (e.keyCode == 13) 
		{		
			if (jQuery("#pp_course_<?php echo $id;?>").val() != '' && jQuery("#pp_course_<?php echo $id;?>").val() != '<?php echo $search_box_text; ?>') 
			{
				<?php if (get_option('permalink_structure') == '') 
				{ 
					?>jQuery("#fr_pp_search_widget_<?php echo $id;?>").submit();<?php 
				} 
				else
				{
					?>var pp_search_url_<?php echo $id;?> = '<?php echo rtrim( usam_get_url_system_page( 'search' ), '/' );?>/keyword/'+jQuery("#pp_course_<?php echo $id;?>").val();
					<?php if ($cat_slug != '') 
					{ 
						?> pp_search_url_<?php echo $id;?> += '/scat/<?php echo $cat_slug; ?>';	<?php 
					} 
					elseif ($tag_slug != '') 
					{ 
						?> pp_search_url_<?php echo $id;?> += '/stag/<?php echo $tag_slug; ?>'; <?php 
					} 
					?>
					window.location = pp_search_url_<?php echo $id;?>;<?php
				} ?>
				return false;
			} 
			else
			{			
				return false;
			}
		}
	});			
	var ul_width = jQuery("#pp_search_container_<?php echo $id;?>").find('.ctr_search').innerWidth();
	jQuery("#pp_course_<?php echo $id;?>").autocomplete({	
		source: '<?php echo $url; ?>',					
		minLength   : 2, 
		autoFocus 	: true,		
		open : function( event, ui ) 
		{ 			
			jQuery( '.predictive_results_<?php echo $id;?>' ).css( "width", ul_width ).find('li.product_not_selection').removeClass('ui-menu-item').removeClass('ui-state-focus');
		},															
		select : function(event, ui) 
		{ 
			if ( !ui.item.hasClass('product_not_selection') )
			{
				jQuery("#select_product_<?php echo $id;?>").val(ui.item.value).trigger( "change" );										
				ui.item.value = jQuery(ui.item.label).text();							
			}
		},	
		create : function(event, ui) 
		{ 
			jQuery(this).data("ui-autocomplete")._renderMenu = function( ul, items ) 
			{
				var that = this;
				jQuery.each( items, function( index, item ) {
					that._renderItemData( ul, item );
				});			
				jQuery( ul ).css( "width", ul_width ).addClass("predictive_results").addClass("predictive_results_<?php echo $id;?>").find( "li:odd" ).addClass( "odd" );
			}			
		},		
	}).data("ui-autocomplete")._renderItem = function (ul, item) {						
		return jQuery("<li></li>").addClass('product_'+item.type).data("item.autocomplete", item).append(item.label).appendTo(ul);
	};	
});
</script>
        <div class="pp_search_container" id="pp_search_container_<?php echo $id;?>" style=" <?php echo $style; ?> ">     
		<form autocomplete="off" action="<?php echo usam_get_url_system_page( 'search' );?>" method="get" class="fr_search_widget" id="fr_pp_search_widget_<?php echo $id;?>">
        	<?php
			if (get_option('permalink_structure') == '') 
			{
				?>
				<input type="hidden" name="page_id" value="<?php echo usam_get_url_system_page( 'search' ); ?>"  />
				<?php
			} 
			?>
			<div class="ctr_search">
				<input style = "font-size: 100%;" type="text" placeholder='<?php echo $search_box_text; ?>' id="pp_course_<?php echo $id;?>" onblur="if (this.value == '') {this.value = '<?php echo $search_box_text; ?>';}" onfocus="if (this.value == '<?php echo $search_box_text; ?>') {this.value = '';}" value="<?php echo $search_keyword; ?>" name="rs" class="txt_livesearch" />
				<span class="bt_search" id="bt_pp_search_<?php echo $id;?>"></span>
            </div>
            <?php
			if ($cat_slug != '') 
			{ 
				?><input type="hidden" name="scat" value="<?php echo $cat_slug; ?>"  /><?php
			}
			elseif ($tag_slug != '')
			{ 
				?><input type="hidden" name="stag" value="<?php echo $tag_slug; ?>"  /><?php
			}
			?>
		</form>
        </div>
        <?php if (trim($style) == '') { ?>
			<div style="clear:both;"></div>
		<?php } ?>
    	<?php
		$search_form = ob_get_clean();
		return $search_form;
	}
	
	function update( $new_instance, $old_instance ) 
	{
		$instance = $new_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number_items'] = $new_instance['number_items'];
		$instance['text_lenght'] = strip_tags($new_instance['text_lenght']);
		$instance['search_global'] =  $new_instance['search_global'];
		$instance['search_box_text'] = strip_tags($new_instance['search_box_text']);	
		return $instance;
	}
	
	public static function get_items_search()
	{
		$items_search = array(
				'product'				=> array( 'number' => 6, 'name' => __('Имя товара', 'usam') ),
				'p_sku'					=> array( 'number' => 0, 'name' => __('Артикул товара', 'usam') ),
				'p_cat'					=> array( 'number' => 0, 'name' => __('Категория товара', 'usam') ),
				'p_tag'					=> array( 'number' => 0, 'name' => __('Тег товара', 'usam') ),
				'post'					=> array( 'number' => 0, 'name' => __('Запись', 'usam') ),
				'page'					=> array( 'number' => 0, 'name' => __('Страница', 'usam') )
			);			
		return $items_search;
	}	

	function form( $instance ) 
	{
		$items_search_default = USAM_Search_Widgets::get_items_search();				
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'number_items' => 5, 'text_lenght' => 100, 'search_global' => 1, 'search_box_text' => get_option('usam_search_box_text')) );
		$title = strip_tags($instance['title']);
		$number_items = $instance['number_items'];
		if (empty($number_items) || is_array($number_items) ) 
			$number_items = 5;
		else 
			$number_items = strip_tags($instance['number_items']);
		$text_lenght = strip_tags($instance['text_lenght']);
		$search_global = $instance['search_global'];
		$search_box_text = $instance['search_box_text'];		
?>		
		<style>			
			.item_heading{ width:130px; display:inline-block;}
			ul.predictive_search_item li{padding-left:15px; background:url(<?php echo USAM_CORE_IMAGES_URL; ?>/sortable.gif) no-repeat left center; cursor:pointer;}
			ul.predictive_search_item li.ui-sortable-placeholder{border:1px dotted #111; visibility:visible !important; background:none;}
			ul.predictive_search_item li.ui-sortable-helper{background-color:#DDD;}
		</style>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Название:', 'usam'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
            <p><label for="<?php echo $this->get_field_id('search_box_text'); ?>"><?php _e('Текст окна поиска:', 'usam'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('search_box_text'); ?>" name="<?php echo $this->get_field_name('search_box_text'); ?>" type="text" value="<?php echo esc_attr($search_box_text); ?>" /></p>
            <p><label for="<?php echo $this->get_field_id('number_items'); ?>"><?php _e('Число результатов для показа:', 'usam'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('number_items'); ?>" name="<?php echo $this->get_field_name('number_items'); ?>" type="text" value="<?php echo esc_attr($number_items); ?>" /></p>
			<p><label for="<?php echo $this->get_field_id('text_lenght'); ?>"><?php _e('Количество символов описания:', 'usam'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('text_lenght'); ?>" name="<?php echo $this->get_field_name('text_lenght'); ?>" type="text" value="<?php echo esc_attr($text_lenght); ?>" /></p>  			
			<ul class="ui-sortable predictive_search_item">
			<?php 
			$items_search_default = array();
			foreach ($items_search_default as $key => $data) 
			{ 
				if ( empty($instance[$key]))
					$value = $data['number'];
				else
					$value =  $instance[$key];				
				?>
				<li><span class="item_heading"><label><?php echo $data['name']; ?></label></span> <input name="<?php echo $this->get_field_name($key); ?>" type="text" value="<?php echo $value; ?>" style="width:50px;" /></li>
			<?php } ?>
			</ul>			
			<p><input type="radio" id="<?php echo $this->get_field_id('search_global'); ?>_1" name="<?php echo $this->get_field_name('search_global'); ?>" value="1" <?php checked($search_global,1); ?>/> <label for="<?php echo $this->get_field_id('search_global'); ?>_1"><?php _e('Поиск по товарам', 'usam'); ?></label><br />
			<input type="radio" id="<?php echo $this->get_field_id('search_global'); ?>_2" name="<?php echo $this->get_field_name('search_global'); ?>" value="0" <?php checked($search_global,0); ?>/> <label for="<?php echo $this->get_field_id('search_global'); ?>_2"><?php _e('Глобальный поиск', 'usam'); ?></label>
			</p>		
<?php
	}
}

function usam_search_widget( $id = 100 )
{ 		
	$search_box_text = get_option('usam_search_box_text', '');
	$number_items = get_option('usam_search_result_items', 5);	
	$text_lenght  = get_option('usam_search_length_description_items', 100);	
	return USAM_Search_Widgets::results_search_form($id, $number_items, $text_lenght, '',0, $search_box_text);	
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Search_Widgets");' ) );
?>