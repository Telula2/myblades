<?php
/**
 * Виджет выбора диапазона цены
 * @since 3.8
 */
class USAM_Widget_Price_Range extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array(
			'classname'   => 'widget_price_range',
			'description' => __( 'Виджет диапазона цен', 'usam' )
		);		
		parent::__construct( 'usam_price_range', __( 'Диапазон цен', 'usam' ), $widget_ops );	
	}

	/**
	 * Widget Output
	 * @todo Add individual capability checks for each menu item rather than just manage_options.
	 */
	function widget( $args, $instance ) 
	{		
		global $wpdb, $table_prefix;
		
		extract( $args );
		
		echo $before_widget;
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Диапазон цен', 'usam' ) : $instance['title'] );
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		usam_price_range();
		echo $after_widget;	
	}

	/**
	 * Update Widget
	 * @param $new_instance (array) New widget values.
	 * @param $old_instance (array) Old widget values.
	 *
	 * @return (array) New values.
	 */
	function update( $new_instance, $old_instance ) 
	{	
		$instance = $old_instance;
		$instance['title']  = strip_tags( $new_instance['title'] );

		return $instance;		
	}

	/**
	 * Widget Options Form
	 * @param $instance (array) Widget values.
	 */
	function form( $instance ) 
	{		
		global $wpdb;		
		// Defaults
		$instance = wp_parse_args( (array)$instance, array( 'title' => '' ) );		
		// Values
		$title  = esc_attr( $instance['title'] );		
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<?php		
	}

}

add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Price_Range");' ) );

/**
  Функция содержание виджет "диапазон цен". Отображает список ценовых диапазов.
*/
function usam_price_range( $args = null ) 
{
	global $wpdb, $type_price;	
	$args = wp_parse_args( (array)$args, array() );
	
	$product_page = usam_get_url_system_page('products-list');
	$result = $wpdb->get_results( "SELECT DISTINCT CAST(`meta_value` AS DECIMAL) AS `price` FROM " . $wpdb->postmeta . " AS `m` WHERE `meta_key` IN ('_usam_price_".$type_price."') ORDER BY `price` ASC", ARRAY_A );
	
	if ( $result != null ) 
	{
		sort( $result );
		$count = count( $result );
		$price_seperater = ceil( $count / 6 );
		for ( $i = 0; $i < $count; $i += $price_seperater ) {
			$ranges[] = round( $result[$i]['price'], -1 );
		}
		$ranges = array_unique( $ranges );
		
		$final_count = count( $ranges );
		$ranges = array_merge( array(), $ranges );
		$_SESSION['price_range'] = $ranges;
		echo '<ul>';
		for ( $i = 0; $i < $final_count; $i++ ) 
		{
			$j = $i;
			if ( $i == $final_count - 1 )
				echo "<li><a href='" . esc_url(add_query_arg( 'range', $ranges[$i]. '-', $product_page )) . "'>".__( 'до', 'usam' )." " . usam_currency_display( $ranges[$i] ). "</a></li>";
			else if ( $ranges[$i] == 0 )
				echo "<li><a href='" . esc_url(add_query_arg( 'range', '-' . ($ranges[$i+1]-1), $product_page )) . "'>".__( 'после', 'usam' )." " . usam_currency_display( $ranges[$i + 1] ). "</a></li>";
			else 
				echo "<li><a href='".esc_url(add_query_arg( 'range', $ranges[$i]."-".($ranges[$i + 1]-1), $product_page )) ."'>" . usam_currency_display( $ranges[$i] )." - ".usam_currency_display( ($ranges[$i + 1]-1) )."</a></li>";	
		}
		echo "<li><a href='" . esc_url(add_query_arg( 'range', 'all', $product_page ) ) . "'>" . __( 'Показать все', 'usam' ) . "</a></li>";
		echo '</ul>';
	}	
}
?>