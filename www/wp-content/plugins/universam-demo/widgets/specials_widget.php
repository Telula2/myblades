<?php
/**
 * Виджет специальные предложения товаров
 * @since 3.8
 */
class USAM_Widget_Product_Specials extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array(
			'classname'   => 'widget_usam_product_specials',
			'description' => __( 'Виджет специальные предложения товаров', 'usam' )
		);
		parent::__construct( 'usam_product_specials', __( 'Предложения товаров', 'usam' ), $widget_ops );
	}

	/**
	 * Widget Output
	 */
	function widget( $args, $instance ) 
	{
		global $wpdb, $table_prefix;

		extract( $args );

		echo $before_widget;
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Предложения товаров', 'usam' ) : $instance['title'] );
		if ( $title )
			echo $before_title . $title . $after_title;

		usam_specials($args, $instance);
		echo $after_widget;
	}

	/**
	 * Update Widget
	 */
	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['number'] = (int)$new_instance['number'];
		$instance['show_thumbnails'] = (bool)$new_instance['show_thumbnails'];
		$instance['show_description']  = (bool)$new_instance['show_description'];

		return $instance;
	}

	/**
	 * Widget Options Form
	 * @param $instance (array) Widget values.
	 */
	function form( $instance )
	{
		global $wpdb;		
		$instance = wp_parse_args( (array)$instance, array(
			'title' => '',
			'show_description' => false,
			'show_thumbnails' => false,
			'number' => 5
		) );
		
		$title = esc_attr( $instance['title'] );
		$number = (int)$instance['number'];
		$show_thumbnails = (bool)$instance['show_thumbnails'];
		$show_description = (bool)$instance['show_description'];
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Количество продуктов для показа:', 'usam' ); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $number; ?>" size="3" />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'show_description' ); ?>" name="<?php echo $this->get_field_name( 'show_description' ); ?>" <?php echo $show_description ? 'checked="checked"' : ""; ?>>
			<label for="<?php echo $this->get_field_id( 'show_description' ); ?>"><?php _e( 'Показать описание', 'usam' ); ?></label><br />
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'show_thumbnails' ); ?>" name="<?php echo $this->get_field_name( 'show_thumbnails' ); ?>" <?php echo $show_thumbnails ? 'checked="checked"' : ""; ?>>
			<label for="<?php echo $this->get_field_id( 'show_thumbnails' ); ?>"><?php _e( 'Показать миниатюру', 'usam' ); ?></label>
		</p>
<?php
	}
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Product_Specials");' ) );


function usam_specials( $args = null, $instance )
{
	global $type_price;
	$args = wp_parse_args( (array)$args, array( 'number' => 5 ) );
	$siteurl = get_option( 'siteurl' );
	if ( !$number = (int) $instance['number'] )
		$number = 5;

	$show_thumbnails  = isset($instance['show_thumbnails']) ? (bool)$instance['show_thumbnails'] : FALSE;
	$show_description  = isset($instance['show_description']) ? (bool)$instance['show_description'] : FALSE;
	
	$query_vars = array(
		'post_type'   		=> 'usam-product',	
		'post_status' 		=> 'publish',
		'post_parent'		=> 0,		
		'posts_per_page' 	=> $number
	) ;
	$query_vars['meta_query'] = array( array( 'key' => '_usam_old_price_'.$type_price, 'value' => 0, 'type' => 'numeric', 'compare' => '>' ),
									   array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '>', 'type' => 'numeric' ));
									   
	$special_products = usam_get_products( $query_vars );
	$output = '';	
	if ( count( $special_products ) > 0 ) 
	{
		foreach( $special_products as $product ) 
		{			
			$link = usam_product_url( $product->ID );//usam_product_url
			$title = usam_the_product_title( $product->ID );
			
			if( $show_thumbnails ):
				?>
				<a href="<?php echo $link; ?>"><?php echo usam_get_product_thumbnail( $product->ID, 'product-image', $title); ?></a>
			<?php endif; ?>		
			<br />			
			<strong><a class="usam_product_title" href="<?php echo $link; ?>"><?php echo $title; ?></a></strong><br />
			<span id="special_product_price_<?php echo $product->ID; ?>"><?php echo usam_get_product_price_currency( $product->ID ); ?></span><br />			
			
			<?php 
			if( $show_description ): ?>
				<div class="usam-special-description">
					<?php echo usam_the_product_description( $product->ID ); ?>
				</div>
			<?php 
			endif; 
		}
	}
}
?>