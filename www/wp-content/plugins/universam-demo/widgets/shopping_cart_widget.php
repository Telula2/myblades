<?php
/**
 * Класса виджет корзина
 */
class USAM_Widget_Shopping_Cart extends WP_Widget
{
	/**
	 * Конструктор виджета
	 */
	function __construct() 
	{
		$widget = array(
			'classname'   => 'widget_usam_shopping_cart',
			'description' => __( 'Виджет корзины', 'usam' )
		);
		parent::__construct( 'usam_shopping_cart', __( 'Корзина', 'usam' ), $widget );
	}
	
	function widget( $args, $instance )
	{
		global $cache_enabled;
		extract( $args );
		
		$fancy_collapser = '';
		if ( !empty($instance['usam_widget_show_sliding_cart']) ) 
		{
			if ( isset($_SESSION['slider_state']) && is_numeric( $_SESSION['slider_state'] ) ) 
			{
				if ( $_SESSION['slider_state'] == 0 ) 
					$collapser_image = 'plus.png';
				else 
					$collapser_image = 'minus.png';				
				$fancy_collapser = ' <a href="#" onclick="return shopping_cart_collapser()" id="fancy_collapser_link"><img src="' . USAM_CORE_IMAGES_URL . '/' . $collapser_image . '" title="" alt="" id="fancy_collapser" /></a>';
			} 
			else 
			{
				if ( isset($_SESSION['usam_files_cart']) && $_SESSION['usam_files_cart'] == null ) 				
					$collapser_image = 'plus.png';				
				else 				
					$collapser_image = 'minus.png';				
				$fancy_collapser = ' <a href="#" onclick="return shopping_cart_collapser()" id="fancy_collapser_link"><img src="' . USAM_CORE_IMAGES_URL . '/' . $collapser_image . '" title="" alt="" id="fancy_collapser" /></a>';
			}
		}		
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Корзина', 'usam' ) : $instance['title'] );
		echo $before_widget;

		//if ( $title )
		//	echo $before_title . $title . $fancy_collapser . $after_title;

		$display_state = '';
		if ( ( ( isset( $_SESSION['slider_state'] ) && ( $_SESSION['slider_state'] == 0 ) ) || ( usam_get_basket_number_items() < 1 ) ) && ( get_option( 'usam_widget_show_sliding_cart' ) == 1 ) )
			$display_state = 'style="display: none;"';

		$use_object_frame = false;
		if ( ( $cache_enabled == true ) && ( !defined( 'DONOTCACHEPAGE' ) || ( constant( 'DONOTCACHEPAGE' ) !== true ) ) ) 
		{			
			echo '<div id="sliding_cart" class="shopping-cart-wrapper">';
			if ( ( strstr( $_SERVER['HTTP_USER_AGENT'], "MSIE" ) == false ) && ( $use_object_frame == true ) ) 
			{
				?>
				<object codetype="text/html" type="text/html" data="index.php?usam_action=cart_html_page" border="0">
					<p><?php _e( 'Загрузка...', 'usam' ); ?></p>
				</object>
				<?php
			} else {
				?>
				<div class="usam_cart_loading"><p><?php _e( 'Загрузка...', 'usam' ); ?></p></div>
				<?php
			}
			echo '</div>';
		} 
		else 
		{	
			ob_start();
			echo '<div id="sliding_cart" class="shopping-cart-wrapper" '.$display_state.'>';
			usam_include_template_file( 'widget-basket' );
			echo '</div>';
		}		
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['usam_widget_show_sliding_cart']  = strip_tags( $new_instance['usam_widget_show_sliding_cart'] );
		return $instance;
	}
	
	function form( $instance )
	{	
		$instance = wp_parse_args( (array)$instance, array(
			'title' => __( 'Корзина', 'usam' ),
			'usam_widget_show_sliding_cart' => 0
		) );		
		$title = esc_attr( $instance['title'] );
		$show_sliding_cart = esc_attr( $instance['usam_widget_show_sliding_cart'] );
		if( 1 == $show_sliding_cart)
			$show_sliding_cart = 'checked="checked"';
		else
			$show_sliding_cart = '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<input type='hidden' name="<?php echo $this->get_field_name( 'usam_widget_show_sliding_cart' ); ?>" value='0' />
		<p>			
			<label for="<?php echo $this->get_field_id('usam_widget_show_sliding_cart'); ?>"><?php _e( 'Использование раздвижной корзины:', 'usam' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'usam_widget_show_sliding_cart' ); ?>" name="<?php echo $this->get_field_name( 'usam_widget_show_sliding_cart' ); ?>" type="checkbox" value="1" <?php echo $show_sliding_cart; ?> />
		</p>		
		<?php
	}
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Shopping_Cart");' ) );

function usam_widget_cart( $args, $instance )
{
	$widget = new USAM_Widget_Shopping_Cart();
	$widget->widget( $args, $instance );
}
?>