<?php
/**
 * Класс виджета меню админа 
 * @since 3.8
 */
class USAM_Widget_Admin_Menu extends WP_Widget 
{
	function __construct()
	{
		$widget_ops = array(
			'classname'   => 'widget_usam_admin_menu',
			'description' => __( 'Виджет меню администратора', 'usam' )
		);		
		parent::__construct( 'usam_admin_menu', __( 'Меню администратора', 'usam' ), $widget_ops );	
	}

	/**
	 * Widget Output
	 * @param $args (array)
	 * @param $instance (array) Widget values.	
	 */
	function widget( $args, $instance ) 
	{		
		extract( $args );
	
		if ( current_user_can( 'manage_options' ) ) {
			echo $before_widget;
			$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Меню администратора', 'usam' ) : $instance['title'] );
			if ( $title ) {
				echo $before_title . $title . $after_title;
			}
			$this->admin_menu();
			echo $after_widget;
		}	
	}

	/**
	 * Update Widget	
	 * @param $new_instance (array) New widget values.
	 * @param $old_instance (array) Old widget values.
	 *
	 * @return (array) New values.
	 */
	function update( $new_instance, $old_instance ) 
	{	
		$instance = $old_instance;
		$instance['title']  = esc_attr( strip_tags( $new_instance['title'] ) );
		return $instance;		
	}

	/**
	 * Widget Options Form
	 * @param $instance (array) Widget values.
	 */
	function form( $instance ) 
	{	
		$instance = wp_parse_args( (array)$instance, array( 'title' => '' ) );	// По умолчанию			
		$title  = esc_attr( $instance['title'] );		
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<?php
		
	}
	
	/**
	 * Admin Menu Widget content function
	 */
	function admin_menu( $args = null ) 
	{		
		if ( current_user_can( 'manage_options' ) ) 
		{
			echo '<ul>';
			echo '<li><a title="'.esc_attr__( 'Добавить новую страницу', 'usam' ).'" href="'.admin_url( 'post-new.php?post_type=page' ).'">'.esc_html__( 'Добавить страницу', 'usam' ). '</a></li>';
			echo '<li><a title="' . esc_attr__( 'Добавить новый товар', 'usam' ) . '" href="' . admin_url( 'post-new.php?post_type=usam-product' ) . '">' . esc_html__( 'Добавить товар', 'usam' ) . '</a></li>';	
			echo '</ul>';
		}	
	}
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Admin_Menu");' ) );
?>