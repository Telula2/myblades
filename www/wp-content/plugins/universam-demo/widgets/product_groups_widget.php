<?php
/**
 * Класс виджета "Группы товаров"
 */
class USAM_Widget_Product_Groups extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array(
			'classname' => 'widget_usam_product_groups',
			'description' => __( 'Виджет групп товаров', 'usam' )
		);
		parent::__construct( 'usam_product_groups', __( 'Группы товаров', 'usam' ), $widget_ops );
	}

	/**
	 * Widget Output	
	 */
	function widget( $args, $instance )
	{
		global $usam_query, $wp_query;
		extract( $args );

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'] );

		echo $before_widget;

		if ( $title )
			echo $before_title . $title . $after_title;
					
		// Получить активную категорию первого уровня			
		$current_default   = get_option( 'usam_default_menu_category', 0 );					
		if ( !empty( $instance['active'] ) )
		{
			$args_category_list = array();
			if ( !empty($usam_query->query['usam-category']))
			{			
				$term_data = get_term_by( 'slug', $usam_query->query['usam-category'] , 'usam-category' );		
				$active_category = $term_data->term_id;	
				$args_category_list = array( 'taxonomy' => 'usam-category', 'select' => array( $active_category ) );	
				
				$terms = get_ancestors( $term_data->term_id, 'usam-category' );
				if ( !empty($terms))	
					foreach ($terms as $term) 
					{			
						$term_data = get_term_by('id', $term, 'usam-category');
						if ( $term_data->term_id == $current_default)
							break;
						$active_category = $term_data->term_id;	
					}
			}		
			else
				$active_category = '';
			$args_category_list['select'] = array( $active_category );	
		}	
		$args_category = array( "hide_empty" => 0, 'child_of' => $current_default, 'update_term_meta_cache' => 0 );
		
		if ( isset($wp_query->query['usam-brands']) || isset($wp_query->query['usam-category_sale']) || 
		   ( isset($wp_query->query['pagename']) && ($wp_query->query['pagename'] == 'new-arrivals' || $wp_query->query['pagename'] == 'sale') ) )
		{	
			$new_query = $usam_query->query_vars;			
			unset($new_query['usam-category']);			
			unset($new_query['taxonomy']);	
			unset($new_query['term']);	
		
			//$new_query['meta_query'] = array();	
			$new_query['nopaging'] = true;
			$new_query['fields'] = 'ids';
			$new_query['cache_results'] = false;
			$new_query['update_post_meta_cache'] = false;
			$new_query['update_post_term_cache'] = false;
			$new_query['post_type'] = 'usam-product';	
		
			$query = new WP_Query( $new_query );		
			$args = array( 'orderby' => 'name', 'fields' => 'ids' );	
			$object_terms = wp_get_object_terms( $query->posts, 'usam-category', $args );		
			
			$terms_ids = array();
			foreach ( $object_terms as $term_id ) 
			{			
				$terms_ids[] = $term_id;
				$terms_ancestors = get_ancestors( $term_id, 'usam-category' );
				$terms_ids = array_merge( $terms_ids, $terms_ancestors );	
			}				
			$args_category['include'] = $terms_ids;		
		}		
		$args_category['orderby'] = 'meta_value_num';
		$args_category['meta_key'] = 'usam_sorting_menu_categories';	
		
		//$args['pad_counts'] = true;	
		
		$args_brands_list = array( 'taxonomy' => 'usam-brands' );
		if ( !empty($usam_query->query['usam-brands']))
		{
			$args_brands_list['select'] = array( $usam_query->query['usam-brands'] );
		}			
		$args_brands = array( "hide_empty" => 0, 'update_term_meta_cache' => 0 );		
		?>
		<div id='product_groups' class = "usam_tabs">
			<div class='header_tab'>
				<ul>
					<li class="tab"><a class ="current" href="#tab_categories"><h3><?php _e( 'Категории', 'usam' ); ?></h3></a></li>
					<li class="tab"><a href="#tab_brands"><h3><?php _e( 'Бренды', 'usam' ); ?></h3></a></li>				
				</ul>
			</div>
		<div class='countent_tabs'>
			<div id="tab_categories" class="tab">
				<?php 
				if ( isset($usam_query->query['usam-brands']) )
				{
					global $usam_query;			
					$url = '';			
					if ( !empty( $usam_query->query_vars['usam-category'] ) )	
						$url = get_term_link( $usam_query->query_vars['usam-category'], 'usam-category' );
					else
					{
						$all_categories = get_terms( 'usam-category', 'hide_empty=0&number=1&orderby=id' );
						if ( !empty( $all_categories ))
							$url = get_term_link( (int)$all_categories[0]->term_id, 'usam-category' );
					}
					echo '<div class="title_categorisation_brand">'.
						__('Категории бренда','usam').': '.$usam_query->query['usam-brands'].'
						<a class = "link_del_brand" href="'.$url.'" title="'. __('Показать все категории', 'usam').'">
							<img src="'.USAM_CORE_IMAGES_URL.'/cross.png"/>	
						</a>
					</div>';
				}					
				echo '<div class="usam_categories_list">'.usam_get_walker_terms_list( $args_category, $args_category_list ).'</div>
			</div>
			<div id="tab_brands" class="tab">
				<div class="usam_categories_list">'.usam_get_walker_terms_list( $args_brands, $args_brands_list ).'</div>
			</div>
		</div>';	
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title']      = strip_tags( $new_instance['title'] );
		$instance['active']      = $new_instance['active'] ? 1 : 0;	
		return $instance;
	}

	function form( $instance ) 
	{				
		$instance = wp_parse_args((array) $instance, array(
			'title' => __( 'Категории товаров', 'usam'  ),		
			'active' => true,			
		));	
		$title    = esc_attr( $instance['title'] );
		$active    = (bool) $instance['active'];	
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Название:', 'usam' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>		
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('active'); ?>" name="<?php echo $this->get_field_name('active'); ?>"<?php checked( $active ); ?>/>
			<label for="<?php echo $this->get_field_id('active'); ?>"><?php _e('Раскрывать активные', 'usam'); ?></label>
		</p>
<?php
	}
}
add_action( 'widgets_init', create_function( '', 'return register_widget("USAM_Widget_Product_Groups");' ) );