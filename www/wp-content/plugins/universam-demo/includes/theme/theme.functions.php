<?php
/**
 * Функции для темы
 * @since 3.7
 */
 
function usam_site_map()
{		
	require_once( USAM_FILE_PATH . '/includes/taxonomy/class-walker-product_category-list.php'     );

	$current_default   = get_option( 'usam_default_menu_category', 0 );	
	$args_menu = array(
				'descendants_and_self' => 0,				
				'taxonomy'             => 'usam-category',				
				'checked_ontop'        => false, 			
				'before' => '', 
				'after' => '',
				'link_before' => '', 
				'link_after' => '',
				'split' => 3,
			);	
	$categories = get_terms( 'usam-category', "hide_empty=0&child_of=$current_default&update_term_meta_cache=0" );				
	
	$walker = new Walker_Product_Category_List();
	echo '<div class="map_categories"><ul class ="main_categories">'.call_user_func_array( array( $walker, 'walk' ), array( $categories, 0, $args_menu ) ).'</ul></div>';		
}


// Проверяет на наличие таксоманий магазина
function usam_is_viewable_taxonomy()
{
	global $wp_query;
	if(isset($wp_query->query_vars['taxonomy']) && ('usam-category' == $wp_query->query_vars['taxonomy'] || 'usam-category_sale' == $wp_query->query_vars['taxonomy'] || 'usam-brands' == $wp_query->query_vars['taxonomy'] || 'product_tag' == $wp_query->query_vars['taxonomy'] ) || isset($wp_query->query_vars['usam-category']) || isset($wp_query->query_vars['usam-brands']))
		return true;
	else
		return false;
}

function _usam_is_in_custom_loop() 
{
	global $usam_query;
	if ( isset($usam_query->usam_in_the_loop) && $usam_query->usam_in_the_loop )
		return true;
	return false;
}


function usam_stylesheet() 
{
	$stylesheet_uri = get_stylesheet_uri();
	$data = get_file_data( $stylesheet_uri, array('ver'=>'Version', 'author'=>'Author', 'name'=>'Theme Name' ) );	
	?>
	<link rel="stylesheet" href="<?php echo $stylesheet_uri.'?'.$data['ver']; ?>" type="text/css" />	
	<?php
}

/**
 * Возвращает ID текущей категории продукта или по умолчанию идентификатор категории, если он установлен. Если не установлен и нет текущей категории, возвращает пустую строку
 */
function usam_get_current_category_id() 
{
	global $wp_query;

	$category_id = '';
	if ( isset($wp_query) && isset($wp_query->query_vars['taxonomy']) && ('usam-category' == $wp_query->query_vars['taxonomy']) || is_numeric(get_option('usam_default_category')) )
		$category_id = isset( $wp_query->query_vars['term'] ) && is_string( $wp_query->query_vars['term'] ) ? usam_get_category_id( $wp_query->query_vars['term'], 'slug' ) : get_option( 'usam_default_category' );
	return $category_id;
}


function usam_product_list_rss_feed() 
{
	$rss_url = get_option('siteurl');
	$rss_url = add_query_arg( 'usam_action', 'rss', $rss_url );
	$rss_url = str_replace('&', '&amp;', $rss_url);
	$rss_url = esc_url( $rss_url ); // URL santization - IMPORTANT!

	echo "<link rel='alternate' type='application/rss+xml' title='" . get_option( 'blogname' ) .' '.__('Список товаров','usam')."' href='{$rss_url}'/>";
}
add_action( 'wp_head', 'usam_product_list_rss_feed' );

/**
 * Возвращает URL динамического пользовательского CSS
 * @since 3.8.9
 */
function usam_get_dynamic_user_css_url() 
{
	$uploads_dir     = wp_upload_dir();
	$upload_folder   = $uploads_dir['path'];

	if ( is_writable( $upload_folder ) && file_exists( $upload_folder . '/usam_cached_styles.css' ) )
		return add_query_arg( 'timestamp', get_option( 'usam_dynamic_css_hash', time() ), $uploads_dir['url'] . '/usam_cached_styles.css' );
	if ( ! is_writable( $upload_folder ) )
		return add_query_arg( 'usam_user_dynamic_css', 'true', home_url( 'index.php' ) );
	if ( is_writable( $upload_folder ) && ! file_exists( $upload_folder . '/usam_cached_styles.css' ) )
		return usam_cache_to_upload();
}

/**
 * Перемещение динамически с генерированный вход в файл в папке добавления.  
 * Также обновляет CSS хэш метки. Отметка времени добавляется к URL
 * @since 3.8.9
 */
function usam_cache_to_upload() 
{
	$uploads_dir     = wp_upload_dir();
	$upload_folder   = $uploads_dir['path'];
	$path            = $upload_folder . '/usam_cached_styles.css';
	if ( ! is_writable( $upload_folder ) )
		return false;
	if ( false === file_put_contents( $path, usam_get_user_dynamic_css() ) )
		return false;
	$timestamp = time();
	update_option( 'usam_dynamic_css_hash', $timestamp );
	return add_query_arg( 'timestamp', $timestamp, $uploads_dir['url'] . '/usam_cached_styles.css' );
}
add_action( 'update_option_product_image_width'     , 'usam_cache_to_upload' );
add_action( 'update_option_product_image_height'    , 'usam_cache_to_upload' );
add_action( 'update_option_single_view_image_width' , 'usam_cache_to_upload' );
add_action( 'update_option_single_view_image_height', 'usam_cache_to_upload' );
add_action( 'update_option_category_image_width'    , 'usam_cache_to_upload' );
add_action( 'update_option_category_image_height'   , 'usam_cache_to_upload' );

/**
 * Вывести динамический CSS. Эта функция находится в ведении либо когда динамического URL, или когда мы должны захватить новый CSS к кэш-памяти.
 * @since 3.8.9
 * @return CSS
 */
function usam_user_dynamic_css() 
{
	header( 'Content-Type: text/css' );
	header( 'Expires: ' . gmdate( 'r', mktime( 0, 0, 0, date( 'm' ), ( date( 'd' ) + 12 ), date( 'Y' ) ) ) );
	header( 'Cache-Control: public, must-revalidate, max-age=86400' );
	header( 'Pragma: public' );
	echo usam_get_user_dynamic_css();
	exit;
}
if ( isset( $_GET['usam_user_dynamic_css'] ) && 'true' == $_GET['usam_user_dynamic_css'] )
	add_action( 'plugins_loaded', 'usam_user_dynamic_css', 1 );

/**
 * Возвращает динамический CSS в виде строки. Эта функция находится в ведении либо когда динамический URL, или когда мы должны захватить новый CSS из кэш-памяти.
 * @since 3.8.9
 */
function usam_get_user_dynamic_css() 
{
	global $wpdb;

	ob_start();

	if ( ! defined( 'USAM_DISABLE_IMAGE_SIZE_FIXES' ) || (constant( 'USAM_DISABLE_IMAGE_SIZE_FIXES' ) != true ) ) 
	{
		$product_image = get_option( 'usam_product_image' );		
		$thumb_width = $product_image['width'];

		if ( $thumb_width <= 0 )
			$thumb_width = 96;
		
		$thumb_height = $product_image['height'];

		if ( $thumb_height <= 0 )
			$thumb_height = 96;

		$single_view_image = get_option( 'usam_single_view_image' );
		$single_thumb_width  = $single_view_image['width'];
		$single_thumb_height = $single_view_image['height'];	

		if ( $single_thumb_width <= 0 )
			$single_thumb_width = 128;

		$category_image = get_option( 'usam_category_image' );
		$category_width = $category_image['width'];
		$category_height = $category_image['height'];
?>
		/*
		* По умолчанию стили просмотра
		*/	
		.usam_category_grid_item  { display:block; float:left; width: <?php echo $category_width; ?>px;	height: <?php echo $category_height; ?>px;}
		.usam_category_grid_item  span{ position:relative; top:<?php echo ($thumb_height - 2)/9; ?>px; }
		div.default_product_display div.item_no_image a  { width: <?php echo $thumb_width - 2; ?>px;}
		div.default_product_display .imagecol img.no-image, #content div.default_product_display .imagecol img.no-image { width: <?php echo $thumb_width; ?>px;	height: <?php echo $thumb_height; ?>px; }
		<?php do_action( 'usam_dynamic_css' ); ?>
		/*
		* Single View Styling
		*/		
		div.single_product_display div.item_no_image  {	width: <?php echo $single_thumb_width - 2; ?>px; height: <?php echo $single_thumb_height - 2; ?>px; }
		div.single_product_display div.item_no_image a  { width: <?php echo $single_thumb_width - 2; ?>px; }

<?php
	}
	if ( (isset($_GET['brand']) && is_numeric( $_GET['brand'] )) ) 
	{
		$brandstate = 'block';
		$categorystate = 'none';
	} else {
		$brandstate = 'none';
		$categorystate = 'block';
	}
?>
	div#branddisplay{ display: <?php echo $brandstate; ?>; }
<?php
	$css = ob_get_contents();
	ob_end_clean();
	return $css;	
}


/**
 *  функция изменяет заголовок страницы категории или продукта
 * @return string - новое название страницы
 */
function usam_obtain_the_title() 
{
	global $wpdb, $wp_query;
	$output = null;
	$category_id = null;
	if( !isset( $wp_query->query_vars['usam-category']) &&  !isset( $wp_query->query_vars['usam-product']))
		return;

	if ( !isset( $wp_query->query_vars['usam-category'] ) && isset($wp_query->query_vars['usam-product']) )
		$wp_query->query_vars['usam-category'] = 0;


	if ( isset( $wp_query->query_vars['taxonomy'] ) && 'usam-category' ==  $wp_query->query_vars['taxonomy'] || isset($wp_query->query_vars['usam-category']))
		$category_id = usam_get_the_category_id($wp_query->query_vars['usam-category'],'slug');

	if ( $category_id > 0 ) 
	{
		if ( isset( $usam_title_data['category'][$category_id] ) ) {
			$output = $usam_title_data['category'][$category_id];
		} 
		else 
		{
			$term = get_term($category_id, 'usam-category');
			$output = $term->name;
			$usam_title_data['category'][$category_id] = $output;
		}
	}
	if ( !isset( $_GET['usam-product'] ) )
		$_GET['usam-product'] = 0;

	if ( !isset( $wp_query->query_vars['usam-product'] ) )
		$wp_query->query_vars['usam-product'] = '';

	if ( isset( $wp_query->query_vars['usam-product'] ) || is_string( $_GET['usam-product'] ) ) 
	{
		$product_name = $wp_query->query_vars['usam-product'];
		if ( isset( $usam_title_data['product'][$product_name] ) ) {
			$product_list = array( );
			$full_product_name = $usam_title_data['product'][$product_name];
		} 
		else if ( $product_name != '' ) 
		{
			$product_id = $wp_query->post->ID;
			$full_product_name = $wpdb->get_var( $wpdb->prepare( "SELECT `post_title` FROM `$wpdb->posts` WHERE `ID`= %d LIMIT 1", $product_id ) );
			$usam_title_data['product'][$product_name] = $full_product_name;
		} 
		else 
		{
			if( isset($_REQUEST['product_id']))
			{
				$product_id = absint( $_REQUEST['product_id'] );
				$product_name = $wpdb->get_var( $wpdb->prepare( "SELECT `post_title` FROM `$wpdb->posts` WHERE `ID`= %d LIMIT 1", $product_id ) );
				$full_product_name = $wpdb->get_var( $wpdb->prepare( "SELECT `post_title` FROM `$wpdb->posts` WHERE `ID`= %d LIMIT 1", $product_id ) );
				$usam_title_data['product'][$product_name] = $full_product_name;
			}
			else
			{				
				$category = $wp_query->query_vars['usam-category'];
				$cat_term = get_term_by('slug',$wp_query->query_vars['usam-category'], 'usam-category');
				$full_product_name = $cat_term->name;
			}
		}
		$output = $full_product_name;
	}
	if ( isset( $full_product_name ) && ($full_product_name != null) )
		$output = esc_html(  $full_product_name );
	$seperator = ' | ';
	$seperator = apply_filters('usam_the_wp_title_seperator' , $seperator);
	return $output.$seperator;
}

// Описание: Получить название страницы
function usam_get_page_name() 
{	
	global $post, $wp_query;		

	$obj = $wp_query->get_queried_object();	
	$name = '';		
	if( is_single() )		
	{ 
		$name = $obj->post_title;
	}		
	elseif( !empty($obj->term_id) )		
	{				
		$name = $obj->name;
	}	
	elseif ( $obj->post_type == 'page' || true )
	{		
		$name = $obj->post_title;		
	}	
	return $name;
}

// Описание название страницы
function usam_page_name_html() 
{		
	echo '<h1 class ="usam_page_name">'.usam_get_page_name().'</h1>';
}

function usam_feedback_link( $type, $title ) 
{		
	echo "<a href='#{$type}' class = 'usam_modal_feedback'>$title</a>";
}

/**
 * Создает канонические ссылки
 * @since 3.7
 */
function usam_change_canonical_url( $url = '' ) 
{
	global $wpdb, $wp_query;
	if ( $wp_query->is_single == true && 'usam-product' == $wp_query->query_vars['post_type'] && !empty($wp_query->get_queried_object()->ID) )
		$url = get_permalink( $wp_query->get_queried_object()->ID );	
	return apply_filters( 'usam_change_canonical_url', $url );
}

function usam_insert_canonical_url() 
{
	$usam_url = usam_change_canonical_url( null );
	echo "<link rel='canonical' href='$usam_url' />\n";
}
?>