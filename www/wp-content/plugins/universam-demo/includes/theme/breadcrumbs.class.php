<?php
/**
* @return boolean
*/
function usam_has_breadcrumbs() 
{ 
	global $usam_breadcrumbs;	
	$usam_breadcrumbs = new USAM_Breadcrumbs();
	if( ($usam_breadcrumbs->breadcrumb_count > 0) && (get_option('usam_show_breadcrumbs', 1) == 1) )
		return true;
	else
		return false;	
}

function usam_have_breadcrumbs()
{
	global $usam_breadcrumbs;	
	return $usam_breadcrumbs->have_breadcrumbs();
}

function usam_the_breadcrumb() 
{
	global $usam_breadcrumbs;	
	$usam_breadcrumbs->the_breadcrumb();
}

function usam_breadcrumb_name() 
{
	global $usam_breadcrumbs;
	return $usam_breadcrumbs->breadcrumb['name'];
}


function usam_breadcrumb_slug() 
{
	global $usam_breadcrumbs;
	return (isset($usam_breadcrumbs->breadcrumb['slug']) ? $usam_breadcrumbs->breadcrumb['slug'] : '');
}

/**
* Получить ссылку на объект
*/
function usam_breadcrumb_url() 
{
	global $usam_breadcrumbs;	

	if($usam_breadcrumbs->breadcrumb['url'] == '') 	
		return false;
	else 
		return $usam_breadcrumbs->breadcrumb['url'];	
}

/**
* Вывод "хлебных крошек"
*/
function usam_output_breadcrumbs( $options = null )
{
	global $wp_query;
	
	if ( !usam_has_breadcrumbs() )
		return;

	// Настройки по умолчанию
	$options = apply_filters( 'usam_output_breadcrumbs_options', $options );
	$options = wp_parse_args( (array)$options, array(
		'before-breadcrumbs' => '<div class="usam-breadcrumbs">',
		'after-breadcrumbs'  => '</div>',
		'before-crumb'       => '',
		'after-crumb'        => '',
		'crumb-separator'    => ' &raquo; ',
		'show_home_page'     => true,
		'show_products_page' => true,
		'echo'               => true
	) );
	
	$products_page_id = usam_get_system_page_id( 'products-list' );
	$sale_page_id = usam_get_system_page_id('sale');
	$newarrivals_page_id = usam_get_system_page_id('new-arrivals');
	$id = $wp_query->get_queried_object_id();
	
	switch ( $id ) 
	{		
		case $sale_page_id:
			$page	 = get_the_title( $sale_page_id );	
		break;
		case $newarrivals_page_id:
			$page	 = get_the_title( $newarrivals_page_id );	
		break;
		case $products_page_id:
		default:
			$page	 = get_the_title( $products_page_id );	
		break;	
	}
	
	$filtered_products_page = array(
		'url'  => usam_get_url_system_page('products-list'),
		'name' => apply_filters ( 'the_title', $page )
	);
	$filtered_products_page = apply_filters( 'usam_change_pp_breadcrumb', $filtered_products_page );
	
	$output = '';
	
	// Если это страница (главная, страница распродаж, новинки...), то показать её, а не каталог ссылок
	if ( get_option( 'page_on_front' ) != $products_page_id && $options['show_home_page'] ) 
	{
		$output .= $options['before-crumb'];
		$output .= '<a class="usam-crumb" id="usam-crumb-home" href="' . get_option( 'home' ) . '">' . __( 'Главная', 'usam' ) . '</a>';
		$output .= $options['after-crumb'];
	}
	if (  $id == $products_page_id || $id == $sale_page_id || $id == $newarrivals_page_id )
	{		
		$output .= $options['crumb-separator'];
		$output .= '<span class="usam-crumb" id="usam-crumb-' . $id . '">' . $page . '</span>';
		$output .= $options['after-crumb'];		
	}	
	else	
	{
		while ( usam_have_breadcrumbs() )
		{
			usam_the_breadcrumb();			
			if ( !empty( $output ) )
				$output .= $options['crumb-separator'];			
			$output .= $options['before-crumb'];					
			if ( usam_breadcrumb_url() )				
				$output .= '<a class="usam-crumb" id="usam-crumb-' . usam_breadcrumb_slug() . '" href="' . usam_breadcrumb_url() . '">' . usam_breadcrumb_name() . '</a>';
			else
				$output .= '<span class="usam-crumb" id="usam-crumb-' . usam_breadcrumb_slug() . '">' . usam_breadcrumb_name() . '</span>';		
			$output .= $options['after-crumb'];	
		}
	}
	$output = $options['before-breadcrumbs'] . apply_filters( 'usam_output_breadcrumbs', $output, $options ) . $options['after-breadcrumbs'];
	if ( $options['echo'] )
		echo $output;
	else
		return $output;	
}

class USAM_Breadcrumbs
{
	private $breadcrumbs = array();
	public  $breadcrumb_count = 0;
	public  $current_breadcrumb = -1;
	public  $breadcrumb;
	
	public function __construct( )
	{
		global $wp_query, $usam_query;	
		$query_data = array();
		if ( isset($wp_query->query_vars['post_type']) && 'usam-product' == $wp_query->query_vars['post_type'] && isset($wp_query->post)) 
			$query_data['product'] = $wp_query->post->post_title;

		if ( !empty($usam_query->query_vars['usam-category']) ) 
			$query_data['category'] = $usam_query->query_vars['usam-category']; 
			
		if ( !empty($usam_query->query_vars['usam-brands']) ) 
			$query_data['usam-brands'] = $usam_query->query_vars['usam-brands']; 
		
		if(!empty($query_data['product']) && !empty($wp_query->post))
		{
			$this->breadcrumbs[] = array(
				'name' => htmlentities($wp_query->post->post_title, ENT_QUOTES, 'UTF-8'),
				'url' => '',
				'slug' => $wp_query->post->post_name
			);
		}			
		if( !empty($wp_query->post_count) && 1 == $wp_query->post_count )
		{		
			$categories = get_the_terms( $wp_query->post->ID , 'usam-category' );
			if( $categories )
			{
				$count_categories = count($categories);			
				if( $count_categories > 1 && isset($usam_query->query_vars['usam-category']))
					$query_data['category'] = $usam_query->query_vars['usam-category'];
				elseif( $count_categories > 0)
					$query_data['category'] = current($categories)->slug;	
			}
		}
		if( isset( $query_data['category'] ) )			
		{	
			$term_data = get_term_by('slug', $query_data['category'], 'usam-category');		
			if (empty($query_data['product'])) 		
			{			
				$this->breadcrumbs[] = array(
					'name' => htmlentities( $term_data->name, ENT_QUOTES, 'UTF-8'),
					'url' => '',
					'slug' => $term_data->slug
				);						
			}
			else
				$this->breadcrumbs[] = array(
					'name' => htmlentities( $term_data->name, ENT_QUOTES, 'UTF-8'),
					'url' => get_term_link( $term_data->term_id, 'usam-category'),
					'slug' => $term_data->slug
				);					
			$i = 0;	
			$default_menu_category = get_option( 'usam_default_menu_category' );					
			while(($term_data->parent > 0) && ($i <= 10))
			{				
				$term_data = get_term($term_data->parent, 'usam-category');
				if ( $default_menu_category == $term_data->term_id )
					break;				
				$this->breadcrumbs[] = array(
					'name' => htmlentities( $term_data->name, ENT_QUOTES, 'UTF-8'),
					'url' => get_term_link( $term_data->term_id, 'usam-category')
				);
				$i++;
			}		
		}			
		if ( !empty($usam_query->query_vars['usam-category_sale']) ) 
		{
			$term_data = get_term_by('slug', $usam_query->query_vars['usam-category_sale'], 'usam-category_sale');
			$this->breadcrumbs[] = array(
						'name' => htmlentities( $term_data->name, ENT_QUOTES, 'UTF-8'),
						'url' => get_term_link( $term_data->term_id, 'usam-category_sale'),
						'slug' => $term_data->slug
					);
		}
		elseif ( !empty($usam_query->query_vars['usam-brands']) ) 
		{
			$term_data = get_term_by('slug', $query_data['usam-brands'], 'usam-brands');
			$this->breadcrumbs[] = array(
						'name' => htmlentities( $term_data->name, ENT_QUOTES, 'UTF-8'),
						'url' => get_term_link( $term_data->term_id, 'usam-brands'),
						'slug' => $term_data->slug
					);
		}
		elseif ( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'sale' ) //Если страница распродаж
		{
			$sale_page_id = usam_get_system_page_id('sale');	
			$page	 = get_the_title( $sale_page_id );
			$this->breadcrumbs[] = array(
					'name' => htmlentities( $page, ENT_QUOTES, 'UTF-8'),
					'url' => get_permalink($sale_page_id)
				);
		}
		elseif ( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'new-arrivals' ) //Если страница новинки		
		{
			$newarrivals_page_id = usam_get_system_page_id('new-arrivals');
			$page	 = get_the_title( $newarrivals_page_id );
			$this->breadcrumbs[] = array(
					'name' => htmlentities( $page, ENT_QUOTES, 'UTF-8'),
					'url' => get_permalink($newarrivals_page_id)
				);
		}				
		$this->breadcrumbs = apply_filters( 'usam_breadcrumbs', array_reverse( $this->breadcrumbs ) );
		$this->breadcrumb_count = count($this->breadcrumbs);
	}	
	
	function next_breadcrumbs() 
	{
		$this->current_breadcrumb++;
		$this->breadcrumb = $this->breadcrumbs[$this->current_breadcrumb];
		return $this->breadcrumb;
	}	
	
	function the_breadcrumb() 
	{
		$this->breadcrumb = $this->next_breadcrumbs();
	}
	
	function have_breadcrumbs() 
	{
		if ($this->current_breadcrumb + 1 < $this->breadcrumb_count) {
			return true;
		} else if ($this->current_breadcrumb + 1 == $this->breadcrumb_count && $this->breadcrumb_count > 0) {
			$this->rewind_breadcrumbs();
		}
		return false;
	}
	
	function rewind_breadcrumbs() 
	{
		$this->current_breadcrumb = -1;
		if ($this->breadcrumb_count > 0) {
			$this->breadcrumb = $this->breadcrumbs[0];
		}
	}	
}
?>