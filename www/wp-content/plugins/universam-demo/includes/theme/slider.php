<?php
/**
 * Функции для управления складами
 */
 
 /**
 * Клас управления складом
 */ 
class USAM_Slider
{
	 // строковые
	private static $string_cols = array(		
		'name',		
		'type',			
		'setting',
		'template',
	);
	
	// цифровые
	private static $int_cols = array(
		'id',		
		'active',		
	);
	// рациональные
	private static $float_cols = array(		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		
	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'code'  )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_slider' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		if ( $col === 'slides' )
			return true;
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_storage ) 
	{
		$id = $_storage->get( 'id' );	
		wp_cache_set( $id, $_storage->data, 'usam_slider' );			
		do_action( 'usam_slider_update_cache', $_storage );
	}

	/**
	 * Удалить кеш	 
	 */
	public function delete_cache( ) 
	{
		wp_cache_delete( $this->get( 'id' ), 'usam_slider' );	
		wp_cache_delete( $this->get( 'id' ), 'usam_slides' );	
		do_action( 'usam_slider_delete_cache', $this );	
	}
	
	public function delete( ) 
	{		
		global  $wpdb;
		
		$id = $this->get( 'id' );	
		
		do_action( 'usam_slider_before_delete', $this );
		
		self::delete_cache( );	
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SLIDES." WHERE slider_id = '$id'");
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SLIDER." WHERE id = '$id'");
		
		do_action( 'usam_slider_delete', $id );
		
		return $result;
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_SLIDER." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{		
			$this->exists = true;
			
			$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_SLIDES." WHERE slider_id = %d ORDER BY sort", $value );			
			$data['setting'] = maybe_unserialize($data['setting']);			
		
			$this->data = apply_filters( 'usam_slider_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_slider_fetched', $this );	
		$this->fetched = true;			
	}
	
	public function get_slides()
	{
		$object_type = 'usam_slides';	
		$slider_id = $this->get( 'id' );	
		$cache = wp_cache_get( $slider_id, $object_type );			
		if ( $cache === false )			
		{							
			global $wpdb;	
			$sql = "SELECT * FROM ".USAM_TABLE_SLIDES." WHERE slider_id = %d";
			$cache = $wpdb->get_results( $wpdb->prepare( $sql, $slider_id) );

			wp_cache_set( $slider_id, $cache, $object_type );						
		}	
		return $cache;
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_slider_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_slider_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{				
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();
		
		foreach ( $properties as $key => &$value ) 
		{						
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;			
		}	
		$this->data = apply_filters( 'usam_slider_set_properties', $this->data, $this );			
		return $this;
	}
		
	
	private function get_data_format_slides( $data ) 
	{
		$formats = array();
		foreach ( $data as $key => $value ) 
		{			
			$format = false;
			
			if ( in_array( $key, array('title','description', 'link', 'fon', 'setting','interval_from','interval_to') ) )
				$format = '%s';
			elseif ( in_array( $key, array('id','slider_id','object_id', 'sort') ) )
				$format = '%d';
			
			if ( $format !== false )		
				$formats[$key] = $format;			
		}
		return $formats;
	}	

	public function insert_slides( $slide ) 
	{
		global $wpdb;
		
		$slide['slider_id'] = $this->get('id');
		
		$format = $this->get_data_format_slides( $slide );	
		if ( empty($slide['interval_from']) )
			$slide['interval_from'] = NULL;
		
		$result = $wpdb->insert( USAM_TABLE_SLIDES, $slide, $format );
		return $wpdb->insert_id;
	}	
	
	public function update_slides( $slide ) 
	{
		global $wpdb;

		if ( empty($slide) || count($slide) < 2 )
			return false;
		
		$where = array( 'id' => $slide['id'] );			
		$format = $this->get_data_format_slides( $slide );		

		foreach ( $format as $key => $value ) 
		{
			if ( $slide[$key] == null )
			{
				$str[] = "`$key` = NULL";
				unset($slide[$key]);
			}
			else
				$str[] = "`$key` = '$value'";	
		}			
		$sql = "UPDATE `".USAM_TABLE_SLIDES."` SET ".implode( ', ', $str )." WHERE id='%d' ";
		$result = $wpdb->query( $wpdb->prepare( $sql, array_merge( array_values( $slide ), array( $slide['id'] ) ) ) );	
		return $result;		
	}
		
	public function save_slides( $slides ) 
	{	
		foreach ( $slides as $slide ) 
		{			
			if ( !empty($slide['id']) )
				$this->update_slides( $slide );
			else
				$this->insert_slides( $slide );
		}	
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_slider_pre_save', $this );	
		$where_col = $this->args['col'];	
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );			
			
			do_action( 'usam_slider_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_val);

			$this->data = apply_filters( 'usam_slider_update_data', $this->data );			
			$format = $this->get_data_format( );
			$this->data_format( );
			$data = $this->data;			
			$data['setting'] = maybe_serialize($data['setting']);				
					
			$result = $wpdb->update( USAM_TABLE_SLIDER, $data, $where, $format, array($where_format) );		
			do_action( 'usam_slider_update', $this );
		} 
		else 
		{   
			do_action( 'usam_slider_pre_insert' );		
			unset( $this->data['id'] );	
		
			if ( !isset($this->data['name']) )		
				$this->data['name'] = '';

			if ( !isset($this->data['active']) )		
				$this->data['active'] = 0;			

			if ( !isset($this->data['type']) )		
				$this->data['type'] = 'i';
			
			if ( !isset($this->data['setting']) )		
				$this->data['setting'] = '';
			
			$this->data = apply_filters( 'usam_slider_insert_data', $this->data );			
			$format = $this->get_data_format( );
			$this->data_format( );
			$data = $this->data;
			$data['setting'] = maybe_serialize($data['setting']);			
			
			$result = $wpdb->insert( USAM_TABLE_SLIDER, $data, $format );	
			if ( $result ) 
			{				
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;

				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_slider_insert', $this );
		} 					
		do_action( 'usam_slider_save', $this );

		return $result;
	}
}

// Обновить
function usam_update_slider( $id, $data )
{
	$slider = new USAM_Slider( $id );
	$slider->set( $data );
	return $slider->save();
}

// Получить
function usam_get_slider( $id )
{
	$slider = new USAM_Slider( $id );
	$data = $slider->get_data( );
	if ( empty($data) )
		return array();
	
	$data['slides'] = $slider->get_slides();	
	usort($data['slides'], function($a, $b){  return ($a->sort - $b->sort); });
	
	return $data;	
}

// Добавить
function usam_insert_slider( $data )
{
	$slider = new USAM_Slider( $data );
	$result = $slider->save();
	if ( $result )
		return $slider->get( 'id' );
	else
		return false;
}

// Удалить
function usam_delete_slider( $id )
{
	$slider = new USAM_Slider( $id );
	return $slider->delete();
}

function usam_delete_slides( $slide_id ) 
{
	global $wpdb;

	$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SLIDES." WHERE id = '$slide_id'");
	return $result;		
}


function usam_display_slider( $number = 1 )
{		
	global $user_ID;
	
	$filename = 'slider.php';
	$slides_style = 'slider.css';

	$slider = usam_get_slider( $number );
	if ( empty($slider) ) 
		return;		
	
	if ( !empty($slider['setting']['condition']) ) 
	{
		if ( !usam_conditions_user( $slider['setting']['condition'] ) )
			return ;
	} 
	$current_time = current_time('timestamp');	
	$slides = array();
	foreach( $slider['slides'] as $key => $slide )
	{					
		if ( ( !empty($slide->interval_to) && strtotime($slide->interval_to) < $current_time ) || ( !empty($slide->interval_from) && strtotime($slide->interval_from) > $current_time ) )
		{			
			continue;
		}		
		if (  $slider['type'] == 'I' )
			$slides[] = array('id' => $slide->id, 'object_id' => $slide->object_id, 'title' => $slide->title, 'description' => $slide->description, 'link' => $slide->link, 'fon' => $slide->fon);
		elseif (  $slider['type'] == 'P' )
		{			
			$post_title = get_the_title( $slide->object_id );			
			$thumbnail_id = get_post_thumbnail_id($slide->object_id);
			$link = usam_product_url( $slide->object_id );
			
			$slides[] = array('id' => $slide->id, 'object_id' => $thumbnail_id, 'title' => $post_title, 'description' => $slide->description, 'link' => $link, 'fon' => $slide->fon);
		}
	}		
	$file_path = usam_get_module_template_file( 'slidertemplate', $slider['template'] );	
	if ( $file_path )
		include( $file_path );	
	else
		return false;
	$style_url = usam_get_module_template_url( 'slidertemplate', $slider['template'] );
	wp_enqueue_script( 'usam-slider' );
	
	if ( !empty($slider['setting']['button']['styling']) )
		$button_styling = $slider['setting']['button']['styling'];
	else
		$button_styling = '';
	
	if ( !empty($slider['setting']['autospeed']) )
		$autospeed = $slider['setting']['autospeed'];
	else
		$autospeed = 100;	
	?>	
	<link type="text/css" media="screen" rel="stylesheet" href="<?php echo $style_url; ?>"/>			
	<script type="text/javascript">
		var $featuredArea = jQuery('#featured #slides');	
		jQuery(window).load(function()
		{		
			if ($featuredArea.length) 
			{
				$featuredArea.css( 'backgroundImage', 'none' );
				$featuredArea.et_switcher({
					linksNav: '#switcher a',
					auto: true,
					button_styling: '<?php echo $button_styling; ?>',				
					autoSpeed: '<?php echo $autospeed; ?>',
					findParent: true,
					lengthElement: 'div'
				});
			};
		});
	</script>
	<?php 	
}

function usam_get_sliders( $qv = array() )
{ 
	global $wpdb;	
	
	if ( empty($qv['fields']) )
	{
		$qv['fields'] = 'all';
	}		
	
	$fields = $qv['fields'] == 'all'?'*':$qv['fields'];
	
	$_where[] = '1=1'	;
	if ( !isset($qv['active']) || $qv['active'] == '1')
		$_where[] = "active = '1'";
	elseif ( $qv['active'] == '0' )
		$_where[] = "active = '0'";			
		
	if ( !isset($qv['cache_results']) )
		$qv['cache_results'] = false;	
		
	if ( isset($qv['include']) )
		$_where[] = "id IN( '".implode( "','", $qv['include'] )."' )";
	
	$where = implode( " AND ", $_where);	
	if ( !empty($qv['condition']) ) 
	{		
		foreach ( $qv['condition'] as $condition )
		{					
			$select = '';
			if ( empty($condition['col']) )
				continue;
			
			switch ( $condition['col'] )
			{		
				case 'code' :
					$select = "code";			
				break;				
				default:				
					$select = $condition['col'];			
				break;				
			}
			if ( $select == '' )
				continue;
			
			$compare = "=";	
			switch ( $condition['compare'] ) 
			{
				case '<' :
					$compare = "<";					
				break;
				case '=' :
					$compare = "=";					
				break;	
				case '!=' :
					$compare = "!=";					
				break;
				case '>' :
					$compare = ">";					
				break;				
			}
			$value = $condition['value'];
			
			if ( empty($condition['relation']) )
				$relation = 'AND';
			else
				$relation = $condition['relation'];
			
			$where .= $wpdb->prepare( " $relation $select $compare %s", $value );			
		}
	}	
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'DESC';	
	if ( isset($qv['output_type']) )	
		$output_type = $qv['output_type'];	
	else
		$output_type = 'OBJECT';
	
	if ( $where != '' )
		$where = " WHERE $where ";
	
	$results = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_SLIDER." $where $orderby $order", $output_type );	
	
	if ( 'all' == $qv['fields'] )
	{			
		if ( $qv['cache_results'] )
		{	
			$ids = array();
			foreach ( $results as $result ) 
			{
				$result->setting = maybe_unserialize($result->setting);	
				wp_cache_set( $result->id, (array)$result, 'usam_slider' );
				$ids[] = $result->id;
			}	
			$tables = array( USAM_TABLE_SLIDES => 'usam_slides' );
			$column = 'slider_id';
			usam_update_cache( $ids, $tables, $column );			
		}
	}	
	return $results;
}
?>