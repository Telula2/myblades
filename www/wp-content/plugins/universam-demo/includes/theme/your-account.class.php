<?php
/**
 *   Этот класс отвечает за тематизации страницы учетных записей пользователей
 */
final class USAM_Your_Account
{
	private $current_tab_id = '';
	private $name_group_tab = '';
	private $tab = array();
	private $tabs = array();
	private $menu_group = array();
	
	private $messages = array();
	private $errors = array();
	public function __construct() 
	{
		$this->init();
		$this->callback_submit();
	}
	
	public function init()
	{			
		$this->menu_group = array(
			array(
				'menu_title'  => __( 'Покупка товаров', 'usam' ),				
				'slug'        => 'purchase-history',
			),			
			array(
				'menu_title'  => __( 'Мои скидки', 'usam' ),	
				'slug'        => 'my-discounts',
			),
			array(
				'menu_title'  => __( 'Центр поддержки', 'usam' ),
				'slug'        => 'my-feedback',			
			),
			array(
				'menu_title'  => __( 'Настройки', 'usam' ),
				'slug'        => 'my-settings',
			),					
		);				
		$this->menu_group = apply_filters( 'usam_your_account_menu', $this->menu_group );
								
		$this->tabs['purchase-history'] = array(
			array(
				'menu_title'  => __( 'История покупок', 'usam' ),				
				'page_title'  => '',	
				'slug'        => 'my-orders',
			),			
			array(
				'menu_title'  => __( 'Избранное', 'usam' ),				
				'page_title'  => __( 'Избранное', 'usam' ),	
				'slug'        => 'my-desired',
			),
			array(
				'menu_title'  => __( 'Просмотренные товары', 'usam' ),				
				'page_title'  => __( 'Просмотренные товары', 'usam' ),	
				'slug'        => 'my-views',
			),
			array(
				'menu_title'  => __( 'Ваша корзина', 'usam' ),				
				'page_title'  => __( 'Ваша корзина', 'usam' ),	
				'slug'        => 'my-basket',
			),	
			array(
				'menu_title'  => __( 'Ваш список сравнений', 'usam' ),				
				'page_title'  => __( 'Ваш список сравнений', 'usam' ),	
				'slug'        => 'my-compare',
			),						
		);
		$this->tabs['my-discounts'] = array(
			array(
				'menu_title'  => __( 'Мои бонусы', 'usam' ),				
				'page_title'  => __( 'Мои бонусы', 'usam' ),	
				'slug'        => 'my-bonus',
			),						
		);
		$this->tabs['my-feedback'] = array(
			array(
				'menu_title'  => __( 'Мои сообщения', 'usam' ),				
				'page_title'  => __( 'Мои сообщения', 'usam' ),	
				'slug'        => 'my-message',
			),			
			array(
				'menu_title'  => __( 'Новая тема', 'usam' ),				
				'page_title'  => __( 'Новая тема', 'usam' ),	
				'slug'        => 'new_topic',
			),
			array(
				'menu_title'  => __( 'Мои отзывы', 'usam' ),				
				'page_title'  => __( 'Мои отзывы', 'usam' ),	
				'slug'        => 'my-comments',
			),							
		);
		$this->tabs['my-settings'] = array(
			array(
				'menu_title'  => __( 'Мой профиль', 'usam' ),				
				'page_title'  => __( 'Мой профиль', 'usam' ),	
				'slug'        => 'my-profile',
			),	
			array(
				'menu_title'  => __( 'Тип плательщика', 'usam' ),				
				'page_title'  => __( 'Тип плательщика', 'usam' ),	
				'slug'        => 'type_payer',
			),				
			array(
				'menu_title'  => __( 'Оформление заказа', 'usam' ),				
				'page_title'  => __( 'Оформление заказа', 'usam' ),	
				'slug'        => 'my-checkout',
			),
			array(
				'menu_title'  => __( 'Изменить пароль', 'usam' ),				
				'page_title'  => __( 'Изменить пароль', 'usam' ),	
				'slug'        => 'change-password',
			),
			array(
				'menu_title'  => __( 'Подписка на новости', 'usam' ),				
				'page_title'  => __( 'Подписка на новости', 'usam' ),	
				'slug'        => 'subscribe',
			),					
		);
		if ( usam_availability_check_price_list() )
		{
			$this->tabs['my-partner'][] = array(
				'menu_title'  => __( 'Прайс-лист', 'usam' ),				
				'page_title'  => __( 'Прайс-лист', 'usam' ),	
				'slug'        => 'price-list',
			);
		}		
		$this->tabs = apply_filters( 'usam_your_account_sub_menu', $this->tabs );
		
		if ( !empty($this->tabs['my-partner']) )
		{
			$this->menu_group[] = array(
				'menu_title'  => __( 'Партнерство', 'usam' ),
				'slug'        => 'my-partner',
			);
		}
		$account_current = usam_your_account_current_tab();	
		$this->set_current_tab( $account_current['tab'] );
		
	//	$this->current_tab_id = $account_current['subtab'];			
	}
	
	public function set_current_tab( $tab_id = null )
	{		
		if ( empty($this->tabs) )
			return;
		
		foreach ( $this->menu_group as $menu )
		{ 
			foreach ( $this->tabs[$menu['slug']] as $submenu )
			{			
				if ( $submenu['slug'] == $tab_id || empty($tab_id) )
				{
					$this->name_group_tab = $menu['slug'];
					$this->current_tab_id = $submenu['slug'];					
					$this->tab = $submenu;	
					break 2;
				}		
			}			
		}		
		if ( empty($this->current_tab_id) ) 
			$this->set_current_tab( );
	}	
	
	public function get_next_step_link( $step = '' ) 
	{
		if ( ! $step ) {
			$step = $this->step;
		}

		$keys = array_keys( $this->steps );
		if ( end( $keys ) === $step ) {
			return admin_url();
		}

		$step_index = array_search( $step, $keys );
		if ( false === $step_index ) {
			return '';
		}

		return add_query_arg( 'step', $keys[ $step_index + 1 ] );
	}	
	
	public function display_header_tab() 
	{
		?>	
		<div class="usam_user_profile_header">
			<div class="usam_user_profile_header_content">		
			<ul id="usam_accordion" class="usam_your_account_menu">
			<?php		
			foreach ( $this->menu_group as $menu )
			{
				?>
					<li class="usam_menu <?php echo ($this->name_group_tab == $menu['slug'])?'current_menu':''; ?> usam_menu-<?php echo $menu['slug']; ?>">
						<span class="usam_menu_name"><?php echo $menu['menu_title']; ?></span>				
						<ul class="usam_your_account_sub_menu">
						<?php 
						foreach ( $this->tabs[$menu['slug']] as $sub_menu )
						{
							?>
							<li class="usam_submenu <?php echo ($this->current_tab_id == $sub_menu['slug'])?'current_submenu':''; ?> usam_submenu-<?php echo $sub_menu['slug']; ?>">
								<a class = "usam_submenu_link-<?php echo $sub_menu['slug']; ?>" href="<?php echo  usam_get_user_account_url( $sub_menu['slug'] ); ?>"><?php echo $sub_menu['menu_title']; ?></a>
							</li>
							<?php
						}
						?>
						</ul>
					</li>
				<?php
			}
			?>
			</ul>
			</div>
		</div>		
		<?php
	}
	
	public function display_content_tab() 
	{		
		echo '<div class="usam_user_profile_content">';	
		echo '<form action="" method="post">';
		if ( $this->tab['page_title'] != '' )
			echo '<h2>'.$this->tab['page_title'].'</h2>';	
		
		$this->display_message( 'message' );
		$this->display_message( 'error' );	
		
		usam_include_template_file( $this->current_tab_id, 'your-account' );	
		echo wp_nonce_field( 'usam_user_profile', '_usam_user_profile' );	
		echo '</form>';
		echo '</div>';		
	}	
	
	private function display_message( $type ) 
	{
		global $current_screen;	
		
		if ( $type == 'message' )		
			$messages = usam_get_user_screen_message( 'your-account' );
		else		
			$messages = usam_get_user_screen_error( 'your-account' );		
	
		if ( !empty($messages) )		
		{
			foreach ( $messages as $message )
			{
				if ( $type == 'message' )	
					echo "<div class=\"updated\"><p>{$message}</p></div>";
				else
					echo "<div class=\"error\"><p>{$message}</p></div>";
			}
		}		
	}
	
	// выводит сообщения об ошибках при сохранении данных в профиле пользователя
	private function callback_submit() 
	{		
		if( !empty($_POST) )
		{ 
			if( ! wp_verify_nonce( $_POST['_usam_user_profile'], 'usam_user_profile') )
			{
				$this->errors[] = __( 'Казалось бы либо вы пытаетесь взломать этого аккаунта, или ваша сессия истекла. Надеюсь на последние.', 'usam' );			
			}
			else
			{ 
				$action = str_replace( '-', '_', $this->current_tab_id );	
				$method = 'update_'.$action;			
				if ( method_exists($this, $method) )
				{ 				
					$this->$method();				
				}			
				if( isset($_POST['updated']) )
				{			
					$this->messages[] = __( 'Спасибо, ваши изменения были сохранены.', 'usam' );
				}						
			}
			usam_set_user_screen_message( $this->messages, 'your-account' );		
			usam_set_user_screen_error( $this->errors, 'your-account' );
			
			wp_redirect( esc_url_raw( usam_get_user_account_url( $this->current_tab_id ) ) );
			exit;
		}
	}
	
	function update_subscribe()
	{				
		$userlists = usam_get_currentuser_list( );		
		if ( !empty($_POST['mail_list']) )
		{		
			$mail_list = $_POST['mail_list'];		
			$result = array_diff($mail_list, $userlists);	
			if ( !empty($result) )
			{
				usam_set_user_lists( $result );			
			}				
			$result = array_diff($userlists, $mail_list);	
			if ( !empty($result) )
			{
				usam_delete_user_lists( $result );
			}	
		}
		else
		{ 	
			usam_delete_user_lists( $userlists );
		} 
	}
	
	function update_my_checkout()
	{ 
		if ( ! empty($_POST['collected_data']) ) 
		{					
			$checkout = stripslashes_deep($_POST['collected_data']);					
			$this->errors = usam_user_checkout_update( $checkout );		
		}	
	}
	
	function update_type_payer()
	{ 
		if ( ! empty($_REQUEST['type_payer']) )
		{
			$types_payers = usam_get_group_payers();	
			foreach ( $types_payers as $value )
			{
				if ( $value['id'] == $_REQUEST['type_payer'] )
				{
					usam_update_customer_meta( 'type_payer', $value['id'] );
				}
			}
		}
	}
		
	function update_my_profile()
	{				
		global $user_ID;
		
		$userdata = array(
			'ID' => $user_ID,			
		);		
		if ( !empty($_REQUEST['email']) )
			$userdata['email'] = sanitize_email($_REQUEST['email']);		
		if ( !empty($_REQUEST['first_name']) )
			$userdata['first_name'] = sanitize_text_field($_REQUEST['first_name']);		
		if ( !empty($_REQUEST['last_name']) )
			$userdata['last_name'] = sanitize_text_field($_REQUEST['last_name']);
		
		if ( !empty($_REQUEST['sex']) )
			update_user_meta($user_ID, 'usam_sex', sanitize_title($_REQUEST['sex']) ); 		
	
		$birthday = usam_get_datepicker('birthday');
		update_user_meta($user_ID, 'usam_birthday', $birthday ); 
		
		$user_id = wp_update_user( $userdata );		
	}
}


function usam_go_back_user_account_tab( $tab = null ) 
{
	if ( $tab === null )
	{
		$account_current = usam_your_account_current_tab();	
		$tab = $account_current['tab'];
		$subtab = $account_current['subtab'];
	}
	$url = usam_get_user_account_url( $tab );
	?>
	<div class = 'go_back'>		
		<a href='<?php echo $url; ?>'><i class="go_back-icon icon"></i><?php _e('Назад', 'usam'); ?></a>
	</div>
	<?php
}

//Проверить существует ли запрошенный заказ
function usam_show_details_order() 
{
	if ( !empty($_GET['id']) )
	{
		global $user_ID;
		$order_id = absint($_GET['id']);		
		$order = new USAM_Order( $order_id );
		$customer_id = $order->get( 'user_ID' );		
		if ( $customer_id == $user_ID )
			return true;		
	}
	return false;
}

function usam_display_pay_the_order() 
{
	global $user_ID;
	if ( isset($_GET['id']) )		
		$order_id = absint($_GET['id']);	
	else
		return false;		
	
	$order = new USAM_Order( $order_id );		
	$order_data = $order->get_data();	
	echo "<div class='pay_the_order_box'>";
	if ( $order->get( 'user_ID' ) == $user_ID )
	{						
		$payment_history = usam_get_payments( array( 'order_data' => 'DESC', 'document_id' => $order_id, 'document_type' => 'order', 'status' => 1 ) );			
			
		$payment_status_sum = $order->get_payment_status_sum();
		echo "<h2>".__('Оплата заказа','usam')." <span class ='order_id'># ".$order_data['id']."</span> ".__('на сумму','usam')." <span class ='order_sum'>".usam_currency_display($order_data['totalprice'])."</span> </h2>";
		echo "<form method='POST' id='pay_the_order' action=''>
		<p>".__('Информация об оплате','usam')."</p>";
		
		echo '<div class="detail_amount">
		<table class="usam_user_profile_table detail_amount_table">
			<thead>
				<tr>
					<td>'.__('Осталось оплатить','usam').'</td>
					<td class="separator"></td>
					<td style="color: #749511;">'.__('Оплачено','usam').'</td>
					<td class="separator"></td>
					<td style="color: #f29129;">'.__('Не оплаченно','usam').'</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="order_payment_required">'.usam_currency_display($payment_status_sum['payment_required']).'</td>
					<td class="separator"></td>
					<td id="order_total_paid">'.usam_currency_display($payment_status_sum['total_paid']).'</td>
					<td class="separator"></td>
					<td id="order_total_unpaid">'.usam_currency_display($payment_status_sum['total_unpaid']).'</td>
				</tr>
			</tbody>
		</table></div>';		
		
		echo "<p>".__('Не оплаченные счета','usam')."</p>
		<div class = 'tbox payment_documents'>
		<table class = 'usam_user_profile_table tlog'>
		<thead>
			<tr>				
				<td></td>
				<td>".__('Дата счета','usam')."</td>
				<td>".__('Сумма оплаты','usam')."</td>
				<td>".__('Статус','usam')."</td>
				<td>".__('Номер документа','usam')."</td>	
				<td>".__('Доступность','usam')."</td>				
			</tr>
		</thead>
		<tbody>";
		if ( empty($payment_history) )
		{
			echo "<tr><td colspan='6'>".__('Нет выставленных счетов','usam')."</td></tr></table></div>";
		}
		else
		{
			$checked = "checked ='checked'";	
			$available = false;			
			foreach ( $payment_history as $document )
			{		
				
				echo "<tr><td>";
					if ( $document->pay_up >= date( "Y-m-d H:i:s" ) )
					{
						echo "<input type='radio' id='document_$document->id' $checked value='$document->document_number' name='payment_document'/>";
						$available = true;
					}
					echo "</td>
					<td>".date_i18n(  get_option( 'date_format' ).' H:i', strtotime($document->date_time))."</td>
					<td>".$document->sum."</td>
					<td>".usam_get_payment_document_status_name($document->status)."</td>
					<td><label for='document_".$document->id."'>".$document->document_number."</label></td><td>";
					if ( $document->pay_up >= date( "Y-m-d H:i:s" ) )
					{
						echo "<span class = 'available'>".__('Доступен','usam')."</span>";
					}
					else
					{
						echo "<span class = 'not_available'>".__('Не доступен','usam')."</span>";					
					}	
				echo "</td></tr>";
				$checked = "";				
			}			
			echo "</tbody></table></div><p>".__('Доступные способы оплаты','usam')."</p>
			<div class='gateway_container'>		
			<table class = 'usam_user_profile_table'>			
			";
			wp_nonce_field( 'purchase_'.$user_ID, 'new_transaction' );			
			$checked = "checked ='checked'";
			$gateways = usam_get_payment_gateways( array( 'type' => 'a' ) );		
			if ( !empty($gateways) ) 
			{
				foreach ($gateways as $gateway) 
				{		
					echo "
					<tr>
						<td>
							<input type='radio' id='gateway_{$gateway->id}' value='".$gateway->id."' ".$checked." name='gateway'/> <label for='gateway_{$gateway->id}'>".$gateway->name."</label> 
						</td>
					</tr>";	
					$checked = "";				
				}
			}
			else
			{
				echo "<tr><td>".__('Нет доступного способа оплаты','usam')."</td></tr>";	
			}
			if ( !$available )
				$disabled = 'disabled="disabled"';
			else
				$disabled = '';
			echo '		
			</table>
			</div>
			<div class="gateway_button">			
				<input type="hidden" value="1" name="pay_the_order" />		
				<input type="submit" value="'.__('Оплатить','usam').'" '.$disabled.' name="button" class="button button_buy">
			</div>';
		}
	}
	else
		echo "<h2>".__('Заказ не найден','usam')."</h2>";
	echo '</form></div>';
}


// Выводит подробные сведения выбранного заказа
function usam_show_order_customer() 
{	
	if ( isset($_GET['display']) && $_GET['display'] == 'pay_the_order' )	
		usam_display_pay_the_order();
	else
		usam_order_details();
}


// Выводит подробные сведения выбранного заказа
function usam_order_details() 
{
	global $wpdb, $user_ID;
	if ( isset($_GET['id']) )		
		$order_id = absint($_GET['id']);	
	else
		return false;
	
	$order = new USAM_Order($order_id );
	$order_data = $order->get_data();
	
	$args =  array(
		'display_currency_symbol' => true,
		'decimal_point'   		  => true,
		'display_currency_code'   => false,
		'display_as_html'         => false,
		'code'                 => false,
	);
	
	if ( $order_data['user_ID'] != $user_ID )
		return false;	
	
	//Вывод корзины купленных товаров
	echo "<div class = 'order_details'>";
	echo "<h2>".__('Подробности заказа','usam')."<span class ='order_id'># ".$order_data['id']."</span></h2>";
	echo "<div class = 'tbox'>";
	echo "<div id='usam_order-".$order_data['id']."'>\n\r";
	echo "<p class='order_status'>".__( 'Статус заказа', 'usam' ).": <strong class='order_status_name'>".usam_get_order_status_name( $order_data['status'] )."</strong></p>";
	echo "<p class='order_status_description'>".usam_get_order_status_description( $order_data['status'] )."</p>";
	
	$order_products = $order->get_order_products();	
	$j = 0;	
	if ( $order_products != null ) 
	{
		echo "<table class='tlog usam_user_profile_table' style='width: 100%'>";
		echo "<thead>";
			echo "<tr>";			
				echo " <td class='center_cell'>".__( 'Имя', 'usam' )."</td>";
				echo " <td class='center_cell'>".__( 'Количество', 'usam' )." </td>";
				echo " <td class='center_cell'>".__( 'Цена', 'usam' )." </td>";
				echo " <td class='center_cell'>".__( 'Сумма', 'usam' )." </td>";				
			echo "</tr>";			
		echo "</thead>";			
		echo "<tbody>";			
		$gsttotal = false;
		$endtotal = 0;
		foreach ( (array)$order_products as $product ) 
		{
			$sum = $product->price * $product->quantity;
			$endtotal += $sum;
			$alternate = "";
			$j++;

			if ( ($j % 2) != 0 )
				$alternate = "class='alt'";
			
			$billing_country = !empty($country_data[0]['value']) ? $country_data[0]['value'] : '';
			$shipping_country = !empty($country_data[0]['value']) ? $country_data[0]['value'] : '';
		
			echo "<tr $alternate>";

			echo " <td>".apply_filters( 'the_title', $product->name )." </td>";
			echo " <td class='center_cell'>".$product->quantity." </td>";
			echo " <td class='center_cell'>".usam_currency_display( $product->price, $args )." </td>";			
			echo " <td class='center_cell'>".usam_currency_display( $sum, $args )." </td>";		
			echo '</tr>';
		}
		echo "</tbody>";
		echo "<tfoot>";
		echo "<tr>";				
		echo " <td></td>";	
		echo " <td colspan = '2'>";
		echo "<strong>" . __( 'Сумма', 'usam' ) . ":</strong><br />";
		if ( $order_data['shipping']>0 ) 
			echo "<strong>" . __( 'Доставка', 'usam' ) . ":</strong><br />";
		if ( $order_data['total_tax']>0 ) 
			echo "<strong>" . __( 'Всего налог', 'usam' ) . ":</strong><br />";
		
		if ( $order_data['coupon_discount']>0 ) 
			echo "<strong>" . __( 'Скидка по купону', 'usam' ) . ":</strong><br />";
		if ( $order_data['discount']>0 ) 
			echo "<strong>" . __( 'Скидка', 'usam' ) . ":</strong><br />";		
		if ( $order_data['bonus']>0 ) 
			echo "<strong>" . __( 'Использумые бонусы', 'usam' ) . ":</strong><br />";
		
		echo "<strong>" . __( 'Итог', 'usam' ) . ":</strong>";
		echo " </td>";
		echo " <td>";		
		echo usam_currency_display( $endtotal, $args  ) . "<br />";		
		if ( $order_data['shipping']>0 )
			echo usam_currency_display( $order_data['shipping'], $args  ) . "<br />";		
		if ( $order_data['total_tax']>0 )		
			echo usam_currency_display( $order_data['total_tax'], $args ). "<br />";	
		
		if ( $order_data['coupon_discount']>0 ) 
			echo usam_currency_display( $order_data['coupon_discount'], $args ). "<br />";	
		if ( $order_data['discount']>0 ) 
			echo usam_currency_display( $order_data['discount'], $args ). "<br />";			
		if ( $order_data['bonus']>0 ) 
			echo usam_currency_display( $order_data['bonus'], $args ). "<br />";	
		
		echo usam_currency_display( $order_data['totalprice'], $args );
		echo " </td>";
		echo '</tr>';
		echo "</tfoot>";
			
		echo "</table>";			
		echo "</div>";
		echo "</div>";
		echo "<br />";	

		echo "<div class='tbox'><strong class='form_group'>" . __( 'Покупатель', 'usam' ) . "</strong>";
		$customer_data = $order->get_customer_data();		
		echo "<table class='tlog customer_details usam_user_profile_table' style='width: 100%'>";	
	
		if ( !empty($customer_data) ) 
		{			
			$props_group = usam_get_order_props_groups( array('type_payer' => $order_data['type_payer']) );
			$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data' ) );	
			$html = '';			
			foreach ( $props_group as $group ) 
			{
				$html .= "<tr><td class='title' colspan='2'>{$group->name}:</td></tr>";
				foreach ( $list_properties as $unique_name => $field ) 			
				{
					if ( $group->id === $field->group  && !empty($customer_data[$unique_name]['value']) )
					{						
						$html .= "<tr><td>" . esc_html( $field->name ) . ":</td><td>" . usam_get_display_order_property( $customer_data[$unique_name]['value'], $field->type ) . "</td></tr>";
					}
				}				
			}	
			echo $html;			
		}			
		echo "</table>";
		echo "  </div>\n\r";					
		
		?>
		<div class='tbox'><strong class='form_group'><?php _e('Получение товара', 'usam' ); ?></strong>
			<table class='tlog documents_shipped usam_user_profile_table' style='width: 100%'>
				<thead>
					<tr>						
						<th><?php _e( 'Способ получения', 'usam' ); ?></th>	
						<th><?php _e( 'Предпологаемая дата готовности', 'usam' ); ?></th>					
						<th><?php _e( 'Дата и время доставки', 'usam' ); ?></th>										
					</tr>
				</thead>
				<tbody>				
		<?php	
		$documents_shipped = $order->get_full_details_of_shipment();		
		if ( !empty($documents_shipped) ) 
		{			
			foreach ( $documents_shipped as $document ) 
			{
				$storage_name = '';				
				if ( $document['storage_pickup'] != 0 )
				{
					$storage = usam_get_storage( $document['storage_pickup'] );
					$storage_pickup_name = isset($storage['address'])?$storage['address']:''; 
					$storage_pickup_phone = isset($storage['phone'])?' - '.__( 'т.', 'usam' ).' '.$storage['phone']:''; 
					$storage_name = "<p>".$document['name']."</p>".__( 'Офис получения', 'usam' ).': '.$storage_pickup_name.$storage_pickup_phone;
				}
				else
				{
					$storage_name = $document['name'];
				}
				$date_allow_delivery = $readiness = __( 'Не данных', 'usam' );
				if ( !empty($document['readiness']) )
				{
					$readiness = date_i18n("d.m.Y H:i",strtotime($document['readiness']));
				}	
				if ( !empty($document['date_allow_delivery']) )
				{
					$date_allow_delivery = date_i18n("d.m.Y H:i",strtotime($document['date_allow_delivery']));
				}					
				?>
				<tr>
					<td><?php echo $storage_name; ?></td>
					<td><?php echo $readiness; ?></td>
					<td><?php echo $date_allow_delivery; ?></td>				
				</tr>
				<?php
			}
		}	
		else
		{
			?>
			<tr><td colspan = '6' class = "payment_history_empty"><?php _e( 'Не было никаких оплат для этого заказа', 'usam' ); ?> </td></tr>
			<?php
		}
		?>
		</tbody>
		</table>
		</div>	
		
		<div class='tbox'><strong class='form_group'><?php _e('История оплаты', 'usam' ); ?></strong>
			<table class='tlog customer_details usam_user_profile_table' style='width: 100%'>
				<thead>
					<tr>
						<th><?php _e( 'Номер документа', 'usam' ); ?></th>	
						<th><?php _e( 'Дата', 'usam' ); ?></th>					
						<th><?php _e( 'Способ оплаты', 'usam' ); ?></th>
						<th><?php _e( 'Результат оплаты', 'usam' ); ?></th>						
						<th><?php _e( 'Сумма', 'usam' ); ?></th>						
					</tr>
				</thead>
				<tbody>				
		<?php
		$payments = $order->get_payment_history();	
		if ( !empty($payments) ) 
		{			
			foreach ( $payments as $payment ) 
			{
				?>
				<tr>
					<td><?php echo $payment['document_number']; ?></td>
					<td><?php echo date_i18n("d.m.Y H:i",strtotime($payment['date_time'])); ?></td>
					<td><?php echo $payment['name']; ?></td>
					<td><?php echo $payment['status_name']; ?></td>					
					<td><?php echo $payment['sum']; ?></td>
				</tr>
				<?php
			}
		}	
		else
		{
			?>
			<tr><td colspan = '6' class = "payment_history_empty"><?php _e( 'Не было никаких оплат для этого заказа', 'usam' ); ?> </td></tr>
			<?php
		}
		?>
		</tbody>
		</table>
		</div>			
			<?php
			if ( !$order->is_closed_order()  )
			{
			?>			
			<div class='tbox order_cancellation_reason'><strong class='form_group'><?php _e('Отменить заказ', 'usam' ); ?></strong>
				<form method="post" action = "">				
					<table class ="usam_user_profile_table">
						<tr>
							<td class ="name" ><label for = "cancellation_reason"><?php _e( 'Причина отмены:', 'usam' ); ?></label></td>
							<td class ="text"><textarea id = "cancellation_reason" cols="35" maxlength="255" rows="3" class="typearea" name="cancellation_reason" placeholder="<?php _e( 'Напишите причину отмены...', 'usam' ); ?>"></textarea></td>
						</tr>
					</table>
					<input type="submit" id="cancel_order" class="button button_close" value="<?php _e( 'Отменить', 'usam' ); ?>" name="button" />
					<input type="hidden" value="<?php echo $order_id; ?>" name="order_id" />
					<input type="hidden" value="cancel_order" name="usam_action" />					
				</form>
			</div>		
			<?php
			}
			?>
		</div>
		<?php
	}
}
?>