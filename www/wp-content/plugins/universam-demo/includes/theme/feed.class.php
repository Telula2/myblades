<?php 
$feed = new USAM_Product_Feed();
class USAM_Product_Feed
{
	function __construct( ) 
	{		
		if ( isset($_GET["action"]) && $_GET["action"] == "feed" ) 	
			add_action( 'wp', array( $this, 'generate_feed') );		
	}
	
	function generate_feed() 
	{
		if ( isset($_GET["xmlformat"]) )
		{
			set_time_limit(0);	
			if ( function_exists ( 'wp_suspend_cache_addition' ) )
				wp_suspend_cache_addition(true);	
			
			define( 'DONOTCACHEPAGE',TRUE ); // Не кэшировать в WP Super-Cache
			
			switch ( $_GET["xmlformat"] ) 
			{
				case 'rss' :
					$this->product_feed( 'rss' );	
				break;		
				case 'google' :
					$this->product_feed( 'google' );						
				break;				
				case 'yandex' :
					$this->yandex_generate_product_feed();						
				break;		
			}
			exit();
		}
	}
	
	private function text_decode( $text ) 
	{
		$text = strip_shortcodes( $text );
		$text = html_entity_decode( $text );
		$text = strip_tags( $text );			
		$text = str_replace("\n", " ", $text); // Переносы строк заменить на пробелы
		$text = preg_replace('| +|', ' ', $text);	// Несколько пробелов заменить на один		
		$text = str_replace( '&', ' ', $text );		
		$text = str_replace( 'x', ' ', $text );			
		return $text;		
	}	
	
	function text_decode2( $content ) 
	{		
		$content = trim( html_entity_decode( strip_tags( $content ) ) );		//		stripslashes
		$content = preg_replace('/\r\n|\r|\n/u', ' ', $content); 
		$content = preg_replace('| +|', ' ', $content);		
		$content = str_replace( '&', ' ', $content );	
		$content = str_replace( '<', ' ', $content );
		$content = str_replace( '>', ' ', $content );	
		$content = str_replace( '"', '', $content );		
		$content = str_replace( "'", '', $content );	
		return $content;
	}	
	
	// Если пользователь хочет получать новости о товарах
	function product_feed( $xmlformat ) 
	{
		global $post;	
		
		$chunk_size = apply_filters ( 'usam_productfeed_chunk_size', 500 );	
		$args = array(			
			'post_status'   => array('publish'),
			'posts_per_page'   => $chunk_size,
			'offset'        => 0,			
		);
		$args = apply_filters( 'usam_productfeed_query_args', $args );		
		$products = usam_get_products( $args );
		
		$out = '';
		while ( count ( $products ) ) 
		{
			foreach ($products as $post) 
			{
				setup_postdata($post);
				$product_url = usam_product_url( $post->ID );
				$out .= "    <item>\n\r";
				if ( $xmlformat == 'google' )
					$out .= "      <g:payment_notes>" . __( 'Google Wallet', 'usam' ) . "</g:payment_notes>\n\r";
			
				$out .= "      <title><![CDATA[".$post->ID.' '.$this->text_decode( get_the_title() )."]]></title>\n\r";
				$out .= "      <link>$product_url</link>\n\r";
				$out .= "      <description><![CDATA[ ".$this->text_decode( get_the_content() )." ]]></description>\n\r";
				$out .= "      <pubDate>".$post->post_modified_gmt."</pubDate>\n\r";
				
				$product_categories = wp_get_post_terms($post->ID, 'usam-category', array("fields" => "all"));
				if ( !empty($product_categories[0]) )					
				{					
					$category_name = $product_categories[0]->name;	
					$out .= "      <category>$category_name</category>\n\r";
				}				
				$image = usam_get_product_thumbnail_src();				
				
				$price = usam_get_product_price( $post->ID );
				$currargs = array(
					'display_currency_symbol' => false,
					'display_decimal_point'   => true,
					'display_currency_code'   => false,
					'display_as_html'         => false
				);
				$price = usam_currency_display( $price, $currargs );			
				if ( $xmlformat == 'google')
				{				
					$out .= "<g:image_link>$image</g:image_link>\n\r";
					$out .= "      <g:price>".$price."</g:price>\n\r";					
					$out .= "      <g:id>".$post->ID."</g:id>\n\r";
				} 
				else 
				{
					$out .= "      <product:price>".$price."</product:price>\n\r";
					$out .= "<enclosure url='$image' />\n\r";		
				}
				$out .= "    </item>\n\r";			
			}
			$args['offset'] += $chunk_size;
			$products = usam_get_products ( $args );
		}		
		$selected_category = '';
		$selected_product = '';
		
		$self = htmlspecialchars(site_url( "/index.php?xmlformat=rss&action=feed$selected_category$selected_product" ));

		header("Content-Type: application/xml; charset=UTF-8");
		header('Content-Disposition: inline; filename="Product_List.rss"');

		$html  = "<?xml version='1.0' encoding='UTF-8' ?>\n\r";
		
		$html .= "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'";	
		if ( $xmlformat == 'google')	
			$html .= ' xmlns:g="http://base.google.com/ns/1.0"';		
		else 		
			$html .= ' xmlns:product="http://www.buy.com/rss/module/productV2/"';		
		$html .= ">\n\r";
		
		$html .= "  <channel>\n\r";
		$html .= "    <title><![CDATA[" . sprintf( __( '%s товары', 'usam' ), get_option( 'blogname' ) ) . "]]></title>\n\r";
		$html .= "    <link>".get_option('siteurl')."/wp-admin/admin.php?page=".USAM_DIR_NAME."/display-log.php</link>\n\r";
		$html .= "    <description>" . __( 'Это списк товаров магазина для RSS ленты', 'usam' ) . "</description>\n\r";
		$html .= "    <generator>" . __( 'УНИВЕРСАМ', 'usam' ) . "</generator>\n\r";
		$html .= "    <atom:link href='$self' rel='self' type='application/rss+xml' />\n\r";	
		$html .= $out;
		$html .= "  </channel>\n\r";
		$html .= "</rss>";	

		echo $html;
	}	

	function list_categories( $category_id = 0, $level = 0 )
	{	
		$output = '';
		if (!empty($category_id))
			$parent = '&parent='.$category_id;
		else
			$parent = '';
		$category_list = get_terms('usam-category',$parent);
		++$level;
		if($category_list != null) 
		{
			foreach((array)$category_list as $category) 
			{
				$parentId = ($category_id == 0 ? '' :'parentId="'.$category->term_id.'"');
				$output .= '<category id="'.$category->term_id.'" '.$parentId.'>'.$category->name.'</category>';
				$this->list_categories($category->term_id, $level);
			}
		}
		return $output;
	}	
	
	
	// Если пользователь хочет получать новости о товарах
	function yandex_generate_product_feed() 
	{
		global $post, $type_price;	
		
		$date = date_i18n( "Y-m-d H:i" );
		
		$categories = $this->list_categories();			
		
		
		$chunk_size = apply_filters ( 'usam_productfeed_chunk_size', 500 );	

		$args = array(			
			'post_status'   => array('publish'),
			'posts_per_page'   => $chunk_size,
			'offset'        => 0,	
		);
		$args = apply_filters( 'usam_productfeed_query_args', $args );		
		$products = usam_get_products( $args );
		
		if ( !empty($_GET['code_price']) )
			$code_price = $_GET['code_price'];
		else
			$code_price = $type_price;
		
		$currency = usam_get_currency_price_by_code( $code_price );
		
		$out = "";
		while ( count ( $products ) ) 
		{
			foreach ($products as $post) 
			{								
				setup_postdata($post);				
				$brand = wp_get_post_terms($post->ID, 'usam-brands', array("fields" => "names"));
				if ( !empty($brand[0]) )
				{
					$brand_name = $brand[0];
				
					$price = usam_get_product_price( $post->ID, $code_price );	
					if ( empty($price) )					
						continue;	

					$product_categories = wp_get_post_terms($post->ID, 'usam-category', array("fields" => "all"));
					if ( empty($product_categories[0]) )					
						continue;
					$category_id = $product_categories[0]->term_id;
					$category_name = $product_categories[0]->name;					
					
					$purchase_link = usam_product_url($post->ID);
					$stock = usam_get_product_meta( $post->ID, 'stock' );
					if ( $stock > 0 )
						$available = 'true';
					else
						$available = 'false';						
					$image = usam_get_product_thumbnail_src();				
					if ( empty($post->post_excerpt) ) 
						$content = $post->post_content;	
					else
						$content = $post->post_excerpt;	
				
					//<local_delivery_cost>$local_delivery_cost</local_delivery_cost>	
					
					$out .= "<offer id='$post->ID' type='vendor.model' available='$available'>
						<url>$post->guid</url>
						<price>$price</price>		
						<picture>$image</picture>					
						<currencyId>$currency</currencyId>
						<categoryId>$category_id</categoryId>
						<delivery>1</delivery>					
						<typePrefix>".$this->text_decode( $category_name )."</typePrefix>
						<vendor>$brand_name</vendor>			
						<model>".$this->text_decode( get_the_title() )."</model>
						<description>".$this->text_decode( $content )."</description>					
					</offer>\n\r";				
				}
			}			
			$args['offset'] += $chunk_size;
			$products = usam_get_products ( $args );
		}	
		header("Content-Type: application/xml; charset=windows-1251");
		header('Content-Disposition: inline; filename="Product_List.rss"');	
		
		$xml = '<?xml version="1.0" encoding="windows-1251"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="'.$date.'">
	<shop>
		<name>'. sprintf( __( '%s товары', 'usam' ), get_option( 'blogname' ) ) ."</name>
		<company>".get_option( 'blogname' )."</company>
		<url>".get_option( 'siteurl' )."</url>
		<currencies>
			<currency id='$currency' rate='1'/>
		</currencies>	
		
		<categories>$categories</categories>	
		<offers>\n\r".$out."</offers></shop></yml_catalog>";
		
		echo iconv ('UTF-8', 'windows-1251//IGNORE', $xml);	
	}
}
?>