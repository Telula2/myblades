<?php

function usam_shopping_cart( $input = null, $override_state = null ) 
{
	global $wpdb;

	$customer_id = usam_get_current_customer_id();

	if ( is_numeric( $override_state ) )
		$state = $override_state;
	else
		$state = get_option( 'usam_cart_location', 3 );

	if ( get_option( 'usam_widget_show_sliding_cart' ) == 1 ) 
	{
		if ( isset( $_SESSION['slider_state'] ) && is_numeric( $_SESSION['slider_state'] ) ) {
			if ( $_SESSION['slider_state'] == 0 ) {
				$collapser_image = 'plus.png';
			} else {
				$collapser_image = 'minus.png';
			}
			$fancy_collapser = "<a href='#' onclick='return shopping_cart_collapser()' id='fancy_collapser_link'><img src='" . USAM_CORE_IMAGES_URL . "/$collapser_image' title='' alt='' id='fancy_collapser' /></a>";
		} else {
			if ( $customer_id ) {
				$collapser_image = 'minus.png';
			} else {
				$collapser_image = 'plus.png';
			}
			$fancy_collapser = "<a href='#' onclick='return shopping_cart_collapser()' id='fancy_collapser_link'><img src='" . USAM_CORE_IMAGES_URL . "/$collapser_image' title='' alt='' id='fancy_collapser' /></a>";
		}
	} 
	else 
		$fancy_collapser = "";	

	if ( $state == 1 ) 
	{
		if ( $input != '' ) 
		{
			echo "<div id='sideshoppingcart'><div id='shoppingcartcontents'>";
			echo usam_shopping_basket_internals();
			echo "</div></div>";
		}
	} 
	else if ( ($state == 3) || ($state == 4) ) 
	{
		if ( $state == 4 ) 
		{
			echo "<div id='widgetshoppingcart'>";
			echo "<h3>" . __( 'Корзина', 'usam' ) . "$fancy_collapser</h3>";
			echo "  <div id='shoppingcartcontents'>";
			echo usam_shopping_basket_internals(false, false, true );
			echo "  </div>";
			echo "</div>";
			$dont_add_input = true;
		} 
		else 
		{
			echo "<div id='sideshoppingcart'>";
			echo "<h3>" . __( 'Корзина', 'usam' ) . "$fancy_collapser</h3>";
			echo "  <div id='shoppingcartcontents'>";
			echo usam_shopping_basket_internals( false, false, true );
			echo "  </div>";
			echo "</div>";
		}
	} 
	else 
	{				
		echo "<div id='shoppingcart'>";
		echo "<h3>" . __( 'Корзина', 'usam' ) . "$fancy_collapser</h3>";
		echo "  <div id='shoppingcartcontents'>";
		echo usam_shopping_basket_internals( false, false, true );
		echo "  </div>";
		echo "</div>";
	}	
	return $input;
}

function usam_shopping_basket_internals( $deprecated = false, $quantity_limit = false, $no_title=false ) 
{
	global $wpdb;

	$display_state = '';

	if ( ( ( ( isset( $_SESSION['slider_state'] ) && $_SESSION['slider_state'] == 0) ) || ( usam_get_basket_number_items() < 1 ) ) && ( get_option( 'usam_widget_show_sliding_cart' ) == 1 ) )
		$display_state = "style='display: none;'";
	echo "    <div id='sliding_cart' class='shopping-cart-wrapper' $display_state>";
	include_once( usam_get_template_file_path( 'widget-basket' ) );
	echo "    </div>";
}
?>