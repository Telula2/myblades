<?php
/*  Отображения блоков с товарами в шаблоне
	template-black
 */
 
/* $query    - какие товары вывести 
 * $template - шаблон
 * $limit    - количество товаров
 */
class USAM_Display_Product_Groups
{
	private $limit;
	private $args;
	
	function __construct( $args = array() )
	{					
		if ( !empty($args) )
		{
			$this->args = $args;		
			$this->limit = !empty($args['limit'])?(int)$args['limit']:7;  //Количество товаров, для вывода
			$this->display();
		}
    }	

// из категории по 1 посту случайно	
	private function query_cat()
	{		
		global $wpdb, $type_price;
		$args = array(
			'hide_empty' => true, 		
			'number'     => $this->limit,	
			'fields'     => 'ids',	
			'orderby'    => 'count',	
 			'order'      => 'DESC',
		); 
		$all_cats_ids = get_terms( 'usam-category', $args );
		$proucts_id = array();
		foreach ($all_cats_ids as $cat_id)
		{ 					
			$proucts_id[] = $wpdb->get_var("SELECT ID FROM $wpdb->posts 			
			INNER JOIN $wpdb->postmeta AS pm ON ($wpdb->posts.ID = pm.post_id AND pm.meta_key = '_usam_price_$type_price')
			INNER JOIN $wpdb->postmeta AS pm2 ON ($wpdb->posts.ID = pm2.post_id AND pm2.meta_key = '_usam_stock')
			INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) 
			INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) 
			WHERE $wpdb->term_taxonomy.taxonomy = 'usam-category' AND $wpdb->term_taxonomy.term_id IN ('$cat_id') AND post_type = 'usam-product' AND CAST(pm.meta_value AS SIGNED) > '0' AND CAST(pm2.meta_value AS SIGNED) > '0' AND post_status = 'publish' ORDER BY rand() LIMIT 1");						
		}
		$query = array (			
			'post__in' => $proucts_id,				
		);		
		return $query;
	}	
	
	// из категории по 1 самому популярному посту	
	private function query_product_views_cat()
	{		
		global $wpdb;	
		
		$proucts_ids = $wpdb->get_col("SELECT ID FROM $wpdb->posts 
			INNER JOIN $wpdb->postmeta AS pm ON (ID = pm.post_id AND pm.meta_key = '_usam_product_views')
			INNER JOIN $wpdb->postmeta AS pm2 ON (ID = pm2.post_id AND pm2.meta_key = '_usam_stock')
			INNER JOIN $wpdb->term_relationships AS tr ON ($wpdb->posts.ID = tr.object_id) 
			INNER JOIN $wpdb->term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id AND tt.taxonomy = 'usam-category') 
			WHERE post_type = 'usam-product' AND post_status = 'publish' AND CAST(pm2.meta_value AS SIGNED) > '0' GROUP BY tt.term_id ORDER BY CAST(pm.meta_value AS SIGNED) DESC LIMIT $this->limit");
		
		$query = array (			
			'post__in' => $proucts_ids,
			'orderby' => 'post__in',
		);		
		return $query;
	}

	// Найти случайные товары
	private function query_rand()
	{			
		$query = array (			
			'orderby'   => 'rand',		
		);		
		return $query;
	}	
	
	// Избранные товары
	private function query_sticky()
	{			
		$query = array (			
			'orderby'   => 'rand',		
			'meta_key' => '_usam_sticky_products',
			'meta_value' => '1',				
		);		
		return $query;
	}	
		
	
// Популярные товары
	private function query_popularity()
	{				
		$query = array (		
			'orderby'   => array( 'meta_value_num' => 'DESC' ),		
			'meta_key' => '_usam_product_views',				
		);		
		return $query;
	}	
	
// Новинки	
	private function query_news()
	{				
		$query = array (			
			'orderby'   => '_usam_product_views',
			'order' => 'DESC',	
		);		
		return $query;
	}	
	
	private function query_also_bought()
	{				
		global $wpdb;
		$product_id = get_the_ID();
		
		$order_ids = $wpdb->get_col( "SELECT order_id FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE product_id='{$product_id}' ORDER BY order_id DESC LIMIT 20" );		
		if ( empty($order_ids) )
			return false;
		
		$product_ids = $wpdb->get_col( "SELECT DISTINCT product_id FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE product_id!={$product_id} AND order_id IN (".implode(",",$order_ids).") ORDER BY price DESC LIMIT 30" );			
		
		if ( empty($product_ids) )
			return false;	
	
		$query = array (			
			'post__in'   => array_values($product_ids),
			'order' => 'DESC',				
			'orderby' => 'post__in',				
		);		
		return $query;
	}	
	
// История просмотра товара	
	private function query_history_views()
	{				
		$product_id = get_the_ID();
		$args = array( 'user_list' => 'view', 'product_id__not_in' => $product_id, 'number' => $this->limit + 4, 'fields' => 'product_id' );
		$product_ids = usam_get_user_products( $args );	
		if ( empty($product_ids) )
			return false;	
	
		$query = array (			
			'post__in'   => array_values($product_ids),
			'order' => 'DESC',				
			'orderby' => 'post__in',	
		);		
		return $query;
	}	
	
		
	// Товары в одной коллекции
	private function query_collection()
	{				
		$product_id = get_the_ID();
		if ( empty($product_id) )
			return false;	

		$term = get_term_by( 'slug', 'collection', 'usam-product_attributes' );	
		if ( empty($term->term_id) )
			return false;	
		
		$collection = usam_get_product_meta( $product_id, 'product_attributes_'.$term->term_id );				
		if ( empty($collection))		
			return false;	
		
		$query = array (		
			'orderby'   => array( 'meta_value_num' => 'DESC' ),		
			'post__not_in' => array($product_id),
			'meta_key' => '_usam_product_attributes_'.$term->term_id,		
			'meta_value' => $collection,				
		);
		return $query;
	}
	
	// Товары на скидке
	private function query_sale()
	{				
		global $type_price;
		
		$query = array 
		(							
			'meta_query' => array( array( 'key' => '_usam_old_price_'.$type_price, 'value' => 0, 'compare' => '>', 'type' => 'NUMERIC') ),	
		);		
		return $query;
	}
	
	// upsell для текущего товара
	private function query_upsell()
	{				
		global $type_price;
		$product_id = get_the_ID();
		$categories = get_the_terms( $product_id, 'usam-category');
		$terms = array();
		if( $categories != '' )
		{			 
			foreach ($categories as $cat_item)
				$terms[] = $cat_item->slug;
		} 
		if ( empty($terms) )
			return false;	
		
		$price = usam_get_product_price( $product_id );
		
		if ( empty($price) )
			return false;	
		
		$query = array (				
			'tax_query' => array(
				array( 'taxonomy' => 'usam-category', 'field' => 'slug','terms' => $terms	)
			),	
			'meta_key' => '_usam_price_'.$type_price,
			'meta_query' => array( array( 'key' => '_usam_price_'.$type_price, 'value' => $price, 'compare' => '>', 'type' => 'NUMERIC') ),
			'post__not_in' => array($product_id),
			'order' => 'ASC',				
			'orderby'   => 'meta_value_num',
		);		
		return $query;
	}
	
	// в одной категории с текущим товаром
	private function query_same_category()
	{				
		$product_id = get_the_ID();
		$categories = get_the_terms( $product_id, 'usam-category');
		$terms = array();
		if( $categories != '' )
		{			 
			foreach ($categories as $cat_item)
				$terms[] = $cat_item->slug;
		}
		if ( empty($terms) )
			return false;	
		
		$query = array (				
			'tax_query' => array(
				array( 'taxonomy' => 'usam-category',	'field' => 'slug','terms' => $terms	)
			),	
			'post__not_in' => array($product_id),
			'order' => 'DESC',				
			'orderby'   => 'rand',
		);		
		return $query;
	}	
	
	// crosssell для текущего товара
	private function query_related_products()
	{		
		$product_id = get_the_ID();
		if ( empty($product_id) )
			return false;	

		$product_ids = usam_get_associated_products( $product_id, 'crosssell' );
		if ( empty($product_ids))		
			return false;	
		
		$query = array( 
			'post__in' => $product_ids, 
		);		
		return $query;
	}
	
	// с одинаковым тегом для текущего товара
	private function query_product_tag()
	{				
		$product_id = get_the_ID();
		$product_tag = wp_get_object_terms( $product_id, 'product_tag');		
		$terms = array();
		if( $product_tag != '' )
		{		
			foreach ($product_tag as $tag_item) {
				$terms[] = $tag_item->slug;
			}
		}
		if ( empty($terms) )
			return false;	
		
		$query = array (				
			'tax_query' => array(
				array( 'taxonomy' => 'product_tag',	'field' => 'slug','terms' => $terms	)
			),	
			'post__not_in' => array($product_id),
			'order'        => 'DESC',				
			'orderby'      => 'rand',
		);		
		return $query;
	}	

	private function get_query()
	{
		global $type_price;			
		
		$type_query = !empty($this->args['query'])?$this->args['query']:'query_popularity';
		
		$controller = 'query_'.$type_query;
		if ( !method_exists( $this, $controller ) )	
			$controller = 'query_popularity';
		
		$query = $this->$controller();
		if ( empty($query) )
			return false;
		
		$query['post_status'] = 'publish';
		$query['post_type'] = 'usam-product';
		$query['update_post_meta_cache'] = true;
		$query['update_post_term_cache'] = true;
		$query['cache_results'] = true;
		$query['posts_per_page'] = $this->limit;
		
		$meta_query = array(
				'relation' => 'AND',
				array('key' => '_usam_stock', 'value' => '0', 'compare' => '>','type' => 'numeric' ),	
				array( 'key' => '_usam_price_'.$type_price, 'value' => '0',	'compare' => '!=') 
			);
			
		if ( empty($query['meta_query']) )
			$query['meta_query'] = $meta_query;
		else
			$query['meta_query'] = array_merge( $meta_query, $query['meta_query'] );
	
		global $wp_query;
		$wp_query = new WP_Query( $query );		
	
		update_post_thumbnail_cache( $wp_query );
		return true;
	}
	
	function display()
	{
		global $title_products_for_buyers;
		
		$title_products_for_buyers = !empty($this->args['title'])?$this->args['title']:'';	
		
		if ( !$this->get_query() )
			return '';
		
		if( have_posts() )
		{				
			global $post;		
			$file_name = usam_get_module_template_file( 'products', $this->args['template'] );
			if ( file_exists($file_name) )
				include( $file_name );
		}
		wp_reset_postdata();
		wp_reset_query();
	}
} 

function usam_display_product_groups( $args )
{	
	new USAM_Display_Product_Groups( $args );	
}
?>