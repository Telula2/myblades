<?php
//================================ ВЫВОД СТРАНИЦ УНИВЕРСАМА ==================================================

//template.php 

new USAM_Page();
// Обрабатывает вывод основных страниц
class USAM_Page
{
	public function __construct()
	{ 		
		//add_filter( 'the_content', array($this,'content'), 12 );		
		add_action('template_redirect', array($this,'load_page_query'), 2 );	
		add_action('template_redirect', array($this,'template_redirect'), 30 );		
	}		
	
	public function template_redirect()
	{ 				
		$this->canonical_url();
		$this->swap_the_template();	
	}
	
	private function canonical_url() 
	{
		$usam_url = usam_change_canonical_url( null );
		if ( $usam_url != null ) {
			remove_action( 'wp_head', 'rel_canonical' );
			add_action( 'wp_head', 'usam_insert_canonical_url' );
		}
	}
	
	public function load_page_query( )
	{
		global $wp_query;		
	
		$obj = $wp_query->get_queried_object();
		$page_name = isset( $wp_query->post->post_name ) ? $wp_query->post->post_name : '';
		$this->load_page( $page_name );	
	}	
	
	// Узнать какая страница сейчас загружается и загрузить её
	public function load_page( $page_name )
	{						
		switch ( $page_name ) 
		{
			case 'basket' :			
				if ( ! defined( 'DONOTCACHEPAGE' ) )
					define( 'DONOTCACHEPAGE', true );	
				
			case 'checkout' :
				global $usam_gateway, $usam_checkout; 		
					
				if ( ! defined( 'DONOTCACHEPAGE' ) )
					define( 'DONOTCACHEPAGE', true );
				$usam_gateway = new USAM_Gateways();		
				
				$usam_checkout = new USAM_Checkout( );	
				$usam_checkout->public_part( );	
			break;
			case 'your-account' :
			
				if ( ! defined( 'DONOTCACHEPAGE' ) )
					define( 'DONOTCACHEPAGE', true );
				
				if (!is_user_logged_in() )
				{
					wp_redirect( usam_get_url_system_page('login') );
					exit;
				}					
				global $your_account; 
				$your_account = new USAM_Your_Account();				
			break;
		}
	}		
	
// Выбрать нужный шаблон	
	private function swap_the_template()
	{		
		do_action('usam_swap_the_template');			

		$template = $this->get_template();		
		if ( !empty($template) )
		{ 
			load_template( $template, false );			
			exit;
		} 
	}
	
	private function get_template()
	{
		global $post, $wp_query, $usam_query;		

		$obj = $wp_query->get_queried_object();
		$content = isset( $obj->post_content ) ? $obj->post_content : '';
		$page_name = isset( $wp_query->query_vars['pagename'] ) ? $wp_query->query_vars['pagename'] : '';
	
		$templates = array();		
		if( is_single() && get_query_var( 'post_type' ) == 'usam-product' )		
		{		
			$templates[] = USAM_THEMES_PATH."content-single_product-{$post->post_name}.php";
			$templates[] = USAM_THEMES_PATH."content-single_product-{$post->ID}.php";		
			$templates[] = usam_get_template_file_path("single-product");						
		}		
		elseif( $category = get_query_var( 'usam-category' ) )		
		{											
			$templates[] = usam_get_template_file_path("category-$category");	
			$templates[] = usam_get_template_file_path("category");	
			$templates[] = usam_get_template_file_path("page-products");		
			$templates[] = usam_get_template_file_path("page");
		}	
		elseif( $brand = get_query_var ('usam-brands' ) )
		{		
			$templates[] = usam_get_template_file_path("brand-$brand");
			$templates[] = usam_get_template_file_path("brand");	
			$templates[] = usam_get_template_file_path("page-products");		
			$templates[] = usam_get_template_file_path("page");
		}	
		elseif( $product_tag = get_query_var ('product_tag' ) )
		{		
			$templates[] = usam_get_template_file_path("product_tag-$product_tag");
			$templates[] = usam_get_template_file_path("product_tag");	
			$templates[] = usam_get_template_file_path("page-products");		
			$templates[] = usam_get_template_file_path("page");
		}	
		elseif( $category_sale = get_query_var ('usam-category_sale' ) )
		{				
			$templates[] = usam_get_template_file_path("category_sale-$category_sale");
			$templates[] = usam_get_template_file_path("category_sale");	
			$templates[] = usam_get_template_file_path("page-products");		
			$templates[] = usam_get_template_file_path("page");
		}					
		elseif ( is_user_logged_in() && $page_name == 'login' )
		{
			wp_redirect( usam_get_url_system_page('your-account') );
			exit;
		}	
		else
		{			
			$virtual_page = usam_get_virtual_page( $page_name );				
			if ( !empty($virtual_page) ) 
			{
				$file_name = str_replace("-", "_", $page_name);
				$templates[] = usam_get_template_file_path("page-$file_name");				
				$templates[] = usam_get_template_file_path("page");					
			}		
			else
			{
				$pages = usam_system_pages( );
				foreach( $pages as $page )
				{			
					$shortcode = '/'.preg_quote($page['content']).'/';
					if ( preg_match( $shortcode, $content) )
					{ 			
						$file_name = str_replace("-", "_", $page['name']);
						if( in_array($page['name'], array('new-arrivals','sale','recommend','popular') ) )
							$templates[] = usam_get_template_file_path("page-products_list");
						
						$templates[] = usam_get_template_file_path("page-$file_name");				
						$templates[] = usam_get_template_file_path("page");		
						break;
					}
				}				
			}
			if ( empty($templates) )
			{						
				$shortcode_pages = array( 'shareonline', 'readysets' );	
				foreach( $shortcode_pages as $file_name )
				{	
					if ( preg_match("/\[$file_name\]/", $content) )
					{ 							
						$templates[] = usam_get_template_file_path("page-$file_name");				
						$templates[] = usam_get_template_file_path("page");		
						break;
					}
				}
			}			
		} 
		if ( !empty($templates) )
		{ 
			foreach( $templates as $template )
			{ 
				if ( !empty($template) && file_exists( $template ) )
				{ 			
					return $template;
				}
			}
		} 
		return false;
	}
		
	//Обрабатывает замену тегов в страницах
	function content( $content = '' )
	{ 			
		if ( ! in_the_loop() )
			return $content;	
		
		remove_filter( 'the_content', array($this,'content'), 12 );
		
		return $content;
	}	
}

function usam_get_page_content( $pagename, $template_name )
{
	$page = new USAM_Page();
	$page->load_page( $pagename );
	
	ob_start();

	usam_load_template( $template_name );
	
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
?>