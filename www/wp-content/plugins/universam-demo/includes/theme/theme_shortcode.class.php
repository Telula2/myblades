<?php
/**
 * Класс шоркодов
 * @since 3.7
*/

new USAM_Theme_Shortcode();
class USAM_Theme_Shortcode
{			
	public function __construct() 
	{		
		add_shortcode('usam_products', array( &$this, 'products_shorttag') );
		add_shortcode('usam_product', array( &$this, 'product_shorttag') );
		add_shortcode('buy_now_button', array( &$this, 'buy_now_shortcode') );
		add_shortcode('usam_slider', array( &$this, 'slider') );
		add_shortcode('usam_search_my_order', array( &$this, 'search_my_order') );		
		add_shortcode('usam_showcategories', array( &$this, 'show_categories') );		
		add_shortcode( 'usam_recent_reviews', array(&$this, 'shortcode_reviews') );		
		add_shortcode( 'contactform', array(&$this, 'contactform') );
		add_shortcode( 'buy_product', array(&$this, 'buy_product') );
		add_shortcode( 'usam_point_receipt_products', array(&$this, 'point_receipt_products') );		
		add_shortcode( 'usam_map', array(&$this, 'map') );	
	}
	
	// Получить страницу "Контактная форма"
	function contactform( $atts ) 
	{
		$cf = new USAM_Contactform();
		return $cf->display();			
	}
	
	// показать кнопку "Добавить в корзину"
	function buy_product( $atts ) 
	{		
		$content = usam_get_buy_product_button( $atts['product_id'] );
		return $content;	
	}
	
	function shortcode_reviews( $atts ) 
	{			
		extract( shortcode_atts( array('postid' => 'all','num' => '3','hidecustom' => '0','hideresponse' => '0', 'snippet' => '0','more' => ''), $atts ) );
        
        if (strtolower($postid) == 'all') { $postid = -1; /* -1 queries all reviews */ }
        $postid = intval($postid);
        $num = intval($num);
        $hidecustom = intval($hidecustom);
        $hideresponse = intval($hideresponse);
        $snippet = intval($snippet);       
        
        if ($postid < -1) { $postid = -1; }
        if ($num < 1) { $num = 3; }
        if ($hidecustom < 0 || $hidecustom > 1) { $hidecustom = 0; }
        if ($hideresponse < 0 || $hideresponse > 1) { $hideresponse = 0; } 
        if ($snippet < 0) { $snippet = 0; }
      
        $_reviews = new USAM_Customer_Reviews_Theme();
		return $_reviews->output_reviews_show( $postid, array( 'per_page' => $num, 'hide_custom' => $hidecustom, 'hide_response' => $hideresponse, 'snippet_length' => $snippet, 'show_morelink' => $more ) );
	}
				
	function search_my_order( $atts ) 
	{			
	//	$text = __('Что с моим заказом?', 'usam');<label class='search_my_order' for='search_my_order_id-input'>$text</label>			
		$search_box_text = __('Введите номер заказа', 'usam');
		$text_display = '';	
		if ( !empty($_POST['order_id']) && is_numeric($_POST['order_id']) )
		{				
			$order_id = absint($_POST['order_id']);			
			$order = new USAM_Order( $order_id );	
			$order_data = $order->get_data();
			if ( !empty($order_data) )
			{
				$order_status = usam_get_order_status( $order_data['status'] );		
				
				$out = "<h4>".sprintf( __('Ваш заказ №%s','usam'), $order_data['id'])."</h4>";
				$out .= "<table>";
				$out .= "<tr><td><div class='status_order'>".__('Статус заказа','usam')."</td><td>".$order_status['name']."</div></td></tr>";
				$out .= "<tr><td><div class='description'>".__('Описание','usam')."</td><td>".$order_status['description']."</div></td></tr>";
				$out .= "</table>";
				
			}
		}
		else
		{
			$out = "<div class='search_my_order_id'>
					<form method='post' action='' id='search_my_order_id-form'>					
						<div class='search_my_order_id-box'>										
							<input type='search' class='txt_livesearch' id='search_my_order_id-input' name='order_id' value='' autocomplete='off' placeholder='$search_box_text'
							onblur=\"if (this.value =='') {this.value = '$search_box_text';}\" onfocus=\"if (this.value =='$search_box_text') {this.value = '';}\">
							<span class='bt_search' id='bt_search_my_order'></span>	
						</div>		
					</form>
					<div class='search_my_order_id-info'>$text_display</div>
				</div>";		
		}
		$out .= "<h2>Статус заказа может быть следующим:</h2>";
		$order_statuses = usam_get_order_statuses();	
		$out .= "<ul>";
		foreach ( $order_statuses as $key => $status )	
		{
			$out .= "<li><p><strong>«".$status->name."»</strong> - ".$status->description."</p></li>";
		}
		$out .= "</ul>";
		return $out;
	}

	/**
	* @return string - HTML отображения один или несколько продуктов, полученных из usam_display_products
	*/
	function products_shorttag( $atts ) 
	{	
		// отключить шорткод для продуктов
		if ( get_post_type() == 'usam-product' )
			return '';
		$query = shortcode_atts(array(
			'product_id' => 0,	
			'product_name' => null,
			'category_id' => 0,
			'category_url_name' => null,
			'tag' => null,
			'price' => 0, //if price = 'sale' it shows all sale products
			'limit_of_items' => 0,
			'sort_order' => null, // name,dragndrop,price,ID,author,date,title,modified,parent,rand,comment_count
			'order' => 'ASC', // ASC or DESC
			'number_per_page' => 0,
			'page' => 0,
		), $atts);
		$post_id_array = explode(',',$query['product_id']);
		$cat_id_array = explode(',',$query['category_id']);
		if(!empty($post_id_array) && count($post_id_array) > 1)
			$query['product_id'] = $post_id_array;	

		if(!empty($cat_id_array) && count($cat_id_array) > 1)
			$query['category_id'] = $cat_id_array;	
		if ( get_option('usam_product_pagination', 1) ) 
		{
			$page_number = get_query_var( 'paged' );
			if ( ! $page_number )
				$page_number = get_query_var( 'page' );
			$query['page'] = $page_number;		
			$query['number_per_page'] = get_option('usam_products_per_page', 24);
		}
		if ( ! empty( $atts['number_per_page'] ) )
			$query['number_per_page'] = $atts['number_per_page'];

		return usam_display_products_page( $query );
	}
	
	function map( $atts )
	{
		static $i;
		
		add_action('wp_footer', create_function( '', 'return wp_enqueue_script("yandex_maps");' ) );

		$title = !empty($atts['title'])?$atts['title']:'';
		$description = !empty($atts['description'])?$atts['description']:'';
		$GPS_S = !empty($atts['s'])?$atts['s']:'';
		$GPS_N = !empty($atts['n'])?$atts['n']:'';
		
		$i = empty($i)?0:$i+1;
		
		ob_start();			
		?>	
		<div id = "usam_maps">	
			<div id="usam_map-<?php echo $i; ?>" class="usam_map" data-title="<?php echo $title; ?>" data-description="<?php echo $description; ?>" data-gps_s="<?php echo $GPS_S; ?>" data-gps_n="<?php echo $GPS_N; ?>"></div>		
		</div>	
		<?php
		$output = ob_get_contents();			
		ob_end_clean();	
		return $output;
	}
	
	function point_receipt_products( $atts )
	{		
		$args = array( 'fields' => 'all' , 'orderby' => 'sort', 'order' => 'ASC', 'issuing' => 1 );
		if ( !empty($atts['point']) )
			$args['include'] = (int)$atts['point'];
		
		$city = usam_get_customer_city( );	
		if ( !empty($city) )
			$args['location_id'] = $city['id'];
		
		$stores = usam_get_stores( $args );		
		if ( empty($stores) && !empty($city) )
		{
			unset($args['location_id']);
			$stores = usam_get_stores( $args );		
		}
		add_action('wp_footer', create_function( '', 'return wp_enqueue_script("yandex_maps");' ) );
		
		ob_start();			
		?>						
		<div id="point_receipt_products">		
			<h2><?php 
			if ( !empty($city) )
				printf( __('Точки получения товара в городе %s', 'usam'), '«'.$city['name'].'»' ); 
			else
				_e('Точки получения товара', 'usam');
			?>	
			</h2>
			<div id = "usam_maps" class ="point_receipt_products_list">	
				<?php						
				foreach( $stores as $store )
				{					
					$location = usam_get_location( $store->location_id );
					$city = isset($location['name'])?$location['name']:'';
					$title = empty($store->address)?$store->title:__('город','usam').' '.$city.' '.$store->address;
					?>
					<div class="store_row">			
						<div class="name"><h3><?php echo $title; ?></h3></div>
						<?php if ( $store->phone != '' ) { ?>
						<div class="phone"><?php _e('Телефон', 'usam'); ?>: <?php echo $store->phone; ?></div>
						<?php }
						if ( $store->email != '' ) { ?>
						<div class="email"><?php _e('Электронная почта', 'usam'); ?>: <a href="mailto:<?php echo $store->email; ?>"><?php echo $store->email; ?></a></div>
						<?php }										
						if ( $store->schedule != '' ) { ?>
						<div class="shud"><?php _e('Режим работы', 'usam'); ?>: <?php echo $store->schedule; ?></div>
						<?php } ?>
						
						<?php if ( $store->description != '' ) { ?>
						<div class="desc"><?php _e('Описание', 'usam'); ?>: <?php echo $store->description; ?></div>
						<?php } ?>
						<div id="usam_map-<?php echo $store->id; ?>" class="usam_map" data-title="<?php echo $store->title; ?>" data-description="<?php echo $store->description; ?>" data-gps_s="<?php echo $store->GPS_S; ?>" data-gps_n="<?php echo $store->GPS_N; ?>"></div>
					</div>												
					<?php
				}
				?>		
			</div>				
		</div>	
		<?php
		$output = ob_get_contents();			
		ob_end_clean();	
		return $output;
	}
		
	function buy_now_shortcode($atts)
	{
		$output = usam_buy_now_button( $atts['product_id'], true );
		return $output;
	}
	
	function product_shorttag( $atts )
	{		
		$product_id = $atts['product_id'];	
		
		if ( !empty($atts['width']) && !empty($atts['height']) )
			$size = array($atts['width'], $atts['height']);
		else
			$size = 'product-image';
		
		$product_id = $atts['product_id'];
		$product = get_post( $product_id );		
		if ( empty($product) )
			return;
		
		$out = '<div class="usam_product_shorttag">';
			if ( !empty($atts['thumbnail']) )
				$out .= "<a href='$product->guid'>".usam_get_product_thumbnail($product_id, $size, $product->post_title)."</a>";		
			if ( !empty($atts['title']) )
				$out .= "<p class ='product_title'>$product->post_title</p>"; 
			if ( !empty($atts['price']) )
				$out .= "<p class ='product_price'>".usam_get_product_price_currency( $product_id )."</p>"; 
			
			if ( !empty($atts['add_to_cart']) )
			{
				static $fancy_notification_output = false;			
				$out .= usam_add_to_cart_button( $product_id, true );
				if ( ! $fancy_notification_output ) 
				{
					$out .= usam_fancy_notifications( false );
					$fancy_notification_output = true;
				}
			}			
		$out .= "</div>";
		return $out;
	}
	
	// отображает список категорий, когда код [showcategories] присутствует в посте или на странице.
	function show_categories( $atts ) 
	{		
		$args = array( "hide_empty" => 0, 'update_term_meta_cache' => 0 );
		if ( !empty($atts['child_of']) )
			$args['child_of'] = (int)$atts['child_of'];
		
		$args['orderby'] = 'meta_value_num';		
		$args['meta_key'] = 'usam_sort_order';	
		
		$output = '<div class="usam_show_categories">'.usam_get_walker_terms_list( $args, array( 'taxonomy' => 'usam-category' ) ).'</div>';	
		return $output;
	}
	
	function slider( $atts )
	{
		$id = 1;
		if ( !empty($atts['id']) )
			$id = $atts['id'];
		
		return usam_display_slider( $id );
	}
}
?>