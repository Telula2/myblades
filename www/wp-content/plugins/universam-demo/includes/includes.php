<?php
require_once( USAM_FILE_PATH . '/includes/customer/customer.php' );
require_once( USAM_FILE_PATH . '/includes/customer/bonus.class.php'    );
require_once( USAM_FILE_PATH . '/includes/customer/work_bonuses.class.php'    );
require_once( USAM_FILE_PATH . '/includes/customer/user_products_query.class.php' );
require_once( USAM_FILE_PATH . '/includes/customer/user_product.class.php' );

require_once( USAM_FILE_PATH . '/includes/installer.class.php' ); 
require_once( USAM_FILE_PATH . '/includes/post-types.class.php' );
require_once( USAM_FILE_PATH . '/includes/query/query.php'  );
require_once( USAM_FILE_PATH . '/includes/system_page.php' );	

require_once( USAM_FILE_PATH . '/includes/deprecated.php' );
require_once( USAM_FILE_PATH . '/includes/admin_bar.class.php'     );
require_once( USAM_FILE_PATH .'/includes/autocomplete_forms.class.php' );

require_once( USAM_FILE_PATH .'/includes/seo/seo.php' );

require_once( USAM_FILE_PATH .'/includes/system_processes.class.php' );
require_once( USAM_FILE_PATH .'/includes/ajax.php' );
require_once( USAM_FILE_PATH .'/includes/compare.functions.php' );
require_once( USAM_FILE_PATH .'/includes/option_data.php' );
//--------------//
require_once( USAM_FILE_PATH . '/includes/walker_nav_menu.php'    );

if ( isset($_REQUEST['usam_ajax_action']) || isset($_REQUEST['usam_action']) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) )
	require_once( USAM_FILE_PATH . '/includes/ajax.functions.php'     );

//Функции и классы товара
require_once( USAM_FILE_PATH . '/includes/product/product.php'               );
require_once( USAM_FILE_PATH . '/includes/product/product-template.php'      );
require_once( USAM_FILE_PATH . '/includes/product/product.class.php'         );
require_once( USAM_FILE_PATH . '/includes/product/price_comparison.class.php'      );
require_once( USAM_FILE_PATH . '/includes/product/product_download_status.class.php' );
require_once( USAM_FILE_PATH . '/includes/product/product_day.class.php'            );
require_once( USAM_FILE_PATH . '/includes/product/products_day_query.class.php'        );

require_once( USAM_FILE_PATH . '/includes/basket/cart.class.php'               );
require_once( USAM_FILE_PATH . '/includes/basket/checkout.class.php'           );
require_once( USAM_FILE_PATH . '/includes/basket/coupons.class.php'            );
require_once( USAM_FILE_PATH . '/includes/basket/merchant.class.php'           );
require_once( USAM_FILE_PATH . '/includes/basket/delivery_service.php'         );
require_once( USAM_FILE_PATH . '/includes/basket/payment_gateways_query.class.php'   );
require_once( USAM_FILE_PATH . '/includes/basket/delivery_services.class.php'   );
require_once( USAM_FILE_PATH . '/includes/basket/payment_gateway.php'          );
require_once( USAM_FILE_PATH . '/includes/basket/shipping.class.php'           );
require_once( USAM_FILE_PATH . '/includes/basket/taxes.class.php'              ); // Налог

require_once( USAM_FILE_PATH . '/includes/template.php'                     );
require_once( USAM_FILE_PATH . '/includes/theme.php'                        );

require_once( USAM_FILE_PATH . '/includes/printing-form.php' );
require_once( USAM_FILE_PATH . '/includes/misc.functions.php'                  );
require_once( USAM_FILE_PATH . '/includes/screen.php'                          );
require_once( USAM_FILE_PATH . '/includes/file.php'                            );

require_once( USAM_FILE_PATH . '/includes/seo/determining_position_site.class.php' );		

// Обратная связь
require_once( USAM_FILE_PATH . '/includes/feedback/feedback_query.class.php'    );
require_once( USAM_FILE_PATH . '/includes/feedback/feedback_init.class.php'    );
require_once( USAM_FILE_PATH . '/includes/feedback/feedback.class.php'    );
require_once( USAM_FILE_PATH . '/includes/feedback/contactform.class.php'      );
require_once( USAM_FILE_PATH . '/includes/feedback/chat_init.class.php'        );
require_once( USAM_FILE_PATH . '/includes/feedback/chat.class.php'             );
require_once( USAM_FILE_PATH .'/includes/feedback/customer_reviews.class.php'  );
require_once( USAM_FILE_PATH .'/includes/feedback/customer_reviews_theme.class.php'  );
require_once( USAM_FILE_PATH .'/includes/feedback/customer_reviews_query.class.php' );
require_once( USAM_FILE_PATH .'/includes/feedback/email.class.php'  );
require_once( USAM_FILE_PATH .'/includes/feedback/email_query.class.php'  );
require_once( USAM_FILE_PATH .'/includes/feedback/sms.class.php'  );

// CRM
require_once( USAM_FILE_PATH . '/includes/crm/crm.helpers.php'          );
require_once( USAM_FILE_PATH . '/includes/crm/event.class.php'          );
require_once( USAM_FILE_PATH . '/includes/crm/events_query.class.php'  );
require_once( USAM_FILE_PATH . '/includes/crm/contact.php'              );
require_once( USAM_FILE_PATH . '/includes/crm/company.php'              );
require_once( USAM_FILE_PATH . '/includes/crm/customers_document.class.php' );
require_once( USAM_FILE_PATH . '/includes/crm/means_communication.php'  );
require_once( USAM_FILE_PATH . '/includes/crm/companies_query.class.php' );	
require_once( USAM_FILE_PATH .'/includes/crm/document.class.php'  );
require_once( USAM_FILE_PATH ."/includes/mcommunicator/sms_gateway.class.php" );

require_once( USAM_FILE_PATH . '/includes/taxonomy/category.php'     );
require_once( USAM_FILE_PATH . '/includes/taxonomy/category_sale.php'     );
require_once( USAM_FILE_PATH . '/includes/taxonomy/brand.php'     );
require_once( USAM_FILE_PATH . '/includes/taxonomy/taxonomy.php'     );
require_once( USAM_FILE_PATH . '/includes/taxonomy/variations.class.php'  );

require_once( USAM_FILE_PATH . '/includes/processing.functions.php'            );
require_once( USAM_FILE_PATH . '/includes/formatting.functions.php'            );
require_once( USAM_FILE_PATH . '/includes/notification.class.php'              );
require_once( USAM_FILE_PATH . '/includes/shortcode.class.php'                 );	
require_once( USAM_FILE_PATH . '/includes/currency.class.php'                  );
require_once( USAM_FILE_PATH . '/includes/country.class.php'                   );
require_once( USAM_FILE_PATH . '/includes/location.php'                        );
require_once( USAM_FILE_PATH . '/includes/locations_query.class.php'           );
require_once( USAM_FILE_PATH . '/includes/store.class.php'                     );
require_once( USAM_FILE_PATH . '/includes/currency.helpers.php'                );
require_once( USAM_FILE_PATH . '/includes/manager_notification.class.php'      );
require_once( USAM_FILE_PATH . '/includes/ftp.class.php'                       );

require_once( USAM_FILE_PATH . '/includes/personal.class.php');
require_once( USAM_FILE_PATH . '/includes/webspy.class.php'    );
require_once( USAM_FILE_PATH . '/includes/cron.php'            );
include_once( USAM_FILE_PATH . '/includes/theme/slider.php'    );
require_once( USAM_FILE_PATH . '/includes/assets.class.php'    );	
require_once( USAM_FILE_PATH . '/includes/universam_api.class.php' );
require_once( USAM_FILE_PATH . '/includes/tracker.class.php' );	

require_once( USAM_FILE_PATH . '/includes/data_exchange/data_exchange.class.php'  );
require_once( USAM_FILE_PATH . '/includes/data_exchange/export-import.functions.php');

require_once( USAM_FILE_PATH . '/includes/technical/technical.functions.php'   );
require_once( USAM_FILE_PATH . '/includes/technical/support_message.class.php' );

//Рассылка
require_once( USAM_FILE_PATH . '/includes/mailings/sending_messages.class.php' );
require_once( USAM_FILE_PATH . '/includes/mailings/newsletter.class.php' );
require_once( USAM_FILE_PATH . '/includes/mailings/subscriber-list.class.php'  );
require_once( USAM_FILE_PATH . '/includes/mailings/trigger.class.php' );
require_once( USAM_FILE_PATH . '/includes/mailings/email-styling.php' ); // Подключение стиль письма
require_once( USAM_FILE_PATH . '/includes/mailings/pop3worker.class.php'    );//Управление почтой

//Заказ
require_once( USAM_FILE_PATH . '/includes/order/order.class.php'        );
require_once( USAM_FILE_PATH . '/includes/order/order_status.class.php'        );
require_once( USAM_FILE_PATH . '/includes/order/orders_query.class.php'         );
require_once( USAM_FILE_PATH . '/includes/order/payments_query.class.php' );
require_once( USAM_FILE_PATH . '/includes/order/payment.class.php'              );
require_once( USAM_FILE_PATH . '/includes/order/payment.helpers.php'            );
require_once( USAM_FILE_PATH . '/includes/order/order.helpers.php'            );
require_once( USAM_FILE_PATH . '/includes/order/order_status_change.class.php' );
require_once( USAM_FILE_PATH . '/includes/order/order-notification.class.php' );
require_once( USAM_FILE_PATH . '/includes/order/payment-notification.class.php' );
require_once( USAM_FILE_PATH . '/includes/order/shipped_document.class.php'     );
require_once( USAM_FILE_PATH . '/includes/order/shipped_documents_query.class.php'  );
require_once( USAM_FILE_PATH . '/includes/order/order_property.class.php' );
require_once( USAM_FILE_PATH . '/includes/order/order_properties_value_query.php'  );
require_once( USAM_FILE_PATH . '/includes/order/order_properties_query.php' );	
require_once( USAM_FILE_PATH . '/includes/order/product_order.php'     );
require_once( USAM_FILE_PATH . '/includes/order/order_shortcode.class.php'   );
require_once( USAM_FILE_PATH . '/includes/order/return_purchases.class.php'     );
require_once( USAM_FILE_PATH . '/includes/order/return_purchases_query.class.php'   );
require_once( USAM_FILE_PATH . '/includes/cashbox.class.php'               );	

// Социальные сети
require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte.class.php' );
//require_once( USAM_FILE_PATH . '/includes/social_networks/odnoklassniki.class.php' );

//Поиск
require_once( USAM_FILE_PATH . '/includes/search/class-search-filter.php' );
require_once( USAM_FILE_PATH . '/includes/search/class-search-shortcodes.php' );

// Виджеты
include_once( USAM_FILE_PATH . '/widgets/product_tag_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/shopping_cart_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/specials_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/latest_product_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/price_range_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/admin_menu_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/category_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/product_groups_widget.php' );
include_once( USAM_FILE_PATH . '/widgets/search-widgets.php' );
?>