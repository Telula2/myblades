<?php
// * основной класс ЖУРНАЛА ПРОДАЖ. Создает заказы в таблице*
class USAM_Order
{	
	const CREATURE  = 'incomplete_sale';
	const CLOSED    = 'closed';	
	/**
	 * Имена колонок таблицы Журнала продаж в базе данных
	 * @since 4.9
	 */
	 // строковые
	private static $string_cols = array(
		'sessionid',	
		'coupon_name',
		'order_type',				
		'date_modified',
		'date_insert',
		'date_paid',		
		'notes',	
		'cancellation_reason',
		'type_price',
		'status',				
	);
	// цифровые
	private static $int_cols = array(
		'id',			
		'user_ID',	
		'customer_id',		
		'manager_id',			
		'type_payer',		
		'paid',			
	);	
	// цифровые
	private static $float_cols = array(
		'coupon_discount',		
		'discount',		
		'total_tax',			
		'bonus',				
		'totalprice',			
	);
	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */
	private $order_id = null;	
	private $data = array();		
	private $fetched           = false;
	private $is_status_changed = false;
	private $is_order_paid     = false;	
	private $previous_status   = false;	
	
	private $recalculated = false; // Пересчитать заказ

	private $order_products = array();	
	private $customer_data = array();	
	private $payment_history = array();
	private $payment_status_sum = array();	
	private $document_shipped = array();
	private $document_payment = null;
	private $downloadable_files = null;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
		
	private $currentitem = -1;	
	private $number_product_cart = 0;
	private $purchitem;
	
	private $payment_amount = 0;	
	/**
	 * Конструктор объекта Журнала покупок. Если аргумент не передается, то создаст новый пустой объект. В противном случае, будет пытаться получить заказы из БД либо с помощью id заказа или SessionID.
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{		
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value ); 
			return;
		}
		if ( ! in_array( $col, array( 'id', 'sessionid' ) ) )
			return;		
			
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'sessionid'  && $id = wp_cache_get( $value, 'usam_order_sessionid' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{
			$this->order_id = $value;			
			$this->data = wp_cache_get( $value, 'usam_order' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;			
		}	
		else
			$this->fetch();		
	}

	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		
		return '%d';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{ 
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_order' );
		if ( $sessionid = $log->get( 'sessionid' ) )
			wp_cache_set( $sessionid, $id, 'usam_order_sessionid' );	
		do_action( 'usam_order_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{ 
		$log = new USAM_Order( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_order' );
		wp_cache_delete( $log->get( 'sessionid' ), 'usam_order_sessionid' );
		wp_cache_delete( $log->get( 'id' ), 'usam_products_order' );
		wp_cache_delete( $log->get( 'id' ), 'usam_shipped_documents_order' );
		wp_cache_delete( $log->get( 'id' ), 'usam_payment_order' );
		wp_cache_delete( $log->get( 'id' ), 'usam_properties_order' );
		do_action( 'usam_order_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $order_id ) 
	{		
		do_action( 'usam_order_before_delete', $order_id );
		self::delete_cache( $order_id );
		usam_delete_order( $order_id );	
		do_action( 'usam_order_delete', $order_id );
	}		
	
	private function get_from_base_customer_data( ) 
	{							
		$order_id = $this->get('id');
		$object_type = 'usam_properties_order';	
		$cache = wp_cache_get( $order_id, $object_type );			
		if ( $cache === false )			
		{							
			global $wpdb;	
			$sql = "SELECT * FROM ".USAM_TABLE_ORDER_PROPS_VALUE." WHERE order_id = %d";
			$cache = $wpdb->get_results( $wpdb->prepare( $sql, $order_id ) );

			wp_cache_set( $order_id, $cache, $object_type );						
		}	
		$order_id = $this->get('id');
		return apply_filters( 'usam_get_order_customer_data', $cache, $order_id );
	}
		
	// Получить данные клиента
	public function get_customer_data( $format = 'unique_name' ) 
	{			
		$data = $this->get_from_base_customer_data( );	
		
		$return = array();
		if ( $format == 'unique_name' )
		{
			if ( empty( $this->customer_data ) )	
			{
				foreach ( $data as $fields ) 				
					$return[$fields->unique_name] = (array)$fields;			
			}
			else
				$return = $this->customer_data;
		}
		else
			$return = (array)$data;
	
		return $return;
	}
	
	// Получить заданные данные клиента
	public function get_item_customer_data( $key ) 
	{
		$data = $this->get_customer_data( 'unique_name' );	
		$value = isset( $data[$key] ) ? $data[$key] : null;
		return apply_filters( 'get_item_customer_data', $value, $key, $this );
	}	
	
	/**
	 * Сохраняет данные клиента в заказе. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.	
	 */
	public function save_customer_data( $key, $value = null ) 
	{		
		if ( $this->exists == 0 )
			return false;
		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );
		}	
		global $wpdb;	
		
		$properties = apply_filters( 'usam_customer_data_set_properties', $properties, $this );	
		
		do_action( 'usam_customer_data_pre_save', $this );
		$i = 0;				
		$insert = false;		
		$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );	
		
		$order_id = $this->get('id');
		$customer_data = $this->get_customer_data( 'unique_name' );	

		foreach( $properties as $unique_name => $data )
		{						
			if ( !isset($list_properties[$unique_name]) )
				continue;			
			
			$update = 0;
			if ( is_array($data) )
			{
				$new = array();
				foreach( $data as $value )
				{
					$value = stripcslashes(wp_unslash($value));		
					$new[] = html_entity_decode( $value, ENT_QUOTES,'UTF-8');
				}
				$data = serialize($new);
			}
			else
			{			
				$data = stripcslashes(wp_unslash($data));		
				$data = html_entity_decode( $data, ENT_QUOTES,'UTF-8');
			}
			if ( $list_properties[$unique_name]->type == 'phone' || $list_properties[$unique_name]->type == 'mobile_phone' )		
				$data = preg_replace('/[^0-9]/', '', $data);
			elseif ( $list_properties[$unique_name]->type == 'email' )		
				$data = str_replace(" ", "", $data);
	
			$code = $list_properties[$unique_name]->unique_name;
			if ( isset($customer_data[$code]) )
			{			
				if ( $customer_data[$code]['value'] != $data )
				{	
					$update_data = array( 'value' => $data );
					$where = array( 'order_id' => $order_id, 'unique_name' => $code ); 
					$update = $wpdb->update( USAM_TABLE_ORDER_PROPS_VALUE, $update_data, $where, array( '%s' ), array( '%d', '%s' ) );
					$customer_data[$code]['value'] = $data;

					$description = sprintf( __('%s: %s '), $list_properties[$unique_name]->name, $data );	
					$this->order_change_history( 'update_customer_data', $description );					
				}
			}
			elseif ( $data != '' && isset($list_properties[$unique_name]) )			
			{			
				$insert_data = array( 'order_id' => $order_id, 'unique_name' => $code, 'value' => $data );				
				$update = $wpdb->insert( USAM_TABLE_ORDER_PROPS_VALUE, $insert_data, array( '%d', '%s', '%s' ) );
				$customer_data[$code] = array( 'id' => $wpdb->insert_id, 'unique_name' => $code, 'order_id' => $order_id, 'value' => $data );
								
				$description = sprintf( __('%s: %s '), $list_properties[$unique_name]->name, $data );	
				$this->order_change_history( 'insert_customer_data', $description );				
				
				$insert = true;				
			}					
			if ( $update >= 1 )		
			{
				$i++;						
				$customer_data[$code]['value'] = $data;	
			}
		}				
		wp_cache_delete( $order_id, 'usam_properties_order' );	
		
		
		do_action( 'usam_insert_data_customer_after', $order_id, $customer_data, $this );	
		do_action( 'usam_customer_data_save', $this );	
		
		return $i;
	}

// ДОКУМЕНТЫ ОТГРУЗКИ==========================================================================================================

	/**
	 * Добавить документ отгрузки
	 */
	public function add_document_shipped( $document_shipped ) 
	{			
		if ( $this->exists == 0 )
			return false;
		
		$document_shipped['order_id'] = $this->get('id');			
		if ( !empty($document_shipped['products']) ) 
		{		
			$products = $document_shipped['products'];
			unset($document_shipped['products']);
		}
		else
			$products = array();
		$shipped = new USAM_Shipped_Document( $document_shipped );		
		$shipped->save();		
		$shipped->set_products( $products );		
		
		$document_id = $shipped->get( 'id' );		
		$description = sprintf( __('Добавлен документ отгрузки №%s.'), $document_id );	
		$this->order_change_history( 'add_document_shipped', $description );	
		return $document_id;
	}		
		
	//Обновляет документ отгрузки
	public function edit_document_shipped( $document_id, $document_shipped ) 
	{			
		$shipped = new USAM_Shipped_Document( $document_id );
		$order_shipped_id = $shipped->get( 'order_id' );
		
		$order_id = $this->get('id');		
		if ( $order_id != $order_shipped_id )
			return false;	
		
		if ( !empty($document_shipped['products']) ) 
		{
			$products = $document_shipped['products'];
			$shipped->set_products( $products );		
			unset($document_shipped['products']);
		}			
		$shipped->set( $document_shipped );		
		$shipped->save();	
		
		$description = sprintf( __('Обновлен документ отгрузки №%s.'), $document_id );	
		$this->order_change_history( 'edit_document_shipped', $description );	
		return false;
	}	
		
	public function get_not_shipped_products( )
	{
		global $wpdb;
		$id = $this->get( 'id' );	
	
		$sql = "SELECT cc.id, cc.name, (cc.quantity-IFNULL(t.quantity,0)) AS quantity, cc.product_id, t.reserve FROM `".USAM_TABLE_PRODUCTS_ORDER."` AS cc		
			LEFT JOIN 
			( SELECT SUM(sp.quantity) AS quantity, sp.basket_id, sd.id, sp.reserve FROM `".USAM_TABLE_SHIPPED_DOCUMENTS."` AS sd LEFT JOIN `".USAM_TABLE_SHIPPED_PRODUCTS."` AS sp ON (sd.id=sp.documents_id ) WHERE sd.order_id='{$id}' GROUP BY sp.basket_id) 
			AS t ON (cc.id=t.basket_id)			
			WHERE cc.order_id='{$id}' AND ( cc.quantity-IFNULL(t.quantity,0)) > 0";		
		$products = $wpdb->get_results( $sql );		
		return $products;
	}
	
	// Получить товары отгрузки
	public function get_products_shipping_document( $document_id ) 
	{
		global $wpdb;
		
		$products = $wpdb->get_results( $wpdb->prepare( "SELECT cc.*, sp.quantity AS quantity_shipment, sp.reserve AS reserve FROM `". USAM_TABLE_SHIPPED_PRODUCTS ."` AS sp 
					LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc ON ( cc.id=sp.basket_id )					
					WHERE sp.documents_id = '%d'", $document_id), ARRAY_A );
					
		return $products;
	}		
	
	// Получить общую сумму доставки
	public function get_delivery_order_price( ) 
	{
		global $wpdb;
		$order_id = $this->get('id');
		$price = $wpdb->get_var( "SELECT SUM(price) FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE order_id = '{$order_id}'" );
		return $price;
	}
	
	// Получить данные об отгрузках
	public function get_shipped_documents( ) 
	{		
		$order_id = $this->get('id');
		if ( empty($order_id) )
			return array();
		
		$object_type = 'usam_shipped_documents_order';		
		$cache = wp_cache_get( $order_id, $object_type );
		if ( $cache === false )		
		{	
			global $wpdb;				
			$cache = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE order_id = '{$order_id}'" );
			if ( empty($cache) )
				$cache = array();
			
			wp_cache_set( $order_id, $cache, $object_type );
		}	
		return $cache;
	}		
	
	/**
	 * Получить данные об отгрузках включая товары
	 */
	public function get_full_details_of_shipment( ) 
	{			
		if ( !empty($this->document_shipped) )	
			return $this->document_shipped;		
		
		$documents = $this->get_shipped_documents();	
		foreach( $documents as $document  )
		{	
			$this->document_shipped[$document->id] = (array)$document;			
			$this->document_shipped[$document->id]['products'] = $this->get_products_shipping_document($document->id);
			
		}		
		return $this->document_shipped;
	}	
	
// ДОКУМЕНТЫ ОПЛАТЫ==========================================================================================================
	
	// Проверить документ оплаты на возможность оплаты
	public function document_check_payment( $payment ) 
	{					
		$result = false;	
		if ( $payment->status == 1 && $payment->pay_up >= date( "Y-m-d H:i:s" ) )
			$result = true;	
		return $result;
	}	
	
	// Получить документ оплаты
	public function get_payment_documents( ) 
	{				
		$order_id = $this->get('id');
		if ( empty($order_id) )
			return array();
		
		$query = array( 'order' => 'DESC', 'document_id' => $order_id, 'document_type' => 'order' );			
		$object_type = 'usam_payment_order';	
		$cache = wp_cache_get( $order_id, $object_type );			
		if ( $cache === false )			
		{							
			$payments = new USAM_Payments_Query( $query );	
			$cache = $payments->get_results();	

			wp_cache_set( $order_id, $cache, $object_type );						
		}	
		return $cache;
	}		
			
	// Получить данные о платежах
	public function get_payment_history( ) 
	{					
		if ( !empty($this->payment_history) )	
			return $this->payment_history;
		
		$payments = $this->get_payment_documents();	
		$current_date = date("Y-m-d H:i:s");
		
		$this->payment_status_sum = array('can_be_paid' => 0, 'total_unpaid' => 0, 'total_rejected' => 0, 'total_paid' => 0, 'total' => 0, 'repayment' => 0 );
		foreach ($payments as $key => $payment)
		{
			$this->payment_history[] = array(
				'id' => $payment->id,
				'document_number' => $payment->document_number,
				'date_time' => $payment->date_time,
				'date_payed' => $payment->date_payed,
				'pay_up' => $payment->pay_up,
				'sum' => $payment->sum,
				'name' => $payment->name,					
				'status' => $payment->status,
				'status_name' => usam_get_payment_document_status_name( $payment->status ),
				'gateway_id' => $payment->gateway_id,	
				'payment_type' => $payment->payment_type,				
				'transactid' => $payment->transactid,				
			);			
			switch ( $payment->status ) 
			{
				case 1:
					if ( $payment->pay_up >= $current_date )
					{ // Можно оплатить
						$this->payment_status_sum['can_be_paid'] += $payment->sum;
					}
					$this->payment_status_sum['total_unpaid'] += $payment->sum;											
				break;
				case 2:
					$this->payment_status_sum['total_rejected'] += $payment->sum;											
				break;												
				case 3:			// Оплачено					
					if ( $payment->payment_type )
						$this->payment_status_sum['total_paid'] += $payment->sum;											
				break;
			}
			if ( $payment->payment_type == 0 )
				$this->payment_status_sum['repayment'] += $payment->sum;
			else			
				$this->payment_status_sum['total'] += $payment->sum;		
		}		
		$this->payment_status_sum['payment_required'] = $this->data['totalprice'] - $this->payment_status_sum['total_paid'];
		if ( $this->payment_status_sum['payment_required'] == 0 )
			$this->payment_status_sum['order_status'] = __( 'Оплачен', 'usam' );
		elseif ( $this->payment_status_sum['payment_required'] == $this->data['totalprice'] )
			$this->payment_status_sum['order_status'] = __( 'Не оплачен', 'usam' );	
		elseif ( $this->payment_status_sum['payment_required'] > 0 )
			$this->payment_status_sum['order_status'] = __( 'Частично оплачен', 'usam' );		
		else
			$this->payment_status_sum['order_status'] = __( 'Переплачен', 'usam' );				
		return $this->payment_history;
	}	
	
	public function get_payment_status_sum( ) 
	{
		$this->get_payment_history();
		return $this->payment_status_sum;
	}
	
	// Последний документ оплаты
	public function get_payment_document( ) 
	{				
		$payment_documents = $this->get_payment_documents();	
		$payment_document = (array)array_pop($payment_documents);		
		return $payment_document;
	}
	
	// Последний способ доставки
	public function get_shipped_document( ) 
	{				
		$shipped_documents = $this->get_shipped_documents();	
		$shipped_document = (array)array_pop($shipped_documents);		
		return $shipped_document;
	}
	
	// Получить сумму оплаты
	public function get_order_payment_amount(  ) 
	{
		if ( empty( $this->payment_amount) )		
		{
			global $wpdb;			
			$order_id = $this->get('id');
			$this->payment_amount = $wpdb->get_var( "SELECT SUM(sum) FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE document_id={$order_id} AND document_type='order' AND status = '3'" );
		}	
		return $this->payment_amount;
	}	
	
	/**
	 * Добавляет способ оплаты.
	 */
	public function add_payment_history( $payment ) 
	{
		if ( $this->exists == 0 )
			return false;
		
		$payment['document_id']   =	$this->get('id');
		$payment['document_type'] =	'order';
		
		if ( empty($payment['sum']) )			
			$payment['sum'] = $this->get('totalprice');		
		
		$shipped = new USAM_Payment_Document( $payment );		
		$shipped->save();			
	
		return $shipped->get('document_number');
	}
	
	//Обновляет способ оплаты.
	public function edit_payment_history( $payment, $col = 'id' ) 
	{
		if ( $this->exists == 0 )
			return false;
		
		$payment['document_id']   =	$this->get('id');
		$payment['document_type'] =	'order';		

		$payment_document = new USAM_Payment_Document( $payment[$col], $col );
		$payment_document->set( $payment );
		$update = $payment_document->save();			
		
		$this->payment_history = array();	
		return $update;
	}		
	
	// Пометить заказ как оплаченный
	public function mark_order_as_paid( ) 
	{			
		$totalprice = $this->get( 'totalprice' );		
		$sum_payment = $this->get_order_payment_amount( );
			
		if ( $sum_payment >= $totalprice )
		{	// Полностью оплачен					
			$order_id = $this->get('id');			
			$args['date_paid'] = usam_get_payments( array( 'order' => 'DESC', 'document_id' => $order_id, 'document_type' => 'order', 'status' => 3, 'fields' => 'date_payed', 'number' => 1 ) ); 
			
			$args['paid'] = 2;			
			$this->set( $args );
			$this->save( );
		}			
		elseif ( $sum_payment > 0 )
		{ // Частично оплачен		
			$args['paid'] = 1;
			$args['date_paid'] = NULL;
			$this->set( $args );
			$this->save( );
		}	
		elseif ( $sum_payment == 0 )
		{ // Не оплачен				
			$args['paid'] = 0;
			$args['date_paid'] = NULL;
			$this->set( $args );
			$this->save( );
		} 		
	}
	
	public function get_downloadable_files( )
	{
		global $wpdb;	
		if ( empty($this->downloadable_files) )
		{			
			$order_id = $this->get('id');
			$this->downloadable_files = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_DOWNLOAD_STATUS." WHERE order_id = '%d'", $order_id ) );	
		}
		return $this->downloadable_files;
	}

	public function edit_downloadable_files( $files ) 
	{			
		$files = apply_filters( 'usam_downloadable_files_set_properties', $files, $this );			
		do_action( 'usam_downloadable_files_pre_edit', $this );
		
		$i = 0;		
		foreach( $files as $file_id => $data )
		{		
			$download_status = new USAM_PRODUCT_DOWNLOAD_STATUS( $file_id );
			$download_status->set( $data );			
			$download_status->save();		
		}		
		$this->downloadable_files = array();
		
		do_action( 'usam_downloadable_files_edit', $this );
		return $i;
	}			
	
	function next_purch_item() 
	{
		$this->currentitem++;
		$this->purchitem = $this->order_products[$this->currentitem];
		return $this->purchitem;
	}

	function the_purch_item() 
	{
		$this->purchitem = $this->next_purch_item();
	}

	function have_purch_item() 
	{
		if ( $this->currentitem + 1 < $this->number_product_cart )
			return true;	  
		else if ( $this->currentitem + 1 == $this->number_product_cart && $this->number_product_cart > 0 ) 
			$this->rewind_purch_item();	  
		return false;
	}

	function rewind_purch_item() 
	{
		$this->currentitem = -1;
		if ( $this->number_product_cart > 0 )
			$this->purchitem = $this->order_products[0];
	}	
	
	// Получить купленные товары
	public function get_order_products( ) 
	{		
		if ( empty($this->order_id) )
			return array();
		
		$object_type = "usam_products_order";
		$cache = wp_cache_get( $this->order_id, $object_type );		
		if ( $cache === false )		
		{
			global $wpdb;			
			
			$cache = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE order_id = '%d'", $this->order_id ) );			
			if ( empty($cache))
				$cache = array();			
			wp_cache_set( $this->order_id, $cache, $object_type );	
		}	
		$this->order_products = $cache;			
		return $this->order_products;
	}	
	
	// Формат таблицы товаров заказа
	private function get_data_format_order_product( $data ) 
	{
		$formats_bd = array( 'id' => '%d', 'product_id' => '%d', 'name' => '%s', 'order_id' => '%d', 'price' => '%f', 'old_price' => '%f', 'product_day' => '%d', 'tax' => '%f', 'quantity' => '%d', 'bonus' => '%f', 'subscribed' => '%s', 'date_insert' => '%s', 'date_modified' => '%s' );		
		$formats = array();		
		foreach ( $data as $key => $value ) 
		{						
			if ( isset($formats_bd[$key]) )		
				$formats[$key] = $formats_bd[$key];				
		}
		return $formats;
	}
	
	// Изменить купленные товары
	public function edit_order_products( $products ) 
	{	
		global $wpdb;		
		
		$products = apply_filters( 'usam_edit_order_products_set_properties', $products, $this );			
		do_action( 'usam_edit_order_products_pre_edit', $this );
		$i = 0;			
		$this->get_order_products();		
		foreach( $products as $key => $product )
		{	
			$products[$key]['date_modified'] = date( "Y-m-d H:i:s" );
			if ( isset($product['old_price']) && $product['old_price'] == 0 )			
				$products[$key]['old_price'] = $product['price'];
		}		
		foreach( $products as $id => $product )
		{				
			$format = $this->get_data_format_order_product( $product );			
			$where = array( 'id' => $id, 'order_id' => $this->order_id );			
			$format_where = $this->get_data_format_order_product( $where );				
			$wpdb->update( USAM_TABLE_PRODUCTS_ORDER, $product, $where, $format, $format_where );	
		}		
		if ( !empty($this->order_products) )
		{
			foreach( $this->order_products as $number => $product )
			{				
				if ( isset($products[$product->id]) )	
				{							
					foreach( $products[$product->id] as $key => $value )
					{
						if ( $value != $product->$key )
						{
							$description = '';
							switch ( $key ) 
							{
								case 'price':
									$description = sprintf( __('Изменение цены товара %s с %s на %s.'), $product->name, $product->$key, $value );		
								break;	
								case 'old_price':
									$description = sprintf( __('Изменение старой цены товара %s с %s на %s.'), $product->name, $product->$key, $value );		
								break;	
								case 'quantity':
									$description = sprintf( __('Изменение количества товара %s с %s на %s.'), $product->name, $product->$key, $value );											
								break;										
							}						
							if ( !empty($description) )
								$this->order_change_history( 'edit_product', $description );	
						}
						$this->order_products[$number]->$key = $value;	
					}							
				}
			} 						
			wp_cache_set( $this->order_id, $this->order_products, "usam_products_order" );
		}	
		$this->recalculated = true;		
		
		$this->save();	
		do_action( 'usam_edit_order_products_edit', $this );
		return $i;
	}	
		
	public function add_order_products( $products, $shipped_document_id = 0 ) 
	{
		global $wpdb;
		
		if ( $this->exists == 0 )
			return false;
		
		$insert = false;
		$shipped_products = array();
		$download = get_option('usam_max_downloads');
		
		$discounts_rule = usam_get_product_discount_rules( array( 'fields' => 'id=>data') );	
		$product_ids = array();
		foreach( $products as $product )
		{
			if ( !empty($product['product_id']) )
				$product_ids[] = $product['product_id'];
		}
		if ( empty($product_ids) )
			return false;
		
		$type_price = $this->get('type_price');
		$order_id = $this->get('id');		
		$products_discounts = usam_get_products_discounts( array('product_id' => $product_ids, 'code_price' => $type_price ) );		
		
		foreach( $products as $product )
		{
			$product['date_insert'] = date( "Y-m-d H:i:s" );
			$product['date_modified'] = date( "Y-m-d H:i:s" );
			$product['order_id'] = $order_id;		
			
			if ( isset($product['id']) )
				unset($product['id']);	
			
			$product['tax'] = !isset($product['tax']) ? 0:$product['tax'];	
			$product['price'] = !isset($product['price']) ? 0:$product['price'];	
			$product['quantity'] = !isset($product['quantity']) ? 0:$product['quantity'];	
			$product['product_day'] = !isset($product['product_day']) ? 0:$product['product_day'];				
			
			if ( empty($product['old_price']) )
				$product['old_price'] = $product['price'];
		
			$format = $this->get_data_format_order_product( $product );
			$insert = $wpdb->insert( USAM_TABLE_PRODUCTS_ORDER, $product, $format );
			if ( $insert ) 
			{					
				$insert_id = $wpdb->insert_id;
				$product['id'] = $insert_id;			
			
				if ( isset($products_discounts[$product['product_id']]) )
				{ // Скидка на товар
					$format = array( 'discount_id' => '%d', 'order_id' => '%d', 'product_id' => '%d', 'name' => '%s','priority' => '%d','discount' => '%f','dtype' => '%s','conditions' => '%s');			
					foreach( $products_discounts[$product['product_id']] as $product_discount )
					{
						$insert = array( 'discount_id' => $product_discount->discount_id, 'order_id' => $order_id, 'product_id' => $product['product_id'] );
						$insert_format = array();						
						foreach ( $discounts_rule[$product_discount->discount_id] as $key => $value )
						{	
							if ( isset($format[$key]) )
							{
								$insert[$key] = $value;
								$insert_format[] = $format[$key];
							}
							else
								unset($insert[$key]);
						}			
						$insert['conditions'] = serialize($insert['conditions']);	
						$result = $wpdb->insert( USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER, $insert, $insert_format );
					}
				}
				if ( $shipped_document_id > 0 )
					$shipped_products[] = array( 'basket_id' => $insert_id, 'quantity' => $product['quantity'], 'reserve' => $product['quantity'] );		
				
				$product_files = (array) get_posts( array( 'post_type' => 'usam-product-file', 'post_parent' => $product['product_id'], 'numberposts' => -1, 'post_status' => 'inherit') );			
				if( count($product_files) > 0 ) 			
				{		
					foreach($product_files as $file)
					{ 
						$args = array( 'file_name' => $file->post_title, 'fileid' => $file->ID, 'order_id' => $order_id, 'basket_id' => $insert_id,  'downloads' => $download*$product['quantity'] );							
						usam_insert_product_download_status( $args );
					}		
				}				
				$description = sprintf( __('Добавлен товар %s в количестве %s.'), $product['name'], $product['quantity'] );	
				$this->order_change_history( 'add_product', $description );									
			}
		}
		wp_cache_delete( $this->get( 'id' ), 'usam_products_order' );
		
		$this->recalculated = true;				
		$this->save();		
		
		if ( $shipped_document_id > 0 )
		{
			$shipped = new USAM_Shipped_Document( $shipped_document_id );
			$shipped->set_products( $shipped_products );
		}
		return $insert;
	}
	
	// Получить данные купленного товара
	public function get_order_product( $product_id ) 
	{
		$products = $this->get_order_products();			
		foreach( $products as $product )
		{
			if ( $product->product_id == $product_id )
				return $product;			
		}	
		return array();
	}
		
	// Удалить товар из заказа
	public function delete_order_product( $basket_id ) 
	{		
		global $wpdb;
		
		$product_name = usam_get_cart_product( $basket_id, 'name' );	
		$product_id = usam_get_cart_product( $basket_id, 'product_id' );	
		$order_id = $this->get('id');				
		
		$result_deleted = $wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_SHIPPED_PRODUCTS."` WHERE `basket_id` = %d", $basket_id ) );
		$result_deleted = $wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_DOWNLOAD_STATUS."` WHERE `basket_id` = %d", $basket_id ) );
		$result_deleted = $wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER."` WHERE `order_id` = %d && `product_id` = %d", $order_id, $product_id ) );
		$result_deleted = $wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_PRODUCTS_ORDER."` WHERE `id` = %d", $basket_id ) );		
		
		self::delete_cache( $this->order_id );	

		$this->recalculated	= true;	
		$this->save();				
		
		$description = sprintf( __('Удален товар %s из заказа.'), $product_name );	
		$this->order_change_history( 'delete_product', $description );
		
		
		return $result_deleted;
	}		
	
	private function order_change_history( $operation, $description = '' ) 
	{ 
		if ( !$this->is_creature_order() )
		{ 
			global $wpdb, $user_ID;
			$record = array( 'order_id' => $this->order_id,	
							 'date' => date( "Y-m-d H:i:s" ),		
							 'user_id' => $user_ID,		
							 'operation' => $operation,		
							 'description' => $description,			
			);
			$insert = $wpdb->insert( USAM_TABLE_ORDER_CHANGE_HISTORY, $record, array( '%d', '%s', '%d', '%s', '%s' ) );
			return $wpdb->insert_id;				
		} 
		return false;		
	}
	
	public function get_order_change_history(  ) 
	{
		global $wpdb;
		
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_ORDER_CHANGE_HISTORY." WHERE order_id = %d", $this->order_id );
		$history = $wpdb->get_results( $sql );
		
		return $history;	
	}
		
	public function set_order_feedback_history( $manager_id, $message, $source = 'email' ) 
	{		
		global $wpdb;
		$record = array( 'order_id' => $this->order_id,								 
						 'manager_id' => $manager_id,		// Если ноль, то ответ прислал клиент
						 'source' => $source,		
						 'message' => $message,		
						'date_insert' => date( "Y-m-d H:i:s" ),									 
		);
		$insert = $wpdb->insert( USAM_TABLE_ORDER_FEEDBACK, $record, array( '%d', '%d', '%s', '%s', '%s' ) );
		return $wpdb->insert_id;
	}
	
	// Получить историю общений с клиентом об этом заказе
	public function get_order_feedback_history(  ) 
	{
		global $wpdb;
		
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_ORDER_FEEDBACK." WHERE order_id = %d", $this->order_id );
		$history = $wpdb->get_results( $sql );
		
		return $history;	
	}
	
	public function delete_order_feedback_history(  ) 
	{
		global $wpdb;
		
		return $wpdb->query( $wpdb->prepare( "DELETE FROM " . USAM_TABLE_ORDER_FEEDBACK . " WHERE order_id='%s'", $this->order_id ) );
	}
	
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_ORDERS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{					
			$this->exists = true;
			$this->data = apply_filters( 'usam_order_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}	
		do_action( 'usam_order_fetched', $this );	
		$this->fetched = true;			
	}	

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_order_get_property', $value, $key, $this );
	}

	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{ 
		if ( empty($this->data) )
			$this->fetch();
		
		return apply_filters( 'usam_order_get_data', $this->data, $this );
	}
	
	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_order_set_properties', $properties, $this );	
		if ( array_key_exists( 'paid', $properties ) ) 
		{	
			$order_paid = $this->get( 'paid' );
			if ( $properties['paid'] != $order_paid && $properties['paid'] == 2 )
				$this->is_order_paid = true;			
		}	
		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}
		// Проверим нужно ли пересчитать заказ
		$keys = array('bonus', 'coupon_discount', 'total_tax', 'shipping' );	
		foreach ( $keys as $key ) 
		{
			$previous = $this->get( $key );
			if ( isset($properties[$key]) && $properties[$key] != $previous )
			{				
				$this->recalculated = true;			
				break;
			}
		}			
		if ( ! is_array($this->data) )
			$this->data = array();		
		$this->data = array_merge( $this->data, $properties );				
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb;
		do_action( 'usam_order_pre_save', $this );	
		$where_col = $this->args['col'];
		$result = false;
		$this->data['date_modified'] = date( "Y-m-d H:i:s" );	
		
		if ( isset($this->data['user_ID']) )
			$this->data['user_ID'] = (int)$this->data['user_ID'];
		
		if ( isset($this->data['type_payer']) )
			$this->data['type_payer'] = (int)$this->data['type_payer'];
		
		if ( isset($this->data['notes']) )
		{
			$str = wp_unslash($this->data['notes']);			
			$this->data['notes'] = stripcslashes($str);		
		}
		if ( isset($this->data['cancellation_reason']) )
		{
			$str = wp_unslash($this->data['cancellation_reason']);			
			$this->data['cancellation_reason'] = stripcslashes($str);			
		}				
		if ( $this->is_closed_order() && !$this->is_transaction_completed() )
			$this->data['status'] = $this->previous_status;
	
		if ( empty($this->data['status']) )
			$this->data['status'] = 'incomplete_sale';
		
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_order_pre_update', $this );				
			$this->calculate_totalprice();	

			$this->data = apply_filters( 'usam_order_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );
			
			$data = $this->data;		
			
			$str = array();
			foreach ( $format as $key => $value ) 
			{
				if ( $data[$key] == null )
				{
					$str[] = "`$key` = NULL";
					unset($data[$key]);
				}
				else
					$str[] = "`$key` = '$value'";	
			}			
			$sql = "UPDATE `".USAM_TABLE_ORDERS."` SET ".implode( ', ', $str )." WHERE $where_col = '$where_format' ";
			$result = $wpdb->query( $wpdb->prepare( $sql, array_merge( array_values( $data ), array( $where_val ) ) ) );
			if ($result >= 1 )
			{					
				self::update_cache( $this );				
				if ( $this->recalculated )
					$this->mark_order_as_paid( );
				
				if ( $this->is_order_paid ) 
				{
					do_action( 'usam_order_paid', $this );
				}			
				if ( $this->is_status_changed ) 
				{				
					$current_status = $this->get( 'status' );
					if ( self::CREATURE != $this->previous_status )
					{
						$description = sprintf( __('Статус заказа был изменен на %s.'), usam_get_order_status_name( $current_status ) );	
						$this->order_change_history( 'status_changed', $description );	
					}					
					if ( $this->is_transaction_completed() )
						$this->update_downloadable_status();
					
					$previous_status = $this->previous_status;
					$this->previous_status = $current_status;
					$this->is_status_changed = false;	
					
					if ( usam_check_order_is_completed( $current_status ) )
					{
						if ( $this->is_closed_order() )					
							$document_update['status'] = 'shipped';	
						else
							$document_update['status'] = 'сanceled';	
						
						$documents = $this->get_shipped_documents();	
						foreach( $documents as $document  )
						{	
							$this->edit_document_shipped( $document->id, $document_update );
						}
						$this->delete_order_feedback_history();
					}
					do_action( 'usam_update_order_status', $this->order_id, $current_status, $previous_status, $this );
				}			
				do_action( 'usam_order_update', $this );
			}
		} 
		else 
		{   
			do_action( 'usam_order_pre_insert' );
			
			$this->data['sessionid'] = (mt_rand( 100, 999 ) . time());
			
			$default = array( 'totalprice' => 0, 'type_price' => '', 'paid' => 0, 'status' => '', 'type_payer' => 0, 'user_ID' => 0, 'manager_id' => 0, 'modified_customer' => '', 'coupon_name' => '', 'coupon_discount' => 0, 'discount' => 0, 'bonus' => 0, 'notes' => '','cancellation_reason' => '', 'shipping' => 0, 'total_tax' => 0, 'taxes_rate' => 0, 'order_type' => '' );
			
			if ( isset($this->data['id']) )
				unset($this->data['id']);			
						
			$this->data = array_merge($default, $this->data);		
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );
			$this->data = apply_filters( 'usam_order_insert_data', $this->data );
			$format = $this->get_data_format( );	
			$this->data_format( );		
		
			$result = $wpdb->insert( USAM_TABLE_ORDERS, $this->data, $format );	
			if ( $result ) 
			{ 
				$this->set( 'id', $wpdb->insert_id );
				$this->order_id = $wpdb->insert_id;
// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col' => 'id',  'value' => $this->order_id, );	
				$this->exists = true;	
				
				do_action( 'usam_order_insert', $this );
			}			
		} 	
		do_action( 'usam_order_save', $this );
		return $result;
	}
	
	// Пересчитать общую сумму заказа, когда добавляют или удаляют товар в заказе
	private function calculate_totalprice( ) 
	{	
		if ( $this->recalculated )
		{ 				
			$order_products = $this->get_order_products( );		
			$totalprice = 0;	
			foreach ( $order_products as $product ) 
			{
				$totalprice += $product->quantity * ($product->price + $product->tax);	
			}			
			$id = $this->get( 'id' );	
			$this->data['totalprice'] = $totalprice - $this->data['bonus'] - $this->data['coupon_discount'] + $this->data['shipping'];				
		}		
	}
	
	public function get_calculated_data( )
	{
		if ( ! $this->exists() )
			return array();
		
		$products = $this->get_order_products();

		$order_basket = 0;		
		$order_final_basket = 0;		
		$data['weight'] = 0;	
		$data['volume'] = 0;
		$data['width'] = 0;
		$data['height'] = 0;
		$data['length'] = 0;
		$min = 0;
		
		$length = array();
		foreach ( $products as $item ) 
		{			
			$order_basket += $item->old_price * $item->quantity;		
			$order_final_basket += $item->price * $item->quantity;				
			$weight = (float)usam_get_product_meta( $item->product_id, 'weight' );
			
			$data['weight'] += $weight * $item->quantity;
			$data['volume'] += usam_get_product_volume( $item->product_id ) * $item->quantity;
			
			$product_meta = maybe_unserialize( usam_get_product_meta($item->product_id , 'product_metadata' ) );	
		
			if ( isset($product_meta['dimensions']) && isset($product_meta['dimensions']['width']) )
			{
				if ( $data['width'] == 0 )
				{
					$data['width'] = $product_meta['dimensions']['width'];
					$data['height'] = $product_meta['dimensions']['height'];
					$data['length'] = $product_meta['dimensions']['length'];
				}
				else
				{
					switch ( $min ) 
					{
						case $data['width']:
							$data['width'] += $product_meta['dimensions']['width'];				
						break;
						case $data['height']:
							$data['height'] += $product_meta['dimensions']['height'];													
						break;												
						case $data['length']:
							$data['length'] += $product_meta['dimensions']['length'];												
						break;					
					}					
				}
				$min = min(array($data['width'], $data['height'], $data['length']));
				$length[] = $product_meta['dimensions']['length'];
			}
		}		
		$data['order_basket'] = $order_basket;		
		$data['order_final_basket'] = $order_final_basket;	
		$data['order_basket_discount'] = $order_basket - $order_final_basket;	
		return apply_filters( 'usam_order_calculated_data', $data, $this );	
	}


	private function update_downloadable_status()
	{
		global $wpdb;
		$wpdb->update(	USAM_TABLE_DOWNLOAD_STATUS,	array( 'active' => '1' ), array( 'order_id' => $this->get( 'id' ) ) );				
	}		
	
	public function is_creature_order() {
		return $this->get( 'status' ) == self::CREATURE;
	}	
	
	public function is_transaction_completed() {
		return $this->get( 'paid' ) == 2;
	}
	
	public function is_closed_order() {
		return $this->get( 'status' ) == self::CLOSED;
	}
}

// Получить данные заказа
function usam_get_order( $value, $col = 'id' )  
{
   $order = new USAM_Order( $value, $col );	
   return $order->get_data();
}

function usam_update_order( $id, $data, $by = 'id' )
{
	$order = new USAM_Order( $id, $by );
	$order->set( $data );
	return $order->save();
}

function usam_have_purchaselog_details() 
{
   global $purchlogitem;
   return $purchlogitem->have_purch_item();
}


function usam_the_purchaselog_item() 
{
   global $purchlogitem;
   return $purchlogitem->the_purch_item();
}

//Статус оплаты заказа
function usam_get_order_payment_status_name( $paid ) 
{
	if ( $paid == 2 )
		$payment_status = __( 'Оплачен', 'usam' );
	elseif ( $paid == 1 )
		$payment_status = __( 'Частично оплачен', 'usam' );
	else
		$payment_status = __( 'Не оплачен', 'usam' );
	
	return $payment_status;
}