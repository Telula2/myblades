<?php
/*
*	В файле содержатся вспомогательные функции для "Журнала продаж"
*/


// Копировать заказ
function usam_order_copy( $order_id ) 
{
	add_filter( 'usam_prevent_notification_change_order_status', create_function('', 'return false;'));		
	
	$order = new USAM_Order( $order_id );
	$data = $order->get_data();
	
	$order_products = $order->get_order_products();
	$shipped_documents = $order->get_shipped_documents();			
	
	$customer_data = $order->get_customer_data( );			
	
	$data['status'] = '';
	$data['order_type'] = 'manager';			
	$order_new = new USAM_Order( $data );
	$order_new->save();	

	$new_products = array();
	foreach ( $order_products as $product ) 
	{				
		$new_products[] = (array)$product;
	}
	$order_new->add_order_products( $new_products );
	
	$products = $order_new->get_order_products( );
	foreach( $products as $product )
	{			
		$document_shipped_products[$product->product_id] = array( 'basket_id' => $product->id, 'quantity' => $product->quantity, 'reserve' => $product->quantity );
	}		
	foreach ( $shipped_documents as $document ) 
	{
		$products_shipped = $order->get_products_shipping_document( $document->id );			
		$new_document = (array)$document;
		foreach ( $products_shipped as $product ) 
		{
			$new_document['products'][] = array( 'basket_id' => $document_shipped_products[$product['product_id']]['basket_id'], 'quantity' => $product['quantity'], 'reserve' => $product['quantity'] );
		}	
		$order_new->add_document_shipped( $new_document );
	}				
	$new_customer_data = array();
	foreach ( $customer_data as $unique_name => $data ) 
	{
		$new_customer_data[$unique_name] = $data['value'];
	}			
	$order_new->save_customer_data( $new_customer_data );
	
	$order_new->set( array( 'status' => 'job_dispatched' ) );	
	$order_new->save();	
	
	return  $order_new->get('id');
}


// Пересчитать доставку заказа
function usam_calculate_amount_shipped_document( $order_id, $shipped_document_id ) 
{
	$order = new USAM_Order( $order_id );	
	$order_data = $order->get_data();
	$products = usam_get_products_shipped_documents( $shipped_document_id );	
	$shipped_document = usam_get_shipped_document( $shipped_document_id );	
	$customer_data = $order->get_customer_data();		
	
	$parameters['any_balance'] = true;

	$properties = array( 'bonus' => $order_data['bonus'], 'type_price' => $order_data['type_price'], 'coupon_name' => $order_data['coupon_name'] );	
	if ( isset($customer_data['shippinglocation']) )
		$properties['location'] = $customer_data['shippinglocation']['value'];	
	
	$cart = new USAM_CART( $order_id, $properties );	
	foreach ( $products as $product ) 
	{
		$parameters['quantity'] = $product->quantity;	
		$cart->add_product_basket( $product->product_id, $parameters );			
	}	
	$cart->set_shipping_methods();
	
	$properties = array( 'selected_shipping' => $shipped_document['method'], 'storage_pickup' => $shipped_document['storage_pickup'] );		
	$cart->set_properties( $properties );

	$totalprice = $cart->calculate_total_price();	

	$document['price'] = $cart->get_properties( 'shipping' );	
	$order->edit_document_shipped( $shipped_document_id, $document );
}

// Добавить товар в заказ
function usam_add_product_to_order( $order_id, $product_id, $quantity, $cart_parameters = array() ) 
{	
	// настройки по умолчанию
	$default_parameters['document_shipped'] = 0;
			
	$parameters = array_merge( $default_parameters, (array)$cart_parameters );
	$storage = usam_get_product_meta( $product_id, 'storage' );
	$return = array ( 'error' => '', 'result' => false );		
	if ( $storage < $quantity )
	{
		$return['error'] = sprintf( _n( 'Извините, но есть только %s единицы этого товара.', 'Извините, но есть только %s единиц этого товара.', $storage, 'usam' ), $storage );
	}
	else
	{	
		$purchase_log = new USAM_Order( $order_id );		
		$product = $purchase_log->get_order_product( $product_id );
		if ( !empty($product) )
		{
			$new_quantity = $product->quantity + $quantity;
			$update[$product->id]['quantity'] = $new_quantity;
			$purchase_log->edit_order_products( $update );
			$return['result'] = true;			
			if ( !empty($parameters['documents_payment']) ) 
			{							
				$payment_document = usam_get_payment_document( $parameters['documents_payment'] );				
				$payment_update['id'] = $parameters['documents_payment'];
				$payment_update['sum'] = $payment_document['sum'] + $product->quantity*$product->price;
				$purchase_log->edit_payment_history( $payment_update );	
			}
			if ( !empty($parameters['document_shipped']) )
			{
				$product_shipped[] = array( 'basket_id' => $product->id, 'quantity' => $new_quantity, 'reserve' => $new_quantity );	
				$shipped = new USAM_Shipped_Document( $parameters['document_shipped'] );
				$shipped->set_products( $product_shipped );			
			}
		}
		else
		{
			$data = $purchase_log->get_data();	
			$name = get_post_field( 'post_title', $product_id );
			$price = usam_get_product_price( $product_id, $data['type_price'] );
			$old_price = usam_get_product_old_price( $product_id, $data['type_price'] );		
			
			$product_meta = usam_get_product_meta( $product_id, 'product_metadata' );		
			$bonus = 0;
			if ( !empty($product_meta['bonuses']['value']) )
			{
				if ( $product_meta['bonuses']['type'] == 'p' )
					$bonus = usam_round_price($price + $price * $product_meta['bonuses']['value'] / 100, $data['type_price'] );
				else
					$bonus = $product_meta['bonuses']['value'];
			}		
			$products = array( array(
					'product_id'         => $product_id,
					'name'           => $name,
					'order_id'       => $order_id,
					'price'          => $price,
					'old_price'      => $old_price,				
					'bonus'          => $bonus,				
					'quantity'       => $quantity,					
					) );		
			$insert = $purchase_log->add_order_products( $products, $parameters['document_shipped'] );
			if ( $insert ) 
			{							
				if ( !empty($parameters['documents_payment']) ) 
				{			
					$sum = $price*$quantity;					
					$payment_document = usam_get_payment_document( $parameters['documents_payment'] );				
					$payment_update['id'] = $parameters['documents_payment'];
					$payment_update['sum'] = $payment_document['sum'] + $sum;
					$purchase_log->edit_payment_history( $payment_update );	
				}							
				$return['result'] = true;
			}
			else
				$return['error'] = __('Проблемы с добавлением товара, обратитесь к разработчикам!!!','usam');
		}
	}	
	return $return;
}

// Заказ в корзину
function usam_load_order_in_basket( $order_id ) 
{
	$order = new USAM_Order( $order_id );	
	$order_data = $order->get_data();
	$products = usam_get_products_shipped_documents( $shipped_document_id );	
	$shipped_document = usam_get_shipped_document( $shipped_document_id );	
	$customer_data = $order->get_customer_data();		
	
	$parameters['any_balance'] = true;

	$properties = array( 'bonus' => $order_data['bonus'], 'type_price' => $order_data['type_price'], 'selected_shipping' => $shipped_document['method'], 'storage_pickup' => $shipped_document['storage_pickup'], 'coupon_name' => $order_data['coupon_name'] );	
	if ( isset($customer_data['shippinglocation']) )
		$properties['location'] = $customer_data['shippinglocation']['value'];	
	
	$cart = new USAM_CART( $order_id, $properties );		
	foreach ( $products as $product ) 
	{
		$parameters['quantity'] = $product->quantity;	
		$cart->add_product_basket( $product->product_id, $parameters );			
	}	
	return $cart;
}


function usam_get_plaintext_table( $headings, $rows ) 
{
	$colwidths = array();
	$output = array();
	$alignment = array_values( $headings );
	$headings = array_keys( $headings );
	
	foreach ( $headings as $heading ) 	
		$colwidths[] = strlen( $heading );
	
	foreach ( $rows as $row ) 
	{
		$i = 0;
		foreach ( $row as $col ) 
		{
			$colwidths[$i] = max( strlen( $col ), $colwidths[$i] );
			$i ++;
		}
	}		
	foreach ( $rows as &$row )
	{
		$i = 0;
		foreach ( $row as &$col ) {
			$align = ( $alignment[$i] == 'left' ) ? STR_PAD_RIGHT : STR_PAD_LEFT;
			$col = str_pad( $col, $colwidths[$i], ' ', $align );
			$i ++;
		}
		$output[] = implode( '  ', $row );
	}
	$line = array();
	$i = 0;
	foreach ( $colwidths as $width ) 
	{
		$line[] = str_repeat( '-', $width );
		$headings[$i] = str_pad( $headings[$i], $width );
		$i ++;
	}
	$line = implode( '--', $line );
	array_unshift( $output, $line );
	if ( ! empty( $headings ) ) 
	{
		array_unshift( $output, implode( '  ', $headings ) );
		array_unshift( $output, $line );
	}	
	$output[] = $line;
	return implode( "\r\n", $output ) . "\r\n";
}


//Строит таблицу купленных товаров. Отображается на странице Результаты транзакции и в письме клиенту
function usam_get_purchase_log_html_table( $headings, $rows )
{
	ob_start();
	?>	
	<div class ="usam_transaction_results_table_wrapper">
	<table class="usam_transaction_results_table table_message">
		<?php if ( ! empty( $headings ) ): ?>
			<thead class="transaction_results_thead">
				<th class = "table_message_th"></th>
				<?php foreach ( $headings as $heading => $align ): ?>
					<th class="table_message_th"><?php echo esc_html( $heading ); ?></th>
				<?php endforeach; ?>
			</thead>
		<?php endif; ?>
		<tbody>		
			<?php 
			$j = 1;	
			foreach ( $rows as $row ): 				
				?>				
				<tr class="transaction_results_row">
					<td class="transaction_results_cell colum_number"><?php echo esc_html( $j ); ?></td>
					<?php foreach ( $row as $key => $col ):	?>
						<td class="transaction_results_cell colum_<?php echo esc_html( $key ); ?>"><?php echo esc_html( $col ); ?></td>
					<?php endforeach; ?>
				</tr>
			<?php 
			$j++;
			endforeach; ?>
		</tbody>
	</table>	
	</div>	
	<?php	
	$output = ob_get_clean();
	$output = apply_filters( 'usam_get_purchase_log_html_table', $output, $headings, $rows );	
	return $output;
}

function get_order_operation_message( $operation ) 
{
	$default_operation = array( 
		'add_product'    => __('Добавление товар','usam'), 
		'edit_product'   => __('Изменен товар','usam'), 
		'delete_product' => __('Удаление товар','usam'),
		'status_changed' => __('Изменение статуса','usam'), 
		'insert_customer_data' => __('Добавлены данные клиента','usam'),
		'update_customer_data' => __('Обновлены данные клиента','usam'),
		'edit_document_shipped' => __('Изменен документ отгрузки','usam'),
		'add_document_shipped' => __('Добавлен документ отгрузки','usam'),
		
	);
	if ( isset($default_operation[$operation]) )
		return $default_operation[$operation];
	else
		return '';
}

// Удалить заказ
function usam_delete_order( $orders ) 
{	
	global $wpdb;
	if ( !is_array($orders) )	
		$orders = array( $orders );	
			
	$in = implode( ', ', $orders );			
	
	$wpdb->query( "DELETE FROM " . USAM_TABLE_PRODUCTS_ORDER . " WHERE order_id IN ($in)" );			
	$wpdb->query( "DELETE FROM " . USAM_TABLE_DOWNLOAD_STATUS . " WHERE order_id IN ($in)" );		
	$wpdb->query( "DELETE FROM " . USAM_TABLE_ORDER_PROPS_VALUE . " WHERE order_id IN ($in)" );
	$wpdb->query( "DELETE FROM " . USAM_TABLE_PAYMENT_HISTORY." WHERE document_id IN ($in) AND document_type='order'" );	
	foreach ( $orders as $order_id ) 
	{		
		$shippeds_document = usam_get_shipping_documents( array( 'order_id' => $order_id, 'fields' => 'id' ) );	
		foreach ( $shippeds_document as $document_id ) 
		{
			usam_delete_shipped_document( $document_id );			
		}
	}		
	$wpdb->query( "DELETE FROM " . USAM_TABLE_ORDER_CHANGE_HISTORY . " WHERE order_id IN ($in)" );
	$wpdb->query( "DELETE FROM " . USAM_TABLE_ORDER_FEEDBACK . " WHERE order_id IN ($in)" );
	$wpdb->query( "DELETE FROM " . USAM_TABLE_CUSTOMER_BONUSES." WHERE order_id IN ($in)" );
	$wpdb->query( "DELETE FROM " . USAM_TABLE_ORDER_DISCOUNT." WHERE order_id IN ($in)" );
	$wpdb->query( "DELETE FROM " . USAM_TABLE_ORDERS . " WHERE id IN ($in)" );		
	
	return true;
}



// Статусы документов отгрузки
function usam_get_shipped_document_status( ) 
{												
	$order_status = array(
		array(
			'code'       => 'pending',
			'work'       => true,
			'name'       => __( 'Ожидает обработки', 'usam' ),	
		),
		array(
			'code'       => 'packaging',
			'work'       => true,
			'name'       => __( 'Комплектация заказа', 'usam' ),
		),
		array(
			'code'       => 'arrival',
			'work'       => true,
			'name'       => __( 'Ожидаем приход товара', 'usam' ),
		),
		array(
			'code'       => 'expect_tc',
			'work'       => true,
			'name'       => __( 'Ожидание транспортную компанию', 'usam' ),
		),
		array(
			'code'       => 'collected',
			'work'       => false,
			'name'       => __( 'Ожидает вручения', 'usam' ),
		),
		array(
			'code'       => 'referred',
			'work'       => false,
			'name'       => __( 'Передан в службу доставки', 'usam' ),
		),
		array(
			'code'       => 'shipped',
			'work'       => false,
			'name'       => __( 'Отгружен', 'usam' ),		
		),
		array(
			'code'       => 'сanceled',
			'work'       => false,
			'name'       => __( 'Отменен', 'usam' ),		
		),
	);	
	return $order_status;			
}

function usam_check_shipped_document_is_work( $code ) 
{
	$status = usam_get_shipped_document_status( $code );	
	return $status['work'];
}

function usam_get_shipped_document_status_name( $status_code ) 
{
	$statuses = usam_get_shipped_document_status();
	$result = '';
	foreach( $statuses as $key => $status )
	{
		if ( $status['code'] == $status_code )
			$result = $status['name'];
	}
	return $result;
}

//Получить ссылку на заказ
function usam_get_url_order( $id ) 
{
	$url = add_query_arg( array('page' => 'orders', 'action' => 'view', 'id' => $id), admin_url('admin.php') );
	return $url;
}

//Получить ссылку на заказ
function usam_get_link_order( $id ) 
{
	return '<a class="order-link" href="'.esc_url( usam_get_url_order( $id ) ).'" target="_blank" title="'.esc_attr__( 'Посмотреть детали заказа', 'usam' ).'">'.$id.'</a>';
}

function usam_get_order_type( $type ) 
{
	$order_type = array( 'order' => __('Обычный заказ','usam'), 'fast_order' => __('Быстрый заказ','usam'), 'manager' => __('Создан менеджером','usam') );
	if ( isset($order_type[$type]) )
		$return = $order_type[$type];
	else
		$return = __('Тип неизвестен');
	return $return;
}


// Добавить скидку заказа
function usam_set_order_discount( $data ) 
{
	global $wpdb;
	
	if ( empty($data['discount_id']) )
		return false;		
	
	$format = array( 'discount_id' => '%d', 'order_id' => '%d','name' => '%s','priority' => '%d','type' => '%s','value' => '%f','value_type' => '%s','condition' => '%s');
	
	$insert = array();
	$insert_format = array();
	foreach ( $data as $key => $value )
	{	
		if ( isset($format[$key]) )
		{
			$insert[$key] = $value;
			$insert_format[] = $format[$key];
		}
	}
	if ( empty($insert) )
		return;
	
	$insert['condition'] = serialize($insert['condition']);	
	
	$result = $wpdb->insert( USAM_TABLE_ORDER_DISCOUNT, $insert, $insert_format );	
	return $wpdb->insert_id;
}

// Добавить скидку корзины
function usam_set_cart_discount( $data, $cart_id ) 
{	
	global $wpdb;
	
	$discount_id = usam_set_order_discount( $data );
	$result = false;
	if ( $discount_id )
	{				
		$insert = array( 'cart_id' => $cart_id, 'discount_order_id' => $discount_id);	
		$insert_format = array( 'cart_id' => '%d', 'discount_order_id' => '%d');		
		$wpdb->insert( USAM_TABLE_DISCOUNT_BASKET, $insert, $insert_format );	
		$result = $discount_id;
		
		$cache_key = 'cart_discounts_ids';
		if( ! $discont_ids = wp_cache_get($cache_key, $cart_id ) )
		{		
			$discont_ids = array();
		}
		$discont_ids[] = $discount_id;		
		wp_cache_set( 'cart_discounts_ids', $discont_ids, $cart_id );
	}
	return $result;
}

//Изменить скидку заказа или корзины
function usam_update_order_discount( $data, $colum = 'id' ) 
{
	global $wpdb;

	if ( !isset($data[$colum]) || !in_array($colum, array('order_id', 'id' )) || empty($data['discount_id']) )
		return false;	
		
	$format = array( 'id' => '%d', 'discount_id' => '%d', 'order_id' => '%d','name' => '%s','priority' => '%d','type' => '%s','value' => '%f','value_type' => '%s','condition' => '%s');
	
	$where_format = array( $colum => $format[$colum] );	
	$where = array( $colum => $data[$colum] );
	
	unset($data[$colum]);
	
	$update = array();
	$insert_format = array();
	foreach ( $data as $key => $value )
	{	
		if ( isset($format[$key]) )
		{
			$insert[$key] = $value;
			$insert_format[] = $format[$key];
		}
	}	
	if ( empty($insert) )
		return;
	
	if ( isset($insert['condition']) )
		$insert['condition'] = serialize($insert['condition']);			

	$result = $wpdb->update( USAM_TABLE_ORDER_DISCOUNT, $insert, $where, $insert_format, $where_format );		
	return $result;
}


// Получить список скидок заказа
function usam_get_order_discounts( $args ) 
{
	global $wpdb;
	
	if ( isset($args['fields']) )
	{
		$fields = $args['fields'] == 'all'?'*':implode( ",", $args['fields'] );
	}
	else
		$fields = '*';
/*=========================*/	
	$_where = array('1=1');
	if ( isset($args['order_id']) )
		$_where[] = "order_id = '".$args['order_id']."'";
	
	if ( isset($args['ids']) )
		$_where[] = "id IN( '".implode( "','", $args['ids'] )."' )";
	
	$where = implode( " AND ", $_where);
/*=========================*/		
	if ( isset($args['orderby']) )
	{
		$orderby = $args['orderby'];
	}
	else
		$orderby = 'id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($args['order']) )	
		$order = $args['order'];	
	else
		$order = 'DESC';	
	if ( isset($args['output_type']) )	
		$output_type = $args['output_type'];	
	else
		$output_type = 'OBJECT';
	
	$result = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_ORDER_DISCOUNT." WHERE $where $orderby $order", $output_type );	
	return $result;
}

// Получить список скидок на товары заказа
function usam_get_disconts_products_order( $args ) 
{
	global $wpdb;
	
	if ( isset($args['fields']) )
	{
		$fields = $args['fields'] == 'all'?'*':implode( ",", $args['fields'] );
	}
	else
		$fields = '*';
/*=========================*/	
	$_where = array('1=1');
	if ( isset($args['order_id']) )
		$_where[] = "order_id = '".$args['order_id']."'";
	
	if ( isset($args['ids']) )
		$_where[] = "id IN( '".implode( "','", $args['ids'] )."' )";
	
	$where = implode( " AND ", $_where);
/*=========================*/		
	if ( isset($args['orderby']) )
	{
		$orderby = $args['orderby'];
	}
	else
		$orderby = 'id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($args['order']) )	
		$order = $args['order'];	
	else
		$order = 'DESC';	
	if ( isset($args['output_type']) )	
		$output_type = $args['output_type'];	
	else
		$output_type = 'OBJECT';
	
	$result = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER." WHERE $where $orderby $order", $output_type );	
	return $result;
}

// Получить id скидок корзины
function usam_get_cart_discounts_ids( $cart_id ) 
{
	global $wpdb;
	
	$cache_key = 'cart_discounts_ids';
	$discont_ids = wp_cache_get($cache_key, $cart_id );
	if( $discont_ids === false )
	{		
		$discont_ids = $wpdb->get_col( "SELECT discount_order_id FROM ".USAM_TABLE_DISCOUNT_BASKET." WHERE cart_id = '$cart_id'" );		
		wp_cache_set( $cache_key, $discont_ids, $cart_id );
	}	
	return $discont_ids;
}

// Получить список скидок корзины
function usam_get_cart_discounts( $cart_id ) 
{
	$discont_ids = usam_get_cart_discounts_ids( $cart_id );	
	$result = array();
	if ( !empty($discont_ids) )
		$result = usam_get_order_discounts( array( 'ids' => $discont_ids, 'order_id' => 0 ) );	
	return $result;
}

// Получить данные скидки заказа
function usam_get_order_discount( $discount_id, $order_id ) 
{
	global $wpdb;
		
	$result = $wpdb->get_var( "SELECT * FROM ".USAM_TABLE_ORDER_DISCOUNT." WHERE discount_id = '$discount_id' AND order_id = '$order_id'" );	
	return $result;
}

// Получить данные скидки заказа
function usam_get_cart_discount( $discount_id, $cart_id ) 
{
	global $wpdb;
	$discont_ids = usam_get_cart_discounts_ids( $cart_id );	
	if ( empty($discont_ids) )
		return false;
	
	$result = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_ORDER_DISCOUNT." WHERE discount_id = '$discount_id' AND id IN ('".implode( "','", $discont_ids )."')" );
	return $result;
}

// Удалить акции корзины
function usam_delete_cart_discount( $cart_id ) 
{	
	$discont_ids = usam_get_cart_discounts_ids( $cart_id );	
	usam_delete_order_discount( $discont_ids );
	
	$result = usam_unlink_cart_discount( $cart_id );
	return $result;
}

// Удалить акции заказа
function usam_delete_order_discount( $ids, $colum = 'id' ) 
{
	global $wpdb;
	
	if ( empty($ids) || !in_array($colum, array('order_id', 'id' )) )
		return false;
	
	if ( is_numeric($ids) )
		$ids = array($ids);
	
	$ids = array_map( 'intval', $ids );
	$in = implode( ', ', $ids );
				
	$result = $wpdb->query("DELETE FROM ".USAM_TABLE_ORDER_DISCOUNT." WHERE $colum IN ($in)");
	return $result;
}

function usam_set_invoice_to_pdf( $id ) 
{		
	$file_path = USAM_UPLOAD_DIR.'order_invoices/invoice-'.$id.'.pdf';	
	if ( file_exists( $file_path ) )	
		unlink( $file_path );
	$html = usam_get_printing_forms( 'payment_invoice', $id );	
	
	$file = usam_export_to_pdf( $html );
		
	file_put_contents($file_path, $file ); 
	return $file_path;
}

function usam_get_order_view_group( $id )
{
	$option = get_option('usam_order_view_grouping');
	$view_grouping = maybe_unserialize( $option );	
	$result = array();
	if ( !empty($view_grouping) ) 
	{
		foreach ( $view_grouping as $value )		
		{
			if ( $value['id'] == $id )
			{
				$result = $value;
				break;
			}
		}
	}
	return $result;
}

function usam_get_user_order_view_group( $id = null )
{
	global $user_ID;
	if ( $id == null )
	{
		$id = $user_ID;
	}
	$view_grouping = get_user_meta($id, 'usam_order_view_grouping', true);	
	$view_group = usam_get_order_view_group( $view_grouping );	
	return $view_group;
}