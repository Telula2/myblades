<?php
/**
 * Документы отгрузки
 */ 
class USAM_Shipped_Document
{
	const CLOSED    = 'shipped';
	// строковые
	private static $string_cols = array(
		'name',		
		'method',		
		'planned_date',
		'date_insert',	
		'date_update',	
		'date_allow_delivery',	
		'readiness_date',	
		'track_id',	
		'doc_number',	
		'doc_data',	
		'status',	
		'notes',	
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'order_id',				
		'courier',			
		'storage_pickup',
		'storage',		
		'readiness',					
		'export',	
		'include_in_cost',
	);
	// рациональные
	private static $float_cols = array(
		'price',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		
	private $is_track_id_changed;
	private $previous_price;
	private $is_price_changed;
	private $include_in_cost_changed = false;
		
	private $is_status_changed = false;
	private $is_order_paid     = false;	
	private $previous_status   = false;	
	
	private $data     = array();		
	private $products = null;		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_document_shipped' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_shipped_document ) 
	{
		$id = $_shipped_document->get( 'id' );	
		wp_cache_set( $id, $_shipped_document->data, 'usam_document_shipped' );		
		do_action( 'usam_document_shipped_update_cache', $_shipped_document );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$shipped_document = new USAM_Shipped_Document( $value, $col );
		wp_cache_delete( $shipped_document->get( 'id' ), 'usam_document_shipped' );	
		do_action( 'usam_document_shipped_delete_cache', $shipped_document, $value, $col );	
	}			

	/**
	 *  Удалить документ отгрузки
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_document_shipped_before_delete', $this );
		
		$id = $this->get( 'id' );
				
		$products = $this->get_products( );			
		
		foreach( $products as $key => &$product )
		{
			$product = (array)$product;		
			$product['quantity'] = 0;		
			$product['reserve'] = 0;				
		}					
		$this->set_products( $products );
		
		$order_id = $this->get( 'order_id' );	
		$price = $this->get( 'price' );
		
		$data = usam_get_order( $order_id );
		$shipping = $data['shipping'] - $price;		
		usam_update_order( $this->data['order_id'], array( 'shipping' => $shipping ) );		
		
		wp_cache_delete( $order_id, 'usam_shipped_documents_order' );	
		self::delete_cache( $id );				
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE id = '$id'");		
		do_action( 'usam_document_shipped_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_document_shipped_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_document_shipped_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_document_shipped_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_document_shipped_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();				
		
		if ( !empty($properties['method']) )
			$properties['name'] = usam_get_delivery_name( $properties['method'] );		
		if ( isset($properties['readiness']) )
			$properties['readiness'] = $properties['readiness'] > 100 ?100:$properties['readiness'];	
		
		if ( array_key_exists( 'track_id', $properties ) ) 
		{				
			if ( $properties['track_id'] != $this->get( 'track_id' ) )
				$this->is_track_id_changed = true;			
		}			
		if ( array_key_exists( 'price', $properties ) ) 
		{	
			$this->previous_price = $this->get( 'price' );
			if ( $properties['price'] != $this->previous_price )
				$this->is_price_changed = true;			
		}
		if ( array_key_exists( 'include_in_cost', $properties ) ) 
		{	
			if ( $properties['include_in_cost'] !=  $this->get( 'include_in_cost' ) )
				$this->include_in_cost_changed = true;			
		}	
		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_document_shipped_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	// Снять все товары с резерва
	public function remove_all_product_from_reserve( ) 
	{					
		$products = $this->get_products( );				
		foreach( $products as $product  )
		{
			$product = (array)$product;
			if ( $product['reserve'] )
			{
				$product['reserve'] = 0;
				$this->add_product( $product );	
			}
		}				
	}
	
	// Получить товары в отгрузке
	public function get_products(  ) 
	{
		global $wpdb;
		
		$document_id = $this->get( 'id' );	
		if ( $this->products === null )
			$this->products = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM `". USAM_TABLE_SHIPPED_PRODUCTS ."` WHERE documents_id = '%d'", $document_id) );
					
		return $this->products;
	}
	
	// Получить товары отгрузки с данными товаров из заказа
	public function get_order_products( ) 
	{
		global $wpdb;
		
		$document_id = $this->get( 'id' );	
		
		$products = $wpdb->get_results( $wpdb->prepare( "SELECT cc.*, sp.quantity AS quantity_shipment, sp.reserve AS reserve FROM `". USAM_TABLE_SHIPPED_PRODUCTS ."` AS sp 
					LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc ON ( cc.id=sp.basket_id )					
					WHERE sp.documents_id = '%d'", $document_id) );
					
		return $products;
	}
	
	private function get_order_product_quantity( $basket_id ) 
	{
		$order = new USAM_Order( $this->data['order_id'] );
		$order_products = $order->get_order_products();
		
		$quantity = false;
		foreach( $order_products as $product )
		{
			if ( $product->id == $basket_id )
			{
				$quantity = $product->quantity;
				break;				
			}
		}		
		return $quantity;
	}
		
	// Добавить товар
	private function add_product( $update_product ) 
	{
		global $wpdb;			
			
		$document_id = $this->get('id');				
		if ( empty($document_id) )
			return false;

		if ( !empty($update_product['quantity']) && !empty($update_product['basket_id']) )
		{					
			$products = $this->get_products( );			
			$current_reserve = 0;
			foreach( $products as $product )
			{			
				if ( $update_product['basket_id'] == $product->basket_id )
				{						
					$current_reserve = $product->reserve;
					if ( $update_product['reserve'] == $product->reserve && $update_product['quantity'] == $product->quantity )	
						return false;
					else
						break;
				}				
			} 			
			$quantity = $this->get_order_product_quantity( $update_product['basket_id'] );		
			if ( $quantity === false )
				return false;
		
			if ( $quantity < $update_product['quantity'] )			
				$update_product['quantity'] = $quantity;
				
			if ( isset($update_product['reserve']) )
			{   // Изменить резерв		
				$update_product['reserve'] = (int)$update_product['reserve'];
				if ( $update_product['reserve'] > $update_product['quantity'] )			
					$update_product['reserve'] = $update_product['quantity'];
					
				if ( $current_reserve !== $update_product['reserve'] ) 
				{		
					$product_id = usam_get_cart_product( $update_product['basket_id'], 'product_id' );	
					$reserve = usam_get_product_meta($product_id, 'reserve' );	
				
					$reserve = $reserve - $current_reserve + $update_product['reserve'];
					
					usam_update_product_meta($product_id, 'reserve', $reserve );	
					usam_recalculate_stock_product( $product_id );					
				}
			}		
			else
				$update_product['reserve'] = 0;	
	
			$sql = "INSERT INTO `".USAM_TABLE_SHIPPED_PRODUCTS."` (`documents_id`,`basket_id`,`quantity`,`reserve`) VALUES ('%d','%d','%d','%d') ON DUPLICATE KEY UPDATE `quantity`='%d', `reserve`='%d'";
			$insert = $wpdb->query( $wpdb->prepare($sql, $document_id, $update_product['basket_id'], $update_product['quantity'], $update_product['reserve'], $update_product['quantity'], $update_product['reserve'] ));	
			$result = $wpdb->insert_id;		
		}
		else
			$result = false;		
		return $result;		
	}	
	
	//Обновляет информацию в товарах документах отгрузки
	public function set_products( $new_products ) 
	{	
		global $wpdb;	
	
		$document_id = $this->get('id');
		$new_products = apply_filters( 'usam_products_document_shipped_set_properties', $new_products, $this );		
		do_action( 'edit_products_document_shipped_pre', $this );			
					
		foreach( $new_products as $update_product )
		{
			if ( !empty($update_product['quantity']) )				
				$this->add_product( $update_product );			
			else
			{				
				$products = $this->get_products( );	
				$start = false;					
				foreach( $products as $product )
				{ 
					if ( $update_product['basket_id'] == $product->basket_id )
					{										
						if ( $product->reserve !== 0 ) 
						{		
							$product_id = usam_get_cart_product( $update_product['basket_id'], 'product_id' );	
							$reserve = usam_get_product_meta($product_id, 'reserve' );	
							$reserve = $reserve - $product->reserve;					
							usam_update_product_meta($product_id, 'reserve', $reserve );	
							usam_recalculate_stock_product( $product_id );					
						}
						$wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_SHIPPED_PRODUCTS."` WHERE documents_id='%d' AND basket_id='%d'", $document_id, $product->basket_id));
						break;
					}				
				} 				
			}
		}		
		do_action( 'edit_products_document_shipped_after', $this );	
	}		

	// уменьшение запасов
	private function products_stock_updates( ) 
	{		
		if ( $this->is_closed() )
		{			
			$add = false;
		}
		elseif ( $this->previous_status == self::CLOSED )				
			$add = true;
		else
			return;
		
		$products = $this->get_order_products( );			
		if ( empty($products) )
			return;		
		
		if ( !$add )
			$this->remove_all_product_from_reserve();
		
		if ( get_option( 'usam_ftp_download_balances' ) )
			return;		

		$storage_id = $this->get('storage');		
		if ( !empty($storage_id) )
		{		
			usam_products_stock_updates($products, $storage_id, $add );
		}		
	}	

	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_document_shipped_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$this->data['date_update'] = date( "Y-m-d H:i:s" );		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );			
	
			do_action( 'usam_document_shipped_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);

			$this->data = apply_filters( 'usam_document_shipped_update_data', $this->data );			
			
			$format = $this->get_data_format( );		
			$this->data_format( );		
			
			if ( $this->is_track_id_changed && $this->get('status') != 'shipped' )
				$this->data['status'] = 'referred';
								
			foreach( $this->data as $key => $value)
			{				
				if (  $key == 'date_insert' )
					continue;
				
				if (  $key == 'planned_date' || $key == 'doc_data' || $key == 'date_allow_delivery' || $key == 'readiness_date' )
					if ( empty($value) )
						$set[] = "`{$key}`=NULL";
					else					
						$set[] = "`{$key}`='".date( "Y-m-d H:i:s", strtotime( $value ) )."'";
				else
					$set[] = "`{$key}`='{$value}'";						
			}	
			$result = $wpdb->query( $wpdb->prepare("UPDATE `".USAM_TABLE_SHIPPED_DOCUMENTS."` SET ".implode( ', ', $set )." WHERE $where_col ='$where_format'", $where_val) );	
			if ( $result ) 
			{ 
				$include_in_cost = $this->get('include_in_cost');										
				if ( $this->include_in_cost_changed || $this->is_price_changed  )
				{ 
					$price = $this->get('price');
					$order_id = $this->get('order_id');	
					$data = usam_get_order( $order_id );
					if ( $this->is_price_changed && $this->include_in_cost_changed == false && $include_in_cost )
					{ // Если сменилась цена, но включение в цену доставки не изменилось						
						$shipping = $data['shipping'] + $price - $this->previous_price;	
					}
					elseif ( $this->is_price_changed && $this->include_in_cost_changed && $include_in_cost == 0 )
					{						
						$shipping = $data['shipping'] - $this->previous_price;						
					}
					elseif ( $this->include_in_cost_changed && $include_in_cost )
					{						
						$shipping = $data['shipping'] + $price;						
					}
					elseif ( $this->include_in_cost_changed && $include_in_cost == 0  )
					{						
						$shipping = $data['shipping'] - $price;						
					}	
					else
						$shipping = 0;	
					
					usam_update_order( $order_id, array( 'shipping' => $shipping ) );
				}					
				if ( $this->is_status_changed ) 
				{				
					$current_status = $this->get( 'status' );	
					$this->products_stock_updates( );	
					$id = $this->get('id');
					
					do_action( 'usam_update_document_shipped_status',  $id, $current_status, $this->previous_status, $this );
					$this->previous_status = $current_status;
					$this->is_status_changed = false;		
				}			
			} 
			do_action( 'usam_document_shipped_update', $this );
		} 
		else 
		{  
			do_action( 'usam_document_shipped_pre_insert' );			
			
			if ( !isset($this->data['order_id']) )
				return false;
			
			if ( isset($this->data['id']) )
				unset($this->data['id']);
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );				

			if ( empty($this->data['doc_data']) )
				unset($this->data['doc_data']);
			
			if ( empty($this->data['status']) )
				$this->data['status'] = 'pending';	
			
			if ( empty($this->data['readiness_date']) )
				unset($this->data['readiness_date']);
			
			if ( empty($this->data['date_allow_delivery']) )
				unset($this->data['date_allow_delivery']);
			
			if ( !isset($this->data['include_in_cost']) )
				$this->data['include_in_cost'] = 1;
			
			if ( empty($this->data['storage']) )
				$this->data['storage'] = get_option('usam_default_reservation_storage', '' );		
		
			$this->data = apply_filters( 'usam_document_shipped_insert_data', $this->data );			
			$format = $this->get_data_format( );					
			$this->data_format( );		
			$result = $wpdb->insert( USAM_TABLE_SHIPPED_DOCUMENTS, $this->data, $format );
					
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );					
				if ( !empty($this->data['price']) && $this->data['include_in_cost'] )
				{ 
					$data = usam_get_order( $this->data['order_id'] );
					$shipping = $data['shipping'] + $this->data['price'];					
					usam_update_order( $this->data['order_id'], array( 'shipping' => $shipping ) );					
				}						
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );				
			}
			do_action( 'usam_document_shipped_insert', $this );
		} 		
		do_action( 'usam_document_shipped_save', $this );

		return $result;
	}
	
	public function is_closed() 
	{
		return $this->get( 'status' ) == self::CLOSED;
	}
}


// Получить документ доставки
function usam_get_shipped_document( $document_id )
{
	$shipped = new USAM_Shipped_Document( $document_id );
	return $shipped->get_data( );	
}

// Добавить документ доставки
function usam_insert_shipped_document( $data, $products = array() )
{
	$shipped = new USAM_Shipped_Document( $data );	
	$result = $shipped->save();
	$shipped->set_products( $products );
	return $result;
}

// Обновить документ доставки
function usam_update_shipped_document( $document_id, $data, $products = array() )
{
	$shipped = new USAM_Shipped_Document( $document_id );
	$shipped->set( $data );
	$shipped->set_products( $products );
	return $shipped->save();
}

// Удалить документ доставки
function usam_delete_shipped_document( $document_id )
{
	$shipped = new USAM_Shipped_Document( $document_id );
	return $shipped->delete();
}


function usam_get_products_shipped_documents( $document_id, $output_type = 'OBJECT' ) 
{
	global $wpdb;
			
	$products = $wpdb->get_results( $wpdb->prepare( "SELECT cc.*, sp.quantity AS quantity_shipment, sp.reserve AS reserve FROM `". USAM_TABLE_SHIPPED_PRODUCTS ."` AS sp 
					LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc ON ( cc.id=sp.basket_id )					
					WHERE sp.documents_id = '%d'", $document_id), $output_type );
	return $products;
}
?>