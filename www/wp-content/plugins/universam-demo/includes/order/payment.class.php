<?php
/**
 * Документы оплаты
 */ 
class USAM_Payment_Document
{
	 // строковые
	private static $string_cols = array(
		'name',		
		'document_number',
		'document_type',		
		'date_payed',
		'pay_up',		
		'transactid',
		'external_document',
		'date_update',			
		'date_time',	
		'note',		
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'document_id',
		'status',	
		'payment_type',	
		'gateway_id',		
		'bank_account_id',
	);
	// рациональные
	private static $float_cols = array(
		'sum',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $is_status_changed = false;	
	private $previous_status   = false;	
	
	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'document_number', 'transactid' ) ) )
			return;		
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'document_number'  && $id = wp_cache_get( $value, 'usam_payment_document_number' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		if ( $col == 'transactid'  && $id = wp_cache_get( $value, 'usam_payment_transactid' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_payment_document' );
		}		
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_payment_document ) 
	{
		$id = $_payment_document->get( 'id' );	
		wp_cache_set( $id, $_payment_document->data, 'usam_payment_document' );		
		if ( $document_number = $_payment_document->get( 'document_number' ) )
			wp_cache_set( $document_number, $id, 'usam_payment_document_number' );
		if ( $transactid = $_payment_document->get( 'transactid' ) )
			wp_cache_set( $transactid, $id, 'usam_payment_transactid' );
		do_action( 'usam_payment_document_update_cache', $_payment_document );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$_payment_document = new USAM_Payment_Document( $value, $col );
		wp_cache_delete( $_payment_document->get( 'id' ), 'usam_payment_document' );	
		wp_cache_delete( $_payment_document->get( 'document_number' ), 'usam_payment_document_number' );	
		wp_cache_delete( $_payment_document->get( 'transactid' ), 'usam_payment_transactid' );			
		do_action( 'usam_payment_document_delete_cache', $_payment_document, $value, $col );	
	}		
	
	/**
	 *  Удалить документ отгрузки
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_payment_document_before_delete', $this );
		
		$id = $this->get( 'id' );
		
		$order_id = $this->get( 'order_id' );	
		wp_cache_delete( $order_id, 'usam_payment_order' );	
		self::delete_cache( $id );				
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE id = '$id'");
		
		do_action( 'usam_payment_document_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE {$col} = {$format}", $value );
		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{	
			$this->exists = true;
			$this->data = apply_filters( 'usam_payment_document_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_payment_document_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_payment_document_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_payment_document_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}	
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_payment_document_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_payment_document_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$this->data['date_update'] = date( "Y-m-d H:i:s" );
		
		if ( isset($this->data['status']) && $this->data['status'] == 3 && empty($this->data['date_payed']) )			
			$this->data['date_payed'] = date( "Y-m-d H:i:s" );	

		if ( !empty($this->data['date_payed']) )
		{
			$this->data['status'] = 3;	
			$status = $this->get( 'status' );
			if ( $this->previous_status != $this->data['status'] )
			{				
				$this->is_status_changed = true;
			}
		}
		if ( !empty($this->data['gateway_id']) )
		{
			$payment_gateway = usam_get_payment_gateway( $this->data['gateway_id'] );			
			$this->data['name'] = $payment_gateway['name'];
		}			
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	

			if ( isset($this->data) )
				unset($this->data['date_time']);
			
			if ( isset($this->data['payment_type']) )
				unset($this->data['payment_type']);
			
			do_action( 'usam_payment_document_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);

			$this->data = apply_filters( 'usam_payment_document_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );	
			
			foreach( $this->data as $key => $value)
			{										
				if ( $key == 'date_payed' )
					if ( empty($value) )
						$set[] = "`{$key}`=NULL";
					else					
						$set[] = "`{$key}`='".date( "Y-m-d H:i:s", strtotime( $value ) )."'";
				else
					$set[] = "`{$key}`='{$value}'";						
			}	
			$result = $wpdb->query( $wpdb->prepare("UPDATE `".USAM_TABLE_PAYMENT_HISTORY."` SET ".implode( ', ', $set )." WHERE $where_col ='$where_format'", $where_val) );
			do_action( 'usam_payment_document_update', $this );
			if ( $this->is_status_changed ) 
			{		
				if ( $this->data['document_type'] == 'order' )
				{
					$order_id = $this->get('document_id');
					
					$order = new USAM_Order( $order_id );
					$order->mark_order_as_paid( ); 
				}
				$current_status = $this->get( 'status' );
				do_action( 'usam_update_payment_document_status', $current_status, $this->previous_status, $this );				
				$this->previous_status = $current_status;
					
			}
		} 
		else 
		{   
			do_action( 'usam_payment_document_pre_insert' );		
			unset( $this->data['id'] );				
			
			if ( empty($this->data['pay_up']) )
			{
				$day = get_option( 'usam_number_days_delay_payment', 3 );
				$this->data['pay_up'] = date( "Y-m-d H:i:s", mktime(date('H'), date('i'), 0, date('m'), date('d') + $day, date('Y')));		
			}
			else
				$this->data['pay_up'] = date( "Y-m-d H:i:s", strtotime( $this->data['pay_up']) );	
			
			if ( empty($this->data['gateway_id']) )
				$this->data['gateway_id'] = 0;
			
			if ( !isset($this->data['transactid']) )
				$this->data['transactid'] = '';
			
			if ( !isset($this->data['status']) )
				$this->data['status'] = 1;
			
			if ( isset($this->data['date_payed']) && $this->data['date_payed'] == '' )
				unset($this->data['date_payed']);
			
			if ( !isset($this->data['sum']) )
				$this->data['sum'] = 0;
			
			if ( !isset($this->data['payment_type']) )
				$this->data['payment_type'] = 1;					
			
			$this->data['date_time'] = date( "Y-m-d H:i:s" );			

			$this->data = apply_filters( 'usam_payment_document_insert_data', $this->data );			
			$format = $this->get_data_format(  );
			$this->data_format( );				
				
			$result = $wpdb->insert( USAM_TABLE_PAYMENT_HISTORY, $this->data, $format );
					
			if ( $result ) 
			{				
				$sumbol = 10;		
				$k = iconv_strlen( (string)$wpdb->insert_id );
				$r = $sumbol - $k;
				$number = str_repeat( '0' , $r );
				$current_payment = 'PH'.$number.$wpdb->insert_id;		
				$update = $wpdb->query ("UPDATE `".USAM_TABLE_PAYMENT_HISTORY."` SET document_number='{$current_payment}' WHERE id='{$wpdb->insert_id}'");
				
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id );		

				if ( $this->data['document_type'] == 'order' )
				{
					$order_id = $this->get('document_id');
					
					$order = new USAM_Order( $order_id );
					$order->mark_order_as_paid( );					
				}					
			}
			do_action( 'usam_payment_document_insert', $this );
		} 		
		do_action( 'usam_payment_document_save', $this );

		return $result;
	}
}

// Обновить документ оплаты
function usam_update_payment_document( $document_id, $data, $colum = 'id' )
{
	$_payment = new USAM_Payment_Document( $document_id, $colum );
	$_payment->set( $data );
	return $_payment->save();
}

// Получить документ оплаты
function usam_get_payment_document( $document_id, $colum = 'id' )
{
	$_payment = new USAM_Payment_Document( $document_id, $colum );
	$result = $_payment->get_data( );		
	return $result;	
}

// Добавить документ оплаты
function usam_insert_payment_document( $data )
{
	$_payment = new USAM_Payment_Document( $data );
	return $_payment->save();
}

// Удалить документ оплаты
function usam_delete_payment_document( $document_id )
{
	$_payment = new USAM_Payment_Document( $document_id );
	return $_payment->delete();
}


// Выбор статуса оплаты
function usam_get_payment_status_dropdown( $current_status, $args = '' ) 
{				
	static $count = 0;
	$count ++;
	
	$defaults = array(
		'name'                  => 'payment_status',
		'id'                    => "usam-payment_status-dropdown-{$count}",
		'class'                 => 'usam-payment_status_dropdown',
		'additional_attributes' => '',
	);
	$args = wp_parse_args( $args, $defaults );
	$statuses = usam_get_status_payment_documents();
	return usam_get_status_dropdown( $statuses, $current_status, $args );
}

// Вернуть статусы документов оплаты
function usam_get_status_payment_documents( ) 
{
	$statuses = array( 1 => __( 'Не оплачено', 'usam' ), 
					   2 =>	__( 'Отклонено', 'usam' ),
					   3 =>	__( 'Оплачено', 'usam' ),	
					   4 =>	__( 'Платеж возвращен', 'usam' ),	
					   5 =>	__( 'Ошибка оплаты', 'usam' ),	
					   6 =>	__( 'В ожидании', 'usam' ),	
	);
	return $statuses;
}

function usam_get_payment_document_status_name( $status_number ) 
{
	$status = usam_get_status_payment_documents();
	if ( isset($status[$status_number]) )
		$result = $status[$status_number];
	else
		$result = '';
	return $result;
}
?>