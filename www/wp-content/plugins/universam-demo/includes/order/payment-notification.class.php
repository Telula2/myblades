<?php
// Форматы сообщений электронной почты
class USAM_Payment_Notification extends USAM_Notification
{
	protected $payment;	
	
	public function __construct( $payment_id ) 
	{
		$this->payment = usam_get_payment_document( $payment_id );	
		if ( empty($this->payment) )			
		{			
			return false;
		}	
		if ( $this->payment['document_type'] == 'order' )
		{
			$this->object_type     = 'order';
			$this->object_id       = $this->payment['document_id'];
			
			$this->email = usam_get_buyers_email( $this->payment['document_id'] );		
		}
		$this->plaintext_message = $this->process_plaintext_args( );	
		$this->html_message      = $this->process_html_args( );	
	}	
	
	protected function get_plaintext_args() 
	{				
		$order_shortcode = new USAM_Order_Shortcode( $this->payment['document_id'] );
		$args = $order_shortcode->get_plaintext_args();
	
		$args = apply_filters( 'usam_payment_notification_plaintext_args', $args, $this );
		return $args;
	}
	
	protected function get_html_args() 
	{				
		$order_shortcode = new USAM_Order_Shortcode( $this->payment['document_id'] );
		$args = $order_shortcode->get_html_args();
	
		$args = apply_filters( 'usam_payment_notification_html_args', $args, $this );
		return $args;
	}
	
	protected function get_args() 
	{					
		$gateway = usam_get_payment_gateway( $this->payment['gateway_id'] );
		$args = array(			
			'payment_date'      => usam_local_date( $this->payment['date_time'] ),					
			'payment_type'     => $this->payment['document_type'],
			'payment_id'       => $this->payment['id'],
			'payment_name'     => $this->payment['name'],
			'payment_number'   => $this->payment['document_number'],
			'payment_date_payed' => usam_local_date( $this->payment['date_payed'] ),
			'payment_pay_up'    => usam_local_date( $this->payment['pay_up'] ), // Оплатить до
			'payment_sum'      => $this->payment['sum'],
			'payment_sum'      => usam_currency_display( $this->payment['sum'], array( 'display_as_html' => false ) ),
			'payment_status_name' => usam_get_payment_document_status_name( $this->payment['status'] ),	
			'status'              => isset($this->payment['status'])?$this->payment['status']:'',	
			'payment_gateway_id'   => $this->payment['gateway_id'],
			'payment_gateway_name' => $gateway['name'],			
			'payment_transactid' => $this->payment['transactid'],
			'payment_note'       => $this->payment['note'],		
		);		
		$args = apply_filters( 'usam_payment_notification_args', $args, $this );
		return $args;
	}			
}


// Отправить счет 
class USAM_Сustomer_Notification_Invoice extends USAM_Payment_Notification
{		
	public function get_raw_message()
	{			
		$message = get_option( 'usam_payment_invoice_message', __( 'Счет на оплату', 'usam' ) );		
		return apply_filters( 'usam_customer_notification_invoice_raw_message', $message, $this );
	}
		
	public function get_attachments()
	{			
		$attachments = array();		
		
		$file_path = usam_set_invoice_to_pdf( $this->payment['id'] );			
		if ( file_exists( $file_path ) )			
			$attachments = array( $file_path );		

		return $attachments;
	}	

	public function get_subject() 
	{		
		$subject = get_option( 'usam_payment_invoice_subject', __( 'Счет на оплату', 'usam' ) );
		return apply_filters( 'usam_customer_notification_invoice_subject', $subject, $this );
	}
}