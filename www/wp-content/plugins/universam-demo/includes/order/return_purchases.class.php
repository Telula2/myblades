<?php
/**
 * Документы отгрузки
 */ 
class USAM_Return_Purchases
{
	const DRAFT  = 'draft';
	const CLOSED = 'conducted';

	// строковые
	private static $string_cols = array(
		'date_insert',	
		'date_update',	
		'status',	
		'reason',	
		'notes',
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'order_id',				
		'storage',		
	);
	// рациональные
	private static $float_cols = array(
		'sum',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		
    private $data     = array();		
	private $products = null;		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	private $is_status_changed = false;
	private $is_order_paid     = false;	
	private $previous_status   = false;	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_return_purchases' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_return_purchases ) 
	{
		$id = $_return_purchases->get( 'id' );	
		wp_cache_set( $id, $_return_purchases->data, 'usam_return_purchases' );		
		do_action( 'usam_return_purchases_update_cache', $_return_purchases );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$shipped_document = new USAM_Return_Purchases( $value, $col );
		wp_cache_delete( $shipped_document->get( 'id' ), 'usam_return_purchases' );	
		do_action( 'usam_return_purchases_delete_cache', $shipped_document, $value, $col );	
	}	

	public function get_products(  ) 
	{
		global $wpdb;
		
		$document_id = $this->get( 'id' );	
		if ( $this->products === null )
			$this->products = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM `". USAM_TABLE_RETURNED_PRODUCTS ."` WHERE documents_id = '%d'", $document_id) );
					
		return $this->products;
	}		

	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_return_purchases_before_delete', $this );
		
		$id = $this->get( 'id' );
				
		$products = $this->get_products( );
		$products_delete = array();
		foreach( $products as $key => $product )
		{
			$products_delete[$product->basket_id]['quantity'] = 0;			
		}					
		$this->set_products( $products_delete );
		
		$order_id = $this->get( 'order_id' );		
		self::delete_cache( $id );				
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_RETURNED_PRODUCTS." WHERE WHERE documents_id = '$id'");		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_RETURN_PURCHASES." WHERE id = '$id'");		
		do_action( 'usam_return_purchases_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_RETURN_PURCHASES." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_return_purchases_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_return_purchases_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_return_purchases_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_return_purchases_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	

		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}		
				
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_return_purchases_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}

	private function get_order_product_quantity( $basket_id ) 
	{
		$order = new USAM_Order( $this->data['order_id'] );
		$order_products = $order->get_order_products();
		
		$quantity = false;
		foreach( $order_products as $product )
		{
			if ( $product->id == $basket_id )
			{
				$quantity = $product->quantity;
				break;				
			}
		}		
		return $quantity;
	}
			
	// Добавить товар
	private function add_product( $update_product ) 
	{
		global $wpdb;		
		
		$document_id = $this->get('id');				
		if ( empty($document_id) )
			return false;
			
		if ( !empty($update_product['quantity']) )
		{			
			$start = true;
			$products = $this->get_products( );			
			foreach( $products as $product )
			{
				if ( $update_product['basket_id'] == $product->basket_id )
				{
					$start = false;
					$results = array_diff( $update_product, (array)$product );
					if ( count( $results ) > 0 ) 
						$start = true;
					break;
				}				
			} 	
			if ( !$start )
				return false;			
		
			$quantity = $this->get_order_product_quantity( $update_product['basket_id'] );				
			if ( $quantity === false )
				return false;
			
			if ( $quantity < $update_product['quantity'] )			
				$update_product['quantity'] = $quantity;
			
			$sql = "INSERT INTO `".USAM_TABLE_RETURNED_PRODUCTS."` (`documents_id`,`basket_id`,`quantity`) VALUES ('%d','%d','%d') ON DUPLICATE KEY UPDATE `quantity`='%d'";
			$insert = $wpdb->query( $wpdb->prepare($sql, $document_id, $update_product['basket_id'], $update_product['quantity'], $update_product['quantity'] ));	
			$result = $wpdb->insert_id;		
		}
		else
			$result = false;	
		return $result;		
	}	
	
	//Обновляет информацию в товарах документах отгрузки
	public function set_products( $new_products ) 
	{	
		global $wpdb;	
		
		$document_id = $this->get('id');		
		$new_products = apply_filters( 'usam_products_return_purchases_set_properties', $new_products, $this );		
		do_action( 'edit_products_return_purchases_pre', $this );		
		foreach( $new_products as $id => $update_product )
		{				
			if ( $update_product['quantity'] > 0 )				
				$this->add_product( $update_product );			
			else
			{					
				$start = false;						
				$products = $this->get_products( );	
				foreach( $products as $product )
				{
					if ( $update_product['basket_id'] == $product->basket_id )
					{						
						$results = array_diff( $update_product, (array)$product );
						if ( count( $results ) > 0 ) 
							$start = true;
						break;
					}				
				}
				if ( $start )
				{				
					$wpdb->query( $wpdb->prepare("DELETE FROM `".USAM_TABLE_RETURNED_PRODUCTS."` WHERE documents_id = '%d' AND basket_id='%d'", $document_id, $update_product['basket_id'] ) );	
				}
			}
		}	
		$document_price = $wpdb->get_var( "SELECT SUM(quantity * (SELECT price FROM `".USAM_TABLE_PRODUCTS_ORDER."` WHERE ".USAM_TABLE_PRODUCTS_ORDER.".id=".USAM_TABLE_RETURNED_PRODUCTS.".basket_id) ) FROM `".USAM_TABLE_RETURNED_PRODUCTS."` WHERE documents_id = '$document_id'" );		
		$this->set( array( 'sum' => $document_price) );	
		$this->save( );	
		do_action( 'edit_products_return_purchases_after', $this );	
	}	
	
	// уменьшение запасов
	private function products_stock_updates( ) 
	{		
		$status = $this->get( 'status' );
		if ( $status == self::CLOSED )
		{			
			$add = true;
		}
		elseif ( $this->previous_status == self::CLOSED )				
			$add = false;
		else
			return;
	
		$id = $this->get( 'id' );		
		$products = usam_get_products_return_purchasess( $id );			
		if ( empty($products) )
			return;		
	
		if ( get_option( 'usam_ftp_download_balances' ) )
			return;		

		$storage_id = $this->get('storage');		
		if ( !empty($storage_id) )
		{		
			usam_products_stock_updates($products, $storage_id, $add );
		}		
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_return_purchases_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$this->data['date_update'] = date( "Y-m-d H:i:s" );		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );			
			
			do_action( 'usam_return_purchases_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_val);

			$this->data = apply_filters( 'usam_return_purchases_update_data', $this->data );			
			
			$format = $this->get_data_format( );		
			$this->data_format( );		
			
			$result = $wpdb->update( USAM_TABLE_RETURN_PURCHASES, $this->data, $where, $format, $where_format );				
			if ( $this->is_status_changed ) 
			{				
				$current_status = $this->get( 'status' );			
				$id = $this->get( 'id' );		
				$this->products_stock_updates( );
				
				do_action( 'usam_update_return_purchases_status', $id, $current_status, $this->previous_status, $this );
				$this->previous_status = $current_status;	
				$this->is_status_changed = false;	
			}			
			do_action( 'usam_return_purchases_update', $this );
		} 
		else 
		{   
			do_action( 'usam_return_purchases_pre_insert' );				
			
			if ( isset($this->data['id']) )
				unset($this->data['id']);
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );				
			
			if ( empty($this->data['status']) )
				$this->data['status'] = 'draft';				
			
			$this->data = apply_filters( 'usam_return_purchases_insert_data', $this->data );			
			$format = $this->get_data_format( );					
			$this->data_format( );				
			$result = $wpdb->insert( USAM_TABLE_RETURN_PURCHASES, $this->data, $format );
					
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );					
// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );				
			}
			do_action( 'usam_return_purchases_insert', $this );
		} 		
		do_action( 'usam_return_purchases_save', $this );

		return $result;
	}
}


// Получить документ 
function usam_get_return_purchases( $document_id )
{
	$return = new USAM_Return_Purchases( $document_id );
	return $return->get_data( );	
}

// Добавить документ 
function usam_insert_return_purchases( $data, $products = array() )
{
	$return = new USAM_Return_Purchases( $data );	
	$result = $return->save();
	$return->set_products( $products );
	return $result;
}

// Обновить документ 
function usam_update_return_purchases( $document_id, $data, $products = array() )
{
	$return = new USAM_Return_Purchases( $document_id );
	$return->set( $data );
	$return->set_products( $products );
	return $return->save();
}

// Удалить документ 
function usam_delete_return_purchases( $document_id )
{
	$return = new USAM_Return_Purchases( $document_id );
	return $return->delete();
}

/**
 * Получить товары документа возврата
 */  
function usam_get_products_return_purchasess( $document_id, $output_type = 'OBJECT' ) 
{
	global $wpdb;
			
	$products = $wpdb->get_results( $wpdb->prepare( "SELECT cc.*, sp.quantity AS quantity_shipment FROM `". USAM_TABLE_RETURNED_PRODUCTS ."` AS sp 
					LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc ON ( cc.id=sp.basket_id )					
					WHERE sp.documents_id = '%d'", $document_id), $output_type );
	return $products;
}

// Вернуть статусы документов возврата
function usam_get_status_return_purchases_document( ) 
{
	$statuses = array( 'draft' => __( 'Не проведено', 'usam' ), 
					   'conducted' => __( 'Проведено', 'usam' ),					   
	);
	return $statuses;
}

function usam_get_title_status_return_purchases_document( $status_number ) 
{
	$status = usam_get_status_return_purchases_document();
	if ( isset($status[$status_number]) )
		$result = $status[$status_number];
	else
		$result = '';
	return $result;
}

// Выбор статуса
function usam_get_refunds_purchases_status_dropdown( $current_status, $args = '' ) 
{				
	static $count = 0;
	$count ++;
	
	$defaults = array(
		'name'                  => 'payment_status',
		'id'                    => "usam-payment_status-dropdown-{$count}",
		'class'                 => 'usam-payment_status_dropdown',
		'additional_attributes' => '',
	);
	$args = wp_parse_args( $args, $defaults );
	$statuses = usam_get_status_return_purchases_document();
	return usam_get_status_dropdown( $statuses, $current_status, $args );
}

//Получить ссылку на редактирование возврата
function usam_get_url_edit_refunds_purchases( $id ) 
{
	$url = add_query_arg( array('page' => 'orders', 'tab' => 'refunds', 'action' => 'edit', 'id' => $id), admin_url('admin.php') );
	return $url;
}

?>