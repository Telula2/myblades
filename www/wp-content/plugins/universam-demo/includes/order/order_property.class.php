<?php
class USAM_Order_Property
{		
	 // строковые
	private static $string_cols = array(
		'name',		
		'type',		
		'unique_name',
		'options',	
		'mask',	
		'date_insert',			
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'group',				
		'mandatory',			
		'active',
		'fast_buy',
		'profile',
		'sort',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'unique_name' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'unique_name'  && $id = wp_cache_get( $value, 'usam_properties_order_code' ) )
		{   // если находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_properties_order' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_properties_order' );
		if ( $unique_name = $log->get( 'unique_name' ) )
			wp_cache_set( $unique_name, $id, 'usam_properties_order_code' );	
		do_action( 'usam_properties_order_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_Order_Property( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_properties_order' );
		wp_cache_delete( $log->get( 'unique_name' ), 'usam_properties_order_code' );		
		do_action( 'usam_properties_order_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_properties_order_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_ORDER_PROPS." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_properties_order_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_ORDER_PROPS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$data['options'] = unserialize( $data['options'] );		
			
			$this->exists = true;
			$this->data = apply_filters( 'usam_properties_order_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_properties_order_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_properties_order_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_properties_order_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_properties_order_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_properties_order_pre_save', $this );	
		$where_col = $this->args['col'];
			
		if ( isset($this->data['options']) )	
			$this->data['options']     = serialize( $this->data['options'] );
		
		$result = false;	
		if ( $where_col ) 
		{	// обновление 		
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_properties_order_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			$this->data = apply_filters( 'usam_properties_order_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );		
			$data = $this->data;
			
			$str = array();
			foreach ( $format as $key => $value ) 
			{
				if ( $data[$key] === null )
				{
					$str[] = "`$key` = NULL";
					unset($data[$key]);
				}
				else
					$str[] = "`$key` = '$value'";	
			}			
			$sql = "UPDATE `".USAM_TABLE_ORDER_PROPS."` SET ".implode( ', ', $str )." WHERE $where_col = '$where_format' ";			
			$result = $wpdb->query( $wpdb->prepare( $sql, array_merge( array_values( $data ), array( $where_val ) ) ) );
			do_action( 'usam_properties_order_update', $this );
		} 
		else 
		{  
			if ( !empty($this->data['unique_name']) )
			{
				$checkout_id = get_checkout_id( $this->data['unique_name'] );
				if ( !empty($checkout_id) )
					return;
			}
			else
				return;
			do_action( 'usam_properties_order_pre_insert' );			
			
			unset( $this->data['id'] );
						
			$this->data = apply_filters( 'usam_properties_order_insert_data', $this->data );
			$format = $this->get_data_format( );
			$this->data_format( );		
			
			$result = $wpdb->insert( USAM_TABLE_ORDER_PROPS, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_properties_order_insert', $this );
		} 		
		do_action( 'usam_properties_order_save', $this );
		
		return $result;
	}
}

// Получить данные заказа
function usam_get_order_property( $value, $col = 'id' )  
{
   $order_property = new USAM_Order_Property( $value, $col );	
   return $order_property->get_data();
}

function usam_update_order_property( $id, $data )
{
	$order_property = new USAM_Order_Property( $id );
	$order_property->set( $data );
	return $order_property->save();
}

/*
 * Получить id поля оформления заказа
 */
function get_checkout_id( $unique_name ) 
{
	global $wpdb;
	$sql = $wpdb->prepare( "SELECT `id` FROM `" . USAM_TABLE_ORDER_PROPS . "` WHERE `unique_name` = '%s'", $unique_name );
	$id = $wpdb->get_var( $sql );	
	return $id;
}


// Получить группы свойств заказов
function usam_get_order_props_groups( $args = array() ) 
{
	global $wpdb;
	
	$_where = array( '1=1' );
	if ( isset($args['type_payer']) )
		$_where[] = 'type_payer_id='.$args['type_payer'];
	
	if ( !empty($args['output_type']) )
		$output_type = $args['output_type'];
	else
		$output_type = 'OBJECT_K';
	
	$where = implode( ' AND ', $_where);
	
	if ( empty($args['orderby']) )
		$orderby = 'sort';
	
	if ( empty($args['order']) )
		$order = 'ASC';
					
	$result = $wpdb->get_results( "SELECT * FROM `" . USAM_TABLE_ORDER_PROPS_GROUP . "` WHERE $where ORDER BY $orderby $order", $output_type );	
	return $result;
}		

// Получить группу свойств заказа
function usam_get_order_props_group( $id ) 
{
	global $wpdb;			

	$cache_key = 'usam_order_props_group';	
	if( ! $result = wp_cache_get($id, $cache_key ) )			
	{
		$result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM `" . USAM_TABLE_ORDER_PROPS_GROUP . "` WHERE id='%d'", $id ), ARRAY_A );
		wp_cache_set( $id, $result, $cache_key );
	}
	return $result;
}	

// Добавить группу свойств заказа
function usam_insert_order_props_group( $insert ) 
{	
	global $wpdb;		
	
	$format_bd = array( 'id' => '%d', 'name' => '%s', 'type_payer_id' => '%d', 'sort' => '%d' );
	$format = array();
	foreach ( $insert as $key => $value ) 
	{
		if ( isset($format_bd[$key]) ) 
			$format[] = $format_bd[$key];
	}
	$wpdb->insert( USAM_TABLE_ORDER_PROPS_GROUP, $insert, $format );
	
	return $wpdb->insert_id;
}		


// Обновить группу свойств заказа
function usam_update_order_property_group( $id, $update ) 
{	
	global $wpdb;		
	
	$where['id'] = $id;
	
	$format_bd = array( 'id' => '%d', 'name' => '%s', 'type_payer_id' => '%d', 'sort' => '%d' );
	$format = array();
	foreach ( $update as $key => $value ) 
	{
		if ( isset($format_bd[$key]) ) 
			$format[] = $format_bd[$key];
	}
	$format_where[] = '%d';
	$wpdb->update( USAM_TABLE_ORDER_PROPS_GROUP, $update, $where, $format, $format_where );
}		

// Получить форматированное свойство заказа
function usam_get_display_order_property( $value, $type ) 
{
	switch ( $type )
	{							
		case 'phone':	
		case 'mobile_phone':	
			$display = usam_get_phone_format( $value );
		break;
		case 'location':	
			$display = usam_get_full_locations_name( $value );
		break;
		case 'checkbox':	
			$values = maybe_unserialize($value);
			$display = implode( ', ', $values );
		break;
		case 'location_type':					
			if(is_numeric($value))
			{
				$location = usam_get_location( $value );
				$display = $location['name'];
			}
			else
				$display = $value;								
		break;
		default:
			$display = $value;
	}
	return esc_html( $display );
}
?>