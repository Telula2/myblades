<?php
class USAM_Order_Status_Change
{
	private $purchase_log;
	public function __construct( ) 
	{	
		add_action( 'usam_update_order_status', array( $this, 'update_order_status' ), 10, 4 );	
	}

	// Действие при обновлении статуса заказа в Журнале продаж. Обрабатывает статусы заказов при их получении. Уменьшает запасы на складе
	function update_order_status( $order_id, $status, $old_status, $purchase_log ) 
	{		
		$this->purchase_log = $purchase_log;				
		if ( !$this->purchase_log->is_creature_order() )
		{
			$this->send_email_customer( );	// отправить письмо покупателю	
			$this->process_transaction_coupon( );		
		}			
		if ( $this->purchase_log->is_closed_order() )
		{			
			$user_id = $this->purchase_log->get( 'user_ID' );
			
			if ( $user_id != 0 )
			{
				$bonus = new USAM_Work_Bonuses( $user_id );
				$bonus->order_completed( $order_id );
				$bonus->write_off_bonuses( $order_id );						

				$discount = usam_get_accumulative_discount_customer( 'price', $user_id );		
				update_user_meta( $user_id, 'usam_accumulative_discount', $discount );			
			}
			$this->process_transaction_rules_coupon( );	
		}	
		if ( $status == 'no_connection' )
		{
			$this->unable_to_contact();
		}		
	/*	if ( usam_check_order_is_completed( $this->purchase_log->get( 'status' ) ) )
		{			
			
		}		*/
	}	
	
	public function unable_to_contact(  )
	{
		$order = $this->purchase_log->get_data();		
		$task['title'] = sprintf( __('Связаться с клиентом о заказе №%s','usam'), $order['id'] );				
		$task['date_time'] = date("Y-m-d H:i:s", mktime( date('H'),date('i'),0,date('m'),date('d')+1,date('Y')));
		
		$objects = array( array('object_type' => 'order', 'object_id' => $order['id']) );
		
		if ( usam_is_type_payer_company($order['type_payer']) ) 		
			$customer_type = 'company';		
		else
			$customer_type = 'contact';

		if ( !empty($order['customer_id']) )
			$objects[] = array('object_type' => $customer_type, 'object_id' => $order['customer_id'] ); 
		
		usam_insert_system_event( $task, $objects );		
	}
	
	// отправка письма при изменение статуса заказа
	public function send_email_customer(  )
	{		
		if (  apply_filters( 'usam_prevent_notification_change_order_status', true ) ) 	
		{
			$notification = new USAM_Сustomer_Notification_Change_Order_Status( $this->purchase_log );			
			$email_sent = $notification->send_mail();
			$sms_sent = $notification->send_sms();	
		}
	}
	
	//процессе обработки купона сделки
	function process_transaction_coupon( ) 
	{
		global $wpdb;	

		$coupon_name = $this->purchase_log->get( 'coupon_name' );
		if ( ! empty( $coupon_name ) ) 
		{
			$_coupon = new USAM_Coupon( $coupon_name, 'coupon_code' );
			$coupon_data = $_coupon->get_data();
			if ( $coupon_data['active'] )
			{
				if ( 1 == $coupon_data['use_once'] ) 	
				{
					$coupon_data['active'] = 0;				
				}					
				$coupon_data['is_used']++;
				if ( $coupon_data['max_is_used'] > 0 && $coupon_data['max_is_used'] < $coupon_data['is_used'] )
					$coupon_data['active'] = 0;
				
				$_coupon->set( $coupon_data );
				$_coupon->save();			
			}
		}
	}
	
	//процесс обработки правил купонов
	function process_transaction_rules_coupon( ) 
	{
		$option = get_option('usam_coupons_roles', array() );	
		$coupons_roles = maybe_unserialize( $option );
		if ( ! empty( $coupons_roles ) ) 
		{
			$order = $this->purchase_log->get_data();
			
			$totalprice = (int)$order['totalprice'];			
			$user = get_userdata( $order['user_ID'] );				
			$order_id = $this->purchase_log->get( 'id' );	
			$location = usam_get_buyers_location( $order_id );
			if ( !empty($location) )
				$current_location_ids = usam_get_array_locations_up( $location, 'id' );
			else
				$current_location_ids = array();					
			
			foreach( $coupons_roles as $key => $role )
			{	
				if ( $role['active'] == 1 && $role['totalprice'] <= $totalprice )
				{			
					if ( !empty($role['roles']) )
					{
						if ( empty($order['user_ID']) && in_array( 'notLoggedIn', $role['roles']) )						
						{							
							
						}
						elseif ( !empty($order['user_ID']) )
						{							
							if ( !empty($user) )
							{
								$result_in_roles = array_intersect($role['roles'], $user->roles);	
								if ( empty($result_in_roles) )
									continue;
							}
							else
								continue;
						}
						else
							continue;						
					}				
					$_coupon = new USAM_Coupon( $role['coupon_id'] );
					$coupon = $_coupon->get_data();				
			
					if (empty($coupon))
						continue;
						
					if ( !empty($role['locations']) )
					{						
						if ( empty($current_location_ids) )						
							continue;
						
						$result = array_intersect($role['locations'], $current_location_ids );		
						if ( empty($result) )
						{
							continue;
						}
					}		
					switch( $role['discount_type'] )
					{			
						case 0:
							$coupon['value'] = (double)$role['discount'];
							$coupon['is_percentage'] = 0;
						break;
						case 1:
							$coupon['value'] = (double)$role['discount'];
							$coupon['is_percentage'] = 1;
						break;
						case 2:
							$coupon['value'] = (double) round($role['discount']*$totalprice/100, 0);						
							$coupon['is_percentage'] = 0;
						break;
						case 3:
							$coupon['value'] = 0;
							$coupon['is_percentage'] = 2;
						break;
					}
					if ( $role['percentage_of_use'] && $coupon['is_percentage'] === 0 )
					{
						$order_sum = (double) round( $coupon['value']*100/$role['percentage_of_use'], 0);	
						$condition = array( 'type' => 'simple', 'property' => 'subtotal', 'logic' => 'greater', 'value' => $order_sum );						
						if ( !empty($coupon['condition']) )
						{
							$coupon['condition'][] = array( 'logic_operator' => 'AND' );
						}
						$coupon['condition'][] = $condition;
					}
					else
						$order_sum = 0;					
					
					$coupon['coupon_code']   = usam_rand_string(10,'1234567890');
					$coupon['description']   = sprintf( __( 'Купон создан заказом #%s из правила #%s.', 'usam' ), $order_id, $role['title'] );
					$coupon['active']        = 1;				
					$coupon['start']         = date( "Y-m-d H:i:s" );			
					$coupon['expiry']        = date( 'Y-m-d H:i:s', mktime(0, 0, 0, date("m"), date("d")+$role['day'], date("Y")) );
					$coupon['coupon_type']   = 'coupon';										
					if ( !empty($role['customer']) )	
						$coupon['customer'] = 0;	
					elseif ( is_numeric($role['customer']) )					
						$coupon['customer'] = $role['customer'];					
					elseif ( $role['customer'] == 'order' )
					{
						$user = $this->purchase_log->get( 'user_ID' );							
						$coupon['customer'] = $user;	
					}
					$_coupon = new USAM_Coupon();
					$_coupon->set( $coupon );
					$_coupon->save();								
		
					$email = usam_get_buyers_email( $order_id );					
					if ( $email == '' )
						return;					
					$args = array(									
						'total_price'     => $totalprice,
						'shop_name'       => get_option( 'blogname' ),
						'coupon_sum'      => $coupon['value'],						
						'coupon_code'     => $coupon['coupon_code'],
						'coupon_day'      => $role['day'],
						'order_sum'       => $order_sum,		
						'order_id'        => $order_id,
						'procent'         => $role['discount'],
					);					
					$args = array_map( 'esc_html', $args );	
					$tokens = array_keys( $args );
					$values = array_values( $args );

					foreach ( $tokens as &$token ) 
					{	
						$token = "%{$token}%";
					}	
					$html = str_replace( $tokens, $values, get_option( 'usam_coupon_add_message' ) );
					$html = wpautop( $html );		
					$message = str_replace( "<br />", '<br /><br />',$html);
					
					$subject = get_option( 'usam_coupon_add_subject' );
					
					$email_sent = usam_send_mail_by_id( $email, '', $message, $subject);		
					$sms_message = str_replace( $tokens, $values, get_option( 'usam_coupon_add_sms' ) );					
					
					$phone = usam_get_buyers_phone( $order_id );
					$sms_sent = usam_send_sms( $phone, $sms_message, $order_id, 'order' );					
					return true;
				}				
			}		
		}
	}	
}
new USAM_Order_Status_Change();