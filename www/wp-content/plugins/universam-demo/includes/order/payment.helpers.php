<?php
class USAM_Payment_Status_Change
{	
	public function __construct( ) 
	{	
		add_action( 'usam_update_payment_document_status', array( $this, 'update_status' ), 10, 4 );	
	}
	
	function update_status( $status, $old_status, $payment ) 
	{
		$gateway_id = $payment->get('gateway_id');
		if ( $status == 3 && $gateway_id )
		{
			$gateway = usam_get_payment_gateway( $gateway_id );
			if ( $gateway['cashbox'] )
			{ 				
				$class = usam_get_cashbox_class( $gateway['cashbox'] );		
				if ( is_object($class) )		
				{
					$gateway_id = $payment->get('document_id');
					if ( $payment->get('document_type') == 'order' )
						$class->sell( $gateway_id);
				//	if ( $payment->get('document_type') == 'refund' )
				//		$class->sell_refund( $gateway_id);					
				}
			}
		}		
	}	
}
new USAM_Payment_Status_Change();