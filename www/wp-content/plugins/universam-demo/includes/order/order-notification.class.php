<?php
// Форматы сообщений электронной почты

class USAM_Order_Notification extends USAM_Notification
{
	protected $purchase_log;
	protected $is_order = false;
	protected $shipping_id;
	
	protected $plaintext_product_list = '';
	protected $html_product_list = '';
	protected $html_advertising_products = '';		
	
	public function __construct( $order_id, $shipping_id = null ) 
	{	
		if ( is_numeric( $order_id ) )		
			$purchase_log = new USAM_Order( $order_id );
		else
			$purchase_log = $order_id;
		
		$order = $purchase_log->get_data();		
		if ( empty($order) )		
			return false;	
		
		$this->is_order        = true;				
		$this->purchase_log    = $purchase_log;					
		$this->shipping_id     = $shipping_id;			
		
		$this->object_type     = 'order';
		$this->object_id       = $order['id'];			
		
		$this->phone = usam_get_buyers_phone( $order['id'] );	
		$this->email = usam_get_buyers_email( $order['id'] );
			
		$this->plaintext_message = $this->process_plaintext_args( );			
		$this->html_message      = $this->process_html_args( );		
		$this->sms_message       = $this->process_sms_message_args( );		
	}	
	
	protected function get_plaintext_args() 
	{				
		$order_shortcode = new USAM_Order_Shortcode( $this->purchase_log );
		$args = $order_shortcode->get_plaintext_args();
	
		$args = apply_filters( 'usam_order_notification_plaintext_args', $args, $this );
		return $args;
	}
	
	protected function get_html_args() 
	{		
		$args = array(			
			'advertising_products' => $this->create_html_advertising_products(),
		); 
		$order_shortcode = new USAM_Order_Shortcode( $this->purchase_log );
		$args += $order_shortcode->get_html_args();

		$args = apply_filters( 'usam_order_notification_html_args', $args, $this );
		return $args;
	}
	
	
	private function create_html_advertising_products()
	{
		$item = 3;
		$args = array( 'orderby' => 'rand', 'post_status' => 'publish', 'posts_per_page' => $item, 'meta_key' => '0', 'meta_value' => '_usam_stock', 'meta_compare' => '>' );  				
		$attachments = usam_get_products( $args, true );  
		if ($attachments) 
		{  
			$output = '<table class="usam_advertising_products" style ="width: 100%;">';
			$output .= "<tr><td colspan='$item' style='color:#697A9C;border-bottom: 2px solid #697A9C;text-align:left;font-style:italic;'>Мы рекомендуем!</td></tr>";				
			$output .= '<tr>';
			$i = 0;			
			foreach ( $attachments as $post ) 
			{  
				$product_id = $post->ID;
				$price = usam_currency_display( usam_get_product_price( $product_id ), array( 'display_as_html' => false ) );				
				$old_price = usam_currency_display( usam_get_product_old_price( $product_id ), array( 'display_as_html' => false ) );
				if ($i % 3 == 0)
					$style_blok = 'float: left;';
				else
					$style_blok = 'float: right;';
				$output .= "
			<td style ='padding-top:15px;'>				
				<div class='product_grid_item' style ='width: 160px; height: 235px; margin: 0 auto !important; display: block;box-shadow: #000000 0px 0px 10px 0px;'>
					<a href='".get_permalink($product_id)."' style = 'text-decoration: none; color: black; font-size: 11px;'>
						<div class='image_box'>
							".usam_get_product_thumbnail($product_id, 'product-image', $post->post_title)."							
						</div>
						<div class='ptitle'>
							<p class='title' style = 'height: 31px;overflow: hidden;line-height: 11px;margin:2px; font-family: Georgia,Arial,sans-serif;text-transform: uppercase;font-weight:700;font-size: 10px;text-align:center;'>".$post->post_title."</p>
							<p class='old_price' style = 'height: 13px;text-decoration: line-through;margin:0;text-align:center;'>". (($old_price > 0) ? $old_price : '')."</p>
							<p class='price' style = 'margin:0;text-align:center;'>$price</p>
						</div>
					</a>							
				</div>
			</td>";
				$i++;
			}  
			$output .= '</tr>';
			$output .= '</table>';			
			wp_reset_postdata();  			
		} 	
		return $output;
	}
	
	protected function add_message_customer_data( ) 
	{
		$customer_data = $this->purchase_log->get_customer_data();	
		$args['type_payer']	= $this->purchase_log->get('type_payer');
		$props_group = usam_get_order_props_groups( $args );
		$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );	
		$message = '';	
		foreach ( $props_group as $group ) 
		{
			$message .= "<strong>{$group->name}</strong>\r\n";		
			foreach ( $list_properties as $unique_name => $field ) 				
				if ( $group->id === $field->group  && !empty($customer_data[$unique_name]['value']) )
					$message .= $field->name . ' : ' . usam_get_display_order_property( $customer_data[$unique_name]['value'], $field->type ) . "\r\n";
			$message .= "\r\n";
		}
		return $message;
	}	
	
	public function get_raw_sms_message() 
	{	
		$status = $this->purchase_log->get('status');
		$message = get_option( "usam_{$status}_sms", '' );		
		return $message;
	}	
}




// Письмо администратору магазина о новом заказе
class USAM_Order_Admin_Notification extends USAM_Order_Notification
{			
	//Письмо информация о транзакции
	public function get_raw_sms_message() 
	{			
		$message = __( 'Получен новый заказ #%order_id% на сумму %total_price%', 'usam' );
		return apply_filters( 'usam_order_admin_notification_raw_sms_message', $message, $this );
	}
	
//Письмо информация о транзакции
	public function get_raw_message() 
	{					
		$message = '<strong>' . __( 'Получен новый заказ', 'usam' ) . ": #%order_id%</strong>\r\n";			
		// Скидка				
		$coupon_name = $this->purchase_log->get('coupon_name');		
		if ( ! empty( $coupon_name ) ) 
		{
			$message .= __( 'Код купона', 'usam' ) . ': %coupon_code%' . "\r\n";
			$message .= __( 'Размер скидки', 'usam' ) . ': %coupon_discount%' . "\r\n";
		}
		//Итого, налог, доставка, сумма
		$message .= __( 'Cумма заказа' ,'usam' ) . ': %order_final_basket_currency%' . "\r\n";		
		if ( $this->purchase_log->get( 'tax' ) != 0 ) 			
			$message .= __( 'Налог', 'usam' ) . ': %total_tax_currency%' . "\r\n";			
		if ( $this->purchase_log->get( 'shipping' ) != 0 ) 
			$message .= __( 'Доставка', 'usam' ) . ': %total_shipping_currency%' . "\r\n";
		$message .= __( 'Итог', 'usam' ) . ': %total_price_currency%' . "\r\n";
		$message .= '<strong>' . __( 'Способ оплаты', 'usam' ) . "</strong>\r\n";
		$message .= "%payment_list%\r\n";		
		$message .= "\r\n";			
		$message .= __( 'Способ доставки', 'usam' ) . ': %shipping_method_name%' . "\r\n";
		$message .= "\r\n";
		// Товары
		$message .= '<strong>' . __( 'Купленные товары', 'usam' ) . "</strong>\r\n";
		$message .= "%product_list%\r\n";		
		$message .= "\r\n";				
		$message .= $this->add_message_customer_data( );			
		return apply_filters( 'usam_order_admin_notification_raw_message', $message, $this );
	}	

	public function get_subject() 
	{
		return apply_filters( 'usam_order_admin_notification_subject', __( 'Получен новый заказ', 'usam' ), $this );
	}
}
/* 
 * Описание: Отправка сообщений о дате готовности заказа
 */
class USAM_Order_Notification_Willingness extends USAM_Order_Notification
{
	public function get_raw_message() 
	{		
		$message = get_option( 'usam_willingness_order_message', '' );
		return apply_filters( 'usam_customer_notification_willingness_order_raw_message', $message, $this );
	}

	public function get_subject() 
	{
		$subject = get_option( 'usam_willingness_order_subject', __( 'Информация о готовности Вашего заказа', 'usam' ) );
		return apply_filters( 'usam_customer_notification_willingness_order_subject', $subject, $this );
	}
}

/* 
 * Подготовка письма при изменении статуса заказа
 */
class USAM_Сustomer_Notification_Change_Order_Status extends USAM_Order_Notification
{		
	public function get_raw_message()
	{	
		$status = $this->purchase_log->get('status');		
		$message = get_option( "usam_{$status}_message", '' );		
		return apply_filters( 'usam_customer_notification_change_order_status_raw_message', $message, $this );
	}
	
	public function get_subject() 
	{
		$status = $this->purchase_log->get('status');	
		$subject = get_option( "usam_{$status}_subject", __( 'Информация о вашем заказе', 'usam' ) );
		return apply_filters( 'usam_customer_notification_change_order_status_subject', $subject, $this );
	}
}

// Отправить письмо повторно
class USAM_Сustomer_Notification_Order extends USAM_Order_Notification
{		
	public function get_raw_message()
	{			
		$message = get_option( 'usam_received_message', '' );	
		return apply_filters( 'usam_customer_notification_order_raw_message', $message, $this );
	}
		
	public function get_subject() 
	{		
		$subject = get_option( 'usam_order_received_subject', __( 'Информация о вашем заказе', 'usam' ) );
		return apply_filters( 'usam_customer_notification_order_subject', $subject, $this );
	}
}

// Отправить трек-номер
class USAM_Сustomer_Notification_Tracking extends USAM_Order_Notification
{			
	public function get_raw_message()
	{	
		$message = get_option( 'usam_trackingid_message' );	
		return apply_filters( 'usam_customer_notification_trackingid_raw_message', $message, $this );
	}
	
	public function get_subject() 
	{	
		$subject = get_option( 'usam_trackingid_subject', __( 'Номер отслеживания вашего заказа', 'usam' ) );
		return apply_filters( 'usam_customer_notification_trackingid_subject', $subject, $this );
	}
}