<?php
class USAM_Order_Shortcode
{		
	protected $purchase_log;
	protected $shipping_id;	
	
	public function __construct( $order_id, $shipping_id = null ) 
	{	
		if ( is_numeric($order_id) )	
			$this->purchase_log = new USAM_Order( $order_id );
		elseif ( is_object($order_id) )
			$this->purchase_log = $order_id;
		else
			return false;
	
		$order = $this->purchase_log->get_data();		
		if ( empty($order) )		
			return false;			
		
		$this->shipping_id     = $shipping_id;			
	}

	public function get_common_args() 
	{		
		if ( !is_object($this->purchase_log) )
			return array();
	
		$data = $this->purchase_log->get_calculated_data();
		$order = $this->purchase_log->get_data();		
		if ( $this->shipping_id !== null )
			$shipped_document = usam_get_shipped_document( $this->shipping_id );
		else
		{		
			$shipped_documents = $this->purchase_log->get_shipped_documents();
			if ( empty($shipped_documents) )				
				$shipped_document = array();
			else
				$shipped_document = (array)array_pop($shipped_documents);
		}		
		$payment_documents = $this->purchase_log->get_payment_history();

		if ( empty($payment_documents) )				
			$payment_document = array();
		else
			$payment_document = array_pop($payment_documents);
			
		$discount = $order['coupon_discount']+$order['bonus'];

		if ( !empty($shipped_document['storage_pickup']) )
			$storage = usam_get_storage( $shipped_document['storage_pickup'] );
		else
			$storage = array();	
		
		$payment = $this->purchase_log->get_payment_status_sum();		
		
		$currency_display1 = array(
			'display_currency_symbol' => false,
			'decimal_point'   		  => true,
			'display_currency_code'   => false,
			'display_as_html'         => false,
			'code'                 => false,
		);
		$currency_display2 = array(
			'display_currency_symbol' => true,
			'decimal_point'   		  => true,
			'display_currency_code'   => false,
			'display_as_html'         => false,
			'code'                 => false,
		);
		$products = $this->purchase_log->get_order_products();
	
		$date_format = get_option('date_format', "d.m.Y");
		$args = array(			
			'order_date'       => usam_local_date( $order['date_insert'] ),
			'current_date'     => date_i18n( $date_format ),			
			'order_id'         => $order['id'],		
			'order_basket'         => usam_currency_display( $data['order_basket'], $currency_display1 ),			
			'order_basket_currency' => usam_currency_display( $data['order_basket'], $currency_display2 ),
			'order_final_basket'         => usam_currency_display( $data['order_final_basket'], $currency_display1 ),			
			'order_final_basket_currency' => usam_currency_display( $data['order_final_basket'], $currency_display2 ),
			'order_basket_discount'         => usam_currency_display( $data['order_basket_discount'], $currency_display1 ),			
			'order_basket_discount_currency' => usam_currency_display( $data['order_basket_discount'], $currency_display2 ),
			'status_name'      => $order['status'],
			'status_title'      => usam_get_order_status_name( $order['status'] ),
			'status_description' => usam_get_order_status_description( $order['status'] ),
			'payment_status'   => usam_get_order_payment_status_name( $order['paid'] ),		
			'order_paid'       => $order['paid'],	
			'number_products'  => count($products),	
			'total_tax'        => usam_currency_display( $order['total_tax'],$currency_display1 ),
			'total_tax_currency' => usam_currency_display( $order['total_tax'], $currency_display2 ),
			'total_tax_title'   => sprintf( __( 'Налог: %s', 'usam' ), $order['total_tax'] )."\r\n",
			'total_shipping'   => usam_currency_display($order['shipping'], $currency_display1 ),
			'total_shipping_currency' => usam_currency_display( $order['shipping'], $currency_display2 ),
			'total_price'      => $order['totalprice'],
			'total_price_currency' => usam_currency_display( $order['totalprice'], $currency_display2 ),
			'total_price_word' => usam_get_number_word( $order['totalprice'] ),
			'shop_name'        => get_option( 'blogname' ),			
			'total_discount_title' => sprintf( __( 'Сумма скидки: %s', 'usam' ), $discount )."\r\n",			
			'coupon_code'      => $order['coupon_name'],
			'coupon_discount'  =>usam_currency_display( $order['coupon_discount'], $currency_display1),
			'total_discount'   => $discount,
			'total_discount_currency'   => usam_currency_display( $discount, $currency_display2 ),	
			'bonus'            => $order['bonus'],				
			'shipping_method'  => isset($shipped_document['method']) ? $shipped_document['method'] : '' ,
			'shipping_method_name' => isset($shipped_document['name']) ? $shipped_document['name'] : '' ,
			'shipping_readiness_date' => isset($shipped_document['readiness_date']) ? date_i18n($date_format, strtotime($shipped_document['readiness_date'])): '' ,
			'track_id'         => isset($shipped_document['track_id']) ? $shipped_document['track_id'] : '',	
			'storage_address'  => isset($storage['address']) ? $storage['address'] : '' ,	
			'storage_city'     => isset($storage['city']) ? $storage['city'] : '' ,			
			'storage_phone'    => isset($storage['phone']) ? $storage['phone'] : '' ,
			'storage_schedule' => isset($storage['schedule']) ? $storage['schedule'] : '',	
			'gateway_name' 	   => isset($payment_document['name']) ? $payment_document['name'] : '' ,
			'gateway' 		   => isset($payment_document['gateway']) ? $payment_document['gateway'] : '' ,	
			'total_paid'       => usam_currency_display( isset($payment['total_paid']) ? $payment['total_paid'] : 0, $currency_display1 ),		
			'total_paid_currency' => usam_currency_display( isset($payment['total_paid']) ? $payment['total_paid'] : 0, $currency_display2 ),	
		);	
		$requisites = usam_shop_requisites_shortcode();			
		$args += $requisites;
		
		$list_properties = usam_get_order_properties( array('fields' => 'id=>unique_name', 'active' => 'all' ) );		
		$customer_data = $this->purchase_log->get_customer_data();		
		foreach ($list_properties as $id => $unique_name )	
		{		
			$args[$unique_name] = isset($customer_data[$unique_name]['value'])?$customer_data[$unique_name]['value']:'';
		}	
		
		if ( usam_is_type_payer_company( $order['type_payer'] ) ) 
		{ 			
			$company = isset($customer_data['company']['value'])?$customer_data['company']['value']:'';
			$inn = isset($customer_data['inn']['value'])?$customer_data['inn']['value']:'';
			$kpp = isset($customer_data['kpp']['value'])?$customer_data['kpp']['value']:'';
			$contact_person = isset($customer_data['contact_person']['value'])?$customer_data['contact_person']['value']:'';
			$company_phone = isset($customer_data['company_phone']['value'])?$customer_data['company_phone']['value']:'';			
			$address_location = isset($customer_data['company_shippinglocation']['value'])?usam_get_full_locations_name($customer_data['company_shippinglocation']['value']):'';
			$address = isset($customer_data['company_shippingaddress']['value'])?$customer_data['company_shippingaddress']['value']:'';
			
			$args['counterparty'] = $company." ".$inn."/".$kpp.' '.$contact_person.' '.$address_location.' '.$address.' '.$company_phone;
			
			$args['counterparty_shippingaddress'] = $address;
		}
		else
		{ 			
			$billingfirstname = isset($customer_data['billingfirstname']['value'])?$customer_data['billingfirstname']['value']:'';
			$billinglastname = isset($customer_data['billinglastname']['value'])?$customer_data['billinglastname']['value']:'';
			$billingphone = isset($customer_data['billingphone']['value'])?$customer_data['billingphone']['value']:'';
			$address = isset($customer_data['billingaddress']['value'])?$customer_data['billingaddress']['value']:'';
			$address = $address=='' && isset($customer_data['shippingaddress']['value'])?$customer_data['shippingaddress']['value']:'';
			
			$args['counterparty'] = $billingfirstname." ".$billinglastname." ".$billingphone;
			$args['counterparty_shippingaddress'] = $address;
		}			
		return apply_filters( 'usam_order_notification_common_args', $args, $this );
	}

	private function get_payment_table_args() 
	{		
		$rows   = array();
		$headings = array(
			__( 'Номер документа', 'usam' ) => 'left',
			__( 'Дата'      , 'usam' ) => 'right',
			__( 'Способ оплаты'  , 'usam' ) => 'right',
			__( 'Результат оплаты', 'usam' ) => 'right',		
			__( 'Сумма' ,  'usam' ) => 'right',
		);				
		$payment_history = $this->purchase_log->get_payment_history();
		foreach( $payment_history as $item ) 
		{
			$rows[] = array( $item['document_number'], $item['date_time'], $item['name'], $item['status_name'], $item['sum'],);
		}			
		$table_args = array( 'headings' => $headings, 'rows' => $rows );
		return apply_filters( 'usam_order_notification_payment_table_args', $table_args, $this );
	}
	
	private function get_table_args() 
	{
		$order_id = $this->purchase_log->get( 'id' );
		$log_data = $this->purchase_log->get_data();
		$rows   = array();
		
		$headings = array(
			__( 'Имя'       , 'usam' ) => 'left',			
			__( 'Цена'      , 'usam' ) => 'right',
			__( 'Количество', 'usam' ) => 'right',
			__( 'Сумма' ,  'usam' )    => 'right',		
		);
		foreach( $this->purchase_log->get_order_products() as $item ) 
		{
			$item_total = $item->quantity * $item->price;
			$item_total = usam_currency_display( $item_total , array( 'display_as_html' => false ) );
			$item_price = usam_currency_display( $item->price, array( 'display_as_html' => false ) );
			$item_name = apply_filters( 'the_title', $item->name );
			$rows[] = array( 'name' => $item->name, 'price' => $item_price, 'quantity' => $item->quantity, 'totalprice' => $item_total );
		}				
		$table_args = array( 'headings' => $headings, 'rows' => $rows );
		return apply_filters( 'usam_order_notification_product_table_args', $table_args, $this );
	}
	
	private function create_plaintext_payment_list() 
	{
		$table_args = $this->get_payment_table_args();		
		$output = usam_get_plaintext_table( $table_args['headings'], $table_args['rows'] );	
		return $output;
	}

	private function create_plaintext_product_list() 
	{
		$table_args = $this->get_table_args();		
		$output = usam_get_plaintext_table( $table_args['headings'], $table_args['rows'] );
		return $output;
	}
	
	private function create_plaintext_downloadable_links() 
	{
		$output = '';
		$links = usam_get_downloadable_links( $this->purchase_log );
		if ( empty( $links ) )
			return $output;
		
		foreach ( $links as $link ) 
		{
			$output .= $link['product_name'] . "\r\n";			
			$output .= '  - ' . $link['name'] . "\r\n" . '    ' . $link['url'] . "\r\n";
			$output .= "\r\n";
		}
		$output .= "\r\n";		
		return $output;
	}
	
	private function create_html_downloadable_links() 
	{
		$output = '';
		$links = usam_get_downloadable_links( $this->purchase_log );
		if ( empty( $links ) )
			return $output;		
			
		foreach ( $links as $link ) 
		{
			$output .= '<p>';
			$output .= '<em>'.$link['product_name'] . '</em><br />';		
			$output .= '<a href="' . esc_attr( $link['url'] ) . '">' . esc_html( $link['name'] ) . '</a><br />';
			$output .= '</p>';
		}
		return $output;
	}
	
	private function create_html_payment_list() 
	{
		$table_args = $this->get_payment_table_args();		
		$output = usam_get_purchase_log_html_table( $table_args['headings'], $table_args['rows'] );		
		return $output;
	}

	private function create_html_product_list() 
	{
		$table_args = $this->get_table_args();
		$output = usam_get_purchase_log_html_table( $table_args['headings'], $table_args['rows'] );		
		return $output;
	}	

	public function get_plaintext_args() 
	{		
		$plaintext_args = array(
			'product_list'         => $this->create_plaintext_product_list(),
			'downloadable_links'   => $this->create_plaintext_downloadable_links(),
			'payment_list'         => $this->create_plaintext_payment_list(),			
		);
		$plaintext_args = apply_filters( 'usam_order_notification_plaintext_args', $plaintext_args, $this );
		return array_merge( $this->get_common_args(), $plaintext_args );
	}

	public function get_html_args() 
	{
		$common_args = $this->get_common_args();
		$common_args = array_map( 'esc_html', $common_args );		
		$html_args = array(
			'product_list'         => $this->create_html_product_list(),
			'downloadable_links'   => $this->create_html_downloadable_links(),
			'payment_list'         => $this->create_html_payment_list(),				
		);				
		$html_args = apply_filters( 'usam_order_notification_html_args', $html_args, $this );
		return array_merge( $common_args, $html_args );
	}	
	
	public function get_plaintext( $text ) 
	{	
		$shortcode = new USAM_Shortcode();		
		return $shortcode->process_args( $this->get_plaintext_args(), $text );
	}
	
	// из аргументов собрать строку
	public function get_html( $html ) 
	{		
		$shortcode = new USAM_Shortcode();		
		$html = $shortcode->process_args( $this->get_html_args(), $html );
	
		$html = wpautop( $html );			
		return $html;
	}	
}