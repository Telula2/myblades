<?php
// Класс работы с заказами 
class USAM_Orders_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $request;

	private $compat_fields = array( 'results', 'total' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );
		require_once( USAM_FILE_PATH . '/includes/query/meta_query.class.php' );
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}

	/**
	 * Fills in missing query variables with default values.
	 */
	public static function fill_query_vars( $args ) 
	{
		$defaults = array(
			'date_group' => 'insert',
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'monthnum' => '',
			'year' => '',
			'w' => '',
			'paid' => '',			
			'status' => '',
			'status__in' => array(),
			'status__not_in' => array(),
			'payment_gateway' => '',			
			'user_id' => '',			
			'manager_id' => '',
			'manager__in' => array(),
			'manager__not_in' => array(),
			'meta_key' => '',
			'meta_value' => '',
			'meta_compare' => '',
			'include' => array(),
			'exclude' => array(),
			'search' => '',
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => '',
			'number' => '',
			'paged' => 1,
			'count_total' => true,
			'cache_results' => false,	
			'cache_order_documents' => false,				
			'fields' => 'all',		
			'property' => array(),		
			
		);
		return wp_parse_args( $args, $defaults );
	}
	
	public function prepare_query( $query = array() ) 
	{
		global $wpdb;
		
		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_orders', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );
				
		$join = array();	
		if ( is_array( $qv['fields'] ) ) 
		{			
			$qv['fields'] = array_unique( $qv['fields'] );			
			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) 
			{		
				if ( $field == 'all_order' )
					$this->query_fields[] = USAM_TABLE_ORDERS.".*";	
				elseif ( $field == 'average_order' )
					$this->query_fields[] = "ROUND( SUM( ".USAM_TABLE_ORDERS.".totalprice)/COUNT(*),2) AS average_order";
				elseif ( $field == 'count' )
					$this->query_fields[] = "COUNT(*) AS count";
				elseif ( $field == 'user_login' )
					$this->query_fields[] = "( SELECT user_login FROM $wpdb->users WHERE ID = ".USAM_TABLE_ORDERS.".user_id ) AS user_login";
				elseif ( $field == 'sum' )				
					$this->query_fields[] = "SUM(".USAM_TABLE_ORDERS.".totalprice) AS sum";
				elseif ( $field == 'sum_paid' )				
					$this->query_fields[] = " SUM( IF(".USAM_TABLE_ORDERS.".paid=2,".USAM_TABLE_ORDERS.".totalprice,0) ) AS sum_paid";
				elseif ( $field == 'count_paid' )
					$this->query_fields[] = "SUM( IF(".USAM_TABLE_ORDERS.".paid=2, 1, 0) ) AS count_paid";							
				elseif ( $field == 'product_count' )	
				{					
					$this->query_fields[] = "IFNULL( ( SELECT SUM(c.quantity) FROM ".USAM_TABLE_PRODUCTS_ORDER." AS c WHERE c.order_id = ".USAM_TABLE_ORDERS.".id ) ,0) AS product_count";
				}
				else
				{
					$field = 'ID' === $field ? 'ID' : sanitize_key( $field );						
					$this->query_fields[] = USAM_TABLE_ORDERS.".$field";
				}
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 
		elseif ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_ORDERS.".*";		
		else 	
			$this->query_fields = USAM_TABLE_ORDERS.".".$qv['fields'];	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;
		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_ORDERS;
		$this->query_where = "WHERE 1=1";		
		
	// Обрабатывать другие параметры даты
		$date_parameters = array();
		if ( '' !== $qv['hour'] )
			$date_parameters['hour'] = $qv['hour'];

		if ( '' !== $qv['minute'] )
			$date_parameters['minute'] = $qv['minute'];

		if ( '' !== $qv['second'] )
			$date_parameters['second'] = $qv['second'];

		if ( $qv['year'] )
			$date_parameters['year'] = $qv['year'];

		if ( $qv['monthnum'] )
			$date_parameters['monthnum'] = $qv['monthnum'];

		if ( $qv['w'] )
			$date_parameters['week'] = $qv['w'];

		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];

		if ( $qv['date_group'] == 'insert' ) 		
			$qv['date_group'] = USAM_TABLE_ORDERS.'.date_insert';
		elseif ( $qv['date_group'] == 'update' )
			$qv['date_group'] = USAM_TABLE_ORDERS.'.date_modified';
		else
			$qv['date_group'] = USAM_TABLE_ORDERS.'.date_paid';
				
		if ( $date_parameters ) 
		{
			$date_query = new USAM_Date_Query( array( $date_parameters ), $qv['date_group'] );
			$this->query_where .= $date_query->get_sql();
		}
		if ( ! empty( $qv['date_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_query'], $qv['date_group'] );
			$this->query_where .= $this->date_query->get_sql();
		}	
		
		// Обрабатывать статусы
		$status = array();
		if ( !empty( $qv['status'] ) ) 
		{
			if ( is_array( $qv['status'] ) ) 
				$status = $qv['status'];			
			elseif ( is_string( $qv['status'] ) && ! empty( $qv['status'] ) )
				$status = array_map( 'trim', explode( ',', $qv['status'] ) );
		}

		$status__in = array();
		if ( isset( $qv['status__in'] ) ) {
			$status__in = (array) $qv['status__in'];
		}

		$status__not_in = array();
		if ( isset( $qv['status__not_in'] ) ) {
			$status__not_in = (array) $qv['status__not_in'];
		}	
		if ( ! empty( $status ) ) 
		{	
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".status IN ('".implode( "','",  $status )."')";
		}		
		if ( ! empty($status__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".status NOT IN ('".implode( "','",  $status__not_in )."')";
		}
		if ( ! empty( $status__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".status IN ('".implode( "','",  $status__in )."')";
		}	
		$manager = array();
		if ( isset( $qv['manager_id'] ) ) 
		{
			if ( is_array( $qv['manager_id'] ) ) 
				$manager = $qv['manager_id'];			
			elseif ( ! empty($qv['manager_id']) && is_string( $qv['manager_id'] ) )
				$manager = array_map( 'trim', explode( ',', $qv['manager_id'] ) );
			if ( is_numeric( $qv['manager_id'] ) ) 
				$manager = array( $qv['manager_id'] );
		}
		$manager__in = array();
		if ( isset( $qv['manager__in'] ) ) {
			$manager__in = (array) $qv['manager__in'];
		}
		$manager__not_in = array();
		if ( isset( $qv['manager__not_in'] ) ) {
			$manager__not_in = (array) $qv['manager__not_in'];
		}
		if ( ! empty( $manager ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".manager_id IN (".implode( ',',  $manager ).")";
		}		
		if ( ! empty($manager__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".manager_id NOT IN (".implode( ',',  $manager__not_in ).")";
		}
		if ( ! empty( $manager__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".manager_id IN (".implode( ',',  $manager__in ).")";
		}		
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];
			$ordersby_array = array();		
			foreach ( $groupby as $_value ) 
			{				
				switch ( $_value ) 
				{
					case 'day' :
						$ordersby_array[] = 'DAY(date_insert)';		
						$ordersby_array[] = 'MONTH(date_insert)';	
						$ordersby_array[] = 'YEAR(date_insert)';						
					break;
					case 'month' :
						$ordersby_array[] = 'MONTH(date_insert)';	
						$ordersby_array[] = 'YEAR(date_insert)';						
					break;
					case 'year' :
						$ordersby_array[] = 'YEAR(date_insert)';				
					break;
					default:
						$ordersby_array[] = $_value;
				}		
			}	
		}		
		if ( ! empty($ordersby_array) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $ordersby_array );
		else
			$this->query_groupby = '';		
		
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{	// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}	
		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}			
			if ( is_int( $_key ) ) 
			{	// Integer key means this is a flat array of 'orderby' fields.
				$_orderby = $_value;
				$_order = $order;
			} 
			else 
			{ // Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}
			$parsed = $this->parse_orderby( $_orderby );

			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = USAM_TABLE_ORDERS.'.'.$parsed . ' ' . $this->parse_order( $_order );
		}
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = USAM_TABLE_ORDERS.".id $order";		}

		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );
		
		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) 
		{
			if ( $qv['offset'] ) 
			{
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			} 
			else 
			{
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		} 
		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) 
		{
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'product_sku', 'product_name', 'user_login', 'customer_name', 'shippingaddress', 'company', 'billingemail', 'company_email' ) );
		
			if ( ! $search_columns ) 
			{
				if ( false !== strpos( $search, '@') )
					$search_columns = array('billingemail', 'company_email');
				elseif ( is_numeric($search) )
					$search_columns = array('id', 'user_id', 'billingphone', 'billingmobilephone', 'product_sku', 'user_login' );
				else
					$search_columns = array( 'product_sku', 'product_name', 'billingfirstname', 'billinglastname', 'shippingaddress', 'company', 'user_login', 'customer_name' );
			}
			$search_columns = apply_filters( 'usam_orders_search_columns', $search_columns, $search, $this );
			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
			//$this->query_fields = ' DISTINCT '.$this->query_fields;
		}
		if ( ! empty( $qv['include'] ) ) 
			$include = wp_parse_id_list( $qv['include'] );
		else 
			$include = false;
		
		if ( ! empty( $include ) ) 
		{			
			$ids = implode( ',', $include );
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".ID NOT IN ($ids)";
		}					
		if ( ! empty( $qv['type_prices'] ) ) 
		{
			$type_prices = implode( "','",  (array)$qv['type_prices'] );
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".type_price IN ('$type_prices')";				
		}	
		if ( $qv['user_id'] != '' ) 
		{		
			$user_id = implode( ',',  (array)$qv['user_id'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".user_ID IN ($user_id)";
		}	
		if ( !empty($qv['customer_id']) ) 
		{		
			$type_payer_ids = array();
			$types_payers = usam_get_group_payers();
			foreach( $types_payers as $value )
			{						
				if ( $value['type'] == 'I' )
				{
					$type_payer_ids[] = $value['id'];
					break;
				}		
			}
			$type_payer_ids = implode( "','",  $type_payer_ids );
			$customer_id = implode( ',',  (array)$qv['customer_id'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".customer_id IN ($customer_id) AND type_payer IN ('$type_payer_ids')";
		}				
		if ( !empty($qv['companies']) ) 
		{		
			$type_payer_ids = array();
			$types_payers = usam_get_group_payers();
			foreach( $types_payers as $value )
			{						
				if ( $value['type'] == 'E' )
				{
					$type_payer_ids[] = $value['id'];
					break;
				}		
			}
			$type_payer_ids = implode( "','",  $type_payer_ids );	
			$companies = implode( ',',  array_map( 'intval',(array)$qv['companies']) );								
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".customer_id IN ($companies) AND type_payer IN ('$type_payer_ids')";
		}	
		if ( !empty( $qv['contacts'] ) ) 
		{		
			$type_payer_ids = array();
			$types_payers = usam_get_group_payers();
			foreach( $types_payers as $value )
			{						
				if ( $value['type'] == 'I' )
				{
					$type_payer_ids[] = $value['id'];
					break;
				}		
			}
			$type_payer_ids = implode( "','",  $type_payer_ids );	
			$contacts = implode( ',',  array_map( 'intval',(array)$qv['contacts']) );								
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".customer_id IN ($contacts) AND type_payer IN ('$type_payer_ids')";
		}		
		if ( isset( $qv['user_login'] ) && $qv['user_login'] != '' ) 
		{	
			$user = get_user_by('login', $qv['user_login'] );			
			if ( !empty($user->ID) && is_numeric( $user->ID ) )
				$this->query_where .= " AND ".USAM_TABLE_ORDERS.".user_ID = {$user->ID}";
			else
				$this->query_where .= " AND ".USAM_TABLE_ORDERS.".user_ID = '&' ";
		}
		if ( $qv['payment_gateway'] != '' ) 
		{		
			$payment_gateway = implode( ',',  (array)$qv['payment_gateway'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".id IN (SELECT order_id FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE gateway_id IN ($payment_gateway))";
		}
		if ( !empty( $qv['shipping_method'] ) ) 
		{		
			$shipping_method = implode( ',',  (array)$qv['shipping_method'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".id IN (SELECT order_id FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE method IN ($shipping_method))";
		}			
		if ( !empty( $qv['shipping_storage'] ) ) 
		{		
			$shipping_storage = implode( ',',  (array)$qv['shipping_storage'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".id IN (SELECT order_id FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE storage IN ($shipping_storage))";
		}
		if ( !empty( $qv['storage_pickup'] ) ) 
		{		
			$storage_pickup = implode( ',',  (array)$qv['storage_pickup'] );		
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".id IN (SELECT order_id FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE storage IN ($storage_pickup))";
		}			
		if ( $qv['paid'] !== '' ) 
		{
			$qv['paid'] = absint( $qv['paid'] );
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".paid = '".$qv['paid']."'";
		}	
		if ( !empty( $qv['type_payer'] ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_ORDERS.".type_payer = '".$qv['type_payer']."'";
		}		
		if ( !empty($qv['condition']) ) 
		{		
			foreach ( $qv['condition'] as $condition )
			{					
				$select = '';
				switch ( $condition['col'] )
				{
					case 'sum' :
						$select = "totalprice";					
					break;
					case 'prod' :
						$select = "( SELECT COUNT(*) FROM ".USAM_TABLE_PRODUCTS_ORDER." AS c WHERE ".USAM_TABLE_PRODUCTS_ORDER.".order_id = id )";				
					break;
					case 'coupon' :
						$select = "coupon_name";			
					break;	
					case 'coupon_v' :
						$select = "coupon_discount";			
					break;	
					case 'bonus' :
						$select = "bonus";			
					break;	
					case 'taxes' :
						$select = "total_tax";			
					break;						
					case 'discount' :
						$select = "discount";			
					break;		
					case 'shipping' :
						$select = "shipping";			
					break;					
				}
				if ( $select == '' )
					continue;
				
				$compare = "=";	
				switch ( $condition['compare'] ) 
				{
					case '<' :
						$compare = "<";					
					break;
					case '!=' :
						$compare = "!=";					
					break;
					case '=' :
						$compare = "=";					
					break;	
					case '>' :
						$compare = ">";					
					break;				
				}
				$value = $condition['value'];
				
				if ( empty($condition['relation']) )
					$relation = 'AND';
				else
					$relation = $condition['relation'];
				
				$this->query_where .= " $relation ( $select $compare '$value' ) ";
			}			
		} 
		$meta_query = new USAM_Meta_Query( $qv['property'] );
		if ( ! empty( $meta_query->queries ) ) 
		{ 
			$clauses = $meta_query->get_sql( 'order', USAM_TABLE_ORDER_PROPS_VALUE, USAM_TABLE_ORDERS, 'id', $this );
			$this->query_join   .= $clauses['join'];
			$this->query_where  .= $clauses['where'];			
		}
		do_action_ref_array( 'usam_pre_orders_query', array( &$this ) );
	}
	
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;
		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";		

		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] )
		{
			$this->results = $wpdb->get_results( $this->request );
		} 
		elseif ( $qv['number'] == 1 )
		{
			$this->results = $wpdb->get_var( $this->request );
		} 		
		else 
		{
			$this->results = $wpdb->get_col( $this->request );
		}			
		
		if ( !$this->results )
			return;
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total = $wpdb->get_var( 'SELECT FOUND_ROWS()' );		
		
		if ( 'all' == $qv['fields'] || is_array($qv['fields']) && in_array('all_order', $qv['fields']) )
		{			
			if ( $qv['cache_results'] )
			{					
				if ( $qv['number'] == 1 )
					wp_cache_set( $this->results->id, (array)$this->results, 'usam_order' );	
				else
				{					
					foreach ( $this->results as $result ) 
					{
						wp_cache_set( $result->id, (array)$result, 'usam_order' );						
					}
				}		
			}
		}	
		if ( $qv['cache_order_documents'] )
		{
			$ids = array();	
			foreach ( $this->results as $order ) 
			{
				$ids[] = $order->id; 					
			}	
			update_order_cache( $ids );
		}						
	}
	
	public function get( $query_var ) 
	{
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}
	
	public function set( $query_var, $value )
	{
		$this->query_vars[$query_var] = $value;
	}

	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;		
			
		foreach ( $cols as $col )
		{			
			if ( 'id' == $col || 'user_id' == $col ) 
			{
				$searches[] = $wpdb->prepare( USAM_TABLE_ORDERS.".$col = %s", $string );
			} 
			elseif ( 'product_sku' == $col ) 
			{ 
				$product_id = usam_get_product_id_by_sku( $string );		
				if ( !empty($product_id) )
				{
					$searches[] = USAM_TABLE_ORDERS.".id IN ( SELECT DISTINCT order_id FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE product_id ='$product_id' )";	
				}
			}
			elseif ( 'product_name' == $col ) 
			{
				$searches[] = USAM_TABLE_ORDERS.".id IN ( SELECT DISTINCT order_id FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE name LIKE LOWER ('{$like}') )";			
			}
			elseif ( 'user_login' == $col ) 
			{
				$user = get_user_by('login', $string );
				if ( !empty($user->ID) )
					$searches[] = USAM_TABLE_ORDERS."user_ID ={$user->ID}";
			}					
			elseif ( 'billingemail' == $col || 'company_email' == $col || 'billingphone' == $col || 'billingmobilephone' == $col || 'company' == $col || 'billingfirstname' == $col || 'billinglastname' == $col )
			{				
				$table_as = 's'.$col;					
				$this->query_join .=  $wpdb->prepare( " LEFT OUTER JOIN ".USAM_TABLE_ORDER_PROPS_VALUE." AS {$table_as} ON {$table_as}.order_id = ".USAM_TABLE_ORDERS.".id AND {$table_as}.unique_name ='%s'", $col );
				
				$searches[] = $wpdb->prepare($table_as . ".value LIKE LOWER ('%s')", $like );
			}		
			elseif ( 'customer_name' == $col ) 
			{										
				$names = explode(' ', $like);			
				$search_name2 = array();
				$code_name = array('billingfirstname', 'billinglastname' );
				
				foreach ( $code_name as $key => $code )	
				{
					$table_as = "name_{$key}";	
					$this->query_join .=  $wpdb->prepare( " LEFT OUTER JOIN ".USAM_TABLE_ORDER_PROPS_VALUE." AS {$table_as} ON {$table_as}.order_id = ".USAM_TABLE_ORDERS.".id AND {$table_as}.unique_name ='%s'", $code );	
				}
				foreach ( $names as $key => $name )
				{
					$search_name1 = array();
					$table_as = "name_{$key}";											
					foreach ( $code_name as $code )					
					{					
						$search_name1[] = $wpdb->prepare($table_as . ".value LIKE LOWER ('%s')", $name );					
					}					
					$search_name2[] = '(' . implode(' OR ', $search_name1) . ')';					
				}			
				$searches[] = '(' . implode(' AND ', $search_name2) . ')';
			}
			elseif ( 'shippingaddress' == $col )
			{				
				$table_as = 's'.$col;					
				$this->query_join .=  $wpdb->prepare( " LEFT OUTER JOIN ".USAM_TABLE_ORDER_PROPS_VALUE." AS {$table_as} ON {$table_as}.order_id = ".USAM_TABLE_ORDERS.".id AND {$table_as}.unique_name ='%s'", $col );
				
				$searches[] = $wpdb->prepare($table_as . ".value LIKE LOWER ('%s')", $like );
			}	
			else 
			{
				$searches[] = $wpdb->prepare( USAM_TABLE_ORDERS.".$col LIKE %s", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}
	
	public function get_results() 
	{
		return $this->results;
	}

	/**
	 * Return the total number of users for the current query.
	 */
	public function get_total() 
	{
		return $this->total;
	}
	
	public function get_total_amount() 
	{
		global $wpdb;
		
		$request = "SELECT SUM(".USAM_TABLE_ORDERS.".totalprice) AS sum $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit"; 
		$total_amount = $wpdb->get_var( $request );		
		return $total_amount;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';		
		if ( 'ID' == $orderby ) 
		{
			$_orderby = 'id';
		} 
		elseif ( 'totalprice' == $orderby ) 
		{
			$_orderby = sprintf( "CAST( %s )", esc_sql( $orderby ) );
		} 				
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) ) 
		{
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_ORDERS.".id, $include_sql )";
		} 
		else
		{
			$_orderby = $orderby;
		} 
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) 
	{
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments ) {
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

// Кешировать документы заказа
function update_order_cache( $object_ids )
{		
	$column = 'order_id';
	$tables = array( USAM_TABLE_SHIPPED_DOCUMENTS => 'usam_shipped_documents_order', USAM_TABLE_PRODUCTS_ORDER => 'usam_products_order', USAM_TABLE_ORDER_PROPS_VALUE => 'usam_properties_order' );	
	$out = usam_update_cache( $object_ids, $tables, $column );	
	
	global $wpdb;	
	
	if ( !is_array($object_ids) ) 
	{
		$object_ids = preg_replace('|[^0-9,]|', '', $object_ids);
		$object_ids = explode(',', $object_ids);
	}
	$tables = array( USAM_TABLE_PAYMENT_HISTORY => 'usam_payment_order' );
	
	$object_ids = array_map('intval', $object_ids);		
	$column = 'document_id';
	foreach ( $tables as $table => $cache_key ) 
	{					
		$ids = array();
		$cache = array();
		foreach ( $object_ids as $id ) 
		{
			$cached_object = wp_cache_get( $id, $cache_key );
			if ( false === $cached_object )
				$ids[] = $id;
			else
				$cache[$id] = $cached_object;
		}

		if ( empty( $ids ) )
			return $cache;
		
		$id_list = join( ',', $ids );	
		$meta_list = $wpdb->get_results( "SELECT * FROM $table WHERE $column IN ($id_list) AND document_type = 'order' ORDER BY $column ASC" );
	
		if ( !empty($meta_list) ) 
		{		
			foreach ( $meta_list as $metarow) 
			{				
				$cache[$metarow->$column][] = $metarow;
			}
		}				
		foreach ( $ids as $id ) 
		{
			if ( ! isset($cache[$id]) )
			{
				$cache[$id] = array();
			}		
			wp_cache_add( $id, $cache[$id], $cache_key );
		}			
		$out[$cache_key] = $cache;
	}
	return $out;
}

function usam_get_orders( $query )
{	
	$query['count_total'] = false;
	$orders = new USAM_Orders_Query( $query );	
	return $orders->get_results();	
}