<?php
// Получить данные товара из заказа
function usam_get_cart_product( $basket_id, $colum = 'all' ) 
{		
	global $wpdb;
			
	$cart_item = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM `". USAM_TABLE_PRODUCTS_ORDER ."` WHERE `id` = %d", $basket_id) );
	
	if ( $colum == 'all' )
		$result = $cart_item;
	elseif ( isset($cart_item->$colum) )
		$result = $cart_item->$colum;
	else
		$result = false;
	return $result;
}	