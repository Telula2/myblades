<?php
/**
 * Статусы заказа
 */ 

/*
 * Найти имя статуса заказа по его номеру
 */
function usam_get_order_status_name( $internalname ) 
{
	$status = usam_get_order_status( $internalname );	
	return $status['name'];
}

/*
 * Найти описание статуса заказа по его номеру
 */
function usam_get_order_status_description( $internalname ) 
{
	$status = usam_get_order_status( $internalname );	
	return $status['description'];
}

/*
 *  Узнать завершен ли заказ по id статуса
 */
function usam_check_order_is_completed( $internalname ) 
{
	$status = usam_get_order_status( $internalname );	
	return $status['close'];
}


function usam_get_order_status( $internalname )
{
	global $wpdb;
		
	$cache_key = 'usam_order_status';
	if( ! $data = wp_cache_get($cache_key, $internalname ) )
	{		
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_ORDER_STATUS." WHERE internalname ='%s'", $internalname );	
		$data = $wpdb->get_row( $sql, ARRAY_A ); 
		$data = apply_filters( 'usam_order_status_data', $data );	
		wp_cache_set( $cache_key, $data, $internalname );
	}	
	do_action( 'usam_order_status_fetched', $data );	
	return $data;
}


function usam_update_order_status( $internalname, $update )
{
	global $wpdb;
	
	$formats_db = array( 'internalname' => '%s', 'name' => '%s', 'description' => '%s', 'short_name' => '%s', 'sort' => '%d', 'visibility' => '%d', 'close' => '%d', 'color' => '%s');
	$formats = array();
	foreach ( $update as $key => $value ) 
	{					
		if ( isset($formats_db[$key]) )		
			$formats[$key] = $formats_db[$key];	
		else
			unset($update[$key]);
	}	
	$where = array( 'internalname' => $internalname );	
	$result_insert = $wpdb->update( USAM_TABLE_ORDER_STATUS, $update, $where, $formats, array( $formats_db['internalname'] ) );	
	return $result_insert;
}

function usam_insert_order_status( $insert )
{
	global $wpdb;	
	
	if ( empty($insert['internalname']) )
		return false;
	
	$check_availability = usam_get_order_status( $insert['internalname'] );	
	if ( !empty($check_availability) )
		return false;
			
	$formats_db = array( 'internalname' => '%s', 'name' => '%s', 'description' => '%s', 'short_name' => '%s', 'sort' => '%d', 'visibility' => '%d', 'close' => '%d', 'color' => '%s');
	$formats = array();
	foreach ( $insert as $key => $value )
	{					
		if ( isset($formats_db[$key]) )		
			$formats[$key] = $formats_db[$key];
		else
			unset($insert[$key]);		
	}	
	$wpdb->insert( USAM_TABLE_ORDER_STATUS, $insert, $formats );
	return $insert['internalname'];
}

function usam_delete_order_status( $internalname )
{
	global $wpdb;
	
	$sql = $wpdb->prepare( "DELETE FROM " . USAM_TABLE_ORDER_STATUS . " WHERE internalname ='%s'", $internalname );		
	return $wpdb->query( $sql );	
}


function usam_get_order_statuses( $args = null )
{ 
	global $wpdb;
	
	if ( isset($args['fields']) )
	{		
		if ( is_array( $args['fields'] ) )
		{
			$fields = implode(',',$args['fields']);
		}
		else
			$fields = $args['fields'] == 'all'?'*':$args['fields'];
	}
	else
	{
		$args['fields'] = 'all';
		$fields = '*';
	}
	$_where = array('1=1');
	if ( isset($args['visibility']) )
	{
		if ( $args['visibility'] )
			$_where[] = "visibility = '1'";
		else
			$_where[] = "visibility = '0'";
	}
	if ( isset($args['close']) )
	{
		if ( $args['close'] )
			$_where[] = "close = '1'";
		else
			$_where[] = "close = '0'";
	}
	if ( isset($args['internalname']) )		
		$_where[] = "internalname IN( '".implode( "','", $args['internalname'] )."' )";
	
	$where = implode( " AND ", $_where);	
	
	if ( isset($args['orderby']) )
	{
		$orderby = $args['orderby'];
	}
	else
		$orderby = 'sort';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($args['order']) )	
		$order = $args['order'];	
	else
		$order = 'ASC';		
	
	$sql = "SELECT $fields FROM ".USAM_TABLE_ORDER_STATUS." WHERE $where $orderby $order";
	if ( is_array($args['fields']) || 'all' == $args['fields'] )
	{
		$results = $wpdb->get_results( $sql );
	} 		
	else 
	{
		$results = $wpdb->get_col( $sql );
	}		
	if ( $fields == '*' )
	{
		$cache_key = 'usam_order_status';
		foreach ( $results as $status ) 
		{
			wp_cache_set( $cache_key, (array)$status, $status->internalname );
			
		}
	}
	return $results;
}
?>