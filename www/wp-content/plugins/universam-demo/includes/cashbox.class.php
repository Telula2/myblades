<?php
/**
 * Класс оплаты магазина УНИВЕРСАМ *
 * @since 3.7.6
 */

abstract class USAM_Cashbox 
{	
	protected $option = array();
	protected $id = 0;
	protected $api_url = '';
	protected $callback_url = '';		
	protected $error = '';	
		
	function __construct( $id ) 
	{
		$this->callback_url = get_bloginfo( 'url' ).'/cashbox';
		$this->id = $id;	
		$this->option = usam_get_cashbox( $id );
	}
	
	protected function set_error( $error )
	{	
		$this->error = sprintf( __('Приложение вызвало ошибку №%s. Текст ошибки: %s'), $error['code'], $error['text']);
	}

	protected function send_request( $function, $params = array() )
	{		
		$data = wp_remote_post($this->api_url.$function, array('sslverify' => false,'timeout' => 5, 'body' => $params ));
	
		if ( is_wp_error($data) )
		{
			$message = $data->get_error_message();
			$this->set_error( $message );	
			return false;
		}
		$resp = json_decode($data['body'],true);			
		return $resp;		
	}	
	
	
	protected function controller_export_ftp() 
	{ 		
		$output = $this->get_export_data();		
		
		$this->ftp = new USAM_FTP();
		$result	= $this->ftp->ftp_open();		
		if ( !$result )		
			$this->error = $this->ftp->get_errors();		
		else
		{						
			$this->ftp->fwrite_tp_put( $output, $this->filename, $this->option['setting']['ftp_folder'], 'windows-1251' );			
		}	
	}
	
	protected function controller_export_file() 
	{
		$output = $this->get_export_data();
		header( 'Content-Type: text/plain; charset=windows-1251' );
		header( 'Content-Disposition: attachment; filename="'.$this->filename.'"' );			
		echo $output;
		exit;		
	}
	
	protected function get_data_sell( $id ) 
	{		
		return false;
	}
	
	protected function get_send_data( $id, $payment_type ) 
	{		
		return false;
	}
	
	protected function send_sell( $params ) 
	{		
		return false;
	}
	
	//Пробить чек
	public function sell( $order_id, $payment_type = 1 ) 
	{
		$data = $this->get_data_sell( $order_id );		
		$params = $this->get_send_data( $data, $payment_type );				
		return $this->send_sell( $params );
	}
	
	//Пробить возврат чека
	public function sell_refund( $id, $payment_type = 1 ) 
	{
		$data = $this->get_data_refund( $id );		
		$params = $this->get_send_data( $data, $payment_type );
	
		return $this->send_sell_refund( $params );
	}
	
	public function export( $order_ids ) 
	{				
		$this->order_ids = $order_ids;
		$method = 'controller_export_'.$this->option['setting']['variant'];				
		if ( method_exists($this, $method) )
		{
			$html = $this->$method();				
		}
		else
			$this->error = sprintf( __( 'Не верный параметр для вызова функции: %s', 'usam'), $this->option['setting']['variant']);	
	}
	
	public function get_error()
	{
		return $this->error;
	}
}

// Подключить класс кассы
function usam_get_cashbox_class( $id )
{
	$cashbox = usam_get_cashbox( $id );	
	$file =  USAM_FILE_PATH . "/cashbox/".$cashbox['handler'].".php";
	if ( file_exists( $file ) )
	{
		require_once( $file );
		$class = 'USAM_Cashbox_'.$cashbox['handler'];		
		$cashbox = new $class( $id );		
		
		return $cashbox;		
	}	
	return false;
}

function usam_get_cashbox( $id )
{
	$option = get_option('usam_cashbox');
	$cashbox = maybe_unserialize( $option );	
	$result = array();
	foreach ( $cashbox as $value )		
	{
		if ( $value['id'] == $id )
		{
			$result = $value;
			break;
		}
	}
	return $result;
}

function usam_update_cashbox( $update, $id ) 
{	
	$option = get_option('usam_cashbox', array() );						
	$array = maybe_unserialize($option);	
	if ( empty($array) )
		return false;
	
	$result = false;
	foreach ( $array as $key => $data ) 
	{
		if ( $data['id'] == $id )	
		{
			$result = true;
			$array[$key] = array_merge($array[$key], $update );			
			break;
		}
	}
	if ( $result )
		update_option('usam_cashbox', maybe_serialize($array) );
	return $result;
}
?>