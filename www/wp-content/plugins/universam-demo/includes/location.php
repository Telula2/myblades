<?php
// Получить типы местоположений
function usam_get_types_location( $return = 'id' )
{
	$cache_key = 'types_location';
	$object_type = 'usam_system';
	if( ! $cache = wp_cache_get($cache_key, $object_type ) )
	{		
		global $wpdb;
	
		$sql = "SELECT * FROM `".USAM_TABLE_LOCATION_TYPE."` ORDER BY level ASC";	
		$cache = $wpdb->get_results( $sql, 'OBJECT' );	
		wp_cache_set( $cache_key, $cache, $object_type );
	}		
	$out = array();
	if ( $return == 'id' )
		foreach ( $cache as $value )
		{		
			$out[$value->id] = $value;		
		}	
	else
		foreach ( $cache as $value )
		{		
			$out[$value->code] = $value;		
		}	
	return $out;	
}


function usam_get_type_location( $value, $colum = 'id' )
{
	if ( $colum == 'code' )
	{
		$cache_code = wp_cache_get($value, 'usam_type_location_code' );
		if ( $cache_code )
		{ 
			$value =  $cache_code;
			$colum = 'id';
		}	
	}
	$object_type = 'usam_type_location';
	if( $colum == 'code' || ! $cache = wp_cache_get($value, $object_type ) )
	{		
		if ( $colum == 'id' ) 
			$format = '%d';
		else
			$format = '%s';
		
		global $wpdb;	
		$cache = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `".USAM_TABLE_LOCATION_TYPE."` WHERE $colum='$format'", $value ),'ARRAY_A' );	
		if ( !empty($cache) && $colum == 'code' )
		{
			$value = $cache['id'];
			$colum = 'id';
		}		
		if ( $colum == 'id' )
			wp_cache_set( $value, $cache, $object_type );
	}		
	return $cache;	
}

// Получить массив местоположений
function usam_get_array_locations( $level = 0 )
{
	global $wpdb;	
	
	$type_location = usam_get_types_location();
	$locations = $wpdb->get_results("SELECT * FROM ".USAM_TABLE_LOCATION." WHERE parent='$level' ");	
	$out = array();	
	foreach ( $locations as $location )			
	{			
		$result = usam_get_array_locations( $location->id );
		$out[] = array( 'name' => $location->name, 'id_type' => $location->id_type, 'name_type' => $type_location[$location->id_type]->name, 'code_type' => $type_location[$location->id_type]->code,'id' => $location->id, 'level' => $result );
	}
	return $out;
}


// Получить массив местоположений по заданному типу
function usam_get_locations_by_id_type( $type )
{	
	global $wpdb;	
	
	$locations = $wpdb->get_results("SELECT * FROM ".USAM_TABLE_LOCATION." WHERE id_type='$type' ");		
	return $locations;
}

// Получить массив местоположений по заданному коду
function usam_get_locations_by_code( $code, $parent = null )
{	
	global $wpdb;	
	$where = '';
	if ( $parent != null )
		$where = " AND l.parent='{$parent}'";	
	$locations = $wpdb->get_results("SELECT l.* FROM `".USAM_TABLE_LOCATION."` AS l INNER JOIN ".USAM_TABLE_LOCATION_TYPE." AS lt ON (lt.id=l.id_type) WHERE lt.code = '$code' $where ORDER BY l.sort");	
	return $locations;
}

// Получить код типа местоположения
function usam_get_code_by_location_id( $id )
{	
	global $wpdb;	
	
	$code = $wpdb->get_var("SELECT lt.code FROM `".USAM_TABLE_LOCATION."` AS l INNER JOIN ".USAM_TABLE_LOCATION_TYPE." AS lt ON (lt.id=l.id_type) WHERE l.id = '$id' ORDER BY l.sort");			
	return $code;
}

// Получить массив местоположений
function usam_get_array_locations_up( $parent = 0, $field = 'all', $key = '', $level = 0 )
{
	static $out, $type_location;	
	
	if ( $level == 0 )			
		$out = array();	
	
	if ( $key == 'code' && !isset($type_location) )
		$type_location = usam_get_types_location();	
	$level ++;
		
	$locations = usam_get_location( $parent );
	if ( !empty($locations) )
	{		
		if ( $field == 'all' )		
			$values = $locations;		
		elseif ( isset($locations[$field]) )
			$values = $locations[$field];
		else
			return array();	
		
		if ( $key == 'code' )
			$out[$type_location[$locations['id_type']]->code] = $values;
		else
			$out[] = $values;
		
		if ( $locations['parent'] != 0 )						
			$result = usam_get_array_locations_up( $locations['parent'], $field, $key, $level );	
	}	
	return $out;
}

function usam_get_full_locations_name( $location_id ) 
{				
	$str = '';
	$tree_locations = usam_get_array_locations_up( $location_id );		
	$z = '';
	foreach ( $tree_locations as $tree_location ) 
	{			
		$str .= $z.$tree_location['name'];	
		$z = ', ';			
	}	
	return $str;		
}

// Проверить наличие местоположения в заданной ветке местоположений
function usam_seach_location_down( $id, $seach_id )
{		
	global $wpdb;	
	$locations = $wpdb->get_col("SELECT id FROM ".USAM_TABLE_LOCATION." WHERE parent='$id' ");
	$result = false;
	if ( in_array($seach_id, $locations) )			
		$result = true;
	elseif ( !empty($locations) )
		foreach( $locations as $id )
		{
			$result = usam_seach_location_down( $id, $seach_id );		
			if ( $result )
				break;
		}
	return $result;		
}

// Проверить наличие местоположения в заданной ветке местоположений
function usam_seach_location_up( $id, $seach_id )
{		
	global $wpdb;	
	$location = $wpdb->get_var("SELECT parent FROM ".USAM_TABLE_LOCATION." WHERE id='$seach_id' ");
	$result = false;	
	if ( $location == $id )			
		$result = true;
	elseif ( $location != 0 )
		$result = usam_seach_location_up( $id, $location );		
	return $result;		
}

// Получить информацию о местоположений
function usam_get_location( $id )
{
	global $wpdb;

	if ( !is_numeric($id) )
		return array();		

	$object_type = 'usam_location';	
	if( ! $location = wp_cache_get($id, $object_type ) )
	{	
		$location = $wpdb->get_row("SELECT * FROM ".USAM_TABLE_LOCATION." WHERE id='$id' ", ARRAY_A);	
		wp_cache_set( $id, $location, $object_type );
	}		
	if ( !$location )
	{
		$location = array();		
	}	
	return $location;
}


// Выбор одного региона
function usam_get_list_locations( $locations, $select, $recursion = 0 )
{		
	$recursion++;
	$prefix = str_repeat( '&nbsp;&nbsp;&nbsp;' , $recursion );
	$out = '<ul>';
	foreach ( $locations as $location )			
	{
		$active = ($location['id'] == $select)?'active':'';
		$out .= "<li id = 'list_".$location['id']."' data-location = '".$location['id']."'>$prefix";
		
		if ( $location['code_type'] == 'city' )
			$out .= "<a href='".add_query_arg( array( 'location' => $location['id'] ) )."'><label class = 'title $active' for = 'location_".$location['id']."'>".$location['name']."</label></a>";
		else
			$out .= "<label class = 'title $active' for = 'location_".$location['id']."'>".$location['name']."</label>";
		if ( !empty($location['level']) )
		{				
			$sub_location = usam_get_list_locations( $location['level'], $select, $recursion );		
			$out .= "<span id = 'open_".$location['id']."' > + </span>".$sub_location;
		}
		$out .= "</li>";			
	}
	$out .= '</ul>';
	return $out;
}

function usam_get_current_user_location( )
{ 
	$ip = $_SERVER['REMOTE_ADDR'];	
	$location_by_ip = usam_get_location_by_ip( $ip );	
	$current_location = 0;
	if ( !empty($location_by_ip['city']) )
	{
		$query = array( 'search' => $location_by_ip['city'], 'type' => 'city' );	
		$result = usam_get_locations( $query );
		if ( !empty($result[0]) )
		{	
			$current_location = $result[0]->id;			
		}
	}
	return $current_location;
}		

function usam_get_location_by_ip( $ip )
{ 
	if ( !rest_is_ip_address($ip) )
		return false;
	
	$params = array( 'ip' => $ip );	
	$query = http_build_query($params);  
	
	$url = 'http://ipgeobase.ru:7020/geo?'.$query;	
	
	$xml = new DOMDocument();
	
	$result = array();
	
	if ( @$xml->load($url) )
	{		
		$root = $xml->documentElement;
		if ( !empty($root->getElementsByTagName('city')->item(0)->nodeValue) )
			$result = array(
				'country' => $root->getElementsByTagName('country')->item(0)->nodeValue,
				'region'	=> $root->getElementsByTagName('region')->item(0)->nodeValue,
				'city' => $root->getElementsByTagName('city')->item(0)->nodeValue,
				'district' => $root->getElementsByTagName('district')->item(0)->nodeValue
			);		
	}
    return $result;
}

function usam_get_name_sales_area( $area_id )
{	
	if ( empty($area_id) )
		return '';
	
	$option = get_option('usam_sales_area');
	$grouping = maybe_unserialize( $option );
	$name = '';
	foreach( $grouping as $group )
	{				
		if ( $group['id'] == $area_id )
		{
			$name = $group['name'];
			break;
		}
	}
	return $name;
}

// Получить координаты по названию
function usam_get_geocode( $name )
{ 
	$headers["Accept"] = 'application/json';
	$headers["Content-type"] = 'application/json';		
	
	$query = http_build_query(array( 'geocode' => $name ));  
	$data = wp_remote_get( 'https://geocode-maps.yandex.ru/1.x/?'.$query, array('sslverify' => false, 'body' => array( 'geocode' => $name, 'format' => 'json' ), 'headers' => $headers ));	
	if ( is_wp_error($data) )
		return false;
	$resp = json_decode($data['body'], true);	
	if ( isset( $resp['error'] ) ) 
	{		
		return false;
	}		
	if ( isset($resp['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']))
		return explode(' ', $resp['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']);

	return false;
}

// Получить расстояние между координатами
function usam_get_distance_yandex( $origins, $destinations, $mode = 'driving' )
{ 
	$apikey = '';
	
	$headers["Accept"] = 'application/json';
	$headers["Content-type"] = 'application/json';		
	
	$params = array( 'origins' => $origins, 'destinations' => $destinations, 'mode' => $mode, 'apikey' => $apikey );
	
	$query = http_build_query(array( 'geocode' => $name ));  
	$data = wp_remote_get( 'https://api.routing.yandex.net/v1.0.0/?'.$query, array('sslverify' => false, 'body' => $params, 'headers' => $headers ));	
	
	if ( is_wp_error($data) )
		return false;
	
	$resp = json_decode($data['body'], true);	
	if ( isset( $resp['error'] ) ) 
	{		
		return false;
	}	
	return $resp;
}
	
// Получить расстояние между координатами
function usam_get_distance_by_name( $from, $to )
{ 	
	$from = urlencode($from);
	$to = urlencode($to);

	$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=ru-RU&sensor=false");
	$results = json_decode($data, true);
	
	if ( !empty($results['rows'][0]['elements'][0]['distance']['value']) )
		return $results['rows'][0]['elements'][0]['distance']['value'];	
	return false;
}