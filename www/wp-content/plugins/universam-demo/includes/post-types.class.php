<?php
/**
 * Регистрирует таксомании
 * @since 3.8
 */
class USAM_Post_types
{
	function __construct( ) 
	{			
		add_action( 'init', array('USAM_Post_types', 'register_taxonomies'), 1 );		
		
		add_action( 'init', array($this, 'create_new_post_status') );	
		add_filter( 'display_post_states', array($this, 'display_status') );
	}	
	
	/**
	Описание: Создать новые статусы
	 */
	function create_new_post_status()
	{
		register_post_status( 'archive', array(
			'label'                     => __( 'Архив', 'usam' ),
			'public'                    => false, // Показывать ли посты с этим статусом в лицевой части сайта.
			'exclude_from_search'       => false, //Исключить ли посты с этим статусом из результатов поиска.
			'show_in_admin_all_list'    => false,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Архив <span class="count">(%s)</span>', 'Архив <span class="count">(%s)</span>' ),
		) );
	/*	register_post_status( 'to_order', array(
			'label'                     => __( 'Под заказ', 'usam' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => false,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Под заказ <span class="count">(%s)</span>', 'Под заказ <span class="count">(%s)</span>' ),
		) );*/		
	}
	
	function display_status( $states )
	{
		global $post;
		$arg = get_query_var( 'post_status' );
		if($arg != 'archive')
		{
			if($post->post_status == 'archive')
				return array(__( 'Архив', 'usam' ));
		}
		return $states;
	}	

	/**
	 * В этой функции регистрируем наши типы потов
	 */
	public static function register_taxonomies() 
	{
		$permalinks        = get_option( 'usam_permalinks' );
		// Товары
		$labels = array(
			'name'               => __( 'Товары'                    , 'usam' ),
			'singular_name'      => __( 'Товар'                     , 'usam' ),
			'add_new'            => __( 'Добавить товар'            , 'usam' ),
			'add_new_item'       => __( 'Добавить новый товар'      , 'usam' ),
			'edit_item'          => __( 'Изменить товар'            , 'usam' ),
			'new_item'           => __( 'Новый товар'               , 'usam' ),
			'view_item'          => __( 'Посмотреть товар'          , 'usam' ),
			'search_items'       => __( 'Поиск товаров'             , 'usam' ),
			'not_found'          => __( 'Товары не найдены'         , 'usam' ),
			'not_found_in_trash' => __( 'Товары не найдены в корзине', 'usam' ),
			'menu_name'          => __( 'Товары'                     , 'usam' ),
			'parent_item_colon'  => '',
		  );
		$args = array(
			'capability_type'      => 'post',
			'supports'             => array( 'title', 'editor', 'thumbnail' ),
			'hierarchical'         => true, //Будут ли записи этого типа иметь древовидную структуру (как постоянные страницы). true - да, будут древовидными, false - нет, будут связаны тексономией (категориями)
			'exclude_from_search'  => false,
			'public'               => true,
			'show_ui'              => true,
			'show_in_nav_menus'    => true,
			'menu_icon'            => "dashicons-cart",
			'labels'               => $labels,
			'query_var'            => true,
			'register_meta_box_cb' => 'usam_meta_boxes',
			'rewrite'              => array(
				'slug'       => untrailingslashit( empty( $permalinks['product_base'] ) ? '' : $permalinks['product_base'] ),
				'with_front' => false,
				'feeds' => true
			)
		);
		$args = apply_filters( 'usam_register_post_types_products_args', $args );
		register_post_type( 'usam-product', $args );
		
		// Свойства товаров
		$labels = array(
			'name'              => _x( 'Свойства товаров', 'название таксоманий' , 'usam' ),
			'singular_name'     => _x( 'Свойство товара'         , 'название одной таксомании', 'usam' ),
			'search_items'      => __( 'Поиск свойства' , 'usam' ),
			'all_items'         => __( 'Все свойства'    , 'usam' ),
			'parent_item'       => __( 'Группа'  , 'usam' ),
			'parent_item_colon' => __( 'Группа:', 'usam' ),
			'edit_item'         => __( 'Изменить свойство'    , 'usam' ),
			'update_item'       => __( 'Обновить свойство'  , 'usam' ),
			'add_new_item'      => __( 'Добавить новое свойство' , 'usam' ),
			'new_item_name'     => __( 'Имя нового свойства товаров', 'usam' ),
			'not_found'         => __( 'Свойств товаров не найдено', 'usam' ),
		);
		$args = array(
			'hierarchical' => true,
			'query_var'    => 'product_attributes',
			'rewrite'      => false,
			'public'       => true,
			'labels'       => $labels,
			'show_in_quick_edit' => false,			
		);			
		$args = apply_filters( 'usam_register_taxonomies_product_attributes_args', $args );	
		register_taxonomy( 'usam-product_attributes', 'usam-product', $args );
		
		$args = array(
			'hierarchical' => false,
			'query_var'    => 'product_type',
			'rewrite'      => false,
			'public'       => false,			
		);			
		$args = apply_filters( 'usam_register_taxonomies_product_type_args', $args );	
		register_taxonomy( 'usam-product_type', 'usam-product', $args );
		
		// Приобретаемые файлы для товаров
		$args = array(
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'hierarchical'        => false,
			'exclude_from_search' => true,
			'rewrite'             => false,
			'labels'              => array(
				'name'          => __( 'Файлы товара', 'usam' ),
				'singular_name' => __( 'Файл товара' , 'usam' ),
			),
		);
		$args = apply_filters( 'usam_register_post_types_product_files_args', $args );
		register_post_type( 'usam-product-file', $args );		

		// Теги продуктов
		$labels = array(
			'name'          => _x( 'Метки товаров'       , 'название таксоманий' , 'usam' ),
			'singular_name' => _x( 'Метка товаров'       , 'название одной таксомании', 'usam' ),
			'search_items'  => __( 'Поиск меток товаров' , 'usam' ),
			'all_items'     => __( 'Все теги товаров'    , 'usam' ),
			'edit_item'     => __( 'Изменить тег'            , 'usam' ),
			'update_item'   => __( 'Обновить тег'          , 'usam' ),
			'add_new_item'  => __( 'Добавить новый тег товаров' , 'usam' ),
			'new_item_name' => __( 'Имя нового тега товаров', 'usam' ),
		);

		$args = array(
			'hierarchical' => false,
			'labels' => $labels,
			'rewrite' => array(
				'slug' => empty($permalinks['tag_base']) ? 'product-tag' : $permalinks['tag_base'],
				'with_front' => false )
		);
		$args = apply_filters( 'usam_register_taxonomies_product_tag_args', $args );
		register_taxonomy( 'product_tag', 'usam-product', $args );

		// Категории продукции, является иерархическими и можно использовать постоянные
		$labels = array(
			'name'              => _x( 'Категории товаров'       , 'название таксоманий' , 'usam' ),
			'singular_name'     => _x( 'Категория товаров'         , 'название одной таксомании', 'usam' ),
			'search_items'      => __( 'Поиск категории товаров', 'usam' ),
			'all_items'         => __( 'Все категории товаров'   , 'usam' ),
			'parent_item'       => __( 'Параметры категории товаров'  , 'usam' ),
			'parent_item_colon' => __( 'Параметры категории товаров:' , 'usam' ),
			'edit_item'         => __( 'Изменить категорию товаров'    , 'usam' ),
			'update_item'       => __( 'Обновить категорию товаров'  , 'usam' ),
			'add_new_item'      => __( 'Добавить новую категорию товаров' , 'usam' ),
			'new_item_name'     => __( 'Имя новой категории товаров', 'usam' ),
			'menu_name'         => _x( 'Категории', 'название таксомании', 'usam' ),
		);
		$args = array(
			'labels'       => $labels,
			'hierarchical' => true,
			'rewrite'      => array(
				'slug'         => empty($permalinks['category_base']) ? 'product-category' : $permalinks['category_base'],
				'with_front'   => false,
				'hierarchical' => (bool) get_option( 'usam_category_hierarchical_url', 0 ),
			),
		);
		$args = apply_filters( 'usam_register_taxonomies_product_category_args', $args );
		register_taxonomy( 'usam-category', 'usam-product', $args );

		// Вариации товаров
		$labels = array(
			'name'              => _x( 'Вариации'        , 'название таксоманий' , 'usam' ),
			'singular_name'     => _x( 'Вариация'         , 'название одной таксомании', 'usam' ),
			'search_items'      => __( 'Поиск вариации' , 'usam' ),
			'all_items'         => __( 'Все вариации'    , 'usam' ),
			'parent_item'       => __( 'Параметры вариации'  , 'usam' ),
			'parent_item_colon' => __( 'Параметры вариаций:', 'usam' ),
			'edit_item'         => __( 'Изменить вариацию'    , 'usam' ),
			'update_item'       => __( 'Обновить вариацию'  , 'usam' ),
			'add_new_item'      => __( 'Добавить новую вариацию' , 'usam' ),
			'new_item_name'     => __( 'Имя новой вариации', 'usam' ),
			'not_found'         => __( 'Вариаций не найдено', 'usam' ),
		);
		$args = array(
			'hierarchical' => true,
			'query_var'    => 'variations',
			'rewrite'      => false,
			'public'       => true,
			'show_in_quick_edit' => false,	
			'labels'       => $labels
		);
		$args = apply_filters( 'usam_register_taxonomies_product_variation_args', $args );	
		register_taxonomy( 'usam-variation', 'usam-product', $args );		
		
		// Бренды товаров
		$labels = array(
			'name'              => _x( 'Бренды'        , 'taxonomy general name' , 'usam' ),
			'singular_name'     => _x( 'Бренд'         , 'taxonomy singular name', 'usam' ),
			'search_items'      => __( 'Поиск бренда' , 'usam' ),
			'all_items'         => __( 'Все бренды'    , 'usam' ),
			'parent_item'       => __( 'Родитель бренда'  , 'usam' ),
			'parent_item_colon' => __( 'Родители брендов:', 'usam' ),
			'edit_item'         => __( 'Редактирование бренда'    , 'usam' ),
			'update_item'       => __( 'Обновление бренда'  , 'usam' ),
			'add_new_item'      => __( 'Добавить новый бренд' , 'usam' ),
			'new_item_name'     => __( 'Добавить имя бренда', 'usam' ),
			'not_found'         => __( 'Брендов не найдено', 'usam' ),			
		);		
		$args = array(
			'hierarchical' => true,
			'rewrite'      => array(
				'slug'         => empty($permalinks['brand_base']) ? 'brand' : $permalinks['brand_base'],
				'with_front'   => false,
				'hierarchical' => true,
			),	
			'labels'       => $labels
		);		
		$args = apply_filters( 'usam_register_taxonomies_product_brands_args', $args );
		register_taxonomy( 'usam-brands', 'usam-product', $args );
		
		// категории для скидок
		$labels = array(
			'name'              => __( 'Категории скидок', 'usam' ),
			'singular_name'     => __( 'Категория скидок', 'usam' ),
			'search_items'      => __( 'Поиск категории скидок' , 'usam' ),
			'all_items'         => __( 'Все категории скидок'    , 'usam' ),
			'parent_item'       => __( 'Родитель категории скидок'  , 'usam' ),
			'parent_item_colon' => __( 'Родители категории скидок:', 'usam' ),
			'edit_item'         => __( 'Редактирование категории скидок'    , 'usam' ),
			'update_item'       => __( 'Обновление категории скидок'  , 'usam' ),
			'add_new_item'      => __( 'Добавить новую' , 'usam' ),
			'new_item_name'     => __( 'Добавить имя категории скидок', 'usam' ),
			'popular_items'     =>  null,
		);
		$args = array(			
			'hierarchical' => true,	
			'rewrite'      => array(
				'slug'         => empty($permalinks['category_sale_base']) ? 'category_sale' : $permalinks['category_sale_base'],
				'with_front'   => false,
				'hierarchical' => false,
			),					
			'labels'       => $labels
		);			
		register_taxonomy( 'usam-category_sale', 'usam-product', $args );		

		do_action( 'usam_register_post_types_after' );
		do_action( 'usam_register_taxonomies_after' );
	}	
}
new USAM_Post_types();
?>