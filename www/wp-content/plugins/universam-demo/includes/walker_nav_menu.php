<?php
function usam_productspage_nav_class( $classes, $item )
{
	global $usam_query;
    if( (isset($usam_query->query_vars['taxonomy'])) && ($usam_query->query_vars['taxonomy'] == 'usam-category') && ($item->object_id == usam_get_system_page_id('products-list')) )
        $classes[] = 'current_page_item';
    return $classes;
}
add_filter( 'nav_menu_css_class', 'usam_productspage_nav_class', 10, 2 );


require_once( USAM_FILE_PATH . '/includes/taxonomy/class-walker-product_category-menu.php' );

class USAM_Walker_Nav_Menu extends Walker_Nav_Menu 
{	
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) 
	{
		global $wp_query;
		
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;		
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';	
		if ( $item->object_id == usam_get_system_page_id('products-list') )
		{ 
			$current_default   = get_option( 'usam_default_menu_category', 0 );	
			$args_menu = array(
						'descendants_and_self' => 0,
						'selected_cats'        => array( $current_default ),
						'popular_cats'         => false,
						'walker'               => new Walker_Product_Category_Nav_Menu(),
						'taxonomy'             => 'usam-category',				
						'checked_ontop'        => false, 
						'echo'                 => false,
						'before' => '', 
						'after' => '',
						'link_before' => '', 
						'link_after' => '',
					);	
			$default_query = array( "hide_empty" => 0, "hide_empty" => 0, "update_term_meta_cache" => 0 );					
			if ( !empty($current_default) )
				$default_query['child_of'] = $current_default;
			
			$query = $default_query;
			$query['orderby'] = 'meta_value_num';
			$query['meta_key'] = 'usam_sorting_menu_categories';					
			$categories = get_terms( 'usam-category', $query );		
			if ( empty($categories)	)
				$categories = get_terms( 'usam-category', $default_query );	
			
			$map   = "<li class = 'usam_map'><a href='".usam_get_url_system_page('map')."'>".__('Карта категорий','usam')."</a></li>";
			$item_output .= '<ul class ="sub-menu category_menu">'.call_user_func_array( array( $args_menu['walker'], 'walk' ), array( $categories, 0, $args_menu ) ).$map.'</ul>';
		}	
		$item_output .= $args->after;
		$output .= apply_filters( 'usam_walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
?>