<?php
// Класс валют
class USAM_Currency
{
	// строковые
	private static $string_cols = array(
		'name',
		'code',
		'symbol',
		'symbol_html',
		'code',		
	);
	// цифровые
	private static $int_cols = array(
		'numerical'     => '',		
		'display_currency'    => '',		
	);	
	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	public function __construct( $value = null, $col = 'code' ) 
	{
		if ( empty($value) )
		{
			$value = get_option("usam_currency_type");		
		}			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'code', 'numerical' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'numerical'  && $code = wp_cache_get( $value, 'usam_currency_numerical' ) )
		{   // если находится в кэше, вытащить идентификатор
			$col = 'code';
			$value = $code;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'code' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_currency' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}

	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$code = $log->get( 'code' );
		wp_cache_set( $code, $log->data, 'usam_currency' );
		if ( $numerical = $log->get( 'numerical' ) )
			wp_cache_set( $numerical, $code, 'usam_currency_numerical' );	
		do_action( 'usam_currency_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'code' ) 
	{
		$log = new USAM_Currency( $value, $col );
		wp_cache_delete( $log->get( 'code' ), 'usam_currency' );
		wp_cache_delete( $log->get( 'numerical' ), 'usam_currency_numerical' );		
		do_action( 'usam_currency_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $code ) 
	{		
		global  $wpdb;
		do_action( 'usam_currency_before_delete', $code );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_CURRENCY." WHERE code = '$code'");
		self::delete_cache( $code );		
		do_action( 'usam_currency_delete', $code );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_CURRENCY." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_currency_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_currency_fetched', $this );	
		$this->fetched = true;			
	}	

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_currency_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_currency_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_currency_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_currency_pre_save', $this );	
		$where_col = $this->args['col'];			
	
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_currency_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			$this->data = apply_filters( 'usam_currency_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );	
			
			$result = $wpdb->update( USAM_TABLE_CURRENCY, $this->data, array( $where_col => $where_val ), $format, $where_format );		
			do_action( 'usam_currency_update', $this );
		} 
		else 
		{   
			do_action( 'usam_currency_pre_insert' );		
			
			$this->data = apply_filters( 'usam_currency_insert_data', $this->data );
			$format = $this->get_data_format(  );
			$this->data_format( );		
			$result = $wpdb->insert( USAM_TABLE_CURRENCY, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'code', $wpdb->insert_id );
				$this->code = $wpdb->insert_id;
				$this->args = array('col' => 'code',  'value' => $this->code, );				
			}
			do_action( 'usam_currency_insert', $this );
		} 		
		do_action( 'usam_currency_save', $this );

		return $result;
	}
}

// Обновить валюту 
function usam_update_currency( $code, $data, $colum = 'code' )
{
	$currency = new USAM_Currency( $code, $colum );
	$currency->set( $data );
	return $currency->save();
}

// Получить валюту
function usam_get_currency( $code = null, $colum = 'code' )
{
	$currency = new USAM_Currency( $code, $colum );
	return $currency->get_data( );	
}

// Добавить валюту
function usam_insert_currency( $data )
{
	$currency = new USAM_Currency( $data );
	return $currency->save();
}

// Удалить валюту
function usam_delete_currency( $code )
{
	$currency = new USAM_Currency( $code );
	return $currency->delete();
}

function usam_get_currency_sign( $code = null, $colum = 'code' )
{
	$currency = new USAM_Currency( $code, $colum );
	$symbol = $currency->get('symbol');
	$currency_sign = !empty( $symbol ) ? $symbol : $currency->get('code');
	return $currency_sign;
}

function usam_get_currency_name( $code = null, $colum = 'code' )
{
	$currency = new USAM_Currency( $code, $colum );
	return $currency->get( 'code' ).' ('.$currency->get( 'name' ).')';
}

function usam_get_currencies( $args = array(), $output = "OBJECT" )
{		
	$currencies_list = wp_cache_get( 'usam_currencies_list' );
	if( !$currencies_list || $currencies_list && empty($args) ) 
	{	
		global $wpdb;		
		
		$defaults = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all' );
		$args = wp_parse_args( $args, $defaults );
		if ( $args['fields'] == 'all' )
			$fields = " * ";	
		elseif ( is_array($args['fields']) )
			$fields = implode( ', ',$args[ 'fields']);
		else 
			$fields = $args['fields'];
		
		$where = '';
		if ( !empty($args['where']) ) 
		{			
			foreach ( $args['where'] as $column => $value ) 
			{	
				switch( $value['compare'] )
				{							
					case 'in':	
						$compare = " IN ";					
						$condition = "('".implode("','",$value['condition'])."') ";					
					break;
					case 'not in':	
						$compare = " NOT IN ";				
						$condition = "('".implode("','",$value['condition'])."') ";					
					break;
					default:
						$compare = $value['compare'];				
						$condition = $value['condition'];
						$condition = esc_sql( $condition );		
						$condition = ( is_numeric( $condition ) ) ? " {$condition}" : "'{$condition}'";							
					break;
				}				
				$where_query[] = "{$column}{$compare}{$condition}";
			}
		}
		if ( isset( $where_query ) )
			$where = " WHERE ".implode( ' AND ', $where_query );		
		$orderby = isset( $args[ 'orderby' ] ) ? $args[ 'orderby' ] : '';
		$order = isset( $args[ 'order' ] ) ? $args[ 'order' ] : '';		
		
		$query = "SELECT $fields FROM ".USAM_TABLE_CURRENCY." $where ORDER BY $orderby $order";	
		$currencies_list = $wpdb->get_results( $query );	
		if ( !isset( $where_query ) )
		{			
			wp_cache_set( 'usam_currencies_list', $currencies_list );
		}		
	}	
	if ( $output != 'OBJECT' )
	{
		$new_array = array();
		foreach( (array) $currencies_list as $row )
		{
			if ( $output == ARRAY_N ) 			
				$new_array[] = array_values( get_object_vars( $row ) );
			else			
				$new_array[] = get_object_vars( $row );			
		}
		return $new_array;
	}
	return $currencies_list;
}