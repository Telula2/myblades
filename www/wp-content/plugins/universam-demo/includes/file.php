<?php
/**
 * Функции для работы с файлами
 */

/**
 * Копировать папку из $src в $dst
 */
function usam_recursive_copy( $src, $dst ) 
{
	$dir = opendir( $src );
	@mkdir( $dst );
	while ( false !== ( $file = readdir( $dir )) ) 
	{
		if ( ( $file != '.' ) && ( $file != '..' ) ) 
		{
			if ( is_dir( $src . '/' . $file ) )
				usam_recursive_copy( $src . '/' . $file, $dst . '/' . $file );			
			else
				@ copy( $src . '/' . $file, $dst . '/' . $file );			
		}
	}
	closedir( $dir );
}

function usam_remove_dir( $path )
{
	if(file_exists($path) && is_dir($path))
	{
		$dirHandle = opendir($path);
		while(false!==($file = readdir($dirHandle)))
		{
			if($file!='.' && $file!='..')
			{
				$tmpPath = $path.'/'.$file;
				chmod($tmpPath, 0777);
				if( is_dir( $tmpPath ) )				
					usam_remove_dir($tmpPath);
				else 
					unlink($tmpPath);
			}
		}
		closedir($dirHandle);
		rmdir($path);
	} 	
}

//получает файлы в директории $dirname
function usam_list_dir( $dirname ) 
{		
	$dirlist = array();
	if ($dir = opendir($dirname)) 
	{		
		$num = 0;
		while ( ($file = readdir( $dir )) !== false ) 
		{	
			if ( ($file != "..") && ($file != ".") && !stristr( $file, "~" ) && !stristr( $file, "Chekcout" ) && !stristr( $file, "error_log" ) && !( strpos( $file, "." ) === 0 ) )
			{
				$dirlist[$num] = $file;
				$num++;
			}
		}
	}
	return $dirlist;
}

function usam_readfile_chunked( $filename, $retbytes = true ) 
{
	$chunksize = 1 * (1024 * 1024); // how many bytes per chunk
	$buffer = '';
	$cnt = 0;
	$handle = fopen( $filename, 'rb' );
	if ( $handle === false ) {
		return false;
	}
	while ( !feof( $handle ) ) 
	{
		$buffer = fread( $handle, $chunksize );
		echo $buffer;
		ob_flush();
		flush();
		if ( $retbytes ) {
			$cnt += strlen( $buffer );
		}
	}
	$status = fclose( $handle );
	if ( $retbytes && $status ) {
		return $cnt; // return num. bytes delivered like readfile() does.
	}
	return $status;
}

//Получает расширение файла.
function usam_get_extension( $str ) 
{	
	if ( stripos ($str, '.') === false ) 
		return '';
	$parts = explode( '.', $str );
	return end( $parts );
}

function usam_check_upload_file( $filename ) 
{ 
	$allowed = array('png', 'jpg', 'gif','zip','pdf','doc','docx','xls','xlsx','odc','csv','txt','mp3','avi','mpeg','mpg','mkv');
	$extension = pathinfo($filename, PATHINFO_EXTENSION);		 
	if(in_array(strtolower($extension), $allowed))	
		return true;	
	return false;
}

function usam_fileupload( $file, $directory, $max_filesize = 20000000 )
{ 			
	$results = array( 'status' => 'error', 'error_message' => __('Неизвестная ошибка','usam') );	
	if ( isset($file['tmp_name']) )
	{
		$file_name = sanitize_file_name($file['name']);
		$filesize = filesize($file['tmp_name']);
		if ( $filesize < $max_filesize )
		{			
			if( usam_check_upload_file($file['name']) )
			{	 				
				$file_path = $directory.'/'.$file['name'];
				if ( !is_dir($directory) )
					mkdir($directory, 0775);
		
				if( move_uploaded_file($file['tmp_name'], $file_path))
				{
					$icon = usam_get_icon_by_file_extension( $file_path );
					$results = array( 'status' => 'success', 'icon' => $icon, 'file_title' => $file['name'], 'file_name' => $file_name );				
				}
			}
			else
				$results['error_message'] = __('Запрещенный файл','usam');
		}
		else
			$results['error_message'] = __('Большой файл','usam');
	}	
	return $results;
}

function usam_get_filename( $filename, $directory ) 
{ 
	$i = 1;
	$file_path = $directory.$filename;
	if ( file_exists($file_path)) 
	{
		do 
		{		
			$filename = $filename."_$i";
			$file_path = $directory.$filename;
			if ( !file_exists($file_path)) 
				break;						
			$i++;
		} 
		while ( true );  
	}	
	return $file_path;
}				

function usam_get_icon_by_file_extension( $filename ) 
{ 
	$file_ext = usam_get_extension( $filename );
	$mimes = get_allowed_mime_types();	
	foreach ($mimes as $type => $mime) 
	{
		if (strpos($type, $file_ext) !== false)
		{  
			return wp_mime_type_icon( $mime );
		}
	}
	return wp_mime_type_icon( 'default' );
}

function usam_force_download_file( $file_id )
{
	$file_data = get_post( $file_id );
	if ( ! $file_data )
		wp_die( __( 'Неверный идентификатор файла.', 'usam' ) );

	$file_name = basename( $file_data->post_title );
	$file_path = USAM_FILE_DIR . $file_name;

	if ( is_file( $file_path ) )
	{
		if( !ini_get('safe_mode') ) set_time_limit(0);
		header( 'Content-Type: ' . $file_data->post_mime_type );
		header( 'Content-Length: ' . filesize( $file_path ) );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Content-Disposition: attachment; filename="' . stripslashes( $file_name ) . '"' );
		if ( isset( $_SERVER["HTTPS"] ) && ($_SERVER["HTTPS"] != '') ) 
		{
			/* Существует ошибка в том, как IE обрабатывает загрузку с серверов, использующих протокол HTTPS, это поможет исправить
			   session_cache_limiter('public');
			   session_cache_expire(30);
			   В начале вашего файла index.php или до начала сеанса
			*/
			header( "Pragma: public" );
			header( "Expires: 0" );
			header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
			header( "Cache-Control: public" );
		} 
		else
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );		
		header( "Pragma: public" );
        header( "Expires: 0" );
		// уничтожить сессию, чтобы файл был загружен на некоторые глючные браузеры и веб-серверов
		session_destroy();
		usam_readfile_chunked( $file_path );
		exit();
	}
	else
		wp_die(__('К сожалению что-то пошло не так с вашей загрузкой!', 'usam'));
}