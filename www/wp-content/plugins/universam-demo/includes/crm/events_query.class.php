<?php
// Класс работы с делами менеджеров
class USAM_Tasks_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $meta_query = false;
	public $object_query = false;	
	public $request;

	private $compat_fields = array( 'results' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );		
	//	require_once( USAM_FILE_PATH . '/includes/query/meta_query.class.php' );
		require_once( USAM_FILE_PATH . '/includes/query/objects_query.class.php' );
		
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}

	/**
	 * Fills in missing query variables with default values.
	 */
	public static function fill_query_vars( $args ) 
	{			
		$defaults = array(
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'column' => '',			
			'monthnum' => '',
			'year' => '',
			'w' => '',	
			'id'  => '',
			'user_id' => '',
			'calendar'  => '',			
			'date_insert' => '',	
			'start'  => '',			
			'end' => '',		
			'date_time' => '',				
			'title' => '', 		
		//	'status__in' => array(),
		//	'status__not_in' => array(),						
			'include' => array(),
			'exclude' => array(),
			'search' => '',		
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => '',
			'number' => '',
			'paged' => 1,
			'count_total' => true,	
			'cache_objects' => false,	
			'cache_results' => false,				
			'fields' => 'all',			
		);		
		return wp_parse_args( $args, $defaults );
	}

	/**
	 * Prepare the query variables.
	 *
	 * @since 3.1.0
	 * @since 4.1.0 Added the ability to order by the `include` value.
	 * @since 4.2.0 Added 'meta_value_num' support for `$orderby` parameter. Added multi-dimensional array syntax
	 *              for `$orderby` parameter.
	 * @since 4.3.0 Added 'has_published_posts' parameter.
	 * @since 4.4.0 Added 'paged', 'status__in', and 'status__not_in' parameters. The 'status' parameter was updated to
	 *              permit an array or comma-separated list of values. The 'number' parameter was updated to support
	 *              querying for all users with using -1.
	 *
	 * @param string|array $query {
	 *     Optional. Array or string of Query parameters.	
	 *     @type string|array $status                An array or a comma-separated list of status names that users must match
	 *                                             to be included in results. Note that this is an inclusive list: users
	 *                                             must match *each* status. Default empty.
	 *     @type array        $status__in            An array of status names. Matched users must have at least one of these
	 *                                             processes. Default empty array.
	 *     @type array        $status__not_in        
	 *                                             processes will not be included in results. Default empty array.
	 *     @type string       $meta_key            User meta key. Default empty.
	 *     @type string       $meta_value          User meta value. Default empty.
	 *     @type string       $meta_compare        Comparison operator to test the `$meta_value`. Accepts '=', '!=',
	 *                                             '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN',
	 *                                             'BETWEEN', 'NOT BETWEEN', 'EXISTS', 'NOT EXISTS', 'REGEXP',
	 *                                             'NOT REGEXP', or 'RLIKE'. Default '='.
	 *     @type array        $include             An array of user IDs to include. Default empty array.
	 *     @type array        $exclude             An array of user IDs to exclude. Default empty array.
	 *     @type string       $search              Search keyword. Searches for possible string matches on columns.
	 *                                             When `$search_columns` is left empty, it tries to determine which
	 *                                             column to search in based on search string. Default empty.
	 *     @type array        $search_columns      Array of column names to be searched. Accepts 'ID', 'login',
	 *                                             'nicename', 'email', 'url'. Default empty array.
	 *     @type string|array $orderby             Field(s) to sort the retrieved users by. May be a single value,
	 *                                             an array of values, or a multi-dimensional array with fields as
	 *                                             keys and orders ('ASC' or 'DESC') as values. Accepted values are
	 *                                             'ID', 'display_name' (or 'name'), 'include', 'user_login'
	 *                                             (or 'login'), 'user_nicename' (or 'nicename'), 'user_email'
	 *                                             (or 'email'), 'user_url' (or 'url'), 'user_registered'
	 *                                             or 'registered'), 'post_count', 'meta_value', 'meta_value_num',
	 *                                             the value of `$meta_key`, or an array key of `$meta_query`. To use
	 *                                             'meta_value' or 'meta_value_num', `$meta_key` must be also be
	 *                                             defined. Default 'user_login'.
	 *     @type string       $order               Designates ascending or descending order of users. Order values
	 *                                             passed as part of an `$orderby` array take precedence over this
	 *                                             parameter. Accepts 'ASC', 'DESC'. Default 'ASC'.
	 *     @type int          $offset              Number of users to offset in retrieved results. Can be used in
	 *                                             conjunction with pagination. Default 0.
	 *     @type int          $number              Number of users to limit the query for. Can be used in
	 *                                             conjunction with pagination. Value -1 (all) is supported, but
	 *                                             should be used with caution on larger sites.
	 *                                             Default empty (all users).
	 *     @type int          $paged               When used with number, defines the page of results to return.
	 *                                             Default 1.
	 *     @type bool         $count_total         Whether to count the total number of users found. If pagination
	 *                                             is not needed, setting this to false can improve performance.
	 *                                             Default true.
	 *     @type string|array $fields              Which fields to return. Single or all fields (string), or array
	 *                                             of fields. Accepts 'ID', 'display_name', 'user_login',
	 *                                             'user_nicename', 'user_email', 'user_url', 'user_registered'.
	 *                                             Use 'all' for all fields and 'all_with_meta' to include
	 *                                             meta fields. Default 'all'.
	 */
	public function prepare_query( $query = array() ) 
	{
		global $wpdb;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_affairs', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );		
		
		$join = array();	

		if ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_EVENTS.".*";
		else
		{
			if ( !is_array( $qv['fields'] ) )				
				$fields = array( $qv['fields'] );
			else
				$fields = $qv['fields'];
			
			$fields = array_unique( $fields );

			$this->query_fields = array();
			foreach ( $fields as $field ) 
			{	
				if ( $field == 'count' )
					$this->query_fields[] = "COUNT(*) AS count";				
				else
				{
					$field = 'ID' === $field ? 'ID' : sanitize_key( $field );
					$this->query_fields[] = USAM_TABLE_EVENTS.".$field";
				}
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;
		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_EVENTS;
		$this->query_where = "WHERE 1=1";		
		
	// Обрабатывать другие параметры даты
		$date_parameters = array();
		if ( '' !== $qv['hour'] )
			$date_parameters['hour'] = $qv['hour'];

		if ( '' !== $qv['minute'] )
			$date_parameters['minute'] = $qv['minute'];

		if ( '' !== $qv['second'] )
			$date_parameters['second'] = $qv['second'];

		if ( $qv['year'] )
			$date_parameters['year'] = $qv['year'];

		if ( $qv['monthnum'] )
			$date_parameters['monthnum'] = $qv['monthnum'];

		if ( $qv['w'] )
			$date_parameters['week'] = $qv['w'];

		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];
		
		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];
		
		if ( $qv['column'] )
			$date_parameters['column'] = $qv['column'];
		
		if ( $date_parameters ) 
		{
			$date_query = new USAM_Date_Query( array( $date_parameters ), USAM_TABLE_EVENTS.'.date_insert' );
			$this->query_where .= $date_query->get_sql();
		}	
		if ( ! empty( $qv['date_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_query'], USAM_TABLE_EVENTS.'.date_insert' );
			$this->query_where .= $this->date_query->get_sql();
		}	
		// Parse and sanitize 'include', for use by 'orderby' as well as 'include' below.
		if ( ! empty( $qv['include'] ) )
			$include = wp_parse_id_list( $qv['include'] );
		else
			$include = false;
			
		$status = array();
		if ( isset( $qv['status'] ) && $qv['status'] != 'all' ) 
		{
			if ( is_array( $qv['status'] ) ) 
				$status = $qv['status'];			
			elseif ( is_string( $qv['status'] ) && ! empty( $qv['status'] ) )
				$status = array_map( 'trim', explode( ',', $qv['status'] ) );
			elseif ( is_numeric( $qv['status'] ) )
				$status = array( $qv['status'] );
		}
		$status__in = array();
		if ( isset( $qv['status__in'] ) ) {
			$status__in = (array) $qv['status__in'];
		}

		$status__not_in = array();
		if ( isset( $qv['status__not_in'] ) ) {
			$status__not_in = (array) $qv['status__not_in'];
		}

		if ( ! empty( $status ) ) 
		{ 
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".status IN (".implode( "','",  $status ).")";		
		}		
		if ( ! empty($status__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".status NOT IN ('".implode( "','",  $status__not_in )."')";
		}
		if ( ! empty( $status__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".status IN ('".implode( "','",  $status__in )."')";
		}			
			
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];			
			$ordersby_array = array();		
			foreach ( $groupby as $_value ) 
			{				
				switch ( $_value ) 
				{
					case 'day' :
						$ordersby_array[] = 'DAY(date_time)';						
					break;
					case 'month' :
						$ordersby_array[] = 'MONTH(date_time)';						
					break;
					case 'year' :
						$ordersby_array[] = 'YEAR(date_time)';				
					break;					
					default:
						$ordersby_array[] = $_value;
				}		
			}	
		}		
		if ( ! empty($ordersby_array) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $ordersby_array );
		else
			$this->query_groupby = '';		
		
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{
			// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}

		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}		
			if ( 'length_event' == $_value ) 
			{
				$orderby_array[] = "UNIX_TIMESTAMP(".USAM_TABLE_EVENTS.'.end) - UNIX_TIMESTAMP('. USAM_TABLE_EVENTS.'.start) ' . $this->parse_order( $_value );	
				continue;				
			} 			
			if ( is_int( $_key ) ) 
			{	
				$_orderby = $_value;
				$_order = $order;
			} 
			else 
			{	// Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}
			$parsed = $this->parse_orderby( $_orderby );
			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = USAM_TABLE_EVENTS.'.'.$parsed . ' ' . $this->parse_order( $_order );
		}

		// If no valid clauses were found, order by id.
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = USAM_TABLE_EVENTS.".id $order";
		}
		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );

		// limit
		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) {
			if ( $qv['offset'] ) {
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			} else {
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		}

		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) 
		{
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');
			$search_columns = array();
			if ( $qv['search_columns'] )			
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'user_id', 'calendar', 'title' ) );
			if ( ! $search_columns ) 
			{
				if ( is_numeric($search) )
					$search_columns = array( 'id', 'user_id', 'calendar' );				
				else
					$search_columns = array( 'title'  );
			}	
			$search_columns = apply_filters( 'usam_affairs_search_columns', $search_columns, $search, $this );

			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}

		if ( ! empty( $include ) ) 
		{	// Sanitized earlier.
			$ids = implode( ',', $include );
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".ID NOT IN ($ids)";
		}		
				
		if ( ! empty( $qv['id'] ) ) 
		{
			$id = implode( ',',  (array)$qv['id'] );
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".id IN ($id)";
		}		
		
		if ( ! empty( $qv['user_id'] ) ) 
		{
			$user_ids = is_array($qv['user_id']) ? $qv['user_id']: array( $qv['user_id'] );		
			$user_ids = array_map('intval', $user_ids);					
			
			$this->query_where .= " AND (".USAM_TABLE_EVENTS.".user_id IN ('".implode( "', '", $user_ids )."') OR id IN (SELECT event_id FROM ".USAM_TABLE_EVENT_USERS." WHERE user_id IN ('".implode( "', '", $user_ids )."') ) )";
		}			
		if ( ! empty( $qv['calendar'] ) ) 
		{
			$calendar = is_array($qv['calendar']) ? $qv['calendar']: array( $qv['calendar'] );		
			$calendar = array_map('intval', $calendar);
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".calendar IN ('".implode( "', '", $calendar )."')";
		}			
		if ( ! empty( $qv['manager_id'] ) ) 
		{
			$manager_id = (int)$qv['manager_id'];
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".manager_id='$manager_id'";
		}	
		if ( !empty( $qv['type'] ) ) 
		{ 
			$type = is_array($qv['type']) ? $qv['type']: array( $qv['type'] );	
			$this->query_where .= " AND ".USAM_TABLE_EVENTS.".type IN ('".implode( "', '", $type )."')";
		}		
		if ( !empty($qv['object_type']) && !empty($qv['object_id']) ) 
		{
			$object_id = is_array($qv['object_id']) ? $qv['object_id']: array( $qv['object_id'] );		
			$object_id = array_map('intval', $object_id);			
			$qv['object_query'][] = array( 'object_type' => $qv['object_type'], 'object_id' => $object_id, 'field' => 'slug' );		
		}		
		if ( ! empty( $qv['object_query'] ) ) 
		{
			$this->object_query = new USAM_Object_Query( $qv['object_query'] );		
			$clauses = $this->object_query->get_sql( USAM_TABLE_EVENTS, USAM_TABLE_EVENT_RELATIONSHIPS, 'id', 'event_id' );
			$this->query_join .= $clauses['join'];
			$this->query_where .= $clauses['where'];
		}			
		do_action_ref_array( 'usam_pre_affairs_query', array( &$this ) );
	}

	/**
	 * Execute the query, with the current variables.	
	 */
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;

		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";

		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] && $qv['number'] != 1 ) 		
			$this->results = $wpdb->get_results( $this->request );		
		elseif ( $qv['number'] == 1 && 'all' !== $qv['fields'] )		
			$this->results = $wpdb->get_var( $this->request );
		elseif ( $qv['number'] == 1 && 'all' == $qv['fields'] )		
			$this->results = $wpdb->get_row( $this->request );
		else 
			$this->results = $wpdb->get_col( $this->request );
			
		if ( !$this->results )
			return;	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total = $wpdb->get_var( apply_filters( 'found_tasks_query', 'SELECT FOUND_ROWS()' ) );	
		
		if ( 'all' == $qv['fields'] && $qv['cache_results'] )
		{
			if ( $qv['number'] == 1 )
				wp_cache_set( $this->results->id, (array)$this->results, 'usam_event' );	
			else
			{					
				foreach ( $this->results as $result ) 
				{
					wp_cache_set( $result->id, (array)$result, 'usam_event' );						
				}
			}	
		}		
		if ( $qv['cache_objects'] )
		{
			$ids = array();	
			foreach ( $this->results as $result ) 
			{
				$ids[] = $result->id; 					
			}	
			$tables = array( USAM_TABLE_EVENT_RELATIONSHIPS => 'usam_event_relationships' );
			usam_update_cache( $object_ids, $tables, 'object_id' );	
		}
	}

	/**
	 * Retrieve query variable.
	 */
	public function get( $query_var ) 
	{
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}

	/**
	 * Set query variable.
	 */
	public function set( $query_var, $value ) {
		$this->query_vars[$query_var] = $value;
	}

	/**
	 * Used internally to generate an SQL string for searching across multiple columns
	 */
	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;

		foreach ( $cols as $col ) {
			if ( 'ID' == $col ) {
				$searches[] = $wpdb->prepare( "$col = %s", $string );
			} else {
				$searches[] = $wpdb->prepare( "$col LIKE %s", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}

	public function get_results() 
	{
		return $this->results;
	}

	public function get_total() 
	{
		return $this->total;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';

		if ( in_array( $orderby, array( 'user_id', 'calendar', 'title' , 'date_insert', 'start', 'end', 'date_time', 'reminder', 'importance', 'object_type' ) ) )
		{
			$_orderby = $orderby;
		} 					
		elseif ( 'ID' == $orderby || 'id' == $orderby ) 
		{
			$_orderby = 'id';
		} 		
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) )
		{
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_EVENTS.".id, $include_sql )";
		} 		
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) 
	{
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments )
	{
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

function usam_get_tasks( $args = array() )
{
	$args['count_total'] = false;	
	$tasks = new USAM_Tasks_Query( $args );	
	$result = $tasks->get_results();	
	
	return $result;
}