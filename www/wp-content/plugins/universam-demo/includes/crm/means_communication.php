<?php
function usam_get_communication( $id )
{	
	global $wpdb;
	return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE id = '%d'", $id ), ARRAY_A );		
}

function usam_insert_communication( $data ) 
{		
	global $wpdb;
	
	/*if ( empty($data['contact_id']) )
		return false;

	if ( empty($data['customer_type']) )
		return false;	
	
	$communication = usam_get_customer_communication($data['contact_id'], $data['customer_type']);	
		*/
	$formats = array( 'type'  => '%s', 'value_type' => '%s', 'value'  => '%s', 'contact_id' => '%d', 'customer_type' => '%s' );	
	$format = array();
	
	do_action( 'usam_contact_means_communication_pre_insert', $data );
	$data = apply_filters( 'usam_insert_means_communication_set_properties', $data );		
	$select = array();
	foreach ( $data as $key => &$value ) 
	{					
		if ( !empty($formats[$key]) )		
		{
			$format[$key] = $formats[$key];	
			$select[] = "$key='".$formats[$key]."'";
		}
		else
			unset( $data[$key] );		
	}		
	if ( count($data) != 5 )		
		return false;	
		
	$data['value'] = wp_unslash( $data['value'] );
	$data['value'] = trim($data['value']);
	
	if ( $data['type'] == 'phone' )		
		$data['value'] = preg_replace('/[^0-9]/', '', $data['value']);
	elseif ( $data['type'] == 'email' && !is_email( $data['value'] ) )	
		return false;	
	
	if ( $data['value'] == '' )
		return false;
	
	$id = $wpdb->get_var( $wpdb->prepare("SELECT id FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE ".implode(' AND ',$select), $data ) );	
	if ( empty($id) )	
	{
		$result = $wpdb->insert( USAM_TABLE_MEANS_COMMUNICATION, $data, $format );	
		$id = $wpdb->insert_id;
	}	
	do_action( 'usam_means_communication_insert', $id );	
	return $id;
}

function usam_update_communication( $id, $data ) 
{		
	global $wpdb;
		
	$data = apply_filters( 'usam_update_means_communication_set_properties', $data );		
	
	$where = array( 'id' => $id );
	
	if ( !empty($data['type']) )		
	{
		if ( $data['type'] == 'phone' )		
			$data['value'] = preg_replace('/[^0-9]/', '', $data['value']);
		elseif ( $data['type'] == 'email' && !is_email( $data['value'] ) )	
			return false;	
	}
	$formats = array( 'value_type' => '%s', 'value'  => '%s' );	
	$format = array();
	foreach ( $data as $key => $value ) 
	{				
		if ( isset($formats[$key]) )		
			$format[$key] = $formats[$key];	
		else
			unset( $data[$key] );
	}		
	$data['value'] = wp_unslash( $data['value'] );
	$data['value'] = trim($data['value']);	
	
	if ( $data['value'] == '' )
		return false;
	
	$result = $wpdb->update( USAM_TABLE_MEANS_COMMUNICATION, $data, $where, $format, array( '%d' ) );
	return $result;
}

function usam_get_communication_by_type( $args )
{	
	global $wpdb;
	
	$where = array('1=1');
	if ( !empty($args['type']))
		$where[] = "type = '".$wpdb->esc_like($args['type'])."'";
	
	if ( !empty($args['value']))
		$where[]= "value = '".$wpdb->esc_like($args['value'])."'";
	
	if ( !empty($args['customer_type']))
		$where[] = "customer_type = '".$wpdb->esc_like($args['customer_type'])."'";
	
	if ( !empty($args['id']))
		$where[] = "id = '".$wpdb->esc_like($args['id'])."'";
	
	$str_where = implode(' AND ', $where);	
	return $wpdb->get_row( "SELECT * FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE ".$str_where  );		
}

function usam_get_communication_by_email( $email, $customer_type )
{	
	return usam_get_communication_by_type( array('value' => $email, 'type' => 'email', 'customer_type' => $customer_type) );	
}

function usam_get_communication_by_phone( $phone, $customer_type )
{	
	return usam_get_communication_by_type( array('value' => $phone, 'type' => 'phone', 'customer_type' => $customer_type) );
}

function usam_get_customer_communication( $id, $customer_type )
{	
	global $wpdb;
	
	$cache_key = 'usam_'.$customer_type.'_means_communication';
	$communication = wp_cache_get( $id, $cache_key );
	if( $communication === false )
	{	
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE contact_id = '%d' AND customer_type='%s'", $id, $customer_type );
		$communication = $wpdb->get_results( $sql );
	
		wp_cache_set( $cache_key, $communication, $id );
	}
	$result['email'] = array();
	$result['phone'] = array();
	$result['site'] = array();
	$result['social'] = array();
	foreach ( $communication as $value ) 
	{				
		$result[$value->type][$value->id] = array( 'value' => $value->value, 'value_type' => $value->value_type );
	}
	return $result;
}


function usam_delete_means_communication( $data )
{	
	global $wpdb;
	
	$formats = array( 'id'  => '%d', 'type'  => '%s', 'value_type' => '%s', 'value'  => '%s', 'contact_id' => '%d', 'customer_type' => '%s' );	
	$format = array();
	foreach ( $data as $key => $value ) 
	{				
		if ( isset($formats[$key]) )		
			$format[$key] = $formats[$key];	
		else
			unset( $data[$key] );		
	}
	
	$result = $wpdb->delete( USAM_TABLE_MEANS_COMMUNICATION, $data, $format );
	return $result;
}

function usam_update_communication_cache( $object_ids, $customer_type )
{	
	global $wpdb;	
	
	$tables = array( USAM_TABLE_MEANS_COMMUNICATION => 'usam_'.$customer_type.'_means_communication' );
	$column = 'contact_id';
	
	if ( !is_array($object_ids) ) 
	{
		$object_ids = preg_replace('|[^0-9,]|', '', $object_ids);
		$object_ids = explode(',', $object_ids);
	}
	$object_ids = array_map('intval', $object_ids);		
	
	$out = array();
	foreach ( $tables as $table => $cache_key ) 
	{					
		$ids = array();
		$cache = array();
		foreach ( $object_ids as $id ) 
		{
			$cached_object = wp_cache_get( $id, $cache_key );
			if ( false === $cached_object )
				$ids[] = $id;
			else
				$cache[$id] = $cached_object;
		}

		if ( empty( $ids ) )
			return $cache;
		
		$id_list = join( ',', $ids );	
		$meta_list = $wpdb->get_results( "SELECT * FROM $table WHERE $column IN ($id_list) AND customer_type = '$customer_type' ORDER BY $column ASC" );	
		if ( !empty($meta_list) ) 
		{		
			foreach ( $meta_list as $metarow) 
			{				
				$cache[$metarow->$column][] = $metarow;
			}
		}				
		foreach ( $ids as $id ) 
		{
			if ( ! isset($cache[$id]) )
			{
				$cache[$id] = array();
			}		
			wp_cache_add( $id, $cache[$id], $cache_key );
		}			
		$out[$cache_key] = $cache;
	}
	return $out;
}