<?php
class USAM_Document
{		
	private static $string_cols = array(	 // строковые		
		'number',	
		'date_insert',
		'name',
		'type_price',
		'closedate',
		'description',
		'conditions',
		'notes',		
		'status',	
		'customer_type',
		'type',		
	);	
	private static $int_cols = array( // цифровые
		'id',			
		'contact_id',		
		'manager_id',	
		'customer_id',		
		'bank_account_id',			
	);	
	private static $float_cols = array(
		'shipping',		
		'totalprice',			
	);	
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
			
		return false;
	}
	/* Содержит значения извлекаются из БД */
	private $data    = array();
	private $products = array();	
	private $fetched = false;	
	private $args    = array( 'col'   => '', 'value' => '' );
	private $exists  = false;
	
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( false === $value )
			return;

		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'contact_id' ) ) )
			return;

		$this->args = array( 'col' => $col, 'value' => $value );	
	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{
			$this->data = wp_cache_get( $value, 'usam_document' );			
		}
		// кэш существует
		if ( $this->data ) 
		{	
			$this->fetched = true;
			$this->exists = true;			
		}	
		else
			$this->fetch();	
	}

	/**
	 * Обновить кеш
	 */
	public static function update_cache( &$co ) 
	{		
		$id = $co->get( 'id' );
		wp_cache_set( $id, $co->data, 'usam_document' );	
		do_action( 'usam_document_update_cache', $co );
	}

	/**
	 * Удалить кеш
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{		
		$co = new USAM_Document( $value, $col );
		wp_cache_delete( $co->get( 'id' ), 'usam_document' );	
		wp_cache_delete( $co->get( 'id' ), 'usam_document_products' );	
		do_action( 'usam_document_update_cache', $co, $value, $col );
	}

	/**
	 * Удаляет подписчика
	 * @since 4.9	
	 */
	public function delete( ) 
	{
		global $wpdb;	
		
		$id = $this->data['id'];		
		do_action( 'usam_document_before_delete', $id );		
					
		$sql = $wpdb->prepare( "DELETE FROM ".USAM_TABLE_PRODUCTS_DOCUMENT." WHERE document_id = '%d'", $id );		
		$wpdb->query( $sql );			
		$sql = $wpdb->prepare( "DELETE FROM ".USAM_TABLE_DOCUMENTS." WHERE id = %d", $id );	
		$wpdb->query( $sql );
		$sql = $wpdb->prepare( "DELETE FROM ".USAM_TABLE_DOCUMENT_META." WHERE document_id = %d", $id );	
		$wpdb->query( $sql );		
		$sql = $wpdb->prepare( "DELETE FROM ".USAM_TABLE_CUSTOMER_DOCUMENT." WHERE document_id = %d", $id );	
		$wpdb->query( $sql );	

		usam_remove_dir( USAM_DOCUMENTS_DIR.$id );	
		
		self::delete_cache( $id );		
		do_action( 'usam_document_delete', $id );
	}	

	/**
	 * Выбирает фактические записи из базы данных
	 * @since 4.9
	 */
	private function fetch() 
	{	
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );		
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_DOCUMENTS . " WHERE {$col} = {$format}", $value );

		$this->exists = false;
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{						
			$this->exists = true;
			$this->data = apply_filters( 'usam_document_data', $data );			
			self::update_cache( $this );
		}
		do_action( 'usam_document_fetched', $this );
		$this->fetched = true;
	}

	/**
	 * Проверить существует ли строка в БД
	 * @since 4.9
	 */
	public function exists() 
	{
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства из БД
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_document_get_property', $value, $key, $this );
	}

		/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_document_get_data', $this->data, $this );
	}
	
	public function get_contacts( ) 
	{
		$id = $this->get('id');
		if ( empty($id) )
			return array();
		
		$object_type = "usam_invoice_contacts";
		$cache = wp_cache_get( $id, $object_type );		
		if ( $cache === false )		
		{								
			$args = array( 'document_id' => $id );
			$customers = usam_get_contacts_document( $args );		
			foreach( $customers as $customer )
			{
				$contact_ids[] = $customer['customer_id'];
			}			
			$cache = array();	
			if ( !empty($contact_ids))
			{
				require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );	
				$query = array( 'include' => $contact_ids, 'cache_communication' => true, 'cache_results' => true );
				$contacts = usam_get_contacts( $query );			
				if ( !empty($contacts))
					$cache = $contacts;	
			}
			wp_cache_set( $id, $cache, $object_type );	
		}			
		return $cache;
	}
	
	public function add_contact( $customer_id ) 
	{		
		$id = $this->get('id');
		if ( empty($id) )
			return false;						
		
		return usam_add_contact_document( $id, $customer_id );
	}
	
	public function delete_contact( $customer_id ) 
	{		
		$id = $this->get('id');
		if ( empty($id) )
			return false;				
		
		$result = usam_delete_contact_document( $id, $customer_id );		
		return $result;
	}
	
	public function get_products( ) 
	{		
		$id = $this->get('id');
		if ( empty($id) )
			return array();
		
		$object_type = "usam_document_products";
		$cache = wp_cache_get( $id, $object_type );		
		if ( $cache === false )		
		{
			global $wpdb;			
			
			$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_PRODUCTS_DOCUMENT." WHERE document_id = '%d'", $id );
			$cache = $wpdb->get_results( $sql, ARRAY_A );			
			if ( empty($cache))
				$cache = array();	
			
			wp_cache_set( $id, $cache, $object_type );	
		}	
		$this->products = $cache;
		return $cache;
	}		

	// Изменить товары
	public function edit_products( $products ) 
	{	
		global $wpdb;		
		
		$id = $this->get('id');
		if ( empty($id) )
			return false;
		
		require_once( USAM_FILE_PATH . '/includes/product/product_document.class.php' );
		
		$products = apply_filters( 'usam_edit_document_products_set_properties', $products, $this );			
		do_action( 'usam_edit_document_products_pre_edit', $this );
		$i = 0;		
		
		foreach( $products as $key => $product )
		{	
			$product['document_id'] = $id;
			
			$p = new USAM_Product_Document( $key );
			if ( isset($product['discount']) )
			{				
				if ( !isset($product['old_price']) )
					$product['old_price'] = $p->get('old_price');			
				if ( $product['discount'] == 0 )
				{ 
					if ( $product['old_price'] > 0 )
					{
						$product['price'] = $product['old_price'];
						$product['old_price'] = 0;
					}
				}
				else
				{					
					if ( $product['old_price'] == 0 )
						$product['old_price'] = $product['price'];
					 
					if ( $product['type'] == 'p' )										
						$product['price'] = $product['old_price'] - $product['old_price']*$product['discount']/100;						
					else
						$product['price'] = $product['old_price']-$product['discount'];						
					
					$product['price'] = usam_round_price( $product['price'], $this->get('type_price') );	
				}
			}		
			$p->set( $product );
			$p->save( );
		}		
		$this->calculate_totalprice( );	
		
		do_action( 'usam_document_products_edit', $this );
		return $i;
	}			
		
	public function add_product( $product ) 
	{
		$id = $this->get('id');
		if ( empty($id) )
			return false;
		
		if ( empty($product['product_id']) )
			return false;
		
		require_once( USAM_FILE_PATH . '/includes/product/product_document.class.php' );
		
		$product['document_id'] = $id;
		if ( empty($product['quantity']) )
			$product['quantity'] = 1;
		
		$prod_id = false;
		foreach( $this->get_products() as $prod )
		{
			if ( $prod['product_id'] == $product['product_id'] )
			{
				$prod_id = $prod['id'];			
				$product['quantity'] = $prod['quantity'] + 1;
				break;
			}
		}	
		if ( $prod_id )
		{					
			$p = new USAM_Product_Document( $prod_id );
			$p->set( $product );	
		}
		else
		{
			$type_price = $this->get('type_price');
			if ( !empty($type_price) )
			{
				$product['price'] = usam_get_product_price( $product['product_id'], $type_price );
				$product['old_price'] = usam_get_product_old_price( $product['product_id'], $type_price );
			}								
			$p = new USAM_Product_Document( $product );
		}
		$insert_id = $p->save( );	
		if ( $insert_id ) 
		{		
			$this->calculate_totalprice( );			
		}				
		return $insert_id;
	}
	
	public function calculate_totalprice( ) 
	{
		wp_cache_delete( $this->get( 'id' ), 'usam_document_products' );	
		$totalprice = 0;
		foreach( $this->get_products() as $product )
		{
			$totalprice += ($product['price'] + $product['tax'])*$product['quantity'];			
		}	
		$this->data['totalprice'] = $totalprice;		
		$this->save();						
	}
	
	// Получить данные купленного товара
	public function get_order_product( $product_id ) 
	{
		$products = $this->get_products();			
		foreach( $products as $product )
		{
			if ( $product->product_id == $product_id )
				return $product;			
		}	
		return array();
	}
		
	// Удалить товар
	public function delete_product( $basket_id ) 
	{		
		global $wpdb;
		
		$id = $this->get('id');
		if ( empty($id) )
			return false;
		
		$result_deleted = $wpdb->query( $wpdb->prepare( "DELETE FROM `". USAM_TABLE_PRODUCTS_DOCUMENT ."` WHERE `id` = %d AND `document_id` = '%d'", $basket_id, $id ) );	
		self::delete_cache( $id );	

		$this->calculate_totalprice( );
		
		return $result_deleted;
	}		
	
	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );
		}
		$properties = apply_filters( 'usam_document_set_properties', $properties, $this );	

		$this->fetch();		
		if ( ! is_array( $this->data ) )
			$this->data = array();

		$this->data = array_merge( $this->data, $properties );
		return $this;
	}

	/**
	 * Вернуть формат столбцов таблицы
	 * @since 4.9	
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	private function save_file_name()
	{
		global $wpdb;
			
		$type = $this->get('type');			
		if ( $type )
		{ 
			$id = $this->get('id');		
			$file_name = usam_save_printing_form_to_pdf( $type, $id );
		}
	}
		
	/**
	 * Сохраняет в базу данных
	 * @since 4.9
	 */
	public function save()
	{
		global $wpdb;

		do_action( 'usam_document_pre_save', $this );	

		$this->data['date_modified'] = date( "Y-m-d H:i:s" );		
		
		$where_col = $this->args['col'];
		$result = false;		
		if ( $where_col ) 
		{	// обновление				
			
		} 
		else 
		{   // создание	
			do_action( 'usam_document_pre_insert' );		
			
			if ( !isset($this->data['type']) )
				return false;
				
			if ( !isset($this->data['status']) )
				$this->data['status'] = 'draft';	
			
			if ( !isset($this->data['number']) )
			{
				$number = $wpdb->get_var( "SELECT COUNT(ID) FROM `".USAM_TABLE_DOCUMENTS."` WHERE `type` = '".$this->data['type']."' ORDER BY ID" );	
				$this->data['number'] = $number+1;
			}
			if ( !isset($this->data['bank_account_id']) )
				$this->data['bank_account_id'] = get_option( 'usam_shop_company', 0 );			
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );
			
			if ( isset($this->data['closedate']) && $this->data['closedate'] == '' )
				unset($this->data['closedate']);
			
			$this->data = apply_filters( 'usam_document_insert_data', $this->data );			
			$format = $this->get_data_format(  );
			$this->data_format( );		

			$result = $wpdb->insert( USAM_TABLE_DOCUMENTS, $this->data, $format );	
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );	
								
				// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col'   => 'id',  'value' => $this->get( 'id' ), );				
				do_action( 'usam_document_insert', $this );
			}			
		} 		
		do_action( 'usam_document_save', $this );
		return $result;
	}	
}

function usam_get_document( $value, $colum = 'id' )
{	
	$doc = new USAM_Document($value, $colum);	
	return $doc->get_data();	
}

function usam_delete_document( $value, $colum = 'id' )
{	
	$doc = new USAM_Document( $value, $colum );	
	return $doc->delete();	
}

function usam_document_uploaded_file( $id, $file ) 
{
	$file_name = sanitize_file_name( $file['name'] );
	$dir_path = USAM_DOCUMENTS_DIR.$id;
	$file_path = $dir_path.'/'.$file_name;
	if ( !is_dir( $dir_path ) )
		mkdir($dir_path);
	
	if ( file_exists( $file_path ) )
		unlink($file_path);
	
	if ( move_uploaded_file( $file['tmp_name'], $file_path ) )
	{
		return array( 'name' => $file['name'], 'file' => $file_name );		
	}	
	return false;
}

function usam_save_printing_form_to_pdf( $printed_form, $id ) 
{ 	
	$pdf_file = usam_get_printing_forms_to_pdf( $printed_form, $id );
	$format = $printed_form;
	$ext = 'pdf';			
	$directory = USAM_DOCUMENTS_DIR.$id; 	
	if ( !is_dir($directory) )
		mkdir($directory, 0775);
	
	$i = 1;
	do 
	{		
		$sanitized_media_title = $format."_$i";
		$file_path = $directory . '/' . $sanitized_media_title .'.'.$ext;
		if ( !file_exists( $file_path ) )
			break;						
		$i++;
	} 
	while ( true );	
	
	$result = file_put_contents($file_path, $pdf_file );			
	if ( $result)
	{
		$file_name = $sanitized_media_title .'.'.$ext;
		usam_update_document_metadata($id, 'file_name', $file_name ); 			
		return $file_name;
	}
	else
		return false;
}

function usam_get_status_name_document_suggestion( $key_status ) 
{
	$status = usam_get_status_suggestion();	
	if ( isset($status[$key_status]) )
		return $status[$key_status];
	else
		return '';
}

function usam_get_status_suggestion( ) 
{
	$status = array( 'draft' => __('Черновик','usam'), 'sent' => __('Отправлено клиенту','usam'), 'received' => __('Рассматривается клиентом','usam'), 'approved' => __('Утверждено','usam'), 'unanswered' => __('Ответ не получен','usam'), 'declained' => __('Отклонено','usam'));	
	return $status;
}

function usam_get_status_name_document_invoice( $key_status ) 
{
	$status = usam_get_status_invoice();	
	if ( isset($status[$key_status]) )
		return $status[$key_status];
	else
		return '';
}

function usam_get_status_invoice( ) 
{
	$status = array( 'draft' => __('Черновик','usam'), 'sent' => __('Отправлено клиенту','usam'), 'paid' => __('Оплачен','usam'), 'notpaid' => __('Не оплачен','usam') );	
	return $status;
}

function usam_get_status_name_document_contract( $key_status ) 
{
	$status = usam_get_status_contract();	
	if ( isset($status[$key_status]) )
		return $status[$key_status];
	else
		return '';
}

function usam_get_status_contract( ) 
{
	$status = array( 'draft' => __('Черновик','usam'), 'received' => __('Рассматривается контрагентом','usam'), 'signed' => __('Подписан','usam'), 'notsigned' => __('Не подписан','usam') );	
	return $status;
}

function usam_get_document_metadata( $object_id, $meta_key = '', $single = true) 
{	
	return usam_get_metadata('document', $object_id, USAM_TABLE_DOCUMENT_META, $meta_key, $single );
}

function usam_update_document_metadata($object_id, $meta_key, $meta_value, $prev_value = false ) 
{
	return usam_update_metadata('document', $object_id, $meta_key, $meta_value, USAM_TABLE_DOCUMENT_META, $prev_value );
}

/*
$unique(логический)
Определение уникальности ключа.

false - означает, что для этого объекта может быть определено несколько одинаковых ключей.
true - значит, что ключ для этого объекта может быть только один, т.е. если такой ключ уже существует, то функция не добавит никаких данных.
*/
function usam_add_document_metadata($object_id, $meta_key, $meta_value, $prev_value = true ) 
{
	return usam_add_metadata('document', $object_id, $meta_key, $meta_value, USAM_TABLE_DOCUMENT_META, $prev_value );
}

function usam_send_email_document( $event_id ) 
{
	$id = absint($_REQUEST['id']);	
									
	$document = new USAM_Document( $id );					
	$document->set( array( 'status' => 'sent' ) );	
	$document->save();				

	$data = $document->get_data();	
				
	$objects = array(
		array('object_type' => $data['type'], 'object_id' => $id ), 
		array('object_type' => $data['customer_type'], 'object_id' => $data['customer_id'])
	);
	foreach ( $objects as $object ) 
	{
		$object['event_id'] = $event_id;
		usam_set_event_object( $object ); 
	}
}
add_action( 'usam_send_email_document', 'usam_send_email_document' );
