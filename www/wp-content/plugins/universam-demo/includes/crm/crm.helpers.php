<?php

new USAM_CRM_Helpers(  );
final class USAM_CRM_Helpers
{	
	function __construct( )
	{	
		add_action( 'usam_insert_data_customer_after', array($this, 'insert_data_customer'), 10, 2 );
		add_action( 'user_register', array( $this, 'insert_user'), 10, 1 );
	}
	
	public function insert_data_customer( $order_id, $customer_data )
	{			
		$order = usam_get_order( $order_id );		
		
		$properties = usam_get_order_properties( array('type_payer' => $order['type_payer'], 'fields' => 'unique_name=>data') );
		$communications = array();	
		if ( usam_is_type_payer_company( $order['type_payer'] ) ) 
		{				
			foreach ( $customer_data as $unique_name => $field )
			{			
				switch ( $properties[$unique_name]->type ) 
				{						
					case 'email':					
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'work', 'type' => 'email', 'customer_type' => 'company' );					
					break;
					case 'phone':						
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'work', 'type' => 'phone', 'customer_type' => 'company' );
					break;
					case 'mobile_phone':						
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'mobile', 'type' => 'phone', 'customer_type' => 'company' );
					break;				
				}	
			}
			if ( !empty($communications) )
			{			
				$args['type'] = 'customer';	
				if ( !empty($customer_data['company']) )
					$args['name'] = $customer_data['company']['value'];				
				
				if ( !empty($customer_data['inn']) )
					$meta['name'] = $customer_data['inn']['value'];
				if ( !empty($customer_data['kpp']) )
					$meta['name'] = $customer_data['kpp']['value'];				
				if ( !empty($customer_data['company_shippingpostcode']) )
					$meta['name'] = $customer_data['company_shippingpostcode']['value'];			
				
				if ( $order['customer_id'] ) 
				{
					$company = new USAM_Company( $order['customer_id'], $args );	
					$company->save();
					$this->set_communication( $order['customer_id'], $communications );
				}
				else
				{					
					$company = new USAM_Company( $args );	
					$company->save();
					$customer_id = $company->get('id');					
					$company->update_meta( $meta );
					
					$this->set_communication( $customer_id, $communications );
						
					usam_update_order( $order_id, array('customer_id' => $customer_id) );
				}				
			}
		}
		else
		{				
			foreach ( $customer_data as $unique_name => $field )
			{			
				switch ( $properties[$unique_name]->type ) 
				{						
					case 'email':					
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'private', 'type' => 'email', 'customer_type' => 'contact' );					
					break;
					case 'phone':						
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'private', 'type' => 'phone', 'customer_type' => 'contact' );
					break;
					case 'mobile_phone':						
						$communications[] = array( 'value' => $field['value'], 'value_type' => 'mobile', 'type' => 'phone', 'customer_type' => 'contact' );
					break;				
				}	
			}
			if ( !empty($communications) )
			{
				$args['contact_source'] = 'order';
				if ( !empty($customer_data['billinglastname']) )
					$args['lastname'] = $customer_data['billinglastname']['value'];
				if ( empty($args['lastname']) && !empty($customer_data['shippinglastname']) )
					$args['lastname'] = $customer_data['shippinglastname']['value'];
				
				if ( !empty($customer_data['billingfirstname']) )
					$args['firstname'] = $customer_data['billingfirstname']['value'];
				if ( empty($args['firstname']) && !empty($customer_data['shippingfirstname']) )
					$args['firstname'] = $customer_data['shippingfirstname']['value'];
				
				$args['user_id'] = $order['user_ID'];			
				$args['location_id'] = usam_get_user_location( $order['user_ID'] );				
				if ( !empty($order['customer_id']) ) 
				{
					$args['id'] = $order['customer_id'];
					$this->update_subscriber( $args, $communications );
				}
				else
				{
					$contact = usam_get_contact( $order['user_ID'], 'user_id' ); 	
					if ( !empty($contact) ) 				
					{
						$customer_id = $order['customer_id'] = $contact['id'];
						$this->update_subscriber( $args, $communications );
					}
					else
					{
						$customer_id = $this->add_subscriber( $args, $communications );						
					}
					usam_update_order( $order_id, array('customer_id' => $customer_id) );
				}		
			}
		}	
	}	
	
	public function insert_user( $user_id )
	{					
		$user_info = get_userdata( $user_id );
		
		if ( empty($user_info->user_email) )
			return false;
		
		if ( empty($user_info->last_name) )
			$lastname = $user_info->display_name;
		else
			$lastname = $user_info->last_name;	
		
		$args = array( 'lastname' => $lastname, 'firstname' => $user_info->first_name, 'contact_source' => 'register' );
		$communications = array();
		$communications[] = array( 'value' => $user_info->user_email, 'value_type' => 'private', 'type' => 'email', 'customer_type' => 'contact' );			
		$args['user_id'] = $user_id;
		$args['location_id'] = usam_get_user_location( $user_id );
		
		$this->add_subscriber( $args, $communications );			
	}
	
// Добавить подписчика
	private function add_subscriber( $insert, $communications )
	{				
		$contact_id = usam_insert_contact( $insert );		
		if ( $contact_id )
		{
			$this->set_communication( $contact_id, $communications );	
		}
		return $contact_id;
	}
	
	private function update_subscriber( $update, $communications )
	{			
		//usam_update_contact( $update );	
		if ( !empty($update['id']) )
			$this->set_communication( $update['id'], $communications );			
	}
	
	private function set_communication( $customer_id, $communications )
	{				
		$users_to_mailing_list = get_option('usam_add_users_to_mailing_list', '' );		
		foreach ( $communications as $communication ) 
		{
			$communication['contact_id'] = $customer_id;
			$communication_id = usam_insert_communication( $communication );
			if ( $communication_id && $communication['type'] == 'email' && !empty($users_to_mailing_list) )
			{						
				$list = get_option('usam_add_users_to_mailing_list' );												
				usam_set_subscriber_lists( array( 'id_communication' => $communication_id, 'status' => 1, 'id' => $list ) );	
			}				
		}				
	}		
}


function usam_get_customer_case( $customer_id, $customer_type )
{	
	$events = wp_cache_get( $customer_id, 'usam_customer_case_'.$customer_type );			
	if ( $events === false )			
	{							
		$events = usam_get_tasks( array( 'object_type' => $customer_type, 'object_id' => $customer_id, 'type' => array( 'affair', 'meeting', 'call', 'event'  ) ) );	
		wp_cache_set( $customer_id, $events, 'usam_customer_case_'.$customer_type );						
	}
	return $events;
}


function usam_update_customer_case_cache( $customer_ids, $customer_type )
{	
	$events = usam_get_tasks( array( 'object_type' => $customer_type, 'object_id' => $customer_ids, 'type' => array( 'affair', 'meeting', 'call', 'event'  ) ) );	
	foreach ( $events as $event ) 
	{				
		$results[$event->id] = $event; 	
		$event_ids[] = $event->id;
	}
	$cache = array();
	if ( !empty($event_ids) )
	{
		$event_objects = usam_get_event_objects( array( 'event_id' => $event_ids, 'object_type' => $customer_type ) );	
		foreach ( $event_objects as $object ) 
		{
			$cache[$object->object_id][] = $results[$object->event_id]; 	
		}
	}
	foreach ( $customer_ids as $customer_id ) 
	{
		if ( !isset($cache[$customer_id]) )
			$cache[$customer_id] = array();
		
		wp_cache_set( $customer_id, $cache[$customer_id], 'usam_customer_case_'.$customer_type );		
	}
	return $cache;
}
?>