<?php
class USAM_Comment
{
	// строковые
	private static $string_cols = array(
		'date_insert',		
		'object_type',		
		'message',	
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'user_id',		
		'object_id',
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_comment' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_comment' );	
		do_action( 'usam_comment_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_Comment( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_comment' );	
		do_action( 'usam_comment_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public function delete(  ) 
	{		
		global  $wpdb;
		
		$id = $this->get('id');
		do_action( 'usam_comment_before_delete', $id );		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_COMMENTS." WHERE id = '$id'");		
		self::delete_cache( $id );		
		do_action( 'usam_comment_delete', $id );
		
		return $result;
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_COMMENTS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_comment_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_comment_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_comment_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_comment_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_comment_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}		
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_comment_pre_save', $this );	
		$where_col = $this->args['col'];
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_comment_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			$where = array( $this->args['col'] => $where_val);

			$this->data = apply_filters( 'usam_comment_update_data', $this->data );	
			$formats = $this->get_data_format( );
			$this->data_format( );		
			$data = $this->data;
			
			$result = $wpdb->update( USAM_TABLE_COMMENTS, $this->data, $where, $formats, array( $where_format ) );	
			do_action( 'usam_comment_update', $this );
		} 
		else 
		{   
			do_action( 'usam_comment_pre_insert' );			
			if ( isset($this->data['id']) )
				unset( $this->data['id'] );	
			
			$this->data['date_insert']     = date( "Y-m-d H:i:s" );	
			if ( empty($this->data['user_id']) )
				$this->data['user_id'] = $user_ID;	
			
			if ( empty($this->data['message']) )
				$this->data['message'] = '';
			
			if ( empty($this->data['object_id']) )
				return false;
			
			if ( empty($this->data['object_type']) )
				return false;		
						
			$this->data = apply_filters( 'usam_comment_insert_data', $this->data );
			$format = $this->get_data_format(  );
			$this->data_format( );	
					
			$result = $wpdb->insert( USAM_TABLE_COMMENTS, $this->data, $format ); 
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_comment_insert', $this );
		} 		
		do_action( 'usam_comment_save', $this );

		return $result;
	}
}

function usam_get_comment( $id, $colum = 'id' )
{
	$comment = new USAM_Comment( $id, $colum );
	return $comment->get_data( );	
}

function usam_delete_comment( $id ) 
{
	$comment = new USAM_Comment( $id );
	$result = $comment->delete( );
	return $result;
}

// Вставить комментарий
function usam_insert_comment( $data ) 
{
	$comment = new USAM_Comment( $data );
	$comment->save();
	return $comment->get('id');
}

function usam_update_comment( $id, $data ) 
{
	$comment = new USAM_Comment( $id );
	$comment->set( $data );
	return $comment->save();
}


function usam_get_comment_metadata( $object_id, $meta_key = '', $single = true) 
{	
	return usam_get_metadata('comment', $object_id, USAM_TABLE_COMMENT_META, $meta_key, $single );
}

function usam_update_comment_metadata($object_id, $meta_key, $meta_value, $prev_value = true ) 
{
	return usam_update_metadata('comment', $object_id, $meta_key, $meta_value, USAM_TABLE_COMMENT_META, $prev_value );
}
?>