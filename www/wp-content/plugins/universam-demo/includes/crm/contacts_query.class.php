<?php
// Класс работы с контактами
class USAM_Contacts_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $request;

	private $compat_fields = array( 'results', 'total' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}	

	/**
	 * Fills in missing query variables with default values.
	 */
	public static function fill_query_vars( $args ) 
	{
		$defaults = array(
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'monthnum' => '',
			'year' => '',
			'w' => '',
			'status' => '',
			'name_is_known' => false,			
			'status__in' => array(),
			'status__not_in' => array(),
			'meta_key' => '',
			'meta_value' => '',
			'meta_compare' => '',
			//'include' => array(),
			'exclude' => array(),
			'search' => '',
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => '',
			'number' => '',
			'paged' => 1,
			'count_total' => true,
			'fields' => 'all',	
			'cache_communication' => true,		
			'cache_case' => false,					
			'cache_results' => false,				
		);
		return wp_parse_args( $args, $defaults );
	}
	
	public function prepare_query( $query = array() ) 
	{
		global $wpdb, $user_ID;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) 
		{
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_orders', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );	
				
		$join = array();		
	

		if ( is_array( $qv['fields'] ) ) 
		{
			$qv['fields'] = array_unique( $qv['fields'] );

			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) 
			{		
				if ( $field == 'count' )
					$this->query_fields[] = "COUNT(*) AS count";			
				else
				{
					$field = 'ID' === $field ? 'ID' : sanitize_key( $field );						
					$this->query_fields[] = USAM_TABLE_CONTACTS.".$field";
				}
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 
		elseif ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_CONTACTS.".*";	
		elseif ( 'count' == $qv['fields'] ) 		
			$this->query_fields = "COUNT(*) AS count";
		else 	
			$this->query_fields = USAM_TABLE_CONTACTS.".ID";

		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;

		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_CONTACTS;
		$this->query_where = "WHERE 1=1";		
		
	// Обрабатывать другие параметры даты
		$date_parameters = array();
		if ( '' !== $qv['hour'] )
			$date_parameters['hour'] = $qv['hour'];

		if ( '' !== $qv['minute'] )
			$date_parameters['minute'] = $qv['minute'];

		if ( '' !== $qv['second'] )
			$date_parameters['second'] = $qv['second'];

		if ( $qv['year'] )
			$date_parameters['year'] = $qv['year'];

		if ( $qv['monthnum'] )
			$date_parameters['monthnum'] = $qv['monthnum'];

		if ( $qv['w'] )
			$date_parameters['week'] = $qv['w'];

		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];

		if ( $date_parameters ) 
		{
			$date_query = new USAM_Date_Query( array( $date_parameters ), USAM_TABLE_CONTACTS.'.date_insert' );
			$this->query_where .= $date_query->get_sql();
		}

		// Handle complex date queries
		if ( ! empty( $qv['date_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_query'], USAM_TABLE_CONTACTS.'.date_insert' );
			$this->query_where .= $this->date_query->get_sql();
		}			
		if ( ! empty( $qv['include'] ) ) 		
			$include = wp_parse_id_list( $qv['include'] );		 
		elseif ( isset( $qv['include'] ) ) 		
			$include = array( 0 );		 
		else 		
			$include = false;
	
		$status = array();
		if ( isset( $qv['status'] ) ) 
		{
			if ( is_array( $qv['status'] ) ) 
				$status = $qv['status'];			
			elseif ( is_string( $qv['status'] ) && ! empty( $qv['status'] ) )
				$status = array_map( 'trim', explode( ',', $qv['status'] ) );
		}

		$status__in = array();
		if ( isset( $qv['status__in'] ) ) {
			$status__in = (array) $qv['status__in'];
		}

		$status__not_in = array();
		if ( isset( $qv['status__not_in'] ) ) {
			$status__not_in = (array) $qv['status__not_in'];
		}

		if ( ! empty( $status ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".status IN ('".implode( "','",  $status )."')";	
		}		
		if ( ! empty($status__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".status NOT IN (".implode( ',',  $status__not_in ).")";
		}
		if ( ! empty( $status__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".status IN (".implode( ',',  $status__in ).")";
		}
		
		$status_subscriber = array();
		if ( isset( $qv['status_subscriber'] ) ) 
		{
			if ( is_array( $qv['status_subscriber'] ) ) 
				$status_subscriber = $qv['status_subscriber'];			
			elseif ( is_string( $qv['status_subscriber'] ) && ! empty( $qv['status_subscriber'] ) )
				$status_subscriber = array_map( 'trim', explode( ',', $qv['status_subscriber'] ) );
		}
		$status_subscriber__not_in = array();
		if ( isset( $qv['status_subscriber__not_in'] ) ) {
			$status_subscriber__not_in = (array) $qv['status_subscriber__not_in'];
		}

		if ( ! empty( $status_subscriber ) ) 
		{		
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_MEANS_COMMUNICATION." ON (".USAM_TABLE_MEANS_COMMUNICATION.".contact_id = ".USAM_TABLE_CONTACTS.".id)";
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." ON (".USAM_TABLE_MEANS_COMMUNICATION.".id = ".USAM_TABLE_SUBSCRIBER_LISTS.".id_communication)";
				
			$this->query_where .= " AND ".USAM_TABLE_SUBSCRIBER_LISTS.".status IN (".implode( ',',  $status_subscriber ).")";		
		}
		if ( ! empty($status_subscriber__not_in) ) 
		{			
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_MEANS_COMMUNICATION." ON (".USAM_TABLE_MEANS_COMMUNICATION.".contact_id = ".USAM_TABLE_CONTACTS.".id)";
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." ON (".USAM_TABLE_MEANS_COMMUNICATION.".id = ".USAM_TABLE_SUBSCRIBER_LISTS.".id_communication)";
				
			$this->query_where .= " AND ".USAM_TABLE_SUBSCRIBER_LISTS.".status NOT IN (".implode( ',',  $status_subscriber__not_in ).")";	
		}		
		
		$list_subscriber = array();
		if ( isset( $qv['list_subscriber'] ) ) 
		{
			if ( is_array( $qv['list_subscriber'] ) ) 
				$list_subscriber = $qv['list_subscriber'];			
			elseif ( is_string( $qv['list_subscriber'] ) && ! empty( $qv['list_subscriber'] ) )
				$list_subscriber = array_map( 'trim', explode( ',', $qv['list_subscriber'] ) );
		}
		$list_subscriber__not_in = array();
		if ( isset( $qv['list_subscriber__not_in'] ) ) {
			$list_subscriber__not_in = (array) $qv['list_subscriber__not_in'];
		}

		if ( ! empty( $list_subscriber ) ) 
		{		
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_MEANS_COMMUNICATION." ON (".USAM_TABLE_MEANS_COMMUNICATION.".contact_id = ".USAM_TABLE_CONTACTS.".id)";
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." ON (".USAM_TABLE_MEANS_COMMUNICATION.".id = ".USAM_TABLE_SUBSCRIBER_LISTS.".id_communication)";
				
			$this->query_where .= " AND ".USAM_TABLE_SUBSCRIBER_LISTS.".list IN (".implode( ',',  $list_subscriber ).")";		
		}
		if ( ! empty($list_subscriber__not_in) ) 
		{
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_MEANS_COMMUNICATION." ON (".USAM_TABLE_MEANS_COMMUNICATION.".contact_id = ".USAM_TABLE_CONTACTS.".id)";
			$this->query_join .= " LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." ON (".USAM_TABLE_MEANS_COMMUNICATION.".id = ".USAM_TABLE_SUBSCRIBER_LISTS.".id_communication)";
				
			$this->query_where .= " AND ".USAM_TABLE_SUBSCRIBER_LISTS.".list NOT IN (".implode( ',',  $list_subscriber__not_in ).")";	
		}	
		if ( !empty( $qv['not_subscriber'] ) ) 
		{			
			$this->query_where .= " AND NOT EXISTS (SELECT 1
				FROM ".USAM_TABLE_MEANS_COMMUNICATION." INNER JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." ON (".USAM_TABLE_MEANS_COMMUNICATION.".id=".USAM_TABLE_SUBSCRIBER_LISTS.".id_communication) 
				WHERE ".USAM_TABLE_MEANS_COMMUNICATION.".contact_id=".USAM_TABLE_CONTACTS.".id )";
		}		
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];
			$ordersby_array = array();		
			foreach ( $groupby as $_value ) 
			{				
				switch ( $_value ) 
				{
					case 'day' :
						$ordersby_array[] = 'DAY(date_insert)';						
					break;
					case 'month' :
						$ordersby_array[] = 'MONTH(date_insert)';
						
					break;
					case 'year' :
						$ordersby_array[] = 'YEAR(date_insert)';				
					break;
					default:
						$ordersby_array[] = $_value;
				}		
			}	
		}		
		if ( ! empty($ordersby_array) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $ordersby_array );
		else
			$this->query_groupby = '';
		
		
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{
			// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}

		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}
			
			if ( is_int( $_key ) ) 
			{				
				$_orderby = $_value;
				$_order = $order;
			} 
			else
			{				
				$_orderby = $_key;
				$_order = $_value;
			}
			$parsed = $this->parse_orderby( $_orderby );
			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = USAM_TABLE_CONTACTS.'.'.$parsed . ' ' . $this->parse_order( $_order );
		}		
		if ( empty( $orderby_array ) ) 
		{
			$orderby_array[] = USAM_TABLE_CONTACTS.".id $order";
		}
		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );

		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) 
		{
			if ( $qv['offset'] ) {
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			} else {
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		}

		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) 
		{
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'lastname', 'firstname', 'name' ) );
			if ( ! $search_columns ) 
			{
				if ( false !== strpos( $search, '@') )
					$search_columns = array('user_email');
				elseif ( is_numeric($search) )
					$search_columns = array('id', 'user_phone' );			
				else
					$search_columns = array( 'lastname', 'firstname', 'name');
			}	
			$search_columns = apply_filters( 'usam_contacts_search_columns', $search_columns, $search, $this );

			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}
		if ( ! empty( $include ) ) 
		{
			$ids = implode( ',', $include );			
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".ID NOT IN ($ids)";
		}			
		if ( !empty( $qv['company_id'] ) ) 
		{		
			$company_id = implode( ',',  (array)$qv['company_id'] );		
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".company_id IN ($company_id)";
		}
		if ( isset( $qv['user_ID'] ) && $qv['user_ID'] !== '' ) 
		{		
			$user_ID = implode( ',',  (array)$qv['user_ID'] );		
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".user_ID IN ($user_ID)";
		}	
		if ( isset( $qv['manager_id'] ) && $qv['manager_id'] !== '' ) 
		{		
			$manager = implode( ',',  (array)$qv['manager_id'] );		
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".manager_id IN ($manager)";
		}			
		if ( isset( $qv['open'] ) ) 
		{		
			$open = $qv['open'] == 1?1:0 ;
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".open='$open'";
		}
		if ( empty($qv['manager_id']) && !isset($qv['open'])) 
		{				
			$this->query_where .= " AND (".USAM_TABLE_CONTACTS.".manager_id IN ($user_ID) OR ".USAM_TABLE_CONTACTS.".open='1')";
		}	
		if ( !empty( $qv['source'] ) ) 
		{		
			$source = implode( "','",  (array)$qv['source'] );		
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".contact_source IN ('$source')";
		}	
		if ( $qv['name_is_known'] ) 
		{				
			$this->query_where .= " AND ".USAM_TABLE_CONTACTS.".lastname != '' AND ".USAM_TABLE_CONTACTS.".firstname != ''";
		}		
		if ( !empty($qv['is_communication']) ) 
		{		
			$this->query_where .= " AND EXISTS(SELECT DISTINCT contact_id FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE customer_type = 'contact' AND ".USAM_TABLE_CONTACTS.".id=contact_id LIMIT 1)";			
		}	
		do_action_ref_array( 'usam_pre_orders_query', array( &$this ) );
	}

	/**
	 * Execute the query, with the current variables.	
	 */
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;

		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";	

		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] && $qv['number'] != 1 ) 		
			$this->results = $wpdb->get_results( $this->request );		
		elseif ( $qv['number'] == 1 && 'all' !== $qv['fields'] )		
			$this->results = $wpdb->get_var( $this->request );
		elseif ( $qv['number'] == 1 && 'all' == $qv['fields'] )		
			$this->results = $wpdb->get_row( $this->request );
		else 
			$this->results = $wpdb->get_col( $this->request );

		if ( !$this->results )
			return;	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total = $wpdb->get_var( 'SELECT FOUND_ROWS()' );
				
		if ( $qv['cache_results'] && 'all' == $qv['fields'] )
		{ 
			if ( $qv['number'] == 1 )
				wp_cache_set( $this->results->id, (array)$this->results, 'usam_contact' );	
			else
			{					
				foreach ( $this->results as $result ) 
				{
					wp_cache_set( $result->id, (array)$result, 'usam_contact' );						
				}
			}			
		}			
		if ( isset($this->results[0]->id) )		
		{ 
			$ids = array();	
			foreach ( $this->results as $result ) 
			{				
				$ids[] = $result->id; 					
			}	
			if ( $qv['cache_communication'] )		
			{							
				usam_update_communication_cache( $ids, 'contact' );
			}
			if ( $qv['cache_case'] )		
			{
				usam_update_customer_case_cache( $ids, 'contact' );
			}
		}
	}

	/**
	 * Retrieve query variable.
	 */
	public function get( $query_var ) {
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}

	public function set( $query_var, $value ) {
		$this->query_vars[$query_var] = $value;
	}


	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;

		foreach ( $cols as $col ) 
		{
			if ( 'ID' == $col || 'id' == $col ) 
			{
				$searches[] = $wpdb->prepare( USAM_TABLE_CONTACTS.".$col = %s", $string );
			} 
			elseif ( 'user_email' == $col )
			{
				$searches[] = $wpdb->prepare( "id IN (SELECT DISTINCT contact_id FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE customer_type = 'contact' AND type = 'email' AND value LIKE %s)", $like );			
			}		
			elseif ( 'user_phone' == $col )
			{
				$searches[] = $wpdb->prepare( "id IN (SELECT DISTINCT contact_id FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE customer_type = 'contact' AND type = 'phone' AND value LIKE %s)", $like );	
			}	
			elseif ( 'name' == $col ) 
			{
				$name = explode(' ', $string);
				if ( count($name)>1 )
				{			
					$name[0] = $leading_wild . $wpdb->esc_like( $name[0] ) . $trailing_wild;
					$name[1] = $leading_wild . $wpdb->esc_like( $name[1] ) . $trailing_wild;
					$searches[] = $wpdb->prepare( USAM_TABLE_CONTACTS.".lastname LIKE %s", $name[0] )." AND ".$wpdb->prepare( USAM_TABLE_CONTACTS.".firstname LIKE %s", $name[1] );
					$searches[] = $wpdb->prepare( USAM_TABLE_CONTACTS.".firstname LIKE %s", $name[0] )." AND ".$wpdb->prepare( USAM_TABLE_CONTACTS.".lastname LIKE %s", $name[1] );
				}				
			} 					
			else 
			{
				$searches[] = $wpdb->prepare( "$col LIKE %s", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}

	/**
	 * Return the list of users.
	 */
	public function get_results() 
	{
		return $this->results;
	}

	/**
	 * Return the total number of users for the current query.
	 */
	public function get_total() 
	{
		return $this->total;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';
		if ( in_array( $orderby, array( 'id', 'firstname', 'lastname' ) ) )
		{
			$_orderby = $orderby;
		} 		
		elseif ( 'ID' == $orderby || 'id' == $orderby ) 
		{
			$_orderby = 'id';
		} 		
		elseif ( 'meta_value' == $orderby || $this->get( 'meta_key' ) == $orderby ) 
		{
			$_orderby = "$wpdb->usermeta.meta_value";
		} 
		elseif ( 'meta_value_num' == $orderby ) {
			$_orderby = "$wpdb->usermeta.meta_value+0";
		} 
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) ) {
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_CONTACTS.".id, $include_sql )";
		} 		
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) {
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments ) {
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

function usam_get_contacts( $query )
{	
	$query['count_total'] = false;
	$contacts = new USAM_Contacts_Query( $query );		
	return $contacts->get_results();	
}	