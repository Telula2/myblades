<?php
class USAM_Company 
{	
	 // строковые
	private static $string_cols = array(	
		'date_insert',
		'type',
		'industry',
		'employees',
		'description',
		'group',		
		'name',		
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'logo',
		'revenue',
		'manager_id',
		'open',			
	);

	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
			
		return false;
	}
	/**
	 *Содержит значения извлекаются из БД
	 * @since 4.9
	 */
	private $data    = array();
	private $fetched = false;	
	private $args    = array( 'col'   => '', 'value' => '' );
	private $exists  = false;
	
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( false === $value )
			return;

		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;

		$this->args = array( 'col' => $col, 'value' => $value );	
	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{
			$this->data = wp_cache_get( $value, 'usam_company' );			
		}
		// кэш существует
		if ( $this->data ) 
		{	
			$this->fetched = true;
			$this->exists = true;			
		}	
		else
			$this->fetch();	
	}

	/**
	 * Обновить кеш
	 */
	public static function update_cache( &$subscriber ) 
	{		
		$id = $subscriber->get( 'id' );
		wp_cache_set( $id, $subscriber->data, 'usam_company' );	
		do_action( 'usam_company_update_cache', $subscriber );
	}

	/**
	 * Удалить кеш
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{		
		$subscriber = new USAM_Company( $value, $col );
		wp_cache_delete( $subscriber->get( 'id' ), 'usam_company' );	
		wp_cache_delete( $subscriber->get( 'id' ), 'usam_company_meta' );	
		wp_cache_delete( $subscriber->get( 'id' ), 'usam_company_means_communication' );			
		
		do_action( 'usam_company_update_cache', $subscriber, $value, $col );
	}

	/**
	 * Удаляет
	 * @since 4.9	
	 */
	public function delete( ) 
	{
		global $wpdb;	
		
		$id = $this->get('id');		
		do_action( 'usam_company_before_delete', $id );			
		$delete_ids = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE contact_id = '%d' AND customer_type = 'company'", $id ) );			
		if ( !empty($delete_ids) )
		{
			$wpdb->query( "DELETE FROM " . USAM_TABLE_SUBSCRIBER_LISTS . " WHERE id_communication IN (".implode(',', $delete_ids).")" );	
			
			$args = array( 'contact_id' => $id, 'customer_type' => 'company' );			
			usam_delete_means_communication( $args );
		}	
		$where = array( 'company_id' => $id );	
		$result = $wpdb->delete( USAM_TABLE_COMPANY_ACC_NUMBER, $where, array( '%d' ) );
		$result = $wpdb->delete( USAM_TABLE_COMPANY_META, $where, array( '%d' ) );		
		$result = $wpdb->delete( USAM_TABLE_COMPANY, array( 'id' => $id ), array( '%d' ) );
		
		self::delete_cache( $id );		
		do_action( 'usam_company_delete', $id );
	}	

	/**
	 * Выбирает фактические записи из базы данных
	 * @since 4.9
	 */
	private function fetch() 
	{	
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );		
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_COMPANY . " WHERE {$col} = {$format}", $value );

		$this->exists = false;
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{				
			$this->exists = true;
			$this->data = apply_filters( 'usam_company_data', $data );			
			self::update_cache( $this );
		}
		do_action( 'usam_company_fetched', $this );
		$this->fetched = true;
	}

	/**
	 * Проверить существует ли строка в БД
	 * @since 4.9
	 */
	public function exists() 
	{
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства из БД
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_company_get_property', $value, $key, $this );
	}

		/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_company_get_data', $this->data, $this );
	}
	
	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );
		}
		$properties = apply_filters( 'usam_company_set_properties', $properties, $this );	

		$this->fetch();		
		if ( ! is_array( $this->data ) )
			$this->data = array();

		$this->data = array_merge( $this->data, $properties );
		return $this;
	}

	/**
	 * Вернуть формат столбцов таблицы
	 * @since 4.9	
	 */
	private function get_data_format( ) 
	{
		$formats = array();		
		$this->data = wp_unslash( $this->data );		
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function check_duplicates( ) 
	{
		global $wpdb;
		
		if ( !empty($this->data['name']) )
		{
			$result = $wpdb->get_var( $wpdb->prepare("SELECT id FROM ".USAM_TABLE_COMPANY." WHERE name = '%s' LIMIT 1", $this->data['name'] ) );	
			if ( $result > 0 )
			{
				$this->args = array('col' => 'id', 'value' => $result );
				$this->fetch();	
				return true;
			}
		}
		else
			return true;
		
		return false;
	}
	
	/**
	 * Сохраняет в базу данных
	 * @since 4.9
	 */
	public function save()
	{
		global $wpdb;

		do_action( 'usam_company_pre_save', $this );	
		
		$where_col = $this->args['col'];
		$result = false;		
		if ( $where_col ) 
		{	// обновление			
			$subscriber_id = $this->get('id');
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_company_pre_update', $this );
			self::delete_cache( $where_val, $where_col );			
			$this->data = apply_filters( 'usam_company_update_data', $this->data );			
			$format = $this->get_data_format( );
					
			$result = $wpdb->update( USAM_TABLE_COMPANY, $this->data, array( $where_col => $where_val ), $format, array( $where_format ) );		
			do_action( 'usam_company_update', $this );
		} 
		else 
		{   // создание	
			do_action( 'usam_company_pre_insert' );	
			
			if ( !isset($this->data['user_id']) )
				$this->data['user_id'] = 0;
			else
					
			if ( !isset($this->data['name']) )
				$this->data['name'] = '';
			else
				$this->data['name'] = trim($this->data['name']);						
			
			if ( !isset($this->data['industry']) )
				$this->data['industry'] = 'other';		

			if ( !isset($this->data['type']) )
				$this->data['type'] = 'other';				
			
			if ( $this->check_duplicates( ) )
			{					
				return false;			
			}					
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );				
			$this->data = apply_filters( 'usam_company_insert_data', $this->data );				
			$format = $this->get_data_format( );			
		
			$result = $wpdb->insert( USAM_TABLE_COMPANY, $this->data, $format );	
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );				
				
				// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col'   => 'id',  'value' => $this->get( 'id' ), );				
				do_action( 'usam_company_insert', $this );
			}			
		} 		
		do_action( 'usam_company_save', $this );
		return $result;
	}
	
	
	public function insert_communication( $data ) 
	{
		$data['contact_id'] = $this->get( 'id' );
		$data['customer_type'] = 'company';
		$id = usam_insert_communication( $data );
		return $id;
	}
	
	public function update_communication( $id, $data ) 
	{		
		$contact_id = $this->get( 'id' );	
		$result = usam_update_communication( $id, $data ); 		
		return $result;
	}
	
	public function update_meta( $data ) 
	{		
		global $wpdb;	
	
		$company_id = $this->get( 'id' );			
		$result = 0;
		foreach ( $data as $unique_name => $value ) 
		{		
			$value = trim( wp_unslash( $value ) );	
	
			if ( $value == '' )
				continue;
			
			$sql = "INSERT INTO `".USAM_TABLE_COMPANY_META."` (`unique_name`, `company_id`, `value`) VALUES ('%s','%d','%s') ON DUPLICATE KEY UPDATE `value`='%s'";					
			$update = $wpdb->query( $wpdb->prepare($sql, $unique_name, $company_id, $value, $value ) );
			
			if( $update )
				$result++;
		}
		return $result;
	}
	
	public function get_meta( ) 
	{
		$object_type = 'usam_company_meta';	
		$company_id = $this->get( 'id' );	
		$cache = wp_cache_get( $company_id, $object_type );			
		if ( $cache === false )			
		{							
			global $wpdb;	
			$sql = "SELECT * FROM ".USAM_TABLE_COMPANY_META." WHERE company_id = %d";
			$cache = $wpdb->get_results( $wpdb->prepare( $sql, $company_id) );

			wp_cache_set( $company_id, $cache, $object_type );						
		}
		$meta	= array();		
		foreach ( $cache as $value ) 
		{
			$meta[$value->unique_name] = $value->value;
		}
		$company_fields = get_option("usam_crm_company_fields");
		foreach ( $company_fields as $value ) 
		{
			if ( !isset($meta[$value['unique_name']]) )
				$meta[$value['unique_name']] = '';
		}
		return $meta;
	}
	
	public function get_bank_accounts_format( $data ) 
	{
		$formats = array( 'id' => '%d', 'company_id' => '%d', 'name' => '%s', 'bic' => '%s', 'number' => '%s', 'bank_ca'  => '%s', 'currency' => '%s', 'address'  => '%s', 'swift'  => '%s', 'note'  => '%s' );	
		$format = array();
		foreach ( $data as $key => $value ) 
		{				
			if ( isset($formats[$key]) )		
				$format[$key] = $formats[$key];			
		}			
		return $format;
	}
	
	public function get_bank_accounts( ) 
	{
		$object_type = 'usam_company_bank_accounts';	
		$company_id = $this->get( 'id' );	
		$cache = wp_cache_get( $company_id, $object_type );			
		if ( $cache === false )			
		{							
			global $wpdb;	
			$sql = "SELECT * FROM ".USAM_TABLE_COMPANY_ACC_NUMBER." WHERE company_id = %d";
			$cache = $wpdb->get_results( $wpdb->prepare( $sql, $company_id) );

			wp_cache_set( $company_id, $cache, $object_type );						
		}	
		return $cache;
	}
		
	public function insert_bank_accounts( $data ) 
	{				
		global $wpdb;		
		$data = apply_filters( 'usam_company_bank_accounts_insert', $data, $this );	
		
		if ( empty($data['number']) )
			return false;
		
		$data = wp_unslash( $data );
		
		$data['company_id'] = $this->get( 'id' );			
		$format = $this->get_bank_accounts_format( $data );
		$result = $wpdb->insert( USAM_TABLE_COMPANY_ACC_NUMBER, $data, $format );
		
		do_action( 'usam_company_bank_accounts_insert', $data, $this );	
		return $result;
	}
	
	public function update_bank_accounts( $id, $data ) 
	{		
		global $wpdb;	
		$company_id = $this->get( 'id' );	
		$data = apply_filters( 'usam_company_bank_accounts_update', $data, $this );	
			
		$data = wp_unslash( $data );
		
		$where = array( 'id' => $id, 'company_id' => $company_id );
		 
		$format = $this->get_bank_accounts_format( $data );
		$result = $wpdb->update( USAM_TABLE_COMPANY_ACC_NUMBER, $data, $where, $format, array( '%d', '%d' ) );	

		do_action( 'usam_company_bank_accounts_update', $data, $this );			
		return $result;
	}
	
	public function delete_bank_accounts( $id ) 
	{		
		global $wpdb;	
		$company_id = $this->get( 'id' );			
		$where = array( 'id' => $id, 'company_id' => $company_id );	
		$format = $this->get_bank_accounts_format( $where );
		$result = $wpdb->delete( USAM_TABLE_COMPANY_ACC_NUMBER, $where, $format );
		do_action( 'usam_company_bank_accounts_delete', $company_id, $id, $this );			
		return $result;
	}
}

function usam_get_company_means_communication( $id )
{		
	$result = usam_get_customer_communication( $id, 'company' );
	return $result;
}

function usam_get_company( $value, $colum = 'id' )
{	
	$company = new USAM_Company($value, $colum);	
	$company_data = $company->get_data();	
	if ( empty($company_data) )
		return array();
	
	$communication = usam_get_company_means_communication( $company_data['id'] );
	$data = array_merge ($company_data, $communication);
	return $data;	
}

function usam_update_company( $data, $colum = 'id' )
{	
	if ( !isset($data[$colum]) )
		return false;
	
	$value = $data[$colum];
	
	$company = new USAM_Company( $value, $colum );	
	$company->set( $data );	
	return $company->save();
}

function usam_insert_company( $value )
{	
	$company = new USAM_Company( $value );	
	$company->save();
	return $company->get('id');		 
}

function usam_delete_company( $value, $colum = 'id' )
{	
	$company = new USAM_Company( $value, $colum );
	return $company->delete();		 
}

// Получить параметры счета
function usam_get_acc_number( $id )
{	
	$cache_key = 'usam_get_acc_number';
	$cache = wp_cache_get( $id, $cache_key );
	if( $cache === false )
	{	
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_COMPANY_ACC_NUMBER . " WHERE id='%d' ", $id );
		$cache = $wpdb->get_row( $sql, ARRAY_A );
	 
		wp_cache_set( $cache_key, $cache, $id );
	}
	return $cache;
}

function usam_get_display_company_by_acc_number( $bank_account_id )
{
	$bank_account = usam_get_acc_number( $bank_account_id );	
	$select_bank_account = '';
	if ( !empty($bank_account) )
	{
		$company = new USAM_Company( $bank_account['company_id'] );	
		$company_data = $company->get_data();
		$currency = usam_get_currency_name( $bank_account['currency'] );
		$select_bank_account = $company_data['name']." - ".$bank_account['name']." ".$bank_account['number']." $currency";
	}
	return $select_bank_account;
}

function usam_get_company_by_acc_number( $bank_account_id )
{
	$bank_account = usam_get_acc_number( $bank_account_id );	
	if ( !empty($bank_account) )
	{
		$company = new USAM_Company( $bank_account['company_id'] );	
		$company_data = $company->get_data();
		$meta = $company->get_meta();
		
		$communications = usam_get_company_means_communication( $company_data['id'] );
		foreach ( $communications as $key => $communication ) 	
		{
			foreach ( $communication as $data ) 
			{
				$company_data[$key] = $data['value'];
				break;
			}			
		}		
		$result = array_merge ($meta, $company_data);	
		$result['full_contactaddress'] = $meta['contactpostcode'].', '.', '.$meta['contactaddress'];		
		if ( !empty($meta['contactlocation']) )
		{
			$locations = usam_get_array_locations_up( $meta['contactlocation'], 'name', 'code' );
			if ( !empty($locations) )
				$result['full_contactaddress'] = $meta['contactpostcode'].', '.$locations['country'].', '.$locations['city'].', '.$meta['contactaddress'];				
		}			
		$result['full_legaladdress'] = $meta['legalpostcode'].', '.$meta['legaladdress'];
		if ( !empty($meta['legallocation']) )
		{
			$locations = usam_get_array_locations_up( $meta['legallocation'], 'name', 'code' );
			if ( !empty($locations) )
				$result['full_legaladdress'] = $meta['legalpostcode'].', '.$locations['country'].', '.$locations['city'].', '.$meta['legaladdress'];		
		}		
		$result['bank_details'] = __('р/с','usam').' '.$bank_account['number'].' '.$bank_account['name'].' '.__( 'БИК', 'usam' ).' '.$bank_account['bic'].' '.__('кор/с','usam').' '.$bank_account['bank_ca'];
		$result['bank_account'] = $bank_account;		
	}
	else
		$result = array();
	return $result;
}

function usam_get_companies_industry( )
{
	return array( 
		'it' => __('Информационные технологии','usam'),
		'telecom' => __('Телекоммуникации и связь','usam'),
		'manufacture' => __('Производство','usam'),
		'banking' => __('Банковские услуги','usam'),
		'consulting' => __('Консалтинг','usam'),
		'finance' => __('Финансы','usam'),
		'government' => __('Правительство','usam'),
		'delivery' => __('Доставка','usam'),
		'entertainmet' => __('Развлечения','usam'),
		'notprofit' => __('Не для получения прибыли','usam'),	
		'other' => __('Другое','usam'),
	);	
}

function usam_get_companies_types( )
{
	return array( 
		'customer' => __('Клиент','usam'),
		'prospect' => __('Будущий клиент','usam'),
		'partner' => __('Партнер','usam'),
		'reseller' => __('Реселлер','usam'),
		'competitor' => __('Конкурент','usam'),
		'investor' => __('Инвестор','usam'),
		'integrator' => __('Интегратор','usam'),
		'press' => __('СМИ','usam'),
		'own' => __('Своя','usam'),	
		'other' => __('Другое','usam'),
	);		
}

function usam_get_name_type_company( $type )
{	
	$types = usam_get_companies_types();
	if ( isset($types[$type]) )
		return $types[$type];
	else
		return '';
}

function usam_get_name_industry_company( $type )
{	
	$industry = usam_get_companies_industry();
	if ( isset($industry[$type]) )
		return $industry[$type];
	else
		return '';
}

function usam_get_name_group_company( $group )
{	
	$option = get_option('usam_crm_company_group', array() );
	$company_groups = maybe_unserialize( $option );	
	if ( isset($company_groups[$group]) )
		return $company_groups[$group];
	else
		return '';
}