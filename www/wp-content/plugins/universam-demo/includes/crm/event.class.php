<?php
class USAM_Event
{
	// строковые
	private static $string_cols = array(
		'date_insert',		
		'title',		
		'description',
		'end',
		'start',
		'date_time',			
		'type',			
		'color',			
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'user_id',		
		'manager_id',
		'reminder',	
		'importance',
		'calendar',	
		'status',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_event' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_event' );	
		do_action( 'usam_event_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_Event( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_event' );	
		do_action( 'usam_event_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_event_before_delete', $id );
		$wpdb->query("DELETE FROM ".USAM_TABLE_EVENT_RELATIONSHIPS." WHERE event_id = '$id'");		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_EVENTS." WHERE id = '$id'");		
		self::delete_cache( $id );		
		do_action( 'usam_event_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_EVENTS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_event_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_event_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_event_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_event_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_event_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}		
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_event_pre_save', $this );	
		$where_col = $this->args['col'];

		if ( isset($this->data['start']) && isset($this->data['end']) && $this->data['start'] > $this->data['end'] )
		{
			$time = strtotime($this->data['start']);		
			$this->data['end'] = date("Y-m-d H:i:s", mktime(date("H", $time)+1, 0, 0, date("m", $time), date("d", $time), date("Y", $time)));
		}		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_event_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$this->data = apply_filters( 'usam_event_update_data', $this->data );			
			$format = $this->get_data_format(  );
			$this->data_format( );					
			$data = $this->data;
			
			$str = array();
			foreach ( $format as $key => $value ) 
			{
				if ( $data[$key] == null )
				{
					$str[] = "`$key` = NULL";
					unset($data[$key]);
				}
				else
					$str[] = "`$key` = '$value'";	
			}				
			$sql = "UPDATE `".USAM_TABLE_EVENTS."` SET ".implode( ', ', $str )." WHERE $where_col = '$where_format' ";	
			$result = $wpdb->query( $wpdb->prepare( $sql, array_merge( array_values( $data ), array( $where_val ) ) ) );
	
			do_action( 'usam_event_update', $this );			
		} 
		else 
		{   
			
		} 		
		do_action( 'usam_event_save', $this );

		return $result;
	}
}

function usam_get_event( $id, $colum = 'id' )
{
	$_event = new USAM_Event( $id, $colum );
	return $_event->get_data( );	
}

function usam_delete_event( $id ) 
{
	global $wpdb;
	$result = $wpdb->query( $wpdb->prepare("DELETE FROM " . USAM_TABLE_EVENTS . " WHERE id = '%d'", $id) );
	return $result;
}

// Вставить задачу
function usam_insert_event( $data ) 
{
	$_event = new USAM_Event( $data );
	$_event->save();
	return $_event->get('id');
}

function usam_update_event( $id, $data ) 
{
	$_event = new USAM_Event( $id );
	$_event->set( $data );
	return $_event->save();
}



//Список действий
function usam_get_event_action_lists( $args ) 
{
	global $wpdb;	
	
	$query_where = '1=1';	
	if ( !empty($args['event_id']) )
	{
		$event_id = implode( ',',  (array)$args['event_id'] );		
		$query_where .= " AND ".USAM_TABLE_EVENT_ACTION_LIST.".event_id IN ($event_id)";
	}	
	$data = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_EVENT_ACTION_LIST." WHERE $query_where" );
	return $data;	
}

function usam_insert_event_action( $insert ) 
{
	global $user_ID, $wpdb;
	
	$formats = array( 'event_id' => '%d', 'name' => '%s','made' => '%d','sort' => '%d' );	
	$format = array();
	$insert['date_insert'] = date( "Y-m-d H:i:s" );	
	if ( !isset($insert['user_id']) )
		$insert['user_id'] = $user_ID;
	
	foreach ( $insert as $key => $value ) 
	{		
		if ( isset($formats[$key]) )		
			$format[] = $formats[$key];	
		else
			unset($insert[$key]);
	}	
	$result = $wpdb->insert( USAM_TABLE_EVENT_ACTION_LIST, $insert, $format ); 
	return $wpdb->insert_id;
}


function usam_delete_event_action( $id ) 
{
	global $wpdb;

	$result = $wpdb->delete( USAM_TABLE_EVENT_ACTION_LIST, array( 'id' => $id ), array('%d') ); 
	return $result;
}

function usam_update_event_action( $id, $update ) 
{
	global $wpdb;	
	
	$formats = array( 'event_id' => '%d', 'name' => '%s','made' => '%d','sort' => '%d' );
	$format = array();
	foreach ( $update as $key => $value ) 
	{		
		if ( isset($formats[$key]) )		
			$format[] = $formats[$key];	
		else
			unset($update[$key]);
	}		
	$result = $wpdb->update( USAM_TABLE_EVENT_ACTION_LIST, $update, array( 'id' => $id ), $format, array( '%d' ) ); 
	return $result;
}


function usam_get_event_object( $args ) 
{
	global $wpdb;
	
	if ( empty($args['event_id']) || empty($args['object_id']) || empty($args['object_type']) )
		return false;
	
	$event_id = absint($args['event_id']);
	$object_id = absint($args['object_id']);
	$object_type = $args['object_type'];
	
	$data = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_EVENT_RELATIONSHIPS." WHERE event_id = '$event_id' AND object_id = '$object_id' AND object_type = '$object_type'", ARRAY_A );	
	return $data;	
}

function usam_delete_event_object( $args ) 
{
	global $wpdb;
	
	if ( empty($args['event_id']) || empty($args['object_id']) || empty($args['object_type']) )
		return false;
	
	$event_id = absint($args['event_id']);
	$object_id = absint($args['object_id']);
	$object_type = $args['object_type'];
	
	return $wpdb->delete( USAM_TABLE_EVENT_RELATIONSHIPS, array( 'event_id' => $event_id, 'object_id' => $object_id, 'object_type' => $object_type ), array( '%d', '%d', '%s' ) );	
}

function usam_get_event_objects_all( $event_id ) 
{	
	$cache_key = 'usam_event_relationships';
	$cache = wp_cache_get( $event_id, $cache_key );
	if( $cache === false )
	{	
		$cache = usam_get_event_objects( array( 'event_id' => $event_id ) );
		wp_cache_set( $event_id, $cache, $cache_key );
	}	
	return $cache;
}

function usam_get_event_objects( $args ) 
{
	global $wpdb;
		
	$query_where = '1=1';	
	if ( !empty($args['event_id']) )
	{
		$event_id = implode( ',',  (array)$args['event_id'] );		
		$query_where .= " AND ".USAM_TABLE_EVENT_RELATIONSHIPS.".event_id IN ($event_id)";
	}
	if ( !empty($args['object_id']) )
	{
		$object_id = implode( ',',  (array)$args['object_id'] );		
		$query_where .= " AND ".USAM_TABLE_EVENT_RELATIONSHIPS.".object_id IN ($object_id)";
	}
	if ( !empty($args['object_type']) )
	{
		$object_type = implode( "','",  (array)$args['object_type'] );		
		$query_where .= " AND ".USAM_TABLE_EVENT_RELATIONSHIPS.".object_type IN ('$object_type')";
	}
	$data = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_EVENT_RELATIONSHIPS." WHERE $query_where" );	
	return $data;	
}

function usam_set_event_object( $object ) 
{
	global $wpdb;	
	
	if ( empty($object['event_id']) || empty($object['object_id']) || empty($object['object_type']) )
		return false;
	
	$sql = "INSERT INTO `".USAM_TABLE_EVENT_RELATIONSHIPS."` (`event_id`,`object_type`,`object_id`) VALUES ('%d','%s','%d') ON DUPLICATE KEY UPDATE `object_id`='%d'";	
	$insert = $wpdb->query( $wpdb->prepare($sql, $object['event_id'], $object['object_type'], $object['object_id'], $object['object_id'] ) );	
	return $wpdb->insert_id;
}

function usam_get_name_type_event_object( $object_type ) 
{
	$result = '';
	switch ( $object_type ) 
	{
		case 'product' :
			$result = __('Товар','usam');
		break;
		case 'order' :
			$result = __('Заказ','usam');
		break;	
		case 'review' :
			$result = __('Страница с отзывами','usam');
		break;
		case 'questions' :
			$result = __('Сообщение покупателя','usam');
		break;
		case 'email' :
			$result = __('Письмо','usam');
		break;	
		case 'contact' :
			$result = __('Контакт','usam');
		break;
		case 'company' :
			$result = __('Компания','usam');
		break;	
		case 'suggestion' :
			$result = __('Коммерческое предложение','usam');
		break;	
		case 'invoice' :
			$result = __('Счет','usam');
		break;		
	}	
	return $result;
}

function usam_get_type_events( $type ) 
{		
	$result = '';
	switch ( $type ) 
	{
		case 'affair' :
			$result = __('Дело','usam');
		break;
		case 'meeting' :
			$result = __('Встреча','usam');
		break;	
		case 'call' :
			$result = __('Звонок','usam');
		break;
		case 'event' :
			$result = __('Событие','usam');
		break;
		case 'email' :
			$result = __('Сообщение','usam');
		break;
		case 'task' :
			$result = __('Задание','usam');
		break;			
	}
	return $result;
}

// Добавить системное событие
function usam_insert_system_event( $data, $objects ) 
{
	$data['status'] = 1;	
	if ( !empty($task['date_time'] ) )
		$task['reminder'] = 1;		
	$data['type'] = 'event';
	$data['calendar'] = usam_get_id_system_calendar('affair');	
	$event_id = usam_insert_event( $data );	
	foreach ( $objects as $object ) 
	{
		$object['event_id'] = $event_id;
		usam_set_event_object( $object ); 
	}
	return $event_id;
}

function usam_set_affair( $data, $objects ) 
{
	global $user_ID;
	
	if ( empty($data['calendar']) )
		$data['calendar'] = usam_get_id_system_calendar('affair');	
	
	if ( empty($data['status']) )
	{
		$data['status'] = 3;	
		$data['end'] = date( "Y-m-d H:i:s" );	
	}
	
	if ( empty($data['type']) )
		$data['type'] = 'affair';
	
	if ( empty($data['start']) )
		$data['start'] =  date( "Y-m-d H:i:s" );	
	
	if ( empty($data['end']) )
		$data['end'] = date( "Y-m-d H:i:s" );		
	
	$data['user_id'] = $user_ID;		
	
	$event_id = usam_insert_event( $data );	
	
	if ( !isset($objects[0]) )
		$objects = array( $objects );	
	
	foreach ( $objects as $object ) 
	{
		$object['event_id'] = $event_id;
		usam_set_event_object( $object ); 
	}
	return $event_id;
}

function usam_work_on_task( $data, $objects ) 
{	
	global $user_ID;
	$record = get_option('usam_record_affairs_managers', array() );
	if ( !empty($record) && in_array($user_ID, $record))
	{				
		$today = getdate();			
		// Если нет объекта, то отсортируем по дате и узнаем было ли не завершеное задание сегодня
		$args = array( 'number' => 1, 'date_query' => array( 'year' => $today["year"], 'monthnum' => $today["mon"], 'day' => $today["mday"], 'column' => 'date_insert' ), 'type' => 'work', 'status__not_in' => 3, 'user_id' => $user_ID );	
		$event = usam_get_tasks( $args );		
		
		$data['calendar'] = usam_get_id_system_calendar('affair');			
		$data['status'] = empty($data['status'])?1:$data['status'];
		
		if( !empty($event) )		
		{
			$id = $event->id;
			$update = array( 'end' => date( "Y-m-d H:i:s" ), 'status' => 3 );	
			usam_update_event( $event->id, $update );
		}		
		$data['start'] = date( "Y-m-d H:i:s" );	
		$data['type'] = 'work';
		$id = usam_insert_event( $data );
		if ( !isset($objects[0]) )
			$objects = array( $objects );				
		
		foreach ( $objects as $object ) 
		{
			$object['event_id'] = $id;
			usam_set_event_object( $object ); 
		}				
		$ssl = 'http';
		if ( is_ssl() )
			$ssl = 'https';				
		
		if ( $data['status'] != 3 )
		{
			?>
			<script type="text/javascript">
				jQuery(window).bind("beforeunload", function() { 
					var data = {
							action     :  'affair_complete',
							id         :  '<?php echo $id; ?>',									
							nonce      :  '<?php echo wp_create_nonce( 'affair_complete' ) ?>'
						};					
					jQuery.post('<?php echo admin_url( 'admin-ajax.php', $ssl ); ?>', data, function(response) 
					{								
			
					});										
				})	
			</script>
			<?php		
		}
	}
	else
		return false;
}

function usam_get_display_event_object( $object )
{
	$out = usam_get_name_type_event_object ( $object->object_type );
	switch ( $object->object_type ) 
	{
		case 'product' : 
			$out .= " - <a href='".usam_product_url( $object->object_id )."'>".get_the_title( $object->object_id )."</a>";					
		break;
		case 'order' :
			$out .= " - ".usam_get_link_order( $object->object_id );	
		break;	
		case 'review' :
			
		break;
		case 'questions' :		
			$out .= " - <a href='".add_query_arg( array('page' => 'feedback', 'tab' => 'questions', 'action' => 'edit', 'id' => $object->object_id ), admin_url('admin.php') )."'>$object->object_id</a>";	
		break;
		case 'email' :
			$email = usam_get_email( $object->object_id );					
			$out .= " - <a href='".add_query_arg( array('page' => 'feedback', 'tab' => 'email', 'f' => $email['folder'], 'm' => $email['mailbox_id'], 'id' => $object->object_id ), admin_url('admin.php') )."'>".$email['subject']."</a>";	
		break;	
		case 'contact' :				
			$contact = usam_get_contact( $object->object_id );
			$out .= " - <a href='".add_query_arg( array('page' => 'crm', 'tab' => 'contacts', 'action' => 'view', 'id' => $object->object_id ), admin_url('admin.php') )."'>".$contact['lastname'].' '. $contact['firstname']."</a>";
		break;
		case 'company' :
			$company = usam_get_company( $object->object_id );
			$out .= " - <a href='".add_query_arg( array('page' => 'crm', 'tab' => 'company', 'action' => 'view', 'id' => $object->object_id ), admin_url('admin.php') )."'>".$company['name']."</a>";
		break;	
		case 'suggestion' :	
			$document = usam_get_document( $object->object_id );
			$out .= " - <a href='".add_query_arg( array('page' => 'crm', 'tab' => 'suggestions', 'action' => 'view', 'id' => $object->object_id ), admin_url('admin.php') )."'>".$document['name']."</a>";
		break;			
		case 'invoice' :
			$document = usam_get_document( $object->object_id );
			$out .= " - <a href='".add_query_arg( array('page' => 'crm', 'tab' => 'invoice', 'action' => 'view', 'id' => $object->object_id ), admin_url('admin.php') )."'>".$document['name']."</a>";
		break;			
	}	
	return $out;
}


function usam_set_event_user( $event_id, $user_id ) 
{
	global $wpdb;	
	$sql = "INSERT INTO `".USAM_TABLE_EVENT_USERS."` (`event_id`,`user_id`) VALUES ('%d','%d') ON DUPLICATE KEY UPDATE `user_id`='%d'";	
	return $wpdb->query( $wpdb->prepare($sql, $event_id, $user_id, $user_id ));	
}

function usam_delete_event_user( $event_id, $user_id ) 
{
	global $wpdb;	
	return $wpdb->delete( USAM_TABLE_EVENT_USERS, array( 'event_id' => $event_id, 'user_id' => $user_id ), array( '%d', '%d' ) );	
}

function usam_get_event_users( $event_id ) 
{
	global $wpdb;	
	$user_ids = $wpdb->get_col( "SELECT user_id FROM ".USAM_TABLE_EVENT_USERS." WHERE event_id = '$event_id'" );	
	return $user_ids;	
}


function usam_get_event_metadata( $object_id, $meta_key = '', $single = true) 
{	
	return usam_get_metadata('event', $object_id, USAM_TABLE_EVENT_META, $meta_key, $single );
}

function usam_update_event_metadata($object_id, $meta_key, $meta_value, $prev_value = false ) 
{ 
	return usam_update_metadata('event', $object_id, $meta_key, $meta_value, USAM_TABLE_EVENT_META, $prev_value );
}
?>