<?php
function usam_add_contact_document( $document_id, $customer_id ) 
{
	global $wpdb;	
	
	if ( empty($document_id) )
		return false;
		
	if ( empty($customer_id) )
		return false;	
					
	$sql = "INSERT INTO `".USAM_TABLE_CUSTOMER_DOCUMENT."` (`customer_id`,`document_id`) VALUES ('%d','%d') ON DUPLICATE KEY UPDATE `customer_id`='%d'";			
	$update = $wpdb->query( $wpdb->prepare($sql, $customer_id, $document_id, $customer_id ) );
	return $update;
}

function usam_delete_contact_document( $document_id, $customer_id = '' ) 
{
	global $wpdb;

	if ( empty($customer_id) )
		return false;
			
	$delete = array( 'document_id' => $document_id );
	$format = array('%d');
	
	if ( !empty($customer_id) )
	{
		$delete['customer_id'] = $customer_id;
		$format[] = '%d';
	}	
	$result = $wpdb->delete( USAM_TABLE_CUSTOMER_DOCUMENT, $delete, $format );				
	return $result;
}
	
function usam_get_contacts_document( $qv = array() )
{ 
	global $wpdb;	

	if ( isset($qv['fields']) )
	{
		$fields = $qv['fields'] == 'all'?'*':$qv['fields'];
	}
	else
		$fields = '*';
	
	$_where[] = '1=1';
	
	if ( isset($qv['document_id']) )
		$_where[] = "document_id = '".$qv['document_id']."'";	
	
	if ( isset($qv['ids']) )
		$_where[] = "id IN( '".implode( "','", $qv['ids'] )."' )";
	
	$where = implode( " AND ", $_where);	
	if ( !empty($qv['condition']) ) 
	{		
		foreach ( $qv['condition'] as $condition )
		{					
			$select = '';
			if ( empty($condition['col']) )
				continue;
			
			switch ( $condition['col'] )
			{								
				default:				
					$select = $condition['col'];			
				break;				
			}
			if ( $select == '' )
				continue;
			
			$compare = "=";	
			switch ( $condition['compare'] ) 
			{
				case '<' :
					$compare = "<";					
				break;
				case '=' :
					$compare = "=";					
				break;	
				case '!=' :
					$compare = "!=";					
				break;
				case '>' :
					$compare = ">";					
				break;				
			}
			$value = $condition['value'];
			
			if ( empty($condition['relation']) )
				$relation = 'AND';
			else
				$relation = $condition['relation'];
			
			$where .= $wpdb->prepare( " $relation $select $compare %s", $value );			
		}
	}	
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'document_id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'DESC';	
	if ( isset($qv['output_type']) )	
		$output_type = $qv['output_type'];	
	else
		$output_type = 'ARRAY_A';
	
	if ( $where != '' )
		$where = " WHERE $where ";
	
	$result = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_CUSTOMER_DOCUMENT." $where $orderby $order", $output_type );
	return $result;
}