<?php
class USAM_Contact 
{	
	 // строковые
	private static $string_cols = array(	
		'date_insert',
		'sex',
		'lastname',
		'firstname',
		'patronymic',
		'birthday',		
		'post',
		'address',
		'address2',
		'postal_code',	
		'about_contact',
		'contact_source',
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'user_id',	
		'manager_id',	
		'open',		
		'foto',
		'company_id',
		'location_id',		
	);

	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
			
		return false;
	}
	/**
	 *Содержит значения извлекаются из БД
	 * @since 4.9
	 */
	private $data    = array();
	private $fetched = false;	
	private $args    = array( 'col'   => '', 'value' => '' );
	private $exists  = false;
	
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( false === $value )
			return;

		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'user_id' ) ) )
			return;

		$this->args = array( 'col' => $col, 'value' => $value );	
	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{
			$this->data = wp_cache_get( $value, 'usam_contact' );			
		}
		// кэш существует
		if ( $this->data ) 
		{	
			$this->fetched = true;
			$this->exists = true;			
		}	
		else
			$this->fetch();	
	}

	/**
	 * Обновить кеш
	 */
	public static function update_cache( &$contact ) 
	{		
		$id = $contact->get( 'id' );
		wp_cache_set( $id, $contact->data, 'usam_contact' );	
		do_action( 'usam_contact_update_cache', $contact );
	}

	/**
	 * Удалить кеш
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{		
		$contact = new USAM_Contact( $value, $col );
		wp_cache_delete( $contact->get( 'id' ), 'usam_contact' );	
		wp_cache_delete( $contact->get( 'id' ), 'usam_contact_means_communication' );
		do_action( 'usam_contact_update_cache', $contact, $value, $col );
	}

	/**
	 * Удаляет подписчика
	 * @since 4.9	
	 */
	public function delete( ) 
	{
		global $wpdb;	
		
		$id = $this->get('id');		
		do_action( 'usam_contact_before_delete', $id );
		
		$sql = $wpdb->prepare( "DELETE FROM " . USAM_TABLE_CONTACTS . " WHERE id = %d", $id );		
		$wpdb->query( $sql );	
		
		$sql = $wpdb->prepare( "SELECT id FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE contact_id = '%d' AND customer_type = 'contact'", $id );
		$delete_ids = $wpdb->get_col( $sql );			
		if ( !empty($delete_ids) )
		{
			$wpdb->query( "DELETE FROM " . USAM_TABLE_SUBSCRIBER_LISTS . " WHERE id_communication IN (".implode(',', $delete_ids).")" );	
			
			$args = array( 'contact_id' => $id, 'customer_type' => 'contact' );			
			usam_delete_means_communication( $args );
		}
		self::delete_cache( $id );		
		do_action( 'usam_contact_delete', $id );
	}	

	/**
	 * Выбирает фактические записи из базы данных
	 * @since 4.9
	 */
	private function fetch() 
	{	
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );		
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_CONTACTS . " WHERE {$col} = {$format}", $value );

		$this->exists = false;
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{	
			$this->exists = true;
			$this->data = apply_filters( 'usam_contact_data', $data );			
			self::update_cache( $this );
		}
		do_action( 'usam_contact_fetched', $this );
		$this->fetched = true;
	}	

	/**
	 * Проверить существует ли строка в БД
	 */
	public function exists() 
	{
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства из БД
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_contact_get_property', $value, $key, $this );
	}

	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_contact_get_data', $this->data, $this );
	}
	
	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );
		}
		$properties = apply_filters( 'usam_contact_set_properties', $properties, $this );	

		$this->fetch();		
		if ( ! is_array( $this->data ) )
			$this->data = array();

		$this->data = array_merge( $this->data, $properties );
		return $this;
	}

	/**
	 * Вернуть формат столбцов таблицы
	 * @since 4.9	
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function check_duplicates( ) 
	{
		global $wpdb;
		
		if ( $this->data['lastname'] != '' && $this->data['firstname'] != '' )
		{
			$result = $wpdb->get_col( $wpdb->prepare("SELECT id FROM ".USAM_TABLE_CONTACTS." WHERE lastname = '%s' AND firstname = %s", $this->data['lastname'], $this->data['firstname']) );	
			if ( !empty($result) )
				return $result[0];
		}		
	/*	$keys = array( 'email', 'phone', 'site', 'social' );		
		foreach( $keys as $type )
		{
			if ( isset($this->data[$type]) && is_array($this->data[$type]) )
			{
				foreach( $this->data[$type] as $id => $data)
				{	
					if ( !is_numeric($id) )
					{
						$result = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE type = '%s' AND value = '%s'", $type, $data['value']));						
						if ( $result > 0 )
							unset($this->data[$type][$id]);
					}
				}
				if ( empty($this->data[$type]) )
					unset($this->data[$type]);
			}
		}			
	//	if ( empty($this->data['email']) && empty($this->data['phone']) )
	//		return true;
		*/
		return false;
	}
	
	/**
	 * Сохраняет в базу данных
	 * @since 4.9
	 */
	public function save()
	{
		global $wpdb;

		do_action( 'usam_contact_pre_save', $this );	
		
		$where_col = $this->args['col'];
		$result = false;		
		if ( $where_col ) 
		{	// обновление			
			$subscriber_id = $this->get('id');
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_contact_pre_update', $this );
			self::delete_cache( $where_val, $where_col );					
			$this->data = apply_filters( 'usam_contact_update_data', $this->data );			
			$format = $this->get_data_format( );
					
			foreach( $this->data as $key => $value)
			{										
				if ( $key == 'birthday' )
					if ( empty($value) )
						$set[] = "`{$key}`=NULL";
					else					
						$set[] = "`{$key}`='".date( "Y-m-d H:i:s", strtotime( $value ) )."'";
				else
					$set[] = "`{$key}`='{$value}'";						
			}	
			$result = $wpdb->query( $wpdb->prepare("UPDATE `".USAM_TABLE_CONTACTS."` SET ".implode( ', ', $set )." WHERE $where_col ='$where_format'", $where_val) );
			do_action( 'usam_contact_update', $this );
		} 
		else 
		{   // создание	
			do_action( 'usam_contact_pre_insert' );	
			
			if ( empty($this->data['user_id']) )
				$this->data['user_id'] = 0;
			else
			{
				$result = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM ".USAM_TABLE_CONTACTS." WHERE user_id = '%s'", $this->data['user_id'] ));	
				if ( $result > 0 )
					$this->data['user_id'] = 0;
			}
			
			if ( !isset($this->data['lastname']) )
				$this->data['lastname'] = '';
			else
				$this->data['lastname'] = trim($this->data['lastname']);						
			
			if ( !isset($this->data['firstname']) )
				$this->data['firstname'] = '';
			else
				$this->data['firstname'] = trim($this->data['firstname']);
			
			if ( $id = $this->check_duplicates( ) )
			{		
				$this->args = array('col' => 'id', 'value' => $id );			
				$this->fetch();	
				return $id;			
			}				
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );				
			$this->data = apply_filters( 'usam_contact_insert_data', $this->data );				
			$format = $this->get_data_format( );			
		
			$result = $wpdb->insert( USAM_TABLE_CONTACTS, $this->data, $format );	
			if ( $result ) 
			{				
				$this->set( 'id', $wpdb->insert_id );			
				
				// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col'   => 'id',  'value' => $this->get( 'id' ) );				
				do_action( 'usam_contact_insert', $this );
			}			
		} 		
		do_action( 'usam_contact_save', $this );
		return $result;
	}
		
	/*public function to_array() {
		return get_object_vars( $this->data );
	}	*/
}

function usam_get_contact( $value, $colum = 'id' )
{	
	$contact = new USAM_Contact($value, $colum);	
	$contact_data = $contact->get_data();	
	if ( !empty($contact_data) )
	{
		$communication = usam_get_contact_means_communication( $contact_data['id'] );
		$contact_data = array_merge ($contact_data, $communication);
	}
	return $contact_data;	
}

function usam_update_contact( $data, $colum = 'id' )
{	
	if ( !isset($data[$colum]) )
		return false;
	
	$value = $data[$colum];
	
	$contact = new USAM_Contact( $value, $colum );	
	$contact->set( $data );	
	return $contact->save();
}

function usam_insert_contact( $value, $communications = array() )
{	
	$contact = new USAM_Contact( $value );	
	$contact->save();
	
	foreach ( $communications as $communication ) 
	{
		usam_insert_communication( $communication );
	}
	return $contact->get('id');		 
}

function usam_delete_contact( $value, $colum = 'id' )
{	
	$subscriber = new USAM_Contact( $value, $colum );
	return $subscriber->delete();		 
}


function usam_get_contact_means_communication( $id )
{	
	$result = usam_get_customer_communication( $id, 'contact' );
	return $result;
}


function usam_get_name_contact_source( $contact_source )
{	
	$option = get_option('usam_crm_contact_source', array() );
	$contact_sources = maybe_unserialize( $option );
	
	$result = '';
	foreach ( $contact_sources as $value ) 
	{
		if ( $value['id'] == $contact_source )
		{
			$result = $value['name'];
			break;
		}
	}
	return $result;
}