<?php

class USAM_Admin_Bar_Menu
{
	public function __construct( ) 
	{	
		if ( is_admin() )
			add_action('admin_menu', array($this, "event_counting"), 1);
		else
			add_action('admin_bar_menu', array($this, "event_counting"), 1);
		add_action('admin_bar_menu', array($this, "admin_bar"), 9999);		
		add_action( 'wp_head', array($this, "admin_bar_css") );				
		add_action('admin_footer', array( $this,'admin_bar_css' ) );
	}	
	
	public function event_counting()
	{
		global $wpdb, $usam_events, $user_ID;
		
		if ( usam_check_current_user_role('administrator') || usam_check_current_user_role('editor') || usam_check_current_user_role('shop_manager') || usam_check_current_user_role('shop_crm') )
		{
			/*$timestamp   = mktime(0,0,0,date('m'),date('d')-1,date('Y'));		
			$date_class = new DateTime();
			$date_class->modify('-3 day');
			$date = $date_class->format('Y-m-d H:i:s');		*/
			$usam_events['order'] = (int)$wpdb->get_var("SELECT COUNT(*) FROM `".USAM_TABLE_ORDERS."` WHERE `status` IN ('received','incomplete_sale')");		
			$usam_events['feedback'] = (int)$wpdb->get_var("SELECT COUNT(*) FROM `".USAM_TABLE_FEEDBACK."` WHERE `status` = '0'") + $wpdb->get_var("SELECT COUNT(id) FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE status = '0'");		
			$usam_events['reviews'] = (int)$wpdb->get_var("SELECT  COUNT(*) FROM `".USAM_TABLE_CUSTOMER_REVIEWS."` WHERE `status`= '0'");

			$sql = "SELECT COUNT(*) FROM ".USAM_TABLE_CHAT_DIALOGS." AS d LEFT JOIN ".USAM_TABLE_CHAT." AS c ON ( c.topic=d.id ) WHERE c.user_id != '$user_ID' AND c.status = '0' AND d.manager_id='$user_ID'";		
			$usam_events['chat'] = (int)$wpdb->get_var( $sql );				
			
			$mailboxes = usam_get_mailboxes( array( 'fields' => 'id', 'user_id' => $user_ID ) );				
			$email = 0;
			if ( !empty($mailboxes) )
			{							
				$reads = usam_get_email_folders( array('fields' => 'not_read', 'read' => 0, 'slug' => 'inbox', 'mailbox_id' => $mailboxes) );
				foreach ( $reads as $number ) 
					$email += $number;		
			}
			$usam_events['email'] = $email;	
			$usam_events['total'] = $usam_events['chat'] + $usam_events['order'] + $usam_events['feedback'] + $usam_events['reviews'] + $usam_events['email'];		
		}
	}
		
	public function admin_bar()
	{		
		global $usam_events, $wp_admin_bar, $user_ID;		
		
		if ( usam_check_current_user_role('administrator') || usam_check_current_user_role('editor') || usam_check_current_user_role('shop_manager') || usam_check_current_user_role('shop_crm') )
		{		
			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-tasks-sub',
				'parent' => 'new-content', // родительский пункт
				'title' => '<span class="ab-icon1"></span><span class="ab-label1">'.__('Задание','usam').'</span>',
				'href'  => admin_url('admin.php?page=crm&tab=tasks&action=add'),
				'meta'  => array(
					'title' => '',             //Текст всплывающей подсказки
					'target' => '',             //_blank
					'class' => 'usam-tasks-link' 
				),
			));			
			$purchaselogs_page = admin_url('index.php?page=orders');	
			$feedback_page = admin_url('admin.php?page=feedback');	
			$events = array(
				array( 'name' => __('Новый заказ','usam'), 'n' => $usam_events['order'], 'link' => $purchaselogs_page, 'icon' => ''),		
				array( 'name' => __('Новый диалог','usam'), 'n' => $usam_events['chat'], 'link' => $feedback_page.'&tab=chat', 'icon' => ''),
				array( 'name' => __('Новое сообщение','usam'), 'n' => $usam_events['feedback'], 'link' => $feedback_page, 'icon' => 'dashicons-format-status'),			
				array( 'name' => __('Новый отзыв','usam'), 'n' => $usam_events['reviews'], 'link' => $feedback_page.'&tab=reviews', 'icon' => ''),
				array( 'name' => __('Новое письмо','usam'), 'n' => $usam_events['email'], 'link' => $feedback_page.'&tab=email', 'icon' => ''),
			);			
			if ( $usam_events['total'] > 0 )
				$number_message = usam_get_style_number_message( $usam_events['total'] );
			else
				$number_message = '';
			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-events-main-item',
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('События','usam').$number_message.'</span>',
				'href'  => '#', // Ваша ссылка 
				'meta'  => array(
					'title' => 'События вашего магазина',            
				),
			));				
			
			foreach ( $events as $key => $event )
			{	
				$wp_admin_bar->add_menu( array(
					'id'    => 'usam-events-sub-item-'.$key,
					'parent' => 'usam-events-main-item', // родительский пункт
					'title' => '<span class="ab-icon1"></span><span class="ab-label1">'.$event['name'].' ('.$event['n'].')</span>',
					'href'  => $event['link'],
					'meta'  => array(
						'title' => '',             //Текст всплывающей подсказки
						'target' => '',             //_blank
						'class' => 'usam-events-link' // произвольный CSS класс ссылки
					),
				));
			}
			
			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-menu-crm-item',
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('Мои дела','usam').'</span>',
				'href'  => admin_url('admin.php?page=crm&tab=tasks'), 
				'meta'  => array(
					'title' => __('Управление делами менеджера интернет-магазина','usam'),            
				),
			));			
			$admin_bar_link = array(
				array( 'name' => __('Добавить задание','usam'),     'link' =>  admin_url('admin.php?page=crm&tab=tasks&action=add'), 'icon' => ''),
				array( 'name' => __('Календарь','usam'),        'link' =>  admin_url('admin.php?page=crm&tab=calendar'), 'icon' => ''),		
				array( 'name' => __('Отправить письмо','usam'), 'link' =>  admin_url('admin.php?page=feedback&tab=email&action=new'), 'icon' => ''),			
				array( 'name' => __('CRM','usam'), 'link' =>  admin_url('admin.php?page=crm'), 'icon' => ''),				
			);			
			foreach ( $admin_bar_link as $key => $menu )
			{	
				$wp_admin_bar->add_menu( array(
					'id'    => 'usam-menu-crm-sub-item-'.$key,
					'parent' => 'usam-menu-crm-item', // родительский пункт
					'title' => $menu['name'],
					'href'  => $menu['link'],
					'meta'  => array(
						'title' => '',             //Текст всплывающей подсказки
						'target' => '',             //_blank
						'class' => 'usam-events-link' // произвольный CSS класс ссылки
					),
				));
			}	

			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-menu-main-item',
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('Магазин','usam').'</span>',
				'href'  => '', 
				'meta'  => array(
					'title' => 'Продажи вашего магазина',            
				),
			));			
			$admin_bar_link = array(
				array( 'name' => __('Журнал продаж','usam').usam_get_style_number_message( $usam_events['order'] ), 'link' =>  $purchaselogs_page, 'icon' => ''),
				array( 'name' => __('Товары','usam'), 'link' =>  admin_url('edit.php?post_type=usam-product'), 'icon' => ''),	
				array( 'name' => __('Коммерческие предложения','usam'), 'link' =>  admin_url('admin.php?page=crm&tab=suggestions'), 'icon' => ''),
				array( 'name' => __('Счета','usam'), 'link' =>  admin_url('admin.php?page=crm&tab=invoice'), 'icon' => ''),
			);			
			foreach ( $admin_bar_link as $key => $menu )
			{	
				$wp_admin_bar->add_menu( array(
					'id'    => 'usam-menu-main-sub-item-'.$key,
					'parent' => 'usam-menu-main-item', // родительский пункт
					'title' => $menu['name'],
					'href'  => $menu['link'],
					'meta'  => array(
						'title' => '',             //Текст всплывающей подсказки
						'target' => '',             //_blank
						'class' => 'usam-events-link' // произвольный CSS класс ссылки
					),
				));
			}	
			$option = get_option( 'usam_set_events', '' );		
			if ( !empty( $option ) )
				$task_manager = unserialize( $option );	
		
			$number_events_message = '';
			if ( !empty($task_manager) )
			{
				$count = 0;
				foreach ( $task_manager as $key => $event )
				{					
					if ( !$event['status'] )
						$count++;
				}		
				if ( $count > 0)
					$number_events_message = usam_get_style_number_message( $count );
			}		
			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-menu-task_manager-item',
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('Диспетчер задач','usam').$number_events_message.'</span>',
				'href'  => '', 
				'meta'  => array(
					'title' => __('Задачи вашего магазина','usam'),            
				),
			));				
			if ( !empty($task_manager) )
			{			
				foreach ( $task_manager as $key => $event )
				{					
					if ( $event['status'] )
					{
						$title_status = __('выполнена','usam');
						$class = 'usam_performed';
					}
					else
					{
						$title_status = __('не выполнена','usam');
						$class = 'usam_not_performed';
					}				
					if ( $event['count'] == 0 )
						$p = 0;
					else
						$p = round($event['done']*100/$event['count'],0);
				
					$wp_admin_bar->add_menu( array(
						'id'    => 'usam-menu-sub-item-'.$key,
						'parent' => 'usam-menu-task_manager-item', // родительский пункт
						'title' => $event['title']." $p% <span class = 'delete' data-event_id='$key'>х</span>",
						'href'  => '',
						'meta'  => array(
							'title' => $title_status,             //Текст всплывающей подсказки
							'target' => '',             //_blank
							'class' => 'usam-task_manager-link '.$class // произвольный CSS класс ссылки
						),
					));
				}	
			}
			else
			{			
				$wp_admin_bar->add_menu( array(
						'id'    => 'usam-menu-sub-item-1',
						'parent' => 'usam-menu-task_manager-item', // родительский пункт
						'title' => __('Нет задач','usam'),
						'href'  => '',
						'meta'  => array(
							'title' => '',             //Текст всплывающей подсказки
							'target' => '',             //_blank
							'class' => 'usam-events-link' // произвольный CSS класс ссылки
						),
					));
			}		
			if ( usam_check_current_user_role('shop_crm') == false )
			{
			
				$status_online_consultant = usam_get_status_online_consultants();		
				if ( $status_online_consultant )
					$status_text = "<span class='selector_status_consultant active'>".__('Включен','usam')."</span>";
				else
					$status_text = "<span class='selector_status_consultant'>".__('Выключен','usam')."</span>";		
				
				$status_text = usam_get_style_number_message( $usam_events['chat'] )." $status_text";
				
				$wp_admin_bar->add_menu( array(
					'id'    => 'usam-menu-online_consultant-item',
					'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('Чат','usam').$status_text.'</span>',
					'href'  => '', 
					'meta'  => array(
						'title' => __('Чат','usam'),            
					),			
				));				
			}
/*
			$working_day = get_user_meta( $user_ID, 'usam_working_day', true );
			if ( empty($working_day) )
				$status_text = "<span class='selector_status_working_day stop'>".__('Выключен','usam')."</span>";
			elseif ( $working_day == 1 )
				$status_text = "<span class='selector_status_working_day pause'>".__('Пауза','usam')."</span>";				
			else
				$status_text = "<span class='selector_status_working_day play'>".__('Включен','usam')."</span>";			
						
			$wp_admin_bar->add_menu( array(
				'id'    => 'usam-menu-working_day-item',
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.__('Рабочий день','usam').$status_text.'</span>',
				'href'  => '', 
				'meta'  => array(
					'title' => __('Чат','usam'),            
				),			
			));		
			*/
	//		$classes = apply_filters( 'usam_debug_css_classes', array() );			
		}
		if ( usam_check_current_user_role('administrator') )
		{
			$wp_admin_bar->add_menu( array(
				'id'    => 'performance_site',
				'parent' => 'top-secondary',			
				'title' => '<span class="ab-icon"></span><span class="ab-label">'.round(memory_get_usage()/1024/1024, 2).' MB '.' | '.get_num_queries().' SQL | '.timer_stop().' '.__('сек','usam').'</span>',
			//	'href'  => '#', // Ваша ссылка 
				'meta'  => array(
					'title' => 'События вашего магазина',            
				),
			));		
		}
	}
	
	public function admin_bar_css()
	{	
		?>
		<style type="text/css">
		#wp-admin-bar-usam-menu-crm-item .ab-icon:before{ font-family: "dashicons" !important; content: "\f481" !important; }
		#wp-admin-bar-usam-events-main-item .ab-icon:before{ font-family: "dashicons" !important; content: "\f481" !important; }
		#wp-admin-bar-usam-menu-main-item .ab-icon:before{ font-family: "dashicons" !important; content: "\f116" !important; }		
		#wp-admin-bar-usam-menu-task_manager-item .ab-icon:before{ font-family: "dashicons" !important; content: "\f107" !important; }
		#wp-admin-bar-usam-menu-online_consultant-item .ab-icon:before{ font-family: "dashicons" !important; content: "\f482" !important; }
		#wp-admin-bar-usam-menu-task_manager-item .usam_not_performed .ab-item{border-left:2px solid red; margin-left:5px;}
		#wp-admin-bar-usam-menu-task_manager-item .usam_performed .ab-item{ border-left:2px solid #32CD32; margin-left:5px; }
		.number_events.count-0{display:none;}
		.number_events {display: inline-block; background-color: #d54e21!important; color: #fff!important; font-size: 9px!important; line-height: 17px!important; font-weight: 600!important; margin: 5px 0 0 2px!important; vertical-align: top!important; border-radius: 10px!important; z-index: 26!important;}
		span.number_events span { display: block!important; padding: 0 6px!important; line-height: 17px!important;}
		.selector_status_consultant{margin: 5px 0 0 2px!important;padding: 2px 14px!important; color: #ffffff!important; background: #2ecaf7!important; border-radius: 10px!important; line-height: 12px!important;font-size: 12px!important;}
		.selector_status_consultant.active{background: #a4286a!important;}
		
		#wp-admin-bar-usam-menu-working_day-item .stop:before{ font-family: "dashicons" !important; content: "\f330" !important; }
		#wp-admin-bar-usam-menu-working_day-item .pause:before{ font-family: "dashicons" !important; content: "\f523" !important; }
		#wp-admin-bar-usam-menu-working_day-item .play:before{ font-family: "dashicons" !important; content: "\f522" !important; }
		
		@media screen and (max-width: 1500px) 
		{
			#wpadminbar .ab-label{display:none}
		}			
		</style>
		<?php 
	} 
}
$admin_menu = new USAM_Admin_Bar_Menu();	

// Получает стиль отображения чисел новых сообщений
function usam_get_style_number_message( $number)
{	
	return " <span class='number_events count-".$number."'><span class='numbers'>".$number."</span></span>";	
}
?>