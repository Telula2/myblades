<?php
// Установка магазина
final class USAM_Install
{	
	public static $new_install = false;
	
	function __construct( ) 
	{	
		$version = get_option( 'universam_version', 0 );		

		if ( $version === false )
			add_option( 'universam_version', USAM_VERSION, '', 'yes' );
		else
			update_option( 'universam_version', USAM_VERSION );			
	}
	
	/**
	 * При деактивации
	 */
	public static function deactivate() 
	{			
		foreach ( wp_get_schedules() as $cron => $schedule ) 		
			wp_clear_scheduled_hook( "usam_{$cron}_cron_task" );			

		wp_clear_scheduled_hook("usam_product_day");

		$api = new Universam_API();
		$result = $api->universam_deactivation( );		
	}	
	
	public static function install() 
	{
		global $wpdb;		
			
		// надежным способом обнаружения, является ли это новая установка, проверив, существует ли USAM_TABLE_PRODUCTS_ORDER
		if ( $wpdb->get_var( "SHOW TABLES LIKE '" . USAM_TABLE_PRODUCTS_ORDER . "'" ) != USAM_TABLE_PRODUCTS_ORDER )
			self::$new_install = true;
		
		self::create_or_update_tables();
		self::add_option();		
		self::create_storages();
		self::create_system_pages();
		self::create_payment_gateway();
		self::create_shipping();
		
		USAM_Post_types::register_taxonomies();	
		include( USAM_FILE_PATH . '/admin/db/db-install/system_default_data.php' );	
		if ( self::$new_install )
		{
			$api = new Universam_API();
			$result = $api->set_free_license( );	

			USAM_Tracker::send_tracking_data( true );
			
			set_transient( '_usam_activation_redirect', 1, 30 );
			set_transient( 'usam_process_complete', true, DAY_IN_SECONDS );
			
			add_option( 'usam_db_version', USAM_DB_VERSION, '', 'yes' );		
			new USAM_Load_System_Default_Data( array('location_type','location','order_props_group','order_props','currency','country', 'order_status', 'product_attributes', 'tables') );
			
			include( USAM_FILE_PATH . '/admin/db/db-install/default_db_data.php' );			
		}	
		else
		{		
			$api = new Universam_API();
			$result = $api->universam_activation( );				
		}		
		self::add_role();					
		self::create_upload_directories();		
		
		flush_rewrite_rules( );
	}		
	
	protected static function create_payment_gateway() 
	{		
		global $wpdb;		
			
		$payment_gateways = usam_get_payment_gateways( array( 'active' => 'all', 'fields' => 'id' ) );
		if ( count($payment_gateways) == 0 )
		{
			$gateways = array(
				array( 'name' => __( 'Наличными курьеру', 'usam' ), 'description' => '', 'active' => 1, 'img' => '', 'sort' => 10, 'debug' => 0, 'type' => 'c', 'bank_account_id' => '', 'handler' => '', 'setting' => array( 'ipn' => 0, 'use' => 'all','shipping' => array(), 'gateway_option' => array() ) ),
				array( 'name' => __( 'Банковской картой курьеру', 'usam' ), 'description' => '', 'active' => 1, 'img' => '', 'sort' => 10, 'debug' => 0, 'type' => 'c', 'bank_account_id' => '', 'handler' => '', 'setting' => array( 'ipn' => 0, 'use' => 'all','shipping' => array(), 'gateway_option' => array() ) ),
				array( 'name' => __( 'На расчетный счет ВТБ', 'usam' ), 'description' => '', 'active' => 1, 'img' => '', 'sort' => 10, 'debug' => 0, 'type' => 'c', 'bank_account_id' => '', 'handler' => '', 'setting' => array( 'ipn' => 0, 'use' => 'all','shipping' => array(), 'gateway_option' => array() ) ),
				array( 'name' => __( 'Сбербанк', 'usam' ), 'description' => '', 'active' => 0, 'img' => '', 'sort' => 10, 'debug' => 0, 'type' => 'c', 'bank_account_id' => '', 'handler' => 'sberbank', 'setting' => array( 'ipn' => 0, 'use' => 'all','shipping' => array(), 'gateway_option' => array() ) ),
			);		
			foreach( (array)$gateways as $gateway )
			{
				$gateway['setting'] = serialize($gateway['setting']);				
				$wpdb->insert( USAM_TABLE_PAYMENT_GATEWAY, $gateway );	
			}
		}
	}
	
	protected static function create_shipping() 
	{		
		global $wpdb;		
		
		$delivery_services = usam_get_delivery_services( array( 'active' => 'all', 'fields' => 'id' ) );
		if ( count($delivery_services) == 0 )
		{
			$shippings = array( 
				array('name' => __( 'Курьером', 'usam' ), 'description' => '', 'active' => 1, 'handler' => '', 'img' => 0, 'sort' => 10, 'period_from' => 0, 'period_to' => 0, 'period_type' => 'D', 'price' => 1000, 'setting' => array( 'locations' => array(),'stores' => array(), 'margin' => 0, 'margin_type' => 'P', 'handler_setting' => array() ) ), 
				array('name' => __( 'Почта России', 'usam' ), 'description' => '', 'active' => 1, 'handler' => '', 'img' => 0, 'sort' => 10, 'period_from' => 0, 'period_to' => 0, 'period_type' => 'D', 'price' => 1000, 'setting' => array( 'locations' => array(),'stores' => array(), 'margin' => 0, 'margin_type' => 'P', 'handler_setting' => array() ) ), 
				array('name' => __( 'EMS', 'usam' ), 'description' => '', 'active' => 1, 'handler' => '', 'img' => 0, 'sort' => 10, 'period_from' => 0, 'period_to' => 0, 'period_type' => 'D', 'price' => 1000, 'setting' => array( 'locations' => array(),'stores' => array(), 'margin' => 0, 'margin_type' => 'P', 'handler_setting' => array() ) ), 				
				array('name' => __( 'Самовывоз', 'usam' ), 'description' => '', 'active' => 1, 'handler' => '', 'img' => 0, 'sort' => 10, 'period_from' => 0, 'period_to' => 0, 'period_type' => 'D',  'price' => 1000, 'setting' => array( 'locations' => array(), 'stores' => array(), 'margin' => 0, 'margin_type' => 'P', 'handler_setting' => array() ) ), 
				);	
			foreach( (array)$shippings as $shipping )
			{
				$shipping['setting'] = serialize($shipping['setting']);				
				$wpdb->insert( USAM_TABLE_DELIVERY_SERVICE, $shipping );	
			}
		}
	}
	
	/*Эта часть создает страницы и автоматически ставит их URL-адреса в странице параметров. */
	protected static function create_system_pages() 
	{		
		global $wpdb, $wp_rewrite;
		$pages = usam_system_pages();
		
		$post_date = date( "Y-m-d H:i:s" );
		$post_date_gmt = gmdate( "Y-m-d H:i:s" );
		$newpages = false; // Индикатор, будем ли мы создавать новые страницы
		//Создадим системные страницы
		foreach( (array)$pages as $page )
		{			
			$page_id = $wpdb->get_var("SELECT id FROM `".$wpdb->posts."` WHERE `post_content` LIKE '%".$page['content']."%'	AND `post_type` != 'revision'");		
			if( empty($page_id) )
			{		
				$page_id = wp_insert_post( array(
					'post_title' 	=>	$page['title'],
					'post_type' 	=>	'page',
					'post_name'		=>	$page['name'],
					'comment_status'=>	'closed',
					'ping_status' 	=>	'closed',
					'post_content' 	=>	$page['content'],
					'post_status' 	=>	'publish',
					'post_author' 	=>	1,
					'menu_order'	=>	0,
					'post_parent'	=>	''
				));
				$newpages = true;
			}			
			$link = 'usam_'.$page['name'].'_link';		
			update_option( $link, get_page_link( $page_id ) );	
		} 
		if ( $newpages ) 
		{
			wp_cache_delete( 'all_page_ids', 'pages' );
			$wp_rewrite->flush_rules();
			usam_update_permalink_slugs();
		}
		
		/*$my_post = array(
			'post_status' => 'publish',
			'post_type' => 'subscriber',
			'post_author' => 1,
			'post_content' => '[page_subscriber]',
			'post_title' => __('Параметры подписчика', 'usam'),
			'post_name' => 'subscriber');
		
		wp_insert_post( $my_post );*/
	}	
	
	// Создать склад интернет магазина	
	protected static function create_storages() 
	{	
		global $wpdb;
		$count = $wpdb->get_var(" SELECT COUNT(id) FROM `".USAM_TABLE_STORAGE_LIST."`" );		
		if ( empty($count) )
		{
			$storage = array( 'sort' => 0, 'site_id' => '', 'GPS_N' => '','GPS_S' => '','code' => '','title' => __('Интернет-магазин','usam'),'address' => '', 'description' => __('Основной склад интернет-магазина','usam'),'phone' => '','schedule' => '','email' => '','img' => '','author' => get_current_user_id(), 'date' => date( "Y-m-d H:i:s" ), 'active' => 1,'issuing' => 1, 'shipping' => 1 );	
			
			$_storage = new USAM_Storage( $storage );	
			$_storage->save();
		}
	}			
		
	protected static function add_option() 
	{
		global $wpdb;				
		
		add_option( 'usam_currency_type', 'RUB', '', true );
		
		$prices = array(
			array( 'id' => 1, 'type' => 'R', 'title' => __('Цена по умолчанию', 'usam'), 'code' => 'tp_1', 'available' => 1, 'currency' => 'RUB', 'base_type' => 0, 'underprice' => 80, 'rounding' => '0.01', 'sort' => 1), 
			array( 'id' => 2, 'type' => 'R', 'title' => __('Оптовая', 'usam'), 'code' => 'tp_2', 'available' => 1, 'currency' => 'RUB', 'base_type' => 'tp_1', 'underprice' => -20, 'rounding' => '0.01', 'sort' => 1, 'roles' => array('wholesale_buyer') ),
			array( 'id' => 3, 'type' => 'P', 'title' => __('Закупочная', 'usam'), 'code' => 'tp_3', 'currency' => 'RUB', 'rounding' => '0.01', 'sort' => 1) 
		);
		$underprice_rules = array(
			array( 'id' => 1, 'title' => '+10%', 'value' => 10, 'category' => array(), 'brands' => array(), 'category_sale' => array(), 'type_prices' => array() ), 
			array( 'id' => 2, 'title' => '+30%', 'value' => 30, 'category' => array(), 'brands' => array(), 'category_sale' => array(), 'type_prices' => array() ),
			array( 'id' => 3, 'title' => '-20%', 'value' => -20, 'category' => array(), 'brands' => array(), 'category_sale' => array(), 'type_prices' => array() ) 
		);		
		$calendares = array( 1 => array( 'id' => 1,  'name' => __('Заказы', 'usam'), 'user_id' => 0, 'sort' => 1, 'type' => 'order' ), 2 => array( 'id' => 2,  'name' => __('Дела', 'usam'), 'user_id' => 0, 'sort' => 2, 'type' => 'affair' ) );		
	
		add_option( 'usam_calendars', serialize($calendares), '', false );						
		$types_payers = array( array( 'id' => 1, 'name' => __( 'Физическое лицо', 'usam' ), 'type' => 'I', 'active' => 1, 'sort' => 10 ), 
							   array( 'id' => 2, 'name' => __( 'Юридическое лицо', 'usam' ), 'type' => 'E', 'active' => 1, 'sort' => 20 ) );
									
									
		$default_message = usam_feedback_message();
		$buy_product = array( 'message' => $default_message['buy_product'], 'fields' => array( 'name' => array( 'show' => 1, 'require' => 1, 'title' => '' ),
																								   'mail' => array( 'show' => 1, 'require' => 1, 'title' => '' ),
																								   'phone' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
																								   'message' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
		) );	
		$notify_stock = array( 'message' => $default_message['notify_stock'], 'fields' => array( 'name' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								 'mail' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								 'phone' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
																								 'message' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
		) );	
		$product_info = array( 'message' => $default_message['product_info'], 'fields' => array( 'name' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'mail' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'phone' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
																								   'message' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
		) );			
		$product_error = array( 'message' => $default_message['product_error'], 'fields' => array( 'name' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'mail' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'phone' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
																								   'message' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
		) );		
		$price_comparison = array( 'message' => $default_message['price_comparison'], 'fields' => array( 'name' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'mail' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
																								   'phone' => array( 'show' => 0, 'require' => 0, 'title' => '' ),
																								   'message' => array( 'show' => 1, 'require' => 0, 'title' => '' ),
		) );							
		$crm_contact_source = array( 
			array( 'name' => __('Свой контакт'), 'id' => 'self' ), 
			array( 'name' => __('Сделал заказ'), 'id' => 'order' ),
			array( 'name' => __('Зарегистрировался на Вашем сайте'), 'id' => 'register' ),
			array( 'name' => __('Существующий клиент'), 'id' => 'partner' ),
			array( 'name' => __('Звонок'), 'id' => 'call' ),
			array( 'name' => __('Веб-сайт'), 'id' => 'web' ),
			array( 'name' => __('Электронная почта'), 'id' => 'email' ),
			array( 'name' => __('Конференция'), 'id' => 'conference' ),
			array( 'name' => __('Выставка'), 'id' => 'trade_show' ),
			array( 'name' => __('Сотрудник'), 'id' => 'employee' ),
			array( 'name' => __('Компания'), 'id' => 'company' ),
			array( 'name' => __('Письмо'), 'id' => 'mail' ),
			array( 'name' => __('Импортированный'), 'id' => 'import' ),
			array( 'name' => __('Другое'), 'id' => 'orher' ),
		);	
		
$crm_company_fields = array( 
//Фактический адрес 	
	array( 'name' => __('Местоположение','usam'), 'unique_name' => 'contactlocation','type' => 'location', 'group' => 'actual_address',  'sort' => 80,   ),	
	array( 'name' => __('Адрес','usam'), 'unique_name' => 'contactaddress','type' => 'address', 'group' => 'actual_address',  'sort' => 90,   ),
	array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'contactpostcode','type' => 'text', 'group' => 'actual_address',  'sort' => 100,   ),
	array( 'name' => __('Офис','usam'), 'unique_name' => 'contactoffice','type' => 'text', 'group' => 'actual_address',  'sort' => 100,   ),	
//Юридический адрес 	
	array( 'name' => __('Местоположение','usam'), 'unique_name' => 'legallocation','type' => 'location', 'group' => 'legal_address',  'sort' => 80,   ),	
	array( 'name' => __('Адрес','usam'), 'unique_name' => 'legaladdress','type' => 'address', 'group' => 'legal_address',  'sort' => 90,   ),
	array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'legalpostcode','type' => 'text', 'group' => 'legal_address',  'sort' => 100,   ),
	array( 'name' => __('Офис','usam'), 'unique_name' => 'legaloffice','type' => 'text', 'group' => 'legal_address',  'sort' => 100,   ),	
//Реквизиты компании
	array( 'name' => __('Сокращенное наименование компании','usam'), 'unique_name' => 'company_name', 'type' => 'text', 'group' => 'requisites',  'sort' => 200,   ),
	array( 'name' => __('Полное наименование компании','usam'), 'unique_name' => 'full_company_name', 'type' => 'text', 'group' => 'requisites',  'sort' => 200,   ),
	array( 'name' => __('ИНН','usam'), 'unique_name' => 'inn', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('КПП','usam'), 'unique_name' => 'ppc','type' => 'text', 'group' => 'requisites',  'sort' => 230,   ),
	array( 'name' => __('ОГРН','usam'), 'unique_name' => 'ogrn', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('Дата государственной регистрации','usam'), 'unique_name' => 'date_registration', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('ОКПО','usam'), 'unique_name' => 'okpo','type' => 'text', 'group' => 'requisites',  'sort' => 240,   ),
	array( 'name' => __('ОКТМО','usam'), 'unique_name' => 'oktmo', 'type' => 'text', 'group' => 'requisites',  'sort' => 250,   ),
	array( 'name' => __('Ген. директор','usam'), 'unique_name' => 'gm', 'type' => 'text', 'group' => 'requisites',  'sort' => 250,   ),
	array( 'name' => __('Гл. бухгалтер','usam'), 'unique_name' => 'accountant', 'type' => 'text', 'group' => 'requisites',  'sort' => 260,   ),	
);


  $reviews_default_options = array(           
            'form_location' => 0,            
            'goto_show_button' => 1, //скрытие кнопки "оставить отзыв"
            'hreview_type' => 'business',           
            'reviews_per_page' => 10,         
            'show_hcard' => 1,
            'show_hcard_on' => 1,
			'fields' => array( 
				'name' => array('title' => esc_html__( 'Имя', 'usam' ), 'ask' => 1, 'require' => 0, 'show' => 1, 'type' => 'input',  'maxlength' => "150" ), 
				'mail' => array('title' => esc_html__( 'Электронная почта', 'usam' ), 'ask' => 1, 'require' => 0, 'show' => 0, 'type' => 'input',  'maxlength' => "150" ), 
				'phone' => array('title' => esc_html__( 'Телефон', 'usam' ), 'ask' => 0, 'require' => 0, 'show' => 0, 'type' => 'input',  'maxlength' => "150" ),
				'title' => array('title' => esc_html__( 'Название отзыва', 'usam' ), 'ask' => 1, 'require' => 0, 'show' => 1, 'type' => 'input',  'maxlength' => "150" ),		
			)
		); 

$company_fields_group = array( array( 'id' => 'actual_address',  'name' => __( 'Фактический адрес', 'usam' ), 'sort' => 10 ), 
							array( 'id' => 'legal_address',      'name' => __( 'Юридический адрес', 'usam' ), 'sort' => 20 ),
							array( 'id' => 'requisites', 'name' => __( 'Реквизиты компании', 'usam' ), 'sort' => 30 ),
						);	
			
		$add_option = array(				
			'db_version' => array( 'value' => USAM_DB_VERSION, 'autoload' => true ),	
			'crm_company_fields_group' => array( 'value' =>  $company_fields_group, 'autoload' => false ),	
			'crm_company_fields' => array( 'value' =>  $crm_company_fields, 'autoload' => true ),				
			'crm_contact_source' => array( 'value' =>  $crm_contact_source, 'autoload' => false ),	
									
			'google_analytics_active' => array( 'value' =>  0, 'autoload' => true ),	
			'google' => array( 'value' =>  array(), 'autoload' => true ),	
			'yandex_metrika_active' => array( 'value' =>  0, 'autoload' => true ),	
			'yandex' => array( 'value' => array(), 'autoload' => true ),
			'check_position_site' => array( 'value' => 0, 'autoload' => true ),
			
			'shop_location' => array( 'value' =>  981, 'autoload' => true ),			
			'set_events' => array( 'value' =>  array(), 'autoload' => true ),
			
			'cashbox' => array( 'value' =>  array(), 'autoload' => false ),			
			
			'product_discount_rules' => array( 'value' =>  array(), 'autoload' => false ),	
			'usam_rules_discounts_shopping_cart' => array( 'value' =>  array(), 'autoload' => false ),	
			'type_prices' => array( 'value' => $prices , 'autoload' => true ),
			'underprice_rules' => array( 'value' => $underprice_rules , 'autoload' => false ),
			'default_reservation_storage' => array( 'value' => '' , 'autoload' => true ),			
			
			'notifications' => array( 'value' => '' , 'autoload' => false ),			
	
			'crop_thumbnails' => array( 'value' => 0 , 'autoload' => true ),	//Обрезать миниатюры				
			
			'max_downloads' => array( 'value' => 1 , 'autoload' => true ),		//Максимальное количество закачек файлов
			'shipwire' => array( 'value' => 0 , 'autoload' => true ),		
			'force_ssl' => array( 'value' => 0 , 'autoload' => true ),	
			'share_this' => array( 'value' => 0 , 'autoload' => true ),		
			
			'allow_tracking' => array( 'value' => 1 , 'autoload' => true ),	
			'pointer' => array( 'value' => false , 'autoload' => true ),		

 			'sets_of_products' => array( 'value' => array(), 'autoload' => false ),	
			
			'time_keeping_baskets' => array( 'value' => 7 , 'autoload' => true ),	
			'product_reserve_clear_period' => array( 'value' => 7 , 'autoload' => true ),		

			'vk_api' => array( 'value' => array('api_id' => '', 'service_token' => ''), 'autoload' => true ),	
			'vk_publish_product_day' => array( 'value' => 1, 'autoload' => true ),	
			'vk_publish_reviews' => array( 'value' => 1 , 'autoload' => true ),		
			'vk_groups' => array( 'value' => array() , 'autoload' => false ),	
			'vk_profile' => array( 'value' => array() , 'autoload' => false ),				 
			'vk_publishing_rules' => array( 'value' => array() , 'autoload' => false ),					
			'vk_autopost' => array( 'value' => array( 
				'upload_photo_count' => 4, 
				'excerpt_length' => 25, 
				'excerpt_length_strings' => 2688, 
				'from_group' => 1, 
				'from_signed' => 1, 
				'add_link' => 1,
				'type_price' => '',				
				'birthday' => __('Интернет-магазин','usam').' '.strtoupper(get_bloginfo('name')).' поздравляет с Днем Рождения участников группы, родившихся сегодня!\n\n{%user_link%}',				
				'product_review_message' => '%header%\n\n%review_title%\n%review_author%\n\n%link%\n\n%review_excerpt%\n\n%review_response%', 
				'reviews_message' => '%header%\n\n%review_title%\n%review_author%\n\n%link%\n\n%review_excerpt%\n\n%review_response%', 
				'post_message' => "%title%\n\n%excerpt%", 
				'product_message' => __('Интернет-магазин','usam').' '.strtoupper(get_bloginfo('name')).' '.__('предлагает','usam').':\n%title%\n\nКупить - %link%\n%price%', 
				'product_day_message' => '---------------------------------------'.__('ТОВАР ДНЯ','usam').'---------------------------------------\n\n%name%\n%title%\nКупить - %link%\n%price%',
			) , 'autoload' => false ),	
			
		// Презентация			
			'widget_show_sliding_cart' => array( 'value' => '', 'autoload' => true ),			
			'show_breadcrumbs' => array( 'value' => 1 , 'autoload' => true ),	
			'product_pagination' => array( 'value' => 1 , 'autoload' => true ),		
			'products_per_page' => array( 'value' => 24 , 'autoload' => true ),		
			'hide_addtocart_button' => array( 'value' => 0 , 'autoload' => true ),					
			'share_this' => array( 'value' => 0 , 'autoload' => true ),		
			'facebook_like' => array( 'value' => 0 , 'autoload' => true ),		
			'default_category' => array( 'value' => 'all' , 'autoload' => true ),				
			'display_categories' => array( 'value' => 1, 'autoload' => true ),
			'display_sold_products' => array( 'value' => 0, 'autoload' => true ),
			'show_zero_price' => array( 'value' => 1, 'autoload' => true ),			
			'show_comments_by_default' => array( 'value' => 0, 'autoload' => true ),	
			'enable_comments' => array( 'value' => 1, 'autoload' => true ),	
			
			'uses_coupons' => array( 'value' => 1 , 'autoload' => true ),	
			'show_product_rating' => array( 'value' => 1 , 'autoload' => true ),
			'show_availability_stock' => array( 'value' => 0 , 'autoload' => true ),	
			'show_fancy_notifications' => array( 'value' => 1 , 'autoload' => true ),	
			'show_multi_add' => array( 'value' => 1 , 'autoload' => true ),				

			'product_sort_by' => array( 'value' => 'id' , 'autoload' => true ),			
			'product_order' => array( 'value' => 'ASC' , 'autoload' => true ),
			
			'category_hierarchical_url' => array( 'value' => 0 , 'autoload' => true ),
			'show_subcatsprods_in_cat' => array( 'value' => 1 , 'autoload' => true ),
			'display_category_name' => array( 'value' => 1 , 'autoload' => true ),
			'category_description' => array( 'value' => 1 , 'autoload' => true ),				
		// Поиск			
			'search_result_items' => array( 'value' => 5 , 'autoload' => true ),	
			'search_exclude_pages' => array( 'value' => array() , 'autoload' => true ),	
			'search_exclude_posts' => array( 'value' => array() , 'autoload' => true ),	
			'search_exclude_p_tags' => array( 'value' => array() , 'autoload' => true ),	
			'search_exclude_p_categories' => array( 'value' => array() , 'autoload' => true ),		
			'search_text_lenght' => array( 'value' => 100, 'autoload' => true ),
			'search_sku_enable' => array( 'value' => 1, 'autoload' => true ),	
			'search_in_stock_enable' => array( 'value' => 1, 'autoload' => true ),	
			'search_price_enable' => array( 'value' => 1, 'autoload' => true ),
			'search_categories_enable' => array( 'value' => 1, 'autoload' => true ),	
			'search_tags_enable' => array( 'value' => 1, 'autoload' => true ),	
			'search_box_text' => array( 'value' => __( 'Поиск по названию и коду', 'usam' ), 'autoload' => true ),		
			'search_length_description_items' => array( 'value' => 100, 'autoload' => true ),			
			
			'list_of_subscribers' => array( 'value' => array( array('title' => 'Рассылка', 'date_insert' => date("Y-m-d H:i:s"), 'id' => 1 ) ) , 'autoload' => false ),  // Списки подписчиков
			'terms_and_conditions' => array( 'value' => '' , 'autoload' => false ),    // Сроки и условия			
		// Сообщения			
			'styling_mail_template' => array( 'value' => '' , 'autoload' => false ),		
			'return_email' => array( 'value' => '' , 'autoload' => false ),				
			'incomplete_sale_subject' => array( 'value' => __( 'Отложенный заказ: Требуется оплата', 'usam' ) , 'autoload' => false ),
			'incomplete_sale_message' => array( 'value' => __( 'Благодарим Вас за покупку в магазине %shop_name%. Ваш заказ требует оплату! После оплаты он будет обработан как можно скорее.
	Вы заказали эти товары:
	%product_list%%total_shipping%%total_price%', 'usam' ), 'autoload' => false ),
			'order_received_subject' => array( 'value' => __( 'Квитанция о покупке для клиента', 'usam' ) , 'autoload' => false ),
			'order_received_message' => array( 'value' => __( 'Благодарим Вас за покупку в магазине %shop_name%, ваш заказ будет обработан как можно скорее.
	Вы заказали эти товары:
	%product_list%%total_shipping%%total_price%', 'usam' ), 'autoload' => false ),		
			'orders_canceled_subject' => array( 'value' => '' , 'autoload' => false ),// Заказ отменен
			'orders_canceled_message' => array( 'value' => '' , 'autoload' => false ),	
			'orders_collected_subject' => array( 'value' => '' , 'autoload' => false ),// Заказ собран
			'orders_collected_message' => array( 'value' => '' , 'autoload' => false ),	
			'job_dispatched_subject' => array( 'value' => '' , 'autoload' => false ),// Заказ в обработке
			'job_dispatched_message' => array( 'value' => '' , 'autoload' => false ),			
			'payment_incomplete_subject' => array( 'value' => '' , 'autoload' => false ),
			'payment_incomplete_message' => array( 'value' => '' , 'autoload' => false ),			
			'trackingid_subject' => array( 'value' => __( 'Номер отслеживания почтового отправления', 'usam' ) , 'autoload' => false ),
			'trackingid_message' => array( 'value' => __( "Функция отслеживания означает, что Вы можете отслеживать ход вашей посылки с помощью нашей онлайн службы отслеживания, просто войдите на наш сайт и введите идентификационный номер Вашего заказа, чтобы посмотреть его статус.\n\nИдентификационный номер заказа: %track_id%", 'usam' ) , 'autoload' => false ),
			'orders_collected_subject' => array( 'value' => '' , 'autoload' => false ),
			'orders_collected_message' => array( 'value' => '' , 'autoload' => false ),
			'order_shipped_subject' => array( 'value' => '' , 'autoload' => false ),
			'order_shipped_message' => array( 'value' => '' , 'autoload' => false ),
			'coupon_add_subject' => array( 'value' => '' , 'autoload' => false ),
			'coupon_add_message' => array( 'value' => '' , 'autoload' => false ),
			'coupon_add_sms' => array( 'value' => '' , 'autoload' => false ),				
				
			'feedback_notify_stock' => array( 'value' => $notify_stock , 'autoload' => false ),
			'feedback_buy_product' => array( 'value' => $buy_product , 'autoload' => false ),
			'feedback_product_info' => array( 'value' => $product_info , 'autoload' => false ),
			'feedback_product_error' => array( 'value' => $product_error , 'autoload' => false ),
			'feedback_price_comparison' => array( 'value' => $price_comparison , 'autoload' => false ),
			
			//Обратная связь
			'email_contactform_copy' => array( 'value' => 0 , 'autoload' => true ),
			'reviews' => array( 'value' => $reviews_default_options, 'autoload' => true ),
	
			'printing_form' => array( 'value' => '' , 'autoload' => false ),
			'purchase_rules' => array( 'value' => '' , 'autoload' => false ),
			'coupons_roles' => array( 'value' => '' , 'autoload' => false ),
			'update_price_settings_excel' => array( 'value' => '' , 'autoload' => false ),	
			'product_view' => array( 'value' => 'default' , 'autoload' => true ),				
			'show_thumbnails_thickbox' => array( 'value' => 0 , 'autoload' => true ),		
			'permalinks' => array( 'value' => array('product_base' => 'products') , 'autoload' => true ),				
			'types_payers' => array( 'value' => $types_payers, 'autoload' => true ), // Типы плательщиков			
			
			'price_list_setting' => array( 'value' => '' , 'autoload' => true ),		// Типы цен	

			'add_users_to_mailing_list' => array( 'value' => array(), 'autoload' => true ),		//Списки рассылок
			
			// Скидки	
			'product_discount' => array( 'value' => '' , 'autoload' => true ),	//автоскидка товаров, хранится массив товаров со скидкой	
			'product_day_rules' => array( 'value' => '' , 'autoload' => true ),	
			'bonuses_rules' => array( 'value' => '' , 'autoload' => true ),				
			
			// Текст страниц
			'page_transaction_results' => array( 'value' => '' , 'autoload' => false ),	
			
			//валюта
			'currency_type' => array( 'value' => 'RUB' , 'autoload' => true ),
			'thousands_separator' => array( 'value' => '.' , 'autoload' => true ),
			'decimal_separator' => array( 'value' => ',' , 'autoload' => true ),			
			'currency_sign_location' => array( 'value' => '2' , 'autoload' => true ),//Расположение валюты	
			
			'weight_unit' => array( 'value' => 'g' , 'autoload' => true ),
			'dimension_unit' => array( 'value' => 'mm' , 'autoload' => true ),			
		
			'license' => array( 'value' => '' , 'autoload' => true ),			
			
			//размеры для изображений
			'product_image' => array( 'value' => array ( 'width'  => 160, 'height' => 160 ) , 'autoload' => true ),
			'category_image' => array( 'value' => array ( 'width'  => 160, 'height' => 160 ) , 'autoload' => true ),
			'brand_image' => array( 'value' => array ( 'width'  => 160, 'height' => 160 ) , 'autoload' => true ),
			'single_view_image' => array( 'value' => array ( 'width'  => 450, 'height' => 450 ) , 'autoload' => true ),
			'gallery_image' => array( 'value' => array ( 'width'  => 160, 'height' => 160 ) , 'autoload' => true ),
			
			'online_consultants' => array( 'value' => array(), 'autoload' => true ),		
			'get_customer_location' => array( 'value' => 1, 'autoload' => true ),	
		);
		$order_statuses = usam_get_order_statuses();	
		foreach ( $order_statuses as $status )
		{			
			$option_key_subject = USAM_OPTION_PREFIX.$status->internalname.'_subject';
			$option_key_message = USAM_OPTION_PREFIX.$status->internalname.'_message';
			$option_key_sms_message = USAM_OPTION_PREFIX.$status->internalname.'_sms';			
			
			$wpdb->query( "UPDATE `$wpdb->options` SET `autoload` = 'no' WHERE `option_name` = '{$option_key_subject}'");				
			$wpdb->query( "UPDATE `$wpdb->options` SET `autoload` = 'no' WHERE `option_name` = '{$option_key_message}'");		
			$wpdb->query( "UPDATE `$wpdb->options` SET `autoload` = 'no' WHERE `option_name` = '{$option_key_sms_message}'");			
		}
		foreach( (array)$add_option as $key => $option )
		{
			$option_key = USAM_OPTION_PREFIX.$key;
			add_option( $option_key, $option['value'], '', $option['autoload'] );
			if ( $option['autoload'] )
				$wpdb->query( "UPDATE `$wpdb->options` SET `autoload` = 'yes' WHERE `option_name` = '{$option_key}'");	
			else
				$wpdb->query( "UPDATE `$wpdb->options` SET `autoload` = 'no' WHERE `option_name` = '{$option_key}'");	
		}	
//wp_cache_delete( $option, 'options' );		
	}
	
	public static function add_role() 
	{	
		global $wp_roles;		
		
		$capabilities = array( 
			'store_section'       => array( 'administrator' ),	
			'view_orders'         => array( 'administrator' ),		
			'edit_order'          => array( 'administrator' ),		
			'edit_payment'        => array( 'administrator' ),
			'edit_shipped'        => array( 'administrator' ),				
			'view_feedback'       => array( 'administrator' ),				
			'view_crm'            => array( 'administrator' ),				
			'manage_prices'       => array( 'administrator' ),				
			'view_storage'        => array( 'administrator' ),		
			'view_customers'      => array( 'administrator'),		
			'view_interface'      => array( 'administrator' ),
			'view_exchange'       => array( 'administrator' ),
			'view_partners'       => array( 'administrator' ),
			'manage_discounts'    => array( 'administrator' ),				
			'view_reports'        => array( 'administrator' ),
			
			'marketing_section'    => array( 'administrator' ),	
			'view_marketing'       => array( 'administrator' ),
			'view_social_networks' => array( 'administrator' ),	
			'view_newsletter'      => array( 'administrator' ),						
			'view_marketing'       => array( 'administrator' ),
			'view_seo'             => array( 'administrator' ),
			
			'shop_tools'           => array( 'administrator' ),
			'edit_shop_settings'   => array( 'administrator' ),
		);			
		foreach ( $capabilities as $capability_id => $roles ) 
		{	
			foreach ( $wp_roles->role_objects as $wp_role ) 
			{				
				if ( in_array($wp_role->name, $roles) )
				{								
					if ( !$wp_role->has_cap( $capability_id ) )
					{						
						$wp_role->add_cap( $capability_id );						
					}	
				}
			}
		}			
		add_role( 'wholesale_buyer', __( 'Оптовый покупатель', 'usam' ), array(
			'read' 						=> true,
			'edit_posts' 				=> false,
			'delete_posts' 				=> false
		) );
		add_role( 'remote_agents', __( 'Удаленные агенты', 'usam' ), array(
			'read' 						=> true,
			'edit_posts' 				=> false,
			'delete_posts' 				=> false
		) );	
		add_role( 'courier', __( 'Курьер', 'usam' ), array(
			'store_section'        => true,		
			'view_orders'          => true,		
			'edit_shipped'         => true,
			
			'read' 			       => true,
			'edit_posts' 		   => false,
			'delete_posts' 		   => false
		) );						
		add_role( 'shop_manager', __( 'Менеджер магазина', 'usam' ), array(
			'store_section'          => true,
			'view_orders'            => true,		
			'edit_order'             => true,		
			'edit_payment'           => true,
			'edit_shipped'           => true,	

			'view_feedback'          => true,
			'view_crm'               => true,					
			'manage_prices'          => true,
			'view_storage'           => true,
			'view_customers'         => true,
			'view_interface'         => true,
			'view_exchange'          => true,
			'view_partners'          => true,
			'manage_discounts'       => true,
			'view_reports'           => true,
			
			'marketing_section'      => true,
			'view_marketing'         => true,
			'view_social_networks'   => true,
			'view_newsletter'        => true,	
			'view_seo'               => true,	

			'edit_shop_settings'     => true,			
			
			'level_9'                => true,			
			'level_8'                => true,
			'level_7'                => true,
			'level_6'                => true,
			'level_5'                => true,
			'level_4'                => true,
			'level_3'                => true,
			'level_2'                => true,
			'level_1'                => true,
			'level_0'                => true,
			'read'                   => true,
			'read_private_pages'     => true,
			'read_private_posts'     => true,
			'edit_users'             => true,		
			'edit_posts'             => true,
			'edit_pages'             => true,
			'edit_published_posts'   => true,
			'edit_published_pages'   => true,
			'edit_private_pages'     => true,
			'edit_private_posts'     => true,
			'edit_others_posts'      => true,
			'edit_others_pages'      => true,
			'publish_posts'          => true,
			'publish_pages'          => true,
			'delete_posts'           => true,
			'delete_pages'           => true,
			'delete_private_pages'   => true,
			'delete_private_posts'   => true,
			'delete_published_pages' => true,
			'delete_published_posts' => true,
			'delete_others_posts'    => true,
			'delete_others_pages'    => true,
			'manage_categories'      => true,
			'manage_links'           => true,
			'moderate_comments'      => true,
			'unfiltered_html'        => true,
			'upload_files'           => true,
			'export'                 => true,
			'import'                 => true,
			'list_users'             => true			
		) );
		//remove_role( 'shop_crm' );
		add_role( 'shop_crm', __( 'CRM менеджер', 'usam' ), array(
			'store_section'          => true,
			'view_orders'            => true,		
			'edit_order'             => true,		
			'edit_payment'           => true,
			'edit_shipped'           => true,	

			'view_feedback'          => true,
			'view_crm'               => true,					
			'manage_prices'          => false,
			'view_storage'           => true,
			'view_customers'         => true,
			'view_interface'         => false,
			'view_exchange'          => false,
			'view_partners'          => true,
			'manage_discounts'       => false,
			'view_reports'           => true,
			
			'marketing_section'      => false,
			'view_marketing'         => false,
			'view_social_networks'   => false,
			'view_newsletter'        => false,	
			'view_seo'               => false,	

			'edit_shop_settings'     => false,			
			
			'level_9'                => false,			
			'level_8'                => false,
			'level_7'                => false,
			'level_6'                => false,
			'level_5'                => true,
			'level_4'                => true,
			'level_3'                => true,
			'level_2'                => true,
			'level_1'                => true,
			'level_0'                => true,
			'read'                   => true,
			'read_private_pages'     => false,
			'read_private_posts'     => false,
			'edit_users'             => false,		
			'edit_posts'             => false,
			'edit_pages'             => false,
			'edit_published_posts'   => false,
			'edit_published_pages'   => false,
			'edit_private_pages'     => false,
			'edit_private_posts'     => false,
			'edit_others_posts'      => false,
			'edit_others_pages'      => false,
			'publish_posts'          => false,
			'publish_pages'          => false,
			'delete_posts'           => false,
			'delete_pages'           => false,
			'delete_private_pages'   => false,
			'delete_private_posts'   => false,
			'delete_published_pages' => false,
			'delete_published_posts' => false,
			'delete_others_posts'    => false,
			'delete_others_pages'    => false,
			'manage_categories'      => false,
			'manage_links'           => false,
			'moderate_comments'      => false,
			'unfiltered_html'        => false,
			'upload_files'           => false,
			'export'                 => false,
			'import'                 => false,
			'list_users'             => false		
		) );
		
		add_role( 'shop_seo', __( 'SEO специалист', 'usam' ), array(
			'store_section'          => false,
			'view_orders'            => false,		
			'edit_order'             => false,		
			'edit_payment'           => false,
			'edit_shipped'           => false,	
			
			'view_seo'               => true,
			'edit_shop_settings'     => false,			
			
			'level_9'                => false,			
			'level_8'                => false,
			'level_7'                => false, // ниже 7 не загружаются js и css
			'level_6'                => true,
			'level_5'                => true,
			'level_4'                => true,
			'level_3'                => true,
			'level_2'                => true,
			'level_1'                => true,
			'level_0'                => true,
			'read'                   => true,
			'read_private_pages'     => true,
			'read_private_posts'     => true,
			'edit_users'             => true,		
			'edit_posts'             => true,
			'edit_pages'             => true,
			'edit_published_posts'   => true,
			'edit_published_pages'   => true,
			'edit_private_pages'     => true,
			'edit_private_posts'     => true,
			'edit_others_posts'      => true,
			'edit_others_pages'      => true,
			'publish_posts'          => true,
			'publish_pages'          => true,
			'delete_posts'           => false,
			'delete_pages'           => false,
			'delete_private_pages'   => false,
			'delete_private_posts'   => false,
			'delete_published_pages' => false,
			'delete_published_posts' => false,
			'delete_others_posts'    => false,
			'delete_others_pages'    => false,
			'manage_categories'      => true,
			'manage_links'           => true,
			'moderate_comments'      => true,
			'unfiltered_html'        => true,
			'upload_files'           => true,
			'export'                 => false,
			'import'                 => false,
			'list_users'             => false			
		) );
	}
	
	// Создает директории в папке UPLOAD
	protected static function create_upload_directories() 
	{
		$folders = array(
			array( 'dir' => USAM_UPLOAD_DIR, 'mode' => 0775, 'htaccess' => 0 ),
			array( 'dir' => USAM_FILE_DIR, 'mode' => 0775, 'htaccess' => 0 ),
			array( 'dir' => USAM_DOCUMENTS_DIR, 'mode' => 0775, 'htaccess' => 0 ),		
			array( 'dir' => USAM_USER_UPLOADS_DIR, 'mode' => 0775, 'htaccess' => 0 ),
			array( 'dir' => USAM_UPLOAD_DIR.'events/', 'mode' => 0775, 'htaccess' => 0 ),
			array( 'dir' => USAM_UPLOAD_DIR.'exchange/', 'mode' => 0775, 'htaccess' => 0 ),
			array( 'dir' => USAM_UPLOAD_DIR.'Log/', 'mode' => 0775, 'htaccess' => 1 ),		
			array( 'dir' => USAM_UPLOAD_DIR.'order_invoices/', 'mode' => 0775, 'htaccess' => 1 ),
		);
		$htaccess = "order deny,allow\n\r";
		$htaccess .= "deny from all\n\r";
		$htaccess .= "allow from none\n\r";
		foreach ( $folders as $folder ) 
		{ 
			wp_mkdir_p( $folder['dir'] );		
			@ chmod( $folder['dir'], $folder['mode'] );
			$filename = $folder['dir'] . ".htaccess";				
			if ( $folder['htaccess'] && !is_file( $filename ) ) 
			{			
				$file_handle = @ fopen( $filename, 'w+' );
				@ fwrite( $file_handle, $htaccess );
				@ fclose( $file_handle );
				@ chmod( $file_handle, 0665 );
			}
		}	
	}
	
	/**
	 * Создание или изменение таблиц базы данных
	 */
	public static function create_or_update_tables( )
	{
		global $wpdb;	

		include( USAM_FILE_PATH . '/admin/db/database_template.php' );
		$template_hash = sha1( serialize( $database_template ) );
	// Фильтр для добавления или изменения шаблона базы данных, убедитесь, ваша функция вернет массив, иначе обновления таблиц базы данных не состоится
		$database_template = apply_filters( 'usam_alter_database_template', $database_template );

		$failure_reasons = array( );
		$upgrade_failed = false;
		foreach ( (array)$database_template as $table_name => $table_data )
		{
			// убедитесь, что таблица не существует под правильным именем, а затем проверить, есть ли предыдущее название, если есть, проверьте таблицы под этим именем тоже
			if ( !$wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) && (!isset( $table_data['previous_names'] ) || (isset( $table_data['previous_names'] ) && !$wpdb->get_var( "SHOW TABLES LIKE '{$table_data['previous_names']}'" )) ) ) 
			{
				//if the table does not exixt, create the table
				$constructed_sql_parts = array( );
				$constructed_sql = "CREATE TABLE `{$table_name}` (\n";

				// loop through the columns
				foreach ( (array)$table_data['columns'] as $column => $properties ) {
					$constructed_sql_parts[] = "`$column` $properties";
				}
				// Затем с помощью индексов
				foreach ( (array)$table_data['indexes'] as $properties ) {
					$constructed_sql_parts[] = "$properties";
				}
				$constructed_sql .= implode( ",\n", $constructed_sql_parts );
				$constructed_sql .= "\n) ENGINE=MyISAM";

				// if mySQL is new enough, set the character encoding
				if ( method_exists( $wpdb, 'db_version' ) && version_compare( $wpdb->db_version(), '4.1', '>=' ) ) {
					$constructed_sql .= " CHARSET=utf8";
				}
				$constructed_sql .= ";";

				if ( !$wpdb->query( $constructed_sql ) ) {
					$upgrade_failed = true;
					$failure_reasons[] = $wpdb->last_error;
				}

				if ( isset( $table_data['actions']['after']['all'] ) && is_callable( $table_data['actions']['after']['all'] ) ) {
					$table_data['actions']['after']['all']();
				}
			} 
			else 
			{
				// проверить, если новое имя таблицы в использовании
				if ( !$wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) && (isset( $table_data['previous_names'] ) && $wpdb->get_var( "SHOW TABLES LIKE '{$table_data['previous_names']}'" )) ) 
				{
					$wpdb->query( "ALTER TABLE	`{$table_data['previous_names']}` RENAME TO `{$table_name}`;" );
					$failure_reasons[] = $wpdb->last_error;
				}				
				$existing_table_columns = array( );
				//проверить и, возможно, обновить кодировку символов
				if ( method_exists( $wpdb, 'db_version' ) && version_compare( $wpdb->db_version(), '4.1', '>=' ) ) 
				{ 
					$table_status_data = $wpdb->get_row( "SHOW TABLE STATUS LIKE '$table_name'", ARRAY_A );
					if ( $table_status_data['Collation'] != 'utf8_general_ci' ) {
						$wpdb->query( "ALTER TABLE `$table_name` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci" );
					}
				}
				if ( isset( $table_data['actions']['before']['all'] ) && is_callable( $table_data['actions']['before']['all'] ) ) {
					$table_data['actions']['before']['all']();
				}			
				$existing_table_column_data = $wpdb->get_results( "SHOW FULL COLUMNS FROM `$table_name`", ARRAY_A );

				foreach ( (array)$existing_table_column_data as $existing_table_column ) 
				{
					$column_name = $existing_table_column['Field'];
					$existing_table_columns[] = $column_name;

					$null_match = false;
					if ( $existing_table_column['Null'] = 'NO' ) {
						if ( isset( $table_data['columns'][$column_name] ) && stristr( $table_data['columns'][$column_name], "NOT NULL" ) !== false ) {
							$null_match = true;
						}
					} else {
						if ( isset( $table_data['columns'][$column_name] ) && stristr( $table_data['columns'][$column_name], "NOT NULL" ) === false ) {
							$null_match = true;
						}
					}
					if ( isset( $table_data['columns'][$column_name] ) && ((stristr( $table_data['columns'][$column_name], $existing_table_column['Type'] ) === false) || ($null_match != true)) ) 
					{
						if ( isset( $table_data['actions']['before'][$column_name] ) && is_callable( $table_data['actions']['before'][$column_name] ) )
							$table_data['actions']['before'][$column_name]( $column_name );						
						if ( !$wpdb->query( "ALTER TABLE `$table_name` CHANGE `$column_name` `$column_name` {$table_data['columns'][$column_name]} " ) ) 
						{
							$upgrade_failed = true;
							$failure_reasons[] = $wpdb->last_error;
						}
					}
				}
				$supplied_table_columns = array_keys( $table_data['columns'] );
				// сравните поставленные и существующие столбцы, чтобы найти различия
				$missing_or_extra_table_columns = array_diff( $supplied_table_columns, $existing_table_columns );
			//	$missing_or_extra_table_columns = array_diff( $existing_table_columns, $supplied_table_columns );
				if ( count( $missing_or_extra_table_columns ) > 0 ) 
				{
					foreach ( (array)$missing_or_extra_table_columns as $missing_or_extra_table_column ) 
					{
						if ( isset( $table_data['columns'][$missing_or_extra_table_column] ) ) {
							//table column is missing, add it
							$index = array_search( $missing_or_extra_table_column, $supplied_table_columns ) - 1;

							$previous_column = isset( $supplied_table_columns[$index] ) ? $supplied_table_columns[$index] : '';
							if ( $previous_column != '' ) {
								$previous_column = "AFTER `$previous_column`";
							}
							$constructed_sql = "ALTER TABLE `$table_name` ADD `$missing_or_extra_table_column` ".$table_data['columns'][$missing_or_extra_table_column]." $previous_column;";
							if ( !$wpdb->query( $constructed_sql ) ) {
								$upgrade_failed = true;
								$failure_reasons[] = $wpdb->last_error;
							}
							// запустить обновление функции
							if ( isset( $table_data['actions']['after'][$missing_or_extra_table_column] ) && is_callable( $table_data['actions']['after'][$missing_or_extra_table_column] ) ) {
								$table_data['actions']['after'][$missing_or_extra_table_column]( $missing_or_extra_table_column );
							}
						}
					}
				}
				if ( isset( $table_data['actions']['after']['all'] ) && is_callable( $table_data['actions']['after']['all'] ) ) {
					$table_data['actions']['after']['all']();
				}
				// получить список существующих индексов
				$existing_table_index_data = $wpdb->get_results( "SHOW INDEX FROM `$table_name`", ARRAY_A );
				$existing_table_indexes = array( );
				foreach ( $existing_table_index_data as $existing_table_index ) {
					$existing_table_indexes[] = $existing_table_index['Key_name'];
				}
				$existing_table_indexes = array_unique( $existing_table_indexes );
				$supplied_table_indexes = array_keys( $table_data['indexes'] );

				// сравните поставляемые и существующие индексы, чтобы найти различия
				$missing_or_extra_table_indexes = array_diff( $supplied_table_indexes, $existing_table_indexes );
				//$missing_or_extra_table_indexes = array_diff( $existing_table_indexes, $supplied_table_indexes );
				if ( count( $missing_or_extra_table_indexes ) > 0 ) 
				{
					foreach ( $missing_or_extra_table_indexes as $missing_or_extra_table_index ) 
					{
						if ( isset( $table_data['indexes'][$missing_or_extra_table_index] ) ) 
						{
							$constructed_sql = "ALTER TABLE `$table_name` ADD " . $table_data['indexes'][$missing_or_extra_table_index] . ";";
							if ( !$wpdb->query( $constructed_sql ) ) {
								$upgrade_failed = true;
								$failure_reasons[] = $wpdb->last_error;
							}
						}
					}
				}
			}
		}
		if ( $upgrade_failed !== true )			
			return true;	 
		else
			return false;
	}
}