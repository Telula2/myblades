<?php
// Описание файла: Здесь указываются константы


/**
	Описание: Основные константы, необходимые для начала загрузки
 */
 
// Определить отладки переменные для разработчиков
if( isset($_SESSION['usam_activate_debug']) && $_SESSION['usam_activate_debug'] ) 
	define( 'USAM_DEBUG', true );
else
	define( 'USAM_DEBUG', false );	


if ( !defined('LOCALHOST') )
{
	if ( $_SERVER['REMOTE_ADDR'] == '127.0.0.1' )	
		define( 'LOCALHOST', true );
	else
		define( 'LOCALHOST', false );
}
define( 'USAM_DIR_NAME',  basename( USAM_FILE_PATH ) ); // Имя папки плагина		
define( 'USAM_PLUGINSLUG',  USAM_DIR_NAME.'/universam.php' );

define( 'USAM_PRINTING_FORM',  USAM_FILE_PATH.'/printing-forms' );			

// Путь к папке с картинками
define( 'USAM_CORE_IMAGES_URL',  USAM_URL . '/assets/images' );
define( 'USAM_CORE_IMAGES_PATH', USAM_FILE_PATH . '/assets/images' );
// JS URL
define( 'USAM_CORE_JS_URL',  USAM_URL . '/assets/js' );

// Пути к папке magazine в активной теме
define( 'USAM_THEMES_PATH', get_template_directory() . '/magazine/' );
define( 'USAM_THEME_URL', get_template_directory_uri() . '/magazine/' );
		
// Папка с темой
define( 'USAM_CORE_THEME_PATH', USAM_FILE_PATH . '/theme/' );
define( 'USAM_CORE_THEME_URL' , USAM_URL       . '/theme/' );			

define( 'USAM_CUSTOMER_COOKIE', 'usam_customer_cookie_' . COOKIEHASH );	
define( 'USAM_CUSTOMER_COOKIE_PATH', COOKIEPATH );
define( 'USAM_CUSTOMER_DATA_EXPIRATION', 48 * 3600 );

define( 'USAM_UNLIMITED_STOCK', 999999 );


/**
* Список глобальные констант для таблиц
 */
global $table_prefix, $wpdb;

if ( !empty( $wpdb->prefix ) )
	$wp_table_prefix = $wpdb->prefix;
else if ( !empty( $table_prefix ) )
	$wp_table_prefix = $table_prefix;

define( 'USAM_META_PREFIX', '_usam_' );              //  префикс для мета данных товаров
define( 'USAM_OPTION_PREFIX', 'usam_' );
$usam_table_prefix = $wp_table_prefix.'usam_'; // префикс для таблиц	

define( 'USAM_TABLE_PRODUCTS_BASKET',        "{$usam_table_prefix}products_basket" );
define( 'USAM_TABLE_USERS_BASKET',           "{$usam_table_prefix}users_basket" );	
define( 'USAM_TABLE_DISCOUNT_BASKET',        "{$usam_table_prefix}discount_cart" );	
define( 'USAM_TABLE_ORDER_PROPS',            "{$usam_table_prefix}order_props" );
define( 'USAM_TABLE_COUPON_CODES',           "{$usam_table_prefix}coupon_codes" ); 	

define( 'USAM_TABLE_USER_PRODUCTS',          "{$usam_table_prefix}user_products" ); 		

define( 'USAM_TABLE_ORDERS',                 "{$usam_table_prefix}orders" );
define( 'USAM_TABLE_ORDER_STATUS',           "{$usam_table_prefix}order_status" );		
define( 'USAM_TABLE_SHIPPED_PRODUCTS',       "{$usam_table_prefix}shipped_products" );	
define( 'USAM_TABLE_SHIPPED_DOCUMENTS',      "{$usam_table_prefix}shipped_documents" );			
define( 'USAM_TABLE_PAYMENT_HISTORY',        "{$usam_table_prefix}payment_history" );		
define( 'USAM_TABLE_RETURN_PURCHASES',       "{$usam_table_prefix}return_purchases" );
define( 'USAM_TABLE_RETURNED_PRODUCTS',      "{$usam_table_prefix}returned_products" );

define( 'USAM_TABLE_PRODUCTS_ORDER',         "{$usam_table_prefix}products_order" );
define( 'USAM_TABLE_ORDER_CHANGE_HISTORY',   "{$usam_table_prefix}order_change_history" );	
define( 'USAM_TABLE_ORDER_FEEDBACK',         "{$usam_table_prefix}order_feedback" );			
define( 'USAM_TABLE_ORDER_DISCOUNT',         "{$usam_table_prefix}order_discount" );	
define( 'USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER', "{$usam_table_prefix}discounts_products_order" );	
define( 'USAM_TABLE_ORDER_PROPS_GROUP',      "{$usam_table_prefix}order_props_group" );		
define( 'USAM_TABLE_ORDER_PROPS_VALUE',      "{$usam_table_prefix}order_props_value" );
define( 'USAM_TABLE_DOWNLOAD_STATUS',        "{$usam_table_prefix}download_status" );		

define( 'USAM_TABLE_COUNTRY',                "{$usam_table_prefix}country" );	
define( 'USAM_TABLE_CURRENCY',               "{$usam_table_prefix}currency_list" );	
define( 'USAM_TABLE_CURRENCY_RATES',         "{$usam_table_prefix}currency_rates" );			
define( 'USAM_TABLE_LOCATION',    			 "{$usam_table_prefix}location" );	
define( 'USAM_TABLE_LOCATION_TYPE',    	     "{$usam_table_prefix}location_type" );		
define( 'USAM_TABLE_STORAGE_LIST',           "{$usam_table_prefix}storage_list" );		

define( 'USAM_TABLE_PRODUCT_ATTRIBUTES',     "{$usam_table_prefix}product_attributes" );		
define( 'USAM_TABLE_TAXONOMY_RELATIONSHIPS', "{$usam_table_prefix}taxonomy_relationships" );
define( 'USAM_TABLE_ASSOCIATED_PRODUCTS',    "{$usam_table_prefix}associated_products" );
define( 'USAM_TABLE_PRODUCT_VIEWS',          "{$usam_table_prefix}product_views" );
define( 'USAM_TABLE_PRODUCT_FILTER',          "{$usam_table_prefix}product_filter" );
define( 'USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS', "{$usam_table_prefix}product_discount_relationships" );	
define( 'USAM_TABLE_PRODUCT_DAY',            "{$usam_table_prefix}product_day" );			

define( 'USAM_TABLE_PRICE_COMPARISON',       "{$usam_table_prefix}price_comparison" );	
define( 'USAM_TABLE_FEEDBACK',               "{$usam_table_prefix}feedback" );		
define( 'USAM_TABLE_MAILBOX',                "{$usam_table_prefix}mailbox" );
define( 'USAM_TABLE_MAILBOX_USERS',          "{$usam_table_prefix}mailbox_users" );
define( 'USAM_TABLE_EMAIL',                  "{$usam_table_prefix}email" );
define( 'USAM_TABLE_EMAIL_FILTERS',          "{$usam_table_prefix}email_filters" );
define( 'USAM_TABLE_EMAIL_FOLDERS',          "{$usam_table_prefix}email_folders" );
define( 'USAM_TABLE_EMAIL_FILE',             "{$usam_table_prefix}email_file" );		
define( 'USAM_TABLE_SMS',                    "{$usam_table_prefix}sms" );		
		
define( 'USAM_TABLE_CUSTOMER_BONUSES',       "{$usam_table_prefix}customer_bonuses" );
define( 'USAM_TABLE_CUSTOMER_REVIEWS',       "{$usam_table_prefix}customer_reviews" );
//	define( 'USAM_TABLE_MONEY',                  "{$usam_table_prefix}money" );	
//	define( 'USAM_TABLE_TRANSACT',               "{$usam_table_prefix}transact" );			
define( 'USAM_TABLE_CHAT',                  "{$usam_table_prefix}chat" );
define( 'USAM_TABLE_CHAT_DIALOGS',          "{$usam_table_prefix}chat_dialogs" );
define( 'USAM_TABLE_EVENTS',                "{$usam_table_prefix}events" );		
define( 'USAM_TABLE_EVENT_META',            "{$usam_table_prefix}event_meta" );	
define( 'USAM_TABLE_EVENT_ACTION_LIST',     "{$usam_table_prefix}event_action_list" );	
define( 'USAM_TABLE_EVENT_USERS',           "{$usam_table_prefix}event_users" );	

define( 'USAM_TABLE_COMMENTS',              "{$usam_table_prefix}comments" );	
define( 'USAM_TABLE_COMMENT_META',          "{$usam_table_prefix}comment_meta" );

define( 'USAM_TABLE_EVENT_RELATIONSHIPS',    "{$usam_table_prefix}event_relationships" );			

define( 'USAM_TABLE_CONTACTS',               "{$usam_table_prefix}contacts_list" );	
define( 'USAM_TABLE_COMPANY',                "{$usam_table_prefix}company" );	
define( 'USAM_TABLE_COMPANY_META',           "{$usam_table_prefix}company_meta" );			
define( 'USAM_TABLE_COMPANY_ACC_NUMBER',     "{$usam_table_prefix}company_acc_number" );		
define( 'USAM_TABLE_MEANS_COMMUNICATION',    "{$usam_table_prefix}means_communication_list" );	

define( 'USAM_TABLE_DOCUMENTS',              "{$usam_table_prefix}documents_crm" );	
define( 'USAM_TABLE_DOCUMENT_META',          "{$usam_table_prefix}document_meta" );	
define( 'USAM_TABLE_CUSTOMER_DOCUMENT',      "{$usam_table_prefix}customers_document" );
define( 'USAM_TABLE_PRODUCTS_DOCUMENT',      "{$usam_table_prefix}products_document" );		
		
define( 'USAM_TABLE_SUBSCRIBER_LISTS',       "{$usam_table_prefix}subscriber_lists" );
define( 'USAM_TABLE_NEWSLETTER_TEMPLATES',   "{$usam_table_prefix}newsletter_templates" );
define( 'USAM_TABLE_NEWSLETTER_USER_STAT',   "{$usam_table_prefix}newsletter_user_stat" );	
define( 'USAM_TABLE_NEWSLETTER_LISTS',       "{$usam_table_prefix}newsletter_lists" );			
define( 'USAM_TABLE_NEWSLETTER_TRIGGERED',   "{$usam_table_prefix}newsletter_triggered" );			

define( 'USAM_TABLE_TAXES',     	         "{$usam_table_prefix}taxes" );	
define( 'USAM_TABLE_DELIVERY_SERVICE',     	 "{$usam_table_prefix}delivery_service" );	
define( 'USAM_TABLE_PAYMENT_GATEWAY',     	 "{$usam_table_prefix}payment_gateway" );			

define( 'USAM_TABLE_ADVERTISING_BANNER',     "{$usam_table_prefix}advertising_banner" );	

define( 'USAM_TABLE_SLIDER',                 "{$usam_table_prefix}slider" );	
define( 'USAM_TABLE_SLIDES',                 "{$usam_table_prefix}slides" );

define( 'USAM_TABLE_KEYWORDS',               "{$usam_table_prefix}keywords" );	
define( 'USAM_TABLE_SITES',                  "{$usam_table_prefix}sites" );		
define( 'USAM_TABLE_STATISTICS_KEYWORDS',    "{$usam_table_prefix}statistics_keywords" );
define( 'USAM_TABLE_SEARCH_ENGINE_REGIONS',  "{$usam_table_prefix}search_engine_regions" );			

define( 'USAM_TABLE_SUPPORT_MESSAGE',        "{$usam_table_prefix}support_message" );	


 // Константы каталога загрузки
$upload_path = '';
$upload_url = '';
$wp_upload_dir_data = wp_upload_dir();

// Сообщение об ошибке
if ( isset( $wp_upload_dir_data['error'] ) )
	$error_msg = $wp_upload_dir_data['error'];

// Добавить путь
if ( isset( $wp_upload_dir_data['basedir'] ) )
	$upload_path = $wp_upload_dir_data['basedir'];

// добавить каталог
if ( isset( $wp_upload_dir_data['baseurl'] ) )
	$upload_url = $wp_upload_dir_data['baseurl'];

// Проверка SSL адресов
if ( is_ssl() )
	$upload_url = str_replace( 'http://', 'https://', $upload_url );

// Установка URL-адреса и папки для $usam_upload_sub_dir в папке upload
$usam_upload_sub_dir = '/universam/';
$usam_upload_dir     = $upload_path . $usam_upload_sub_dir;
$usam_upload_url     = $upload_url  . $usam_upload_sub_dir;

// субкаталоги внутри папки $usam_upload_sub_dir
$sub_dirs = array(
	'documents',
	'downloadables',		
	'user_uploads',	
	'theme_backup',				
	'exchange',
	'Log',		
);
// Расположение директории Upload 
define( 'USAM_UPLOAD_ERR', $error_msg );
define( 'USAM_UPLOAD_DIR', $usam_upload_dir );
define( 'USAM_UPLOAD_URL', $usam_upload_url );		
// Цикл по вложенным каталогам
foreach ( $sub_dirs as $sub_directory )
{
	$paths[] = trailingslashit( $usam_upload_dir . $sub_directory );
	$urls[]  = trailingslashit( $usam_upload_url . $sub_directory );	
}
// Указание пути
define( 'USAM_DOCUMENTS_DIR',    $paths[0] );
define( 'USAM_FILE_DIR',         $paths[1] );		
define( 'USAM_USER_UPLOADS_DIR', $paths[2] );
define( 'USAM_THEME_BACKUP_DIR', $paths[3] );			

// Определение URL-адресов
define( 'USAM_DOCUMENTS_URL',    $urls[0] );
define( 'USAM_FILE_URL',         $urls[1] );	
define( 'USAM_USER_UPLOADS_URL', $urls[2] );
define( 'USAM_THEME_BACKUP_URL', $urls[3] );




/**
 * Инициализация базовых констант
 */
class USAM_Load_Constants
{	
	function __construct() 
	{			
		add_filter( 'rewrite_rules_array', array(&$this, 'taxonomy_rewrite_rules'), 99 ); // Добавляет в новых правила перезаписи
		add_filter( 'intermediate_image_sizes_advanced', array( &$this, 'intermediate_image_sizes_advanced'), 10, 1 );
		
		add_action( 'init', array( $this, 'load_thumbnail_sizes' ) );	 // Загрузка размеров эскизов	
	}	
	
	/*
	 * Загружает основные размеры миниатюр продуктов
	 */
	function load_thumbnail_sizes() 
	{
		$product_image = get_option( 'usam_product_image' );
		$single_view_image = get_option( 'usam_single_view_image' );	
		add_image_size( 'product-thumbnails', $product_image['width'], $product_image['height'], get_option( 'usam_crop_thumbnails', false )  );
		add_image_size( 'admin-product-thumbnails', 60, 60, get_option( 'usam_crop_thumbnails', true )  );
		add_image_size( 'cart-product-thumbnails', 100, 100, get_option( 'usam_crop_thumbnails', false )  );
		//add_image_size( 'featured-product-thumbnails', 425, 215, get_option( 'usam_crop_thumbnails', true )  );			

		add_image_size( 'medium-single-product', $single_view_image['width'], $single_view_image['height'], get_option( 'usam_crop_thumbnails', false) ); // Средний
		add_image_size( 'small-product-thumbnail', 60, 60, get_option( 'usam_crop_thumbnails', false ) ); // Маленький
	}
	
	function intermediate_image_sizes_advanced($sizes)
	{	
		$crop = get_option( 'usam_crop_thumbnails', false );		
		
		$sizes['small-product-thumbnail'] = array( "width" => 60, "height" => 60, "crop" => $crop );
		
		$single_view_image = get_option( 'usam_single_view_image' );	
		$sizes['medium-single-product'] = array( "width" => $single_view_image['width'], "height" => $single_view_image['height'], "crop" => $crop );
		
		/*$sizes['featured-product-thumbnails']=array( "width" => 425, "height" => 215,	"crop" => $crop	);*/
		$sizes['admin-product-thumbnails'] = array( "width" => 60, "height" => 60, "crop" => true );	
		
		$product_image = get_option( 'usam_product_image' );		
		$sizes['product-thumbnails'] = array( "width" => $product_image['width'], "height" => $product_image['height'], "crop" => $crop	);
		$gallery_image = get_option( 'usam_gallery_image' );
		$sizes['gallery-thumbnails'] = array( "width" => $gallery_image['width'], "height" => $gallery_image['height'], "crop" => $crop );
		return $sizes;
	}
	
	
	/**
	 * Добавляет в новых правила перезаписи для категорий, продукции, страницы категорий, и неоднозначные(либо категорий или продуктов)
	 * Также изменяет правила перезаписи для URL-адресов продукции, чтобы добавить в типе записи.
	 * @since 3.8
	 */	 
	function taxonomy_rewrite_rules( $rules ) 
	{ 
		$permalinks        = get_option( 'usam_permalinks' );
		$product_permalink = empty( $permalinks['product_base'] ) ? '' : trim($permalinks['product_base']);
		if ( $product_permalink == '/products/%product_cat%' )
			$product_permalink = 'products';	
		
		$products = array();		
		$products["{$product_permalink}/(.+?)/(.+?)$"] = 'index.php?post_type=usam-product&usam-product=$matches[2]';	
		$rules = array_merge($products, $rules);	
		
		// используется, чтобы вывести товары следующей страницы на странице товаров
		$products_list = usam_get_system_page_name( 'products-list' );
		$new_rules_products_page = array();			
		$new_rules_products_page['('.$products_list.')/page/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&paged=$matches[2]';				
		$rules = array_merge($new_rules_products_page, $rules);

		// используется, чтобы найти страницу категории на странице распродаж
		$new_rules_sale_page = array();			
		$new_rules_sale_page['(sale)/page/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&paged=$matches[2]';			
		$new_rules_sale_page['(sale)/(.+?)/page/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&usam-category=$matches[2]&paged=$matches[3]';
		$new_rules_sale_page['(sale)/(.+?)/?$'] = 'index.php?pagename=$matches[1]&usam-category=$matches[2]';			
		$rules = array_merge($new_rules_sale_page, $rules);
		
		$new_rules_new_arrivals = array();
		$new_rules_new_arrivals['(new-arrivals)/page/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&paged=$matches[2]';			
		$new_rules_new_arrivals['(new-arrivals)/(.+?)/page/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&usam-category=$matches[2]&paged=$matches[3]';
		$new_rules_new_arrivals['(new-arrivals)/(.+?)/?$'] = 'index.php?pagename=$matches[1]&usam-category=$matches[2]';
		$rules = array_merge($new_rules_new_arrivals, $rules);		
		// используется, чтобы найти страницу search
		$new_rules_search = array();
		$new_rules_search['(search)/(.+?)/(.+?)/?$'] = 'index.php?pagename=search&keyword=$matches[3]';	
		$new_rules_search['(keyword)/(.+?)/?$'] = 'index.php?pagename=search&keyword=$matches[2]';
		$new_rules_search['(search)$'] = 'index.php?pagename=search';			
		$rules = array_merge($new_rules_search, $rules);			
			
		// используется, чтобы найти страницу
		$new_rules = array();		
		
		$new_rules['(login)?$'] = 'index.php?pagename=login';	
		$virtual_page =  usam_virtual_page( );
		foreach ( $virtual_page as $page_slug => $page )
		{
			if ( $page_slug != 'your-account' || $page_slug != 'transaction-results'  )
				$new_rules["($page_slug)?$"] = "index.php?pagename=$page_slug";
		}
		$rules = array_merge($new_rules, $rules);			
		
		// используется, чтобы найти страницу в профиле пользователя
		$new_rules = array();
		$new_rules['(your-account)/(.+?)/(.+?)/?$'] = 'index.php?pagename=your-account&tabs=$matches[2]&subtab=$matches[3]';	
		$new_rules['(your-account)/(.+?)/?$'] = 'index.php?pagename=your-account&tabs=$matches[2]';	
		$new_rules['(your-account)$'] = 'index.php?pagename=your-account';	
		$rules = array_merge($new_rules, $rules);		
		
		$new_rules = array();
		$new_rules['(transaction-results)/(.+?)/(.+?)/?$'] = 'index.php?pagename=transaction-results&tabs=$matches[2]&gateway_id=$matches[3]';	
		$new_rules['(transaction-results)/(.+?)/?$'] = 'index.php?pagename=transaction-results&tabs=$matches[2]';	
		$new_rules['(transaction-results)$'] = 'index.php?pagename=transaction-results';	
		$rules = array_merge($new_rules, $rules);	
			
	/*	$new_rules_brands = array();
	//	$new_rules_brands['(brands)/(.+?)/(.+?)/?$'] = 'index.php?usam-brands=$matches[1]&usam-category=$matches[2]&paged=$matches[3]';	
		$new_rules_brands['(brands)/(.+?)/?$'] = 'index.php?usam-brands=$matches[1]&paged=$matches[2]';	
		$new_rules_brands['(brands)/?$'] = 'index.php?usam-brands=$matches[1]';	
		$rules = array_merge($new_rules_brands, $rules);
		*/		
		
		$new_rules_category_sale = array();	
		$new_rules_category_sale['category_sale/(.+?)/(.+?)/page/([0-9]+)/?$'] = 'index.php?usam-category_sale=$matches[1]&usam-category=$matches[2]&paged=$matches[3]';	
		$new_rules_category_sale['category_sale/(.+?)/page/([0-9]+)/?$'] = 'index.php?usam-category_sale=$matches[1]&paged=$matches[2]';			
		$new_rules_category_sale['category_sale/(.+?)/(.+?)/?$'] = 'index.php?usam-category_sale=$matches[1]&usam-category=$matches[2]';				
		$rules = array_merge($new_rules_category_sale, $rules);
		
		return $rules;
	}
}
new USAM_Load_Constants();



function usam_feedback_message()
{		
	$message = array( 'price_comparison' => "<p><strong>Этот продукт продается в другом месте по более низкой цене?</strong>Пожалуйста, заполните форму ниже и мы будем работать с нашими поставщиками для того чтобы представить вам более выгодную цену.</p><br><p>Если заявленный товар идентичен товару ".get_bloginfo( 'name' )." и найдено более выгодное предложение, то цена на продукт на странице будет обновлена в течение 48 часов (пн-пт) после того, как получена заявка.</p>",
	
	'product_error' => "<p>Помогите нам постоянно совершенствоваться, сообщая об ошибках на этой странице!<br>Если какая-либо информация является неверной, то, пожалуйста, сообщите нам здесь. Вся информация будет просмотрена в течение 48 часов (пн-пт).</p>",
	
	'product_info' => "<p>Отправьте ваш вопрос в Центр поддержки клиентов. Наши специалисты ответят вам в самое ближайшее время.<br>Вся информация будет просмотрена в течение 48 часов (пн-пт).</p>",
	'all' => "<p>Отправьте ваш вопрос в наш Центр поддержки клиентов. Наши специалисты ответят вам в самое ближайшее время.<br>Вся информация будет просмотрена в течение 48 часов (пн-пт).</p>",
	'buy_product' => "<p>Этот товар есть на других наших складах. Оставьте свой заказ и мы привезем его специально для вас. Подробности о вариантах получения и способах оплаты вам сообщит наш менеджер</p>",
	'ask_question' => "<p>Отправьте ваш вопрос в Центр поддержки клиентов. Наши специалисты ответят вам в самое ближайшее время.<br>Вся информация будет просмотрена в течение 48 часов (пн-пт).</p>",
	'notify_stock' => "<p>Оставьте ваши данные и мы сообщим вам когда товар появится в продаже.</p>",
	);
	return $message;
}


function usam_system_pages()
{
	$pages = array(
		'products-list' => array(			
			'title' =>  __( 'Каталог', 'usam'),
			'content' => '[productspage]',
			'name' => 'products-list',			
		),		
		'sale' => array(		
			'title' =>  __( 'Распродажа', 'usam'),
			'content' => '[sale]',
			'name' => 'sale',			
		),
		'special-offer' => array(			
			'title' =>  __('Товар дня', 'usam'),
			'content' => '[special_offer]',
			'name' => 'special-offer',			
		),			
		'new-arrivals' => array(			
			'title' =>  __('Новинки', 'usam'),
			'content' => '[newarrivals]',
			'name' => 'new-arrivals',			
		),	
		'reviews' => array(			
			'title' => __( 'Отзывы', 'usam' ),
			'content' => '[reviews]',
			'name' => 'reviews',
		),	
		'brands' => array(			
			'title' => __( 'Бренды', 'usam' ),
			'content' => '[brands]',
			'name' => 'brands',
		),	
		'recommend' => array(			
			'title' => __( 'Рекомендуемые товары', 'usam' ),
			'content' => '[recommend]',
			'name' => 'recommend',
		),		
		'popular' => array(			
			'title' => __( 'Популярные товары', 'usam' ),
			'content' => '[popular]',
			'name' => 'popular',
		),			
	);	
	return $pages;
}

function usam_virtual_page( ) 
{	
	$pages = array( 
		'search' => array('slug' => 'search',  'title' => __('Поиск','usam'), 'content' => "[search]" ),   
		'map' => array('slug' => 'map',  'title' => __('Карта категорий','usam'), 'content' => "[map]" ),
		'compare' => array('slug' => 'compare',  'title' => __('Сравнение товаров','usam'), 'content' => "[compare_products]" ),
		'wish-list' => array('slug' => 'wish-list',  'title' => __('Избранное','usam'), 'content' => "[wishlist]" ),   
		'checkout' => array('slug' => 'checkout',  'title' => __('Оформить заказ','usam'), 'content' => "[checkout]" ),   
		'transaction-results' => array('slug' => 'transaction-results',  'title' => __('Ваш заказ принят!','usam'), 'content' => "[transaction_results]" ),		
		'basket' => array('slug' => 'basket',  'title' => __('Корзина','usam'), 'content' => "[basket]" ),   
		'your-account' => array('slug' => 'your-account',  'title' => __('Личный кабинет','usam'), 'content' => "[your_account]" ),   
		'your-subscribed' => array('slug' => 'your-subscribed',  'title' => __('Подписка на новости','usam'), 'content' => "[your_subscribed]")
	); 	
	return $pages;
}

function usam_get_message_transaction() 
{			
	$message = array(
		'completed' => array( 'title' => __('Успешная транзакция','usam'), 'text' => file_get_contents( USAM_FILE_PATH . '/includes/basket/message_transaction/completed.htm' ) ),
		'insufficient_funds' => array( 'title' => __('Недостаточно средств для оплаты','usam'), 'text' => sprintf(  __( 'Извините Ваша сделка не была принята из-за нехватки средств.<br /><a href="%1$s">Нажмите здесь, чтобы вернуться к странице оформления заказа</a>.', 'usam' ), usam_get_url_system_page('checkout') ) ),
		'fail' => array( 'title' => __('Сделка не была принята','usam'), 'text' => sprintf( __( 'К сожалению ваша сделка не была принята.<br /> Нажмите <a href="%1$s">здесь</a>, чтобы вернуться к странице оформления заказа.', 'usam' ), usam_get_url_system_page('checkout')) ),
		'unknown_transaction' => array( 'title' => __('Не известная транзакция','usam'),'text' => sprintf( __( 'Сервер вернул не известную транзакцию. Мы проверим данные и свяжемся с вами. <br /> Нажмите <a href="%1$s">здесь, чтобы вернуться к товарам.</a>', 'usam' ), usam_get_url_system_page('product_list')) ),
		'old_transaction' => array( 'title' => __('Время оплаты вышло','usam'),'text' => sprintf( __( 'К сожалению это заказ нельзя оплатить. Прошло слишком много времени. Создайте заказ по новой. <br /><a href="%1$s">Нажмите здесь, чтобы вернуться к товарам</a>.', 'usam' ), usam_get_url_system_page('product_list')) ),
		'order_paid' => array( 'title' => __('Заказ уже оплачен','usam'),'text' => sprintf( __( 'Этот заказ уже оплачен. <br /><a href="%1$s">Нажмите здесь, чтобы посмотреть его в Вашем кабинете</a>.', 'usam' ), usam_get_url_system_page('your-account')) ),
		'gateway' => array( 'title' => __('Ошибка платежного шлюза','usam'),'text' => sprintf( __( 'Через выбранный платежный шлюз нельзя оплатить или он отключен. Попробуйте еще раз.', 'usam' )) ),
	);
	return $message;
}

function usam_get_site_bots( )
{
	$bots = array(
		'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
		'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
		'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
		'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
		'dataparksearch','google-sitemaps','appengine-google','feedfetcher-google',
		'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
		'googlealert.com','seo-rus.com','yadirectbot','yandeg','yandex','yandeximages','yandexbot','yandexvideo','yandexwebmaster','yandexmedia','yandexblogs','yandexdirect',
		'yandexsomething','copyscape.com','adsbot-google','domaintools.com',
		'nigma.ru','bing.com','dotnetdotcom',		
	);
	return $bots;
}


/*	$current_offset = get_option('gmt_offset');
$tzstring = get_option('timezone_string');

if ( false !== strpos($tzstring,'Etc/GMT') )
	$tzstring = '';

if ( empty($tzstring) ) 
{			
	if ( 0 == $current_offset )
		$tzstring = '+0';
	elseif ($current_offset < 0)
		$tzstring = $current_offset;
	else
		$tzstring = '+' . $current_offset;
}			
date_default_timezone_set( 'Etc/GMT'.$tzstring );
*/