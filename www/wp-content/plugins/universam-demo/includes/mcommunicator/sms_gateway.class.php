<?php
// Phone Gateway
abstract class USAM_SMS_Gateway 
{
	protected $login = null;
	protected $password = null;
	
	protected $number_message = null;
		
	function __construct(  ) 
	{		
		$sms_gateway = get_option( 'usam_sms_gateway_option' );
		$this->login = $sms_gateway['login'];
		$this->password = $sms_gateway['password'];					
	}
	
	public function send_message( $phone, $message, $naming = '', $object_id = 0, $object_type = '' )
	{			
		if ( empty($phone) || empty($message) )
			return false;
		
		$result = false;				
		
		require_once( USAM_FILE_PATH .'/includes/feedback/sms_query.class.php'  );
		$args = array( 'date_query' => array( 'year' => date('Y'), 'month'  => date('n') ), 'fields' => 'id' );			
		$sms = usam_get_sms_query( $args );					
		
		$args = array( 'phone' => $phone, 'folder' => 'outbox', 'message' => $message, 'object_id' => $object_id, 'object_type' => $object_type );		
		
		$max_number = get_option("usam_max_number_of_sms_month");	
		if ( empty($max_number) || count($sms) <= $max_number )
		{		
			$result = $this->send( $phone, $message, $naming );		
			if ( $result )
			{				
				$args['folder'] = 'sent';
				$args['sent_at'] = date( "Y-m-d H:i:s" );	
				$args['number'] = $this->number_message;	
			}
		}	
		if ( apply_filters( 'usam_insert_send_sms', true, $args ) )
		{ 
			usam_insert_sms( $args );
		} 
		return $result;
	}	
	
	protected function set_error( $error )
	{
		usam_log_file( __('Ошибки смс шлюза','usam').' '.$error );
	}
}

/**
 * Отправка смс сообщения
 */
function usam_send_sms( $phone, $message, $object_id = 0, $object_type = '' )
{	
	$sms_gateway = get_option( 'usam_sms_gateway' );	
	$filename = USAM_FILE_PATH ."/includes/mcommunicator/gateway/{$sms_gateway}.php";
	
	if ( file_exists($filename) && !LOCALHOST ) 
	{
		$title = get_option( 'usam_sms_gateway_name' ); 			
		require_once( $filename );		
		$class_name = "USAM_SMS_Gateway_$sms_gateway";	
		$gateway = new $class_name();				
		$sent = $gateway->send_message( $phone, $message, $title, $object_id, $object_type );
	}
	else
		$sent = false;
	return $sent;	
}
?>