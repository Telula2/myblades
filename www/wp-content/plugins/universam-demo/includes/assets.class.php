<?php
/**
 * Загрузка стилей и скриптов.
 * @version     2.2.0
 */

class USAM_Assets 
{	
	private $version = '';	
	private $core_js_url = '';	
	
	public function __construct() 
	{			
		if ( defined('WP_DEBUG') && WP_DEBUG  )
			$this->version = time();		
		else
			$this->version = USAM_VERSION;	
		
		$this->core_js_url = USAM_URL . '/assets/js';
		
		add_action( 'init', array( $this, 'register' ) );		
		
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_bar' ), 2 );		
		add_action( 'wp_enqueue_scripts', array( $this, 'admin_bar' ), 2 );	
		
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_script_and_css' ), 5 );			
		add_action( 'wp_enqueue_scripts-usam-chat-iframe', array($this, 'theme_register_css'), 1 );	
	}
	
	/**
	 * Загрузка скриптов
	 */
	public function admin_bar( $pagehook ) 
	{  		
		if ( is_user_logged_in() &&  usam_check_current_user_role( 'administrator' ) || usam_check_current_user_role('shop_manager') )
		{
			wp_enqueue_script( 'usam-admin_bar' );
			wp_localize_script( 'usam-admin_bar', 'USAM_Admin_Bar', array(
					'ajaxurl'   => 'index.php',	
					'number_of_unread_menager_chat_messages_nonce' => usam_create_ajax_nonce( 'number_of_unread_menager_chat_messages' ),
					'delete_event_task_manager_nonce'      => usam_create_ajax_nonce( 'delete_event_task_manager' ), // Удалить задание из диспетчера задач
					)
				);			
			wp_enqueue_style( 'usam-form' );						
		}	
	}	
	
	/**
	 * Зарегистрировать стили и скрипты
	 */
	public function register( ) 
	{						
		$scripts = array( 
			'usam-rating' => array( 'file_name' => 'rating', 'deps' => array( 'jquery' ), 'in_footer' => false ),//Рейтинг
			'hc-sticky' => array( 'file_name' => 'jquery.hc-sticky.min', 'deps' => array( 'jquery' ), 'in_footer' => false ),//Фиксированное меню
			'usam-countdown' => array( 'file_name' => 'jquery.countdown', 'deps' => array( 'jquery' ), 'in_footer' => false ),//счетчик времени
			'd3' => array( 'file_name' => 'd3.v3.min', 'deps' => array( 'jquery' ), 'in_footer' => false ),//График
			'usam-customer-reviews' => array( 'file_name' => 'customer-reviews', 'deps' => array( 'jquery' ), 'in_footer' => false ),//отзывы клиентов
			'usam-productzoo' => array( 'file_name' => 'productzoo', 'deps' => array( 'jquery' ), 'in_footer' => true ),
			'usam-variation-image-swap' => array( 'file_name' => 'variation-image-swap', 'deps' => array( 'jquery' ), 'in_footer' => true ),
			'usam-chat' => array( 'file_name' => 'chat', 'deps' => array( 'jquery' ), 'in_footer' => false ),//Чат
			'usam-admin_bar' => array( 'file_name' => 'admin_bar', 'deps' => array( 'jquery' ), 'in_footer' => false ),
			'bootstrap' => array( 'file_name' => 'bootstrap', 'deps' => array( 'jquery' ), 'in_footer' => true ),//Модальные окна
			'jcarousel' => array( 'file_name' => 'jquery.jcarousel', 'deps' => array( 'jquery' ), 'in_footer' => true ),//Карусель
			'masked_input' => array( 'file_name' => 'jquery.maskedinput', 'deps' => array( 'jquery' ), 'in_footer' => true ),// Маска для input
			'chosen' => array( 'file_name' => 'chosen/chosen.jquery', 'deps' => array( 'jquery' ), 'in_footer' => true ),// красивые селекты
			'usam-tab' => array( 'file_name' => 'tab', 'deps' => array( 'jquery' ), 'in_footer' => true ),// вкладки
			'usam-slider' => array( 'file_name' => 'slider', 'deps' => array( 'jquery' ), 'in_footer' => true ),// слайдер							
			'fileupload' => array( 'file_name' => 'jquery.fileupload', 'deps' => array( 'jquery' ), 'in_footer' => true ),// загрузка файлов			
			'iframe-transport' => array( 'file_name' => 'jquery.iframe-transport', 'deps' => array( 'jquery' ), 'in_footer' => true ),// загрузка файлов	
			'knob' => array( 'file_name' => 'jquery.knob', 'deps' => array( 'jquery' ), 'in_footer' => true ),// загрузка файлов				
		);		
		foreach( $scripts as $name => $script )
		{
			wp_register_script( $name, $this->core_js_url.'/'.$script['file_name'].'.js', $script['deps'], $this->version, $script['in_footer'] );	
		}		
		wp_register_script( 'yandex_maps', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', array( 'jquery' ), $this->version );
		
		$styles = array( 				
			'chosen-style' => array( 'file_name' => 'chosen', 'deps' => array(), 'media' => 'all' ),	//Красивые селекты		
		);		
		foreach( $styles as $name => $style )
		{ 
			wp_register_style( $name, USAM_URL . '/assets/css/'.$style['file_name'].'.css', $style['deps'], $this->version, $style['media'] );	
		}
		wp_register_style( 'usam-form', USAM_URL .'/admin/css/form.css', false, $this->version );	
		wp_register_style( 'usam-chat', USAM_URL .'/admin/css/chat.css', false, $this->version, 'all', true );		
	}
	
	function theme_register_css() 
	{		
		$styles = array( 					
			'usam-theme' => array( 'file_name' => 'usam-default', 'deps' => false, 'media' => 'all' ),	
			'usam-chat-iframe' => array( 'file_name' => 'usam-chat', 'deps' => array(), 'media' => 'all' ),				
		);		
		foreach( $styles as $name => $style )
		{
			wp_register_style( $name, usam_get_template_file_url( $style['file_name'].'.css' ), $style['deps'], $this->version, $style['media'] );	
		}	
	}
	
	/**
	 * поставить в очередь все JavaScript и CSS
	 */
	function theme_script_and_css() 
	{
		global $wp_styles, $usam_theme_url, $wp_query, $post, $user_ID;
		
		if ( has_filter( 'usam_enqueue_user_script_and_css' ) && apply_filters( 'usam_mobile_scripts_css_filters', false ) )
			do_action( 'usam_enqueue_user_script_and_css' );	
		else 
		{						
			$page_id = isset($post->ID) ? $post->ID : 0;			
			$post_content = isset($post->post_content) ? $post->post_content : '';
			
			$ssl = 'http';
			if ( is_ssl() )
				$ssl = 'https';	

			$this->theme_register_css();	

			wp_register_script( 'usam-feedback', USAM_CORE_JS_URL. '/feedback.js', array( 'jquery' ), $this->version );			
			
			wp_enqueue_style( 'dashicons' );			
			wp_enqueue_script( 'usam-tab' );			
			
			wp_enqueue_script( 'jquery' );				
			wp_enqueue_script( 'jquery-ui-slider' );
			
			wp_enqueue_script( 'livequery', $this->core_js_url. '/jquery.livequery.js', array( 'jquery' ), $this->version );				

			wp_enqueue_script( 'jcarousel' );
		
			wp_enqueue_script( 'universam-theme',          $this->core_js_url	. '/universam.theme.js',      array( 'jquery' ), $this->version );	
			wp_localize_script( 'universam-theme', 'USAM_Theme', array(
				'ajaxurl'   => 'index.php',				
				'spinner'   => esc_url( USAM_CORE_IMAGES_URL."/loading.gif" ),
				'page_id'   => $page_id,
				)
			);							
			if ( !empty( $post ) )
			{
				if ( !empty($post->post_name) ) 
				{
					switch ( $post->post_name ) 
					{
						case 'checkout' :
							wp_enqueue_script( 'usam-checkout-page', $this->core_js_url. '/theme/checkout-page.js', array( 'jquery' ), $this->version );
							wp_enqueue_script( 'yandex_maps' );
							wp_enqueue_script( 'masked_input' ); 
						break;
						case 'your-account' :
							wp_enqueue_script( 'usam-your-account-page', $this->core_js_url. '/theme/your-account.js',    array( 'jquery' ), $this->version );
							wp_enqueue_script( 'jquery-ui-accordion' );								
							wp_enqueue_script( 'jquery-ui-datepicker' );							
						break;	
						case 'special-offer' :
							wp_enqueue_script( 'usam-countdown' );	
						break;	
					}				
					if( stristr($post_content, '[readysets]') !== false)
					{
						wp_enqueue_script( 'usam-ready_sets-page', $this->core_js_url. '/theme/ready_sets-page.js',  array( 'jquery' ), $this->version, true );
					}
				}
			}					
			wp_enqueue_script( 'usam-rating' );
			wp_enqueue_script( 'bootstrap' );			
			if ( get_option( 'usam_show_thumbnails_thickbox' ) == 1 )
			{
				$lightbox = get_option('usam_lightbox', 'thickbox');		
				if( $lightbox == 'thickbox' )
				{
					wp_enqueue_script( 'usam-thickbox',$this->core_js_url . '/thickbox.js',   array( 'jquery' ), $this->version );
					wp_enqueue_style(  'usam-thickbox', usam_get_template_file_url( 'thickbox.css' ), false, $this->version, 'all' );
				} 
				elseif( $lightbox == 'colorbox' )
				{
					wp_enqueue_script( 'colorbox-min',	$this->core_js_url . '/jquery.colorbox-min.js', array( 'jquery' ), $this->version );
					wp_enqueue_script( 'usam_colorbox',	$this->core_js_url . '/usam_colorbox.js', array( 'jquery', 'colorbox-min' ), $this->version );
					wp_enqueue_style( 'usam_colorbox-css', usam_get_template_file_url('colorbox.css'), false, $this->version, 'all' );
				}
			}		
			$openapi = false;
			/*if ( get_option( "usam_vk_сollect_customer_data" ) && is_user_logged_in() && empty($_SESSION['not_receive_vk_id']) )
			{ 				
				$vk_user_id = get_user_meta( $user_ID, 'usam_vk_user_id', true );			
				if ( !empty($vk_user_id) )				
				{			
					$_SESSION['not_receive_vk_id'] = true;
					$vk_api = get_option('usam_vk_api', array() );						 
					if ( !empty($vk_api['api_id']) )	
					{								
						$openapi = true;
						wp_enqueue_script( 'vk',  $this->core_js_url.'/vk.js', array( 'jquery', 'vk-openapi' ), $this->version, true );															
						wp_localize_script( 'vk', 'USAM_VK', array(	'api' => $vk_api['api_id'], )	);	
					}
				}
			}		*/
			if( usam_is_product() && usam_show_vk_like() || $openapi )
			{
				wp_enqueue_script( 'vk-openapi', 'http://vk.com/js/api/openapi.js', '1' );	// Авторизация в контакте				
			}								
			if ( usam_is_product() || !empty($post) && $post->post_name == 'special-offer' )
			{			
				if( get_option( 'usam_share_this' ) == 1 ) 
				{   // Показать Share This (социальные закладки)
					$remote_protocol = is_ssl() ? 'https://ws' : 'http://w';
					wp_enqueue_script( 'sharethis', $remote_protocol . '.sharethis.com/button/buttons.js', array(), $this->version, true );
				}			
				wp_enqueue_script( 'usam-variation-image-swap' );			
				wp_enqueue_script( 'usam-productzoo' );
				
				$localized_vars = array(
					'ajaxurl' => admin_url( 'admin-ajax.php', $ssl ),
					'image_swap_nonce' => wp_create_nonce( 'image_swap' )
				);		
				wp_localize_script( 'variation-image-swap', 'USAM_Variation_Image_Swap', $localized_vars );
			}		
			elseif ( empty($post) || $post->post_name != 'your-account' && $post->post_name != 'basket' && $post->post_name != 'checkout' )
			{
				wp_enqueue_script( 'usam-product_filter',  $this->core_js_url.'/product-filter.js', array( 'jquery' ), $this->version );	
				wp_localize_script( 'usam-product_filter', 'USAM_Product_Filter', array(
					'ajaxurl'   => admin_url( 'admin-ajax.php' ),
					'spinner'   => esc_url( USAM_CORE_IMAGES_URL."/loading.gif" ),
					'page'      => isset($wp_query->query_vars['pagename'])?$wp_query->query_vars['pagename']:'',
					'cat'       => isset($wp_query->query_vars['usam-category'])?$wp_query->query_vars['usam-category']:'',
					)
				);		
			}		
			wp_add_inline_style( 'usam-theme-css', usam_get_user_dynamic_css() );		
			wp_enqueue_style( 'usam-theme' );	
		}		
	}	
}
new USAM_Assets();
/*
Скрипты, которые идут в комплекте с WordPress
https://wp-kama.ru/function/wp_enqueue_script
*/
?>