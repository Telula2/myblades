<?php				
new USAM_Autocomplete_Forms();
class USAM_Autocomplete_Forms
{		
	private $limit = 20;
	function __construct( )
	{	
		add_action('wp_ajax_autocomplete', array($this,'handler_ajax'));
		add_action('wp_ajax_nopriv_autocomplete', array($this,'handler_ajax'));	
	}	
	
	public static function add_frontend_script_style() 
	{
		wp_enqueue_script('jquery');	
		wp_enqueue_script( 'jquery-ui-autocomplete' );
	}
	
	public function handler_ajax() 
	{	
		if ( isset($_REQUEST['term']) && isset($_REQUEST['get']) )
		{	
			check_ajax_referer( $_REQUEST['get'], 'security' );	
			$search = sanitize_text_field($_REQUEST['term']);				
			$result = array();
			
			$callback = 'controller_'.sanitize_title($_REQUEST['get']);
			if ( method_exists( $this, $callback )) 
				$result = $this->$callback( $search );	

			echo json_encode( $result );			
		}
		exit;			
	}
	
	private function get_url( $method ) 
	{
		$url = add_query_arg( array( 'action' => 'autocomplete', 'get' => $method, 'security' => wp_create_nonce( $method ) ), admin_url( 'admin-ajax.php', 'relative' ) );	
		return $url;
	}
	
	private function get_locations_str( $location_id ) 
	{
		$str = '';
		$tree_locations = usam_get_array_locations_up( $location_id );		
		foreach ( $tree_locations as $tree_location ) 
		{			
			$str .= ", ".$tree_location['name'];					
		}					
		return $str;		
	}
	
	// Вывод нескольких результатов, сразу при вводе поиска
	public static function controller_search_keyword( $search_keyword ) 
	{	
		add_filter( 'posts_search', array('USAM_Search_Hook_Filter', 'search_by_site'), 500, 2 );
		add_filter( 'posts_orderby', array('USAM_Search_Hook_Filter', 'posts_orderby'), 500, 2 );
		add_filter( 'posts_join', array('USAM_Search_Hook_Filter', 'product_join'), 500, 2 );
		add_filter( 'posts_request', array('USAM_Search_Hook_Filter', 'posts_request_unconflict_role_scoper_plugin'), 500, 2);		
		
		$exclude_products = get_option('usam_search_exclude_products', '');
		if (is_array($exclude_products))
			$id_excludes = implode(",", $exclude_products);
		else
			$id_excludes = $exclude_products;
	
		$cat_slug = '';
		$tag_slug = '';			
		
		$items = array();
		if ($search_keyword != '') 
		{
			$extra_parameter = '';
			$text_lenght = get_option('usam_search_length_description_items', 100);	
			$global_search = get_option('usam_search_global_search', 1);			
								
			$row = isset($_REQUEST['row']) ? absint($_REQUEST['row']):get_option('usam_search_result_items', 5);
			$text_lenght = isset($_REQUEST['text_lenght']) ? absint($_REQUEST['text_lenght']):get_option('usam_search_length_description_items', 100);				
			
			$args = array( 's' => $search_keyword, 'posts_per_page' => $row+1, 'offset'=> 0, 'orderby' => 'predictive', 'order' => 'ASC', 'post_status' => 'publish', 'exclude' => $id_excludes, 'suppress_filters' => FALSE, 'update_post_term_cache' => false );		
			$length_name = get_option('usam_search_length_name_items', '');		
			
			$search_products = usam_get_products($args);						
			if ( $search_products && count($search_products) > 0 ) 
			{					
				$html = "<div class='ajax_search_content_title'>".__('Товары', 'usam')."</div>";
				$items[] = array( 'type' => 'not_selection', 'label' => $html, 'value' => '' );				
				$end_row = $row;
				foreach ( $search_products as $product ) 
				{					
					if ( $length_name != '' )
						$name = usam_limit_words(stripslashes( $product->post_title), $length_name, false);
					else
						$name = stripslashes( $product->post_title);
					$link_detail = get_permalink($product->ID);					
					$avatar = usam_get_product_thumbnail($product->ID, array(100, 100), $product->post_title ); 										
					$description_output = USAM_Search_Shortcodes::get_product_description( $product );												
					$item = '<div class="ajax_search_content"><div class="result_row"><span class="rs_avatar"><a href="'.$link_detail.'">'.$avatar.'</a></span><div class="rs_content_popup"><a href="'.$link_detail.'"><span class="rs_name">'.$name.'</span><span class="rs_description">'.$description_output.'</span></div></a></div></div>';
					
					$items[] = array( 'type' => 'simple', 'label' => $item, 'value' => $name );
					$end_row--;
					if ($end_row < 1) 
						break;					
				}			
				if ( count($search_products) > $row ) 
				{
					if (get_option('permalink_structure') == '')
						$link_search = usam_get_url_system_page( 'search' ).'&rs='.$search_keyword.$extra_parameter;
					else
						$link_search = rtrim( usam_get_url_system_page( 'search' ), '/' ).'/keyword/'.$search_keyword.$extra_parameter;
					
					$html = '<div class="more_result" rel="more_result"><a href="'.$link_search.'">'.__('Показать все результаты поиска по', 'usam').' '.$search_keyword.' <span class="see_more_arrow"></span></a><p>'.sprintf( _n( 'Показаны первые %s результата.', 'Показаны первые %s результатов', $row, 'usam' ), $row ).'</p></div>';
					
					$items[] = array( 'type' => 'not_selection', 'label' => $html, 'value' => '' );
				}
			} 
			else 
			{
				$items[] = array( 'type' => 'not_selection', 'label' => "<div class= 'ajax_no_result'>".__('Ничего не найдено. Попробуйте изменить параметры поиска','usam')."</div>", 'value' => '' );
			}
		}
		echo json_encode( $items );		
		exit();
	}	
		
	// Определение позиции
	public function controller_position_location( $search ) 
	{		
		$search = "*".$search."*";		

		$items = array();	
		
		$query = array( 'search' => $search, 'number' => $this->limit );	
		if ( !empty($_REQUEST['type']))
		{
			$type = sanitize_title($_REQUEST['type']);					
			if ( $type != 'all' )
				$query['type'] = $type;		
		}
		else
			$query['type'] = 'city';
		
		$locations = usam_get_locations( $query );			
		if ( !empty($locations) )
		{
			foreach ( $locations as $value ) 
			{
				$str = $value->name;					
				$str .= $this->get_locations_str( $value->parent );	
				$str = str_replace( $search, "<span>$search</span>", $str );
				$items[] = array( 'label' => $str, 'value' => $value->id );
			}
			if ( $this->limit == count($locations) )
				$items[] = array( 'label' => __('...','usam'), 'value' => 0 );
		}
		else
		{
			$items[] = array( 'label' => sprintf( __('Местоположение не найдено. Нажмите %s, чтобы мы узнали, куда нам доставить ваш заказ','usam'), '<a class="special_link" id="add_location" href="">'.__('добавить местоположение?','usam').'</a>'), 'value' => '' );			
		}
		return $items;	
	}
	
	
	public function controller_contact( $search ) 
	{		
		require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );		
		$search = "*".$search."*";		

		$items = array();			
		$query = array( 'search' => $search, 'number' => $this->limit );	
		$contacts = usam_get_contacts( $query );			
		if ( !empty($contacts) )
		{
			foreach ( $contacts as $contact ) 
			{
				$str = $contact->firstname." ".$contact->lastname;				
				$str = str_replace( $search, "<span>$search</span>", $str );				
				$items[] = array( 'label' => $str, 'value' => $contact->id );
			}
			if ( $this->limit == count($contacts) )
				$items[] = array( 'label' => __('...','usam'), 'value' => 0 );
		}
		else
		{
			$items[] = array( 'label' => __('Контакты не найдены','usam'), 'value' => '' );	
		}
		return $items;	
	}	
	
	public function controller_company( $search ) 
	{			
		$search = "*".$search."*";	
		$items = array();			
		$query = array( 'search' => $search, 'cache_bank_account' => true, 'number' => $this->limit );	
		$companies = usam_get_companies( $query );			
		if ( !empty($companies) )
		{
			foreach ( $companies as $company ) 
			{
				$str = $company->name;				
				$str = str_replace( $search, "<span>$search</span>", $str );					
				$items[] = array( 'label' => $str, 'value' => $company->id );
			}
			if ( $this->limit == count($companies) )
				$items[] = array( 'label' => __('...','usam'), 'value' => 0 );
		}
		else
		{
			$items[] = array( 'label' => __('Компании не найдены','usam'), 'value' => '' );
		}
		return $items;	
	}
		
	public function controller_product( $search ) 
	{		
		$args = array( 'post_status' => 'publish', 's' => $search, 'update_post_term_cache' => false, 'cache_results' => false, 'update_post_meta_cache' => true, 'suppress_filters' => false, 'orderby' => 'title', 'order' => 'asc', 'post_parent' => 0, 'meta_query' => array(), 'posts_per_page' => $this->limit );
		
		if ( !empty($_REQUEST['stock']) )
			$args['meta_query'][] = array( 'key' => '_usam_stock', 'value'   => 0, 'compare' => '>','type' => 'numeric' );
		if ( !empty($_REQUEST['storage']) )
			$args['meta_query'][] = array( 'key' => '_usam_storage', 'value' => 0, 'compare' => '>','type' => 'numeric' );
		
		$products = usam_get_products( $args );			
		$items = array();	
		if ( !empty($products) )
		{
			$products_ids = array();
			foreach($products as $product) 
			{
				$products_ids[] = $product->ID;	
			}
			$args = array( 'post_parent__in' => $products_ids, 'post_status' => 'inherit' );				
			$products_variation = usam_get_products( $args );	
			
			$variations	= array();
			foreach($products_variation as $product) 
			{
				$variations[$product->post_parent][] = $product;
			}
			foreach($products as $product) 
			{					
				$sku = usam_get_product_meta( $product->ID, 'sku' );					
				if ( isset($variations[$product->ID]) )
				{									
					$items[] = array( 'type' => 'variable', 'label' => "<span class= 'dropdown-product_parent_variation' data-product_id ='{$product->ID}' data-product_sku='$sku'>{$product->post_title} - '{$sku}</span>", 'value' => 0 );							
					foreach($variations[$product->ID] as $variation) 
					{
						$sku = usam_get_product_meta( $variation->ID, 'sku' ); 
						$storage = usam_get_product_meta( $variation->ID, 'storage' );
						$items[] = array( 'type' => 'variation', 'label' => "<span class= 'dropdown-product_variation' data-product_id ='{$variation->ID}' data-product_sku='$sku'>{$variation->post_title} - $sku ({$storage} ".__('шт','usam').")</span>", 'value' => $variation->ID );
					}																		
				}
				else
				{				
					$storage = usam_get_product_meta( $product->ID, 'storage' );						
					$items[] = array( 'type' => 'simple', 'label' => "<span class= 'dropdown-product' data-product_id ='{$product->ID}' data-product_sku='$sku'>{$product->post_title} - $sku ({$storage} ".__('шт','usam').")</span>", 'value' => $product->ID );
				}
			}
			if ( $this->limit == count($products) )
				$items[] = array( 'type' => 'more', 'label' => __('...','usam'), 'value' => 0 );
		}
		else
		{
			$items[] = array( 'type' => 'none', 'label' => __('Товары не найдены','usam'), 'value' => 0 );
		}	
		return $items;
	}
		
	private function title_location_yandex( $location ) 
	{		
		$parent_text = '';
		if ( !empty($location['parent']) )
			$parent_text = ' '.$this->title_location_yandex( $location['parent'] );
		
		$text = $location['name'].$parent_text;
		return $text;
	}
	
	private function controller_location_yandex( $search ) 
	{			
		$first = mb_substr($search,0,1);//первая буква
		$last = mb_substr($search,1);//все кроме первой буквы
		$first = mb_strtoupper($first);
		$last = mb_strtolower($last);
		
		$name = $first.$last;
		
		require_once( USAM_FILE_PATH . '/includes/seo/yandex/market.class.php' );		
		$market = new USAM_Yandex_Market();
		$locations = $market->get_region( array( 'name' => $name ) );
		
		$items = array();		
		if ( !empty($locations) )
		{
			foreach($locations as $location) 
			{
				$title = $this->title_location_yandex( $location );						
				$items[] = array( 'label' => $title, 'value' => $location['id'] );		
			}
		}
		else
		{
			$items[] = array( 'label' => __('Местоположения в Яндексе не найдены','usam'), 'value' => '' );		
		}
		return $items;	
	}	

	function get_form_contact( $contact_id = null, $args = array() ) 
	{			
		add_action('wp_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);
		add_action('admin_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);
		
		$form_id = empty($args['id'])?'1':absint($args['id']);
		$name = isset($args['name'])?$args['name']:'contact';
		$onChange = empty($args['onChange'])?'[]':$args['onChange'];
		$text_default = empty($args['text'])?__("Введите имя...", "usam"):$args['text'];
				
		$display_text = $text_default;
		$sku = '';
		if ( is_numeric($contact_id) )
		{
			$contact = usam_get_contact( $contact_id );			
			if ( !empty($contact) )	
			{	
				$display_text = $contact['firstname']." ".$contact['lastname'];	
			}
		}
		$url = $this->get_url( 'contact' );				
		?>		
		<script type="text/javascript">
			jQuery(document).ready(function() 
			{	
				if ( jQuery("#search_contact_<?php echo $form_id;?>").length )
				{ 
					var block = jQuery("#search_contact_<?php echo $form_id;?>");						
					block.autocomplete( {	
						source: '<?php echo $url; ?>',					
						minLength   : 2, 
						autoFocus 	: true,						
						open : function( event, ui ) 
						{ 													
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_more').removeClass('ui-menu-item').removeClass('ui-state-focus');
						},	
						select      : function(event, ui) 
						{					
							jQuery("#select_contact_<?php echo $form_id;?>").val(ui.item.value).trigger( "change" );												
							var onChange = <?php echo $onChange; ?>;	
							onChange && onChange.constructor == Function ? onChange( ui.item.value ) : null;	
							ui.item.value = ui.item.label;					
						}	
					});	
				}
			});
		</script>	       	
		<div id="contact_box" class="ctr_search"  data-id="<?php echo $form_id;?>">
			<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $contact_id; ?>" id="select_contact_<?php echo $form_id;?>">
			<input autocomplete="off" style = "font-size: 100%;" type="text" placeholder='<?php echo $text_default; ?>' id="search_contact_<?php echo $form_id;?>" onblur="if (this.value == '') {this.value = '<?php echo $text_default; ?>';}" onfocus="if (this.value == '<?php echo $text_default; ?>') {this.value = '';}" value="<?php echo $display_text; ?>" class="txt_livesearch" /><span class="bt_search" id="pl_button<?php echo $form_id;?>"></span>
		</div>     
		<?php
	}		
	
	function get_form_company( $company_id = null, $args = array() ) 
	{			
		add_action('wp_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул		
		add_action('admin_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул	
				
		$form_id = empty($args['id'])?'1':absint($args['id']);
		$name = empty($args['name'])?'company':$args['name'];
		$onChange = empty($args['onChange'])?'[]':$args['onChange'];
		$text_default = empty($args['text'])?__("Введите название компании или ИНН...", "usam"):$args['text'];
						
		$display_text = $text_default;
		$sku = '';
		if ( is_numeric($company_id) )
		{
			$company = usam_get_company( $company_id );			
			if ( !empty($company) )	
			{	
				$display_text = $company['name'];	
			}
		}		
		$company_type = '';
		$url = $this->get_url( 'company' );				
		?>		
		<script type="text/javascript">
			jQuery(document).ready(function() 
			{		
				if ( jQuery("#search_company_<?php echo $form_id;?>").length )
				{
					var block = jQuery("#search_company_<?php echo $form_id;?>");								
					block.autocomplete({	
						source: '<?php echo $url; ?>',					
						minLength   : 2, 
						autoFocus 	: true,					
						open : function( event, ui ) 
						{ 																
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_more').removeClass('ui-menu-item').removeClass('ui-state-focus');
						},	
						select      : function(event, ui) 
						{										
							jQuery("#select_company_<?php echo $form_id;?>").val(ui.item.value).trigger( "change" );	
							var onChange = <?php echo $onChange; ?>;	
							onChange && onChange.constructor == Function ? onChange( ui.item.value ) : null;	
							ui.item.value = ui.item.label;							
						}	
					});					
				}
			});
		</script>			
		<div id="company_box" class="ctr_search"  data-id="<?php echo $form_id;?>">
			<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $company_id; ?>" id="select_company_<?php echo $form_id;?>">
			<input autocomplete="off" style = "font-size: 100%;" type="text" placeholder='<?php echo $text_default; ?>' id="search_company_<?php echo $form_id;?>" onblur="if (this.value == '') {this.value = '<?php echo $text_default; ?>';}" onfocus="if (this.value == '<?php echo $text_default; ?>') {this.value = '';}" value="<?php echo $display_text; ?>" class="txt_livesearch" /><span class="bt_search" id="pl_button<?php echo $form_id;?>"></span>
		</div>     
		<?php
	}		
	
	function get_form_product( $product_id = null, $args = array() ) 
	{			
		add_action('wp_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул		
		add_action('admin_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул	
				
		$form_id = empty($args['id'])?'1':absint($args['id']);
		$name = empty($args['name'])?'product':$args['name'];
		$onChange = empty($args['onChange'])?'[]':$args['onChange'];
		$text_default = empty($args['text'])?__("Введите название и или артикул...", "usam"):$args['text'];
				
		$display_text = $text_default;
		$sku = '';
		if ( is_numeric($product_id) )
		{
			$post = get_post( $product_id );			
			if ( !empty($post) )	
			{				
				$display_text = $post->post_title;	
				$sku = usam_get_product_meta( $post->ID, 'sku' );
			}
		}		
		$url = $this->get_url( 'product' );		
		
		if ( !empty($args['query']['stock']) )
			$url = add_query_arg( array( 'stock' => 1 ), $url );
		
		if ( !empty($args['query']['storage']) )
			$url = add_query_arg( array( 'storage' => 1 ), $url );	
		?>		
		<script type="text/javascript">
			jQuery(document).ready(function() 
			{		
				if ( jQuery("#search_product_<?php echo $form_id;?>").length )
				{ 
					jQuery("#search_product_<?php echo $form_id;?>").autocomplete({	
						source: '<?php echo $url; ?>',					
						minLength   : 2, 
						autoFocus 	: true,	
						open : function( event, ui ) 
						{ 									
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_variable').removeClass('ui-menu-item').removeClass('ui-state-focus');
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_more').removeClass('ui-menu-item').removeClass('ui-state-focus');
						},															
						select : function(event, ui) 
						{ 
							jQuery("#select_product_<?php echo $form_id;?>").val(ui.item.value).trigger( "change" );	
							var onChange = <?php echo $onChange; ?>;	
							onChange && onChange.constructor == Function ? onChange( ui.item.value ) : null;	
													
							ui.item.value = jQuery(ui.item.label).text();							
						},								
					}).data("ui-autocomplete")._renderItem = function (ul, item) {						
						return jQuery("<li></li>").addClass('product_'+item.type).data("item.autocomplete", item).append(item.label).appendTo(ul);
					};
				}				
			});
		</script>	       	
		<div id="product_box" class="ctr_search"  data-id="<?php echo $form_id;?>">
			<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $product_id; ?>" data-product_sku="<?php echo $sku; ?>" id="select_product_<?php echo $form_id;?>">
			<input autocomplete="off" style = "font-size: 100%;" type="text" placeholder='<?php echo $text_default; ?>' id="search_product_<?php echo $form_id;?>" onblur="if (this.value == '') {this.value = '<?php echo $text_default; ?>';}" onfocus="if (this.value == '<?php echo $text_default; ?>') {this.value = '';}" value="<?php echo $display_text; ?>" class="txt_livesearch" /><span class="bt_search" id="pl_button<?php echo $form_id;?>"></span>
		</div>     
		<?php
	}	
		
	// Определение позиции
	function get_form_position_location( $location_id = null, $args = array() ) 
	{			
		add_action('wp_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул		
		add_action('admin_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул	
						
		$form_id = empty($args['id'])?'1':absint($args['id']);
		$name = empty($args['name'])?'location':$args['name'];
		$onChange = empty($args['onChange'])?'[]':$args['onChange'];
		$text_default = empty($args['text'])?__("Введите название и выберете из списка...", "usam"):$args['text'];
	
		$display_text = $text_default;	
		if ( is_numeric($location_id) )
		{
			$display_text = usam_get_full_locations_name( $location_id );			
			if ( $display_text == '' )			
				$display_text = $text_default;	
		}			
		$url = $this->get_url( 'position_location' );
		if ( !empty($args['type']) )
			$url = add_query_arg( array( 'type' => $args['type'] ), $url );	
		?>		
		<script type="text/javascript">
			jQuery(document).ready(function() 
			{		
				if ( jQuery("#search_location_<?php echo $form_id;?>").length )
				{					
					var block = jQuery("#search_location_<?php echo $form_id;?>");						
					block.autocomplete({	
						source: '<?php echo $url; ?>',					
						minLength   : 2, 
						autoFocus 	: false,					
						open : function( event, ui ) 
						{ 																
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_more').removeClass('ui-menu-item').removeClass('ui-state-focus');
						},		
						select      : function(event, ui) 
						{										
							jQuery("#select_location_<?php echo $form_id;?>").val(ui.item.value).trigger( "change" );	
							var onChange = <?php echo $onChange; ?>;	
							onChange && onChange.constructor == Function ? onChange( ui.item.value ) : null;	
							ui.item.value = ui.item.label;							
						}	
					}).data("ui-autocomplete")._renderItem = function (ul, item) {
						return jQuery("<li></li>").data("item.autocomplete", item).append(item.label).appendTo(ul);
					};
				}
			});
		</script>	       	
		<div id="location_box" class="ctr_search"  data-id="<?php echo $form_id;?>">
			<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $location_id; ?>" id="select_location_<?php echo $form_id;?>">
			<input autocomplete="off" type="text" placeholder='<?php echo $text_default; ?>' id="search_location_<?php echo $form_id;?>" onblur="if (this.value == '') {this.value = '<?php echo $text_default; ?>';}" onfocus="if (this.value == '<?php echo $text_default; ?>') {this.value = '';}" value="<?php echo $display_text; ?>" class="txt_livesearch" /><span class="bt_search" id="pl_button<?php echo $form_id;?>"></span>
		</div>     
		<?php
	}		
	
	
	function get_form_location_yandex( $location_id = null, $args = array() ) 
	{			
		add_action('wp_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул		
		add_action('admin_footer',array('USAM_Autocomplete_Forms','add_frontend_script_style'), 100);//Добавить Ajax Search сценарий и стиль в нижний колонтитул	
						
		$form_id = empty($args['id'])?'1':absint($args['id']);
		$name = empty($args['name'])?'yandex_location':$args['name'];
		$onChange = empty($args['onChange'])?'[]':$args['onChange'];
		$text_default = empty($args['text'])?__("Введите название и выберете из списка...", "usam"):$args['text'];
				
		$display_text = $text_default;	
		if ( is_numeric($location_id) )
		{
			$display_text = usam_get_full_locations_name( $location_id );			
			if ( $display_text == '' )			
				$display_text = $text_default;	
		}			
		$url = $this->get_url( 'location_yandex' );			
		?>		
		<script type="text/javascript">
			jQuery(document).ready(function() 
			{		
				if ( jQuery("#search_location_<?php echo $form_id;?>").length )
				{					
					var block = jQuery("#search_location_<?php echo $form_id;?>");						
					block.autocomplete({	
						source: '<?php echo $url; ?>',					
						minLength   : 2, 
						autoFocus 	: true,					
						open : function( event, ui ) 
						{ 																
							jQuery('#ui-id-<?php echo $form_id;?>').find('li.product_more').removeClass('ui-menu-item').removeClass('ui-state-focus');
						},	
						select      : function(event, ui) 
						{										
							jQuery("#select_location_<?php echo $form_id;?>").val(ui.item.value).trigger( "change" );	
							var onChange = <?php echo $onChange; ?>;	
							onChange && onChange.constructor == Function ? onChange( ui.item.value ) : null;	
							ui.item.value = ui.item.label;								
						}	
					});
				}
			});
		</script>	       	
		<div id="location_box" class="ctr_search"  data-id="<?php echo $form_id;?>">
			<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $location_id; ?>" id="select_location_<?php echo $form_id;?>">
			<input autocomplete="off" style = "font-size: 100%;" type="text" placeholder='<?php echo $text_default; ?>' id="search_location_<?php echo $form_id;?>" onblur="if (this.value == '') {this.value = '<?php echo $text_default; ?>';}" onfocus="if (this.value == '<?php echo $text_default; ?>') {this.value = '';}" value="<?php echo $display_text; ?>" class="txt_livesearch" /><span class="bt_search" id="pl_button<?php echo $form_id;?>"></span>
		</div>     
		<?php
	}	
}



function usam_display_autocomplete_form( $method, $args = array() )
{
	$ya = new USAM_Autocomplete_Forms();
	$method = "get_form_".$method;	
	if ( method_exists($ya, $method) )
	{
		return $ya->$method( $params, $args );
	}
	return false;
}
?>