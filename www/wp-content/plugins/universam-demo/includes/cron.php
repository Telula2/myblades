<?php
new USAM_cron();
class USAM_cron
{	
	public  $errors  = array();	
	function __construct()
	{	
		add_filter('cron_schedules', array( &$this, 'crony_schedules'), 10, 2 );
		add_action( 'wp', array( &$this, 'start_cron') );	
		
		add_action( 'usam_five_minutes_cron_task', array( &$this, 'working_mail'), 99 ); 		
		
		add_action( 'usam_cron_task_day', array( &$this, 'reset_time_elapsed') );           // Изменение статуса бонусов, когда прошло время возврата
		add_action( 'usam_product_day', array( &$this, 'refill_the_queue_product_day') ); // подбирает товар дня	
		add_action( 'usam_product_day', array( &$this, 'change_product_day') );		
		//add_action( 'usam_cron_task_day', array( $this, 'product_views_save_day'),20 ); // Сохраняет просмотры товаров за день		
	
		
		add_action( 'usam_cron_task_day', array( &$this, 'start_working'), 1 );
		add_action( 'usam_cron_task_day', array( &$this, 'end_working') , 99 );
		
		add_action( 'usam_cron_task_day', array( &$this, 'calculate_increase_sales_product'), 1 );	
		add_action( 'usam_cron_task_day', array( &$this, 'api') );
		
		add_action( 'usam_twicehourly_cron_task', array( &$this, 'process_product_discount') ); 
		add_action( 'init', array( &$this, 'process_product_discount') ); 
		add_action( 'usam_theme_installer', array( &$this, 'theme_installer' ), 10, 1 );	
		
		add_action( 'usam_cron_task_day', array( &$this, 'MySQL_backup') );	
    }
	
	private function set_error( $error )
	{			
		$this->errors[]  =  sprintf( __('Cron. Ошибка %s'), $error );
	}
	
	private function set_log_file( )
	{	
		usam_log_file( $this->errors );
		$this->errors = array();
	}
	
	function start_working( ) 
	{ 
		$this->set_error( 'Начало работы. Время работы: '.microtime( true ) );
		$this->set_log_file( );
	}
	
	function end_working( ) 
	{
		$this->set_error( 'Конец работы. Время работы: '.microtime( true ) );
		$this->set_log_file( );
	}
		

	// Интервалы времени для cron
	function crony_schedules( $schedules ) 
	{
	//	$schedules['everymin']          = array( 'interval' => 60,  'display' => __('Каждую минуту'), 'usam');
		$schedules['five_minutes']      = array( 'interval' => 300, 'display' => __('Каждые пять минут', 'usam') );  
		$schedules['ten_minutes']       = array( 'interval' => 600, 'display' => __('Каждые десять минут', 'usam') );  
		$schedules['twicehourly']       = array( 'interval' => 1800, 'display' => __('Каждые полчаса', 'usam') );			
		$schedules['every_two_hours']   = array( 'interval' => 7200, 'display' => __('Каждые два часа', 'usam') );	
		return apply_filters('crony_schedules',$schedules);
	}
		

	/*
		Описание: Выполнение заданий...
	*/
	function start_cron()
	{			
		$ve = get_option( 'gmt_offset' ) > 0 ? '+' : '-';	
		wp_schedule_event( strtotime( '00:00 tomorrow ' . $ve . get_option( 'gmt_offset' ) . ' HOURS' ), 'daily', "usam_product_day" );
		wp_schedule_event( strtotime( '08:00 tomorrow ' . $ve . get_option( 'gmt_offset' ) . ' HOURS' ), 'daily', "usam_vk_publish" );
			
			
		if ( !wp_next_scheduled("usam_cron_task_day") ) wp_schedule_event( mktime(1, 0, 0, date("n"), date("j"), date('Y')), 'daily', "usam_cron_task_day" );
		
		foreach ( wp_get_schedules() as $cron => $schedule ) 
		{				
			if ( !wp_next_scheduled("usam_{$cron}_cron_task") ) 		
				wp_schedule_event( mktime(date("H"), 0, 1, date("n"), date("j"), date('Y')), $cron, "usam_{$cron}_cron_task" );	
		}		
		if ( !wp_next_scheduled("usam_tracker_send_event") ) 	
			wp_schedule_event( time(), 'daily', 'usam_tracker_send_event' );		
	}	
	
	// Планирование скидок
	public function process_product_discount( )
	{			
		$option = get_option('usam_product_discount_rules', '');
		$rules = maybe_unserialize( $option );	
		if ( !empty($rules) )
		{		
			$current_time = time();					
			
			$discount_ids = array();
			foreach ( $rules as $key => $rule )			
			{					
				if ( empty($rule['process']) && $rule['active'] && ( empty($rule['start_date']) || strtotime($rule['start_date']) <= $current_time ) )
				{						
					$rules[$key]['process'] = 1;										
					$discount_ids[] = $rule['id'];	
				}
				elseif ( $rule['process'] == 1 && ( $rule['active'] == 0 || ( !empty($rule['end_date']) && strtotime($rule['end_date']) <= $current_time ) ) )
				{					
					$rules[$key]['process'] = 2;						
					$discount_ids[] = $rule['id'];	
				}				
			}				
			if ( !empty($discount_ids) )
			{		
				$product_ids = usam_get_product_discount_ids( $discount_ids );	
				
				$args = array('post__in' => $product_ids);
				usam_recalculate_price_products( $args );	
				
				update_option('usam_product_discount_rules', serialize($rules) );				
			}
		}
	}	
	
	public function MySQL_backup( )
	{
		global $wpdb;
		$dir = USAM_UPLOAD_DIR."db_backup";
		if( !is_dir($dir) )
		{
			mkdir($dir);
			if ( !mkdir($dir, 0777, true) ) 
			{
				return false;
			}
		}	
		$file = $dir.'/'.DB_NAME.'.sql';
		if ( file_exists($file) )
			unlink($file);
		
		require_once( USAM_FILE_PATH . '/includes/technical/mysql_backup.class.php' );
		$sql_dump = new USAM_MySQL_Backup( array( 'dumpfile' => $file ) );
		foreach ( $sql_dump->tables_to_dump as $key => $table ) 
		{
			if ( $wpdb->prefix != substr( $table,0 , strlen( $wpdb->prefix ) ) )
				unset( $sql_dump->tables_to_dump[ $key ] );
		}
		$sql_dump->execute();
	}
	
	public function calculate_increase_sales_product( )
	{
		usam_process_calculate_increase_sales_product();
	}
	
	 // Заполняет очередь товар дня
	public function refill_the_queue_product_day( )
	{
		$pday = new USAM_Work_Product_Day();
		$pday->refill_the_queue_product_day();
	}
	
	/*	Описание: автоматическое изменения Товара дня
	*/
	public function change_product_day() 
	{				
		$pday = new USAM_Work_Product_Day();
		$pday->change_product_day();
	}
	
	public function api() 
	{				
		$api = new Universam_API();
		$result = $api->check_license( );
	}
	
		// Работает по Cron. Меняет статус на доступный
	function reset_time_elapsed( )
	{		
		global $wpdb;	
		$reset_time_elapsed = true;
		if ( $reset_time_elapsed )
		{
			$reset_time = 7;
			$time = date( "Y-m-d H:i:s", mktime( 0, 0, 0, date( 'm' ), date( 'd' ) - $reset_time, date( 'Y' )));
			$where = "AND `use_date`<'$time'";
		}
		else
			$where = "";
		
		$wpdb->query( "UPDATE `" .USAM_TABLE_CUSTOMER_BONUSES. "` SET `status` = '3' WHERE `type` IN ('4','5') AND `status` = '2' AND `use_date` IS NOT NULL $where");			
	}	
		
	// Сохраняет просмотры товаров за день
	function product_views_save_day()
	{
		global $wpdb;
		$sql = "SELECT meta_value, post_id FROM `$wpdb->postmeta` WHERE meta_key = '_usam_product_views' AND meta_value != 0";
		$product_views = $wpdb->get_results( $sql, ARRAY_A );	
		$date = date( 'Y-m-d' );	
		if (!empty($product_views) )	
			foreach ( $product_views as $value ) 
			{
				$data  = array( 'id_product' => $value['post_id'] , 'views' => $value['meta_value'] , 'date' => $date );
				$wpdb->insert( USAM_TABLE_PRODUCT_VIEWS, $data );	
			}
	}
	
	function working_mail()
	{
		$mailbox_ids = usam_get_mailboxes( array( 'fields' => 'id' ) );	
		foreach ( $mailbox_ids as $mailbox_id ) 
		{
			usam_download_email_pop3_server( $mailbox_id );
			usam_send_mails( $mailbox_id );				
		}
	}
	
	public static function wordpress_theme_installer( $theme_slug ) 
	{
		wp_clear_scheduled_hook( 'usam_wordpress_theme_installer', func_get_args() );

		if ( ! empty( $theme_slug ) ) 
		{			
			ob_start();
			try 
			{
				$theme = wp_get_theme( $theme_slug );

				if ( ! $theme->exists() ) 
				{
					require_once( ABSPATH . 'wp-admin/includes/file.php' );
					include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
					include_once( ABSPATH . 'wp-admin/includes/theme.php' );

					WP_Filesystem();

					$skin     = new Automatic_Upgrader_Skin;
					$upgrader = new Theme_Upgrader( $skin );
					$api      = themes_api( 'theme_information', array(
						'slug'   => $theme_slug,
						'fields' => array( 'sections' => false ),
					) );
					$result   = $upgrader->install( $api->download_link );

					if ( is_wp_error( $result ) ) {
						throw new Exception( $result->get_error_message() );
					} elseif ( is_wp_error( $skin->result ) ) {
						throw new Exception( $skin->result->get_error_message() );
					} elseif ( is_null( $result ) ) {
						throw new Exception( 'Unable to connect to the filesystem. Please confirm your credentials.' );
					}
				}
				switch_theme( $theme_slug );
			} 
			catch ( Exception $e ) 
			{
				new WP_Error( 'usam_wordpress_theme_installer', __( "Не загрузить тему для Универсам.", 'usam' ) );	
			}			
			ob_end_clean();
		}
	}	

	public static function theme_installer( $theme_slug ) 
	{
		wp_clear_scheduled_hook( 'usam_theme_installer', func_get_args() );				
		usam_upload_theme( $theme_slug );
	}
}



new USAM_Clear();
class USAM_Clear
{				
	function __construct()
	{
      	add_action( 'usam_cron_task_day', array($this, 'clear_list') );		
		add_action( 'usam_hourly_cron_task', array($this, 'clear'), 1 ); 	
    }
	
	function clear()
	{					
		$this->time_keeping_baskets(); 
		$this->product_reserve_clear_period();		
	}
	
	/**
	 * Очищает корзины
	 */
	function time_keeping_baskets()
	{
		global $wpdb;

		$time = (float) get_option( 'usam_time_keeping_baskets', 1 );			
		$seconds = current_time('timestamp')-$time*86400;
		$ids = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM ".USAM_TABLE_USERS_BASKET." WHERE UNIX_TIMESTAMP(date_modified)<%d", $seconds ) );	
		
		if ( empty($ids) )
			return false;			

		usam_delete_cart( $ids );
	}	
	
	/**
	 * Очищает утвержденные на складе
	 */
	function product_reserve_clear_period()
	{
		$time = get_option( 'usam_product_reserve_clear_period', 1 );			
		
		if ( $time == 0 )
			return false;
		
		$time = (float)$time;		
		$seconds = current_time('timestamp')-$time*86400;
		
		$args = array( 'fields' => 'all', 
			'date_query' => array(
					array(
						'before'  => date( 'r', $seconds),				
					),				
				), 
			'meta_query' => array(
				array(
					'key'     => 'reserve',
					'value'   => '0',
					'compare' => '>'
				),			
			),
			'cache_documents' => true,
		);
		$shippeds_document = usam_get_shipping_documents( $args );	
		foreach ( $shippeds_document as $document ) 
		{			
			$shipped = new USAM_Shipped_Document( $document->id );
			$shipped->remove_all_product_from_reserve();
		}				
	}
	
	public function clearing_message_pop3_server()
	{		
		$mailboxes = usam_get_mailboxes( );			
		foreach ( $mailboxes as $mailbox ) 
		{			
			if ( $mailbox->delete_server && $mailbox->delete_server_day )
			{
				$pop3 = new USAM_POP3( $mailbox->id );
				$result = $pop3->delete_messages( );			
			}
		}	
	}
	
	public function clear_customer_products( $user_list, $day )
	{		
		global $wpdb;
		$date_insert = date( "Y-m-d H:i:s", mktime( 0, 0, 0, date( 'm' ), date( 'd' ) - $day, date( 'Y' )));
		
		$wpdb->query( "DELETE FROM ".USAM_TABLE_USER_PRODUCTS." WHERE user_list ='$user_list' AND date_insert<'$date_insert'" );	
	}
	
	
	public function clear_list()
	{		
		$this->clearing_message_pop3_server();		
		$this->clear_customer_products( 'compare', 3 );
		$this->clear_customer_products( 'desired', 360 );
		$this->clear_customer_products( 'view', 360 );		
	}		
}