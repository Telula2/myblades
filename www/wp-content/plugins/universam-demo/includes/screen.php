<?php
/* Экранные функции
 */

// Установить сообщение
function usam_set_user_screen_message( $error, $current_screen, $type = 'message' )
{		
	if ( empty($error) )
		return;
	if ( !isset($_SESSION[$type]) )
		$_SESSION[$type] = array();	
	
	if ( !isset($_SESSION[$type][$current_screen]) )
		$_SESSION[$type][$current_screen] = array();
	if ( is_array($error))
		$_SESSION[$type][$current_screen] = array_merge($_SESSION[$type][$current_screen], $error);	
	else
		$_SESSION[$type][$current_screen][] = $error;		
}

// Получить сообщение
function usam_get_user_screen_message( $current_screen, $type = 'message' )
{	
	$return = false;
	if ( !empty($_SESSION[$type][$current_screen]) )
	{		
		$return = $_SESSION[$type][$current_screen];
		unset($_SESSION[$type][$current_screen]);
	}	
	return $return;
}

// Установить ошибку
function usam_set_user_screen_error( $error, $screen_id = null )
{		
	if ( $screen_id == null )
	{
		global $current_screen;
		if ( !empty($current_screen->id) )
			$screen_id = $current_screen->id;
		else
			$screen_id = 'user_screen';
	}		
	return usam_set_user_screen_message( $error, $screen_id, 'error' );
}

// Получить ошибку
function usam_get_user_screen_error( $screen_id = null )
{
	if ( $screen_id == null )
	{
		global $current_screen;
		if ( !empty($current_screen->id) )
			$screen_id = $current_screen->id;
		else
			$screen_id = 'user_screen';
	}
	return usam_get_user_screen_message( $screen_id, 'error' );
}

?>