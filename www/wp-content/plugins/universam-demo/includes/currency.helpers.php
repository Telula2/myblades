<?php
/*
 *	Форматирование цены, добавление нулей, валюты
 */
function usam_currency_display( $price_in, $args = null )
{	
	$args = apply_filters( 'usam_toggle_display_currency_code', $args );
	$query = shortcode_atts( array(
		'display_currency_symbol' => true,
		'decimal_point'   		  => false,
		'display_currency_code'   => false,
		'display_as_html'         => false,
		'code'                    => false,
		'currency'                => usam_get_currency_price_by_code(),
	), $args );

	if ( false == $query['decimal_point'] )
		$decimals = 0;
	else
		$decimals = 2; // default is 2

	$decimals = apply_filters('usam_modify_decimals' , $decimals);
	$decimal_separator = get_option( 'usam_decimal_separator', ',' );
	$thousands_separator = get_option( 'usam_thousands_separator', '.' );	

	$price_out = number_format( (double)$price_in, $decimals, $decimal_separator, $thousands_separator );

	$currency_data = usam_get_currency( $query['currency'] );

	if ( empty($currency_data) )
		return $price_out;
	
	// Выясните, код валюты
	if ( $query['display_currency_code'] )
		$currency_code = $currency_data['code'];
	else
		$currency_code = '';
	// Выясните, знак валюты
	$currency_sign = '';	
	if ( $query['display_currency_symbol'] ) 
	{
		if ( !empty( $currency_data['symbol'] ) ) 
		{
			if ( $query['display_as_html'] && !empty($currency_data['symbol_html']) )
				$currency_sign = $currency_data['symbol_html'];
			else
				$currency_sign = $currency_data['symbol'];
		} 
		else 
		{
			$currency_sign = $currency_data['code'];
			$currency_code = '';
		}
	}
	$currency_sign_location = get_option( 'usam_currency_sign_location', 2 );
	// расположение знака валюты
	switch ( $currency_sign_location ) 
	{
		case 1:
			$format_string = '%3$s%1$s%2$s';
			break;
		case 2:
			$format_string = '%3$s %1$s%2$s';
			break;
		case 4:
			$format_string = '%1$s%2$s  %3$s';
			break;
		case 3:
		default:
			$format_string = '%1$s %2$s%3$s';
			break;
	}
	// Compile the output
	$output = trim( sprintf( $format_string, $currency_code, $currency_sign, $price_out ) );
	if ( !$query['display_as_html'] )
		$output = "".$output."";
	else
		$output = "<span class='pricedisplay'>".$output."</span>";
	return apply_filters( 'usam_currency_display', $output );
}

function usam_string_to_float( $string ) 
{	
	//$decimal_separator = get_option( 'usam_decimal_separator' );		
	$string = preg_replace( '/[^0-9\\.,]/', '', $string );
	//$string = str_replace( $decimal_separator, '.', $string );
	if( stristr($string, '.') !== false && stristr($string, ',') !== false ) 
	{ // Если найдено и . и ,
		$string = str_replace( ',', '', $string );	
	}		
	else
		$string = str_replace( ',', '.', $string );	
	return (float)$string;
}

function usam_format_price( $amt, $currency_code = false )
{
	$currencies_without_fractions = array( 'JPY', 'HUF' );
	if ( ! $currency_code )
	{
		$currency = new USAM_Currency( );
		$currency_code = $currency->get( 'code' );
	}
	$dec = in_array( $currency_code, $currencies_without_fractions ) ? 0 : 2;
	return number_format( $amt, $dec );
}

function usam_format_convert_price( $amt, $from_currency = false, $to_currency = false ) 
{
	return usam_format_price( usam_convert_currency( $amt, $from_currency, $to_currency ), $to_currency );	
}

// Округление цены в зависимости от настроек
function usam_round_price( $price, $price_code = '' )
{
   if ( empty($price_code) )
	   global $price_code;
   
   $type_prices = usam_get_setting_price_by_code( $price_code );   
   return round($price, $type_prices['rounding']);	
}

function usam_convert_currency( $amt, $from, $to ) 
{
	if ( empty( $from ) || empty( $to ) || $from == $to )
		return $amt;
	
	$rate = usam_get_currency_rate( $from, $to );
	if ( empty( $rate ) )
		return $amt;

	return $rate * $amt;
}

function usam_get_currency_rate( $basic_currency, $currency ) 
{
	global $wpdb;
		
	$result = $wpdb->get_var( $wpdb->prepare( "SELECT DISTINCT rate FROM ".USAM_TABLE_CURRENCY_RATES." WHERE basic_currency ='%s' AND currency ='%s'",$basic_currency, $currency ) );			
	return $result;	
}


/**
* PHP-класс для получения курсов валют с сайта ЦБ РФ:
* @version 2.0
*/
class USAM_ExchangeRatesCBRF
{	
	public $rates = array('byChCode' => array(), 'byCode' => array());
	
	/**
	* This method creates a connection to webservice of Central Bank of Russia 
	* and obtains exchange rates, parse it and fills $rates property
	* @param string $date The date on which exchange rates will be obtained
	*/ 
	public function __construct($date = null)
	{
		if (!isset($date)) 
			$date = date("Y-m-d");
		
		if ( !class_exists('SoapClient') )
			return false;
		
		$client = new SoapClient("http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL");
		$curs = $client->GetCursOnDate(array("On_date" => $date));
		$rates = new SimpleXMLElement($curs->GetCursOnDateResult->any);
		foreach ($rates->ValuteData->ValuteCursOnDate as $rate)
		{
			$r = (float)$rate->Vcurs / (int)$rate->Vnom;
			$this->rates['byChCode'][(string)$rate->VchCode] = $r;	
			$this->rates['byCode'][(int)$rate->Vcode] = $r;	
		}		
		// Adding an exchange rate of Russian Ruble 
		$this->rates['byChCode']['RUB'] = 1;	
		$this->rates['byCode'][643] = 1;		
	}

	/**
	* This method returns exchange rate of given currency by its code
	* @param mixed $code The alphabetic or numeric currency code
	* @return float The exchange rate of given currency
	*/
	public function GetRate($code)
	{
		if (is_string($code))
		{
			$code = strtoupper(trim($code));
			return (isset($this->rates['byChCode'][$code])) ? $this->rates['byChCode'][$code] : false;
		
		}
		elseif (is_numeric($code))
		{
			return (isset($this->rates['byCode'][$code])) ? $this->rates['byCode'][$code] : false;
		}
		else 
		{
			return false;		
		}
	}
	
	public function GetCrossRate($CurCodeToSell, $CurCodeToBuy)
	{
		$CurToSellRate = $this->GetRate($CurCodeToSell);
		$CurToBuyRate = $this->GetRate($CurCodeToBuy);
		
		if ($CurToSellRate && $CurToBuyRate)
		{
			return $CurToBuyRate / $CurToSellRate;
		}
		else
		{
			return false;
		}		
	}	
	/**
	* This method returns the array of exchange rates
	* @return array The exchange rates
	*/
	public function GetRates()
	{
		return $this->rates;
	}	
}


/*
	конвертация валюты.
*/
Class USAM_CURRENCY_CONVERTER
{
	var $_amt=1;
	var $_to="";
	var $_from="";
	var $_error="";
	
	public function __construct( $amt=1, $to="", $from="" )
	{
		$this->_amt=$amt;
		$this->_to=$to;
		$this->_from=$from;
	}
	function error()
	{
		return $this->_error;
	}

	/**
	 * Конвертер валют
	 */
	function convert($amt = NULL, $to = "", $from = "")
	{
		$amount = urlencode(round($amt,2));
		$from_Currency = urlencode($from);
		$to_Currency = urlencode($to);

		$url = "http://www.google.com/finance/converter?hl=en&a={$amount}&from={$from_Currency}&to={$to_Currency}";

		$ch = curl_init();
		$timeout = 20;
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$rawdata = curl_exec($ch);
		curl_close($ch);
		if(empty($rawdata))
		{
			throw new Exception( __( 'не удалось подключиться к службе конвертации валюты', 'usam' ) );
		}		
		preg_match( '/bld[^"]+"([\d\s.,]+)/', $rawdata, $matches );
		if ( isset( $matches[1] ) )
			$to_amount = (float) str_replace( array( ',', ' ' ), '', $matches[1] );
		else 
		{
			$rawdata = preg_replace( '/(\{|,\s*)([^\s:]+)(\s*:)/', '$1"$2"$3', $rawdata );
			$to_amount = json_decode( $rawdata );
		}
		$to_amount = round( $to_amount, 2 );
		return $to_amount;
	}
}


//Кодирование сервером строки JSON при помощи стандартной функции в php "json_encode()". В результате русские буковки остаются русскими.
function usam_json_encode_cyr($str) 
{
	$arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
	'\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
	'\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
	'\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
	'\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
	'\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
	'\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
	'\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
	$arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
	'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
	'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
	'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');
	$str1 = json_encode($str);
	$str2 = str_replace($arr_replace_utf,$arr_replace_cyr,$str1);
	return $str2;
}

function usam_get_number_word($source, $IS_MONEY = "Y", $currency = "")
{
	$result = '';

	$IS_MONEY = ((string)($IS_MONEY) == 'Y' ? 'Y' : 'N');
	$currency = (string)$currency;
	if ($currency == '' || $currency == 'RUR')
		$currency = 'RUB';
	if ($IS_MONEY == 'Y')
	{
		if ($currency != 'RUB' && $currency != 'UAH')
			return $result;
	}

	$arNumericLang = array(
		"RUB" => array(
			"zero" => "ноль",
			"1c" => "сто ",
			"2c" => "двести ",
			"3c" => "триста ",
			"4c" => "четыреста ",
			"5c" => "пятьсот ",
			"6c" => "шестьсот ",
			"7c" => "семьсот ",
			"8c" => "восемьсот ",
			"9c" => "девятьсот ",
			"1d0e" => "десять ",
			"1d1e" => "одиннадцать ",
			"1d2e" => "двенадцать ",
			"1d3e" => "тринадцать ",
			"1d4e" => "четырнадцать ",
			"1d5e" => "пятнадцать ",
			"1d6e" => "шестнадцать ",
			"1d7e" => "семнадцать ",
			"1d8e" => "восемнадцать ",
			"1d9e" => "девятнадцать ",
			"2d" => "двадцать ",
			"3d" => "тридцать ",
			"4d" => "сорок ",
			"5d" => "пятьдесят ",
			"6d" => "шестьдесят ",
			"7d" => "семьдесят ",
			"8d" => "восемьдесят ",
			"9d" => "девяносто ",
			"5e" => "пять ",
			"6e" => "шесть ",
			"7e" => "семь ",
			"8e" => "восемь ",
			"9e" => "девять ",
			"1et" => "одна тысяча ",
			"2et" => "две тысячи ",
			"3et" => "три тысячи ",
			"4et" => "четыре тысячи ",
			"1em" => "один миллион ",
			"2em" => "два миллиона ",
			"3em" => "три миллиона ",
			"4em" => "четыре миллиона ",
			"1eb" => "один миллиард ",
			"2eb" => "два миллиарда ",
			"3eb" => "три миллиарда ",
			"4eb" => "четыре миллиарда ",
			"1e." => "один рубль ",
			"2e." => "два рубля ",
			"3e." => "три рубля ",
			"4e." => "четыре рубля ",
			"1e" => "один ",
			"2e" => "два ",
			"3e" => "три ",
			"4e" => "четыре ",
			"11k" => "11 копеек",
			"12k" => "12 копеек",
			"13k" => "13 копеек",
			"14k" => "14 копеек",
			"1k" => "1 копейка",
			"2k" => "2 копейки",
			"3k" => "3 копейки",
			"4k" => "4 копейки",
			"." => "рублей ",
			"t" => "тысяч ",
			"m" => "миллионов ",
			"b" => "миллиардов ",
			"k" => " копеек",
		),
		"UAH" => array(
			"zero" => "нyль",
			"1c" => "сто ",
			"2c" => "двісті ",
			"3c" => "триста ",
			"4c" => "чотириста ",
			"5c" => "п'ятсот ",
			"6c" => "шістсот ",
			"7c" => "сімсот ",
			"8c" => "вісімсот ",
			"9c" => "дев'ятьсот ",
			"1d0e" => "десять ",
			"1d1e" => "одинадцять ",
			"1d2e" => "дванадцять ",
			"1d3e" => "тринадцять ",
			"1d4e" => "чотирнадцять ",
			"1d5e" => "п'ятнадцять ",
			"1d6e" => "шістнадцять ",
			"1d7e" => "сімнадцять ",
			"1d8e" => "вісімнадцять ",
			"1d9e" => "дев'ятнадцять ",
			"2d" => "двадцять ",
			"3d" => "тридцять ",
			"4d" => "сорок ",
			"5d" => "п'ятдесят ",
			"6d" => "шістдесят ",
			"7d" => "сімдесят ",
			"8d" => "вісімдесят ",
			"9d" => "дев'яносто ",
			"5e" => "п'ять ",
			"6e" => "шість ",
			"7e" => "сім ",
			"8e" => "вісім ",
			"9e" => "дев'ять ",
			"1e." => "один гривня ",
			"2e." => "два гривні ",
			"3e." => "три гривні ",
			"4e." => "чотири гривні ",
			"1e" => "один ",
			"2e" => "два ",
			"3e" => "три ",
			"4e" => "чотири ",
			"1et" => "одна тисяча ",
			"2et" => "дві тисячі ",
			"3et" => "три тисячі ",
			"4et" => "чотири тисячі ",
			"1em" => "один мільйон ",
			"2em" => "два мільйона ",
			"3em" => "три мільйона ",
			"4em" => "чотири мільйона ",
			"1eb" => "один мільярд ",
			"2eb" => "два мільярда ",
			"3eb" => "три мільярда ",
			"4eb" => "чотири мільярда ",
			"11k" => "11 копійок",
			"12k" => "12 копійок",
			"13k" => "13 копійок",
			"14k" => "14 копійок",
			"1k" => "1 копійка",
			"2k" => "2 копійки",
			"3k" => "3 копійки",
			"4k" => "4 копійки",
			"." => "гривень ",
			"t" => "тисяч ",
			"m" => "мільйонів ",
			"b" => "мільярдів ",
			"k" => " копійок",
		)
	);

	// k - penny
	if ($IS_MONEY == "Y")
	{
		$source = (string)((float)$source);
		$dotpos = strpos($source, ".");
		if ($dotpos === false)
		{
			$ipart = $source;
			$fpart = '';
		}
		else
		{
			$ipart = substr($source, 0, $dotpos);
			$fpart = substr($source, $dotpos + 1);
			if ($fpart === false)
				$fpart = '';
		}
		;
		if (strlen($fpart) > 2)
		{
			$fpart = substr($fpart, 0, 2);
			if ($fpart === false)
				$fpart = '';
		}
		$fillLen = 2 - strlen($fpart);
		if ($fillLen > 0)
			$fpart .= str_repeat('0', $fillLen);
		unset($fillLen);
	}
	else
	{
		$ipart = (string)((int)$source);
		$fpart = '';
	}

	if (is_string($ipart))
	{
		$ipart = preg_replace('/^[0]+/', '', $ipart);
	}

	$ipart1 = strrev($ipart);
	$ipart1Len = strlen($ipart1);
	$ipart = "";
	$i = 0;
	while ($i < $ipart1Len)
	{
		$ipart_tmp = substr($ipart1, $i, 1);
		// t - thousands; m - millions; b - billions;
		// e - units; d - scores; c - hundreds;
		if ($i % 3 == 0)
		{
			if ($i==0) $ipart_tmp .= "e";
			elseif ($i==3) $ipart_tmp .= "et";
			elseif ($i==6) $ipart_tmp .= "em";
			elseif ($i==9) $ipart_tmp .= "eb";
			else $ipart_tmp .= "x";
		}
		elseif ($i % 3 == 1) $ipart_tmp .= "d";
		elseif ($i % 3 == 2) $ipart_tmp .= "c";
		$ipart = $ipart_tmp.$ipart;
		$i++;
	}

	if ($IS_MONEY == "Y")
	{
		$result = $ipart.".".$fpart."k";
	}
	else
	{
		$result = $ipart;
		if ($result == '')
			$result = $arNumericLang[$currency]['zero'];
	}

	if (substr($result, 0, 1) == ".")
		$result = $arNumericLang[$currency]['zero']." ".$result;

	$result = str_replace("0c0d0et", "", $result);
	$result = str_replace("0c0d0em", "", $result);
	$result = str_replace("0c0d0eb", "", $result);

	$result = str_replace("0c", "", $result);
	$result = str_replace("1c", $arNumericLang[$currency]["1c"], $result);
	$result = str_replace("2c", $arNumericLang[$currency]["2c"], $result);
	$result = str_replace("3c", $arNumericLang[$currency]["3c"], $result);
	$result = str_replace("4c", $arNumericLang[$currency]["4c"], $result);
	$result = str_replace("5c", $arNumericLang[$currency]["5c"], $result);
	$result = str_replace("6c", $arNumericLang[$currency]["6c"], $result);
	$result = str_replace("7c", $arNumericLang[$currency]["7c"], $result);
	$result = str_replace("8c", $arNumericLang[$currency]["8c"], $result);
	$result = str_replace("9c", $arNumericLang[$currency]["9c"], $result);

	$result = str_replace("1d0e", $arNumericLang[$currency]["1d0e"], $result);
	$result = str_replace("1d1e", $arNumericLang[$currency]["1d1e"], $result);
	$result = str_replace("1d2e", $arNumericLang[$currency]["1d2e"], $result);
	$result = str_replace("1d3e", $arNumericLang[$currency]["1d3e"], $result);
	$result = str_replace("1d4e", $arNumericLang[$currency]["1d4e"], $result);
	$result = str_replace("1d5e", $arNumericLang[$currency]["1d5e"], $result);
	$result = str_replace("1d6e", $arNumericLang[$currency]["1d6e"], $result);
	$result = str_replace("1d7e", $arNumericLang[$currency]["1d7e"], $result);
	$result = str_replace("1d8e", $arNumericLang[$currency]["1d8e"], $result);
	$result = str_replace("1d9e", $arNumericLang[$currency]["1d9e"], $result);

	$result = str_replace("0d", "", $result);
	$result = str_replace("2d", $arNumericLang[$currency]["2d"], $result);
	$result = str_replace("3d", $arNumericLang[$currency]["3d"], $result);
	$result = str_replace("4d", $arNumericLang[$currency]["4d"], $result);
	$result = str_replace("5d", $arNumericLang[$currency]["5d"], $result);
	$result = str_replace("6d", $arNumericLang[$currency]["6d"], $result);
	$result = str_replace("7d", $arNumericLang[$currency]["7d"], $result);
	$result = str_replace("8d", $arNumericLang[$currency]["8d"], $result);
	$result = str_replace("9d", $arNumericLang[$currency]["9d"], $result);

	$result = str_replace("0e", "", $result);
	$result = str_replace("5e", $arNumericLang[$currency]["5e"], $result);
	$result = str_replace("6e", $arNumericLang[$currency]["6e"], $result);
	$result = str_replace("7e", $arNumericLang[$currency]["7e"], $result);
	$result = str_replace("8e", $arNumericLang[$currency]["8e"], $result);
	$result = str_replace("9e", $arNumericLang[$currency]["9e"], $result);

	$result = str_replace("1et", $arNumericLang[$currency]["1et"], $result);
	$result = str_replace("2et", $arNumericLang[$currency]["2et"], $result);
	$result = str_replace("3et", $arNumericLang[$currency]["3et"], $result);
	$result = str_replace("4et", $arNumericLang[$currency]["4et"], $result);
	$result = str_replace("1em", $arNumericLang[$currency]["1em"], $result);
	$result = str_replace("2em", $arNumericLang[$currency]["2em"], $result);
	$result = str_replace("3em", $arNumericLang[$currency]["3em"], $result);
	$result = str_replace("4em", $arNumericLang[$currency]["4em"], $result);
	$result = str_replace("1eb", $arNumericLang[$currency]["1eb"], $result);
	$result = str_replace("2eb", $arNumericLang[$currency]["2eb"], $result);
	$result = str_replace("3eb", $arNumericLang[$currency]["3eb"], $result);
	$result = str_replace("4eb", $arNumericLang[$currency]["4eb"], $result);

	if ($IS_MONEY == "Y")
	{
		$result = str_replace("1e.", $arNumericLang[$currency]["1e."], $result);
		$result = str_replace("2e.", $arNumericLang[$currency]["2e."], $result);
		$result = str_replace("3e.", $arNumericLang[$currency]["3e."], $result);
		$result = str_replace("4e.", $arNumericLang[$currency]["4e."], $result);
	}
	else
	{
		$result = str_replace("1e", $arNumericLang[$currency]["1e"], $result);
		$result = str_replace("2e", $arNumericLang[$currency]["2e"], $result);
		$result = str_replace("3e", $arNumericLang[$currency]["3e"], $result);
		$result = str_replace("4e", $arNumericLang[$currency]["4e"], $result);
	}

	if ($IS_MONEY == "Y")
	{
		$result = str_replace("11k", $arNumericLang[$currency]["11k"], $result);
		$result = str_replace("12k", $arNumericLang[$currency]["12k"], $result);
		$result = str_replace("13k", $arNumericLang[$currency]["13k"], $result);
		$result = str_replace("14k", $arNumericLang[$currency]["14k"], $result);
		$result = str_replace("1k", $arNumericLang[$currency]["1k"], $result);
		$result = str_replace("2k", $arNumericLang[$currency]["2k"], $result);
		$result = str_replace("3k", $arNumericLang[$currency]["3k"], $result);
		$result = str_replace("4k", $arNumericLang[$currency]["4k"], $result);
	}

	if ($IS_MONEY == "Y")
	{
		if (substr($result, 0, 1) == ".")
			$result = $arNumericLang[$currency]['zero']." ".$result;

		$result = str_replace(".", $arNumericLang[$currency]["."], $result);
	}

	$result = str_replace("t", $arNumericLang[$currency]["t"], $result);
	$result = str_replace("m", $arNumericLang[$currency]["m"], $result);
	$result = str_replace("b", $arNumericLang[$currency]["b"], $result);

	if ($IS_MONEY == "Y")
		$result = str_replace("k", $arNumericLang[$currency]["k"], $result);
	
	return substr($result, 0, 1).substr($result, 1);
}