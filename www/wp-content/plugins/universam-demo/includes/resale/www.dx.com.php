<?php 
class USAM_Processing_Site_Content
{
	public function content( $content )
	{
		$content = $this->translation( $content );
		$content = $this->content_clear_text( $content );
		//$content = $this->style_clear( $content );	
		return $content;		
	}
	
	// удалить скобки и то что в не скобок
	private function content_clear_text( $content )
	{
		$search_header = 'Размер и вес';
		$search_footer = '</table>';
		$search = array( 0 => array('<td class="value">', '(') );
		$buf = $content;
		$number_pos_header = strpos( $buf, $search_header ); // найти позицию
		if ( $number_pos_header !== false)
		{
			$buf = substr($buf, $number_pos_header);         			// удалить все что левее найденной позиции	
			$number_pos_footer = strpos( $buf, $search_footer ); // найти позицию
			$str_1 = $str_2 = substr($buf, 0, $number_pos_footer );			
			$str_1 = str_replace ( ')' , '' , $str_1 );			
			$content = str_replace ( $str_2 , $str_1 , $content );			
			$i = 0;			
			for ($j = 0; $j <= 1;$j++) 
			{				
				foreach ( $search as $key ) 
				{ 
					$size = strlen( $key[0] );
					$number_pos_0 = strpos( $buf, $key[0] ) + $size; // найти позицию
					$size = strlen( $key[1] );
					$number_pos_1 = strpos( $buf, $key[1] ) + $size; // найти позицию
					$str[$i] = substr($buf, $number_pos_0, $number_pos_1 - $number_pos_0 );
					$i++;
				}
				$buf = substr($buf, $number_pos_1);				
				$number_pos = strpos( $buf, 'Вес' ); // если только строка "размер", отсутствует строка "вес"
				if ( $number_pos_header !== false){	}
				else break;					
			}			
			for ($j = 0; $j <= $i; $j++) 		
				$content = str_replace ( $str[$j] , '' , $content );					
		}		
		return $content;		
	}
	
	private function style_clear( $content )
	{
		$blocks = array( "<div", "<table", "<tr" );		
		$buf = $content;	
		foreach ( $blocks as $key ) 
		{ 
			$size = strlen( $key );
			$number_pos_header = strpos( $buf, $key ) + $size;
			$str = substr($buf, $number_pos_header);
			$number_pos_footer = strpos( $str, '>' );
			$pos_end = $number_pos_header + $number_pos_footer - $size;
			$str_del = substr( $buf, $number_pos_header, $pos_end );		
			$content = str_replace ( $str_del , '' , $content );	
			$buf = substr($buf, $number_pos_footer + $size +1);	
		}
		return $content;
	}
	
	public function translation( $content )
	{
		if (!empty($content))
		{
			$dictionary = array('Марка' => 'Бренд',
								'Упаковочный лист' => 'Комплектация',
								'Hour' => 'часов',
								'hour' => 'часов',
								'Цвет BIN' => 'Цвет свечения',
								'lumens' => 'люменов',
								'</tr><tr><td class="attr">Внешний пикселя камера</td><td>Нет</td></tr>' => '',
								'Датчик изображения Размер' => 'Размер датчика изображения',
								'Пикселя камера' => 'Количество пикселей',
								'Anti-Shake' => 'Стабилизации изображения',
								'Баланс белого Режим' => 'Баланс белого',
								'Стрельба' => 'Видеосъемка',
								'Расшифруйте Формат' => 'Формат декодирования',
								'Изображения' => 'Формат изображения',
								'Тем не менее Разрешение изображения' => 'Разрешение изображения',
								'Монодии' => 'Моно',
								'Авто-Power On' => 'Авто включение питания',
								'Светодиодные Кол-во' => 'Кол-во светодиодов',
								'Loop Record' => 'Разделение записи по времени',
								'Time Stamp' => 'Метки времени',
								'МаксимумМощность' => 'Максимальная емкость',
								'Расширение памяти' => 'Поддержка карт памяти',
								'O интерфейс ввода /' => 'Интерфейсы ввода',
								'Car DVR' => 'Автомобильный видеорегистратор',
								'Chinese / Russian / English user manual' => 'Руководство пользователя на русском и английском',
								'English / Russian / Chinese user manual' => 'Руководство пользователя на русском и английском',
								'Car charger' => 'Автомобильное зарядное устройство',		
								'Совместимость марка' => 'Совместимость с брендом',	
								'Переработка время' => 'Время перезарядки',	
								'Макс Скорость синхронизации' => 'Скорость синхронизации',								
								'Батарея Количество' => 'Количество батарей',
								'Г.Н.' => 'Ведущее число',	
								'Подключите Технические характеристики' => 'Вилка питания',		
								'Plug' => 'вилка',									
								'cable' => 'кабель',
								'Видео сжатом формате' => 'Формат сжатия видео',
								'Подключите характеристики' => 'Вилка питания',								
								'Nigt Видение Расстояние' => 'Расстояние ночного видения',
								'IR-LED Количество' => 'Количество IR-LED',
								'Слот SIM-карты' => 'Слоты SIM карт',	
								'На сайте' => 'Количество посетителей на сайте',
								'Мобильный телефон Платформа' => 'Платформы мобильных телефонов',
								'Напряжение тока тарифа	' => 'Напряжение тока',				
								'Holder' => 'Держатель',
								'Номинальная емкость' => 'Емкость',	
								'Battery' => 'Аккумулятор',	
								'Charger' => 'Зарядное устройство',	
								'китайский упрощенный,' => '',
								'китайский традиционный,' => '',
								'Теоретическая люмен' => 'Световой поток',
								'Чип Бренд' => 'Производитель чипа',						
								'Общий' => 'Общие',
								'Видео Сжатый формат' => 'Формат сжатия видео',	
								'Видении, nigt расстояние' => 'Расстояние ночного видения',								
								'<tr><td class="attr">Количество</td><td>1шт</td></tr>' => '',
								'<tr><td class="attr">Количество</td><td>1Set</td></tr>' => ''
								);
			foreach ( $dictionary as $key => $value ) 	
				$content = str_replace ( $key , $value , $content );		
		}
		return $content;
	}
}
?>