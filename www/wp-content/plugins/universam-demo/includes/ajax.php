<?php
// Класс обратных запросов ajax 
class USAM_Callback 
{	
	protected $action = '';
	protected $error_code = false;
	protected $error = '';
	protected $init_parameter = '';
	protected $ajax_parameter = '';
	protected $verify_nonce = true;
	
	public function __construct() 
	{		
		add_action( 'wp_ajax_usam_ajax', array($this, 'handler_ajax') );					
	}	
	
	protected function set_log_file()
	{		
		usam_log_file( $this->error );
		$this->error = '';
	}
		
	/**
	 * Обработчик для всех запросов.	
	 */
	public function handler()
	{		
		$this->action = $_REQUEST[$this->init_parameter];		
	
		$this->sendback = wp_get_referer();
		$this->sendback = remove_query_arg( array( $this->init_parameter, 'callback_error', '_wpnonce', 'nonce' ), $this->sendback );		
		if ( $this->verify_nonce )
		{		
			$result = $this->verify_nonce( $this->action.'_nonce' );		
			if ( ! is_wp_error( $result ) )
				$result = $this->fire_callback( );	
			else
				$this->sendback = add_query_arg( 'verify_nonce', 1, $this->sendback );
		}
		else
			$result = $this->fire_callback( );
	
		$this->set_log_file();
		if ( is_wp_error( $result ) ) 
		{	// ошибки возникшие во время исполнения запросов
			$output = array(
				'code'     => $result->get_error_code(),
				'messages' => $result->get_error_messages(),
				'data'     => $result->get_error_data(),
			);
			$this->sendback = add_query_arg( 'callback_error', 1, $this->sendback );
		} 			
		wp_redirect( $this->sendback );
		exit();		
	}	
		
	/**
	 * AJAX обработчик для всех запросов Ajax. Эта функция автоматизирует проверку одноразового номера и выдает ответ JSON.
	 */
	public function handler_ajax()
	{			
		if ( empty($_REQUEST[$this->ajax_parameter]) )
			return false;
		$this->action = str_replace( '-', '_', $_REQUEST[$this->ajax_parameter] );						
		
		$result = $this->verify_nonce( 'usam_ajax_' . $this->action );		
				
		if ( ! is_wp_error( $result ) )
			$result = $this->fire_callback( );

		$this->set_log_file();
		$output = array( 'is_successful' => false, );

		if ( is_wp_error( $result ) ) 
		{	// ошибки возникшие во время исполнения запросов Ajax
			$output['error'] = array(
				'code'     => $result->get_error_code(),
				'messages' => $result->get_error_messages(),
				'data'     => $result->get_error_data(),
			);
		} 
		else 
		{
			$output['is_successful'] = true;
			$output['obj'] = $result;        // Данные возвращаемые функцией
		}
		echo json_encode( $output );
		exit;		
	}	
	
	/**
	 * Проверка запроса на безопасность
	 * @since  3.8.9
	 */
	protected function verify_nonce( $usam_action ) 
	{				
		$nonce = ''; // nonce могут быть переданы с именем usam_nonce или _wpnonce
		if ( isset( $_REQUEST['nonce'] ) )
			$nonce = $_REQUEST['nonce'];
		elseif ( isset( $_REQUEST['_wpnonce'] ) )
			$nonce = $_REQUEST['_wpnonce'];
		else
			return new WP_Error( 'usam_ajax_invalid_nonce', sprintf(__( 'Ключ для проверки безопасности для вызова %s не найден.', 'usam' ), $this->action) );		
		// проверить nonce	
		if ( ! wp_verify_nonce( $nonce, $usam_action ) )
			return new WP_Error( 'usam_ajax_invalid_nonce', __( 'Проверка не пройдена. Возможна ваша сессия истекла. Обновите страницу и попробуйте еще раз.', 'usam' ) );	
	
		return true;
	}

	/**
	 * Проверка AJAX обратного вызова и вызвать его, если он существует.
	 * @since  3.8.9
	 */
	protected function fire_callback( )
	{			
		$callback = "controller_{$this->action}";	
		if ( method_exists( $this, $callback )) 
			$result = $this->$callback();
		elseif ( is_callable( $callback ) )
			$result = call_user_func( $callback );
		else
			$result = new WP_Error( 'usam_invalid_ajax_callback', __( 'Неверный AJAX обратного вызова.', 'usam' ) );
		return $result;
	}	
	
	
	/**
	 * функция маштаба изображения, динамически изменяет размер изображения если не существует изображение такого размера.
	 */
	public function controller_scale_image() 
	{	
		if ( !isset($_REQUEST['attachment_id']) || !is_numeric( $_REQUEST['attachment_id'] ) )
			return;
			
		global $wpdb;	
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attachment_id = absint( $_REQUEST['attachment_id'] );
		$width = absint( $_REQUEST['width'] );
		$height = absint( $_REQUEST['height'] );
		$intermediate_size = '';

		if ( (($width >= 10) && ($height >= 10)) && (($width <= 10240) && ($height <= 10240)) )
		{
			$intermediate_size = "usam-{$width}x{$height}";
			$generate_thumbnail = true;	
		} 
		else 
		{
			if ( isset( $_REQUEST['intermediate_size'] ) )
				$intermediate_size = sanitize_text_field( $_REQUEST['intermediate_size'] );
			$generate_thumbnail = false;
		}
		// Если ID вложения больше 0, а ширина и высота больше или равна 10, и меньше, чем или равно 1024
		if ( ($attachment_id > 0) && ($intermediate_size != '') ) 
		{
			// Get all the required information about the attachment
			$uploads = wp_upload_dir();
			$image_meta = get_post_meta( $attachment_id, '' );
			$file_path = get_attached_file( $attachment_id );
			foreach ( $image_meta as $meta_name => $meta_value ) { // clean up the meta array
				$image_meta[$meta_name] = maybe_unserialize( array_pop( $meta_value ) );
			}
			if ( !isset( $image_meta['_wp_attachment_metadata'] ) )
				$image_meta['_wp_attachment_metadata'] = '';
			$attachment_metadata = $image_meta['_wp_attachment_metadata'];

			if ( !isset( $attachment_metadata['sizes'] ) )
				$attachment_metadata['sizes'] = '';
			if ( !isset( $attachment_metadata['sizes'][$intermediate_size] ) )
				$attachment_metadata['sizes'][$intermediate_size] = '';

			// determine if we already have an image of this size
			if ( (count( $attachment_metadata['sizes'] ) > 0) && ($attachment_metadata['sizes'][$intermediate_size]) ) 
			{
				$intermediate_image_data = image_get_intermediate_size( $attachment_id, $intermediate_size );
				if ( file_exists( $file_path ) ) 
				{
					$original_modification_time = filemtime( $file_path );
					$cache_modification_time = filemtime( $uploads['basedir'] . "/" . $intermediate_image_data['path'] );
					if ( $original_modification_time < $cache_modification_time ) {
						$generate_thumbnail = false;
					}
				}
			}
			if ( $generate_thumbnail == true )
			{			
				$crop = apply_filters( 'usam_scale_image_cropped', true );	
				$intermediate_size_data = image_make_intermediate_size( $file_path, $width, $height, $crop );			
				$attachment_metadata['sizes'][$intermediate_size] = $intermediate_size_data;
				wp_update_attachment_metadata( $attachment_id, $attachment_metadata );
				$intermediate_image_data = image_get_intermediate_size( $attachment_id, $intermediate_size );
			}
			if ( is_ssl ( ) )
				$output_url = str_replace( "http://", "https://", $intermediate_image_data['url'] );
			else
				$output_url = $intermediate_image_data['url'];					
			wp_redirect( $output_url );
		} 
		else 	
			_e( "Недопустимые параметры изображения", 'usam' );					
		exit();
	}
	
	function controller_number_of_unread_menager_chat_messages()
	{
		if ( usam_check_current_user_role( 'administrator' ) || usam_check_current_user_role('shop_manager') )
		{
			global $wpdb, $user_ID;
			$sql = "SELECT COUNT(*) FROM ".USAM_TABLE_CHAT_DIALOGS." AS d LEFT JOIN ".USAM_TABLE_CHAT." AS c ON ( c.topic=d.id ) WHERE c.user_id != '$user_ID' AND c.status = '0' AND d.manager_id='$user_ID'";	
			$count = $wpdb->get_var( $sql );
			
			return $count;
		}
	}
	
	function controller_delete_event_task_manager()
	{			
		if ( usam_check_current_user_role( 'administrator' ) || usam_check_current_user_role('shop_manager') )
		{
			$event_id = $_POST['id'];		
			
			$option = get_option( 'usam_set_events', '' );
			$events = unserialize( $option );		
			if ( isset($events[$event_id]) )
			{
				unset($events[$event_id]); 
				update_option( 'usam_set_events', serialize($events) );	
			}			
		}
	}	
}

function usam_url_admin_action( $action, $url = '', $arg = array() )
{
	if ( empty($url) )
		$url = $_SERVER['REQUEST_URI'];
	
	if ( !empty($arg) )
		$url = add_query_arg( $arg, $url );	
	
	return wp_nonce_url( add_query_arg( array('usam_admin_action' => $action), $url ), $action.'_nonce' );
}

function usam_url_action( $action, $url = '', $arg = array() )
{
	if ( empty($url) )
		$url = $_SERVER['REQUEST_URI'];
	
	if ( !empty($arg) )
		$url = add_query_arg( $arg, $url );	
	
	return wp_nonce_url( add_query_arg( array('usam_action' => $action), $url ), $action.'_nonce' );
}

/**
 * Вспомогательная функция, которая генерирует временное значение для действия AJAX. Функция автоматически добавлять префикс.
 * @since  3.8.9
 */
function usam_create_ajax_nonce( $ajax_action ) 
{
	return wp_create_nonce( "usam_ajax_{$ajax_action}" );
}