<?php
/**
 * Класс содержит загрузку модальных окон для сайта
 */			
class USAM_Modal
{		
	private $type_modal = '';
	
	public function __construct( $type_modal ) 
	{				
		$this->type_modal = $type_modal ;
	}
	
	public function get_html_modal( ) 
	{				
		return $this->fire_callback( $this->type_modal );
	}	
	
	private function fire_callback( $action )
	{			
		$callback = "controller_{$action}";	
		if ( method_exists( $this, $callback )) 
		{
			$result = $this->$callback();			
		}
		else
			$result = new WP_Error( 'usam_invalid_modal_callback', __( 'Неверный вызов метода класса USAM_Modal.', 'usam' ) );
		return $result;
	}	
	/**
	 * Получить html списка регионов 
	 */	
	private function controller_regions_search()
	{
		global $usam_cart;	
		$selecting = $usam_cart->get_properties('location');		
		ob_start();	
		
		$t = new USAM_Autocomplete_Forms();		
		$t->get_form_position_location( );
		
		$html = ob_get_contents();
		ob_end_clean();				
		
		$out = '<div class="select_region"><strong>'.__('Ваш регион', 'usam').':</strong>'.usam_get_full_locations_name($selecting).'</div>';
		$out .= '<form id="city-radio-from" action="" method="get">';
		$out .= '<input type="hidden" name="usam_action" value="location" />
				<table class="regions_search"><tr><td>'.$html.'</td><td><input type="submit" class="button button_save" name="submit" value="'.__('Выбрать', 'usam').'" /></td></tr></table>';
		$out .= '</form>';	
		return usam_get_modal_window( __('Выбор региона', 'usam'), 'regions_search', $out );	
	}
	
	/**
	 * Получить html списка регионов 
	 */	
	private function controller_regions()
	{
		global $usam_cart;	
		$selecting = $usam_cart->get_properties('location');
		$locations = usam_get_array_locations();		
		$out = '<div class="list_locations">'.usam_get_list_locations( $locations, $selecting ).'</div>';		
		$out .= "
		<script type='text/javascript'>
			jQuery(document).ready(function($)
			{				
				jQuery('.list_locations span').click(function(e) 
				{	
					var block = $(this).parent('li');		
					var location_id = $(this).parent('li').data('location');	
					if ( block.children('ul').is(':visible') )		
						block.find('span#open_'+location_id).html(' + ');		
					else
						block.find('span#open_'+location_id).html(' - ');
					block.children('ul').toggle();			
				}); 	
				
				$('.list_locations ul ul').hide();	
				$('.list_locations ul li .active').each(function()
				{									
					$(this).parents('ul').show();					
					$(this).parent().parent().parents('li').each(function()
					{						
						var location_id = $(this).data('location');						
						$('span#open_'+location_id).html(' - ');	
					});						
				});			
			});
		</script>";									
		return usam_get_modal_window( __('Выбор региона', 'usam'), 'regions', $out );	
	}
	
	private function controller_availability_by_warehouses()
	{	
		$product_id = absint($_REQUEST['id']);
		$title = get_the_title($product_id);
		$stores = usam_get_stores( array( 'issuing' => 1 ) );			
		$out = '<table class="usam_stores_table">';
		$out .= '<thead>';
		$out .= '<tr>';
		$out .= '<td>'.__('Адрес', 'usam').'</td>';	
		$out .= '<td>'.__('Режим работы', 'usam').'</td>';		
		$out .= '<td>'.__('Доступность', 'usam').'</td>';
		$out .= '</tr>';		
		$out .= '</thead>';
		$out .= '<tbody>';
		foreach ( $stores as $store )
		{
			$stock = usam_get_product_meta( $product_id, $store->meta_key );
			$availability = !empty($stock)?__('в наличии', 'usam'):__('отсутствует', 'usam');
			$class = !empty($stock)?'usam_product_in_stock':'usam_product_not_available';
			$location = usam_get_location( $store->location_id );
			$city = isset($location['name'])?$location['name']:'';
			$out .= '<tr>';		
			$out .= "<td class='title'><span class='store_name'>".__('город','usam')." {$city} {$store->address}</span><p class='store_phone'>$store->phone</p></td>";			
			$out .= "<td class='schedule'>$store->schedule</td>";
			$out .= "<td class='availability'><span class='{$class}'>$availability</span></td>";			
			$out .= '</tr>';		
		}
		$out .= '</tbody>';
		$out .= '</table>';		
		return usam_get_modal_window( sprintf(__('Доступность товара %s', 'usam'),$title), 'availability_by_warehouses', $out );	
	}
	
	private function controller_pickup_order()
	{		
		global $usam_cart;
		$shipping_method = $usam_cart->get_selected_shipping_method( );  
		$out = '';
		if ( !empty($shipping_method->setting['stores']) )
		{		
			$args = array( 'fields' => 'all' , 'include' => $shipping_method->setting['stores'], 'orderby' => 'sort', 'order' => 'ASC' );
			$stores = usam_get_stores( $args );
			
			ob_start();			
			?>						
			<div id="store_shop">		
			<div class ="store_shop_list">	
				<table id="store_table">
					<tbody>
						<?php				
						$selected_store = $usam_cart->get_properties( 'storage_pickup' );			
						foreach( $stores as $store )
						{
							$selected = $selected_store == $store->id?"selected":'';	
							$location = usam_get_location( $store->location_id );
							$city = isset($location['name'])?$location['name']:'';
							$title = empty($store->address)?$store->title:__('город','usam').' '.$city.' '.$store->address;
							?>
							<tr class="store_row <?php echo $selected; ?>" id="store_<?php echo $store->id; ?>" data-store_id="<?php echo $store->id; ?>" data-GPS_S="<?php echo $store->GPS_S; ?>" data-GPS_N="<?php echo $store->GPS_N; ?>" onclick="usam_selected_store(<?php echo $store->id; ?>);">
								<td class="lilne">
									<label for="store_1">
										<div class="name"><strong><?php echo $title; ?></strong></div>
										<?php if ( $store->phone != '' ) { ?>
										<div class="phone"><?php _e('Телефон', 'usam'); ?>: <?php echo $store->phone; ?></div>
										<?php }
										if ( $store->email != '' ) { ?>
										<div class="email"><?php _e('Электронная почта', 'usam'); ?>: <a href="mailto:<?php echo $store->email; ?>"><?php echo $store->email; ?></a></div>
										<?php }										
										if ( $store->schedule != '' ) { ?>
										<div class="shud"><?php _e('Режим работы', 'usam'); ?>: <?php echo $store->schedule; ?></div>
										<?php } ?>
									</label>
									<?php if ( $store->description != '' ) { ?>
									<div class="desc"><?php _e('Описание', 'usam'); ?>: <?php echo $store->description; ?></div>
									<?php } ?>
								</td>
							</tr>	
							<?php
						}
						?>			
					</tbody>
				</table>
			</div>
			<div id="map"></div>		
			</div>	
			<div class="popButton">
				<button type="button" class="button button_save" data-dismiss="modal" onclick="selected_pickup_method(<?php echo $shipping_method->id; ?> );"><?php _e( 'Сохранить', 'usam' ); ?></button>
				<button type="button" class="button button_close" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
			</div>		
			<?php					
			$out = ob_get_contents();			
			ob_end_clean();	
			
			$html_id = "pickup_".$shipping_method->id;
			$out = usam_get_modal_window( __('Пункты выдачи заказа', 'usam'), $html_id, $out );			
		}	
		return $out;
	}
}
?>