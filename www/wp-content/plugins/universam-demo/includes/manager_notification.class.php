<?php
/**
 * Класс уведомлений менеджера
 * @since 3.7
*/
class USAM_Manager_Notification
{			
	public function __construct() 
	{
		add_action( 'usam_feedback_insert', array($this, 'notification_feedback') );
		add_action( 'usam_price_comparison_insert', array($this, 'notification_feedback') );
		add_action( 'usam_review_insert', array($this, 'notification_review') );
		add_action( 'usam_set_gateway_order', array($this, 'notification_order') );
		add_action( 'usam_chat_message_insert', array($this, 'notification_chat_message') );
		add_action( 'usam_no_link_to_product', array($this, 'notification_no_link_to_product') );	
		add_action( 'usam_update_stock', array($this, 'notification_stock'), 10, 3 );
	}
	
	private function get_notifications( $type ) 
	{
		$option = get_option('usam_notifications');
		$notifications = maybe_unserialize( $option );	
		if ( empty($notifications) )
			return array();
		
		$results = array();
		foreach ( $notifications as $notification ) 
		{
			if ( $notification['active'] )
			{
				if ( empty($notification['events'][$type]['email']) )
					unset($notification['email']);
				if ( empty($notification['events'][$type]['sms']) )
					unset($notification['phone']);
				$results[] = $notification;
			}
		}
		return $results;
	}	
	
	public function send_mail( $address, $subject, $message ) 
	{
		$message .= "<p>".sprintf( __('Система уведомлений с сайта %s','usam'), get_bloginfo('name'))."</p>";
		$subject = sprintf( __('Уведомление от %s','usam'),get_bloginfo('name')).' - '.$subject;	
		usam_mail( $address, $subject, $message );
	}
	
	public function notification_feedback( $t ) 
	{		
		$notifications = $this->get_notifications( 'feedback' );
		foreach ( $notifications as $notification ) 
		{		
			if ( !empty($notification['email']) )
			{										
				$message = "<h2>".__('Новое сообщение покупателя:','usam')."</h2>";	
				$message .= "<p>".$t->get('message')."</p><br>";		
				$message .= "<a href='".admin_url('admin.php')."?page=feedback'>".__('Посмотреть сообщение', 'usam')."</a>";	
				
				$subject = __('получено сообщение от покупателя','usam');
				
				$this->send_mail( $notification['email'], $subject, $message );
			}
			if ( !empty($notification['phone']) )
			{					
				$message = __('Получено новое сообщение от покупателя.','usam');
				$sent = usam_send_sms( $notification['phone'], $message );
			}
		}
	}
	
	public function notification_review( $t ) 
	{		
		$notifications = $this->get_notifications( 'feedback' );
		foreach ( $notifications as $notification ) 
		{
			if ( !empty($notification['email']) )
			{							
				$message = "<h2>".__('Получен новый отзыв посетителя:','usam')."</h2>";	
				$message .= $t->get('name')."<br><br>".$t->get('title')."<br><br>";		
				$message .= "<a href='".admin_url('admin.php')."?page=feedback&tab=reviews'>".__('Посмотреть посмотреть', 'usam')."</a>";	
				$message .= "<p>".__("Вам нужно будет утвердить этот отзыв, прежде чем появиться на вашем сайте","usam")."</p>";
			
				$subject = __('получено сообщение от покупателя','usam');
				
				$this->send_mail( $notification['email'], $subject, $message );
			}
			if ( !empty($notification['phone']) )
			{					
				$message = __('Получен новый отзыв посетителя.','usam');
				$sent = usam_send_sms( $notification['phone'], $message );
			}
		}
	}
				
	public function notification_chat_message( $t ) 
	{		
		if ( current_user_can('manage_options') )
			return false;
		
		$notifications = $this->get_notifications( 'feedback' );
		foreach ( $notifications as $notification ) 
		{
			if ( !empty($notification['email']) )
			{						
				$message = "<h2>".__('Новое сообщение покупателя отправленное с помощью чата:')."</h2>";	
				$message .= "<p>".$t->get('message')."</p><br>";		
				$message .= "<a href='".admin_url('admin.php')."?page=feedback&tab=chat'>".__('Посмотреть сообщение', 'usam')."</a>";					
				
				$subject = __('получено сообщение от покупателя','usam');
				$this->send_mail( $notification['email'], $subject, $message );
			}
			if ( !empty($notification['phone']) )
			{					
				$message = __('Получено новое сообщение от покупателя.');
				$sent = usam_send_sms( $notification['phone'], $message );
			}
		}
	}
	
	public function notification_no_link_to_product( $product_ids ) 
	{	
		$notifications = $this->get_notifications( 'webproduct' );
		foreach ( $notifications as $notification ) 
		{
			if ( !empty($notification['email']) )
			{	
				$message = sprintf(__('Есть товары, ссылки на сайты поставщиков которых больше не доступны. Ошибка 404. Проверьте товары. ID товаров:/n %s'), $product_ids);		
				
				$subject = __('товары сняты с продажи','usam');
				$this->send_mail( $notification['email'], $subject, $message );
			}
			if ( !empty($notification['phone']) )
			{					
				$message = __('Есть товары, ссылки на сайты поставщиков которых больше не доступны.');
				$sent = usam_send_sms( $notification['phone'], $message );
			}
		}
	}
	
	public function notification_order( $t ) 
	{		
		$order_type = $t->get('order_type');
		if ( $order_type == 'manager' )
			return false;		

		$notifications = $this->get_notifications( 'order' );		
		foreach ( $notifications as $notification ) 
		{			
			$type_price = $t->get('type_price');			
			foreach ( $notification['events']['order']['conditions'] as $type => $value ) 
			{ 
				if ( $type == 'prices' && !( in_array('', $value) || in_array($type_price, $value) ) )
				{		
					return false;
				}				
			}			
			$order_notification = new USAM_Order_Admin_Notification( $t );			
			if ( !empty($notification['email']) )
			{						
				$order_notification->set_address( $notification['email'] );
				$email_sent = $order_notification->send_mail();		
			}
			if ( !empty($notification['phone']) )
			{						
				$order_notification->set_phone( $notification['phone'] );				
				$email_sent = $order_notification->send_sms();
			}
		}	
	}
	
	public function notification_stock( $product_id, $stock, $old_stock ) 
	{		
		$product_title = get_the_title( $product_id );
		$product_sku = usam_get_product_meta( $product_id, 'sku' );
		$product_barcode = usam_get_product_meta( $product_id, 'barcode' );
		
		if ( $stock > 0 )
		{
			$notifications = $this->get_notifications( 'low_stock' );		
			foreach ( $notifications as $notification ) 
			{						
				if ( $notification['events']['low_stock']['stock'] <= $stock )
				{
					continue;
				}				
				$compare = new USAM_Compare();		
				foreach ( $notification['events']['low_stock']['conditions'] as $type => $c ) 
				{ 
					if ( $type == 'category' )
					{							
						if ( !in_array('', $c) )
						{
							$result = $compare->compare_terms( $product_id, 'usam-category', $c );
							if ( !$result )
								break;
						}
					}			
					elseif ( $type == 'category_sale' )
					{	
						$result = $compare->compare_terms( $product_id, 'usam-category_sale', $c );
						if ( !$result )
							break;
					}		
					elseif ( $type == 'brads' )
					{	
						$result = $compare->compare_terms( $product_id, 'usam-brads', $c );
						if ( !$result )
							break;
					}				
				}			
				if ( !empty($notification['email']) )
				{		
					$subject = __('не большой запас', 'usam');
					$message = '<h2>'.sprintf(__('У товара %s (артикул %s) не большой запас'), $product_title, $product_sku).'</h2>';		
					$message .= "<span id='product_barcode'>".$product_barcode."</span>";					
					$this->send_mail( $notification['email'], $subject, $message );
				}
				if ( !empty($notification['phone']) )
				{					
					$message = sprintf(__('%s - не большой запас'), $product_title);		
					$sent = usam_send_sms( $notification['phone'], $message );
				}
			}
		}
		else
		{
			$notifications = $this->get_notifications( 'no_stock' );		
			foreach ( $notifications as $notification ) 
			{						
				$compare = new USAM_Compare();		
				foreach ( $notification['events']['no_stock']['conditions'] as $type => $c ) 
				{ 
					if ( $type == 'category' )
					{				
						if ( !in_array('', $c) )
						{  
							$result = $compare->compare_terms( $product_id, 'usam-category', $c );
							if ( !$result )
								break;
						}					
					}			
					elseif ( $type == 'category_sale' )
					{	
						$result = $compare->compare_terms( $product_id, 'usam-category_sale', $c );
						if ( !$result )
							break;
					}		
					elseif ( $type == 'brads' )
					{	
						$result = $compare->compare_terms( $product_id, 'usam-brads', $c );
						if ( !$result )
							break;
					}				
				}			
				if ( !empty($notification['email']) )
				{									
					$subject = __('распродан', 'usam');
					$message = '<h2>'.sprintf(__('Товар %s (артикул %s) распродан'), $product_title, $product_sku).'</h2>';		
					$message .= "<span id='product_barcode'>".$product_barcode."</span>";					
					$this->send_mail( $notification['email'], $subject, $message );
				}
				if ( !empty($notification['phone']) )
				{					
					$message = sprintf(__('%s - распродан'), $product_title);		
					$sent = usam_send_sms( $notification['phone'], $message );
				}
			}	
		}
	}		
}
new USAM_Manager_Notification();
?>