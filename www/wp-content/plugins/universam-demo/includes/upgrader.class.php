<?php
// Класс обновления */
class USAM_Upgrader 
{
	private $itemId = null;
	private $personalToken = null;
	private $currentVersion = null;
	private $slug = null;
	private $pluginSlug = null;
	private $name = null;

	public $enabled = false;

	public function __construct( $id, $data ) 
	{ 	
		if ( ! empty( $id ) ) 
		{
			$this->itemId         = $id;
			$this->personalToken  = ( isset( $data['token'] ) ) ? $data['token'] : null;
			$this->currentVersion = ( isset( $data['version'] ) ) ? $data['version'] : null;
			$this->slug           = ( isset( $data['slug'] ) ) ? $data['slug'] : null;
			$this->pluginSlug     = ( isset( $data['pluginSlug'] ) ) ? $data['pluginSlug'] : null;
			$this->name           = ( isset( $data['name'] ) ) ? $data['name'] : null;
		}
		$this->enabled = self::is_enabled();
		
	//	$site	=	get_option('_site_transient_update_plugins');
	//	$plugins	= get_site_transient( 'update_plugins' );		
		if ( $this->enabled ) 
		{ 
			add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_update' ) ); // вызывает wp_update_plugins
			add_filter( 'plugins_api', array( &$this, 'check_info' ), 10, 3 );
			add_filter( 'upgrader_package_options', array( &$this, 'setUpdatePackage' ) );
		//	add_action( 'pre_set_site_transient_update_themes', array( __CLASS__, 'transient_update_themes' ), 21, 1 );
		}
	}

	private function is_enabled() 
	{
		return (
			! is_null( $this->itemId ) &&
			! is_null( $this->personalToken ) &&
			! is_null( $this->currentVersion ) &&
			! is_null( $this->slug ) &&
			! is_null( $this->pluginSlug ) &&
			! is_null( $this->name )
		);
	}
	
	function send_request( $params )
	{
		$args = array(
			'headers' => array(
				'Authorization' => 'Bearer ' . $this->personalToken,
			),
			'timeout' => 30,
		);
		$url  = 'http://wp-universam.ru/api';			
					
		$query = http_build_query($params);
		
		$response = wp_remote_get( esc_url_raw( $url.'?'. $query), $args );
		$response_code    = wp_remote_retrieve_response_code( $response );
		$response_message = wp_remote_retrieve_response_message( $response );
	
		if ( 200 !== $response_code && ! empty( $response_message ) )
			return new WP_Error( $response_code, $response_message );
		elseif ( 200 !== $response_code )
			return new WP_Error( $response_code, sprintf( __( 'Произошла API ошибка №%s.', 'usam' ),$response_code) );
		else 
		{
			$out = json_decode( wp_remote_retrieve_body( $response ), true );
			if ( null === $out ) {
				return new WP_Error( 'api_error', __( 'Произошла неизвестная ошибка API.', 'usam' ) );
			}
			return $out;
		}		
	}
	
	public function check_info( $result, $action, $args ) 
	{		
		if ( isset( $args->slug ) && sanitize_key( $args->slug ) == sanitize_key( $this->slug ) ) 
		{
			$params = array(
				'license' => $this->personalToken,
				'software' => $this->itemId,
				'query' => 'check_info',
			);	
			$pluginInfo = $this->send_request( $params );		
			if ( isset( $pluginInfo['wordpress_plugin'] ) ) 
			{
				$info        = $pluginInfo['wordpress_plugin'];			
				$sections    = explode( '<h2 id="item-description__changelog">Changelog</h2>', $pluginInfo['description'] );
				$description = ( isset( $sections[0] ) ) ? $sections[0] : '';
				$changelog   = ( isset( $sections[1] ) ) ? $sections[1] : '';

				$plugin                  = new stdClass();
				$plugin->name            = $info['plugin_name'];
				$plugin->author          = $info['author'];
				$plugin->slug            = $this->slug;
				$plugin->version         = $info['version'];
				$plugin->requires        = $info['required'];
				$plugin->tested          = $info['tested'];
				$plugin->rating          = ( (int) $pluginInfo['rating']['count'] < 3 ) ? 100.0 : 20 * (float) $pluginInfo['rating']['rating'];
				$plugin->num_ratings     = (int) $pluginInfo['rating']['count'];
				$plugin->active_installs = (int) $pluginInfo['number_of_sales'];
				$plugin->last_updated    = $pluginInfo['updated_at'];
				$plugin->added           = $pluginInfo['published_at'];
				$plugin->homepage        = $pluginInfo['homepage'];
				$plugin->sections        = array(
					'description' => $pluginInfo['description'],
					'changelog'   => $pluginInfo['changelog']
				);
				$plugin->download_link   = $pluginInfo['url'];
				$plugin->banners         = array(
					'high' => $pluginInfo['previews']['landscape_url']
				);
				return $plugin;
			} 
			else 
				return false;
		} 
		else 
		{
			return false;
		}
	}
	
	public static function transient_update_themes( $transient ) 
	{
		$update_data = self::get_update_data();

		foreach ( WC_Helper::get_local_woo_themes() as $theme ) 
		{
			if ( empty( $update_data[ $theme['_product_id'] ] ) ) {
				continue;
			}

			$data = $update_data[ $theme['_product_id'] ];
			$slug = $theme['_stylesheet'];

			$item = array(
				'theme' => $slug,
				'new_version' => $data['version'],
				'url' => $data['url'],
				'package' => '',
			);

			if ( self::_has_active_subscription( $theme['_product_id'] ) ) {
				$item['package'] = $data['package'];
			}

			if ( version_compare( $theme['Version'], $data['version'], '<' ) ) {
				$transient->response[ $slug ] = $item;
			} else {
				unset( $transient->response[ $slug ] );
				$transient->checked[ $slug ] = $data['version'];
			}
		}

		return $transient;
	}
	
	public function check_update( $transient ) 
	{		
	//	if ( empty( $transient->checked ) ) 		
	//		return $transient;		

		$params = array(
			'license' => $this->personalToken,
			'software' => $this->itemId,
			'query' => 'check_update',			
		);	
		$info = $this->send_request( $params );				
	
		$plugin                                   = new stdClass();				
		$plugin->id                               = $this->slug;
		$plugin->slug                             = $this->slug;
		$plugin->plugin                           = $this->pluginSlug;				
		if ( !is_wp_error( $info ) && !empty($info['version']) ) 
		{			
			$plugin->id                               = $this->slug;
			$plugin->slug                             = $this->slug;
			$plugin->plugin                           = $this->pluginSlug;				
			if ( version_compare( $this->currentVersion, $info['version'], '<' ) )
			{ 
				$plugin->new_version = $info['version'];	
				$plugin->package     = $info['package'];
				$transient->response[ $this->pluginSlug ] = (object) $plugin;				
				unset( $transient->no_update[ $this->pluginSlug ] );
			}
			else
			{
				$transient->no_update[ $this->pluginSlug ] = (object) $plugin;
				unset( $transient->response[ $this->pluginSlug ] );
			}
		}
		else
		{
			$transient->no_update[ $this->pluginSlug ] = (object) $plugin;
			unset( $transient->response[ $this->pluginSlug ] );
		}	
		return $transient;
	}
	
// Получить файл плагина
	public function setUpdatePackage( $options ) 
	{ 
		$package = $options['package'];
		if ( $package === $this->pluginSlug ) 
		{ 
			$params = array(
				'license' => $this->personalToken,
				'software' => $this->itemId,
				'query' => 'check_update',	
			);	
			$result = $this->send_request( $params );			
			if ( !is_wp_error( $result ) && isset($result['package']) && empty($response['error']) ) 
			{			
				$options['package'] = $result['package'];
			}
		}
		return $options;
	}
}
