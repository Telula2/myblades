<?php
// Класс работы с местоположениями 
class USAM_Locations_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $request;

	private $compat_fields = array( 'results', 'total' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}

	/**
	 * Fills in missing query variables with default values.
	 */
	public static function fill_query_vars( $args ) 
	{
		$defaults = array(
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'monthnum' => '',
			'year' => '',
			'w' => '',
			'status' => '',
			'status__in' => array(),
			'status__not_in' => array(),
			'manager_id' => '',
			'manager__in' => array(),
			'manager__not_in' => array(),
			'meta_key' => '',
			'meta_value' => '',
			'meta_compare' => '',
			'include' => array(),
			'exclude' => array(),
			'search' => '',
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => '',
			'number' => '',
			'paged' => 1,
			'count_total' => true,
			'cache_results' => false,						
			'fields' => 'all',				
		);
		return wp_parse_args( $args, $defaults );
	}
	
	public function prepare_query( $query = array() ) 
	{
		global $wpdb;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_locations', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );		
				
		
		$join = array();	
		if ( is_array( $qv['fields'] ) ) 
		{
			$qv['fields'] = array_unique( $qv['fields'] );
			
			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) 
			{				
				$field = 'ID' === $field ? 'ID' : sanitize_key( $field );				
				$this->query_fields[] = USAM_TABLE_LOCATION.".$field";				
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 
		elseif ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_LOCATION.".*";		
		elseif ( $qv['fields'] == 'id=>name' ) 
			$$this->query_fields = USAM_TABLE_LOCATION.".name,".USAM_TABLE_LOCATION.".id";	
		else	
			$this->query_fields = USAM_TABLE_LOCATION.".".$qv['fields'];	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;
		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_LOCATION;
		$this->query_where = "WHERE 1=1";		
		
		if ( ! empty( $qv['include'] ) ) {
			$include = wp_parse_id_list( $qv['include'] );
		} else {
			$include = false;
		}				
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];			
		}		
		if ( ! empty($groupby) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $groupby );
		else
			$this->query_groupby = '';		
		
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{	// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}	
		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}
			
			if ( is_int( $_key ) ) 
			{	// Integer key means this is a flat array of 'orderby' fields.
				$_orderby = $_value;
				$_order = $order;
			} 
			else 
			{ // Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}
			$parsed = $this->parse_orderby( $_orderby );

			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = $parsed . ' ' . $this->parse_order( $_order );
		}

		// If no valid clauses were found, order by id.
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = USAM_TABLE_LOCATION.".id $order";		}

		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );
		
		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) 
		{
			if ( $qv['offset'] ) 
			{
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			} 
			else 
			{
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		} 
		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) 
		{
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'name' ) );
			
			if ( ! $search_columns ) 
			{
				if ( is_numeric($search) )
					$search_columns = array('id' );
				else
					$search_columns = array( 'name');
			}	
			$search_columns = apply_filters( 'usam_location_search_columns', $search_columns, $search, $this );
			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}
		if ( ! empty( $include ) ) 
		{			
			$ids = implode( ',', $include );
			$this->query_where .= " AND ".USAM_TABLE_LOCATION.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_LOCATION.".ID NOT IN ($ids)";
		}	

		if ( ! empty( $qv['type'] ) ) 
		{
			if ( !is_array($qv['type']) )
				$qv['type'] = array( $qv['type'] );
			
			$type_location = usam_get_types_location( 'code' );
			$type_ids = array();
			foreach ( $qv['type'] as $type )
			{
				if ( isset($type_location[$type]) )
					$type_ids[] = $type_location[$type]->id;
			}
			if ( !empty($type_ids) )
			{						
				$this->query_where .= " AND ".USAM_TABLE_LOCATION.".id_type IN (".implode( ',', $type_ids ).")";
			}
		}			
		elseif ( ! empty( $qv['type_to'] ) ) 
		{						
			$type_location = usam_get_types_location( 'code' );
			$type_ids = array();
			foreach ( $type_location as $code => $type )
			{					
				$type_ids[] = $type->id;	
				if ( $type->code == $qv['type_to'] )
				{	
					break;
				}
			}
			if ( !empty($type_ids) )
			{						
				$this->query_where .= " AND ".USAM_TABLE_LOCATION.".id_type IN (".implode( ',', $type_ids ).")";
			}
		}		
		do_action_ref_array( 'usam_pre_location_query', array( &$this ) );
	}
	
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;
		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";		
		if ( 'id=>name' == $qv['fields'] ) 
		{			
			$this->results = $wpdb->get_results( $this->request );			

			if ( !$this->results )
				return;		
			
			$r = array();
			foreach ( $this->results as $key => $result ) 
			{
				$r[ $result->id ] = $result->name;
			}			
			$this->results = $r;
			
			if ( isset( $qv['count_total'] ) && $qv['count_total'] )
				$this->total = $wpdb->get_var( apply_filters( 'found_location_query', 'SELECT FOUND_ROWS()' ) );				
		}
		else
		{
			if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] )
			{
				$this->results = $wpdb->get_results( $this->request );
			} 
			elseif ( $qv['number'] == 1 )
			{
				$this->results = $wpdb->get_var( $this->request );
			} 		
			else 
			{
				$this->results = $wpdb->get_col( $this->request );
			}			
			if ( !$this->results )
				return;		
			
			if ( isset( $qv['count_total'] ) && $qv['count_total'] )
				$this->total = $wpdb->get_var( apply_filters( 'found_location_query', 'SELECT FOUND_ROWS()' ) );					
			
			if ( 'all' == $qv['fields'] )
			{			
				if ( $qv['cache_results'] )
				{	
					if ( $qv['number'] == 1 )
						wp_cache_set( $this->results->id, (array)$this->results, 'usam_location' );	
					else
					{					
						foreach ( $this->results as $result ) 
						{
							wp_cache_set( $result->id, (array)$result, 'usam_location' );						
						}
					}		
				}
			}	
		}				
	}
	
	public function get( $query_var ) 
	{
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}
	
	public function set( $query_var, $value )
	{
		$this->query_vars[$query_var] = $value;
	}

	/**
	 * Used internally to generate an SQL string for searching across multiple columns
	 */
	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;		
				
		foreach ( $cols as $col )
		{			
			if ( 'ID' == $col || 'id_type' == $col ) 
			{
				$searches[] = $wpdb->prepare( "$col = %s", $string );
			} 			
			else 
			{
				$searches[] = $wpdb->prepare( "$col LIKE LOWER ('%s')", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}
	
	public function get_results() 
	{
		return $this->results;
	}

	/**
	 * Return the total number of users for the current query.
	 */
	public function get_total() 
	{
		return $this->total;
	}
	
	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';
		if ( 'ID' == $orderby ) 
		{
			$_orderby = USAM_TABLE_LOCATION.'.id';
		} 	
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) ) 
		{
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_LOCATION.".id, $include_sql )";
		} 
		else
			$_orderby = USAM_TABLE_LOCATION.'.'.$orderby;
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) {
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments ) {
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

function usam_get_locations( $query = array() )
{	
	$query['count_total'] = false;
	$orders = new USAM_Locations_Query( $query );	
	return $orders->get_results();	
}