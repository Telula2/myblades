<?php
/* Технические функции работы кода магазина
 * @since 3.7
 */
 
/**
 * Исключение обработчика
 */
class USAM_Exception extends Exception { }


/**
 * Вывод информации
 */
class USAM_Debug
{
	private $debug = array();	

	public function __construct()
	{			
		add_action( 'wp_enqueue_scripts', array( &$this, 'css' ) );	
		add_action( 'admin_enqueue_scripts', array( &$this, 'css' ) );			
					
		$this->debug = get_option( 'usam_debug', array('location' => '' ) );			
		switch( $this->debug['location'] )				
		{							
			case 'all':		
				add_action('wp_footer', array( $this,'display' ) );
				add_action('admin_footer', array( $this,'display' ) );
			break;
			case 'admin':			
				add_action('admin_footer', array( $this,'display' ) );	
			break;
			case 'site':			
				add_action('wp_footer', array( $this,'display' ) );	
			break;
		}			
		add_action('wp_footer', array( $this,'insert_performance_numbers' ),999 );
		add_action('admin_footer', array( $this,'insert_performance_numbers' ),999 );	
		
		if ( defined('WP_DEBUG') && WP_DEBUG )
			add_filter( 'http_request_host_is_external', create_function('', 'return true;'), 10, 3 );
	}
	
	function insert_performance_numbers()
	{	
		if ( usam_check_current_user_role('administrator') )
		{
			?>	
			<script type="text/javascript">
				jQuery(document).ready(function() 
				{				
					jQuery("#wp-admin-bar-performance_site .ab-label").html('<?php echo round(memory_get_usage()/1024/1024, 2).' MB '.' | '.get_num_queries().' SQL | '.timer_stop().' '.__('сек','usam') ?>');				
				});
			</script>
			<?php
		}
	}
	
	function css()
	{		
		wp_enqueue_style( 'usam-technical', USAM_URL . '/admin/css/technical.css', false, '1.1', 'all' );		
	}
	
	function display()
	{
		if ( !usam_check_current_user_role('administrator') ) 
			return '';
			
		$this->title_header = array( 'sql' => __("SQL запросы", 'usam'), 'globals' => __("GLOBALS", 'usam'), );
		
		if ( is_admin() )
			$class = 'admin';
		else
			$class = 'site';		
		echo '
		<div class = "technical_display">
		<div class = "technical_display_'.$class.'">
		<div class = "tabs_header">		
			<ul>';
				$current = true;
				foreach ( $this->debug['display'] as $tab => $display ) 
				{
					if ( $display == 1 )
					{
						
						echo '<li id="'.$tab.'" class="tab_header '.($current?'current':'hidden').'"><a href="">'.$this->title_header[$tab].'</a></li>';
						$current = false;
					}
				}							
			echo '</ul>
		</div>';
		echo '<div class = "tabs_body">';
		$current = true;
		foreach ( $this->debug['display'] as $tab => $display ) 
		{
			if ( $display == 1 )
			{
				echo '<div id = "'.$tab.'" class = "tab  '.($current?'current':'hidden').'">';
					$this->$tab();	
				echo '</div>';
				$current = false;
			}
		}
		echo '</div></div>';
	}
 
 //Производительность
	function sql()
	{
		if ( defined('SAVEQUERIES') && SAVEQUERIES ) 
		{
			global $wpdb;
			echo '########## SQL запросы ##########<br /><br />'; 
			echo '<span style="margin-left: 10px;"><b>'; echo 'Количество SQL-запросов = ' . sizeof($wpdb->queries); echo '</b>
			</span><br />'; 
			$sqlstime = 0; 
			foreach ( $wpdb->queries as $qrarr ) 
			{ 
				$sqlstime += $qrarr[1]; 
			} 
			echo '<span style="margin-left: 10px;"><b>'; echo 'Затрачено времени = ' . round( $sqlstime, 4 ) . ' секунд'; echo '</b></span><br /><br />'; 
			$i = 1;
			foreach ( $wpdb->queries as $qrarr )
			{ 
					echo '<span style="color: #EDE7C2;margin-left: 10px;">№ '.$i.' SQL-запрос = </span>' . $qrarr[0] . '<br />';
					echo '<span style="color: #EDE7C2;margin-left: 10px;">Время выполнения = </span>' . round( $qrarr[1], 4 ) . ' секунд<br />';
					echo '<span style="color: #EDE7C2;margin-left: 10px;">Файлы и функции: </span><br />'; $filesfunc = explode( ",", $qrarr[2] ); 
				foreach ( $filesfunc as $funcs ) 
				{ 
					echo '<span style="margin-left: 20px;">+ ' . $funcs . '</span><br />'; 
				} echo '<br />'; 
				$i++;
			} 
			echo '<br />########## END: SQL запросы ##########<br /><br />'; 
		}
	}
	
	function globals()
	{		
		print_r($GLOBALS);	
	}		
}
new USAM_Debug();



/*	Описание: класс для создания лог файлов
 */
class USAM_Log_File
{
	private $f_log;
	private $file_log;
	
	function __construct( $file_name = '', $file_text_header = '' )
	{	
		if ( $file_text_header == '' )
			$file_text_header = __('Ошибки и уведомления UNIVERSAM','usam');
		
		if ( $file_name == '' )
			$file_name = 'log'; 
		
		$ext = 'txt';
		$this->file_log  = USAM_UPLOAD_DIR.'Log/'.current_time("m-Y").'_'.$file_name.'.'.$ext;	
		$this->f_log = fopen($this->file_log,"a");
		if (file_exists($file_name)) 
		{		
			$this->file_fwrite_header( $file_text_header );
		}
	}
	
	private function fwrite( $text ) 
	{					
		if ( empty($text) )
			return false;
		
		$text = "[".current_time("d-m-Y H:i:s")."] ".$text;		
		$text .= "\r\n";
		return fwrite($this->f_log, $text );
	}
	
	private function file_fwrite_header( $text ) 
	{			
		if ( empty($text) )
			return false;
		
		$text = mb_strtoupper($text, 'UTF-8'); 
		$text .= "______________________________ $text ______________________________\r\n";
		return fwrite($this->f_log, $text );
	}
	
	private function text_filter( $text ) 
	{			
		return preg_replace('#<br(\s+)?\/?>#i', "\r\n", $text);
	}
	
	public function file_fwrite( $file_text ) 
	{	
		if ( $file_text !== '' && $this->f_log !== false )
		{			
			if (is_array($file_text) )
			{
				$str = "\r\n";
				foreach ($file_text as $key => $value) 
				{
					$str .= $this->text_filter( $key.' => '.$value )."\r\n";	
				}
			}
			else
			{			
				$str = $this->text_filter( $file_text );				
			}
			$this->fwrite( $str );
		}		
	}	
	
	public function debug_backtrace( ) 
	{			
		ob_start();		
		
		print_r( debug_backtrace() );
		
		$str = ob_get_contents();
		ob_end_clean();
		
		$this->fwrite( $str );
	}
	
	public function fwrite_array( $file_text ) 
	{	
		ob_start();		
		
		print_r( $file_text );
		
		$str = ob_get_contents();
		ob_end_clean();
		
		$this->fwrite( $str );
	}	
	
	public function file_fclose( $delimiter = true ) 
	{
		if ( $this->f_log !== false )
		{
			if ( $delimiter )
				fwrite($this->f_log, "\r\n_________________________________________________\r\n\r\n\r\n");
			
			fclose($this->f_log);
		}
	}
}	
	
function usam_log_file( $error, $file_name = '', $delimiter = true ) 
{
	if ( empty($error) )
		return false;
	
	$Log = new USAM_Log_File( $file_name ); 							
	$Log->file_fwrite( $error );			
	$Log->file_fclose( $delimiter );
}

function usam_start_measure_performance() 
{
	global $usam_performance;
	
	$usam_performance = array( 'timer' => microtime(true), 'queries' => get_num_queries(), 'memory' => round(memory_get_usage()/1024/1024, 2) );
}

function usam_end_measure_performance() 
{
	global $usam_performance;
	
	$timer = microtime( true ) - $usam_performance['timer'];
	$memory = round(memory_get_usage()/1024/1024, 2) - $usam_performance['memory'];
	$queries = get_num_queries() - $usam_performance['queries'];
			
	return "$memory MB | $queries SQL | $timer ".__('сек','usam');	
}
?>