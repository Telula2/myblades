<?php
// Класс работы с письмами
class USAM_Email_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $meta_query = false;
	public $request;

	private $compat_fields = array( 'results' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}
	
	/**
	 * Fills in missing query variables with default values.
	 */
	public static function fill_query_vars( $args ) 
	{			
		$defaults = array(
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'monthnum' => '',
			'year' => '',
			'w' => '',	
			'id'  => '',			
			'from_name'  => '',		
			'from_email'  => '',		
			'date_insert'  => '',
			'sent_at' => '',		
			'folder' => '',	
			'read' => '',			
			'from_email__in' => array(),
			'from_email__not_in' => array(),			
			'include' => array(),
			'exclude' => array(),
			'search' => '',
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => '',
			'number' => '',
			'paged' => 1,
			'count_total' => true,
			'cache_results' => false,
			'cache_attachments' => false,		
			'cache_object' => false,				
			'fields' => 'all',			
		);		
		return wp_parse_args( $args, $defaults );
	}

	/**
	 * Prepare the query variables.
	 *
	 * @since 3.1.0
	 * @since 4.1.0 Added the ability to order by the `include` value.
	 * @since 4.2.0 Added 'meta_value_num' support for `$orderby` parameter. Added multi-dimensional array syntax
	 *              for `$orderby` parameter.
	 * @since 4.3.0 Added 'has_published_posts' parameter.
	 * @since 4.4.0 Added 'paged', 'from_email__in', and 'from_email__not_in' parameters. The 'status' parameter was updated to
	 *              permit an array or comma-separated list of values. The 'number' parameter was updated to support
	 *              querying for all users with using -1.
	
	 * @global wpdb $wpdb WordPress database abstraction object.
	 * @global int  $blog_id
	 *
	 * @param string|array $query {
	 *     Optional. Array or string of Query parameters.	
	 *     @type string|array $status                An array or a comma-separated list of status names that users must match
	 *                                             to be included in results. Note that this is an inclusive list: users
	 *                                             must match *each* status. Default empty.
	 *     @type array        $from_email__in            An array of status names. Matched users must have at least one of these
	 *                                             processes. Default empty array.
	 *     @type array        $from_email__not_in        
	 *                                             processes will not be included in results. Default empty array.
	 *     @type string       $meta_key            User meta key. Default empty.
	 *     @type string       $meta_value          User meta value. Default empty.
	 *     @type string       $meta_compare        Comparison operator to test the `$meta_value`. Accepts '=', '!=',
	 *                                             '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN',
	 *                                             'BETWEEN', 'NOT BETWEEN', 'EXISTS', 'NOT EXISTS', 'REGEXP',
	 *                                             'NOT REGEXP', or 'RLIKE'. Default '='.
	 *     @type array        $include             An array of user IDs to include. Default empty array.
	 *     @type array        $exclude             An array of user IDs to exclude. Default empty array.
	 *     @type string       $search              Search keyword. Searches for possible string matches on columns.
	 *                                             When `$search_columns` is left empty, it tries to determine which
	 *                                             column to search in based on search string. Default empty.
	 *     @type array        $search_columns      Array of column names to be searched. Accepts 'ID', 'login',
	 *                                             'nicename', 'email', 'url'. Default empty array.
	 *     @type string|array $orderby             Field(s) to sort the retrieved users by. May be a single value,
	 *                                             an array of values, or a multi-dimensional array with fields as
	 *                                             keys and orders ('ASC' or 'DESC') as values. Accepted values are
	 *                                             'ID', 'display_name' (or 'name'), 'include', 'user_login'
	 *                                             (or 'login'), 'user_nicename' (or 'nicename'), 'user_email'
	 *                                             (or 'email'), 'user_url' (or 'url'), 'user_registered'
	 *                                             or 'registered'), 'post_count', 'meta_value', 'meta_value_num',
	 *                                             the value of `$meta_key`, or an array key of `$meta_query`. To use
	 *                                             'meta_value' or 'meta_value_num', `$meta_key` must be also be
	 *                                             defined. Default 'user_login'.
	 *     @type string       $order               Designates ascending or descending order of users. Order values
	 *                                             passed as part of an `$orderby` array take precedence over this
	 *                                             parameter. Accepts 'ASC', 'DESC'. Default 'ASC'.
	 *     @type int          $offset              Number of users to offset in retrieved results. Can be used in
	 *                                             conjunction with pagination. Default 0.
	 *     @type int          $number              Number of users to limit the query for. Can be used in
	 *                                             conjunction with pagination. Value -1 (all) is supported, but
	 *                                             should be used with caution on larger sites.
	 *                                             Default empty (all users).
	 *     @type int          $paged               When used with number, defines the page of results to return.
	 *                                             Default 1.
	 *     @type bool         $count_total         Whether to count the total number of users found. If pagination
	 *                                             is not needed, setting this to false can improve performance.
	 *                                             Default true.
	 *     @type string|array $fields              Which fields to return. Single or all fields (string), or array
	 *                                             of fields. Accepts 'ID', 'display_name', 'user_login',
	 *                                             'user_nicename', 'user_email', 'user_url', 'user_registered'.
	 *                                             Use 'all' for all fields and 'all_with_meta' to include
	 *                                             meta fields. Default 'all'.
	 */
	public function prepare_query( $query = array() ) 
	{
		global $wpdb, $user_ID;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_emails', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );		
		
		$join = array();			

		if ( is_array( $qv['fields'] ) ) 
		{
			$qv['fields'] = array_unique( $qv['fields'] );

			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) 
			{	
				if ( $field == 'count' )
					$this->query_fields[] = "COUNT(*) AS count";
				elseif ( $field == 'total_sum' )				
					$this->query_fields[] = "SUM(".USAM_TABLE_EMAIL.".sum) AS sum";
				elseif ( $field == 'date_format_month' )			
					$this->query_fields[] = "DATE_FORMAT(".USAM_TABLE_EMAIL.".date_insert, '%Y-%m-01 00:00:00') AS date_format_month";		
				elseif ( $field == 'date_format_year' )				
					$this->query_fields[] = "DATE_FORMAT(".USAM_TABLE_EMAIL.".date_insert, '%Y-01-01 00:00:00') AS date_format_year";					
				else
				{
					$field = 'ID' === $field ? 'id' : sanitize_key( $field );
					$this->query_fields[] = USAM_TABLE_EMAIL.".$field";
				}
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 
		elseif ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_EMAIL.".*";		
		else 
		{
			$field = 'ID' === $qv['fields'] ? 'id' : sanitize_key( $qv['fields'] );
			$this->query_fields = USAM_TABLE_EMAIL.".$field";
		}

		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;

		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_EMAIL;
		$this->query_where = "WHERE 1=1";		
		
	// Обрабатывать другие параметры даты
		$date_parameters = array();
		if ( '' !== $qv['hour'] )
			$date_parameters['hour'] = $qv['hour'];

		if ( '' !== $qv['minute'] )
			$date_parameters['minute'] = $qv['minute'];

		if ( '' !== $qv['second'] )
			$date_parameters['second'] = $qv['second'];

		if ( $qv['year'] )
			$date_parameters['year'] = $qv['year'];

		if ( $qv['monthnum'] )
			$date_parameters['monthnum'] = $qv['monthnum'];

		if ( $qv['w'] )
			$date_parameters['week'] = $qv['w'];

		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];

		if ( $date_parameters ) 
		{
			$date_query = new USAM_Date_Query( array( $date_parameters ), USAM_TABLE_EMAIL.'.sent_at' );
			$this->query_where .= $date_query->get_sql();
		}	
		if ( ! empty( $qv['date_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_query'], USAM_TABLE_EMAIL.'.sent_at' );
			$this->query_where .= $this->date_query->get_sql();
		}		
		if ( ! empty( $qv['date_insert_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_payed_query'], USAM_TABLE_EMAIL.'.date_insert' );
			$this->query_where .= $this->date_query->get_sql();
		}
	
		// Parse and sanitize 'include', for use by 'orderby' as well as 'include' below.
		if ( ! empty( $qv['include'] ) )
			$include = wp_parse_id_list( $qv['include'] );
		else
			$include = false;
			
		$from = array();
		if ( isset( $qv['from'] ) ) 
		{
			if ( is_array( $qv['from'] ) ) 
				$from = $qv['from'];			
			elseif ( is_string( $qv['from'] ) && ! empty( $qv['from'] ) )
				$from = array_map( 'trim', explode( ',', $qv['from'] ) );
			elseif ( is_numeric( $qv['from'] ) )
				$from = $qv['from'];
		}
		$from_in = array();
		if ( isset( $qv['from_in'] ) ) {
			$from_in = (array) $qv['from_in'];
		}
		$from_not_in = array();
		if ( isset( $qv['from_not_in'] ) ) {
			$from_not_in = (array) $qv['from_not_in'];
		}
		if ( ! empty( $from ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".from_email IN ('".implode( "','",  $from )."')";		
		}		
		if ( ! empty($from_not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".from_email NOT IN ('".implode( "','",  $from_not_in )."')";
		}
		if ( ! empty( $from_in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".from_email IN ('".implode( "','",  $from_in )."')";
		}
		
		$folder = array();
		if ( isset( $qv['folder'] ) ) 
		{
			if ( is_array( $qv['folder'] ) ) 
				$folder = $qv['folder'];			
			elseif ( is_string( $qv['folder'] ) && ! empty( $qv['folder'] ) )
				$folder = array_map( 'trim', explode( ',', $qv['folder'] ) );
			elseif ( is_numeric( $qv['folder'] ) )
				$folder = $qv['folder'];
		}
		$folder_in = array();
		if ( isset( $qv['folder_in'] ) ) {
			$folder_in = (array) $qv['folder_in'];
		}
		$folder_not_in = array();
		if ( isset( $qv['folder_not_in'] ) ) {
			$folder_not_in = (array) $qv['folder_not_in'];
		}
		if ( ! empty( $folder ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".folder IN ('".implode( "','",  $folder )."')";		
		}		
		if ( ! empty($folder_not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".folder NOT IN ('".implode( "','",  $folder_not_in )."')";
		}
		if ( ! empty( $folder_in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".folder IN ('".implode( "','",  $folder_in )."')";
		}
		
			
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];			
			$ordersby_array = array();		
			foreach ( $groupby as $_value ) 
			{				
				switch ( $_value ) 
				{
					case 'day' :
						$ordersby_array[] = 'YEAR(sent_at)';
						$ordersby_array[] = 'MONTH(sent_at)';		
						$ordersby_array[] = 'DAY(sent_at)';						
					break;
					case 'month' :
						$ordersby_array[] = 'YEAR(sent_at)';
						$ordersby_array[] = 'MONTH(sent_at)';													
					break;
					case 'year' :
						$ordersby_array[] = 'YEAR(sent_at)';				
					break;					
					default:
						$ordersby_array[] = $_value;
				}		
			}	
		}		
		if ( ! empty($ordersby_array) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $ordersby_array );
		else
			$this->query_groupby = '';		
		
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{
			// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}

		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}
			
			if ( is_int( $_key ) ) 
			{	// Integer key means this is a flat array of 'orderby' fields.
				$_orderby = $_value;
				$_order = $order;
			} 
			else 
			{	// Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}
			$parsed = $this->parse_orderby( $_orderby );
			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = USAM_TABLE_EMAIL.'.'.$parsed . ' ' . $this->parse_order( $_order );
		}

		// If no valid clauses were found, order by id.
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = USAM_TABLE_EMAIL.".id $order";
		}
		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );

		// limit
		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) {
			if ( $qv['offset'] ) {
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			} else {
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		}

		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) 
		{
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )			
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'to', 'from_email', 'from_name', 'to_email', 'to_name', 'reply_to_email', 'reply_to_name', 'subject' ) );
			if ( ! $search_columns ) 
			{
				if ( is_numeric($search) )
					$search_columns = array('id' );				
				else
					$search_columns = array( 'to', 'from_email', 'from_name', 'to_email', 'to_name', 'reply_to_email', 'reply_to_name', 'subject' );
			}	
			$search_columns = apply_filters( 'usam_email_search_columns', $search_columns, $search, $this );

			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}

		if ( ! empty( $include ) ) 
		{	
			$ids = implode( ',', $include );
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".ID NOT IN ($ids)";
		}		
				
		if ( ! empty($qv['read']) ) 
		{			
			$read = (int)$qv['read'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".read=$read";
		}			
		if ( isset($qv['mailbox']) ) 
		{		
			$mailbox = (int)$qv['mailbox'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".mailbox_id='$mailbox'";
		}		
		else
		{
			$mailboxes = usam_get_mailboxes( array( 'fields' => 'id', 'user_id' => $user_ID ) );
			if ( empty($mailboxes) )
				$ids = 0;
			else
				$ids = implode( ',', $mailboxes );
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".mailbox_id IN ($ids)";
		}
		if ( isset($qv['object_id']) ) 
		{					
			$object_id = (int)$qv['object_id'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".object_id='$object_id'";
		}		
		if ( isset($qv['reply_message_id']) ) 
		{		
			$reply_message_id = (int)$qv['reply_message_id'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".reply_message_id='$reply_message_id'";
		}		
		if ( isset($qv['object_type']) ) 
		{		
			$object_type = $qv['object_type'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".object_type='$object_type'";
		}	
		if ( isset($qv['importance']) ) 
		{		
			$importance = (int)$qv['importance'];
			$this->query_where .= " AND ".USAM_TABLE_EMAIL.".importance='$importance'";
		}			
		do_action_ref_array( 'usam_pre_emails_query', array( &$this ) );
	}

	/**
	 * Execute the query, with the current variables.	
	 */
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;

		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";	
		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] && $qv['number'] != 1 ) 		
			$this->results = $wpdb->get_results( $this->request );		
		elseif ( $qv['number'] == 1 && 'all' !== $qv['fields'] )		
			$this->results = $wpdb->get_var( $this->request );
		elseif ( $qv['number'] == 1 && 'all' == $qv['fields'] )		
			$this->results = $wpdb->get_row( $this->request );
		else 
			$this->results = $wpdb->get_col( $this->request );
		
		if ( !$this->results )
			return;	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total = $wpdb->get_var( apply_filters( 'found_emails_query', 'SELECT FOUND_ROWS()' ) );		
		
		if ( 'all' == $qv['fields'] )
		{						
			if ( $qv['cache_results'] )
			{	
				if ( $qv['number'] == 1 )
					wp_cache_set( $this->results->id, (array)$this->results, 'usam_email' );	
				else
				{					
					foreach ( $this->results as $result ) 
					{
						wp_cache_set( $result->id, (array)$result, 'usam_email' );						
					}
				}	
			}
		}				
		if ( $qv['cache_attachments'] )
		{
			$ids = array();	
			foreach ( $this->results as $result ) 
			{
				$ids[] = $result->id; 					
			}	
			update_email_attachments_cache( $ids );
		}
		if ( $qv['cache_object'] )
		{
			$order_ids = array();	
			$company_ids = array();	
			$contact_ids = array();	
			foreach ( $this->results as $result ) 
			{
				switch ( $result->object_type ) 
				{
					case 'order' :
						$order_ids[] = $result->object_id; 	
					break;
					case 'company' :
						$company_ids[] = $result->object_id; 	
					break;
					case 'contact' :
						$contact_ids[] = $result->object_id; 	
					break;
				}								
			}	
			if ( !empty($order_ids) )
				usam_get_orders( array( 'include' => $order_ids) );
			if ( !empty($company_ids) )
				usam_get_companies( array( 'include' => $company_ids) );
			if ( !empty($contact_ids) )
				usam_get_contacts( array( 'include' => $contact_ids) );
			
			update_email_attachments_cache( $ids );
		}			
	}

	/**
	 * Retrieve query variable.
	 */
	public function get( $query_var ) 
	{
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}

	/**
	 * Set query variable.
	 */
	public function set( $query_var, $value ) {
		$this->query_vars[$query_var] = $value;
	}

	/**
	 * Used internally to generate an SQL string for searching across multiple columns
	 */
	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;

		foreach ( $cols as $col ) {
			if ( 'ID' == $col ) {
				$searches[] = $wpdb->prepare( USAM_TABLE_EMAIL.".$col = %s", $string );
			} else {
				$searches[] = $wpdb->prepare( USAM_TABLE_EMAIL.".$col LIKE %s", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}

	public function get_results() 
	{
		return $this->results;
	}

	public function get_total() 
	{
		return $this->total;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';

		if ( in_array( $orderby, array( 'from_email', 'from_name', 'to_email', 'to_name', 'subject', 'date_insert', 'sent_at', 'folder' ) ) )
		{
			$_orderby = $orderby;
		}	
		elseif ( 'ID' == $orderby || 'id' == $orderby ) 
		{
			$_orderby = 'id';
		} 		
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) )
		{
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_EMAIL.".id, $include_sql )";
		} 		
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) 
	{
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) 
	{
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments )
	{
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

function usam_get_emails( $args = array() )
{
	$args['count_total'] = false;
	$emails = new USAM_Email_Query( $args );	
	$result = $emails->get_results();	
	
	return $result;
}


// Кешировать
function update_email_attachments_cache( $object_ids )
{	
	$tables = array( USAM_TABLE_EMAIL_FILE => 'usam_email_attachments' );
	$column = 'email_id';
	return usam_update_cache( $object_ids, $tables, $column );	
}