<?php
// Класс работы с отзывами клиентов

class USAM_Customer_Reviews_Query 
{
	public $query_vars = array();
	private $results;
	private $total = 0;
	public $request;

	private $compat_fields = array( 'results', 'total' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_join;	
	public $query_where;
	public $query_orderby;
	public $query_groupby;	 
	public $query_limit;
	
	public $date_query;	
	
	public function __construct( $query = null ) 
	{
		require_once( USAM_FILE_PATH . '/includes/query/date.php' );
		if ( ! empty( $query ) ) 
		{
			$this->prepare_query( $query );
			$this->query();
		}
	}	

	public static function fill_query_vars( $args ) 
	{
		$defaults = array(
			'second' => '',
			'minute' => '',
			'hour' => '',
			'day' => '',
			'monthnum' => '',
			'year' => '',
			'w' => '',
			'status' => '',
			'status__in' => array(),
			'status__not_in' => array(),
			'user_id' => '',
			'user_id' => array(),
			'user_id__not_in' => array(),		
			'include' => array(),
			'exclude' => array(),
			'search' => '',
			'search_columns' => array(),
			'orderby' => 'id',
			'order' => 'ASC',
			'offset' => 0,
			'number' => 0,
			'paged' => 1,
			'count_total' => true,
			'fields' => 'all',				
		);
		return wp_parse_args( $args, $defaults );
	}
	
	public function prepare_query( $query = array() ) 
	{
		global $wpdb;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = $this->fill_query_vars( $query );
		}			
		do_action( 'usam_pre_get_customer_reviews', $this );
		
		$qv =& $this->query_vars;
		$qv =  $this->fill_query_vars( $qv );
		
		$qv['paged'] = (int)$qv['paged'];
		$qv['number'] = (int)$qv['number'];
		$qv['offset'] = (int)$qv['offset'];
				
		$join = array();			

		if ( is_array( $qv['fields'] ) ) 
		{
			$qv['fields'] = array_unique( $qv['fields'] );

			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) 
			{	
				$field = 'ID' === $field ? 'ID' : sanitize_key( $field );						
				$this->query_fields[] = USAM_TABLE_CUSTOMER_REVIEWS.".$field";				
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} 
		elseif ( 'all' == $qv['fields'] ) 		
			$this->query_fields = USAM_TABLE_CUSTOMER_REVIEWS.".*";		
		else 	
			$this->query_fields = USAM_TABLE_CUSTOMER_REVIEWS.".".$qv['fields'];

		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;
		
		$this->query_join = implode( ' ', $join );

		$this->query_from = "FROM ".USAM_TABLE_CUSTOMER_REVIEWS;
		$this->query_where = "WHERE 1=1";		
		
	// Обрабатывать другие параметры даты
		$date_parameters = array();
		if ( '' !== $qv['hour'] )
			$date_parameters['hour'] = $qv['hour'];

		if ( '' !== $qv['minute'] )
			$date_parameters['minute'] = $qv['minute'];

		if ( '' !== $qv['second'] )
			$date_parameters['second'] = $qv['second'];

		if ( $qv['year'] )
			$date_parameters['year'] = $qv['year'];

		if ( $qv['monthnum'] )
			$date_parameters['monthnum'] = $qv['monthnum'];

		if ( $qv['w'] )
			$date_parameters['week'] = $qv['w'];

		if ( $qv['day'] )
			$date_parameters['day'] = $qv['day'];

		if ( $date_parameters ) 
		{
			$date_query = new USAM_Date_Query( array( $date_parameters ), USAM_TABLE_CUSTOMER_REVIEWS.'.date_time' );
			$this->query_where .= $date_query->get_sql();
		}
		if ( ! empty( $qv['date_query'] ) ) 
		{
			$this->date_query = new USAM_Date_Query( $qv['date_query'], USAM_TABLE_CUSTOMER_REVIEWS.'.date_time' );
			$this->query_where .= $this->date_query->get_sql();
		}		
	
		if ( ! empty( $qv['include'] ) ) {
			$include = wp_parse_id_list( $qv['include'] );
		} else {
			$include = false;
		}
		$status = array();
		if ( isset( $qv['status'] ) ) 
		{
			if ( is_array( $qv['status'] ) ) 
				$status = $qv['status'];			
			elseif ( is_string( $qv['status'] ) && ! empty( $qv['status'] ) )
				$status = array_map( 'trim', explode( ',', $qv['status'] ) );
			elseif ( is_numeric($qv['status']) )
				$status = array( $qv['status'] );
		}
		$status__in = array();
		if ( isset( $qv['status__in'] ) ) {
			$status__in = (array) $qv['status__in'];
		}

		$status__not_in = array();
		if ( isset( $qv['status__not_in'] ) ) {
			$status__not_in = (array) $qv['status__not_in'];
		}

		if ( ! empty( $status ) ) 
		{	
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".status IN ('".implode( "','",  $status )."')";
		}		
		if ( ! empty($status__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".status NOT IN ('".implode( "','",  $status__not_in )."')";
		}
		if ( ! empty( $status__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".status IN ('".implode( "','",  $status__in )."')";
		}
		$user_id = array();
		if ( !empty( $qv['user_id'] ) ) 
		{
			if ( is_array( $qv['user_id'] ) ) 
				$user_id = $qv['user_id'];			
			elseif ( is_string( $qv['user_id'] ) )
				$user_id = array_map( 'trim', explode( ',', $qv['user_id'] ) );
			else
				$user_id = array( $qv['user_id'] );	
		}
		$user_id__in = array();
		if ( isset( $qv['user_id__in'] ) ) {
			$user_id__in = (array) $qv['user_id__in'];
		}
		$user_id__not_in = array();
		if ( isset( $qv['user_id__not_in'] ) ) {
			$user_id__not_in = (array) $qv['user_id__not_in'];
		}
		if ( ! empty( $user_id ) ) 
		{
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".user_id IN (".implode( ',',  $user_id ).")";
		}		
		if ( ! empty($user_id__not_in) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".user_id NOT IN (".implode( ',',  $user_id__not_in ).")";
		}
		if ( ! empty( $user_id__in ) ) 
		{			
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".user_id IN (".implode( ',',  $user_id__in ).")";
		}		
		// Группировать
		$qv['groupby'] = isset( $qv['groupby'] ) ? $qv['groupby'] : '';
		
		if ( $qv['groupby'] != '' )
		{
			if ( is_array($qv['groupby']) )					
				$groupby = $qv['groupby'];					
			else
				$groupby[] = $qv['groupby'];
			$ordersby_array = array();		
			foreach ( $groupby as $_value ) 
			{				
				switch ( $_value ) 
				{
					case 'day' :
						$ordersby_array[] = 'DAY(date_time)';		
						$ordersby_array[] = 'MONTH(date_time)';	
						$ordersby_array[] = 'YEAR(date_time)';						
					break;
					case 'month' :
						$ordersby_array[] = 'MONTH(date_time)';	
						$ordersby_array[] = 'YEAR(date_time)';						
					break;
					case 'year' :
						$ordersby_array[] = 'YEAR(date_time)';				
					break;
					default:
						$ordersby_array[] = $_value;
				}		
			}	
		}		
		if ( ! empty($ordersby_array) )
			$this->query_groupby = 'GROUP BY ' . implode( ', ', $ordersby_array );
		else
			$this->query_groupby = '';
				
		// СОРТИРОВКА
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) 
		{	// Default order is by 'id'.			
			$ordersby = array( 'id' => $order );
		} 
		elseif ( is_array( $qv['orderby'] ) ) 
		{
			$ordersby = $qv['orderby'];
		} 
		else 
		{
			// Значения 'orderby' могут быть списком, разделенным запятыми или пробелами
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}
		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) 
		{
			if ( ! $_value ) {
				continue;
			}			
			if ( is_int( $_key ) )
			{
				$_orderby = $_value;
				$_order = $order;
			} 
			else 
			{ // Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}

			$parsed = $this->parse_orderby( $_orderby );

			if ( ! $parsed ) {
				continue;
			}
			$orderby_array[] = USAM_TABLE_CUSTOMER_REVIEWS.'.'.$parsed . ' ' . $this->parse_order( $_order );
		}

		// If no valid clauses were found, order by id.
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = USAM_TABLE_CUSTOMER_REVIEWS.".id $order";
		}

		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );
		// limit
		if ( isset( $qv['number'] ) && $qv['number'] > 0 ) 
		{
			if ( $qv['offset'] ) 			
				$this->query_limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			else 
			{				
				$this->query_limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
			}
		}
		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) {
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )
				$search_columns = array_intersect( $qv['search_columns'], array( 'id', 'user_email', 'user_nicename' ) );
			if ( ! $search_columns ) 
			{
				if ( false !== strpos( $search, '@') )
					$search_columns = array('user_email');
				elseif ( is_numeric($search) )
					$search_columns = array('id', 'user_login' );				
				else
					$search_columns = array('id', 'user_email', 'user_nicename', 'display_name');
			}	
			$search_columns = apply_filters( 'usam_customer_search_columns', $search_columns, $search, $this );

			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}

		if ( ! empty( $include ) ) 
		{
			// Sanitized earlier.
			$ids = implode( ',', $include );
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".ID IN ($ids)";
		} 
		elseif ( ! empty( $qv['exclude'] ) ) 
		{
			$ids = implode( ',', wp_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".ID NOT IN ($ids)";
		}
		if ( isset( $qv['page_id'] ) && $qv['page_id'] != '' ) 
		{		
			$page_id = implode( ',',  (array)$qv['page_id'] );		
			$this->query_where .= " AND ".USAM_TABLE_CUSTOMER_REVIEWS.".page_id IN ($page_id)";
		}			
		do_action_ref_array( 'usam_precustomer_reviews_query', array( &$this ) );
	}
	
	public function query()
	{
		global $wpdb;

		$qv =& $this->query_vars;

		$this->request = "SELECT $this->query_fields $this->query_from $this->query_join $this->query_where $this->query_groupby $this->query_orderby $this->query_limit";

		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] && $qv['number'] != 1 ) 		
			$this->results = $wpdb->get_results( $this->request );		
		elseif ( $qv['number'] == 1 && 'all' !== $qv['fields'] )		
			$this->results = $wpdb->get_var( $this->request );
		elseif ( $qv['number'] == 1 && 'all' == $qv['fields'] )		
			$this->results = $wpdb->get_row( $this->request );
		else 
			$this->results = $wpdb->get_col( $this->request );
		
		if ( !$this->results )
			return;	
		
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total = $wpdb->get_var( 'SELECT FOUND_ROWS()' );		

		if ( isset( $qv['cache_results'] ) && $qv['cache_results'] && 'all' == $qv['fields'] )
		{					
			if ( $qv['number'] == 1 )
				wp_cache_set( $this->results->id, (array)$this->results, 'usam_review' );	
			else
			{					
				foreach ( $this->results as $result ) 
				{
					wp_cache_set( $result->id, (array)$result, 'usam_review' );						
				}
			}
		}		
	}
	
	public function get( $query_var ) 
	{
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}
	
	public function set( $query_var, $value )
	{
		$this->query_vars[$query_var] = $value;
	}

	/**
	 * Used internally to generate an SQL string for searching across multiple columns
	 */
	protected function get_search_sql( $string, $cols, $wild = false ) 
	{
		global $wpdb;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $wpdb->esc_like( $string ) . $trailing_wild;

		foreach ( $cols as $col ) {
			if ( 'ID' == $col ) {
				$searches[] = $wpdb->prepare( "$col = %s", $string );
			} else {
				$searches[] = $wpdb->prepare( "$col LIKE %s", $like );
			}
		}
		return ' AND (' . implode(' OR ', $searches) . ')';
	}
	
	public function get_results() 
	{
		return $this->results;
	}

	/**
	 * Return the total number of users for the current query.
	 */
	public function get_total() 
	{
		return $this->total;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 */
	protected function parse_orderby( $orderby ) 
	{
		global $wpdb;

		$_orderby = '';			
		if ( 'ID' == $orderby ) 
		{
			$_orderby = 'id';
		} 	
		elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) ) 
		{
			$include = wp_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( ".USAM_TABLE_CUSTOMER_REVIEWS.".id, $include_sql )";
		} 
		else 
		{
			$_orderby = $orderby;
		} 
		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 */
	protected function parse_order( $order ) {
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 */
	public function __get( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 */
	public function __isset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 */
	public function __unset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 */
	public function __call( $name, $arguments ) {
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

function usam_get_customer_reviews( $query = array() )
{	
	$query['count_total'] = false;
	$reviews = new USAM_Customer_Reviews_Query( $query );	
	return $reviews->get_results();	
}

function usam_total_reviews( $page_id = null )
{	
	if ( $page_id == null )
	{
		global $post;
		$page_id = $post->ID;
	}
	$query = array( 'page_id' => $page_id, 'status' => 1 );
	
	$reviews = new USAM_Customer_Reviews_Query( $query );	
	$reviews->get_results();
	return $reviews->get_total();
}

function usam_get_aggregate_reviews( $page_id )
{
	global $wpdb;
	$page_id = intval($page_id);
	$row = $wpdb->get_results("SELECT COUNT(*) AS `total`,AVG(rating) AS `aggregate_rating`,MAX(rating) AS `max_rating` FROM `".USAM_TABLE_CUSTOMER_REVIEWS."` WHERE `page_id`=$page_id AND `status`=1");
	
	if ($wpdb->num_rows == 0 || $row[0]->total == 0) 
		return array( "aggregate" => 0, "max" => 0, "total" => 0 );
	else
		return array("aggregate" => round($row[0]->aggregate_rating,1), "max" => $row[0]->max_rating, "total" => $row[0]->total );
}	