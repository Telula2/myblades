<?php
/*
* Description: Обратная связь.
*/
class USAM_Contactform
{
	private $fields;
	public function __construct()
	{
		$current_user = wp_get_current_user();	
		if (isset($_POST['usam_your_name'])) 
			$usam_your_name = sanitize_text_field($_POST['usam_your_name']);
		else 
			$usam_your_name = $current_user->user_login; 
		if (isset($_POST['usam_email'])) 
			$usam_email = sanitize_text_field($_POST['usam_email']);
		else 
			$usam_email = $current_user->user_email;
		if (isset($_POST['usam_cols'])) 
			$usam_cols = sanitize_text_field($_POST['usam_cols']); 
		else 
			$usam_cols = ''; 
		if (isset($_POST['usam_rows'])) 
			$usam_rows = sanitize_text_field($_POST['usam_rows']);
		else 
			$usam_rows = ''; 
		if (isset($_POST['usam_msg'])) 
			$usam_msg = sanitize_text_field($_POST['usam_msg']);
		else 
			$usam_msg = ''; 
		if (isset($_POST['usam_usersubject'])) 
			$usam_usersubject = sanitize_text_field($_POST['usam_usersubject']);
		else 
			$usam_usersubject = ''; 
		if (isset($_POST['usam_response'])) 
			$usam_response = sanitize_text_field($_POST['usam_response']);
		else 
			$usam_response = '';	
			
		$this->fields=array(
			'name'=>'<input class="field" type="text" name="usam_your_name" id="usam_your_name" maxlength="50" tabindex="11" value="'.htmlentities($usam_your_name).'" />',
			'email'=>'<input class="field" type="text" name="usam_email" id="usam_email" maxlength="50" tabindex="12" value="'.htmlentities($usam_email).'" />',
			'response'=>'<input class="field" type="text" name="usam_response" id="usam_response" maxlength="50" tabindex="13" value="'.htmlentities($usam_response).'" />',	
			'usersubject'=>'<input class="field" type="text" name="usam_usersubject" id="usam_usersubject" tabindex="15" maxlength="50" value="'.htmlentities($usam_usersubject).'" />',
			'msg'=>'<textarea name="usam_msg" id="usam_msg" cols="'.htmlentities($usam_cols).'" rows="'.htmlentities($usam_rows).'" tabindex="16">'.htmlentities($usam_msg).'</textarea>',
			'error'=>''
		);			
	}
	
	function is_malicious( $input ) 
	{		
		$bad_inputs = array("\r", "mime-version", "content-type", "cc:", "to:");
		foreach ($bad_inputs as $bad_input) 
		{
			if (strpos(strtolower($input), strtolower($bad_input)) !== false)
				return true;			
		}
		return false;
	}
	
	private function check_input() 
	{
		if (isset($_POST['process_contactform'])) 
		{
			$reason = 'ok';
			if (empty($_POST['usam_your_name'])) 
			{
				$usam_your_name = stripslashes(trim($_POST['usam_your_name']));
				$reason='empty';
				$this->fields['name']='<input type="text" name="usam_your_name" class="field error" id="usam_your_name" maxlength="50" tabindex="11" value="'.htmlentities($usam_your_name).'" class="contacterror" />';
			}	
			elseif ( $this->is_malicious($_POST['usam_your_name']) )
				$reason='malicious';
			if ( !isset ($_POST['usam_email']) || (!is_email(stripslashes(trim($_POST['usam_email']))))) 
			{			
				$usam_email = stripslashes(trim($_POST['usam_email']));
				$reason='empty';			
				$this->fields['email']='<input type="text" name="usam_email" class="field error" id="usam_email" maxlength="50" tabindex="12" value="'.htmlentities($usam_email).'" class="contacterror" />';
			}	
			elseif ( $this->is_malicious($_POST['usam_email']) )
				$reason='malicious';			
			if ( empty($_POST['usam_usersubject']) ) 
			{
				$usam_usersubject = stripslashes(trim($_POST['usam_usersubject']));
				$reason = 'empty';
				$this->fields['usersubject']='<input type="text" name="usam_usersubject" class="field error" id="usam_usersubject" tabindex="15" maxlength="50" value="'.htmlentities($usam_usersubject).'" class="contacterror" />';
			}
			if (empty($_POST['usam_msg'])) 
			{
				$usam_msg = stripslashes(trim($_POST['usam_msg']));
				if ( !empty ($_POST['usam_cols']) )
					$usam_cols = stripslashes(trim($_POST['usam_cols']));
				else 
					$usam_cols = '';
				if ( !empty ($_POST['usam_rows']) )
					$usam_rows = stripslashes(trim($_POST['usam_rows']));
				else
					$usam_rows = '';
				$reason = 'empty';
				$this->fields['msg']='<textarea name="usam_msg" class="error" id="usam_message" tabindex="16" cols="'.htmlentities($usam_cols).'" rows="'.htmlentities($usam_rows).'" class="contacterror">'.htmlentities($usam_msg).'</textarea>';
			}
			switch( $reason )
			{					
				case 'malicious':					
					$this->fields['error']='<p class=\"alert\">'.__("Вы не можете использовать любое из следующих действий в имени или адресе электронной почты: разрыв строки или фразы &acute;mime-version&acute;,&acute;content-type&acute;,&acute;cc:&acute; или &acute;to:&acute;","shop").'</p>';	
				break;
				case 'empty':					
					$this->fields['error']='<p class="alert">'.__('Пожалуйста, заполните необходимые поля', 'shop').'.</p>';				
				break;
				case 'wrong':					
					$this->fields['error']='<p class=\"alert\">'.__('Вы ответили на контрольный вопрос неправильно.','shop').'</p>';
				break;
				case 'ok':					
					return true;
				break;
			}			
		}
		return false;		
	}	
	
	public function display() 
	{
		if ( $this->check_input() ) 
		{				
			$subject = sanitize_text_field($_POST['usam_usersubject']);			
			$name = sanitize_text_field($_POST['usam_your_name']);
			$email = sanitize_text_field($_POST['usam_email']);
		
			$message      = sanitize_textarea_field($_POST['usam_msg']);			
			$message      = wordwrap($message, 80, "\n")."\n\n";	
						
			$bd_message   = $subject."<br/>".$message;
			
			global $user_ID;			
			$data = array( 'user_id' => $user_ID, 'type_message' => 'contact-form', 'mail' => $email, 'name' => $name, 'message' => $bd_message );
			$data['location_id'] = usam_get_user_location( $user_ID );		
			usam_insert_feedback( $data );		
	
			if( get_option('usam_email_contactform_copy', 0) == 1 ) 
			{						
				usam_mail($email, $subject, $message);				
			}
			$form = '<div style="font-weight: bold;">'.__('Спасибо за ваши сообщение!', 'shop').'</div>';			
		} 
		else 
		{
			$question = stripslashes(get_option('usam_question'));	
			
			$form  = '<div class="usam_box">';
			$form .= '<div id="wps">';		
			$form .= '<form action="'.get_permalink().'" name="usam_form" method="post">';
			$form .='<p class="usam_header">'.__('Отправьте нам сообщение', 'shop').'</p>';
			$form .= '<input type="hidden" name="process_contactform" value="true"/>';		
			$form .= '<p><label for="usam_your_name">'. __('Ваше имя:','shop').'<strong style="color: #ff6c36">*</strong>'.'</label>';
			$form .= $this->fields['name'].'</p>';
			$form .= '<p><label for="usam_email">'. __('Ваш E-mail:','shop').'<strong style="color: #ff6c36">*</strong>'.'</label>';
			$form .= $this->fields['email'].'</p>';								
			$form .= '<p><label for="usam_usersubject">'. __('Тема сообщения:','shop').'*</label>';
			$form .= $this->fields['usersubject'];
			$form .= '</p>';
			$form .= '<p><label for="usam_msg">'. __('Ваше сообщение:','shop').'<strong style="color: #ff6c36">*</strong>'.'</label>';
			$form .= $this->fields['msg'].'</p>';			
			$form .= '<p class="title"> '.__("Поля, отмеченные","shop"). '<strong style="color: #ff6c36"> *</strong>, '.__("подлежат обязательному заполнению","shop").'</p>';		
			$form .= '<p class="button_box"><input class="button" type="submit" value="'. __('Отправить','shop').'" tabindex="18" name="Submit"/></p>';
			$form .= $this->fields['error'];			
			$form .= '</form></div></div>';
		}
		return $form;	
	}
}	
?>