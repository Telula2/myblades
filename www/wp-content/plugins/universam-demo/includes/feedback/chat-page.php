<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" <?php do_action('admin_xml_ns'); ?> <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" <?php do_action('admin_xml_ns'); ?> <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php echo get_option('blog_charset'); ?>" />
<title><?php esc_html_e( 'Консультант', 'usam' ); ?></title>
<script type="text/javascript">

</script>
<?php
	do_action("wp_enqueue_scripts-$hook_suffix");
//	do_action('wp_enqueue_scripts');
?>
<style type="text/css">
	html { background-color:transparent; }	
	body {background: #ffffff; width: 100%; height: 100%; font-family: 'Open Sans', sans-serif; font-weight: 400; font-smoothing: antialiased; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); position: relative; margin: 0px;}
</style>
</head>
<body class="no-js wp-admin wp-core-ui usam-product-variation-iframe">
	<script type="text/javascript">document.body.className = document.body.className.replace('no-js','js');</script>
	<div id="usam_form_dialog">				
		<div class = "usam_dialog_box">		
			<div class="usam_dialog_header">
				<div class="name_chat">
					<?php _e('Ваш личный консультант','usam'); ?>	
				</div>				
				<div class="icons">
					<div class="icon icon-close" style="">Х</div>
				</div>
			</div>
			<?php 					
			if ( $manager_id ) 
			{
				$manager = get_user_by('id', $manager_id );				
				?>	
				<div class="body">							
					<div class="user-bar">
						<div class="photo noav" style=""><?php echo get_avatar( $manager_id, 32 ); ?></div>
						<div class="info" style="width: 129px;">
							<div class="name"><?php echo $manager->display_name; ?></div>
								<div class="title"><?php _e('консультант','usam'); ?></div>
								<div class="stars">
								<div class="rate">
									<div class="star icon-rate"></div> 
									<div class="star icon-rate"></div>
									<div class="star icon-rate"></div> 
									<div class="star icon-rate"></div>
									<div class="star icon-rate"></div> 
								</div>
							</div>
						</div> 
						<div class="menu-button icon-menu"></div>
						<div class="clear"></div>
					</div>	
					<div class="user_messages">						
						<?php				
						$chat = new USAM_Chat_Template();
						$chat->display_dialog_message( $dialog_id );				
						?>	
					</div>								
				</div>
				<div class="footer">				
					<div class="compose">
						<div id = "im_controls_wrap" class="input-block freeze">
							<textarea id ="message" class="client-text" placeholder="введите сообщение и нажмите Enter..."></textarea>
						</div> 
					</div>									
				</div>
				<?php 				
			}
			else
			{
				?><div class="body"><?php	_e('Нет свободных сотрудников готовых помочь вам. Попробуйте позже...','usam'); ?></div><?php
			}	
			?>	
		</div>
	</div>
	<?php
	do_action('wp_print_footer_scripts');
	do_action("wp_print_footer_scripts-" . $GLOBALS['hook_suffix']);
	?>
	<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>
</body>
</html>