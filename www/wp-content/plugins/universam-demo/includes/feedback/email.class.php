<?php
require_once( USAM_FILE_PATH . '/includes/mailings/email_folder.class.php' );
require_once( USAM_FILE_PATH . '/includes/mailings/email_folders_query.class.php' );
/**
 * Работа с электронными письмами
 */ 
class USAM_Email
{
	 // строковые
	private static $string_cols = array(
		'reply_to_email',	
		'reply_to_name',
		'copy_email',		
		'to_email',	
		'to_name',
		'from_name',
		'from_email',
		'subject',				
		'date_insert',
		'sent_at',			
		'folder',		
		'body',	
		'opened_at',	
		'object_type',		
	);
	// цифровые
	private static $int_cols = array(
		'id',	
		'read',	
		'mailbox_id',	
		'server_message_id',	
		'object_id',	
		'reply_message_id',		
		'importance',		
	);
	// рациональные
	private static $float_cols = array(
		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		
	private $previous_folder   = '';	
	private $is_folder_changed = false;		
	private $previous_read     = '';	
	private $is_status_read    = false;	
	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;		
					
		$this->args = array( 'col' => $col, 'value' => $value );			
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_email' );
		}		
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_email ) 
	{
		$id = $_email->get( 'id' );	
		wp_cache_set( $id, $_email->data, 'usam_email' );		
		do_action( 'usam_email_update_cache', $_email );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$_email = new USAM_Email( $value, $col );
		wp_cache_delete( $_email->get( 'id' ), 'usam_email' );	
		wp_cache_delete( $_email->get( 'id' ), 'usam_email_attachments' );			
		do_action( 'usam_email_delete_cache', $_email, $value, $col );	
	}		
	
	/**
	 *  Удалить документ отгрузки
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_email_before_delete', $this );
		
		$id = $this->get( 'id' );
					
		self::delete_cache( $id );			
		
		$attachments = $this->get_attachments();
		$dir = USAM_UPLOAD_DIR."e-mails/$id/";
		usam_remove_dir( $dir );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_EMAIL_FILE." WHERE email_id = '$id'");
		
		$read = $this->get( 'read' );		
		$email_folder = $this->get('folder');
		$mailbox_id = $this->get('mailbox_id');
		$folder = usam_get_email_folders( array('slug' => $email_folder, 'mailbox_id' => $mailbox_id, 'number' => 1) );			
		$count = $folder->count - 1;								
		$update = array( 'count' => $count );
		if ( $read == 1 )
			$update['not_read'] = $folder->not_read - 1;		
		
		$mailbox = usam_get_mailbox( $mailbox_id );		
		if ( $mailbox['delete_server_deleted'] )		
		{
			$server_message_id = $this->get('server_message_id');
			$pop3 = new USAM_POP3( $mailbox_id );
			$result = $pop3->delete_message( $server_message_id );			
		}		
		usam_update_email_folder( $folder->id, $update );	
						
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_EMAIL." WHERE id = '$id'");
		
		do_action( 'usam_email_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_EMAIL." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_email_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_email_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_email_get_property', $value, $key, $this );
	}	
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_email_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		if ( array_key_exists( 'folder', $properties ) ) 
		{	
			$this->previous_folder = $this->get( 'folder' );
			if ( $properties['folder'] != $this->previous_folder )
				$this->is_folder_changed = true;			
		}
		if ( array_key_exists( 'read', $properties ) ) 
		{	
			$this->previous_read = $this->get( 'read' );
			if ( $properties['read'] != $this->previous_read )
				$this->is_status_read = true;			
		}
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_email_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	public function get_attachments( $type = 'U' ) 
	{		
		$email_id = $this->get('id');	
		
		$cache_key = 'usam_email_attachments';
		if( ! $data = wp_cache_get($cache_key, $email_id ) )
		{				
			global $wpdb;
			$format = self::get_column_format( 'id' );
			
			$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_EMAIL_FILE." WHERE email_id = '{$format}' AND type = '$type'", $email_id );
			$data = $wpdb->get_results( $sql, ARRAY_A );			
			wp_cache_set( $cache_key, $data, $email_id );
		}	
		return apply_filters( $cache_key, $data );									
	}

	public function set_attachments( $attachments ) 
	{		
		global $wpdb;
		
		$id = $this->get('id');
		if ( empty($id) || empty($attachments) )
			return false;
		
		$dir = USAM_UPLOAD_DIR."e-mails/$id/";
		if( !is_dir($dir) )
		{
			if (!mkdir($dir, 0777, true)) 
			{
				return false;
			}
		}			
		foreach ( $attachments as $attachment ) 
		{						
			if ( !empty($attachment['file_path']) )
			{ 
				if ( !file_exists($attachment['file_path'])) 
					continue;
		
				$filename = basename($attachment['file_path']);				
				$title = !empty($attachment['title'])?$attachment['title']:$filename;				
				$type = 'U';
				
				$file_path = $dir.$filename;	
				if ( !copy($attachment['file_path'], $file_path) )										
					continue;		
				if ( !empty($attachment['delete']) ) 	
					unlink($attachment['file_path']);							
			}
			else
			{				
				if ( empty($attachment['filename']) )
					$attachment['filename'] = 'no_name';
				
				if ( empty($attachment['file']) )
					continue;	

				$title = !empty($attachment['title'])?$attachment['title']:$attachment['filename'];					
						
				$type = $attachment['type'];
				$filename = sanitize_file_name(usam_sanitize_title_with_translit( $attachment['filename'] ) );				
				$file_path = usam_get_filename( $filename, $dir );
				
				file_put_contents($file_path, $attachment['file'] ); 	
			}	
			$filetype = wp_check_filetype( $file_path );
			$mime_type = $filetype['type'];			

			$insert = array( 'email_id' => $id, 'title' => $title, 'name' => $filename, 'type' => $type, 'mime_type' => $mime_type );
			$format = array( '%d', '%s', '%s', '%s', '%s' );
			$result = $wpdb->insert( USAM_TABLE_EMAIL_FILE, $insert, $format );					
		}		
	}	
			
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_email_pre_save', $this );	
		$where_col = $this->args['col'];	
		
		if ( isset($this->data['sent_at']) )
			$this->data['sent_at'] = date( "Y-m-d H:i:s", strtotime($this->data['sent_at']) );			
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	

			if ( isset($this->data['date_insert']) )
				unset($this->data['date_insert']);
				
			do_action( 'usam_email_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );		

			$this->data = apply_filters( 'usam_email_update_data', $this->data );			
			$format = $this->get_data_format( );						
	
			$result = $wpdb->update( USAM_TABLE_EMAIL, $this->data, array( $where_col => $where_val ), $format, $where_format );
			if ( $result )
			{	
				$mailbox_id = $this->get('mailbox_id');
				if ( $mailbox_id && ( $this->is_status_read || $this->is_folder_changed ) )
				{
					$email_folder = $this->get('folder');
					$folder = usam_get_email_folders( array('slug' => $email_folder, 'mailbox_id' => $mailbox_id, 'number' => 1) );								
					if ( $this->is_folder_changed )
					{								
						$read = $this->get('read');	
						$count = $folder->count + 1;								
						$update = array( 'count' => $count );
						if ( $read == 0 )
							$update['not_read']++;	
						
						usam_update_email_folder( $folder->id, $update );	
						
						$previous_folder = usam_get_email_folders( array('slug' => $this->previous_folder, 'mailbox_id' => $mailbox_id, 'number' => 1) );	
						$count = $previous_folder->count - 1;								
						$update = array( 'count' => $count );
						if ( $this->is_status_read && $this->previous_read == 0 || $read == 0 )
							$update['not_read'] = $previous_folder->not_read - 1;						
					
						usam_update_email_folder( $previous_folder->id, $update );	
					}
					elseif ( $this->is_status_read )
					{								
						$update = array( );
						if ( $this->data['read'] == 1 )
							$update['not_read'] = $folder->not_read - 1;	
						else
							$update['not_read'] = $folder->not_read + 1;						
						usam_update_email_folder( $folder->id, $update );	
					}				
				}				
			}
			do_action( 'usam_email_update', $this );
		} 
		else 
		{   
			do_action( 'usam_email_pre_insert' );		
			unset( $this->data['id'] );	
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );	
			
			if ( !isset($this->data['from_email']) )
				$this->data['from_email'] = '';			
			if ( !isset($this->data['folder']) )
				$this->data['folder'] = 'drafts';		
			if ( !isset($this->data['from_name']) )
				$this->data['from_name'] = '';		
			if ( !isset($this->data['reply_to_email']) )
				$this->data['reply_to_email'] = '';	
			if ( !isset($this->data['reply_to_name']) )
				$this->data['reply_to_name'] = '';			
			if ( !isset($this->data['to_email']) )
				$this->data['to_email'] = '';	
			if ( !isset($this->data['to_name']) )
				$this->data['to_name'] = '';
			if ( !isset($this->data['subject']) )
				$this->data['subject'] = '';	
			if ( !isset($this->data['body']) )
				$this->data['body'] = '';				
			if ( !isset($this->data['read']) )
				$this->data['read'] = 0;	
			if ( !isset($this->data['mailbox_id']) )
				$this->data['mailbox_id'] = 0;					
			if ( !isset($this->data['server_message_id']) )
				$this->data['server_message_id'] = 0;					
			if ( !isset($this->data['reply_message_id']) )
				$this->data['reply_message_id'] = 0;				
			
			if ( !empty($this->data['mailbox_id']) )
			{
				$mailbox = usam_get_mailbox( $this->data['mailbox_id'] );
				if ( !empty($mailbox) )
				{
					if ( $this->data['server_message_id'] )
					{
						$this->data['to_name'] = $mailbox['name'];
						$this->data['to_email'] = $mailbox['email'];
					}
					else
					{
						$this->data['from_name'] = $mailbox['name'];
						$this->data['from_email'] = $mailbox['email'];
					}
				}
			}
			else
				$this->data['mailbox_id'] = 0;	
			
			$this->data = apply_filters( 'usam_email_insert_data', $this->data );			
			$format = $this->get_data_format(  );			
			$result = $wpdb->insert( USAM_TABLE_EMAIL, $this->data, $format );
							
			if ( $result ) 
			{					
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );	
				if ( $this->data['mailbox_id'] )
				{
					$folder = usam_get_email_folders( array('slug' => $this->data['folder'], 'mailbox_id' => $this->data['mailbox_id'], 'number' => 1 ));											
					$update = array( 'count' => $folder->count + 1 );
					if ( $this->data['read'] == 0 )
						$update['not_read'] = $folder->not_read + 1;
					
					usam_update_email_folder( $folder->id, $update );					
				}
			}
			do_action( 'usam_email_insert', $this );
		} 		
		do_action( 'usam_email_save', $this );

		return $result;
	}
	
	public function send_mail()
	{		
		$mail_id = $this->get('id');		
		$subject = $this->get('subject');
		$message = $this->get('body');
		$to_email = $this->get('to_email');
		
		$from_name = $this->get('from_name');
		$from_email = $this->get('from_email');
		$reply_to_name = $this->get('reply_to_name');
		$reply_to_email = $this->get('reply_to_email');		
		$copy_email = $this->get('copy_email');
			
		$headers[] = 'From: '.$from_name.' <'.$from_email.'>';
		$headers[] = 'content-type: text/html';		
		
		if ( !empty($copy_email) )
			$headers[] = "Cc: $copy_email";		
		
		if ( !empty($reply_to_email) )
			$headers[] = "Reply-to: $reply_to_email";	

		$object_type = $this->get('object_type');
		$object_id = $this->get('object_id');
		
		$url = get_bloginfo( 'url' );		
		$open_link = add_query_arg(array('mail_id' => $mail_id, 'usam_action' => 'email_open'), $url );
		$message .= '<br><br><br><img style="display:none;width:1px;height:1px;" src="'.$open_link.'"/>';			
		$message .= '<span id = "object_type" style="display:none;font-size:1px;">###'.$object_type.'-'.$object_id.'###</span>';
		$message .= '<span id = "mail_id" style="display:none;font-size:1px;">%%%'.$mail_id.'%%%</span>';

		$attachments = $this->get_attachments();		
		$dir = USAM_UPLOAD_DIR."e-mails/$mail_id/";

		$attachments_filepath = array();
		foreach ( $attachments as $attachment )
		{
			$attachments_filepath[] = $dir.$attachment['name'];
			
		}		//$to_email	получатель письма
		$email_sent = wp_mail( $to_email, $subject, $message, $headers, $attachments_filepath );
		
		if ( $email_sent )
		{ 
			$insert_email['folder'] = 'sent';
			$insert_email['sent_at'] = date( "Y-m-d H:i:s");
		}
		else
			$insert_email['folder'] = 'outbox';
		$insert_email['server_message_id'] = 0;
		$insert_email['read'] = 1;
		
		$this->set( $insert_email );
		$this->save();	
		return $email_sent;
	}
}

// Обновить 
function usam_update_email( $id, $data )
{
	$_email = new USAM_Email( $id );
	$_email->set( $data );
	return $_email->save();
}

// Получить 
function usam_get_email( $id, $colum = 'id' )
{
	$_email = new USAM_Email( $id, $colum );
	$email_data = $_email->get_data( );	
	
	if ( !empty($email_data['body']) )
	{
		$dir = USAM_UPLOAD_URL."e-mails/".$email_data['id']."/";
		if( preg_match_all('/src="(.[^\s]*)"/i', $email_data['body'], $regs, PREG_SET_ORDER ) ) 		
		{	
			$replacement = array();
			foreach ( $regs as $reg ) 
			{	
				if ( !in_array($reg[1], $replacement) && !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $reg[1]) )	
				{
					$replacement[] = $reg[1];
					$email_data['body'] = str_replace($reg[1], $dir.$reg[1], $email_data['body'] );
				}				
			}
		}	
	}			
	return $email_data;	
}

// Вставить 
function usam_insert_email( $data )
{
	if ( isset($data['attachments']) )
	{
		$attachments = $data['attachments'];
		unset($data['attachments']);
	}
	$_email = new USAM_Email( $data );
	$_email->save();
	
	if ( !empty($attachments) )
		$_email->set_attachments( $attachments );
	
	return $_email->get('id');
}

function usam_insert_email_attachments( $id, $attachments  )
{
	$_email = new USAM_Email( $id );
	return $_email->set_attachments( $attachments );
}

// Получить вложения
function usam_get_attachments( $id )
{
	$_email = new USAM_Email( $id );
	return $_email->get_attachments();
}

// Удалить 
function usam_delete_email( $id )
{
	$_email = new USAM_Email( $id );
	return $_email->delete();
}

function usam_send_mails( $mailbox_id = '' ) 
{				
	$args = array( 'cache_results' => true, 'cache_attachments' => false, 'folder' => 'outbox', 'mailbox_id' => $mailbox_id, 'number' => 100, 'order' => 'ASC', 'orderby' => 'date_insert' );		
			
	$query_emails = new USAM_Email_Query( $args );
	$emails = $query_emails->get_results();		
	foreach ($emails as $email ) 
	{
		$_email = new USAM_Email( $email->id );
		$_email->send_mail();
	}
}

function usam_send_mail( $mailbox_id ) 
{	
	$_email = new USAM_Email( $mailbox_id );
	return $_email->send_mail();	
}

// Обработка получения и отправки сообщений
function usam_download_email_pop3_server( $mailbox_id ) 
{			
	$mailboxes = new USAM_POP3( $mailbox_id );	
	if ( $mailboxes->connect_open() )
		$result = $mailboxes->download_messages();
	else
		$result = false;
		
	$error = $mailboxes->get_message_errors();	
	if ( !empty($error) )
	{
		if ( !defined( 'DOING_CRON' ) || ! DOING_CRON )		
		{
			usam_set_user_screen_error( $error );	
		}
	}			
	return $result;
}

function usam_email_replace_body( $matches ) 
{	
	$count = substr_count( $matches[0], '>' );
	$replace = str_repeat('&emsp;', $count);	
	  
	return "<br/>".$replace;
}

function usam_send_mail_by_id( $to_email, $to_name, $message, $subject, $mailbox_id = '', $attachments = array() )
{
	if ( empty($message) && empty($attachments) )
		return false;	
	
	if ( empty($mailbox_id) || !is_numeric($mailbox_id) )
		$mailbox_id = get_option("usam_return_email");	
			
	$mailbox = usam_get_mailbox( $mailbox_id );
	if ( !empty($mailbox['email']) )
	{
		$style = new USAM_Mail_Styling();
		$message = $style->process_plaintext_args( $message );
	
		$insert_email = array( 'body' => $message, 'subject' => $subject, 'to_email' => $to_email, 'to_name' => $to_name, 'mailbox_id' => $mailbox['id'] );								
		$_email = new USAM_Email( $insert_email );
		$_email->save();	
		
		if ( !empty($attachments) )
			$_email->set_attachments( $attachments );
		
		$email_sent = $_email->send_mail();		
	}		
	else
		$email_sent = false;
	return $email_sent;	
}


function usam_get_mailboxes( $qv = array() )
{ 
	global $wpdb;
	
	$cache_key = 'usam_mailboxes';
	if ( empty($qv) )
	{
		$cache = wp_cache_get( $cache_key );			
		if ( $cache !== false )			
		{							
			return $cache;								
		}	
	}	
	if ( isset($qv['fields']) && $qv['fields'] != 'all' )
	{		
		if ( is_array($qv['fields']) )
			$fields = implode(',',$qv['fields']);
		else
			$fields = $qv['fields'];
	}
	else
	{
		$qv['fields'] = 'all';
		$fields = '*';		
	}	
	$_where[] = '1=1';		
	if ( isset($qv['email']) )
		$_where[] = "email = '".$qv['email']."'";	
		
	if ( !empty( $qv['user_id'] ) ) 
	{
		$user_ids = is_array($qv['user_id']) ? $qv['user_id']: array( $qv['user_id'] );		
		$user_ids = array_map('intval', $user_ids);					
		
		$_where[] = "id IN (SELECT id FROM ".USAM_TABLE_MAILBOX_USERS." WHERE user_id IN ('".implode( "', '", $user_ids )."') )";
	}	
		
	if ( isset($qv['include']) )
		$_where[] = "id IN( '".implode( "','", $qv['include'] )."' )";	
	
	if ( isset($qv['search']) )
		$_where[] = "name LIKE LOWER ('%".$qv['search']."%') OR email LIKE LOWER ('%".$qv['search']."%')";
	
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'sort';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'ASC';	
	if ( isset($qv['output_type']) )	
		$output_type = $qv['output_type'];	
	else
		$output_type = 'OBJECT';
	
	if ( isset($qv['paged']) )	
	{
		$paged = $qv['paged']-1;	
	
		if ( isset($qv['number']) )	
			$number = $qv['number'];
		else
			$number = 20;
		
		$offset = $paged*$number;
		
		$limit = " LIMIT $offset,$number";
	}
	else
		$limit = '';
	
	if ( !empty($_where) )
	{
		$where = implode( " AND ", $_where );		
		$where = " WHERE $where ";
	}		
	if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] )
		$results = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_MAILBOX." $where $orderby $order $limit", $output_type );
	else
		$results = $wpdb->get_col( "SELECT $fields FROM ".USAM_TABLE_MAILBOX." $where $orderby $order $limit" );
	
	if ( empty($qv) )
		wp_cache_set( $cache_key, $results );		
	return $results;
}


function usam_get_mailbox( $id )
{
	global $wpdb;	
	
	$cache_key = 'usam_mailbox';		
	if( ! $cache = wp_cache_get($cache_key, $id ) )		
	{							
		$result = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".USAM_TABLE_MAILBOX." WHERE id='%d'", $id), ARRAY_A );
		wp_cache_set( $id, $result, $cache_key );	
	}		
	$encryption  = new USAM_Encryption();	
	if ( !empty($result['smtppass']) )
		$result['smtppass']	= $encryption->data_decrypt( $result['smtppass'] );
	if ( !empty($result['pop3pass']) )
		$result['pop3pass']	= $encryption->data_decrypt( $result['pop3pass'] );
	return $result;
}

// Обновить почтовый ящик
function usam_update_mailbox( $id, $update )
{
	global $wpdb;	

	$format = array( 'user_id' => '%d', 'name' => '%s', 'email' => '%s', 'pop3server' => '%s', 'pop3port' => '%d', 'pop3user' => '%s', 'pop3pass' => '%s','pop3ssl' => '%d', 'smtpserver' => '%s', 'smtpport' => '%d', 'smtpuser' => '%s', 'smtppass' => '%s', 'smtp_secure' => '%s', 'letter_id' => '%d', 'sort' => '%s', 'delete_server' => '%d', 'delete_server_day' => '%s', 'delete_server_deleted' => '%d' );
	
	$formats = array();
	foreach( $update as $key => $value) 	
	{
		if ( isset($format[$key]) )
			$formats[] = $format[$key];
		else				
			unset($update[$key]);
	}	
	if ( isset($update['email']) && !is_email($update['email']) )
	{
		return false;
	}	
	$id = (int)$id;
	$where  = array( 'id' => $id );
	$result = $wpdb->update( USAM_TABLE_MAILBOX, $update, $where );	//$formats
	return $result;
}

function usam_get_mailbox_users( $id ) 
{
	global $wpdb;	
	$user_ids = $wpdb->get_col( "SELECT user_id FROM ".USAM_TABLE_MAILBOX_USERS." WHERE id = '$id'" );	
	return $user_ids;	
}

// Получить основную почту
function usam_get_primary_mailbox( )
{		
	$id = get_option("usam_return_email");
	$mailbox = usam_get_mailbox( $id );
	return $mailbox;
}
?>