<?php
// Класс обратной связи
class USAM_Feedback_Init
{	
	public function __construct()
	{					
		add_action( 'wp_enqueue_scripts', array($this, 'scripts') );	
	}	
	
	public function get_window_modal( $modal ) 
	{
		$modal = sanitize_title( $modal );				
		$method = "get_window_modal_$modal";
	
		if ( method_exists( $this, $method ) )
			$output = $this->$method();					
		else
			$output = '';			
		return $output;
	}	
	
	public function scripts() 
	{
		global $post;
		
		$page_id = isset( $post->ID ) ? $post->ID : 0;
		wp_enqueue_script( 'usam-feedback' );
		wp_localize_script( 'usam-feedback', 'USAM_Feedback', array(				
			'page_id'                          => $page_id,			
			'spinner'                          => esc_url( USAM_CORE_IMAGES_URL."/loading.gif" ),		
			'url'                              => 'index.php',
			'message'                          => __('Сообщение отправлено! Спасибо за обращение в центр поддержки клиентов. Специалист центра скоро его обработает...','usam'),
			'message_quick_purchase'           => __('Заказ создан! Когда специалист обработает Ваш заказ, он свяжется с вами...','usam'),
			'message_quick_purchase_error'     => __('Ошибка создания заказа. Попробуйте сменить браузер и попробуйте еще раз.','usam'),
		) );			
	}	
		
	private function print_message_body( $tmessage ) 
	{
		?>
		<div id = "<?php echo $tmessage; ?>_form" class = "modal_content">
			<?php $this->print_message_fields( $tmessage ); ?>
			<p class = "note"><?php _e( 'Обязательные поля отмечены *', 'usam' ); ?></p>
			<?php $this->print_message_button( ); ?>			
		</div>
		<?php		
	}
	
	private function print_message_button( )
	{
		?>
		<div id ="feedback_button" class="popButton">
			<button id = "modal_action" type="button" class="button button_save"><?php _e( 'Отправить', 'usam' ); ?></button>				
			<button type="button" class="button button_close" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
		</div>
		<?php		
	}
	
	private function print_message_fields( $tmessage ) 
	{
		$option = 'usam_feedback_'.$tmessage;
		$feedback = get_option( $option, '' );		
		
		$default_fields = array( 'name' => array( 'title' => esc_html__( 'Ваше имя', 'usam' ), 'type' => 'input', 'maxlength' => '32', 'message' => ''), 								 
								 'mail' => array( 'title' => esc_html__( 'Электронная почта', 'usam' ), 'type' => 'input', 'maxlength' => '32', 'message' => 'Если вы хотите получить от нас ответ, то укажите электронный адрес.'),
								 'phone' => array( 'title' => esc_html__( 'Телефон', 'usam' ), 'type' => 'input', 'maxlength' => '32', 'message' => 'Если вы хотите получить от нас ответ, по телефону.'),
								 'url'  => array( 'title' => esc_html__( 'Ссылка', 'usam' ), 'type' => 'input', 'maxlength' => '255', 'message' => ''), 
								 'message' => array( 'title' => esc_html__( 'Сообщение', 'usam' ), 'type' => 'textarea', 'message' => '', 'maxlength' => '255'),								 
								 );									 
		$customer = usam_get_customer_meta( 'checkout_details' );
		$list_properties = usam_get_order_properties( array('fields' => 'id=>unique_name') );	
		
		foreach( $list_properties as $id => $unique_name )
		{
			if ( isset($customer[$id]) )
				$customer_data[$unique_name] = $customer[$id];
			else
				$customer_data[$unique_name] = '';
		}			
		?>		
		<table class = "message_fields">		
			<?php			
			foreach( $default_fields as $type_field => $field )
			{
				if ( empty($feedback) )
					$feedback['fields'][$type_field] = array( 'show' => 1, 'require' => 0, 'title' => '' );
		
				if ( !empty($feedback['fields'][$type_field]['show']))
				{
					switch( $type_field )
					{
						case 'name':
							$value = $customer_data['billinglastname'].' '.$customer_data['billingfirstname'];
						break;
						case 'mail':
							$value = $customer_data['billingemail'];
						break;
						case 'phone':
							$value = $customer_data['billingphone'];
						break;
						default:
							$value = '';
						break;
					}					
					if ( empty($feedback['fields'][$type_field]['require']))
						$required = 0;
					else
						$required = 1;
					?>
					<tr>
						<td><label><?php echo $field['title'].($required == 1?'*':''); ?>:</label></td>
						<td class = "entry_field">
						<?php 								
						$parameters = 'id="txt_'.$type_field.'" class="txt uneditable-input" data-required = "'.$required.'" data-type = "'.$type_field.'"';
						switch($field['type'] )
						{
							case 'input':
								?>						
								<input <?php echo $parameters; ?> type="text" maxlength="<?php echo $field['maxlength']; ?>" value="<?php echo $value; ?>"/>
								<?php 
							break;
							case 'textarea':
								?>						
								<textarea <?php echo $parameters; ?>><?php echo $value; ?></textarea>
								<?php 
							break;	
						}	
						if ( $feedback['fields'][$type_field]['title'] != '' )	
						{
							?><p><?php echo $feedback['fields'][$type_field]['title']; ?></p><?php 
						}						
						?>
						<span class ="hidden"><?php esc_html_e( 'Не заполнено', 'usam' ); ?></span>
						</td>
					</tr>
					<?php 
				}
			}
			?>
		</table>	
		<?php
	}
	
	function get_window_modal_quick_purchase( ) 
	{
		global $usam_checkout;	
		
		ob_start();		
		?>			
		<div class = "modal_content">
			<p>Наш сотрудник Вам позвонит в самое ближайшее время и уточнить способ доставки товара</p>
			<table class = "message_fields">										
				<?php					
				$usam_checkout = new USAM_Checkout( );	
				$usam_checkout->profile_part( array( 'fast_buy' => 1 ) );						
				while ( usam_have_checkout_items()) : usam_the_checkout_item(); 						
					?>
					<tr> 
					   <td class='<?php echo usam_checkout_form_element_id(); ?> title'>
						  <label for='<?php echo usam_checkout_form_element_id(); ?>'><?php echo usam_checkout_form_name();?></label>
					   </td>
					   <td>
						  <?php echo usam_checkout_form_field();?>
						   <?php if(usam_the_checkout_item_error() != ''): ?>
								  <p class='validation-error'><?php echo usam_the_checkout_item_error(); ?></p>
						  <?php endif; ?>
					   </td>
					</tr>			
				<?php endwhile; ?>								
			</table>					
			<div class="popButton">
				<button id = "modal_action" type="button" class="button button_save"><?php _e( 'Отправить', 'usam' ); ?></button>				
				<button type="button" class="button button_close" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
			</div>	
		</div>
		<?php 		
		$html = ob_get_contents();
		ob_end_clean();
		
		$modal_html = usam_get_modal_window( __('Быстрая покупка', 'usam'), 'quick_purchase', $html );
		return $modal_html;		
	}	
		
	public function get_window_modal_ask_question() 
	{		
		ob_start();		
		?>	
		<div class="ask_question_topic">	
			<label><?php _e('Тема', 'usam'); ?>:</label>						
			<select class="uneditable-input" id="txt_ask_question" data-required = "0" data-type = "topic" name = "txt_topic">	
				<?php
				$topic = usam_get_feedback_topic();
				foreach ( $topic as $key => $value ) 
				{
					if ( usam_is_product() && $key == 'product-info' )
						$selected = 'selected="selected"';
					else
						$selected = '';
					?>
					<option value='<?php echo $key; ?>' <?php echo $selected; ?>><?php echo $value; ?></option>
					<?php
				}
				?>
			</select>				
		</div>
		<?php 	
		$this->print_message_body('ask_question'); 		
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Ваш вопрос', 'usam'), 'ask_question', $html );
		return $modal_html;
	}	
	
	function get_window_modal_buy_product( ) 
	{
		$page_id    = absint($_POST['page_id']);
		$price      = usam_get_product_price_currency( $page_id );		
		?>
		<div class = "box_header">
			<div class = "box_left">
				<div class = "image"><?php echo usam_get_product_thumbnail( $page_id, 'product-image' ); ?></div>
				<div class = "price"><?php echo $price; ?></div>
			</div>
			<div class = "box_right"><p><?php echo usam_get_feedback_message('buy_product'); ?></p></div>
		</div>		
		<?php 
		$this->print_message_body('buy_product');			
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Заказать товар', 'usam'), 'buy_product', $html );
		return $modal_html;
	}	
	
	function get_window_modal_price_comparison( ) 
	{
		$page_id = absint($_POST['page_id']);
		$price      = usam_get_product_price_currency( $page_id );				
		?>
		<div class = "box_header">
			<div class = "box_left">
				<div class = "image"><?php echo usam_get_product_thumbnail( $page_id, 'product-image' ); ?></div>
				<div class = "price"><?php echo $price; ?></div>
			</div>
			<div class = "box_right"><p><?php echo usam_get_feedback_message('price_comparison'); ?></p></div>
		</div>			
		<?php 		
		$this->print_message_body('price_comparison');			
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Есть дешевле?', 'usam'), 'price_comparison', $html );
		return $modal_html;
	}	
			
	public function get_window_modal_product_error( ) 
	{
		$page_id = absint($_POST['page_id']);
		$price      = usam_get_product_price_currency( $page_id );	
		?>
		<div class = "box_header">
			<div class = "box_left">
				<div class = "image"><?php echo usam_get_product_thumbnail($page_id, 'product-image'); ?></div>
				<div class = "price"><?php echo $price; ?></div>
			</div>
			<div class = "box_right"><p><?php echo usam_get_feedback_message('product_error'); ?></p></div>
		</div>
		<?php 		
		$this->print_message_body('product_error');			
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Сообщить об ошибке', 'usam'), 'product_error', $html );
		return $modal_html;
	}
		
	public function get_window_modal_product_info( ) 
	{
		$page_id = absint($_POST['page_id']);		
		$price      = usam_get_product_price_currency( $page_id );	
		?>
		<div class = "box_header">
			<div class = "box_left">
				<div class = "image"><?php echo usam_get_product_thumbnail( $page_id, 'product-image' ); ?></div>
				<div class = "price"><?php echo $price; ?></div>
			</div>
			<div class = "box_right"><p><?php echo usam_get_feedback_message('product_info'); ?></p></div>	
		</div>		
		<?php			
		$this->print_message_body('product_info');			
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Вопрос о товаре', 'usam'), 'product_info', $html );
		return $modal_html;
	}	
	
	public function get_window_modal_back_call( ) 
	{		
		$customer = usam_get_customer_meta( 'checkout_details' );
		$list_properties = usam_get_order_properties( array('fields' => 'id=>unique_name') );	
		
		foreach( $list_properties as $id => $unique_name )
		{
			if ( isset($customer[$id]) )
				$customer_data[$unique_name] = $customer[$id];
			else
				$customer_data[$unique_name] = '';
		}		
		$mask = "7(999) 999-9999";		
		?>		
		<div id = "back_call_form" class = "modal_content">				
			<table class = "message_fields">			
				<tr>
					<td><label><?php esc_html_e( 'Ваш телефон', 'usam' ); ?>:</label></td>
					<td class = "entry_field">				
						<input id="txt_billingphone" class="txt uneditable-input" data-required = "1" data-type = "phone" data-mask='<?php echo $mask; ?>' type="text" value="<?php echo $customer_data['billingmobilephone']; ?>"/>						
						<span class ="hidden"><?php esc_html_e( 'Не заполнено', 'usam' ); ?></span>
					</td>
				</tr>
			</table>		
			<?php $this->print_message_button( ); ?>	
		</div>		
		<?php
		$html = ob_get_contents();
		ob_end_clean();
	
		$modal_html = usam_get_modal_window( __('Обратный звонок', 'usam'), 'back_call', $html );
		return $modal_html;
	}	
}
new USAM_Feedback_Init();



function usam_get_feedback_topic() 
{
	$topic = array( 'sale'             =>  __('Получить информацию об акциях, купонах, скидках', 'usam'),
					'product-info'     =>  __('Уточнить характеристики товара', 'usam'),
					'product-monitor'  =>  __('Монитор товаров', 'usam'),
					'shipping-info'    =>  __('Уточнить информацию по доставке заказа', 'usam'),
					'cancel-order'     =>  __('Отменить заказ', 'usam'),
					'return-product'   =>  __('Уточнить условия возврата товара', 'usam'),
					'unsubscribe'      =>  __('Отписать от рассылки', 'usam'),			
					'customer-reviews' =>  __('Оставить отзыв о работе магазина', 'usam'),
					'buy-product'      =>  __('Заказать товар', 'usam'),
					'error-site'       =>  __('Ошибка или некоректная работа сайта', 'usam'),
					'product-error'    =>  __('Ошибка в описании товара', 'usam'),
					'price-comparison' =>  __('Нашел товар дешевле', 'usam'),
					'contact-form'     =>  __('Контактная форма', 'usam'),
					'back-call'        =>  __('Обратный звонок', 'usam'),					
					'' =>  __('Другие вопросы', 'usam'),
					
	);
	return $topic;
}

function usam_get_feedback_message( $message_type ) 
{
	$option = 'usam_feedback_'.$message_type;
	$feedback = get_option( $option, '' );	
	if ( empty($feedback['message']) )
	{
		$default_message = usam_feedback_message();			
		return $default_message[$message_type];
	}
	else
		return $feedback['message'];
}
?>