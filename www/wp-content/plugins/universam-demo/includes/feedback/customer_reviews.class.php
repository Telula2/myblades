<?php
/*
 * Description: Отзывы клиентов позволяет Вашим клиентам и посетителям оставить отзывы или рекомендации ваших услуг.
 * Version: 2.4.5
 * Revision Date: 05.09.2017
 */

class USAM_Customer_Reviews 
{	
	 // строковые
	private static $string_cols = array(
		'date_time',		
		'name',
		'mail',		
		'reviewer_ip',		
		'title',
		'review_text',
		'review_response',		
		'custom_fields',	
	);
	// цифровые
	private static $int_cols = array(
		'id',				
		'user_id',
		'status',
		'rating',		
		'page_id',		
	);	
	private $is_status_changed = false;	
	private $previous_status   = false;	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		 
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );			
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_review' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$class ) 
	{
		$id = $class->get( 'id' );
		wp_cache_set( $id, $class->data, 'usam_review' );		
		do_action( 'usam_review_update_cache', $class );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$class = new USAM_Customer_Reviews( $value, $col );
		wp_cache_delete( $class->get( 'id' ), 'usam_review' );	
		do_action( 'usam_review_delete_cache', $class, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_review_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_CUSTOMER_REVIEWS." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_review_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_CUSTOMER_REVIEWS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{		
			$this->exists = true;
			$data['custom_fields']     = unserialize( $data['custom_fields'] );	
			$this->data = apply_filters( 'usam_review_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_review_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_review_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_review_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}	
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}	
		$this->data = apply_filters( 'usam_review_set_properties', $this->data, $this );		
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );			
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_review_pre_save', $this );	
		$where_col = $this->args['col'];
			
		if ( isset($this->data['custom_fields']) )	
			$this->data['custom_fields']     = serialize( $this->data['custom_fields'] );	
		
		if ( isset($this->data['name']) )	
			$this->data['name'] = trim(strip_tags($this->data['name']));
		if ( isset($this->data['mail']) )	
			$this->data['mail'] = trim(strip_tags($this->data['mail']));
		if ( isset($this->data['title']) )	
			$this->data['title'] = trim(strip_tags($this->data['title']));
		if ( isset($this->data['review_text']) )	
			$this->data['review_text'] = trim(strip_tags($this->data['review_text']));
		if ( isset($this->data['rating']) )	
			$this->data['rating'] = intval($this->data['rating']);
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_review_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			$this->data = apply_filters( 'usam_review_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );				
			
			$where = array( $this->args['col'] => $where_val);
			$result = $wpdb->update( USAM_TABLE_CUSTOMER_REVIEWS, $this->data, $where, $format, array($where_format) );
				
			do_action( 'usam_review_update', $this );
			if ( $result ) 
			{ 
				if ( $this->is_status_changed ) 
				{		
					$current_status = $this->get( 'status' );
					$previous_status = $this->previous_status;
					$this->previous_status = $current_status;
					$this->is_status_changed = false;	
					$id = $this->get('id');
				
					do_action( 'usam_update_customer_review_status', $id, $current_status, $previous_status, $this );		
				}
			}
		} 
		else 
		{   
			do_action( 'usam_review_pre_insert' );			
			
			unset( $this->data['id'] );
			if ( empty($this->data['coupon_type']))
				$this->data['coupon_type'] = 'coupon';			
			$this->data['date_time']      = date( "Y-m-d H:i:s" );		
			$this->data['user_id']        = $user_ID;		
			$this->data['reviewer_ip']    = $_SERVER['REMOTE_ADDR'];			
			
			$this->data = apply_filters( 'usam_review_insert_data', $this->data );
			$format = $this->get_data_format(  );
			$this->data_format( );	

			$result = $wpdb->insert( USAM_TABLE_CUSTOMER_REVIEWS, $this->data, $format ); 
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_review_insert', $this );
		} 		
		do_action( 'usam_review_save', $this );

		return $result;
	}
}

// Обновить 
function usam_update_review( $id, $data )
{
	$review = new USAM_Customer_Reviews( $id );
	$review->set( $data );
	return $review->save();
}

// Получить 
function usam_get_review( $id, $colum = 'id' )
{
	$review = new USAM_Customer_Reviews( $id, $colum );
	return $review->get_data( );	
}

// Добавить 
function usam_insert_review( $data )
{
	$review = new USAM_Customer_Reviews( $data );
	return $review->save();
}

// Удалить
function usam_delete_review( $id )
{
	$review = new USAM_Customer_Reviews( $id );
	return $review->delete();
}

function usam_reviews_url( $review, $page )
{  
	$link = get_permalink( $review->page_id );
	
	if (strpos($link,'?') === false)
		$link = trailingslashit($link) . "?cr=$page#hreview-$review->id";
	else 
		$link = $link . "&cr=$page#hreview-$review->id";              
	return $link;    
}
?>