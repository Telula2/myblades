<?php
/*
 * Description: Отзывы клиентов позволяет Вашим клиентам и посетителям оставить отзывы или рекомендации ваших услуг.
 * Version: 2.4.5
 * Revision Date: 25 мая 2017
 */

class USAM_Customer_Reviews_Theme 
{        
    private $options = array();
    private $page = 1;
	
	public function __construct( $args = array() )
	{		             
		$default = get_option('usam_reviews',$args );
		$this->options = array_merge( $default, $args );	
		
        if ( isset($_GET['cr']) ) 
			$this->page = intval($_GET['cr']);      
    }
		  
    function add_style_script() 
	{     
		wp_enqueue_script('usam-customer-reviews');
    }	     
	
    function is_active_page()
	{
        global $post;
        
        if ( !isset($post) || !isset($post->ID) || intval($post->ID) == 0 )
            return false; 
				
        if ( is_singular() && usam_get_product_meta($post->ID, "reviews_enable", false) ) 
            return true;      
	
        return false;
    }     

    function aggregate_footer() 
	{        
        $aggregate_footer_output = '';
       
        if ($this->options['show_hcard_on'] != 0 ) 
		{           
            if ( $this->options['show_hcard_on'] == 1 ) 
                $show = true;
            elseif ( $this->options['show_hcard_on'] == 2 && ( is_home() || is_front_page() ) ) 
                $show = true;
            elseif ( $this->options['show_hcard_on'] == 3 && $this->is_active_page() ) 
                $show = true;
			else
				 $show = false;
            
            $div_id = "usam_hcard_h";
            if ( $this->is_active_page() ) 
			{
                if ( $this->options['show_hcard'] == 1 )
                    $div_id = "usam_hcard_s";                
            }
            if ($show) 
			{             
				$business = usam_shop_requisites_shortcode();
				$aggregate_footer_output = '<div id="'.$div_id.'" class="vcard">';
                $aggregate_footer_output .= $business['full_company_name'].'<br />';  			
               
				$aggregate_footer_output .= '<span class="adr">';
				if ( !empty($business['legaladdress']) )
					$aggregate_footer_output .= '<span class="street-address">' . $business['legaladdress'] . '</span>&nbsp;';
				
				if ( !empty($business['legalcity']) ) 
					$aggregate_footer_output .='<span class="locality">' . $business['legalcity'] . '</span>,&nbsp;';
				
				if ( !empty($business['legalpostcode']) ) 
					$aggregate_footer_output .='<span class="postal-code">' . $business['legalpostcode'] . '</span>&nbsp;';
				   
				if ( !empty($business['legalcountry']) ) 				   
					$aggregate_footer_output .='<span class="country-name">' . $business['legalcountry'] . '</span>&nbsp;';
				$aggregate_footer_output .= '</span>';
                
                if ( !empty($business['email']) && !empty($business['phone']) )
                    $aggregate_footer_output .= '<br />';                
                if ( !empty($business['email']) ) 
                    $aggregate_footer_output .= '<a class="email" href="mailto:' . $business['email'] . '">' . $business['email'] . '</a>';
                if ( !empty($business['email']) && !empty($business['phone']) ) 
                    $aggregate_footer_output .= '&nbsp;&bull;&nbsp';                
                if ( !empty($business['phone']) ) 
                    $aggregate_footer_output .= '<span class="tel">' . $business['phone'] . '</span>';
                $aggregate_footer_output .= '</div>';
            }
        }
        return $aggregate_footer_output;
    }

    function iso8601($time=false) 
	{
        if ($time === false)
            $time = time();
        $date = date('Y-m-d\TH:i:sO', $time);
        return (substr($date, 0, strlen($date) - 2) . ':' . substr($date, -2));
    }

    function pagination($total_results, $reviews_per_page) 
	{
        global $post; 
		
		if ( empty($reviews_per_page) )
			return '';
		
		$reviews_per_page = (int)$reviews_per_page;
			
        $out = '';
        $uri = false;
        $pretty = false;

        $range = 2;
        $showitems = ($range * 2) + 1;

        $paged = $this->page;
        if ($paged == 0) { $paged = 1; }       

        $pages = ceil($total_results / $reviews_per_page);
        if ($pages > 1) 
		{           
			$uri = trailingslashit(get_permalink($post->ID));
			if (strpos($uri, '?') === false) {
				$url = $uri . '?';
				$pretty = true;
			} 
			else
			{
				$url = $uri . '&amp;';
				$pretty = false;
			}   
            $out .= '<div id="usam_pagination"><div id="usam_pagination_page">'.__('Страница','usam').': </div>';
            if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) {
                if ($uri && $pretty) {
                    $url2 = $uri;
                } 
				else 
				{
                    $url2 = $url;
                }
                $out .= '<a href="' . $url2 . '">&laquo;</a>';
            }
            if ($paged > 1 && $showitems < $pages) {
                $out .= '<a href="' . $url . 'cr=' . ($paged - 1) . '">&lsaquo;</a>';
            }
            for ($i = 1; $i <= $pages; $i++) 
			{
                if ($i == $paged) 
                    $out .= '<span class="usam_current">' . $paged . '</span>';  
				elseif (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems) 
				{
                    if ($i == 1) 
					{
                        if ($uri && $pretty)
                            $url2 = $uri;
                        else 
                            $url2 = $url;                        
                        $out .= '<a href="' . $url2 . '" class="usam_inactive">' . $i . '</a>';
                    } 
					else 
                        $out .= '<a href="' . $url . 'cr=' . $i . '" class="usam_inactive">' . $i . '</a>';
                }
            }
            if ($paged < $pages && $showitems < $pages) {
                $out .= '<a href="' . $url . 'cr=' . ($paged + 1) . '">&rsaquo;</a>';
            }
            if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
                $out .= '<a href="' . $url . 'cr=' . $pages . '">&raquo;</a>';
            }
            $out .= '</div>';
            $out .= '<div class="usam_clear"></div>';
            return $out;
        }
    }
        
    function output_reviews_show( $page_id, $args = array() )
	{     
	    $snippet_length = !empty($args['snippet_length'])?$args['snippet_length']:0;
		$per_page = isset($args['per_page'])?$args['per_page']:-1;
		$user_id = isset($args['user_id'])?$args['user_id']:NULL;		
		$summary_rating = isset($args['summary_rating'])?$args['summary_rating']:true;
		 
		add_action('wp_footer',array( $this, 'add_style_script'));	  
				
		$query = array( 'status' => 1, 'page_id' => $page_id, 'paged' => $this->page, 'number' => $per_page, 'user_id' => $user_id, 'order' => 'DESC' );		
		
		$post = get_post( $page_id );	
		if ( $post->post_type == 'usam-product' || $this->options['hreview_type'] == 'product' )
			$hreview_type = 'product';
		else
			$hreview_type = $this->options['hreview_type'];
		
		$_reviews = new USAM_Customer_Reviews_Query( $query );	
		$reviews = $_reviews->get_results();
		$total_reviews = $_reviews->get_total();
		
        $reviews_content = '';
        $hidesummary = '';  
		
        /* пытается получить доступ к странице, которая не существует - отправить на главную страницу */
        if ( $this->page != 1 && count($reviews) == 0 )
		{
            $url = get_permalink($page_id);
            $this->redirect($url);
        }        
        if ($page_id == 0) 
		{ /*при использовании шорткод показать отзывы для всех страниц, мог сделать странные вещи при использовании типа продукта */
            $page_id = $reviews[0]->page_id;
        }  
        $reviews_content .= '<div id="customer_reviews">';          
        if (count($reviews) == 0) 
		{
            /* $reviews_content .= '<p>Еще нет отзывов. Будьте первым, оставьте свой!</p>'; */
        } 
		else 
		{ 
            $got_aggregate = usam_get_aggregate_reviews( $page_id );   
            $best_score = number_format($got_aggregate["max"], 1);
            $average_score = number_format($got_aggregate["aggregate"], 1);

            if ( $hreview_type == 'product')
			{
				$brands_list = wp_get_post_terms($page_id, 'usam-brands', array("fields" => "names"));	
				$category_list = wp_get_post_terms($page_id, 'usam-category', array("fields" => "names"));					
				$meta_product_name = get_the_title($page_id);  
				$meta_product_brand = $brands_list[0];			
				$meta_product_sku = usam_get_product_meta( $page_id, 'sku', true );
				$meta_product_model = $category_list[0];
			    $reviews_content .= '
                    <span class="item hproduct" id="hproduct-' . $page_id . '">
                        <span class="usam_hide">
                            <span class="brand">' . $meta_product_brand . '</span>
                            <span class="fn">' . $meta_product_name . '</span>                 
                            <span class="identifier">
                                <span class="type">Артикул</span>
                                <span class="value">' . $meta_product_sku . '</span>
                            </span>                           
                            <span class="identifier">
                                <span class="type">Model</span>
                                <span class="value">' . $meta_product_model . '</span>
                            </span>
                        </span>';	
            }
			if ( $summary_rating )
			{
				$ratings = array( 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0 );
				$sum = 0;
				foreach ($reviews as $review) 
				{					
					$ratings[$review->rating]++;
					$sum += $review->rating;
				} 
				$average_rating = round($sum/$total_reviews,1);
				$reviews_content .= '<div class="usam_summary_rating">
					<div class="usam_summary">					
						<div class="sp-summary-rating" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
							<div class="usam_summary_rating_stars">'.$this->output_rating( $average_rating ).'</div>
							<div class="usam_summary_rating_disclaimer">
								<span itemprop="reviewCount">'.$total_reviews.'</span> '._n('отзыв','отзывов',$total_reviews,'usam').' | 
								<span class="sp-summary-rating-value" itemprop="ratingValue">'.$average_rating.'</span> '.__('из','usam').' 5
							</div>
						</div>
					</div>
					<div class="usam_summary_rating_distribution">';	           
						for ($i = count($ratings); $i>0; $i--)
						{
							$reviews_content .=
							'<div class="usam_summary_rating_distribution_item">  
								<div class="usam_summary_rating_distribution_item_label">'.$i.' '.__('звезд','usam').'</div>
								<div class="usam_summary_rating_distribution_item_bar">'.$this->output_rating( $i ).'</div>
								<div class="usam_summary_rating_distribution_item_value">('.$ratings[$i].')</div>
							</div>';
						}
					$reviews_content .= '</div>
				</div><hr>';
			}           
			$summary = '';
			foreach ($reviews as $review) 
			{                
                $summary = $summary == ''?$review->review_text:$summary;				
				if (  $snippet_length > 0 )
                {
                    $review->review_text = usam_limit_words($review->review_text, $snippet_length);
                }                
                $review->review_text .= '<br />';
                $hide_name = '';				
                if ( !empty($this->options['fields']['name']['show']) ) 
				{
                    $review->name = __('Анонимный','usam');
                    $hide_name = 'usam_hide';
                }
                if ($review->name == '')
                    $review->name = __('Анонимный','usam');
              
                if ( !empty($this->options['fields']['email']['show']) && $review->mail != '') {
                    $review->review_text .= '<br /><small>' . $review->mail . '</small>';
                }
                if ( !empty($this->options['fields']['title']['show']) ) {
                    /* do nothing */
                } else {
                    $review->title = substr($review->review_text, 0, 150);
                    $hidesummary = 'usam_hide';
                }                
               if ( !empty($args['show_morelink']) )		
				{
                    $review->review_text .= " <a href='".usam_reviews_url( $review, 1 )."'>".$args['show_morelink']."</a>";
                }                
                $review->review_text = wp_unslash(nl2br($review->review_text));
                $review_response = '';
                
                if ( empty($args['hide_response']) )
                {
                    if (strlen($review->review_response) > 0) 
					{
                        $review_response = '<p class="response"><span class="response_header"><strong>'.__('Официальный ответ','usam').':</strong></span><br> ' . wp_unslash(nl2br($review->review_response)). '<br><span class="response_footer"><strong>Команда сайта.</strong></span></p>';
                    }
                }
                $custom_shown = '';
                if ( empty($args['hide_custom']) )
                {
                    $custom_fields_unserialized = @unserialize($review->custom_fields);
                    if (!is_array($custom_fields_unserialized)) {
                        $custom_fields_unserialized = array();
                    }
					
                    foreach ($custom_fields_unserialized as $key => $value )				
					{  
                        if ( !empty($this->options['fields'][$key]['show']) ) 
						{
                            $custom_shown .= "<div class='usam_fl'>".$this->options['fields'][$key]['title'].': '.$value.'&nbsp;&bull;&nbsp;</div>';
                        }
                    }
                    $custom_shown = preg_replace("%&bull;&nbsp;</div>$%si","</div><div class='usam_clear'></div>",$custom_shown);
                }
                $name_block = '' .
                    '<div class="usam_fl usam_rname">' .
                    '<abbr itemprop="datePublished" title="' . $this->iso8601(strtotime($review->date_time)) . '" class="dtreviewed">' . date_i18n( get_option('date_format'), strtotime($review->date_time)) . '</abbr>&nbsp;' .
                    '<span class="' . $hide_name . '">'.__("от","usam").'</span>&nbsp;' .
                    '<span class="reviewer vcard" id="hreview-usam-reviewer-' . $review->id . '">' .
                    '<span itemprop="author" class="fn ' . $hide_name . '">' . $review->name . '</span>' .
                    '</span>' .
                    '<div class="usam_clear"></div>' .
                    $custom_shown .
                    '</div>';
                if ( $hreview_type == 'product') 
				{
                    $reviews_content .= '
                        <div class="hreview" id="hreview-' . $review->id . '">
                            <h2 itemprop="name" class="summary ' . $hidesummary . '">' . $review->title . '</h2>
                            <span class="item" id="hreview-usam-hproduct-for-' . $review->id . '" style="display:none;">
                                <span class="fn">' . $meta_product_name . '</span>
                            </span>
                            <div class="usam_fl usam_sc">
                                <abbr class="rating" title="' . $review->rating . '"></abbr>
                                <div class="usam_rating">'.$this->output_rating($review->rating).'</div>					
                            </div>
                            ' . $name_block . '
                            <div class="usam_clear"></div>
                            <blockquote itemprop="reviewBody" class="description"><p>' . $review->review_text . '</p></blockquote>
                            ' . $review_response . '
                            <span style="display:none;" class="type">product</span>
                            <span style="display:none;" class="version">0.3</span>
                        </div>
                        <hr />';
                } 
				else
				{	
                    $business = usam_shop_requisites_shortcode();			
					$reviews_content .= '
                        <div class="hreview" id="hreview-' . $review->id . '">
                            <h2 class="summary ' . $hidesummary . '">' . $review->title . '</h2>
                            <div class="usam_fl usam_sc">
                                <abbr class="rating" title="' . $review->rating . '"></abbr>
                                <div class="usam_rating">'.$this->output_rating($review->rating).'</div>					
                            </div>
                            ' . $name_block . '
                            <div class="usam_clear usam_spacing1"></div>
                            <span class="item vcard" id="hreview-usam-hcard-for-' . $review->id . '" style="display:none;">
                                <span class="name">'.$business['full_company_name'] . '</span>
                                <span class="tel">' . $business['phone'] . '</span>
                                <span class="adr">
                                    <span class="country-name">' . $business['legalcountry'] . '</span>
									<span class="locality">' . $business['legalcity'] . '</span>
									<span class="street-address">' . $business['legaladdress'] . '</span>
                                    <span class="postal-code">' . $business['legalpostcode'] . '</span>
                                </span>
                            </span>
                            <blockquote itemprop="reviewBody" class="description"><p>' . $review->review_text . '</p></blockquote>
                            ' . $review_response . '
                            <span style="display:none;" class="type">business</span>
                            <span style="display:none;" class="version">0.3</span>
                       </div>
                       <hr />';					
                }
            } 
            if ( $hreview_type == 'product')
			{
                $reviews_content .= '
                    <span class="hreview-aggregate" id="hreview-usam-aggregate">
                       <span style="display:none;">
                           <span class="rating">
                             <span class="average">' . $average_score . '</span>
                             <span class="best">' . $best_score . '</span>
                           </span>  
                           <span class="votes">' . $got_aggregate["total"] . '</span>
                           <span class="count">' . $got_aggregate["total"] . '</span>
                           <span class="summary">' . $summary . '</span>
                           <span class="item" id="hreview-usam-vcard">
                            <span class="fn">' . $meta_product_name . '</span>
                           </span>
                       </span>
                    </span>';
                $reviews_content .= '</span>'; 
            } 
			else
			{  
                $business = usam_shop_requisites_shortcode();
				$reviews_content .= '  
				<span itemprop="itemReviewed" class="hreview-aggregate" id="hreview-usam-aggregate">
				   <span style="display:none;">
						<span class="item vcard" id="hreview-usam-vcard">
							<span itemprop="name" class="name">'.$business['full_company_name'] . '</span>
							<span itemprop="telephone" class="tel">' . $business['phone'] . '</span>
							<span itemprop="address" class="adr">
								<span itemprop="addressCountry" class="country-name">' . $business['legalcountry'] . '</span>
								<span itemprop="addressLocality" class="locality">' . $business['legalcity'] . '</span>
								<span itemprop="streetAddress" class="street-address">' . $business['legaladdress'] . '</span> 
								<span itemprop="postalCode" class="postal-code">' . $business['legalpostcode'] . '</span>								   
							</span>
						</span>
					   <span class="rating">
							 <span class="average">' . $average_score . '</span>
							 <span class="best">' . $best_score . '</span>
					   </span>  
					   <span class="votes">' . $got_aggregate["total"] . '</span>
					   <span class="count">' . $got_aggregate["total"] . '</span>
					   <span class="summary">' . $summary . '</span>
				   </span>
				</span>';
            }
        }   
        $reviews_content .= '</div>';   
		$reviews_content .= $this->pagination( $total_reviews, $per_page );		
        return $reviews_content;
    }
  
	function output_reviews_content( $page_id = null, $user_ID = null ) 
	{       
		if ( $page_id === null )
		{	 
			global $post; 		
			$page_id = $post->ID;
		}				
		if ( empty($page_id) )
			$show_page_id = usam_get_system_page_id('reviews');	
		else
			$show_page_id = $page_id;
		
		$the_content = '';  
        $the_content .= '<div id="customer_reviews_page">';      
        
        if ($this->options['form_location'] == 0) 
		{
            $the_content .= $this->show_reviews_form( $show_page_id );
        }		
        $the_content .= $this->output_reviews_show( $page_id, array( 'user_id' => $user_ID, 'per_page' => $this->options['reviews_per_page'] ) );
        if ($this->options['form_location'] == 1) 
		{
            $the_content .= $this->show_reviews_form( $show_page_id );
        }	
        $the_content .= $this->aggregate_footer(); 
        $the_content .= '</div>'; 
        $the_content = preg_replace('/\n\r|\r\n|\n|\r|\t/', '', $the_content); 
        return $the_content;
    }
	
	// Вывести отзывы пользователя
	function output_reviews_content_user() 
	{
        global $user_ID;  		
		return $this->output_reviews_content( 0, $user_ID ); 
    }

    function output_rating( $rating = 0 ) 
	{
		$out = '<div class="rating">';		
		for ($i = 1; $i <= 5; $i++) 
		{
			$selected = $rating >= $i?'selected':'';
			$out .= "<span class='star $selected'></span>";
		}
		$out .= '</div>';
        return $out;
    }

    function show_reviews_form( $page_id ) 
	{		
		$out = '';
		if ( isset($_GET['review']) && $_GET['review'] == 1 )
		{
			$out .= '<div class="usam_status_msg">'.__('Спасибо за Ваш отзыв. Все комментарии проходят модерацию и в случае одобрения, он появится на сайте в ближайшее время.','usam').'</div>';	
		}
		else
		{
			if ( isset($_GET['review']) && $_GET['review'] == 'error' )
			{			
				$out .= '<div class="usam_message_error">'.__('Не удалось опубликовать ваш отзыв.','usam').'</div>';	
			}
			if ( !empty($this->options['goto_show_button']) ) 
			{			
				$out .= '<p><a id="button_new_review" href="javascript:void(0);">'.__('Нажмите здесь, чтобы отправить свой отзыв','usam').'</a></p>';
			}
			$out .= '<div id="usam_add_new_review"><hr />';
			$out .= '<form id="usam_commentform" method="post" action="javascript:void(0);">
							<input type="hidden" id="rating" name="review[rating]" />
							<table id="add_review_table">
								<tbody>
									<tr><td colspan="2"><div id="usam_postcomment">' . __('Добавить свой отзыв', 'usam'). '</div></td></tr>';
			foreach( $this->options['fields'] as $type_field => $field )
			{		
				if ( !empty($this->options['fields'][$type_field]) && !empty($this->options['fields'][$type_field]['ask']) && !empty($field['title']) )
				{					
					$value = isset($_POST['review'][$type_field])?$_POST['review'][$type_field]:'';
					
					if ( empty($this->options['fields'][$type_field]['require']))
						$required = 0;
					else
						$required = 1;				
					$out .= '<tr>';
						$out .= '<td><label>'.$field['title'].($required == 1?'*':'').':</label></td>
						<td class = "entry_field">';									
						$parameters = "name='review[$type_field]' id='txt_$type_field' class='uneditable-input' data-required = '$required' data-type = '$type_field'";
						$out .= "<input $parameters type='text' maxlength='32' value='$value'/>";
						if( $required )					
							$out .= "<span class ='hidden'>".__( 'Обязательное поле', 'usam' )."</span>";
						$out .= "</td>
					</tr>";
				}
			}	
		   $text = isset($_POST['review']['text'])?$_POST['review']['text']:'';
		   $out .= '   
				<tr>
					<td><label class="comment-field">'.__("Ваша оценка", "usam").'*:</label></td>
					<td>
						<div class="your_review_rating" itemprop="reviewRating"><input id ="review_rating" name="review[rating]" type="hidden" value="" />'.$this->output_rating().'</div>
					</td>
				</tr>';
			$out .= '
						<tr><td colspan="2"><label for="review-text" class="comment-field">'.__("Отзыв", "usam").'*:</label></td></tr>
						<tr><td colspan="2"><textarea id="review-ftext" data-required="1" name="review[review_text]" data-type = "text" rows="8" class="uneditable-input" cols="50">'.$text.'</textarea></td></tr>
						<tr>
							<td colspan="2" id="usam_check_confirm">										
								<div class="usam_fl"><input type="checkbox" name="confirm" id="confirm" value="1" /></div>
								<div class="usam_fl" style="margin:-2px 0px 0px 5px"><label for="confirm">'.__("Я согласен с публикацией отзыва на сайте","usam").'.</label></div>					
							</td>
						</tr>					
						<tr><td colspan="2"><input id="usam_submit_review" name="submit_review" class ="button button_save" type="submit" value="'.__('Оставить свой отзыв','usam').'" /></td></tr>
					</tbody>
				</table>
			<input name="review[page_id]" type="hidden" value="'.$page_id.'" />
			<input name="usam_action" type="hidden" value="add_review" />		
			</form>';			
			$out .= '<hr /></div>';  
		}		
        return $out;
    }

    function redirect($url, $cookie = array()) 
	{        
        $headers_sent = headers_sent();        
        if ($headers_sent == true) 
		{          
            $out = "<html><head><title>Redirecting...</title></head><body><div style='clear:both;text-align:center;padding:10px;'>".
					__("Обработка. Пожалуйста подождите...","usam") .
                    "<script type='text/javascript'>";
						foreach ($cookie as $col => $val) {
							$val = preg_replace("/\r?\n/", "\\n", addslashes($val));
							$out .= "document.cookie=\"$col=$val\";";
						}
            $out .= "window.location='$url';";
            $out .= "</script>";
            $out .= "</div></body></html>";
            echo $out;
        } 
		else 
		{
            foreach ($cookie as $col => $val) 
			{
                setcookie($col, $val); /* add cookie via headers */
            }        
            wp_redirect($url); /* перенаправление */
        }        
        exit();
    }  
}	

// Вывести отзывы
function usam_reviews( $header_b = true )
{	
	if ( $header_b )
		$header =  '<h2>'.__('Отзывы','usam').'</h2>';
	else 
		$header = '';
	$customer_reviews = new USAM_Customer_Reviews_Theme();
	echo "<div class='product_reviews'>$header".$customer_reviews->output_reviews_content()."</div>";
}
?>