<?php
/**
 * Класс СМС сообщений.
 */
class USAM_SMS
{		
	 // строковые
	private static $string_cols = array(
		'phone',		
		'message',
		'date_insert',		
		'sent_at',		
		'folder',
		'object_type',		
		'number',
	);
	// цифровые
	private static $int_cols = array(
		'id',			
		'object_id',
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'number' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'number'  && $id = wp_cache_get( $value, 'usam_sms_number' ) )
		{   // если находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_sms' ); 
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_sms' );
		if ( $number = $log->get( 'number' ) )
			wp_cache_set( $number, $id, 'usam_sms_number' );	
		do_action( 'usam_sms_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_SMS( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_sms' );
		wp_cache_delete( $log->get( 'number' ), 'usam_sms_number' );		
		do_action( 'usam_sms_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_sms_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SMS." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_sms_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_SMS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_sms_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_sms_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_sms_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_sms_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_sms_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_sms_pre_save', $this );	
		$where_col = $this->args['col'];
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_sms_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			if ( isset($this->data['date_insert']) )
				unset($this->data['date_insert']);
			
			$this->data = apply_filters( 'usam_sms_update_data', $this->data );	
			$format = $this->get_data_format( );			
			$data = $this->data;
			
			$str = array();
			foreach ( $format as $key => $value ) 
			{
				if ( $data[$key] === null )
				{
					$str[] = "`$key` = NULL";
					unset($data[$key]);
				}
				else
					$str[] = "`$key` = '$value'";	
			}			
			$sql = "UPDATE `".USAM_TABLE_SMS."` SET ".implode( ', ', $str )." WHERE $where_col = '$where_format' ";
			$result = $wpdb->query( $wpdb->prepare( $sql, array_merge( array_values( $data ), array( $where_val ) ) ) );
			do_action( 'usam_sms_update', $this );
		} 
		else 
		{   
			do_action( 'usam_sms_pre_insert' );			
			
			unset( $this->data['id'] );			
			$this->data['date_insert']      = date( "Y-m-d H:i:s" );		
			
			$this->data = apply_filters( 'usam_sms_insert_data', $this->data );
			$format = $this->get_data_format(  );					
			$result = $wpdb->insert( USAM_TABLE_SMS, $this->data, $format );			
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_sms_insert', $this );
		} 		
		do_action( 'usam_sms_save', $this );

		return $result;
	}
}

function usam_update_sms( $id, $data )
{
	$_sms = new USAM_SMS( $id );
	$_sms->set( $data );
	return $_sms->save();
}

// Получить 
function usam_get_sms( $id, $colum = 'id' )
{
	$_sms = new USAM_SMS( $id, $colum );
	$data = $_sms->get_data( ); 
	return $data;	
}

function usam_insert_sms( $data )
{
	$_sms = new USAM_SMS( $data );
	return $_sms->save();
}


// Удалить 
function usam_delete_sms( $id )
{
	$_sms = new USAM_SMS( $id );
	return $_sms->delete();
}
?>