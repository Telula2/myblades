<?php
/*=========================================================== Чат ==================================================*/
class USAM_Сhat_Load
{	
	public $iframe = false;
	private $manager_id = false;
	public function __construct()
	{		
		add_action('admin_footer', array(&$this, 'modal_window'), 100);		
		add_action('wp_footer', array(&$this, 'modal_window'), 100);	
	
		add_action( 'init', array($this, 'load') );	
		
		if ( isset( $_REQUEST['chat'] ) ) 
			add_action( 'init', array($this, 'handler_ajax') );	
	
		add_action( 'wp_enqueue_scripts-usam-chat-iframe', array($this, 'iframe_dialog_scripts') );
		
		add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
		add_action( 'admin_enqueue_scripts', array($this, 'scripts') );
	} 
	
	public function load() 
	{	
		$this->manager_id = usam_get_online_consultant();	
	}	
	
	public function modal_window() 
	{	
		$checked = usam_get_status_online_consultants();
		echo usam_get_modal_window( __('Диалог с клиентами','usam').usam_get_switch( $checked ), 'form_dialog' );	
		
		echo '<audio id="chat_audio">
			<source src="'.USAM_URL .'/assets/sound/notify.wav" type="audio/wav">
			<source src="'.USAM_URL .'/assets/sound/notify.mp3" type="audio/mpeg">
			<source src="'.USAM_URL .'/assets/sound/notify.ogg" type="audio/ogg">
		</audio>';
	}		
	
	public function iframe_dialog_scripts() 
	{	
		$version = USAM_VERSION;
		wp_enqueue_style( 'usam-chat-iframe' );
	}

	public function scripts()
	{			
		global $post, $wpdb, $user_ID;	
		
		if ( $this->iframe )
		   return;	
	 
		if ( $this->iframe )
			$t = 'iframe';
		else
			$t = 'site';
		
		$dialog_id = usam_get_customer_meta( 'dialog_id' );
			
		if ( isset($_GET['sel']) && is_numeric($_GET['sel']) )
		{
			$dialog = absint($_GET['sel']);				
			$id_message = (int)$wpdb->get_var("SELECT Max(ID) FROM ".USAM_TABLE_CHAT." WHERE topic = '$dialog'");	
		}	
		elseif ( !empty($dialog_id) )
		{		
			$dialog = $dialog_id;				
			$id_message = (int)$wpdb->get_var("SELECT Max(ID) FROM ".USAM_TABLE_CHAT." WHERE topic = '$dialog'");	
		}			
		else
			$dialog = $id_message = 0;		
				
		wp_enqueue_style( 'usam-chat' );		
		wp_enqueue_script( 'usam-chat' );	

		wp_localize_script( 'usam-chat', 'USAM_Chat', array(		
			'user'       => usam_get_customer_name( $user_ID ),					
			'dialog_id'  => $dialog,
			'manager_id' => $this->manager_id,			
			't'          => $t,
			'id_message' => $id_message,		
			'spinner'   => esc_url( USAM_CORE_IMAGES_URL."/loading.gif" ),
			'message'        => __('Сообщение отправлено! Спасибо за обращение в центр поддержки клиентов. Специалист центра скоро его обработает...','usam')
		) );		
	}
	
	function handler_ajax( ) 
	{				
		switch( $_REQUEST['chat'] )
		{
			case 'sent_message':	
				$response = $this->sent_message();
			break;
			case 'iframe_dialog':	
				$response = $this->get_iframe_dialog();
			break;
			case 'refresh':	
				$response = $this->refresh();
			break;
			case 'refresh_dialogs':	
				$response = $this->refresh_dialogs();
			break;			
			case 'update_online_consultants':	
				$response = $this->update_online_consultants();
			break;
			case 'modal_html':	
				$response = $this->get_modal_html();
			break;
			case 'dialog_message':	
				$response = $this->get_dialog_message();
			break;
		}
		if ( !empty($response) )
			echo json_encode( $response );		
		exit;
	}
	
	private function get_dialog_message()
	{		
		global $wpdb, $user_ID;
		
		if ( empty($_POST['dialog_id']))
			return false;	
		
		$dialog_message = array();
		if ( usam_check_current_user_role('administrator') || usam_check_current_user_role('shop_manager') )
		{
			$dialog_id   = absint($_POST['dialog_id']);	
		
			$sql_query = "SELECT * FROM ".USAM_TABLE_CHAT." WHERE topic = '$dialog_id' ORDER BY date_insert ASC";			
			$dialog_message = $wpdb->get_results($sql_query, ARRAY_A);	
			$dialog_message = $this->format_message( $dialog_message );
		}
		return $dialog_message;			
	}
	
	private function format_message( $dialog_message )
	{		
		global $user_ID;
		
		$result = array();
		foreach ( $dialog_message as $message ) 
		{		
			$result[$message['id']]['id']      = (int)$message['id'];
			$result[$message['id']]['user']    = usam_get_customer_name( $message['user_id'] );
			$result[$message['id']]['message'] = $message['message'];
			$result[$message['id']]['date']    = get_date_from_gmt( $message['date_insert'], "d.m.Y H:i");		
			$result[$message['id']]['read']    = $user_ID == $message['user_id']?1:$message['status'];						
		}
		return $result;
	}
		
	private function get_modal_html()
	{			
		ob_start();	
		$chat = new USAM_Chat_Template();			
		?>							
		<div class="chat">
			<?php $chat->display_chat_dialogs(); ?>	
			<?php $chat->display_dialogs( $this->manager_id ); ?>			
		</div>						
		<?php
		$response = ob_get_contents();
		ob_end_clean();	
		return $response;
	}	
	
	private function update_online_consultants()
	{			
		$response = '';
		if ( !empty($_POST['status']) )
		{
			$status = 1;
			$response = __('Включен', 'usam');
		}
		else
		{
			$status = 0;		
			$response = __('Выключен', 'usam');
		}			
		usam_update_status_online_consultants( $status );			
		return $response;
	}	
	
	function get_iframe_dialog( ) 
	{		
		global $hook_suffix;
				
		$this->iframe = true;

		$dialog_id = usam_get_customer_meta( 'dialog_id' );
		if ( empty($dialog_id) )
		{
			$dialog_id = 0;
		}		
		$manager_id = $this->manager_id;
		$GLOBALS['hook_suffix'] = 'usam-chat-iframe';		
		require_once( dirname(__FILE__) .'/chat-page.php');
	}
	
	public function sent_message( ) 
	{
		global $wpdb, $user_ID;			
		
		$response = array();
		
		
			return $response;			
		
	
	}	
	
	public function refresh_dialogs( ) 
	{
		global $wpdb, $user_ID;		
	
		$sql_query = "SELECT topic, COUNT(id) AS count FROM ".USAM_TABLE_CHAT." WHERE status = '0' AND user_id != '$user_ID' GROUP BY topic";			
		$response = $wpdb->get_results($sql_query, ARRAY_A);			
		return $response;		
	}	
	
	public function refresh( ) 
	{
		global $wpdb, $user_ID;		
		
		$dialog         = absint($_POST['dialog_id']);				
		$user_id        = $user_ID;	
		$id_message     = absint($_POST['id_message']);
		
		$sql_query = "UPDATE ".USAM_TABLE_CHAT." SET status=1 WHERE topic = '$dialog' AND user_id != '$user_ID' AND  id <= '$id_message'";
        $result_update =  $wpdb->query ( $sql_query );
		$response = array();			
		
		$sql_query = "SELECT * FROM ".USAM_TABLE_CHAT." WHERE topic = '$dialog' AND id > '$id_message'";			
		$dialog_message = $wpdb->get_results($sql_query, ARRAY_A);						
		$dialog_message = $this->format_message( $dialog_message );
		return $dialog_message;			
	}	
}
new USAM_Сhat_Load();




class USAM_Chat_Template
{		
	public function display_chat( $type ) 
	{	
		switch( $type )
		{
			case 'dialogs':				
				if ( isset($_GET['sel']) && is_numeric($_GET['sel']) )
				{				
					$dialog = absint($_GET['sel']);
					$this->display_chat_dialogs( $dialog );
				}
				else
					$this->display_dialogs();				
			break;
			case 'new':
				$this->display_chat_dialogs();
			break;	
		}
	}	

	public function display_chat_dialogs( $dialog = 0 ) 
	{	
		?>			
		<div id="content-chat" class = "content-chat">
			<div class = "dialog_messages">
				<?php $this->display_dialog_message( $dialog ); ?>
				<div class="im_controls_wrap">
					<div id="im_controls">
						<div class="row_message"><textarea id="message" placeholder = "<?php _e('Введите Ваше сообщение!','usam'); ?>"></textarea></div>
						<div class="row_button"><button id = "sent_message" type="button" class="sent_message"><?php _e('Отправить','usam'); ?></button></div>
					</div>
				</div>
			</div>
		</div>	
		<?php	
	}	
		
	public function display_dialog_message( $dialog = 0 ) 
	{			
		global $wpdb, $user_ID;
		?>			
		<div class = "im_rows_wrap">
			<?php usam_loader(); ?>
			<table id="im_rows">
				<?php
				if ( $dialog != 0 )
				{					
					$sql_query = "SELECT * FROM ".USAM_TABLE_CHAT." WHERE topic = '$dialog' ORDER BY date_insert ASC";			
					$dialog_message = $wpdb->get_results( $sql_query );						
					foreach ( $dialog_message as $message ) 
					{						
						if ( $message->status == 0 && $user_ID != $message->user_id )
							$class = "class = 'new_message'";
						else
							$class = "";
						?>	
						<tr id="im_rows-<?php echo $message->id; ?>" <?php echo $class; ?>>
							<td class = "message_body">
								<div class='message_header'>
									<span class='message_user'><?php echo usam_get_customer_name($message->user_id); ?></span>
									<span class='message_date'><?php echo usam_local_formatted_date( $message->date_insert ); ?></span>
								</div>								
								<div class = "message_text">
									<?php echo $message->message; ?>
								</div>
							</td>						
						</tr>	
						<?php
					}						
				}
				?>	
			</table>
		</div>		
		<?php
	}	
	
	public function display_dialogs( $manager = false ) 
	{	
		global $wpdb, $user_ID;
		$orderby = 'id';
		$order = 'DESC';
		$where['user_id'] = $user_ID;
		
		if ( $manager )
			$where['manager_id'] = $manager;
		
		$where_str = implode( ' AND ', $where );	

		$sql_query = "SELECT * FROM ".USAM_TABLE_CHAT_DIALOGS." WHERE $where_str ORDER BY $orderby $order";		
		$dialogs = $wpdb->get_results($sql_query, ARRAY_A);
		?>	
		<div id="dialogs" class="dialogs">	
			<?php	
			if ( count( $dialogs) > 0 )
				foreach ( $dialogs as $dialog ) 
				{
					$count = usam_number_new_messages( $dialog['id'] );
					if ( $count == 0 ) 
						$count = '';
					else
						 $count = "<div class='count_new'>+$count</div>";
					?>				
					<div class="dialogs_row" id="im_dialog-<?php echo $dialog['id']; ?>" data-dialog_id="<?php echo $dialog['id']; ?>">				
						<table>
							<tr id="im_dialog-<?php echo $dialog['id']; ?>" class = "is_dialogs">
								<td class = "dialogs_text">
									<div class = "dialogs_user"><?php echo usam_get_customer_name( $dialog['user_id'] ); ?></div>
									<div class = "dialogs_date"><?php echo get_date_from_gmt($dialog['date_insert'], 'd.m.Y H:i'); ?></div>
								</td>
								<td class = "dialogs_name"><?php echo $dialog['name']; ?></td>
								<td class = "dialogs_unread_td"><?php echo $count; ?></td>										
							</tr>
						</table>				
					</div>
					<?php
				}
				else
				{
					_e('Вы еще не общались в нашу службу поддержки клиентов. Если Вам есть что сказать, то, пожалуйста, начните новую тему!','usam'); 			
				}
			?>
		</div>
		<?php
	}
} 		

function usam_start_dialog( ) 
{		
	$dialog_id = usam_get_customer_meta( 'dialog_id' );
	if ( !empty($dialog_id) )	
		$count = usam_number_new_messages( $dialog_id );
	else
		$count = 0;
	
	if ( $count > 0 )
		$text_count = " <span class='numbers'>$count</span>";
	else
		$text_count = '';	
	?>
	<div class="usam_dialog">	
		<a id = "start_dialog"><?php echo __('Консультация онлайн','usam').$text_count; ?></a>
		<div id="usam_dialog_iframe">	
			<iframe id="usam_iframe" src="<?php echo wp_nonce_url( add_query_arg( array( 'chat' => 'iframe_dialog' ) ), 'iframe_dialog_nonce' ); ?>"></iframe>					
		</div>					
	</div>
	<?php
}



function usam_update_status_online_consultants( $status, $user_ID = null ) 
{		
	if ( $user_ID === null )
	{
		global $user_ID;
	}
	if ( $user_ID > 0)
	{
		$consultants = get_option('usam_online_consultants');
		$consultants[$user_ID] = $status;
		update_option( 'usam_online_consultants', $consultants );	
	}	
}

// Получить статус консультанта
function usam_get_status_online_consultants( $user_ID = null ) 
{			
	if ( $user_ID === null )
	{
		global $user_ID;
	}		
	$consultants = get_option('usam_online_consultants');
	if ( isset($consultants[$user_ID]) )
		return $consultants[$user_ID];
	return false;		
}

function usam_get_online_consultant( ) 
{				
	$consultants = get_option('usam_online_consultants');
	if ( !empty($consultants) )
	{
		foreach( $consultants as $manager_id => $status )
		{
			if ( $status )
				return $manager_id;
		}
	}
	return false;		
}

function usam_get_unread_chat_messages()
{
	global $wpdb, $user_ID;	
	$dialog_id = usam_get_customer_meta( 'dialog_id' );
	
	$sql = "SELECT COUNT(*) FROM ".USAM_TABLE_CHAT_DIALOGS." AS d LEFT JOIN ".USAM_TABLE_CHAT." AS c ON ( c.topic=d.id ) WHERE c.id='$dialog_id' AND c.user_id != '$user_ID' AND c.status = '0'";		
	return $wpdb->get_var( $sql );
}
?>