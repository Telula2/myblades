<?php
/**
 * Класс купона.
 */
class USAM_Coupon
{		
	 // строковые
	private static $string_cols = array(
		'coupon_code',		
		'action',
		'is_percentage',		
		'description',		
		'start',
		'expiry',
		'condition',
		'coupon_type',	
		'date_insert',	
	);
	// цифровые
	private static $int_cols = array(
		'id',				
		'use_once',
		'is_used',
		'max_is_used',		
		'active',
		'customer',
		'amount_bonuses_author',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'coupon_code' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'coupon_code'  && $id = wp_cache_get( $value, 'usam_coupon_code' ) )
		{   // если находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_coupon' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_coupon' );
		if ( $coupon_code = $log->get( 'coupon_code' ) )
			wp_cache_set( $coupon_code, $id, 'usam_coupon_code' );	
		do_action( 'usam_coupon_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_Coupon( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_coupon' );
		wp_cache_delete( $log->get( 'coupon_code' ), 'usam_coupon_code' );		
		do_action( 'usam_coupon_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_coupon_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_COUPON_CODES." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_coupon_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_COUPON_CODES." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{
			$data['condition'] = unserialize( $data['condition'] );		
			$this->exists = true;
			$this->data = apply_filters( 'usam_coupon_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_coupon_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_coupon_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_coupon_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_coupon_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_coupon_pre_save', $this );	
		$where_col = $this->args['col'];
			
		if ( isset($this->data['condition']) )	
			$this->data['condition']     = serialize( $this->data['condition'] );
		
		if ( isset($this->data['coupon_code']) )	
			$this->data['coupon_code'] = mb_strtoupper(sanitize_title( $this->data['coupon_code'] ));
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_coupon_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			$this->data = apply_filters( 'usam_coupon_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );		
			$data = $this->data;
			
			$str = array();
			foreach ( $format as $key => $value ) 
			{
				if ( $data[$key] === null )
				{
					$str[] = "`$key` = NULL";
					unset($data[$key]);
				}
				else
					$str[] = "`$key` = '$value'";	
			}			
			$result = $wpdb->query( $wpdb->prepare( "UPDATE `".USAM_TABLE_COUPON_CODES."` SET ".implode( ', ', $str )." WHERE $where_col = '$where_format' ", array_merge( array_values( $data ), array( $where_val ) ) ) );			
			do_action( 'usam_coupon_update', $this );
		} 
		else 
		{   
			do_action( 'usam_coupon_pre_insert' );			
			
			$coupon = new USAM_Coupon( $this->data['coupon_code'], 'coupon_code' );	
			$id = $coupon->get('id');
			if ( !empty($id) )
			{	
				$this->args = array('col' => 'id', 'value' => $id );
				return $result;				
			}						
			unset( $this->data['id'] );
			if ( empty($this->data['coupon_type']))
				$this->data['coupon_type'] = 'coupon';			
			$this->data['date_insert']      = date( "Y-m-d H:i:s" );		
							
			$this->data = apply_filters( 'usam_coupon_insert_data', $this->data );
			$format = $this->get_data_format(  );
			$this->data_format( );		
			$result = $wpdb->insert( USAM_TABLE_COUPON_CODES, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id );				
			}
			do_action( 'usam_coupon_insert', $this );
		} 		
		do_action( 'usam_coupon_save', $this );

		return $result;
	}
}

function usam_get_coupon( $value, $colum = 'id' )
{	
	$coupon = new USAM_Coupon($value, $colum);	
	$data = $coupon->get_data();	
	if ( empty($data) )
		return array();
	
	return $data;	
}

function usam_update_coupon( $data, $colum = 'id' )
{	
	if ( !isset($data[$colum]) )
		return false;
	
	$value = $data[$colum];
	
	$coupon = new USAM_Coupon( $value, $colum );	
	$coupon->set( $data );	
	return $coupon->save();
}

function usam_insert_coupon( $value )
{	
	$coupon = new USAM_Coupon( $value );	
	$coupon->save();
	return $coupon->get('id');		 
}

function usam_generate_coupon_code( $format = '**********', $type_format = 'n' )
{	
	require_once( USAM_FILE_PATH . '/includes/basket/coupons_query.class.php' );
	switch ( $type_format )
	{
		case 'ln':		
			$chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890';		
		break;
		case 'l':		
			$chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ';			
		break;
		case 'n':		
			$chars = '1234567890';			
		break;
	}
	$str = preg_replace('/[^\*]/', '', $format);
	$strlen = strlen ($str);		
	$code = preg_replace('/[\*]/', '', $format);	
	do 
	{
		$coupon_code = $code.usam_rand_string( $strlen, $chars );	
		$coupons = usam_get_coupons( array( 'coupon_code' => $coupon_code ) );
		if ( empty($coupons) )
			break;
	}	
	while ( true );
	return $coupon_code;
}
?>