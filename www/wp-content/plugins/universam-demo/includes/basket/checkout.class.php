<?php
/**
 * Класс корзины и оформления заказа
 */ 
 
/**
 * возвращает истину или ложь в зависимости от того есть и в корзине товары
  * @since 3.7
 */ 
function usam_have_checkout_items()
{
	global $usam_checkout;
	return $usam_checkout->have_checkout_items();
}

/**
 * Получить текущий элемент
 */
function usam_the_checkout_item() 
{
	global $usam_checkout;
	return $usam_checkout->the_checkout_item();
}


// Получить id текущеей группы
function usam_checkout_item_group_id() 
{
	global $usam_checkout;	
	return $usam_checkout->checkout_item->group;
}

// Получить id текущеей группы
function usam_checkout_item_group_name() 
{
	global $usam_checkout;
	$props_group = usam_get_order_props_group( $usam_checkout->checkout_item->group );
	return $props_group['name'];
}

/**
 * Нужно ли регистрироваться перед покупкой
 * @since 3.8
 */
function usam_show_user_login_form()
{
	if(!is_user_logged_in() && get_option('users_can_register') && get_option('usam_registration_require', 0))
		return true;
	else
		return false;
}
/**
 * проверяет страны в категориях выбранного товара если есть конфликты, продукты этой категории не могут быть отправлены в выбранну страну
 * @since 3.8
 */
function usam_has_category_and_country_conflict()
{
	$conflict = usam_get_customer_meta( 'category_shipping_conflict' );
	return ( ! empty( $conflict ) );
}

/**
 * Нужно ли выводить условия при оформлении заказа
 */
function usam_has_tnc()
{
	if('' == get_option('usam_terms_and_conditions'))
		return false;
	else
		return true;
}

function usam_shipping_details()
{
	global $usam_checkout;
	if ( stristr( $usam_checkout->checkout_item->unique_name, 'shipping' ) != false ) 
		return ' usam_shipping_forms';
	else 
		return "";	
}

function usam_the_checkout_item_error_class( $as_attribute = true ) 
{
	global $usam_checkout;
	
	$class = '';
	if ( $usam_checkout->get_checkout_item_error() ) 
	{
		$class = 'validation-error';
	}
	if ( ($as_attribute == true ) )
		$output = "class='" . $class . usam_shipping_details() . "'";
	else
		$output = $class;	
	return $output;
}

function usam_the_checkout_item_error() 
{
	global $usam_checkout;	
	return $usam_checkout->get_checkout_item_error();
}

function usam_checkout_form_name() 
{
	global $usam_checkout;
	return $usam_checkout->form_name();
}

function usam_checkout_form_element_id() {
	global $usam_checkout;
	return $usam_checkout->form_element_id();
}

function usam_checkout_form_field() 
{
	global $usam_checkout;
	return $usam_checkout->form_field();
}

/**
 * Класс данных покупателя
 */
class USAM_Checkout 
{
	private $checkout_items = array( );
	private $code_location_cart = null;	
	public  $checkout_item;
	private $checkout_item_count = 0;
	private $current_checkout_item = 0;
	private $in_the_loop = false;	
	private $checkout_details = array();	
	private $checkout_error_messages = array();
	
	private $errors = array();
	
	public function __construct(  ) 
	{
				
	}
	
	public function public_part( )
	{		
		global $usam_cart;		
		
		$type_payer = usam_get_customer_meta( 'type_payer' );		
				
		$args = array( 'type_payer' => $type_payer, 'orderby' => array( 'group', 'sort' ) );	
		$this->checkout_items = usam_get_order_properties( $args );	
		
		$category_ids = usam_cart_categories_ids( );
		$groups = array( );
		foreach ( $category_ids as $category_id )
		{
			$category_form = get_term_meta( $category_id, 'usam_order_props_group', true );
			if ( $category_form != '' )
				$groups[] = $category_form;				
		}	
		if ( !empty($groups) )
		{
			$args = array( 'group' => $groups );			
			$properties = usam_get_order_properties( $args );
			foreach ( $properties as $property )			
				$this->checkout_items[] = $property;			
		}						
		$this->checkout_error_messages = usam_get_customer_meta( 'checkout_error_messages' );	
		usam_delete_customer_meta( 'checkout_error_messages'      );		
		$this->checkout_details = apply_filters( 'usam_get_customer_checkout_details', usam_get_customer_meta( 'checkout_details' ) );	
		
		if ( ! is_array( $this->checkout_details ) )
			$this->checkout_details = array();	
		
		// Если пользователь выбрал ввод данных вручную
		$checkout_location = usam_get_customer_meta( 'checkout_location' );	
		foreach( $this->checkout_details as $key => &$value )
		{			
			if ( !empty($checkout_location[$key]) )
			{
				$value = '#';
			}			
		}		
		$this->processing_location();
		$this->checkout_item_count = count( $this->checkout_items );	
	}
	
	// Для профиля
	public function profile_part( $args = array() )
	{
		global $wpdb;
		$this->checkout_details = apply_filters( 'usam_get_customer_checkout_details', usam_get_customer_meta( 'checkout_details' ) );			
				
		$type_payer = usam_get_customer_meta( 'type_payer' );		
		if ( empty($type_payer) )
			$type_payer = $wpdb->get_var( "SELECT type_payer FROM `".USAM_TABLE_ORDER_PROPS_GROUP."` ORDER BY sort ASC LIMIT 1" );		
		
		$default = array( 'type_payer' => $type_payer, 'profile' => 1, 'orderby' => array( 'group', 'sort' ) );		
		
		$args = array_merge($default, $args );		
		$this->checkout_items = usam_get_order_properties( $args );	
		
		$this->processing_location();
		$this->checkout_item_count = count( $this->checkout_items );
	}	
	
	public function get_items( )
	{
		return $this->checkout_items;			
	}	
		
	private function get_type_locations_down( $id )
	{		
		global $wpdb;	
		
		$code = $wpdb->get_var("SELECT tl.code FROM ".USAM_TABLE_LOCATION." AS l LEFT JOIN ".USAM_TABLE_LOCATION_TYPE." AS tl ON ( l.id_type=tl.id ) WHERE l.id='$id' LIMIT 1");
		return $code;		
	}
		
		
	// Отключение ввода типов местоположений верхнего уровня, если местоположение введено покупателем
	private function processing_location( )
	{				
		// Найти все группы где есть местоположение
		$groups = array();
		foreach( $this->checkout_items as $key => $field)
		{			
			if ( $field->type == 'location' )								
				$groups[] = $field->group;									
		}			
		// Удалить у найденных групп типы местоположений	
		foreach( $groups as $id )	
		{
			foreach( $this->checkout_items as $key => $item)
			{
				if ( $item->group == $id)
				{
					if ( $item->type == 'location_type' )
						unset($this->checkout_items[$key]);	
				}
			}
		}		
	}
	
//Вывести названия свойства
	function form_name()
	{
		if ( $this->form_name_is_required() )
			return esc_html( $this->checkout_item->name ).' <span class="asterix">*</span> ';
		else
			return esc_html( $this->checkout_item->name );
	}
	
//Узнать обязательное ли поле к заполнению
	function form_name_is_required()
	{		
		if ( $this->checkout_item->mandatory == 0 )
			return false;
		else 
			return true;
	}
	
	function form_element_id() 
	{
		return 'usam_checkout_form_' . $this->checkout_item->id;
	}
	
	//Список местоположений
	function location_list_by_code( ) 
	{				
		$value = $this->get_customer_details_item();
		$code = str_replace("shipping", "", $this->checkout_item->unique_name);
		$code = str_replace("billing", "", $code);	
		$code = str_replace("company_", "", $code);	
		
		$list = usam_get_locations_by_code( $code );	

		if ( $this->form_name_is_required() )
			$class = 'checkout_item required';
		else
			$class = 'checkout_item';
		
		$output = "<select id = '".$this->checkout_item->unique_name."' title='".$this->checkout_item->name."' name='collected_data[".$this->checkout_item->id."]' data-props_id = '".$this->checkout_item->id."' class = 'location_list chzn-select $class' data-placeholder='".__('Выберете из списка...','usam')."'>\n\r  
		<option value=''>--".__('Не выбрано','usam')."--</option>";	
		foreach ( $list as $item )
		{
			if ( $value == $item->id )
				$selected = "selected='selected'";
			else
				$selected = "";				
			$output .= "<option value='".$item->id."' $selected>".esc_html( $item->name )."</option>\n\r";
		}
		$output .= "</select>\n\r";		
		return $output;
	}
	
	public function get_customer_details_item() 
	{		
		return empty( $this->checkout_details[$this->checkout_item->id] ) ? null : $this->checkout_details[$this->checkout_item->id];
	}
	
	/**
	 * Форма вывода полей информации о покупателе, адрес доставки и проживания, а также и произвольных полей на строницы "Оформление заказа"
	 */
	public function form_field() 
	{						
		$output = '';		
		$checkout_id = $this->checkout_item->id;		
		$name_submit = "collected_data[{$checkout_id}]";	
				
		$class[] = "checkout_item";		
		$class[] = "usam_checkout_field_".$this->checkout_item->type;		
		$class[] = isset($this->checkout_error_messages[$checkout_id])?'usam_checkout_error':'';
		$class[] = $this->form_name_is_required()?'required':'';	
		
		$attributes = "class='".implode(' ',$class)."' ";				
		$attributes .= 'id="' . esc_attr( $this->form_element_id() ) .'"';			
		switch ( $this->checkout_item->type ) 
		{
			case "location":					
				$saved_form_data = usam_get_customer_location();					
				$t = new USAM_Autocomplete_Forms();		
				$t->get_form_position_location( $saved_form_data, array( 'id' => $this->checkout_item->id, 'name' => $name_submit, 'onChange' => 'selected_location' ));
				$output = __('Введите название города или название района если вы проживаете не в городе','usam')."<br><br>";
			break;
			case "location_type":				
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				if ( $saved_form_data != '' && !is_numeric($saved_form_data) )
				{					
					if ( $saved_form_data == '#' )
						$saved_form_data = '';
					$output = "<input title='".$this->checkout_item->name."' type='text' $attributes value='$saved_form_data' name='$name_submit' />";
				}
				else
				{
					$output = $this->location_list_by_code();
					$output .= '<a id="add_inhabited_locality" data-location_id = "'.$checkout_id.'" href="#add_inhabited_locality">'.__('Нет в списке?','usam').'</a>';
				}
			break;	
			case "location_text":			
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				$output = "<input title='".$this->checkout_item->name."' type='text' $attributes value='$saved_form_data' name='$name_submit' />";
			break;	
			case "find_us":			
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				$output = "<select name='$name_submit' $attributes>
						<option value='word_of_mouth' " . selected( 'word_of_mouth', $saved_form_data, false ).">".__('Услышали от кого-либо' , 'usam')."</option>
						<option value='advertisement' " . selected( 'advertisement', $saved_form_data, false ).">".__('Реклама' , 'usam')."</option>
						<option value='internet' " . selected( 'internet', $saved_form_data, false ).">".__('Интернет' , 'usam')."</option>
						<option value='customer' " . selected( 'customer', $saved_form_data, false ).">".__('Рассказал наш клиент' , 'usam')."</option>
					</select>";
			break;	
			case "textarea":
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				$attributes .= !empty($this->checkout_item->mask) ? ' data-mask="' . esc_attr( $this->checkout_item->mask ) .'"' : '';
				$output .= "<textarea title='".$this->checkout_item->name."' $attributes name='$name_submit' rows='3' cols='40' >".esc_html( $saved_form_data )."</textarea>";
			break;
			case "checkbox":			
				if ( !empty($this->checkout_item->options) ) 
				{					
					$saved_form_data = $this->get_customer_details_item();	
					$options = maybe_unserialize($this->checkout_item->options);
					foreach ( $options as $option ) 
					{							
						if ( is_array($saved_form_data) && in_array($option['value'], $saved_form_data) )
							$checked = checked( 1, 1, false );
						else
							$checked = '';
						
						$output .= "<label><input $checked  type='checkbox' $attributes name='{$name_submit}[]' value='".$option['value']."'/>".$option['name']."</label>";
					}
				}
			break;					
			case "select":						
				if ( !empty($this->checkout_item->options) ) 
				{
					$saved_form_data = esc_attr( $this->get_customer_details_item() );	
					$output = "<select name='$name_submit' $attributes>";
					$output .= "<option value='-1'>" . _x( 'Выберите вариант',' Выпадающее меню при вызове на странице оформления заказа' , 'usam' ) . "</option>";
					$options = maybe_unserialize($this->checkout_item->options);
					foreach ( $options as $option ) 
					{
						$output .="<option " . selected( $option['value'], $saved_form_data, false )." value='".$option['value']."'>".esc_html__( $option['name'] )."</option>\n\r";
					}
					$output .="</select>";
				}
			break;
			case "radio":						
				if ( !empty($this->checkout_item->options) ) 
				{
					$saved_form_data = esc_attr( $this->get_customer_details_item() );	
					$options = maybe_unserialize($this->checkout_item->options);
					foreach ( $options as $option ) 
					{
						$output .= "<label><input type='radio' ".checked( $option['value'], $saved_form_data, false )." name='$name_submit' value='".$option['value']."' $attributes />".esc_html__( $option['name'] )."</label>";					
					}
				}
			break;		
			case "mobile_phone":		
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				$mask = "7(999) 999-9999";
				$output = "<input title='".$this->checkout_item->name."' type='text' $attributes value='".esc_attr( $saved_form_data )."'  data-mask='$mask' name='$name_submit' />";
			break;			
			case "phone":	
			case "text":		
			case "email":
			default:				
				$saved_form_data = esc_attr( $this->get_customer_details_item() );	
				$attributes .= !empty($this->checkout_item->mask) ? ' data-mask="' . esc_attr( $this->checkout_item->mask ) .'"' : '';
				$output = "<input title='".$this->checkout_item->name."' type='text' $attributes value='".esc_attr( $saved_form_data )."' name='$name_submit' />";
			break;
		}
		return $output;
	}
	
	public function get_checkout_item_error() 
	{			
		$output = false;
		if ( ! empty( $this->checkout_error_messages ) && !empty( $this->checkout_error_messages[$this->checkout_item->id] ) ) 
		{
			$output = $this->checkout_error_messages[$this->checkout_item->id];		
		}
		return $output;
	}

	/**
	 * Метод проверяет входные данные со страницы Оформить заказ
	 */
	function validate_forms() 
	{
		global $usam_cart, $current_user, $user_ID;
		$any_bad_inputs = false;	
		
		// Сменить плательщика	
		if ( isset( $_REQUEST['type_payer'] ) && is_numeric($_REQUEST['type_payer']) )
		{			
			$properties = array( 'type_payer' => absint($_REQUEST['type_payer']) );
			$usam_cart->set_properties( $properties );			
		}		
		$any_bad_inputs = false;
		// Основная проверка полей формы для выставления счетов	
		foreach ( $this->checkout_items as $item ) 
		{			
			if( isset( $_POST['collected_data'][$item->id] ) )
				$value = stripslashes_deep( $_POST['collected_data'][$item->id] );
			else
				$value = '';			
			if ( $item->mandatory == 1 )
			{	
				if ( empty($value) )
				{
					$this->checkout_error_messages[$item->id] = sprintf(__( 'Пожалуйста, введите <span class="usam_error_msg_field_name">%s</span>.', 'usam' ), esc_attr($item->name) );					
					$any_bad_inputs = true;					
				}				
				else
				{
					$bad_input = false;
					switch ( $item->type ) 
					{
						case "email":
							if ( !preg_match( "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\.[a-zA-Z]{2,5}$/", $value ) ) 
							{
								$any_bad_inputs = true;
								$bad_input = true;
							}						
						break;
						case "phone":
						case "mobile_phone":
							$value = preg_replace('/[^0-9]/', '', $value);						
							if ( empty($value) || strlen($value) < 5 ) 
							{
								$any_bad_inputs = true;
								$bad_input = true;
							}						
						break;
						case "location":					
							if ( !usam_get_location( $value ) ) 
							{					
								$any_bad_inputs = true;
								$bad_input = true;
							}						
						break;				
						case "location_type":				
							if ( $value != '#' ) 
							{					
								$any_bad_inputs = true;
								$bad_input = true;
							}					
						break;
						case "select":
							if ( $value == '-1' ) 
							{
								$any_bad_inputs = true;
								$bad_input = true;
							}
						break;						
					}				
					if ( $bad_input === true )
					{
						$this->checkout_error_messages[$item->id] = sprintf(__( 'Пожалуйста, введите коректный <span class="usam_error_msg_field_name">%s</span>.', 'usam' ), esc_attr($item->name) );
						$value = '';
					}
				}
			}	
			$this->checkout_details[$item->id] = $value;				
		}
		usam_update_customer_meta( 'checkout_error_messages'     , $this->checkout_error_messages     );

		$filtered_checkout_details = apply_filters( 'usam_update_customer_checkout_details', $this->checkout_details );	
		usam_update_customer_meta( 'checkout_details', $filtered_checkout_details );
	
		$any_bad_inputs = apply_filters('usam_checkout_form_validation', $any_bad_inputs);
		return $any_bad_inputs;
	}	

	/**
	 * Следующий элемент
	 */
	function next_checkout_item() 
	{		
		$this->current_checkout_item++;				
		if ( $this->in_the_loop )		
			$this->checkout_item = next($this->checkout_items);	
		else
			$this->checkout_item = current($this->checkout_items);
		return $this->checkout_item;
	}

	function the_checkout_item() 
	{		
		$this->next_checkout_item();
		$this->in_the_loop = true;
		if ( $this->current_checkout_item == 0 )
			do_action( 'usam_checkout_loop_start' );
	}

	function have_checkout_items() 
	{	
		if ( !$this->in_the_loop )
			reset($this->checkout_items);
		if ( $this->current_checkout_item < $this->checkout_item_count ) 
			return true;		
		elseif ( $this->current_checkout_item == $this->checkout_item_count && $this->checkout_item_count > 0 ) 
		{
			do_action( 'usam_checkout_loop_end' );			
			$this->rewind_checkout_items();
		}
		$this->in_the_loop = false;
		return false;
	}

	// Предыдущий элемент
	function rewind_checkout_items() 
	{
		global $checkout_error_messages;
		$this->checkout_error_messages = array();
		usam_delete_customer_meta( 'checkout_error_messages' );
		$this->current_checkout_item = 0;
		if ( $this->checkout_item_count > 0 )
			$this->checkout_item = reset($this->checkout_items);		
	}
}



// Получить сообщения по кодам
function usam_get_errors_checkout(  ) 
{
	$return = array();
	$errors = (array)usam_get_user_screen_error( 'checkout' );	
	
	$cart_errors = usam_cart_errors_message();	
	$errors = array_merge ($errors, $cart_errors);	
	if ( !empty($errors) )
	{
		foreach( $errors as $error )	
		{
			switch ( $error ) 
			{
				case "gateway":
					$return[] = __( 'Вы должны выбрать способ оплаты, в противном случае мы не можем обработать ваш заказ.', 'usam' );
				break;
				case "shipping_method":
					$return[] = __( 'Вы должны выбрать способ доставки, в противном случае мы не можем обработать ваш заказ.', 'usam' );
				break;
				case "has_tnc":
					$return[] = __( 'Пожалуйста, согласитесь с условиями, в противном случае мы не можем обработать ваш заказ.', 'usam' );
				break;
				case "verify_nonce":
					$return[] = __( 'Сеанс устарел. Попробуйте еще раз.', 'usam' );
				break;	
				case "validate_forms":
					$return[] = __( 'Заполните все поля формы.', 'usam' );
				break;					
				case "target_market":
					
				break;		
				case "save_order":
					$return[] = __( 'Не возможно создать заказ. Попробуйте еще раз или свяжитесь с нами...', 'usam' );
				break;				
				case "cart_empty":
					$return[] = __( 'Нет ни одного товара в корзине. Пожалуйста, добавьте товар в корзину и попробуйте еще раз, в противном случае мы не можем обработать ваш заказ.', 'usam' );
				break;					
			}				
		} 
	}
	return $return;
}

// Получить типы плательщиков
function usam_get_group_payers( $sort = true ) 
{
	$option = get_option('usam_types_payers',array());
	$types_payers = maybe_unserialize($option);		
	
	if ( $sort )	
		usort($types_payers, function($a, $b){  return ($a['sort'] - $b['sort']); });		
		
	return $types_payers;
}

// Получить имя плательщика
function usam_get_name_payer( $type_payer_id )
{		
	$type_payer = usam_get_payer( $type_payer_id );
	if ( !empty($type_payer) )
		$name = $type_payer['name'];
	else
		$name = '';		
	return $name;
}

// Тип плательщика пользователя
function usam_get_type_payer_user( $user = null )
{		
	if ( $user == null )
	{
		global $user_ID;		
		$user = $user_ID;		
	}
	$type_payer_id = usam_get_customer_meta( 'type_payer', $user );	
	
	$type_payer = usam_get_payer( $type_payer_id );
	if ( !empty($type_payer) )
		$type = $type_payer['type'];
	else
		$type = 'I';		
	return $type;
}

// Тип плательщика компания
function usam_is_type_payer_company( $type_payer_id )
{	
	$type_payer = usam_get_payer( $type_payer_id );
	if ( !empty($type_payer) && $type_payer['type'] == 'E' )
		$type = true;
	else
		$type = false;		
	return $type;
}

function usam_get_payer( $type_payer_id )
{	
	$option = get_option('usam_types_payers',array());
	$types_payers = maybe_unserialize($option);	
	$type_payer = array();		
	foreach( $types_payers as $value )
	{						
		if ( $type_payer_id == $value['id'] )
		{
			$type_payer = $value;
			break;
		}		
	}
	return $type_payer;
}
?>