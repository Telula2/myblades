<?php
// Класс корзины товаров

function usam_cart_errors_message() 
{
	global $usam_cart;
	return $usam_cart->get_errors_message();
}
/**
* Показать формы для ввода купонов
*/
function usam_uses_coupons() 
{	
	return get_option( 'usam_uses_coupons', 1 );
}

function usam_coupons_code()
{
	global $usam_cart;	
	if ( $usam_cart->coupon_name != '' )		
		return $usam_cart->coupon_name;	
}

function usam_cart_get_select_storage_address()
{
	global $usam_cart;	
	
	$storage_pickup = $usam_cart->get_properties( 'storage_pickup' );
	if ( !empty($storage_pickup) )
	{
		$store = usam_get_storage($usam_cart->get_properties( 'storage_pickup' ));	
		if ( isset($store['address']) )		
		{
			$location = usam_get_location( $store['location_id'] );			
			$city = isset($location['name'])?$location['name']:'';
			echo __('город', 'usam').' '.$city.' '.$store['address'];	
		}
		else
			_e('магазин', 'usam');	
	}
	else
		echo "<span class ='validation-error'>".__('не выбран', 'usam')."</span>";	
}

function usam_get_basket_number_items()
{
	global $usam_cart;		
	return $usam_cart->get_cart_product_count();
}

function usam_get_basket_subtotal( $forDisplay = true )
{
	global $usam_cart;	
	
	 if( $forDisplay == true )
		$output = usam_currency_display( $usam_cart->get_properties( 'subtotal' ) );
   else 
		$output = $usam_cart->get_properties( 'subtotal' );	
	
	return $output;
}



/*
 * Скидка по купону
 */
function usam_coupon_amount( $forDisplay = true ) 
{
   global $usam_cart;

   if( $forDisplay == true )
		$output = usam_currency_display( $usam_cart->get_properties( 'coupons_amount' ) );
   else 
		$output = $usam_cart->get_properties( 'coupons_amount' ); 
   return $output;
}

/*
 * Скидка корзины
 */
function usam_discount_cart( $forDisplay = true ) 
{
   global $usam_cart;

   $discount = $usam_cart->get_properties( 'discount' );
   if( $forDisplay == true )
		$output = usam_currency_display( $discount);
   else 
		$output =  $discount; 
   return $output;
}

/*
 * Бонусы корзины
 */
function usam_bonuses_cart( $forDisplay = true ) 
{
   global $usam_cart;

   $discount = $usam_cart->get_bonuses();   
   if($forDisplay == true)
		$output = usam_currency_display( $discount);
   else 
		$output =  $discount; 
   return $output;
}


/**
 * Вернуть полную стоимость корзины
 */
function usam_cart_total( $forDisplay = true ) 
{
    global $usam_cart;   
   	
	$total = $usam_cart->get_properties( 'total_price' );	
    if( $forDisplay )
        return usam_currency_display( $total );
    else
        return $total;
}

/**
 * Виджет корзины
 * Может использоваться для отображения Корзины за исключением исключением доставки, налогов или купонов.
 */
function usam_cart_total_widget( $shipping = true, $tax = true, $bonuses = true, $coupons_amount = true ) 
{
	global $usam_cart;
	
	$subtotal = $usam_cart->get_properties( 'subtotal' );
	$total = $subtotal;

	if ( $shipping )
		$total += $usam_cart->get_properties( 'shipping' );   	

	if ( $bonuses ) 
		$total -= $usam_cart->get_properties( 'bonuses' );
	
	if ( $coupons_amount ) 
		$total -= $usam_cart->get_properties( 'coupons_amount' );	
	
	return usam_currency_display( $total );  
}

/**
* Вес всей корзины
*/
function usam_cart_weight_total() 
{
	global $usam_cart;
	return $usam_cart->calculate_total_weight();
}

/**
* Существует налог корзины
*/
function usam_cart_tax_enabled( )
{
   global $usam_cart;
   $total_tax = $usam_cart->get_properties( 'total_tax' );  
   if( $total_tax > 0 )
		return true;
   else
      return false;
}

/**
* Вернуть налог корзины
*/
function usam_cart_tax( $forDisplay = true)
{
   global $usam_cart;
   $total_tax = $usam_cart->get_properties( 'total_tax' );  
   if( $forDisplay )
		return usam_currency_display( $total_tax );
   else
      return  $total_tax;
}

/**
* Получить стоимость доставки
*/
function usam_cart_shipping() 
{
   global $usam_cart;
   return apply_filters( 'usam_cart_shipping', usam_currency_display( $usam_cart->get_properties( 'shipping' ) ) );
}


/**
* Узнать количество категорий добавленных товаров в корзину
*/
function usam_cart_categories_ids( ) 
{
   global $usam_cart;  
	return $usam_cart->get_cart_category_ids();
}

/**
* Получить следующий товар корзины
*/
function usam_have_cart_items() 
{
   global $usam_cart;
   return $usam_cart->have_cart_items();
}

function usam_the_cart_item() 
{
   global $usam_cart;
   return $usam_cart->the_cart_item();
}

/**
* Получить текущий товар корзины
*/
function usam_the_cart_item_key() 
{
   global $usam_cart;
   return $usam_cart->cart_item->id;
}

 /**
* Получить название текущего товара в корзине
*/
function usam_cart_item_name( ) 
{
	global $usam_cart;	
	
	$title = $usam_cart->cart_item->name;
	$title = apply_filters( 'the_title', $title );
	
	$product_name = apply_filters( 'usam_cart_item_name', $title, $usam_cart->cart_item->product_id );
	return $product_name;
}


function usam_cart_item_sku( ) 
{
	global $usam_cart;	
	
	$sku = $usam_cart->cart_item->sku;
	return $sku;
}


 /**
* Получить ID текущего товара в корзине
*/
function usam_cart_item_product_id() 
{
   global $usam_cart;
   return $usam_cart->cart_item->product_id;
}

/**
* Получить количество текущего товара в корзине
*/
function usam_cart_item_quantity() 
{
   global $usam_cart;
   return $usam_cart->cart_item->quantity;
}

/**
* Цена товара в корзине
*/
function usam_cart_item_price( $forDisplay = true ) 
{
	global $usam_cart;
	
	$total = ($usam_cart->cart_item->price + $usam_cart->cart_item->tax )* $usam_cart->cart_item->quantity;	
	if( $forDisplay )   
		return usam_currency_display($total);
	else
		return $total;   
}

/**
 * Получить цену товара 
 */
function usam_cart_single_item_price($forDisplay = true)
 {
	global $usam_cart;
	if( $forDisplay )
		return usam_currency_display( $usam_cart->cart_item->price );
	else
		return $usam_cart->cart_item->price;
}

/**
 * Получить цену товара 
 */
function usam_cart_single_item_discont($forDisplay = true)
{
	global $usam_cart;
	if( $usam_cart->cart_item->old_price )
		$price = $usam_cart->cart_item->old_price - $usam_cart->cart_item->price;
	else
		$price = 0;
	
	if( $forDisplay )
		return usam_currency_display( $price );
	else
		return $price;
}

function usam_cart_single_item_oldprice($forDisplay = true)
{
	global $usam_cart;
	if( $usam_cart->cart_item->old_price )
		$price = $usam_cart->cart_item->old_price;
	else
		$price = $usam_cart->cart_item->price;
	
	if( $forDisplay )
		return usam_currency_display( $price );
	else
		return $price;
}

/**
 * Получить цену товара 
 */
function usam_get_cart_single_item_tax( $forDisplay = true )
 {
	global $usam_cart;
	if( $forDisplay )
		return usam_currency_display( $usam_cart->cart_item->tax );
	else
		return $usam_cart->cart_item->tax;
}

/**
* Получить ссылку на товар
*/
function usam_cart_item_url() 
{
	global $usam_cart;
	$product_url = usam_product_url( $usam_cart->cart_item->product_id );
	return apply_filters( 'usam_cart_item_url', $product_url, $usam_cart->cart_item->product_id );
}

/**
* возвращает URL к изображению для продукта в корзине
*/
function usam_cart_item_image( $width = 100, $height = 100 )
{
	global $usam_cart;
	return usam_get_product_thumbnail($usam_cart->cart_item->product_id, array($width, $height), usam_cart_item_name() ); 
}

/**
* Получить следующий метод доставки
*/
function usam_have_shipping_methods() 
{
   global $usam_cart;
   return $usam_cart->have_shipping_methods();
}
/**
* Получить текущий метод доставки
*/
function usam_the_shipping_method() 
{
   global $usam_cart; 
   return $usam_cart->the_shipping_method();
}

// Получить url на картинку доставки
function usam_shipping_method_url() 
{
	global $usam_cart;
	$shipping_method = $usam_cart->get_properties( 'shipping_method' );
	$url = USAM_CORE_IMAGES_URL."/shipping.jpg";
	if ( !empty($shipping_method->img) )
	{
		$image_attributes = wp_get_attachment_image_src( $shipping_method->img, 'thumbnail' );			
		if ( !empty($image_attributes[0]) )
		{
			$url = $image_attributes[0];
		}	
	}	
	return $url;
}

// Текущий способ доставки самовывоз
function usam_shipping_method_pickup() 
{
	global $usam_cart;
   
    $shipping_method = $usam_cart->get_properties( 'shipping_method' );  
	if ( !empty($shipping_method->setting['stores']) )
		return true;	
	return false;
}

// Выбран ли текущий в цикле способ доставки
function usam_shipping_method_select() 
{
	global $usam_cart;	
    $shipping_method = $usam_cart->get_properties( 'shipping_method' );	
	if ( $shipping_method->id == $usam_cart->get_properties( 'selected_shipping' ) )
		return true;	
	return false;
}
/**
* Получить текущий метод доставки в цикле
*/
function usam_shipping_method_internal_name() 
{
	global $usam_cart;
	return $usam_cart->get_properties( 'shipping_method' );
}

/**
* have shipping quotes function, no parameters
*/
function usam_have_shipping_quotes()
 {
   global $usam_cart;
   return $usam_cart->have_shipping_quotes();
}

/**
* the shipping quote function, no parameters
*/
function usam_the_shipping_quote()
{
   global $usam_cart;
   return $usam_cart->the_shipping_quote();
}

function usam_selected_location()
{
	global $usam_cart;
	$location = $usam_cart->get_properties( 'location' );
	if ( $location )
		return true;
	else
		return false;
}	

function usam_virtual_products_in_basket()
{
	global $usam_cart;		
	if ( $usam_cart->check_virtual_products() )	
		return true;
	else
		return false;
}	


/**
 * Класс корзины
 */
class USAM_CART
{	
	private static $string_cols = array(				
		'date_modified',
		'date_insert',	
		'coupon_name',	
		'errors',	
	);
	private static $int_cols = array(
		'id',		
		'user_id',	
		'shipping_method',		
		'gateway_method',	
		'storage_pickup',
	);	
	private $version = 1;	
	
	private $location = null;	
	private $location_ids = array();
	
	private $order_id = 0;
	private $id = null;	
	private $user_id;		
	
	private $type_payer = 0;
	private $storage_pickup = null; // Склад самовывоза	
	
	private $total_tax = null;
	private $shipping = null;	
	private $total_price = null;
	private $accumulative_discount = null;
	private $bonuses = 0;
	private $discount = 0;	
	private $subtotal = null;	
	private $cart_item_count = 0;	

	private $type_price;

	public  $cart_item;
	private $cart_items     = array();
	private $cart_items_keys = array();	
	
	private $current_cart_item = -1;
	private $in_the_loop = false;	
	
	private $selected_shipping = null; 
	private $selected_gateway = null;
	
	private $shipping_methods = array();
	private $shipping_method;
	private $shipping_method_count = 0;
	private $current_shipping_method = -1;	
	
	private $include_in_cost_shipping = true;		

	// Переменные цикла модулей доставки
	public $shipping_quotes = array();
	public $shipping_quote;
	public $shipping_quote_count = 0;
	public $current_shipping_quote = -1;

	public  $coupon_name = '';
	public  $coupons_amount = 0;	
	
	private $accrue_bonuses;
	
	private $coupons_error = false;	
	private $errors = array();	

	public function __construct( $cart_id = null, $args = array() ) 
	{		
		global $type_price, $user_ID;
		$this->type_price = $type_price;
		$this->user_id = $user_ID;
	
		if ( is_array( $cart_id) )
		{
			$args = $cart_id;		
			$cart_id = null;
		}		
		$this->save_properties( $args );			
		if ( !empty($cart_id)  )
		{
			$this->id = (int)$cart_id;			
			$data = $this->get_cart( );		
			if ( empty($data) )						
			{							
				$this->insert_cart();				
			}	
			else
			{						
				$this->selected_shipping = $data['shipping_method'];
				$this->storage_pickup = $data['storage_pickup'];				
				$this->selected_gateway = $data['gateway_method'];
				$this->coupon_name = mb_strtoupper($data['coupon_name']);
				$this->order_id = $data['order_id'];
				$this->errors = (array)maybe_unserialize( $data['errors'] );				
	
				$cart_items = usam_get_products_basket( $this->id );			
				if ( !empty($cart_items) )
				{
					$this->get_accumulative_discount( ); // Получить накопительную скидку	
					$post_ids = array();
					foreach( $cart_items as &$cart_item )
					{
						$post_ids[] = $cart_item->product_id;
						
						$cart_item->old_price = (float)$cart_item->old_price;
						$cart_item->price = (float)$cart_item->price;
				
						$this->cart_items[$cart_item->id] = $cart_item;
					}			
					update_meta_cache('post', $post_ids);
					$cache_key = $this->id.'_cart_item';		
					foreach( $this->cart_items as $key => $cart_item )
					{				
						wp_cache_set( $cache_key, $cart_item, $cart_item->product_id );				
					}		
					$this->cart_item_count = count($this->cart_items);
					$this->calculate_subtotal();						
						
					$this->refresh_cart_items();	
					$this->update_location();	
					$this->apply_coupons( );	
					$this->calculate_total_price();	
				}									
			}			
		}		
	//	else
	//		$this->insert_cart();
		
		
	}	
	
	/*
    * Создать корзину
    */
	public function get_cart( )
	{
		global $wpdb;
	
		$sql = "SELECT * FROM ".USAM_TABLE_USERS_BASKET." WHERE id ='$this->id' ";		
		$cache_key = 'usam_users_basket';
		if( ! $cache = wp_cache_get($cache_key, $this->id ) )
		{		
			$cache = $wpdb->get_row( $sql, ARRAY_A );
			wp_cache_set( $cache_key, $cache, $this->id );
		}		
		return $cache;
	}	
		
	// Пересчитать заказ
	public function set_order( $order_id )
	{
		if ( $this->id == null )
			$this->insert_cart();
		
		$this->order_id = (int)$order_id;
		
		$order = new USAM_Order( $this->order_id );	
		$order_data = $order->get_data();
		$cart_product = $order->get_order_products();	
		$customer_data = $order->get_customer_data();

		$properties =  array( 'bonus' => $order_data['bonus'], 'coupon_name' => $order_data['coupon_name'] );
		if ( isset($customer_data['shippinglocation']) )
			$properties['location'] = $customer_data['shippinglocation']['value'];
		
		$properties['type_price'] = $order_data['type_price'];		
		$properties['type_payer'] = $order_data['type_payer'];	
		$properties['user_id'] = $order_data['user_ID'];
	
		usam_delete_order_discount( $this->order_id, 'order_id' );

		$this->save_properties( $properties );		
	
		$parameters['any_balance'] = true;
		$order_products = array();		
		foreach ( $cart_product as $product ) 
		{					
			$new_data['old_price'] = usam_get_product_old_price( $product->product_id, $this->type_price);
			$new_data['price']     = usam_get_product_price( $product->product_id, $this->type_price );
			$new_data['quantity']  = $product->quantity;			
		
			$result = $this->insert_basket_product( $product->product_id, $new_data );				
			if( $result ) 
			{							
				$order_products[$product->product_id] = $product->id;				
			}								
		}			
		$this->calculate_subtotal();
		$this->update_location();	
		$this->apply_coupons( );
		$this->cart_item_count = count($this->cart_items);		
		
		$this->calculate_total_price();
		
		$args =  array(
				'totalprice'          => $this->total_price,					
				'coupon_name'         => $this->coupon_name,		
				'coupon_discount'     => $this->coupons_amount,								
				'bonus'               => $this->bonuses,		// Общие бонусы	
				'total_tax'           => $this->total_tax,				
			);	
		$products = array();		
		foreach ( $this->cart_items as $product ) 	
		{				
			$products[$order_products[$product->product_id]] = array( 'price' => $product->price, 'old_price' => $product->old_price );
		}		
		$order->edit_order_products( $products );				
		$order->set( $args );	
		$order->save();
		
		$this->order_id = $order->get('id');	
		$this->set_order_discounts( );	
	} 
	
	public function update( )
	{
		global $wpdb;

		do_action( 'usam_cart_pre_update', $this );			
		
		$result = false;	
		
		$where_col = 'id';		
		
		$data['user_ID'] = $this->user_id;
		$data['date_modified'] = date( "Y-m-d H:i:s" );		
		$data['quantity'] = count($this->cart_items);	
		$data['totalprice'] = $this->total_price;	
		$data['shipping_method'] = (int)$this->selected_shipping;
		$data['storage_pickup'] = (int)$this->storage_pickup;		
		$data['gateway_method'] = (int)$this->selected_gateway;	
		$data['coupon_name'] = $this->coupon_name;		
		$data['errors'] = serialize($this->errors);		
		$data['order_id'] = (int)$this->order_id;			
	
		$data = apply_filters( 'usam_cart_update_data', $data );	
		$format = $this->get_data_format( $data );	

		$where[$where_col] = $this->id;		
		$where_format = $this->get_data_format( $where );			
		
		$result = $wpdb->update( USAM_TABLE_USERS_BASKET, $data, $where, $format, $where_format );	
		if ( $result ) 
		{
			
		}		
		do_action( 'usam_cart_update', $this );		

		return $result;
	}	
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}	
	
	private function get_data_format( $data ) 
	{
		$format = array();
		foreach ( $data as $key => $value ) 
		{			
			$format[$key] = self::get_column_format( $key );
		}
		return $format;
	}
	
	// создание корзины	
	public function insert_cart()
	{
		global $wpdb;

		do_action( 'usam_cart_pre_insert' );
	
		$this->empty_cart();
		
		$result = false;		
		
		$data['user_ID']       = $this->user_id;
		$data['date_modified'] = date( "Y-m-d H:i:s" );
		$data['date_insert']   = date( "Y-m-d H:i:s" );			
		$data['shipping_method'] =(int)$this->selected_shipping;
		$data['gateway_method'] = (int)$this->selected_gateway;
		$data['storage_pickup'] = (int)$this->storage_pickup;	
		$data['errors']         = serialize($this->errors);
		$data['coupon_name'] = $this->coupon_name;	

		$data = apply_filters( 'usam_cart_insert_data', $data );
		$format = $this->get_data_format( $data );		
		$result = $wpdb->insert( USAM_TABLE_USERS_BASKET, $data, $format );
		if ( $result ) 
		{
			$this->id = $wpdb->insert_id;				
		}
		do_action( 'usam_cart_insert', $this );
		return $result;
	}	
	
	public function get_properties( $properties ) 
	{
		$return = null;
		if ( isset($this->$properties))
			$return = $this->$properties;		
		return $return;
	}
	
	private function save_properties( $properties ) 
	{		
		if ( !empty($properties) && is_array($properties) )
		{	
			foreach( $properties as $key => $value )
			{				
				if ( property_exists($this, $key) && ( is_array($this->$key) || $this->$key != $value ) )
				{				
					switch( $key ) 
					{																
						case 'coupon_name':								
							$this->$key = mb_strtoupper($value);				
						break;					
						case 'storage_pickup':													
						case 'selected_shipping':	
						case 'user_id':
						case 'order_id':						
						case 'selected_gateway':										
						case 'type_payer':		
						case 'accumulative_discount':	
						case 'location':							
							$this->$key = (int)$value;									
						break;						
						case 'type_price':						
							$this->$key = $value;								
						break;											
					}	
				}	
				else
					unset($properties[$key]);
			}	
			return $properties;	
		}
		return array();	
    }
	
	
	public function set_properties( $properties ) 
    {			
		$properties = $this->save_properties( $properties );
		if ( empty($properties) )
			return false;
	
		$update_location = false;
		$update_coupons = false;
		$update_code_price = false;	
		$update_shipping = false;			
		
		foreach( $properties as $key => $value )
		{				
			if ( $key == 'location' )
				$update_location = true;
			
			if ( $key == 'coupon_name' )
				$update_coupons = true;
			
			if ( $key == 'selected_shipping' )
				$update_shipping = true;		
			
			if ( $key == 'type_price' )
				$update_code_price = true;				
		}	
		if ( $update_code_price )
			$this->refresh_cart_items( );	
		if ( $update_location )
			$this->update_location( );	
		if ( $update_shipping )
			$this->update_shipping( );		
		if ( $update_coupons )
			$this->apply_coupons( );		
				
		$this->calculate_total_price();	
		$this->update();	
		
		return true;
    }

	/**
	* Обновить регион доставки
	*/
	private function update_location( ) 
	{	
		$shop_location = get_option( 'usam_shop_location' );		
		if ( $this->location === null )
		{
			$this->location = $shop_location;				
		}
		else
		{			
			$location_data = usam_get_location( $this->location );
			if ( empty($location_data) )
				$this->location = $shop_location;					
		}		
		$this->location_ids = usam_get_customer_location_array_up( ); 				
		$this->set_shipping_methods();				
	}
     
  /**
   * Получить доступные методы доставки
   */   
	function set_shipping_methods() 
	{	
		$this->shipping_methods = array();
		
		if ( $this->subtotal == 0 )
		{			
			return true;
		}
		if ( $this->check_virtual_products() )		
		{
			$this->selected_shipping = 0;	
			return true;
		}			
		$price = $this->subtotal - ( $this->coupons_amount + $this->bonuses + $this->discount );	
		$weight = $this->calculate_total_weight();
		
		$restrictions = array( 'price' => $price, 'weight' => $weight, 'locations' => $this->location_ids );												
		$ds = new USAM_Delivery_Services( $restrictions );										
		$this->shipping_methods = $ds->get_delivery_services();
		$selected_shipping = false;
		foreach( $this->shipping_methods as $value )
		{
			if ( $this->selected_shipping == $value->id )
			{
				$selected_shipping = true;
				break;
			}
		}	
		$this->shipping_method_count = count($this->shipping_methods);	
		
		if ( !$selected_shipping && $this->shipping_method_count > 0 )
			$this->selected_shipping = $this->shipping_methods[0]->id;	
	}
	
	public function check_shipping_method() 
	{
		if ( $this->check_virtual_products() )
			return true;
		
		$selected_shipping = $this->get_selected_shipping_method();
		if ( !empty($selected_shipping) ) 
			return true;
		else
			return false;
	}	
	
	private function get_shipping_method( $method_id )
	{
		$shipping_method = array();
		if ( empty($method_id) )
			return $shipping_method;
		
		foreach( $this->shipping_methods as $shipping )
		{
			if ( $shipping->id == $method_id )
			{
				$shipping_method = $shipping;
				break;
			}			
		}
		return $shipping_method;
	}  
	
	/**
    * Получить параметры выбранного способа доставки
   */
	public function get_selected_shipping_method()
	{
		$selected_shipping = array();
		if ( empty($this->selected_shipping) )
			return $selected_shipping;
		
		return $this->get_shipping_method( $this->selected_shipping );
	}  
	
	/**
    * Получить стоимость доставки
   */
	private function get_shipping_cost( $method_id )
	{					
		$shipping_method = $this->get_shipping_method( $method_id );
		if ( empty($shipping_method) )
		{
			$shipping = 0;				
		}	
		if ( !empty($shipping_method->setting['include_in_cost']) )
			$this->include_in_cost_shipping = false;
		
		$shipped_instance = usam_get_shipping_class( $method_id );
		
		$args['weight'] = $this->calculate_total_weight();		
		$args['location'] = $this->location;
		
		$result = $shipped_instance->get_shipping_cost( $args );	
		if ( $result !== false )
			$shipping = $result;
		else
		{
			$shipping = 0;	
			$this->set_error( __('Не возможно посчитать доставку. Стоимость доставки вам сообщит менеджер.','usam') );
			$this->set_error( $shipped_instance->get_errors() );
		}
		return $shipping;				
	}  
		
	/*
	* Обновить доставку
	*/
	private function update_shipping( )
	{	
		$selected_shipping = $this->get_selected_shipping_method();
		if ( !empty($selected_shipping) )
		{						
			if ( !empty($selected_shipping->setting['stores']) )	
			{
				if ( empty($this->storage_pickup) )	
				{
					reset($selected_shipping->setting['stores']);
					$this->storage_pickup = current($selected_shipping->setting['stores']);	
				}		
			}
			else
			{					
				$this->storage_pickup = null;				
			}			
		}
		else
			$this->selected_shipping = '';		
	}
		
	/**
    * Применить купон
    */
	function apply_coupons( )
	{	
		$discount = 0;			
		if ( $this->coupon_name != '' )
		{
			$_coupon = new USAM_Coupon( $this->coupon_name, 'coupon_code' );
			$coupon_data = $_coupon->get_data();		
						
			if ( $this->validate_coupon( $coupon_data ) )
			{		
				$this->cancel_reserve_bonuses();		
				
				if ( $coupon_data['action'] == 's' ) //Бесплатная доставка
					$sum = $this->get_shipping_cost( $this->selected_shipping );
				else
				{
					$return = $this->check_condition( $coupon_data['condition'] ); // получить товары, которые удовлетворяют условиям купона			
					$sum = $return['sum'];
				}				
				if ( $sum )
				{
					if ( $coupon_data['is_percentage'] == 1  ) 			
						$discount = $coupon_data['value'] * $sum / 100;  			
					else 					
						$discount = $coupon_data['value'];					
					$discount = usam_round_price( $discount, $this->type_price );	
				}			
			}
			else
				$this->coupon_name = '';
		}
		$this->coupons_amount = $discount;		
		return apply_filters( 'usam_coupons_amount', $discount, $this->coupon_name );
  	}


	/**
	 * Проверяет, является ли текущий купон действителен для использования (Дата окончания действия, активность, использование ).
	 */
	function validate_coupon( $coupon_data ) 
	{						
		$this->coupons_error = true;
		if ( !empty($coupon_data) )
		{					
			if ( $coupon_data['customer'] == 0 || $this->user_id == $coupon_data['customer'] )
			{
				if ( $coupon_data['active'] == '1' )
				{
					$now = time();		
					if ((strtotime($coupon_data['start']) < $now) && (strtotime($coupon_data['expiry']) > $now))
					{					
						$this->coupons_error = false;
						return true;	
					}
					else
					{ 
						$this->set_error( __('Время действия купона истекло или еще не наступило','usam') );		
					}
				}
				else
					$this->set_error( __('Купон не активен','usam') );		
			}
		}
		else
				$this->set_error( __('Купон не действителен','usam') );
		return false;
	}	
  
   /**
    * Метод добавляет товар
    */	
	public function add_product_basket( $product_id, $parameters )
	{		
		$result = false;				
		
		$default = array( 'any_balance' => false, 'quantity' => 1, 'group_storage' => 'stock' );
		$parameters = array_merge ($default, $parameters);
		$post = get_post( $product_id );		
		if ( $post->post_type == 'usam-product' && ( $parameters['any_balance'] || $this->check_remaining_quantity( $product_id, $parameters['quantity'], $parameters['group_storage'] ) ) )
		{			
			do_action('usam_set_cart_item' , $product_id , $parameters , $this);				
			
			$price = usam_get_product_price( $product_id, $this->type_price);
			$old_price = usam_get_product_old_price( $product_id, $this->type_price);			
			if ( $this->accumulative_discount > 0 )
			{ 				
				if ( $old_price == 0)
					$old_price = $price;
				
				$price = $price - ($price * $this->accumulative_discount / 100);
				$price = usam_round_price( $price, $this->type_price );					
			}			
			$new_data['price']     = $price;
			$new_data['old_price'] = $old_price;	
			$new_data['quantity']  = $parameters['quantity'];								
			
			if( $this->insert_basket_product( $product_id, $new_data ) ) 
			{			
				$this->recalculation_changing_basket_products();
				$result = true;
			}
			else
			{
				$_cart_item = new USAM_Cart_Item( $product_id, $this->id, $this );	
				$cart_item_data = $_cart_item->get_data();					
				$result = $this->update_quantity( $cart_item_data->id, $parameters['quantity'] );	
			}
		}			
		return $result;
	}	
	
   /**
    * Метод изменения товара в корзине    
   */
	function update_quantity( $basket_id, $new_quantity )
	{	
		$result = false;					
		if( !empty($this->cart_items[$basket_id]->product_id) ) 
		{	
			$product_id = $this->cart_items[$basket_id]->product_id;
			$quantity = $this->cart_items[$basket_id]->quantity + $new_quantity;	
			if( $quantity >= 1 && ( $new_quantity < 0 || $this->check_remaining_quantity( $product_id, $quantity) ) )
			{								
				$new_data['quantity'] = $quantity;							
				$result = $this->update_basket_product( $basket_id, $new_data );					
				if( $result ) 		
					$this->recalculation_changing_basket_products();
			}		
		} 			
		return $result;		
	}
			
	function insert_basket_product( $product_id, $new_data ) 
	{		
		if ( $this->id == null )
			$this->insert_cart();
		
		$result = false;		
		$_cart_item = new USAM_Cart_Item( $product_id, $this->id, $this );	
		$cart_item_data = $_cart_item->get_data();		
		if ( empty($cart_item_data) )
		{	
			if ( isset($new_data['price']) )
				$new_data['tax'] = $this->get_calculate_tax( $new_data['price'], $product_id );			
		
			$result = $_cart_item->set( $new_data );		
			if ( $result )		
			{
				$cart_item_data = $_cart_item->get_data();				
				$this->cart_items[$cart_item_data->id] = $cart_item_data;	
				$result = $cart_item_data->id;	
	
				$this->total_tax += $cart_item_data->tax * $cart_item_data->quantity;							
				$this->cart_item_count = count($this->cart_items);

				do_action( 'usam_add_item', $product_id, $cart_item_data->id, $new_data, $this );					
			}			
		}
		return $result;
	}
	
	function update_basket_product( $basket_id, $new_data ) 
	{		
		$result = false;
		if ( isset($this->cart_items[$basket_id]->product_id) )
		{
			$product_id = $this->cart_items[$basket_id]->product_id;	
			
			if ( isset($new_data['price']) )
				$new_data['tax']  = $this->get_calculate_tax( $new_data['price'], $product_id );	

			$_cart_item = new USAM_Cart_Item( $product_id, $this->id, $this );		
			$result = $_cart_item->set( $new_data );
		
			if ( $result )		
			{
				$this->cart_items[$basket_id] = $_cart_item->get_data();
				do_action( 'usam_edit_item', $product_id, $new_data, $basket_id, $this );	
			}
		}
		return $result;
	}
	
	   /**
    * Обновление корзины
    */
	private function refresh_cart_items() 
	{
		foreach ( $this->cart_items as &$cart_item ) 
		{					
			if ( !$cart_item->gift )
			{					
				if ( $this->accumulative_discount > 0 )
				{ 
					$price = usam_get_product_price( $cart_item->product_id, $this->type_price);
					$old_price = usam_get_product_old_price( $cart_item->product_id, $this->type_price);
					if ( $old_price == 0)
						$old_price = $price;
					
					$price = $price - ($price * $this->accumulative_discount / 100);
					$price = usam_round_price( $price, $this->type_price );					
				}
				else
				{
					$price = usam_get_product_price( $cart_item->product_id, $this->type_price);
					$old_price = usam_get_product_old_price( $cart_item->product_id, $this->type_price);
				}
				$new_data['price']     = $price;
				$new_data['old_price'] = $old_price;			
				
				$_cart_item = new USAM_Cart_Item( $cart_item->product_id, $this->id, $this );		
				$result = $_cart_item->refresh_item( $new_data );
			}
		}		
		$this->calculate_subtotal();		
	} 	
	
	private function recalculation_changing_basket_products( )
	{
		$this->calculate_subtotal();
		$this->apply_coupons( ); 
		$this->calculate_total_price();
		
		$this->update();
	}	
		
	//Рассчитать налог
	private function get_calculate_tax( $price, $product_id ) 
	{			
		if ( $price == 0 )
			return 0;
		
		$tax = 0;				
		$taxes = usam_get_taxes();	
		foreach( $taxes as $value )
		{			
			$value->setting = maybe_unserialize( $value->setting );	
			if (  !$value->active )
				continue;
			
			if (  empty($value->type_payer) || $value->type_payer == $this->type_payer )
			{	
				if ( empty($value->setting['locations']) || $this->compare_arrays('equal', $value->setting['locations'], $this->location_ids ) )
				{	
					foreach( array('category', 'brands') as $key ) 
					{						
						if ( !empty($value->setting[$key]) )					
						{						
							$result = $this->compare_terms( $product_id, 'usam-'.$key, array( 'logic' => 'equal', 'value' => $value->setting[$key]) );
							if ( !$result )
							{
								return 0;
							}
						}
					}						
					$tax += $price*$value->value/100;	
					$price += $tax;
				}
			}
		}		
		$tax = (float)usam_round_price( $tax, $this->type_price );
		return $tax;
	}
	
	/**
    * Удалить из корзины товар
    */
	public function remove_item( $product_basket_id )
	{
		global $wpdb;		
		
		if ( $this->id == null )
			return false;
	
		$return = $wpdb->query( $wpdb->prepare( "DELETE FROM `".USAM_TABLE_PRODUCTS_BASKET."` WHERE `cart_id` IN (%s) AND `id`=%d", $this->id, $product_basket_id ) );		
		if( $return )
		{				
			unset($this->cart_items[$product_basket_id]);
			
			$this->cart_item_count = count($this->cart_items);
			$this->current_cart_item = -1;
					
			$this->recalculation_changing_basket_products();	
			
			do_action( 'usam_remove_item', $product_basket_id, $this );
			$return = true;			
		}			
		return $return;
	}
		
	// Получить номер товара в цикле
	public function get_item_id_by( $value, $colum = 'product_id' )
	{
		$return = false;
		
		$colums = array( 'id', 'product_id' );		
		if ( !in_array($colum,$colums) )
			return $return;		
		
		foreach((array)$this->cart_items as $product_basket_id => $cart_item) 
		{
			if ( $cart_item->$colum == $value )
			{
				$return = $product_basket_id; 	
				break;
			}
		}
		return $return;
	}
	
	// Количество товаров в корзине
	public function get_cart_product_count()
	{		
		$count = 0;
		foreach((array)$this->cart_items as $cart_item) 
			$count += $cart_item->quantity;   
		return $count;
	}
	
	public function set_error( $error )
	{
		if ( !empty($error) )
		{
			$this->errors[] = $error;
			$this->update( );
		}
	}
	
	public function get_errors_message(  )
	{			
		$error = $this->errors;
		if ( !empty($this->errors) )
		{ 
			$this->errors = array();
			$this->update( );	
		}
		return $error;
	}

   /*
	* Метод проверяет оставшееся количество. В настоящее время только проверяет оставшиеся запасы, в будущем будет делать заявки на запасы и лимиты количества
	*/
	function check_remaining_quantity( $product_id, $quantity, $group_storage = 'stock' )
	{		
		$output = false;	
		if( $group_storage == 'stock' ) 
			$stock = usam_get_product_meta($product_id, 'stock' );	
		else
			$stock = usam_get_product_meta($product_id, 'storage' );
		
		if( $stock > 0 ) 
		{			
			if( $quantity <= $stock )
			   $output = true;
			else
			{			  				
			    $this->set_error( sprintf( _n('Извините, но есть только %s единицы этого товара.', 'Извините, но есть только %s единиц этого товара.', $stock, 'usam'), $stock ) );			    
			}
		}
		else
		{
			$product = get_post( $product_id );			
			$this->set_error( sprintf( __( 'Извините, но товара "%s" нет в наличии.', 'usam' ), $product->post_title ) );			
		}	
		return $output;
	}	
   
   /**
    * Метод очистки Корзина
   */
  function empty_cart( ) 
  {				
		$this->errors = array();
		$this->cart_items = array();
		$this->cart_item = null;
		$this->cart_item_count = 0;
		$this->current_cart_item = -1;
		$this->subtotal = null;	
		
		$this->coupons_amount = 0;		
		$this->coupon_name = '';	
		
		$this->bonuses	= 0;	
		$this->order_id	= 0;		
		
		$this->clear_cache();	
		
		if ( $this->id )
		{
			usam_remove_products_basket( $this->id );	
			usam_delete_cart_discount( $this->id );
			
			$this->update();	
		}				
		do_action( 'usam_clear_cart', $this );
	}
	
   /**
    * Метод Очистить кэш, используется для очистки кэшированных итоги
    */
	function clear_cache()
	{				
		$this->total_tax = null;
		$this->shipping = null;	
		$this->total_price = null;		
		$this->accumulative_discount = null;			
		$this->discount = 0;			
		$this->shipping_quotes = null;	
	
		do_action ( 'usam_after_cart_clear_cache', $this );		
	}  	
	
	public function cancel_reserve_bonuses(  ) 
	{
		$this->bonuses = 0;
		$_bonus = new USAM_Work_Bonuses();	
		$_bonus->cancel_reserve_bonuses();
	} 
	
	public function get_bonuses(  ) 
	{
		return usam_get_available_user_bonuses( $this->user_id );
	}  	
	
	/**
    * Метод считает сумму корзины
    */
	function calculate_total_price() 
	{				
		if ( $this->subtotal != 0 )
		{	// Рассчитать индивидуальный компонент, составляющих корзина	
			$total = 0;
							
			$this->bonuses = $this->get_bonuses();			
			$this->shipping = $sum = $this->get_shipping_cost( $this->selected_shipping );
		
			$this->calculation_basket_shares( );				
			$total += $this->subtotal;	
			$total -= $this->discount;
			$total -= $this->bonuses;
			
			if ( $this->coupons_amount > $total )
				$this->coupons_amount = $total;	
			
			$total -= $this->coupons_amount;	
			if ( $this->include_in_cost_shipping )
				$total += $this->shipping;				
			
			$this->total_price = apply_filters( 'usam_calculate_total_price', $total, $this );// общий фильтр				
		}
		else
			$this->total_price = 0;
		return $this->total_price;
	}
	
	// Расчет правил корзины
	function calculation_basket_shares() 
	{	
		$this->discount = 0;			
		$option = get_option('usam_rules_discounts_shopping_cart' );						
		$discounts_rule = maybe_unserialize($option);			
		if ( !empty($discounts_rule) )
		{		
			$comparison = new USAM_Comparison_Object( 'priority', 'ASC' );
			usort( $discounts_rule, array( $comparison, 'compare' ) );
		
			$current_time = current_time('timestamp');	
			$ids_discount = array();			
			usam_delete_cart_discount( $this->id );	
			
			$discont_price_product = array();
			// Получить массив цен со скидками для расчета правил
			foreach ( $this->cart_items as $key => $cart_item )	
			{
				$discont_price_product[$key] = usam_get_product_price( $cart_item->product_id, $this->type_price );
			}			
			$discont_price_product_start = false;
			foreach ( (array)$discounts_rule as $rule )	
			{				
				$gift_add = false;		
				if ( $rule['active'] && ( empty($rule['start_date']) || strtotime($rule['start_date']) <= $current_time ) && ( empty($rule['end_date']) || strtotime($rule['end_date']) >= $current_time ) )
				{			
					$return = $this->check_condition( $rule['condition'] ); 
					$sum = $return['sum'];			
					if ( $sum )
					{						
						if ( $rule['value_type'] == 'f' )									
							$discount = $rule['value'];				
						else															
							$discount = usam_round_price( $rule['value']*$sum / 100, $this->type_price);					
						switch( $rule['type'] )
						{							
							case 'p': // Изменить цену товара								
								$discont_price_product_start = true;
								foreach ( $return['items'] as $key )	
								{								
									if ( $rule['value_type'] == 'f' )									
										$discount = $rule['value'];				
									else															
										$discount = usam_round_price( $rule['value']*$discont_price_product[$key] / 100, $this->type_price);
									
									$discont_price_product[$key] = $discont_price_product[$key] - $discount;
								}		
							break;
							case 's': // Изменить доставку							
								$this->shipping = -$discount;
								if ( $this->shipping < 0)
									$this->shipping = 0;
							break;
							case 'b': // Добавить бонусы															
								$this->accrue_bonuses = $discount;
							break;							
							case 'g': // Добавить подарок					
								if ( !empty($rule['gift']) )
								{
									foreach ( (array)$rule['gift'] as $product_id => $quantity )	
									{		
										$new_data = array('old_price' => usam_get_product_price( $product_id, $this->type_price), 'price' => '0.00', 'tax' => '0.00', 'quantity' => $quantity, 'gift' => 1 );
										$id = $this->get_item_id_by( $product_id );										
										if ( $id )	
										{												
											$result = $this->update_basket_product( $id, $new_data );
											unset( $discont_price_product[$id] );
										}
										else											
											$result = $this->insert_basket_product( $product_id, $new_data );
									}
									$gift_add = true;
								}
							break;
						}							
						$rule['discount_id'] = $rule['id'];
						$rule['name'] = $rule['title'];						
						
						usam_set_cart_discount( $rule, $this->id );
						
						if ( $rule['end'])
							break;							
					}									
				}							
				if ( !$gift_add && $rule['type'] == 'g' && !empty($rule['gift']) )
				{
					foreach ( (array)$rule['gift'] as $product_id => $quantity )	
					{						
						$id = $this->get_item_id_by( $product_id );
						if ( $id !== false && !empty($this->cart_items[$id]->gift) )
						{						
						    $result = $this->remove_item( $this->cart_items[$id]->id );				
						}
					}
				}				
			}	
			if ( $discont_price_product_start && !empty($discont_price_product) )	
			{ 	
				foreach ( $discont_price_product as $key => $price )	
				{	
					$old_price = $this->cart_items[$key]->old_price == 0 ? $this->cart_items[$key]->price : $this->cart_items[$key]->old_price;
					$new_data = array( 'price' => $price, 'old_price' => $old_price );	
					$result = $this->update_basket_product( $this->cart_items[$key]->id, $new_data );					
				}											
				$this->calculate_subtotal();			
			}				
		}				
	}
	
	function compare_products ($v1, $v2) 
	{		
		if ($v1["priority"] == $v2["priority"]) return 0;
		return ($v1["priority"] > $v2["priority"])? -1: 1;
	}
	
	private function get_accumulative_discount( ) 
	{	
		if ( $this->accumulative_discount === null && $this->user_id )
		{						
			$this->accumulative_discount = get_user_meta( $this->user_id, 'usam_accumulative_discount', true );							
		}
	}
	
	// Получить товары
	public function get_cart_items()
	{					
		return $this->cart_items;		
	}

  /**
    * Рассчитывает сумму по корзине
   */
	private function calculate_subtotal( )
	{	
		$this->subtotal = 0;	
		$this->total_tax = 0;		
		$this->discount = 0;		
		foreach($this->cart_items as $key => $cart_item)
		{					
			$tax = $cart_item->tax * $cart_item->quantity;
			$this->subtotal += $cart_item->quantity*$cart_item->price + $tax;
			
			$this->total_tax += $tax;		
			if ( $cart_item->old_price )
				$this->discount += $cart_item->quantity*($cart_item->old_price - $cart_item->price);	
		}			
	}
	
	public function check_virtual_products( )
	{					
		$virtual_products = false;
		foreach( $this->cart_items as $key => $cart_item )
		{							
			$virtual = (bool)usam_get_product_meta( $cart_item->product_id, 'virtual' );				
			if ( $virtual )
				$virtual_products = true;
			else
			{
				$virtual_products = false;
				break;
			}
		}	
		return $virtual_products;
	}
	
	/**
	 * Возвращает элементы корзины.
	 * Принимает массив аргументов:
	 * - 'fields': По умолчанию «all», который возвращает все поля. В противном случае, укажите поля, которые нужно получить, например, «quantity» или «pnp».
	 * - 'orderby': Specify a field to sort the cart items. Default to '', which means "unsorted".
	 * - 'order'  : Specify the direction of the sort, 'ASC' for ascending, 'DESC' for descending.
	 *              Defaults to 'DESC'
	 */
	public function get_items( $args = array() )
	{
		$defaults = array('fields'  => 'all', 'orderby' => '', 'order'   => 'ASC', );

		$r = wp_parse_args( $args, $defaults );
		extract( $r, EXTR_SKIP );
		$results = $this->cart_items;

		if ( ! empty( $orderby ) ) 
		{
			$comparison = new USAM_Comparison_Object( $orderby, $order );
			usort( $results, array( $comparison, 'compare' ) );
		}
		if ( $fields != 'all' )
			$results = wp_list_pluck( $results, $fields );

		return $results;
	}
   
   /** 
    * Рассчитать вес корзины
   */
   function calculate_total_weight() 
   {		
		$total = 0;		
		foreach($this->cart_items as $key => $cart_item)
		{
			$weight = (float)usam_get_product_meta( $cart_item->product_id, 'weight' );
			if ( empty($weight) )			
				continue;
			$total += $weight*$cart_item->quantity;			
		}
		return $total;
	}	

  /**
	получить ID категорий
   */
	function get_cart_category_ids()
	{
		$category_ids = array();
		foreach($this->cart_items as $key => $cart_item) 
		{			
			$ids = usam_get_product_term_ids( $cart_item->product_id );			
			$category_ids = array_merge($ids, $category_ids);		
		}
		return $category_ids;
	}  
  
	function spend_bonuses()
	{			
		$usam_bonus = new USAM_Work_Bonuses();	
		$this->bonuses = $usam_bonus->reserve_bonuses( $this->total_price );
		$error = $usam_bonus->get_error();
		$this->set_error( $error ); 
		
		$this->calculate_total_price(); 
	} 		
	
   /**
    * Перебирает товары в корзине -------------------------------------------------------------------
   */
	function next_cart_item() 
	{
		$this->current_cart_item++;	
		
		$this->cart_item = $this->cart_items[$this->cart_items_keys[$this->current_cart_item]];
		return $this->cart_item;
   }

  function the_cart_item() 
  {
		$this->in_the_loop = true;		
		$this->cart_item = $this->next_cart_item();
		if ( $this->current_cart_item == 0 ) 
			do_action('usam_cart_loop_start');
   }

   function have_cart_items() 
   {
		if ($this->current_cart_item + 1 < $this->cart_item_count)
		{
			$this->cart_items_keys = array_keys($this->cart_items);
			return true;      
		}
		else if ($this->current_cart_item + 1 == $this->cart_item_count && $this->cart_item_count > 0) 
		{
			do_action('usam_cart_loop_end');       
			$this->rewind_cart_items();
		}
		$this->in_the_loop = false;
		return false;
   }

   function rewind_cart_items() 
   {
		$this->current_cart_item = -1;
		if ($this->cart_item_count > 0) 
			$this->cart_item = $this->cart_items[$this->cart_items_keys[0]];      
   }
   
   
  /**
    * Перебирает варианты доставки
   */ 
	private function next_shipping_method() 
	{
		$this->current_shipping_method++;				
		$this->shipping_method = $this->shipping_methods[$this->current_shipping_method];
		$this->shipping_method->delivery_period = '';
		if ( $this->shipping_method->period_from > 0)
		{
			switch( $this->shipping_method->period_type ) 
			{
				case 'D':
					$type = __('дней','usam');
				break;
				case 'H':
					$type = __('недель','usam');
				break;
				case 'M':
					$type = __('месецев','usam');
				break;
				default:
					$type = '';
				break;
			}
			if ( $type )
				$this->shipping_method->delivery_period = sprintf( __('от %s до %s %s', 'usam'), $this->shipping_method->period_from, $this->shipping_method->period_to, $type );
		}		
		$this->shipping_method->price = $this->get_shipping_cost( $this->shipping_method->id );		
			
		if ( $this->shipping_method->price > 0 )
			$this->shipping_method->info_price = sprintf( __('Стоимость доставки: %s', 'usam'), usam_currency_display( $this->shipping_method->price ) );		
		elseif ( $this->shipping_method->handler === 0 )
			$this->shipping_method->info_price = __('Бесплатная доставка', 'usam');
		else
			$this->shipping_method->info_price = '';
		
		return $this->shipping_method;
	}


   public function the_shipping_method() 
   {		
		return $this->next_shipping_method();		
   }

   function have_shipping_methods() 		
   {
		if ($this->current_shipping_method + 1 < $this->shipping_method_count) 
			return true;
		else if ($this->current_shipping_method + 1 == $this->shipping_method_count && $this->shipping_method_count > 0)          
			$this->rewind_shipping_methods();      
		return false;
   }

	function rewind_shipping_methods() 
	{	
		$this->current_shipping_method = -1;
		if ( $this->shipping_method_count > 0 )
			$this->shipping_method = $this->shipping_methods[0];
	}

   function next_shipping_quote() 
   {
		$this->current_shipping_quote++;
		$this->shipping_quote = $this->shipping_quotes[$this->current_shipping_quote];		 
		return $this->shipping_quote;
   }

   function the_shipping_quote() 
   {  
		$this->shipping_quote = $this->next_shipping_quote();		
   }

   function have_shipping_quotes() 
   {
		if ($this->current_shipping_quote + 1 < $this->shipping_quote_count)
			return true;
		else if ($this->current_shipping_quote + 1 == $this->shipping_quote_count && $this->shipping_quote_count > 0)      
			$this->rewind_shipping_quotes();      
		return false;
   }

   function rewind_shipping_quotes() 
   {
		$this->current_shipping_quote = -1;		
		if ( $this->shipping_quote_count > 0 ) 
			$this->shipping_quote = $this->shipping_quotes[0];      
   }  
   
   /**
   * Обновить заказ
   */
	public function save_order( )
	{	
		$this->calculate_total_price();				
		if ( !$this->calculate_purchase_rules() || $this->total_price == 0 )
			return false;			

		$args =  array(
				'totalprice'          => $this->total_price,				
				'status'              => 'incomplete_sale',				
				'type_price'          => $this->type_price,	
				'type_payer'          => $this->type_payer,				
				'user_ID'             => $this->user_id,			
				'coupon_name'         => $this->coupon_name,		
				'coupon_discount'     => $this->coupons_amount,								
				'bonus'               => $this->bonuses,		// Общие бонусы
				'order_type'          => 'order',
				'total_tax'           => $this->total_tax,				
			);			
		
	/*	if ( is_numeric($this->order_id) )
		{
			$purchase_log = new USAM_Order( $this->order_id );		
			$purchase_log->set( $args );	
		}
		else		
			$purchase_log = new USAM_Order( $args );	
	*/		
		
		$purchase_log = new USAM_Order( $args );	
		$result = $purchase_log->save();	
		if ( !$result )
			return false;
	
		$this->order_id = $purchase_log->get( 'id' );		
		$this->set_order_discounts( );		
		
		// сохранить товары заказа в базу			
		$product_reserve_condition = get_option('usam_product_reserve_condition', '' );
		$document_shipped_products = array();	
		foreach( $this->cart_items as $key => $cart_product )
		{
			$product_meta = usam_get_product_meta( $cart_product->product_id, 'product_metadata' );		
			$bonus = 0;
			if ( !empty($product_meta['bonuses']['value']) )
			{
				if ( $product_meta['bonuses']['type'] == 'p' )
					$bonus = usam_round_price($cart_product->price + $cart_product->price * $product_meta['bonuses']['value'] / 100, $this->type_price);
				else
					$bonus = $product_meta['bonuses']['value'];
			}	
			$rule_product_day = usam_get_active_products_day_by_codeprice( $this->type_price, $cart_product->product_id );			
			$product_day = !empty($rule_product_day)?$rule_product_day->rule_id:0;
			$products[] = array(
				'product_id'     => $cart_product->product_id,
				'name'           => $cart_product->name,
				'order_id'       => $this->order_id,
				'price'          => $cart_product->price,
				'old_price'      => $cart_product->old_price,	
				'product_day'    => $product_day,	
				'tax'            => $cart_product->tax,							
				'bonus'          => $bonus,				
				'quantity'       => $cart_product->quantity,	
			);				
		}			
		$selected_shipping = $this->get_selected_shipping_method();
		
		$document_shipped['method']         = $this->selected_shipping;	
		$document_shipped['name']           = !empty($selected_shipping)?$selected_shipping->name:'';
		$document_shipped['storage_pickup'] = $this->storage_pickup;
		
		if ( !empty($this->storage_pickup) )		
			$document_shipped['storage'] = $this->storage_pickup;
		else
			$document_shipped['storage'] = get_option('usam_default_reservation_storage', '' );		
		
		$document_shipped['status']         = 1;
		$document_shipped['price']          = $this->shipping;
		$document_shipped['include_in_cost'] = $this->include_in_cost_shipping;		
		$document_shipped['products']       = array();	
		
		$purchase_log = new USAM_Order( $this->order_id );	
		$purchase_log->add_order_products( $products );	
		$products = $purchase_log->get_order_products( );
		foreach( $products as $product )
		{
			if ( $product_reserve_condition == 'o' )
				$reserve = $product->quantity;					
			else
				$reserve = 0;			
			$document_shipped_products[] = array( 'basket_id' => $product->id, 'quantity'  => $product->quantity, 'reserve' => $reserve );
		}		
		$document_id = $purchase_log->add_document_shipped( $document_shipped );			
		
		$shipped = new USAM_Shipped_Document( $document_id );
		$shipped->set_products( $document_shipped_products );		
		
		do_action( 'usam_basket_add_document_shipped', $document_id );
		
		
		$this->set_bonus_account( $this->order_id );			

		do_action( 'usam_basket_add_order', $this->order_id );				
		return $this->order_id;		
	}
	
	public function set_order_discounts( )
	{	
		$cart_discounts = usam_get_cart_discounts( $this->id );			
		if ( !empty($cart_discounts))
		{	 
			foreach( $cart_discounts as $cart_discount)
			{
				$order_discount = (array)$cart_discount;				
				$order_discount['order_id'] = $this->order_id;			
				usam_update_order_discount( $order_discount );					
			}	
			usam_unlink_cart_discount( $this->id );
		}				
	}
	
	// Добавляет бонусы по купону
	public function set_bonus_account( $order_id )
	{
		global $wpdb;
		
		$_coupon = new USAM_Coupon( $this->coupon_name, 'coupon_code' );
		$coupon_data = $_coupon->get_data();
		
		if ( !empty($coupon_data['amount_bonuses_author']) )
		{					
			$data = array( $this->user_id, $order_id, $coupon_data['amount_bonuses_author'], 1, 0, date( "Y-m-d H:i:s" ) );
			$wpdb->query( $wpdb->prepare( "INSERT INTO ".USAM_TABLE_CUSTOMER_BONUSES." ( user_id, order_id, bonuses, status, type, date ) VALUES (%d, %d, %d, %s, %s, %s)", $data ) );				
		}
	}
	
	
	// Сравнить строки
	public function compare_string( $logic, $n1, $n2 ) 
	{		
		$n1 = (string)$n1;
		$n2 = (string)$n2;
		
		$return = false;
		switch( $logic )
		{
			case 'equal'://равно		
				if ( $n1 == $n2 ) $return = true; 
			break;
			case 'not_equal': // не равно
				if ( $n1 != $n2 ) $return = true;
			break;
			case 'contains': //содержит
				$pos = strpos($n1, $n2);
				if ($pos !== false)
					$return = true;		
			break;
			case 'not_contain'://не содержит
				$pos = strpos($n1, $n2);
				if ($pos === false)
					$return = true;		
			break;	
			case 'begins'://начинается с
				preg_match("/^".$n1."/", $n2, $match);
				if (!empty($match))
					$return = true;
			break;
				case 'ends'://заканчивается на
				preg_match("/".$n1."$/", $n2, $match);
				if (!empty($match))
					$return = true;
			break;				
			default:
				$return = false;
			break;
		}
		return $return;
	}	
	
	// Сравнить числа
	public function compare_number( $logic, $n1, $n2 ) 
	{		
		$n1 = (float)$n1;
		$n2 = (float)$n2;
		
		$return = false;
		switch( $logic )
		{
			case 'equal'://равно		
				if ( $n1 == $n2 ) $return = true; 
			break;
			case 'not_equal': // не равно
				if ( $n1 != $n2 ) $return = true;
			break;
			case 'greater'://больше					
				if ($n1 > $n2)	$return = true;
			break;
			case 'less'://меньше
				if ($n1 < $n2) $return = true;
			break;
			case 'eg'://больше либо равно					
				if ($n1 >= $n2)	$return = true;
			break;
			case 'el'://меньше либо равно
				if ($n1 < $n2) $return = true;
			break;
			default:
				$return = false;
			break;
		}
		return $return;
	}	
	
	// Проверить массив на условие
	public function compare_array( $logic, $array, $number ) 
	{		
		$return = false;
		switch( $logic )
		{
			case 'equal'://равно
				if ( in_array($number, $array) ) $return = true;
			break;
			case 'not_equal': // не равно
				if ( !in_array($number, $array) ) $return = true;
			break;		
			default:
				$return = false;
			break;
		}
		return $return;
	}	
	
	// Проверить массив на условие
	public function compare_arrays( $logic, $array1, $array2 ) 
	{		
		$return = false;
		$result = array_intersect($array1, $array2);
		switch( $logic )
		{
			case 'equal'://равно				
				if ( !empty($result) ) $return = true;
			break;
			case 'not_equal': // не равно
				if ( empty($result) ) $return = true;
			break;		
			default:
				$return = false;
			break;
		}
		return $return;
	}
	
		// Проверить термины на условие
	private function compare_terms( $product_id, $term, $c ) 
	{		
		$result = false;
		$product_id = (int)$product_id;		
		$ids = usam_get_product_term_ids( $product_id, $term );		
		if ( is_array($c['value']) )
			$result = $this->compare_arrays($c['logic'], $ids, $c['value'] );
		else
		{
			$value = (int)$c['value'];					
			$result = $this->compare_array($c['logic'], $ids, $value );				
		}		
		return $result;
	}	
	
	/**
	 * Проверка логики для всей корзины
	 */
	private function cart_compare_logic( $c ) 
	{
		$c = apply_filters( 'usam_cart_compare_logic_before', $c );
		$result = false;
		
		$key = $c['property'];	
		if ( property_exists($this, $key) )
		{		
			if ( is_array($c['value']) )								
			{	
				$key = $c['property'];		
				$result = $this->compare_arrays($c['logic'], $this->$key, $c['value'] );			
			}
			elseif ( is_string($this->$key) )
			{	
				$key = $c['property'];			
				$result = $this->compare_string($c['logic'], $this->$key, $c['value'] );		
			}
			else								
			{	
				$key = $c['property'];			
				$result = $this->compare_number($c['logic'], $this->$key, $c['value'] );		
			}
		}	
		elseif ($c['property'] == 'item_count_total')
		{
			$quantity = 0;
			foreach( $this->cart_items as $key => $cart_item)
			{
				$quantity += $cart_item->quantity;
				$result = $this->compare_number($c['logic'], $quantity, $c['value'] );
				if ( $result )
					break;
			}	
		}
		elseif ($c['property'] == 'locations')
		{
			$result = $this->compare_arrays( $c['logic'], $this->location_ids, $c['value'] );
		}			
		elseif ($c['property'] == 'category')
		{		
			foreach( $this->cart_items as $key => $cart_item)
			{
				$result = $this->compare_terms( $cart_item->product_id, 'usam-category', $c );
				if ( $result )
					break;
			}
		}
		elseif ($c['property'] == 'brands')
		{		
			foreach( $this->cart_items as $key => $cart_item)
			{
				$result = $this->compare_terms( $cart_item->product_id, 'usam-brands', $c );
				if ( $result )
					break;
			}			
		}
		elseif ($c['property'] == 'category_sale')
		{		
			foreach( $this->cart_items as $key => $cart_item)
			{
				$result = $this->compare_terms( $cart_item->product_id, 'usam-category_sale', $c );
				if ( $result )
					break;
			}			
		}		
		elseif ($c['property'] == 'birthday')
		{							
			if ( is_user_logged_in() )
			{					
				$birthday = get_user_meta( $this->user_id, 'usam_birthday', true );				
				if ( !empty($birthday) )
				{
					$birthday = get_date_from_gmt($birthday, "Y-m-d H:i:s");	
					$birthday = strtotime( $birthday ); 						
					$day = date('d', $birthday);
					$m = date('m', $birthday);
					if ( date('d') == $day && date('m') == $m )
						$result = true;						
				}
			} 
		}	
		elseif ($c['property'] == 'newcustomer')
		{			
			$checkout = usam_get_customer_meta( 'checkout_details' );		
			$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );	
			if ( !empty($checkout) )
			{ 				
				$billingmobilephone = isset($list_properties['billingmobilephone']) && isset($checkout[$list_properties['billingmobilephone']->id])?$checkout[$list_properties['billingmobilephone']->id]:0;
				$billingphone = isset($list_properties['billingphone']) && isset($checkout[$list_properties['billingphone']->id])?$checkout[$list_properties['billingphone']->id]:0;
				$billingemail = isset($list_properties['billingemail']) && isset($checkout[$list_properties['billingemail']->id])?$checkout[$list_properties['billingemail']->id]:0;	
				
				if ( empty($billingmobilephone) && empty($billingmobilephone) && empty($billingmobilephone))
					$result = false;
				else
				{ 
					$condition = array( 
						array( 'relation' => 'OR', 
							array( 'relation' => 'OR',
								array('key' => 'value', 'compare' => '=', 'value' => $billingmobilephone ), array('key' => 'unique_name', 'compare' => '=', 'value' =>'billingmobilephone' ), ),
							array( 'relation' => 'AND',	
								array('key' => 'value', 'compare' => '=', 'value' => $billingphone ), array('key' => 'unique_name', 'compare' => '=', 'value' =>'billingphone' )
							) ),
						array( 'relation' => 'AND', array('key' =>'value', 'compare' =>'=', 'value' =>$billingemail ), array('key' => 'unique_name', 'compare' => '=', 'value' =>'billingemail', 'relation' =>'OR' ) ),			
					);			
					$props = usam_get_orders( array( 'fields' => 'id', 'property' => $condition ) );
					if ( empty($props) )
						$result = true;			
				}
			}			
		}
		elseif ($c['property'] == 'weekday')
		{		
			$value = (int)$c['value'];		
			$result = $this->compare_number($c['logic'], date("w"), $value );			
		}				
		elseif ($c['property'] == 'user')
		{					
			$value = (int)$c['value'];	
			$result = $this->compare_number($c['logic'], $this->user_id, $value );			
		}	
		elseif ($c['property'] == 'roles')
		{						
			if ( $this->user_id > 0 )
			{
				$user = get_userdata( $this->user_id  );				
				if ( is_array($c['value']) )								
					$result = $this->compare_arrays($c['logic'], $user->roles, $c['value'] );			
				else								
					$result = $this->compare_array($c['logic'], $user->roles, $c['value'] );	
			}
			else
			{
				if ( is_array($c['value']) )								
					$result = $this->compare_array( $c['logic'], 'notLoggedIn', $c['value'] );	
				else								
					$result = $this->compare_number( $c['logic'], 'notLoggedIn', $c['value'] );
			}				
		}			
		elseif ( stristr($c['property'], 'order_property') !== false)
		{		
			$property = str_replace("order_property-", "", $c['property']);

			$checkout = usam_get_customer_meta( 'checkout_details' );
			if ( isset($checkout[$property]) )
			{
				$result = $this->compare_string($c['logic'], $checkout[$property], $c['value'] );			
			}
		}				
		else 
			$result = true;	
		$result = apply_filters( 'usam_cart_compare_logic_after', $result, $c );		
		return $result;		
	}
		
	public function check_condition( $conditions ) 
	{
		$check_items_key = array();
		foreach( $this->cart_items as $key => $cart_item)
		{
			$check_items_key[$key] = $key;
		}		
		$return_items_key = $this->compare_logic( $conditions, $check_items_key );			
		$sum = 0;
		if ( !empty($return_items_key) ) 
		{							
			foreach( $return_items_key as $key )
			{
				$sum += $this->cart_items[$key]->price*$this->cart_items[$key]->quantity;				
			}			
		}	
		return array( 'sum' => $sum, 'items' => $return_items_key );	
	}
	
	
	/**
	 * Проверяет, соответствует ли корзина логике	
	 */
	public function compare_logic( $conditions, $check_items_key ) 
	{				
		$result = true;	
		$allow_operation = true;	
		$return_items_key = $check_items_key;
		foreach( $conditions as $c )
		{				
			if ( !isset($c['logic_operator']) )
			{
				if ( !$allow_operation )
					continue;
				
				switch( $c['type'] )
				{
					case 'products':						
						$return_items_key = $this->get_products_meet_conditions( $c['rules'], $return_items_key );							
						if ( empty($return_items_key) )
							$result = false;
						else
							$result = true;						
					break;
					case 'group':
						$return_items_key = $this->compare_logic( $c['rules'], $return_items_key );
						if ( empty($return_items_key) )
							$result = false;
						else
							$result = true;								
					break;
					case 'simple':
						$result = $this->cart_compare_logic( $c ); //print_r($c); echo $result."<br>"; 
					break;
				}
				if ( !$result )
				{
					$return_items_key = array();			
					$allow_operation = false;
				}
			}				
			else
			{ // Если условие И, ИЛИ
				if ( $c['logic_operator'] == 'AND' )
				{ // Если и
							
				}
				else					
				{ // Если или		
					if ( $result )
					{	// Если условия истина до ближайшего оператора ИЛИ то завершить цикл
						break;
					}
					else	
					{
						$return_items_key = $check_items_key;
						$allow_operation = true;
					}
				}				
			}			
		}	
		return $return_items_key;
	}	
	
		/**
	 * Получить сумму товаров соответствующих логике
	 */
	private function get_products_meet_conditions( $conditions, $check_items_key ) 
	{		
		$product_keys = array();
		foreach( $check_items_key as $key )
		{			
			if ( $this->cart_item_conditions( $conditions, $this->cart_items[$key] ) )
			{	
				$product_keys[] = $key;			
			}				
		}			
		return $product_keys;		
	}
	
	/**
	 * Проверить удовлетворяет ли товар соответствующих логике
	 */
	private function cart_item_conditions( $conditions, $cart_item ) 
	{	
		$result = true;		
		$allow_operation = true; // Разрешить операцию			
		foreach( $conditions as $c )
		{			
			if ( !isset($c['logic_operator']) && $allow_operation )
			{						
				switch( $c['type'] )
				{					
					case 'group':
						$result = $this->cart_item_conditions( $c['rules'], $cart_item );				
					break;
					case 'simple':
						$result = $this->check_condition_simple_basket_item( $c, $cart_item );						
					break;
				}				
				if ( !$result )		
					$allow_operation = false;
			}
			elseif ( isset($c['logic_operator']) )
			{ // Если условие И, ИЛИ
				if ( $c['logic_operator'] == 'AND' )
				{ // Если и
							
				}
				else					
				{ // Если или		
					if ( $result )
					{	// Если условия истина до ближайшего оператора ИЛИ то завершить цикл
						break;
					}
					else						
						$allow_operation = true;					
				}				
			}			
		}	
		return apply_filters( 'usam_cart_item_compare_logic', $result, $conditions, $cart_item );	
	}	
	
	/**
	 * Проверить удовлетворяет ли товар соответствующих логике
	 */
	private function check_condition_simple_basket_item( $c, $cart_item ) 
	{				
		if ( $c['property'] == 'item_name' ) 
		{					
			$post_title = get_post_field( 'post_title', $cart_item->product_id);			
			$return = $this->compare_string($c['logic'], $post_title, $c['value'] );			
		}	
		elseif ($c['property'] == 'item_quantity') //Количество товара
		{					
			$return = $this->compare_number($c['logic'], $cart_item->quantity, $c['value'] );		
		} 			
		elseif ($c['property'] == 'item_price')
		{				
			$price = usam_get_product_price( $cart_item->product_id, $this->type_price );
			$return = $this->compare_number($c['logic'], $price, $c['value'] );					
		} 					
		elseif ($c['property'] == 'item_old_price')
		{						
			$old_price = usam_get_product_old_price( $cart_item->product_id, $this->type_price);
			$return = $this->compare_number($c['logic'], $old_price, $c['value'] );	
		}
		elseif ($c['property'] == 'item_sku')
		{						
			$return = $this->compare_string($c['logic'], $cart_item->sku, $c['value'] );
		}	
		elseif ($c['property'] == 'item_barcode')
		{				
			$barcode = usam_get_product_meta( $cart_item->product_id, 'barcode' );			
			$return = $this->compare_string($c['logic'], $barcode, $c['value'] );
		}			
		elseif ( $c['property'] == 'category' ) 
		{			
			$return = $this->compare_terms( $cart_item->product_id, 'usam-category', $c );
		}				
		elseif ( $c['property'] == 'brands' ) 
		{	
			$return = $this->compare_terms( $cart_item->product_id, 'usam-brands', $c );
		}
		elseif ( $c['property'] == 'category_sale' ) 
		{		
			$return = $this->compare_terms( $cart_item->product_id, 'usam-category_sale', $c );
		}					
		else 
		{		
			$return = true;		
		}			
		return $return;	
	}		
	
// Проверить правила покупки	
	public function calculate_purchase_rules( ) 
	{
		$option = get_option('usam_purchase_rules');
		$rules = maybe_unserialize( $option );	
		if ( empty($rules) )
			return true;
	
		foreach( $rules as $rule )
		{
			if ( $rule['active'] == 1 )
			{
				$return = $this->check_condition( $rule['conditions'] );
				$sum = $return['sum'];
				if ( $sum > 0 )
					return false;
			}
		}	
		return true;
	}
}


/**
 * Класс элементов корзины
 */
class USAM_Cart_Item 
{  
	private $cart;	
	private $cart_id;	
	private $product_id;
	private $data = null;
	
  /**
   * требуется идентификатор продукта и параметры для продукта
   */
   public function __construct( $product_id, $cart_id, $cart ) 
   {		
		$this->product_id = absint($product_id);	
		$this->cart_id    = $cart_id;	
		$this->cart       = $cart;	
	}  

	public function refresh_item( $newdata )
	{						
		$data = $this->get_data();		
		if ( empty($data) )
			return false;

		$data = (array)$data;		
		if ( $newdata['price'] != $data['price'] || $newdata['old_price'] != $data['old_price'] ) 		
			$result = $this->update( $newdata ); 			
		else
			$result = true;
		return $result;
	}	

	private function get_title( ) 
	{		
		$product = get_post( $this->product_id );	
		if ( ! $product->post_parent )
			return get_post_field( 'post_title', $this->product_id);	
		
		if ( $product->post_parent )					
			$product_attribute = wp_get_object_terms( $this->product_id, 'usam-variation', array( 'fields' => 'names' ) );	
		
		$title = get_post_field( 'post_title', $product->post_parent );			
		$vars   = implode( ', ', $product_attribute );
		$title .= ' (' . $vars . ')';
			
		$title = apply_filters( 'usam_cart_product_title', $title, $this->product_id );
		return $title;
    }  
	
	public function get_data() 
	{		
		global $wpdb;
		
		if ( $this->data == null )
		{
			$cache_key = $this->cart_id.'_cart_item';
			if( ! $this->data = wp_cache_get($cache_key, $this->product_id ) )
			{		
				$this->data = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_PRODUCTS_BASKET." WHERE product_id = '$this->product_id' AND cart_id = '$this->cart_id'" );			
				if ( !empty($this->data) )
				{
					$this->data->old_price = (float)$this->data->old_price;
					$this->data->price = (float)$this->data->price;
				}
				wp_cache_set( $cache_key, $this->data, $this->product_id );				
			}				
		}		
		return $this->data;
	}
	
	public function set( $new_data ) 
	{			
		$data = $this->get_data();
		if ( !empty($data) )				
			$result = $this->update( $new_data );	
		else
		{				
			$new_data['name'] = $this->get_title( );		
			$new_data['sku'] = usam_get_product_meta( $this->product_id, 'sku' );
			
			$result = $this->insert( $new_data );				
		}
		return $result;
	}	
	
	public function update( $data ) 
	{	
		global $wpdb;		
	
		if ( isset($data['price']) )
			$data['price'] = (float)$data['price'];
		
		if ( isset($data['old_price']) )
			$data['old_price'] = (float)$data['old_price'];		
				
		$result = array_diff_assoc ($data, (array)$this->data );		
		if ( empty($result) )
			return true;
	
		$data['date_modified'] = date( "Y-m-d H:i:s" );	
		
		$where_format = array( 'id' => '%d', 'product_id' => '%d' );		
		$where = array( 'product_id' => $this->product_id, 'cart_id' => $this->cart_id );	
		
		$this->set_data( $data );	
		$insert_format = $this->get_format_data( );
		if ( empty($this->data) )
			return false;	
	
		$update = (array)$this->data; 		
		$update = apply_filters( 'usam_update_product_basket', $update, $this->product_id );	
	
		$update_result = $wpdb->update( USAM_TABLE_PRODUCTS_BASKET, $update, $where, $insert_format, $where_format );		
	
		wp_cache_set( $this->cart_id.'_cart_item', $this->data, $this->product_id );	
		return $update_result;
	}
	
	public function set_data( $data )
	{
		$format = $this->get_format();		
		
		if ( $this->data === null )
			$this->data = (object)$this->data;
		$update_format = array();
		foreach ( $data as $key => $value )
		{	
			if ( isset($format[$key]) )
			{
				$this->data->$key = $value;				
			}
		}				
	}
	
	public function get_format( )
	{
		$format = array( 'id' => '%d', 'product_id' => '%d', 'cart_id' => '%d','name' => '%s','sku' => '%s','price' => '%f','old_price' => '%f', 'tax' => '%f','quantity' => '%d', 'bonus' => '%f', 'date_insert' => '%s', 'date_modified' => '%s', 'gift' => '%d' );		
		return $format;
	}
	
	public function get_format_data( )
	{
		$format = $this->get_format();		
		
		$update_format = array();
		foreach ($this->data as $key => $value )
		{	
			if ( isset($format[$key]) )
			{
				$update_format[] = $format[$key];
			}
			else
				unset($this->data->$key);
		}		
		return $update_format;
	}	
	
	private function insert( $data )
	{
		global $wpdb;
		
		$data['cart_id'] = $this->cart_id;
		$data['product_id'] = $this->product_id;
		$data['date_insert'] = date( "Y-m-d H:i:s" );	
		$data['date_modified'] = date( "Y-m-d H:i:s" );	
	
		$this->set_data( $data );	
		$insert_format = $this->get_format_data( );
		if ( empty($this->data) )
			return false;	
		
		$insert = (array)$this->data;		
		$insert = apply_filters( 'usam_update_product_basket', $insert, $this->product_id );	
		
		$result = $wpdb->insert( USAM_TABLE_PRODUCTS_BASKET, $insert, $insert_format );	
		$this->data->id = $wpdb->insert_id;
		
		wp_cache_set( $this->cart_id.'_cart_item', (object)$this->data, $this->product_id );	
		return $result;
	}  
}

function usam_delete_cart( $ids ) 
{
	global $wpdb;
	
	if ( empty($ids) )
		return false;
	
	if ( is_numeric($ids) )
		$ids = array( $ids );
	
	$ids = array_map( 'intval', $ids );

	foreach($ids as $id)
		usam_delete_cart_discount( $id );
		
	usam_remove_products_basket( $ids );
	
	$in = implode( ', ', $ids );
	
	$result = $wpdb->query("DELETE FROM ".USAM_TABLE_USERS_BASKET." WHERE id IN ($in)");
	return $result;
}


// Удалить товары по выбранной корзине
function usam_remove_products_basket( $cart_ids ) 
{
	global $wpdb;
		
	if ( empty($cart_ids) )
		return false;
	
	if ( is_numeric($cart_ids) )
		$cart_ids = array($cart_ids);	

	$ids = array_map( 'intval', $cart_ids );
	$in = implode( ', ', $ids );
	
		
	$wpdb->query( $wpdb->prepare( "DELETE FROM `".USAM_TABLE_PRODUCTS_BASKET."` WHERE `cart_id` IN (%s)", $in ) );
}

//Разорвать связь между скидками и корзиной, когда заказ создан
function usam_unlink_cart_discount( $cart_id ) 
{
	global $wpdb;
	
	wp_cache_delete( $cart_id, 'cart_discounts_ids' );
	$result = $wpdb->query("DELETE FROM ".USAM_TABLE_DISCOUNT_BASKET." WHERE cart_id='$cart_id'");	
	return $result;
}

function usam_get_products_basket( $cart_id ) 
{
	global $wpdb;	
	$object_type = 'usam_products_basket';	
	$cache = wp_cache_get( $cart_id, $object_type );			
	if ( $cache === false )			
	{							
		$sql = "SELECT * FROM ".USAM_TABLE_PRODUCTS_BASKET." WHERE cart_id ='$cart_id' ORDER BY id ASC";	
		$cache = $wpdb->get_results( $sql );	

		wp_cache_set( $cart_id, $cache, $object_type );						
	}
	return $cache;
}