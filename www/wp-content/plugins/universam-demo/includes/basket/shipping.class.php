<?php
/**
 * Класс доставки
 */
class USAM_Shipping
{
	protected $API_URL;
	protected $internal_name;
	protected $name;
	protected $is_external = false;

	protected $message = array();
	protected $error  = '';
	
	protected $setting = array();
	protected $deliver = array();
	protected $id = 0;
	
	public function __construct( $id )
	{ 
		$this->id = absint($id);
		if ( !empty($this->id) )
		{
			$deliver = usam_get_delivery( $id );	
			$this->deliver = $deliver;
			if ( !empty($deliver['setting']['handler_setting']) )
				$this->setting = $deliver['setting']['handler_setting'];
		}
		else
		{
			$this->deliver = array( 'period_from' => 0, 'period_to' => 0, 'period_type' => '', 'price' => '' );
		}
	}
	
	public function set_log_file()
	{
		usam_log_file( $this->error );
	}
	
	private function set_error( $error ) 
	{
		$this->error = sprintf( __('Модуль доставки. Ошибка: %s'), $error );		
	}
	
	function get_errors() 
	{
		return $this->error;
	}

	function getName() 
	{
		return $this->name;
	}

	function getInternalName() 
	{		
		return $this->internal_name;
	}	
	
	public function get_shipping_cost( $args ) 
	{		
		return (float)$this->deliver['price'];
	}	
	
	function get_history_mail( $args ) 
	{		
		return array();
	}	
	
	function get_form( ) 
	{		
		?>				
		<tr>							
			<td class ="name"><?php _e( 'Сроки доставки', 'usam' ); ?>:</td>
			<td class ="row_option">
				<?php _e( 'от', 'usam' ); ?> <input type="text" maxlength = "3" size = "3" style='width:100px' name="shipping_module[period_from]" value="<?php echo $this->deliver['period_from']; ?>"/> <?php _e( 'до', 'usam' ); ?> <input type="text" maxlength = "3" size = "3" style='width:100px' name="shipping_module[period_to]" value="<?php echo $this->deliver['period_to']; ?>"/>
				<select name="shipping_module[period_type]" style='width:100px'>
					<option value="D" <?php selected($this->deliver['period_type'],'D'); ?>><?php _e( 'день', 'usam' ); ?></option>
					<option value="H" <?php selected($this->deliver['period_type'],'H'); ?>><?php _e( 'час', 'usam' ); ?></option>
					<option value="M" <?php selected($this->deliver['period_type'],'M'); ?>><?php _e( 'месяц', 'usam' ); ?></option>
				</select>								
			</td>
		</tr>		
		<tr>							
			<td class ="name"><?php _e( 'Стоимость доставки', 'usam' ); ?>:</td>
			<td class ="row_option">					
				<input type="text" name="shipping_module[price]" maxlength = "12" size = "12" style='width:100px' value="<?php echo $this->deliver['price']; ?>"/>
			</td>
		</tr>			
		<?php
	}
	
}

// Подключить класс
function usam_get_shipping_class( $id )
{
	$delivery = usam_get_delivery( $id );
	$shipping_class = 'USAM_Shipping';	
	if ( !empty($delivery['handler']) )
	{
		$file =  USAM_FILE_PATH . '/shipping/' . $delivery['handler'].'.php';
		if ( file_exists( $file ) )
		{
			require_once( $file );
			$shipping_class = 'USAM_Shipping_'.$delivery['handler'];			
		}
	}
	$shipping_instance = new $shipping_class( $id );	
	return $shipping_instance;
}
?>