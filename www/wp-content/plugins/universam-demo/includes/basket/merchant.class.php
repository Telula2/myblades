<?php
/**
 * Класс оплаты магазина УНИВЕРСАМ
 * Это базовый оплаты, все торговые файлы, использующие новый API расширить этот класс.
 * @since 3.7.6
 * @abstract
 */

/**
 * Это класс Торгового Шлюза. Он обрабатывает все, от сопоставления данных пользователя до данных корзины, так что все шлюзы работают согласованно.
 */
class USAM_Merchant 
{
	protected $api_version = '2.0';
	protected $type_operation = 'c';
	protected $error;	
	protected $content_type = '';		
	
	protected $order;
	protected $payment;
	protected $gateway_data;
	protected $purchase_log;
	protected $gateway_option = array();
	protected $gateway_system = array();	
	protected $data_sending_gateway = array();	

	protected $currency_list = array();		
	
	protected $purchase_id = null;
	protected $payment_number = null;
	protected $session_id = null;
	protected $payment_type = 'cart'; // Тип оплаты

	protected $notification_url;
	protected $url_cancel_return = '';
	protected $url_return = '';
	protected $ipn = false;
		
	protected $payment_link = '';
	protected $test_payment_link = '';
	protected $user_account_url = '';
	
	function __construct( ) { }	

	function USAM_Merchant( ) 
	{
		$this->__construct( );
	}	
	
	public function get_type_operation( ) 
	{
		return $this->type_operation;	
	}		
	
	protected function get_option( $gateway_id ) 
	{		
		$this->gateway_system = usam_get_payment_gateway( $gateway_id );
		if ( isset($this->gateway_system['setting']['gateway_option']) )
			$this->gateway_option = $this->gateway_system['setting']['gateway_option'];
	}		
	
	// Сохранить информацию о транзакции при успешном платеже
	protected function update_payment_document( $payment ) 
	{
		if ( !usam_check_order_is_completed($this->purchase_log->get( 'status' )) )
		{
			if ( isset($payment['status']) )
			{
				$status = '';
				switch( $payment['status'] ) 
				{
					case 1:
						$status = 'received';
					break;				
					case 3:
						$status = 'accepted_payment';
						if ( empty($payment['date_payed']) )
							$payment['date_payed'] = date( "Y-m-d H:i:s");	
					break;
					case 4:
						$status = 'job_dispatched';
					break;					
				}				
				if ( $status )
				{
					$this->set_purchase_processed_by_purchid( $status );
				}
			}
			$payment['document_number'] = $this->payment_number;			
			usam_update_payment_document($this->payment_number, $payment, 'document_number');
		}
	}
	
	function is_debug( ) 
	{
		if ( $this->gateway_system['debug'] )
			return true;
		else
			return false;
	}	
	
	function get_user_account_url( ) 
	{
		return $this->user_account_url;
	}
	
	function get_ipn( ) 
	{
		return $this->ipn;
	}
	
	function get_currency_list( ) 
	{
		return $this->currency_list;
	}
	
	// Стандартные сообщения
	protected function message_transaction( $key ) 
	{		
		$transaction_results = get_option( 'usam_page_transaction_results' );		
		if ( !empty($transaction_results[$key]) )
			$text = $transaction_results[$key];
		else
		{
			$message = usam_get_message_transaction();			
			$text = $message[$key]['text'];
		}				
		return $text;
	}	
			
	public function get_customer_data_by_type( $type ) 
	{		
		$customer_data = $this->purchase_log->get_customer_data();		
		$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );	
		
		$result = '';
		foreach ($list_properties as $unique_name => $data) 
		{
			if ( $data->type == $type && !empty($customer_data[$unique_name]) )
			{
				$result = $customer_data[$unique_name]['value'];
				break;
			}
		}
		return $result;
	}
	
	function get_customer_data( $data ) 
	{				
		$return = $this->purchase_log->get_item_customer_data( $data );
		if ( $return == '' && stristr($data, 'shipping') !== FALSE )
		{
			$data = str_replace( "shipping", "billing", $data );
			$return = $this->purchase_log->get_item_customer_data( $data );
		}		
		if ( empty($return['value']) ) 
			$return['value'] == '';
		return $return['value'];
	}
	
	/**
	 * Вернуть в корзину
	 */
	function return_to_checkout() 
	{
		wp_redirect( usam_get_url_system_page('basket') );
		exit(); 
	}

	function go_to_transaction_results( $result = 1 ) 
	{	
		$url = $result?$this->url_return:$this->url_cancel_return;
		wp_redirect( $url );
		exit();
	}
		
	/**
	 * Помогает изменить статус заказа по его номеру
	 */
	function set_purchase_processed_by_purchid( $status = 'waiting_payment' ) 
	{				
		return usam_update_order( $this->purchase_id, array('status' => $status) );
	}

	// Подготовить данные заказа и создать кнопку отправки данных на шлюз
	public function send_gateway_parameters( $payment, $payment_type = null ) 
	{		
		if ( $payment_type !== null )
			$this->payment_type = $payment_type;
	
		if ( empty($payment['gateway_id']) )
			$this->return_to_checkout( );
	
		$this->get_option( $payment['gateway_id'] );	
		
		$payment['bank_account_id'] = $this->gateway_system['bank_account_id'];
				
		$this->purchase_log = new USAM_Order( $payment['document_id'] );		
		$this->purchase_id = $this->purchase_log->get( 'id' );		
		$this->order = $this->purchase_log->get_data();		

		if ( !empty($payment['document_number']) )
		{
			$this->payment_number = $payment['document_number'];
			$this->update_payment_document( $payment );				
		}
		else	
		{ 			
			$_payment = new USAM_Payment_Document( $payment );
			$_payment->save();
			$this->payment_number = $_payment->get('document_number');	
			do_action( 'usam_set_gateway_order', $this->purchase_log);	
		}				
		$this->payment = usam_get_payment_document( $this->payment_number, 'document_number');
				
		$this->notification_url = usam_get_url_system_page('transaction-results').'/notification/'.$this->gateway_system['id'];
		$this->url_cancel_return = add_query_arg( array('payment_number' => $this->payment_number), usam_get_url_system_page('transaction-results').'/fail/'.$this->gateway_system['id']);
		$this->url_return = add_query_arg( array('payment_number' => $this->payment_number), usam_get_url_system_page('transaction-results').'/success/'.$this->gateway_system['id']);
					
		$this->submit();
	}	
	
	function get_country_code( )
	{
		$country = usam_get_country_location( );
		return isset($country['code'])?$country['code']:'';
	}
	
	function get_gateway_currency_code( )
	{
		if ( !empty($this->payment['bank_account_id']) )
		{
			$acc_number = usam_get_acc_number( $this->payment['bank_account_id'] );
			return $acc_number['currency'];
		}
		else
		{
			$local_currency_code = usam_get_currency_price_by_code();	
			return $local_currency_code;
		}
	}
	
	function convert( $price )
	{		
		return $price;
		$local_currency_code = usam_get_currency_price_by_code();	
		
		$acc_number = usam_get_acc_number( $this->payment['bank_account_id'] );
		$gateway_currency_code = $acc_number['currency'];
		
		if( $local_currency_code != $gateway_currency_code ) 
			$price = usam_format_convert_price($price, $local_currency_code, $gateway_currency_code);
		
		return $price;
	}
	

	protected function get_url( ) 
	{
		if ( $this->is_debug() && $this->test_payment_link )
			return $this->test_payment_link;
		elseif ( !empty($this->payment_link) )
			return $this->payment_link;
		else
			return $this->url_return;
	}
	
	// Параметры для отправки в платежный шлюз
	protected function get_vars( $aggregate ) 
	{
		return array();
	}
	
	// Отправить запрос
	function send_request( $url, $args )
	{	
		$data = wp_remote_post( $url, $args );
		if (is_wp_error($data))
			return $data->get_error_message();

		$resp = json_decode($data['body'],true);
		if ( isset( $resp['error'] ) ) 
		{			
			$this->set_error( $resp['error'] );	
			return false;
		}		
		return $resp;		
	}
	
	function get_url_submit( $aggregate = false ) 
	{				
		$url = $this->get_url();
		
		$this->data_sending_gateway = $this->get_vars( $aggregate );

		if ( empty($this->data_sending_gateway)) 
			return $url;
		
		$name_value_pairs = array();
		foreach ( $this->data_sending_gateway as $key => $value)
			$name_value_pairs[] = $key . '=' . urlencode($value);
			
		$gateway_values =  implode('&', $name_value_pairs);

		return $url."?".$gateway_values;				
	}
	
	/**
	 * отправить данные в торговый шлюз, продлен в торговых файлы
	 */
	protected function submit() 
	{			
		if ( !empty($this->gateway_system['handler']) )
		{ 				
			$this->set_purchase_processed_by_purchid();
			$redirect = $this->get_url_submit();
			
			if ( strlen($redirect) > 2000 ) 		
				$redirect = $this->get_url_submit( true );
			
			if ( $this->is_debug() )
			{
				echo "<a href='".esc_url($redirect)."' target='_blank'>".$redirect."</a>";
				echo "<pre>".print_r($this->data_sending_gateway,true)."</pre>";
			} 
			else
				wp_redirect( $redirect );
		}
		else
		{  
			$result = $this->set_purchase_processed_by_purchid( 'received' );			
			$this->go_to_transaction_results( );			
		} 
		$this->set_log_file();	
		exit();
	}
	
	protected function get_payment_number( ) 
	{
		if (isset($this->gateway_data['payment_number']))
			return $this->gateway_data['payment_number'];
		else
			return false;
	}

	/**
	 * Загрузить параметры
	 */
	private function load_gateway_parameters( ) 
	{			
		switch( $this->content_type ) 
		{
			case 'json':
				$json = file_get_contents('php://input'); 
				$this->gateway_data = json_decode($json);
			break;
			default:			
				$this->gateway_data = stripslashes_deep($_REQUEST);
			break;
		}				
// См. url_cancel_return	
		if (isset($this->gateway_data['payment_number']))
			$this->payment_number = $this->gateway_data['payment_number'];
		else
			$this->payment_number = $this->get_payment_number();	
		
		if ( $this->payment_number )
		{			
			$this->payment = usam_get_payment_document($this->payment_number, 'document_number');			
			if ( !empty($this->payment) )
			{ 
				$this->get_option( $this->payment['gateway_id'] );
				$this->purchase_log = new USAM_Order( $this->payment['document_id'] );		
				$this->purchase_id = $this->purchase_log->get( 'id' );	
				return true;
			}			
		}		
		return false;
	}
	
	/**
	 * разобрать уведомление шлюза, принимает и преобразует уведомления в массив, если это возможно, расширен в торговых файлы
	 */
	protected function parse_gateway_notification() { }	
	
	/**
	 * Процесс шлюза уведомления, проверяет и решает, что делать с данными шлюза, выданных в торговых файлы
	 */
	public function process_gateway_notification() 
	{		
		if( $this->load_gateway_parameters() ) 
		{ 
			if ( !$this->gateway_system['ipn'] )
				return false;		
			
			if ( $this->parse_gateway_notification() )	
			{	
		
			}			
		}		
		else
			$this->set_error( __('Не удается загрузить данные для уведомления платежа.') ); 
	
		$this->set_log_file();	
		exit();			
	}	
		
	// Вывести на странице транзакции
	protected function display_the_page_transaction() {	}
	
	// Обрабатывает результаты ответа платежного шлюза. Оповещение об успешных или не успешных платежах в автоматическом режиме
	private function result_transaction( $type_display ) 
	{	
		$message = $this->message_transaction( 'fail' );
		switch( $type_display )
		{
			case 'success':		 // Платеж успешно прошел
				switch( $this->purchase_log->get('paid') )
				{		
					case 0:	// Не оплаченные			
						$message = $this->message_transaction( 'completed' );								
					break;	
					case 1: // Частично оплачен			
						$message = $this->message_transaction( 'completed' );				
					break;
					case 2: // Оплачен	
						$this->message = $this->message_transaction( 'completed' );				
					break;
				}					
			break;					   
			case 'fail':		 // Ошибка оплаты		
				if ( isset($_REQUEST['result']) )
				{
					switch( $_REQUEST['result'] )
					{								
						case 2: // Платеж не прошел										
							$message = $this->message_transaction( 'fail' );
						break;	
						case 3: // Платеж не прошел. Старая транзакция							
							$message = $this->message_transaction( 'old_transaction' );				
						break;
						case 4: // Платеж не прошел. Заказ уже оплачен							
							$message = $this->message_transaction( 'order_paid' );
						break;
						case 5: // Платеж не прошел. Неактивный платежный шлюз								
							$message = $this->message_transaction( 'gateway' );				
						break;	
					}						
				}
			break;
		}
		return $message;
	}
	
	public function display_transaction_theme( $type_display ) 
	{			
		if( $this->load_gateway_parameters() ) 
		{ 				
			$message = $this->result_transaction( $type_display );			
			if ( $type_display == 'success' )
			{
				global $usam_cart;				
				$usam_cart->empty_cart( );		
			}
			// показывать результаты транзакции						
			if ( $this->purchase_log->get( 'status' ) == 'incomplete_sale' )
				$this->set_purchase_processed_by_purchid( 'received' );					
			
			$order_shortcode = new USAM_Order_Shortcode( $this->purchase_log );				
			$message_html = $order_shortcode->get_html( $message );	
			
			$html = $this->display_the_page_transaction();
			if ( !empty($html) )
				$message_html .= '<br>'.$html;
			
			do_action( 'usam_transaction_results_shutdown', $this->purchase_id );
			return $message_html;
		}		
		return $this->message_transaction('unknown_transaction');				
	}
	
	public function get_form( $gateway_id )
	{
		$payment_gateway = usam_get_payment_gateway( $gateway_id );				
		$this->gateway_option = !empty($payment_gateway['setting']['gateway_option'])?$payment_gateway['setting']['gateway_option']:array();				
		return $this->form();
	}
	
	protected function form()
	{
		return '';
	}	
	
	protected function set_error( $error )
	{			
		$this->error = sprintf( __('Платежные шлюзы. Оплата %s. Ошибка: %s'), $this->payment_number, $error );
	}
	
	public function set_log_file()
	{
		usam_log_file( $this->error, 'merchant_gateway' );
		$this->error = '';
	}
}

// Подключить платежный класс
function usam_get_merchant_class( $handler )
{
	$merchant_class = 'USAM_Merchant';	
	if ( $handler != '' )
	{
		$file =  USAM_FILE_PATH . '/merchants/' . $handler.'.php';
		if ( file_exists( $file ) )
		{
			require_once( $file );
			$merchant_class = 'USAM_Merchant_'.$handler;			
		}
	}
	$merchant_instance = new $merchant_class( );	
	return $merchant_instance;
}

// Обработка завершения транзакции.
function usam_get_transaction_theme( )
{
	global $wp_query;
	
	$display = 'fail';	
	if ( isset($wp_query->query['tabs']) )
	{
		$display = $wp_query->query['tabs'];
	}	
	$handler = '';
	$gateway_id = isset($wp_query->query['gateway_id'])?$wp_query->query['gateway_id']:'';	
	if ( $gateway_id )
	{ 
		$gateway = usam_get_payment_gateway( $gateway_id );		
		if ( !empty($gateway['handler']) )
			$handler = $gateway['handler'];
	}		
	$merchant_instance = usam_get_merchant_class( $handler );		
	return $merchant_instance->display_transaction_theme( $display );
}


function usam_gateway_notification( )
{
	global $wp_query;
	
	$display = 'fail';	
	if ( isset($wp_query->query['tabs']) && $wp_query->query['tabs'] == 'notification' )
	{ 
		$handler = '';
		$gateway_id = isset($wp_query->query['gateway_id'])?$wp_query->query['gateway_id']:'';							
		if (  is_numeric($gateway_id)  )
		{ 
			$gateway = usam_get_payment_gateway( $gateway_id );		
			if ( !empty($gateway['handler']) )
			{				
				$merchant_instance = usam_get_merchant_class( $gateway['handler'] );
				$merchant_instance->process_gateway_notification( );
			}
		}				
	}	
}
add_action('template_redirect','usam_gateway_notification', 22);


/*
function usam_log_file66(  ) 
{	
	$json = file_get_contents('php://input'); 
	$Log = new USAM_Log_File( 'file_name' ); 							
	$Log->fwrite_array( $_REQUEST );			
	$Log->fwrite_array( $_SERVER );		
$Log->fwrite_array( json_decode($json) );		
	$Log->file_fclose();
}
add_action('init','usam_log_file66');
*/
?>