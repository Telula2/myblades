<?php
function usam_get_payment_gateway( $id )
{
	global $wpdb;
	
	if ( !is_numeric($id) )
		return array();
	
	$cache_key = 'usam_payment_gateway';
	$cache = wp_cache_get( $id, $cache_key );
	if( $cache === false )	
	{		
		$cache = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_PAYMENT_GATEWAY." WHERE id = '$id'", ARRAY_A );			
		wp_cache_set( $id, $cache, $cache_key );
	}					
	$cache['setting'] = maybe_unserialize($cache['setting']);	
	return $cache;
}


/**
 * Функции и класс оплаты
 */
function usam_gateway_count() 
{
	global $usam_gateway;	
	return $usam_gateway->gateway_count;
}

function usam_have_gateways() 
{
	global $usam_gateway;
	return $usam_gateway->have_gateways();
}

function usam_the_gateway() 
{
	global $usam_gateway;
	return $usam_gateway->the_gateway();
}

function usam_gateway_method_select() 
{
	global $usam_gateway, $usam_cart;
	$is_checked = false;
	$selected_gateway = $usam_cart->get_properties( 'selected_gateway' );	
	if ( $selected_gateway ) 
	{
		$gateway = $usam_gateway->get_gateway( );	
		if ( $gateway->id == $selected_gateway ) 
			$is_checked = true;		
	} 
	else 
	{
		if ( $usam_gateway->current_gateway == 0 ) 
			$is_checked = true;
	}
	return $is_checked;
}

function usam_get_gateway_id() 
{
	global $usam_gateway;		
	$gateway = $usam_gateway->get_gateway( );	
	return $gateway->id;
}

function usam_print_gateway_name() 
{
	global $usam_gateway;
	$gateway = $usam_gateway->get_gateway( );
	echo $gateway->name;	
}

function usam_print_gateway_description() 
{
	global $usam_gateway;
	$gateway = $usam_gateway->get_gateway( );
	echo $gateway->description;	
}

// Получить url на картинку доставки
function usam_gateway_method_url() 
{
	global $usam_gateway;

	$gateway = $usam_gateway->get_gateway( );
	$url = USAM_CORE_IMAGES_URL."/gateway.jpg";	
	if ( !empty($gateway->img) )
	{  
		$image_attributes = wp_get_attachment_image_src( $gateway->img, 'thumbnail' );	
		if ( !empty($image_attributes[0]) )
		{
			$url = $image_attributes[0];
		}	
	}	
	return $url;
}

/**
 * Класс шлюза оплаты
 */
class USAM_Gateways 
{
	private $gateways = array();
	private $gateway;
	public $gateway_count = 0;
	public $current_gateway = -1;
	private $in_the_loop = false;

	public function __construct( )	
	{		
		global $usam_cart;	
	
		$gateways = usam_get_payment_gateways( );			

		$admin = current_user_can('administrator');
		$selected_shipping_method = $usam_cart->get_properties( 'selected_shipping' );
		foreach ( $gateways as $gateway ) 
		{		
			$setting = maybe_unserialize( $gateway->setting );				
			if ( empty($setting['condition']) || ( usam_conditions_user( $setting['condition'] ) && (empty($setting['condition']['shipping']) || in_array($selected_shipping_method, $setting['condition']['shipping'])) ))
				$this->gateways[] = $gateway;
		}	
		$selected_gateway = $usam_cart->get_properties( 'selected_gateway' );	

		if ( !$this->check_gateway_method() && !empty($this->gateways) )
			$usam_cart->set_properties( array( 'selected_gateway' => $this->gateways[0]->id ) );	
	
		$this->gateway_count = count( $this->gateways );			
	}
	
	public function check_gateway_method() 
	{
		global $usam_cart;	
		
		$result = false;
		$selected_gateway = $usam_cart->get_properties( 'selected_gateway' );	
		if ( $selected_gateway != null ) 
		{
			foreach( $this->gateways as $gateway )	
			{ 
				if ( $gateway->id == $selected_gateway)
				{
					$result = true;
					break;
				}
			}			
		}
		return $result;
	}
	
	function get_gateway() 
	{		
		return $this->gateway;
	}

	function next_gateway() 
	{
		$this->current_gateway++;
		$this->gateway = $this->gateways[$this->current_gateway];
		return $this->gateway;
	}

	function the_gateway() 
	{
		$this->in_the_loop = true;
		$this->gateway = $this->next_gateway();
	}

	function have_gateways() 
	{ 
		if ( $this->current_gateway + 1 < $this->gateway_count ) 
		{
			return true;
		} 
		else if ( $this->current_gateway + 1 == $this->gateway_count && $this->gateway_count > 0 ) 
		{				
			$this->rewind_gateways();
		}
		$this->in_the_loop = false;
		return false;
	}

	function rewind_gateways() 
	{
		$this->current_gateway = -1;
		if ( $this->gateway_count > 0 ) {
			$this->gateway = $this->gateways[0];
		}
	}
}	