<?php
class USAM_Tax
{	
	 // строковые
	private static $string_cols = array(
		'name',		
		'description',		
		'date_update',
		'setting',	
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'active',				
		'sort',			
		'type_payer',
		'is_in_price',		
	);
	// рациональные
	private static $float_cols = array(
		'value',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data    = array();		
	private $fetched = false;
	private $args    = array( 'col'   => '', 'value' => '' );	
	private $exists  = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_taxes' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$tax ) 
	{
		$id = $tax->get( 'id' );
		wp_cache_set( $id, $tax->data, 'usam_taxes' );		
		do_action( 'usam_taxes_update_cache', $tax );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$tax = new USAM_Tax( $value, $col );
		wp_cache_delete( $tax->get( 'id' ), 'usam_taxes' );	
		do_action( 'usam_taxes_delete_cache', $tax, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_taxes_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_TAXES." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_taxes_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_TAXES." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{
			$data['setting'] = unserialize( $data['setting'] );		
			$this->exists = true;
			$this->data = apply_filters( 'usam_taxes_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_taxes_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_taxes_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_taxes_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_taxes_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_taxes_pre_save', $this );	
		$where_col = $this->args['col'];
			
		if ( isset($this->data['setting']) )	
			$this->data['setting'] = serialize( $this->data['setting'] );
		
		$this->data['date_update']      = date( "Y-m-d H:i:s" );		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );
			
			do_action( 'usam_taxes_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);

			$this->data = apply_filters( 'usam_taxes_update_data', $this->data );			
			$format = $this->get_data_format( );
			$this->data_format( );
			
			$result = $wpdb->update( USAM_TABLE_TAXES, $this->data, $where, $format, array( $where_val ) );			
			do_action( 'usam_taxes_update', $this );
		} 
		else 
		{   
			do_action( 'usam_taxes_pre_insert' );		
			unset( $this->data['id'] );	
			
			$this->data = apply_filters( 'usam_taxes_insert_data', $this->data );		
			$format = $this->get_data_format( );	
			$this->data_format( );			
					
			$result = $wpdb->insert( USAM_TABLE_TAXES, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id );			
			}
			do_action( 'usam_taxes_insert', $this );
		} 		
		do_action( 'usam_taxes_save', $this );

		return $result;
	}
}



// Получить налоговые ставки
function usam_get_taxes( $args = array() ) 
{
	global $wpdb;
	
	if ( isset($args['fields']) )
	{
		$fields = $args['fields'] == 'all'?'*':implode( ",", $args['fields'] );
	}
	else
		$fields = '*';
/*=========================*/	
	$_where = array('1=1');
	if ( isset($args['active']) && $args['active'] == 0 )
		$_where[] = "active = '0'";
	else
		$_where[] = "active = '1'";
	
	if ( isset($args['type_payer']) )
		$_where[] = "type_payer = '".$args['type_payer']."'";	
	
	if ( isset($args['is_in_price']) )
		$_where[] = "is_in_price = '".$args['is_in_price']."'";	
	
	if ( isset($args['ids']) )
		$_where[] = "id IN( '".implode( "','", $args['ids'] )."' )";
	
	$where = implode( " AND ", $_where);
/*=========================*/		
	if ( isset($args['orderby']) )
	{
		$orderby = $args['orderby'];
	}
	else
		$orderby = 'sort';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($args['order']) )	
		$order = $args['order'];	
	else
		$order = 'DESC';	
	if ( isset($args['output_type']) )	
		$output_type = $args['output_type'];	
	else
		$output_type = 'OBJECT';
	
	$result = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_TAXES." WHERE $where $orderby $order", $output_type );
	return $result;
}
?>