<?php
function usam_get_delivery( $id )
{
	global $wpdb;
	
	if ( !is_numeric($id) )
		return array();
	
	$cache_key = 'usam_delivery_service';
	$cache = wp_cache_get( $id, $cache_key );
	if( $cache === false )
	{		
		$cache = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_DELIVERY_SERVICE." WHERE id = '$id'", ARRAY_A );			
		wp_cache_set( $id, $cache, $cache_key );	
	}			
	$cache['setting'] =  maybe_unserialize($cache['setting']);	
	return $cache;
}

function usam_get_delivery_name( $id )
{
	global $wpdb;
	
	if ( !is_numeric($id) )
		return '';
	
	$delivery_service = usam_get_delivery( $id );	
	if ( !empty($delivery_service) )		
		return $delivery_service['name'];
	else
		return '';
}

/**
 * Класс способов доставки
 */
class USAM_Delivery_Services
{
	private $delivery_services = array();
	private $delivery_service_disabled = array();
	private $delivery_service;
	private $delivery_service_count = 0;
	private $current_delivery_service = -1;
	private $in_the_loop = false;

	public function __construct( $args )	
	{			
		$delivery_service = usam_get_delivery_services( array('cache' => true) );
		$compare = new USAM_Compare();		
		foreach( $delivery_service as $value )
		{			
			$value->setting = maybe_unserialize( $value->setting );
			$disabled = true;						
			if ( !empty($value->setting['restrictions']) )
			{ 				
				$restrictions = $value->setting['restrictions'];
				if ( ( empty($restrictions['price_from']) || $restrictions['price_from'] <= $args['price'] ) && ( empty($restrictions['price_to']) || $restrictions['price_to'] >= $args['price'] ) )
				{	
					if ( ( empty($restrictions['weight_from']) || $restrictions['weight_from'] <= $args['weight'] ) && ( empty($restrictions['weight_to']) || $restrictions['weight_to'] >= $args['weight'] ) )
					{		
						if ( empty($restrictions['locations']) || ( !empty($args['locations']) && $compare->compare_arrays('equal', $restrictions['locations'], $args['locations']) ) )			
						{					
							$disabled = false;
						}
					}
				} 
			}			
			else
				$disabled = false;
			if ( $disabled )			
				$this->delivery_service_disabled[] = $value;
			else
				$this->delivery_services[] = $value;
		}			
		$this->delivery_service_count = count($this->delivery_services);
	}
	
	function get_delivery_services_disabled() 
	{		
		return $this->delivery_service_disabled;
	}
	
	function get_delivery_services() 
	{		
		return $this->delivery_services;
	}
		
	function get_current_services() 
	{		
		return $this->delivery_service;
	}

	function next_delivery_services() 
	{
		$this->current_delivery_service++;
		$this->delivery_service = $this->delivery_services[$this->current_delivery_service];
		return $this->delivery_service;
	}

	function the_delivery_services() 
	{
		$this->in_the_loop = true;
		$this->delivery_service = $this->next_gateway();
	}

	function have_delivery_services() 
	{
		if ( $this->current_delivery_service + 1 < $this->delivery_service_count ) 
		{
			return true;
		} 
		else if ( $this->current_delivery_service + 1 == $this->delivery_service_count && $this->delivery_service_count > 0 ) 
		{				
			$this->rewind_delivery_services();
		}
		$this->in_the_loop = false;
		return false;
	}

	function rewind_delivery_services() 
	{
		$this->current_delivery_service = -1;
		if ( $this->delivery_service_count > 0 ) {
			$this->delivery_service = $this->delivery_services[0];
		}
	}
}
