<?php
class USAM_Submit_Checkout
{
	private $error_messages = array();
	private $error_code;
	
	function __construct() 
	{ 
		if ( !empty( $_REQUEST['submit_checkout'] ) )
			add_action( 'template_redirect', array( $this, 'purchase'), 13 );
		if ( !empty( $_REQUEST['pay_the_order'] ) )
			add_action( 'init', array( $this, 'pay_the_order' ) );
	}

	function submit_checkout( ) 
	{
		$this->__construct( );
	}	
		
	private function redirect_shopping_cart( ) 
	{
		wp_redirect( usam_get_url_system_page('basket') );
		exit;
	} 
	
	private function redirect_checkout( ) 
	{
		wp_redirect( usam_get_url_system_page( 'checkout' ) );
		exit;
	} 
	
	private function redirect_transact( $args = array(), $type_display = 'fail' ) 
	{		
		$url = add_query_arg( $args, usam_get_url_system_page( 'transaction-results' ).'/'.$type_display );
		wp_redirect( $url );
		exit;
	} 
	
	/**
	 * функция проверки нажатия кнопки покупки, используется через AJAX и в нормальной загрузки страницы.
	 */
	public function purchase( )
	{ 
		global $usam_cart, $user_ID, $usam_checkout, $usam_gateway;
//	usam_start_measure_performance();	
		do_action( 'usam_before_submit_checkout' );	
		if ( $usam_checkout->validate_forms() )
		{
			$this->error_code[] = 'validate_forms';
		}	
		if ( usam_has_tnc() && ( ! isset( $_POST['agree'] ) || $_POST['agree'] != 'yes' ) ) 
		{				
			$this->error_code[] = 'has_tnc';
		}				
		if ( !wp_verify_nonce($_POST['new_transaction'],'purchase_'.$user_ID) )
		{
			$this->error_code[] = 'verify_nonce';
		}	
		if ( $usam_cart->get_properties('cart_item_count') == 0 )
		{
			$this->error_code[] = 'cart_empty';
		}		
		if ( !$usam_cart->check_shipping_method() ) 
		{				
			$this->error_code[] = 'shipping_method';
		}			
		if ( !$usam_gateway->check_gateway_method() )		
		{		
			$this->error_code[] = 'gateway';
		}		
		if ( empty($this->error_code) ) 
		{						
			$order_id = $usam_cart->save_order( );
			if ( !$order_id )
			{	
				usam_set_user_screen_error( array('save_order'), 'checkout' );
				$this->redirect_checkout();
			}	
			usam_update_customer_meta( 'checkout_location', array() );
			
			$order = new USAM_Order( $order_id );	
			$order_id = $order->get( 'id' );
			$total = $order->get( 'totalprice' );			
			
			$type_payer = usam_get_customer_meta( 'type_payer' );
			$args = array( 'type_payer' => $type_payer, 'fields' => 'id', 'fields' => 'id=>unique_name' );	
			$properties = usam_get_order_properties( $args );					
			
			if ( !empty($_POST['collected_data']) ) 
			{
				$customer_data = stripslashes_deep( $_POST['collected_data'] );
				foreach ( $customer_data as $id => $value )
				{
					if ( !empty($properties[$id]) )				
						$customer_data[$properties[$id]] = $value;
				}				
				$result = $order->save_customer_data( $customer_data );
			}
			$bonus = new USAM_Work_Bonuses();
			$bonus->submit_checkout( $order_id );	

			$payment['gateway_id'] = $usam_cart->get_properties( 'selected_gateway' );
			$payment['sum'] = $total;	
			$payment['document_id'] = $order_id;
			$payment['document_type'] =	'order';
		
			do_action( 'usam_submit_checkout', $order);	
			
			$this->send_data_payment_gateway( $payment, 'cart' ); 				
		}
		else
		{				
			usam_set_user_screen_error( $this->error_code, 'checkout' );		
			$this->redirect_checkout();			
		}	
	}	
	
	// Отправить данные в платежный шлюз
	private function send_data_payment_gateway( $payment, $payment_type )
	{
		$document_number = !empty($payment['payment_number'])?$payment['payment_number']:'';		
		if ( !empty($payment['gateway_id']) )
			$gateway = usam_get_payment_gateway( $payment['gateway_id'] );
		else
			$this->redirect_transact( array( 'error' => 5, 'payment_number' => $document_number ) );
		
		if ( empty($gateway) )
			$this->redirect_transact( array( 'error' => 5, 'payment_number' => $document_number ) );
		
		$merchant_instance = usam_get_merchant_class( $gateway['handler'] );			
	
		do_action_ref_array( 'usam_pre_submit_gateway', array( &$merchant_instance ) );				
		$merchant_instance->send_gateway_parameters( $payment, $payment_type );
	}
	
	// Оплатить заказ, из кабинета
	public function pay_the_order() 
	{		
		global $user_ID;
		if ( empty($_POST['new_transaction']) || !wp_verify_nonce($_POST['new_transaction'],'purchase_'.$user_ID) || empty($_REQUEST['gateway']) || empty($_REQUEST['payment_document']) )
		{		
			wp_redirect( usam_get_url_system_page('your-account') );
			exit;
		}		
		$gateway = absint($_REQUEST['gateway']);
		$document_number = sanitize_title($_REQUEST['payment_document']);
		$payment = usam_get_payment_document( $document_number, 'document_number' );		
		
		$purchase_log = new USAM_Order( $payment['document_id'] );
		$order = $purchase_log->get_data();		
		if ( $payment['status'] == 3 )	
		{						
			$this->redirect_transact( array( 'error' => 4, 'payment_number' => $document_number ) );					
		}
		elseif ( $payment['pay_up'] < date( "Y-m-d H:i:s" ) )	
		{						
			$this->redirect_transact( array( 'error' => 3, 'payment_number' => $document_number ) );					
		}			
		elseif ( usam_check_order_is_completed( $order['status'] ) )	
		{
			$this->redirect_transact( array( 'error' => 4, 'payment_number' => $document_number ) );					
		}			
		$payment['gateway_id'] = $gateway;	
		$this->send_data_payment_gateway( $payment, 'surcharge' );		
	}	
}
new USAM_Submit_Checkout();
?>