<?php
/**
 * Функции для управления складами
 */
 
 /**
 * Клас управления складом
 */ 
class USAM_Storage
{
	 // строковые
	private static $string_cols = array(
		'code',		
		'title',			
		'address',
		'description',	
		'date_modified',	
		'phone',	
		'schedule',	
		'meta_key',					
		'email',
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'active',				
		'issuing',			
		'shipping',	
		'sort',	
		'site_id',	
		'location_id',			
		'img',					
	);
	// рациональные
	private static $float_cols = array(	
		'GPS_N',	
		'GPS_S',
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $is_shipping_changed = false; // Изменена отгрузка
	private $is_active_changed = false; // Изменена активность
	
	private $data     = array();		
	private $products = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'code' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'code'  && $id = wp_cache_get( $value, 'usam_storage_code' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_storage_list' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_storage ) 
	{
		$id = $_storage->get( 'id' );	
		wp_cache_set( $id, $_storage->data, 'usam_storage_list' );		
		if ( $code = $_storage->get( 'code' ) )
			wp_cache_set( $code, $id, 'usam_storage_code' );
		do_action( 'usam_storage_list_update_cache', $_storage );
	}

	/**
	 * Удалить кеш	 
	 */
	public function delete_cache( ) 
	{
		wp_cache_delete( $this->get( 'id' ), 'usam_storage_list' );	
		wp_cache_delete( $this->get( 'code' ), 'usam_storage_code' );
		
		do_action( 'usam_storage_list_delete_cache', $this );	
	}
	
	public function delete( ) 
	{		
		global  $wpdb;
		
		$id = $this->get( 'id' );	
		
		do_action( 'usam_storage_list_before_delete', $this );
		
		self::delete_cache( );	
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_STORAGE_LIST." WHERE id = '$id'");
		
		do_action( 'usam_storage_list_delete', $id );
		
		return $result;
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_STORAGE_LIST." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_storage_list_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_storage_list_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_storage_list_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_storage_list_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{				
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();
		
		if ( array_key_exists( 'shipping', $properties ) ) 
		{	
			$shipping = $this->get( 'shipping' );
			if ( $properties['shipping'] != $shipping )
				$this->is_shipping_changed = true;			
		}	
		if ( array_key_exists( 'active', $properties ) ) 
		{	
			$active = $this->get( 'active' );
			if ( $properties['active'] != $active )
				$this->is_active_changed = true;			
		}			
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_storage_list_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_storage_list_pre_save', $this );	
		$where_col = $this->args['col'];	
		
		$this->data['date_modified'] = date( "Y-m-d H:i:s" );
		$this->data['author'] = $user_ID;		

		if ( isset($this->data['sort']) && $this->data['sort']>999 )		
			$this->data['sort'] = 100;	
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );			
			
			do_action( 'usam_storage_list_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);
			
			$this->data = apply_filters( 'usam_storage_list_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );			
			
			$result = $wpdb->update( USAM_TABLE_STORAGE_LIST, $this->data, $where, $format, $where_val );			
			do_action( 'usam_storage_list_update', $this );
			
			if ( $this->is_shipping_changed || $this->is_active_changed )
			{
				usam_recalculate_stock_products();		
			}
		} 
		else 
		{   
			do_action( 'usam_storage_list_pre_insert' );		
			unset( $this->data['id'] );	
						
			if ( !isset($this->data['code']) )		
			$this->data['code'] = '';

			if ( !isset($this->data['title']) )		
				$this->data['title'] = '';

			if ( !isset($this->data['active']) )		
				$this->data['active'] = 0;			
			
			if ( !isset($this->data['issuing']) )		
				$this->data['issuing'] = 0;

			if ( !isset($this->data['shipping']) )		
				$this->data['shipping'] = 0;

			if ( !isset($this->data['address']) )		
				$this->data['address'] = '';			
			
			if ( !isset($this->data['description']) )		
				$this->data['description'] = '';

			if ( !isset($this->data['phone']) )		
				$this->data['phone'] = '';

			if ( !isset($this->data['schedule']) )		
				$this->data['schedule'] = '';	
			
			if ( !isset($this->data['email']) )		
				$this->data['email'] = '';	
			
			if ( !isset($this->data['img']) )		
				$this->data['img'] = 0;
			
			if ( !isset($this->data['site_id']) )		
				$this->data['site_id'] = 0;
			
			if ( !isset($this->data['GPS_N']) )		
				$this->data['GPS_N'] = 0;
			
			if ( !isset($this->data['GPS_S']) )		
				$this->data['GPS_S'] = 0;		
						
			$this->data = apply_filters( 'usam_storage_list_insert_data', $this->data );			
			$format = $this->get_data_format(  );	
			$this->data_format( );
			
			$result = $wpdb->insert( USAM_TABLE_STORAGE_LIST, $this->data, $format );					
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
				
				$meta_key = 'storage_'.$wpdb->insert_id;	
				$wpdb->query("UPDATE `" . USAM_TABLE_STORAGE_LIST . "` SET `meta_key` = '$meta_key' WHERE id = '$this->id'");

				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_storage_list_insert', $this );
		} 		
		do_action( 'usam_storage_list_save', $this );

		return $result;
	}
}

// Обновить склад
function usam_update_storage( $id, $data )
{
	$shipped = new USAM_Storage( $id );
	$shipped->set( $data );
	return $shipped->save();
}

// Получить склад
function usam_get_storage( $id, $column = 'id' )
{
	$shipped = new USAM_Storage( $id, $column );
	return $shipped->get_data( );	
}

// Добавить склад
function usam_insert_storage( $data )
{
	$shipped = new USAM_Storage( $data );
	$shipped->save();
	return $shipped->get('id');
}

// Удалить склад
function usam_delete_storage( $id )
{
	$shipped = new USAM_Storage( $id );
	return $shipped->delete();
}

function usam_get_store_field( $id, $fled )
{ 
	$_storage = new USAM_Storage( $id );
	$data = $_storage->get_data();
	
	if ( isset($data[$fled]) )
		return $data[$fled];
		
	return false;
}

 
function usam_get_stores( $qv = array() )
{ 
	global $wpdb;
	
	$key_cache = 'usam_stores';
	if ( empty($qv) )
	{
		$cache = wp_cache_get( $key_cache );			
		if ( $cache !== false )			
		{							
			return $cache;								
		}	
	}	
	if ( isset($qv['fields']) )
	{
		$fields = $qv['fields'] == 'all'?'*':$qv['fields'];
	}
	else
		$fields = '*';
	
	$_where[] = '1=1'	;
	if ( !isset($qv['active']) || $qv['active'] == '1')
		$_where[] = "active = '1'";
	elseif ( $qv['active'] == '0' )
		$_where[] = "active = '0'";	
	
	if ( isset($qv['location_id']) )
		$_where[] = "location_id = '".$qv['location_id']."'";
		
	if ( isset($qv['issuing']) )
	{
		$issuing = !empty($qv['issuing'])?1:0;
		$_where[] = "issuing = '".$issuing."'";
	}
	
	if ( isset($qv['include']) )
		$_where[] = "id IN( '".implode( "','", $qv['include'] )."' )";
	
	$where = implode( " AND ", $_where);	
	if ( !empty($qv['condition']) ) 
	{		
		foreach ( $qv['condition'] as $condition )
		{					
			$select = '';
			if ( empty($condition['col']) )
				continue;
			
			switch ( $condition['col'] )
			{		
				case 'code' :
					$select = "code";			
				break;				
				default:				
					$select = $condition['col'];			
				break;				
			}
			if ( $select == '' )
				continue;
			
			$compare = "=";	
			switch ( $condition['compare'] ) 
			{
				case '<' :
					$compare = "<";					
				break;
				case '=' :
					$compare = "=";					
				break;	
				case '!=' :
					$compare = "!=";					
				break;
				case '>' :
					$compare = ">";					
				break;				
			}
			$value = $condition['value'];
			
			if ( empty($condition['relation']) )
				$relation = 'AND';
			else
				$relation = $condition['relation'];
			
			$where .= $wpdb->prepare( " $relation $select $compare %s", $value );			
		}
	}	
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'DESC';	
	if ( isset($qv['output_type']) )	
		$output_type = $qv['output_type'];	
	else
		$output_type = 'OBJECT';
	
	if ( $where != '' )
		$where = " WHERE $where ";
	
	$storages = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_STORAGE_LIST." $where $orderby $order", $output_type );
	
	if ( empty($qv) )
		wp_cache_set( $key_cache, $storages );		
	return $storages;
}
?>