<?php
// Подключить файл темы
function usam_get_template_part( $slug, $name = '' ) 
{
	$template = '';
	if ( $name && ! USAM_THEMES_PATH )
		$template = locate_template( array( "{$slug}-{$name}.php", USAM_THEMES_PATH . "{$slug}-{$name}.php" ) );	

	if ( ! $template && $name && file_exists( USAM_CORE_THEME_PATH . "{$slug}-{$name}.php" ) )
		$template = USAM_CORE_THEME_PATH. "{$slug}-{$name}.php";
	
	if ( ! $template )
		$template = locate_template( array( "{$slug}.php", USAM_THEMES_PATH."{$slug}.php" ) );
	
	$template = apply_filters( 'usam_get_template_part', $template, $slug, $name );

	if ( $template )
		load_template( $template, false );
}

/**
 * Проверяет активную папку темы для конкретного файла, если он существует, то вернуть активную тему URL, в противном случае вернуть глобальную usam_theme_url
 * @since 3.8
 */
function usam_get_template_file_url( $file, $group = '' ) 
{		
	if ( $group != '' )
		$template_dir = "$group/";
	else
		$template_dir = '';
	
	$file_path =  USAM_THEMES_PATH.$template_dir.$file;
	
	if ( file_exists( $file_path ) )
		$file_url =  USAM_THEME_URL.$template_dir.$file;
	else
		$file_url = USAM_CORE_THEME_URL . $file;	
	if ( is_ssl() )
		$file_url = str_replace('http://', 'https://', $file_url);		
	return $file_url;
}

/**
 * Проверяет активную папку темы для конкретного файла, если он существует, то вернуть активную директорию темы в противном случае вернет глобальное usam_theme_path
 * @since 3.8
 */
function usam_get_template_file_path( $file, $folder = '' )
{	
	if ( $folder != '' )
		$template_dir = "$folder/";
	else
		$template_dir = '';
	
	$file_path = USAM_THEMES_PATH.$template_dir.$file.'.php';	//Ищите файл в папке магазина	
	if ( file_exists( $file_path ) )		
		return $file_path;
	
	$file_path = USAM_CORE_THEME_PATH .$template_dir.$file.'.php'; //Ищите файл в шаблоне	
	if ( file_exists( $file_path ) )		
		return $file_path;
	
	return false;
}

/**
 * Подключает заданный файл
 * @since 3.8
 */
function usam_include_template_file( $file, $folder = '' )
{
	$file_path = usam_get_template_file_path( $file, $folder );	
	if ( $file_path )
		include( $file_path );
}

function usam_load_template( $file, $folder = '' )
{	
	do_action( 'usam_before_load_template' );
	
	$file_path = usam_get_template_file_path( $file, $folder );		
	if ( $file_path )
		load_template( $file_path );	

	do_action( 'usam_after_load_template' );
}

function usam_get_content( ) 
{
	global $wp_query, $usam_query;	
	
	do_action( 'usam_get_content' );

	$obj = $wp_query->get_queried_object();
	$content = isset( $obj->post_content ) ? $obj->post_content : '';
	$post_page = isset( $wp_query->query_vars['pagename'] ) ? $wp_query->query_vars['pagename'] : '';
	
	$replace_query = false;	
	$templates = array();	
	if( $term = get_query_var( 'usam-category' ) ||
		$term = get_query_var( 'usam-brands' ) ||
		$term = get_query_var ('product_tag' ) || 
		$term = get_query_var('usam-category_sale') )		
	{		
		$templates[] = usam_get_template_file_path("content-page-products-{$term}");
		$templates[] = usam_get_template_file_path("content-page-products");		
	}	
	elseif( is_single() && get_query_var( 'post_type' ) == 'usam-product' )		
	{						
		global $post;					
		$templates[] = USAM_THEMES_PATH."content-single_product-{$post->post_name}.php";
		$templates[] = USAM_THEMES_PATH."content-single_product-{$post->ID}.php";
		$templates[] = usam_get_template_file_path("content-single_product");	
	}			
	elseif( is_page()  )	
	{		
		$pages = array( 'productspage', 'newarrivals', 'checkout', 'basket', 'transaction_results', 'your_account', 'shareonline', 'reviews', 'compare_products', 'wishlist', 'map', 'brands', 'readysets', 'special_offer', 'your_subscribed', 'search', 'recommend', 'popular' );
		
		foreach( $pages as $page )
		{	
			if ( preg_match("/\[$page\]/", $content) )
			{ 						
				$templates[] = usam_get_template_file_path("content-page-$page");			
				if ( in_array($page, array( 'compare_products', 'wishlist', 'special_offer') ) )
				{
					$replace_query = true;
				}				
				break;
			}
		}
	}				
	if ( !empty($templates) )
	{ 				
		foreach( $templates as $template )
		{ 
			if ( !empty($template) && file_exists( $template ) )
			{ 					
				if ( $replace_query )
				{
					list($wp_query, $usam_query) = array( $usam_query, $wp_query );
				}
				include( $template );				
				if ( $replace_query )
				{
					list($wp_query, $usam_query) = array( $usam_query, $wp_query );
				}
			}
		}
	} 			
}

//===================================================== ШАБЛОНЫ МОДУЛЕЙ =============================================

/*
 * Получить ссылку на файлу шаблона
 */
function usam_get_module_template_url( $type_template, $template, $file = 'style.css' )
{
	ob_start();	
	$file_name = USAM_THEMES_PATH . "$type_template/$template/$file";				
	if ( !file_exists($file_name) )
		$file_url = USAM_CORE_THEME_URL. "$type_template/$template/$file";	
	else
		$file_url = USAM_THEME_URL . "$type_template/$template/$file";			
	
	if ( is_ssl() )
		$file_url = str_replace('http://', 'https://', $file_url);	

	return $file_url;
}

/**
 * Получить путь к файлу шаблона
 */
function usam_get_module_template_file( $type_template, $template, $file = 'index.php' )
{	
	$file_name = USAM_THEMES_PATH ."$type_template/$template/$file";		
	if ( !file_exists($file_name) )
		$file_name = USAM_CORE_THEME_PATH. "$type_template/$template/$file";	

	if ( !file_exists($file_name) )
		$file_name = '';		

	return $file_name;
}

// Получить шаблоны
function usam_get_templates( $type )
{	
	$template_list = array();
	$dir_path = USAM_CORE_THEME_PATH. $type;
	if ($dir = opendir($dir_path)) 
	{			
		while ( ($name_template = readdir( $dir )) !== false ) 
		{			
			if ($dir_mailtemplate = opendir($dir_path. '/'.$name_template)) 
			{
				while ( ($file = readdir( $dir_mailtemplate )) !== false ) 
				{	
					if ( $file == 'index.php'  )
					{
						$data = get_file_data( $dir_path.'/'.$name_template.'/style.css', array('ver'=>'Version', 'author'=>'Author', 'name'=>'Theme Name' ) );
						$data['screenshot'] = USAM_CORE_THEME_URL."{$type}/{$name_template}/screenshot.jpg";
						$template_list[$name_template] = $data;
						break;
					}
				}
			}
		}
	}		
	return $template_list;
}

function usam_get_templates_display( $type, $active, $url = '' )
{
	$list = usam_get_templates( $type.'template' );		
	?>		
	<div class ="theme-browser content-filterable rendered" >
		<div class ="themes wp-clearfix" >
		<?php				
		foreach ($list as $template => $data ) 
		{
			$class = $template == $active?'active':'';			
			?>				
			<div class="theme <?php echo $class; ?>" tabindex="0" aria-describedby="<?php echo $template; ?>-action <?php echo $template; ?>-name">
	
				<div class="theme-screenshot">
					<img src="<?php echo $data['screenshot']; ?>" alt="">
				</div>				
				<span class="more-details"><?php echo __('Автор', 'usam').": ".$data['author']; ?></span>				
				<h3 class="theme-name"><?php echo $template; ?></h3>

				<div class="theme-actions">
					<a class="button button-primary" href="<?php echo usam_url_admin_action('install_'.$type, $url, array( 'theme' => $template ) ); ?>
					"><?php _e('Установить', 'usam'); ?></a>						
				</div>
			</div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
}

//===================================================== РАССЫЛКА =============================================

/**
 * Получить ссылку на файл шаблона рассылки
 */
function usam_get_email_template_url( $file, $template )
{
	if ( $template == '' )
	{
		$template = get_option('usam_mailtemplate');	
		if ( $template == '' )
		{
			$templates = usam_get_templates( 'mailtemplate' );
			$template = key($templates);	
			update_option('usam_mailtemplate', $template);	
		}
	}	
	return usam_get_module_template_url( 'mailtemplate', $template, $file );
}


/**
 * Получить путь к файл шаблона рассылки
 */
function usam_get_email_template( $file = 'index', $template = '' )
{	
	if ( $template == '' )
	{
		$template = get_option('usam_mailtemplate');	
		if ( $template == '' )
		{
			$templates = usam_get_templates( 'mailtemplate' );
			$template = key($templates);	
			update_option('usam_mailtemplate', $template);	
		}
	}
	$file_name = usam_get_module_template_file( 'mailtemplate', $template, $file.'.php' );

	$out = '';
	if ( $file_name != '' )
	{
		ob_start();	
		include $file_name;			
		$out = ob_get_contents();
		ob_end_clean();
		
		$out = trim($out);
	}	
	return $out;
}


function usam_upload_theme( $theme_slug )
{
	if ( ! empty( $theme_slug ) ) 
	{			
		ob_start();
		try 
		{ 
			$theme = wp_get_theme( $theme_slug ); //&& !empty($theme['Version'])
			if ( !$theme->exists()  ) 
			{ 
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
				include_once( ABSPATH . 'wp-admin/includes/theme.php' );

				WP_Filesystem();		
				$theme_url = "http://wp-universam.ru/wp-content/uploads/sl/downloadables/wp-theme/{$theme_slug}/{$theme_slug}.zip";

				$skin     = new Automatic_Upgrader_Skin;					
				$upgrader = new Theme_Upgrader( $skin );							
				$result   = $upgrader->install( $theme_url );
				if ( is_wp_error( $result ) ) {
					throw new Exception( $result->get_error_message() );
				} elseif ( is_wp_error( $skin->result ) ) {
					throw new Exception( $skin->result->get_error_message() );
				} elseif ( is_null( $result ) ) {
					throw new Exception( 'Unable to connect to the filesystem. Please confirm your credentials.' );
				}
			}
			switch_theme( $theme_slug );
		} 
		catch ( Exception $e ) 
		{
			new WP_Error( 'usam_theme_installer', __( "Не загрузить тему для Универсам.", 'usam' ) );	
		}			
		ob_end_clean();
	}
}
?>