<?php
/**
 * фильтры поиска
 */
class USAM_Search_Hook_Filter 
{	
	/*
	* Включить скрипт поиска
	*/
	public static function add_frontend_script_style() 
	{		
		wp_enqueue_script( 'jquery-ui-autocomplete' );	
	}
		
	public static function search_in_cat_where( $where ) 
	{
		global $wpdb;
		
		if ( !empty($_REQUEST['cat_slug']) )
		{				
			$term_id = absint($_REQUEST['cat_slug']);
			$where = " AND $wpdb->term_taxonomy.taxonomy = 'usam-category' AND $wpdb->term_taxonomy.term_id IN (".$term_id.") ".$where;
		}
		return $where;		
	}
	
	public static function search_in_cat_join( $join ) 
	{
		global $wpdb; 
		if ( !empty($_REQUEST['cat_slug']) )					
			$join = "INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)";
		
		return $join;
	}	
	
	public static function search_by_site( $search, $wp_query )
	{ 
		global $wpdb;
		
		//remove_filter( 'posts_search', array('USAM_Search_Hook_Filter', 'search_by_site'), 500, 2 );	
		$q = $wp_query->query_vars;
		if ( empty( $search) || !isset($q['s']) )
			return $search; // пропустить обработку - нет поиска в запросе
		
		$search = '';		
		$s = esc_sql( $wpdb->esc_like( trim($q['s']) ) );	
		
		$search .= "$wpdb->posts.post_title LIKE LOWER ('%{$s}%') ";
		
		$search_terms = explode( ' ', $s );			
		if ( !empty($search_terms) && count($search_terms) > 1 )
		{
			$variants = array();
			foreach	( $search_terms as $term ) 
			{		
				if ( strlen($term) > 3 )
					$variants[] = "($wpdb->posts.post_title LIKE '{$term}%' OR $wpdb->posts.post_title LIKE '%{$term}%')";				
			}	
			if ( !empty($variants) )
				$search .= " OR ".implode(" AND ", $variants);
		}			
		$search .= "OR $wpdb->posts.ID IN (SELECT $wpdb->term_relationships.object_id FROM $wpdb->term_relationships 
		INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) 
		INNER JOIN $wpdb->terms ON ($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id) 
		WHERE $wpdb->term_taxonomy.taxonomy = 'usam-brands' AND $wpdb->terms.name LIKE LOWER ('%{$s}%') ) ";	

		$search .= "OR $wpdb->posts.id IN (SELECT ID FROM $wpdb->posts AS p, $wpdb->postmeta AS m WHERE p.ID = m.post_id AND m.meta_value = '$s' AND m.meta_key = '_usam_search')";	
		
		$args = array( 'hide_empty' => 0, 'meta_value' => 1, 'meta_key' => 'usam_search', 'fields' => 'ids' );
		$terms_ids = get_terms('usam-product_attributes', $args);	
		foreach	( $terms_ids as $id ) 
			$search .= "OR $wpdb->posts.id IN (SELECT ID FROM $wpdb->posts AS p, $wpdb->postmeta AS m WHERE p.ID = m.post_id AND m.meta_value = '$s' AND m.meta_key = '_usam_product_attributes_$id')";	
		
				
		if (strripos($s, ' ') === false) 
		{
			$search .= "OR $wpdb->posts.id IN (SELECT ID FROM $wpdb->posts AS p, $wpdb->postmeta AS m WHERE p.ID = m.post_id AND m.meta_value = '$s' AND m.meta_key = '_usam_sku')";	
		}
		$search = "AND sprice.meta_value>0 AND ($search)";		
		return $search;
	}	
	
	public static function posts_orderby( $orderby, $wp_query ) 
	{
		global $wpdb;
				
		$q = $wp_query->query_vars;
		if (isset($q['orderby']) && $q['orderby'] == 'predictive' && !empty($q['s']) ) 
		{
			$term = esc_sql( $wpdb->esc_like( trim($q['s']) ) );
			$orderby = "IF(pstock_sort.meta_value>0,1,0) DESC, $wpdb->posts.post_title NOT LIKE '{$term}%' ASC, $wpdb->posts.post_title ASC";
		}		
		return $orderby;
	}	
	
	public static function product_join( $sql, $wp_query ) 
	{	
		global $wpdb, $type_price;		
		
		if ( !empty($wp_query->query_vars['s']) )
		{		
			$sql .= " INNER JOIN $wpdb->postmeta AS pstock_sort ON ($wpdb->posts.id = pstock_sort.post_id AND pstock_sort.meta_key = '_usam_stock')";	
			$sql .= " INNER JOIN $wpdb->postmeta AS sprice ON ($wpdb->posts.id = sprice.post_id AND sprice.meta_key = '_usam_price_{$type_price}')";	
		}			
		return $sql;	
	}	
		
	public static function posts_request_unconflict_role_scoper_plugin( $posts_request, $wp_query ) 
	{
		$posts_request = str_replace('1=2', '2=2', $posts_request);		
		return $posts_request;
	}
}
?>