<?php
/**
 * выводит результаты поиска
 */
class USAM_Search_Shortcodes 
{	
	public static function get_id_excludes() 
	{
		$exclude_products = get_option('usam_search_exclude_products', '');
		if (is_array($exclude_products))
			$exclude_products = implode(",", $exclude_products);
		return $exclude_products;
	}
	
	public static function get_product_sku( $product_id ) 
	{
		if ( get_option('usam_search_sku_enable', true) )
		{
			$sku = usam_get_product_meta( $product_id, 'sku', true );
			$out = "<div class = 'rs_rs_sku'>".__('Артикул', 'usam').": <span>$sku</span></div>";
		}
		else
			$out = "";
		return $out;
	}
	
	private static function get_product_stock_title( $product_id ) 
	{
		if ( get_option('search_in_stock_enable', true) )
		{
			$stock = usam_get_product_meta( $product_id, 'stock' );	
			if ( $stock >0 )
				$title = __('В наличии', 'usam');
			else
				$title = __('Продан', 'usam');
			$out = "<div class = 'rs_rs_stock_title'>$title</div>";
		}
		else
			$out = "";
		return $out;
	}	
	
	
	public static function get_product_price( $product_id ) 
	{
		if ( get_option('usam_search_price_enable', true) )
		{
			$price = usam_get_product_price_currency( $product_id );			
			$out = "<div class = 'rs_rs_price'>$price</div>";
		}
		else
			$out = "";
		return $out;
	}
	
	public static function get_product_tags( $product_id ) {}	
	
	public static function strip_shortcodes ($content='')
	{
		$content = preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $content);		
		return $content;
	}	
	
	public static function get_product_description( $product ) 
	{
		$text_lenght = get_option('usam_search_text_lenght', 100);	
		if ( trim( $product->post_excerpt ) == '') 
			$description = usam_limit_words( strip_tags( USAM_Search_Shortcodes::strip_shortcodes( strip_shortcodes( $product->post_content ) ) ),$text_lenght,'...');
		else
			$description = usam_limit_words( strip_tags( USAM_Search_Shortcodes::strip_shortcodes( strip_shortcodes( $product->post_excerpt ) ) ),$text_lenght,'...');
		//$description = preg_replace('/('.implode('|', $search_key) .')/iu', '<strong class="search-excerpt">\0</strong>', stripslashes($description) );
		return $description;
	}	
	
	public static function get_product_categories( $product_id ) 
	{	
		if ( get_option('usam_search_categories_enable', true) )
		{
			$categories = get_the_terms( $product_id , 'usam-category' );	
			if ( empty($categories) )
				return $out = "";
			
			$i = 0;
			$sumbol = ' &raquo; ';
			$category = current( $categories );	
			$id = (int)$category->term_id;
			$link_cat = "<a href='".get_term_link($id, "usam-category")."'>".$category->name."</a>";		
			$terms = get_ancestors( $category->term_id, 'usam-category' );
			foreach ($terms as $term) 
			{				
				$term_data = get_term_by('id', $term, 'usam-category');
				$link_cat = "<a href='".get_term_link($term, "usam-category")."'>".$term_data->name."</a>".$sumbol.$link_cat;	
				$i++;			
			}		
			$out = "<div class = 'rs_rs_cat'>$link_cat</div>";
		}
		else
			$out = "";
		return $out;		
	}
	
	public static function get_product_category($product_id, $show = true) 
	{	
		if ( $show )
		{
			$categories = get_the_terms( $product_id , 'usam-category' );			
			$category = current( $categories );	
			$id = (int)$category->term_id;
			$link_cat = "<a href='".get_term_link($id, "usam-category")."'>".$category->name."</a>";
			$out = "<div class = 'rs_rs_cat'>$link_cat</div>";
		}
		else
			$out = "";
		return $out;		
	}
	
	public static function number_of_products_in_cat( $products )
	{		
		$cat = array();				
		foreach ( $products as $product ) 
		{			
		 	$terms = get_the_terms( $product->ID, 'usam-category' );					
			foreach ((array)$terms as $term) 
			{	//term_taxonomy_id	
				if ( !isset($term->term_id) )
					continue;
				
				if ( isset($cat[$term->term_id]) )
				{
					$cat[$term->term_id]['name'] = $term->name;
					$cat[$term->term_id]['count']++;	
				}
				else
					$cat[$term->term_id] = array( 'name' => $term->name, 'count' => 1);	
			}
		}	
		return $cat;
	}
	// строит список категорий в сайтбаре
	public static function category_selection($category_children, $number_cat, $recursion = 0)
	{		
		$str = '';
		if (!empty($category_children))	
		{				
			$prefix = str_repeat( '&nbsp;&nbsp;&nbsp;', $recursion );
			$recursion++;			
			foreach((array)$category_children as $id => $value)
			{	
				if ( is_array($value) )		
				{
					$cat_id = $id;
					$sub_category_data = USAM_Search_Shortcodes::category_selection($value, $number_cat, $recursion);	
				}
				else
				{
					$cat_id = $value;			
					$sub_category_data = '';
				}
				if ( isset($number_cat[$cat_id]) )
					$str .="<option value='".$cat_id."'>".$prefix.$number_cat[$cat_id]['name']." (".$number_cat[$cat_id]['count'].")</option>";	
				$str .= $sub_category_data;	
			}			
		}	
		return $str;
	}	
		
	// Вывести товары
	private static function get_product_out( $products, $search_key )
	{
		$html = '';
		if (!empty($products))	
		{				
			foreach ( $products as $product ) 
			{				
				$stock = (int)usam_get_product_meta( $product->ID, 'stock' );	
				if ( $stock > 0 )
					$class = 'in_stock';
				else					
					$class = 'not_available';					
						
				$title_output = preg_replace('/('.implode('|', $search_key) .')/iu', '<strong class="search-excerpt">\0</strong>', stripslashes($product->post_title) );
				$price_output = USAM_Search_Shortcodes::get_product_price( $product->ID );	
				$sku_output = USAM_Search_Shortcodes::get_product_sku( $product->ID );					
				$stock_title_output = USAM_Search_Shortcodes::get_product_stock_title( $product->ID );					
				$cats_output = USAM_Search_Shortcodes::get_product_categories( $product->ID );	
				$description_output = USAM_Search_Shortcodes::get_product_description( $product );	
				$tags_output = USAM_Search_Shortcodes::get_product_tags( $product->ID );				
				$html .= '
				<div id = "product-'.$product->ID.'" class="rs_result_row '.$class.'">
					<span class="rs_rs_avatar"><a href="'.$product->guid.'">'.usam_get_product_thumbnail( $product->ID, array(100, 100), $product->post_title ).'</a></span>
					<div class="rs_content">
						<a href="'.$product->guid.'"><span class="rs_rs_name">'.$title_output.'</span></a>
						'.$price_output.$stock_title_output.$sku_output.'
						<div class="rs_rs_description">'.$description_output.'</div>'.$cats_output.$tags_output.'
					</div>
				</div>';					
				$html .= '<div style="clear:both"></div>';					
			}
		}	
		return $html;
	}	
	
	public static function display_search( $attributes = array() ) 
	{ 
		global $wp_query;	
		$p = 0;
		$row = 5;
		$search_keyword = '';	
		$tag_slug = '';
		$extra_parameter = '';			
		$id_excludes = USAM_Search_Shortcodes::get_id_excludes();
		add_filter('posts_where', array('USAM_Search_Hook_Filter', 'search_in_cat_where'), 500, 2 );
		add_filter('posts_join', array('USAM_Search_Hook_Filter', 'search_in_cat_join'), 500, 2 );	
		if (isset($wp_query->query_vars['keyword'])) 
			$search_keyword = stripslashes( strip_tags( urldecode( $wp_query->query_vars['keyword'] ) ) );
		else 
			if ( isset($_REQUEST['rs']) ) 
				$search_keyword = stripslashes( strip_tags( trim($_REQUEST['rs'] )) );
		
		$start = $p * $row;
		$end_row = $row;		
		if ($search_keyword != '') 
		{
			$args = array( 's' => $search_keyword, 'offset'=> $start, 'orderby' => 'predictive', 'order' => 'ASC', 'post_status' => 'publish', 'exclude' => $id_excludes, 'posts_per_page' => $row, 'suppress_filters' => FALSE);				
						
			if ( isset($_REQUEST['cat_slug']) )
				$args['usam-category'] = $_REQUEST['cat_slug'];	
			
			$search_products = usam_get_products( $args );				
			$html = '<p class="rs_result_heading">'.__('Список всех результатов поиска', 'usam').' | '.$search_keyword.'</p>';		
			if ( $search_products && count($search_products) > 0 )
			{					
				unset($args['posts_per_page']);
				$total_args = $args;
				$total_args['nopaging'] = true;
				$total_args['offset'] = 0;
			//	$total_args['fields'] = 'ids';
				$total_args['update_post_meta_cache'] = false;
			//	$total_args['cache_results'] = false;
				$total_args['update_post_term_cache'] = true;				
					
				$search_all_products = usam_get_products( $total_args );					
				$number_of_products_in_cat = USAM_Search_Shortcodes::number_of_products_in_cat( $search_all_products );	
				$category_children = get_option('usam-category_children');
				
				$html .= '<div class = "select_category_box">';	
				$html .= '<select name="rr_category" id="rr_category" class = "rr_category">';	
				$html .= '<option value="0">Поиск по всем категориям</option>';			
				$html .= USAM_Search_Shortcodes::category_selection($category_children, $number_of_products_in_cat );
				$html .= '</select>';	
				$html .= '</div>';
				$html .= '<div class="rs_ajax_search_content">';	
				$search_key = explode(" ",$search_keyword);			
				$html .= USAM_Search_Shortcodes::get_product_out( $search_products, $search_key );
				$html .= '</div>';
				if ( count($search_all_products) > $row )
				{
					$nonce = wp_create_nonce("usam-get-result-search-page");
					$rs_more_result = '<span class="p_data">'.($p + 1).'</span><img src="'.USAM_CORE_IMAGES_URL.'/more-results-loader.png" /><div><em>'.__('Загрузка еще результатов...', 'usam').'</em></div>';
					$html .= '<div id="search_more_rs"></div><div style="clear:both"></div><div id="rs_more_check"></div><div class="rs_more_result">'.$rs_more_result.'</div>';					
					$loading_div = '<div style="text-align:center; padding:30px;"><img src="'.USAM_CORE_IMAGES_URL.'/loading.gif"/></div>';
					$html .= "<script>jQuery(document).ready(function() {
var search_rs_obj = jQuery('#rs_more_check');
var is_loading = false;
	
jQuery('select#rr_category').change(function()
{				
	var category = jQuery('select#rr_category').val();
	var search_more = jQuery('#search_more_rs .rs_ajax_search_content'); 
	jQuery('.rs_more_result').html('".$rs_more_result."').fadeOut(2000);
	search_more.detach(); 	
	jQuery('.rs_ajax_search_content').empty().html('".$loading_div."');	
	var urls = '&p=0&row=".$row."&cat_slug='+category+'&q=".$search_keyword.$extra_parameter."&usam_ajax_action=get_result_search_page&security=".$nonce."';

	jQuery.post('index.php', urls, function(theResponse)
	{		
		jQuery('.rs_ajax_search_content').html(theResponse);
		is_loading = false;		
	});
	return false;
	
});
	
function auto_click_more() 
{
	if (is_loading == false) 
	{
		var visibleAtTop = search_rs_obj.offset().top + search_rs_obj.height() >= jQuery(window).scrollTop();
		var visibleAtBottom = search_rs_obj.offset().top <= jQuery(window).scrollTop() + jQuery(window).height();
		if (visibleAtTop && visibleAtBottom) 
		{			
			is_loading = true;
			jQuery('.rs_more_result').fadeIn('normal');
			var p_data_obj = jQuery('.rs_more_result .p_data');
			var p_data = p_data_obj.html();
			p_data_obj.html('');
			var category = jQuery('select#rr_category').val();
			var urls = '&p='+p_data+'&row=".$row."&cat_slug='+category+'&q=".$search_keyword.$extra_parameter."&usam_ajax_action=get_result_search_page&security=".$nonce."';
			jQuery.post('index.php', urls, function(theResponse)
			{			
				if(theResponse != '')
				{
					var num = parseInt(p_data)+1;
					p_data_obj.html(num);
					jQuery('#search_more_rs').append(theResponse);
					is_loading = false;
					jQuery('.rs_more_result').fadeOut('normal');					
				}
				else
				{
					jQuery('.rs_more_result').html('<em>".__('Нет больше результатов для вывода', 'usam')."</em>').fadeOut(2000);
				}
			});
			return false;
		}
	}
}
jQuery(window).scroll(function(){
	auto_click_more();
});
auto_click_more();
});</script>";
				}
			} 
			else 
				$html .= '<p style="text-align:center">'.__('Ничего не найдено! Попробуйте изменить параметры поиска и попробуйте снова.', 'usam').'</p>';						
			return $html;
		}
	}
	
	public static function get_result_search_page() 
	{
		check_ajax_referer( 'usam-get-result-search-page', 'security' );
		add_filter( 'posts_search', array('USAM_Search_Hook_Filter', 'search_by_site'), 500, 2 );
		add_filter( 'posts_orderby', array('USAM_Search_Hook_Filter', 'posts_orderby'), 500, 2 );
		add_filter( 'posts_join', array('USAM_Search_Hook_Filter', 'product_join'), 500, 2 );
		add_filter( 'posts_request', array('USAM_Search_Hook_Filter', 'posts_request_unconflict_role_scoper_plugin'), 500, 2);
		
		add_filter('posts_where', array('USAM_Search_Hook_Filter', 'search_in_cat_where'), 500, 2 );
		add_filter('posts_join', array('USAM_Search_Hook_Filter', 'search_in_cat_join'), 500, 2 );
	
		$p = 0;
		$row = 5;
		$search_keyword = '';
		$cat_slug = '';
		$tag_slug = '';
		$extra_parameter = '';		
		$id_excludes = USAM_Search_Shortcodes::get_id_excludes();
		if ( isset($_REQUEST['p']) ) $p = absint($_REQUEST['p']);
		if ( isset($_REQUEST['row']) ) $row = absint( $_REQUEST['row'] );
		if ( !empty($_REQUEST['q']) )  $search_keyword = trim(stripslashes( strip_tags( $_REQUEST['q'] ) ));
		if ( !empty($_REQUEST['scat']) ) $cat_slug = trim(stripslashes( strip_tags( $_REQUEST['scat'] ) ));
		if ( !empty($_REQUEST['stag']) ) $tag_slug = trim(stripslashes( strip_tags( $_REQUEST['stag'] ) ));
		
		$start = $p * $row;
		$end = $start + $row;
		
		$html = '';
		if ($search_keyword != '')
		{	
			$args = array( 's' => $search_keyword, 'posts_per_page' => $row+1, 'offset'=> $start, 'orderby' => 'predictive', 'order' => 'ASC', 'post_type' => 'usam-product', 'post_status' => 'publish', 'exclude' => $id_excludes, 'suppress_filters' => FALSE);	
						
			if ( $cat_slug != '' )
				$args['usam-category'] = $cat_slug;			
			
			$search_products = get_posts($args);		
			if ( $search_products && count($search_products) > 0 )
			{
				$html .= '<div class="rs_ajax_search_content">';			
				$search_key = explode(" ",$search_keyword);	
				$html .= USAM_Search_Shortcodes::get_product_out( $search_products, $search_key );
				if ( count($search_products) <= $row ) 
				{					
					$html .= '';
				}				
				$html .= '</div>';
			}			
		}	
		echo $html;		
		die();
	}
}
?>