<?php 
new USAM_WebSpy();

// Класс для получения данных товаров на других сайтах
class USAM_WebSpy
{
	public $message = array();
	public $errors = array();
	
	public function __construct( ) 
	{		
		if ( isset($_REQUEST['check_product_availability_item']))
			add_action( 'usam_edit_product', array($this, 'check_product_availability_item'), 99, 1);
		if ( isset($_POST['check_products_availability']))
			$this->request_check_balances( );
		
		add_action( 'usam_ten_minutes_cron_task', array($this, 'check_products_availability_cron'),12 );// Функция проверки остатков. Формирует массив данных.
	}
	
	// Функция проверки остатков. Формирует массив данных. Работает через Cron
	function check_products_availability_cron()
	{	
		$hours = date_i18n( 'G', current_time('timestamp') );			
		if ( $hours < 9 )
		{			
			$option = get_option('usam_settings_sites_suppliers');
			$sites = maybe_unserialize( $option );	
			if ( !empty($sites) )	
			{				
				$this->check_products_availability( 10 );
				$this->set_log_file();
			}			
		}
	}
	
	public function request_check_balances( $limit = 50 )
	{	
		$this->check_products_availability( 30 );
		usam_set_user_screen_message( $this->message, 'webspy' );		
		usam_set_user_screen_error( $this->errors, 'webspy' );	
	}
	
	
	// Функция проверки остатков. Формирует массив данных. 
	private function check_products_availability( $limit = 50 )
	{	
		$d = date('Y-m-d H:i:s');
		$date = new DateTime($d);
		$date->modify("-10 hours");
		$check_date = $date->format('Y-m-d H:i:s');
		
		$args = array('cache_results' => false, 'update_post_term_cache' => false, 'fields' => 'ids', 'meta_query' => array( array( 'key' => '_usam_date_externalproduct', 'value' => $check_date, 'compare' => '<' )));
		$products = usam_get_products( $args );	
		$this->start_check_products_availability( $products );	
	}
		
	// Функция проверки остатков переданного номера товара. Формирует массив данных
	function check_product_availability_item( $id )
	{		
		$this->start_check_products_availability( array( $id ) );	
			
		usam_set_user_screen_message( __("Цена и наличие товара на сайте поставщика проверено","usam"), 'webspy' );		
		usam_set_user_screen_error( $this->errors, 'webspy' );	
	}

	function extract_the_contents( $text, $name_blok )
	{
		preg_match("/(<$name_blok>)(.*)(<\/$name_blok>)/", $text, $out);
		return $out[0];
	}
	
	function set_stock_product( $product_id, $status, $store )
	{
		global $wpdb;	
		if ( $status == true )
			$stock = 999999;
		else
			$stock = 0;
		$update_stock = $wpdb->update( $wpdb->postmeta, array( 'meta_value' => $stock ),array( 'meta_key' => $store, 'post_id' => $product_id ), array( '%d' ));		
		
		usam_recalculate_stock_product( $product_id );		
		return $update_stock;
	}
	
	private function set_error( $product_id, $error )
	{			
		$this->errors[] = sprintf( __('Проверка наличие товара у поставщика. ID продукта %s. Ошибка: %s'), $product_id, $error );
	}
	
	private function set_log_file(  )
	{				
		usam_log_file( $this->errors );
		$this->errors = array();
	}

	// Функция проверки остатков. Получает массив данных
	private function start_check_products_availability( $products )
	{		
		if ( empty($products) )
			return false;
		
		$start_time = microtime(true);		
		
		$product_ids_error_url = array();
		$sku_message = '';	
		
		ini_set("max_execution_time", "10660");		
		$i = 0;	
		$cache_currency_code = array();
		require_once( USAM_FILE_PATH . '/includes/simple_html_dom.php' ); 
		
		foreach ($products as $product_id) 
		{  	
			$p_meta = maybe_unserialize( usam_get_product_meta( $product_id, 'product_metadata' ) );	
			if (!empty($p_meta['webspy_link']))
			{						
				$site_setting = $this->get_option_url( $p_meta['webspy_link'] );									
				if ( $site_setting == '' )	
				{						
					$this->set_error( $product_id, __("Не найдены настройки.",'usam') );
					continue;				
				}						
				$storage = usam_get_storage( $site_setting['store'] );
				if ( empty($storage['meta_key']) )
					continue;		
				
				$prod_storage_key = USAM_META_PREFIX.$storage['meta_key'];		
			//	$html = file_get_html( $p_meta['webspy_link'] );	
				$URL_OBJ = usam_get_url_object( $p_meta['webspy_link'] );	
				if( $URL_OBJ )
				{					
					$contents = $URL_OBJ['content'];
				//	$HEADER = $URL_OBJ['header'];
				//	$TITLE = $URL_OBJ['title'];
				//	$DESCRIPTION = $URL_OBJ['description'];
				//	$KEYWORDS = $URL_OBJ['keywords'];
				//	$TIME_REQUEST = $URL_OBJ['time'];
				//	$URL_RESULT['url'];				

					$html = str_get_html($contents);	
					if ( !empty($site_setting['setting']['not_available']) )
					{						
						$div = $html->find($site_setting['setting']['not_available'], 0);	
						if ( empty($div) )	
							$type = 0;
						else 
							$type = 1;
					}
					else 
						$type = 1;						
							
					$currency_code = $site_setting['currency_code'];										
					$div = $html->find($site_setting['setting']['price'], 0);					
					$price = $this->number($div->innertext );					
			
					$product_price = usam_get_product_price( $product_id, $site_setting['type_price'] );		
					if (empty($price))
					{					
						$this->set_error( $product_id, __("Закупочная цена не найдена.",'usam') );
						$type = 0;
					}
					elseif ( $product_price != $price )
					{								
						$prices['price_'.$site_setting['type_price']] = $price;				
						usam_edit_product_prices( $product_id, $prices );						
					}						
					if ( !empty($site_setting['type_price']) )
					{
						$currency = usam_get_currency_price_by_code( $site_setting['type_price'] );
						if ( $currency != $currency_code )		
							$type = 0;
					}					
					switch ( $type ) 
					{
						case 0: //Товар купить нельзя	
							$update = $this->set_stock_product( $product_id, false, $prod_storage_key);
						break;
						case 1:	//Товар купить можно							
							$update = $this->set_stock_product( $product_id, true, $prod_storage_key);
						break;					
					}	
				}
				else
				{							
					$update = $this->set_stock_product( $product_id, false, $prod_storage_key);			
					$this->set_error( $product_id, __("Ссылка больше недоступна. Ошибка 404.",'usam') );		
					if ( $update )
					{									
						$this->set_error( $product_id, __("Товар снят с продажи.",'usam') );	
					}			
					$product_ids_error_url[] = $product_id;									
				}
				usam_update_product_meta( $product_id, 'date_externalproduct', date("Y-m-d H:i:s") );	
				$i++;		
			}	
			else							
				$this->set_error( $product_id, __("Не найдена ссылка.",'usam') );	
		}		
		$end_time = microtime(true);
		$result_time = $end_time - $start_time;			
		$this->message[] = "Проверка наличие товара у поставщика закончена. Проверенно $i товаров. Время работы: ".$result_time;		
		if ( !empty($product_ids_error_url) )
		{
			do_action( 'usam_no_link_to_product', $product_ids_error_url );
		}
	}
	
	// Получить текст между тегами
	function get_text( $contents, $format )
	{
		if ( $format['name'] != '' )
		{
			$names = explode(",", $format['name']);	
			foreach ( $names as $name ) 
			{	
				$str = '#<'.$format['tag'].'[^>]+'.$format['type'].'="'.$name.'[^>]+>(.+?)</'.$format['tag'].'>#is';
				preg_match($str, $contents, $arr);			
				if ( !empty($arr[1]) )
				{
					$out = trim ( $arr[1] );					
					return $out;	
				}
			}
		}
		return false;
	}
	
	function number( $number )
	{
		$number = preg_replace("|[^0-9.,]|i", "", $number );
		return usam_string_to_float ( $number );
	}
	
	function get_option_url( $url )
	{
		$host = parse_url($url, PHP_URL_HOST);	
	//	$host = str_replace("www.","",$host);					
		$option = get_option('usam_settings_sites_suppliers');
		$sites = maybe_unserialize( $option );	
		
		$site_setting = '';
		foreach ( $sites as $site ) 
		{		
			if ( $host == $site['domain'] )
			{
				$site_setting = $site;				
				break;
			}
		}
		return $site_setting;
	}
	
	// Загрузка характеристик, артикула и цены товара с dx.com
	function loading_information( $url )
	{	
		if (!empty($url))
		{
			$site_setting = $this->get_option_url( $url );								
			if ( $site_setting == '' )		
			{
				$response['error'] = __("Настройки сайта не найдены", "usam");			
				$response['status'] = 1;
			}
			else
			{    
				require_once( USAM_FILE_PATH . '/includes/simple_html_dom.php' );
				$URL_OBJ = usam_get_url_object( $url ); 
				if( $URL_OBJ )
				{						
					$contents = $URL_OBJ['content'];			
					//$contents = mb_detect_encoding($contents, 'UTF-8', true);
				//	$contents = iconv('UTF-8', 'chcp1251', $contents);		
					
					$html = str_get_html($contents);					
					foreach ($site_setting['setting'] as $key => $value) 
					{ 						
						$div = $html->find($value, 0);					
						$response[$key] = $div->innertext;	
					}	
					$filename = USAM_FILE_PATH . '/includes/resale/'.$site_setting['domain'].'.php';		
					if (file_exists($filename))
					{
						require_once( $filename );
						$class_site = new USAM_Processing_Site_Content();	
						$response['content'] = $class_site->content( $response['content'] );
					}				
					$response['price'] = $this->number( $response['price'] );		
					$response['status'] = 0;	
				}
				else
				{
					$response['error'] = __("Страница не доступна", "usam");
					$response['status'] = 1;
				}
			}
		}	
		else
		{
			$response['error'] = __("А где ссылка?", "usam");
			$response['status'] = 1;
		}
		return $response;
	}	
	
	
		public function processing_blocks( $buf, $search )
	{	
		$header_box = '<'.$search['type'];
		$footer_box = '</'.$search['type'].'>';	
		if (is_array($search['name']))
		{
			$result = '';
			foreach ($search['name'] as $key) 
			{	
				$header_box_id =  $search['blok'].'="'.$key.'"';	
				$number_sumbul = strripos( $buf, $header_box_id );
				if ( $number_sumbul !== false )	
				{
					$str = substr($buf, $number_sumbul);
					$pos = strpos($str, '>');					
					$str = substr($str, $pos+1);		

					$blocks = $this->processing_footer_blocks( $str, $footer_box, $header_box, 1);
					$out = substr($str, 0, $blocks[1]);
				}				
				$result .= $out;
			}		
		}
		else
		{			
			$header_box_id = $search['blok'].'="'.$search['name'].'"';
			
			$number_sumbul = strripos( $buf, $header_box_id );
			$str = substr($buf, $number_sumbul);
			$pos = strpos($str, '>');					
			$str = substr($str, $pos+1);		

			$blocks = $this->processing_footer_blocks( $str, $footer_box, $header_box, 1);
			$result = substr($str, 0, $blocks[1]);
		}
		$result = trim ( $result );	
		return $result;
	}	
	
	private function processing_footer_blocks( $buf, $footer_box, $header_box, $number_blocks )
	{	
		$pos_end = 0;
		$size_header_box = strlen( $header_box );
		$size_footer_box = strlen( $footer_box );
		do
		{
			$footer_pos = strpos($buf, $footer_box );
			$header_pos = strpos($buf, $header_box );
			if ( $header_pos !== false && $footer_pos !== false )
			{
				if ($header_pos < $footer_pos )
				{
					$pos = $header_pos + $size_header_box;
					$number_blocks++;
				}
				else 
				{
					$pos = $footer_pos + $size_footer_box;
					$number_blocks--;
				}
			}
			else
			{
				if($footer_pos !== false)
				{
					$pos = $footer_pos + $size_footer_box;				
					$number_blocks--;				
				}	
				if($header_pos !== false)
				{
					$pos = $header_pos + $size_header_box;				
					$number_blocks++;
				}			
				if ($footer_pos === false && $header_pos === false )
				{				
					if ($number_blocks != 0 )
						$pos_end = 0;
					break;
				}
			}
			$pos_end += $pos;		
			$buf = substr($buf, $pos, strlen($buf) );
		}
		while ($number_blocks != 0);
		
		if ($number_blocks == 0 )
			$pos_end -= $size_footer_box;	
		
		return array($number_blocks, $pos_end);
	}
}

function usam_get_url_object($url, $user_agent = null)
{
	$ABI_URL_STATUS_OK = 200;
	$ABI_URL_STATUS_REDIRECT_301 = 301;
	$ABI_URL_STATUS_REDIRECT_302 = 302;
	$ABI_URL_STATUS_NOT_FOUND = 404;
	$MAX_REDIRECTS_NUM = 5;	
	$TIME_START = explode(' ', microtime());
	$TRY_ID = 0;
	$URL_RESULT = false;	
	do
	{			
		$URL_PARTS = @parse_url($url);
		if( !is_array($URL_PARTS))
		{
		  break;
		};
		$URL_SCHEME = ( isset($URL_PARTS['scheme']))?$URL_PARTS['scheme']:'http';
		$URL_HOST = ( isset($URL_PARTS['host']))?$URL_PARTS['host']:'';
		$URL_PATH = ( isset($URL_PARTS['path']))?$URL_PARTS['path']:'/';
		$URL_PORT = ( isset($URL_PARTS['port']))?intval($URL_PARTS['port']):80;
		if( isset($URL_PARTS['query']) && $URL_PARTS['query']!='' )
		{
			$URL_PATH .= '?'.$URL_PARTS['query'];
		};
		$URL_PORT_REQUEST = ( $URL_PORT == 80 )?'':":$URL_PORT";
		//--- build GET request ---
		$USER_AGENT = ( $user_agent == null )?'Mozilla/5.0 (Windows NT 6.1; rv:16.0) Gecko/20100101 Firefox/16.0':strval($user_agent);
		$GET_REQUEST = "GET $URL_PATH HTTP/1.0\r\n"
		."Host: $URL_HOST$URL_PORT_REQUEST\r\n"
		."Accept: text/plain\r\n"
		."Accept-Encoding: identity\r\n"
		."User-Agent: $USER_AGENT\r\n\r\n";
		//--- open socket ---
		$SOCKET_TIME_OUT = 30;
		$SOCKET = @fsockopen($URL_HOST, $URL_PORT, $ERROR_NO, $ERROR_STR, $SOCKET_TIME_OUT);		
		if( $SOCKET )
		{
			if( fputs($SOCKET, $GET_REQUEST))
			{ 
				socket_set_timeout($SOCKET, $SOCKET_TIME_OUT);
				//--- read header ---
				$header = '';
				$SOCKET_STATUS = socket_get_status($SOCKET);
				while( !feof($SOCKET) && !$SOCKET_STATUS['timed_out'] )
				{
					$temp = fgets($SOCKET, 128);
					if( trim($temp) == '' ) break;
					$header .= $temp;
					$SOCKET_STATUS = socket_get_status($SOCKET);					
				};				
				if( preg_match('~HTTP\/(\d+\.\d+)\s+(\d+)\s+(.*)\s*\\r\\n~si', $header, $res))
				   $SERVER_CODE = $res[2];
				else
				   break;				
				if( $SERVER_CODE == $ABI_URL_STATUS_OK )
				{						
					$content = '';
					$SOCKET_STATUS = socket_get_status($SOCKET);
					while( !feof($SOCKET) && !$SOCKET_STATUS['timed_out'] )
					{
					  $content .= fgets($SOCKET, 1024*8);
					  $SOCKET_STATUS = socket_get_status($SOCKET);
					};
					//--- time results ---
					$TIME_END = explode(' ', microtime());
					$TIME_TOTAL = ($TIME_END[0]+$TIME_END[1])-($TIME_START[0]+$TIME_START[1]);
					//--- output ---
					$URL_RESULT['header'] = $header;
					$URL_RESULT['content'] = $content;
					$URL_RESULT['time'] = $TIME_TOTAL;
					$URL_RESULT['description'] = '';
					$URL_RESULT['keywords'] = '';
					$URL_RESULT['url'] = $url;					
					$URL_RESULT['title'] =( preg_match('~<title>(.*)<\/title>~U', $content, $res))?strval($res[1]):'';
				
					if( preg_match_all('~<meta\s+name\s*=\s*["\']?([^"\']+)["\']?\s+content\s*=["\']?([^"\']+)["\']?[^>]+>~', $content, $res, PREG_SET_ORDER) > 0 )
					{
						foreach($res as $meta)
							$URL_RESULT[strtolower($meta[1])] = $meta[2];
					}
				}
				elseif( $SERVER_CODE == $ABI_URL_STATUS_REDIRECT_301 || $SERVER_CODE == $ABI_URL_STATUS_REDIRECT_302 )
				{
					if( preg_match('~location\:\s*(.*?)\\r\\n~si', $header, $res))
					{
						$REDIRECT_URL = rtrim($res[1]);
						$URL_PARTS = @parse_url($REDIRECT_URL);
						if( isset($URL_PARTS['scheme'])&& isset($URL_PARTS['host']))
							$url = $REDIRECT_URL;
						else
							$url = $URL_SCHEME.'://'.$URL_HOST.'/'.ltrim($REDIRECT_URL, '/');
					}
					else
					{
					 break;
					};
				};
			};
			fclose($SOCKET);
		}
		else
		{
			break;
		};
		$TRY_ID++;
	}
	while( $TRY_ID <= $MAX_REDIRECTS_NUM && $URL_RESULT === false );
	return $URL_RESULT;
};
?>