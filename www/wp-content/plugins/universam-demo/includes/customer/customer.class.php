<?php
// Получить данные покупателей

class USAM_Customer
{		
	public $result_report;
	public $columns;
	
	function get_data( $get_columns = '' )
	{		
		global $wpdb;
		
		$columns_all = array(  
			'billingfirstname' => __( 'Имя', 'usam' ),
			'billinglastname'  => __( 'Фамилия', 'usam' ),	
			'billingemail'     => __( 'Электронная почта', 'usam' ),						
			'last_order'       => __( 'Последний заказ', 'usam' ),			
			'order_total'     => __( 'Последняя сумма', 'usam' ),	
			'count_orders'     => __( 'Количество заказов', 'usam' ),	
			'total_orders'     => __( 'Всего куплено', 'usam' ),				
		);	
		
		if ( isset($_GET['groupby_customer']) )		
			switch ( $_GET['groupby_customer'] ) 
			{
				case 'family':	
					$groupby_customer = "billinglastname";	
				break;
				case 'mail':	
				default:
					$groupby_customer = "billingemail";
				break;				
			}			
		else
			$groupby_customer = "billingemail";			
		
		if ( isset($_GET['by_coupon']) )		
			switch ( $_GET['by_coupon'] ) 
			{
				case 'all':	
					$couponby = "";
				break;
				case 'yes':	
					$couponby = "wtpl.coupon_name <> '' AND ";
				break;
				case 'no':
					$couponby = "wtpl.coupon_name = '' AND ";
				break;
				default:
					$couponby = " wtpl.coupon_name = '".$_GET['by_coupon']."' AND ";
				break;				
			}			
		else
			$couponby = '';
	
		$query_max_users_ids = "SELECT GROUP_CONCAT(wtpl.id ORDER BY wtpl.date_insert DESC SEPARATOR ',' ) AS last_order_id,
								GROUP_CONCAT(wtpl.totalprice ORDER BY wtpl.date_insert DESC SEPARATOR ',' ) AS order_total,
								MAX(wtpl.date_insert ) AS last_order,
								COUNT(wtpl.id) AS count_orders,
								SUM(wtpl.totalprice) AS total_orders				
								FROM " . USAM_TABLE_ORDERS . " AS wtpl
										 LEFT JOIN " . USAM_TABLE_ORDER_PROPS_VALUE . " AS customer_email ON ( customer_email.order_id = wtpl.id AND customer_email.unique_name = '$groupby_customer' )
								WHERE $couponby wtpl.user_ID > 0 GROUP BY customer_email.value";
		$result_max_users_ids = $wpdb->get_results($query_max_users_ids, 'ARRAY_A' );
		
		$query_max_guest_ids = "SELECT GROUP_CONCAT(wtpl.id ORDER BY wtpl.date_insert DESC SEPARATOR ',' ) AS last_order_id,
								GROUP_CONCAT(wtpl.totalprice ORDER BY wtpl.date_insert DESC SEPARATOR ',' ) AS order_total,
								MAX(wtpl.date_insert) AS last_order,
								COUNT(wtpl.id) AS count_orders,
								SUM(wtpl.totalprice) AS total_orders								
								FROM " . USAM_TABLE_ORDERS . " AS wtpl
										 LEFT JOIN " . USAM_TABLE_ORDER_PROPS_VALUE . " AS customer_email ON ( customer_email.order_id = wtpl.id AND customer_email.unique_name = '$groupby_customer' )
								WHERE $couponby wtpl.user_ID = 0
								GROUP BY customer_email.value
								ORDER BY last_order DESC";
		$result_max_guest_ids = $wpdb->get_results($query_max_guest_ids, 'ARRAY_A' );	
		if ( empty($result_max_guest_ids) )
			return array();
	
		$result_report['total_orders'] = 0;
		$result_report['count_orders'] = 0;
		$max_id = array();
		for ($i=0;$i<sizeof($result_max_guest_ids);$i++)
		{
			$temp_id =  explode(",",$result_max_guest_ids[$i]['last_order_id']);
			$max_id[$i] = $temp_id[0];
			
			$count_orders[$max_id[$i]] = $result_max_guest_ids[$i]['count_orders'];
			$total_orders[$max_id[$i]] = $result_max_guest_ids[$i]['total_orders'];
			$order_date[$max_id[$i]] = $result_max_guest_ids[$i]['last_order'];		
			$temp_tot =  explode(",",$result_max_guest_ids[$i]['order_total']);
			$last_order_total[$max_id[$i]] = $temp_tot[0];	
			
			$result_report['total_orders'] = $result_report['total_orders'] + $result_max_guest_ids[$i]['total_orders'];
			$result_report['count_orders'] = $result_report['count_orders'] + $result_max_guest_ids[$i]['count_orders'];			
		}		
		$result_report['total_orders'] = usam_currency_display( $result_report['total_orders']);
		$j=sizeof($max_id);
		
		for ($i=0;$i<sizeof($result_max_users_ids);$i++,$j++)
		{
			$temp_id =  explode(",",$result_max_users_ids[$i]['last_order_id']);
			$max_id[$j] = $temp_id[0];
			
			$count_orders[$max_id[$j]] = $result_max_users_ids[$i]['count_orders'];
			$total_orders[$max_id[$j]] = $result_max_users_ids[$i]['total_orders'];
			$order_date[$max_id[$j]] = $result_max_users_ids[$i]['last_order'];
			
			$temp_tot =  explode(",",$result_max_users_ids[$i]['order_total']);
			$last_order_total[$max_id[$j]] = $temp_tot[0];
			
		}			
		$customer_details_query_select = "SELECT wtsfd.order_id AS order_id,
										GROUP_CONCAT( wtcf.unique_name ORDER BY wtcf.id SEPARATOR '###' ) AS meta_keys,
										GROUP_CONCAT( wtsfd.value ORDER BY wtsfd.unique_name SEPARATOR '###' ) AS meta_values 
										FROM " . USAM_TABLE_ORDER_PROPS_VALUE . " AS wtsfd
									    JOIN " . USAM_TABLE_ORDER_PROPS . " AS wtcf ON ( wtcf.unique_name = wtsfd.unique_name AND wtcf.active = 1 AND wtcf.unique_name IN ('billingfirstname','billinglastname','billingaddress', 'billingcity','billingregion','billingcountry','billingpostcode', 'billingemail','billingphone') )
										WHERE order_id IN (". implode(",",$max_id).") GROUP BY order_id";		              
		$order_by = " ORDER BY FIND_IN_SET(order_id,'". implode(",",$max_id)."')";
			
		$full_customer_details_query = $customer_details_query_select . $order_by;
		$customer_details_results = $wpdb->get_results( $full_customer_details_query, 'ARRAY_A' );	

		$num_records = count( $customer_details_results );			
		//Код для получения всех пользователей вместе с их ID и электронной почтой в массиве
		$query = "SELECT users.ID, users.user_email, GROUP_CONCAT(usermeta.meta_value ORDER BY usermeta.umeta_id SEPARATOR '###' ) AS name
					FROM $wpdb->users AS users
					JOIN $wpdb->usermeta  AS usermeta ON usermeta.user_id = users.id
					WHERE usermeta.meta_key IN ('first_name','last_name','_usam_customer_profile') GROUP BY users.id DESC";
		$reg_user = $wpdb->get_results ($query ,'ARRAY_A');	
		for ($i=0;$i<sizeof($reg_user);$i++)
		{			
			$user_email[$reg_user[$i]['ID']] = $reg_user[$i]['user_email'];
			$name = explode ("###",$reg_user[$i]['name']);			
			$user_fname[$reg_user[$i]['ID']] = $name[0];
			$user_lname[$reg_user[$i]['ID']] = isset($name[1])?$name[1]:'';
			if (!empty($name[2])) 
			{				
				$unserialized_detail = maybe_unserialize($name[2]); 			
				if (isset($unserialized_detail[4]))
					$user_add[$reg_user[$i]['ID']]      = $unserialized_detail[4];
				if (isset($unserialized_detail[5]))
					$user_city[$reg_user[$i]['ID']]     = $unserialized_detail[5];
				if (isset($unserialized_detail[6]))
					$user_region[$reg_user[$i]['ID']]   = $unserialized_detail[6];
				if (isset($unserialized_detail[7][0]))
					$user_country[$reg_user[$i]['ID']]  = $unserialized_detail[7][0];
				if (isset($unserialized_detail[8]))
					$user_pcode[$reg_user[$i]['ID']]    = $unserialized_detail[8];
				if (isset($unserialized_detail[18]))
					$user_phone[$reg_user[$i]['ID']]    = $unserialized_detail[18];
			}			
		}		
		$country_result = $wpdb->get_results( "SELECT code,name FROM " . USAM_TABLE_COUNTRY ,'ARRAY_A');
		$country_rows = $wpdb->num_rows;		
		if ($country_rows > 0) 
		{
			for ($i=0;$i<sizeof($country_result);$i++) {
				$country[$country_result[$i]['code']] = $country_result[$i]['name'];
			}
		}		
		if ($num_records == 0) 	
			$records = array();	
		else 
		{				
			error_reporting(0);
			foreach ( $customer_details_results as $result ) 
			{
				$meta_keys = explode( '###', $result['meta_keys'] );
				$meta_values = explode( '###', $result['meta_values'] );
				unset( $result['meta_keys'] );
				unset( $result['meta_values'] );
				if ( count( $meta_keys ) == count( $meta_values ) ) 
				{
					$customer_detail_data[$result['order_id']] = array_combine( $meta_keys, $meta_values );
				}

				$result['last_order_id'] =  $result['order_id'];
				$result['last_order'] = $order_date[$result['order_id']];
				$result['order_total'] = $last_order_total[$result['order_id']];
				$result['count_orders']= $count_orders[$result['order_id']];
				$result['total_orders']= $total_orders[$result['order_id']];
	
				if ( empty( $customer_detail_data[$result['last_order_id']] ) ) {
					$num_records--;
					continue;
				}
				$billing_user_details = $customer_detail_data[$result['last_order_id']];
				$billing_user_details['billingregion'] = ( !empty( $regions_ids[$billing_user_details['billingregion']] ) ) ? $regions_ids[$billing_user_details['billingregion']] : $billing_user_details['billingregion'];
				$billing_user_details['billingcountry'] = ( !empty( $country_data[$billing_user_details['billingcountry']] ) ) ? $country_data[$billing_user_details['billingcountry']] : $billing_user_details['billingcountry'];			
				//Code to get the email for reg users from wp_users table
				if ($result['id'] > 0) 
				{
					$result['email']  = $user_email[$result['id']];
					$billing_user_details ['billingemail']      = $user_email[$result['id']];
					
					if(!(empty($user_fname[$result['id']]))) {
						$billing_user_details ['billingfirstname']  = $user_fname[$result['id']];
					}
					if(!(empty($user_lname[$result['id']]))) {
						$billing_user_details ['billinglastname']   = $user_lname[$result['id']];
					}
					
					$billing_user_details ['billingaddress']    = $user_add[$result['id']];
					$billing_user_details ['billingcity']       = $user_city[$result['id']];
					$billing_user_details ['billingregion']      = ( !empty( $regions_ids[$user_region[$result['id']]] ) ) ? $regions_ids[$user_region[$result['id']]] : $user_region[$result['id']];
					$billing_user_details ['billingcountry']    = $country[$user_country[$result['id']]];
					$billing_user_details ['billingpostcode']   = $user_pcode[$result['id']];
					$billing_user_details ['billingphone']      = $user_phone[$result['id']];
				}					
				$result ['Old_Email_Id'] = $billing_user_details ['billingemail'];
				$records[] = ( !empty( $billing_user_details ) ) ? array_merge ( $billing_user_details, $result ) : $result;			

			   unset($result);
			   unset($meta_keys);
			   unset($meta_values);
			   unset($billing_user_details);
			}
			$this->totalCount = $num_records;
			$this->result_report = $result_report;
		}		
		if ( !empty($get_columns) )
		{			
			// Выбрать столбцы
			foreach ( $get_columns as $key )
			{			
				if ( isset($columns_all[$key]))
				{
					$this->columns[$key] = $columns_all[$key];			
				}
			}
			$out = array();
			$i = 0;			
			// Выбрать данные
			foreach ( $records as $record )
			{								
				foreach ( $get_columns as $key )
				{
					if ( isset($record[$key]))
					{
						$out[$i][$key] = $record[$key];			
					}
				}
				$i++;
			}			
			return $out;
		}
		else
		{
			$this->columns = $columns_all;
			return $records;	
		}
	}
}
?>