<?php
function usam_get_customer_profile( $unique_name, $customer_id = null )
{
	$profile = usam_get_customer_meta( 'checkout_details' );
	$result = isset($profile[$unique_name])?$profile[$unique_name]:'';
	return $result;
}

function usam_update_customer_profile( $unique_name, $value,  $customer_id = null )
{
	$profile = usam_get_customer_meta( 'checkout_details' );
	$profile = isset($profile)?$profile:array();
	$profile[$unique_name] = $value;
	$result = usam_update_customer_meta( 'checkout_details', $profile );		
	return $result;
}

function usam_customer_location_name( $customer_id = null )
{
	$location = usam_get_location( usam_get_customer_location( $customer_id ) );
	echo isset($location['name'])?$location['name']:'';
}

function usam_get_user_location( $customer_id )
{	
	$current_location = 0;
	if ( $customer_id )
	{
		$current_location = usam_get_customer_meta( 'location', $customer_id ); 	
		if ( !empty($current_location) )		
			$location = usam_get_location( $current_location );		
		
		if ( empty($location) )
			$current_location = 0;
	}
	return $current_location;
}


function usam_get_customer_location( $customer_id = null )
{	
	global $user_ID;
	
	if ( $customer_id === null )
		$customer_id = $user_ID;	

	$current_location = usam_get_customer_meta( 'location', $customer_id ); 
	if ( !empty($current_location) )		
		$location = usam_get_location( $current_location );		
		
	if ( empty($location) )
	{
		$current_location = 0;
		if ( usam_is_bot() )
		{
			$bot = usam_get_bot();
			$option = get_option('usam_search_engine_location');
			$search_engine_location = maybe_unserialize( $option );	
			foreach( $search_engine_location as $value )	
			{
				if ( $value['search_engine'] == $bot )
				{					
					usam_update_customer_location( $value['location'], $customer_id );	
					return $value['location'];
				}
			}
		}	
		elseif ( get_option('usam_get_customer_location', 1) )
		{			
			$current_location = usam_get_current_user_location( );		
			if ( !empty($current_location) )
			{
				usam_update_customer_location( $current_location, $customer_id );				
			}			
		}
	}		
	if ( empty($current_location) )
	{		
		$current_location = get_option( 'usam_shop_location' );	
		usam_update_customer_location( $current_location, $customer_id );
	}
	return $current_location;
}

function usam_get_customer_sales_area( $customer_id = null )
{
	$customer_location_ids = usam_get_customer_location_array_up( $customer_id );

	$option = get_option('usam_sales_area');
	$grouping = maybe_unserialize( $option );
	$id = false;
	foreach( $grouping as $group )
	{					
		$result = array_intersect($group['locations'], $customer_location_ids );			
		if ( !empty($result) )
		{
			$id = $group['id'];
			break;
		}
	}
	return $id;
}

function usam_in_customer_sales_area( $area_id, $customer_id = null )
{
	global $user_ID;
	
	if ( empty($area_id) )
		return true;	
	
	$customer_sales_area = usam_get_customer_sales_area( $customer_id );
	if ( $area_id == $customer_sales_area )
		return true;
	else
		return false;	
}


// Обновить местоположение пользователя
function usam_update_customer_location( $location_id, $customer_id = null )
{		
	global $user_ID;
	
	if ( $customer_id === null )
		$customer_id = $user_ID;
	
	$location_id = (int) $location_id;
	
	usam_update_customer_meta( 'location', $location_id );		
	$current_location_ids = usam_get_array_locations_up( $location_id, 'id' ); 
	
	usam_update_customer_meta( 'location_array_up', $current_location_ids );		
}

// Получить все вложения местоположения пользователя
function usam_get_customer_location_array_up( $customer_id = null )
{		
	global $user_ID;
	
	if ( $customer_id === null )
		$customer_id = $user_ID;
	
	$current_location_ids = usam_get_customer_meta( 'location_array_up', $customer_id );		
	if ( empty($current_location_ids) )
	{
		$current_location = usam_get_customer_location( $customer_id );				
		$current_location_ids = usam_get_array_locations_up( $current_location, 'id' ); 

		usam_update_customer_meta( 'location_array_up', $current_location_ids );	
	}			
	return $current_location_ids;	
}

function usam_get_customer_city( $customer_id = null )
{	
	$location_ids = usam_get_customer_location_array_up( $customer_id );	
	$type_location = usam_get_types_location();
	foreach( $location_ids as $location_id )
	{
		$location = usam_get_location( $location_id );
		if ( $type_location[$location['id_type']]->code == 'city' )
			return $location;
	}
	return false;
}

function usam_get_customer_price_code( $customer_id = null )
{
	global $user_ID;
	
	if ( $customer_id === null )
		$customer_id = $user_ID;
	
	if ( $customer_id ) 		
	{
		$user = get_userdata( $customer_id );	
		$current_location_ids = usam_get_customer_location_array_up( $customer_id );
	}
	else
	{		
		$current_location = usam_get_customer_location();		
		$current_location_ids = usam_get_array_locations_up( $current_location, 'id' );				
	}
	$type_prices = usam_get_prices( array('available' => 1, 'type' => 'R') );
	$code = ''; 		
	foreach ( $type_prices as $id => $value )
	{			
		if ( !empty($value['roles']) )
		{			
			if ( empty($user))
				$roles = array('notLoggedIn');
			else
				$roles = $user->roles;
			
			$result = array_intersect($roles, $value['roles'] );
			if ( empty($result) )
				continue;			
		}		
		if ( empty($value['locations']) )
		{
			$code = $value['code'];
			break;	
		}			
		$result = array_intersect( $value['locations'], $current_location_ids );			
		if ( !empty($result) )
		{
			$code = $value['code'];
			break;
		}				
	}			
	return $code;
}

/**
 * В случае, если пользователь не вошел в систему, создать клиенту куки с уникальным ID в паре с переходным в базе данных.
 * @since 3.8.9
 */
function usam_create_customer_id()
{
	$expire = time() + USAM_CUSTOMER_DATA_EXPIRATION; // valid for 48 hours
	$secure = is_ssl();
	$id = '_' . wp_generate_password(); // make sure the ID is a string
	$data = $id . $expire;
	$hash = hash_hmac( 'md5', $data, wp_hash( $data ) );
	// store ID, expire and hash to validate later
	$cookie = $id . '|' . $expire . '|' . $hash;

	setcookie( USAM_CUSTOMER_COOKIE, $cookie, $expire, USAM_CUSTOMER_COOKIE_PATH, COOKIE_DOMAIN, $secure, true );
	$_COOKIE[USAM_CUSTOMER_COOKIE] = $cookie;
	return $id;
}

/**
 * Убедитесь, что куки клиента не будет нарушены.
 * @since 3.8.9
 * @return Возвращает смешанный идентификатор клиента, если куки, ложно, если в противном случае.
 */
function usam_validate_customer_cookie() 
{
	$cookie = $_COOKIE[USAM_CUSTOMER_COOKIE];
	list( $id, $expire, $hash ) = explode( '|', $cookie );
	$data = $id . $expire;
	$hmac = hash_hmac( 'md5', $data, wp_hash( $data ) );
	if ( $hmac != $hash )
		return false;
	return $id;
}

/**
 * Слияние анонимных данных клиента (хранится в переходных) с данными счета мета, когда клиент входит в систему
 * @since 3.8.9
 */
function _usam_merge_customer_data()
{
	$account_id = get_current_user_id();
	$cookie_id = usam_validate_customer_cookie();	
	
	if ( ! $cookie_id )
		return;

	$cookie_data = get_transient( "usam_customer_meta_{$cookie_id}" );
	if ( ! is_array( $cookie_data ) || empty( $cookie_data ) )
		return;

	if ( !empty($cookie_data['basket_id']) )
	{
		global $user_ID, $wpdb;
		$basket_id = usam_get_customer_meta( 'basket_id', $user_ID );
		if ( $basket_id )
		{ // Если у зарегистрированного пользователя есть корзина			
			$result = $wpdb->update( USAM_TABLE_PRODUCTS_BASKET, array( 'cart_id' => $basket_id ), array( 'cart_id' => $cookie_data['basket_id'] ) );
		}
		else
			$result = $wpdb->update( USAM_TABLE_USERS_BASKET, array( 'user_id' => $user_ID ), array( 'id' => $cookie_data['basket_id'] ) );
	}
	foreach ( $cookie_data as $key => $value )
		usam_add_customer_meta( $key, $value, $account_id );	

	delete_transient( "usam_customer_meta_{$cookie_id}" );
	setcookie( USAM_CUSTOMER_COOKIE, '', time() - 3600, USAM_CUSTOMER_COOKIE_PATH, COOKIE_DOMAIN, is_ssl(), true );
	unset( $_COOKIE[USAM_CUSTOMER_COOKIE] );
}

/**
 * Получить текущий ID клиента.
 * Если пользователь вошел в систему, возвращает идентификатор пользователя. В противном случае возвращает идентификатор, связанный с куки клиента.
 * Если $mode установлен в «создать», создаст идентификатор клиента, если он еще не был создан.
 * @since 3.8.9
 */
function usam_get_current_customer_id( $mode = '' )
{ 
	if ( is_user_logged_in() && isset( $_COOKIE[USAM_CUSTOMER_COOKIE] ) )
		_usam_merge_customer_data();
	
	if ( is_user_logged_in() )
		return get_current_user_id();
	elseif ( isset( $_COOKIE[USAM_CUSTOMER_COOKIE] ) )
		return usam_validate_customer_cookie();
	elseif ( $mode == 'create' )
		return usam_create_customer_id();

	return false;
}

/* Возвращает массив, содержащий все метаданные клиента
 * @since 3.8.9
 */
function usam_get_all_customer_meta( $id = false )
{
	global $wpdb;

	if ( ! $id )
		$id = usam_get_current_customer_id(); 
	if ( ! $id )
		return new WP_Error( 'usam_customer_meta_invalid_customer_id', __( 'Неверный ID клиента', 'usam' ), $id );

	$blog_prefix = is_multisite() ? $wpdb->get_blog_prefix() : '';
	if ( is_numeric( $id ) )
		$profile = get_user_meta( $id, "_usam_{$blog_prefix}customer_profile", true );
	else
		$profile = get_transient( "usam_customer_meta_{$blog_prefix}{$id}" );

	if ( ! is_array( $profile ) )
		$profile = array();

	return apply_filters( 'usam_get_all_customer_meta', $profile, $id );
}

/**
 * Получить значение клиентской меты.
 * @since  3.8.9
 */
function usam_get_customer_meta( $key = '', $id = false )
{	
	$profile = usam_get_all_customer_meta( $id );
	if ( is_wp_error( $profile ) && ! $id ) 
	{
		usam_create_customer_id();
		$profile = usam_get_all_customer_meta();
	}
	if ( is_wp_error( $profile ) || ! array_key_exists( $key, $profile ) )
		return null;

	return $profile[$key];
}

/**
 * Переписать мета клиентов с массивом meta_key => meta_value.
 * @since  3.8.9
 */
function usam_update_all_customer_meta( $profile, $id = false )
{
	global $wpdb;

	if ( ! $id )
		$id = usam_get_current_customer_id( 'create' );

	$blog_prefix = is_multisite() ? $wpdb->get_blog_prefix() : '';

	if ( is_numeric( $id ) )
		return update_user_meta( $id, "_usam_{$blog_prefix}customer_profile", $profile );
	else
		return set_transient( "usam_customer_meta_{$blog_prefix}{$id}", $profile, USAM_CUSTOMER_DATA_EXPIRATION );
}
/**
 * Обновить мета клиента
 * @since  3.8.9
 */
function usam_update_customer_meta( $key, $value, $id = false )
{
	if ( ! $id )
		$id = usam_get_current_customer_id( 'create' );
	$profile = usam_get_all_customer_meta( $id );

	if ( is_wp_error( $profile ) )
		return $profile;

	$profile[$key] = $value;
	return usam_update_all_customer_meta( $profile, $id );
}
/**
 * Добавить мета клиента
 * @since  3.8.9
 */
function usam_add_customer_meta( $key, $value, $id = false )
{
	if ( ! $id )
		$id = usam_get_current_customer_id( 'create' );
	$profile = usam_get_all_customer_meta( $id );

	if ( is_wp_error( $profile ) )
		return $profile;

	$return = false;
	if ( !isset($profile[$key]) )
	{
		$profile[$key] = $value;
		$return = usam_update_all_customer_meta( $profile, $id );
	}
	return $return;
}
/**
 * Удалить мета клиента
 * @since  3.8.9
 */
function usam_delete_customer_meta( $key, $id = false ) 
{
	$profile = usam_get_all_customer_meta( $id );
	if ( is_wp_error( $profile ) )
		return $profile;
	if ( array_key_exists( $key, $profile ) )
		unset( $profile[$key] );

	return usam_update_all_customer_meta( $profile, $id );
}

/* Удалить все мета клиентов для определенного определенного идентификатора клиента
 * @since  3.8.9.4
 */
function usam_delete_all_customer_meta( $id = false )
{
	global $wpdb;

	if ( ! $id )
		$id = usam_get_current_customer_id();
	$blog_prefix = is_multisite() ? $wpdb->get_blog_prefix() : '';
	if ( is_numeric( $id ) )
		return delete_user_meta( $id, "_usam_{$blog_prefix}customer_profile" );
	else
		return delete_transient( "usam_customer_meta_{$blog_prefix}{$id}" );
}

function usam_get_user_select_calendars( )
{
	global $user_ID;
	$user_calendars = get_the_author_meta('usam_calendars', $user_ID);
	if (empty($user_calendars))
		$user_calendars = array();
	return $user_calendars;
}

function usam_get_user_calendars( )
{
	global $user_ID;
	
	$option = get_option('usam_calendars');
	$calendars = maybe_unserialize( $option );

	$user_calendars = array();
	if ( !empty($calendars) )
		foreach( $calendars as $key => $item )
		{	
			if ( $item['user_id'] == $user_ID )
				$user_calendars[] = $item;
		}	

	if ( empty($user_calendars) )
	{
		$default_user_calendars = array( 'name' => __('Календарь по умолчанию', 'usam'), 'user_id' => $user_ID, 'sort' => 1, 'type' => 'user' );	
		$user_calendars[] = usam_insert_user_calendars( $default_user_calendars );			
	}
	return $user_calendars;
}

// Получить название календаря по id
function usam_get_calendar_name_by_id( $id  )
{
	$option = get_option('usam_calendars');
	$calendars = maybe_unserialize( $option );
	if ( !empty($calendars) )
		foreach( $calendars as $key => $calendar )
		{	
			if ( $calendar['id'] == $id )
				return $calendar['name'];
		}	
	return false;
}


// Получить номер системного календаря
function usam_get_id_system_calendar( $type = 'order' )
{	
	$option = get_option('usam_calendars');
	$calendars = maybe_unserialize( $option );
	
	if ( !empty($calendars) )
		foreach( $calendars as $key => $item )
		{	
			if ( $item['type'] == $type )
			{
				return $item['id'];
			}
		}	
	return false;
}

// Получить системные календари
function usam_get_system_calendars( )
{	
	$option = get_option('usam_calendars');
	$calendars = maybe_unserialize( $option );

	$system_calendars = array();
	if ( !empty($calendars) )
		foreach( $calendars as $key => $item )
		{	
			if ( $item['user_id'] == 0 )
				$system_calendars[] = $item;
		}	
	return $system_calendars;
}

// Добавить календарь
function usam_insert_user_calendars( $user_calendar )
{
	global $user_ID;
	
	$option = get_option('usam_calendars', array() );						
	$calendares = maybe_unserialize($option);		
								
	$user_calendar['user_id'] = $user_ID;
	if ( empty($calendares) )				
		$calendares[1] = $user_calendar;					
	else
		$calendares[] = $user_calendar;	
	
	end($calendares);
	$key = key($calendares);
	
	$calendares[$key]['id'] = $key;			
	update_option('usam_calendars', serialize($calendares) );	
	
	return $calendares[$key];
}

// Получить списки текущего пользователя
function usam_get_currentuser_list( )
{	
	$current_user = wp_get_current_user();
	$return = array();
	
	$communication = usam_get_communication_by_email( $current_user->user_email, 'contact' );		
	if ( empty($communication) )
		return $return;
	
	$lists = usam_get_subscriber_list( $communication->id );
	foreach ( $lists as $list )
	{
		$return[] = $list['list'];
	}
	return $return;
}

function usam_subscribe_for_newsletter( $subscriber )
{
	$communication = usam_get_communication_by_email( $subscriber['email'], 'contact' );				
	if ( empty($communication) )
	{						
		$contact_id = usam_insert_contact( $subscriber );			
		$communication = array( 'value' => $subscriber['email'], 'value_type' => 'private', 'type' => 'email', 'contact_id' => $contact_id, 'customer_type' => 'contact' );
		$communication_id = usam_insert_communication( $communication );						
	}
	else
	{
		$contact_id = $communication->contact_id;	
		$communication_id = $communication->id;						
	} 
	$lists = usam_get_subscribers_list( );
	$insert_lists = array();
	foreach ( $lists as $list )
	{
		usam_set_subscriber_lists( array( 'id_communication' => $communication_id, 'status' => 1, 'id' => $list['id'] ));			
	}
	do_action( 'usam_subscribe_for_newsletter', $communication_id );	
	return $communication_id;
}

// Найти ID электроной почты в CRM пользователя
function usam_get_user_crm_communication_id( $user_id = null )
{	
	if ( $user_id == null )
	{
		global $user_ID;
		$user_id = $user_ID;
	}
	$user_info = get_userdata( $user_id );
	
	if ( empty($user_info->user_email) )
		return false;
	
	$contact_data = usam_get_contact( $user_info->ID, 'user_id' );			
	if ( empty($contact_data ) )
	{
		$args = array( 'lastname' => $user_info->last_name, 'firstname' => $user_info->first_name, 'contact_source' => 'register', 'user_id' => $user_info->ID );		
		$contact_id = usam_insert_contact( $args );			
	}
	else
	{
		$contact_id = $contact_data['id'];
		$communication = usam_get_contact_means_communication( $contact_id );
		$communication_id = 0;
		foreach ( $communication['email'] as $id => $email )
		{
			if ( $email['value'] == $user_info->user_email )
			{
				$communication_id = $id;
				break;
			}
		}			
	}	
	if ( $communication_id == 0 )
	{			
		$data = array( 'value' => $user_info->user_email, 'value_type' => 'private', 'type' => 'email', 'contact_id' => $contact_id, 'customer_type' => 'contact' );
		$communication_id = usam_insert_communication( $data );		
	}		
	return $communication_id;
}


// Добавить в рассылку пользователя
function usam_set_user_lists( $lists, $user_id = null )
{					
	$communication_id = usam_get_user_crm_communication_id( $user_id );
	
	$insert_lists = array();
	foreach ( $lists as $list_id )
		usam_set_subscriber_lists( array( 'id_communication' => $communication_id, 'status' => 1, 'id' => $list_id ) );	
}


// Удалить подписку пользователя
function usam_delete_user_lists( $lists, $user_id = null )
{					
	$communication_id = usam_get_user_crm_communication_id( $user_id );
	$delete_lists = array();
	foreach ( $lists as $list_id )
		$delete_lists[$communication_id] = $list_id;		
	usam_delete_subscriber_lists( $delete_lists );	
}

// Удалить товар из списка клиента
function usam_delete_product_from_customer_list( $product_id, $list_name )
{		
	global $wpdb;
	$user_code = usam_get_current_customer_id();
	$result = $wpdb->query( $wpdb->prepare("DELETE FROM ".USAM_TABLE_USER_PRODUCTS." WHERE product_id='%d' AND user_list='%s' AND  user_code='%s'", $product_id, $list_name, $user_code ));
}

function usam_check_current_user_role( $role, $user_id = null ) 
{
	$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();

	if( ! $user )
		return false;

	return in_array( $role, (array) $user->roles );
}

function usam_get_customer_primary_mailbox_id( )
{		
	global $user_ID;	
	$id = get_the_author_meta('usam_email_default', $user_ID);
	if ( empty($id) )
		 $id = get_option("usam_return_email", false);
	 
	return $id;
}


// Проверяет есть ли товар в списке
function usam_checks_product_from_customer_list( $list_name, $product_id = null )
{
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$product_ids = usam_get_product_ids_in_user_list( $list_name );		
	if ( !empty($product_ids) && in_array($product_id, $product_ids) )
		return true;
	else
		return false;
}

function usam_get_product_ids_in_user_list( $user_list )
{
	$cache_key = "user_list";
	if( ! $product_ids = wp_cache_get($cache_key, $user_list ) )
	{		
		$args = array( 'user_list' => $user_list, 'fields' => 'product_id' );
		$product_ids = usam_get_user_products( $args );		
		wp_cache_set( $cache_key, $product_ids, $user_list );
	}	
	return $product_ids;
}
	
// Возвращает количество товара в сравнении
function usam_get_quantity_products_compare( )
{
	$products = usam_get_products_compare();	
	return count($products);
}	
// Возвращает товары в сравнении
function usam_get_products_compare( )
{	
	$product_ids = usam_get_product_ids_in_user_list( 'compare' );	
	return $product_ids;
}

// Возвращает количество товара в избранном
function usam_get_quantity_products_desired( )
{
	$desired_products = usam_get_products_desired();	
	return count($desired_products);
}	

// Возвращает товары в избранном
function usam_get_products_desired( )
{
	$product_ids = usam_get_product_ids_in_user_list( 'desired' );	
	return $product_ids;
}	

function usam_get_customer_name( $user_id )
{
	$user = get_user_by('id', $user_id );
	return isset($user->display_name)?"$user->display_name":__('Гость','usam'); //({$user->user_login})
}	

function usam_get_accumulative_discount_customer( $type = 'price', $customer_id = null ) 
{		                        
	global $user_ID, $type_price;
	
	if ( empty($customer_id) )
		$customer_id = $user_ID;
			 
	$option = get_option('usam_accumulative_discount', '');		
	$rules = maybe_unserialize( $option );				
	$applied_rules = array();
	$discount = 0;
	if ( !empty($rules) )
	{						
		foreach ( $rules as $rule )
		{										
			if ( $rule['active'] && $rule['method'] == $type && ( in_array($type_price, $rule['type_prices']) ) )
			{ 						
				$args = array('status' => 'closed', 'user_id' => $customer_id, 'type_price' => $rule['type_prices'] );						
				if ( $rule['period'] == 'd' )
				{ // За период								
					$date_from = strtotime($rule['start_date']);		
					$date_to = strtotime($rule['end_date']);	
					$args['date_query'] = array(
						'after'     => array( 'year'  => date('Y',$date_from), 'month' => date('m',$date_from), 'day' => date('d',$date_from) ),
						'before'    => array( 'year'  => date('Y',$date_to),   'month' => date('m',$date_to),   'day' => date('d',$date_to) ),
						'inclusive' => true,
					);
				}
				elseif ( $rule['period'] == 'p' )
				{
					//Последнее несколько дней, месяцев, лет							
					$date = new DateTime();									
					if ( $rule['period_from_type'] == 'y' )
					{
						$date->modify('-'.$rule['period_from'].' year');	
					}
					elseif ( $rule['period_from_type'] == 'm' )
					{
						$date->modify('-'.$rule['period_from'].' month');	
					}
					else
					{								
						$date->modify('-'.$rule['period_from'].' day');	
					}
					$args['date_query'] = array( 'after' => $date->format('Y-m-d H:i:s'), 'inclusive' => true );
				}						
				$args['count_total'] = false;
				$orders = new USAM_Orders_Query( $args );				
				$total_sum = $orders->get_total_amount(); 
				if ( !$total_sum )
					continue;	
			
				foreach ($rule['layers'] as $sum => $procent)	
				{
					if ( $total_sum < $sum )
						break;
					else													
						$discount = $procent;
				}			
			}	
		}
	} 
	return $discount;
}

/**
 * Проверка доступности прайс-листа
 */
function usam_availability_check_price_list( )
{
	global $user_ID;
	
	$options = get_option('usam_price_list_setting', array() );
	$user = get_userdata( $user_ID );	
	
	if ( !empty($options) )
	{
		foreach($options as $id => $option)	
		{							
			if ( $option['status'] == 1 )
			{
				$result = array_intersect($option['roles'], $user->roles);		
				if ( !empty($result) )
				{
					return true;
				}
			}
		}
	}
	return false;
}

function usam_user_checkout_update( $checkout ) 
{
	global $user_ID;
	
	$message = array();	
				
	$list_properties = usam_get_order_properties( array('fields' => 'id=>data') );	
	//$meta_data = usam_get_customer_meta( 'checkout_details' );	
	$meta_data = array();
	foreach ( (array)$checkout as $id => $value )
	{
		if ( !empty($list_properties[$id]) )
		{		
			switch ( $list_properties[$id]->type ) 
			{
				case "email":
					if ( !preg_match( "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\.[a-zA-Z]{2,5}$/", $value ) ) 
					{											
						$message[] = __( 'Пожалуйста, введите корректную электронную почту', 'usam' );
					}
					else
						$meta_data[$id] = $value;
				break;	
				case "phone":
				case "mobile_phone":
					$value = preg_replace('/[^0-9]/', '', $value);						
					if ( empty($value) || strlen($value) < 5 ) 
					{										
						$message[] = __( 'Пожалуйста, введите корректный телефон', 'usam' );
					}			
					else
						$meta_data[$id] = $value;
				break;
				case "location":	
					$meta_data[$id] = (int)$value;				
				break;	
				default:
					$meta_data[$id] = $value;				
				break;
			}
		}
	}
	$meta_data = apply_filters( 'usam_user_checkout_update', $meta_data, $user_ID );			
	usam_update_customer_meta( 'checkout_details', $meta_data );
	
	return apply_filters( 'usam_profile_message', $message );	
}

function usam_update_view()
{	
	switch ( $_REQUEST['view_type'] ) 
	{			
		case 'grid':
			$view_type = 'grid';
		break;				
		case 'list':
			$view_type = 'list';		
		break;
		case 'default':
		default:
			$view_type = 'default';				
		break;		
	}		
	usam_update_customer_meta( 'display_type', $view_type );
}
if ( isset( $_REQUEST['view_type'] ) ) 
	add_action( 'init', 'usam_update_view', 1 );
?>