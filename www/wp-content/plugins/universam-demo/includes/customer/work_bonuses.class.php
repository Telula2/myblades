<?php
/**
 * Класс бонусов
 */
 
//Получить все записи бонусов
function usam_get_bonus_account( $user_id = '' )
{
    global $user_ID;
	$bonus = new USAM_Work_Bonuses();
	if (  $user_id == '')
		$user_id =  $user_ID;
	return $bonus->get_bonus_account(  $user_id );	
}

//Сумма всех бонусов
function usam_get_total_bonus_account( $user_id = 0 )
{
	global $user_ID;
	if ( $user_id == 0 )
		$user_id = $user_ID;
	if ( $user_id == 0 )
		$total_bonus = 0;
	else
	{
		global $wpdb;
		$total_bonus = 0;
		if ( is_user_logged_in() )
		{
			$cache_key = 'total_bonus_account';
			if( ! $total_bonus = wp_cache_get($cache_key, $user_ID ) )
			{		
				$total_bonus = (int)$wpdb->get_var( "SELECT SUM(bonus) FROM `".USAM_TABLE_CUSTOMER_BONUSES."` WHERE user_id = '{$user_ID}' AND status = '3'" );				
				wp_cache_set( $cache_key, $total_bonus, $user_ID );
			}								
		}
		return $total_bonus;
	}
	return $total_bonus;
}

function usam_get_bonus_types( )
{
	$types = array( 'friends' =>  __('За приведенных знакомых','usam'), 'birthday' => __('За Ваш день рождения','usam'), 'socnetwork' => __('За активность в группе','usam'), 'help' => __('За помощь нашему проекту','usam'), 'buy' => __('За Ваши покупки','usam'), 'accumulative' => __('По программе &laquo;Накопительные скидки&raquo;','usam'), 'discont' => __('За участие в &laquo;Программе скидок&raquo;','usam'), 'product' => __('За товар','usam') 
	);	
	return $types;
}

function usam_get_bonus_type( $key )
{
	$types = usam_get_bonus_types();
	$result = false;
	if ( isset($types[$key]) )
		$result = $types[$key];
	
	return $types[$key];
}

function usam_get_statuses_bonus( $sort = false ) 
{
	$status_bonus = array( 	
				array(    
						'internalname'   => 'locked_order_completion',
						'title'          => __( 'Блокированные до завершения заказа', 'usam' ),
						'name'           => __( 'Блокированные', 'usam' ),						
						'sort'           => 1,
						'number'         => 1,						
					),	
				array(    
						'internalname'   => 'locked_time',
						'title'          => __( 'Блокированные до истечение времени возврата товара', 'usam' ),
						'name'           => __( 'Блокированные', 'usam' ),						
						'sort'           => 2,
						'number'         => 2,						
					),		
				array(    
						'internalname'   => 'available',
						'title'          => __( 'Доступные для использования', 'usam' ),
						'name'           => __( 'Доступные', 'usam' ),						
						'sort'           => 3,
						'number'         => 3,							
					),			
				array(    
						'internalname'   => 'locked_payment',
						'title'          => __( 'Блокированные для оплаты', 'usam' ),
						'name'           => __( 'Блокированные', 'usam' ),						
						'sort'           => 4,
						'number'         => 4,							
					),
				array(    
						'internalname'   => 'locked_order_completion',
						'title'          => __( 'Блокированные после использования до завершения заказа', 'usam' ),
						'name'           => __( 'Блокированные', 'usam' ),						
						'sort'           => 5,
						'number'         => 5,						
					),
				array(    
						'internalname'   => 'used',
						'title'          => __( 'Использованые', 'usam' ),
						'name'           => __( 'Использованые', 'usam' ),						
						'sort'           => 6,
						'number'         => 6,						
					),							
	);		
	$result_sort = array();
	if ( $sort )
	{			
		foreach ( $status_bonus as $status1 )
		{
			$min = 100;		
			foreach ( $status_bonus as $status2 )
				if ( $status2['order'] < $min )
					$min = $status2['order'];	
			foreach ( $status_bonus as $key => $status2 )
			{
				if ( $status2['order'] == $min )
				{
					$result_sort[] = $status2;
					unset($status_bonus[$key]);
				}
			}		
		}
	}
	else
		$result_sort = $status_bonus;
	$result = array();
	foreach ( $result_sort as $value )
	{
		$result[$value['number']] = $value;
	}
	return $result;
}
	


/**
 * Класс бонус
 */
class USAM_Work_Bonuses
{	
	private $user_id;
	private $error;

	public function __construct ( $user_id = 0 ) 
	{		
		global $wpdb;
		if ( $user_id == 0 )
			$user_id = get_current_user_id();	
		$this->user_id = $user_id;		
	}
	
	function get_error(  ) 
	{
		return $this->error;
	}
		
	//Выполняется после создания заказа в Журнале продаж
	function submit_checkout( $order_id ) 
	{
		if ( is_user_logged_in() )
		{
			$this->payment_bonuses();			
			$this->accumulative_discount_bonuses( $order_id );
		}
	}
	
	function get_bonus_account(  $user_id ) 
	{		
		global $wpdb;	
		return $wpdb->get_results("SELECT * FROM `".USAM_TABLE_CUSTOMER_BONUSES."` WHERE `user_id` = '$user_id' AND `status` IN ('1','2','3','4')",ARRAY_A);		
	}	
	
// Получить бонусы которые можно потратить	и заблокировать их
	function reserve_bonuses( $total_price )
	{
		global $wpdb, $usam_cart;
		if ( $this->user_id == 0 )	// Если пользователь не зарегистрирован он не может тратить бонусы
			return 0;		
		
		$rules = get_option('usam_bonus_rules', '');	
		if ( !empty($rules['percent']) )
			$btotal_price = round($total_price * $rules['percent'] / 100, 0);		
		
		if ( empty($rules['bonus_coupon']) && $usam_cart->get_properties( 'coupons_amount' ) != 0 )
		{
			$this->error = __('Нельзя использовать и бонусы и купоны одновременно', 'usam');
			return 0;	
		}		
		$total_bonus = 0;
		$bonus_sum = 0;		
		$ids = array( );				
		$bonus_account = $wpdb->get_results("SELECT * FROM `".USAM_TABLE_CUSTOMER_BONUSES."` WHERE `user_id` = '{$this->user_id}' AND `status` = '3' ORDER BY `bonus` ASC ");
		
		if ( !empty($bonus_account) )
		{
			foreach($bonus_account as $bonus)
			{							
				$bonus_sum += $bonus->bonus;
				if ( $bonus_sum < $btotal_price )
				{
					$total_bonus = $bonus_sum;
					$ids[] = $bonus->id;	
				}
				else
					break;
			}				
			if ( $total_bonus == 0 )
			{				
				$price = round($bonus_sum * 100/$rules['percent'], 0);
				$this->error = sprintf( __('Доступные бонусы можно использовать если сумма заказа будет больше %s', 'usam'),  usam_currency_display( $price ) );
			}
			else
				$wpdb->query( "UPDATE `" .USAM_TABLE_CUSTOMER_BONUSES. "` SET `status` = '4' WHERE `id` IN ('".implode( "','", $ids )."')");	
		}
		else
			$this->error = __('Нет доступных бонусов для использования', 'usam');		
		return $total_bonus;
	}

// Получить бонусы которые можно потратить
	function get_reserve_bonuses( )
	{
		global $wpdb;	

		$cache_key = 'reserve_bonuses';			
		$object_type = 'bonus';
		$total_bonus = wp_cache_get($cache_key, $object_type );
		if( $total_bonus === false )
		{				
			$total_bonus = (int)$wpdb->get_var("SELECT SUM(bonus) FROM `".USAM_TABLE_CUSTOMER_BONUSES."` WHERE `user_id` = '{$this->user_id}' AND `status` = '4' ");		
			wp_cache_set( $cache_key, $total_bonus, $object_type );			
		}					
		return $total_bonus;
	}	
	
// отменить бонусы, например когда заказ не завершен
	function cancel_reserve_bonuses()
	{				
		$data = array( 'status' => 3 );
		$data_where = array( 'status' => 4 );
		$this->update( $data, $data_where );		
	}	
	
	// Оплатить бонусами. Меняет статус и списывает бонусы
	function payment_bonuses()
	{		
		$data = array( 'status' => 5 );
		$data_where = array( 'status' => 4 );
		$this->update( $data, $data_where );
	}	
	
	// Списать бонусы. 
	function write_off_bonuses( $order_id )
	{	
		$data = array( 'status' => 6 );
		$data_where = array( 'order_id' => $order_id, 'status' => 5 );
		$this->update( $data, $data_where );		
	}

// Заказ завершен. Меняет статус. Блокирует до истечении времени возврата
	function order_completed( $order_id )
	{					
		$data = array( 'status' => 2, 'use_date' => date( "Y-m-d H:i:s" ) );
		$data_where = array( 'order_id' => $order_id, 'status' => 1, );
		$this->update( $data, $data_where );		
		
		$purchase_log = new USAM_Order( $order_id );		
		$products = $purchase_log->get_order_products();		
		
		$bonus = 0;				
		foreach($products as $product) 
		{					
			$p_meta = usam_get_product_meta( $product->product_id, 'product_metadata' );			
			if ( empty($p_meta['bonus']['value']) )
				continue;
			
			if ( $p_meta['bonus']['type'] == 'p' )
				$bonus += round($product->price*$product->quantity*$p_meta['bonus']['value']/100,0);
			else
				$bonus += $product->quantity*$p_meta['bonus']['value'];
		}		
		if ( $bonus != 0 )
		{
			$data = array( 'order_id' => $order_id, 'bonus' => $bonus, 'type' => 'product' );
			usam_insert_bonus( $data );
		}
	}
	
	function accumulative_discount_bonuses( $order_id )
	{	
		$discount = usam_get_accumulative_discount_customer( 'bonus' );		
		if ( $discount != 0 )
		{
			$purchase_log = new USAM_Order( $order_id );		
			$products = $purchase_log->get_order_products();		

			$sum = 0;				
			foreach($products as $product) 
			{			
				if ( $product->old_price == 0 )
					$sum += $product->quantity*$product->price;
			}
			if ( $sum > 0 )
			{
				$bonus = round( $sum * $discount / 100, 2 );	
				if ( $bonus != 0 )
				{
					$data = array( 'order_id' => $order_id, 'bonus' => $bonus, 'type' => 'accumulative' );
					usam_insert_bonus( $data );
				}		
			}
		}		
	}
	
	public function update( $data, $data_where ) 
	{	
		global $wpdb;	
		
		if ( $this->user_id == 0 )
			return;
		
		do_action( 'usam_update_bonuses_pre_edit', $this );
		
		$data['date_update'] = date( "Y-m-d H:i:s" );	
		$update = array();
		foreach( $data as $key => $value)
		{				
			$value = esc_sql(html_entity_decode(stripslashes($value), ENT_QUOTES,'UTF-8'));				
			if ( $value == '' )			
				continue;
			if ( $key == 'date' )
				$value = date( "Y-m-d H:i:s", strtotime( $value ) );				
			$update[] ="$key='$value'";
		}	
		$where = array();
		foreach( $data_where as $key => $value)
		{
			$where[] ="$key='$value'";
		}
		$wpdb->query("UPDATE ".USAM_TABLE_CUSTOMER_BONUSES." SET ".implode(',', $update)." WHERE user_id='".$this->user_id."' AND ".implode(' AND ', $where)."");
		do_action( 'usam_update_bonuses_edit', $this );		
	}
}

function usam_get_available_user_bonuses( $user_id = 0 )
{
	$bonus = new USAM_Work_Bonuses( $user_id );	
	return $bonus->get_reserve_bonuses();
}		

/**
 * Класс бонусов
 */
new USAM_Rules_Calculating_Bonuses();
class USAM_Rules_Calculating_Bonuses
{	
	private $rules = array();	
	public function __construct( ) 
	{
		$option = get_option('usam_bonuses_rules', array() );
		if ( !empty($option) )
			$this->rules = maybe_unserialize( $option );
		
		add_action( 'user_register', array( $this, 'event_user_register'), 5, 1 );
		add_action( 'usam_update_customer_review_status', array( $this, 'event_update_customer_review_status'), 10, 4 );
	}
	
	public function event_user_register( $user_id )
	{ 
		$this->work( 'register', $user_id );
	}
	
	public function event_update_customer_review_status( $id, $current_status, $previous_status, $t )
	{ 
		$user_id = $t->get('user_id');
		if (  $current_status == 1 && $user_id )
			$this->work( 'review', $user_id );
	}
	
	private function work( $type, $user_id )
	{				
		foreach( $this->rules as $rule )		
		{
			if ( $this->validate_rule( $rule ) && $type == $rule['type'] )
			{				
				$data = array( 'bonus' => $rule['value'], 'type' => $rule['type'], 'user_id' => $user_id ); 						
				usam_insert_bonus( $data );				
			}	
		}				
	}	
	
	
	private function validate_rule( $rule ) 
	{			
		$current_time = time();	
		if ( $rule['active'] && ( empty($rule['start_date']) || strtotime($rule['start_date']) <= $current_time ) && ( empty($rule['end_date']) || strtotime($rule['end_date']) >= $current_time ) )
		{		
			return true;
		}	
		return false;
	}	
}
?>