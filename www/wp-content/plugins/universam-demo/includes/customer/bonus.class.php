<?php
/**
 * Класс бонусов
 */
 class USAM_Bonus
{
	 // строковые
	private static $string_cols = array(
		'type',		
		'use_date',
		'date_insert',		
		'date_update',	
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'user_id',
		'order_id',	
		'payment_order_id',	
		'bonus',		
		'status',
	);
	// рациональные
	private static $float_cols = array(		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $is_status_changed = false;	
	private $previous_status   = false;		

	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;		
					
		$this->args = array( 'col' => $col, 'value' => $value );			
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_bonus' );
		}		
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_bonus ) 
	{
		$id = $_bonus->get( 'id' );	
		wp_cache_set( $id, $_bonus->data, 'usam_bonus' );				
		do_action( 'usam_bonus_update_cache', $_bonus );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$_bonus = new USAM_Bonus( $value, $col );
		wp_cache_delete( $_bonus->get( 'id' ), 'usam_bonus' );		
		do_action( 'usam_bonus_delete_cache', $_bonus, $value, $col );	
	}		
	
	/**
	 *  Удалить документ отгрузки
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_bonus_before_delete', $this );
		
		$id = $this->get( 'id' );
		self::delete_cache( $id );	
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_CUSTOMER_BONUSES." WHERE id = '$id'");
		
		do_action( 'usam_bonus_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_CUSTOMER_BONUSES." WHERE {$col} = {$format}", $value );
		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{	
			$this->exists = true;
			$this->data = apply_filters( 'usam_bonus_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_bonus_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_bonus_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_bonus_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}	
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_bonus_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_bonus_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$this->data['date_update'] = date( "Y-m-d H:i:s" );				
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	

			if ( isset($this->data) )
				unset($this->data['date_insert']);	
			
			do_action( 'usam_bonus_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);

			$this->data = apply_filters( 'usam_bonus_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );	
								
			foreach( $this->data as $key => $value)
			{										
				if ( $key == 'use_date' )
					if ( empty($value) )
						$set[] = "`{$key}`=NULL";
					else					
						$set[] = "`{$key}`='".date( "Y-m-d H:i:s", strtotime( $value ) )."'";
				else
					$set[] = "`{$key}`='{$value}'";						
			}	
			$result = $wpdb->query( $wpdb->prepare("UPDATE `".USAM_TABLE_CUSTOMER_BONUSES."` SET ".implode( ', ', $set )." WHERE $where_col ='$where_format'", $where_val) );
						
			do_action( 'usam_bonus_update', $this );
			if ( $this->is_status_changed ) 
			{		
				$current_status = $this->get( 'status' );
				$previous_status = $this->previous_status;
				$this->previous_status = $current_status;
				$this->is_status_changed = false;	
				$id = $this->get('id');
					
				do_action( 'usam_update_bonus_status', $id, $current_status, $previous_status, $this );		
			}
		} 
		else 
		{   
			do_action( 'usam_bonus_pre_insert' );		
			unset( $this->data['id'] );				
		
			if ( empty($this->data['order_id']) )
				$this->data['order_id'] = 0;
			
			if ( !isset($this->data['bonus']) )
				$this->data['bonus'] = 0;
			
			if ( !isset($this->data['status']) )
				$this->data['status'] = 1;		
			
			if ( !isset($this->data['user_id']) )				
				$this->data['user_id'] = $user_ID;
			
			if ( !isset($this->data['type']) )
				$this->data['type'] = '';		
			
			if ( empty($this->data['use_date']) )
				unset($this->data['use_date']);					
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );			

			$this->data = apply_filters( 'usam_bonus_insert_data', $this->data );			
			$format = $this->get_data_format(  );
			$this->data_format( );				

			$result = $wpdb->insert( USAM_TABLE_CUSTOMER_BONUSES, $this->data, $format );
					
			if ( $result ) 
			{								
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );						
			}
			do_action( 'usam_bonus_insert', $this );
		} 		
		do_action( 'usam_bonus_save', $this );

		return $result;
	}
}

// Обновить бонус
function usam_update_bonus( $data, $colum = 'id' )
{
	if ( empty($data[$colum]) )
		return false;
	
	$_bonus = new USAM_Bonus( $data[$colum], $colum );
	$_bonus->set( $data );
	return $_bonus->save();
}

// Получить бонус
function usam_get_bonus( $id, $colum = 'id' )
{
	$_bonus = new USAM_Bonus( $id, $colum );
	return $_bonus->get_data( );	
}

// Добавить бонус
function usam_insert_bonus( $data )
{	
	$_bonus = new USAM_Bonus( $data );
	$_bonus->save();	
	return $_bonus->get('id');
}

// Удалить бонус
function usam_delete_bonus( $id )
{
	$_bonus = new USAM_Bonus( $id );
	return $_bonus->delete();
}
?>