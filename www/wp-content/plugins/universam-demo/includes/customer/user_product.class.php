<?php
/**
 * Класс товаров пользователей
 */
class USAM_User_Product
{		
	 // строковые
	private static $string_cols = array(
		'user_code',		
		'date_insert',
		'user_list',
	);
	// цифровые
	private static $int_cols = array(
		'id',				
		'product_id',
		'user_id',		
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data = array();		
	private $fetched           = false;
	private $args = array( 'col'   => '', 'value' => '' );	
	private $exists = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'coupon_code'  && $id = wp_cache_get( $value, 'usam_user_product' ) )
		{   // если находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_user_product' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';

		return '%f';
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$log ) 
	{
		$id = $log->get( 'id' );
		wp_cache_set( $id, $log->data, 'usam_user_product' );		
		do_action( 'usam_user_product_update_cache', $log );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$log = new USAM_User_Product( $value, $col );
		wp_cache_delete( $log->get( 'id' ), 'usam_user_product' );		
		do_action( 'usam_user_product_delete_cache', $log, $value, $col );	
	}

	/**
	 * Удаляет из базы данных
	 */
	public static function delete( $id ) 
	{		
		global  $wpdb;
		do_action( 'usam_user_product_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_USER_PRODUCTS." WHERE id = '$id'");
		self::delete_cache( $id );		
		do_action( 'usam_user_product_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_USER_PRODUCTS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{
			$data['condition'] = unserialize( $data['condition'] );		
			$this->exists = true;
			$this->data = apply_filters( 'usam_user_product_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_user_product_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_user_product_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_user_product_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}		
		$properties = apply_filters( 'usam_user_product_set_properties', $properties, $this );
	
		if ( ! is_array($this->data) )
			$this->data = array();
		$this->data = array_merge( $this->data, $properties );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_user_product_pre_save', $this );	
		$where_col = $this->args['col'];	
							
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_user_product_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );

			$this->data = apply_filters( 'usam_user_product_update_data', $this->data );	
			$format = $this->get_data_format( );
			$this->data_format( );	
			
			$result = $wpdb->update( USAM_TABLE_USER_PRODUCTS, $this->data, array( $where_col => $where_val ), $format, $where_format );	
			do_action( 'usam_user_product_update', $this );
		} 
		else 
		{   
			do_action( 'usam_user_product_pre_insert' );			
			
			unset( $this->data['id'] );
			if ( !isset($this->data['user_id']))
				$this->data['user_id'] = $user_ID;	
			
			if ( !isset($this->data['user_code']))
				$this->data['user_code'] = usam_get_current_customer_id();	
			
			$this->data['date_insert']      = date( "Y-m-d H:i:s" );		
			
			$this->data = apply_filters( 'usam_user_product_insert_data', $this->data );
			$format = $this->get_data_format(  );
			$this->data_format( );		
			$result = $wpdb->insert( USAM_TABLE_USER_PRODUCTS, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
// установить $this->args так, что свойства могут быть загружены сразу после вставки строки в БД
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_user_product_insert', $this );
		} 		
		do_action( 'usam_user_product_save', $this );

		return $result;
	}	
}

// Обновить 
function usam_update_user_product( $id, $data )
{
	$user_product = new USAM_User_Product( $id );
	$user_product->set( $data );
	return $user_product->save();
}

// Получить
function usam_get_user_product( $id, $colum = 'id' )
{
	$user_product = new USAM_User_Product( $id, $colum );
	return $user_product->get_data( );	
}

// Добавить
function usam_insert_user_product( $data )
{
	$user_product = new USAM_User_Product( $data );
	return $user_product->save();
}

// Удалить 
function usam_delete_user_product( $id )
{
	$user_product = new USAM_User_Product( $id );
	return $user_product->delete();
}
?>