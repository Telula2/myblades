<?php
/**
 * Обратная связь
 */ 
class USAM_AI
{
	 // строковые
	private static $string_cols = array(
		'type_message',		
		'mail',		
		'phone',
		'name',		
		'message',
		'description',			
		'date_insert',		
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'id_product',				
		'user_id',	
		'manager_id',	
		'status',
		'location_id',
	);
	// рациональные
	private static $float_cols = array(	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */		
	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $text ) 
	{
		if ( empty($text) )
			return;	
		
		$this->sensor( $text );
	}
	
	private function sensor( $text ) 
	{
		$offers = explode('.', $text );		
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_feedback ) 
	{
		$id = $_feedback->get( 'id' );	
		wp_cache_set( $id, $_feedback->data, 'usam_feedback' );		
		do_action( 'usam_feedback_update_cache', $_feedback );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$_feedback = new USAM_Feedback( $value, $col );
		wp_cache_delete( $_feedback->get( 'id' ), 'usam_feedback' );				
		do_action( 'usam_feedback_delete_cache', $_feedback, $value, $col );	
	}		
	
	/**
	 *  Удалить документ отгрузки
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_feedback_before_delete', $this );
		
		$id = $this->get( 'id' );	
		self::delete_cache( $id );				
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_FEEDBACK." WHERE id = '$id'");
		
		do_action( 'usam_feedback_delete', $id );
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_FEEDBACK." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_feedback_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_feedback_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_feedback_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_feedback_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_feedback_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_feedback_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	

			if ( isset($this->data['date_insert']) )
				unset($this->data['date_insert']);
			
			do_action( 'usam_feedback_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $where_col => $where_val );			

			$this->data = apply_filters( 'usam_feedback_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );		
			
			$result = $wpdb->update( USAM_TABLE_FEEDBACK, $this->data, $where, $format, $where_format );			
			do_action( 'usam_feedback_update', $this );
		} 
		else 
		{   
			do_action( 'usam_feedback_pre_insert' );		
			unset( $this->data['id'] );	
						
			if ( !isset($this->data['status']) )
				$this->data['status'] = 0;
			
			if ( !isset($this->data['manager_id']) )
				$this->data['manager_id'] = 0;
			
			if ( !isset($this->data['mail']) )
				$this->data['mail'] = '';
						
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );			
			
			$this->data = apply_filters( 'usam_feedback_insert_data', $this->data );			
			$format = $this->get_data_format(  );		
					
			$this->data_format( );							
			$result = $wpdb->insert( USAM_TABLE_FEEDBACK, $this->data, $format );
					
			if ( $result ) 
			{						
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );				
			}
			do_action( 'usam_feedback_insert', $this );
		} 		
		do_action( 'usam_feedback_save', $this );

		return $result;
	}
}

// Обновить
function usam_update_feedback( $id, $data )
{
	$feedback = new USAM_Feedback( $id );	
	$feedback->set( $data );
	return $feedback->save();
}

// Получить
function usam_get_feedback( $id, $colum = 'id' )
{
	$feedback = new USAM_Feedback( $id, $colum );
	return $feedback->get_data( );	
}

// Добавить
function usam_insert_feedback( $data )
{
	$feedback = new USAM_Feedback( $data );
	return $feedback->save();
}

// Удалить
function usam_delete_feedback( $id )
{
	$feedback = new USAM_Feedback( $id );
	return $feedback->delete();
}
?>