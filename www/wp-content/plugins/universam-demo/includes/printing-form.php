<?php		
class USAM_Printing_Form
{	
	protected $id;	
	protected $options;	
	protected $edit;
	
	public function __construct( $id )
	{		
		if ( !empty($id) )			
			$this->id = $id;	
	}		
	
	public function get_template( $printed_form )
	{ 
		$file = USAM_PRINTING_FORM."/$printed_form.php";
		$html = '';
		if ( file_exists($file) )
		{				
			ob_start();			
			include( USAM_PRINTING_FORM."/$printed_form.php" );			
			$html = ob_get_contents();
			ob_end_clean();	
		} 
		return $html;
	}
	
	public function get_edit_printing_forms( $printed_form )
	{			
		$this->edit = true;
		
		$out = $this->get_template( $printed_form );
		if ( empty($out) )
			return false;		
					
		$options = $this->get_printing_forms_options( $printed_form );
		
		if ( !isset($options['table']) )			
			$options['table'] = array();	

		if ( !isset($options['data']) )			
			$options['data'] = array();		

		$pattern = '#%%(.+?)%%#s';	
		preg_match_all($pattern, $out, $result, PREG_SET_ORDER);	
		$parent = array();
		$i = 0;
		foreach ( $result as $arr ) 
		{
			$args = explode(' ', $arr[1] );	
			$html = '';
			if ( $args[0] == 'edit_colum' )
			{				
				if ( isset($args[1]) )			
					$name = $args[1];			
				else
					$name = '';	
				
				if ( isset($options['table'][$name]) )			
					$option = $options['table'][$name];			
				else
					$option = array( 'c' => true, 'title' => '' );
				
				if ( empty($option['title']) )	
				{
					preg_match('~"(.*?)(?:"|$)|([^"]-)~',$arr[1], $title ); 
					if ( isset($title[1]) )
						$value = $title[1];
					else
						$value = '';
				}
				else
					$value = $option['title'];						
		
				if ( $option['c'] )	
					$checked = true;
				else
					$checked = false;
				$html = "<input type='text' class='table_colum_edit' style='width:100%; border-color: #8B0000' value='$value' name='$name' />
				<input style='display:block; margin:auto;' type='checkbox' class='table_colum_edit_show' value='1' ".checked( $checked, true, false )." name='{$name}_show' />";
			}	
			else	
			{	
				preg_match_all('~"(.*?)(?:"|$)|([^"]-)~',$arr[1], $title ); 
				if ( !empty($options['data'][$args[0]]) )			
					$value = $options['data'][$args[0]];	
				elseif ( isset($title[1][0]) )
					$value = $title[1][0];	
				else
					$value = '';				
		
				$parent["##$i##"] = $value;
				$value = "##$i##";
				$i++;
				
				$types = array( 'style' => 'width:100%; border-color: #8B0000', 'name' => $args[0] );
				if ( isset($title[1][0]) )
				{
					$parent["##$i##"] = $title[1][0];
					$types['placeholder'] = "##$i##";
					$i++;
					$parent["##$i##"] = isset($title[1][1])?$title[1][1]:$title[1][0];
					$types['title'] = "##$i##";
					$i++;	
				}					
				$str = '';
				foreach ( $types as $key => $v ) 
					$str .= " $key='$v'";
		
				if ( !empty($args[1]) && $args[1] == 'input' )
					$html = "<input type='text' class='option_form option_form_input' value='$value' $str/>";
				elseif ( $args[1] == 'textarea' )
					$html = "<textarea rows='9' class='option_form option_form_textarea' $str/>$value</textarea>";				
			}	
			$out = str_replace( $arr[0], $html, $out );
		}
		$out = $this->process_args( $out, 'no' );	
	
		$values = array_values($parent);
		$keys = array_keys($parent);
		$out = str_replace( $keys, $values, $out );	
		
		echo $out;
		exit;
	}	
	
	public function get_printing_forms_options( $printed_form )
	{
		$printing_form_options = get_option( 'usam_printing_form', array() );		
		if ( isset($printing_form_options[$printed_form]) )
			$options = $printing_form_options[$printed_form];
		else
			$options = array();	
		
		return $options;
	}
	
	protected function get_table_columns( $columns )
	{
		$display_columns = array();
		foreach ( $columns as $column_key => $column_name ) 
		{	
			if ( isset($this->options['table'][$column_key]) && !$this->options['table'][$column_key]['c'] )						
				continue;
			
			if ( $this->edit )		
			{
				$display_name = $column_name;
			}			
			else
			{
				if ( !isset($this->options['table'][$column_key]) )				
				{
					preg_match('~"(.*?)(?:"|$)|([^"]-)~',$column_name, $result ); 
					if ( isset($result[1]) )
						$display_name = $result[1];
					else
						$display_name = '';
				}
				else
					$display_name = $this->options['table'][$column_key]['title'];
			}
			$display_columns[$column_key] = $display_name;
		}	
		return $display_columns;
	}
	
	protected function display_table_thead( $columns )
	{				
		foreach ( $columns as $column_key => $display_name ) 
		{						
			echo "<th class='column-$column_key'>$display_name</th>";		
		}		
	}
	
	protected function display_table( $columns )
	{ 
		$display_columns = $this->get_table_columns( $columns );
		?>
		<table id="products-table" style="width:100%">
			<thead>
				<tr>
					<?php $this->display_table_thead( $display_columns ); ?>
				</tr>
			</thead>
			<tbody>				
				<?php $this->display_table_tbody( $display_columns ); ?>				
			</tbody>
			<tfoot>
				<?php $this->display_table_tfoot( $display_columns ); ?>
			</tfoot>
		</table>
		<?php	
	}
	
	protected function display_table_tbody( $display_columns )
	{
		
	}
	
	protected function display_table_tfoot( $display_columns )
	{
		
	}	
	
	protected function get_args( )
	{
		return array();
	}
	
	private function process_args( $html, $replacement = 'all' ) 
	{ 	
		$args =	$this->get_args();
		$standart_args = $this->get_standart_args();		
		$args = array_merge ($standart_args, $args);		
	
		$shortcode = new USAM_Shortcode();		
		return $shortcode->process_args( $args, $html, $replacement );
	}
		
	private function get_standart_args( )
	{
		$requisites = usam_shop_requisites_shortcode();	
		return $requisites;
	}
	
	protected function get_args_company( $bank_account_id, $company_type ) 
	{ 	
		$bank_account = usam_get_acc_number( $bank_account_id );
		$args = array();
		if ( !empty($bank_account) )
		{
			$company = new USAM_Company( $bank_account['company_id'] );	
			$company_data = $company->get_data();	
			
			$company_meta = $company->get_meta();			
			foreach ( $company_data as $key => $data ) 	
			{
				$_key = $company_type.'_'.$key;
				$args[$_key] = $data;
			}
			$communications = usam_get_company_means_communication( $company_data['id'] );
			foreach ( $communications as $key => $communication ) 	
			{
				foreach ( $communication as $data ) 
				{ 
					$_key = $company_type.'_'.$key;					
					$args[$_key] = $data['value'];	
					break;
				}
			}
			
			foreach ( $company_meta as $key => $data ) 	
			{
				$_key = $company_type.'_'.$key;
				$args[$_key] = $data;
			}
			foreach ( $bank_account as $key => $data ) 	
			{
				if ( $key != 'company_id' )
				{
					$_key = $company_type.'_bank_'.$key;
					$args[$_key] = $data;
				}
			}
		}
		return $args;
	}	
	
	public function get_printing_forms( $printed_form )
	{			
		$this->options = $this->get_printing_forms_options( $printed_form );		
		$this->edit = false;
	
		$html = $this->get_template( $printed_form );	
		if ( empty($html) )
			return false;
		
	//	$pattern = '/\[#[^\[\]]+#\]/';
		$pattern = '#%%(.+?)%%#s';	
		preg_match_all( $pattern, $html, $result, PREG_SET_ORDER);	
		foreach ( $result as $arr ) 
		{			
			$args = explode(' ', $arr[1] );		
			$str = '';
			if ( isset($this->options['data'][$args[0]]) )
				$str = nl2br($this->options['data'][$args[0]]);
			elseif ( isset($arr[1]) )
			{
				preg_match('~"(.*?)(?:"|$)|([^"]-)~',$arr[1], $r ); 
				if ( isset($r[1]) )
					$str = $r[1];
			}					
			$html = str_replace( $arr[0], $str, $html );
		}	
		$html = $this->process_args( $html );			
		return $html;		
	}	
	
	public function get_printing_forms_to_pdf( $printing_form )
	{ 
		$html = $this->get_printing_forms( $printing_form );
		$args = usam_get_data_printing_forms( $printing_form );	
		if ( $args == false )
			return false;
		$pdf = usam_export_to_pdf( $html, $args );		
		return $pdf;
	}
}

function usam_get_printing_forms_to_pdf( $printed_form, $id = null ) 
{ 
	$data = usam_get_data_printing_forms( $printed_form );	
	if ( $data == false )
		return false;
	
	$file_path = USAM_FILE_PATH . "/includes/printing-forms/printing-forms-".$data['type'].".php";	
	$printing = new USAM_Printing_Form( $id );	
	if ( !empty($data['type']))
	{
		if ( file_exists( $file_path ) )	
		{
			require_once( $file_path );	
			$class = "USAM_Printing_Form_".$data['type'];
			if ( class_exists($class) ) 
			{
				$printing = new $class( $id );				
			}			
		}	
	}
	return $printing->get_printing_forms_to_pdf( $printed_form );
}

function usam_get_printing_forms( $printed_form, $id = null ) 
{		
	$data = usam_get_data_printing_forms( $printed_form );
	if ( $data == false )
		return false;
	
	$printing = new USAM_Printing_Form( $id );		
	if ( !empty($data['type']))
	{
		$file_path = USAM_FILE_PATH . "/includes/printing-forms/printing-forms-".$data['type'].".php";		
		if ( file_exists( $file_path ) )	
		{
			require_once( $file_path );	
			$class = "USAM_Printing_Form_".$data['type'];		
			if ( class_exists($class) ) 		
				$printing = new $class( $id );
		}		
	}
	return $printing->get_printing_forms( $printed_form );
}

function usam_get_edit_printing_forms( $printed_form, $id = null ) 
{ 
	$data = usam_get_data_printing_forms( $printed_form );
	if ( $data == false )
		return false;
	
	$file_path = USAM_FILE_PATH . "/includes/printing-forms/printing-forms-".$data['type'].".php";		
	if ( file_exists( $file_path ) )	
	{
		require_once( $file_path );	
		$class = "USAM_Printing_Form_".$data['type'];	
		if ( class_exists($class) ) 
		{ 
			$printing = new $class( $id );
		}		
		else
			$printing = new USAM_Printing_Form( $id );		
	}
	else
		$printing = new USAM_Printing_Form( $id );
	return $printing->get_edit_printing_forms( $printed_form );
}

// Получить данные формы
function usam_get_data_printing_forms( $printed_form ) 
{ 
	$description = '';
	$title = '';
	$type = '';
	$orientation = '';
	$file_path = USAM_PRINTING_FORM .'/'. $printed_form.'.php';		
	if ( file_exists( $file_path ) )	
	{		
		$blanks_data = implode( '', file( $file_path ) );
		if ( preg_match( '|Printing Forms:(.*)$|mi', $blanks_data, $name ) ) 
		{					
			$title = $name[1];
		}		
		if( preg_match('|^type:[ ]?(.[a-z_]+)|mi', $blanks_data, $name) )		
		{	
			$type = $name[1]; 
		}			
		if( preg_match('|^orientation:[ ]?(.[a-z_]+)|mi', $blanks_data, $name) )		
		{	
			$orientation = $name[1]; 
		}	
		else
			$orientation = 'portrait';
		if( preg_match('|Description:(.*)$|mi', $blanks_data, $name) )		
		{	
			$description = $name[1]; 
		}	
		return array( 'title' => $title, 'type' => $type, 'orientation' => $orientation, 'description' => $description );
	}
	return false;
}
?>