<?php
new USAM_Exchange_FTP( 'run_class' );
final class USAM_Exchange_FTP
{
	public  $message = array();
	public  $errors  = array();
	private $connect = false;
	private $ftp = false;
	private $settings;	
	private $mode; 
	private $set_event = true;
	
	public function __construct( $type_initialize = 'run_function' )
	{			
		
	}
	
	public function get_error( )
	{	
		return $this->errors;
	}	
	
	public function get_message( )
	{	
		return $this->message;
	}	
	
	private function set_error( $error )
	{			
		$this->errors[]  =  sprintf( __('Обновление с FTP. Класс USAM_Exchange_FTP. Ошибка: %s'), $error );
	}
	
	private function set_log_file( )
	{	
		usam_log_file( $this->errors );
		$this->errors = array();
	}
		
	private function ftp_open()
	{				
		
		return false;
	}
	
	/* Открыть директорию для обмена */
	function open_dir( $dir_name )
	{
		$path_dir = USAM_UPLOAD_DIR.'exchange/'.$dir_name;
		if ( !is_dir($path_dir) )
		{
			if ( !mkdir($path_dir, 0777, true) )
			{
				$this->set_error( __('Не удалось создать директорию','usam') );	
				return false;
			}
		}
		$handle = opendir($path_dir);
		return $handle;		
	}
	
	// получить данные из файла
	private function get_data_from_file( $path, $columns )
	{
		
		return array();
	}	
	
	function posts_fields( $sql )
	{
		global $wpdb;
		$sql = "$wpdb->posts.ID,$wpdb->postmeta.meta_value AS code";	
		return $sql;
	}
	
	private function get_product_data_from_file( $name_file, $column_str )
	{	
		$columns = explode(',', $column_str);
		$file_data = $this->get_data_from_file( $name_file, $columns );				
		
		$products_data = array();
		foreach ( $file_data as $data )
		{
			$products_data[$data['code_product']] = $data;
		}			
		$args = array(
				'meta_query' => array(			
					array(
						'key'     => '_usam_code',
						'value'   => array_keys($products_data),
						'compare' => 'IN'
					),				
				),
				'cache_results' => false,
				'update_post_term_cache' => false,
				'suppress_filters' => false,					
				
		);			
		add_filter( 'posts_fields', array( &$this, 'posts_fields' ) );
		$products = usam_get_products( $args );
		$return_product_data = array();
		foreach ( $products as $product )
		{
			if ( !empty($products_data[$product->code]) )
			{
				$return_product_data[$product->ID] = $products_data[$product->code];
			}
		}
		return $return_product_data;
	}	
/*=============================================== ЦЕНЫ =======================================================*/		
	
	public function update_price_all_products_cron( )
	{
		$this->update_price_all_products();
		$this->set_log_file( );
	}
	
	public function update_price_products_cron( )
	{
		$this->update_price_products();
		$this->set_log_file( );
	}
	
	// Функция загрузки цен все товаров с FTP
	public function update_price_all_products( )
	{				
		$result = false;		
		if ( $this->ftp_open() )
		{		
			$name_file = 'prices_products.txt';	
			if ( $this->ftp->ftp_get( $name_file, $this->settings['file_price'] )  )
			{					
				$products = $this->get_product_data_from_file( $name_file, $this->settings['column_price'] );	
				
				$i = count($products);						
				if ( $this->set_event || ( defined( 'DOING_CRON' ) && !DOING_CRON) ) 
				{					
					$result = 'set_task';					
					usam_create_system_process( __("Обновление цены","usam" ), $products, array(&$this, 'start_process_update_price' ), $i, 'update_price_ftp' );					
				}
				else
				{
					$this->start_process_update_price( $products );	
					$result = 'completed';					
				}
			}
			else
				$this->set_error( __("Файл с ценами не найден. Возможно Вы забыли выгрузить файл с ценами для сайта из программы.","usam" ) );		
			$this->ftp->ftp_close();			
		}			
		return $result;
	}
	
	
	public function update_price_products( )
	{
		if ( !empty($this->settings['file_price_pereozenka']) && $this->ftp_open() )
		{	
			if ( $handle = $this->open_dir( 'price_update' ) )
			{	
				$this->ftp->copy_files( 'price_update', $this->settings['file_price_pereozenka'] );
				while (false !== ($file = readdir($handle))) 
				{					
					$products = $this->get_product_data_from_file( 'price_update/'.$file, $this->settings['column_price'] );	
					$this->start_process_update_price( $products );
				}	
				closedir($handle);	
			}					
			$this->ftp->ftp_close();
		}
	}	
	
	// Начать процесс обновление цен товаров
	public function start_process_update_price( $products, $step = 0 )
	{				
		ini_set("max_execution_time", "3660");		
		
		$count = count($products);
		$products_ids = array_keys($products);		
		if ( $step > 0 && $count > $step )
		{		
			array_splice($products_ids, $step);			
		}		
		update_object_term_cache($products_ids, "usam-product");
	//	update_postmeta_cache($products_ids);				
		foreach ( $products_ids as $product_id )
		{					
			$product = $products[$product_id];
			$prices = array();		
			$type_price = isset($product['type_price'])?$product['type_price']:$this->settings['type_price'];				
			$prices['price_'.$type_price] = $product['price'];
			usam_edit_product_prices( $product_id, $prices );	
			unset($products[$product_id]);
		}	
		return array( 'done' => count($products_ids), 'data' => $products );	
	}	
	
	// Вернуть цену, когда сохраняется товар
	public function return_product_price( $search_code ) 
	{				
		$prices = array();
		if ( get_option('usam_ftp_download_prices') )
		{		
			if ( $this->ftp_open() )
			{						
				$name_file = 'prices_products.txt';	
				if ( $this->ftp->ftp_get( $name_file, $this->settings['file_price'] )  )			
				{					
					$columns = explode(',', $this->settings['column_price']);
					$file_data = $this->get_data_from_file( $name_file, $columns );	
					foreach ( $file_data as $data )
					{
						if ( $data['code_product'] == $search_code )
						{
							$type_price = !empty($data['type_price'])?$data['type_price']:$this->settings['type_price'];									
							$prices['price_'.$type_price] = $data['price'];												
						}						
					}				
				}				
				$this->ftp->ftp_close();
			}	
		}
		return $prices;
	}	
/*=============================================== ОСТАТКИ =======================================================*/	
	
	public function update_product_quantity_cron( )
	{ 	
		$this->update_product_quantity();			
		$this->set_log_file( ); 
	}
	
	public function update_product_all_quantity_cron( )
	{			
		$this->update_product_all_quantity();
		$this->set_log_file( );		
	}
	
	// получить данные для обновления остатков
	private function get_data_for_updating_quantity( $local_file )
	{
		$columns = explode(',', $this->settings['column_stock']);
		$file_data = $this->get_data_from_file( $local_file, $columns );	

		if ( empty($file_data) )		
			return array();

		if ( $this->settings['where_load_stock'] != 'any' )
		{
			$storage = usam_get_storage( $this->settings['where_load_stock'] );
			if ( empty($storage['meta_key']) )
				return array();
		
			$prod_storage_key = $storage['meta_key'];
		}
		else
			$prod_storage_key = $this->settings['where_load_stock'];
		
		$data = array();				
		foreach ( $file_data as $item )
		{			
			if ( $this->settings['min_stock'] != '' && $item['stock'] < $this->settings['min_stock'] ) 
				$item['stock'] = 0;	
			 
			if ( $prod_storage_key == 'any' )
				$data[$item['code_product']][$item['code_warehouse']] = (int)$item['stock'];
			else
				$data[$item['code_product']][$prod_storage_key] += (int)$item['stock'];			
		}		
		return $data;
	}
	
	// Получить товары для обновления остатков
	private function get_product_for_updating_quantity( $data, $products_not_found = 'out_of_stock', $all_code = true, $remove_missing = true ) 
	{
		if ( $all_code )
		{
			$args = array(
					'meta_query' => array(			
						array(
							'key'     => '_usam_code',
							'value'   => '',
							'compare' => '!='
						),				
					),					
			);		
		}
		else
		{
			$args = array(
					'meta_query' => array(			
						array(
							'key'     => '_usam_code',
							'value'   => array_keys($data),
							'compare' => 'IN'
						),				
					),					
			);
		}	
		$args['update_post_term_cache'] = false;
		$args['fields'] = 'ids';	
		
		$products_ids = usam_get_products( $args );	
		
	//	update_postmeta_cache($products_ids);		
		$update_products = array();
		if ( !empty($products_ids) )
		{ 
			$storages = usam_get_stores();	
			foreach ( $products_ids as $product_id)
			{				
				$code = usam_get_product_meta($product_id, 'code');
				if ( ( $products_not_found === 'out_of_stock' && !isset($data[$code]) ) || isset($data[$code]) )
				{	// Не снимать с продажи товары, не найденные в файле с остатками								
					foreach ( $storages as $storage)
					{							
						if ( $storage->code != '' )
						{
							if ( isset($data[$code][$storage->code]) )						
								$stock = $data[$code][$storage->code];	
							else
							{	
								if ( !$remove_missing )
									continue;
								$stock = 0;	// Снять с продажи товары, не найденные в файле с остатками								
							}
							$stock_db = usam_get_product_meta($product_id, $storage->meta_key, true);
							if ( $stock_db != $stock )
								$update_products[$product_id][$storage->meta_key] = $stock; 
						}
					}							
				}
			}
		}		
		return $update_products;
	}		
	
		// Начать процесс обновление остатков товаров
	public function start_process_update_stock( $products, $count = 0 )
	{		
		global $wpdb;
		
		ini_set("max_execution_time", "3660");			
		if ( empty($products) )
			return array( 'done' => 0, 'data' => array() );			
			
		$products_ids = array_keys($products);
		
		if ( $count )	
			$products_ids = array_slice( $products_ids, 0, $count );
				
		update_postmeta_cache( $products_ids );
			
		$i = 0;			
		foreach ( (array)$products as $product_id => $product )
		{				
			foreach ( $product as $storage_code => $stock )
			{			
				$result_update = $wpdb->update( $wpdb->postmeta, array( 'meta_value' => $stock ), array( 'post_id' => $product_id, 'meta_key' => USAM_META_PREFIX.$storage_code ), array( '%s' ), array( '%d', '%s' ) );
			}	
			usam_recalculate_stock_product( $product_id );	
			$i++;		
			unset($products[$product_id]);
			if ( $count != 0 && $i >= $count)
				break;
		}		
		return array( 'done' => $i, 'data' => $products );	
	}
	
// Функция загрузки остатков товара с FTP ( 1 файл - остатки после продажи )
	public function update_product_quantity( )
	{		
		if ( $this->ftp_open() )
		{			
			if ( empty($this->settings['file_stock']) || $this->settings['where_load_stock'] != 'any' ) 
			{	// папка не найдена, прервать выполнение
				$this->set_error( __("Папка в настройках не указана. Укажите папку.","usam" ) );
				return; 
			}		
			$name_dir = 'stock';
			if ( $handle = $this->open_dir( $name_dir ) )
			{ 	
				$result = $this->ftp->copy_files( $name_dir, $this->settings['file_stock'] );				
				$data = array();
				while (false !== ($file_name = readdir($handle))) 
				{		
					$return_data = $this->get_data_for_updating_quantity( $name_dir.'/'.$file_name );
					$data = array_merge ($data, $return_data);
				}	
				closedir($handle);	
				$products = $this->get_product_for_updating_quantity( $data, 'skip', false, false );
				$this->start_process_update_stock( $products );				
			}		
			$this->ftp->ftp_close();	
		}	
	}
	
	// Функция загрузки всех остатков товара с FTP
	public function update_product_all_quantity( ) 
	{	
		$result = false;
		if ( $this->ftp_open() )
		{		
			$name_file = "stock.txt";				
			if ( $this->ftp->ftp_get( $name_file, $this->settings['file_all_stock'] )  )							
			{					
				$data = $this->get_data_for_updating_quantity( $name_file );				
				$products = $this->get_product_for_updating_quantity( $data, $this->settings['products_not_found'] ); 
				$i = count($products);			
				if ( $this->set_event || ( defined( 'DOING_CRON' ) && !DOING_CRON) ) 
				{						
					$result = 'set_task';
					usam_create_system_process( __("Обновление остатков(FTP)","usam" ), $products, array(&$this, 'start_process_update_stock' ), $i, 'update_stock_ftp' );
				}
				else
				{
					$result = 'completed';
					$this->start_process_update_stock( $products );
					$this->message[] = str_replace( '%s', $i, __("Обновлены остатки у %s продуктов","usam" ));			
				}								
			}
			else			
				$this->set_error( __("Файл не скопирован на FTP","usam") );					
			
			$this->ftp->ftp_close();
		}		
		return $result;
	}	
		
	// Узнать загружаются ли остатки с ftp
	public function know_product_stock_loaded_ftp( $search_code ) 
	{		
		$product_stock = array();	
		if ( get_option( 'usam_ftp_download_balances', 0 ) )
		{
			if ( $this->ftp_open() )
			{			
				$name_file = 'stock.txt';					
				if ( $this->ftp->ftp_get( $name_file, $this->settings['file_all_stock'] )  )							
				{					
					$columns = explode(',', $this->settings['column_stock']);
					$file_data = $this->get_data_from_file( $name_file, $columns );				
					
					$args = array( 'fields' => 'code, meta_key', 'condition' => array( array( 'col' => 'code', 'compare' => '!=', 'value' => '' ) ) );
					$storages = usam_get_stores( $args ); 
				
					$storages_active = array();
					foreach ( $storages as $storage)
					{
						$storages_active[$storage->code] = $storage->meta_key;
						$product_stock[$storage->meta_key] = 0;		
					}	
					foreach ( $file_data as $data)
					{
						if ( $search_code == $data['code_product'] )
						{
							if ( isset($storages_active[$data['code_warehouse']]))
								$product_stock[$storages_active[$data['code_warehouse']]] = $data['stock'];						
						}						
					}				
				}
				$this->ftp->ftp_close();
			}
		}	
		return $product_stock;
	}
	
/*=============================================== ЗАГРУЗИТЬ ДРУГИЕ ДАННЫЕ =======================================================*/		
	public function update_all_product_data( )
	{		
		if (  $this->ftp_open() )
		{	
			$this->ftp->ftp_get( "data.txt", $this->settings['file_data'] );							
			$products = $this->get_product_data_from_file( "data.txt", $this->settings['column_data'] );		
			$i = 0;	
			foreach ( $products as $product_id => $data )
			{	
				$barcode = usam_get_product_meta($product_id, 'barcode');
				if ( !empty($data['barcode']) && $data['barcode'] != $barcode )
				{
					$result_update_barcode = usam_update_product_meta( $product_id, 'barcode', $data['barcode'] );		
					if ( $result_update_barcode >= 1 )
						$i++;
				}		
			}			
			$this->message = str_replace( '%s', $i, __("Обновлено %s продуктов","usam" ));		
		}
	}
	
// Найти товары, которых нет на сайте, а есть в файле на FTP	
	public function find_the_missing_products( )
	{
		global $wpdb;
		$return = array();	
		if (  $this->ftp_open() )
		{					
			$name_file = 'data.txt';					
			if ( $this->ftp->ftp_get( $name_file, $this->settings['file_data'] ) )							
			{				
				$columns = explode(',', $this->settings['search_products']['columns'] );
				$file_data = $this->get_data_from_file( $name_file, $columns );	
				
				$product_ftp_data = array();
				foreach($file_data as $data ) 
				{
					if ( isset($data['stock']) && ( empty($this->settings['search_products']['stock']) || $this->settings['search_products']['stock'] <= $data['stock'] ) )
						$product_ftp_data[$data['code_product']] = $data;
				}				
				$products_in_sku = $wpdb->get_col("SELECT meta_value FROM $wpdb->postmeta WHERE meta_value IN ('".implode("','", array_keys($product_ftp_data) )."') AND meta_key = '_usam_code'");	
				foreach($product_ftp_data as $code_product => $product ) 
				{
					if ( !in_array($code_product, $products_in_sku) )
						$return[] = $product;
				}	
			}
		}		
		return $return;
	}
	
// Добавить товары, которых нет на сайте, а есть в файле на FTP	
	public function insert_the_missing_products( )
	{
		global $wpdb;
		$ids = array();	
		if (  $this->ftp_open() )
		{					
			$name_file = 'data.txt';					
			if ( $this->ftp->ftp_get( $name_file, $this->settings['file_data'] ) )							
			{				
				$columns = explode(',', $this->settings['search_products']['columns'] );
				$file_data = $this->get_data_from_file( $name_file, $columns );	
				
				$product_ftp_data = array();
				foreach($file_data as $data ) 
				{
					if ( !empty($data['code_product']))
					{
						if ( isset($data['stock']) && ( empty($this->settings['search_products']['stock']) || $this->settings['search_products']['stock'] <= $data['stock'] ) )
							$product_ftp_data[$data['code_product']] = $data;
					}
				}				
				$products_in_sku = $wpdb->get_col("SELECT meta_value FROM $wpdb->postmeta WHERE meta_value IN ('".implode("','", array_keys($product_ftp_data) )."') AND meta_key = '_usam_code'");	
				foreach($product_ftp_data as $code_product => $product ) 
				{
					if ( !in_array($code_product, $products_in_sku) )
					{						
						$product = array(
							'post_title'             => isset($product['title'])?$product['title'] : '',	
							'post_status'            => 'draft',
							'post_content'           => isset($product['content'])?$product['content'] : '',	
							'post_excerpt'           => isset($product['excerpt'])?$product['excerpt'] : '',	
							'thumbnail'              => isset($product['thumbnail'])?$product['thumbnail'] : '',	
							'meta'                   => array(								
								'sku'               => isset($product['sku'])?$product['sku']:$code_product,	
								'weight'            => isset($product['weight'])?$product['weight'] : '',	
								'code'              => $code_product,
								'barcode'           => isset($product['barcode'])?$product['barcode'] : '',	
							)
						);
						$_product = new USAM_Product( $product );		
						$ids[] = $_product->insert_product();	
					}
				}	
			}
		}		
		return $ids;
	}
		
	private function create_data_file_by_order_id( $order_id )
	{
		$export_status = false;		
		
		$order = new USAM_Order( $order_id );		
		$order_data = $order->get_data();
		$products = $order->get_order_products();	
		
		if ( empty($products) )
		{
			$this->set_error( __('В документе отгрузка нет товаров','usam') );
			return $export_status;		
		}				
		$totalprice = usam_string_to_float($order_data['totalprice']);	
		$discount = $order_data['coupon_discount'] + $order_data['bonus'];		
		$procent_coupon = 0;
		if ( $discount != 0 )
			$procent_coupon = $discount * 100 / ( $totalprice - $order_data['shipping'] + $discount );	
		
		$balance = 0;
		$ostatok = 0;
		$columns = explode(',', $this->settings['column_order']);	
		$data = array();
		$count = count($products);

		$r = 0;
		foreach((array)$products as $key => $product ) 
		{	
			$code = usam_get_product_meta( $product->product_id, 'code' );						
			$price = usam_string_to_float( $product->price );			
			if ( $procent_coupon > 0 )
			{
				$p = $price * $procent_coupon / 100;				
				$discont = round($p , 3);
				$ostatok += $discont - $p;				
				if ( $price >= $discont )
					$price -= $discont;
				else
				{
					$balance += $discont - $price;
					$price = 0;
				}					
			}		
			if ( $balance > 0 )		
			{
				if ( $price >= $balance )	
				{
					$price -= $balance;
					$balance = 0;
				}
				else
				{
					$balance -= $price;
					$price = 0;
				}		
			}	
			if ( $count == $key )
			{
				$price += $ostatok;				
				$price = round($price , 3);
			}
			$r += $price*$product->quantity;
			$item = array( 'code_product' => $code, 'price' => $price, 'code_warehouse' => $this->settings['code_warehouse'], 'quantity' => $product->quantity );	
				
			$row = array();
			foreach($columns as $column ) 
			{
				if ( isset($item[$column]) )
					$row[] = $item[$column];
			}
			$data[] = implode( $this->settings['file']['separator'], $row );			
		}			
		$out = implode( PHP_EOL, $data );
				 
		$filename = $order_id.'.txt';	
		$result = $this->ftp->fwrite_tp_put( $out, $filename, $this->settings['file_receipt'], $this->settings['file_sale_encoding'] );	
		if ( $result )			
			$this->message[]  = __('Файл закачен на ftp','usam');			
		else	
		{
			foreach( $this->ftp->get_errors() as $error ) 			
				$this->set_error( $error );	
		}
		return $result;
	}
	
	// Создает файл
	private function create_data_file( $document_id )
	{
		global  $wpdb;	
		
		$document = usam_get_shipped_document( $document_id );
		$code_warehouse = usam_get_store_field( $document['storage_pickup'], 'code' );	
		if ( empty( $code_warehouse ) )	
		{				
			return false;	
		}			
		$products = $wpdb->get_results( $wpdb->prepare( "SELECT cc.*, sp.quantity AS quantity_shipment, sp.reserve AS reserve FROM `". USAM_TABLE_SHIPPED_PRODUCTS ."` AS sp 
					LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc ON ( cc.id=sp.basket_id )					
					WHERE sp.documents_id = '%d' ORDER BY price ASC", $document['id']), ARRAY_A );
		
		if ( empty($products) )
		{
			$this->set_error( sprintf( __('В документе отгрузки №%s нет товаров','usam'),$document['id']) );
			return false;		
		}			
		$order = new USAM_Order( $document['order_id'] );
		$order_data = $order->get_data();	
		
		$totalprice = usam_string_to_float($order_data['totalprice']);	
		$discount = $order_data['coupon_discount'] + $order_data['discount'] + $order_data['bonus'];		
		$procent_coupon = 0;
		if ( $discount != 0 )
			$procent_coupon = $discount * 100 / ( $totalprice - $document['price'] + $discount );	
		
		$balance = 0;
		$ostatok = 0;
		$columns = explode(',', $this->settings['column_order']);	
		$data = array();
		$count = count($products) - 1;
		
		$r = 0;
		foreach((array)$products as $key => $product ) 
		{	
			$code = usam_get_product_meta( $product['product_id'], 'code' );						
			$price = usam_string_to_float( $product['price'] );			
			if ( $procent_coupon > 0 )
			{
				$p = $price * $procent_coupon / 100;				
				$discont = round($p , 3);
				$ostatok += $discont - $p;				
				if ( $price >= $discont )
					$price -= $discont;
				else
				{
					$balance += $discont - $price;
					$price = 0;
				}					
			}		
			if ( $balance > 0 )		
			{
				if ( $price >= $balance )	
				{
					$price -= $balance;
					$balance = 0;
				}
				else
				{
					$balance -= $price;
					$price = 0;
				}		
			}	
			if ( $count == $key )
			{
				$price += $ostatok;				
				$price = round($price , 3);
			}
			$r += $price*$product['quantity_shipment'];
			$item = array( 'code_product' => $code, 'price' => $price, 'code_warehouse' => $code_warehouse, 'quantity' => $product['quantity_shipment'] );	
			
			$row = array();
			foreach($columns as $column ) 
			{
				if ( isset($item[$column]) )
					$row[] = $item[$column];
			}
			$data[] = implode( $this->settings['file']['separator'], $row );
		}		
		$out = implode( PHP_EOL, $data );
				
		$filename = $document['order_id'] . '.txt';	
		$result = $this->ftp->fwrite_tp_put( $out, $filename, $this->settings['file_reserve'], $this->settings['file_sale_encoding'] );	
		if ( $result )			
			$this->message[]  = __('Файл закачен на ftp','usam');			
		else		
		{
			foreach( $this->ftp->get_errors() as $error ) 			
				$this->set_error( $error );	
		}
		return $result;
	}
	
/**
 * Выгружает заказы на FTP
*/	
	private function start_orders_export( $document_id, $type_export = 'reserve' )
	{
		$export_status = false;		
		switch( $type_export )
		{			
			case 'reserve': 
				$export_status = $this->create_data_file( $document_id );									
				if ( $export_status )
				{					
					$this->set_document_export( $document_id );				
				}
			break;					
		}		
		return $export_status;
	}	
		
	function upload_transactions()
	{	
		if ( !empty($this->settings['file_upload_transactions']) && $this->ftp_open() )
		{																			
			if ( $handle = $this->open_dir( 'upload_transactions' ) )
			{	
				$this->ftp->copy_files( 'upload_transactions', $this->settings['file_upload_transactions'] );
				while (false !== ($file = readdir($handle))) 
				{			
					$local_file = 'upload_transactions/'.$file;		
					$columns = explode(',', $this->settings['column_transactions']);
					$file_data = $this->get_data_from_file( $local_file, $columns );				
					
					foreach ( $file_data as $transact_data )
					{
						if ( !isset($transact_data['order']) )
							$order_id = (int)$file;		
						else
							$order_id = (int)$transact_data['order'];
									
						$order = new USAM_Order( $order_id );	
						$data = $order->get_data();
						
						if ( !empty($data) && !empty($transact_data['transactid']) )
						{						
							$update_payment = array( 'sum' => $data['totalprice'], 'status' => 3, 'transactid' => $transact_data['transactid'] );
							if ( !empty($transact_data['date']) )
								$update_payment['date_payed'] = date( "Y-m-d H:i:s", strtotime($transact_data['date']) );
							
							$payment_documents = $order->get_payment_history();
							if ( !empty($payment_documents) )
							{		
								$payment_document = array_pop($payment_documents);
								$payment = array_merge( $payment_document, $update_payment );							
								$order->edit_payment_history( $payment );
							}
							else
							{			
								$order->add_payment_history( $update_payment );
							}							
						}
					}			
				}	
				closedir($handle);
			}					
			$this->ftp->ftp_close();
		}		
	}
		
	// Запускает выгрузку заказов на FTP
	function export_orders_ftp_cron()
	{			
		$statuses = usam_get_order_statuses( array( 'fields' => 'internalname', 'close' => 0 ) );	
		$document_ids = usam_get_shipping_documents( array('fields' => 'id', 'storage_pickup__not_in' => 0, 'export' => 0, 'order_status' => $statuses ) );	
		if ( count($document_ids) > 0 )
		{
			$this->submit_reserve_export( $document_ids );			
		}		
		$this->upload_transactions();	
		$this->set_log_file(  );
	}
	
	// Запустить выгрузку резервов
	public function export_orders_ftp( $document_id )
	{	
		if ( get_option('usam_ftp_export_order') )
		{	
			return $this->submit_reserve_export( $document_id );
		}
	}
	
	// Отправить резерв на FTP при нажатии ссылки "Выгрузить на FTP" в заказе
	public function submit_reserve_export( $document_ids )
	{	
		$result = false;
		if ( $this->ftp_open() )
		{
			if ( !is_array($document_ids) )
				$document_ids = array($document_ids);
			
			foreach ( $document_ids as $document_id )
				$result = $this->start_orders_export( $document_id );
				
			$this->ftp->ftp_close();				
		}
		return $result;	
	}
	
	// Отправить заказ на FTP при нажатии ссылки "Выгрузить на FTP" в заказе
	public function export_order( $order_ids )
	{	
		$result = false;
		if ( $this->ftp_open() )
		{
			if ( !is_array($order_ids) )
				$order_ids = array($order_ids);
			
			foreach ( $order_ids as $order_id )
				$result = $this->create_data_file_by_order_id( $order_id );
				
			$this->ftp->ftp_close();
		}
		return $result;			
	}		
	
	public function set_document_export( $document_id )
	{	
		global $wpdb;	
		$update = $wpdb->query ("UPDATE `".USAM_TABLE_SHIPPED_DOCUMENTS."` SET export='1' WHERE id='{$document_id}'");
	}
}
?>