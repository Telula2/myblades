<?php
// загрузка из Exel
function usam_read_exel_file( $filepath, $sheetname = '' )
{ 
	$results = array();
	if (  file_exists($filepath) )
	{
		require_once( USAM_FILE_PATH . '/resources/PHPExcel/PHPExcel.php');	
		$excel = PHPExcel_IOFactory::load( $filepath );		
		$results = array();
		foreach ($excel->getWorksheetIterator() as $worksheet)
		{ 		
			if ( $sheetname == '' )
			{
				$results = $worksheet->toArray();		
				break;				
			}
			elseif ( $worksheet->getTitle() == $sheetname )
			{
				$results = $worksheet->toArray();			
				break;
			}
		}
	}
	return $results;
}


// Выгрузка в Exel
function usam_write_exel_file( $filename, $data_export, $header = array() )
{
	if ( empty($data_export) )
		return false;
	
	require_once( USAM_FILE_PATH . '/resources/PHPExcel/PHPExcel.php');	// подключаем фреймворк
	$pExcel = new PHPExcel(); //создаем рабочий объект
	$pExcel->setActiveSheetIndex(0); // устанавливаем номер рабочего документа
	$aSheet = $pExcel->getActiveSheet(); // получаем объект рабочего документа
		
	$writer_i = 1;
	$j = 0;		
	if ( !empty($header) )
	{
		foreach($header as $key => $val)
		{						
			$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			$aSheet->setCellValueByColumnAndRow($j,$writer_i,$val); // записываем данные массива в ячейку
			$j++;
		}	
		$writer_i++;
	}
	foreach($data_export as $data)
	{
		$j = 0;			
	//	$aSheet->getColumnDimension($writer_i)->setAutoSize(true);
	//	$aSheet->getStyle($writer_i)->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); // выравнивание	
		if ( empty($header) )
		{		
			foreach($data as $val)
			{			
				$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$aSheet->setCellValueByColumnAndRow($j,$writer_i,$val); // записываем данные массива в ячейку				
				$j++;
			}
		}
		else
		{
			foreach($header as $key => $val)
			{						
				if (isset($data[$key]))
					$item = $data[$key];
				elseif (isset($data[$val]))
					$item = $data[$val];
				else
					$item = '';
				$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);				
				$aSheet->setCellValueByColumnAndRow( $j, $writer_i, $item ); // записываем данные массива в ячейку	
				$j++;				
			}			
		}
		$writer_i++;
	}	
	$objWriter = new PHPExcel_Writer_Excel5( $pExcel ); // создаем объект для записи excel в файл
	header('Content-Type: application/vnd.ms-excel'); // посылаем браузеру нужные заголовки для сохранения файла
	header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
	header('Cache-Control: max-age=0');	
	$objWriter->save('php://output'); // выводим данные в excel формате
	exit;
}

use Dompdf\Dompdf;


function usam_export_to_pdf( $html, $args = array() )
{
	if ( empty($html) )
	{
		$html = '
		<html>
			<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>
			<body><p>'.__('Нет данных для отображения','usam').'</p></body>
        </html>';	
	}
	require_once(USAM_FILE_PATH.'/resources/dompdf/autoload.inc.php');	
	$dompdf = new DOMPDF(array('enable_remote' => true));	
	
	$paper = !empty($args['paper'])?$args['paper']:'a4';
	$orientation = !empty($args['orientation'])?$args['orientation']:'portrait'; //landscape portrait
	
	$dompdf->set_paper( $paper, $orientation );
	$dompdf->load_html( $html );	
	$dompdf->render();	
	$output = $dompdf->output();
	return $output; 
}

// Загрузить данные из CSV файла
function usam_read_txt_file( $file_path, $delimiter = ',', $encoding = 'utf-8' ) 
{			
	$f = fopen( $file_path, 'r' );					
	$length = filesize( $file_path );	
	$results = array();
	while ( $row = fgetcsv( $f, $length, $delimiter ) )
	{			
		if ( $encoding != 'utf-8' )
		{
			foreach($row as $key => $value)				
				$row[$key] = iconv ($encoding, 'utf-8', $value);		
		}
		$results[] = $row;
	}		
	return $results;
}
?>