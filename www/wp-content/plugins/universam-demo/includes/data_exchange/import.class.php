<?php

class USAM_Import
{		
	private $delimiter = ',';	
	
	public function __construct( $delimiter = '' )
	{   		
		if ( !empty($delimiter) )
			$this->delimiter = $delimiter;
		
	}
	
	// Импорт из текстовой переменной
	public function import_text( $default_fields, $text )
	{
		$import = trim(stripslashes($text));
		$import = str_replace(array("\r", "\n\n", "\n\t\t\n\t\n\t\n\t\n", "\n\t\t\n\t\n\t\n", "\xEF\xBB\xBF", "\n\t\n", "\n(+1)"), array("\n", "\n", "\n ;", "\n", '', ';', ''), $import);
		$arraylines = explode("\n", $import);
					
		$results = array();
		foreach ( $arraylines as $str )
		{	
			$results[] = explode($this->delimiter, $str);
		}
		$results = $this->get_results( $results, $default_fields );	
		return $results;		
	}
	
	public function import_file_path( $file_path )
	{
		$results = array();
		$extension = usam_get_extension( $file_path ); // узнать формат файла
		switch ( $extension ) 
		{
			case 'xls':	
			case 'xlsx':
				$results = usam_read_exel_file( $file_path );	
			break;
			case 'txt':
			case 'csv':			
				$results = usam_read_txt_file( $file_path, $this->delimiter );
			break;							
			default:
				return $results;				
			break;
		}	
		return $results;		
	}
	
	// Импорт из файла
	public function import_file( $default_fields, $file_key = 'import_file' )
	{			
		ini_set( 'auto_detect_line_endings', 1 );
	
		$results = array();
		if( !empty($_FILES[$file_key]) && $_FILES[$file_key]['name'] != '' )
		{		
			$file_path = USAM_FILE_DIR . $_FILES[$file_key]['name'];
			if ( !move_uploaded_file( $_FILES[$file_key]['tmp_name'], $file_path ) )
				return $results;
			
			$results = $this->import_file_path( $file_path );
			return $this->get_results( $results, $default_fields );		
		}				
		return $results;
	}
	
	private function get_results( $data, $fields )
	{
		$keys = array( );
		$results = array();
		foreach ( $data as $row )
		{							
			if ( empty($keys) )
			{			
				$count = count($row);			
				for ( $i = 0; $i < $count; $i++ )
				{				
					$key = trim($row[$i]);
					if ( in_array( $key, $fields) )
					{					
						$keys[$key] = $i;
					}	
				}							
				if ( empty($keys) )
				{
					$this->sendback = add_query_arg( array('error' => 'keys'), $this->sendback );	
					return false;
				}								
			}
			else
			{														
				foreach ( $keys as $key => $value )
				{				 
					$contact[$key] = trim($row[$value]);
				}
				if ( !empty($contact) )
					$results[] = $contact;
			}			
		}
		return $results;
	}
}	
?>