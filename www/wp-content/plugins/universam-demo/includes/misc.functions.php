<?php
/**
 * Разные функций магазина УНИВЕРСАМ
 * @since 3.7
 */

/**
 * Отключение проверки SSL для Curl. Добавлять / удалять, например, так:
 * add_filter('http_api_curl', 'usam_curl_ssl');
 * remove_filter('http_api_curl', 'usam_curl_ssl');
 */
function usam_curl_ssl($ch) 
{
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	return $ch;
}

/**
 * Добавить нового пользователя в базу
 * @since 3.7
 */
function usam_add_new_user( $user_login, $user_pass, $user_email )
{
	require_once(ABSPATH . WPINC . '/registration.php');
	$errors = new WP_Error();
	$user_login = sanitize_user( $user_login );
	$user_email = apply_filters( 'user_registration_email', $user_email );

	// Check the username
	if ( $user_login == '' )	
		$errors->add( 'empty_username', __( '<strong>Ошибка</strong>: Пожалуйста, введите имя пользователя.', 'usam' ) );
	elseif ( !validate_username( $user_login ) ) 
	{
		$errors->add( 'invalid_username', __( '<strong>Ошибка</strong>: Это имя пользователя является недействительным. Пожалуйста введите действительное имя пользователя.', 'usam' ) );
		$user_login = '';
	} 
	elseif ( username_exists( $user_login ) )
		$errors->add( 'username_exists', __( '<strong>Ошибка</strong>: Это имя пользователя уже зарегистрировано, пожалуйста, выберите другое.', 'usam' ) );
	
	// Check the e-mail address
	if ( $user_email == '' )
		$errors->add( 'empty_email', __( '<strong>Ошибка</strong>: Пожалуйста, введите свой адрес электронной почты.', 'usam' ) );
	
	elseif ( !is_email( $user_email ) )
	{
		$errors->add( 'invalid_email', __( '<strong>Ошибка</strong>: Адрес электронной почты не правильный.', 'usam' ) );
		$user_email = '';
	} 
	elseif ( email_exists( $user_email ) )
		$errors->add( 'email_exists', __( '<strong>Ошибка</strong>: Этот адрес уже зарегистрирован, пожалуйста, выберите другой.', 'usam' ) );	

	if ( $errors->get_error_code() ) {
		return $errors;
	}
	$user_id = wp_create_user( $user_login, $user_pass, $user_email );
	if ( !$user_id ) 
	{
		$errors->add( 'registerfail', sprintf( __( '<strong>Ошибка</strong>: Не удалось зарегистрировать вас ... Пожалуйста, свяжитесь с <a href="mailto:%s">нами</a> !', 'usam' ), get_option( 'admin_email' ) ) );
		return $errors;
	}
	$credentials = array( 'user_login' => $user_login, 'user_password' => $user_pass, 'remember' => true );
	$user = wp_signon( $credentials );
	return $user;
	//wp_new_user_notification($user_id, $user_pass);
}


/**
 * Обратимое шифрование методом "Двойного квадрата" (Reversible crypting of "Double square" method)
 */
function usam_ds_crypt( $input, $decrypt = false ) 
{
	$o = $s1 = $s2 = array(); // Arrays for: Output, Square1, Square2
	// формируем базовый массив с набором символов
	$basea = array('?','(','@',';','$','#',"]","&",'*');  // base symbol set
	$basea = array_merge($basea, range('a','z'), range('A','Z'), range(0,9) );
	$basea = array_merge($basea, array('!',')','_','+','|','%','/','[','.',' ') );
	$dimension=9; // of squares
	for($i=0;$i<$dimension;$i++) { // create Squares
		for($j=0;$j<$dimension;$j++) {
			$s1[$i][$j] = $basea[$i*$dimension+$j];
			$s2[$i][$j] = str_rot13($basea[($dimension*$dimension-1) - ($i*$dimension+$j)]);
		}
	}
	unset($basea);
	$m = floor(strlen($input)/2)*2; // !strlen%2
	$symbl = $m==strlen($input) ? '':$input[strlen($input)-1]; // last symbol (unpaired)
	$al = array();
	// crypt/uncrypt pairs of symbols
	for ($ii=0; $ii<$m; $ii+=2) {
		$symb1 = $symbn1 = strval($input[$ii]);
		$symb2 = $symbn2 = strval($input[$ii+1]);
		$a1 = $a2 = array();
		for($i=0;$i<$dimension;$i++) { // search symbols in Squares
			for($j=0;$j<$dimension;$j++) {
				if ($decrypt) {
					if ($symb1===strval($s2[$i][$j]) ) $a1=array($i,$j);
					if ($symb2===strval($s1[$i][$j]) ) $a2=array($i,$j);
					if (!empty($symbl) && $symbl===strval($s2[$i][$j])) $al=array($i,$j);
				}
				else {
					if ($symb1===strval($s1[$i][$j]) ) $a1=array($i,$j);
					if ($symb2===strval($s2[$i][$j]) ) $a2=array($i,$j);
					if (!empty($symbl) && $symbl===strval($s1[$i][$j])) $al=array($i,$j);
				}
			}
		}
		if (sizeof($a1) && sizeof($a2)) {
			$symbn1 = $decrypt ? $s1[$a1[0]][$a2[1]] : $s2[$a1[0]][$a2[1]];
			$symbn2 = $decrypt ? $s2[$a2[0]][$a1[1]] : $s1[$a2[0]][$a1[1]];
		}
		$o[] = $symbn1.$symbn2;
	}
	if (!empty($symbl) && sizeof($al)) // last symbol
		$o[] = $decrypt ? $s1[$al[1]][$al[0]] : $s2[$al[1]][$al[0]];
	return implode('',$o);
}

/**
 * Проверьте memory_limit и рассчитать рекомендованный размер памяти
 */
function usam_check_memory_limit() 
{
	if ( (function_exists( 'memory_get_usage' )) && (ini_get( 'memory_limit' )) ) 
	{
		// получить память
		$memory_limit = ini_get( 'memory_limit' );
		if ( $memory_limit != '' )
			$memory_limit = substr( $memory_limit, 0, -1 ) * 1024 * 1024;

		// Расчитать свободную память
		$freeMemory = $memory_limit - memory_get_usage();
		
		$sizes = array( );
		$sizes[] = array( 'width' => 800, 'height' => 600 );
		$sizes[] = array( 'width' => 1024, 'height' => 768 );
		$sizes[] = array( 'width' => 1280, 'height' => 960 );  // 1MP
		$sizes[] = array( 'width' => 1600, 'height' => 1200 ); // 2MP
		$sizes[] = array( 'width' => 2016, 'height' => 1512 ); // 3MP
		$sizes[] = array( 'width' => 2272, 'height' => 1704 ); // 4MP
		$sizes[] = array( 'width' => 2560, 'height' => 1920 ); // 5MP
	
		foreach ( $sizes as $size ) 
		{			
			if ( $freeMemory < round( $size['width'] * $size['height'] * 5.09 ) ) {
				$result = sprintf( __( 'Пожалуйста, воздержитесь от загрузки изображений больших, чем  <strong>%d x %d</strong> пикселей', 'usam' ), $size['width'], $size['height'] );
				return $result;
			}
		}
	}
	return;
}

// Скачать картинку по её url
function usam_download_image_in_url( $url, $path )
{	
	$kurl = curl_init($url);
	$path_parts = pathinfo( $url );
	$file = $path_parts['basename'];
	$path_file = $path.$file;
	
	curl_setopt($kurl, CURLOPT_HEADER, 0);
	curl_setopt($kurl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($kurl, CURLOPT_BINARYTRANSFER,1);
	
	$rawdata = curl_exec($kurl);
	curl_close($kurl);
	if (file_exists($path_file)) 
		unlink($path_file);

	$fp = fopen($path_file,'x');
	fwrite($fp, $rawdata);
	fclose($fp);
	return array( 'path' => $path_file, 'name' => $file );
}

// сжатие css.
// Использовать ob_start("compress_css");
function usam_compress_css($buffer)
{
	/* Удаляем комментарии */
	$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
	/* Удаляем табуляции, пробелы, переводы строки и так далее */
	//$buffer = str_replace(array("\r", "\t", '  ', '    ', '    '), '', $buffer);
	return $buffer;
}

//точно получить IP-адрес
function usam_get_the_user_ip() 
{
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) 
	{	//check ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} 
	elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) 
	{	//to check ip is pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else 
		$ip = $_SERVER['REMOTE_ADDR'];
	return apply_filters( 'edd_get_ip', $ip );
}

/* Функция генерации уникальной строки
  * @var length - длина генерируемой последовательности
  * Так как я в своем примере использую префикс в 2 символа,
  * то по молчанию длинна основного ключа: +6 символов
  * @var chars - набор символов, участвующих в генерации
  * в наборе может присутствовать что угодно: 
  * $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890'
  */
function usam_rand_string( $length = 6, $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890' )
{
     // получаем длину строки символов
    $chars_length = (strlen($chars) - 1);   
    $string = $chars{rand(0, $chars_length)};     
     // Генерируем
    for ($i = 1; $i < $length; $i = strlen($string)) 
    {
        // Берем случайный элемент из набора символов
        $r = $chars{rand(0, $chars_length)};         
         // Убеждаемся, что соседние символы не совпадают.
        if ($r != $string{$i - 1}) $string .=  $r;
    }
    return $string;
}

// очистка переменных GET и POST
function usam_clean_variable( $variable, $option = 'str' )
{		
	switch ( $option )
	{
		case 'str':		
			$variable = trim( $variable );
			$variable = stripslashes($variable);
			$variable = htmlspecialchars($variable);
		break;
		case 'int':		
			$variable = (int) $variable;			
		break;
		case 'array':		
			$variable = $variable;			
		break;
	}
	return $variable;
}

// Массив в ссылку
function usam_url_array_encode( $data )
{
    return strtr(base64_encode(addslashes(gzcompress(serialize($data),9))), '+/=', '-_,');
}


function usam_url_array_decode( $encoded ) {

    return unserialize(gzuncompress(stripslashes(base64_decode(strtr($encoded, '-_,', '+/=')))));
}

function usam_check_thumbnail_support()
{
	if ( !current_theme_supports( 'post-thumbnails' ) ) 
	{
		add_theme_support( 'post-thumbnails' );
		add_action( 'init', 'usam_remove_post_type_thumbnail_support' );
	}
}
add_action( 'after_setup_theme', 'usam_check_thumbnail_support', 99 );

function usam_remove_post_type_thumbnail_support() 
{
	remove_post_type_support( 'post', 'thumbnail' );
	remove_post_type_support( 'page', 'thumbnail' );
}

// Получить текущий квартал
function usam_get_beginning_quarter( $m = null, $y = null ) 
{
	if ( $m == null )	
		$m = date('m' );
	
	if ( $y == null )	
		$y = date('Y' );
	
	$m = (int)$m;
	
	switch ( $m ) 
	{
		case 1:
		case 2:
		case 3:
			return mktime( 0,0,0,1,1,$y);
		break;
		case 4: 
		case 5: 
		case 6:
			return mktime( 0,0,0,4,1,$y);
		break;
		case 7: 
		case 8: 
		case 9:
			return mktime( 0,0,0,7,1,$y);
		break;
		case 10: 
		case 11: 
		case 12:
			return mktime( 0,0,0,10,1,$y);
		break;
			   
	}
}

function usam_loader( $id = '' ) 
{	
	echo usam_get_loader( $id );
}

function usam_get_loader( $id = '' ) 
{	
	$loader ='<img style="margin:auto; vertical-align: middle;" id="ajax-loading'.$id.'" class ="ajax-loading" src="'.USAM_CORE_IMAGES_URL.'/loading.gif" />';
	return $loader;
}

function usam_array_merge_recursive()
{
    $arrays = func_get_args();
    $base = array_shift($arrays);
    foreach($arrays as $array) 
	{
        reset($base);
        while(list($key, $value) = @each($array)) {
            if(is_array($value) && @is_array($base[$key])) {
                $base[$key] = usam_array_merge_recursive($base[$key], $value);
            }
            else {
                $base[$key] = $value;
            }
        }
    }
    return $base;
}

function usam_shop_logo( ) 
{
	$shop_company = get_option( 'usam_shop_company' );
	$bank_account = usam_get_acc_number( $shop_company );
	
	if ( !empty($bank_account['company_id']) )
	{
		$company = new USAM_Company( $bank_account['company_id'] );	
		$company_data = $company->get_data();	
		
		if ( !empty($company_data['logo']) )
			$image_attributes = wp_get_attachment_image_src( $company_data['logo'], 'thumbnail' ); 
	}
	if ( empty($image_attributes[0]) )	
		$thumbnail = USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif';			
	else
		$thumbnail = $image_attributes[0];	
	return $thumbnail;
}

function usam_get_img_shop_logo( ) 
{
	$thumbnail = usam_shop_logo( );	
	$html = '<img class="shop_logo" src="'.$thumbnail.'" alt ="'.get_option( 'blogname' ).'">';
	return $html;	
}

function usam_shop_requisites()
{
	$shop_company = get_option( 'usam_shop_company' );		
	return usam_get_company_by_acc_number( $shop_company );
}

function usam_shop_requisites_shortcode()
{
	$company = usam_shop_requisites();	
	$requisites = array(); 		 
	
	$requisites['contactcountry'] = '';
	$requisites['contactcity'] = '';
	$requisites['legalcountry'] = '';
	$requisites['legalcity'] = '';	 
	if ( !empty($company['contactlocation']) )
	{
		$locations = usam_get_array_locations_up( $company['contactlocation'], 'name', 'code' );
		foreach($locations as $key => $name) 
			$requisites['contact'.$key] = $name;
	}			
	if ( !empty($company['legallocation']) )
	{
		$locations = usam_get_array_locations_up( $company['legallocation'], 'name', 'code' );
		foreach($locations as $key => $name) 
			$requisites['legal'.$key] = $name;
	}			
	$requisites['contactaddress'] = !empty($company['contactaddress'])?$company['contactaddress']:'';
	$requisites['contactpostcode'] = !empty($company['contactpostcode'])?$company['contactpostcode']:'';
	$requisites['legalpostcode'] = !empty($company['legalpostcode'])?$company['legalpostcode']:'';
	$requisites['legaladdress'] = !empty($company['legaladdress'])?$company['legaladdress']:'';;
	$requisites['full_company_name'] = !empty($company['full_company_name'])?$company['full_company_name']:'';
	$requisites['inn'] = !empty($company['inn'])?$company['inn']:'';
	$requisites['ppc'] = !empty($company['ppc'])?$company['ppc']:'';
	$requisites['full_legaladdress'] = !empty($company['full_legaladdress'])?$company['full_legaladdress']:'';
	$requisites['business_bic'] = !empty($company['bank_account']['bic'])?$company['bank_account']['bic']:'';
	$requisites['business_rsch'] = !empty($company['bank_account']['bank_ca'])?$company['bank_account']['bank_ca']:'';
	$requisites['business_bank'] = !empty($company['bank_account']['name'])?$company['bank_account']['name']:'';
	$requisites['business_bank_address'] = !empty($company['bank_account']['address'])?$company['bank_account']['address']:'';
	$requisites['business_company_ca'] = !empty($company['bank_account']['number'])?$company['bank_account']['number']:'';
	$requisites['phone'] = !empty($company['phone'])?$company['phone']:'';	
	$requisites['bank_details']  = !empty($company['bank_details'])?$company['bank_details']:'';
	$requisites['company_details']  = $requisites['full_company_name'].' '.__( "ИНН:", 'usam' ).' '.$requisites['inn'].' '.__( "КПП:", 'usam' ).' '.$requisites['ppc'].' '.$requisites['full_legaladdress'];	
	$requisites['shop_name'] = get_option( 'blogname' );	
	$requisites['shop_logo'] = usam_get_img_shop_logo();
	return $requisites;
}

function usam_is_license_type( $type ) 
{
	$license = get_option ( 'usam_license' );
	if ( empty($license['type']) )
		return false;
	else
		return $license['type'] == $type;
}

function usam_get_name_type_license() 
{
	$license = get_option ( 'usam_license' );
	switch ( $license['type'] ) 
	{
		case 'TEMP' :
			$message = __( 'временная', 'usam' );				
		break;
		case 'FREE' :
			$message = __( 'бесплатная', 'usam' );	
		break;
		case 'LITE' :
			$message = __( 'начальная', 'usam' );	
		break;			
		case 'PRO' :		
			$message = __( 'профессиональная', 'usam' );	
		break;
		default:
			$message = __( 'неизвестный тип', 'usam' );	
	}
	return $message;
}

function usam_get_name_status_license() 
{
	$license = get_option ( 'usam_license' );
	switch ( $license['status'] ) 
	{
		case 1 :
			$message = __( 'активна', 'usam' );				
		break;
		case 2 :
			$message = __( 'заблокирована', 'usam' );	
		break;
		default:
			$message = __( 'не активна', 'usam' );	
	}
	return $message;
}