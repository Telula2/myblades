<?php
// Получить объем
function usam_get_product_volume( $product_id = null )
{	
	if ( $product_id == null )
		$product_id = get_the_ID();
		
	$volume = 0;
	$product_meta = maybe_unserialize( usam_get_product_meta($product_id , 'product_metadata' ) );	
	if ( !empty($product_meta) && isset($product_meta['dimensions']) )
		$volume = $product_meta['dimensions']['width'] * $product_meta['dimensions']['height'] * $product_meta['dimensions']['length'];
	return $volume;	
}

// Узнать тип товара
function usam_get_product_type( $product_id ) 
{		
	$product_type_terms = get_the_terms( $product_id, 'usam-product_type' );		
	if ( !empty($product_type_terms) ) 
		$product_type = $product_type_terms[0]->slug;	
	else
	{
		wp_set_object_terms( $product_id, 'simple', 'usam-product_type' );
		$product_type = 'simple';
	}
	return $product_type;
}


function usam_get_product_price_currency( $product_id = null, $old_price = false ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();
		
	if ( $old_price )	
		$price = usam_get_product_old_price( $product_id );			
	else
		$price = usam_get_product_price( $product_id );	

	if ( $price == 0  )
		return '';
		
	$price = apply_filters( 'usam_do_convert_price', $price, $product_id );		
	$args = array(
		'display_as_html' => false,		
	);
	$price = usam_currency_display( $price, $args );	
	$product_type = usam_get_product_type( $product_id );	
	if ( $product_type == 'variable' ) 
	{
		$from_text = __( 'от %s', 'usam' );
		$from_text = apply_filters( 'usam_product_variation_text', $from_text );
		$price = sprintf( $from_text, $price );
	} 	
	return $price;
}

function usam_product_price_currency( $old_price = false ) 
{	
	$product_id = get_the_ID();	
	echo usam_get_product_price_currency( $product_id, $old_price );
}

/**
 * Получить цену товара
 */
function usam_get_product_price( $product_id = null, $code_price = null ) 
{
	global $type_price;	
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	if ( $code_price === null )	
		$code_price = $type_price;
	
	$price = (float)usam_get_product_meta( $product_id, 'price_'.$code_price );	
	return $price;
}

function usam_is_product_discount( $product_id = null, $code_price = null ) 
{
	if( usam_get_product_old_price( $product_id, $code_price ) != 0 )
		return true;
	else
		return false;
}

/**
 * Получить старую цену товара
 */
function usam_get_product_old_price( $product_id = null, $code_price = null ) 
{
	global $type_price;	

	if ( $product_id == null )
		$product_id = get_the_ID();
	
	if ( $code_price === null )	
		$code_price = $type_price;
	
	$price = (float)usam_get_product_meta( $product_id, 'old_price_'.$code_price );	
	return $price;
}


function usam_get_product_attachment_props( $attachment_id = null, $product = false ) 
{
	$props = array(
		'title'   => '',
		'caption' => '',
		'url'     => '',
		'alt'     => '',
		'src'     => '',
		'srcset'  => false,
		'sizes'   => false,
	);
	if ( $attachment = get_post( $attachment_id ) ) 
	{
		$props['title']   = trim( strip_tags( $attachment->post_title ) );
		$props['caption'] = trim( strip_tags( $attachment->post_excerpt ) );
		$props['url']     = wp_get_attachment_url( $attachment_id );
		$props['alt']     = trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) );

		// Large version.
		$src                 = wp_get_attachment_image_src( $attachment_id, 'full' );
		$props['full_src']   = $src[0];
		$props['full_src_w'] = $src[1];
		$props['full_src_h'] = $src[2];

		// Thumbnail version.
		$src                 = wp_get_attachment_image_src( $attachment_id, 'product-thumbnails' );
		$props['thumb_src']   = $src[0];
		$props['thumb_src_w'] = $src[1];
		$props['thumb_src_h'] = $src[2];

		// Image source.
		$src             = wp_get_attachment_image_src( $attachment_id, 'medium-single-product' );
		$props['src']    = $src[0];
		$props['src_w']  = $src[1];
		$props['src_h']  = $src[2];
		$props['srcset'] = wp_get_attachment_image_srcset( $attachment_id, 'medium-single-product' );
		$props['sizes']  = wp_get_attachment_image_sizes( $attachment_id, 'medium-single-product' );

		// Alt text fallbacks
		$props['alt'] = empty( $props['alt'] ) ? $props['caption']                                               : $props['alt'];
		$props['alt'] = empty( $props['alt'] ) ? trim( strip_tags( $attachment->post_title ) )                   : $props['alt'];
		$props['alt'] = empty( $props['alt'] ) && $product ? trim( strip_tags( get_the_title( $product->ID ) ) ) : $props['alt'];
	}
	return $props;
}

function usam_product_thumbnail( $product_id = null, $size = 'product-image', $title = '' ) 
{
	echo usam_get_product_thumbnail( $product_id, $size, $title );
}
/**
 * Получает html код миниатюры изображения товара
 */
function usam_get_product_thumbnail( $product_id = null, $size = false, $title = '' ) 
{
	if ( $product_id == null )
	{
		$product_id = get_the_ID();
		$title = get_the_title();		
	}
	$thumbnail = usam_the_product_thumbnail($product_id, $size );	
	if( $thumbnail )
		$out = '<img id ="thumb_product_'.$product_id.'" src="'.$thumbnail['src'].'" width="'.$thumbnail['width'].'" height="'.$thumbnail['height'].'" alt="'.$title.'"/>';
	else
	{
		if ( is_array($size) )
		{
			$width = $size[0];
			$height = $size[0];
		}
		elseif ( $size == 'product-image' )
		{
			$product_image = get_option( 'usam_product_image' );
			$width =  $product_image['width'];
			$height = $product_image['height'];			
		}		
		else
		{
			$width = 60;
			$height = 60;
		}
		$out = '<img id ="thumb_product_'.$product_id.'" alt="'.__("Нет изображения","usam").'" width="'.$width.'" height="'.$height.'" src="'.USAM_CORE_THEME_URL.'images/noimage.png" />';
	}
	return $out;
}

function usam_get_admin_product_thumbnail( $product_id  ) 
{
	return usam_get_product_thumbnail($product_id, 'manage-products' );	
}

/*
 * Создать размер изображение товара
 */
function usam_product_image( $attachment_id = 0, $width = null, $height = null ) 
{	
	if ( ( ( $width >= 10 ) && ( $height >= 10 ) ) && ( ( $width <= 1024 ) && ( $height <= 1024 ) ) )
		$intermediate_size = "usam-{$width}x{$height}";
	// Получить адрес изображения, если у нас есть достаточно информации	
	if ( ( $attachment_id > 0 ) && ( !empty( $intermediate_size ) ) ) 
	{ 
		//Получить всю необходимую информацию о вложениях
		$uploads    = wp_upload_dir();
		$image_meta = get_post_meta( $attachment_id, '' );
		$file_path  = get_attached_file( $attachment_id );

		// Clean up the meta array
		foreach ( $image_meta as $meta_name => $meta_value )
			$image_meta[$meta_name] = maybe_unserialize( array_pop( $meta_value ) );
			
		if ( isset( $image_meta['_wp_attachment_metadata']['sizes'] ) && (count( $image_meta['_wp_attachment_metadata']['sizes'] ) > 0) && ( isset( $image_meta['_wp_attachment_metadata']['sizes'][$intermediate_size] ) ) ) 
		{ // Определить, если у нас уже есть изображение такого размера		
			$intermediate_image_data = image_get_intermediate_size( $attachment_id, array($width, $height) );
			$image_url = $intermediate_image_data['url'];
		} 
		else			
			$image_url = home_url( "index.php?usam_action=scale_image&attachment_id={$attachment_id}&width=$width&height=$height" );		
	} 
	else // Не достаточно информации, так попытаться обратится к запасному варианту
	{		
		if ( !empty( $attachment_id ) )
			$image_url = home_url( "index.php?usam_action=scale_image&attachment_id={$attachment_id}&width=$width&height=$height" );
		else
			$image_url = false;	
	}	
	if(empty($image_url) && !empty($file_path))
	{
		$image_meta = get_post_meta( $attachment_id, '_wp_attached_file' );
		if ( ! empty( $image_meta ) )
			$image_url = $uploads['baseurl'].'/'.$image_meta[0];	
	}
    if( is_ssl() ) 
		str_replace('http://', 'https://', $image_url);
	
	return apply_filters( 'usam_product_image', $image_url );
}


/**
 * Получает миниатюрное изображение для продукта
 */
function usam_the_product_thumbnail( $product_id = null, $size = false ) 
{	
	$thumbnail = false;		
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$thumbnail_id = usam_the_product_thumbnail_id( $product_id );	
	if ( ! $thumbnail_id )	
	{ // Если нет миниатюрами не найдено для элемента, получить его post_parent
		$product = get_post( $product_id );
		if ( !empty($product->post_parent) )	
			$thumbnail_id = usam_the_product_thumbnail_id( $product->post_parent );
	}	
	if ( empty($thumbnail_id) )	
		return;	
	
	if ( $size === false )
	{
		if ( is_single() )
			$size = 'single';
		else
			$size = 'product-image';
	}	
	if ( in_the_loop() || is_admin() && $GLOBALS['wp_query'] && !empty($GLOBALS['wp_query']->posts) )
	{		
		update_post_thumbnail_cache( ); 
	}	
	if ( is_array($size) && $size[0] && $size[1] )
	{		
		$width = $size[0];
		$height = $size[1];
		
		// вычисления высоты на основе соотношения первоначальных размеров
		if ( $height == 0 || $width == 0 )
		{
			$attachment_meta = get_post_meta( $thumbnail_id,'_wp_attachment_metadata', false );
			$original_width = $attachment_meta[0]['width'];
			$original_height = $attachment_meta[0]['height'];
			if( $width != 0 )
			{
				$height = ( $original_height / $original_width ) * $width;
				$height = round( $height, 0 );
			} 
			elseif ( $height != 0 ) 
			{
				$width = ( $original_width / $original_height ) * $height;
				$width = round( $width, 0 );
			}
		}		
		$src = usam_product_image( $thumbnail_id, $width, $height );
		$thumbnail = array( 'src' => $src, 'width' => $width, 'height' => $height );
	}	
	elseif( $size == 'single' ) 
	{
		$current_size = image_get_intermediate_size( $thumbnail_id, 'medium-single-product' );		
		if ( ! $current_size )
			_usam_regenerate_thumbnail_size( $thumbnail_id, 'medium-single-product' );			
		$src = wp_get_attachment_image_src( $thumbnail_id, 'medium-single-product' );
		$thumbnail = array( 'src' => $src[0], 'width' => $src[1], 'height' => $src[2] );
	} 	
	elseif ( $size == 'manage-products' )
	{
		$current_size = image_get_intermediate_size( $thumbnail_id, 'admin-product-thumbnails' );		
		if ( ! $current_size )
			_usam_regenerate_thumbnail_size( $thumbnail_id, 'admin-product-thumbnails' );			
		$src = wp_get_attachment_image_src( $thumbnail_id, 'admin-product-thumbnails' );
		$thumbnail = array( 'src' => $src[0], 'width' => $src[1], 'height' => $src[2] );
	}
	elseif ( $size == 'product-image' )
	{
		$product_image = get_option( 'usam_product_image' );
		$width =  $product_image['width'];
		$height = $product_image['height'];	
		
		$src = usam_product_image( $thumbnail_id, $width, $height );		
		$thumbnail = array( 'src' => $src, 'width' => $width, 'height' => $height );
	}	
	if ( $thumbnail )
	{
		if ( is_ssl() )
			$thumbnail['src'] = str_replace( 'http://', 'https://', $thumbnail['src'] );		
			
		return $thumbnail; 
	}
	else 
		return false;
}

function usam_get_product_thumbnail_src( $product_id = 0, $size = false ) 
{
	$thumbnail = usam_the_product_thumbnail($product_id, $size );	
	if ( empty($thumbnail['src']) )
		$image = USAM_CORE_IMAGES_URL . '/noimage.png';	
	else
		$image = $thumbnail['src'];		
	return $image;
}

/**
 * Получить ID миниатюры товара
 */
function usam_the_product_thumbnail_id( $product_id ) 
{
	$thumbnail_id = null;
	if ( has_post_thumbnail( $product_id ) ) 
	{
		$thumbnail_id = get_post_thumbnail_id( $product_id  );
	} 
	else 
	{		
		$attached_images = (array) get_posts( array('post_type' => 'attachment', 'numberposts' => 1, 'post_status' => null, 'post_parent' => $product_id, 'order' => 'ASC') );
		if ( ! empty( $attached_images ) )
			$thumbnail_id = $attached_images[0]->ID;
	}
	return $thumbnail_id;
}

// Получить фотографии товара
function usam_get_product_images( $product_id )
{
	$attachments = (array)get_posts( array(	'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $product_id, 'orderby' => 'menu_order', 'order' => 'ASC' ) );	
	if ( ! $attachments )	
	{ // Если нет миниатюрами не найдено для элемента, получить его post_parent
		$product = get_post( $product_id );
		if ( $product->post_parent )
		{
			$product_id = $product->post_parent;
			$attachments = (array)get_posts( array(	'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $product_id, 'orderby' => 'menu_order', 'order' => 'ASC' ) );	
		}
	}
	return $attachments;
}


/**
* Восстановить размер миниатюры в случае если она отсутствует.
* @since  3.8.9
* @access private
*/
function _usam_regenerate_thumbnail_size( $thumbnail_id, $size ) 
{
	// regenerate size metadata in case it's missing
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	if ( ! $metadata = wp_get_attachment_metadata( $thumbnail_id ) )
		$metadata = array();
	if ( empty( $metadata['sizes'] ) )
		$metadata['sizes'] = array();

	$file = get_attached_file( $thumbnail_id );
	$generated = wp_generate_attachment_metadata( $thumbnail_id, $file );

	if ( empty( $generated ) )
		return false;
	$metadata['sizes'] = array_merge( $metadata['sizes'], $generated['sizes'] );
	wp_update_attachment_metadata( $thumbnail_id, $metadata );
	return true;
}

/*
 * Получает URL продукта
 */
function usam_product_url( $product_id = null )
{
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$post = get_post( $product_id );
	if ( isset($post->post_parent) && $post->post_parent > 0) 
		return get_permalink($post->post_parent);
	else 
		return get_permalink($product_id);	
}

/**
 * Проверка, имеет ли продукт вариации или нет.
 * @since  3.8.9
 */
function usam_product_has_variations( $id = 0 )
{
	static $has_variations = array();

	if ( ! $id )
		$id = get_the_ID();

	if ( ! isset($has_variations[$id]) ) 
	{
		$args = array(
			'post_parent' => $id,
			'post_type'   => 'usam-product',
			'numberposts' => 1,			
			'post_status' => array( 'inherit', 'publish' ),
		);
		$children = get_children( $args );
		$has_variations[$id] = !empty( $children );
	}
	return $has_variations[$id];
}
/**
 * Возвращает информацию о наличии запаса
 * @return boolean - true если продукт имеет запас или не используется на складе возвращает ложно, если нет - истину
 */
function usam_product_has_stock( $product_id = null ) 
{		
	if ( $product_id == null )
		$product_id = get_the_ID();	
	
	$stock = usam_get_product_meta( $product_id, 'stock' );
	if ( $stock > 0 )
		return true;
	else	
		return false;
}

// Вывести штрих-код
function usam_get_product_barcode( $product_id )
{
	$barcode = usam_get_product_meta( $product_id, 'barcode' );
	$out = '';		
	if ( $barcode != null )
	{				
		require_once( USAM_FILE_PATH . '/admin/includes/barcode/barcode.php' );
		$bars = barcode_encode($barcode, 'EAN');
	
		$barcode_html = barcode_outhtml($bars['text'], $bars['bars'], 1, 0, array( 'top'=>0,'bottom'=>0,'left'=>0,'right'=>0 ) );
		$out = $barcode_html.$barcode; 
	}	
	return $out;
}

// Получить Список единиц измерения
function usam_get_list_product_units( $reductions = false )
{	
	if ( $reductions )
		$units_title = array( 'meter'=> __('м','usam'), 'liter'=> __('л','usam'), 'gram'=> __('гр','usam'), 'Kilogram'=> __('кг','usam'), 'thing'=> __('шт','usam') );
	else
		$units_title = array( 'meter'=> __('Метр','usam'), 'liter'=> __('Литер','usam'), 'gram'=> __('Грамм','usam'), 'Kilogram'=> __('Килограмм','usam'), 'thing'=> __('Штука','usam') );
	
	return $units_title;
}


// Получить единицу измерения
function usam_get_product_units( $product_id, $reductions = true )
{
	$product_meta = usam_get_product_meta( $product_id, 'product_metadata' );
	$units_title = usam_get_list_product_units( $reductions );
	
	if ( isset($units_title[$product_meta['unit_measure']]) )
		return $units_title[$product_meta['unit_measure']];
	else
		return $units_title['thing'];
}


/**
 * Получить артикул товара
 */
function usam_product_sku( $id = null ) 
{
	if ( $id = null )		
		$id = get_the_ID();

	$product_sku = usam_get_product_meta( $id, 'sku', true );
	return esc_attr( $product_sku );
}


/**
 * Узнать дату создания товара
 */
function usam_product_creation_time( $format = null )
{
	global $usam_query;

	if ( $format == null )
		$format = "Y-m-d H:i:s";
	return mysql2date( $format, $usam_query->product['date_added'] );
}

// Изменить остаток
function usam_update_stock_product( $product_id, $storage_id, $stock )
{
	$storage = usam_get_storage( $storage_id );
	if ( empty($storage['meta_key']) )
		return false;
	$current_reserve = usam_get_product_meta( $product_id, $storage['meta_key'], true );
	if ( USAM_UNLIMITED_STOCK !== $current_reserve ) 
	{			
		$stock = $stock + $current_reserve;					
		usam_update_product_meta($product_id, $storage['meta_key'], $stock );	
		usam_recalculate_stock_product( $product_id );
	}		
}

// Пересчитать остатки у заданных товаров
function usam_recalculate_stock_products( $products_ids = array() )
{	
	if ( !empty($products_ids) )
		$args['post__in'] = $products_ids;
	
	$new_query['cache_results'] = false;	
	$new_query['update_post_meta_cache'] = false;		
	$new_query['update_post_term_cache'] = false;
	$new_query['fields'] = 'ids';
	$products = usam_get_products( $new_query );				
	usam_create_system_process( __("Пересчет остатков","usam" ), $new_query, 'recalculate_stock_products', count($products), 'recalculate_stock' );				
}

// Пересчитать остаток у заданного товара
function usam_recalculate_stock_product( $product_id )
{
	global $wpdb;
	
	$storages = usam_get_stores();	
	
	$product_meta = get_post_meta( $product_id );
	$stock = $all_stock = 0;
	foreach ( $storages as $storage)
	{
		$pmeta_key = USAM_META_PREFIX.$storage->meta_key;
		if ( !isset($product_meta[$pmeta_key][0]) )
			$product_meta[$pmeta_key][0] = 0;
		
		if ( $storage->shipping == 1 )
			$stock += (int)$product_meta[$pmeta_key][0];
		
		$all_stock += (int)$product_meta[$pmeta_key][0];
	}	
	$reserve = (int)usam_get_product_meta($product_id, 'reserve' );
		
	if ( $stock != 0 )
		$stock = $stock - $reserve;
	
	$stock_db = (int)usam_get_product_meta($product_id, 'stock' );
	if ( $stock_db != $stock )		
	{		
		$wpdb->update( $wpdb->postmeta, array( 'meta_value' => $stock ) ,array( 'meta_key' => '_usam_stock', 'post_id' => $product_id ),  array( '%d', '%s', '%d' ) );		
		do_action( 'usam_update_stock', $product_id, $stock, $stock_db );			
	}
	if ( $all_stock != 0 )
		$all_stock = $all_stock - $reserve;
	
	$all_stock_db = (int)usam_get_product_meta($product_id, 'storage' );
	if ( $all_stock_db != $all_stock )
	{		
		$wpdb->update( $wpdb->postmeta, array( 'meta_value' => $all_stock ) ,array( 'meta_key' => '_usam_storage', 'post_id' => $product_id ),  array( '%d', '%s', '%d' ) );	
		do_action( 'usam_update_stock', $product_id, $all_stock, $all_stock_db );		
	}
}

// Расчитать прибыль
function usam_calculate_profit( $product_id, $t_price ) 
{		
	$setting_price = usam_get_setting_price_by_code( $t_price );
	if ( $setting_price['type'] == 'P' )
		return false;
	
	$prices = usam_get_prices( array('type' => 'P') );	
	if ( empty($prices) )
		return false;
	
	$product_price = usam_get_product_price( $product_id, $t_price );	
	$profit = false;
	foreach ( $prices as $price)
	{
		$purchasing_price = usam_get_product_price( $product_id, $price['code'] );	
		if ( $purchasing_price )
		{
			$profit = $product_price - $purchasing_price;
			break;
		}
	}
	return $profit;
}

function usam_sanitize_meta_key( $key )
{
	return preg_replace( '|[^a-z0-9_]|i', '', $key );
}

/**
 * функция меты продуктов
 */
function usam_add_product_meta($product_id, $key, $value, $unique = true ) 
{ // $unique = true - Произвольное поле уникально. Создавать только одно
	$key = USAM_META_PREFIX.$key;	
	return add_post_meta($product_id, $key, $value, $unique);
}

function usam_delete_product_meta($product_id, $key, $value = '') 
{
	$key = USAM_META_PREFIX.$key;
	return delete_post_meta($product_id, $key, $value );
}

function usam_get_product_meta($product_id, $key = '', $single = true) 
{
	$meta_key = !empty($key)?USAM_META_PREFIX.$key:'';		
	$result = get_metadata('post', $product_id, $meta_key, $single);
	switch ( $key ) 
	{
		case 'rating' :
		case 'rating_count' :
		case 'reserve' :
			$result = (int)$result;
		break;
	}			
	return $result;
}

function usam_delete_products_meta( $meta_key ) 
{
	global $wpdb;
	
	$meta_key = USAM_META_PREFIX.$meta_key;	
	$result = $wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key = '$meta_key'" );	
}

function usam_get_product_id_by_sku( $sku ) 
{
	global $wpdb;
	$key = USAM_META_PREFIX.'sku';
	
	$product = $wpdb->get_results( "SELECT p.ID FROM `$wpdb->posts` AS p			
			INNER JOIN `$wpdb->postmeta` AS pm ON (p.ID = pm.post_id AND pm.meta_key = '$key' AND pm.meta_value = '$sku')
			WHERE  p.post_status IN ('publish', 'pending', 'draft', 'future', 'private', 'trash') AND p.post_type='usam-product'");
	if ( isset($product[0]->ID) )	
		$product_id = $product[0]->ID;	
	else
		$product_id = false;
	return $product_id;
}

function usam_update_product_meta($product_id, $key, $value, $prev_value = '') 
{	
	$usam_key = USAM_META_PREFIX.$key;	
	if ( $the_post = wp_is_post_revision($product_id) )
		$product_id = $the_post;
	$result = update_metadata('post', $product_id, $usam_key, $value, $prev_value);
	
	return $result;
}

function update_product_metadata( $product_id, $key, $value ) 
{	
	$product_meta = usam_get_product_meta( $product_id, 'product_metadata' );
	$product_meta[$key] = $value;
	$result = usam_update_product_meta( $product_id, 'product_metadata', $product_meta );		
}

function get_product_custom( $product_id )
{
	$product_data = get_post_custom( $product_id );
	$product_meta = array();
	foreach ( $product_data as $key => $meta ) 
		$product_meta[$key] = maybe_unserialize( $product_data[$key][0] );	
	return $product_meta;
}

// Получить типы цен
function usam_get_prices( $args = array() )
{
	$option = get_option('usam_type_prices');
	$prices = maybe_unserialize( $option );
	if ( empty($prices) ) 
		return array();

	if ( isset($args['type']) && $args['type'] == 'all' )
		unset($args['type']);

	if ( !empty($args) )
	{
		$results = array();
		foreach( $prices as $setting_price )
		{							
			if ( isset($args['ids']) && !in_array($setting_price['id'], $args['ids']) )
				continue;
			
			if ( isset($args['available']) && (!isset($setting_price['available']) || $args['available'] != $setting_price['available']) )
				continue;
			
			if ( isset($args['type']) && $args['type'] != $setting_price['type'] )
				continue;
			
			if ( isset($args['code']) && $args['code'] != $setting_price['code'] )
				continue;
			
			if ( isset($args['currency']) && $args['currency'] != $setting_price['currency'] )
				continue;
			
			if ( isset($args['base_type']) && $setting_price['type'] == 'R' && $args['base_type'] != $setting_price['base_type'] )
				continue;
			
			$results[] = $setting_price;		
		}
	}
	else
		$results = $prices;
	if ( isset($args['orderby']) )	
		$orderby = $args['orderby'];
	else
		$orderby = 'sort';
	
	$order = isset($args['order'])&&$args['order']=='ASC'?'ASC':'DESC';	
	
	$comparison = new USAM_Comparison_Array( $orderby, $order );
	usort( $results, array( $comparison, 'compare' ) );		
	return $results;
}

// Получить имя цены по коду
function usam_get_name_price_by_code( $code_price )
{	
	$prices = usam_get_prices( array( 'code' => $code_price ) );	
	$name = '';	
	if ( isset($prices[0]) )
		$name = $prices[0]['title'];
	return $name;
}

// Получить валюту цены по коду
function usam_get_currency_price_by_code( $code_price = null )
{	
	global $type_price;
	if ( $code_price === null )
		$code_price = $type_price;
	
	$prices = usam_get_prices( array( 'code' => $code_price ) );	
	$currency = '';	
	if ( isset($prices[0]) )
		$currency = $prices[0]['currency'];
	return $currency;
}

function usam_get_setting_price_by_code( $price_code = null )
{	
	global $type_price;
	if ( $price_code === null )
		$price_code = $type_price;
	
	$prices = usam_get_prices( array( 'code' => $price_code ) );
	$result = array();
	if ( isset($prices[0]) )
		$result = $prices[0];
	
	return $result;
}

add_filter( 'post_type_link', 'usam_product_link', 10, 3 );
/* Создает ссылку на товар. Используется при обновлении товара и при получении ссылки на товар*/
function usam_product_link( $permalink, $post, $leavename )
{
	global $wp_query, $usam_query, $wp_current_filter;

	if ($post->post_type != 'usam-product')
		return $permalink;
	
	$permalink_structure = get_option('permalink_structure');
	if ( empty($permalink_structure) )
		return $permalink;
	
	$permalink_structure = get_option( 'usam_permalinks' );		
	$product_permalink = empty( $permalinks['product_base'] ) ? '' : trim($permalinks['product_base']);	
	if ( $product_permalink != '' || $permalink_structure['product_base'] != 'products-list' || $permalink_structure['product_base'] != 'products' ) 
	{
		$product_categories = get_the_terms( $post->ID, 'usam-category' );
		$count_categories = count( $product_categories );	
		if ( $count_categories == 0 ) 
			$category_slug = 'uncategorized';
		elseif ( $count_categories > 1 ) 
		{   // Если продукт связан с несколькими категориями, определить, какие из них выбрать
			$product_category_slugs = array( );
			foreach ( (array)$product_categories as $product_category )		
				$product_category_slugs[] = $product_category->slug;
			reset($product_categories);					
			if ( (isset($wp_query->query_vars['products']) && $wp_query->query_vars['products'] != null) && in_array($wp_query->query_vars['products'], $product_category_slugs) )
				$product_category = $wp_query->query_vars['products'];
			else 
			{
				$link = current($product_categories)->slug;					
				if ( ! in_array( 'wp_head', $wp_current_filter) && isset( $usam_query->query_vars['usam-category'] ) ) 
				{
					$current_cat = $usam_query->query_vars['usam-category'];
					if ( in_array( $current_cat, $product_category_slugs ) )
						$link = $current_cat;
				}
				$product_category = $link;
			}
			$category_slug = $product_category;		
		} 
		else 
		{	// Если продукт связан только с одной категории, у нас есть только один выбор
			if ( empty($product_categories) || !isset(current($product_categories)->slug) )
				$category_slug = null;	
			else
				$category_slug = current($product_categories)->slug;			
		}		
		if ( get_option( 'usam_category_hierarchical_url', 0 ) )
		{ 
			$selected_term = get_term_by( 'slug', $category_slug, 'usam-category' );
			if ( is_object( $selected_term ) ) 
			{
				$term_chain = array( $selected_term->slug );
				while ( $selected_term->parent ) 
				{
					$selected_term = get_term( $selected_term->parent, 'usam-category' );
					array_unshift( $term_chain, $selected_term->slug );
				}			
				$category_slug = implode( '/', $term_chain );
			}
		}
		if( isset( $category_slug ) && empty( $category_slug ) )
			$category_slug = 'product';

		$category_slug = apply_filters( 'usam_product_permalink_cat_slug', $category_slug, $post->ID );
		$rewritereplace = array($category_slug,	$post->post_name);
		$rewritecode = array( '%product_cat%', $leavename ? '' : '%postname%', );		
		$permalinks = get_option( 'usam_permalinks' );		
		$our_permalink_structure = $permalinks['product_base']."/%postname%/";
		$permalink = str_replace( $rewritecode, $rewritereplace, $our_permalink_structure );
		$permalink = user_trailingslashit( $permalink, 'single' );	
		$permalink = home_url( $permalink );		
	}
	return apply_filters( 'usam_product_permalink', $permalink, $post->ID );
}

// Процесс дублирования товаров
function usam_duplicate_product_process( $post, $new_parent_id = false ) 
{
	global $wpdb;
	$new_post_date = $post->post_date;
	$new_post_date_gmt = get_gmt_from_date( $new_post_date );

	$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = %d", $post->ID  ) );				
	if ( count( $post_meta_infos ) ) 
	{									
		$meta = array();
		$product_attributes = array();
		foreach ( $post_meta_infos as $meta_info )
		{						
			$meta_key = $meta_info->meta_key;
			$meta_value = addslashes( $meta_info->meta_value );						
			switch( $meta_key )
			{
				case '_edit_lock':		
				case '_edit_last':		
					$save_action = false;
				break;	
				case '_thumbnail_id':									
					$thumbnail_id = $meta_value;								
					$save_action = false;
				break;
				case '_usam_reserve':	
				case '_usam_product_views':					
					$meta_value = 0;
					$save_action = true;
				break;																
				case '_usam_product_metadata':
					$meta_value = maybe_unserialize( $meta_info->meta_value );								
					$save_action = true;
				break;
				default:
					if ( stripos($meta_key, '_usam_product_attributes_') !== false) 
					{
						$id = str_replace("_usam_product_attributes_", "", $meta_key);
						$term = get_term_by('name', $terms[$id], 'usam-product_attributes');	
						if ( !empty($term) )
						{ 	
							$product_attributes[$term->term_id] = $meta_info->meta_value;
						}
						$save_action = false;
					}															
					elseif ( stripos($meta_key, '_usam_filter_') !== false) 
						$save_action = true;
					elseif ( stripos($meta_key, '_usam_price_') !== false) 
						$save_action = true;
					elseif ( stripos($meta_key, '_usam_underprice_') !== false) 
						$save_action = true;
					elseif ( stripos($meta_key, '_usam_old_price_') !== false) 
						$save_action = true;
					elseif ( stripos($meta_key, '_usam_') !== false) 
						$save_action = true;
					else
						$save_action = false;
				break;
			}						
			if ( $save_action )
			{
				$meta_key = str_replace("_usam_", "", $meta_key);
				$meta[$meta_key] = $meta_value;
			}
		}					
	}	
	$new_post_type = $post->post_type;
	$post_content = str_replace( "'", "''", $post->post_content );
	$post_content_filtered = str_replace( "'", "''", $post->post_content_filtered );
	$post_excerpt = str_replace( "'", "''", $post->post_excerpt );
	$post_title = str_replace( "'", "''", $post->post_title ) . " (Duplicate)";
	$post_name = str_replace( "'", "''", $post->post_name );
	$comment_status = str_replace( "'", "''", $post->comment_status );
	$ping_status = str_replace( "'", "''", $post->ping_status );

	$product = array(
		'post_status'           => $post->post_status,
		'post_type'             => $new_post_type,
		'ping_status'           => $ping_status,
		'post_parent'           => $new_parent_id ? $new_parent_id : $post->post_parent,
		'menu_order'            => $post->menu_order,
		'to_ping'               =>  $post->to_ping,
		'pinged'                => $post->pinged,
		'post_excerpt'          => $post_excerpt,
		'post_title'            => $post_title,
		'post_content'          => $post_content,
		'post_content_filtered' => $post_content_filtered,
		'post_mime_type'        => $post->post_mime_type,
		'meta'                  => $meta,
		);

	if ( 'attachment' == $post->post_type )
		$product['guid'] = $post->guid;
	
	$_product = new USAM_Product( $product );		
	$new_post_id = $_product->insert_product();
	$_product->calculate_product_attributes( $product_attributes );	
	
	usam_duplicate_taxonomies( $post->ID, $new_post_id, $post->post_type );// Копировать taxonomies	
//	usam_duplicate_children( $post->ID, $new_post_id );// Ноходит дочерние записи (в том числе файлы продуктов и изображения продуктов), их значения мета, и дублирует их.
	return $new_post_id;
}

/**
 * Скопируйте таксономии поста в другой пост
 */
function usam_duplicate_taxonomies( $id, $new_id, $post_type )
{
	$taxonomies = get_object_taxonomies( $post_type ); //array("category", "post_tag");
	foreach ( $taxonomies as $taxonomy ) 
	{
		$post_terms = wp_get_object_terms( $id, $taxonomy );
		for ( $i = 0; $i < count( $post_terms ); $i++ ) {
			wp_set_object_terms( $new_id, $post_terms[$i]->slug, $taxonomy, true );
		}
	}
}

/**
 * Копирует произвольные поля в дублирующий пост
 */
function usam_duplicate_product_meta( $id, $new_id ) 
{
	global $wpdb;

	$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = %d", $id ) );
	if ( count( $post_meta_infos ) ) 
	{
		$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) VALUES ";
		$values = array();
		foreach ( $post_meta_infos as $meta_info ) 
		{
			$meta_key = $meta_info->meta_key;
			$meta_value = addslashes( $meta_info->meta_value );

			$sql_query_sel[] = "( %d, %s, %s )";
			$values[] = $new_id;
			$values[] = $meta_key;
			$values[] = $meta_value;
			$values += array( $new_id, $meta_key, $meta_value );
		}
		$sql_query.= implode( ",", $sql_query_sel );			
		$sql_query = $wpdb->prepare( $sql_query, $values );
		$wpdb->query( $sql_query );
	}
}
/**
 * Дубликаты дети продукта и дети мета
 */
function usam_duplicate_children( $old_parent_id, $new_parent_id ) 
{
	//Get children products and duplicate them
	$child_posts = get_posts( array(
		'post_parent' => $old_parent_id,
		'post_type'   => 'any',
		'post_status' => 'any',
		'numberposts' => -1,
		'order'       => 'ASC',
	) );
	foreach ( $child_posts as $child_post )
	    usam_duplicate_product_process( $child_post, $new_parent_id );
}

/**
 * Связанные товары, например, аксессуары к товару
 * $list 10 символов
 */
function usam_insert_associated_products( $product_id, $product_ids, $list )
{
	global $wpdb;	
		
	if ( !empty($product_ids) && !empty($list)  )
	{
		foreach ( $product_ids as $id ) 
		{
			$sql = "INSERT INTO `".USAM_TABLE_ASSOCIATED_PRODUCTS."` (`product_id`,`associated_id`,`list` ) VALUES ('%d','%d','%s') ON DUPLICATE KEY UPDATE `list`='%s'";
			$insert = $wpdb->query( $wpdb->prepare($sql, $product_id, $id, $list, $list ));	
		}
	}
}

function usam_delete_associated_products( $product_id, $list )
{
	global $wpdb;	
		
	$result = false;
	if ( !empty($product_id) && !empty($list)  )
	{
		$result = $wpdb->query( "DELETE FROM ".USAM_TABLE_ASSOCIATED_PRODUCTS." WHERE product_id='$product_id' AND product_id='$list'" );	
	}
	return $result;
}

function usam_get_associated_products( $product_id, $list, $limit = 40 )
{
	global $wpdb;	
		
	$results = array();
	if ( !empty($product_id) && !empty($list)  )
	{
		$limit = is_numeric($limit)?$limit:20;
		if ( $limit ) 
			$_limit = "LIMIT $limit";
		else
			$_limit = "";
		
		$results = $wpdb->get_col( "SELECT DISTINCT associated_id FROM ".USAM_TABLE_ASSOCIATED_PRODUCTS." WHERE product_id='$product_id' AND list='$list' $_limit" );
	}
	return $results;
}


function usam_insert_product_attribute( $insert )
{
	global $wpdb;	
	
	$attributes = usam_get_product_attribute_values( array( 'attribute_id' => $insert['attribute_id'], 'value' => $insert['value']) );	
	if ( !empty($attributes) )
		return false;
	
	$formats_db = array( 'value' => '%s', 'attribute_id' => '%d', 'sort' => '%d', 'code' => '%s' );
	$formats = array();
	foreach ( $insert as $key => $value ) 
	{	
		if ( isset($formats_db[$key]) )		
			$formats[$key] = $formats_db[$key];		
	}		
	$result = $wpdb->insert( USAM_TABLE_PRODUCT_ATTRIBUTES, $insert, $formats );
	return $result;
}

function usam_update_product_attribute( $id, $update )
{
	global $wpdb;	
	
	$where  = array( 'id' => $id );
	
	$formats_db = array( 'value' => '%s', 'sort' => '%d', 'code' => '%s' );
	$formats = array();
	foreach ( $update as $key => $value ) 
	{	
		if ( isset($formats_db[$key]) )		
			$formats[$key] = $formats_db[$key];		
	}
	$result = $wpdb->update( USAM_TABLE_PRODUCT_ATTRIBUTES, $update, $where, $formats, array('%d') );
	return $result;
}

function usam_get_attributes( $product_id )
{
	global $wpdb;
	$category_ids = usam_get_product_term_ids( $product_id );
	$r_attribute_ids = $wpdb->get_results( "SELECT term_id1, term_id2 FROM ".USAM_TABLE_TAXONOMY_RELATIONSHIPS." WHERE taxonomy = 'usam-category'" );	
	$attribute_ids = array();
	foreach( $r_attribute_ids as $term )
	{
		$attribute_ids[$term->term_id1][] = $term->term_id2;
	}		
	$args = array( 'hide_empty' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sort_order' );
	$product_attributes = get_terms('usam-product_attributes', $args);			

	$return_attributes = array();	
	foreach( $product_attributes as $attr )
	{		
		if ( !isset($attribute_ids[$attr->term_id]) ) 
		{
			$return_attributes[] = $attr;
		}
		else 
		{
			$result = array_intersect($attribute_ids[$attr->term_id], $category_ids);
			if ( !empty($result) )
			{			
				$return_attributes[] = $attr;
			}
		}
	}
	return $return_attributes;
}

function usam_delete_product_attribute( $id, $colum = 'id' )
{
	global $wpdb;	
	
	$where  = array( $colum => $id );	
	$result = $wpdb->delete( USAM_TABLE_PRODUCT_ATTRIBUTES, $where, array('%d') );
	return $result;
}



function usam_get_product_attribute_values( $args )
{
	global $wpdb;
	
	$where = array();
	if ( isset($args['attribute_id__in']) )
	{
		if ( is_numeric($args['attribute_id__in']) )
			$args['attribute_id__in'] = array($args['attribute_id__in']);
		
		$where[] = "attribute_id IN ('".implode("','",$args['attribute_id__in'])."')";
	}	
	if ( isset($args['attribute_id__not_in']) )
	{
		if ( is_numeric($args['attribute_id__not_in']) )
			$args['attribute_id__not_in'] = array($args['attribute_id__not_in']);
		$where[] = "attribute_id NOT IN ('".implode("','",$args['attribute_id__not_in'])."')";
	}	
	if ( !empty($args['attribute_id']) )
	{
		$where[] = "attribute_id = '".$args['attribute_id']."'";
	}
	if ( !empty($args['value']) )
	{
		$where[] = "value = '".$args['value']."'";
	}
	if ( !empty($args['code']) )
	{
		$where[] = "code = '".$args['code']."'";
	}	
	if ( isset($args['id__in']) )
	{
		$where[] = "id IN ('".implode("','",$args['id__in'])."')";
	}	
	if ( isset($args['id__not_in']) )
	{
		$where[] = "id NOT IN ('".implode("','",$args['id__not_in'])."')";
	}	
	if ( !empty($args['id']) )
	{
		$where[] = "id = '".$args['id']."'";
	}	
	if ( !empty($args['order']) )
	{
		$order = $args['order'];
	}	
	else
		$order = 'asc';
	if ( !empty($args['orderby']) )
	{
		$orderby = $args['orderby'];
	}	
	else
		$orderby = 'sort';
	$where_str = implode( ' AND ', $where );
	
	$sql = "SELECT * FROM `".USAM_TABLE_PRODUCT_ATTRIBUTES."` WHERE {$where_str} ORDER BY {$orderby} {$order}";		
	$result = $wpdb->get_results( $sql );
	return $result;
}

function usam_get_product_filters( $product_id )
{
	$product_meta = usam_get_product_meta( $product_id );	
	$product_filters = array();
	foreach( $product_meta as $meta_key => $meta )
	{	
		foreach( $meta as $meta_value )
		{
			$explode = explode('_usam_filter_',$meta_key);		
			if ( !empty($explode[1]) )
			{	
				if ( count($meta) > 1 )
					$product_filters[$explode[1]][$meta_value] = $meta_value;
				else
					$product_filters[$explode[1]][] = $meta_value;
			}
		}		
	}	
	return $product_filters;
}


function usam_get_product_attributes( $product_id )
{	
	$product_meta = usam_get_product_meta( $product_id );	
	$product_attributes = array();
	foreach( $product_meta as $meta_key => $meta )
	{	
		foreach( $meta as $meta_value )
		{
			$explode = explode('_usam_product_attributes_',$meta_key);				
			if ( !empty($explode[1]) )
			{	
				if ( count($meta) > 1 )
					$product_attributes[$explode[1]][$meta_value] = $meta_value;
				else
					$product_attributes[$explode[1]][] = $meta_value;
			}
		}		
	}	
	return $product_attributes;
}


function usam_get_product_search( $product_id )
{
	$product_meta = usam_get_product_meta( $product_id );	
	$product_search = array();
	foreach( $product_meta as $meta_key => $meta )
	{	
		foreach( $meta as $meta_value )
		{		
			if ( $meta_key == '_usam_search' )
			{					
				$product_search[] = $meta_value;
			}
		}		
	}	
	return $product_search;
}

function usam_get_product_attributes_display( $product_id )
{	
	$attribute_value = usam_get_product_attributes( $product_id );
	$attribute_ids = array_keys( $attribute_value );	
	$ready_options = array( );
	if ( !empty($attribute_ids) )
	{
		$args = array( 'attribute_id__in' => $attribute_ids );
		$ready_options_attributes = usam_get_product_attribute_values( $args );				
		foreach( $ready_options_attributes as $option )
		{
			$ready_options[$option->attribute_id][$option->id] = $option->value;
		}
	}	
	$product_attributes = array();	
	foreach( $attribute_value as $attribute_id => $attribute )
	{
		$do_not_show_in_features = get_term_meta($attribute_id, 'usam_do_not_show_in_features', true);
		if ( !empty($do_not_show_in_features) )
			continue;
		$type = get_term_meta($attribute_id, 'usam_type_attribute', true);			
		
		$value = '';		
		switch ( $type ) 
		{
			case 'C' ://Флажок один	
				$value = array();
				foreach( $attribute as $attr )
				{
					if ( !empty($attr) )			
						$value[] = __('Да','usam');				
					else
						$value[] = __('Нет','usam');					
				}					
			break;		
			case 'S' :			
			case 'N' : 
				$value = array();				
				foreach( $attribute as $attr )
				{					
					if ( isset($ready_options[$attribute_id][$attr]) )					
						$value[] = $ready_options[$attribute_id][$attr];
					elseif ( !is_numeric($attr) )
						$value[] = $attr;
				}						
			break;			
			case 'M' :	//Флажок несколько		
				$attribute = maybe_unserialize($attribute);				
				$value = array();				
				foreach( $attribute as $attr )
				{					
					if ( isset($ready_options[$attribute_id][$attr]) )					
						$value[] = $ready_options[$attribute_id][$attr];
				}							
			break;
			default:
				$value = $attribute;
			break;
		}
	//	$term = get_term_by( 'id', $attribute_id, 'usam-product_attributes' );	
		$product_attributes[$attribute_id] = $value;
	}		
	return $product_attributes;
}


// Получить значения фильтров товаров
function usam_get_product_filters_value( $args )
{
	global $wpdb;	
	$where = array( '1=1' );	
	if ( isset($args['id']) )
	{
		if ( !is_array($args['id']) )	
			$ids = array( $args['id'] );	
		else 
			$ids = $args['id'];
		
		$ids = array_map( 'intval', $ids );		
		$where[] = "id IN ('".implode("','",$ids)."')";
	}
	if ( isset($args['attribute_id']) )
	{
		if ( !is_array($args['attribute_id']) )	
			$attribute_ids = array( $args['attribute_id'] );
		else 
			$attribute_ids = $args['attribute_id'];
		
		$attribute_ids = array_map( 'intval', $attribute_ids );		
		$where[] = "attribute_id IN ('".implode("','",$attribute_ids)."')";
	}
	if ( isset($args['value']) )
	{
		if ( !is_array($args['value']) )	
			$value = array( $args['value'] );
		else 
			$value = $args['value'];
				
		$where[] = "value IN ('".implode("','",$value)."')";
	}
	$_where = implode( ' AND ', $where );		
	$sql = "SELECT * FROM `".USAM_TABLE_PRODUCT_FILTER."` WHERE $_where ORDER BY attribute_id, value";		

	$result = $wpdb->get_results( $sql );
	return $result;
}

// Получить строку фильтра из базы данных
function usam_get_product_filter( $args )
{
	global $wpdb;	
	$where = array( '1=1' );	
	if ( isset($args['id']) )
	{
		if ( !is_array($args['id']) )	
			$ids = array( $args['id'] );	
		else 
			$ids = $args['attribute_id'];
		
		$ids = array_map( 'intval', $ids );		
		$where[] = "id IN ('".implode("','",$ids)."')";
	}
	if ( isset($args['attribute_id']) )
	{
		if ( !is_array($args['attribute_id']) )	
			$attribute_ids = array( $args['attribute_id'] );
		else 
			$attribute_ids = $args['attribute_id'];
		
		$attribute_ids = array_map( 'intval', $attribute_ids );		
		$where[] = "attribute_id IN ('".implode("','",$attribute_ids)."')";
	}
	if ( isset($args['value']) )
	{
		if ( !is_array($args['value']) )	
			$value = array( $args['value'] );
		else 
			$value = $args['value'];
				
		$where[] = "value IN ('".implode("','",$value)."')";
	}
	$_where = implode( ' AND ', $where );		
	$sql = "SELECT * FROM `".USAM_TABLE_PRODUCT_FILTER."` WHERE $_where";		
	$result = $wpdb->get_row( $sql, ARRAY_A );
	return $result;
}

// Добавить фильтр товаров
function usam_insert_product_filter( $args )
{
	global $wpdb;	
	if ( isset($args['id']) )
		unset( $args['id'] );
		
	if ( empty($args['attribute_id']) || empty($args['value']) )
		return false;
		
	$id = $wpdb->get_var( $wpdb->prepare("SELECT id FROM `".USAM_TABLE_PRODUCT_FILTER."` WHERE attribute_id = '%d' AND value = '%s' LIMIT 1",$args['attribute_id'], $args['value'] ) );	
	if ( !empty($id) )
		return $id;	
	
	$formats = array( 'id' => '%d', 'attribute_id' => '%d', 'value' => '%s' );
	$format = array();
	foreach( $args as $key => $value) 		
	{
		if ( isset($formats[$key]) )
			$format[] = $formats[$key];
		else
			unset($args[$key]);
	}	
			
	$result = $wpdb->insert( USAM_TABLE_PRODUCT_FILTER, $args, $format );
	return $wpdb->insert_id;
}

// Добавить фильтр товаров
function usam_update_product_filter( $where, $args )
{
	global $wpdb;	
	
	$formats = array( 'id' => '%d', 'attribute_id' => '%d', 'value' => '%s' );
	$format = array();
	foreach( $args as $key => $value) 		
	{
		if ( isset($formats[$key]) )
			$format[] = $formats[$key];
		else
			unset($args[$key]);
	}	
	$where_format = array();
	foreach( $where as $key => $value) 		
	{
		if ( isset($formats[$key]) )
			$where_format[] = $formats[$key];
		else
			unset($where[$key]);
	}	
	$result = $wpdb->update( USAM_TABLE_PRODUCT_FILTER, $args, $where, $format, $where_format );	
	return $wpdb->insert_id;
}

// Удалить фильтр товаров
function usam_delete_product_filter( $args )
{
	global $wpdb;
	
	$formats = array( 'id' => '%d', 'attribute_id' => '%d', 'value' => '%s' );
	$format = array();
	foreach( $args as $key => $value) 				
		$format[] = $formats[$key];
		
	$result = $wpdb->delete( USAM_TABLE_PRODUCT_FILTER, $args, $format );	
	return $result;	
}

function usam_process_calculate_increase_sales_product( )
{
	$products = usam_get_products( array( 'post_status' => 'publish', 'fields' => 'ids','update_post_meta_cache' => false, 'update_post_term_cache' => false ) );
	$i = count($products);
	usam_create_system_process( __("Расчет товаров для увеличения продаж", "usam" ), array(), 'calculate_increase_sales_product', $i, 'calculate_increase_sales_product' );		
}

function usam_content_table_in_array( $content )
{
	$out = array();
	$html_no_attr = preg_replace("#(</?\w+)(?:\s(?:[^<>/]|/[^<>])*)?(/?>)#ui", '$1$2', $content ); // очистить от классов и стилей
	$content =	preg_replace('~(<(.*)[^<>]*>\s*<\\2>)+~i','',$html_no_attr );						// удалить пустые строки таблицы
	
	preg_match_all('#<td>(.+?)</td>#s', $content, $matches); 						
	$result = array_chunk($matches[1], 2);		
	foreach ( $result as $record )
	{	
		if ( empty($record[1]) || empty($record[0]) )
			continue;
		$out[] = array( 'name'  => mb_strtolower(trim(strip_tags($record[0]))),
						'value' => mb_strtolower(trim(strip_tags($record[1])))
					);						
	}
	return $out;
}

// Получить скидки товара
function usam_get_product_discount( $product_id ) 
{	
	$product_id = absint($product_id);		
	$discounts = wp_cache_get( $product_id, 'usam_product_discount' );			
	if ( $discounts === false )			
	{							
		global $wpdb;
		$discounts = $wpdb->get_results( "SELECT discount_id, code_price FROM ".USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS." WHERE product_id ='$product_id'" );	
		wp_cache_set( $product_id, $discounts, 'usam_product_discount' );						
	}	
	$result = array();
	foreach ( $discounts as $value )
	{
		$result[$value->code_price][] = $value->discount_id;
	}		
	return $result;	
}

function usam_get_product_discount_rules( $args = array() ) 
{	
	$option = get_option('usam_product_discount_rules', '');		
	$rules = maybe_unserialize( $option );
	if ( !empty($rules) )
	{
		if ( !empty($args['fields']) )
		{
			$discounts_rule = array();
			foreach ( $rules as $rule )		
			{
				if ( $args['fields'] == 'id=>data')
					$discounts_rule[$rule['id']] = $rule;
			}
		}
		return $discounts_rule;
	}
	return $rules;
}

// Получить скидки товара
function usam_get_products_discounts( $args ) 
{	
	global $wpdb;	
	$where = array('1=1');
	if ( !empty($args['code_price']) )
	{
		$code_price = is_array($args['code_price'])?$args['code_price']:array($args['code_price']);
		$where[] = "code_price IN ('".implode("','",$code_price)."')";
	}
	if ( !empty($args['product_id']) )
	{
		$product_id = is_array($args['product_id'])?$args['product_id']:array($args['product_id']);
		$where[] = "product_id IN ('".implode("','",$product_id)."')";
	}
	if ( !empty($args['discount_id']) )
	{
		$discount_id = is_array($args['discount_id'])?$args['discount_id']:array($args['discount_id']);
		$where[] = "discount_id IN ('".implode("','",$discount_id)."')";
	}	
	$discounts = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS." WHERE ".implode(" AND ",$where)." ORDER BY product_id" );	
	
	$result = array();
	foreach ( $discounts as $value )
	{
		$result[$value->product_id][] = $value;
	}		
	return $result;	
}

// Получить товары у которых установлена скидка
function usam_get_product_discount_ids( $discount_id ) 
{
	global $wpdb;	
	
	if ( is_array($discount_id) )
		$discont = array_map('intval', $discount_id);
	else	
	{
		$discont = array( absint($discount_id) );		
	}	 
	$result = $wpdb->get_col( "SELECT DISTINCT product_id FROM ".USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS." WHERE discount_id IN ('".implode("','",$discont)."') " );			
	return $result;
}



function usam_products_stock_updates( $products, $storage_id, $add = false ) 
{		
	$storage = usam_get_storage( $storage_id );
	$storage_meta_key = $storage['meta_key'];		
	
	$products_ids = array();			
		
	foreach( $products as $product) 
	{				
		$products_ids[] = $product->product_id;
		
		$current_storage_stock = usam_get_product_meta($product->product_id, $storage_meta_key );					
		if ( USAM_UNLIMITED_STOCK == $current_storage_stock ) 
			continue;				
		
		if( $add )
			$remaining_stock = $current_storage_stock + $product->quantity;			
		else
			$remaining_stock = $current_storage_stock - $product->quantity;			
		
		usam_update_product_meta($product->product_id, $storage_meta_key, $remaining_stock);					
	}
	if ( !empty($products_ids) )
		usam_recalculate_stock_products( $products_ids );	
}	

/**
 * Создание вариаций товара
 */
function usam_edit_product_variations( $product_id, $post_data ) 
{
	global $user_ID;
		
	remove_action( 'transition_post_status', '_usam_action_transition_post_status', 10, 3 );
	
	$product_type_object = get_post_type_object('usam-product');
	if (!current_user_can($product_type_object->cap->edit_post, $product_id))
		return;

	$parent = get_post_field( 'post_parent', $product_id );
	if( ! empty( $parent ) )
		return;

	require_once( USAM_FILE_PATH . '/includes/product/variation_combinator.class.php' );
		
	if (!isset($post_data['variations']))
		$post_data['variations'] = array();

	$variations = (array)$post_data['variations'];	
    $combinator = new USAM_Variation_Combinator( $variations ); // Создать массивы для вариации наборов
	$variation_sets = $combinator->return_variation_sets(); // Получить массив, содержащий изменение набор идентификаторов
	$variation_values = $combinator->return_variation_values(); // Извлечь массив, содержащий комбинации каждого варианта набора, связаны с этим продуктом.
	$combinations = $combinator->return_combinations();	

	$variation_sets_and_values = array_merge($variation_sets, $variation_values);
	$variation_sets_and_values = apply_filters('usam_edit_product_variation_sets_and_values', $variation_sets_and_values, $product_id);

	wp_set_object_terms( $product_id, $variation_sets_and_values, 'usam-variation');
	wp_set_object_terms( $product_id, 'variable', 'usam-product_type' );

	$_product = new USAM_Product( $product_id );		
	$child_product = $_product->get_product();	
	
	$child_product['post_author'] = $user_ID;
	$child_product['post_status'] = 'publish';
	$child_product['post_parent'] = $product_id;	
	$child_product['product_type'] = 'variation';
		
	$childs = usam_get_products( array( 'post_parent' => $product_id, 'post_status' => 'all', 'numberposts' => -1, 'update_post_meta_cache' => false, 'update_post_term_cache' => false, 'cache_results' => false) );

	$product_children = array();
	foreach( $combinations as $combination ) 
	{
		$term_names = array();
		$term_ids = array();
		$term_slugs = array();
		$product_values = $child_product;

		$combination_terms = get_terms('usam-variation', array('hide_empty' => 0, 'include' => implode(",", $combination), 'orderby' => 'parent' ));
		foreach($combination_terms as $term) 
		{
			$term_ids[] = $term->term_id;
			$term_slugs[] = $term->slug;
			$term_names[] = $term->name;
		}
		$product_values['post_title'] .= " (".implode(", ", $term_names).")";
		$product_values['post_name'] = sanitize_title($product_values['post_title']);		
		$selected_post = null;
		foreach($childs as $child) 
		{
			if ( $child->post_name == $product_values['post_name'] )
			{
				$selected_post = $child;
				break;
			}
		}		
		$child_product_id = usam_get_child_object_in_terms($product_id, $term_ids, 'usam-variation');	
		if( $child_product_id == false )
		{			
			if( $selected_post == null ) 
			{		
				$product_values = apply_filters( 'insert_child_product_meta', $product_values, $product_id );	
				$_product_v = new USAM_Product( $product_values );			
				$child_product_id = $_product_v->insert_product( );				
			}
		} 
		elseif( ($selected_post != null) && ($selected_post->ID != $child_product_id) ) 
		{
			$child_product_id = $selected_post->ID;			
		} 
		$product_children[] = $child_product_id;
		if( $child_product_id > 0) 		
			wp_set_object_terms($child_product_id, $term_slugs, 'usam-variation');			
	}			
	if(!empty($childs))
	{
		$childs_ids = wp_list_pluck( $childs, 'ID' );
		$old_ids_to_delete = array_diff($childs_ids, $product_children);
		$old_ids_to_delete = apply_filters('usam_edit_product_variations_deletion', $old_ids_to_delete);
		if(is_array($old_ids_to_delete) && !empty($old_ids_to_delete)) 
		{
			foreach($old_ids_to_delete as $object_ids) {
				wp_delete_post($object_ids);
			}
		}
	}
}


/**
 * Обновить условия вариации, присвоенные родительскому продукту, на основе изменений, которые он имеет. 
 */
function usam_refresh_parent_product_terms( $parent_id ) 
{
	$children = get_children( array( 'post_parent' => $parent_id, 'post_status' => array( 'publish', 'inherit' ), ) );
	$children_ids = wp_list_pluck( $children, 'ID' );

	$children_terms = wp_get_object_terms( $children_ids, 'usam-variation' );
	$new_terms = array();
	foreach ( $children_terms as $term ) 
	{
		if ( $term->parent )
			$new_terms[] = $term->parent;
	}
	$children_term_ids = wp_list_pluck( $children_terms, 'term_id' );
	$new_terms = array_merge( $new_terms, $children_term_ids );
	$new_terms = array_unique( $new_terms );
	$new_terms = array_map( 'absint', $new_terms );
	wp_set_object_terms( $parent_id, $new_terms, 'usam-variation' );
}


/**
 * Убедитесь в том, родительские продукты, назначенного термина обновления, когда статусы его вариации "меняются" 
 */
function _usam_action_transition_post_status( $new_status, $old_status, $post ) 
{  
	if ( $post->post_type != 'usam-product' || ! $post->post_parent )
		return;
	usam_refresh_parent_product_terms( $post->post_parent );
}
add_action( 'transition_post_status', '_usam_action_transition_post_status', 10, 3 );
/**
 * Убедитесь, что назначенные термины родительского продукта обновлены, когда его варианты удалены или разбиты
 */
function _usam_action_refresh_variation_parent_terms( $post_id ) 
{
	$post = get_post( $post_id );
	if ( $post->post_type != 'usam-product' || ! $post->post_parent )
		return;
	usam_refresh_parent_product_terms( $post->post_parent );
}
add_action( 'deleted_post', '_usam_action_refresh_variation_parent_terms', 10, 1 );


function usam_get_total_products( $query = array() )
{		
	$args = $query;
	$args['fields'] = 'ids';	
	$args['update_post_meta_cache'] = false;	
	$args['update_post_term_cache'] = false;
	$products = usam_get_products( $args );
	return count($products);
}
?>