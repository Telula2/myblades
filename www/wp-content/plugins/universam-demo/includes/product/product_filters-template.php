<?php
/**
 * Фильтры товаров.
 * @since 3.8
 */ 
 
/*
 * генерирует и возвращает URL-адреса для нумерации страниц
 */
function usam_pagination($totalpages = '', $per_page = '', $current_page = '', $page_link = '')
{
	global $wp_query, $usam_query, $wp_the_query;

	if( empty($totalpages) )	
		$totalpages = $wp_query->max_num_pages;	
	// Если имеется только одна страница
	if($totalpages <= 1) 
		return;
		
	$num_paged_links = 2; //количество ссылок, которые нужно показать по обе стороны от текущей страницы
	$additional_links = '';
	
	//дополнительные ссылки, элементов на странице заказа и продуктов
	if( get_option('permalink_structure') != '' )
		$links_separator = '?';
	else
		$links_separator = '&';
	
	$gets = array('items_per_page' => 'int', 'p_filter' => 'array', 'range' => 'str', 'orderby' => 'str', 'brand' => 'str', 'number_products' => 'int');
	foreach ( $gets as $key => $value )
	{		
		if( !empty( $_GET[$key] ) )
		{			
			$get = usam_clean_variable( $_GET[$key], $value );
			if ( $value == 'array' )
			{
				//$str = "$key=[";
			/*	$str ='';
				foreach ( $get as $key2 => $value2 )
					$str .= $key.'['.$key2.']='.$value2.'&';
				$get = $str;*/
				$get = usam_url_array_encode( $get );
			}
			$additional_links .= $links_separator.$key.'='.$get;
			$links_separator = '&';
		}
	}	
	$additional_links = apply_filters('usam_pagination_additional_links', $additional_links);	

	$current_page = absint( get_query_var('paged') );
	if($current_page == 0)
		$current_page = 1;

	if( empty($page_link))
		$page_link = usam_a_page_url();			
	//если пагинация отключена
	if(!get_option('permalink_structure'))
	{	
		$category = '?';
		if( ! empty( $wp_query->query_vars['usam-category'] ) )
			$category = '?usam-category='.$wp_query->query_vars['usam-category'];
		if(isset($wp_query->query_vars['usam-category']) && is_string($wp_query->query_vars['usam-category']))
			$page_link = get_option('blogurl').$category.'&amp;paged';
		else
			$page_link = usam_get_url_system_page('products-list').$category.'&amp;paged';		
		$separator = '=';	
	}
	else
	{ 
		$separator = 'page/';			
		if ( !empty( $wp_query->query_vars['usam-category'] ) && empty($usam_query->query_vars['pagename']) ) 
		{			
			if ( isset($wp_query->query_vars['usam-brands']) ) 
			{									
				$page_link = usam_get_url_system_page('brands').'/'.$wp_query->query_vars['usam-brands'].'/'.$wp_query->query_vars['usam-category'];
			}
			elseif ( isset($wp_query->query_vars['taxonomy']) && $wp_query->query_vars['taxonomy'] == 'usam-brands' )
			{					
				$page_link = usam_get_url_system_page('brands').'/'.$wp_query->query_vars['term'].'/'.$wp_query->query_vars['usam-category'];			
			}
			elseif ( isset($wp_query->query_vars['usam-category_sale']) ) 
			{			
				$page_link = home_url()."/category_sale/".$wp_query->query_vars['usam-category_sale']."/".$wp_query->query_vars['usam-category'];			
			}
			else
			{
				$category_id = get_term_by( 'slug', $wp_query->query_vars['usam-category'], 'usam-category' );
				$page_link = get_term_link( $category_id, 'usam-category' );				
				// in case we're displaying a category using shortcode, need to use the page's URL instead of the taxonomy URL
				if ( $wp_the_query->is_page() )
				{
					$page = $wp_the_query->get_queried_object();
					if ( preg_match( '/\[usam\_products[^\]]*category_id=/', $page->post_content ) )
						$page_link = get_permalink( $page->ID );	
				}
			}			
		} 
		elseif ( is_tax( 'product_tag' ) ) 
		{
			$tag = get_queried_object();
			$page_link = get_term_link( (int) $tag->term_id, 'product_tag' );
		} 
		elseif ( !empty($usam_query) )
		{ 
			$page_link = usam_get_url_system_page('products-list');
			if ( $usam_query->is_front_page() )
				$page_link = home_url();
			elseif ( !empty($usam_query->query_vars['pagename']) )
			{
				if ( $usam_query->query_vars['pagename'] == 'sale' ) 
				{			
					$page_link = usam_get_url_system_page('sale');		
					if ( !empty( $wp_query->query_vars['usam-category']) )
						$page_link = $page_link.$wp_query->query_vars['usam-category'];
					if ( !empty( $wp_query->query_vars['usam-brands']) )
						$page_link = $page_link.$wp_query->query_vars['usam-brands'];				
				} 
				elseif ( $usam_query->query_vars['pagename'] == 'new-arrivals' ) 
				{
					$page_link = usam_get_url_system_page('new-arrivals');
					if ( !empty( $wp_query->query_vars['usam-category']) )
						$page_link = $page_link.$wp_query->query_vars['usam-category'];
					if ( !empty( $wp_query->query_vars['usam-brands']) )
						$page_link = $page_link.$wp_query->query_vars['usam-brands'];			
				} 	
				else	
				{
					$page_link = usam_get_url_system_page($usam_query->query_vars['pagename']);
				}
			}
			elseif ( isset($usam_query->query_vars['usam-brands']) ) 
			{			
				$page_link = usam_get_url_system_page('brands').'/'.$usam_query->query_vars['term'];			
				$separator = '';
			} 
			elseif ( isset($usam_query->query_vars['usam-category_sale']) ) 
			{			
				$category_id = get_term_by( 'slug', $wp_query->query_vars['usam-category_sale'], 'usam-category_sale' );
				$page_link = get_term_link( $category_id, 'usam-category_sale' );					
			}			
		}
		else 
		{
			$page_link = usam_get_url_system_page('products-list');
			$separator = '';
		}		
	}	
	$page_link = trailingslashit( $page_link );	
	$output ="<div class='usam_navigation'>";
//	$output.= __('Страница: ','usam');
	$output.= ' ';
	if( get_option('permalink_structure') )
	{		
		// Должны ли мы показать первую ссылку странице?
		//if($current_page > 1)
		//	$output .= "<a href=\"". esc_url( $page_link . $additional_links ) . "\" title=\"" . __('Первая страница', 'usam') . "\"><span>" .'&laquo;'. "</span></a>";
		//Должны ли мы показать Ссылка на предыдущую страницу?
		if($current_page > 1) 
		{
			$previous_page = $current_page - 1;
			if( $previous_page == 1 )
				$output .= " <a href=\"".esc_url( $page_link.$additional_links )."\" title=\"".__('Предыдущая Страница', 'usam')."\"><span>&lt;</span></a>";
			else
				$output .= " <a href=\"".esc_url( $page_link.$separator.$previous_page.$additional_links )."\" title=\"".__('Предыдущая Страница', 'usam')."\"><span>".'&lt;'."</span></a>";
		}
		if($current_page > 4) 
		{			
			$output .= "<a href=\"". esc_url( $page_link.$separator. 1 . '/'.$additional_links ) . "\" title=\"" . sprintf( __('Страница %s', 'usam'), ( 1 ) ) . "\"><span>1</span></a>";
			$output .= "<span class='current'>...</span>";
		} 
		elseif($current_page == 4) 						
			$output .= "<a href=\"". esc_url( $page_link.$separator. 1 . '/'.$additional_links )."\" title=\"".sprintf( __('Страница %s', 'usam'), ( 1 ) )."\"><span>1</span></a>";
			
		$i = $current_page - $num_paged_links;
		$count = 1;
		if($i <= 0) 
			$i =1;
		while($i < $current_page)
		{
			if($count <= $num_paged_links)
			{			
				$output .= " <a href=\"". esc_url( $page_link .$separator. $i.'/'.$additional_links ) ."\" title=\"".sprintf( __('Страница %s', 'usam'), $i )." \"><span>$i</span></a>";
			}			
			$i++;
			$count++;
		}	
		if($current_page > 0)
			$output .= "<span class='current'>$current_page</span>";

		//Links after Current Page
		$i = $current_page + $num_paged_links;
		$count = 1;

		if($current_page < $totalpages)
		{
			while($i > $current_page)
			{				
				if($count < $num_paged_links + 1 && ($count + $current_page) <= $totalpages)
				{
					$output .= " <a href=\"". esc_url( $page_link .$separator. ($count+$current_page) ). '/'. $additional_links . "\" title=\"" . sprintf( __('Страница %s', 'usam'), ($count + $current_page) ) . "\"><span>".($count + $current_page)."</span></a>";
					$i++;
				}
				else	
					break;				
				$count ++;
			}			
		}
		if($current_page + 3 < $totalpages) 
		{				
			$output .= "<span class='current'>...</span>";
			$output .= "<a href=\"". esc_url( $page_link.$separator.$totalpages.'/'.$additional_links )."\" title=\"".sprintf(__('Страница %s', 'usam'), $totalpages)."\"><span>$totalpages</span></a>";
		} 
		elseif($current_page + 3 == $totalpages) 						
			$output .= "<a href=\"". esc_url( $page_link.$separator.$totalpages.'/'.$additional_links )."\" title=\"".sprintf(__('Страница %s', 'usam'), $totalpages)."\"><span>$totalpages</span></a>";
			
		if( $current_page < $totalpages ) 
		{
			$next_page = $current_page + 1;		
			$output .= "<a href=\"". esc_url( $page_link  .$separator. $next_page . '/' . $additional_links ) . "\" title=\"" . __('Следующая страница', 'usam') . "\"><span>&gt;</span></a>";	
		//	$output .= "<a href=\"". esc_url( $page_link  .$separator. $totalpages . '/' . $additional_links ) . "\" title=\"" . __('Предыдущая страница', 'usam') . "\"><span>&raquo;</span></a>";
		}
	} 
	else
	{ 
		// Должны ли мы показать связь Первая страница?
		if($current_page > 1)
			$output .= "<a href=\"". remove_query_arg('paged' ) . "\" title=\"" . __('Первая страница', 'usam') . "\"><span>&laquo;</span></a>";

		// Should we show the PREVIOUS PAGE link?
		if($current_page > 1)
		{
			$previous_page = $current_page - 1;
			if( $previous_page == 1 )
				$output .= " <a href=\"".remove_query_arg( 'paged' ).$additional_links."\" title=\"" . __('Предыдущая страница', 'usam') . "\"><span>&lt;</span></a>";
			else
				$output .= " <a href=\"". add_query_arg( 'paged', ($current_page - 1) ).$additional_links . "\" title=\"". __('Предыдущая страница', 'usam')."\"><span>&lt;</span></a>";
		}
		$i = $current_page - $num_paged_links;
		$count = 1;
		if($i <= 0) $i =1;
		while($i < $current_page)
		{
			if($count <= $num_paged_links)
			{
				if($i == 1)
					$output .= " <a href=\"". remove_query_arg('paged' ) . "\" title=\"" . sprintf( __('Страница %s', 'usam'), $i ) . " \"><span>$i</span></a>";
				else
					$output .= " <a href=\"". add_query_arg('paged', $i ) . "\" title=\"" . sprintf( __('Страница %s', 'usam'), $i ) . " \"><span>$i</span></a>";
			}
			$i++;
			$count++;
		}	
		if($current_page > 0)
			$output .= "<span class='current'>$current_page</span>";
		//Ссылки после текущей страницы
		$i = $current_page + $num_paged_links;
		$count = 1;

		if($current_page < $totalpages)
		{
			while(($i) > $current_page)
			{
				if($count < $num_paged_links && ($count+$current_page) <= $totalpages)
				{
					$output .= " <a href=\"".add_query_arg( 'paged', ($count+$current_page) )."\" title=\"".sprintf( __('Страница %s', 'usam'), ($count+$current_page) )."\"><span>".($count+$current_page)."</span></a>";
					$i++;
				}
				else
				{
					break;
				}
				$count ++;
			}
		}
		if($current_page < $totalpages) 
		{
			$next_page = $current_page + 1;
			$output .= "<a href=\"".add_query_arg( 'paged', $next_page )."\" title=\"" . __('Следующая страница', 'usam') . "\"><span>".__('Следующая &gt;', 'usam')."</span></a>";
		}
		// Должны ли мы показать связь последней странице?
		if($current_page < $totalpages) 
		{
			$output .= "<a href=\"".add_query_arg( 'paged', $totalpages )."\" title=\"" . __('Предыдущая страница', 'usam') . "\"><span>".__('Предыдущая &raquo;', 'usam')."</span></a>";
		}
	}
	$output.="</div>";	
	echo $output;
}

function usam_quantity_of_products( $args = array() )
{	
	$per_page = get_option('usam_products_per_page', 24);	
	if ( empty($per_page) )
		return false;
	
	if ( !isset($args['block']) )
	{				
		$args['block'] = array( $per_page, $per_page * 2, $per_page * 3 );
	}
	$post_URL = '';	
	$number_products = isset($_SESSION['number_products'])?$_SESSION['number_products']:25;
	$out ='<div class="number_products">';
	$out.='<ul>';		
	foreach ($args['block'] as $block)
	{
		if ( $number_products == $block )
			$out.= '<li><span class="element active">'.$block.'</span></li>';
		else
			$out.= '<li><a href="'.$post_URL.'?number_products='.$block.'"><span class="element">'.$block.'</span></a></li>';
	}
	$out.= '</ul>';
	$out.= '</div>';	
	echo $out;
}

function usam_product_info()
{
	global $wp_query;		
	
	if ( empty($wp_query) )
		return false;
	
	$kol_prod = $wp_query->found_posts;
	if ( $kol_prod == 0 )
		return;			
	$product_count = $wp_query->post_count;	// количество товаров на текущей странице
	$max_num_pages = $wp_query->max_num_pages; 	// количество страниц
	$current_page = $wp_query->query_vars['paged']; // текущая страница	

	switch ( $current_page ) 
	{
		case 0:
		case 1:
			$first_product = 1;			
			$prod_end = $product_count;				
		break;
		case $max_num_pages:	
			$first_product = $kol_prod - $product_count + 1;
			$prod_end = $kol_prod;		
		break;		
		default:				
			$first_product = ($current_page-1) * $product_count + 1;		
			$prod_end = $current_page * $product_count;			
		break;
	}		
	echo '<div class="products_info">'.sprintf(_n('%s товар', '%s товаров', $wp_query->found_posts, 'usam'), $wp_query->found_posts).' ('.$first_product.' - '.$prod_end.')</div>';
}

// сортировка по цене, имени
function usam_sortby_form()
{
	global $wp_query;	
	
	if ( empty($wp_query) )
		return false;
	
	if ( isset($_GET['orderby']) )
		$orderby = sanitize_title($_GET['orderby']);
	else
	{			
		if ( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'new-arrivals' )	//Если страница новинки
			$orderby = 'date';
		else
		{			
			$orderby = get_option('usam_product_sort_by');
		}
	}		
	$sort = array ( 'popularity'   => __('По популярности','usam'),
					'rating'       => __('По рейтингу','usam'),
					'percent'      => __('По уменьшению скидок','usam'),
					'name'         => __('По имени','usam'),
					'id'           => __('По дате','usam'),
					'priceDESC'    => __('По уменьшению цены','usam'),
					'priceASC'     => __('По увеличению цены','usam'),					
					'product_news' => __('Показать новинки','usam'),
					'sale'         => __('Показать акционные','usam'));		
	$out ='<div class="pfilter product_sort_options">';
	$out.='<form method="get" action="#">';
	$out.='<select id ="usam_products_sort" name="orderby">';
	foreach( $sort as $key => $value )
	{
		if ( $orderby == $key )
		{
			$selected = 'selected';
			//$value = __('Сортировать','usam').' '.$value;
		}
		else
		{
			$selected = '';	
		}
		$out.="<option $selected value='$key'>$value</option>";
	}	
	$out.='</select>';	
	$out.='</form>';
	$out.= '</div>';	
	echo $out;
}

function usam_range_price_slider()
{		
	global $wp_query, $usam_query, $post, $type_price;
	
	if ( empty($wp_query->query_vars) )			
		return;	
	
	if ( empty($usam_query) || $usam_query->found_posts < 2 ) 
		return;	
	
	if ( isset($wp_query->query_vars['usam-category']) && !empty($usam_query->query_vars) )			
		$new_query = $usam_query->query_vars;	
	else	
		$new_query = $usam_query->query_vars;		

	$new_query = array_merge( $new_query, usam_product_sort_order_query_vars('priceDESC') );
	$new_query['posts_per_page'] = 1;
	$new_query['no_found_rows'] = true;	
	$new_query['cache_results'] = false;	
	$new_query['update_post_meta_cache'] = false;		
	$new_query['update_post_term_cache'] = false;
	$new_query['fields'] = 'ids';
	if ( isset($new_query['price_range']) )
		unset($new_query['price_range']);	
	
	if ( !empty($new_query['meta_query']) )
		foreach ( $new_query['meta_query'] as $key => $meta_query )
		{
			if ( $meta_query['key'] ==  '_usam_price_'.$type_price )
				unset($new_query['meta_query'][$key]);
		}	
	$posts = get_posts( $new_query );
	if ( empty($posts[0]) )
		return false;
	
	$max_price = usam_get_product_meta($posts[0],'price_'.$type_price, true);	
	$min_price = 0;
	if( isset($_GET['range']) )
	{		
		$str = preg_replace('/[^0-9-]/', '', $_GET['range'] );
		$range = explode('-', $_GET['range']);	
		$min_price_value = $range[0];
		$max_price_value = $range[1];			
	}
	else
	{
		$min_price_value = $min_price;
		$max_price_value = $max_price;	
	}	
	$number_cells = 4;
	$d = round($max_price/$number_cells, 0 );	
	echo '
	<script>
	jQuery(document).ready(function($){
		$(function() 
			{  
			$( "#slider-range" ).slider({
				range: true,
				min: '.$min_price.',
				max: '.$max_price.',
				values: [ '.$min_price_value.', '.$max_price_value.' ],
				slide: function( event, ui ) {
					$( ".prs_min" ).html( ui.values[ 0 ] );
					$( ".prs_max" ).html( ui.values[ 1 ] );
				}
			});			
		});
	});
	</script>	
	<div class="pfilter price_range_slider">			
		<span class="prs_min prs_price">'.$min_price.'</span>
		<span class="prs_max prs_price">'.$max_price.'</span>
		<div id="slider-range"></div>		
		<div class ="prs_grid">		
			<span class="prs_grid-pol" style="left: 0%"></span>';
		
			$left = 0;		
			$margin = '-1.07407';			
			for ($i=0; $i<$number_cells; $i++)
			{
				$range = $d * $i + $min_price;		
				
				echo '<span class="prs_grid-text js-grid-text-'.$i.'" style="left: '.$left.'%; visibility: visible; margin-left: '.$margin.'%;">'.$range.'</span>';
				$left += 5;
				$end = $left + 15;
				for ( $j=$left; $j<=$end; )
				{			
					echo '<span class="prs_grid-pol small" style="left: '.$j.'%"></span>';
					$j += 5;
				} 
				$left += 20;
				echo '<span class="prs_grid-pol" style="left: '.$left.'%"></span>';			
				$margin = '-4.07407';					
			}		
			echo '<span class="prs_grid-text js-grid-text-4" style="left: 100%; margin-left: -5.18519%;">'.$max_price.'</span>			
		</div> 
	</div>';	
}
	
	
// Фильтр по брендам.
function usam_brand_filter_form()
{
	global $wp_query, $usam_query;	

	if ( isset($usam_query->query_vars['usam-brands']) )
		return;
	
	return;
	$new_query = $wp_query->query_vars;	
	unset($new_query['usam-brands']);	
	$new_query['meta_query'] = array();	
	$new_query['nopaging'] = true;
	$new_query['fields'] = 'ids';
	$new_query['cache_results'] = false;
	$new_query['update_post_meta_cache'] = false;
	$new_query['update_post_term_cache'] = false;
	$new_query['no_found_rows'] = true;		
	
	$query = new WP_Query( $new_query );			
	if ( !empty($query->posts) )
	{
		$args = array( 'orderby' => 'name', 'fields' => 'all', 'update_term_meta_cache' => false );	
		$object_terms = wp_get_object_terms( $query->posts, 'usam-brands', $args );
		
		$brand = isset($_GET['brand'])?$_GET['brand']: '';		
		$out ='<div class="brand_filter_form">';		
		$out.='<select name="brand" onchange="this.form.submit()">';
		$out.="<option ". (!isset($_GET['brand'])?'selected': '') ." value=''> - ".__('выберете бренд','usam')." - </option>";
		foreach( $object_terms as $term )
		{
			if ( $term->slug == $brand )
			{
				$selected = 'selected';		
			}
			else
			{
				$selected = '';	
			}
			$out.="<option $selected value='$term->slug'>$term->name</option>";
		}	
		$out.='</select>';			
		$out.= '</div>';	
		echo $out;
	}
}

//фильтры товаров
function usam_filter_form()
{
	global $wp_query, $wpdb;	
	if( !isset($wp_query->query['usam-category']))
		return;
	
	$category = get_term_by('slug', $wp_query->query['usam-category'], 'usam-category');	
	if ( !empty( $category ))
	{		
		$term_ids = get_ancestors( $category->term_id, 'usam-category' );
		$term_ids[] = $category->term_id;		
		$attribute_ids = usam_get_taxonomy_relationships( $term_ids, 'usam-category', $colum = '2' );		
				
		$new_query = $wp_query->query_vars;	
		$new_query['nopaging'] = true;
		$new_query['fields'] = 'ids';
		$new_query['cache_results'] = false;
		$new_query['update_post_meta_cache'] = false;
		$new_query['update_post_term_cache'] = false;
		$new_query['no_found_rows'] = true;	
		
		$product_ids = usam_get_products( $new_query );	
		if ( empty( $product_ids ))
			return;				
	
		$args = array( 'hide_empty' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sort_order', 'include' => $attribute_ids, 'meta_query' => array( array( 'key' => 'usam_filter','value' => 1, 'compare' => '=' ) ) );
		$terms = get_terms('usam-product_attributes', $args);		
			
		$filter_attribute_ids = array();
		foreach( $terms as $term )
		{	
			$filter_attribute_ids[] = $term->term_id;	
		}			
		if ( empty( $filter_attribute_ids ))
			return;					
		
		$where = array();
		foreach( $filter_attribute_ids as $id )
		{
			$where[] = "meta_key='_usam_filter_$id'";
		}		
		$_where = implode(" OR ", $where);
		
		$sql_query = "SELECT meta_value FROM $wpdb->postmeta WHERE ( $_where ) AND post_id IN ('".implode("','",$product_ids)."')";
		$filters_ids = $wpdb->get_col( $sql_query );
		
		// Получить название опций
		$filters = usam_get_product_filters_value( array( 'id' => $filters_ids ) ); 
		$product_filters = array();
		foreach( $filters as $filter )
		{
			$product_filters[$filter->attribute_id][$filter->id] = $filter->value;
		}				
		$html = '';			
		foreach( $terms as $attr )
		{							
			if ( isset($product_filters[$attr->term_id]) )
			{	
				$filter_html = '';			
				$select_filters = array();
				if ( isset($_GET['p_filter']) )				
				{
					$select_filters = $_GET['p_filter'];
					if ( !is_array($select_filters) )
						$select_filters = usam_url_array_decode($select_filters);
				}					
				foreach($product_filters[$attr->term_id] as $filter_id => $name )
				{								
					if (isset($select_filters[$filter_id]) && $select_filters[$filter_id] == $id_option )
						$selected = 'selected="selected"';
					else
						$selected = '';								
					$filter_html .= '<option '.$selected.' value="'.$filter_id.'">'.$name.'</option>';			
				}
				if ( $filter_html != '' )
				{			
					$html .= "<li id = 'filter_name-$attr->term_id'>";
					$html .= "<h4>".$attr->name."</h4>";						
					$html .= "<select filter_id = '$attr->term_id' name='p_filter[$attr->term_id]'>";	
					$html .= "<option value=''>---</option>".$filter_html;	
					$html .= "</select></li>";							
				}
			}
		}
		echo '<div id = "usam_filter_form" class="usam_filter_form"><ul>'.$html.'</ul></div>';	
	}
}

function usam_display_products_options()
{
	$view_type = usam_get_display_type();		
		
	$out = '<div class="display_products_options">';	
	switch ( $view_type ) 
	{
		case "grid":
			$out.='<span class="option_display grid active" data-view_type="grid"></span>';
			$out.='<a href="?view_type=list" title="'.__('Просмотр списком','usam').'"><span class="option_display list"></span></a>';
			break;
		case "list":			
			$out.='<a href="?view_type=grid" title="'.__('Просмотр сеткой','usam').'"><span class="option_display grid"></span></a>';
			$out.='<span class="option_display list active" data-view_type="list"></span>';
			break;
		default:
			$out.='<span class="option_display grid active" data-view_type="grid"></span>';
			$out.='<a href="?view_type=list" title="'.__('Просмотр списком','usam').'"><span class="option_display list"></span></a>';
		break;		
	}
	$out.= '</div>';
	echo $out;
}
