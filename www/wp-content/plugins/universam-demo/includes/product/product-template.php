<?php
/**
 * Функции товаров. Эквивалент post-template.php
 * @since 3.8
 */ 
 
// Загрузить товар 
class USAM_Load_Product
{
	function __construct( ) 
	{	
		add_action( 'wp_head', array($this, 'opengraph_for_posts') );	
		add_action( 'usam_product_addons', array($this, 'save_product_views') );	
		add_action( 'usam_product_addons', array($this, 'save_history_views_product') );		
		add_action( 'usam_after_main_content', array($this, 'fancy_notifications') );
	}	
	
	//передать в соцсеть информацию
	function opengraph_for_posts() 
	{
		global $post;
		if ( is_singular() )
		{
			setup_postdata( $post );
			$output = '<meta property="og:type" content="article" />' . "\n";
			$output .= '<meta property="og:title" content="' . esc_attr( get_the_title() ) . '" />' . "\n";
			$output .= '<meta property="og:url" content="' . get_permalink() . '" />' . "\n";
			$output .= '<meta property="og:description" content="' . esc_attr( get_the_excerpt() ) . '" />' . "\n";
			if ( has_post_thumbnail() ) 
			{
				$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
				$output .= '<meta property="og:image" content="' . $imgsrc[0] . '" />' . "\n";
			}
			echo $output;
		}		
	}			
	
	/**
	 * Сохраняет историю просмотров товара
	 */
	function save_history_views_product( $product_id )
	{
		global $wpdb, $user_ID;
		$length_list = 20;	
		$args = array( 'user_list' => 'view', 'fields' => 'product_id', 'user_id' => $user_ID );
		$product_ids = usam_get_user_products( $args );		
		if ( !empty($product_ids) )
		{	
			if ( $product_ids[0] == $product_id )
				return false;
				
			$ids = array();
			if ( count($product_ids) > $length_list )
			{
				$ids = array_slice($product_ids,0,$length_list);		
			}		
			if ( !empty($ids) )
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_USER_PRODUCTS." WHERE (product_id NOT IN (".implode(',', $ids).") OR product_id='$product_id') AND user_list='view' AND user_id='$user_ID' ORDER BY date_insert");
		}		
		$data = array( 'user_list' => 'view', 'product_id' => $product_id );
		usam_insert_user_product( $data );
	}

	// Счетчик просмотра товара
	function save_product_views( $post_ID ) 
	{	
		$current_user = wp_get_current_user(); 
		if ( user_can( $current_user, "subscriber" ) || !is_user_logged_in() )
		{
			$count_key = 'product_views';
			$count = usam_get_product_meta($post_ID, $count_key );	 
			if($count == '')					
				usam_add_product_meta($post_ID, $count_key, 0);		
			else 
			{
				$count++;
				usam_update_product_meta($post_ID, $count_key, $count);      
			}
		}	
	}
	
	/*
	Описание: Окошко при добавлении в корзину
	*/
	function fancy_notifications( )
	{
		usam_fancy_notifications( true );
	}

}
new USAM_Load_Product();


function usam_fancy_notifications( $echo = false )
{
	static $already_output = false;

	if ( $already_output )
		return '';

	$output = "";
	if ( get_option( 'usam_show_fancy_notifications', 1 ) ) 
	{
		$output = "";
		$output .= "<div id='fancy_notification'>\n\r";
		$output .= "  <div id='loading_animation'>\n\r";
		$output .= '<img id="fancy_notificationimage" title="'.__( 'Загрузка', 'usam' ).'" alt="'.__( 'Загрузка', 'usam' ).'" src="'.usam_loading_animation_url().'" />'.__( 'Обновление', 'usam' )."...\n\r";
		$output .= "  </div>\n\r";
		$output .= "  <div id='fancy_notification_content'>\n\r";	
		$output .= "  </div>\n\r";
		$output .= "</div>\n\r";
	}
	$already_output = true;
	
	if ( $echo )
		echo $output;	
	else
		return $output;
}

//окошко при добавлении товара в корзину
function usam_fancy_notification_content( $cart_messages ) 
{
	$siteurl = get_option( 'siteurl' );
	$output = '';
	$output .= "<span id='form_close' onclick='jQuery(\"#fancy_notification\").css(\"display\", \"none\"); return false;' class ='form_close'>×</span>\n\r";
	foreach ( (array)$cart_messages as $cart_message ) {
		$output .= "<span>" . $cart_message . "</span><br />";
	}
	$output .= "<a href='" . usam_get_url_system_page('basket') . "' class='go_to_checkout'>" . __( 'Оформить заказ', 'usam' ) . "</a>";
	$output .= "<a href='#' onclick='jQuery(\"#fancy_notification\").css(\"display\", \"none\"); return false;' class='continue_shopping'>" . __( 'Продолжить покупки', 'usam' ) . "</a>";	
	return $output;
}

/* 
 * добавить в корзину, функция кнопки используются для PHP шаблонов тегов и шорткодов
 */
function usam_add_to_cart_button( $product_id, $return = false ) 
{
	global $wpdb, $usam_variations;
	$output = '';	
	if ( $product_id > 0 ) 
	{		
		$usam_variations = new USAM_Variations( $product_id );
		
		if ( $return )
			ob_start();
		?>
			<div class='usam-add-to-cart-button'>
				<form class='usam-add-to-cart-button-form' id='product_<?php echo esc_attr( $product_id ) ?>' action='' method='post'>
					<?php do_action( 'usam_add_to_cart_button_form_begin' ); ?>
					<?php usam_select_product_variation(); ?>					
					<?php usam_addtocart_button( $product_id ); ?>
					<?php do_action( 'usam_add_to_cart_button_form_end' ); ?>
				</form>
			</div>
		<?php
		if ( $return )
			return ob_get_clean();
	}
}
 
/**
 * Получить URL анимированного изображения для загрузки.
 */
function usam_loading_animation_url() 
{
	return apply_filters( 'usam_loading_animation_url', USAM_CORE_THEME_URL . 'images/loading.gif' );
}

function usam_has_pages_top()
{
	$position = get_option( 'usam_page_number_position' );
	if( usam_has_pages() &&  (( $position == 1) || ($position == 3)))
		return true;
	else
		return false;
}

function usam_has_pages_bottom()
{
	$position = get_option( 'usam_page_number_position' );
	if( usam_has_pages() &&  (( $position == 2) || ( $position == 3)))
		return true;
	else
		return false;
}


/**
 * Используется в usam_pagination, генерирует ссылки в нумерации страниц
 */
function usam_a_page_url( $page = null )
{
	global $wp_query;
	$output = '';
	$curpage = $wp_query->query_vars['paged'];
	if($page != '')
		$wp_query->query_vars['paged'] = $page;
	if($wp_query->is_single === true)
	{
		$wp_query->query_vars['paged'] = $curpage;
		return usam_product_url($wp_query->post->ID);
	} 
	else 
	{		
		if( 1 < $wp_query->query_vars['paged'])
		{
			if(get_option('permalink_structure'))
				$output .= "paged/{$wp_query->query_vars['paged']}/";
			else
				$output = add_query_arg('paged', '', $output);
		}
		return $output;
	}
}

/**
 * Проверяет, нужно ли отображать запас для товара
 */
function usam_show_stock_availability()
{
	if( get_option('usam_show_availability_stock') )
		return true;
	else
		return false;
}

function usam_product_no_image_fallback( $image_url = '' )
{
	if ( !empty( $image_url ) )
		return $image_url;
	else
		return apply_filters( 'usam_product_noimage', USAM_CORE_THEME_URL . 'images/noimage.png' );
}
add_filter( 'usam_product_image', 'usam_product_no_image_fallback' );


/**
 * Используется для определения, следует ли отображать продукты на странице
 */
function usam_display_categories()
{
	global $wp_query;
	$output = false;
	if ( !is_numeric( get_option( 'usam_default_category' ) ) && ! get_query_var( 'product_tag' ) )
	{
		if ( isset( $wp_query->query_vars['products'] ) )
			$category_id = $wp_query->query_vars['products'];
		elseif ( isset( $_GET['products'] ) )
			$category_id = absint($_GET['products']);

		// if we have no categories, and no search, show the group list
		if ( is_numeric( get_option( 'usam_default_category' ) ) || (isset( $product_id ) && is_numeric( $product_id )) )
			$output = true;
		if ( (get_option( 'usam_default_category' ) == 'all+list'))
			$output = true;

		if (get_option( 'usam_default_category' ) == 'list' && (!isset($wp_query->query_vars['usam-category']) || !isset($wp_query->query_vars['product_tag']) && get_option('usam_display_categories')))
			$output = true;
	}
	if ( isset( $category_id ) && $category_id > 0 )
		$output = false;
	if ( get_option( 'usam_display_categories' ))
		$output = true;

	return $output;
}

/**
 * Используется, чтобы определить, следует ли отображать продуктов на странице
 */
function usam_display_products() 
{
	global $wp_query;
	$output = false;
	if ( is_object($wp_query) )
	{
		$post = $wp_query->get_queried_object();	
		$output = true;
		if ( usam_display_categories () && $post )
		{
			if ( get_option( 'usam_default_category' ) == 'list' && $post->ID == usam_get_system_page_id('products-list') )
				$output = false;
		}
	}
	return $output;
}

// Показывать комментарии
function usam_show_product_comments( $product_id = 0 )
{	
	if ( empty($product_id) )
		$product_id = get_the_ID();	
	
	if ( $product_id == 0 )
		return false;

	$comments = 1;
	if ( get_option("usam_show_comments_by_default", 0) )
	{
		$product_meta = maybe_unserialize( usam_get_product_meta($product_id , 'product_metadata' ) );
		$comments = !empty($product_meta['comment'])?$product_meta['comment']:0;
	}
	switch( $comments )
	{
		case 0:
			return false;
		break;
		case 1:		
			return true;
		break;	
		default:		
			return true;
		break;
	}	
}
/**
 * 	возвращает URL этой страницы
 */
function usam_this_page_url() 
{
	global $usam_query, $wp_query;
	if ( isset($usam_query->is_single) && $usam_query->is_single ) 
	{
		$output = usam_product_url( $wp_query->post->ID );
	} 
	else if ( isset( $usam_query->category ) && $usam_query->category != null ) 
	{
		$output = usam_category_url( $usam_query->category );
		if ( $usam_query->query_vars['page'] > 1 ) {
			if ( get_option( 'permalink_structure' ) ) {
				$output .= "page/{$usam_query->query_vars['page']}/";
			} else {
				$output = add_query_arg( 'page_number', $usam_query->query_vars['page'], $output );
			}
		}
	} 
	elseif ( isset( $id ) ) 
		$output = get_permalink( $id );
	else 
		$output = get_permalink( get_the_ID() );
	return $output;
}

/**
 * category class function, categories can have a specific class, this gets that
 * @return string - the class of the selected category
 */
function usam_category_class() 
{
	global $wpdb, $wp_query;

	$category_nice_name = '';
	if ( 'usam-category' == $wp_query->query_vars['taxonomy']  ) 
	{
		$catid = usam_get_the_category_id($wp_query->query_vars['term'],'slug');
	}
	else 
	{
		$catid = get_option( 'usam_default_category' );
		if ( $catid == 'all+list' ) 
			$catid = 'all';		
	}
	if ( (int)$catid > 0 )
	{
		$term = get_term($catid, 'usam-category');
		$category_nice_name = $term->slug;
	}
	else if ( $catid == 'all' )	
		$category_nice_name = 'all-categories';	
	return $category_nice_name;
}


/**
 * функция проверки, требуется ли показывать Facebook Like
 */
function usam_show_fb_like()
{
	if('1' == get_option('usam_facebook_like', ''))
		return true;
	else
		return false;
}

/**
 * функция проверки, требуется ли показывать Вконтакте Like
 */
function usam_show_vk_like()
{
	if('1' == get_option('usam_vk_like', ''))
		return true;
	else
		return false;
}
/**
 * цикл по товарам
 */
function usam_have_products() 
{
/*	static $replace_request = true;
	global $wp_query, $usam_query;
	
	$result = have_posts();
	if ( $result )
		$replace_request = true;
	else
		$replace_request = false;
	
	if ( $replace_request )
	{
		list($wp_query, $usam_query) = array( $usam_query, $wp_query );
	}*/
	return have_posts();
}

/**
 * Получить следующий товар
 */
function usam_the_product() 
{
	global $usam_variations;
	the_post();	
	$usam_variations = new USAM_Variations( get_the_ID() );
}


/**
 * Получить ID товара
 */
function usam_the_product_id() 
{
	return get_the_ID();
}

/**
 * Ссылка на изменения товаров
 */
function usam_edit_the_product_link( $product_id = null, $before = '<span class="edit-link">', $after = '</span>' )
{	
	if ( current_user_can( 'edit_published_posts' ) )
	{
		if ( $product_id == null )
			$product_id = get_the_ID();
		
		$text = __( 'редактировать', 'usam' );	
		$url = admin_url('post.php?post='.$product_id.'&action=edit');
		$link = '<a class="post-edit-link" href="' . $url . '">' . $text . '</a>';		
		echo $before . apply_filters( 'edit_product_link', $link, $product_id, $text ) . $after;
	}
}

/**
 * Получить название товара
 */
function usam_the_product_title( $product_id = 0 )
{
	return get_the_title( $product_id );
}

/**
 * Получить описание товара
 */
function usam_the_product_description()
{
	$content = usam_display_product_attributes();
	$product_components = usam_display_product_components();
	if ( $product_components != '' )
	{		
		$content .= $product_components;
	}
	$content .= get_the_content( __( 'Прочитайте описание полностью &raquo;', 'usam' ) );
	if (!empty($content))
	{	
		$content = str_replace(']]>', ']]>', $content);  
	}
	else
	{
		$content = 'К сожалению описание отсутствует. В ближайшее время мы сможем его написать.';
	}	
	return do_shortcode( wpautop( $content,1 ) );
}

/**
 * Получить дополнительное описание товара
 */
function usam_the_product_additional_description()
{
	global $post;
	if ( !empty( $post->post_excerpt ) )
		return $post->post_excerpt;
	else
		return false;
}

/**
 * Получить внешнюю ссылку товара
 */
function usam_product_external_link( $product_id = null ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();

	$product_meta = usam_get_product_meta( $product_id, 'product_metadata', true );
	if ( isset( $product_meta['webspy_link'] ) ) 
	{
		$external_link = $product_meta['webspy_link'];
		return esc_url( $external_link );
	}
}

/**
 * Перенаправляемый ли продукт
 */
function usam_product_external_redirect( $product_id = null ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();

	$product_meta = usam_get_product_meta( $product_id, 'product_metadata', true );
	if ( isset($product_meta['external']['type']) && $product_meta['external']['type'] == 'redirect' ) 
		return true;
	else
		return false;
}

/**
 * Получить текст для внешней ссылки товара
 */
function usam_product_external_link_text( $product_id = null, $default = null ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();

	$external_link_text = __( 'Купить сейчас', 'usam' );
	if ( $default != null )
		$external_link_text = $default;	

	$product_meta = usam_get_product_meta( $product_id, 'product_metadata', true );
	if ( !empty( $product_meta['external']['title'] ) )
		$external_link_text = $product_meta['external']['title'];	
	return esc_html( $external_link_text );
}

/**
 * Открывать ссылку в новом окне
 */
function usam_product_external_link_target( $product_id = null, $external_link_target = true ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();

	$product_meta = usam_get_product_meta( $product_id, 'product_metadata' );
	if ( isset( $product_meta['external']['target'] ) )
		$external_link_target = $product_meta['external']['target'];	
	return esc_attr( $external_link_target );
}
/**
 *  Проверяет текущий продукт является внешним
 */
function usam_is_product_external( $product_id = null ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();

	$external_link = usam_product_external_link( $product_id ); // Получить ссылку на внешний товар
	if ( !empty( $external_link ) )
		return true;
	else
		return false;
}

/**
 * Вернуть остаток товара
 */
function usam_product_remaining_stock( $product_id = null ) 
{
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$product_stock = usam_get_product_meta( $product_id, 'stock' );
	if ( is_numeric( $product_stock ) ) 
		return absint( $product_stock );
	else 
		return null;
}

function usam_single_image( $product_id = null, $title = '' )
{
	if ( $product_id == null )
	{
		$product_id = get_the_ID();
		$title = get_the_title( $product_id );
	}
	$single_view_image = get_option( 'usam_single_view_image' );
	$image_width  = $single_view_image['width'];
	$image_height = $single_view_image['height'];
	
	$post_thumbnail_id = get_post_thumbnail_id( $product_id );
	if( wp_is_mobile() )
	{		
		?>	
		<div class="product_image_mobile">		
			<div class="product_image_mobile_wrapper">	
				<ul>
					<?php					
					$attachments = usam_get_product_images( $product_id );		
					if ( ! empty( $attachments ) ) 
					{
						foreach ( $attachments as $attachment ) 
						{	
							$image_attributes = wp_get_attachment_image_src($attachment->ID, array($image_width, $image_height) );				
							echo '<li class="image"><img src="'.$image_attributes[0].'" alt="'.$attachment->post_title.'"></li>';
						}			
					}						
					?>				
				</ul>
			</div>
		</div>
		<?php
	}
	else
	{			
		?>			
		<div class="product_image_wrapper">			
			<?php 
			if ( !empty($post_thumbnail_id) ) 
			{			
				$attachment_product_thumbnail_full = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );  			
				 if ( $attachment_product_thumbnail_full[1]>$image_width || $attachment_product_thumbnail_full[2]>$image_height ) { ?>
					<a href="<?php echo $attachment_product_thumbnail_full[0]; ?>" class="ProductZoom" id="Zoomer" rel="zoom-position:inner;loading-msg:<?php _e('Загрузка...','usam') ?>;">
						<img itemprop="image" class="product_image" id="product_image_<?php echo $product_id; ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" src="<?php echo usam_get_product_thumbnail_src( $product_id, array($image_width, $image_height) ); ?>"/>
					</a>
				<?php } else { ?>
					<img itemprop="image" class="product_image" id="product_image_<?php echo $product_id; ?>" title="<?php echo $title; ?>" alt="<?php echo $title; ?>" src="<?php echo usam_get_product_thumbnail_src( $product_id, array($image_width, $image_height) ); ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>"/>
				<?php } ?>
				<div class="thumbs" id="thumbs">					
					<?php echo usam_get_images_for_product( $product_id ); ?>		
				</div>	
			<?php 
			}
			else 
			{ ?>					
				<img class="product_image" id="product_image_<?php echo $product_id; ?>" alt="<?php _e("Нет изображения","usam"); ?>" src="<?php echo USAM_CORE_THEME_URL; ?>images/noimage.png" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>"/>				
			<?php 
			} ?>
		</div>
		<?php 
	}
}

/**
 * Проверить тип отображения
 */
function usam_get_display_type()
{
	global $usam_query, $post;
	
	if( wp_is_mobile() )
		$display_type = 'grid';	
	elseif ( isset($usam_query->query_vars['taxonomy']) && 'usam-category' == $usam_query->query_vars['taxonomy'] && is_string($usam_query->query_vars['term']) && 1 < $usam_query->post_count)
		$display_type =	usam_get_the_category_display( $usam_query->query_vars['term']) ;
	elseif ( isset($usam_query->query_vars['taxonomy']) && 'usam-brands' == $usam_query->query_vars['taxonomy'] && is_string($usam_query->query_vars['term']) && 1 < $usam_query->post_count)
		$display_type =	usam_get_the_brand_display( $usam_query->query_vars['term']);

	if ( empty($display_type) || $display_type == 'default' )
	{ 
		$saved_display = usam_get_customer_meta( 'display_type' );		
		$display_type  = !empty($saved_display) ? $saved_display : get_option('usam_product_view', 'default');
	}					
	return $display_type;
}

 // функция выводит продукты в соответствии с запрошенными сортировками
function usam_include_products_page_template( $replace_request = true )
{			
	global $wp_query, $usam_query;	
	
	if ( $replace_request )
		list($wp_query, $usam_query) = array( $usam_query, $wp_query );		
			
	$view_type = usam_get_display_type();			
	switch ( $view_type ) 
	{		
		case "list":
			usam_load_template( 'list_view' );
		break; 			
		case "grid":
		default:
			usam_load_template( 'grid_view' ) ;			
		break; 		
	}	
	if ( $replace_request )
		list($wp_query, $usam_query) = array( $usam_query, $wp_query );	
}

/**
 * Лучшие товары
 * @since 3.8
 */
function usam_the_sticky_image( $product_id )
{
	$attached_images = get_posts( array('post_type' => 'attachment','numberposts' => 1,'post_status' => null,'post_parent' => $product_id,'orderby' => 'menu_order','order' => 'ASC') );
	if ( has_post_thumbnail( $product_id ) ) 
	{
		add_image_size( 'featured-product-thumbnails', 540, 260, TRUE );
		$image = get_the_post_thumbnail( $product_id, 'featured-product-thumbnails' );
		return $image;
	} 
	elseif ( !empty( $attached_images ) ) 
	{
		$attached_image = $attached_images[0];
		$image_link = usam_product_image( $attached_image->ID, 540, 260 );
		return '<img src="' . $image_link . '" alt="" />';
	} 
	else
	{
		return false;
	}
}

/**
 * Показать Популярные(Feautered) товары над страницей товаров:
 */
function usam_display_featured_products_page()
{
	global $wp_query;
	$featured_products = $wpdb->get_col( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_usam_sticky_products' AND meta_value = '1'" );
	if ( !empty( $featured_products ) ) 
	{
		$query = usam_get_products( array( 'post__in' => $featured_products, 'orderby' => 'rand', 'numberposts' => 1, 'posts_per_page' => 1	), true );
		if ( count( $query ) > 0 )
		{		
			$product_image = get_option( 'usam_product_image' );
			$image_width =  $product_image['width'];
			$image_height = $product_image['height'];	
			
			ob_start();			
			
			usam_include_template_file( 'featured_product.php' );
			$is_single = false;
			$output .= ob_get_contents();			
			ob_end_clean();	
			
			echo $output;		
		}
	}
}


// Шаблон меток
/**
 * @return string - html отображение одного или более продуктов
 */
function usam_display_products_page( $query )
{
	global $usam_query, $wp_query, $type_price;

	remove_filter('the_title','usam_the_category_title');
	// Если данные поступают от шорткодов разобрать значения в переменной аргументы
	if(!empty($query))
	{
		$args = array();
		
		$args['post_type'] = 'usam-product';
		if(!empty($query['product_id']) && is_array($query['product_id']))
			$args['post__in'] = $query['product_id'];
		elseif(is_string($query['product_id']))
			$args['post__in'] = (array)$query['product_id'];
		
		if(!empty($query['price']) && 'sale' != $query['price'])
		{
			$args['meta_key'] = '_usam_price_'.$type_price;
			$args['meta_value'] = $query['price'];
		}
		elseif(!empty($query['price']) && 'sale' == $query['price'])
		{
			$args['meta_key'] = '_usam_old_price_'.$type_price;
			$args['meta_compare'] = '>';
			$args['meta_value'] = '0';
		}
		if(!empty($query['product_name']))
			$args['pagename'] = $query['product_name'];			
		if(!empty($query['category_id']))
		{
			$term = get_term($query['category_id'],'usam-category');
			$args['usam-category'] = $term->slug;
			$args['usam-category__in'] = $term->term_id;
		}
		if(!empty($query['category_url_name']))
			$args['usam-category'] = $query['category_url_name'];
				
		if ( isset( $query['orderby'] ) )		
			$orderby =	$query['orderby'];
		else 			
			$orderby = get_option('usam_product_sort_by');   // сортировка по умолчанию
		
		$args = array_merge( $args, usam_product_sort_order_query_vars($orderby) );

		if(!empty($query['order']))
			$args['order'] = $query['order'];
		if( !get_option('usam_product_pagination', 1) )
		{
			$args['nopaging'] = true;
			$args['posts_per_page'] = '-1';
		}
		else
		{	
			if(!empty($query['limit_of_items']) )
				$args['posts_per_page'] = $query['limit_of_items'];				
			elseif(!empty($query['number_per_page']) )
			{
				$args['posts_per_page'] = $query['number_per_page'];
				$args['paged'] = $query['page'];
			}
		}
		if(!empty($query['tag']))
			$args['product_tag'] = $query['tag'];	

		query_posts( $args );
	}
	ob_start();	
	if( isset($wp_query->post->post_type) && 'usam-product' == $wp_query->post->post_type && !is_archive() && $wp_query->post_count <= 1 )
	{
		while ( usam_have_products() ) : usam_the_product(); 
			usam_load_template("content-single_product");
		endwhile; 
	}
	else
		usam_include_products_page_template( false );

	$output = ob_get_contents();
	ob_end_clean();
	$output = str_replace('\$','$', $output);

	if ( ! empty( $query ) ) 
	{
		wp_reset_query();
		wp_reset_postdata();
	}
	return $output;
}

/**
 * Верните класс(ы), которые должны быть применены к тегу <a> изображения продукта.   
 * Если Thickbox эффект включен для изображения продуктов (настройка презентации), имя класса Thickbox включен
 * @since 3.8
 * @return string space-separated list of class names (for use in a class="") attribute
 */
function usam_the_product_image_link_classes() 
{
	$classes = array( );
	if ( get_option( 'usam_show_thumbnails_thickbox' ) )
		$classes[] = 'thickbox';

	$classes[] = 'preview_link';

	$classes = apply_filters( 'usam_the_product_image_link_classes', $classes );
	return implode( ' ', $classes );
}

/**
 * Функция ссылка на комментарии продукта
 */
function usam_product_comment_link() 
{	
	global $usam_query;

	$comment_system = get_option('usam_comment_system');
	if ( $comment_system == 'intensedebate' ) 
	{
		$product_id = get_the_ID();	
		
		$original = array( "&", "'", ":", "/", "@", "?", "=" );
		$entities = array( "%26", "%27", "%3A", "%2F", "%40", "%3F", "%3D" );
		$output = "<div class=\"clear comments\">
				<script src='https://www.intensedebate.com/js/getCommentLink.php?acct=".get_option( "usam_intense_debate_account_id" )."&postid=product_".$usam_query->product['id']."&posttitle=" . urlencode( get_the_title() ) . "&posturl=" . str_replace( $original, $entities, usam_product_url( $product_id ) ) . "&posttime=" . urlencode( date( 'Y-m-d h:i:s', time() ) ) . "&postauthor=author_".$product_id."' type='text/javascript' defer='defer'></script>
				</div>";
	}
	return $output;
}
/**
 * Функция комментирования для продуктов (старый способ)
 * @return string - javascript for the intensedebate comments
 */
function usam_product_comments() 
{
	$output = '';
	$comment_system = get_option('usam_comment_system');
	if ( $comment_system == 'intensedebate' ) 
	{
		$product_id = get_the_ID();		
		$output = "<script>
			var idcomments_acct = '" . esc_js( get_option( 'usam_intense_debate_account_id' ) ) . "';
			var idcomments_post_id = 'product_" . $product_id . "';
			var idcomments_post_url = encodeURIComponent('" . usam_product_url( $product_id ) . "');
			</script>
			<span id=\"IDCommentsPostTitle\" style=\"display:none\"></span>
			<script type='text/javascript' src='https://www.intensedebate.com/js/genericCommentWrapperV2.js'></script>";
	}
	return $output;
}

/**
 * Существует ли еще вариация, если сущетвует, то получить её
 */
function usam_have_variation_groups() 
{
	global $usam_variations;
	return $usam_variations->have_variation_groups();
}

/**
 * получить следущую вариацию
 */
function usam_the_variation_group() 
{
	global $usam_variations;
	$usam_variations->the_variation_group();
}

/**
 * Узнать существует ли следущий вариант вариации
 */
function usam_have_variations() 
{
	global $usam_variations;
	return $usam_variations->have_variations();
}

/**
 * получить следущий вариант вариации
 */
function usam_the_variation() 
{
	global $usam_variations;
	$usam_variations->the_variation();
}


function usam_product_has_multicurrency() 
{
	global $wpdb, $usam_query;

	$currency = usam_get_product_meta(get_the_ID(),'currency',true);
	if ( ! empty( $currency ) )
		return true;
	else
		return false;
}

// Вывести валюты
function usam_display_product_multicurrency() 
{
	global $wpdb, $usam_query;

	$results = usam_get_product_meta(get_the_ID(),'currency',true);
	if ( !empty( $results ) ) 
	{
		foreach ( (array)$results as $code => $curr ) 
		{
			$html = '<span class="usam_small pricefloatright pricedisplay">'.$code.': '.usam_currency_display( $curr, array( 'code' => $code ) ).'</span><br />';
			echo apply_filters( 'usam_display_product_multicurrency', $html, $code, $curr );
		}
	}
	return false;
}

/**
 * Получить название вариации
 */
function usam_the_vargrp_name() 
{
	// get the variation group name;
	global $usam_variations;
	return $usam_variations->variation_group->name;
}

/**
 * Получить ID формы вариации
 */
function usam_vargrp_form_id() 
{
	// generate the variation group form ID;
	global $usam_variations, $usam_variations;
	$product_id = get_the_ID();
	$props_id = "variation_select_{$product_id}_{$usam_variations->variation_group->term_id}";
	return $props_id;
}

/**
 * Получить ID вариации
 */
function usam_vargrp_id() 
{
	global $usam_variations;
	return $usam_variations->variation_group->term_id;
}

/**
 * получить название варианта вариации
 */
function usam_the_variation_name() 
{
	global $usam_variations;
	return esc_html( $usam_variations->variation->name );
}

/**
 * получить ID варианта вариации
 */
function usam_the_variation_id()
{
	global $usam_variations;
	return $usam_variations->variation->term_id;
}

/**
 * HTML отключить выбор опций и переключателей если нет запаса на складе
 */
function usam_the_variation_out_of_stock()
{
	global $usam_variations;
	$out_of_stock = false;

	$product_id = get_the_ID();
//	$stock = usam_get_product_meta( $product_id, 'stock', true );	
	if ( ($usam_variations->variation_group_count == 1) && isset( $usam_variations->variation->slug ) ) //&& is_numeric( $stock )
	{
		$product_id = get_the_ID();
		$variation_group_id = $usam_variations->variation_group->term_id;
		$variation_id = $usam_variations->variation->term_id;

		$wpq = array( 'variations' => $usam_variations->variation->slug, 'post_status' => 'publish', 'post_type' => 'usam-product',	'post_parent' => $product_id );
		$query = new WP_Query( $wpq );

		if ( $query->post_count != 1 ) {
			// Никогда не должно происходить
			return FALSE;
		}
		$variation_product_id = $query->posts[0]->ID;
		$stock = usam_get_product_meta( $variation_product_id, "stock" );	
		$stock = $stock[0];		
		if ( $stock == 0 && $stock !='' ) 
		{
			$out_of_stock = true;
		}	
	}
	if ( $out_of_stock == true )
		return "disabled='disabled'";
	else
		return '';
}


/**
 * Получить комплектацию товара
 */
function usam_display_product_components( $product_id = null )
{
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$output = '';
	$p_meta = usam_get_product_meta( $product_id, 'product_metadata' );	
	if ( !empty($p_meta['components']) ) 
	{
		$output .= '<table class = "table_product_components">';	
		$output .= "<tr><th class='name'>".__( 'Комплектация', 'usam' )."</th></tr>";		
		foreach( $p_meta['components'] as $id => $component )
		{				
			$output .= "<tr id='attribute-$id'><td class='c'>".$component['c']." х ".$component['title']."</td></tr>";
		}				
		$output .= '</table>';
	}	
	return $output;
}

/**
 * Получить характеристики товара.
 */
function usam_display_product_attributes( $product_id = '' )
{
	if ( $product_id == '' )
		$product_id = get_the_ID();
	$output = '';

	$terms = get_terms('usam-product_attributes', array( 'hide_empty' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sort_order') );
	if ( !empty($terms) ) 
	{
		$product_attributes = usam_get_product_attributes_display( $product_id );
		$ids = usam_get_product_term_ids( $product_id );	

		$ready_options = usam_get_product_attribute_values( array( 'attribute_id__in' => $ids ) );			
		$output .= '<table class = "table_product_characteristics">';		
		foreach( $terms as $term )
		{							
			if ( $term->parent == 0 )
			{						
				$row = '';
				foreach( $terms as $attr )
				{					
					if ( $term->term_id == $attr->parent && !empty($product_attributes[$attr->term_id]) )
					{ 
						$title = implode(', ', $product_attributes[$attr->term_id]);						
						$row .= "<tr id='attribute-".$attr->term_id."'><td class='name'>".$attr->name."</td><td class='value'>".$title."</td></tr>";
					}
				}
				if ( $row != '' )
				{
					$output .= "<tr id='attribute-".$term->term_id."'><th class='name' colspan='2'>".$term->name."</th></tr>".$row;
				}	
			}
		}
		$output .= '</table>';	
	}
	return $output;
}

// отображение маленьких картинок в просмотре продукта 
function usam_get_images_for_product( $product_id )
{
	$attachments = usam_get_product_images( $product_id );	
	$count = count($attachments);
	$out = '';
	if ( $count > 1 )
	{
		$items_body = 7;		
		$carousel = "<div class='jcarousel-wrapper carousel_thumbs'>";
		$carousel.= "<section class='carousel'>";
		$body = '';	
		foreach ($attachments as $attachment)
		{						
			$small = wp_get_attachment_image_src($attachment->ID, 'small-product-thumbnail');				
			$image_link_attributes = wp_get_attachment_image_src($attachment->ID, 'medium-single-product');
			$body .= "<li>
			<a data-zoom-id='Zoomer' href='".$image_link_attributes[0]."' data-image='".$attachment->guid."' class='gallery'>
				<img id='thumbnail_small-".$attachment->ID."' src='".$small[0]."' alt='".$attachment->post_title."' width='".$small[1]."' height='".$small[2]."'/>
			</a></li>";				
		}		
		if ($count > $items_body)
		{
			$display_button = "style = display:block";
		}
		else
		{			
			$items_body = $count;
			$display_button = "style = display:none";
		}			
		$button_up   = "<button class='up-carousel' $display_button><span></span></button>";
		$button_down = "<button class='down-carousel' $display_button><span></span></button>";
		$body_header = "<div id = 'carousel_thumbs' class='jcarousel' data-visible-items='$items_body' scroll-auto = 'false' ><ul>";	
		$body_footer = "</ul></div>";	
		$out = $carousel.$button_up.$body_header.$body.$body_footer.$button_down."</section></div>";		
	}
	return $out;
}


/**
 * Функция кнопки "Купить сейчас"
 * @return string - html отображение одного или нескольких продуктов
 */
function usam_buy_now_button( $product_id, $replaced_shortcode = false )
{
	if ( $replaced_shortcode )
		ob_start();
	
	if ( $product_id > 0 ) 
	{		
		$src = 'https://www.paypal.com/en_US/i/btn/btn_buynow_LG.gif';
		$src = apply_filters( 'usam_buy_now_button_src', $src );
		$classes = "usam-buy-now-form usam-buy-now-form-{$product_id}";
		$button_html = '<input class="usam-buy-now-button usam-buy-now-button-'. esc_attr( $product_id ) .'" type="image" name="submit" border="0" src='. esc_url( $src ) .' alt="'. esc_attr( 'PayPal - более безопасный и простой способ оплаты в Интернете', 'usam' ) .'" />';
		$button_html = apply_filters( 'usam_buy_now_button_html', $button_html, $product_id );			
		?>
		<form class="<?php echo esc_attr( $classes ); ?>" target="paypal" action="<?php echo esc_url( home_url() ); ?>" method="post">
			<input type="hidden" name="usam_buy_now_callback" value="1" />
			<input type="hidden" name="product_id" value="<?php echo esc_attr( $product_id ); ?>" />
			<?php if ( usam_has_multi_adding() ): ?>
				<label for="quantity"><?php esc_html_e( 'Количество', 'usam' ); ?></label>
				<input type="text" size="4" id="quantity" name="quantity" value="" /><br />
			<?php else: ?>
				<input type="hidden" name="quantity" value="1" />
			<?php endif ?>
			<?php echo $button_html; ?>
			<img alt='' border='0' width='1' height='1' src='https://www.paypal.com/en_US/i/scr/pixel.gif' />
		</form>
		<?php
	}
	if ( $replaced_shortcode )
		return ob_get_clean();
}



function usam_get_product_rating( $product_id = null, $vote_total = true, $no_grey = false ) 
{			
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$output = '';
	$rating_count = usam_get_product_meta( $product_id, 'rating_count' );		
	$p_rating = usam_get_product_meta( $product_id, 'rating' );
	if ( $no_grey && empty($p_rating) )
		return '';
	
	$p_rating = $p_rating > 6 ? 5 : $p_rating;
	
	$output .= "<span itemprop='reviews' class='rating product_rating' data-product_id='".$product_id."'>";
	for ( $l = 1; $l <= $p_rating; ++$l )
	{
		$output .= "<span class='star selected'></span>";
	}
	$remainder = 5 - $p_rating;
	for ( $l = 1; $l <= $remainder; ++$l ) {
		$output .= "<span class='star'></span>";
	}
	if ( $vote_total )	
		$output .= "<span class='vote_total'>&nbsp;(<span itemprop='ratingValue' id='vote_total_{$product_id}'>" . $rating_count . "</span>)</span> \r\n";
	$output .= "</span> \r\n";
	return $output;
}


function usam_product_rating( $vote_total = false, $no_grey = true ) 
{
	$product_id = get_the_ID();
	echo usam_get_product_rating( $product_id, $vote_total, $no_grey );
}

/**
 * Вывести рейтинг товара.
 */
function usam_product_new_rating()
{			
	$output = '';	
	if ( get_option('usam_show_product_rating', 1) ) 
	{
		$output .= "<div class='product_rating_selected'>";
		$output .= "<span class='product_rating_text'>" . __( 'Ваш рейтинг', 'usam' ) . ":</span>";
		$output .= usam_get_product_new_rating( );
		$output .= "</div>";
	}
	echo $output;
}

function usam_get_product_new_rating( $product_id = null )
{
	global $wpdb;
	
	if ( $product_id == null )
		$product_id = get_the_ID();
	
	$previous_vote = 1;
	if ( isset($_COOKIE['prating'][$product_id]) &&  is_numeric( $_COOKIE['prating'][$product_id] ) )	
		$p_rating = absint( $_COOKIE['prating'][$product_id] );		
	else
		$p_rating = (int)usam_get_product_meta( $product_id, 'rating', true );
	
	$output = "<span class='your_product_rating rating' data-product_id='".$product_id."'>";
	for ( $l = 1; $l <= 5; ++$l )
	{
		$output .= "<span class='star'></span>";
	}
	$output .= "</span>";	
	return $output;
}

/**
 * Используется ли разбиение на страницы
 */
function usam_has_pages()
{
	if( get_option('usam_product_pagination', 1) )
		return true;
	else
		return false;
}
/**
 * Добавить поле "количество" для каждого описания товара
 */
function usam_has_multi_adding() 
{	
	if ( get_option('usam_show_multi_add') )
		return true;
	else 
		return false;	
}
/**
 * функция возвращает количество выведенных продуктов
 */
function usam_product_count() 
{
	global $wp_query;
	
	return !empty($wp_query->posts)?count($wp_query->posts):0;
}

/**
 * show category description
 */
function usam_show_category_description()
{
	return get_option('usam_category_description', '' );
}

function usam_remove_currency_code( $args ) 
{
	$args['display_currency_symbol'] = false;
	$args['display_currency_code']  = false;
	return $args;
}

function usam_you_save( $args = null )
{
	$defaults = array(
		'product_id' => false,
		'type'       => 'percentage',
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args, EXTR_SKIP );
	if ( ! $product_id )		
		$product_id = usam_the_product_id();	

	if ( ! $product_id )
		return 0;
	
	$price = usam_get_product_price( $product_id );
	$old_price = usam_get_product_old_price( $product_id );

	switch( $type )
	{
		case "amount":
			return $old_price - $price;
			break;
		default:
			if ( $old_price == 0 )
				return 0;
			else
				return round( ( $old_price - $price ) / $old_price * 100, 0);
	}
}

function usam_get_downloadable_file($file_id)
{
	return get_post( $file_id );
}

function usam_select_product_variation()
{
	usam_load_template( 'product-variation' );
}

function usam_hide_addtocart_button(  )
{	
	if ( get_option('usam_hide_addtocart_button', 0) ) 
		return false;
	else
		return true;
}

function usam_addtocart_button( $product_id = null )
{		
	if ( $product_id == null )
		$product_id = get_the_ID();
	?>
	<div class="usam_button_buy_wrapper">		
	<?php
	if( usam_hide_addtocart_button() && usam_product_has_stock( $product_id ) ) 
	{ 											
		if( usam_product_external_redirect( $product_id ) ) 
		{
			?>													
			<input class="button button_buy" type="submit" value="<?php echo usam_product_external_link_text( $product_id, __( 'Купить сейчас', 'usam' ) ); ?>" onclick="return gotoexternallink('<?php echo usam_product_external_link( $product_id ); ?>', '<?php echo usam_product_external_link_target( $product_id ); ?>')">
			<?php 
		} 
		elseif ( usam_get_product_price( $product_id ) > 0 )
		{
			?>								
				<input type="submit" value="<?php _e('Добавить в корзину', 'usam'); ?>" name="buy" class="button button_buy" id="product_<?php echo $product_id ?>_submit_button"/>
				<input type="hidden" value="add_to_cart" name="usam_action"/>
				<input type="hidden" value="<?php echo $product_id ?>" name="product_id"/>		
			<?php 
		} 	
	} 
	?>
	</div>		
	<?php
}

function usam_quick_purchase_button( $product_id = null )
{		
	if ( $product_id == null )
		$product_id = get_the_ID();
	?>
	<div class="usam_button_quick_purchase_wrapper">		
	<?php
	if( usam_hide_addtocart_button() && usam_product_has_stock( $product_id ) && !usam_product_external_redirect( $product_id ) && usam_get_product_price( $product_id ) > 0 ) 
	{ 			
		?>	
			<a href="#quick_purchase" id = "usam_quick_purchase" class="usam_modal_feedback button"><?php _e('Быстрая покупка', 'usam'); ?></a>
		<?php 
	} 
	?>
	</div>		
	<?php
}


function usam_get_buy_product_button( $product_id, $attr = array() )
{		
	$url = add_query_arg( array('product_id' => $product_id, 'usam_action' => 'buy_product', 'empty' => 1) , usam_get_url_system_page('basket') );
	$attr['href'] = $url;
	
	$out =  "<a ";
	foreach ($attr as $key => $value )
		$out .= " $key='$value'";
	
	$out .= ">" . __( 'Купить', 'usam' ) . "</a>";
	return $out;
}

function usam_the_product_price_display( $args = array() )
{	
	if ( empty( $args['id'] ) )
		$id = get_the_ID();
	else
		$id = (int) $args['id'];
	
	$current_price       = usam_get_product_price_currency( $id );
	if ( !$current_price )
		return false;

	$defaults = array(
		'id' => $id,
		'old_price_text'   => __( 'Старая цена: %s', 'usam' ),
		'price_text'       => __( 'Цена: %s', 'usam' ),
		/* translators     : %1$s is the saved amount text, %2$s is the saved percentage text, %% is the percentage sign */
		'you_save_text'    => __( 'Вы экономите: %s', 'usam' ),
		'old_price_class'  => 'pricedisplay usam-product-old-price ' . $id,
		'old_price_before' => '<div %s>',
		'old_price_after'  => '</div>',
		'old_price_amount_id'     => 'old_product_price_' . $id,
		'old_price_amount_class' => 'oldprice',
		'old_price_amount_before' => '<span class="%1$s" id="%2$s">',
		'old_price_amount_after' => '</span>',
		'price_amount_id'     => 'product_price_' . $id,
		'price_class'  => 'pricedisplay usam-product-price ' . $id,
		'price_before' => '<div itemprop="price" %s>',
		'price_after' => '</div>',
		'price_amount_class' => 'currentprice pricedisplay ' . $id,
		'price_amount_before' => '<span class="%1$s" id="%2$s">',
		'price_amount_after' => '</span>',
		'you_save_class' => 'pricedisplay usam-product-you-save product_' . $id,
		'you_save_before' => '<div %s>',
		'you_save_after' => '</div>',
		'you_save_amount_id'     => 'yousave_' . $id,
		'you_save_amount_class' => 'yousave',
		'you_save_amount_before' => '<span class="%1$s" id="%2$s">',
		'you_save_amount_after'  => '</span>',
		'output_price'     => true,
		'output_old_price' => true,
		'output_you_save'  => true,
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r );
	
	$old_price           = usam_get_product_price_currency( $id, true );
	$you_save_percentage = usam_you_save();
	$you_save            = usam_currency_display( usam_you_save( 'type=amount' ) ) . '! ( ' . $you_save_percentage . '% )';

	// replace placeholders in arguments with correct values
	$old_price_class = apply_filters( 'usam_the_product_price_display_old_price_class', $old_price_class, $id );
	$old_price_amount_class = apply_filters( 'usam_the_product_price_display_old_price_amount_class', $old_price_amount_class, $id );
	$attributes = 'class="' . esc_attr( $old_price_class ) . '"';
	
	if (  $old_price == '' )
		$attributes .= ' style="display:none;"';
	
	$old_price_before = sprintf( $old_price_before, $attributes );
	$old_price_amount_before = sprintf( $old_price_amount_before, esc_attr( $old_price_amount_class ), esc_attr( $old_price_amount_id ) );

	$price_class = 'class="' . esc_attr( apply_filters( 'usam_the_product_price_display_price_class', esc_attr( $price_class ), $id )  ) . '"';
	$price_amount_class = apply_filters( 'usam_the_product_price_display_price_amount_class', esc_attr( $price_amount_class ), $id );
	$price_before = sprintf( $price_before, $price_class );
	$price_amount_before = sprintf( $price_amount_before, esc_attr( $price_amount_class ), esc_attr( $price_amount_id ) );

	$you_save_class = apply_filters( 'usam_the_product_price_display_you_save_class', $you_save_class, $id );
	$you_save_amount_class = apply_filters( 'usam_the_product_price_display_you_save_amount_class', $you_save_amount_class, $id );
	$attributes = 'class="' . esc_attr( $you_save_class ) . '"';
	
	if (  $old_price == '' )
		$attributes .= ' style="display:none;"';
	$you_save_before = sprintf( $you_save_before, $attributes );
	$you_save_amount_before = sprintf( $you_save_amount_before, esc_attr( $you_save_amount_class ), esc_attr( $you_save_amount_id ) );

	$old_price     = $old_price_amount_before . $old_price . $old_price_amount_after;
	$current_price = $price_amount_before . $current_price . $price_amount_after;
	$you_save      = $you_save_amount_before . $you_save . $you_save_amount_after;

	$old_price_text = $old_price;
	$price_text     = $current_price;
	$you_save_text  = sprintf( $you_save_text, $you_save );

	if ( $output_old_price )
		echo $old_price_before . $old_price_text . $old_price_after . "\n";

	if ( $output_price )
		echo $price_before . $price_text . $price_after . "\n";
	
	if ( $output_you_save )
		echo $you_save_before . $you_save_text . $you_save_after . "\n";
}

function usam_products_same_category( $items_body = 4, $items_body_all = 14, $callback = '' ) 
{	
	if ( get_option( 'usam_show_products_same_category', 1 ) == 0 ) 		
		return '';	
	?>
	<div class = "usam_products_same_category">			
		<h3 class="prodtitles"><?php _e('Товары из выбранной категории', 'usam'); ?></h3>
		
	<?php usam_get_related_products('cat', $items_body = 4, $items_body_all = 14, $callback = '' ); ?>
	</div>
	<?php
}

function usam_display_cross_sells()
{ 
	global $usam_cart;	
	$products_ids = array();
	while (usam_have_cart_items())
	{
		usam_the_cart_item();	
		$crosssell = usam_get_associated_products( $usam_cart->cart_item->product_id, 'crosssell' );
		if ( !empty($crosssell))
		{
			$products_ids = array_merge($products_ids, $crosssell);	
		}				
	}	
	$products_ids = array_unique($products_ids);
	if ( !empty($products_ids) )
	{	
		$args = array( 
			'post__in' => array_values($products_ids), 
			'update_post_meta_cache' => true, 
			'update_post_term_cache' => true, 
			'cache_results' => true, 
			'post_status' => 'publish',
			'meta_query'=> array( array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '!=' ) ) 
		);
		$products = usam_get_products( $args, true );	
		if ( !empty($products) )
		{					
			include( usam_get_template_file_path( 'cross_sells' ) );
		}
	}	
}


final class USAM_Product_Tabs
{
	private $current_tab_id = '';
	private $tab = array();
	private $tabs = array();
	private $product_tabs = array();
	
	private $messages = array();
	private $errors = array();
	
	public function __construct() 
	{
		$this->init();
	}
	
	public function init()
	{			
		$product_tabs = array(
			array(
				'menu_title'  => __( 'Характеристики', 'usam' ),				
				'slug'        => 'characteristics',
			),			
			array(
				'menu_title'  => __( 'Описание', 'usam' ),	
				'slug'        => 'description',
			),
			array(
				'menu_title'  => __( 'Бренд', 'usam' ),
				'slug'        => 'brand',			
			),
			array(
				'menu_title'  => __( 'Отзывы', 'usam' ),
				'slug'        => 'comment',
			),					
		);		
		$default_view_tab = array( 'characteristics' => 1, 'description' => 1, 'brand' => 1, 'comment' => 1 );
		$view_tab = get_option('usam_view_tab', array());
		$view_tab = array_merge( $default_view_tab, $view_tab );	
		
		$brand_description = usam_brand_description(); 	
		$view_tab['brand'] = !empty($brand_description)?$view_tab['brand']:$brand_description;
		$view_tab['comment'] = usam_show_product_comments()?$view_tab['comment']:0;		
		
		foreach ( $product_tabs as $menu )
		{
			if ( !empty($view_tab[$menu['slug']]) )
				$this->product_tabs[] = $menu;
		}
		$this->product_tabs = apply_filters( 'usam_product_tabs', $this->product_tabs );
	
		$this->set_current_tab( );
	}
	
	public function set_current_tab( $tab_id = null )
	{		
		if ( empty($this->tabs) )
			return;
		
		foreach ( $this->product_tabs as $menu )
		{ 
			if ( $submenu['slug'] == $tab_id || empty($tab_id) )
			{
				$this->current_tab_id = $menu['slug'];	
				$this->tab = $menu;	
				break;
			}						
		}		
		if ( empty($this->current_tab_id) ) 
			$this->set_current_tab( );
	}		
	
	public function display_header_tabs() 
	{		
		?>	
		<div class="header_tab usam_product_tabs_header">			
			<ul>
			<?php		
			foreach ( $this->product_tabs as $menu )
			{
				?>
					<li id="product-<?php echo $menu['slug']; ?>" class="tab <?php echo ($this->current_tab_id == $menu['slug'])?'current_menu':''; ?> usam_menu-<?php echo $menu['slug']; ?>">
						<a href='#product-<?php echo $menu['slug']; ?>'><h2><?php echo $menu['menu_title']; ?></h2></a>
					</li>
				<?php
			}
			?>
			</ul>
		</div>		
		<?php
	}
	
	public function display_tab( $tab ) 
	{
		?>
		<div class = "product_tab product_tab_<?php echo $tab; ?>">
		<?php 	
		switch( $tab )
		{
			case 'characteristics':
				echo usam_the_product_description();
			break;
			case 'description':		
				?>
				<p itemprop="description"><?php 
					$description = usam_the_product_additional_description();
					if ( $description )
						echo $description; 
					else 
						_e('К сожалению нет описания. Возможно оно скоро появится. Попробуйте посмотреть позже...','usam');
					?>						
				</p>
				<?php 
			break;	
			case 'brand':		
				echo usam_brand_description(); 		
			break;			
			case 'comment':		
				usam_reviews( false ); 
			break;						
		}	
		?>	
		</div>
		<?php 			
	}
	
	public function display_content_tabs() 
	{		
		echo '<div class = "countent_tabs">';			
		foreach ( $this->product_tabs as $menu )
		{
			?>
			<div id = "product-<?php echo $menu['slug']; ?>" class = "tab">
				<?php $this->display_tab( $menu['slug'] ); ?>
			</div>
			<?php 
		}		
		echo '</div>';
	}

	private function view_tabs() 
	{	
		echo '<div class = "usam_tabs">';
		$this->display_header_tabs();
		$this->display_content_tabs();	
		echo '</div>';	
	}	
	
	private function view_list() 
	{	
		foreach ( $this->product_tabs as $menu )
		{
			?>
			<div class="usam_product_parameter">
				<h2><?php echo $menu['menu_title']; ?></h2>
				<?php $this->display_tab( $menu['slug'] ); ?>
			</div>
			<?php
		}	
	}	
	
	public function display( $view ) 
	{		 
		if( wp_is_mobile() )
		{				
			$view = 'list';
		}
		echo '<div id = "usam_parameters_products" class = "parameters_products">';
			$method = 'view_'.$view;				
			if ( method_exists($this, $method) )
			{
				$html = $this->$method();	
			}					
		echo '</div>';
	}
}

function usam_product_tabs( $view = 'tabs' ) 
{
	$ptab = new USAM_Product_Tabs();
	$ptab->display( $view );
}

function usam_products_for_buyers() 
{	
	$default = array( 
		array( 'type' => 'also_bought', 'title' => __( 'С этим товаром покупали', 'usam' ), 'template' => 'simple_list', 'active' => true, 'number' => 7 ),			
		array( 'type' => 'related_products', 'title' => __( 'К этому товару подойдет', 'usam' ), 'template' => 'carousel', 'active' => true, 'number' => 14 ),
		array( 'type' => 'same_category', 'title' => __( 'Товары в той же категории', 'usam' ), 'template' => 'carousel', 'active' => true, 'number' => 14 ),
		array( 'type' => 'history_views', 'title' => __( 'История просмотра товаров', 'usam' ), 'template' => 'simple_list', 'active' => true, 'number' => 7 ),
	);	
	$pbuyers = get_option('usam_products_for_buyers', $default);		
	foreach ( $pbuyers as $value )
	{	
		if ( $value['active'] )
			usam_display_product_groups( array('title' => $value['title'], 'query' => $value['type'], 'template' => $value['template'], 'limit' => $value['number']) );			
	}
}


/* ================================================  Список желаний ========================================================*/	
/**
 * Отображает список желаний
 */
function usam_desired_product()
{	
	$output = "<div id='usam_desired_product' class='usam_desired_product usam_products'>".usam_get_loader()."</div>";
	?>
	<script type="text/javascript">
		jQuery(document).ready(function()
		{														
			var data = { usam_ajax_action: 'get_desired_product' };			
			usam_get_products(data, 'usam_desired_product' );
		});
	</script>
	<?php	
	echo $output;
}

function usam_get_display_desired_products( $product_ids )
{	
	$width = 160;
	$height = 160;
	$output = '';	
	$products = usam_get_products( array( 'post_status' => 'publish', 'post__in' => $product_ids, 'orderby' => 'post__in', 'update_post_term_cache' => false ));
	foreach ( $products as $product )
	{			
		$old_price = usam_get_product_old_price( $product->ID );	
		$price = usam_get_product_price( $product->ID );	
		$output .= "<div class='usam_desired_product_item'>";
		$output .= "<a href='".usam_product_url($product->ID)."' class='preview_link'>";	
		$output .= usam_get_product_thumbnail( $product->ID, array($width, $height) );					
		$output .= "<div class = 'product_title'>".$product->post_title."</div>";			
		if( !empty($old_price) )										
			$output .= '<div class = "price price_sale">'. usam_get_product_price_currency( $product->ID, true ) .'</div>';				
		if ( !empty($price) )									
			$output .='<div class = "price">' .usam_get_product_price_currency( $product->ID ) . '</div>';				
		$output .= "</a>";
		$output .= "</div>";
	}		
	return $output;
}