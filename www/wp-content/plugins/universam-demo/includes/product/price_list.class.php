<?php
/**
 * Скачать прайс лист
 */
class USAM_Price_List
{		
	private $option = array(); 
	private $size = 500; 
	public function __construct( $file )
	{		
		$options = get_option('usam_price_list_setting', array() );		
		if ( isset($options[$file]) )
			$this->option = $options[$file];
	}

	public function download() 
	{		
		if ( $this->check_availability() )
		{					
			global $wpdb, $type_price;	

			ini_set("max_execution_time", "3660");		
			
			$chunk_size = apply_filters ( 'usam_productlist_chunk_size', $this->size );	
			$args = array(			
				'post_status'   => array('publish'),
				'posts_per_page'   => $chunk_size,
				'offset'        => 0,	
				'cache_results' => false,
				'update_post_meta_cache' => true,
				'update_post_term_cache' => true,	
			);
			
			if ( $this->option['product_group'] != 'all' )
			{
				$args['tax_query'] = array();
				if ( !empty($this->option['tax']['category']) )				
					$args['tax_query'][] = array('taxonomy' => 'usam-category', 'field' => 'id', 'terms' => $this->option['tax']['category'] );
				if ( !empty($this->option['tax']['brands']) )
					$args['tax_query'][] = array('taxonomy' => 'usam-brands', 'field' => 'id', 'terms' => $this->option['tax']['brands'] );
			}							
			$args = apply_filters( 'usam_productlist_query_args', $args );		
			$products = usam_get_products( $args );			
			
			$productslist = array();
			while ( count ( $products ) ) 
			{
				foreach ($products as $product) 
				{					
					$product_import = array();					
					if ( !empty($this->option['column']['price']) )
					{
						$product_import['price'] = usam_get_product_price( $product->ID, $type_price );
					}	
					if ( !empty($this->option['column']['sku']) )
					{
						$product_import['sku'] = usam_get_product_meta( $product->ID, 'sku' );
					}	
					if ( !empty($this->option['column']['stock']) )
					{
						$product_import['stock'] = usam_get_product_meta( $product->ID, 'stock' );
					}	
					if ( !empty($this->option['column']['stock_title']) )
					{
						$stock = usam_get_product_meta( $product->ID, 'stock' );
						if ( $stock )
							$product_import['stock_title'] = __('В наличии','usam');
						else
							$product_import['stock_title'] = __('Нет наличии','usam');
					}				
					if ( !empty($this->option['column']['title']) )
					{
						$product_import['title'] = $product->post_title;
					}	
					if ( !empty($this->option['column']['link']) )
					{
						$product_import['link'] = $product_url = usam_product_url( $product->ID );
					}
					if ( !empty($this->option['column']['image']) )
					{
						$post_thumbnail_id = get_post_thumbnail_id( $product->ID );
						$thumbnail = wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); 
						$product_import['image'] = $thumbnail[0]; 
					}
					if ( !empty($this->option['column']['brands']) )
					{
						$brands_list = wp_get_post_terms($product->ID, 'usam-brands', array("fields" => "names"));
						$product_import['brands'] = implode( ', ', $brands_list );
					}
					if ( !empty($this->option['column']['category']) )
					{
						$category_list = wp_get_post_terms($product->ID, 'usam-category', array("fields" => "names"));	
						$product_import['category'] = implode( ', ', $category_list );
					}
					$productslist[] = $product_import;
				}			
				$args['offset'] += $chunk_size;
				$products = usam_get_products ( $args );
			}	
			unset($products);
			$columns = array( 'title' => __('Название товара','usam'), 'category' => __('Название категории','usam'), 'brands' => __('Бренд','usam'), 'price' => __('Цена','usam'), 'sku' => __('Артикул','usam'), 'stock' => __('Остаток точный','usam'), 'stock_title' => __('Статус остатка','usam'), 'link' => __('Ссылка на товар','usam'), 'image' => __('Ссылка на картинку','usam'));
			foreach ( $this->option['column'] as $column )
			{
				$headers[$column] = $columns[$column];
			}
			usam_write_exel_file( 'price_list', $productslist, $headers );	
		}			
	}	
	
	private function check_availability() 
	{		
		global $user_ID;
		if ( !empty($this->option) )
		{
			if ( $this->option['status'] == 0 )
				return false;			
			
			$user = get_userdata( $user_ID );	
			$result = array_intersect($this->option['roles'], $user->roles);		
			if ( !empty($result) )					
				return true;				
		}	
		return false;		
	}	
}
?>