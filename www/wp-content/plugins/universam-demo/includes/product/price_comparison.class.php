<?php
class USAM_Price_Comparison
{	
	 // ���������
	private static $string_cols = array(
		'url',		
		'mail',		
		'phone',
		'name',		
		'type',
		'description',			
		'date_modified',		
		'date_insert',	
	);
	// ��������
	private static $int_cols = array(
		'id',		
		'product_id',				
		'user_id',	
		'manager_id',	
		'status',
	);
	// ������������
	private static $float_cols = array(	
		'price_found',
	);	
	/**
	 * �������� �������� ����������� �� ��
	 * @since 4.9
	 */		
	private $data     = array();		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // ���� ���������� ������ � ��
	
	/**
	 * ����������� �������
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', ) ) )
			return;		
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_price_comparison' );
		}		
		// ��� ����������
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * ��������� � ��� ���������� ������
	*/
	public static function update_cache( &$_price_comparison ) 
	{
		$id = $_price_comparison->get( 'id' );	
		wp_cache_set( $id, $_price_comparison->data, 'usam_price_comparison' );		
		do_action( 'usam_price_comparison_update_cache', $_price_comparison );
	}

	/**
	 * ������� ���	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$_price_comparison = new USAM_Price_Comparison( $value, $col );
		wp_cache_delete( $_price_comparison->get( 'id' ), 'usam_price_comparison' );				
		do_action( 'usam_price_comparison_delete_cache', $_price_comparison, $value, $col );	
	}		
	
	/**
	 *  ������� �������� ��������
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_price_comparison_before_delete', $this );
		
		$id = $this->get( 'id' );	
		self::delete_cache( $id );				
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE id = '$id'");
		
		do_action( 'usam_price_comparison_delete', $id );
	}		
	
	/**
	 * �������� ����������� ������ �� ���� ������
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_price_comparison_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_price_comparison_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * ���� ������ ���������� � ��
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * ���������� �������� ���������� ��������
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_price_comparison_get_property', $value, $key, $this );
	}
	
	/**
	 * ���������� ������ ������ �� ���� ������ � ���� �������������� �������
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_price_comparison_get_data', $this->data, $this );
	}

	/**
	 * ������������� �������� �� ������������� ��������. ��� ������� ��������� ���� � �������� � �������� ����������, ��� ������������� ������, ���������� ���� ����-��������.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_price_comparison_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * ���������� ������, ���������� ����������������� ���������
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * ��������� � ���� ������	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_price_comparison_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	

			if ( isset($this->data['date_insert']) )
				unset($this->data['date_insert']);
			
			do_action( 'usam_price_comparison_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $where_col => $where_val );			

			$this->data = apply_filters( 'usam_price_comparison_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );		
			
			$result = $wpdb->update( USAM_TABLE_PRICE_COMPARISON, $this->data, $where, $format, $where_format );			
			do_action( 'usam_price_comparison_update', $this );
		} 
		else 
		{   
			do_action( 'usam_price_comparison_pre_insert' );		
			unset( $this->data['id'] );	
						
			if ( !isset($this->data['status']) )
				$this->data['status'] = 0;
			
			if ( !isset($this->data['manager_id']) )
				$this->data['manager_id'] = 0;
			
			if ( !isset($this->data['mail']) )
				$this->data['mail'] = '';
						
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );			
			
			$this->data = apply_filters( 'usam_price_comparison_insert_data', $this->data );			
			$format = $this->get_data_format(  );		
					
			$this->data_format( );							
			$result = $wpdb->insert( USAM_TABLE_PRICE_COMPARISON, $this->data, $format );
					
			if ( $result ) 
			{						
				$this->set( 'id', $wpdb->insert_id );			
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );				
			}
			do_action( 'usam_price_comparison_insert', $this );
		} 		
		do_action( 'usam_price_comparison_save', $this );

		return $result;
	}
}

// ��������
function usam_update_price_comparison( $id, $data )
{
	$price_comparison = new USAM_Price_Comparison( $id );
	if ( !isset($data['manager_id']) && $price_comparison->get('manager_id') == 0 )
	{
		global $user_ID;
		$data['manager_id'] = $user_ID;		
	}	
	$price_comparison->set( $data );
	return $price_comparison->save();
}

// ��������
function usam_get_price_comparison( $id, $colum = 'id' )
{
	$price_comparison = new USAM_Price_Comparison( $id, $colum );
	return $price_comparison->get_data( );	
}

// ��������
function usam_insert_price_comparison( $data )
{
	$price_comparison = new USAM_Price_Comparison( $data );
	return $price_comparison->save();
}

// �������
function usam_delete_price_comparison( $id )
{
	$price_comparison = new USAM_Price_Comparison( $id );
	return $price_comparison->delete();
}
?>