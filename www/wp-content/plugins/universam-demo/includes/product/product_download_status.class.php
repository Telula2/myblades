<?php
class USAM_PRODUCT_DOWNLOAD_STATUS
{	
	 // строковые
	private static $string_cols = array(
		'unique_id',		
		'ip_number',		
		'active',
		'file_name',		
		'date_modified',			
	);
	// цифровые
	private static $int_cols = array(
		'id',						
		'order_id',			
		'basket_id',
		'downloads',
		'fileid',		
	);
	// рациональные
	private static $float_cols = array(
	
	);	
	/**
	 * Содержит значения извлекаются из БД
	 * @since 4.9
	 */	
	private $data    = array();		
	private $fetched = false;
	private $args    = array( 'col'   => '', 'value' => '' );	
	private $exists  = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'unique_id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'unique_id'  && $id = wp_cache_get( $value, 'usam_product_download_unique_id' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}				
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_product_download_status' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$ds ) 
	{
		$id = $ds->get( 'id' );
		wp_cache_set( $id, $ds->data, 'usam_product_download_status' );
		if ( $unique_id = $ds->get( 'unique_id' ) )
			wp_cache_set( $unique_id, $id, 'usam_product_download_unique_id' );		
		do_action( 'usam_product_download_status_update_cache', $ds );
	}

	/**
	 * Удалить кеш	 
	 */
	public function delete_cache( ) 
	{
		wp_cache_delete( $this->get( 'id' ), 'usam_product_download_status' );	
		wp_cache_delete( $this->get( 'unique_id' ), 'usam_product_download_unique_id' );	
		do_action( 'usam_product_download_status_delete_cache', $this );	
	}

	/**
	 * Удалить
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		$id = $this->get('id');		
		
		do_action( 'usam_product_download_status_before_delete', $id );
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_DOWNLOAD_STATUS." WHERE id = '$id'");
		self::delete_cache( );		
		do_action( 'usam_product_download_status_delete', $this );
	}		
	
	/**
	 * Выбирает фактические записи
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_DOWNLOAD_STATUS." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$this->exists = true;
			$this->data = apply_filters( 'usam_product_download_status_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}		
		do_action( 'usam_product_download_status_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_product_download_status_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_product_download_status_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();	
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}		
		$this->data = apply_filters( 'usam_product_download_status_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_product_download_status_pre_save', $this );	
		$where_col = $this->args['col'];
		
		$this->data['date_modified'] = date( "Y-m-d H:i:s" );		
		
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );
		
			do_action( 'usam_product_download_status_pre_update', $this );			

			$this->data = apply_filters( 'usam_product_download_status_update_data', $this->data );			
			$format = $this->get_data_format( );
			$this->data_format( );
		
			$result = $wpdb->update( USAM_TABLE_DOWNLOAD_STATUS, $this->data, array( $where_col => $where_val ), $format, $where_format );
			self::delete_cache( $where_val, $where_col );
			do_action( 'usam_product_download_status_update', $this );
		} 
		else 
		{  
			do_action( 'usam_product_download_status_pre_insert' );		
			if ( isset($this->data['id']) )
				unset( $this->data['id'] );	
			
			$this->data['unique_id'] = sha1(uniqid(mt_rand(), true));	
			if ( !isset($this->data['active']) )
				$this->data['active'] = 0;
			
			$this->data = apply_filters( 'usam_product_download_status_insert_data', $this->data );		
			$format = $this->get_data_format(  );
			$this->data_format( );			
	
			$result = $wpdb->insert( USAM_TABLE_DOWNLOAD_STATUS, $this->data, $format );
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;
				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_product_download_status_insert', $this );
		} 		
		do_action( 'usam_product_download_status_save', $this );

		return $result;
	}
}

function usam_insert_product_download_status( $args ) 
{
	$download_status = new USAM_PRODUCT_DOWNLOAD_STATUS( $args );
	$download_status->save();
}


function usam_get_downloadable_links( $purchase_log ) 
{
	if ( ! $purchase_log->is_transaction_completed() )
		return array();

	$files = $purchase_log->get_downloadable_files();	
	$links = array();
	foreach ( $files as $file ) 
	{
		$downloadid = is_null( $file->unique_id ) ? $file->id : $file->unique_id;		
		$links[$file->basket_id] = array('url' => add_query_arg( array( 'downloadid' => $downloadid, 'usam_action' => 'download_file') , site_url() ), 'name' => $file->file_name );
	}	
	$products = $purchase_log->get_order_products();	
	foreach ( $products as $product ) 
	{
		if ( isset($links[$product->id]) )
			$links[$product->id]['product_name'] = $product->name;
	}
	return $links;
}
?>