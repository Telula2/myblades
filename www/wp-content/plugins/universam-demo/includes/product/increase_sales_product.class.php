<?php
// Расчитать товары для увеличения продаж заданного товара
class USAM_Increase_Sales_Product
{	
	private $product_id;
	private $type_prices;
	private $limit_upsell = 20;
	private $limit_crosssell = 20;
	private $limit_down = 20;
	
	public function __construct( $product_id ) 
	{		
		$this->product_id = $product_id;
		$this->type_prices = usam_get_prices( array('type' => 'R') );			
	}		
	
	 //Рассчитать перекрестные продажи (Cross-Sells) в корзине
	public function cross_sell( )
	{		
		global $wpdb;	
		
		$option = get_option('usam_crosssell_conditions', array() );						
		$rules = maybe_unserialize($option);	
		
		$limit_cross_sells = 10;		
		
		/*foreach ( $this->type_prices as $value )
		{
			$code_price = $value['code'];
			break;
		}*/				
		$post = get_post( $this->product_id );
		$post_title = mb_strtolower($post->post_title);	
		foreach( $rules as $rule )
		{
			if ( empty($rule['active']) )
				continue;
		//	print_r($rule); echo "<br>";
			$work = false;
			foreach( $rule['words'] as $word )
			{
				$pos = strpos( $post_title, mb_strtolower($word) );					
				if ($pos !== false) 
				{					
					$work = true;
					break;
				}
			}	
			if ( $work ) 
			{						
				$joins = array();
				$where = '';
				$count = count($rule['conditions'])-1;
				foreach( $rule['conditions'] as $key => $condition )
				{						
					$value = $condition['value'];	
					if ( $condition['logic_operator'] == 'AND' )
						$relation = 'AND';
					else				
						$relation = 'OR';
					if ( $condition['type'] == 'name' )
					{												
						switch (  $condition['logic'] ) 
						{
							case 'equal' :										
								$compare = '=';
								$value = "'$value'";
							break;
							case 'not_equal' :										
								$compare = '!=';
								$value = "'$value'";
							break;						
							case 'contains' :										
								$compare = 'LIKE LOWER ';
								$value = "('%$value%')";
							break;
							case 'not_contain' :										
								$compare = 'NOT LIKE ';
								$value = "'%$value%'";
							break;
							case 'begins' :										
								$compare = 'LIKE LOWER ';
								$value = "('$value%')";
							break;
							case 'ends' :										
								$compare = 'LIKE LOWER ';
								$value = "('%$value')";
							break;
						}						
						$where .= " (p.post_title $compare {$value}) ";	
					}
					elseif ( $condition['type'] == 'attr' )
					{
						$attribute = usam_get_product_meta( $this->product_id, 'product_attributes_'.$value );	
						switch (  $condition['logic'] ) 
						{
							case 'equal' :										
								$compare = '=';
							break;
							case 'not_equal' :										
								$compare = '!=';
							break;
							case 'greater' :										
								$compare = '>';
							break;
							case 'less' :										
								$compare = '<';
							break;
							case 'eg' :										
								$compare = '>=';
							break;
							case 'el' :										
								$compare = '=<';
							break;
							case 'contains' :										
								$compare = 'LIKE ';
								$attribute = "%$attribute%";
							break;
							case 'not_contain' :										
								$compare = 'NOT LIKE ';
								$attribute = "%$attribute%";
							break;
							case 'begins' :										
								$compare = 'LIKE ';
								$attribute = "$attribute%";
							break;
							case 'ends' :										
								$compare = 'LIKE ';
								$attribute = "%$attribute";
							break;
						}					
						$meta_key = '_usam_product_attributes_'.$value;
						$joins[] = "LEFT JOIN `$wpdb->postmeta` ON (p.ID = $wpdb->postmeta.post_id AND $wpdb->postmeta.meta_key = '$meta_key')";
						$where .= " ( $wpdb->postmeta.meta_value $compare '$attribute' )";							
					}
					elseif ( $condition['type'] == 'category' )
					{
						$joins[] = "INNER JOIN $wpdb->term_relationships AS tr_$key ON (p.ID = tr_$key.object_id) INNER JOIN $wpdb->term_taxonomy AS tt_$key ON (tr_$key.term_taxonomy_id = tt_$key.term_taxonomy_id)";
						$where .= "(tt_$key.taxonomy = 'usam-category' AND tt_$key.term_id IN ('$value'))";		
					}						
					if ( $count != $key )	
						$where .= " $relation ";
				}
				$joins_str = implode( ' ', $joins );
				$sql = "SELECT DISTINCT p.ID FROM ".$wpdb->posts." AS p $joins_str WHERE $where AND p.post_type='usam-product' AND p.post_status='publish' AND p.post_parent = '0' LIMIT $this->limit_crosssell";				
			
				$products = $wpdb->get_results(	$sql );				
				$product_ids = array();
				foreach( $products as $product )
				{
					$product_ids[] = $product->ID;
				}
				usam_delete_associated_products( $this->product_id, 'crosssell' );
				usam_insert_associated_products( $this->product_id, $product_ids, 'crosssell' );				
			}
		}		
	} 	
	
	 //Рассчитать поднятие суммы продажи (Upsell)
	public function up_sell( )		
	{			
		$term = get_term_by( 'slug', 'collection', 'usam-product_attributes' );	
		if ( empty($term->term_id) )
			return false;	
		
		$collection = usam_get_product_meta( $this->product_id, 'product_attributes_'.$term->term_id );				
		if ( empty($collection))		
			return false;
		
		$brand = wp_get_post_terms($this->product_id, 'usam-brands', array("fields" => "all"));		
		if ( empty($brand[0]) )
			return false;		
					
		foreach ( $this->type_prices as $value )
		{
			$code_price = $value['code'];
			break;
		}
		$key_price = 'price_'.$code_price;
		$meta_key = USAM_META_PREFIX.$key_price;	

		$price = usam_get_product_meta($this->product_id, $key_price);		
		
		$query = array (		
			'orderby'   => array( 'meta_value_num' => 'DESC' ),		
			'post__not_in' => array($this->product_id),
			'meta_query' => array(	array('key' => '_usam_product_attributes_'.$term->term_id, 'value' => $collection, 'compare' => '='	),		
						            array('key' => $meta_key, 'value' => $price, 'compare' => '>' ),							
					),					
			'fields' => 'ids',
			'usam-brands' => $brand[0]->slug, 		
			'post_status' => 'publish',
			'update_post_meta_cache' => true,
			'update_post_term_cache' => true,
			'cache_results' => true,
			'posts_per_page' => $this->limit_upsell,
		);
		$product_ids = usam_get_products( $query );	
		usam_delete_associated_products( $this->product_id, 'upsell' );
		usam_insert_associated_products( $this->product_id, $product_ids, 'upsell' );			
	}
	
	public function down_sell( )		
	{
		
		
	}
}
?>