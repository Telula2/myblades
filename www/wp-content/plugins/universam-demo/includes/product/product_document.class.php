<?php
class USAM_Product_Document
{	
	 // строковые
	private static $string_cols = array(			
		'name', 
		'date_insert', 
		'date_modified' 
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'document_id',		
		'product_id',		
		'quantity',
	);
	
	private static $float_cols = array(
		'price', 
		'old_price',		
		'tax',		
	);
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
			
		return false;
	}
	/* Содержит значения извлекаются из БД */
	private $data    = array();	
	private $fetched = false;	
	private $args    = array( 'col'   => '', 'value' => '' );
	private $exists  = false;
	
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( false === $value )
			return;

		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;

		$this->args = array( 'col' => $col, 'value' => $value );	
	
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{
			$this->data = wp_cache_get( $value, 'usam_product_document' );			
		}
		// кэш существует
		if ( $this->data ) 
		{	
			$this->fetched = true;
			$this->exists = true;			
		}	
		else
			$this->fetch();	
	}

	/**
	 * Обновить кеш
	 */
	public static function update_cache( &$p ) 
	{		
		$id = $p->get( 'id' );
		wp_cache_set( $id, $p->data, 'usam_product_document' );	
		do_action( 'usam_product_db_update_cache', $p );
	}

	/**
	 * Удалить кеш
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{		
		$p = new USAM_Product_Document( $value, $col );
		wp_cache_delete( $p->get( 'id' ), 'usam_product_document' );		
		do_action( 'usam_product_db_update_cache', $p, $value, $col );
	}

	/**
	 * Удаляет подписчика
	 * @since 4.9	
	 */
	public function delete( ) 
	{
		global $wpdb;	
		
		$id = $this->data['id'];		
		do_action( 'usam_product_db_before_delete', $id );
	
		$sql = $wpdb->prepare( "DELETE FROM ".USAM_TABLE_PRODUCTS_DOCUMENT." WHERE id = '%d'", $id );		
		$wpdb->query( $sql );	
		
		self::delete_cache( $id );		
		do_action( 'usam_product_db_delete', $id );
	}	

	/**
	 * Выбирает фактические записи из базы данных
	 * @since 4.9
	 */
	private function fetch() 
	{	
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );		
		$sql = $wpdb->prepare( "SELECT * FROM " . USAM_TABLE_PRODUCTS_DOCUMENT . " WHERE {$col} = {$format}", $value );

		$this->exists = false;
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{						
			$this->exists = true;
			$this->data = apply_filters( 'usam_product_db_data', $data );			
			self::update_cache( $this );
		}
		do_action( 'usam_product_db_fetched', $this );
		$this->fetched = true;
	}

	/**
	 * Проверить существует ли строка в БД
	 * @since 4.9
	 */
	public function exists() 
	{
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства из БД
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_product_db_get_property', $value, $key, $this );
	}

		/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_product_db_get_data', $this->data, $this );
	}
		
	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );
		}
		$properties = apply_filters( 'usam_product_db_set_properties', $properties, $this );	

		$this->fetch();		
		if ( ! is_array( $this->data ) )
			$this->data = array();

		$this->data = array_merge( $this->data, $properties );
		return $this;
	}

	/**
	 * Вернуть формат столбцов таблицы
	 * @since 4.9	
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}
		
	/**
	 * Сохраняет в базу данных
	 * @since 4.9
	 */
	public function save()
	{
		global $wpdb;

		do_action( 'usam_product_db_pre_save', $this );	
		
		$this->data['date_modified'] = date( "Y-m-d H:i:s" );		
		$where_col = $this->args['col'];
		$result = false;		
		if ( $where_col ) 
		{	// обновление				
			$where_val = $this->args['value'];
			$where_format = self::get_column_format( $where_col );
			do_action( 'usam_product_db_pre_update', $this );
			self::delete_cache( $where_val, $where_col );			
			
			$this->data = apply_filters( 'usam_product_db_update_data', $this->data );
			$format = $this->get_data_format( );								
			$result = $wpdb->update( USAM_TABLE_PRODUCTS_DOCUMENT, $this->data, array( $where_col => $where_val ), $format, array( $where_format ) );
	
			do_action( 'usam_product_db_update', $this );
		} 
		else 
		{   // создание	
			do_action( 'usam_product_db_pre_insert' );		
						
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );			
			
			$this->data = apply_filters( 'usam_product_db_insert_data', $this->data );			
			$format = $this->get_data_format(  );
			
			$result = $wpdb->insert( USAM_TABLE_PRODUCTS_DOCUMENT, $this->data, $format );	
			if ( $result ) 
			{
				$result = $wpdb->insert_id;
				$this->set( 'id', $wpdb->insert_id );					
				
				$this->args = array('col'   => 'id',  'value' => $this->get( 'id' ), );				
				do_action( 'usam_product_db_insert', $this );
			}			
		} 		
		do_action( 'usam_product_db_save', $this );
		return $result;
	}	
}


function get_products_document( $qv = array() )
{ 
	global $wpdb;	

	if ( isset($qv['fields']) )
	{
		$fields = $qv['fields'] == 'all'?'*':$qv['fields'];
	}
	else
		$fields = '*';
	
	$_where[] = '1=1';
	
	if ( isset($qv['document_id']) )
		$_where[] = "document_id = '".$qv['document_id']."'";
		
	if ( isset($qv['ids']) )
		$_where[] = "id IN( '".implode( "','", $qv['ids'] )."' )";
	
	$where = implode( " AND ", $_where);	
	if ( !empty($qv['condition']) ) 
	{		
		foreach ( $qv['condition'] as $condition )
		{					
			$select = '';
			if ( empty($condition['col']) )
				continue;
			
			switch ( $condition['col'] )
			{								
				default:				
					$select = $condition['col'];			
				break;				
			}
			if ( $select == '' )
				continue;
			
			$compare = "=";	
			switch ( $condition['compare'] ) 
			{
				case '<' :
					$compare = "<";					
				break;
				case '=' :
					$compare = "=";					
				break;	
				case '!=' :
					$compare = "!=";					
				break;
				case '>' :
					$compare = ">";					
				break;				
			}
			$value = $condition['value'];
			
			if ( empty($condition['relation']) )
				$relation = 'AND';
			else
				$relation = $condition['relation'];
			
			$where .= $wpdb->prepare( " $relation $select $compare %s", $value );			
		}
	}	
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'id';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'DESC';	
	if ( isset($qv['output_type']) )	
		$output_type = $qv['output_type'];	
	else
		$output_type = 'OBJECT';
	
	if ( $where != '' )
		$where = " WHERE $where ";
	
	$storages = $wpdb->get_results( "SELECT $fields FROM ".USAM_TABLE_PRODUCTS_DOCUMENT." $where $orderby $order", $output_type );
	
	if ( empty($qv) )
		wp_cache_set( $key_cache, $storages );		
	return $storages;
}