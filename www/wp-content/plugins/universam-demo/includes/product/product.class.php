<?php
// Сохранение карточки товара
class USAM_Product
{	
	private $product_id = 0;
	public  $product_new = false;              // это новый товар
	private $p_data = array();
	private $calculation_meta_key = array();
	private $product_type = 'simple';	
	private $type_prices = array();
	
	public function __construct( $product_id = 0 ) 
	{				
		$this->type_prices = usam_get_prices( array('type' => 'all', 'orderby' => 'type' ) );	
		
		if ( is_array($product_id) )		
			$this->set( $product_id ); 
		else
			$this->product_id = (int)$product_id;	
	}	

	public function set( $properties ) 
	{				
		$properties = apply_filters( 'usam_product_set_properties', $properties, $this );			
		if ( $properties !== null )
		{
			$properties = (array)$properties;				
			$properties = $this->sanitise_product_forms( $properties );	
		}		
		if ( !empty($properties['product_type']) )	
		{		
			$this->product_type = $properties['product_type'];				
		}				
		if ( $this->product_id == 0 )
		{	//Новый товар
			$default_data = $this->get_default_product();
			$this->p_data = $this->merge_meta_deep( $default_data, $properties, $default_data );			
			$this->product_new = true;			
		}
		else			
		{	//Изменение существующего товара			
			if ( empty($properties['product_type']) )	
			{
				$product_type_terms = get_the_terms( $this->product_id, 'usam-product_type' );		
				if ( !empty($product_type_terms) ) 
					$this->product_type = $product_type_terms[0]->slug;			
			}			
			if ( $properties !== null )
			{			
				$properties = (array)$properties;					
				if ( isset($properties['meta']) )
				{								
					$default_data = $this->get_default_product();
					$new_product_data = array();	
					foreach( $properties['meta'] as $key => $meta )
					{
						if ( isset($default_data['meta'][$key]) )
							$new_product_data[$key] = $this->merge_meta_deep( $default_data['meta'][$key], $meta, $default_data['meta'][$key] );
						else // Например атрибуты товаров
							$new_product_data[$key] = $meta;
					}			
					$properties['meta'] = $new_product_data;
					$this->p_data = $this->sanitise_product_forms( $properties );						
				}
				else
					$this->p_data = $properties;				
			}		
		}		
	}
	
	private function get_product_metas( )
	{
		$meta = get_post_meta( $this->product_id );		
		$product_metas = array();
		if ( !empty($meta))
		{			
			$merge_meta_deep = true;
			foreach ( $meta as $meta_key => $meta_value )
			{
				$key = str_replace( USAM_META_PREFIX, "", $meta_key );			
				if ( count($meta_value) == 1 )
					$product_metas[$key] = maybe_unserialize($meta_value[0]);
				else
					$product_metas[$key] = $meta_value;			
			}						
		}			
		return $product_metas;
	}
	
	// Загрузить данные из базы
	private function load_product_data( $merge_meta_deep = false )
	{		
		$post = get_post( $this->product_id, ARRAY_A );			
		$this->p_data = array_merge( $post, $this->p_data );
		$this->p_data['meta'] = $this->get_product_metas();		
		if ( $merge_meta_deep || !empty($this->p_data['meta']) )
		{
			$default_data = $this->get_default_product();
			$this->p_data['meta'] = $this->merge_meta_deep( $default_data['meta'], $this->p_data['meta'], $default_data['meta'] );				
		}
	}	
	
	// Получить данные из класса
	public function get_product( $merge_meta_deep = false )
	{		
		$this->load_product_data( $merge_meta_deep );
		
		return $this->p_data;	
	}	

	public function set_product( $key, $value )
	{		
		$this->p_data['meta'][$key] = $value;
	}	

	public function set_terms( )
	{
		if ( !empty($this->p_data['tax_input']) )
		{ 
			foreach( $this->p_data['tax_input'] as $taxonomy => $terms )				
				wp_set_object_terms( $this->product_id , $terms , $taxonomy );					
		}
	}
	
	// Рассчитать и сохранить все свойства товара
	public function save_product_meta( )
	{		
		if ( $this->product_id != 0 )
		{			
			if ( !empty( $this->p_data['meta']) )
			{ 
				$this->calculate_product( );
				$this->update_product_metas( );				
				$this->update_price_primary_product_variations();							
			}			
			if( !empty($this->p_data['file']) ) 
				$this->add_downloadable_file( $this->p_data['file'] );
			elseif ( !empty($this->p_data['select_product_file'])) 
			{
				$this->item_reassign_file( $this->p_data['select_product_file'] );
			}
			if ( !empty($this->p_data['preview_file']) )
				$this->add_preview_file( $this->p_data['preview_file']);	
					
			$this->insert_media();				
		
			wp_set_object_terms( $this->product_id, $this->product_type, 'usam-product_type' );
			
			do_action('usam_edit_product_meta', $this->product_id );
			return true;
		}
		return false;
	}
	
	
	private function update_price_primary_product_variations( )
	{
		if ( $this->product_type == 'variation' )
		{	// Найти главный товар и обновить его цену
			if ( !empty($this->p_data['post_parent']) )
				$product_id = $this->p_data['post_parent'];
			else
			{
				$product = get_post( $this->product_id );
				$product_id = $product->post_parent;
			}	
			if ( !empty($product_id) )
			{
				usam_edit_product_prices( $product_id );
			}
		}		
	}	
	
	// Обновление меты товара	 
	public function update_product_metas( $keys = 'all' )
	{			
		if ( $this->product_id > 0 )
		{			
			if ( is_string($keys) )
			{
				$keys = array( $keys );
			}			
			$all_metas = $this->get_product_metas();	
			if ( $keys = 'all' )
			{
				foreach( $this->p_data['meta'] as $meta_key => $meta_value)
				{						
					if ( empty($all_metas[$meta_key]) || $all_metas[$meta_key] != $meta_value || is_array($meta_value) )
						usam_update_product_meta($this->product_id, $meta_key, $meta_value);	
				} 				
			}			
			elseif ( is_array($keys) )
			{
				foreach( $keys as $meta_key )
				{						
					if ( isset($this->p_data['meta'][$meta_key]) )	
					{
						if ( empty($all_metas[$meta_key]) || $all_metas[$meta_key] != $this->p_data['meta'][$meta_key] || is_array($this->p_data['meta'][$meta_key]) )
							usam_update_product_meta($this->product_id, $meta_key, $this->p_data['meta'][$meta_key]);	
						
					}										
				} 	
			}			
			return true;
		}
		else
			return false;
	}
	
	/*
	 * Создание товара из переданных данных. Используется при импорте товара.
	 */
	public function insert_product( )
	{
		global $wpdb, $user_ID;	
	
		$product_post_values = array(
			'post_author'    => !empty($this->p_data['user_id'])?$this->p_data['user_id']:$user_ID,
			'post_content'   => $this->p_data['post_content'],
			'post_excerpt'   => $this->p_data['post_excerpt'],
			'post_title'     => $this->p_data['post_title'],
			'post_status'    => $this->p_data['post_status'],
			'post_parent'    => $this->p_data['post_parent'],
			'post_type'      => "usam-product",
			'comment_status' => "open",
			'post_name'      => sanitize_title($this->p_data['post_title'])
		);	
		$product_id = wp_insert_post( $product_post_values );		
		if ( $product_id == 0 )
		{
			return new WP_Error('db_insert_error', __( 'Не удалось вставить товар в базу данных', 'usam' ), $wpdb->last_error);
		}		
		$this->product_id = $product_id;	
		
		if ( isset ( $this->p_data["sticky"] ) )
			stick_post( $this->product_id );
	
		$this->set_terms();			
		
		$this->save_product_meta();				
		return $this->product_id;
	}	
		
	// Рассчитать данные товара
	private function calculate_product( )
	{				
		$this->calculate_prices( );	                  // Рассчитать цену
		$this->calculate_stock();	                  // Рассчитать остаток
		$this->calculate_product_metadata( );	      // Рассчитать данные товара			
		if ( $this->product_type != 'variation' ) 
		{
	
		}	
		if ( $this->product_type != 'variation' ) 
			usam_add_product_meta( $this->product_id, 'product_views', 0 );					
	}
	
	public function calculate_product_metadata( )
	{
		$this->calculate_external_product();
		//Комплектация
		if ( !empty($this->p_data['meta']['product_metadata']['components']) )
		{
			foreach((array)$this->p_data['meta']['product_metadata']['components'] as $key => $component)
			{								
				if ( trim($component['title']) == '' )
				{
					unset($this->p_data['meta']['product_metadata']['components'][$key]);					
				}
			}
		}		
	}
	
		/* Расчет внешнего товара */
	private function calculate_external_product( )
	{
		if ( isset($this->p_data['meta']['product_metadata']['webspy_link']) )
		{
			if ( $this->p_data['meta']['product_metadata']['webspy_link'] != '' )
			{
				if ( !isset($this->p_data['meta']['date_externalproduct']) )
					$this->p_data['meta']['date_externalproduct'] = date( "Y-m-d H:i:s" );
				switch ( $this->p_data['meta']['product_metadata']['external']['type'] ) 
				{
					case 'resale' :		
						$this->p_data['meta']['product_metadata']['external']['title'] = '';
						$this->p_data['meta']['product_metadata']['external']['target'] = '';				
					break;
					case 'redirect' :
					
					break;		
					default:
						$this->p_data['meta']['product_metadata']['external']['title'] = '';
						$this->p_data['meta']['product_metadata']['external']['target'] = '';
						$this->p_data['meta']['product_metadata']['external']['type'] = 'resale';
					break;
				}
			}
			else
			{
				usam_delete_product_meta($this->product_id, 'date_externalproduct');
				unset($this->p_data['meta']['date_externalproduct']);				
				$this->p_data['meta']['product_metadata']['external']['title'] = '';
				$this->p_data['meta']['product_metadata']['external']['target'] = '';
				$this->p_data['meta']['product_metadata']['external']['type'] = '';
			}
		}
	}		
	
/* Расчет свойств товара */
	public function calculate_product_attributes( $new_product_attributes )
	{		
		global $wpdb;
		
		$product_attributes = usam_get_attributes( $this->product_id );		
		$ids = array();				
		foreach( $product_attributes as $term )
		{						
			$ids[] = $term->term_id;					
		}			
		if ( $this->product_type == 'variation' ) 
		{						
			foreach((array)$this->p_data['meta'] as $key => $value)
			{
				if ( strpos($key, "product_attributes_") !== false )
					unset($this->p_data['meta'][$key]);
				
				if ( strpos($key, "filter_") !== false )
					unset($this->p_data['meta'][$key]);
			}		
			return true;
		} 
		if ( empty($new_product_attributes) )
			return false;
		
		$product_attributes = array(); 
		foreach((array)$new_product_attributes as $attribute_id => $value)
		{				
			if ( !in_array($attribute_id, $ids) )
				continue;
			
			if ( !is_array($value) )
				$value = trim($value);
			 
			$type_attribute = get_term_meta($attribute_id, 'usam_type_attribute', true);
			switch ( $type_attribute ) 
			{
		//Флажок	
				case 'C' ://Один											
					if ( $value != '' )
						$product_attributes[$attribute_id][] = $value;
				break;
				case 'M' ://Несколько 														
					$product_attributes[$attribute_id] = $value;
				break;
		//Выбор из варианов
				case 'N' ://Число     					
					$product_attributes[$attribute_id][] = $value;
				break;	
				case 'S' ://Текст										
					$product_attributes[$attribute_id][] = $value;
				break;
		//Другое
				case 'D' ://Дата
					if ( $value != '' ) 
						$product_attributes[$attribute_id][] = date_i18n("d-m-Y",  strtotime($value) );
				break;
		//Базовый тип
				case 'O' ://Число
					if ( is_numeric($value) && $value != '' ) 
						$product_attributes[$attribute_id][] = $value;
				break;
				case 'T' ://Текст
				default:										
					if ( $value != '' )
						$product_attributes[$attribute_id][] = $value;
				break;
			}		
		} 	
		$term = get_term_by( 'slug', 'brand', 'usam-product_attributes' );		
		if ( !empty($term) )
		{
			$brand = usam_product_brand( $this->product_id );
			if ( !empty($brand) )
				$product_attributes[$term->term_id][] = $brand->name;
		}		
		$attributes_bd = usam_get_product_attributes( $this->product_id );
		$filters_bd = usam_get_product_filters( $this->product_id );
		
		$args = array( 'attribute_id__in' => array_keys($product_attributes) );
		$ready_options = usam_get_product_attribute_values( $args );	
		
		$attributes_values = array();
		foreach ( $ready_options as $option )
		{
			$attributes_values[$option->attribute_id][$option->id] = $option->value;
		}		
		$filters_new = array();
		foreach( $product_attributes as $attribute_id => $attributes)
		{				
			$term_filter = get_term_meta($attribute_id, 'usam_filter', true);			
			$attribute_meta_key = "product_attributes_$attribute_id";
			$type_attribute = get_term_meta($attribute_id, 'usam_type_attribute', true);
			foreach( $attributes as $attribute)				
			{ 			
				$attribute = esc_sql($attribute);
				if ( !isset($attributes_bd[$attribute_id]) || !in_array($attribute, $attributes_bd[$attribute_id] ) )
				{ 							
					if ( $type_attribute == 'M' )			
						usam_add_product_meta($this->product_id, $attribute_meta_key, $attribute, false );	
					else						
						usam_update_product_meta($this->product_id, $attribute_meta_key, $attribute );										
				}	
				else
				{
					$key = array_search($attribute, $attributes_bd[$attribute_id]);				
					unset($attributes_bd[$attribute_id][$key]);	
				}
				
				if ( !empty($term_filter) )			
				{	// Если включен фильтр по этому атрибуту	
					if ( $type_attribute == 'M' || $type_attribute == 'S' || $type_attribute == 'N' )		
					{
						if ( isset($attributes_values[$attribute_id][$attribute]) )
							$filter_name = $attributes_values[$attribute_id][$attribute];
						else
							$filter_name = '';
					}
					elseif ( $type_attribute == 'C' )		
					{
						if ( !empty($attribute) )			
							$filter_name = __('Да','usam');				
						else
							$filter_name = __('Нет','usam');						
					}		
					else			
						$filter_name = $attribute;	
					
					if ( $filter_name == '' )
						continue;
					
					$filter = usam_get_product_filter( array( 'attribute_id' => $attribute_id, 'value' => $filter_name ) );	
					if ( empty($filter) )
					{									
						$insert_filter = array( 'attribute_id' => $attribute_id, 'value' => $filter_name );
						$filter_id = usam_insert_product_filter( $insert_filter );						
					}
					else
						$filter_id = $filter['id'];
				
					if ( !isset($filters_bd[$attribute_id]) || !in_array($filter_id, $filters_bd[$attribute_id] ) )
					{ 
						$filter_meta_key = "filter_$attribute_id"; 							
						if ( $type_attribute == 'M' )	
						{													
							usam_add_product_meta($this->product_id, $filter_meta_key, $filter_id, false );								
						}
						else
						{	
							usam_update_product_meta($this->product_id, $filter_meta_key, $filter_id );								
						}	
					}
					else
					{
						$key = array_search($filter_id, $filters_bd[$attribute_id]);				
						unset($filters_bd[$attribute_id][$key]);	
					}
				}				
			}			
		}
		// Удалим не нужные свойства товаров
		foreach( $attributes_bd as $attribute_id => $attributes)
		{						
			$attribute_meta_key = "product_attributes_$attribute_id";				
			foreach( $attributes as $attribute)		
			{   
				usam_delete_product_meta($this->product_id, $attribute_meta_key, $attribute );	
			}
		}				
		// Удалим не нужные фильтры товаров
		foreach( $filters_bd as $attribute_id => $attributes)
		{						
			$filter_meta_key = "filter_$attribute_id";			
			foreach( $attributes as $attribute)		
			{   
				usam_delete_product_meta($this->product_id, $filter_meta_key, $attribute );				
				
				$attribute = esc_sql($attribute);
				$meta_key = USAM_META_PREFIX."filter_".$attribute_id;				
				$result = $wpdb->get_var( "SELECT COUNT(*) FROM `$wpdb->postmeta` WHERE post_id != '$this->product_id' AND meta_key ='$meta_key' AND meta_value='$attribute'" );								
				if ( empty($result) )
					usam_delete_product_filter( array( 'id' => $attribute ) ); // Значение не используется, тогда удалим его	
			}
		}
	}
	
/* Расчет остатка */	
	public function calculate_stock( )
	{			
		if ( $this->product_type == 'variable' )
		{
			$this->calculation_of_inventory_product_variations( );			
		}
		else
		{
			if ( empty($this->p_data['meta']['product_metadata']['webspy_link']) )
			{
				if ( !empty($this->p_data['meta']['code']) )
				{					
					$exchange = new USAM_Exchange_FTP();			
					$return = $exchange->know_product_stock_loaded_ftp( $this->p_data['meta']['code'] );					
					usam_set_user_screen_error( $exchange->get_error( ), 'product' );												
					if ( !empty($return) )
					{						
						$this->p_data['meta'] = array_merge( $this->p_data['meta'], $return );	
					}					
				}
			}			
			$this->calculation_of_inventory_product();	
		}		
	}
	
	public function calculation_of_inventory_product_variations( )
	{
		global $wpdb;
		$sql = "SELECT SUM(pm.meta_value) 
			FROM {$wpdb->posts} AS p INNER JOIN {$wpdb->postmeta} AS pm ON pm.post_id = p.id AND pm.meta_key = '_usam_stock'
			WHERE p.post_type = 'usam-product' AND p.post_parent = '$this->product_id'";
		$stock = $wpdb->get_var( $sql );
		if ( USAM_UNLIMITED_STOCK < $stock )
			$stock = USAM_UNLIMITED_STOCK;
		
		$this->p_data['meta']['stock'] = $stock;
		
		$sql = "SELECT SUM(pm.meta_value) 
			FROM {$wpdb->posts} AS p INNER JOIN {$wpdb->postmeta} AS pm ON pm.post_id = p.id AND pm.meta_key = '_usam_storage'
			WHERE p.post_type = 'usam-product' AND p.post_parent = '$this->product_id'";
		$storage = $wpdb->get_var( $sql );
		
		if ( USAM_UNLIMITED_STOCK < $storage )
			$storage = USAM_UNLIMITED_STOCK;
		
		$this->p_data['meta']['storage'] = $storage;
	}
	
	public function calculation_of_inventory_product( )
	{	
		if ( $this->p_data['meta']['virtual'] )
		{
			$this->p_data['meta']['reserve'] = 0;						
			$this->p_data['meta']['stock'] = USAM_UNLIMITED_STOCK;	
			$this->p_data['meta']['storage'] = USAM_UNLIMITED_STOCK;					
		}
		else
		{
			$storages = usam_get_stores( array( 'fields' => 'id, shipping, meta_key' ) );
			$result = false;
			$_storage = 0;
			$stock = 0;	
			foreach ( $storages as $storage )
			{
				$pmeta_key = $storage->meta_key;			
				if ( isset($this->p_data['meta'][$pmeta_key]) )
				{
					if ($this->p_data['meta'][$pmeta_key] === '' )					
						$this->p_data['meta'][$pmeta_key] = USAM_UNLIMITED_STOCK;
					else
						$this->p_data['meta'][$pmeta_key] = (int)$this->p_data['meta'][$pmeta_key];
						
					if ( $storage->shipping != 0 )				
						$stock += $this->p_data['meta'][$pmeta_key];				
					$_storage += $this->p_data['meta'][$pmeta_key];
					$result = true;
				}
			}			
			if ( $result )
			{
				$reserve = (int)usam_get_product_meta($this->product_id, 'reserve' );
				if ( $_storage > USAM_UNLIMITED_STOCK )					
					$_storage = USAM_UNLIMITED_STOCK;
				else
					$this->p_data['meta']['storage'] = ($_storage<=0?0:$_storage - $reserve);				
				
				if ( $stock > USAM_UNLIMITED_STOCK )					
					$stock = USAM_UNLIMITED_STOCK;
				else
					$this->p_data['meta']['stock'] = ($stock<=0?0:$stock - $reserve);
			}
		}
	}
	
	private function get_product_meta( $meta )
	{		
		$this->p_data['meta'][$meta] = usam_get_product_meta($this->product_id, $meta );		
		$default = $this->get_default_product( $meta );	
		if ( is_array($default) && is_array($this->p_data['meta'][$meta]) )
			$this->p_data['meta'][$meta] = array_merge( $default, $this->p_data['meta'][$meta] );	
		return $this->p_data['meta'][$meta];
	}
	
/* Расчет цены */
	public function calculate_prices( )
	{					
		if ( $this->product_type == 'variable' ) 
		{
			foreach ( $this->type_prices as $value )
			{	// Если сохраняют главный товар вариаций
				$this->calculate_price_variations( $value['code'] );
			}
		}			
		else
		{
			$purchasing_code = '';
			$type_prices = array();
			foreach ( $this->type_prices as $value )
			{			
				if ( !isset($this->p_data['meta']['price_'.$value['code']]) )
					$this->get_product_meta( 'price_'.$value['code'] );						
					
				if ( !isset($this->p_data['meta']['underprice_'.$value['code']]) )
					$this->get_product_meta( 'underprice_'.$value['code'] );
				
				if ( !isset($this->p_data['meta']['old_price_'.$value['code']]) )
					$this->get_product_meta( 'old_price_'.$value['code'] );
					
				$price = $this->p_data['meta']["price_".$value['code']];
				if ( $value['type'] == "R" )
					$type_prices[] = $value; // Соберем только розничные цены
				elseif ( $value['type'] == "P" && $price && $purchasing_code == '' )
				{					
					$purchasing_code = $value['code'];
					$this->p_data['meta']["price_".$purchasing_code] = round($this->p_data['meta']["price_".$purchasing_code], $value['rounding']);	
				}					
			}			
			$comparison = new USAM_Comparison_Array( 'base_type', 'ASC' );
			usort( $type_prices, array( $comparison, 'compare' ) );				
		
			$discounts = usam_get_product_discount( $this->product_id );
			foreach ( $type_prices as $value )
			{	
				$this->calculate_price( $value, $purchasing_code ); 
				$this->calculate_discount_price( $value, $discounts );		
			}	
		}
		$this->p_data['meta']["price_date"]    = date("Y-m-d H:i:s");
	}
	
	// Вычисление цены и скидки	
	private function calculate_price( $type_price, $purchasing_code )
	{		
		$code_price = $type_price['code'];	
		$price_key = "price_".$code_price;			
		
		$underprice = 0;
		$price = 0;
		$use_surcharge = true;
		if ( empty($type_price['base_type']) )		
		{ 
			if ( $purchasing_code )
			{ 
				$base_price = (float)$this->p_data['meta']["price_".$purchasing_code];	
				$base_price_currency = usam_get_currency_price_by_code( $purchasing_code );		
				$price = usam_convert_currency( $base_price, $type_price['currency'], $base_price_currency );						
			}
			else
			{				
				$use_surcharge = false;
				if ( !empty($this->p_data['meta']["old_price_".$code_price]) )
					$price = (float)$this->p_data['meta']["old_price_".$code_price];
				else
					$price = (float)$this->p_data['meta']["price_".$code_price];
			} 
		}
		elseif ( isset($this->p_data['meta']["price_".$type_price['base_type']]) )
		{ 
			$base_price = (float)$this->p_data['meta']["price_".$type_price['base_type']];	
			$base_price_currency = usam_get_currency_price_by_code($type_price['base_type']);			
			$price = usam_convert_currency( $base_price, $type_price['currency'], $base_price_currency );	
		}	
		if ( $use_surcharge )
		{
			$option = get_option('usam_underprice_rules', '');
			$rules = maybe_unserialize( $option );				
			if ( !empty($rules) )
			{			
				$categories = usam_get_product_term_ids( $this->product_id, 'usam-category' );				
				$brands = get_the_terms( $this->product_id , 'usam-brands' );
				$category_sale = get_the_terms( $this->product_id , 'usam-category_sale' );
				
				// Если не установлено подберем подходящую наценку				
				$rule_id = 0;
				foreach ( $rules as $id => $rule )
				{
					if ( empty($rule['type_prices']) || in_array($code_price, $rule['type_prices']) )
					{
						if ( empty($rule['category']) && empty($rule['brands']) && empty($rule['category_sale']) )
							continue;
							
						$result_category = $result_brand = $result_category_sale = $result_ancestor = false;
						if ( !empty($rule['brands']) && !empty($brands) && in_array($brands[0]->term_id, $rule['brands']) )
						{ 
							$result_brand = true; 
						} 
						if ( !empty($rule['category_sale']) && !empty($category_sale) )
						{ 
							foreach ( $category_sale as $category )
							{
								if ( in_array($category->term_id, $rule['category_sale']) )
								{									
									$result_category_sale = true;
									break;
								}						
							}					
						} 
						if ( !empty($rule['category']) && !empty($categories) )
						{		
							foreach ( $categories as $category_id )
							{
								if ( in_array($category_id, $rule['category']) )
								{									
									$result_category = true;
									break;
								}						
							}
						}			
						if ( $result_category == false && $result_brand == false && $result_category_sale == false )
						{
							continue;
						} 					
						if ( $result_category_sale && $result_brand )
						{ 
							$rule_id = $rule['id'];
							break; // Идеальное совпадение, можно завершить
						}
						elseif ( $result_category && $result_brand )
						{
							$rule_id = $rule['id'];
							if ( !$result_ancestor )
								break; // Идеальное совпадение, можно завершить
						}						
						elseif ( ( $result_category || $result_brand || $result_category_sale) && $rule_id == 0 )
						{
							$rule_id = $rule['id'];
						}					
					}
				} 
				if ( $rule_id )
				{						
					$rule = usam_get_data($rule_id, 'usam_underprice_rules');	
					$underprice = (float)$rule['value'];					
					$this->p_data['meta']["underprice_".$code_price] = $rule_id;					
				}
				elseif( !empty($this->p_data['meta']['underprice_'.$code_price]) )
				{ 
					$rule = usam_get_data($this->p_data['meta']["underprice_".$code_price], 'usam_underprice_rules');					
					$underprice = (float)$rule['value'];	
				}		
			}	
			if ( empty($this->p_data['meta']["underprice_".$code_price]) )
			{ 
				$this->p_data['meta']["underprice_".$code_price] = 0;
				$underprice = isset($type_price['underprice'])?(float)$type_price['underprice']:0; 				
			} 
			$price = $price + $price * $underprice / 100;	
		}
		$price = round($price, $type_price['rounding']);
		$this->p_data['meta']["price_".$code_price] = $price;		
	}	
		
	// Вычисление цены и скидки	
	private function calculate_discount_price( $type_price, $discounts )
	{		
		global $wpdb;
			
		$code_price = $type_price['code'];			
		$price_key = "price_".$code_price;
		$old_price_key = "old_price_".$code_price;	
		
		$price = $discount_price = $this->p_data['meta'][$price_key];		
		
		$product = usam_get_active_products_day_by_codeprice( $code_price, $this->product_id );	
		if ( !empty($product) )
		{	
			if ( $product->dtype == 'f' )									
				$discount_price = $discount_price - $product->value;				
			elseif ( $product->dtype  == 'p' )
				$discount_price = $discount_price - $discount_price*$product->value/ 100;
			else
				$discount_price = $product->value;
		}
		else
		{
			$option = get_option('usam_product_discount_rules', '');		
			$rules = maybe_unserialize( $option );		
			if ( !empty($rules) )
			{		
				$current_time = time();						
				foreach ( $rules as $key => $rule )
				{										
					$result = false;
					if ( $rule['active'] )
					{  
						if ( ( empty($rule['start_date']) || strtotime($rule['start_date']) <= $current_time ) && ( empty($rule['end_date']) || strtotime($rule['end_date']) >= $current_time ) && ( in_array($code_price, $rule['type_prices']) ) )
						{								
							$result = $this->compare_logic( $rule['conditions'] );							
							if ( $result ) 
							{  
								if ( $rule['dtype'] == 'f' )									
									$discount_price = $discount_price - $rule['discount'];				
								elseif ( $rule['dtype'] == 'p' )
									$discount_price = $discount_price - $discount_price*$rule['discount']/ 100;
								else
									$discount_price = $rule['discount'];												
								
								$sql = "INSERT INTO `".USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS."` (`product_id`,`code_price`,`discount_id`) VALUES ('%d','%s','%d') ON DUPLICATE KEY UPDATE `discount_id`='%d'";					
								$insert = $wpdb->query( $wpdb->prepare($sql, $this->product_id, $code_price, $rule['id'], $rule['id'] ) );				
	
								if ( !empty($rule['end']) )
									break;								
							}												
						}		
					}
					if ( !$result )
					{ 
						if ( isset($discounts[$code_price]) && in_array($rule['id'], $discounts[$code_price]) )
						{ 		
							$result = $wpdb->delete( USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS, array('product_id' => $this->product_id, 'code_price' => $code_price, 'discount_id' => $rule['id']), array( '%d', '%s', '%d' ) );
						}
					}
				}
			} 	
		}
		$discount_price = usam_round_price( $discount_price, $code_price );	
		if ( $discount_price != $price )		
		{
			$this->p_data['meta'][$old_price_key] = $price;
			$this->p_data['meta'][$price_key] = $discount_price;
		}
		else
			$this->p_data['meta'][$old_price_key] = 0;			
	}
	
	private function calculate_price_variations( $code )
	{		
		global $wpdb;
		$sql = "
			SELECT pm.meta_value AS price, pm2.meta_value AS old_price
			FROM {$wpdb->posts} AS p
			INNER JOIN {$wpdb->postmeta} AS pm ON pm.post_id = p.id AND pm.meta_key = '_usam_price_$code'
			INNER JOIN {$wpdb->postmeta} AS pm2 ON pm2.post_id = p.id AND pm2.meta_key = '_usam_old_price_$code'	
			WHERE p.post_type = 'usam-product' AND p.post_parent = '$this->product_id'"; 
		$price_variations = $wpdb->get_results( $sql );		
		
		$price = $old_price = $max = 99999;
		if ( !empty($price_variations) )
		{				
			foreach( $price_variations as $key => $value )
			{
				if ( $value->price != '0' )
				{
					if ( $value->price < $price )	      $price = $value->price;     // поиск минимальной цены
					if ( $value->old_price < $old_price ) $old_price = $value->old_price;// поиск минимальной цены на продажу
				}				
			}
			if ( $price == $max )
			{				
				$this->p_data['post_status'] = 'draft';					
				$old_price = 0;	
			}			
		}
		else
		{
			$this->p_data['post_status'] = 'draft';		
			$price = 0;
			$old_price = 0;		
		}	
		$this->p_data['meta']['old_price_'.$code] = $old_price;	
		$this->p_data['meta']['price_'.$code] = $price;			
	}	

	public function compare_logic( $conditions ) 
	{				
		$result = true;	
		$allow_operation = true;			
		foreach( $conditions as $c )
		{				
			if ( !isset($c['logic_operator']) )
			{
				if ( !$allow_operation )
					continue;
				
				switch( $c['type'] )
				{					
					case 'group':
						$result = $this->compare_logic( $c['rules'] );
					break;
					case 'simple':
						$result = $this->cart_compare_logic( $c ); 
					break;
				}
				if ( !$result )
				{							
					$allow_operation = false;
				}
			}				
			else
			{ // Если условие И, ИЛИ
				if ( $c['logic_operator'] == 'AND' )
				{ // Если и
							
				}
				else					
				{ // Если или		
					if ( $result )
					{	// Если условия истина до ближайшего оператора ИЛИ то завершить цикл
						break;
					}
					else	
					{						
						$allow_operation = true;
					}
				}				
			}
		}		
		return $result;
	}		
	
	private function cart_compare_logic( $c ) 
	{	
		$c = apply_filters( 'usam_product_discount_compare_logic_before', $c );
		$result = false;
		
		$compare = new USAM_Compare();
		
		if ($c['property'] == 'name')
		{
			$post_title = !isset($this->p_data['meta']['sku'])?$this->get_product_meta( 'sku' ):$this->p_data['meta']['sku'];
			$result = $compare->compare_string($c['logic'], $this->p_data['post_title'], $c['value'] );
		}
		elseif ($c['property'] == 'sku')
		{
			$sku = !isset($this->p_data['meta']['sku'])?$this->get_product_meta( 'sku' ):$this->p_data['meta']['sku'];
			$result = $compare->compare_string($c['logic'], $sku, $c['value'] );
		}			
		elseif ($c['property'] == 'barcode')
		{		
			$barcode = !isset($this->p_data['meta']['barcode'])?$this->get_product_meta( 'barcode' ):$this->p_data['meta']['barcode'];
			$result = $compare->compare_number($c['logic'], $this->p_data['meta']['barcode'], $c['value'] );
		}
		elseif ($c['property'] == 'category')
		{		
			$result = $compare->compare_terms( $this->product_id, 'usam-category', $c );
		}
		elseif ($c['property'] == 'brands')
		{		
			$result = $compare->compare_terms( $this->product_id, 'usam-brands', $c );
		}
		elseif ($c['property'] == 'category_sale')
		{		
			$result = $compare->compare_terms( $this->product_id, 'usam-category_sale', $c );
		}		
		elseif ( stristr($c['property'], 'product_attribute') !== false)
		{			
			$id = str_replace("product_attribute-", "", $c['property']);		
			$attribute = !isset($this->p_data['meta']['product_attributes_'.$id])?$this->get_product_meta( 'product_attributes_'.$id ):$this->p_data['meta']['product_attributes_'.$id];
			$result = $compare->compare_string($c['logic'], $attribute, $c['value'] );
		}
		else 
			$result = true;	
		$result = apply_filters( 'usam_product_discount_compare_logic_after', $result, $c );								
		return $result;		
	}
	
	private function insert_media( )
	{	
		if ( !empty($this->p_data['media_url']) )
		{
			foreach ( $this->p_data['media_url'] as $url ) 
			{			
				$file_array = array();
				$tmp = download_url( $url );	
				if( ! is_wp_error( $tmp ) )
				{
					preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
					$file_array['name'] = basename( $matches[0] );
					$file_array['tmp_name'] = $tmp;	
					media_handle_sideload( $file_array, $this->product_id, $this->p_data['post_excerpt'] );	
				}
			}
		}	
		$attachment_ids = array();
		if ( !empty($this->p_data['image_gallery']) )
		{ 			
			foreach ( $this->p_data['image_gallery'] as $menu_order => $attachment_id )	
			{
				$attachment_id = absint($attachment_id);
				if ( $attachment_id )
				{
					$attachment_ids[] = $attachment_id;
					wp_update_post( array( 'ID' => $attachment_id, 'post_parent' => $this->product_id, 'menu_order' => $menu_order ) );			
				}
			}			
		}		
		$args = array( 'fields' => 'ids', 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $this->product_id, 'post__not_in' => $attachment_ids );		
		$post_thumbnail_id = get_post_thumbnail_id( $this->product_id );		
		if ( empty($post_thumbnail_id) )
			$args['post__not_in'][] = $post_thumbnail_id;	
		
		$attachments = get_posts( $args );
		if ( !empty($attachments) )
		{ 
			foreach ( $attachments as $attachment_id )	
			{				
				if ( empty($this->p_data['image_gallery']) || !in_array($attachment_id, $this->p_data['image_gallery']) )
					wp_update_post( array( 'ID' => $attachment_id, 'post_parent' => 0 ) );			
			}			
		}			
		if ( !empty($this->p_data['thumbnail']) && is_string($this->p_data['thumbnail']) )
		{		
			$file_array = array(); 
			$tmp = download_url( $this->p_data['thumbnail'] );
			if( ! is_wp_error( $tmp ) )
			{
				preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $this->p_data['thumbnail'], $matches );
				$file_array['name'] = basename( $matches[0] );
				$file_array['tmp_name'] = $tmp;	
				$thumbnail_id = media_handle_sideload( $file_array, $this->product_id, $this->p_data['post_excerpt'] );
				if ( is_numeric($thumbnail_id))
					set_post_thumbnail( $this->product_id, $thumbnail_id );
			}
		}
	}
	
	 /**
	 * удалить файлы
	 */
	private function delete_files( $file_type ) 
	{
		$args = array(
			'post_type' => $file_type,
			'post_parent' => $this->product_id,
			'numberposts' => -1,
			'post_status' => 'all'
		);
		$preview_files = (array)get_posts( $args );
		foreach( $preview_files as $preview ) 
		{
			$preview_id = $preview->ID;
			wp_delete_post($preview_id);
		}
		return true;
	}
	
	/**
	* добавить файл предварительного просмотра элемента
	*/
	private function add_preview_file( $file ) 
	{
		$this->delete_files( "usam-preview-file" );
		return $this->insert_file($file, "usam-preview-file");
	}
	
	/**
	* Добавить загружаемый файл
	*/
	private function add_downloadable_file( $file ) 
	{
		return $this->insert_file($file, "usam-product-file");
	}
	
	private function insert_file( $file, $file_type ) 
	{		
		if ( !empty($file['file']) && !empty($file['url']) && !empty($file['type']) )
		{
			$name_parts = pathinfo($file['file']);
			$content = !empty($this->p_data['post_title'])?$this->p_data['post_title']:'';		
			$attachment = array(
				'post_mime_type' => $file['type'],
				'guid' => $file['url'],
				'post_parent' => $this->product_id,
				'post_title' => $name_parts['basename'],
				'post_content' => $content,
				'post_type' => $file_type,
				'post_status' => 'inherit'
			);
			$id = wp_insert_post($attachment, $file['file'], $this->product_id);		
			return $id;
		}
		return false;
	}
	
	/*
	 * Переназначать элемент файла
	 */
	private function item_reassign_file( $selected_files ) 
	{					
		$args = array(
			'post_type' => 'usam-product-file',
			'post_parent' => $this->product_id,
			'numberposts' => -1,
			'post_status' => 'any'
		);
		$attached_files = (array)get_posts($args);

		foreach($attached_files as $key => $attached_file)
			$attached_files_by_file[$attached_file->post_title] = $attached_files[$key];	

		foreach($selected_files as $selected_file) 
		{ // если мы уже используем этот файл, нет смысла делать что-то большее.
			$file_is_attached = false;
			$selected_file_path = USAM_FILE_DIR.basename($selected_file);

			if(isset($attached_files_by_file[$selected_file])) {
				$file_is_attached = true;
			}

			if($file_is_attached == false ) {
				$type = usam_get_mimetype($selected_file_path);
				$attachment = array(
					'post_mime_type' => $type,
					'post_parent' => $this->product_id,
					'post_title' => $selected_file,
					'post_content' => '',
					'post_type' => "usam-product-file",
					'post_status' => 'inherit'
				);
				wp_insert_post($attachment);
			} 
			else 
			{
				$product_post_values = array(
					'ID' => $attached_files_by_file[$selected_file]->ID,
					'post_status' => 'inherit'
				);
				wp_update_post($product_post_values);
			}
		}
		foreach($attached_files as $attached_file) 
		{
			if(!in_array($attached_file->post_title, $selected_files)) 
			{
				$product_post_values = array( 'ID' => $attached_file->ID, 'post_status' => 'draft' );
				wp_update_post( $product_post_values );
			}
		}
		return true;
	}	
	
	/**
	* Функция обработки полей формы продукта. Защищает от не стандартного заполнения формы.
	*/
	private function sanitise_product_forms( $post_data = null )
	{
		$post_data = stripslashes_deep( $post_data );
		
		if ( isset($post_data['tax_input']) )
		{
			$tax_input = array();
			foreach( $post_data['tax_input'] as $taxonomy => $terms )				
				foreach( $terms as $key => $term_id )
				{
					if ( is_numeric($term_id) && $term_id > 0 )
						$tax_input[$taxonomy][] = (int)$term_id;			
				}
			$post_data['tax_input'] = $tax_input;
		}				
		foreach ( $this->type_prices as $value )
		{			
			$price_key = "price_".$value['code'];
			$old_price_key = "old_price_".$value['code'];	
			if ( isset($post_data['meta'][$price_key]) )
				$post_data['meta'][$price_key] = abs(usam_string_to_float( $post_data['meta'][$price_key] ));
			
			if ( isset($post_data['meta'][$old_price_key]) )
				$post_data['meta'][$old_price_key] = abs(usam_string_to_float( $post_data['meta'][$old_price_key] ));
		}
		if ( isset($post_data['meta']['price_date']) )
			$post_data['meta']['price_date'] = date( "Y-m-d H:i:s", strtotime($post_data['meta']['price_date']));
			
		if( isset($post_data['meta']['sku']) && $post_data['meta']['sku'] == __('Нет данных', 'usam') )
			$post_data['meta']['sku'] = '';
		
		if( isset($post_data['meta']['weight']) )
			$post_data['meta']['weight'] = absint($post_data['meta']['weight']);
			
		$post_data['meta']['virtual'] = !empty($post_data['meta']['virtual'])?1:0;
	
		$default_data = $this->get_default_product();
		if ( isset($post_data['meta']['product_metadata']) )
		{							
			if ( isset($post_data['meta']['product_metadata']['dimensions']) ) 
			{
				$post_data['meta']['product_metadata']['dimensions']['width'] = abs(usam_string_to_float( $post_data['meta']['product_metadata']['dimensions']['width'] ));
				$post_data['meta']['product_metadata']['dimensions']['height'] = abs(usam_string_to_float( $post_data['meta']['product_metadata']['dimensions']['height'] ));				
				$post_data['meta']['product_metadata']['dimensions']['length'] = abs(usam_string_to_float( $post_data['meta']['product_metadata']['dimensions']['length'] ));
			}				
			// другие опции	
			if ( empty($post_data['meta']['product_metadata']['comment']) )
				$post_data['meta']['product_metadata']['comment'] = get_option( 'usam_comments_which_products', 1 );
			else
				$post_data['meta']['product_metadata']['comment'] = (int)$post_data['meta']['product_metadata']['comment'];			
			
			foreach ( $post_data['meta']['product_metadata'] as $key => $value )
			{
				if ( !isset($default_data['meta']['product_metadata'][$key]) )
					unset($post_data['meta']['product_metadata'][$key]);
			}
		}				
		if ( isset($post_data['meta']) )
		{			
			$keys = array();
			foreach ( $default_data['meta'] as $key => $data )
			{
				$keys[] = $key;		
			}
			foreach ( $post_data['meta'] as $key => $data )
			{
				if ( !in_array($key, $keys) )
					unset($post_data['meta'][$key]);
			}	
		}		
		return $post_data;
	}
	
	/**
	* Функция получения значений товара по умолчанию
	*/
	public function get_default_product( $meta = null )
	{		
		$default_product = array( 
			'post_title' => '',
			'post_content' => '',
			'post_excerpt' => '',	
			'post_parent' => 0,								
			'meta' => array(										
				'virtual' => 0,				
				'reserve' => 0,	
				'storage' => 0,											
				'stock' => 0,													
				'code' => '',											
				'sku' => '',
				'weight' => '',
				'barcode' => '',									
				'product_views' => 0,		
				'increase_sales_time' => '',
				'price_date' => '',										
				'rating' => 0,	
				'rating_count' => 0, //Количество проголосовавших за товар												
				'product_metadata' =>	array( 																				
					'components' => array(),
					'bonuses' => array ( 'value' => 0, 'type' => 'p' ),	
					'webspy_link' => '',										
					'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
					'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),
					'unit_measure' => '', //Коэффициент единицы измерения
					'unit' => 1,		 //единица измерения								
					'comment' => '', // Использовать комментарии										
					'image_swap' => 0,																								
				),								
			),			
		);
						
		foreach ( $this->type_prices as $type_price )
		{			
			$default_product['meta']["price_".$type_price['code']] = 0;		
			$default_product['meta']["old_price_".$type_price['code']] = 0;			
			$default_product['meta']["underprice_".$type_price['code']] = 0;			
		}	
		$storages = usam_get_stores();					
		foreach ( $storages as $storage )
		{	
			$default_product['meta'][$storage->meta_key] = 0;
		}	
		if ( $meta !== null && isset($default_product['meta'][$meta]) )
			$default_product = $default_product['meta'][$meta];
		return $default_product;
	}
	
	// Слить значения по умолчанию с заданными значениями
	private function merge_meta_deep( $original, $updated, $default ) 
	{		
		if ( !empty($updated) )
		{
			if ( is_array($original) )
			{			
				if (!empty($original))
				{	
					$keys = array_merge( array_keys( $original ), array_keys( $updated ) );							
				}
				else
					$keys = array_keys( $updated );	

				foreach ( $keys as $key ) 
				{
					if ( ! isset( $updated[$key] ) )
						continue;							
					if ( isset($original[$key]) && is_array( $original[$key] ) && is_array($updated[$key]) && !empty($default[$key]) && is_array($default[$key]))
						$original[$key] = $this->merge_meta_deep( $original[$key], $updated[$key], $default[$key] );
					else
					{	
					//	if ( (isset($original[$key]) && !is_array($original[$key])) || !isset($original[$key]) )						
							$original[$key] = $updated[$key];
					}
				}
			}
			else		
				$original = $updated;
		}
		return $original;
	}
	
	private function remove_unnecessary_keys( $original, $updated ) 
	{
		$new = array();
		$keys = array_keys( $original );		
		foreach ( $keys as $key ) 
		{	
			if ( is_array( $updated[$key]) )
				$new[$key] = $this->remove_unnecessary_keys( $original[$key], $updated[$key] );
			else
			{
				if ( isset($updated[$key]) )
					$new[$key] = $updated[$key];
				else
					$new[$key] = $original[$key];
			}
		}
		return $new;
	}
}

function usam_recalculate_price_products_ids( $ids )
{
	if ( !empty($ids) )
	{	
		if ( !is_array($ids) )
			$ids = array($ids);
		
		$args = array('post__in' => $ids );
		return usam_recalculate_price_products( $args );
	}
	else
		return 0;
}

function usam_recalculate_price_products( $args_query = array() )
{		
	if ( empty($args_query) )
	{
		$name = 'recalculate_price';
		$title = __("Пересчет цен","usam" );
		$events = usam_get_system_process( array('id_like' => 'recalculate_price_') );
		if ( !empty($events) )
			foreach ( $events as $id => $event ) 
				usam_delete_system_process( $id );				
	}
	else
	{
		$name = 'recalculate_price_'.time();		
		$title = __("Частичный пересчет цен","usam" );
	} 
	$args = $args_query;
	$args['fields'] = 'ids';	
	$args['update_post_meta_cache'] = false;	
	$args['update_post_term_cache'] = false;
	$products = usam_get_products( $args );

	usam_create_system_process( $title, $args_query, 'recalculate_price_products', count($products), $name );		
}


// Получить товары
function usam_get_products( $args = array(), $thumbnail_cache = false )
{
	$args['post_type'] = 'usam-product';	
	
	$default_args = array(		
		'posts_per_page'  => '-1',
		'orderby'         => 'date',
		'order'           => 'DESC',		
		'meta_key'        => '',
		'meta_value'      => '',
		'include'         => array(),
		'exclude'         => array(),		
	//	'post_parent'     => 0,
		'suppress_filters' => true,
		'post_status'     => array( 'private', 'draft', 'pending', 'publish', 'future' )
	);		
	$args['ignore_sticky_posts'] = true;
	$args['no_found_rows'] = true;

	$args = array_merge( $default_args, (array)$args );	
	
	$get_posts = new WP_Query;
	$results = $get_posts->query($args);	
	
	if ( $thumbnail_cache )
		update_post_thumbnail_cache( $get_posts );	
		
	return 	$results;
}

// Получить файлы товаров
function usam_get_product_files( $args = array() )
{
	$args['post_type'] = 'usam-product-file';
	$default_args = array(		
		'posts_per_page'  => '-1',
		'orderby'         => 'post_date',
		'order'           => 'DESC',			
		'post_status'     => 'all'
	);	
	$args = array_merge( $default_args, (array)$args );		
	return $products = (array)get_posts( $args );
}

// Изменить цену товара
function usam_edit_product_prices( $product_id, $prices = array() )
{			
	$_product = new USAM_Product( $product_id );
	
	if ( !empty($prices) )
		$_product->set( array( 'meta' => $prices ) );	
	
	$_product->calculate_prices( );	
	$_product->update_product_metas( );
}


// Удалить информацию о товаре при удалении самого товара
function _usam_delete_product( $product_id )
{
	global $wpdb; 	
	$wpdb->query( "DELETE FROM ".USAM_TABLE_ASSOCIATED_PRODUCTS." WHERE product_id='$product_id' OR associated_id='$product_id'" );	
	$wpdb->query( "DELETE FROM ".USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS." WHERE product_id='$product_id'" );	
	$wpdb->query( "DELETE FROM ".USAM_TABLE_USER_PRODUCTS." WHERE product_id='$product_id'" );		
}
add_action( 'before_delete_post',  '_usam_delete_product' );