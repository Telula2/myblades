<?php
/**
 * Класс уведомлений
 * @since 3.7
*/
abstract class USAM_Notification
{	
	protected $args = array();	
	protected $plaintext_message = '';
	protected $html_message = '';
	protected $sms_message = '';
	
	protected $object_type = '';
	protected $object_id = '';
	
	protected $email = '';
	protected $phone = '';	
	
	protected function process_plaintext_args() 
	{
		$message = $this->get_raw_message();
		$args = $this->get_plaintext_args();
		
		$shortcode = new USAM_Shortcode();
		return $shortcode->process_args( $args, $message );
	}
	
// из аргументов собрать строку для письма
	protected function process_html_args() 
	{					
		$message = $this->get_raw_message();
		$args = $this->get_html_args();
	
		$shortcode = new USAM_Shortcode();
		$html = $shortcode->process_args( $args, $message );
		
		$html = wpautop( $html );		
		$html = str_replace( "<br />", '<br /><br />', $html);	
		return $html;
	}
	
	protected function process_sms_message_args() 
	{
		$message = $this->get_raw_sms_message();
		$args = $this->get_plaintext_args();
		
		$shortcode = new USAM_Shortcode();
		return $shortcode->process_args( $args, $message );
	}
	
	protected function get_plaintext_args() 
	{				
		return $this->get_args();
	}
	
	protected function get_html_args() 
	{				
		return $this->get_args();
	}

	protected function get_args() 
	{	
		return array();
	}	
	
	public function get_raw_message() 
	{	
		return '';
	}
	
	public function get_raw_sms_message() 
	{	
		return '';
	}	
	
	public function get_subject() 
	{
		return '';
	}
	
	public function set_address( $email ) 
	{
		$this->email = $email;
	}

	public function get_address() 
	{
		return $this->email;
	}
	
	public function set_phone( $phone ) 
	{
		$this->phone = $phone;
	}
	
	public function get_phone() 
	{ 
		return $this->phone;
	}
	
	public function get_attachments() 
	{
		return array();
	}

	public function get_html_message() 
	{
		return $this->html_message;
	}

	public function get_plaintext_message() 
	{
		return $this->plaintext_message;
	}

	public function get_sms_message() 
	{
		return $this->sms_message;
	}	
	
//отправка писем
	public function send_mail() 
	{
		$to_email = $this->get_address();		
		if ( empty( $to_email ) )
			return;

		$message = $this->get_html_message();	
		if ( empty( $message ) )
			return;
		
		$subject       = $this->get_subject();
		$attachments = $this->get_attachments();		
		add_action( 'phpmailer_init', array( $this, '_action_phpmailer_init_multipart' ), 10, 1 );
		
		$mailbox = usam_get_primary_mailbox();			
		if ( empty($mailbox['id']) )
			return;	
		
		$style = new USAM_Mail_Styling();
		$message = $style->process_plaintext_args( $message );
		
		$insert_email = array( 'body' => $message, 'subject' => $subject, 'to_email' => $this->email, 'mailbox_id' => $mailbox['id'] );	
		if ( $this->object_type != '' && $this->object_id != '' )
		{
			$insert_email['object_type'] = $this->object_type;
			$insert_email['object_id'] = $this->object_id;
		}	
		$_email = new USAM_Email( $insert_email );
		$_email->save();	
		
		$file_attachments = array();
		foreach ( $attachments as $attachment )
			$file_attachments[] = array( 'file_path' => $attachment ); 
		
		$_email->set_attachments( $file_attachments );
		$email_sent = $_email->send_mail();		
		remove_action( 'phpmailer_init', array( $this, '_action_phpmailer_init_multipart' ), 10, 1 );
		return $email_sent;		
	}
	
	//отправка смс
	public function send_sms() 
	{		
		$phone = $this->get_phone();		
		
		if ( empty( $phone ) )
			return;

		$message = $this->get_sms_message();			
		if ( empty( $message ) )
			return;		
				
		$sent = usam_send_sms( $phone, $message, $this->object_id, $this->object_type );
		return $sent;		
	}
	
	public function _action_phpmailer_init_multipart( $phpmailer )
	{
		$phpmailer->AltBody = $this->plaintext_message;
	}
}
?>