<?php
function usam_locations_checklist( $args = array() ) 
{	
	require_once( USAM_FILE_PATH . '/includes/walker-location-list.class.php' );
	
	$defaults = array(
		'descendants_and_self' => 0,
		'selected' => false,	
		'walker' => null,			
		'echo' => true,	
		'type_display' => 'detail',	
	);
	$params = apply_filters( 'usam_locations_checklist_args', $args );

	$r = wp_parse_args( $params, $defaults );

	if ( empty( $r['walker'] ) || ! ( $r['walker'] instanceof Walker ) ) 
		$walker = new Walker_Location_Checklist;
	else 
		$walker = $r['walker'];
	
	$descendants_and_self = (int) $r['descendants_and_self'];	

	$args['list_only'] = ! empty( $r['list_only'] );
	$args['type_location_to'] = !empty($args['type_location_to'])?$args['type_location_to']:'city';
	
	$type_location = usam_get_type_location( $args['type_location_to'], 'code' );
	$args['id_type'] = $type_location['id'];

	if ( is_array( $r['selected'] ) ) {
		$args['selected'] = $r['selected'];	
	} else {
		$args['selected'] = array();
	}
	if ( $descendants_and_self )
	{
		/*$categories = (array) get_terms( $taxonomy, array(
			'child_of' => $descendants_and_self,
			'hierarchical' => 0,
			'hide_empty' => 0
		) );
		$self = get_term( $descendants_and_self, $taxonomy );
		array_unshift( $categories, $self );*/
	} 
	else 
		$categories = (array) usam_get_locations( array( 'type_to' => $args['type_location_to'] ) );	
	
	$args['after'] = !empty($args['after'])?$args['after']:'';
	$args['before'] = !empty($args['before'])?$args['before']:'';	

	$output = '';
	if ( $r['type_display'] == 'detail' )
	{	
		$output .= "<div class ='display_locations'>";
		$output .= "<h4>".__( 'Выбор местоположений', 'usam' ).":</h4>";
		$output .= "<ul>";
		$output .= call_user_func_array( array( $walker, 'walk' ), array( $categories, 0, $args ) );
		$output .= "</ul>";
		$output .= "</div>";
		$output .= '<div class ="display_locations_select">';
		$output .= "<h4>". __( 'Выбранные местоположения', 'usam' ).":</h4>";
		$output .= "<ul>";									
		foreach ( $args['selected'] as $location_id )			
		{		
			$str = usam_get_full_locations_name( $location_id );
			$output .= "<li>$str</li>";
		}
		$output .= "</ul></div>";	
	}	
	else
		$output .= call_user_func_array( array( $walker, 'walk' ), array( $categories, 0, $args ) );

	if ( $r['echo'] ) 
		echo $output;

	return $output;
}

/* $size: large, small */
function usam_get_modal_window( $name, $key, $body = '', $size = 'large' ) 
{
	if ( $body == '' )
	{
		$body = '<p style="height: 400px; line-height: 400px; text-align: center;">'.usam_get_loader().'<p>';
	}
	ob_start();		
	?>	
	<div id="<?php echo $key; ?>" class="modal fade hide modal-<?php echo $size; ?>">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<div class = "header-title"><?php echo $name; ?></div>
		</div>
		<div class="modal-body"><?php echo $body; ?></div>
	</div>
	<?php
	$return = ob_get_contents();
	ob_end_clean();
	return $return;
}

function usam_get_switch( $checked, $style = 1 ) 
{
	ob_start();		
	?>	
	<div class="switch style<?php echo 1; ?>">
		<input type="checkbox" <?php checked( $checked, 1 ); ?>>
		<label><i></i></label>
	</div>	
	<?php
	$return = ob_get_contents();
	ob_end_clean();
	return $return;
}

function usam_display_date_interval( $date1, $date2, $id = 'interval', $echo = true, $from_gmt = true ) 
{	
	if ( $date1 != '' )
	{		
		if ( is_numeric( $date1 ) )
		{			
			$date1 = date("Y-m-d H:i:s", $date1);			
		}	
		if ( $from_gmt )
			$date = get_date_from_gmt( $date1, "Y-m-d" );	
		else
			$date = date("Y-m-d", strtotime($date1) );
		
		$time = strtotime($date);

		$date1 = date("d-m-Y", $time);	
		$h = date( 'H', $time );
		$m = date( 'i', $time );	
	}
	else
	{
		$date1 = '';	
		$h = '';	
		$m = '';
	}	
	if ( $date2 != '' )
	{						
		if ( is_numeric( $date2 ) )
		{			
			$date2 = date("Y-m-d H:i:s", $date2); 
		}		
		if ( $from_gmt )
			$date = get_date_from_gmt( $date2, "Y-m-d" );	
		else
			$date = date("Y-m-d", strtotime($date2) );	
		
		$time = strtotime($date);
	
		$date2 = date("d-m-Y", $time);	
		$h = date( 'H', $time );
		$m = date( 'i', $time );	
	}
	else
	{
		$date2 = '';	
		$h = '';	
		$m = '';
	}	
	ob_start();	
		
	?>	
	<input type="text" class="date_interval" name="date_from_<?php echo $id; ?>" id="date_from_<?php echo $id; ?>" maxlength="10" style ="width:90px" value="<?php echo $date1; ?>" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" /> - <input type="text" class="date_interval" name="date_to_<?php echo $id; ?>" id="date_to_<?php echo $id; ?>" maxlength="10" style ="width:90px" value="<?php echo $date2; ?>" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}" />
	
	<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			jQuery( ".date_interval" ).datepicker({
					dateFormat: "dd-mm-yy",
					numberOfMonths: 1,
					//showButtonPanel: true,
					hideIfNoPrevNext: true,	
			});	
		});						
	</script>	
	<?php	
	$return = ob_get_contents();	
	ob_end_clean();	
	if ( $echo )
		echo $return;
	else
		return $return;
}

function usam_display_datetime_picker( $id = 'current_time', $date_time = '', $fields = array('time') ) 
{
	echo usam_get_display_datetime_picker( $id, $date_time, $fields );
}

function usam_get_display_datetime_picker( $id = 'current_time', $date_time = '', $fields = array('time') ) 
{	
	if ( !empty($fields) && in_array('time', $fields) )
	{
		$fields = array( 'hour', 'minute' );
	}	
	if ( $date_time != '' )
	{	
		if ( is_numeric( $date_time ) )
		{			
			$date_time = date("Y-m-d H:i:s", $date_time);			
		}				
		$date = get_date_from_gmt($date_time, "Y-m-d H:i:s");	
		$time = strtotime($date);

		$date = date("d-m-Y", $time);	
		$h = date( 'H', $time );
		$m = date( 'i', $time );	
	}
	else
	{
		$date = '';	
		$h = '';	
		$m = '';
	}	
	$out = "<span id='datetime_picker'><input type='text' class='date-picker-field' name='date_{$id}' id='usam_date_picker-{$id}' maxlength='10' value='{$date}' pattern='(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}' />";
	if ( in_array('hour', $fields) ) 
	{ 
		$out .= "@<input type='text' class='hour' placeholder='".__( 'час', 'usam' )."' name='date_hour_{$id}' id='usam_date_hour-{$id}' maxlength='2' size='2' value='{$h}' pattern='(0[0-9]|1[0-9]|2[0-4])' />";
		if ( in_array('minute', $fields) ) 
		{	
			$out .= ":<input type='text' class='minute' placeholder='".__( 'мин', 'usam' )."' name='date_minute_{$id}' id='usam_date_minute-{$id}' maxlength='2' size='2' value='{$m}' pattern='(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]|6[0-9])' />";
		} 
	} 	
	$out .= '</span>
	<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			jQuery( ".date-picker-field" ).datepicker({
				dateFormat: "dd-mm-yy",
				numberOfMonths: 1,
				//showButtonPanel: true,
				hideIfNoPrevNext: true,			
			});	
		});			
	</script>';	
	return $out;	
}

function usam_display_date_picker( $id = 'current_time', $date_time = '', $echo = true ) 
{	
	$out = usam_get_display_datetime_picker( $id, $date_time, array() );
	if ( $echo )
		echo $out;
	else
		return $out;
}

function usam_get_datepicker( $format = '' ) 
{	
	if ( !empty($_REQUEST['date_'.$format]) )
	{ 
		$gmt = true;
		if ( !empty($_REQUEST['date_hour_'.$format]) )
			$date_hour = (int)$_REQUEST['date_hour_'.$format];
		else
			$date_hour = '00';	
	
		if ( !empty($_REQUEST['date_minute_'.$format]) )	
			$date_minute = (int)$_REQUEST['date_minute_'.$format];
		else	
			$date_minute = '00';
		
		$date = explode('-', $_REQUEST['date_'.$format]);
		$result_date = date("Y-m-d H:i:s", mktime($date_hour,$date_minute,0,$date[1],$date[0],$date[2]));					
		
		if ( $gmt )
			$result_date = get_gmt_from_date($result_date, "Y-m-d H:i:s");		
	}	
	else
		$result_date = false;
	return $result_date;
}
?>