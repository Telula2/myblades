<?php
/**
 * Функции для отображения категорий скидок
 * @since 3.7
 */
 
/**
* Получить ссылку на категорию
*/
function usam_get_category_sale( $args = array() )
{
	$args['meta_key'] = 'usam_status_stock';
	$args['meta_value'] = 1;
	
	if ( !isset($args['hide_empty']) )
		$args['hide_empty'] = false;
	
	if ( !isset($args['orderby']) )
		$args['orderby'] = 'id';
	
	if ( !isset($args['order']) )
		$args['order'] = 'DESC';
	
	$terms = get_terms('usam-category_sale', $args ); 	
	$category_sale = array();
	$time = time();
			
	foreach( $terms as $term ) 
	{
		$start_date = get_term_meta($term->term_id, 'usam_start_date_stock', true);	
		$end_date = get_term_meta($term->term_id, 'usam_end_date_stock', true);							
		if ( ( !empty($start_date) && strtotime($start_date) > $time ) || (!empty($end_date) && strtotime($end_date) < $time) ) 
			continue;
		
		$area = get_term_meta($term->term_id, 'usam_sale_area', true);		
		if ( !usam_in_customer_sales_area($area) )
		{
			continue;
		}
		$category_sale[] = $term;
	}
	return $category_sale;
}

?>