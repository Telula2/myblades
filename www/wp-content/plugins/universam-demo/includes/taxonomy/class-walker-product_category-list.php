<?php
/* Строит меню категорий */
class Walker_Product_Category_List extends Walker
{
	public $tree_type = 'category';
	public $db_fields = array ('parent' => 'parent', 'id' => 'term_id'); 
	public static $number = 0;	
	private $start_with_categories = null;	
	
	public function __construct() 
	{		
		
	}
	
	public function get_count_category_children( $split = 2 ) 
	{
		$category_children = get_option('usam-category_children');	
		$cat_count = array();
		$count = 0;
		$current_default   = get_option( 'usam_default_menu_category', 0 );	
		$categories = get_terms( 'usam-category', "hierarchical=false&parent=$current_default&update_term_meta_cache=0&fields=ids" );	
		foreach ( $categories as $id ) 
		{
			$cat_count[$id] = 1;
			if ( isset($category_children[$id]) )
			{
				$children = $category_children[$id];
				$cat_count[$id] = + count($children) + $this->_get_count_category_children( $children );
				$count += $cat_count[$id];
			}		
		}	
		$result = ceil( $count / $split );
		
		$start_with_categories = array();	
		$sum = 0;	
		foreach ( $cat_count as $id => $count ) 
		{
			$sum += $count;			
			if ( $sum >= $result )
			{
				$sum = 0;
				$start_with_categories[] = $id;
			}			
		}	
		return $start_with_categories;
	}
	
	public function start_lvl( &$output, $depth = 0, $args = array() ) 
	{			
		$indent = str_repeat("\t", $depth);	
		$output .= "\n$indent<ul class=\"categories_list\">\n";	
	}
	
	public function end_lvl( &$output, $depth = 0, $args = array() ) 
	{
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";	
	}
	
	public function _get_count_category_children( $_category_children ) 
	{
		$category_children = get_option('usam-category_children');
		$count = 0;
		foreach ( $_category_children as $id ) 
		{			
			if ( isset($category_children[$id]) )
				$count += $this->_get_count_category_children( $category_children[$id] );
		}	
		return $count;
	}
	
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
	{	
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'category_' . $item->term_id;
		$classes[] = 'category_level_' . $depth;
		if ( !empty($args['select']) && in_array($item->term_id, $args['select'])  )
			$classes[] = 'select_category';		
		
		$args = apply_filters( 'category_list_item_args', $args, $item, $depth );
		
		$class_names = join( ' ', apply_filters( 'product_category_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		
		$id = apply_filters( 'category_list_item_id', 'menu-item-'. $item->term_id, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
	
		$output .= $indent . '<li' . $id . $class_names .'>';	

		$atts = array();
		$atts['title']  = ! empty( $item->slug )   ? $item->slug : '';
		$atts['class']  = ! empty( $item->class )  ? $item->class : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )    ? $item->xfn        : '';
		
		$term_link   = get_term_link($item->term_id, $item->taxonomy);	
		
		$permalinks  = get_option( 'usam_permalinks' );
		if ( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'sale' ) //Если страница распродаж
		{		
			$atts['href'] = str_replace( $permalinks['category_base'], 'sale', $term_link );	
		}
		elseif ( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'new-arrivals' ) //Если страница новинки		
		{		
			$atts['href'] = str_replace( $permalinks['category_base'], 'new-arrivals', $term_link );	
		}
		elseif ( isset($wp_query->query['usam-brands']) ) //Если бренды		
		{	
			if ( !empty($wp_query->query['usam-brands']) )
				$atts['href'] = str_replace($permalinks['category_base'], "brands/".$wp_query->query['usam-brands'], $term_link);
			else
				$atts['href'] = $term_link;
		}	
		elseif ( isset($wp_query->query['usam-category_sale']) ) //Если бренды		
		{
			if ( !empty($wp_query->query['usam-category_sale']) )
				$atts['href'] = str_replace($permalinks['category_base'], "category_sale/".$wp_query->query['usam-category_sale'], $term_link);
			else
				$atts['href'] = $term_link;
		}
		else
			$atts['href'] = $term_link;
		
		$atts = apply_filters( 'category_list_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) 
		{
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}	
		$title = apply_filters( 'category_list_item_title', $item->name, $item, $args, $depth );

		if ( !empty($args['count']) )			
			$title .= " ($item->count)";	
		
		$item_output = $args['before'];
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args['link_before']. $title . $args['link_after'];
		$item_output .= '</a>';
		$item_output .= $args['after'];		
		$output .= apply_filters( 'walker_product_category_list_start_el', $item_output, $item, $depth, $args );
		
	}
	
	public function end_el( &$output, $item, $depth = 0, $args = array() ) 
	{	
		$output .= "</li>\n";
		switch ($depth)
		{
			case 0:						
				self::$number ++;	

				if ( $this->start_with_categories === null && !empty($args['split']) )
					$this->start_with_categories = $this->get_count_category_children( $args['split'] );
				
				if ( !empty($this->start_with_categories) && in_array( $item->term_id, $this->start_with_categories ) )			
				{				
					self::$number = 0;
					$output .= '</ul><ul class ="main_categories">';				
				}					
			break;		
		}
	}
} 