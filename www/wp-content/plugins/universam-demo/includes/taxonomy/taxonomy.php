<?php
/**
 * Общие функции для работы с таксоманиями
 * @since 3.7
 */

/**
* Получает изображение термина или возвращает ложь
*/
function usam_taxonomy_image( $taxonomy_id, $size ) 
{
	if($taxonomy_id < 1)
		return false;
	
	$attachment_id = (int)get_term_meta($taxonomy_id, 'thumbnail', true);
	
	if ( $attachment_id == 0  )
		return false;
	
	$image_attributes = wp_get_attachment_image_src( $attachment_id, $size );	
	return $image_attributes[0];
}

function usam_taxonomy_thumbnail( $taxonomy_id, $size, $title = '' ) 
{ 
	$attachment_id = (int)get_term_meta($taxonomy_id, 'thumbnail', true);
	
	if ( $attachment_id == 0  )
		return false;
	
	$image_attributes = wp_get_attachment_image_src( $attachment_id, $size );	
	
	if ( empty($image_attributes) )
		return false;
	
	$srcset = wp_get_attachment_image_srcset( $attachment_id, 'large' );
	$sizes = wp_get_attachment_image_sizes( $attachment_id, 'large' );
	
	$src = "<img src='".$image_attributes[0]."' width='".$image_attributes[1]."' height='".$image_attributes[2]."' alt='$title' srcset='$srcset' sizes='$sizes'>";		
	return $src;
}

/**
* Получить все параметры термина
*/
function usam_get_term_parents( $term_id, $taxonomy ) 
{
	$term = &get_term( $term_id, $taxonomy );

	if(empty($term->parent))
		return array();
	$parent = &get_term( $term->parent, $taxonomy );
	if ( is_wp_error( $parent ) )
		return array();

 	$parents = array( $parent->term_id );

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $parents ) ) {
		$parents = array_merge($parents, usam_get_term_parents( $parent->term_id, $taxonomy ));
	}

	return $parents;
}

/**
 * Используется в основном в связи с целевым рынкам.
 */
function usam_is_suhosin_enabled() 
{
	return @ extension_loaded( 'suhosin' ) && @ ini_get( 'suhosin.post.max_vars' ) > 0 && @ ini_get( 'suhosin.post.max_vars' ) < 500;
}

// Описание название термина
function usam_product_taxonomy_name() 
{
	usam_product_category_name();
	usam_print_product_brand_name();
}

// Описание вывести термина
function usam_product_taxonomy_description() 
{
	usam_product_category_description();
	usam_print_product_brand_description();
}

function usam_get_walker_terms_list( $args_terms, $args_list ) 
{
	$default_args_list = array(								
		'taxonomy'             => 'usam-category',		
		'select'               => array( ),					
		'checked_ontop'        => false, 
		'echo'                 => false,
		'before' => '', 
		'after' => '',				
		'link_before' => '', 
		'link_after' => '',				
	);				
	$args_list = array_merge( $default_args_list, (array)$args_list );
	
	require_once( USAM_FILE_PATH . '/includes/taxonomy/class-walker-product_category-list.php'     );	
	$terms = get_terms( $args_list['taxonomy'], $args_terms );				

	$walker = new Walker_Product_Category_List();
	$html = '<ul class ="main_categories">'.call_user_func_array( array( $walker, 'walk' ), array( $terms, 0, $args_list ) ).'</ul>';
	return $html;
}

function usam_set_taxonomy_relationships( $term_id1, $term_id2, $taxonomy  ) 
{
	global $wpdb;
	$term_id1 = (int)$term_id1;
	$term_id2 = (int)$term_id2;
	
	$sql = "INSERT INTO `".USAM_TABLE_TAXONOMY_RELATIONSHIPS."` (`term_id1`,`term_id2`,`taxonomy`) VALUES ('%d','%d','%s') ON DUPLICATE KEY UPDATE `term_id2`='%d'";					
	$insert = $wpdb->query( $wpdb->prepare($sql, $term_id1, $term_id2, $taxonomy, $term_id2 ) );
	
	return $insert;
}

function usam_get_taxonomy_relationships( $term_id, $taxonomy, $colum = '2' ) 
{
	global $wpdb;
	
	if ( is_array($term_id) )
		$in = implode("','",$term_id);
	else			
		$in = "$term_id";
	
	if ( $colum == 2 )
	{
		$colum = 'term_id2';
		$fled = 'term_id1';
	}
	else
	{
		$colum = 'term_id1';
		$fled = 'term_id2';
	}	
	if ( !taxonomy_exists($taxonomy) )
		return array();	
	
	$result = $wpdb->get_col( "SELECT DISTINCT $fled FROM ".USAM_TABLE_TAXONOMY_RELATIONSHIPS." WHERE $colum IN ('$in') AND taxonomy = '$taxonomy'" );	
	return $result;
}

function usam_delete_taxonomy_relationships( $delete ) 
{
	global $wpdb;	
				
	$result = $wpdb->delete( USAM_TABLE_TAXONOMY_RELATIONSHIPS, $delete );
	return $result;
}

function usam_get_product_term_ids( $product_id, $term_name = 'usam-category' ) 
{
	$product = get_post( $product_id );		
	if ( $product->post_parent )							
		$terms = get_the_terms( $product->post_parent, $term_name );	
	else
		$terms = get_the_terms( $product_id, $term_name );	

	if ( is_wp_error( $terms ) )
		return array();	
		
	$ids = array();
	if ( !empty( $terms ))
	{ 		
		foreach( $terms as $term )
		{					
			$ids[] = $term->term_id;
			$ancestors = get_ancestors( $term->term_id, $term_name );
			$ids = array_merge( $ids, $ancestors );			
		}	
	}
	return $ids;
}


new USAM_Taxonomy_Save();
class USAM_Taxonomy_Save
{
	function __construct( ) 
	{		
		add_action( 'created_usam-product_attributes', array( &$this, 'save' ), 10 , 2 ); 
		add_action( 'edited_usam-product_attributes', array( &$this, 'save' ), 10 , 2 ); 
		
		add_action( 'create_usam-brands', array( &$this, 'save'), 10 , 2 ); 
		add_action( 'edited_usam-brands', array( &$this, 'save'), 10 , 2 ); 
		
		add_action( 'created_usam-category', array( &$this, 'save' ), 10 , 2 );
		add_action( 'edited_usam-category', array( &$this, 'save' ), 10 , 2 ); 
		
		add_action( 'created_usam-category_sale', array( &$this, 'save' ), 10 , 2 ); 
		add_action( 'edited_usam-category_sale', array( &$this, 'save' ), 10 , 2 ); 	
		
		add_action( 'created_usam-variation', array( &$this, 'save' ), 10 , 2 ); 
		add_action( 'edited_usam-variation', array( &$this, 'save' ), 10 , 2 ); 
		add_action( 'set_object_terms', array( &$this, 'set_object_terms' ), 10, 6 );
	}		
		
	function set_object_terms( $object_id, $terms, $tt_ids, $taxonomy, $append, $old_tt_ids )
	{
		if ( $taxonomy == 'usam-brands' || $taxonomy == 'usam-category' || $taxonomy == 'usam-category_sale' )
		{
			$ids = array_merge( $tt_ids, $old_tt_ids );		
			$args_query['tax_query'] = array(
				'relation' => 'OR',
				array(
					'taxonomy' => $taxonomy,
					'field' => 'term_id',
					'terms' => $ids,
				),			
			);
			usam_recalculate_price_products( $args_query );	
		}
	}

	/**
	 * Сохраняет данные
	 */
	function save( $term_id, $tt_id )
	{				
		add_term_meta( $term_id, 'usam_sort_order', 999 , true );
	}
}
?>