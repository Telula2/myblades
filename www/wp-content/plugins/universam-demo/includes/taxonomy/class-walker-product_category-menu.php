<?php
/* Строит меню категорий */
class Walker_Product_Category_Nav_Menu extends Walker
{
	public $tree_type = 'category';
	public $db_fields = array ('parent' => 'parent', 'id' => 'term_id'); 
	public static $number = 0;	
	public $term_link;
	
	public function start_lvl( &$output, $depth = 0, $args = array() ) 
	{			
		$indent = str_repeat("\t", $depth);			
		switch ( $depth )
		{
			case 0:					
				self::$number = 0;
				$output .= "\n$indent<ul class=\"sub-menu sub-menu-$depth\"><dl class=\"dl-sub-menu dl-sub-menu-$depth\">\n";
			break;
			case 1:					
				$output .= "\n$indent<dl class=\"dl-sub-menu dl-sub-menu-$depth\">\n";			
			break;		
			default:
				$output .= "\n$indent<ul class=\"sub-menu sub-menu-$depth\">\n";								
			break;					
		}		
	}
	
	public function end_lvl( &$output, $depth = 0, $args = array() ) 
	{
		$indent = str_repeat("\t", $depth);				
		switch ( $depth )
		{			
			case 0:		
				$speicalbox = '
				<div class = "speicalbox">
					<div class = "title">'.__('Специальные предложения','usam').'</div>
					<dl class="sub-menu_speical">
						<dt><a href="'.$this->term_link.'?orderby=sale" >'.__('Акционные','usam').'</a></dt>
						<dt><a href="'.$this->term_link.'?orderby=product_news" >'.__('Новинки','usam').'</a></dt>
					</dl>
				</div>';
				$output .= "$indent</dl>$speicalbox</ul>\n";
			break;
			case 1:						
				$output .= "$indent</dl>\n";
			break;		
			default:
				$output .= "$indent</ul>\n";				
			break;					
		}		
	}
	
	public function get_count_category_children( $term_id ) 
	{
		$category_children = get_option('usam-category_children');
		foreach ( $category_children as $children ) 
		{
			if ( in_array($term_id,$children) )	
			{			
				return ceil( count( $children ) / 2 );
			}
		}
		return 0;
	}
	
	public function get_term_link_category_children( $term_id ) 
	{
		$category_children = get_option('usam-category_children');
		foreach ( $category_children as $children ) 
		{
			if ( in_array($term_id,$children) )	
			{
				$this->term_link = get_term_link($term_id, 'usam-category');			
			}
		}	
	}
	
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
	{	
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->term_id;
		
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
		
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->term_id, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
	
		switch ( $depth )
		{			
			case 1:						
				$output .= $indent . '<dt' . $id . $class_names .'>';
			break;
			case 2:						
				$output .= $indent . '<dd' . $id . $class_names .'>';
			break;	
			case 0:					
				$this->get_term_link_category_children( $item->term_id );			
			default:
				$output .= $indent . '<li' . $id . $class_names .'>';
			break;					
		}	

		$atts = array();
	//	$atts['title']  = ! empty( $item->slug ) ? $item->slug : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )   ? $item->xfn        : '';
		$atts['href']   = get_term_link($item->term_id, $item->category );
		
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) 
		{
			if ( ! empty( $value ) ) 
			{
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}	
		$title = apply_filters( 'nav_menu_item_title', $item->name, $item, $args, $depth );

		$item_output = $args['before'];
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args['link_before']. $title . $args['link_after'];
		$item_output .= '</a>';
		$item_output .= $args['after'];

		$output .= apply_filters( 'walker_product_category_nav_menu_start_el', $item_output, $item, $depth, $args );
		
	}
	
	public function end_el( &$output, $item, $depth = 0, $args = array() ) 
	{	
		switch ( $depth )
		{
			case 1:						
				$output .= "</dt>\n";
				self::$number ++;
				$count = $this->get_count_category_children( $item->term_id );
				
				$item->name .= ' number= '.self::$number.' count '.$count;			
				if ( $count == self::$number )			
				{				
					$output .= "</dl><dl class=\"dl-sub-menu dl-sub-menu-$depth\">";				
				}					
			break;
			case 2:						
				$output .= "</dd>\n";
			break;	
			case 0:				
			default:
				$output .= "</li>\n";
			break;					
		}
	}
} 