<?php

function usam_get_the_brand_id( $slug, $type = 'name')
{	
	$brand = get_term_by( $type, $slug, 'usam-brands');
	return empty( $brand ) ? false : $brand->term_id;
}
	
function usam_product_brand( $product_id ) 
{
	$brand = get_the_terms($product_id, 'usam-brands' );	
	return $brand[0];	
}

function usam_get_product_brand_name( $product_id ) 
{
	$brand = usam_product_brand( $product_id );
	$brand_name = !empty($brand->name)?$brand->name:'';
	return $brand_name;	
}

/**
 * Возвращает тип отображения
 * @since 3.8
 */
function usam_get_the_brand_display( $slug )
{
	$brand_id = usam_get_the_brand_id( $slug , 'slug' );
	$display_type = get_term_meta( $brand_id, 'usam_display_type' , true);		
	return  $display_type;
}

/**
* Получает изображение бренда или возвращает ложь
*/
function usam_brand_image( $brand_id ) 
{
	$brand_image = get_option( 'usam_brand_image' );
	$size = array( $brand_image['width'], $brand_image['height'] );
	return usam_taxonomy_image( $brand_id, $size );	
}

function usam_brand_id_post( $id_post = null )
{	
	global $post;
	if (empty($id_post))
		$id_post = $post->ID;
	$term_list = wp_get_post_terms($id_post, 'usam-brands', array("fields" => "ids"));
	if ( empty($term_list) )
		return 0;
	else
		return $term_list[0];
}

function usam_brand_name_id_post( $id_post = null )
{	
	global $post;
	if (empty($id_post))
		$id_post = $post->ID;
	$term_list = wp_get_post_terms($id_post, 'usam-brands', array("fields" => "names"));
	if ( empty($term_list) )
		return 0;
	else
		return $term_list[0];
}

/**
* Вывести описание бренда
*/
function usam_brand_description( $brand_id = null )
{
	if($brand_id < 1)
	{
		$brand_id = usam_brand_id_post( $brand_id );
		if ( empty($brand_id) )
			return false;
	}		
	$brand = get_term_by('id', $brand_id, 'usam-brands' );
	
	if ( empty($brand->description) )
		return false;
	$link = get_term_meta($brand_id, 'link', true);
	$text_link = '';
	if ( $link != '' )
	{
		$host = parse_url($link, PHP_URL_HOST);
		$text_link = "<p>".__('Сайт производителя','usam').": <a target='_blank' title ='".__('Перейти на официальный сайт','usam')."' href='".$link."'>".$host."</a></p>";
	}	
	$out = "<div class='brand_header' id='brand_header'>
					<h5>".__('Бренд','usam').": <a title ='".__('Посмотреть все товары бренда','usam')."' href='".usam_brand_url( $brand_id )."'>".$brand->name."</a></h4></ br>
					$text_link
				</div><p>$brand->description</p>";
	return $out;
}
	
/**
* Получить ссылку на бренд
*/
function usam_brand_url( $brand_id )
{
  return get_term_link( $brand_id, 'usam-brands' );
}

// Вывести название бренда
function usam_print_product_brand_name(  ) 
{
	global $wp_query;		
	if ( (isset($wp_query->query_vars['usam-brands']) || ( isset($wp_query->query_vars['taxonomy']) && $wp_query->query_vars['taxonomy'] == 'usam-brands' )) && get_option( 'usam_display_category_name', 0 ) )
	{
		$term = get_queried_object();
		if ( !empty($term->name) && $term->taxonomy == 'usam-brands' )
		{			
			?><h1 class ="brand_name"><?php echo $term->name; ?></h1><?php
		}
	}
}

function usam_print_product_brand_description( ) 
{
	global $wp_query;
	if ( (isset($wp_query->query_vars['usam-brands']) || ( isset($wp_query->query_vars['taxonomy']) && $wp_query->query_vars['taxonomy'] == 'usam-brands' )) && get_option( 'usam_category_description', 0 ) )
	{
		$term = get_queried_object();		
		if ( !empty($term->description) && $term->taxonomy == 'usam-brands' )
		{
			?>
			<div id='product_category_description' class='product_category_description'>
				<div class='title'><strong><?php echo $term->name; ?></strong></div>
				<div class='description'><?php echo nl2br($term->description); ?></div>
			</div>
			<?php
		}
	}
}
?>