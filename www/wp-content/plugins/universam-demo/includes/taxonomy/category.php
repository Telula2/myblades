<?php
/**
 * Функции для отображения категорий
 * @since 3.7
 */
 
 
/**
 * Получить ID категории продукта, sulg или имя
 * @since 3.8
 */
function usam_get_the_category_id( $slug, $type = 'name')
{	
	$category = get_term_by( $type, $slug, 'usam-category');
	return empty( $category ) ? false : $category->term_id;
}

function usam_get_product_category_name( $product_id ) 
{
	$category = get_the_terms($product_id, 'usam-category' );	
	$category_name = !empty($category[0]) && !empty($category[0]->name)?$category[0]->name:'';
	return $category_name;	
}

/**
* Получить ссылку на категорию
*/
function usam_category_url( $category_id )
{
  return get_term_link( $category_id, 'usam-category');
}

/**
 * Возвращает тип отображения
 * @since 3.8
 */
function usam_get_the_category_display( $slug )
{
	$category_id = usam_get_the_category_id( $slug , 'slug' );
	$display_type = get_term_meta( $category_id, 'usam_display_type' , true);		
	return  $display_type;
}

function usam_is_in_category() 
{
	global $wp_query;
	$is_in_category = false;
	if(!empty($wp_query->query_vars['usam-category']))
		$is_in_category = true;
	elseif( !empty($_GET['usam-category']) )
		$is_in_category = true;	
	return $is_in_category;
}

/**
 * Поиск ID категории по её альтернативному названию (slug)
 */
function usam_category_id($category_slug = '')
{
	if(empty($category_slug))
		$category_slug = get_query_var( 'usam-category' );
	elseif(array_key_exists('usam-category', $_GET))
		$category_slug = sanitize_title($_GET['usam-category']);

	if(!empty($category_slug))
	{
		$category = get_term_by('slug', $category_slug, 'usam-category');
		if(!empty($category->term_id))
			return $category->term_id;
		else
			return false;		
	} 
	else 
		return false;	
}
/**
* Получает изображение категории или возвращает ложь
*/
function usam_category_image( $category_id ) 
{
	$category_image = get_option( 'usam_category_image' );
	$size = array( $category_image['width'], $category_image['height'] );
	return usam_taxonomy_image( $category_id, $size );	
}

function usam_category_thumbnail( $category_id ) 
{
	$category_image = get_option( 'usam_category_image' );
	$size = array( $category_image['width'], $category_image['height'] );
	$term = get_term( $category_id, 'usam-category' );
	
	return usam_taxonomy_thumbnail( $category_id, $size, $term->name );
}

/**
* Вывести описание категории
*/
function usam_category_description($category_id = null)
{
  if($category_id < 1)
	$category_id = usam_category_id();
  $category = get_term_by('id', $category_id, 'usam-category');
  return  $category->description;
}

function usam_category_name($category_id = null) 
{
	if($category_id < 1)
		$category_id = usam_category_id();
	$category = get_term_by('id', $category_id, 'usam-category');
	return $category->name;
}

function usam_product_category_description( ) 
{
	global $wp_query;
	if ( isset($wp_query->query_vars['usam-category']) && get_option( 'usam_category_description', 0 ) )
	{
		$term = get_queried_object();		
		if ( !empty($term->description) && $term->taxonomy == 'usam-category' )
		{
			?>
			<div id='product_category_description' class='product_category_description'>
				<div class='title'><strong><?php echo $term->name; ?></strong></div>
				<div class='description'><?php echo wpautop($term->description); ?></div>
			</div>
			<?php
		}
	}
}

function usam_product_category_name(  ) 
{
	global $wp_query;
	if ( isset($wp_query->query_vars['usam-category']) && get_option( 'usam_display_category_name', 0 ) )
	{
		$term = get_queried_object();		
		if ( !empty($term->name) && $term->taxonomy == 'usam-category' )
		{			
			?><h1 class ="category_name"><?php echo $term->name; ?></h1><?php
		}
	}
}


/**
 * Ссылка на изменения категории товаров
 */
function usam_edit_the_product_cat_link( $term_id, $before = '<span class="edit-link">', $after = '</span>' )
{	
	if ( current_user_can( 'manage_categories' ) )
	{		
		$text = __( 'редактировать', 'usam' );	
		$url = admin_url("edit-tags.php?action=edit&taxonomy=usam-category&tag_ID=".$term_id."&post_type=usam-product");
		$link = '<a class="product_cat-edit-link" href="' . $url . '">' . $text . '</a>';		
		echo $before . apply_filters( 'edit_product_link', $link, $term_id, $text ) . $after;
	}
}
?>