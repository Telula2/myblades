<?php
class Walker_Category_Select extends Walker 
{	
	public $db_fields = array ('parent' => 'parent', 'id' => 'term_id'); 
	private static $recursion = 0;
	private $prefix = '';
	
	public function start_lvl( &$output, $depth = 0, $args = array() ) 
	{
		self::$recursion++;
		$this->prefix = str_repeat( '&nbsp;&nbsp;&nbsp;' , self::$recursion );
		$indent = str_repeat("\t", $depth);	
	}
	
	public function end_lvl( &$output, $depth = 0, $args = array() ) 
	{		
		self::$recursion--;
		$indent = str_repeat("\t", $depth);	
	}
	
	public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 )
	{
		$taxonomy = $args['taxonomy'];
	
		$args['popular_cats'] = empty( $args['popular_cats'] ) ? array() : $args['popular_cats'];	
		$class = in_array( $category->term_id, $args['popular_cats'] ) ? ' class="popular-category"' : '';

		$args['selected_cats'] = empty( $args['selected_cats'] ) ? array() : $args['selected_cats'];
		if ( ! empty( $args['list_only'] ) ) 
		{		
			$inner_class = 'category';				
			$output .= "<option class='$inner_class' data-term-id='$category->term_id' value='$category->slug'".
			disabled( empty( $args['disabled'] ), false, false ).
			selected( in_array( $category->slug, $args['selected_cats'] ), true, false ).">".
			$this->prefix.esc_html( apply_filters('the_category', $category->name) ).'</option>';
		}	
		else 
		{		
			$output .= "<option id='{$taxonomy}-{$category->term_id}'$class value='$category->term_id'".				
				selected( in_array( $category->term_id, $args['selected_cats'] ), true, false ) .
				disabled( empty( $args['disabled'] ), false, false ) . ' /> ' .$this->prefix.
				esc_html( apply_filters( 'the_category', $category->name ) ) . '</option>';
		}
	}
	
	public function end_el( &$output, $category, $depth = 0, $args = array() ) {
		$output .= "";
	}
}