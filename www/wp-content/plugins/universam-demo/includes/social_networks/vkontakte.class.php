<?php
class USAM_VKontakte
{		
	public function __construct( )
	{	
		if ( !LOCALHOST )
		{
			add_action( 'usam_hourly_cron_task', array($this, 'products_publish') );      // Добавить товары в контакт	
			
			add_action( 'usam_hourly_cron_task', array($this, 'friends_add') ); 
			add_action( 'usam_vk_publish', array($this, 'publish_birthday'), 11 ); 
			add_action( 'usam_vk_publish', array($this, 'publish_product_day'), 10 );			
			
			add_action( 'usam_hourly_cron_task', array($this, 'publish_contest') ); 			
			add_action( 'usam_vk_publish', array($this, 'update_products'), 9 ); 
			add_action( 'usam_update_customer_review_status', array( $this, 'event_update_customer_review_status'), 10, 4 );	
		}
	}		
	
	function comments()
	{ 
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();		
		$profiles =  $vkontakte->user_and_group('group');	
		
		if ( !empty($profiles) )
		{						
			foreach ( $profiles as $key => $profile ) 
			{ 					
				$vkontakte->get_comments( $profile );					
			}		
		}				
	}
	
	public function event_update_customer_review_status( $reviews_id, $current_status, $previous_status, $t )
	{
		if ( $current_status == 1 )
		{
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();	
			$profiles =  $vkontakte->user_and_group('group');		
			if ( !empty($profiles) )
			{						
				$post_id = $t->get('page_id');				
				$post = get_post( $post_id );
				foreach ( $profiles as $key => $profile ) 
				{ 			
					if ( !empty($profile['publish_reviews']) && $post->post_type == 'usam-product')
					{
						$vkontakte->publish_product_review( $reviews_id, $profile );
					}						
				}
				$vkontakte->set_log_file();
			}
		}
	}	
	
	function friends_add()
	{
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();	
		$profiles =  $vkontakte->user_and_group('profiles');
		if ( !empty($profiles) )
		{						
			$search = array( 'q' => '', 'sort' => '1', 'online' => 1, 'country' => 1 , 'count' => 1, 'age_from' => 25, 'age_to' => 45, 'sex' => 2, 'has_photo' => 1 );	
			$ok = false;
			$offset = 0;
			foreach ( $profiles as $key => $profile ) 
			{ 	
				if ( !empty($profile['add_friends']) )
				{
					$params = array_merge ($profile, $search);
					$vkontakte->users_search( $params );					
					$profiles[$key]['offset'] = $offset;	
					$ok = true;					
				}
			}		
			if ( $ok )
				update_option( 'usam_vk_profile', serialize($profiles) );
		}			
	}
			
	// Обновляет товары в контакт
	function update_products()
	{							
		$args = array('fields' => 'ids', 'post_status' => 'publish', 'update_post_meta_cache' => false, 'update_post_term_cache' => false, 'cache_results' => false, 'meta_query' => array( array('key' => '_usam_vk_market_id', 'type' => 'numeric', 'value' => '0', 'compare' => '!=', 'posts_per_page' => -1)) );	
		$products = usam_get_products( $args );
		usam_create_system_process( __("Обновить товары в контакте", "usam" ), 1, 'vk_update_all_products', count($products), 'vk_update_all_products' );
	}		
		
	// Добавить товары в контакт
	function products_publish()
	{				
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();	
		
		$profiles =  $vkontakte->user_and_group( 'profiles' );				
		$groups =  $vkontakte->user_and_group( 'group' );		

		$options = get_option('usam_vk_autopost', array() );			
		
		$option = get_option('usam_vk_publishing_rules', '' );
		$rules = maybe_unserialize( $option );				
		$timestamp = current_time('timestamp');
		foreach ( $rules as $rule ) 
		{							
			$hour = date( 'G', $timestamp );			
			if ( usam_validate_rule($rule) && (empty($rule['date_publish']) || $timestamp > ($rule['date_publish'] + ($rule['periodicity']*3600))) && ( empty($rule['from_hour']) || $rule['from_hour'] >= $hour ) && ( empty($rule['to_hour']) || $rule['to_hour'] <= $hour ) )
			{						
				$count = (count($rule['profiles']) +  count($rule['groups'])) * $rule['quantity'];				
				$args = array( 'orderby' => 'rand', 'post_status' => 'publish', 'posts_per_page' => $count, 'meta_query' => array(), 'update_post_term_cache' => true );		
				$args['meta_query'][] = array( 'key' => '_usam_stock', 'value' => $rule['minstock'], 'compare' => '>' );
				$args['meta_query'][] = array( 'key' => '_usam_price_'.$options['type_price'],	'value' => $rule['pricemin'], 'compare' => '>=', 'type' => 'DECIMAL' );
				
				if ( !empty($rule['exclude']) )
				{
					$args['meta_query'][] = array( 
						'relation' => 'OR',
						array( 'key' => '_usam_vk_publish_date', 'compare' => 'NOT EXISTS' ),
						array( 'key' => '_usam_vk_publish_date', 'value' =>  date( "Y-m-d H:i:s", strtotime('-'.$rule['exclude'].' days') ), 'compare' => '>=', 'type' => 'DATETIME' )
					);											
				}
				else
					$args['meta_query'][] = array( 'key' => '_usam_vk_publish_date', 'compare' => 'NOT EXISTS' );	
				
				if ( !empty($rule['pricemax']) )
					$args['meta_query'][] = array( 'key' => '_usam_price_'.$options['type_price'],	'value' => $rule['pricemax'], 'compare' => '<=', 'type' => 'DECIMAL' );			
									
				if ( !empty($rule['terms']) )
				{
					$args['tax_query'] = array();
					foreach( $rule['terms'] as $taxonomy => $terms )		
					{
						if ( !empty($terms) )
							$args['tax_query'][] = array( 'taxonomy' => 'usam-'.$taxonomy, 'field' => 'id', 'terms' => $terms );
					}
				}					
				$products = usam_get_products( $args ); 
				usam_log_file( 'count'.count($products), 'vk' );
				if ( !empty($products) )					
				{								
					foreach ( $profiles as $profile ) 
					{  
						if ( !empty($rule['profiles']) && in_array($profile['id'], $rule['profiles']) )
						{
							$i = 0;
							foreach( $products as $key => $product )		
							{			
								$result = $vkontakte->publish_post($product, $profile);	
								unset($products[$key]);
								$i++;
								if ( $rule['quantity'] <= $i )								
									break;
							}
						}
					} 				
					foreach ( $groups as $group ) 
					{ 
						if ( !empty($rule['groups']) && in_array($group['id'], $rule['groups']) )
						{ 	
							$i = 0;
							foreach( $products as $key => $product )		
							{			
								$result = $vkontakte->publish_post($product, $group);	
								unset($products[$key]);
								$i++;
								if ( $rule['quantity'] <= $i )								
									break;
							}
						}
					} 							
				}
				$rule['date_publish'] = current_time('timestamp');
				usam_edit_data( $rule, $rule['id'], 'usam_vk_publishing_rules' );	
			}	
		}			
	}		
	
	/*
	Поздравляет пользователь группы
	*/
	function publish_birthday()
	{				
		$publish_h = 10; // Час публикации 		
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();	
		$vkontakte->publish_birthday( $publish_h );
	}	
	
	function publish_product_day()
	{		
		if ( get_option('usam_vk_publish_product_day', 0) )
		{
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();	
			
			$options = get_option('usam_vk_autopost', array() );
			$products_day = usam_get_products_day( array('status' => 1, 'code_price' => $options['type_price']) );
			foreach( $products_day as $product )		
			{
				$vkontakte->product_day( $product->product_id );							
			}
		}		
	}	
			
	// Обработка начала и окончания конкурсов
	function publish_contest(  ) 
	{
		$current_time = current_time('timestamp');	
		$option = get_option('usam_vk_contest', '');
		$contests = maybe_unserialize( $option );	
		if ( !empty($contests) )
		{		
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();	
				
			$t_date = time();
			$flag = false;
			foreach ( $contests as $key => $contest )
			{							
				if ( $contest['active'] == 1 )
				{
					if ( $contest['process'] == 0  && strtotime($contest['start_date']) <= $current_time )
					{							
						$contest[$key]['process'] = 1;		
						$vkontakte->publish_contest( $contest );					
					}
					if ( $contest['process'] == 1 && strtotime($contest['end_date']) >= $current_time )
					{					
						$args = array( 'profile' => $contest['profile'], 'in_group' => $contest['in_group'], 'winner_count' => $contest['winner_count'] );
						$contest[$key]['process'] = 2;
						$vkontakte->find_winner_the_contest( $args );
					}				
				}
			}
			if ( $flag )
				update_option('usam_vk_contest', serialize($contests) );
		}		
	}
}
new USAM_VKontakte(  );


/*
Запрос в API вКонтакт
*/
function usam_vkontakte_send_request( $params, $function )
{		
	require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
	$vkontakte = new USAM_VKontakte_API();			
	$result = $vkontakte->send_request( $params, $function );	
	
	$errors = $vkontakte->get_errors();
	if ( !empty($errors) )
	{		
		foreach ( $errors as $error )
			usam_set_user_screen_error( $error );
			
		$vkontakte->set_log_file();
	}	
	return $result;
}		
?>