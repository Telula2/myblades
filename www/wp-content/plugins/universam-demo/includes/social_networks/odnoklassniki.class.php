<?php
/*
 * Класс для работы с api Одноклассники
 */

class Social_APIClient_Odnoklassniki 
{	
	/*
	 * данные для подключения
	 */
	protected $connectionData = array();	
	/*
	 *  Ссылки для аутентификации
	 */
	protected $authUrl = 'http://www.odnoklassniki.ru/oauth/authorize';	
	/*
	 * ссылка для получения токена
	 */
	protected $tokenUrl = 'http://api.odnoklassniki.ru/oauth/token.do?';	
	/*
	 * Ссылка для отправки запросов 
	 */
	protected $apiUrl = 'http://api.odnoklassniki.ru/fb.do?';	
	/*
	 * Редирект урл
	 */
	protected $redirectUrl = '';	
	/*
	 * Токены для доступа
	 */
	protected $token = array();	
	/*
	 * Конструктор
	 */
	public function __construct($connectionData = array())
	{
		$this->connectionData = $connectionData;
	}
	
	/*
	 * Установить редирект урл 
	 */
	public function setRedirectUrl($url = '') 
	{
		$this->redirectUrl = $url;
			echo $this->redirectUrl;
	}
	
	/*
	 * Получить ссылку для подключения
	 */
	public function getLoginUrl($scope = array()) 
	{
		return $this->authUrl . '?'
				. http_build_query(
					array(
						'client_id'     => $this->connectionData['client_id'],
						'response_type' => 'code',
						'redirect_uri'  => $this->redirectUrl,
						'scope' => implode(';', $scope)
					)
				);
	}
	
	/*
	 * Выбросить ошибку
	 */
	public function error($array)
	{
	//	throw new Exception($array['error'] . ':' . (isset($array['error_description']) ? $array['error_description'] : ''));
	}
	
	/*
	 * Выставить токен
	 */
	public function setToken($token) 
	{
		if(is_string($token)) {
			$token = json_decode($token, true);
		}
		$this->token = $token;
	} 
	
	/*
	 * Получить строку токена
	 */
	public function getTokenStr()
	{
		return json_encode($this->token);
	}
	
	/*
	 * Получить токен
	 */
	public function getToken($code = '')
	{
		if($code) 
		{
			$this->token = $this->sendRequest( $this->tokenUrl, array(	'code' => $code,
																		'redirect_uri' => $this->redirectUrl,
																		'grant_type' => 'authorization_code',
																		'client_id' => $this->connectionData['client_id'],
																		'client_secret' => $this->connectionData['client_secret']
																	)
			);
			if(isset($this->token['error']))
				$this->error($this->token);
			else
				$this->token['expires'] = time() + 30 * 60; // Маркер доступа имеет ограниченное время существования - 30 минут
		}			
	}	
	
	/*
	 * Сохранить токен
	 */
	public function saveToken()
	{
		update_option('usam_odnoklassniki_token', $this->token);
	}	
	/*
	 * Обновить токен
	 */
	public function refreshToken() 
	{
		$this->token = $this->sendRequest(
			$this->tokenUrl,
			array(
				'refresh_token' => $this->token['refresh_token'],
				'grant_type' => 'refresh_token',
				'client_id' => $this->connectionData['client_id'],
				'client_secret' => $this->connectionData['client_secret']
			)
		);
		if(isset($this->token['error']))
			$this->error($this->token);		
	}
	
	/*
	 * Получить аксес токен
	 */
	public function getAccessToken()
	{
		if(isset($this->token['access_token']))
		{
			if(isset($this->token['expires']) && $this->token['expires'] < time()) 		
				$this->refreshToken();			
			return $this->token['access_token'];
		}
		return false;
	}
	
	/*
	 * Обратиться к апи 
	 */
	public function api($action = '', $parameters = array(), $method='POST' )
	{
		$accessToken = $this->getAccessToken();
		$paramsArray = array_merge(
			array(
				'application_key' => $this->connectionData['application_key'],
				'method' => $action
			),
			$parameters
		);
		ksort($paramsArray);
		$paramsStr = "";
		foreach($paramsArray as $k=>$v) {
			$paramsStr .= $k . "=" . $v;
		}
		$sig = strtolower(	md5( $paramsStr	. md5(	$accessToken . $this->connectionData['client_secret'] )	) );
		//$paramsArray = array_map('urlencode', $paramsArray);
		$paramsArray['access_token'] = $accessToken;
		$paramsArray['sig'] = $sig;
		print_r($paramsArray);		
		return $this->sendRequest(	$this->apiUrl, $paramsArray, $method	);
	}
	/*
	 * Отправить реквест
	 */
	protected function sendRequest($url = '', $params = array(), $method = 'POST') 
	{
		if(is_array($params)) {
			$params = http_build_query($params);
		}
		$ch = curl_init();
		if($method == 'GET') {
			$url .= $params;
		} else if($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result, true);
	}
}

class Odnoklassniki_working
{
	public function __construct()
	{
		$odnoklassniki_api = get_option('usam_odnoklassniki_api', array());
		$this->class_api = new Social_APIClient_Odnoklassniki( $odnoklassniki_api);
		$token = get_option('usam_odnoklassniki_token', array());
		print_r($token); echo 'ggggggg';
		$this->class_api->setToken($token);
		$this->publish_product();
	}
	
	
	// Добавить товары в однокласники
	function publish_product()
	{			
		$vk_publish  = get_option( 'usam_ok_publish' );
		$vk_publish = '1';
		if ( '1' == $vk_publish )
		{	
			$odnoklassniki_profile = get_option('usam_odnoklassniki_profile', array());	
			$count = count($odnoklassniki_profile);
			$args = array( 'post_type' => 'usam-product', 'orderby' => 'rand', 'post_status' => 'publish', 'posts_per_page' => $count, 'meta_query' => array( array( 'key' => '_usam_stock',	'value' => '0',	'compare' => '!=' ) ) );  
			$attachments = get_posts( $args ); 		
			if ($attachments)
			{			
				foreach ( $odnoklassniki_profile as $profile ) 
				{  					
					
		
					$post = current($attachments);	
				//	$params['uid'] = $profile['page_id'];
					$params['message'] = 'message';		
//print_r($params);					
					$data = $this->class_api->api('stream.publish', $params);
				echo "<br>Сервер вернул:<br>";
				print_r($data);
				exit;
					$post = next( $attachments );	
				} 			
			}
		}
	}		
}
//$odnoklassniki = new Odnoklassniki_working();

//============================================================================================================
/*
$odnoklassniki_api = get_option('usam_odnoklassniki_api', array());
$ok = new Social_APIClient_Odnoklassniki( $odnoklassniki_api);
/*
// закомментировать для получения нового
//$token = '{"token_type":"session","refresh_token":"b...3","access_token":"7...9","expires":1374053503}';
$token = get_option('usam_odnoklassniki_token', array());
if( $token ) {
	$ok->setToken($token);
	//$ok->refreshToken();
} 
else 
{
	$ok->setRedirectUrl('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
	if(isset($_GET['code']))
	{
		$ok->getToken($_GET['code']);
		$ok->saveToken();
		print $ok->getTokenStr();
	} else {
		print '<a href="' . $ok->getLoginUrl(array('VALUABLE ACCESS', 'SET STATUS')) . '">Login!</a>';
	//	exit();
	}
}

// получение UID текущего пользователя
// получение групп пользователя
// получение количества членов группы
$data = $ok->api('users.getCurrentUser');
$uid = $data['uid'];
$data = $ok->api('group.getUserGroupsV2');
$groupId = $data['groups'][0]['groupId'];
$data = $ok->api('group.getMembers', array('uid' => $groupId));

// обновление статуса
$data = $ok->api(
	'users.setStatus', 
	array(
		'status' => 'Тестовый статус (APPLICATION)', 
	)
);
// получение данных пользователя
$data = $ok->api(
	'users.getInfo', 
	array(
		'uids' => '5xxx0',
		'fields' => 'uid,first_name,last_name,name'
	)
);
print_r($data);
/**/
?>