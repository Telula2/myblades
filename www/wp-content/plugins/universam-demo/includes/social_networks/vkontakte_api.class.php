<?php
class USAM_VKontakte_API
{	
	public  $message = array();
	private $errors  = array();
	private $error;
	private $API_URL;
	private $version = '4.5';
	private $options;
	private $service_token = null;
	
	public function __construct( )
	{
		$this->options = get_option('usam_vk_autopost', array() ); 	
		$this->API_URL = '';
		
		$api = get_option('usam_vk_api');
		
		$this->service_token = !empty($api['service_token'])?$api['service_token']:$this->service_token;
	}
	
	public function get_errors( )
	{
		return $this->errors;
	}
	
	private function set_error( $error )
	{	
		$this->error = $error;
		if ( is_string($error) )
			$this->errors[]  =  sprintf( __('Ошибки вКонтакте. Ошибка: %s'), $error);
		else
			$this->errors[]  =  sprintf( __('Ошибки вКонтакте. Приложение %s вызвало ошибку №%s. Текст ошибки: %s'), $error['request_params'][1]['value'], $error['error_code'], $error['error_msg']);
	}
	
	public function set_log_file()
	{
		usam_log_file( $this->errors );
		$this->errors = array();
	}		
			
	function make_excerpt( $post ) 
	{ 		
		if ( !empty($post->post_excerpt) ) 
			$text = $post->post_excerpt;
		else 
		{
			$text = $post->post_content;			
		}
		$text = strip_shortcodes( $text );
		// filter the excerpt or content, but without texturizing
		if ( empty($post->post_excerpt) )
		{
			remove_filter( 'the_content', 'wptexturize' );
			$text = apply_filters('the_content', $text);
			add_filter( 'the_content', 'wptexturize' );
		} 
		else 
		{
			remove_filter( 'the_excerpt', 'wptexturize' );
			$text = apply_filters('the_excerpt', $text);
			add_filter( 'the_excerpt', 'wptexturize' );
		}	
		$pos  = strripos($text, '<table');		
		if ($pos === false )
		{ // Не найдено
			$text = str_replace(']]>', ']]&gt;', $text);	
			$text = wp_strip_all_tags($text);			
			$text = str_replace(array("\r\n","\r","\n"),"\n",$text);		
		}
		else
		{
			$html_no_attr = preg_replace("#(</?\w+)(?:\s(?:[^<>/]|/[^<>])*)?(/?>)#ui", '$1$2', $text); // очистить от классов и стилей
			$html_no_attr =	preg_replace('~(<(.*)[^<>]*>\s*<\\2>)+~i','',$html_no_attr );						// удалить пустые строки таблицы
			preg_match_all('#<td>(.+?)</td>#s', $html_no_attr, $matches); 						
			$result = array_chunk($matches[1], 2);
			$text = '';			
			foreach ( $result as $record )
			{					
				$name = wp_strip_all_tags($record[0]);
				$content = wp_strip_all_tags($record[1]);
				$text .= $name.': '.$content.chr(10);
			}
		}		
		$excerpt_more = apply_filters('excerpt_more', '...');
		$excerpt_more = html_entity_decode($excerpt_more, ENT_QUOTES, 'UTF-8');
		$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
		$text = htmlspecialchars_decode($text);
		
		$max = !empty($this->options['excerpt_length']) ? $this->options['excerpt_length'] : 20;
		if ($max < 1) 
			return ''; // nothing to send
		$words = explode(' ', $text);

		if (count($words) >= $max)
		{
			$words = array_slice($words, 0, $max);
			array_push ($words, $excerpt_more);
			$text = implode(' ', $words);
		}
		$text = $this->excerpt_strlen($text);	
		return $text;
	}
	
	function excerpt_strlen ($text, $max_strlen = 2688)
	{		
		if (isset($this->options['excerpt_length_strings']) && !empty($this->options['excerpt_length_strings'])) 		
			$max_strlen = $this->options['excerpt_length_strings'] > $max_strlen ? $max_strlen : $this->options['excerpt_length_strings'];		

		if (strlen($text) >= $max_strlen) 
		{
			$text = substr($text, 0, $max_strlen);
			$words = explode(' ', $text);
			array_pop($words); // strip last word

			$excerpt_more = apply_filters('excerpt_more', '...');
			$excerpt_more = html_entity_decode($excerpt_more, ENT_QUOTES, 'UTF-8');
			array_push ($words, $excerpt_more);
			$text = implode(' ', $words);
		}
		return $text;
	}
	
	function user_and_group ( $type = 'all' )
	{			
		$results = array();	
		if ( $type == 'group' || $type == 'all' )
		{
			$option = get_option('usam_vk_groups', '' );
			$groups = maybe_unserialize( $option );	
			if ( !empty($groups) )
			{
				foreach ( $groups as $group ) 
				{
					$group['type_page'] = 'group';
					$results[] = $group;
				}	
			}
		}
		if ( $type == 'profiles' || $type == 'all' )
		{
			$option = get_option('usam_vk_profile', '' );
			$profiles = maybe_unserialize( $option );					
			if ( !empty($profiles) )
			{
				foreach ( $profiles as $profile ) 
				{
					$profile['type_page'] = 'user';
					$results[] = $profile;
				}
			}
		}		
		return $results;
	}		
	
	// Добавить товары в контакт
	public function product_day( $product_id )
	{								
		$profiles = $this->user_and_group();
		$i = 0;		
		foreach ( $profiles as $profile ) 
		{ 
			$args = array( 'pin' => $this->options['fix_product_day'], 'message_format' => $this->options['product_day_message'] );	
			if ( $this->publish_post( $product_id, $profile, $args) )
				$i++;
		}		
		return $i;		
	}
	
	function get_access_token( $code, $redirect_uri )
	{		
		$api = get_option('usam_vk_api');		
		$params = array( 'client_id' => $api['api_id'], 'client_secret' => $api['key'], 'redirect_uri' => $redirect_uri, 'code' => $code );
		
		$query = http_build_query($params);  
		$data = wp_remote_post('https://oauth.vk.com/access_token?'.$query, array('sslverify' => false));

		if (is_wp_error($data))
			return $data->get_error_message();

		$resp = json_decode($data['body'],true);				
		if ( isset( $resp['error'] ) ) 
		{			
			$this->set_error( $resp['error'] );	
			return false;
		}		
		return $resp;		
	}	
	
	function get_access_token2( $code, $redirect_uri )
	{		
		$api = get_option('usam_vk_api');		
		$params = array( 'client_id' => $api['api_id'], 'client_secret' => $api['key'], 'grant_type' => 'client_credentials' );
		
		$query = http_build_query($params);  
		$data = wp_remote_post('https://oauth.vk.com/access_token?'.$query, array('sslverify' => false));

		if (is_wp_error($data))
			return $data->get_error_message();

		$resp = json_decode($data['body'],true);				
		if ( isset( $resp['error'] ) ) 
		{			
			$this->set_error( $resp['error'] );	
			return false;
		}		
		return $resp;		
	}	
	
	function get_functions_service_token( )
	{	
		return array( 
			'newsfeed.search',
			'wall.search',
			'users.get',
			'wall.getComments',
			'friends.get',
			'groups.getMembers',
			'likes.getList',		
		);	
	}
	
	function send_request( $params, $function )
	{		
		
			return false;	
	}
	
		/*
	Возвращает список пользователей, которые были приглашены в группу.
	
	group_id - идентификатор группы, список приглашенных в которую пользователей нужно вернуть. 
	offset   - смещение, необходимое для выборки определенного подмножества пользователей. 
	count    - количество пользователей, информацию о которых нужно вернуть. по умолчанию 20
	
	fields   - список дополнительных полей, которые необходимо вернуть. Cписок слов, разделенных через запятую. Доступные значения: 
	sex, bdate, city, country, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, online_mobile, lists, domain, has_mobile, contacts, connections, site, education, universities, schools, can_post, can_see_all_posts, can_see_audio, can_write_private_message, status, last_seen, common_count, relation, relatives, counters
	
			bdate дата рождения пользователя в формате DD.MM.YYYY, например "15.11.1984". 
	
	name_case - падеж для склонения имени и фамилии пользователя. Возможные значения: именительный – nom, родительный – gen, дательный – dat, винительный – acc, творительный – ins, предложный – abl. По умолчанию nom. 
	*/
	function publish_birthday( $publish_h = null )
	{		
		$profiles = $this->user_and_group( 'group' ); 	
		$count_users = 0;	
			
		$current_day = (int)date('d');
		$current_month = (int)date('m');
		
		foreach ( $profiles as $profile ) 
		{  
			if ( !empty($profile['birthday']) )
			{
				$params = array( 'group_id' => $profile['page_id'], 'offset' => 0, 'fields' => 'sex, city, country, has_mobile, online, bdate, photo_50, photo_100' );			
				$users = array();			
				$offset = 1000;
				$i = 0;
				do
				{					
					$i++;
					$result = $this->send_request( $params, 'groups.getMembers' );		
					$params['offset'] += $offset;
					if ( !$result )
						break;
					$users = array_merge($users, $result['users'] );					
				}
				while ( count($result['users']) == $offset );
				if ( empty($users) )
					continue;
				
				$selected_users = array(); 
				foreach ( $users as $user ) 
				{
					if ( isset($user['bdate']) )
					{						
						$day = explode(".",$user['bdate']);					
						if ( isset($day[1]) && (int)$day[0] == $current_day && $current_month == (int)$day[1] )
						{
							$selected_users[] = $user;
						}
					}					
				}	
				preg_match('/\{(.+?)\}/s', $this->options['birthday'], $str ); 
				if ( empty($str[1]) )
					continue;	
				
				if ( empty($selected_users) )
					continue;
								
				$user_message = '';		
				foreach ( $selected_users as $user ) 
				{						
					$args = array();
					foreach ( $user as $key => $value ) 
					{							
						$text = $value;											
						if ( $key == 'city')
						{
							$params = array( 'city_ids' => $value, 'access_token' => $profile['access_token'] );							
							$result = $this->send_request( $params, 'database.getCitiesById' );
							if ( !empty($result[0]['name']) )
								$text = $result[0]['name'];		
							else
								$text = 'не указано';								
						}					
						$args[$key] = $text;
					}				
					if ( is_numeric($user['uid']) )
						$id = 'id'.$user['uid'];
					else
						$id = $user['uid'];
							
					$_text = str_replace( '%user_link%', '['.$id.'|'.$user['first_name'].' '.$user['last_name'].']', $str[1] );
					$user_message .= $this->get_message_publication( $_text, $args ).chr(10);
				}
				$message = str_replace($str[0], $user_message, $this->options['birthday'] );	
				$params = array( 'message' => $this->message_form( $message ) );
				
				if ( $publish_h !== null && $publish_h < date('H') )					
					$params['publish_date'] = mktime($publish_h, 0, 0,  date('m'), $current_day, date('y'));			
			
				$result = $this->wall_post( $profile, $params );
				$count_users += count($selected_users);			
			}
		} 	
		return $count_users;
	}

	// Публикация записи на стене
	function wall_post( $profile, $args ) 
	{
		if ( empty($args['message']) )
			return false;
		
		$owner_id = $profile['type_page'] == 'group'?'-'.$profile['page_id']:$profile['page_id'];
		
		$params = array(
			'access_token' => $profile['access_token'],  		
			'from_group' => $this->options['from_group'], //1 — будет опубликована от имени группы, 0 — будет опубликована от имени пользователя
			'signed' => $this->options['from_signed'], // у записи, размещенной от имени сообщества, будет добавлена подпись (имя пользователя, разместившего запись)
			'message' => $args['message'],   
			'owner_id' => $owner_id,
		); 		
		if (!empty($args['attachments']))
			$params['attachments'] = implode(',', $args['attachments']);	

		if ( isset($args['guid']) )
			$params['guid'] = $args['guid']; // уникальный идентификатор, предназначенный для предотвращения повторной отправки одинаковой записи. 
		
		if ( isset($args['place_id']) )
			$params['place_id'] = $args['place_id']; // идентификатор места, в котором отмечен пользователь
		
		if ( isset($args['lat']) )
			$params['lat'] = $args['lat']; //географическая широта отметки, заданная в градусах
		
		if ( isset($args['long']) ) 
			$params['long'] = $args['long']; //географическая долгота отметки, заданная в градусах
		
		if ( isset($args['mark_as_ads']) )
			$params['mark_as_ads'] = $args['mark_as_ads']; //1 — у записи, размещенной от имени сообщества, будет добавлена метка "это реклама"		
		
		if ( isset($args['publish_date']) )					
			$params['publish_date'] = $args['publish_date'];
		
		$result = $this->send_request( $params, 'wall.post' );		
		if ( isset($result['post_id']) && !empty($args['pin']))
		{		
			$this->wall_pin( $profile, $result['post_id']);
		}		
		return $result;
	}	
	
	// Закрепляет запись на стене (запись будет отображаться выше остальных).
	function wall_pin( $profile, $post_id ) 
	{
		$owner_id = $profile['type_page'] == 'group'?'-'.$profile['page_id']:$profile['page_id'];
		$params = array(
			'access_token' => $profile['access_token'],  		
			'post_id' => $post_id, 			
			'owner_id' => $owner_id,			
		); 
		return $this->send_request( $params, 'wall.pin' );	
	}
	
	// Публикует новую запись на стене.
	function publish_post( $post, $profile, $args = array() ) 
	{		
		if ( empty($post) )
			return false;
		
		if ( is_numeric($post) )
			$post = get_post( $post );
			
		$options = array();
		if ( !empty($args['message_format']) )	
			$options['message_format'] = $args['message_format'];	
		$message = $this->display_message( $post, $options );
		
		$permalink = $this->options['add_link'] ? $post->guid : ''; 

		$attach = array();		
		$images = $this->get_post_photo( $post, $this->options['upload_photo_count'] ); 
		$photo = $this->upload_photo($images, $profile, $message );
		
		if ( is_array($photo) )
			$attach[] = implode(',',$photo);		
		if (!empty($permalink))
			$attach[] = $permalink;	
		$params = $args;
		$params['attachments'] = $attach;
		$params['message'] = $message; 	
		$result = $this->wall_post( $profile, $params );	

		if ( isset($result['post_id']) )
		{	
			if ( isset($args['publish_date']) )
				$publish_date = date("Y-m-d H:i:s", $args['publish_date'] );
			else
				$publish_date = date("Y-m-d H:i:s");
			
			usam_update_product_meta( $post->ID, 'vk_publish_date', $publish_date );
			usam_update_product_meta( $post->ID, 'vk_post_id', $result['post_id'] );
		}	
		return true;
	}
	
	function delete_product( $product_id, $profile ) 
	{			
		$market_id = usam_get_product_meta( $product_id, 'vk_market_id' );			
		if ( $market_id )
		{					
			$owner_id = $profile['type_page'] == 'group'?'-'.$profile['page_id']:$profile['page_id'];			
			$params = array(
				'access_token' => $profile['access_token'],  
				'owner_id' => $owner_id,	
				'item_id' => $market_id,	
			); 		
			if ( $this->send_request( $params, 'market.delete' ) )	
			{
				usam_update_product_meta( $product_id, 'vk_market_publish_date', '' );
				usam_update_product_meta( $product_id, 'vk_market_id', 0 );
				return true;
			}
		}
		return false;
	}
	
	// Обновляет товар
	function edit_product( $post, $profile ) 
	{			
		if ( is_numeric($post) )
			$post = get_post($post);
		
		$market_id = usam_get_product_meta( $post->ID, 'vk_market_id' );
		if ( empty($market_id) )
			return false;			
		
		$thumbnail = $this->get_post_thumbnail_photo( $post ); 			
		if ( empty($thumbnail) )
			return false;	
		
		$params = array( 'access_token' => $profile['access_token'], 'main_photo' => 1, 'group_id' => $profile['page_id'] );		
		$main_photo = $this->upload_product_photos( $thumbnail, $params );	
	
		if ( empty($main_photo) )
			return false;			
		
	/*	$images = $this->get_post_photo( $post, 4 ); 		
		if ( empty($images) )
		{			
			$params = array( 'access_token' => $profile['access_token'], 'main_photo' => 0, 'group_id' => $profile['page_id'] );		
			$photo_ids = $this->upload_product_photos($images, $params );
		}		*/
		$description = $this->make_excerpt( $post );
		$description .= chr(10).$post->guid;
		
		$params = array(
			'access_token' => $profile['access_token'],  
			'owner_id' => ($profile['type_page'] == 'group')?'-'.$profile['page_id']:$profile['page_id'],
			'item_id' => $market_id,		
			'name' => $post->post_title,
			'description' => $description,
			'category_id' => 602,
			'price' => usam_get_product_price( $post->ID, $this->options['type_price'] ),	
			'main_photo_id' => $main_photo[0],					
		); 		
	/*	if ( !empty($photo_ids) )
			$params['photo_ids'] = implode(',',$photo_ids);
	*/	
		return $this->send_request( $params, 'market.edit' );
	}
	
	/*
	
owner_id - идентификатор владельца товара. 
name - название товара. Ограничение по длине считается в кодировке cp1251. cтрока, минимальная длина 4, максимальная длина 100, обязательный параметр
description - описание товара. строка, минимальная длина 10, обязательный параметр
category_id - идентификатор категории товара. положительное число, обязательный параметр
price - цена товара. дробное число, обязательный параметр, минимальное значение 0.01
deleted - статус товара (1 — товар удален, 0 — товар не удален). флаг, может принимать значения 1 или 0
main_photo_id идентификатор фотографии обложки товара. 

Фотография должна быть загружена с помощью метода photos.getMarketUploadServer, передав параметр main_photo. См. подробную информацию о загрузке фотографии товаров 
положительное число, обязательный параметр
photo_ids - идентификаторы дополнительных фотографий товара. 

Фотография должна быть загружена с помощью метода photos.getMarketUploadServer. См. подробную информацию о загрузке фотографии товаров 
список положительных чисел, разделенных запятыми, количество элементов должно составлять не более 4	
*/
	
	// Публикует новую запись на своей или чужой стене.
	function publish_product( $post, $profile, $category_id, $album_id = 0 ) 
	{					
		if ( is_numeric($post) )
			$post = get_post($post);
	 
		$market_id = usam_get_product_meta( $post->ID, 'vk_market_id' );		
		if ( $market_id )
			return $this->edit_product($post, $profile);			
		
		$thumbnail = $this->get_post_thumbnail_photo( $post ); 		
		if ( empty($thumbnail) )
			return false;	
	
		$params = array( 'access_token' => $profile['access_token'], 'main_photo' => 1, 'group_id' => $profile['page_id'] );		
		$main_photo = $this->upload_product_photos( $thumbnail, $params );	
		
		if ( empty($main_photo) )
			return false;			
		
		$images = $this->get_post_photo( $post, 4 ); 		
		if ( empty($images) )
		{			
			$params = array( 'access_token' => $profile['access_token'], 'main_photo' => 0, 'group_id' => $profile['page_id'] );		
			$photo_ids = $this->upload_product_photos($images, $params );
		}			
		$description = $this->make_excerpt( $post );
		$description .= chr(10).$post->guid;		
	
		$params = array(
			'access_token' => $profile['access_token'],  
			'owner_id' => ($profile['type_page'] == 'group')?'-'.$profile['page_id']:$profile['page_id'],			
			'name' => $post->post_title,
			'description' => $description,
			'category_id' => $category_id,
			'price' => usam_get_product_price( $post->ID, $this->options['type_price'] ),	
			'main_photo_id' => $main_photo[0],			
		); 			
		if ( !empty($photo_ids) )
		{			
			$params['photo_ids'] = implode(',',$photo_ids);	
		} 
		$resp = $this->send_request( $params, 'market.add' );
		if ( isset($resp['market_item_id']) )
		{				
			usam_update_product_meta( $post->ID, 'vk_market_publish_date', date("Y-m-d H:i:s") );
			usam_update_product_meta( $post->ID, 'vk_market_id', $resp['market_item_id'] );
			
			if ( $album_id )
				$this->product_add_to_album( $post->ID, $profile, $album_id );
			return true;
		}
		else	
			return false;
	}
	
	//Добавляет товар в одну или несколько выбранных подборок.
	function product_add_to_album( $product_id, $profile, $album_ids  )
	{
		$market_id = usam_get_product_meta( $product_id, 'vk_market_id' );				
		if ( $profile['type_page'] == 'group' )
			$owner_id = '-'.$profile['page_id'];
		else
			$owner_id = $profile['page_id'];
		
		$album_ids = is_array($album_ids)?implode(',',$album_ids):(int)$album_ids;
		
		$params = array(
			'access_token' => $profile['access_token'],  
			'owner_id' => $owner_id,			
			'item_id' => $market_id, 	
			'album_ids' => $album_ids, 						
		); 
		return $this->send_request( $params, 'market.addToAlbum' );			
	}	
	
	
	function publish_product_review( $reviews_id, $profile ) 
	{				
		$review = usam_get_review( $reviews_id );
		$market_id = usam_get_product_meta( $review['page_id'], 'vk_market_id' );	
		if ( !$market_id )
			return false;
		
		$post = get_post( $review['page_id'] );
		
		$args = $this->get_review_args( $review );
		$post_args = $this->get_post_args( $post );
		$args = array_merge ($post_args, $args);	
		
		$message = $this->get_message_publication( $this->options['product_review_message'], $args );			
		
		$images = $this->get_post_photo( $post, 1 ); 
		$photos = $this->upload_photo($images, $profile, $message);

		$attach = array();
		if ( is_array($photos) )
			$attach[] = implode(',',$photos);	

		$owner_id = ($profile['type_page'] == 'group')?'-'.$profile['page_id']:$profile['page_id'];
			
		$params = array(
			'access_token' => $profile['access_token'],  
			'owner_id'     => $owner_id,				
			'item_id'      => $market_id,  			
			'message'      => $message, 
			'from_group'   => $this->options['from_group'], 		
			'guid'         => $review['id'],
		); 									
		if (!empty($attach))
			$params['attachments'] = implode(',', $attach);		
		return $this->send_request( $params, 'market.createComment' );	
	}
	
	// Добавить товары в контакт
	public function customer_reviews( $reviews_id )
	{								
		$profiles = $this->user_and_group();
		$i = 0;
		foreach ( $profiles as $profile ) 
		{  														
			$out = $this->publish_customer_review( $reviews_id, $profile );
			if ( $out == 1 )
				$i++;
		}
		return $i;		
	}
	
	function get_review_args( $review ) 
	{
		$args = array();
			
		$args['review_title'] = $review['title'];			
		$args['review_author'] = __('Автор','usam').': '.$review['name'];
		$args['review_excerpt'] = $review['review_text'];
		$args['review_response'] = $review['review_response'];
		
		$r = '';
		for ( $l = 1; $l <= $review['rating']; ++$l )
			$r .= '&#11088;';
		$args['review_rating'] = $r;
		
		return $args;
	}
	
	// Публикует отзыв на своей или чужой стене.
	function publish_customer_review( $reviews_id, $profile ) 
	{				
		$review = usam_get_review( $reviews_id );
		$post = get_post( $review['page_id'] );			
		
		$args = $this->get_review_args( $review );
		
		if ( $post->post_type == 'usam-product')
			$header_text = __('Отзыв о товаре','usam');			
		else
			$header_text = __('Отзыв покупателя','usam');	

		$args['header'] = $header = '--------------------------------------- '.mb_strtoupper($header_text).' ---------------------------------------'.chr(10);		
		
		$message = $this->get_message_publication( $this->options['reviews_message'], $args );		
		$permalink = $this->options['add_link'] ? $post->guid : ''; 

		$attach = array();
		
		$image_path = USAM_CORE_IMAGES_PATH."/review.jpg";
		if ( version_compare( PHP_VERSION, '5.5', '>=' ) )
			$review_image[ 'file1' ] = new CURLFile( $image_path );
		else 
			$review_image[ 'file1' ] = '@' . $image_path;
	
		$review_image = $this->upload_photo( $review_image, $profile); 		
		$images = $this->get_post_photo( $post, 1 ); 
		$photos = $this->upload_photo($images, $profile, $message);

		if ( is_array($review_image) )
			$attach[] = implode(',',$review_image);
		if ( is_array($photos) )
			$attach[] = implode(',',$photos);	
		
		if (!empty($permalink))
			$attach[] = $permalink;		
			
		$params = array(
			'message' => $message,   
			'attachments' => $attach,		
		); 		
		return $this->wall_post( $profile, $params );	
	}
	
// Получить миниатюру для загрузки в контакт	
	private function get_post_thumbnail_photo( $post )
	{						
		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$path = get_attached_file( $thumbnail_id );		
	
		$i = 0;				
		if ( version_compare( PHP_VERSION, '5.5', '>=' ) )
			$result[ 'file' . $i ] = new CURLFile( $path );
		else 
			$result[ 'file' . $i ] = '@' . $path;			
		return $result;	
	}

// Выбрать фотографии для загрузки в контакт
	private function get_post_photo($post, $number )
	{					
		if ( $number > 5 )
			$number = 5;		
		$post_images = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order id', 'order' => 'ASC', 'numberposts' => $number )); 
			
		$images_path = array();		
		$thumbnail_id = get_post_thumbnail_id( $post->ID );
		$images_path[] = get_attached_file( $thumbnail_id );	
		if ( !empty($post_images) )
		{
			$i = 1;
			foreach($post_images as $image)
			{			
				if ( $i > $number )
					break;
				
				if ( $thumbnail_id != $image->ID )
					$images_path[] = get_attached_file( $image->ID );			
				$i++;
			}
		}
		$result = array();
		$i = 0;
		foreach($images_path as $path)
		{			
			if ( version_compare( PHP_VERSION, '5.5', '>=' ) )
				$result[ 'file' . $i ] = new CURLFile( $path );
			else 
				$result[ 'file' . $i ] = '@' . $path;				
			$i++;
		} 
		return $result;	
	}	
	
	private function upload_server( $images, $params, $function )
	{	
		global $wp_version;
		$result = $this->send_request( $params, $function );	
		if ( empty($result['upload_url']) )
			return false;	
// Загрузить фото		
		$curl = new Wp_Http_Curl();
		$data = $curl->request( $result['upload_url'], array( 'body' => $images, 'timeout' => 45, 'method' => 'POST', 'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),'stream' => false, 'decompress' => false, 'filename' => null, ));	
	
		if ( is_wp_error($data) )
		{
			$this->errors[] = $data->get_error_message();
			return false;	
		}
		$result = json_decode($data['body'],true);		
		return $result;
	}
	
	// Загрузить фото для товара в контакт
	function upload_product_photos( $images, $args )
	{		
//Возвращает адрес сервера для загрузки фотографии на стену пользователя или сообщества.
		$result = $this->upload_server( $images, $args, 'photos.getMarketUploadServer' );	
		if ( empty($result['photo']) )
			return false;
				
// Сохраняет фотографии после успешной загрузки на URI, полученный методом
		$params = array(
			'group_id'     => $args['group_id'],			
			'access_token' => $args['access_token'],			
			'server'       => $result['server'],
			'photo'        => $result['photo'],
			'hash'         => $result['hash'],
		); 			
		if ( !empty($result['crop_data']) )
			$params['crop_data'] = $result['crop_data'];	
		
		if ( !empty($result['crop_hash']) )
			$params['crop_hash'] = $result['crop_hash'];
		
		$result = $this->send_request( $params, 'photos.saveMarketPhoto' );	
		
		if ( !$result )
			return false; 	
		
		foreach($result as $r)				
			$attachments[] = $r['pid'];			
		return $attachments;
	}
	
	// Загрузить фото в контакт
	public function upload_photo($images, $profile, $message = '' )
	{			
		$params = array( 'access_token' => $profile['access_token'] );
// Страница пользователя или группа		
		if ( $profile['type_page'] == 'group' )
			$params['group_id'] = $profile['page_id'];
		else
			$params['uid'] = $profile['page_id'];
//Возвращает адрес сервера для загрузки фотографии на стену пользователя или сообщества.
		$result = $this->upload_server( $images, $params, 'photos.getWallUploadServer' );	
		if ( empty($result['photo']) )
			return false;	

// Сохраняет фотографии после успешной загрузки на URI, полученный методом
		$params = array(
			'access_token' => $profile['access_token'],
			'server' => $result['server'],
			'photo' => $result['photo'],
			'hash' => $result['hash'],
			'caption' => $message,
			
		); 
		if ( $profile['type_page'] == 'group' )
			$params['gid'] = $profile['page_id'];
		else
			$params['uid'] = $profile['page_id'];
	
		$result = $this->send_request( $params, 'photos.saveWallPhoto' );		
		if ( !$result )
			return false;
		
		foreach($result as $r)
		{			
			$attachments[] = $r['id'];				
		}
		return $attachments;
	}
	
//Изменяет описание у выбранной фотографии.	
	function photo_edit( $id, $message, $profile )
	{				
		$owner_id = ($profile['type_page'] == 'group')?'-'.$profile['page_id']:$profile['page_id'];
		$params = array(
			'access_token' => $profile['access_token'],
			'owner_id' => $owner_id,			
			'photo_id' => $id,
			'caption' => $message,			
		); 
		$result = $this->send_request( $params, 'photos.edit' );
		return $result;
	}
	
	private function get_post_args( $post ) 
	{  		
		$args = array( 'title' => get_the_title($post->ID),
					  'excerpt' => $this->make_excerpt($post),
					  'link' => $post->guid,	
		);
		if ( $post->post_type == 'usam-product' )
		{
			$price = usam_get_product_price( $post->ID, $this->options['type_price'] ); 
			$old_price = usam_get_product_old_price( $post->ID, $this->options['type_price'] );
			$currency = usam_get_currency_price_by_code( $this->options['type_price'] );
			$price_currency = usam_currency_display( $price, array( 'currency' => $currency ) );	
			if ($old_price > 0 )
			{
				$discont = round(100 - $price*100/$old_price, 0);				
				$price_and_discont = $price_currency." - СКИДКА: $discont%";	
				$old_price_currency	= usam_currency_display( $old_price, array( 'currency' => $currency ) );		
			}				
			else
			{
				$price_and_discont = $price_currency;
				$discont = '';
				$old_price_currency = '';
			}
			$args['price_currency'] = $price_currency;
			$args['price'] = $price;
			$args['price_and_discont'] = $price_and_discont;
			$args['old_price_currency'] = $old_price_currency;
			$args['old_price'] = $old_price;
			$args['discont'] = $discont;
		}
		return $args;
	}	
	
	//chr(10)
	private function display_message( $post, $args = array() )
	{			
		if ( isset($args['message_format']) )
			$message = $args['message_format'];		
		elseif ( $post->post_type != 'usam-product' )
			$message = $this->options['post_message'];
		else		
			$message = $this->options['product_message'];			
		
		$args = $this->get_post_args( $post );				
		return $this->get_message_publication( $message, $args );
	}
	
	private function get_message_publication( $message, $args )
	{	
		$args['name'] = get_bloginfo('name');
		$args['link_catalog'] = __('Товары','usam').': '.usam_get_url_system_page('products-list');
		
		$shortcode = new USAM_Shortcode();		
		$message = $shortcode->process_args( $args, $message );	
		
		return $this->message_form( $message );
	}
	
	// Сформировать сообщение
	private function message_form( $message )
	{	
		$message = strip_tags($message);
		$message = html_entity_decode($message, ENT_QUOTES, 'UTF-8');
		$message = htmlspecialchars_decode($message);  	
		$message = str_replace('\n', chr(10), $message );	
		return $message;
	}
	
//Загрузка фотографий в альбом пользователя	
	public function upload_photo_album( $post, $profile, $album_id )
	{		
		$message = $this->display_message( $post );
		
		$images = $this->get_post_photo( $post, $this->options['upload_photo_count'] );
		
		$params = array( 'access_token' => $profile['access_token'] , 'album_id' => (int)$album_id );			
// Страница пользователя или группа
		if ( $profile['type_page'] == 'group' )
			$params['group_id'] = $profile['page_id'];			

//Возвращает адрес сервера для загрузки фотографии на стену пользователя или сообщества.
		$result = $this->upload_server( $images, $params, 'photos.getUploadServer' );	

		if ( empty($result['photos_list']) )
			return false;	
			
// Сохраняет фотографии после успешной загрузки. 
		$params = array(
			'access_token' => $profile['access_token'],
			'aid' => $album_id,	
			'photos_list' => $result['photos_list'],
			'server' => $result['server'],
			'hash' => $result['hash'],
			'caption' => $message
		); 
		if ( $profile['type_page'] == 'group' )
			$params['gid'] = $profile['page_id'];
		else
			$params['uid'] = $profile['page_id'];
						
		$result = $this->send_request( $params, 'photos.save' );	
		
		if (!$result)
			return false; 
		return $result;
	}	
		
	//Возвращает комментарии к записям.
	function get_comments( $profile )
	{		
		if ( $profile['type_page'] == 'group' )
			$owner_id = '-'.$profile['page_id'];
		else
			$owner_id = $profile['page_id'];
		
		$params = array(
			'owner_id' => $owner_id,
			'offset' => 0,	
			'count' => 100,	
			'extended' => 0		// будут возвращены три массива wall, profiles и groups.		
		); 	
		$posts = $this->send_request( $params, 'wall.get' );				
		if ( $posts )
		{		
			foreach( $posts as $post)
			{			
				if ( !empty($post['id']) )
				{
					$params = array(
						'access_token' => $profile['access_token'],
						'owner_id' => $owner_id,
						'post_id' => $post['id'],	
					//	'need_likes' => 1,	
						'offset' => 0,
						'count' => 1,	
						'preview_length' => 100,					
					); 	
					$result = $this->send_request( $params, 'wall.getComments' );	
				}
			}			
		}	
		return $result;		
	}
	
	
	function users_search( $search )
	{			
		if (  empty($search['friends_add']) )
			return true;
		$search_default = array(  'q' => '',  'sort' => '1', 'online' => 1, 'country' => 1 , 'count' => 1, 'offset' => 0, 'age_from' => 25 );
		$params = array_merge ($search_default, $search);
//Возвращает список пользователей в соответствии с заданным критерием поиска.

		$result = $this->send_request( $params, 'users.search' );	
		if ( $result )
		{
			foreach( (array)$result as $user)
			{
				if ( isset($user['uid']) )
				{
					$params = array(
						'user_id' => $user['uid'],
						'access_token' => $search['access_token'],
						'page_id' => $search['page_id'],
						'text' => ''			
					);	
					$friends_result = $this->send_request( $params, 'friends.add' );	
					if ( $this->error['error_code'] == 14)
					{
						$this->errors[] = 'Ошибка: '.$this->error['error_code'].' - требуется капча. Анкета - '.$search['page_id'];
						$captcha = array(
							'sid' => $this->error['captcha_sid'],
							'img' => $this->error['captcha_img']
						);
						$autopost_error = get_option('usam_vk_autopost_error');
						$autopost_error['captcha'] = $captcha;
						$autopost_error['method'] = 'friends.add';
						if (!isset($autopost_error['error_email']))
						{	
							$autopost_error['error_email'] = current_time('timestamp', 1);
						}
						update_option('usam_vk_autopost_error', $captcha); 						
						return $this->error['error_code'];
					}					
					if (is_wp_error($data))
						return $data->get_error_message();
					else
						$this->message[] = "Добавлен друг в анкету с id ".$search['page_id'];
				}		
			}
			$this->message[] = 'Завершено без ошибок';
			return true;
		}
		return false;
	}	
	
	// Публикация конкурсов
	function publish_contest( $contest ) 
	{	
		$profile =  $this->get_profile( $contest['profile'] );		
		if ( !$profile )
			return false;
		
		$ve = get_option( 'gmt_offset' ) > 0 ? '-' : '+';
		$date = strtotime($ve . get_option( 'gmt_offset' ).' HOURS', strtotime($contest['start_date']));	
			
		$params = array(
			'message' => $contest['message'],   
			'publish_date' => $date,
			'attachments' => $attach,
			'pin' => $contest['pin'],
		); 					
		$result = $this->wall_post( $profile, $params );	
		return $result['post_id'];
	}
	
	/*Найти победителя в конкурсе*/		
	function find_winner_the_contest( $args ) 
	{			
		$profile =  $this->get_profile( $args['profile'] );			
		if ( !$profile )
			return false;
		
		$post_id = 22921;
		
		$offset = 0;
	
		$params = array(
			'access_token' => $profile['access_token'],  
			'owner_id' => $profile['owner_id'],			
			'post_id' => $post_id,
			'offset' => $offset,
			'count' => 1000,
		); 		
		$resp = $this->send_request( $params, 'wall.getReposts' );		
	
		if ( isset($resp['profiles']) )
		{				
			if ( !empty($args['in_group']) )
			{
				$params = array(
					'access_token' => $profile['access_token'],  
					'gid'          => $profile['page_id'],						
					'offset'       => $offset,
					'count'        => 1000,
				); 		
				$resp_users = $this->send_request( $params, 'groups.getMembers' );	//Возвращает список участников группы. 
				if ( isset($resp_users['users']) )
				{
					$users = array();
					foreach( $resp['profiles'] as $user )
					{	
						if ( in_array($user['uid'], $resp_users['users']) )
							$users[] = $user;
					}		
					$resp['profiles'] = $users;
					unset($users);								
				}
				else
					return false;
			}				
			$array = array();
			$some_man = count($resp['profiles']) - 1;
			for ( $i = 1; $i <= $args['winner_count']; ++$i )
			{				
				$index = rand(0, $some_man); 
				if ( in_array($index , $array) )
					$i--;
				else
					$array[] = $index;
			}
			$winners = array();
			foreach($array as $index) 
			{
				$winners = $resp['profiles'][$index];
			}		
			return $winners;
			
		}
		else	
			return false;
	}
	
	public function get_profile( $page_id ) 
	{
		$profiles =  $this->user_and_group();		
		$return = false;
		foreach ( $profiles as $key => $profile )
		{
			if ( $profile['page_id'] == $page_id )
			{
				$return = $profile;
				if ( $profile['type_page'] == 'group' )
					$return['owner_id'] = '-'.$profile['profile'];
				else
					$return['owner_id'] = $profile['profile'];	
				break;
			}
		}
		return $return;
	}
	
	public function get_user_profile( $page_id ) 
	{
		$profiles =  $this->user_and_group( 'profiles' );	
		foreach ( $profiles as $key => $profile )
		{
			if ( $profile['page_id'] == $page_id )
			{
				return $profile;	
			}
		}
		return false;
	}
	
	public function get_group_profile( $page_id ) 
	{
		$profiles =  $this->user_and_group( 'group' );	
		foreach ( $profiles as $key => $profile )
		{
			if ( $profile['page_id'] == $page_id )
			{
				return $profile;	
			}
		}
		return false;
	}
}
?>