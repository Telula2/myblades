<?php
/**
 * Устаревшие функции 
*/

function usam_get_cart_items_subtotal()
{
	return usam_get_basket_subtotal();
}

function usam_cart_item_count()
{	
	return usam_get_basket_number_items();
}

/**
 * Функция цены товара
 */
function usam_the_product_price( $old_price = false, $product_id = 0 ) 
{
	return usam_get_product_price_currency( $product_id, $old_price );
}

/**
* Получить сcылку на термин
*/
function usam_get_term_link( $term_id, $taxonomy ) 
{ 
	return get_term_link($term_id, $taxonomy );
}

function usam_get_text_div( $str, $len )
{	
	return usam_limit_words( $str, $len ) ;
}

function usam_product_existing_rating( $vote_total = false, $no_grey = true ) 
{
	echo usam_product_rating( $vote_total, $no_grey );
}

function usam_get_product_existing_rating( $product_id = null, $vote_total = true, $no_grey = false ) 
{
	echo usam_get_product_rating( $product_id, $vote_total, $no_grey );
}


/**
 * Получить постоянную ссылку на товар
 */
function usam_the_product_permalink( $product_id = null ) 
{	
	return get_permalink( $product_id );	
}

?>