<?php
// Класс API */
class Universam_API 
{
	private $query = null;
	private $errors = array();
	
	public function __construct()
	{		
		if ( !empty($_REQUEST['query']) && !empty($_REQUEST['software']) && $_REQUEST['software'] == 'universam'  )
		{			
			add_action( 'init', array($this, 'handler') );
		}		
	}		
	
	public function handler() 
	{			
		$this->query = sanitize_title($_REQUEST['query']);	
		$callback = "controller_{$this->query}";	
		if ( method_exists( $this, $callback )) 
		{
			$result = $this->$callback();
			
			echo json_encode( $result );		
			exit();
		}		
	}
	
	private function set_error( $error )
	{			
		$this->errors[] = sprintf( __('Universam API вызвало ошибку %s'), $error );
	}
	
	public function set_log_file()
	{
		usam_log_file( $this->errors );
		$this->errors = array();
	}	
	
	function send_request( $params )
	{ 
		$license = get_option ( 'usam_license', array() );	
		
		$url  = 'http://wp-universam.ru/api';				
		$params['server_name']  = $_SERVER['SERVER_NAME'];
		$params['software']     = 'universam';				
		$params['license']      = !empty($license['name'])?$license['license']:'';	
		$params['version']      = USAM_VERSION;			
				
		$args = array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.1',
			'blocking' => true,
			'headers' => array(),
			'body' => $params
		);					
		$response = wp_remote_post( $url, $args );	
	
		$response_code    = wp_remote_retrieve_response_code( $response );
		$response_message = wp_remote_retrieve_response_message( $response );
		if ( 200 !== $response_code && ! empty( $response_message ) )
		{		
			$this->set_error( sprintf( __( 'Произошла API ошибка №%s. %s.', 'usam' ),$response_code, $response_message) );	
			return false;
		}
		elseif ( 200 !== $response_code )
		{		
			$this->set_error( sprintf( __( 'Произошла API ошибка %s.', 'usam' ),$response_code) );	
			return false;
		}	
		else 
		{ 
			$out = json_decode( wp_remote_retrieve_body( $response ), true );	
			if ( isset( $out['error'] ) ) 
			{		
				$this->set_error( $out['error'] );	
				return false;
			}
			return $out;
		}			
	}

	public function registration( )
	{					
		$params = array(					
			'query'    => 'new_reg',						
		);			
		$request = $this->send_request( $params );			
		
		$result = false;		
		$license = get_option ( 'usam_license' );			
		if ( !empty($request['result']) ) 
		{		
			$license['type'] = strtoupper($request['type']);
			$license['status'] = $request['status'];			
			if ( !empty($request['date']) )
				$license['date'] = date( "Y-m-d", strtotime($request['date']));
			
			$result = true;
		}		
		update_option ( 'usam_license', $license );			
		
		return $result;
	}
	
	public function universam_activation( )
	{
		$params = array(					
			'query'    => 'program_activation',					
		);			
		$request = $this->send_request( $params );			
	}
	
	public function universam_deactivation( )
	{
		$params = array(						
			'query'    => 'program_deactivation',					
		);			
		$request = $this->send_request( $params );			
	}
		
	public function set_free_license( )
	{
		return $this->set_license( 'free' );
	}
	
	public function set_temporary_license( )
	{
		return $this->set_license( 'temporary' );
	}
	
	private function set_license( $type )
	{
		$params = array(						
			'query'    => 'get_license',		
			'type'     => $type,		
			'email'    => get_option('admin_email'),				
		);			
		$request = $this->send_request( $params );	
		
		$result = false;	
		$license = array( 'license' => '', 'name' => '', 'type' => '', 'status' => 0, 'date_to' => '', 'date_issue' => '' );
		if ( !empty( $request['result'] ) ) 
		{		
			$license = array( 'license' => strtoupper($request['license']), 'name' => $request['name'], 'type' => strtoupper($request['type']), 'status' => $request['status'], 'date_to' => '' );
			if ( !empty($request['date_to']) )
				$license['date_to'] = date( "Y-m-d", strtotime($request['date_to']));
			
			if ( !empty($request['date_issue']) )
				$license['date_issue'] = date( "Y-m-d H:i:s", strtotime($request['date_issue']));
			
			$result = true;
		}			
		update_option ( 'usam_license', $license );
	}
		
	public function sent_support_message( $data )
	{
		global $wpdb;
		$php_version = phpversion();
		$locale      = get_locale();		
		
		$params = array(						
			'query'             => 'message_to_developers',			
			'php'               => $php_version,
			'locale'            => $locale,
			'mysql'             => $wpdb->db_version(),					
			'message'           => $data['message'],
			'subject'           => $data['subject'],			
		);	
		$result = $this->send_request( $params );
		if ( !empty($response['error']) ) 
		{			
			return true;
		}
		return false;
	}
	
	public function controller_message_from_developers( )
	{
		$result = false;
		if ( !empty($_REQUEST['message']) )
		{
			$message = sanitize_text_field(stripcslashes($_REQUEST['message']));
			$insert = array( 'message' => $message, 'outbox' => 1  );			
			$result = usam_insert_support_message( $insert );
		}
		return $result;
	}
	
	public function collected_links( $link )
	{					
		$params = array(					
			'query'    => 'collected_links',	
			'link'     => $link,			
		);			
		$request = $this->send_request( $params );		
		return $request;
	}
	
	public function confirm_deactivation( $reason, $message )
	{					
		$params = array(					
			'query'    => 'confirm_deactivation',	
			'email'    => get_option( 'admin_email' ),
			'reason'   => $reason,	
			'message'  => $message,	
		);			
		$request = $this->send_request( $params );		
		return $request;
	}
		
	public function check_license(  )
	{				
		$params = array(					
			'query'    => 'check_license',	
		);			
		$request = $this->send_request( $params );		
		return $request;
	}
	
	public function tracker( $data )
	{				
		$params = array(					
			'query'    => 'tracker',	
			'data'     => $data,			
		);			
		$request = $this->send_request( $params );		
		return $request;
	}
}
$api = new Universam_API();