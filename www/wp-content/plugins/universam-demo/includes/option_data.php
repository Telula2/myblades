<?php
function usam_delete_data( $id, $option_key ) 
{		
	if ( !is_array($id) )
	{
		$ids = array( $id );
	}
	else
		$ids = $id;
		
	$option = get_option($option_key, array() );						
	$array = maybe_unserialize($option);		

	$ids = array_map( 'intval', $ids );
	$result = false;	

	foreach( $array as $key => $value )
	{				
		if ( in_array( $value['id'], $ids) )
		{ 
			unset($array[$key]);
			$result = true;
		}
	} 
	if ( $result )
		update_option($option_key, maybe_serialize($array) );
}

function usam_add_data( $data, $option_key ) 
{	
	$option = get_option($option_key, array() );						
	$array = maybe_unserialize($option);	
	
	if ( !empty($array) )
	{	
		usort($array, function($a, $b){  return ($b['id'] - $a['id']); });		
		$id = $array[0]['id'];
		$id++;				
	}
	else
	{
		$array = array();
		$id = 1;
	}
	$data['id'] = $id;
	$array[] = $data;	
	update_option($option_key, maybe_serialize($array) );
		
	return $id;
}

function usam_edit_data( $update, $id, $option_key ) 
{	
	$option = get_option($option_key, array() );						
	$array = maybe_unserialize($option);	
	if ( empty($array) )
		return false;
	
	$result = false;
	foreach ( $array as $key => $data ) 
	{
		if ( $data['id'] == $id )	
		{
			$result = true;
			$array[$key] = array_merge($array[$key], $update );			
			break;
		}
	}
	if ( $result )
		update_option($option_key, maybe_serialize($array) );
	return $result;
}

function usam_get_data( $id, $option_key ) 
{	
	$option = get_option($option_key, array() );						
	$array = maybe_unserialize($option);	
	
	$result = array();
	foreach ( $array as $key => $data ) 
	{
		if ( $data['id'] == $id )	
		{
			$result = $data;
			break;
		}
	}
	return $result;
}
?>