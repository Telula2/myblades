<?php
// Описание файла: Форматирование данных

// Описание: Конвертирование веса
function usam_convert_weight($in_weight, $in_unit, $out_unit = 'pound', $raw = false) 
{
	switch($in_unit) 
	{
		case "kilogram":
			$intermediate_weight = $in_weight * 1000;
		break;
		case "gram":
			$intermediate_weight = $in_weight;
		break;
		case "once":
		case "ounce":
			$intermediate_weight = ($in_weight / 16) * 453.59237;
		break;
		case "pound":
		default:
			$intermediate_weight = $in_weight * 453.59237;
		break;
	}
	switch($out_unit) 
	{
		case "kilogram":
			$weight = $intermediate_weight / 1000;
		break;
		case "gram":
			$weight = $intermediate_weight;
		break;
		case "once":
		case "ounce":
			$weight = ($intermediate_weight / 453.59237) * 16;
		break;
		case "pound":
		default:
			$weight = $intermediate_weight / 453.59237;
		break;
	}
	if($raw)
		return $weight;
	return round($weight, 2);
}

function usam_get_dimension_units(  ) 
{		
	$dimension_units = array(		
		'mm'    => __( 'мм', 'usam' ),
		'cm'    => __( 'см', 'usam' ),
		'm'     => __( 'метров', 'usam' ),
		'in'    => __( 'дюймов', 'usam' ),
		'yd'    => __( 'ярдов', 'usam' ),			
	);	
	return $dimension_units;
}

function usam_get_weight_units(  ) 
{		
	$weight_units = array(		
		'g'      => __( 'граммы', 'usam' ),
		'kg'     => __( 'килограммы', 'usam' ),
		'lbs'    => __( 'фунты', 'usam' ),
		'oz'     => __( 'унции', 'usam' )
	);
	return $weight_units;
}

function usam_get_name_weight_units(  ) 
{		
	$weight_units = usam_get_weight_units( );
	$weight_unit = get_option('usam_weight_unit');
	return isset($weight_units[$weight_unit])?$weight_units[$weight_unit]:'';
}

function usam_local_date( $date, $date_format = '' ) 
{		
	if ( $date == '' )
		return '';
	if ( $date_format == '' )
		$date_format = get_option( 'date_format', 'Y/m/d' )." H:i";
		
	return date_i18n( $date_format, strtotime(get_date_from_gmt( $date )) );
}

function usam_local_formatted_date( $date, $date_format = '' ) 
{
	$timestamp = strtotime( $date );	
	$time_diff = time() - $timestamp;
	if ( $time_diff > 0 && $time_diff < 86400 ) // 24 * 60 * 60
		return sprintf( __( '%s назад' ), human_time_diff( $timestamp, time() ) );
	else
		return usam_local_date( $date, $date_format );
}
			

/**
 * Получить отформатированный телефонны номер
 */
function usam_get_phone_format( $sPhone )
{ 
    $sPhone = preg_replace("[^0-9]",'',$sPhone); 	
	$len = strlen($sPhone);
	switch ( $len ) 
	{
		case 10:
			$formats = array( '10' => '8 (###) ### ####' );
			$sPhone = usam_phone_format($sPhone, $formats);
		break;
		case 11:
			$formats = array( '11' => '# (###) ### ## ##');
			$sPhone = usam_phone_format($sPhone, $formats);
		break;
	}   
    return $sPhone; 
} 

/**
 * Форматирование телефонного номера по шаблону и маске для замены
 */
function usam_phone_format($phone, $format, $mask = '#')
{
    $phone = preg_replace('/[^0-9]/', '', $phone);

    if ( is_array($format) ) 
	{
        if (array_key_exists(strlen($phone), $format))
            $format = $format[strlen($phone)];
        else 
            return false;
    }
    $pattern = '/' . str_repeat('([0-9])?', substr_count($format, $mask)) . '(.*)/';
    $format = preg_replace_callback( str_replace('#', $mask, '/([#])/'),  function () use (&$counter) {  return '${' . (++$counter) . '}';  },  $format );
    return ($phone) ? trim(preg_replace($pattern, $format, $phone, 1)) : false;
}

 // Русские в английские
function usam_sanitize_title_with_translit( $title, $rtl_standard = '' ) 
{	
	$gost = array(
	   "Є"=>"EH","І"=>"I","і"=>"i","№"=>"#","є"=>"eh",
	   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
	   "Е"=>"E","Ё"=>"JO","Ж"=>"ZH",
	   "З"=>"Z","И"=>"I","Й"=>"JJ","К"=>"K","Л"=>"L",
	   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
	   "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"KH",
	   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
	   "Ы"=>"Y","Ь"=>"","Э"=>"EH","Ю"=>"YU","Я"=>"YA",
	   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
	   "е"=>"e","ё"=>"jo","ж"=>"zh",
	   "з"=>"z","и"=>"i","й"=>"jj","к"=>"k","л"=>"l",
	   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"kh",
	   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
	   "ы"=>"y","ь"=>"","э"=>"eh","ю"=>"yu","я"=>"ya",
	   "—"=>"-","«"=>"","»"=>"","…"=>""
	  );

	$iso = array(
	   "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"#","є"=>"ye","ѓ"=>"g",
	   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
	   "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
	   "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
	   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
	   "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
	   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
	   "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
	   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
	   "е"=>"e","ё"=>"yo","ж"=>"zh",
	   "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
	   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
	   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
	   "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	   "—"=>"-","«"=>"","»"=>"","…"=>""
	); 	
	switch ( $rtl_standard ) 
	{
		case 'off':
		    return $title;		
		case 'gost':
		    return strtr($title, $gost);
		default: 
		    return strtr($title, $iso);
	}
}

// Конвертирование 10M в число байт
function usam_let_to_num( $size )
{		
	$l 		= substr( $size, -1 );
	$ret 	= substr( $size, 0, -1 );
	switch( strtoupper( $l ) ) {
		case 'P':
			$ret *= 1024;
		case 'T':
			$ret *= 1024;
		case 'G':
			$ret *= 1024;
		case 'M':
			$ret *= 1024;
		case 'K':
			$ret *= 1024;
	}
	return $ret;
}

// Обрезать строку до нужной длины
function usam_limit_words( $str = '', $len = 100, $more = true) 
{
   if ( $str == "" || $str == NULL ) 
	   return $str;	 
   $str = trim($str);
   $str = strip_tags(str_replace("\r\n", " ", $str));	
   if ( strlen($str) <= $len ) 
	   return $str;
   $str = substr($str,0,$len);	  
   if ( $str != "" ) 
   {
		if ( !substr_count($str," ") )
		{
			if ( $more ) $str .= " ...";
				return $str;
		}
		while( strlen($str) && ($str[strlen($str)-1] != " ") ) 
		{
			$str = substr($str,0,-1);
		}
		$str = substr($str,0,-1);
		if ( $more ) 
			$str .= " ...";
	}
	return $str;
}
?>