<?php		
class USAM_Printing_Form_Order extends USAM_Printing_Form
{		
	private $purchase_log;	
	protected $cartcontent;	
	protected $payment_document;	
	protected $shipped_document;
	
	public function __construct( $id = null )
	{ 
		if ( !empty($id) )			
			$id = $id;			
		elseif ( isset($_GET['id']) && is_numeric($_GET['id']) )					
			$id = $_GET['id'];		
		else
		{	
			$query = array( 'fields' => 'id', 'number' => 1 );	
			$id = usam_get_orders( $query );	
		}				
		parent::__construct( $id );
		$this->get_order_data();
	}	
	
	public function get_order_data( )
	{					
		$this->purchase_log = new USAM_Order( $this->id );		
		$this->order_data = $this->purchase_log->get_data();	
		$this->order_data['total_discont'] = $this->order_data['coupon_discount']+$this->order_data['discount']+$this->order_data['bonus'];	
		$this->cartcontent = $this->purchase_log->get_order_products();		
		$this->payment_document = $this->purchase_log->get_payment_document();
		$this->shipped_document = $this->purchase_log->get_shipped_document();		
	}	

	protected function display_table_tbody( $display_columns )
	{		
		foreach ( $this->cartcontent as $product ) 
		{
			$total = $product->price * $product->quantity;		
			$row = '';
			foreach ( $display_columns as $column_key => $display_name ) 
			{						
				$column_html ='';
				switch ( $column_key ) 
				{
					case 'name' :
						$column_html = $product->name; 
					break;
					case 'sku' :
						$column_html = usam_get_product_meta( $product->product_id, 'sku' );
					break;
					case 'barcode' :
						$column_html = usam_get_product_barcode( $product->product_id );
					break;				
					case 'quantity' :
						$column_html = $product->quantity;
					break;
					case 'price' :
						$column_html = usam_currency_display($product->price);
					break;
					case 'total' :
						$column_html = usam_currency_display($total);
					break;
				}
				$row .= "<td class ='column-$column_key'>$column_html</td>";
			}
			echo "<tr>$row</tr>";
		}			
	}
	
	protected function display_table_tfoot( $display_columns )
	{	
		$cols = count( $display_columns ) - 4;			
		?>		
		<tr class="subtotal">
			<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Стоимость товаров', 'usam' ); ?></th>
			<td>%order_basket_currency%</td>
		</tr>
		[if order_basket_discount>0.0 {<tr class='total_discount'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Скидка', 'usam' ); ?></th>
			<td>%order_basket_discount_currency%</td>
		</tr>}]	
		[if order_basket_discount>0 {<tr class='total_discount'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Стоимость товаров с учетом скидки', 'usam' ); ?></th>
			<td>%order_final_basket_currency%</td>
		</tr>}]			
		[if total_discount>0.0 {<tr class='total_discount'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Скидка', 'usam' ); ?></th>
			<td>%total_discount_currency%</td>
		</tr>}]		
		[if total_tax>0.0 {<tr class='total_tax'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Налог', 'usam' ); ?></th>
			<td>%total_tax_currency%</td>
		</tr>}]													
		[if total_shipping>0.0 {<tr class="total_shipping">
			<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Доставка', 'usam' ); ?></th>
			<td>%total_shipping_currency%</td>
		</tr>	
		}]				
		<tr class="totalprice">
			<td colspan='<?php echo $cols; ?>'></td>
			<th colspan='3' style="text-align:right;"><?php esc_html_e( 'Итого', 'usam' ); ?></th>
			<td>%total_price_currency%</td>
		</tr>		
		<?php					
	}	
	
	protected function get_args( )
	{		
		$order_shortcode = new USAM_Order_Shortcode( $this->id );
		$args = $order_shortcode->get_common_args();		
		return $args;
	}	
	
	public function display_requisites_shop()
	{
		?>
		<div id="usam_requisite_company">
		<?php	
			$requisites = usam_shop_requisites();	
			echo sprintf( __( "Продавец: %s", 'usam' ), $requisites['full_company_name'] ).' ';
			echo sprintf( __( "ИНН: %s", 'usam' ), $requisites['inn'] ).' ';
			echo sprintf( __( "Адрес: %s", 'usam' ), $requisites['full_legaladdress'] );
		?>
		</div>
		<?php
	}
		
	public function display_customer_details()
	{
		$this->customer_data = $this->purchase_log->get_customer_data();			
		$args['type_payer'] = $this->order_data['type_payer'];
		$order_props_group = usam_get_order_props_groups( $args );
		
		$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );	
		?>
		<table class = "usam_details_box">	
		<tr>		
		<?php 	
		$i = 0;
		foreach( $order_props_group as $group )
		{
			$i++;			
			?>	
			<td valign="top" width="50%">
			<div id='order_props_group-<?php echo $group->id; ?>' class = "order_props_group">
				<h2><?php echo $group->name; ?></h2>
				<div class = 'usam_details'>						
					<table>
						<?php 				
						foreach ( $list_properties as $unique_name => $field ) 			
						{ 		
							if ( $group->id === $field->group && !empty($this->customer_data[$unique_name]['value']) )
							{						
								switch ( $field->type )
								{							
									case 'location':	
										$display = usam_get_full_locations_name( $this->customer_data[$unique_name]['value'] );							
									break;
									case 'location_type':					
										if( is_numeric( $this->customer_data[$unique_name]['value'] ) )
										{
											$location = usam_get_location( $this->customer_data[$unique_name]['value'] );
											$display = $location['name'];
										}
										else
											$display = $this->customer_data[$unique_name]['value'];							
									break;
									default:
										$display = $this->customer_data[$unique_name]['value'];
								}
								echo '<tr><td><strong>'.$field->name.':</strong></td><td><span id = "item-'.$field->unique_name.'">'.$display.'</span></td></tr>';
							}
						}
						?>					
					</table>	 			
				</div>					
			</div>	
			</td>			
			<?php 						
		}
		?>
		</tr>
		</table>
		<?php 		
	}
	
	public function display_order_details()
	{		
		$payments = $this->purchase_log->get_payment_status_sum();
		?>
		<table id="order">
			<thead>
				<tr>
					<th><?php echo esc_html_e( 'Дата заказа', 'usam' ); ?></th>					
					<th><?php echo esc_html_e( 'Способ доставки', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Способ оплаты', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплата', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплачено', 'usam' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo date_i18n("d-m-Y", strtotime($this->order_data['date_insert']) ); ?></td>					
					<td><?php echo isset($this->shipped_document['name'])?$this->shipped_document['name']:''; ?></td>
					<td><?php echo isset($this->payment_document['name'])?$this->payment_document['name']:''; ?></td>
					<td><?php echo usam_get_order_payment_status_name( $this->order_data['paid'] ); ?></td>
					<td><?php echo $payments['total_paid']; ?></td>					
				</tr>
			</tbody>
		</table>
		<?php		
	}	
}
?>