<?php		
class USAM_Printing_Form_payment extends USAM_Printing_Form
{		
	private $purchase_log;	
	protected $cartcontent;	
	protected $payment;
	public function __construct( $id = null )
	{		
		if ( !empty($id) )			
			$id = $id;			
		elseif ( isset($_GET['id']) && is_numeric($_GET['id']) )					
			$id = $_GET['id'];		
		else
		{	
			$query = array( 'fields' => 'id', 'number' => 1 );	
			$id = usam_get_orders( $query );	
		}				
		parent::__construct( $id );
		$this->get_order_data();
	}	
	
	public function get_order_data( )
	{					
		$this->payment = usam_get_payment_document( $this->id );	
		
		$this->purchase_log = new USAM_Order( $this->payment['document_id'] );		
		$this->order_data = $this->purchase_log->get_data();		
		$this->order_data['total_discont'] = $this->order_data['coupon_discount']+$this->order_data['discount']+$this->order_data['bonus'];	
		$this->cartcontent = $this->purchase_log->get_order_products();		
	}	

	protected function display_table_tbody( $display_columns )
	{		
		$i = 0;
		$args = array(
			'display_currency_symbol' => false,
			'decimal_point'   		  => true,
			'display_currency_code'   => false,
			'display_as_html'         => false,
			'code'                 => false,
		);
		foreach ( $this->cartcontent as $product ) 
		{
			$i++;
			$total = ( $product->price * $product->quantity);		
			$row = '';
			foreach ( $display_columns as $column_key => $display_name ) 
			{						
				$column_html ='';
				$align = 'right';
				switch ( $column_key ) 
				{
					case 'number' :
						$column_html = $i; 
						$align = 'center';
					break;
					case 'name' :
						$column_html = $product->name; 
						$align = 'left';
					break;
					case 'sku' :
						$column_html = usam_get_product_meta( $product->product_id, 'sku' );
					break;
					case 'barcode' :
						$column_html = usam_get_product_barcode( $product->product_id );
					break;				
					case 'quantity' :
						$column_html = $product->quantity;
					break;
					case 'price' :
						$column_html = usam_currency_display($product->price, $args);
					break;
					case 'total' :
						$column_html = usam_currency_display($total, $args);
					break;
				}
				$row .= "<td class ='column-$column_key' align='$align'>$column_html</td>";
			}
			echo "<tr>$row</tr>";
		}			
	}
	
	protected function display_table_tfoot( $display_columns )
	{	
		$cols = count( $display_columns ) - 2;	
		?>		
		<tr class="subtotal">
			<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Сумма', 'usam' ); ?></th>
			<td>%order_final_basket%</td>
		</tr>						
		[if total_discount>0.00 {<tr class='total_discount'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Скидка', 'usam' ); ?></th>
			<td>%total_discount%</td>
		</tr>}]		
		[if total_tax>0.00 {<tr class='total_tax'>
		<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Налог', 'usam' ); ?></th>
			<td>%total_tax%</td>
		</tr>}]													
		[if total_shipping>0.00 {<tr class="total_shipping">
			<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Доставка', 'usam' ); ?></th>
			<td>%total_shipping%</td>
		</tr>	
		}]				
		<tr class="totalprice">
			<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Итого', 'usam' ); ?></th>
			<td>%total_price%</td>
		</tr>		
		<?php					
	}	
	
	protected function get_args( )
	{		
		$gateway = usam_get_payment_gateway( $this->payment['gateway_id'] );
		$date_format = get_option('date_format', "d.m.Y");

		$sum = explode(".", ($this->payment['sum']));
		$args = array(			
			'date'                => date_i18n($date_format, strtotime($this->payment['date_time'])),
			'closedate'           => isset($this->payment['date_payed'])?sprintf( __( 'Срок оплаты: %s', 'usam' ),date_i18n($date_format, strtotime($this->payment['date_payed']))):'',			
			'totalprice'          => $this->payment['sum'],	
			'totalprice1'         => $sum[0],	
			'totalprice2'         => $sum[1],			
			'document_number'     => $this->payment['document_number'],			
			'totalprice_currency' => usam_currency_display( $this->payment['sum'], array( 'display_as_html' => false ) ),
			'status'              => isset($this->payment['status'])?$this->payment['status']:'',
			'status_name'         => usam_get_payment_document_status_name( $this->payment['status'] ),					
			'note'                => isset($this->payment['note'])?nl2br($this->payment['note']):'',
			'gateway_id'   => $this->payment['gateway_id'],
			'gateway_name' => isset($gateway['name'])?$gateway['name']:'',
			'document_type' => $this->payment['document_type'],			
			'document_id'   => $this->payment['document_id'],	
		);			
		if ( !empty($this->payment['bank_account_id']) )
			$args +=  $this->get_args_company( $this->payment['bank_account_id'], 'recipient' );

		if ( !empty($this->payment['document_id']) && $this->payment['document_type'] == 'order' )
		{
			$order_shortcode = new USAM_Order_Shortcode( $this->payment['document_id'] );
			$args += $order_shortcode->get_common_args();
		}
		return $args;
	}		
}
?>