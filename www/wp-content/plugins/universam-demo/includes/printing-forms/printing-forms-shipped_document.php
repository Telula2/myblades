<?php		
class USAM_Printing_Form_shipped_document extends USAM_Printing_Form
{		
	private $purchase_log;	
	private $shipped_document = array();	
	private $products_shipped = array();	
	
	public function __construct( $id = null )
	{
		if ( !empty($id) )			
			$id = $id;			
		elseif ( isset($_GET['id']) && is_numeric($_GET['id']) )					
			$id = $_GET['id'];		
		else
		{	
			$query = array( 'fields' => 'id', 'number' => 1 );	
			$id = usam_get_shipping_documents( $query );				
		}				
		parent::__construct( $id );
		
		$this->shipped_document = usam_get_shipped_document( $this->id );	
		if ( empty($this->shipped_document) )
			return false;
		
		$this->purchase_log = new USAM_Order( $this->shipped_document['order_id'] );		
		$this->order_data = $this->purchase_log->get_data();	
		$this->order_data['total_discont'] = $this->order_data['coupon_discount']+$this->order_data['discount']+$this->order_data['bonus'];	
		$this->products_shipped = usam_get_products_shipped_documents( $this->id );		
	}	
	
	protected function display_table_tbody( $display_columns )
	{				
		foreach ( $this->products_shipped as $product ) 
		{
			$total = ( $product->price * $product->quantity);		
			$row = '';
			foreach ( $display_columns as $column_key => $display_name ) 
			{						
				$column_html ='';
				switch ( $column_key ) 
				{
					case 'name' :
						$column_html = $product->name; 
					break;
					case 'sku' :
						$column_html = usam_get_product_meta( $product->product_id, 'sku' );
					break;
					case 'barcode' :
						$column_html = usam_get_product_barcode( $product->product_id );
					break;				
					case 'quantity' :
						$column_html = $product->quantity;
					break;
					case 'price' :
						$column_html = usam_currency_display($product->price);
					break;
					case 'total' :
						$column_html = usam_currency_display($total);
					break;
				}
				$row .= "<td class ='column-$column_key'>$column_html</td>";
			}
			echo "<tr>$row</tr>";
		}			
	}
	
	protected function display_table_tfoot( $display_columns )
	{	
		$cols = count( $display_columns ) - 2;	
		?>
		<tr class="subtotal">
			<td colspan='<?php echo $cols; ?>'></td>
			<th><?php esc_html_e( 'Сумма', 'usam' ); ?>:</th>
			<td>%totalprice_currency%</td>
		</tr>	
		<?php					
	}
	
	protected function get_args( )
	{				
		$totalprice = 0;
		foreach ( $this->products_shipped as $product ) 
		{
			$totalprice += $product->price*$product->quantity;
		}		
		$args['document_id'] = $this->id;	
		$args['shipped_notes'] = $this->id;	
		$args['shipped_price'] = $this->shipped_document['price'];
		$args['shipped_price_currency'] = usam_currency_display($this->shipped_document['price']);

		$args['totalprice'] = $totalprice;		
		$args['totalprice_currency'] = usam_currency_display($totalprice);
		
		$order_shortcode = new USAM_Order_Shortcode( $this->shipped_document['order_id'], $this->id );
		$args += $order_shortcode->get_common_args();
		
		return $args;
	}		
	
	public function display_customer_details()
	{
		$this->customer_data = $this->purchase_log->get_customer_data();			
		$args['type_payer'] = $this->order_data['type_payer'];
		$order_props_group = usam_get_order_props_groups( $args );		
		
		$list_properties = usam_get_order_properties( array('fields' => 'unique_name=>data') );			
		?>
		<table class = "usam_details_box">	
		<tr>		
		<?php 	
		$i = 0;
		foreach( $order_props_group as $group )
		{
			$i++;			
			?>	
			<td valign="top" width="50%">
			<div id='order_props_group-<?php echo $group->id; ?>' class = "order_props_group">
				<h2><?php echo $group->name; ?></h2>
				<div class = 'usam_details'>						
					<table>
						<?php 				
						foreach ( $list_properties as $unique_name => $field ) 			
						{ 		
							if ( $group->id === $field->group && !empty($this->customer_data[$unique_name]['value']) )
							{						
								switch ( $field->type )
								{							
									case 'location':	
										$display = usam_get_full_locations_name( $this->customer_data[$unique_name]['value'] );							
									break;
									case 'location_type':					
										if( is_numeric( $this->customer_data[$unique_name]['value'] ) )
										{
											$location = usam_get_location( $this->customer_data[$unique_name]['value'] );
											$display = $location['name'];
										}
										else
											$display = $this->customer_data[$unique_name]['value'];							
									break;
									default:
										$display = $this->customer_data[$unique_name]['value'];
								}
								echo '<tr><td><strong>'.$field->name.':</strong></td><td><span id = "item-'.$field->unique_name.'">'.$display.'</span></td></tr>';
							}
						}
						?>					
					</table>	 			
				</div>					
			</div>	
			</td>			
			<?php 						
		}
		?>
		</tr>
		</table>
		<?php 		
	}
}
?>