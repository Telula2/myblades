<?php		
class USAM_Printing_Form_crm extends USAM_Printing_Form
{		
	private $products;	
	private $data;	
	public function __construct( $id = null )
	{
		if ( !empty($id) )			
			$id = $id;				
		elseif ( isset($_GET['id']) && is_numeric($_GET['id']) )					
			$id = $_GET['id'];	
		else
		{	
			require_once( USAM_FILE_PATH . '/includes/crm/documents_query.class.php' );	
			$query = array( 'fields' => 'id', 'number' => 1, 'type' => 'suggestion' );	
			$id = usam_get_documents( $query );			
		}		
		parent::__construct( $id );	
		
		$doc = new USAM_Document( $this->id );	
		$this->data = $doc->get_data();
		$this->products = $doc->get_products();	
	}		
	
	protected function get_args( )
	{		
		$subtotal = 0;
		$discount = 0;
		$total_tax = 0;
		foreach ( $this->products as $product ) 		
		{
			$subtotal += ($product['price']+$product['tax'])*$product['quantity'];			
			$discount += ($product['old_price']-$product['price'])*$product['quantity'];
			$total_tax += $product['tax'];
		}						
		$date_format = get_option('date_format', "d.m.Y");
		$customer_type = isset($this->data['customer_type'])?$this->data['customer_type']:0;
		$counterparty = '';
		$counterparty_accounts = ''; 		
		if ( $customer_type == 'contact' )
		{
			$contact_data = usam_get_contact( $this->data['customer_id'] );		
			$counterparty = $contact_data['firstname']." ".$contact_data['lastname']; 
		}
		elseif ( $customer_type == 'company' )
		{
			$company = new USAM_Company( $this->data['customer_id'] );	
			$company_data = $company->get_data();	
			$meta = $company->get_meta();
			$bank_accounts = $company->get_bank_accounts();
			
			if ( !empty($bank_accounts[0]) )
				$bank_accounts = $bank_accounts[0];	
		
			$d = $meta['inn'] != '' && $meta['ppc'] != '' ?"/":'';	
			$legalpostcode = !empty($meta['legalpostcode'])?$meta['legalpostcode']:'';
			$legalcity = !empty($meta['legalcity'])?$meta['legalcity']:'';
			$legaladdress = !empty($meta['legaladdress'])?$meta['legaladdress']:'';
			$ppc = !empty($meta['ppc'])?$meta['ppc']:'';
			$inn = !empty($meta['inn'])?$meta['inn']:'';
			
			$counterparty = $company_data['name'].' '.$inn.$d.$ppc.' '.$legalpostcode.', '.$legalcity.', '.$legaladdress;		
			
			if ( !empty($bank_accounts) )
				$counterparty_accounts = __('р/с','usam').' '.$bank_accounts->number.' '.$bank_accounts->name.' '.__( 'БИК', 'usam' ).' '.$bank_accounts->bic.' '.$bank_accounts->address.' '.__('кор/с','usam').' '.$bank_accounts->bank_ca;
		}			
		$args = array(			
			'date'             => date_i18n($date_format, strtotime($this->data['date_insert'])),
			'closedate'        => isset($this->data['closedate'])?sprintf( __( 'Срок оплаты: %s', 'usam' ),date_i18n($date_format, strtotime($this->data['closedate']))):'',
			'id'               => isset($this->data['number'])?$this->data['number']:'',
			'subtotal'         => $subtotal,			
			'subtotal_currency' => usam_currency_display( $subtotal, array( 'display_as_html' => false ) ),
			'status_name'        => isset($this->data['status'])?$this->data['status']:'',
			'total_tax'           => $total_tax,
			'total_tax_currency'  => usam_currency_display( $total_tax, array( 'display_as_html' => false ) ),
			'total_tax_title'     => sprintf( __( 'Налог: %s', 'usam' ), $total_tax )."\r\n",
			'shipping'            => $this->data['shipping'],
			'shipping_currency'   => usam_currency_display( $this->data['shipping'], array( 'display_as_html' => false ) ),
			'total_price'         => $this->data['totalprice'],
			'total_price_currency'=> usam_currency_display( $this->data['totalprice'] ),		
			'total_discount_title' => sprintf( __( 'Сумма скидки: %s', 'usam' ), $discount )."\r\n",				
			'total_discount'      => $discount,		
			'total_discount_currency' => usam_currency_display( $discount, array( 'display_as_html' => false ) ),	
			'description'         => isset($this->data['description'])?nl2br($this->data['description']):'',	
			'conditions'          => isset($this->data['conditions'])?nl2br($this->data['conditions']):'',	
			'counterparty'        => $counterparty,		
			'counterparty_accounts' => $counterparty_accounts
		);
		$args += $this->get_args_company( $this->data['bank_account_id'], 'recipient' );
		return $args;
	}		
	
	protected function blank_description( )
	{
		return array( 
		'%company_own_name%' => __('Название компании'), 
		'%company_own_inn%' => __('ИНН компании'), 
		'%company_own_ppc%' => __('КПП компании'), 
		'%company_own_legalpostcode%' => __('Индекс компании'), 
		'%company_own_legalcity%' => __('Город компании'), 
		'%company_own_legaladdress%' => __('Адрес компании') );
	}
	
	protected function display_table_tbody( $display_columns )
	{		
		foreach ( $this->products as $product ) 
		{
			$total = ( $product['price']+$product['tax']) * $product['quantity'];		
			$row = '';
			$post = get_post( $product['product_id'] );
			foreach ( $display_columns as $column_key => $display_name ) 
			{								
				$column_html ='';
				switch ( $column_key ) 
				{					
					case 'image' :
						$column_html = usam_get_product_thumbnail( $product['product_id'], 'manage-products', $post->post_title);
					break;
					case 'name' :
						$column_html = $post->post_title; 
					break;
					case 'sku' :
						$column_html = usam_get_product_meta( $product['product_id'], 'sku' );
					break;
					case 'barcode' :
						$column_html = usam_get_product_barcode( $product['product_id'] );
					break;				
					case 'quantity' :
						$column_html = $product['quantity'];
					break;
					case 'price' :
						$price = $product['old_price']>0?$product['old_price']:$product['price'];	
						$column_html = usam_currency_display( $price );
					break;
					case 'discount_price' :
						$column_html = usam_currency_display( $product['price'] );
					break;
					case 'discount' :
						$discount = $product['old_price']>0?($product['old_price'] - $product['price']):0;
						$column_html = usam_currency_display( $discount );
					break;
					case 'tax' :
						$column_html = usam_currency_display( $product['tax'] );
					break;
					case 'total' :
						$column_html = usam_currency_display($total);
					break;
				}
				$row .= "<td class ='column-$column_key'><p>$column_html</p></td>";
			}
			echo "<tr>$row</tr>";
		}			
	}
	
	protected function display_table_tfoot( $display_columns )
	{	
		$cols = count( $display_columns ) - 2;	
		?>		
		<tr class="subtotal">
			<td colspan='<?php echo $cols; ?>'></td>
			<td><?php esc_html_e( 'Сумма', 'usam' ); ?>:</td>
			<td>%subtotal_currency%</td>
		</tr>						
		[if total_discount>0 {<tr class='total_discount'>
			<td colspan='<?php echo $cols; ?>'></td>
			<td><?php esc_html_e( 'Скидка', 'usam' ); ?>:</td>
			<td>%total_discount_currency%</td>
		</tr>}]		
		[if total_tax>0 {<tr class='total_tax'>
			<td colspan='<?php echo $cols; ?>'></td>
			<td><?php esc_html_e( 'Налог', 'usam' ); ?>:</td>
			<td>%total_tax_currency%</td>
		</tr>}]													
		[if total_tax>0 {<tr class="total_shipping">
			<td colspan='<?php echo $cols; ?>'></td>
			<td><?php esc_html_e( 'Доставка', 'usam' ); ?>:</td>
			<td>%total_shipping_currency%</td>
		</tr>	
		}]				
		<tr class="totalprice">
			<td colspan='<?php echo $cols; ?>'></td>
			<td><?php esc_html_e( 'Итого', 'usam' ); ?>:</td>
			<td>%total_price_currency%</td>
		</tr>		
		<?php					
	}
}
?>