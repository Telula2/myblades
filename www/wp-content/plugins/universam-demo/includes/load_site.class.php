<?php
/**
 * Основные функции магазина
 * Такие вещи, как регистрация пользовательских типов пост и таксономий, правила перезаписи
 * @since 3.8
 */

// Загрузить сайт 
class USAM_Load_Site
{
	function __construct( ) 
	{					
		if ( !defined( 'DOING_CRON' ) || !DOING_CRON )
		{								 
			add_action( 'init',  array(&$this, 'core_setup_cart'), 2 );			// Настройка ядра корзины UNIVERSAM	
			add_action( 'shutdown',  array(&$this, 'delete_customer_meta') );
			
			$this->get_customer_location(); // Удалить
			
			$this->load_search();			
			// Темы
			require_once( USAM_FILE_PATH . '/includes/theme/your-account.class.php' );	
			require_once( USAM_FILE_PATH . '/includes/theme/shopping_cart.functions.php'  ); // корзина		
			require_once( USAM_FILE_PATH . '/includes/theme/theme.functions.php'   );	
			require_once( USAM_FILE_PATH . '/includes/theme/display_page_system.class.php'   );
			require_once( USAM_FILE_PATH . '/includes/theme/breadcrumbs.class.php' );			
			require_once( USAM_FILE_PATH . '/includes/theme/display_product_groups.class.php' );			
			require_once( USAM_FILE_PATH . '/includes/theme/theme_shortcode.class.php'  );
			require_once( USAM_FILE_PATH . '/includes/theme/feed.class.php' );				
			require_once( USAM_FILE_PATH . '/includes/product/product_filters-template.php'  );
			require_once( USAM_FILE_PATH . '/includes/basket/submit_checkout.class.php'   );	

			require_once( USAM_FILE_PATH . '/includes/seo/seo-frontend.class.php' );//Аналитика
			
			add_action( 'wp', array(&$this, 'force_ssl') );

			add_action( 'rss2_item', array(&$this, 'add_product_price_to_rss' ) );
			add_action( 'rss_item', array(&$this, 'add_product_price_to_rss') );
			add_action( 'rdf_item', array(&$this, 'add_product_price_to_rss') );	

			add_filter( 'body_class', array(&$this, 'body_class') );		
			
			add_action('usam_catalog_head', array( &$this, 'catalog_head') );	
		}				
		require_once( USAM_FILE_PATH . '/includes/query/usam_query.class.php'  );	
		add_action('usam_cart_insert', array( &$this, 'cart_insert') );
	}		
	
	// Отображение слайдаров
	function catalog_head( $cart ) 
	{	
		$sliders = usam_get_sliders( array( 'cache_results' => true ) );
		if ( !empty($sliders) )
		{ 
			foreach ( $sliders as $slider ) 
			{ 
				$setting = maybe_unserialize($slider->setting);	
				if ( $setting['show'] == 'catalog_head' )
				{ 
					usam_display_slider( $slider->id );
				}		
			}
		}
	}	
	
	function cart_insert( $cart ) 
	{	
		$id = $cart->get_properties('id');
		usam_update_customer_meta( 'basket_id', $id );
	}
		
	function delete_customer_meta() 
	{	
		global $wp_query;
		if ( isset($wp_query) && is_feed() ) 
		{  // избежать переполнения базы ботами
			usam_delete_all_customer_meta();
			return false;
		}
	}
	
	/**
	 * Фильтр основной части. Добавляет дополнительные классы к тегу body.
	 */
	function body_class( $classes )
	{
		global $wp_query, $usam_query;	
		
		if ( empty( $wp_query->post ) )
			return $classes;
		
		$post_content = isset($wp_query->post->post_content) ? $wp_query->post->post_content : '';	
		// если категория или товар...
		if ( usam_is_in_category() )
		{
			$classes[] = 'usam';
			$classes[] = 'usam-category';	
		}
		elseif(  usam_is_product() )
		{	
			$classes[] = 'usam';	
			$classes[] = 'usam-single-product';				
		}	
		else
		{
			switch ( $wp_query->post->post_name ) 
			{
				case 'checkout' :
					$classes[] = 'usam';
					$classes[] = 'usam-checkout';
				break;
				case 'transaction-results' :
					$classes[] = 'usam';
					$classes[] = 'usam-transaction-details';
				break;
				case 'basket' :
					$classes[] = 'usam';
					$classes[] = 'usam-basket';
				break;
				case 'your-account' :
					$classes[] = 'usam';
					$classes[] = 'usam-your-account';
				break;		
			}	
		}		
		return $classes;
	}
		
	/**
	 * Установка корзины
	 */
	function core_setup_cart() 
	{		
		global $usam_cart, $type_price, $user_ID;			

		$basket_parameters = maybe_unserialize( usam_get_customer_meta( 'basket' ) );	
		$basket_id = usam_get_customer_meta( 'basket_id' );	
		
		$type_payer = usam_get_customer_meta( 'type_payer' );
	//	if ( $user_ID )
	//	$accumulative_discount = get_user_meta( $user_ID, 'usam_accumulative_discount', true );		
		if ( empty($type_payer) )
		{
			$types_payers = usam_get_group_payers();		
			if ( !empty($types_payers) )
			{
				$type_payer = $types_payers[0]['id'];
				usam_update_customer_meta( 'type_payer', $type_payer );	
			}
			else
				$type_payer = '';
		}		
		$location = usam_get_customer_location();	
		$type_price = usam_get_customer_price_code();			
		$default = array( 'type_price' => $type_price, 'location' => $location, 'type_payer' => $type_payer, 'user_id' => $user_ID );			
	
		$basket_parameters = array_merge( $default, (array)$basket_parameters );
		$usam_cart = new USAM_CART( $basket_id, $basket_parameters );		
	}
		
	/* Загружает местоположение
	 * @since  3.8.9
	 */
	function get_customer_location() 
	{			
		if ( isset($_GET['location']) )
		{	
			usam_update_customer_location( $_GET['location'] );
		}		
	}

	function add_product_price_to_rss()
	{
		global $post;
		$price = usam_get_product_price( $post->ID );	
		if ( $price )
			echo '<price>'.$price.'</price>';
	}
	
	/**
	 * если пользователь находится на странице оформления заказа, заставить SSL, если эта опция поэтому установите
	 */
	function force_ssl() 
	{
		global $wp_query;
		if ( '1' == get_option( 'usam_force_ssl', 0 ) && ! is_ssl() && ( false !== strpos($wp_query->post->post_name, 'basket') || false !== strpos($wp_query->post->post_name, 'checkout') ) )
		{
			$sslurl = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			wp_redirect( $sslurl );
			exit;
		}
	}
	
	/**
	 * Фильтры поиска для сайта
	 */
	function load_search( ) 
	{	
		add_filter( 'posts_search', array('USAM_Search_Hook_Filter', 'search_by_site'), 500, 2 );
		add_filter( 'posts_orderby', array('USAM_Search_Hook_Filter', 'posts_orderby'), 500, 2 );
		add_filter( 'posts_join', array('USAM_Search_Hook_Filter', 'product_join'), 500, 2 );
		add_filter( 'posts_request', array('USAM_Search_Hook_Filter', 'posts_request_unconflict_role_scoper_plugin'), 500, 2);		
	}
}
new USAM_Load_Site();
?>