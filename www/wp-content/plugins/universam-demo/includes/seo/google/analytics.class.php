<?php
/**
 * Google Analytics
 */
class USAM_Google_Analytics 
{
	public static function print_script() 
	{		
		$option = get_option( 'usam_google', '' );	
		if ( empty($option['analytics_id']) )
			return false;
						
		$output = "<script type='text/javascript'>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			ga('create', '".esc_attr($option['analytics_id'])."', 'auto');				
			ga('set', 'currencyCode', '".usam_get_currency_price_by_code()."');";
			
		if ( !empty($option['analytics_ecommerce']) )
		{			
			$output .= "ga('require', 'ec');";	
			
			if ( usam_is_product() ) 
				$output .= self::product_display( );				
			elseif ( usam_is_product_category_sale() ) 
				$output .= self::list_display( __('Товары акций','usam') );
			elseif ( usam_is_product_brand() ) 
				$output .= self::list_display( __('Товары бренда','usam') );
			elseif ( usam_is_product_category( ) ) 
				$output .= self::list_display( __('Товары категорий','usam') );
			elseif ( is_page('basket') ) 
				$output .= self::basket_display( );
			elseif ( usam_is_transaction_results('success') ) 
				$output .= self::add_pushes( );
		}		
		$output .= "ga('send', 'pageview');";
		$output .= "</script>";	
		echo $output;
	}
	
	private static function list_display( $list ) 
	{  
		$output = '';
		$position = 1;
		while (usam_have_products()) :  	
			usam_the_product(); 
			
			global $post;			
			$name = get_the_title( $post->ID );
			$brand = usam_get_product_brand_name( $post->ID );
			$category = usam_get_product_category_name( $post->ID );	
			$price = usam_get_product_price( $post->ID );	
		
			$output .= "ga('ec:addImpression',{'id':'$post->ID','name':'$name','price':'$price','category':'$category','brand':'$brand','list':'$list','position':'$position'});";
			$output .= "ga('ec:setAction', 'detail');";			
			
			$position++;
		endwhile;
		return $output;
	}
	
	private static function basket_display( ) 
	{
		$output = '';	
		if( usam_get_basket_number_items() > 0 )
		{					
			while (usam_have_cart_items())  
			{
				usam_the_cart_item(); 
				$price = usam_cart_single_item_price();
				$name = usam_cart_item_name();
				$product_id = usam_cart_item_product_id();
				$quantity = usam_cart_item_quantity();
				$brand = usam_get_product_brand_name( $product_id  );
				$category = usam_get_product_category_name( $product_id );	
				
				$output .= "ga('ec:addProduct',{'id':'$product_id','name':'$name','price':'$price','category':'$category','brand':'$brand','quantity':'$quantity'});";
				$output .= "ga('ec:setAction', 'checkout');";	
			}		
		}		
		return $output;
	}	
	
	private static function add_pushes( ) 
	{
		$output = "";			
		$document_payment = usam_get_payment_document( $_GET['payment_number'], 'document_number' );	
		
		$purchase_log = new USAM_Order( $document_payment['document_id'] );
		$order = $purchase_log->get_data();
		if ( empty($order) )
			return '';
		
		$customer_data = $purchase_log->get_customer_data();
		$cart_contents = $purchase_log->get_order_products();			

		foreach( $cart_contents as $item ) 
		{
			$product = array();
			$category = usam_get_product_category_name( $item->product_id );
			$brand = usam_get_product_brand_name( $item->product_id );
			
			$output .= "ga('ec:addProduct',{'id':'$item->product_id','name':'$item->name','category':'$category','brand':'$brand','price':'$item->price','quantity':'$item->quantity'});\n\r";
		}		
		$output .= "			
			ga('ec:setAction', 'purchase', { 
			  'id': '".$order['id']."',             
			  'affiliation': '".get_bloginfo('name')."', 
			  'revenue': '".$order['totalprice']."',                    
			  'tax': '".$order['total_tax']."',                          
			  'shipping': '".$order['shipping']."',                     
			  'coupon': '".$order['coupon_name']."' 
		});";
		return $output;
	}
	
	private static function product_display(  ) 
	{	
		global $post;	
		
		$list = __('Просмотр товара','usam');
		
		$post_title = get_the_title( $post->ID );
		$brand = usam_get_product_brand_name( $post->ID );
		$category = usam_get_product_category_name( $post->ID );	
		$price = usam_get_product_price( $post->ID );	
		
		$output = ''; 
		$output .= "ga('ec:addProduct',{'id':'$post->ID','name':'$post_title','price':'$price','category':'$category','brand':'$brand'});";
		$output .= "ga('ec:setAction', 'detail');";		
		return $output;
	}
}
?>