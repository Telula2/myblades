<?php
class USAM_SEO_Link_Analysis 
{	
	public $result;
	public function __construct( $url ) 
	{
		require_once( USAM_FILE_PATH . '/includes/simple_html_dom.php' ); 	
		
		if ( !empty($url) )
			$this->result = $this->analysis( $url );
		
	}
	
	public function get_results ( ) 
	{
		return $this->result;
	}
	
	public function analysis ( $url ) 
	{
		$URL_OBJ = usam_get_url_object( $url );		
		$result = false;
		if( $URL_OBJ )
		{ 
			$content = preg_replace('#(<script.*</script>)#sU', '', $URL_OBJ['content']);
			$content = preg_replace('#(<style.*</style>)#sU', '', $content);	
			
			$dom = new DOMDocument();				
			@$dom->loadHTML( $content );
			$body = $dom->getElementsByTagName('body')->item(0)->nodeValue;		
			$tags = array( 'h1', 'h2', 'h3', 'h4' );
			$keywords = array();
			foreach ($tags as $tag) 
			{
				foreach ($dom->getElementsByTagName($tag) as $node) 
				{
					$str = $node->nodeValue;	
					$result['tag']['header'][$tag][] = $str;	
					$words = explode(' ', $str);
					$keywords = array_merge ($keywords, $words);			
				}
			}				
			$str = $dom->getElementsByTagName('title')->item(0)->nodeValue;	
			$result['tag']['seo']['title'] = $str;	
			$words = explode(' ', $str);
			$keywords = array_merge ($keywords, $words);

			$result['meta_tags'] = get_meta_tags( $url );		
			
			$words = preg_split('#[\s,]+#', $body);		
			$result['keywords'] = array();
		/*	foreach ($words as $word) 	
			{
				if ( in_array($word, $keywords) )
				{
					$key = array_search($word, $keywords);
					
					if ( isset($result['keywords'][$key]) )
						$result['keywords'][$key]['occurrence']++;
					else
						$result['keywords'][$key] = array( 'name' => $word, 'occurrence' => 1, 'tag' => '' );
				}
			}*/
			$buf = $words;
			$keywords = array();
			$max = 0;
			$exclude_words = array ( 'и', 'в', 'без', 'до', 'из', 'к', 'на', 'по', 'о', 'от', 'перед', 'при', 'через', 'с', 'у', 'за', 'над', 'об', 'под', 'про', 'для', 'руб' );
			foreach ($words as $word) 	
			{
				$word = preg_replace('/[^a-zA-Zа-яА-Я]/ui', '',$word ); 
				$strtolower = mb_strtolower($word);			
				if ( !empty($strtolower) && in_array($word, $buf) && !in_array($strtolower, $exclude_words) )
				{
					$key = array_search($word, $buf);					
					if ( isset($keywords[$key]) )
					{
						$keywords[$key]['occurrence']++;
						if ( $max < $keywords[$key]['occurrence'] )
							$max = $keywords[$key]['occurrence'];
					}
					else
					{
						$in_tags = array();
						foreach ($result['tag']['header'] as $tag => $tags) 
						{
							foreach ( $tags as $tag_title ) 
							{
								if( stristr($tag_title, $word) !== false ) 								
								{
									$in_tags[] = $tag;
								}
							}
						}
						foreach ($result['tag']['seo'] as $tag => $title) 
						{
							if( stristr($title, $word) !== false ) 								
							{
								$in_tags[] = $tag;
							}	
						}
						$keywords[$key] = array( 'name' => $word, 'occurrence' => 1, 'tag' => $in_tags );
					}
				}
			}
			$count = count($keywords);
		//	$average = round($max / 3);
			foreach ($keywords as $key => &$keyword) 	
			{
				$keyword['weight'] = round($keyword['occurrence']/$count*100,2);
				
				//if ( $keyword['occurrence'] < $average )
					//unset( $keywords[$key] );				
			}						
			usort($keywords, function($a, $b){  return ($b['occurrence'] - $a['occurrence']); });
			
			$result['words'] = $keywords;			
		
		}	
		return $result;		
	}
}