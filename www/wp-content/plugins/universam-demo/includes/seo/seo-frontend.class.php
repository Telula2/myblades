<?php
new USAM_SEO_Frontend();
class USAM_SEO_Frontend
{
	private $is_analytics_disabled = false;
	private $is_theme_tracking     = false;
	private $advanced_code         = false;
	private $tracking_id           = '';

	public function __construct()
	{	
		add_action( 'wp_head', array( $this, 'head' ), 1 );		
	}

	public function head( ) 
	{
		$this->webmaster_tools_authentication();		
		global $post;			
		if ( empty($post) || ( $post->post_name != 'your-account' && $post->post_name != 'login') )
		{	
			if ( !current_user_can('manage_options') )
			{ 
				if ( (bool) get_option( 'usam_google_analytics_active', false ) )
				{		
					require_once( USAM_FILE_PATH . '/includes/seo/google/analytics.class.php' );
					USAM_Google_Analytics::print_script();
				}
				if ( (bool) get_option( 'usam_yandex_metrika_active', false ) )
				{
					require_once( USAM_FILE_PATH . '/includes/seo/yandex/metrika_counter.class.php' );
					USAM_Yandex_Metrika_Counter::print_script();
				}
			}
		}
	}
	
	public function webmaster_tools_authentication( ) 
	{			
		$bing = get_option('usam_bing');	
		if ( !empty($bing['verify']) ) {
			echo '<meta name="msvalidate.01" content="' . esc_attr( $bing['verify'] ) . "\" />\n";
		}		
		$google = get_option('usam_google');		
		if ( !empty($google['verify']) ) {
			echo '<meta name="google-site-verification" content="' . esc_attr( $google['verify'] ) . "\" />\n";
		}
		$pinterest = get_option('usam_pinterest');
		if ( !empty($pinterest['verify']) ) {
			echo '<meta name="p:domain_verify" content="' . esc_attr( $pinterest['verify'] ) . "\" />\n";
		}
		$yandex = get_option('usam_yandex');
		if ( !empty($yandex['webmaster']['verify']) ) {
			echo '<meta name="yandex-verification" content="' . esc_attr( $yandex['webmaster']['verify'] ) . "\" />\n";
		}
	}
}