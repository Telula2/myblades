<?php 
new USAM_Determining_Position_Site();
class USAM_Determining_Position_Site
{
	private $setting;	
	private $host;	
	private $errors = array();	
	private $competitor_count = 10;
	private $site_id = 0;
	private $competitor = false;	
		
	public function __construct(  )
	{			
		add_action( 'usam_weekly_cron_task', array( $this, 'delete_sites'), 30 );			
	} 	
	
	public function get_format( )
	{
		$format = array( 'id' => '%d', 'keyword_id' => '%d', 'location_id' => '%d', 'search_engine' => '%s', 'number' => '%d', 'date_insert' => '%s' );		
		return $format;
	}
	
	private function set_error( $error )
	{ 	
		$this->errors[] = sprintf( __('Проверка позиции сайта. Ошибка: %s'), $error );
	}
	
	function get_error_message(  )
	{
		return $this->errors;
	}
	
	protected function set_log_file( )
	{
		usam_log_file( $this->errors );
	}
	
	public function get_format_data( $data )
	{
		$format = $this->get_format();		
		
		$update_format = array();
		foreach ($data as $key => $value )
		{	
			if ( isset($format[$key]) )
			{
				$update_format[] = $format[$key];
			}
		}		
		return $update_format;
	}
		
	public function insert_competitor( $domain, $data )
	{
		if ( $this->competitor == false )
			return false; 
		
		$site = new USAM_Site( $domain, 'domain' );
		$site_data = $site->get_data();			
		if ( empty($site_data) )
		{
			$new_site = array( 'domain' => $domain, 'type' => 'C' );
			$site = new USAM_Site( $new_site );			
			$site->save();	
			$data['site_id'] = $site->get('id');
		}
		else
			$data['site_id'] = $site_data['id'];
		$this->insert_statistic( $data );	
	}
		
	public function insert_statistic( $data )
	{				
		global $wpdb;
		
		
			return false;
		
	}
	
	public function delete_sites()
	{				
		global $wpdb;
		$date = date( 'Y-m-d',strtotime("-1 month", time() ));	
		$site_ids = $wpdb->get_col("SELECT site_id FROM `".USAM_TABLE_STATISTICS_KEYWORDS."` WHERE `date_insert`>='{$date}'");
		
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SITES." WHERE id NOT IN (".implode( ', ', $site_ids ).")");
	}
	
	public function save_position_site( $site_id = 0, $competitor = true )
	{				
		
		
		return true;
	}	
	
	public function get_yandex_position( $keyword, $location )
	{	
		if ( $this->check_ability_request( 'usam_yandex_suspicious_traffic' ) == false ) 	
			return false;
		
		$yandex = get_option('usam_yandex');		
		$yandex_authorization = $yandex['xml'];				
		$pages = 1;	
		$n = 0;	
		
		$query_esc = htmlspecialchars( $keyword->keyword ); // текст поискового запроса
		$group_page = 100; //количество групп на одной странице
					
		$page  = 1;
		$url = '';
		$number = false;
		$is_error = false;				
		while ( $page <= $pages )
		{								
			$params = array(
				'user' => $yandex_authorization['username'], // логин
				'key' => $yandex_authorization['password'], // ключ Яндекс.XML
				'filter' => 'none',
				'groupby' => 'attr=d.mode=deep.groups-on-page='.$group_page, // attr=<служебный атрибут>.mode=<тип группировки>.groups-on-page=<количество групп на одной странице>.docs-in-group=<количество документов в каждой группе>
				'query' => urlencode($query_esc),
				'lr' => $location->code
			);					
			$request_link = "https://yandex.ru/search/xml" . '?' . urldecode(http_build_query($params). '<<host="'.$this->host.'"');
			$response = @file_get_contents($request_link);	
			if ( $response ) 
			{ 
				$xmldoc = new SimpleXMLElement( $response );				
				$xmlresponce = $xmldoc->response;
				if ( $xmlresponce->error ) 
				{
					$is_error = true;						
					if( strpos($response, 'Лимит запросов исчерпан') !== false ) 
					{
						$min = (int)date('i');							
						$time = time() + 60*(60-$min)+1;
						set_transient( 'usam_yandex_suspicious_traffic', $time, HOUR_IN_SECONDS );
						$this->set_error( __( "Яндекс. Лимит запросов исчерпан. Увеличьте количество запросов или подождите...", "usam") );
					}
					else
					{
						$error = $xmldoc->xpath('/yandexsearch/response/results/error');
						$this->set_error( $error );
					}				
					return false;
				}    
				$pos = 1;
			//	$domains = $xmldoc->xpath('/yandexsearch/response/results/grouping/group/doc/domain');	
				$links = $xmldoc->xpath('/yandexsearch/response/results/grouping/group/doc/url');					
				foreach ( $links as $link ) 
				{									
					$domain = mb_strtolower(parse_url($link, PHP_URL_HOST));	
					$domain = str_replace("www.","",$domain);					
					$n++;					
					if ( $this->host == $domain )
					{							
						$url = $link;
						$number = $pos + ( $page - 1 ) * $group_page;	
					}          
					elseif ( $this->competitor_count >= $n )
					{
						$k = $pos + ( $page - 1 ) * $group_page;	
						$this->insert_competitor( $domain, array('keyword_id' => $keyword->id, 'search_engine' => $location->search_engine, 'location_id' => $location->location_id, 'number' => $k, 'url' => $link) );
					}
					if ( $this->competitor_count < $n && $number )
						break 2;
					$pos++;
				}							
			} 
			else 
			{
				$is_error = true;		
				$this->set_error( __( "Яндекс. Внутренняя ошибка сервера", "usam") );								
				break;
			}	
			$page++;					
		}		
		if ( $is_error == false && $number == false )
			$number = 100;
		
		if ( $number )		
		{
			$this->insert_statistic( array('keyword_id' => $keyword->id, 'search_engine' => $location->search_engine, 'location_id' => $location->location_id, 'number' => $number, 'url' => $url, 'site_id' => $this->site_id ) );	
		}
		return $number;	
	}	
	
	// https://www.searchengines.ru/regionalnost-v-google-get-parametr-uule.html
	private function get_google_secret_key( $number )
	{
		$secretkey = array( 4 => 'E', 5 => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J', 10 => 'K', 11 => 'L', 12 => 'M', 13 => 'N', 14 => 'O', 15 => 'P', 16 => 'Q', 17 => 'R', 18 => 'S', 19 => 'T', 20 => 'U', 21 => 'V', 22 => 'W', 23 => 'X', 24 => 'Y', 25 => 'Z', 26 => 'a', 27 => 'b', 28 => 'c', 29 => 'd', 30 => 'e', 31 => 'f', 32 => 'g', 33 => 'h', 34 => 'i', 35 => 'j', 36 => 'k', 37 => 'l', 38 => 'm', 39 => 'n', 40 => 'o', 41 => 'p', 42 => 'q', 43 => 'r', 44 => 's', 45 => 't', 46 => 'u', 47 => 'v', 48 => 'w', 49 => 'x', 50 => 'y', 51 => 'z', 52 => '0', 53 => '1', 54 => '2', 55 => '3', 56 => '4', 57 => '5', 58 => '6', 59 => '7', 60 => '8', 61 => '9', 62 => '--', 63 => '-', 64 => 'A', 65 => 'B', 66 => 'C', 67 => 'D', 68 => 'E', 69 => 'F', 70 => 'G', 71 => 'H', 72 => 'I', 73 => 'J', 76 => 'M', 83 => 'T', 89 => 'L' );
		
		return isset($secretkey[$number])?$secretkey[$number]:'';		
	}
	
	function check_ability_request( $key )
	{
		if ( $time = get_transient( $key ) ) 
		{
			if ( $time < time() ) 						
				delete_transient( $key );
			else
				return false;
		}			
		return true;
	}
		
	function get_google_position( $keyword, $location )
	{			
		if ( $this->check_ability_request( 'usam_google_suspicious_traffic' ) == false ) 	
			return false;
		
		$location_id = 0;
		$page = 0;		
		$number = false;	
		$url = '';
		$is_error = false;
		
		$uule = "w+CAIQICI".$this->get_google_secret_key(mb_strlen($location->name)).base64_encode( $location->name ); 
		$request_link = 'https://www.google.ru/search?q='.urlencode($keyword->keyword)."&uule=$uule&num=100&hl=ru&start=$page&ie=UTF-8";
		$opts = array(
			"ssl" => array(
				"verify_peer" => false,
				"verify_peer_name" => false,
			),
			'http' => array(
				'method'=>"GET",
				'header'=> "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36\r\n"
			)
		);
		$context = stream_context_create($opts);	
		
		if ( version_compare( PHP_VERSION, '5.6.0', '<' ) ) 		
			$webcontent = @file_get_contents($request_link, false, $context);	
		else
		{
			$ch = curl_init();
			$user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
			curl_setopt($ch, CURLOPT_URL, $request_link);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_AUTOREFERER, false);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);

			curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSLVERSION,CURL_SSLVERSION_DEFAULT);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$webcontent = curl_exec ($ch);
			$error = curl_error($ch); 
			curl_close ($ch);
		}		
		if( $webcontent )
		{						
			//$webcontent = iconv('windows-1251', 'utf-8', $webcontent);	
			if( preg_match_all('/<h3 class="r"><a href="(.+?)"/is', $webcontent, $match) )
			{				
				foreach($match[1] as $n => $link)
				{ 								
					$domain = parse_url($link, PHP_URL_HOST);	
					$domain = str_replace("www.","",$domain);
					if( $domain == $this->host )			
					{							
						$number = $n+1;
						$url = $link;
					}
					elseif ( $this->competitor_count > $n )
					{
						$k = $n+1;
						$this->insert_competitor( $domain, array('keyword_id' => $keyword->id, 'search_engine' => $location->search_engine, 'location_id' => $location->location_id, 'number' => $k, 'url' => $link) );
					}
					if ( $this->competitor_count < $n && $number )
						break;
				}
			}	
			else
			{ 
				$is_error = true;
				$this->set_error( __( "Google зарегистрировал подозрительный трафик, исходящий из вашей сети.", "usam") );
				$time = time() + 3600;
				set_transient( 'usam_google_suspicious_traffic', $time, HOUR_IN_SECONDS );
			}				
		}	
		else
		{			
			$is_error = true;
			$this->set_error( __( "Google. Внутренняя ошибка сервера", "usam") );
		}	
		if ( $is_error == false && $number == false )
			$number = 100;

		if ( $number )
		{			
			$this->insert_statistic( array('keyword_id' => $keyword->id, 'search_engine' => $location->search_engine, 'location_id' => $location->location_id, 'number' => $number, 'url' => $url, 'site_id' => $this->site_id) );
		} 		
		return $number;	
	}	
}


function usam_query_position_site()
{		
	$class = new USAM_Determining_Position_Site();
	$result = $class->save_position_site( );			
	if ( $result == false )		
	{
		if ( !get_transient( 'usam_start_query_position_site' ) ) 
			set_transient( 'usam_start_query_position_site', true, DAY_IN_SECONDS );
	}
	else
		delete_transient( 'usam_start_query_position_site' );
}
if ( get_option( 'usam_check_position_site', 0 ) && !LOCALHOST )
{
	add_action( 'usam_cron_task_day', 'usam_query_position_site', 30 );	
	if ( get_transient( 'usam_start_query_position_site' ) ) 
		add_action( 'usam_five_minutes_cron_task', 'usam_query_position_site', 30 );	
}
?>