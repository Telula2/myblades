<?php
/**
 * Передача данных яндекса
 */
require_once( USAM_FILE_PATH . '/includes/seo/yandex/yandex.class.php' );

class USAM_Yandex_Market extends USAM_Yandex
{	
	protected $version = 'v2';
	protected $url_api = 'https://api.partner.market.yandex.ru';
	
	function get_region( $params )
	{	
		$result = $this->send_request( "regions", $params );		
		if ( !empty($result['regions']) )
			return $result['regions'];
		
		return array();
	}	
}
?>