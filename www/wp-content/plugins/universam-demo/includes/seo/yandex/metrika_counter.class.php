<?php
/**
 * Передача данных яндекса
 */
class USAM_Yandex_Metrika_Counter 
{		
	private static $metrika;
	
	public static function print_script( ) 
	{		
		global $post;
		
		$yandex = get_option('usam_yandex');	
		if ( empty($yandex['metrika']['counter_id']) )
			return;			
	
		self::$metrika = $yandex['metrika'];		
		?>
		<script type="text/javascript">
			window.dataLayer = window.dataLayer || [];		
		</script>	
		<?php 		
		
		if ( !empty(self::$metrika['asynchronous']) )
			self::asynchronous_code();
		else
			self::synchronous_code();
		
		if ( !empty(self::$metrika['ecommerce']) && usam_is_product() )
		{ 		
			$category_list = get_the_terms( $post->ID, 'usam-category' );
			$category_name = array();
			if ( !empty($category_list) )
				foreach ( $category_list as $category )
					$category_name[] = $category->name;		
				
			$brand = usam_get_product_brand_name( $post->ID );				
			?>	
			<script type="text/javascript">
			dataLayer.push({
			  'ecommerce' : {
				'detail' : {
				  'products' : [{'name':'<?php echo get_the_title() ?>','id':'<?php echo usam_get_product_meta($post->ID, 'sku') ?>','price' : '<?php echo usam_get_product_price( $post->ID ); ?>','brand' :  '<?php echo $brand; ?>','category' : '<?php echo implode( ',', $category_name); ?>',}]
				}
			  }
			});
		<?php } ?>				
		</script>		
		<?php
	}	
	
	private static function asynchronous_code( ) 
	{		
		?>
		<script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
		<script type="text/javascript" >
		try {
			var yaCounter = new Ya.Metrika({
				id:<?php echo self::$metrika['counter_id']; ?>,
				<?php echo empty(self::$metrika['webvisor'])?'':'webvisor:true,'; ?>								
				clickmap:true,
				trackLinks:true,
				accurateTrackBounce:true,	
				<?php echo empty(self::$metrika['ecommerce'])?'':'ecommerce:"dataLayer"'; ?>	
			});
		} catch(e) { }
		</script>	
		<?php
	}
	
	private static function synchronous_code( ) 
	{	
		?>	
		<script type="text/javascript" >
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter = new Ya.Metrika({
							id:<?php echo self::$metrika['counter_id']; ?>,
							<?php echo empty(self::$metrika['webvisor'])?'':'webvisor:true,'; ?>								
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,	
							<?php echo empty(self::$metrika['ecommerce'])?'':'ecommerce:"dataLayer"'; ?>	
						});
				} catch(e) { }
			});		
			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks"); 
		</script>	
		<?php
	}
	
	private static function metrika2( ) 
	{		
		?>		
		<script type="text/javascript" >					
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter = new Ya.Metrika2({
							id:<?php echo self::$metrika['counter_id']; ?>,
							<?php echo empty(self::$metrika['webvisor'])?'':'webvisor:true,'; ?>								
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,	
							<?php echo empty(self::$metrika['ecommerce'])?'':'ecommerce:"dataLayer"'; ?>	
						});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/tag.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks2");		
		</script>	
		<?php
	}
}
?>