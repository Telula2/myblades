<?php
class USAM_Yandex
{
	protected $token;
	protected $client_id;
	protected $errors = array();
	protected $format = 'json';
	protected $url_api = '';
	protected $version = 'v1';
	
	public function __construct()
	{
		$yandex = get_option( 'usam_yandex' );		
		$this->token = isset($yandex['token'])?$yandex['token']:'';
		$this->client_id = isset($yandex['client_id'])?$yandex['client_id']:'';
	}
	
	public function is_token( )
	{	
		return !empty($this->token)?true:false;
	}
	
	protected function set_error( $error )
	{	
		$this->error = $error;
		$this->errors[] = sprintf( __('Запрос на yandex вызвал ошибку №%s. Текст ошибки: %s'), $error['error_code'], $error['error_message']);
	}
	
	public function get_errors( )
	{	
		return $this->errors;
	}
	
	protected function set_log_file( )
	{	
		usam_log_file( $this->errors );
		$this->errors = array();
	}
	
	protected function get_url( $resource )
	{
		return "$this->url_api/$this->version/{$resource}.{$this->format}";
	}

	protected function send_request( $resource, $params = array() )
	{	
		$params['oauth_token'] = $this->token;
		$params['oauth_client_id'] = $this->client_id;
		
		$headers["Accept"] = 'application/json';
		$headers["Content-type"] = 'application/json';		
		
		$url_api = $this->get_url( $resource );			
		$data = wp_remote_get( $url_api, array('sslverify' => false, 'body' => $params, 'headers' => $headers ));	
		
		if ( is_wp_error($data) )
			return $data->get_error_message();
		$resp = json_decode($data['body'], true);	
		if ( isset( $resp['errors'] ) ) 
		{			
			if ( isset($resp['errors']['message']) )
				$this->set_error( $resp['errors']['message'] );	
			return false;
		}		
		return $resp;		
	}
}
?>