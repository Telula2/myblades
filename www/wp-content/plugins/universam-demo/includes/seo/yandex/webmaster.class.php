<?php
require_once( USAM_FILE_PATH . '/includes/seo/yandex/yandex.class.php' );
class USAM_Yandex_Webmaster extends USAM_Yandex
{
	private $user_id;
	
	public function __construct()
	{
		$yandex = get_option( 'usam_yandex' );				
		$this->user_id = empty($yandex['webmaster']['user_id'])?'':$yandex['webmaster']['user_id'];
		$this->site_id = empty($yandex['webmaster']['site_id'])?'':$yandex['webmaster']['site_id'];
		$this->url_api = "https://api.webmaster.yandex.net/v3/user/";
		
		parent::__construct( );
	}
	
	public function ready( )
	{
		if ( $this->is_token() && $this->user_id && $this->site_id )
			return true;
		return false;
	}
	
	public function get_user( )
	{			
		$result = $this->send_request( );
		
		$user_id = 0;		
		if ( !empty($result['user_id']) )
			$user_id = $result['user_id'];
		return $user_id;
	}
	
	public function get_sites( $user_id )
	{		
		$resource = "{$user_id}/hosts/";		
		$result = $this->send_request( $resource );
		
		$hosts = array();	
		if ( !empty($result['hosts']) )
			$hosts = $result['hosts'];
		return $hosts;
	}		
	
	//Позволяет получить информацию о текущем состоянии индексирования сайта.
	public function get_info_site( )
	{				
		$resource = "{$this->user_id}/hosts/".$this->site_id;	
		$result = $this->send_request( $resource );	
		return $result;
	}	
	
	//Получение статистики сайта
	public function get_statistics_site( )
	{				
		$resource = "{$this->user_id}/hosts/{$this->site_id}/summary";	
		$result = $this->send_request( $resource );	
		return $result;
	}		

	// Получение истории индексирования сайта
	/*
	SEARCHABLE	    Страницы в поиске.
	DOWNLOADED	    Загруженные страницы.
	DOWNLOADED_2XX	Страницы, загруженные с кодом из группы 2xx.
	DOWNLOADED_3XX	Страницы, загруженные с кодом из группы 3xx.
	DOWNLOADED_4XX	Страницы, загруженные с кодом из группы 4xx.
	DOWNLOADED_5XX	Страницы, загруженные с кодом из группы 5xx.
	FAILED_TO_DOWNLOAD	Не удалось загрузить.
	EXCLUDED	     Исключенные страницы.
	EXCLUDED_DISALLOWED_BY_USER	Исключенные по желанию владельца ресурса (4xx-коды, запрет в robots.txt).
	EXCLUDED_SITE_ERROR	Исключенные из-за ошибки на стороне сайта.
	EXCLUDED_NOT_SUPPORTED	Исключенные из-за отсутствия поддержки на стороне роботов Яндекса.
	*/
	public function get_indexing( $date_from, $date_to, $indicators = array('SEARCHABLE'))
	{				
		$date_from = date( "c", strtotime($date_to) );
		$date_to   = date( "c", strtotime($date_to) );
		
		$resource = "{$this->user_id}/hosts/{$this->site_id}/indexing-history";	
		$result = $this->send_request( $resource, array('date_from' => $date_from, 'date_to' => $date_to, 'indexing_indicator' => $indicators) );	
		return $result;
	}		
	
	//Получение информации о популярных запросах
	/*
	* @param $orderBy string ordering: TOTAL_CLICKS|TOTAL_SHOWS
    * @param $indicators array('TOTAL_SHOWS','TOTAL_CLICKS','AVG_SHOW_POSITION','AVG_CLICK_POSITION')
	*/
	public function get_popular( $order_by = 'TOTAL_SHOWS', $indicators = array() )
	{				
		$order_by = $order_by == 'TOTAL_SHOWS'?'TOTAL_SHOWS':'TOTAL_CLICKS';

		$resource = "{$this->user_id}/hosts/{$this->site_id}/search-queries/popular/";		
		$result = $this->send_request( $resource, array('order_by' => $order_by, 'query_indicator' => $indicators) );	
		
		if ( !empty($result['queries']) )
			return $result['queries'];		
		
		return array();
	}	
	
	//Позволяет получить примеры внешних ссылок на страницы сайта.	
	public function get_external( $offset = 0, $limit = 10 )
	{				
		$resource = "{$this->user_id}/hosts/{$this->site_id}/links/external/samples/";		
		$result = $this->send_request( $resource, array('offset' => $offset, 'limit' => $limit) );			
		return $result;
	}	
	
	//Добавление оригинального текста.	
	public function add_original_text( $content )
	{				
		$content = substr($content, 0, 4);
		$resource = "{$this->user_id}/hosts/{$this->site_id}/original-texts/";		
		$result = $this->send_request( $resource, array('content' => $content) );			
		return $result;
	}
	
	private function dataToString($data)
    {
        $queryString = array();
        foreach ($data as $param => $value) 
		{
            if (is_string($value) || is_int($value) || is_float($value))
                $queryString[] = urlencode($param) . '=' . urlencode($value);
            elseif (is_array($value))
			{
                foreach ($value as $valueItem) 
				{
                    $queryString[] = urlencode($param) . '=' . urlencode($valueItem);
                }
            } 
			else 
			{
                $this->errors[] = "Bad type of key {$param}. Value must be string or array";
                continue;
            }
        }

        return implode('&', $queryString);
    }
		
	function send_request( $resource = '', $params = array() )
	{		
		$url = $this->url_api. $resource. '?' . $this->dataToString($params);
		$headers["Authorization"] = 'OAuth ' . $this->token;
		$headers["Accept"] = 'application/json';
		$headers["Content-type"] = 'application/json';
		$data = wp_remote_get( $url, array('sslverify' => false, 'headers' => $headers ));	
		
		if (is_wp_error($data))
			return $data->get_error_message();
		
		$resp = json_decode($data['body'], true); 
		if ( isset( $resp['error_code'] ) ) 
		{			
			$this->set_error( $resp );	
			return false;
		}		
		return $resp;		
	}
}
?>