<?php
/**
 * Передача данных яндекса
 */
require_once( USAM_FILE_PATH . '/includes/seo/yandex/yandex.class.php' );

class USAM_Yandex_Metrika extends USAM_Yandex
{	
	protected $version = 'v1';
	protected $url_api = 'https://api-metrika.yandex.ru/management';
	protected $counter_id;

	public function __construct()
	{
		$yandex = get_option( 'usam_yandex' );				
		$this->counter_id = !empty($yandex['metrika']['counter_id'])?$yandex['metrika']['counter_id']:0;			
		parent::__construct( );
	}
	
	function get_counters( $params = array() )
	{		 
		$this->url_api = 'https://api-metrika.yandex.ru/management';
		
		$result = $this->send_request( "counters", $params );
		if ( !empty($result['counters']) )
			return $result['counters'];
		
		return array();
	}
	
	//metrics=ym:s:visits,ym:s:pageviews&dimensions=ym:s:referer,ym:s:startURLDomain&date1=2015-01-01&date2=yesterday&limit=10000&offset=1&ids=2138128,2215573
	//	metrics=ym:s:visits,ym:s:pageviews&dimensions=ym:s:lastTrafficSource&date1=31daysAgo&date2=yesterday&limit=10000&offset=1&ids=2138128
		
/*Основные из метрик:
ym:s:visits — количество визитов
ym:s:pageviews — суммарное количество просмотров страниц
ym:s:users — количество уникальных посетителей (за отчетный период)
ym:s:bounceRate — показатель отказов
ym:s:pageDepth — глубина просмотра
ym:s:avgVisitDurationSeconds — средняя продолжительность визитов в секундах
ym:s:goal<goal_id>reaches — количество достижений цели, где вместо надо подставить идентификатор цели (зайдите в настройки целей для конкретного счетчика, чтобы получить список их идентификаторов)
ym:s:sumGoalReachesAny — суммарное количество достижений всех целей
*/
	public function get_statistics( $args )
	{	
		if ( !$this->auth() )
			return array();
	
		$this->url_api = 'https://api-metrika.yandex.ru/stat';
		
		$params = array(		
			'id' => $this->counter_id,
			'preset' => "traffic",				
			//'metrics' => "ym:s:visits,ym:s:pageviews,ym:s:users,ym:s:avgVisitDurationSeconds",
			'date1' => date('Y-m-d', strtotime($args['date1'])),
			'date2' => "yesterday",		
		//	'filters' => "m:pv:datePeriodday"			
		);				
		if ( isset($args['group']) )
			$params['group'] = $args['group'];		//day, week, month, quarter, year			
		if ( isset($args['offset ']) )
			$params['offset '] = $args['offset '];
		if ( isset($args['limit']) )
			$params['limit'] = $args['limit'];
		
		$result = $this->send_request( "data", $params );	
		if ( empty($result) )
			return array();
	
		$keys = array();
		foreach ( $result['query']['metrics'] as $key => $value )
		{
			switch ( $value ) 
			{
				case 'ym:s:visits':
					$keys[$key] = 'visits';
				break;
				case 'ym:s:users':
					$keys[$key] = 'users';
				break;
				case 'ym:s:pageviews': //суммарное количество просмотров страниц
					$keys[$key] = 'page_views';
				break;
				case 'ym:s:percentNewVisitors':
					$keys[$key] = 'new_visitors';
				break;
				case 'ym:s:bounceRate':
					$keys[$key] = 'bounce_rate';
				break;	
				case 'ym:s:pageDepth':
					$keys[$key] = 'page_depth';
				break;	
				case 'ym:s:avgVisitDurationSeconds':
					$keys[$key] = 'visit_duration';
				break;	
			}		
		}
		$data = array();
		foreach ( $result['data'] as $value )
		{			
			$statistic = array();
			foreach ( $value['metrics'] as $key => $metric )
			{
				$statistic[$keys[$key]] = $metric;
			
			}			
			$data[] = array( 'dimensions' => $value['dimensions'], 'statistic' => $statistic );
		}	
		return $data;
	}
	
	public function auth( )
	{
		if ( $this->is_token() && $this->counter_id )
			return true;
		return false;
	}
}
?>