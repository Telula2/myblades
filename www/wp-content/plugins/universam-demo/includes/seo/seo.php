<?php
function usam_get_search_engine_regions( $qv = array() )
{ 
	global $wpdb;
	if ( isset($qv['fields']) )
	{
		$fields = $qv['fields'] == 'all'?'*':$qv['fields'];
	}
	else		
	{
		$qv['fields'] = 'all';
		$fields = '*';
	}		
	if ( !isset($qv['active']) || $qv['active'] == '1')
		$_where[] = "active = '1'";
	elseif ( $qv['active'] == '0' )
		$_where[] = "active = '0'";		
	
	$_where[] = '1=1';		
	if ( isset($qv['location_id']) )
		$_where[] = "location_id IN( '".implode( "','", $qv['location_id'] )."' )";
	
	if ( isset($qv['code']) )
		$_where[] = "code = '".$qv['code']."'";
	
	if ( isset($qv['search_engine']) )
		$_where[] = "search_engine = '".$qv['search_engine']."'";
	
	if ( isset($qv['name']) )
		$_where[] = "name = '".$qv['name']."'";	
	
	if ( isset($qv['include']) )
		$_where[] = "id IN( '".implode( "','", $qv['include'] )."' )";
	
	$where = implode( " AND ", $_where);		
	if ( isset($qv['orderby']) )	
		$orderby = $qv['orderby'];	
	else
		$orderby = 'sort';
	$orderby = "ORDER BY $orderby";
	
	if ( isset($qv['order']) )	
		$order = $qv['order'];	
	else
		$order = 'ASC';	
	
	if ( empty( $qv['paged'] ) ) 
		$qv['paged'] = 1;	
	
	$limit = '';
	if ( isset( $qv['number'] ) && $qv['number'] > 0 ) 
	{
		if ( isset($qv['offset']) )
			$limit = $wpdb->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
		else 
			$limit = $wpdb->prepare( "LIMIT %d, %d", $qv['number'] * ( $qv['paged'] - 1 ), $qv['number'] );
	}	
	$sql = "SELECT $fields FROM ".USAM_TABLE_SEARCH_ENGINE_REGIONS." WHERE $where $orderby $order $limit";
	if ( is_array($qv['fields']) || 'all' == $qv['fields'] )
	{
		$results = $wpdb->get_results( $sql );
	} 		
	else 
	{
		$results = $wpdb->get_col( $sql );
	}
	return $results;
}

/* ��� ������� ����� ���������, �������� �� ���������� ������� ��������� ������� */
function usam_is_bot( )
{
	$bots = usam_get_site_bots();
	$agent = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
	foreach($bots as $bot)
	{
		if( stripos($agent, $bot) !== false )
			return true;
	}
	return false;
}

function usam_get_bot( )
{
	$bots = usam_get_site_bots();
	$agent = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
	foreach($bots as $bot)
	{
		if( stripos($agent, $bot) !== false )
			return $bot;
	}
	return false;
}

function usam_insert_keywords( $keywords )
{
	global $wpdb;	
	foreach($keywords as $word)
	{
		if ( !empty($word) )
			$wpdb->insert( USAM_TABLE_KEYWORDS, array( 'keyword' => $word ), array('%s') );
	}
}