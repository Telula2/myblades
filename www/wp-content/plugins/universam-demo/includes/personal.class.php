<?php
/**
 * Файл содержит функции и классы для управления сотрудниками компании
 */
 
 
function usam_get_manager_signature_email( $userid = null )
{
	if ( $userid == null )
	{
		global $user_ID;
		$userid = $user_ID;
	}
	$signature_email = get_the_author_meta('usam_signature_email', $userid);		
	return $signature_email;
}
?>