<?php
/**
 * Обработка событий
 * @since 3.7
 */		

function usam_get_system_process( $args = array() ) 
{
	$option = get_option( 'usam_set_events', '' );		
	if ( empty($option))
		$events = array();
	else
		$events = unserialize( $option );
	
	if ( !empty($args) )
	{
		$results = array();
		foreach ( $events as $id => $event )
		{  
			if ( isset($args['id_like']) && stripos($id, $args['id_like'] ) !== false )
			{
				$results[$id] = $event;			
			}	
		}
		return $results;
	}
	return $events;
}

function usam_delete_system_process( $id ) 
{
	$events = usam_get_system_process( );
	if ( isset($events[$id]) )
	{
		unset($events[$id]);
		update_option( 'usam_set_events', serialize($events) );
	}	
}

function usam_create_system_process( $title, $data, $callback, $count, $id, $priority = 100 ) 
{		
	$events = usam_get_system_process( );
	
	$events[$id] = array( 'priority' => $priority, 'title' => $title, 'data' => $data, 'callback' => $callback, 'count' => $count, 'done' => 0, 'status' => false, 'date_insert' => date("Y-m-d H:i:s" ) );	
	update_option( 'usam_set_events', serialize($events) );
} 

function usam_system_process( ) 
{
	$events = new USAM_System_Process();
	$events->start_events();	
}
add_action('usam_five_minutes_cron_task','usam_system_process', 5 );

class USAM_System_Process
{		
	private $number = 500;
	private $event = array();	
	private $f_log = false;	
	
	/*function file_fopen( $file_name = '' )
	{			
		$file_path  = USAM_UPLOAD_DIR.'Log/'.current_time("m-Y").'_'.$file_name.'.txt';	
		$this->f_log = fopen($file_path,"a");		
	}
	
	private function file_fwrite( $file_name, $text ) 
	{							
		if ( empty($text) )
			return false;
		
		$this->fopen( $file_name );
		
		$text = "[".current_time("d-m-Y H:i:s")."] ".$text;		
		$text .= "\r\n";
		return fwrite($this->f_log, $text );
	}
	
	
	public function file_fclose() 
	{
		if ( $this->f_log !== false )
		{			
			fclose($this->f_log);
		}
	}	*/
	
	public function start_events(  )
	{		
		ini_set("max_execution_time", "3660");	
		
		$events = usam_get_system_process( );
	
		$all_made = true;		
		if ( !empty($events) )
		{		
			ini_set("max_execution_time", "3660");	
			$new_events = $events;
			foreach ( $events as $key => $event )
			{  
				if ( !$event['status'] )
				{									
					$all_made = false;
					$this->event = $event;	
					
					$text = __( 'Запуск процесса', 'usam' ).' ('. usam_local_date( $event['date_insert'], "d.m.Y H:i:s").')   [ '.$key.' ]   '.$event['title'];
					usam_log_file( $text, 'execution_system_processes', false );
					$result = $this->fire_callback( );					
					if ( !is_wp_error( $result ) )
					{ 						
						$this->event['done'] = $result['done']+$this->event['done'];							
						if ( $this->event['done'] >= $this->event['count'] || $result['done'] == 0 )
						{
							$this->event['status'] = true;
							$this->event['data'] = array();	
							
							$text = __( 'Процесс', 'usam' ).' ('. usam_local_date( $event['date_insert'], "d.m.Y H:i:s").')    [ '.$key.' ]    '.$event['title'];
							usam_log_file( $text, 'system_process', false );
							unset($new_events[$key]);
						}			
						else
						{											
							$this->event['data'] = $result['data'];	
							$new_events[$key] = $this->event;													
						}													
					}	
					else
					{		
						$error = new WP_Error( 'usam_invalid_ajax_callback', __( 'Вызвана неверная функция.', 'usam' ) );		
						unset($new_events[$key]);						
					}
				}	
				update_option( 'usam_set_events', serialize($new_events) );	
			}				
		}			
	}	
	
	protected function fire_callback( )
	{			
		if ( is_callable( $this->event['callback'] ) )
			$result = call_user_func( $this->event['callback'], $this->event['data'], $this->number );		
		elseif ( is_string($this->event['callback']) )
		{					
			$callback = "controller_".$this->event['callback'];		
			if ( method_exists( $this, $callback )) 
				$result = $this->$callback();	
			else
				$result = new WP_Error( 'usam_invalid_events_callback', sprintf( __( "Неверный вызов функции: %s.", 'usam' ), $callback) );				
		}				
		return $result;
	}	
	
	// Пересчет цен
	function controller_recalculate_price_products( )
	{	
		$args = $this->event['data'];
		
		if ( empty($args['paged']) )
			$args['paged'] = 1;
	
		$args['posts_per_page'] = $this->number;	
		$args['update_post_meta_cache'] = true;	
		$args['update_post_term_cache'] = true;
		$args['fields'] = 'ids';
		$products = usam_get_products( $args );	
		foreach ( $products as $product_id )
		{		
			usam_edit_product_prices( $product_id );		
		}		
		$number = count($products);
		$args['paged']++;	
	
		return array( 'done' => $number, 'data' => $args );		
	}
	
	function controller_change_attribute_type(  )
	{
		$attribute_id = $this->event['data']['attribute_id'];
		$old_type = $this->event['data']['old_type'];
		$type = $this->event['data']['type'];	
		
		$ready_options = array( );	
		$args = array( 'attribute_id__in' => $attribute_id );
		$ready_options_attributes = usam_get_product_attribute_values( $args );				
		foreach( $ready_options_attributes as $option )
		{
			$ready_options[$option->id] = $option->value;
		}		
		$paged = !empty($this->event['data']['paged'])?$this->event['data']['paged']:1;	
		
		$args = array('fields' => 'ids', 'post_status' => 'publish', 'paged' => $paged, 'posts_per_page' => $this->number, 'update_post_term_cache' => false, 'update_post_meta_cache' => true,'meta_query' => array( array('key' => "_usam_product_attributes_$attribute_id", 'compare' => 'EXISTS') ) );
		
		$products_ids = usam_get_products( $args );				
		foreach( $products_ids as $product_id )
		{
			$product_attributes = usam_get_product_attributes( $product_id );			
			if ( !isset($product_attributes[$attribute_id] ) )
				continue;
				
			switch ( $type ) 
			{
				case 'C' ://Флажок один	
								
				break;		
				case 'S' :			
				case 'N' : 					
					switch ( $old_type ) 
					{
						default:
						case 'T' :	
						case 'O' :								
							foreach( $ready_options as $id => $value )
							{			
								if ( $value == $product_attributes[$attribute_id][0]  )					
								{ 
									usam_update_product_meta( $product_id, "product_attributes_$attribute_id", $id );
									break;
								}
							}	
						break;
					}
				break;	
			}
		}
		$paged++;
		$number = count($products_ids);
		$this->event['data']['paged'] = $paged;	
		return array( 'done' => $number, 'data' => $this->event['data'] );		
	}
	
	// Пересчитать остатки у заданных товаров
	function controller_recalculate_stock_products( )
	{		
		$args = $this->event['data'];
		
		if ( empty($args['paged']) )
			$args['paged'] = 1;
		
		$args['posts_per_page'] = $this->number;	
		$args['cache_results'] = false;	
		$args['update_post_meta_cache'] = true;		
		$args['update_post_term_cache'] = false;
		$args['fields'] = 'ids';			
		$products = usam_get_products( $args );				
		foreach ( $products as $product_id )
		{				
			usam_recalculate_stock_product( $product_id );
		}					
		$number = count($products);	
		$args['paged']++;			
		return array( 'done' => $number, 'data' => $args );	
	}
	
	function controller_download_email_pop3_server( )
	{		
		$mailbox_ids = usam_get_mailboxes( array( 'fields' => 'id' ) );	
		foreach ( $mailbox_ids as $mailbox_id ) 
			usam_download_email_pop3_server( $mailbox_id );
			
		return array( 'done' => 0, 'data' => 0 );	
	}
	
	// Обновление товаров в контакте
	function controller_vk_update_all_products( ) 
	{					
		$count = 20;
		$paged = empty($this->event['data'])?1:$this->event['data'];
		
		$args = array('post_status' => 'publish', 'paged' => $paged, 'posts_per_page' => $count, 'update_post_term_cache' => true, 'meta_query' => array( array('key' => '_usam_vk_market_id', 'type' => 'numeric', 'value' => '0', 'compare' => '!=')) );	
		$products = usam_get_products( $args );	
		if ( !empty($products) )
		{			
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();	
		
			$profiles = $vkontakte->user_and_group('group');	
			foreach ( $profiles as $profile ) 
			{  
				foreach ( $products as $product ) 
				{										
					$result = $vkontakte->edit_product($product, $profile);	
				}
			}
			$vkontakte->set_log_file(); 			
		}							
		$number = count($products);			
		$paged++;				
		return array( 'done' => $number, 'data' => $paged );		
	}
	
	function controller_calculation_accumulative_discount_customer( ) 
	{	
		$paged = $this->event['data'];
		$args = array( 'fields' => 'ID', 'number' => $this->number, 'paged' => $paged, 'orderby' => 'ID' );
		$user_ids = get_users( $args );
			
		foreach ( $user_ids as $user_id )
		{ 
			$discount = usam_get_accumulative_discount_customer( 'price', $user_id );		
			update_user_meta( $user_id, 'usam_accumulative_discount', $discount );	
		}
		$number = count($user_ids);
		$paged++;				
		return array( 'done' => $number, 'data' => $paged );		
	}
	
	function controller_calculate_increase_sales_product( )
	{
		require_once( USAM_FILE_PATH . '/includes/product/increase_sales_product.class.php' );
		
		$paged = empty($this->event['data'])?1:$this->event['data'];
		
		$tomorrow  = mktime(0, 0, 0, date("m"), date("d")-14, date("Y"));		
		$args = array (						
			'meta_query' => array( 'relation' => 'OR', array('key' => '_usam_increase_sales_time', 'value' => $tomorrow, 'compare' => '>' ), array('key' => '_usam_increase_sales_time', 'value' => '', 'compare' => '=' ), array('key' => '_usam_increase_sales_time', 'compare' => 'NOT EXISTS' ), ),					
			'fields' => 'ids',			
			'post_status' => 'publish',
			'update_post_meta_cache' => true,
			'update_post_term_cache' => false,
			'cache_results' => true,
			'posts_per_page' => $this->number,
			'paged' => $paged,
		);		
		$products_ids = usam_get_products( $args );		
		foreach ( $products_ids as $product_id )
		{				
			$cross_sells = new USAM_Increase_Sales_Product( $product_id );
			$cross_sells->cross_sell();			
			usam_update_product_meta( $product_id, 'increase_sales_time', time() );			
		}	
		$number = count($products_ids);			
		$paged++;
		return array( 'done' => $number, 'data' => $paged );	
	}
}
?>