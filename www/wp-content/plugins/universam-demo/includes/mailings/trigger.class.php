<?php
 /**
 * Клас управления триггерной рассылки
 */ 
class USAM_Trigger
{
	 // строковые
	private static $string_cols = array(
		'status',		
		'event_start',		
		'condition',	
		'data',	
		'date_modified',			
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'newsletter_id',				
	);
	// рациональные
	private static $float_cols = array(		
	);	
	
	private $data     = array();	
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД
	
	/**
	 * Конструктор объекта
	 * @since 4.9	
	 */
	public function __construct( $value = false, $col = 'id' ) 
	{
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id', 'newsletter_id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );	
		if ( $col == 'code'  && $id = wp_cache_get( $value, 'usam_trigger_newsletter_id' ) )
		{   // если код_сеанса находится в кэше, вытащить идентификатор
			$col = 'id';
			$value = $id;
		}				
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_trigger' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$_trigger ) 
	{
		$id = $_trigger->get( 'id' );	
		wp_cache_set( $id, $_trigger->data, 'usam_trigger' );	
	
		if ( $newsletter_id = $_trigger->get( 'newsletter_id' ) )
			wp_cache_set( $newsletter_id, $id, 'usam_trigger_newsletter_id' );
		
		do_action( 'usam_trigger_update_cache', $_trigger );
	}

	/**
	 * Удалить кеш	 
	 */
	public function delete_cache( ) 
	{
		wp_cache_delete( $this->get( 'id' ), 'usam_trigger' );	
		wp_cache_delete( $this->get( 'code' ), 'usam_trigger_newsletter_id' );
		
		do_action( 'usam_trigger_delete_cache', $this );	
	}
	
	public function delete( ) 
	{		
		global  $wpdb;
		
		$id = $this->get( 'id' );	
		
		do_action( 'usam_trigger_before_delete', $this );
		
		self::delete_cache( );	
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_NEWSLETTER_TRIGGERED." WHERE id = '$id'");
		
		do_action( 'usam_trigger_delete', $id );
		
		return $result;
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_NEWSLETTER_TRIGGERED." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{	
			$data['condition'] = unserialize( $data['condition'] );		
			$data['data'] = unserialize( $data['data'] );			
			$this->exists = true;
			$this->data = apply_filters( 'usam_trigger_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_trigger_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_trigger_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_trigger_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{				
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();
		
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_trigger_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	private function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
		
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb;

		do_action( 'usam_trigger_pre_save', $this );	
		$where_col = $this->args['col'];	
		
		$this->data['date_modified'] = date( "Y-m-d H:i:s" );
		if ( isset($this->data['condition']) )	
			$this->data['condition']     = serialize( $this->data['condition'] );
		
		if ( isset($this->data['data']) )	
			$this->data['data']     = serialize( $this->data['data'] );
				
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );			
			
			do_action( 'usam_trigger_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $where_col => $where_val );	

			$this->data = apply_filters( 'usam_trigger_update_data', $this->data );			
			$format = $this->get_data_format( );	
			$this->data_format( );	
				
			$result = $wpdb->update( USAM_TABLE_NEWSLETTER_TRIGGERED, $this->data, $where, $format, $where_format );	
			do_action( 'usam_trigger_update', $this );
		} 
		else 
		{   
			do_action( 'usam_trigger_pre_insert' );		
			unset( $this->data['id'] );	
						
			if ( empty($this->data['newsletter_id']) )		
				return '';
			
			if ( !isset($this->data['status']) )		
				$this->data['status'] = 0;

			if ( !isset($this->data['event_start']) )		
				$this->data['event_start'] = '';		

			if ( !isset($this->data['condition']) )		
				$this->data['condition'] = '';				
			
			$this->data = apply_filters( 'usam_trigger_insert_data', $this->data );					
			$format = $this->get_data_format( );	
			$this->data_format( );	
			
			$result = $wpdb->insert( USAM_TABLE_NEWSLETTER_TRIGGERED, $this->data, $format );					
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );
				$this->id = $wpdb->insert_id;

				$this->args = array('col' => 'id',  'value' => $this->id, );				
			}
			do_action( 'usam_trigger_insert', $this );
		} 		
		do_action( 'usam_trigger_save', $this );

		return $result;
	}
}

// Обновить
function usam_update_trigger( $id, $data )
{
	$shipped = new USAM_Trigger( $id );
	$shipped->set( $data );
	return $shipped->save();
}

// Получить 
function usam_get_trigger( $id, $colum = 'id' )
{
	$shipped = new USAM_Trigger( $id, $colum );
	return $shipped->get_data( );	
}

// Добавить 
function usam_insert_trigger( $data )
{
	$shipped = new USAM_Trigger( $data );
	return $shipped->save();
}

// Удалить 
function usam_delete_trigger( $id )
{
	$shipped = new USAM_Trigger( $id );
	return $shipped->delete();
}


function usam_get_trigger_newsletters( $args )
{
	global $wpdb;		

	$default = array( 'status' => 1, 'event_start' => '' );
	$args = array_merge ($default, $args);			
	
	$joins = array();
	$conditions = array();
	
	if ( $args['status'] == 1 )				
	{
		$joins[] = "INNER JOIN ".USAM_TABLE_NEWSLETTER_TEMPLATES." ON (".USAM_TABLE_NEWSLETTER_TEMPLATES.".id=".USAM_TABLE_NEWSLETTER_TRIGGERED.".newsletter_id AND ".USAM_TABLE_NEWSLETTER_TEMPLATES.".status='5')";
	}
	if ( $args['event_start'] != '' )	
	{
		if ( is_array($args['event_start']) )	
			$conditions[] = "event_start IN ('".implode("','", $args['event_start'])."')";
		else
			$conditions[] = "event_start='".$args['event_start']."'";
	}	
	$where = implode(' AND ', $conditions );
	$join = implode(' AND ', $joins );
	
	$sql = "SELECT ".USAM_TABLE_NEWSLETTER_TRIGGERED.".* FROM ".USAM_TABLE_NEWSLETTER_TRIGGERED." $join WHERE $where";	
	$triggers = $wpdb->get_results( $sql );	
	
	return $triggers;
}
?>