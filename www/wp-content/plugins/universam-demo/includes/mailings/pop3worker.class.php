<?php
//http://wincode.org/php/working-with-pop3-in-php

class USAM_POP3
{
	private $pop_conn = NULL; // Здесь храним указатель на поток скрипт<->сервер

	private $server = '';
	private $user   = '';
	private $pass   = '';
	private $ssl    = 0;
	private $port   = 110;
	private $letter_id = 0;
	private $mailbox_id = 0;
	private $delete_server = 0;
	private $delete_server_day = 0;	
	
	private $letterCount = 0; // Количество писем на сервере	
	private $errors = array();
	private $message = '';
	
	private $attachments = array();

	public function __construct( $mailbox_id )
	{		
		
			$this->set_error( __('Купите лицензию',"usam") );			
	}
	
	public function set_error( $error )
	{
		$this->errors[] = sprintf( __('POP3. Пользователь: %s. Ошибка: %s'), $this->user, $error );
	}
	
	public function set_log_file()
	{
		usam_log_file( $this->errors );
	}
	
	public function get_message_errors()
	{	
		return $this->errors;
	}
	
	private function connect_server()
	{	
		if ( $this->ssl ) 
			$this->server = 'ssl://'.$this->server;
		
		$this->pop_conn = fsockopen($this->server, $this->port, $errno, $errstr, 30);		
		if ( !$this->pop_conn ) 		
			$this->set_error( sprintf(__("Сервер не найден. Ошибка: %s %s","usam"), $errno, $errstr) );		
		else
		{	
			$connect_result = fgets($this->pop_conn, 1024);			
			if (strpos($connect_result, 'OK')) 
			{ 
				return true;
			} 
			else 			
				$this->set_error( __('Ошибка подключения к pop3-серверу',"usam") );			
		}
		return false;
	}
	
	public function connect_open()
	{	
		if ( $this->pop_conn ) 
			return true;
		else
			return false;
	}

// Отключение от сервера POP3
	public function disconnect()
	{
		if ( $this->pop_conn ) 
			fputs($this->pop_conn,"QUIT\r\n");
	}
			
// Авторизация на сервере
	private function authPop3Server()
	{
		fputs($this->pop_conn, "USER ".$this->user."\r\n");
		$connect_result = fgets($this->pop_conn, 1024);
	
		// Если получилось автоматизироваться, делаем метку, как true
		if (strpos($connect_result, 'OK')) 
		{
			fputs($this->pop_conn, "PASS ".$this->pass."\r\n");
			$connect_result = fgets($this->pop_conn, 1024);
			if (strpos($connect_result, 'OK')) 
			{				
				return true;
			}
			else
				$this->set_error( __('Неверный пароль или закрыт доступ по протоколу POP в вашем почтовом ящике.','usam') );
		} 
		else
			$this->set_error( __('Ошибка авторизации','usam') );
			
		return false;
	}
	
	//оказывает общую информацию и размер каждого письма. 
	public function get_list_mail()
	{		
		fputs($this->pop_conn, "LIST\r\n");
		$connect_result = fgets($this->pop_conn, 1024);	
		// Получаем общую информацию, распарсив регулярным выражением
		// Возвращаемую сервером строку
		$lists = array();
		if ( strpos($connect_result, 'OK') )
		{ 
			$text = $this->get_data( );				
			$rows = explode ("\n", $text);			
			foreach ( $rows as $row )
			{
				$lists[] = explode(" ", $row);				
			}
		} 
		else
			$this->set_error( __('Не удалось получить информацию из pop3 сервера!','usam') );
		
		return $lists;
	}
	
	//оказывает общую информацию и размер каждого письма. 
	public function get_list_mail_ids()
	{		
		if ( !$this->pop_conn ) 		
		{			
			return false;
		}
		fputs($this->pop_conn, "UIDL\r\n");
		$connect_result = fgets($this->pop_conn, 1024);	
		// Получаем общую информацию, распарсив регулярным выражением
		// Возвращаемую сервером строку
		$lists = array();
		if ( strpos($connect_result, 'OK') )
		{  
			$text = $this->get_data( );			
			$rows = explode ("\n", $text);						
			foreach ( $rows as $row )
			{
				$result = explode(" ", $row);
				$lists[$result[1]] = $result[0];			
			}			
		} 
		else
		{
			$this->set_error( __('Не удалось получить информацию! Проверьте настройки. Возможно вам нужно разрешить доступ по протоколу POP3.','usam') );
			return false;
		}		
		return $lists;
	}	
	
	public function filtering( $mail )	
	{		
		require_once( USAM_FILE_PATH . '/includes/mailings/email_filters_query.class.php' );
		$filters = usam_get_email_filters( $args );
		foreach ( $filters as $filter )
		{				
			if ( $filter->mailbox_id )
			{		
				if ( $mail['mailbox_id'] != $filter->mailbox_id )
					continue;
			}
			else
			{
				
			}
			$result = false;
			$value = '';
			switch ( $filter->if ) 
			{
				case 'sender' : 
					$value = $mail['from_email'];
				break;
				case 'recipient' : 
					$value = $mail['to_email'];
				break;
				case 'copy' : 
					$value = $mail['reply_to_email'];
				break;
				case 'subject' : 
					$value = $mail['subject'];
				break;
				case 'size' : 
					$value = $mail['size'];
				break;
			}		
			switch ( $filter->condition ) 
			{
				case 'equal' : 
					if ( $filter->value == $value )
						$result = true;
				break;
				case 'not_equal' : 
					if ( $filter->value != $value )
						$result = true;
				break;	
				break;
			}
			if ( $result )
			{
				switch ( $filter->action ) 
				{
					case 'read' : 
						$mail['read'] = 1;
					break;
					case 'important' : // Пометить письмо важным
						$mail['importance'] = 1;
					break;
					case 'delete' : 
						return true;
					break;
					case 'folder' : 
						$mail['folder'] = $filter->folder;
					break;
				}
				break;				
			}
		}
		return false;
	}
	
	public function get_download_number_emails()	
	{
		$mail_ids = $this->get_list_mail_ids();		
		if ( $mail_ids === false )
			return false;		
		elseif ( empty($mail_ids) )
			return true;
		
		if ( empty($this->letter_id) )
			return count($mail_ids);
			
		ksort($mail_ids);	
		$i = 0;		
		ini_set("max_execution_time", "3660");		
		foreach ( $mail_ids as $key_id => $id )
		{				
			$key_id = (int) $key_id;			
			if ( $key_id > $this->letter_id )
			{			
				$i++;
			}			
		} 	
		$this->disconnect();		
		$this->set_log_file();
		return $i;		
	}	

	public function download_messages()	
	{
		$mail_ids = $this->get_list_mail_ids();		
		if ( $mail_ids === false )
			return false;		
		elseif ( empty($mail_ids) )
			return true;
			
		ksort($mail_ids);	
		ini_set("max_execution_time", "3660");		
		foreach ( $mail_ids as $key_id => $id )
		{				
			$key_id = (int) $key_id;			
			if ( $key_id > $this->letter_id )
			{			
				$mail = $this->get_mail( $id );				
				if ( !$mail )
					continue;
								
				$insert_email = array( 'server_message_id' => $key_id, 'body' => $mail['body'], 'subject' => $mail['subject'], 'reply_to_email' => $mail['reply_to_email'], 'reply_to_name' => $mail['reply_to_name'], 'to_email' => $mail['to_email'], 'to_name' => $mail['to_name'], 'from_email' => $mail['from_email'], 'from_name' => $mail['from_name'], 'sent_at' => $mail['date'], 'read' => 0, 'folder' => 'inbox', 'mailbox_id' => $this->mailbox_id );	
				if ( preg_match("#\#\#([a-z]*-[0-9]*)\#\##s", $mail['body'], $matches) )
				{								
					$result = explode('-', $matches[1]);
					if ( !empty($result[0]) && !empty($result[1]) && is_numeric($result[1]) ) 
					{
						$insert_email['object_type'] = $result[0];
						$insert_email['object_id'] = $result[1];								
					}
				}		
				if ( preg_match("%\%\%([0-9]*)\%\%%s", $mail['body'], $matches) )
				{								
					$insert_email['reply_message_id'] = (int)$matches[1];	
				}					
				if ( $this->filtering( $insert_email ) )
					continue;
				
				$_email = new USAM_Email( $insert_email );
				$result = $_email->save();					
				if ( $result )
				{
					if ( !empty($mail['attachments']) )
					{		
						$_email->set_attachments( $mail['attachments'] );
					}											
					$this->letter_id = $key_id;
					usam_update_mailbox( $this->mailbox_id, array( 'letter_id' => $key_id ) );	

					if ( $this->delete_server && $this->delete_server_day == 0 )
					{
						$this->delete_message( $key_id );	
					}						
				}
			}			
		} 	
		$this->disconnect();		
		$this->set_log_file();
		return true;		
	}	
	
	public function delete_messages( )	
	{				
		$mail_ids = $this->get_list_mail_ids();	
		if ( $mail_ids === false )
			return false;		
		elseif ( empty($mail_ids) )
			return true;
			
		if ( $this->delete_server_day == 0 )
			return false;
		
		$datetime = strtotime("-{$this->delete_server_day} day");

		ksort($mail_ids);	
		ini_set("max_execution_time", "3660");	
		$i = 0;
		foreach ( $mail_ids as $key_id => $id )
		{				
			$key_id = (int) $key_id;	
			
			$header = $this->get_top_message( $id );			
			$headers = $this->decode_header( $header );				
			$email_date = strtotime($headers["date"]);
			if ( $email_date <= $datetime )
			{
				$this->delete_message( $id );	
				$i++;
			}
		} 	
		$this->disconnect();
		return $i;		
	}
	
	// Возвращает количество писем
	public function get_letter_count()
	{
		return $this->letterCount;
	}
	
	
// Получаем количество писем
	private function startWork()
	{		
		fputs($this->pop_conn, "STAT\r\n");
		$connect_result = fgets($this->pop_conn, 1024);
		// Получаем количество писем, распарсив регулярным выражением
		// Возвращаемую сервером строку
		if ( strpos($connect_result, 'OK') )
		{
			$pattern = '/OK ([0-9]+) [0-9]+/';
			preg_match($pattern, $connect_result, $matches);
			$this->letterCount = intval($matches[1]);			
		} 
		else
			$this->set_error( __('Не удалось получить информацию от pop3-сервера!','usam') );
	}
	
	private function get_data()
	{ 
		$data = "";
		while (!feof($this->pop_conn))
		{
			$result = @fgets( $this->pop_conn );
			if ( $result === false )
			{
				$this->set_error( __('Не удалось получить сообщение. Ошибка в функции fgets.','usam') );
				return false;
			}
			$buffer = chop( $result );		
			if ( trim($buffer) == "." ) 
				break;			
			$data .= "$buffer\r\n";
		}
		return trim($data);
	}	

//Первое, что нужно сделать с полученным письмом — разделить его на информацию и тело (headers & body). 	
	function fetch_structure( $email )
	{
	  $ARemail = Array();
	  $separador = "\r\n\r\n";
	 
	  $header = trim(substr($email,0,strpos($email,$separador)));
	  $bodypos = strlen($header)+strlen($separador);
	  $body = substr($email,$bodypos,strlen($email)-$bodypos);
	 
	  $ARemail["header"] = $header;
	  $ARemail["body"] = $body;
	 
	  return $ARemail;
	}
	
//С помощью этой функции можно узнать email, куда отвечать на письмо, откуда оно пришло, время получения, адрес отправителя, тема письма, тип письма, содержимое письма.	
	function decode_header($header) 
	{		
		$unassembled_headers = explode("\r\n", $header);	
		$headers = array();
		$lasthead = null;
		foreach ( $unassembled_headers as $key => $header ) 
		{ 
			if ( strpos( $header, ': ' ) === false && $lasthead !== null )
			{			
				$headers[$lasthead] .= $header;
			}
			else
			{				
				$headers[$key] = $header;
				$lasthead = $key;
			}						
		}
		$headers = array_values($headers);
		
		$decodedheaders = array();		
		for( $i=0; $i < count($headers); $i++ )
		{
			$thisheader = trim($headers[$i]);					
			$header_item = array();
			if ( strpos( $thisheader, ': ' ) !== false )
			{
				$header_item = explode(": ",$thisheader,2);	
			}
			elseif ( strpos( $thisheader, '=' ) !== false )
			{
				$header_item = explode("=",$thisheader,2);	
			}
			if ( !empty($header_item) )
			{
				$key = strtolower($header_item[0]);
				$values = explode(";",$header_item[1],2);	
				$decodedheaders[$key] = trim($values[0]);
				
			/*	if ( !empty($values[1]) )
				{
					$header_items = explode(" ",$values[1]);				
					foreach ( $header_items as $item ) 
					{
						$header_item = explode("=",$item,2);
						$key = strtolower($header_item[0]);
						$decodedheaders[$key] = trim($header_item[1]);						
					}
				}*/
				//$value = str_replace(";", "", $header_item[1]);			
				
			}
		/*	$thisheader = trim($headers[$i]);
			if(!empty($thisheader))
			{		
				if( preg_match('/^[A-Z0-9a-z_-]+:/i', $thisheader, $regs) )			
				{
					$dbpoint = strpos($thisheader,":");
					$headname = strtolower(substr($thisheader,0,$dbpoint));
					$headvalue = trim(substr($thisheader,$dbpoint+1));					
					$decodedheaders[$headname] = $headvalue;
					$lasthead = $headname;
				}
				elseif ( isset($lasthead) )
				{
					$decodedheaders[$lasthead] .= " $thisheader";
				}
			}*/
		}				
		return $decodedheaders;
	}	
	
	private function decode_text( $return_text, $search ) 
	{			
		if ( strripos( $search, "koi8" ) !== false) 
		{
			$return_text = iconv ('KOI8-R', 'UTF-8', $return_text);			
		}   
		elseif ( strripos( $search, "Windows-1251" ) !== false )
		{
			$return_text = iconv ('windows-1251', 'UTF-8', $return_text);				
		}
		elseif ( strripos( $search, "iso-8859-5" ) !== false )
		{  		
			$return_text = iconv ('iso-8859-5', 'UTF-8', $return_text);	
		}				
		return $return_text;
	}
	
	function compile_body( $body, $enctype ) 
	{	
		if(strtolower($enctype) == "base64") 
			$body = base64_decode($body);
		elseif(strtolower($enctype) == "quoted-printable")
			$body = quoted_printable_decode($body);
		
		return $body;		
	}
		
	private function get_attachment( $attachment, $type = 'U' )
	{ 		
		$email = $this->fetch_structure( $attachment );	
		
		$attachment_headers = $this->decode_header( $email['header'] );			
		if ( empty($attachment_headers["content-type"]) )
			return false;	
	
		if ( !empty($attachment_headers["content-disposition"]) && ( $attachment_headers["content-disposition"] == 'form-data' || $attachment_headers["content-disposition"] == 'attachment' ) )
		{	
			if( !isset($attachment_headers["content-transfer-encoding"]) )
				$file = '';
			elseif(strtolower($attachment_headers["content-transfer-encoding"]) == "base64")
				$file = base64_decode($email["body"]);
			elseif(strtolower($attachment_headers["content-transfer-encoding"]) == "quoted-printable")
				$file = quoted_printable_decode($email["body"]);
			elseif(strtolower($attachment_headers["content-transfer-encoding"]) == "8bit")
				$file = $email["body"];
			elseif(strtolower($attachment_headers["content-transfer-encoding"]) == "7bit")
				$file = $email["body"];
			elseif(strtolower($attachment_headers["content-transfer-encoding"]) == "binary")
				$file = $email["body"];	 	

			if( preg_match('/filename[ ]?="([^"]+)"/i', $email['header'], $regs) ) 			
				$filename = $this->decode_mime_string( $regs[1] );
			elseif( preg_match('/name[ ]?="([^"]+)"/i', $email['header'], $regs) ) 							
				$filename = $this->decode_mime_string( $regs[1] );
			else 
				$filename = 'no_name';	
			if ( $file != '' )
				$this->attachments[] = array( 'filename' => sanitize_file_name($filename), 'file' => $file, 'type' => $type ); 
			return true;	
		}
		return false;			
	}
		
	public function parse_email( $text )
	{			
		$email = $this->fetch_structure( $text );		
		if ( empty($email["header"]) )
			return false;
		
		$headers = $this->decode_header( $email["header"] );			
		if ( empty($headers["content-type"]) )
			return false;
	
		$boundary = '';	
		if(preg_match('/boundary[ ]?="[ ]?(.[^\s]*)"/i', $email["header"], $regs)) 	//\S			
			$boundary = $regs[1];			
		
		$charset = '';
		if( preg_match('/charset[ ]?="([^"]+)"/i', $email["header"], $regs) ) 			
			$charset = $regs[1];		
		
		$return = false;
		$body = '';
		switch ( $headers["content-type"] ) 
		{
			case 'text/plain' : // если это обычный текст			
				$return = true;
				$body = $this->compile_body($email["body"], $headers["content-transfer-encoding"] );	
				$body = $this->decode_text( $body, $charset );
				$body =  nl2br( $body );	
			break;	
			case 'text/html' :	// если это html
				$return = true; 
				$body = $this->compile_body($email["body"], $headers["content-transfer-encoding"] );	
				$body = $this->decode_text( $body, $charset );
				if( preg_match_all('/src="(.[^\s]*)"/i', $body, $regs, PREG_SET_ORDER ) ) 		
				{					
					foreach ( $regs as $reg ) 
					{						
						$text = str_replace('cid:', '', $reg[1] );
						$result = explode("@", $text);							
						if ( isset($result[0]) )
						{
							$body = str_replace($reg[1], $result[0], $body );
						}
					}
				}						
			break;		
			case 'multipart/alternative' : 
				$return = true;
				$types_attachments = preg_split('/--'.$boundary.'/i', $email["body"]);					
				foreach ( $types_attachments as $attachment )
				{										
					$result = $this->parse_email( $attachment );	
					if ( $result !== false )
						$part_body[$result['type']] = $result['body'];
				}	
				if ( !empty($part_body['text/html']) )						
					$body = $part_body['text/html'];
				elseif ( !empty($part_body['text/plain']) )
					$body = $part_body['text/plain'];					
			break;
			case 'multipart/mixed' : //сообщение содержит вложения
				$body = '';
				$return = true;
				$types_attachments = preg_split('/--'.$boundary.'/i', $email["body"]);	
				foreach ( $types_attachments as $attachment )
				{							
					$attachment = trim($attachment);
					if ( empty($attachment) )
						continue;
											
					if ( !$this->get_attachment( $attachment ) )
					{					
						$result = $this->parse_email( $attachment );		
						$body .= $result['body'];	
					}				
				} 
			break;
			case 'multipart/related' :	// сообщение содержит связанные части
				$return = true;
				$types_attachments = preg_split('/--'.$boundary.'/i', $email["body"]);					
				foreach ( $types_attachments as $attachment )
				{ 
					$attachment_headers = $this->decode_header( $attachment );	
					if ( empty($attachment_headers["content-type"]) )
						continue;
					
					if ( $attachment_headers["content-type"] == 'text/html' )
					{
						$email_parts = $this->fetch_structure( $attachment );
						if( preg_match('/boundary[ ]?="[ ]?(["]?.*)"/i', $email_parts['header'], $regs) ) 			
						{ 
							$boundary = $regs[1];
						}
						else
							$boundary = '';		
						
						$parts = preg_split('/--'.$boundary.'/i', $email_parts['body']);	
						foreach ( $parts as $part )
						{	
							$result = $this->parse_email( $part );	
							if ( $result !== false )
								$part_body[$result['type']] = $result['body'];
						}								
						if ( isset($part_body['text/html']) )						
							$body = $part_body['text/html'];
						elseif ( isset($part_body['text/plain']) )
							$body = $part_body['text/plain'];	
					}
					else
					{						
						$this->get_attachment( $attachment, 'R' );					
					}
				}	
			break;			
			default:
				$return = false;
		}
		if ( $return )
			$return = array( 'body' => $body, 'type' => $headers["content-type"] );
		
		return $return;
	}

//цикл по всем письмам	
	public function get_mail( $id )
	{								
		
		return false;		
	
	}
	
	private function get_name_and_email( $header )
	{		
		$return = array( 'email' => '', 'name' => '' );
		$array = explode(' ', $header );		
		if ( isset($array[1]) )		
		{ 				
			if ( preg_match_all('|<(.*)>|Uis', $header, $result) )	
			{
				$str = array();
				foreach ( $result[1] as $email )
				{
					if ( is_email($email) )
						$str[]= $email;
				}				
				$return['email'] = implode(',',$str);
			}	
		/*	if ( preg_match_all('|<(.*)>|Uis', $header, $result) )	
			{			
				if ( !empty($result) )
					$return['name'] = implode(',',$result[1]);
			}	*/		
			$name = trim(str_replace("\"", "", strip_tags( $header ) ));
			$return['name'] = $this->decode_mime_string( $name );	
		}
		else
			$return = array( 'email' => $header, 'name' => '' );
		return $return;
	}
	
	//Декодировать заголовок	
	function decode_mime_string( $text ) 
	{
		$string = str_replace(array("\r\n", "\r", "\n", ), '', $text );	
		if(($pos = strpos($string,"=?")) === false) 
			return $string;
		
		$newresult = '';
		while(!($pos === false))
		{
			$newresult .= substr($string,0,$pos);
			$string = substr($string,$pos+2,strlen($string));
			$intpos = strpos($string,"?");
			$charset = substr($string,0,$intpos);
			$enctype = strtolower(substr($string,$intpos+1,1));
			$string = substr($string,$intpos+3,strlen($string));
			$endpos = strpos($string,"?=");
			$mystring = substr($string,0,$endpos);
			$string = substr($string,$endpos+2,strlen($string));
			if($enctype == "q") 
				$mystring = quoted_printable_decode(str_replace("_"," ",$mystring));
			else if ($enctype == "b") 
				$mystring = base64_decode($mystring);
			$newresult .= $mystring;
			$pos = strpos($string,"=?");
		}	
		return $this->decode_text( $newresult.$string, $text );
	}	
	
	public function delete_message( $id )
	{		
		fputs($this->pop_conn, "DELE ".$id."\r\n");
	}
	
	// Получить i строк письма c id
	public function get_top_message( $id, $i = 0 )
	{		
		fputs($this->pop_conn, "TOP $id $i\r\n");
		$result = $this->get_data( );	
		return $result;
	} 
}
/*
Ниже перечислены поля, которые могут присутствовать в заголовке сообщения и их описание.

Date - Дата и время создания сообщения, когда сообщение готово и может быть отослано.
From - Разделенные запятой почтовые адреса авторов сообщения. В случае, если адресов несколько, должно быть поле Sender.
Sender - Почтовый адрес отправителя. Если поле From содержит один адрес, то поле Sender может отсутствовать. Если значения полей Sender и From совпадают, то поле Sender должно отсутствовать.
Reply-to - Почтовый адрес, на который автор сообщения желал бы получить ответ.
To - Почтовые адреса основных адресатов. Если адресов несколько - они разделяются запятыми.
Cc - Копии. Почтовые адреса других адресатов. Если адресов несколько - они разделяются запятыми.
Bcc - Слепые копии. Почтовые адреса адресатов, которые будут не видны другими адресатами, получающим это сообщение. Если адресов несколько - они разделяются запятыми.
Message-id - Поле предоставляет уникальный идентификатор сообщения. Идентификатор уникален для всего мира.
In-Reply-To - Содержит идентификатор оригинального сообщения, на которое делается ответ.
References - Содержит идентификатор оригинального сообщения, на которое делается ответ.
Subject - Тема сообщения.
Comments - Содержит дополнительные комментарии к сообщению.
Keywords - Ключевые слова и важные слова, которые могут быть полезны адресату.
Resent-Date, Resent-From, Resent-Sender, Resent-To, Resent-Cc, Resent-Bcc, Resent-Message-Id - Используются при пересылке сообщения. Эти поля содержат информацию, измененную тем, кто производил пересылку.
Return-Path - Почтовый адрес, проставляемый SMTP-сервером на стадии финальной отсылки. Чаще всего используется для доставки отчета с описанием возникшей ошибки.
Received - Используется для идентификации SMTP-серверов, которые принимали участие в отправке сообщения от отправителя к получателю. Каждый SMTP-сервер добавляет свое поле.
Encrypted - Указывает на то, что сообщение было подвергнуто шифрованию.
MIME-Version - Содержит версию MIME. Дополнительную информацию можно получить из документов RFC 2045, RFC 2046, RFC 2047, RFC 2048, RFC 2049.
Content-Type - Значением этого поля является наиболее полная информация о содержимом сообщения, которая позволяет почтовому клиенту выбрать соответствующий механизм обработки.
Content-Transfer-Encoding - Указывается способ помещения двоичных данных в тело сообщения.
Поля начинающиеся с X- - Дополнительное незарегистрированное поле. Разные почтовые клиенты могут использовать разные незарегистрированные поля для собственных нужд.
*/
?>