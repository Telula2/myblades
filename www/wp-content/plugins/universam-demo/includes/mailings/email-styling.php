<?php
class USAM_Mail_Styling
{	
	function __construct( )
	{		
		
	}
		
	function class_in_style( $classes, $block )
	{
		foreach($classes as $class => $styles) 
		{			
			$inlineStyles = '';
			foreach($styles as $key => $st)
				 $inlineStyles .= $key.':'.$st.';';
			$styles = preg_replace('/(\n*)/', '', $styles);
			if(in_array($class, array('h1-link', 'h2-link', 'h3-link'))) 
			{
				$classes['#<([^ /]+) ((?:(?!>|style).)*)(?:style="([^"]*)")?((?:(?!>|style).)*)class="[^"]*'.$class.'[^"]*"((?:(?!>|style).)*)(?:style="([^"]*)")?((?:(?!>|style).)*)>#Ui'] = '<$1 $2$4$5$7 style="'.$inlineStyles.'">';
			} 
			else 
			{
				$classes['#<([^ /]+) ((?:(?!>|style).)*)(?:style="([^"]*)")?((?:(?!>|style).)*)class="[^"]*'.$class.'[^"]*"((?:(?!>|style).)*)(?:style="([^"]*)")?((?:(?!>|style).)*)>#Ui'] = '<$1 $2$4$5$7 style="$3$6'.$inlineStyles.'">';
			}	
			unset($classes[$class]);
		}	
		$styledBlock = preg_replace(array_keys($classes), $classes, $block);	
		return $styledBlock;
	}
	
	function process_plaintext_args( $message )
	{		
		$args = array(			
			'mailcontent' => $message,
		);
		$mailtemplate = usam_get_email_template();	
	
		$shortcode = new USAM_Shortcode();
		return $shortcode->process_args( $args, $mailtemplate );		
	}
	
	/* Описание: оформление письма
	*/
	function styleMail( $vars )
	{
		remove_filter('wp_mail',array($this, 'styleMail'),12, 1);
		extract($vars);		
		$classes = array(					
					'transaction_results_cell' => array('border'=>'1px solid #DEDEDE','text-align'=>'center'),					
					'transaction_results_thead' => array('background' => '#EEEEEE'),
					'table_message_th'=> array('text-align'=>'center')
                );			
	
		$message = $this->process_plaintext_args( $message );		
		$message = $this->class_in_style($classes, $message);	
		$message = stripslashes(str_replace('\\&quot;','',$message));
		$message = preg_replace('/\<http(.*)\>/', '<a href="http$1">http$1</a>', $message);
		add_filter( 'wp_mail_content_type', create_function('', 'return "text/html";'));
		
		return compact( 'to', 'subject', 'message', 'headers', 'attachments' );
	}	
}

/**
 * Отправка почты
 */
function usam_mail( $address, $subject, $message, $headers = '', $attachments = array() )
{
	if ( empty($message) )
		return false;
	
	$style = new USAM_Mail_Styling();
	add_filter('wp_mail',array($style, 'styleMail'),12,1);		
	
	if ( $headers == '' )
	{
		$mailbox = usam_get_primary_mailbox();
		if ( empty($mailbox['email']) )
			return false;
		
		$headers = "From: ".$mailbox['name']." <".$mailbox['email'].">" . "\r\n";	
	}
	$email_sent = wp_mail( $address, $subject, $message, $headers, $attachments );
	return $email_sent;	
}
?>