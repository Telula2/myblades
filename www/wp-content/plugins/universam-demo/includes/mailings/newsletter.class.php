<?php
// Класс работы с рассылкой
class USAM_Newsletter
{	
	// строковые
	private static $string_cols = array(
		'subject',		
		'body',		
		'template',
		'date_insert',	
		'date_update',	
		'sent_at',		
		'status',	
		'type',	
		'class',	
		'data',			
	);
	// цифровые
	private static $int_cols = array(
		'id',		
		'mailbox_id',			
		'number_sent',			
		'number_opened',				
		'number_clicked',			
		'number_unsub',
		'number_bounce',		
		'number_forward',	
	);
	// рациональные
	private static $float_cols = array(
		
	);	
	
	private $is_status_changed = false;
	private $previous_status   = false;	
	
	private $data     = array();		
	private $products = null;		
	private $fetched  = false;
	private $args     = array( 'col'   => '', 'value' => '' );	
	private $exists   = false; // если существует строка в БД

	public function __construct( $value = false, $col = 'id' ) 
	{	
		if ( empty($value) )
			return;
			
		if ( is_array( $value ) ) 
		{
			$this->set( $value );
			return;
		}
		if ( ! in_array( $col, array( 'id' ) ) )
			return;
					
		$this->args = array( 'col' => $col, 'value' => $value );		
		// Если идентификатор указан, попытаться получить из кэша
		if ( $col == 'id' ) 
		{			
			$this->data = wp_cache_get( $value, 'usam_newsletter' );
		}			
		// кэш существует
		if ( $this->data ) 
		{
			$this->fetched = true;
			$this->exists = true;
			return;
		}
		else
			$this->fetch();
	}
	
	private static function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	/**
	 * Сохранить в кэш переданный объект
	*/
	public static function update_cache( &$newsletter ) 
	{
		$id = $newsletter->get( 'id' );	
		wp_cache_set( $id, $newsletter->data, 'usam_newsletter' );		
		do_action( 'usam_newsletter_update_cache', $newsletter );
	}

	/**
	 * Удалить кеш	 
	 */
	public static function delete_cache( $value, $col = 'id' ) 
	{
		$newsletter = new USAM_Newsletter( $value, $col );
		wp_cache_delete( $newsletter->get( 'id' ), 'usam_newsletter' );	
		do_action( 'usam_newsletter_delete_cache', $newsletter, $value, $col );	
	}			

	/**
	 *  Удалить
	 */
	public function delete( ) 
	{		
		global  $wpdb;
		
		do_action( 'usam_newsletter_before_delete', $this );
		
		$id = $this->get( 'id' );			
		self::delete_cache( $id );		
		
		$wpdb->query( "DELETE FROM ".USAM_TABLE_NEWSLETTER_TRIGGERED." WHERE newsletter_id= '$id'");	
		$wpdb->query( "DELETE FROM ".USAM_TABLE_NEWSLETTER_LISTS." WHERE newsletter_id = '$id'");	
		$wpdb->query( "DELETE FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." WHERE newsletter_id = '$id'");	
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_NEWSLETTER_TEMPLATES." WHERE id = '$id'");	
		
		do_action( 'usam_newsletter_delete', $id );
		return $result;
	}		
	
	/**
	 * Выбирает фактические записи из базы данных
	 */
	private function fetch() 
	{
		global $wpdb;
		if ( $this->fetched )
			return;

		if ( ! $this->args['col'] || ! $this->args['value'] )
			return;

		extract( $this->args );

		$format = self::get_column_format( $col );
		$sql = $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_NEWSLETTER_TEMPLATES." WHERE {$col} = {$format}", $value );

		$this->exists = false;		
		if ( $data = $wpdb->get_row( $sql, ARRAY_A ) ) 
		{			
			$data['data'] = unserialize(base64_decode($data['data']));		
			$data['body'] = htmlspecialchars_decode($data['body']);	
			
			$this->exists = true;
			$this->data = apply_filters( 'usam_newsletter_data', $data );			
			$this->fetched = true;				
			self::update_cache( $this );
		}			
		do_action( 'usam_newsletter_fetched', $this );	
		$this->fetched = true;			
	}

	/**
	 * Если строка существует в БД
	 * @since 4.9
	 */
	public function exists() 
	{		
		$this->fetch();
		return $this->exists;
	}
	/**
	 * Возвращает значение указанного свойства
	 * @since 4.9
	 */
	public function get( $key ) 
	{
		if ( empty( $this->data ) || ! array_key_exists( $key, $this->data ) )
			$this->fetch();
		
		if ( isset( $this->data[$key] ) )
			$value = $this->data[$key];		
		else
			$value = null;
		return apply_filters( 'usam_newsletter_get_property', $value, $key, $this );
	}
	
	/**
	 * Возвращает строку заказа из базы данных в виде ассоциативного массива
	 * @since 4.9
	 */
	public function get_data()
	{
		if ( empty( $this->data ) )
			$this->fetch();

		return apply_filters( 'usam_newsletter_get_data', $this->data, $this );
	}

	/**
	 * Устанавливает свойство до определенного значения. Эта функция принимает ключ и значение в качестве аргументов, или ассоциативный массив, содержащий пары ключ-значение.
	 * @since 4.9
	 */
	public function set( $key, $value = null ) 
	{		
		if ( is_array( $key ) ) 
			$properties = $key;
		else 
		{
			if ( is_null( $value ) )
				return $this;
			$properties = array( $key => $value );			
		}			
		if ( ! is_array($this->data) )
			$this->data = array();		

		if ( array_key_exists( 'status', $properties ) ) 
		{	
			$this->previous_status = $this->get( 'status' );
			if ( $properties['status'] != $this->previous_status )
				$this->is_status_changed = true;			
		}						
		foreach ( $properties as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )
				$this->data[$key] = $value;
		}
		$this->data = apply_filters( 'usam_newsletter_set_properties', $this->data, $this );			
		return $this;
	}

	/**
	 * Возвращает массив, содержащий отформатированные параметры
	 * @since 4.9
	 */
	private function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = self::get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}	
	
	/**
	 * Сохраняет в базу данных	
	 */
	public function save()
	{
		global $wpdb, $user_ID;

		do_action( 'usam_newsletter_pre_save', $this );	
		$where_col = $this->args['col'];		
		
		$this->data['date_update'] = date( "Y-m-d H:i:s" );	
			
		if ( isset($this->data['data']) )
			$this->data['data'] = base64_encode(serialize( $this->data['data'] ));
	
		if ( isset($this->data['body']) )
		{
			$this->data['body'] = wp_encode_emoji($this->data['body']);
			$this->data['body'] = htmlspecialchars($this->data['body'], ENT_NOQUOTES, 'UTF-8');
		}			
		$result = false;	
		if ( $where_col ) 
		{	
			$where_val = $this->args['value'];			
			$where_format = self::get_column_format( $where_col );	
						
			do_action( 'usam_newsletter_pre_update', $this );			
			self::delete_cache( $where_val, $where_col );
			
			$where = array( $this->args['col'] => $where_format);	
				
			$this->data = apply_filters( 'usam_newsletter_update_data', $this->data );							
			$format = $this->get_data_format( );	
			$result = $wpdb->update( USAM_TABLE_NEWSLETTER_TEMPLATES, $this->data, array( $where_col => $where_val ), $format, $where_format );	
				
	/*	
		//unset($this->data['body']);
			$set = array();
			foreach( $this->data as $key => $value)
			{							
				if (  $key == 'sent_at' )
				{
					if ( empty($value) )
						$set[] = "{$key}=NULL";
					else					
						$set[] = "{$key}='".date( "Y-m-d H:i:s", strtotime( $value ) )."'";
				}
				else
					$set[] = "{$key}='".esc_sql($value)."'";						
			}					
			$result = $wpdb->query( $wpdb->prepare("UPDATE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` SET ".implode( ', ', $set )." WHERE $where_col='$where_format'", $where_val) );	
			
			*/
			if ( $result ) 
			{ 		
				
			}
			do_action( 'usam_newsletter_update', $this );
		} 
		else 
		{   
			do_action( 'usam_newsletter_pre_insert' );							
			if ( isset($this->data['id']) )
				unset($this->data['id']);
			
			if ( isset($this->data['sent_at']) )
				unset($this->data['sent_at']);
			
			$this->data['date_insert'] = date( "Y-m-d H:i:s" );				

			if ( !isset($this->data['status']) )
				$this->data['status'] = 0;	
					
			if ( empty($this->data['type']) )
				$this->data['type'] = 'mail';		

			if ( empty($this->data['class']) )
				$this->data['class'] = 'S';				
			
			if ( !isset($this->data['subject']) )
				$this->data['subject'] = '';
			
			if ( !isset($this->data['body']) )
				$this->data['body'] = '';
			
			if ( !isset($this->data['mailbox_id']) )
				$this->data['mailbox_id'] = 0;

			$this->data['number_sent'] = 0;
			$this->data['number_opened'] = 0;
			$this->data['number_clicked'] = 0;
			$this->data['number_unsub'] = 0;
			$this->data['number_bounce'] = 0;
			$this->data['number_forward'] = 0;
			if ( empty($this->data['mailbox_id']) && $this->data['class'] == 'mail' )	
			{
				$this->data['mailbox_id'] = get_option("usam_return_email");	
			}					
			$this->data = apply_filters( 'usam_newsletter_insert_data', $this->data );			
			$format = $this->get_data_format( );				
			
			$result = $wpdb->insert( USAM_TABLE_NEWSLETTER_TEMPLATES, $this->data, $format );	
			if ( $result ) 
			{
				$this->set( 'id', $wpdb->insert_id );						
				$this->args = array('col' => 'id',  'value' => $wpdb->insert_id, );				
			}
			do_action( 'usam_newsletter_insert', $this );
		} 		
		do_action( 'usam_newsletter_save', $this );

		return $result;
	}	
}

// Получить
function usam_get_newsletter( $id )
{
	$newsletter = new USAM_Newsletter( $id );
	return $newsletter->get_data( );	
}

// Добавить 
function usam_insert_newsletter( $data )
{
	$newsletter = new USAM_Newsletter( $data );	
	$result = $newsletter->save();
	return $newsletter->get('id');
}

// Обновить 
function usam_update_newsletter( $id, $data )
{
	$newsletter = new USAM_Newsletter( $id );
	$newsletter->set( $data );
	return $newsletter->save();
}

// Удалить
function usam_delete_newsletter( $id )
{
	$newsletter = new USAM_Newsletter( $id );
	return $newsletter->delete();
}
?>