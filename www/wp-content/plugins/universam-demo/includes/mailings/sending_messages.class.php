<?php
// Класс отвечает за отправку рассылки
new USAM_Sending_Messages( );
final class USAM_Sending_Messages
{
	public  $message = array();
	public  $errors  = array();
	private $mailing;
	
	function __construct( )
	{					
		add_action( 'usam_five_minutes_cron_task', array( $this, 'send_newsletters') ); 	
		add_action( 'usam_twicehourly_cron_task', array( $this, 'send_trigger_cron') ); 	
		
		add_action( 'usam_order_paid', array( $this, 'send_trigger_order_paid'), 10, 1 ); 		
		add_action( 'wp_login', array( $this, 'send_trigger_site_login'), 10, 2 ); 
		add_action( 'usam_update_order_status', array( $this, 'send_trigger_order_status_change'), 10, 4 ); 
		add_action( 'usam_subscribe_for_newsletter', array( $this, 'send_trigger_subscribe_to_newsletter'), 10, 1 ); 
	}
	
	private function set_error( $error )
	{			
		$this->errors[] = sprintf( __('Запуск отправки рассылки. Ошибка: %s'), $error );
	}
	
	private function set_log_file( )
	{	
		usam_log_file( $this->errors );
		$this->errors = array();
	}	
	
// ТРИГЕРНАЯ РАССЫЛКА
	public function send_trigger_cron( )
	{			
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		require_once( USAM_FILE_PATH . '/includes/basket/users_basket_query.class.php' );
		$triggers = usam_get_trigger_newsletters( array( 'event_start' => array( 'adding_newsletter', 'basket_forgotten', 'order_status', 'sale_dont_buy') ) );
		foreach ( $triggers as $trigger ) 
		{		
			$callback = 'send_trigger_'.$trigger->event_start;
			
			$trigger->condition = unserialize( $trigger->condition );	
			$trigger->data = unserialize( $trigger->data );		
			$newsletter = new USAM_Send_Newsletter();
			if ( method_exists( $newsletter, $callback )) 
			{ 
				$result = $newsletter->$callback( $trigger );
				if ( $result )
				{
					$update = array( 'number_sent' => $result );
					usam_update_newsletter(  $trigger->newsletter_id, $update );
				}
			}
		}		
	}	
	
//Новому подписчику		
	public function send_trigger_subscribe_to_newsletter( $communication_id )
	{ 
		$triggers = usam_get_trigger_newsletters( array( 'event_start' => 'subscribe_to_newsletter') );	
		
		if ( empty($triggers) )
			return false;

		$communication = usam_get_communication( $communication_id );
		foreach ( $triggers as $trigger ) 
		{						
			$this->set_triget( $communication['value'], $trigger );		
		}					
	}	
	
// Изменение статуса заказа
	public function send_trigger_order_status_change( $order_id, $current_status, $previous_status, $order )
	{
		$triggers = usam_get_trigger_newsletters( array( 'event_start' => 'order_status_change') );	
		
		if ( empty($triggers) )
			return false;
			
		$email = usam_get_buyers_email( $order_id );
		
		if ( empty($email) )
			return false;
		
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		$_newsletter = new USAM_Send_Newsletter();
		
		foreach ( $triggers as $trigger ) 
		{						
			$_newsletter->set_triget( $email, $trigger );		
		}		
	}

// Заход на сайт
	public function send_trigger_site_login( $user_login, $user )
	{
		$triggers = usam_get_trigger_newsletters( array( 'event_start' => 'sender_user_auth') );	
		
		if ( empty($triggers) )
			return false;
	
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		$_newsletter = new USAM_Send_Newsletter();
		
		foreach ( $triggers as $trigger ) 
		{		
			$_newsletter->set_triget( $user->user_email, $trigger );			
		}			
	}

// Оплата заказа
	public function send_trigger_order_paid( $_order )
	{			
		$triggers = usam_get_trigger_newsletters( array( 'event_start' => 'order_paid') );	
		
		if ( empty($triggers) )
			return false;
		
		$order_id = $_order->get('id');
		$email = usam_get_buyers_email( $order_id );
		
		if ( empty($email) )
			return false;
		
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		$_newsletter = new USAM_Send_Newsletter();
		
		foreach ( $triggers as $trigger ) 
		{				
			$_newsletter->set_triget( $email, $trigger );						
		}			
	}
				
	public function send_newsletters( )
	{	
		require_once( USAM_FILE_PATH . '/includes/mailings/newsletter_query.class.php' );
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		$_newsletter = new USAM_Send_Newsletter();
		
		$newsletters = usam_get_newsletters( array( 'status' => 5, 'class' => 'S', 'count_total' => true ) );			
		foreach ( $newsletters as $newsletter ) 
		{			
			$result = $_newsletter->send_newsletter( $newsletter );
		}		
		$this->set_log_file( );
	}	
}




function usam_get_user_stat_mailing( $id )
{	
	global $wpdb;	
	$id = (int)$id;	
	$result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." WHERE id = '%d'", $id ), ARRAY_A );	
	return $result;
}

function usam_update_user_stat_newsletter( $id, $update )
{	
	global $wpdb;
	
	$id = (int)$id;	

	$format_default = array( 'sent_at' => '%s', 'opened_at' => '%s', 'clicked' => '%d', 'status' => '%d', 'unsub' => '%d' );	
	$formats = array();	
	foreach ( $update as $key => $value ) 
	{
		if ( isset($format_default[$key]) )	
			$formats[] = $format_default[$key];	
		else
			unset($update[$key]);
	}		
	$result = $wpdb->update( USAM_TABLE_NEWSLETTER_USER_STAT, $update, array( 'id' => $id ), $formats, array( '%d' ) );	
	return $result;
}

function usam_update_newsletter_lists( $newsletter_id, $lists )
{	
	global $wpdb;
	
	$wpdb->query( $wpdb->prepare("DELETE FROM ".USAM_TABLE_NEWSLETTER_LISTS." WHERE newsletter_id ='%d'", $newsletter_id) );
	if ( is_array($lists) )
		foreach ($lists as $list_id )
		{
			$result = $wpdb->insert( USAM_TABLE_NEWSLETTER_LISTS, array( 'newsletter_id' => $newsletter_id, 'list' => $list_id ), array( '%d', '%d', ) );
		}	
}

function usam_get_newsletter_list( $newsletter_id )
{	
	global $wpdb;		
	$lists = $wpdb->get_col( $wpdb->prepare( "SELECT list FROM ".USAM_TABLE_NEWSLETTER_LISTS." WHERE newsletter_id = '%d'", $newsletter_id ) );
	return $lists;
}

// Получить подписчиков по переданным спискам
function usam_get_subscribers_by_lists( $lists, $type )
{
	global $wpdb;	
	$communication_ids = $wpdb->get_col("SELECT sl.id FROM ".USAM_TABLE_MEANS_COMMUNICATION." AS sl LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." AS s_list ON ( s_list.id_communication = sl.id )
		WHERE s_list.list IN ('".implode( "','", $lists )."') AND sl.type ='$type' AND s_list.status!=2" );	
	return $communication_ids;
}

// Добавить список в статистику
function usam_add_list_newsletter_user_stat( $newsletter_id, $type )
{
	$lists = usam_get_newsletter_list( $newsletter_id );
	if ( empty($lists) )
		return false;
	
	$communication_ids = usam_get_subscribers_by_lists( $lists, $type );
	
	foreach ( $communication_ids as $id_communication ) 	
	{			
		$args = array( 'newsletter_id' => $newsletter_id, 'id_communication' => $id_communication );
		usam_set_mailing_user_stat( $args );
	}
	return true;
}


// Добавить электронный адрес в статистику
function usam_set_mailing_user_stat( $args )
{
	global $wpdb;	

	$insert = array();
	if ( isset($args['newsletter_id']) )
		$insert['newsletter_id'] = $args['newsletter_id'];	
	else
		return false;
	
	if ( isset($args['id_communication']) )
		$insert['id_communication'] = $args['id_communication'];	
	else
		return false;
				
	if ( isset($args['sent_at']) )
		$insert['sent_at'] = $args['sent_at'];	
	
	if ( isset($args['opened_at']) )
		$insert['opened_at'] = $args['opened_at'];	
	
	if ( isset($args['status']) )
		$insert['status'] = $args['status'];	
	
	$formats = array();
	foreach( $insert as $key => $value )
	{
		switch ( $key ) 
		{
			case 'id':
			case 'id_communication':
			case 'newsletter_id':					
				$formats[] = '%d';							
			break;				
			case 'sent_at':
			case 'opened_at':
			case 'status':				
				$formats[] = '%s';	
			break;
			default:
				unset($insert[$key]);
		}			
	}	
	$result = $wpdb->insert( USAM_TABLE_NEWSLETTER_USER_STAT, $insert, $formats );	
	return $wpdb->insert_id;
}

// Найти название рассылки по id
function usam_get_name_list_of_subscribers( $id )
{
	$lists = usam_get_subscribers_list(); 
	$name = '';
	foreach ( $lists as $list )
	{
		if ( $list['id'] == $id )
			$name = $list['title'];
	}
	return $name;
}

function usam_get_subscribers_list( )
{
	$option = get_option('usam_list_of_subscribers', array()); 
	$lists = maybe_unserialize( $option );	
	return $lists;
}


function usam_update_location_subscriber( $communication_id )
{
	$communication = usam_get_communication( $communication_id );
	$location = usam_get_customer_location();
	if ( $location )
	{
		$update = array( 'id' => $communication['contact_id'], 'location_id' => $location );		
		if ( $communication['customer_type'] == 'contact' )
			usam_update_contact( $update );		
	//	else
	//		usam_update_company( $communication['contact_id'], $update );
	}	
}
?>