<?php
// Класс отвечает за отправку рассылки
final class USAM_Send_Newsletter
{
	private $mailing;
	private $limit_send_mail = 25;
	
	private function get_subscribed_url( )
	{			
		return $subscribed_url = get_bloginfo( 'url' ).'/your-subscribed';
	}	
	
// ТРИГЕРНАЯ РАССЫЛКА	
	public function set_triget( $email, $trigger )
	{
		$this->mailing = usam_get_newsletter( $trigger->newsletter_id );	
		$message = $this->get_message();	
		$communication = usam_get_communication_by_type( array('value' => $email, 'type' => 'email' ) );	
		if ( !empty($communication) )
		{
			$message = $this->process_args( $communication, $message );	
			$communication_id = $communication['id'];
		}
		else
			$communication_id = '';
		return $this->send_triget( $email, $trigger->newsletter_id, $message, $communication_id );		
	}	
	
	private function get_args( $communication )
	{
		if ( $communication['customer_type'] == 'contact' )
		{
			$contact = usam_get_contact( $communication['contact_id'] );			
			$args = array( 
				'name' => $contact['lastname'].' '.$contact['patronymic'], 
				'lastname' => $contact['lastname'], 
				'firstname' => $contact['firstname'], 
				'patronymic' => $contact['patronymic'], //Отчество
				'birthday' => $contact['birthday'], 			
				'address' => $contact['address'], 				
				'postal_code' => $contact['postal_code'], 
			);		
			$locations = usam_get_array_locations_up( $contact['location_id'], 'name', 'code' );
			$args += $locations;
		}
		else
		{
			$company = new USAM_Company( $communication['contact_id'] );	
			$data = $company->get_data();
			$args = array( 
				'company_name' => $data['name'],
				'name' => $data['name'],
			);
		}
		$default = array( 
				'name' => '', 
				'lastname' => '', 
				'firstname' => '', 
				'patronymic' => '', 
				'birthday' => '', 			
				'address' => '', 				
				'postal_code' => '', 
				'company_name' => '', 
			);	
		$args = array_merge ($default, $args);		
		return $args;
	}
	
	private function process_args( $communication, $message )
	{
		$shortcode = new USAM_Shortcode();
		return $shortcode->process_args( $this->get_args( $communication ), $message, 'no' );
	}
	
//При добавлении в рассылку
	public function send_trigger_adding_newsletter( $trigger )
	{
		global $wpdb;							
		
		$this->mailing = usam_get_newsletter( $trigger->newsletter_id );				
		$number_sent = $this->mailing['number_sent'];	
		
		if ( empty($trigger->condition['lists']) )
			return 0;
		
		if ( empty($trigger->data['communication_id']) )	
		{
			if ( empty($trigger->condition['run_for_old_data']) )
			{
				$id = $wpdb->get_var("SELECT id FROM ".USAM_TABLE_MEANS_COMMUNICATION." ORDER BY id DESC LIMIT 1 " ); 
				$update = array( 'data' => array( 'communication_id' => $id ) );	
				usam_update_trigger( $trigger->id, $update );		
				return 0;	
			}
			$communications = $wpdb->get_results("SELECT sl.* FROM ".USAM_TABLE_MEANS_COMMUNICATION." AS sl LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." AS s_list ON ( s_list.id_communication = sl.id ) WHERE s_list.list IN ('".implode( "','", $trigger->condition['lists'] )."') AND sl.type ='email' AND s_list.status!=2", ARRAY_A );
		}
		else
		{
			$communications = $wpdb->get_results( "SELECT sl.* FROM ".USAM_TABLE_MEANS_COMMUNICATION." AS sl LEFT JOIN ".USAM_TABLE_SUBSCRIBER_LISTS." AS s_list ON ( s_list.id_communication = sl.id ) WHERE sl.id >'".$trigger->data['communication_id']."'  AND s_list.list IN ('".implode( "','", $trigger->condition['lists'] )."') AND sl.type ='email' AND s_list.status!=2", ARRAY_A  );			
		}					
		if ( empty($communications) )
			return 0;
		
		$id = 0;		
		$message = $this->get_message();				
		foreach ( $communications as $communication ) 
		{			
			$send_message = $this->process_args( $communication, $message );
			
			$this->send_triget( $communication['value'], $trigger->newsletter_id, $send_message, $communication['id'] );				
			$number_sent ++;
			if ( $id < $communication['id'] )
				$id = $communication['id'];			
		} 				
		$update = array( 'data' => array( 'communication_id' => $id ) );	
		usam_update_trigger( $trigger->id, $update );		
		return $number_sent;					
	}
	
	// Статус заказа
	public function send_trigger_order_status( $trigger )
	{
		$h = date_i18n('H');			
		$i = date('i');			
		$time_start = explode(":", $trigger->condition['time_start']);	
		$number_sent = 0;
		if ( $time_start[0] == $h && ( ( $time_start[1] == '00' && $i >= '00' && $i < '30' ) || ( $time_start[1] == '30' && $i >= '30' && $i <= '59')) )			
		{ 		
			$query = array( 									
				'fields' => 'id',			
				'status' => $trigger->condition['status'], 		
			);				
			$order_ids = usam_get_orders( $query );			
		
			if ( empty($order_ids) )
				return 0;
			
			$emails = (array)usam_get_order_props_value( array( 'fields' => array( 'order_id', 'value'), 'order_id__in' => $order_ids, 'unique_name' => 'billingemail' ) );				
			$this->mailing = usam_get_newsletter( $trigger->newsletter_id );						
			$number_sent = $this->mailing['number_sent'];	
			
			if ( !empty($emails) )
			{				
				$message = $this->get_message();
				foreach ( $emails as $email ) 
				{						
					$order_shortcode = new USAM_Order_Shortcode( $email->order_id );
					$message_html = $order_shortcode->get_html( $message );							

					$this->send_triget( $email->value, $trigger->newsletter_id, $message_html );						
					$number_sent ++;								
				}
			}
		}
		return $number_sent;
	}
	
	// Забытая корзина
	public function send_trigger_basket_forgotten( )
	{
		$h = date_i18n('H');
		$i = date('i');
		$time_start = explode(":", $trigger->condition['time_start']);	
		// проверить время отправления
		$number_sent = 0;
		if ( $time_start[0] == $h && ( ( $time_start[1] == '00' && $i >= '00' && $i < '30' ) || ( $time_start[1] == '30' && $i >= '30' && $i <= '59')) )			
		{ 
			$query = array( 				
				'date_query' => array( array( 'before' => $trigger->condition['days_basket_forgotten'].' days ago', 'inclusive' => true, ) ),
				'number' => 1000,	
				'count_total' => false,
				'cache_results' => false,		
				'cache_products' => false,			
				'fields' => array('user_id', 'id' ),
			);		
			if ( !isset($trigger->data['basket_id']) )	
			{
				$trigger->data['basket_id'] = 0;
				if ( !$trigger->condition['days_basket_forgotten'] )			
				{
					$day = $trigger->condition['days_basket_forgotten']+1;
					$query['date_query'][] = array( 'after' => "$day days ago",'inclusive' => true, );	
				}
			}		
			$orders = new USAM_Users_Basket_Query();	
			$orders->prepare_query( $query );		
			$orders->query_where .= " AND id>'".$trigger->data['basket_id']."' AND user_id>'0'";		
			$orders->query();			
			$results = $orders->get_results( );
			
			if ( empty($results) )
				return $number_sent;
			
			$user_ids = array();
			foreach ( $results as $result ) 
			{
				$user_ids[] = $result->user_id;
				$trigger->data['basket_id'] = $result->id;
			}
			$args = array(					
				'include'      => $user_ids,
				'count_total'  => false,
				'fields'       => array('user_email'),
			);
			$users = get_users( $args );
			$emails = wp_list_pluck( $users, 'user_email' );
			$this->mailing = usam_get_newsletter( $trigger->newsletter_id );					
			$number_sent = $this->mailing['number_sent'];			
			if ( !empty($emails) )
			{					
				$message = $this->get_message();
				foreach ( $emails as $email ) 
				{									
					$this->send_triget( $email, $trigger->newsletter_id, $message );						
					$number_sent ++;								
				}				
			}			
			$update = array( 'data' => array( 'basket_id' => $trigger->data['basket_id'] ) );	
			usam_update_trigger( $trigger->id, $update );
		}
		return $number_sent;
	}
	
// Давно не покупал
	public function send_trigger_sale_dont_buy( $trigger )
	{
		$number_sent = 0;
		$h = date_i18n('H');
		$i = date('i');
		$time_start = explode(":", $trigger->condition['time_start']);	
		if ( $time_start[0] == $h && ( ( $time_start[1] == '00' && $i >= '00' && $i < '30' ) || ( $time_start[1] == '30' && $i >= '30' && $i <= '59')) )				
		{ 
			$query = array( 						
				'date_query' => array( array( 'before' => $trigger->condition['days_dont_buy'].' days ago', 'inclusive' => true, ) ),
				'order' => 'DESC',					
				'fields' => 'id',			
				'status' => 'closed',		
				'count_total' => false,					
			);						
			if ( !isset($trigger->data['order_id']) )	
			{
				$trigger->data['order_id'] = 0;
				if ( !$trigger->condition['run_for_old_data'] )			
				{
					$day = $trigger->condition['days_dont_buy']+1;
					$query['date_query'][] = array( 'after' => "$day days ago",'inclusive' => true, );	
				}
			}				
			$orders = new USAM_Orders_Query();	
			$orders->prepare_query( $query );		
			$orders->query_where .= " AND id>'".$trigger->data['order_id']."'";		
			$orders->query();			
			$order_ids = $orders->get_results( );
				
			if ( empty($order_ids) )
				return $number_sent;						
	
			$order_id = end($order_ids);	
			$emails1 = (array)usam_get_order_props_value( array( 'fields' => 'value', 'order_id__in' => $order_ids, 'unique_name' => 'billingemail' ) );
			
			$query = array( 'date_query' => array( 'after' => $trigger->condition['days_dont_buy'].' days ago' ), 'fields' => 'id', 'status' => 'closed' );		
			$orders = new USAM_Orders_Query( $query );	
			$order_ids = $orders->get_results();	
			
			if ( !empty($order_ids) )
			{
				$emails2 = (array)usam_get_order_props_value( array( 'fields' => 'value', 'order_id__in' => $order_ids, 'unique_name' => 'billingemail' ) );	
				$emails = array_diff($emails1, $emails2);	
				
				unset($emails2);
			}
			else
				$emails = $emails1;	
			
			unset($emails1);			
	
			$this->mailing = usam_get_newsletter( $trigger->newsletter_id );		
			$number_sent = $this->mailing['number_sent'];			
			if ( !empty($emails) )
			{					
				$message = $this->get_message();
				foreach ( $emails as $email ) 
				{									
					$this->send_triget( $email, $trigger->newsletter_id, $message );						
					$number_sent ++;								
				}				
			}			
			$update = array( 'data' => array( 'order_id' => $order_id ) );	
			usam_update_trigger( $trigger->id, $update );
		}
		return $number_sent;
	}	
	
	// Отправка триггерной рассылки
	public function send_triget( $email, $newsletter_id, $message, $communication_id = 0 )
	{					
		$headers = $this->get_header();			

			return false;
		
	}	
	
	public function send_sms( $newsletter, $stat )
	{					
		return false;		
	}
	
	public function php_mailer( $phpmailer ) 
	{
		$mailbox = usam_get_mailbox( $this->mailing['mailbox_id'] );
		if ( empty($mailbox['smtpserver']) )
			return false;
			
		$phpmailer->isSMTP();     
		$phpmailer->Host = $mailbox['smtpserver'];
		$phpmailer->SMTPAuth = true; // использовать имя и пароль для аутентификации.
		$phpmailer->Port = $mailbox['smtpport'];
		$phpmailer->Username = $mailbox['smtpuser'];
		$phpmailer->Password = $$mailbox['smtppass'];
		if( $mailbox['smtp_secure'] != 'None' )
		{
			$phpmailer->SMTPSecure = $mailbox['smtp_secure'];
		}  
	}
	
	public function send_mail( $newsletter_id, $stat )
	{	
		return false;
	}
	
	// Полностью отправлено
	public function completely_sent( $id )
	{	
		$update = array( 'sent_at' => date( "Y-m-d H:i:s" ), 'status' => 6 );
		usam_update_newsletter( $id, $update );	
	}
// Добавить кнопки	
	private function get_button( $stat_id )
	{
		$url = $this->get_subscribed_url();	
		$open_link = add_query_arg(array('stat_id' => $stat_id, 'usam_action' => 'mailing_open'), $url );
		$unsub_link = add_query_arg(array('stat_id' => $stat_id, 'usam_action' => 'mailing_unsub'), $url );
		$edit_link = add_query_arg(array('stat_id' => $stat_id, 'usam_action' => 'mailing_edit'), $url );
		
		$html = '
<p class="unsubscribe_container" style="font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #FFFFFF;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;">
	<a target="_blank" style="color: #000000;color: #000000 !important;background-color: #FFFFFF;border: 0;" href="'.$unsub_link.'">'.__('Отписаться','usam').'</a>&nbsp;-&nbsp;
	<a target="_blank" style="color: #000000;color: #000000
!important;background-color: #FFFFFF;border: 0;" href="'.$edit_link.'">'.__('Изменить параметры подписки на рассылку','usam').'</a><br><br>
</p><img style="width:1px;height:1px;" src="'.$open_link.'"/>';
		return $html;
	}
	
	private function get_message( $stat_id = '' )
	{		
		$message = $this->mailing['body'];
		if ( empty($message) )
			return '';
	
		$dom = new DOMDocument;
		@$dom->loadHTML( $message );
		foreach ($dom->getElementsByTagName('a') as $node) 
		{
			if( $node->hasAttribute( 'href' ) ) 
			{
				$url = $node->getAttribute( 'href' );				
				if ( $stat_id != '' )
					$url = add_query_arg( array('stat_id' => $stat_id, 'usam_action' => 'm_click' ), $url );
				
				$node->setAttribute( 'href', $url );				
			}
		}
		$message = $dom->saveHTML();
		return $message;		
	}
	
	private function get_header( )
	{		
		$mailbox = usam_get_mailbox( $this->mailing['mailbox_id'] );
		if ( empty($mailbox) )
			return false;
					
		$headers = "From: ".$mailbox['name']." <".$mailbox['email'].">" . "\r\n";		
		$headers .= "Reply-To: ".$mailbox['name']." <".$mailbox['email'].">" . "\r\n"; // Ответ возвращать
	
	//	$headers.= "Disposition-Notification-To: <".$this->mailing['from_email'].">\r\n";              // подтверждение о прочтении
	  //$headers.= "X-Confirm-Reading-To: <it@radov39.ru>\r\n";   // подтверждение о прочтении
	//	$headers.= "Return-Receipt-To: <".$this->mailing['from_email'].">\r\n"; 	                   // подтверждение о доставке	
		$headers.= "X-email_id: <".$this->mailing['id'].">\r\n"; 
		$headers.= "X-Priority: <3>\r\n";
		$headers.= "List-id: <".$this->mailing['id'].">\r\n";
		return $headers;
	}
	
	public function send_mail_preview( $id, $email )
	{					
		$this->mailing = usam_get_newsletter( $id );		
		$message = $this->get_message();
		$headers = $this->get_header();
	
		if ( !$headers || empty($message) )
			return false;
		
		add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
		
		$email_sent = wp_mail( $email, $this->mailing['subject'], $message, $headers );			
		remove_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
		return $email_sent;
	}
	
	public function send_newsletter( $newsletter )
	{	
		global $wpdb;

		$newsletter = (object)$newsletter;
		
		$result = false;
		
		$sql = "SELECT stat.id_communication, mc.value, stat.id AS stat_id  FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat LEFT JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS mc ON ( mc.id = stat.id_communication ) 
		WHERE stat.status = '0' AND stat.newsletter_id='$newsletter->id' LIMIT $this->limit_send_mail";
		$stat = $wpdb->get_results( $sql );		
		
		if ( !empty( $stat ) )
		{ 
			if ( $newsletter->type == 'mail' )
				$result = $this->send_mail( $newsletter->id, $stat );
			elseif ( $newsletter->type == 'sms' )
				$result = $this->send_sms( $newsletter, $stat );
		}				
		$count = count($stat);			
		if ( $count == 0 )
		{
			$this->completely_sent( $newsletter->id );			
		}
		return $result;
	}	
}
?>