<?php
// Добавить в подписку
function usam_set_subscriber_lists( $list )
{
	global $wpdb;		
	if ( empty($list['id']) || empty($list['id_communication']) )
		return false;
	
	$list_id = absint($list['id']);
	$id_communication = absint($list['id_communication']);	
	$status = isset($list['status'])?absint($list['status']):1;
	
	$sql = "INSERT INTO `".USAM_TABLE_SUBSCRIBER_LISTS."` (`id_communication`,`list`) VALUES ('%d','%d') ON DUPLICATE KEY UPDATE `list`='%d', `status`='%d'";	
	$update = $wpdb->query( $wpdb->prepare($sql, $id_communication, $list_id, $list_id, $status ) );
	
	if ( $status == 2 )
		do_action( 'usam_unsubscribed_newsletter', $id_communication, $list_id ); // отписался	
	else
		do_action( 'usam_ubscribed_newsletter', $id_communication, $list_id ); // подписался

	return $update;	
}

// Удалить подписку
function usam_delete_subscriber_lists( $lists )
{
	global $wpdb;		

	foreach ( $lists as $id_communication => $id_list )
	{
		$wpdb->query( $wpdb->prepare("DELETE FROM ".USAM_TABLE_SUBSCRIBER_LISTS." WHERE id_communication = '%d' AND list = '%d'", $id_communication, $id_list ));	
	}	
}

// Удалить подписку
function usam_delete_subscriber_list( $id_communication )
{
	global $wpdb;			
	$wpdb->query( $wpdb->prepare("DELETE FROM ".USAM_TABLE_SUBSCRIBER_LISTS." WHERE id_communication = '%d'", $id_communication ));		
}

// Получить списки
function usam_get_subscriber_list( $condition, $colum = 'id_communication' )
{
	global $wpdb;	
	if ( is_array($condition) )
		$in = implode("','",$condition);
	else
		$in = $condition;
	$sql = "SELECT * FROM " . USAM_TABLE_SUBSCRIBER_LISTS . " WHERE `$colum` IN ('".$in."')";	
	$lists = $wpdb->get_results( $sql, ARRAY_A );	
	return $lists;
}

// Получить подписчика
function usam_get_subscriber_data( $data )
{	
	if ( !empty($data) )
	{
		global $wpdb;
	
		$keys2 = array_keys($data['email']);	
		$keys1 = array_keys($data['phone']);			
		$keys = array_merge($keys2, $keys1);	
	
		if ( !empty($keys) )
		{ 
			$lists = usam_get_subscriber_list( $keys, 'id_communication' );			
			foreach ( $lists as $list )
			{			
				if( isset($data['email'][$list['id_communication']]) )
				{
					$data['email'][$list['id_communication']]['list'][$list['list']] = array( 'list' => $list['list'], 'status' => $list['status'] );					
				}
				elseif ( isset($data['phone'][$list['id_communication']]) )
				{
					$data['phone'][$list['id_communication']]['list'][$list['list']] = array( 'list' => $list['list'], 'status' => $list['status'] );		
				}				
			}
		}		
	}
	return $data;
}

// Получить данные подписчиков
function usam_get_subscribers_data( $args )
{
	global $wpdb;
	
	$number = 0;
	$offset = 0;
	
	$orderby = 'id';
	$order = 'ASC';
	
	$where = array( '1=1' );
	if ( isset($args['user_id']) )		
		$where[] = 'user_id='.$args['user_id'];	
	
	if ( isset($args['lastname']) )		
		$where[] = "lastname='".$args['lastname']."'";
	
	if ( isset($args['firstname']) )		
		$where[] = "firstname='".$args['firstname']."'";
	
	if ( isset($args['email']) )		
		$where[] = "email='".$args['email']."'";
	
	if ( isset($args['phone']) )		
		$where[] = "phone='".$args['phone']."'";
	
	if ( isset($args['status']) )		
		$where[] = 'status='.$args['status'];
	
	if ( isset($args['date_insert']) )		
		$where[] = 'date_insert='.$args['date_insert'];
	
	if ( isset($args['orderby']) )		
		$orderby = $args['orderby'];
	
	if ( isset($args['order']) )		
		$order = $args['order'];
	
	if ( isset($args['number']) )		
		$number = $args['number'];	
	
	if ( isset($args['offset']) )		
		$offset = $args['offset'];
	
	if ( $number != 0 )
		$limit = 'LIMIT $number, $offset';
	else
		$limit = '';	

	$_fields = '*';	
	if ( isset($args['fields']) && $args['fields'] != 'all')
	{		
		$colums = array( 'id', 'user_id','lastname','firstname','email','phone','status', 'date_insert');		
		if ( !is_array( $args['fields'] ) )
		{			
			if ( in_array($args['fields'], $colums) )
				$_fields = $args['fields'];
			elseif ( $args['fields'] == 'count' )
				$_fields = 'COUNT(*) AS count';	
		}
		else
		{
			$fields = array_uintersect($args['fields'], $colums);
			if ( in_array('count', $args['fields']) )
				$fields[] = 'COUNT(*) AS count';
			$_fields = implode( ' AND ', $fields );
		}			
	}	
	$_where = implode( ' AND ', $where );	
	
	$sql = "SELECT $_fields FROM `".USAM_TABLE_CONTACTS."` WHERE $_where ORDER BY $orderby $order $limit";
	if ( !is_array( $args['fields'] ) && $args['fields'] == 'count' )
	{
		$result = $wpdb->get_var( $sql );
	}
	else
	{
		$result = $wpdb->get_results( $sql );
	}
	return $result;
}


function usam_get_form_subscribe_for_newsletter( $echo = true )
{
	$search_box_text = __('Ваш адрес почты','usam');
	$button_text = __('Подписатся','usam');
	$out = "<div class='subscribe_for_newsletter'>
					<form method='post' action='' id='subscribe_for_newsletter-form'>					
						<div class='subscribe_for_newsletter-box'>							
							<input type='hidden' name='usam_action' value='subscribe_for_newsletter'>
							<input type='search' class='txt_livesearch' id='subscribe_for_newsletter-input' name='email' value='' autocomplete='off' placeholder='$search_box_text'
							onblur=\"if (this.value =='') {this.value = '$search_box_text';}\" onfocus=\"if (this.value =='$search_box_text') {this.value = '';}\">
							<span class='subscribe_for_newsletter-button' id='subscribe_for_newsletter_button'>$button_text</span>	
						</div>		
					</form>
				</div>
<script type='text/javascript'>
	jQuery(document).ready(function() 
	{		
		jQuery(document).on('click', '#subscribe_for_newsletter_button', function()
		{		
			if (jQuery('#subscribe_for_newsletter-input').val() != '' && jQuery('#subscribe_for_newsletter-input').val() != '$search_box_text') 
			{			
				jQuery('#subscribe_for_newsletter-form').submit();
			}		
		});		
	});
</script>";		
	
	if ( $echo )
		echo $out;
	else
		return $out;
}