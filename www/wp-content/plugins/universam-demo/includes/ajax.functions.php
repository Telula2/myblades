<?php
/**
 * AJAX и Init функции магазина
 * @since 3.7
 */				

new USAM_Ajax_Init_Action();
class USAM_Ajax_Init_Action extends USAM_Callback
{	
	protected $sendback;
	private   $ajax = false;
	private   $exit = true;	
	
	protected $init_parameter = 'usam_action';
	protected $ajax_parameter = 'usam_ajax_action';
	
	public function __construct() 
	{				
		if ( !is_admin() )
			add_action( 'init', array($this, 'handler'), 11 );
	}
	
	/**
	 * Обработчик для всех запросов.	
	 */
	function handler()
	{		
		if ( isset($_REQUEST[$this->ajax_parameter]) ) 
		{
			$this->ajax = true;
			$this->action = $_REQUEST[$this->ajax_parameter];
			
			$result = $this->fire_callback( );
			
			$output = array( 'is_successful' => false );	
			if ( $this->exit ) 
			{
				if ( is_wp_error( $result ) ) 
				{	// ошибки возникшие во время исполнения запросов Ajax
					$output['error'] = array(
						'code'     => $result->get_error_code(),
						'messages' => $result->get_error_messages(),
						'data'     => $result->get_error_data(),
					);
				} 
				else 
				{
					$output['is_successful'] = true;
					$output['obj'] = $result;        // Данные возвращаемые функцией
				}
				echo json_encode( $output );
				exit;				
			}
		}		
		elseif ( isset($_REQUEST[$this->init_parameter]) ) 
		{
			$this->action = $_REQUEST[$this->init_parameter];	
			$this->sendback = wp_get_referer();
			$this->sendback = remove_query_arg( array( $this->init_parameter, 'error', 'callback_error', '_wpnonce', 'nonce' ), $this->sendback );	
			
			$result = $this->fire_callback( );		
			if ( $this->error_code )
				$this->sendback = add_query_arg( 'error', $this->error_code, $this->sendback );		
			
			wp_redirect( $this->sendback );
			exit();
		}
		else
			return;
	}	
	
	function _verify_nonce( ) 
	{
		$result = $this->verify_nonce( $this->action.'_nonce' );
		if ( ! is_wp_error( $result ) )			
			return true;
		
		return false;
	}
		
	function controller_cart_html_page() 
	{
		require_once(USAM_FILE_PATH . "/includes/theme/shopping_cart_container.php");
		exit();
	}
		
	function controller_number_of_unread_user_chat_messages()
	{
		$dialog_id = usam_get_customer_meta( 'dialog_id' );
		if ( !empty($dialog_id) )	
			$count = usam_number_new_messages( $dialog_id );
		else
			$count = 0;		
		return $count;
	}
	
	/**
	 * Рассчитывает скидку купона при вводе в корзине, используется через AJAX и в нормальной загрузки страницы.
	 */
	function controller_apply_coupon( ) 
	{
		global $usam_cart;		
		
		$coupon = '';
		if ( !empty($_REQUEST['coupon_num']) )			
			$coupon = sanitize_title($_REQUEST['coupon_num']);	
		
		$usam_cart->set_properties( array( 'coupon_name' => $coupon ) );
	}
	
	
	/**
	 * Функция обновления количества, используется через AJAX и в нормальном загрузки страницы.
	 */
	function controller_remove_cart_item() 
	{
		global $usam_cart;

		if ( isset( $_REQUEST['key'] ) ) 
		{
			$key = (int)$_REQUEST['key'];			
			$usam_cart->remove_item( $key );		
		}			
		if ( $this->ajax ) 
		{
			ob_start();

			include_once( usam_get_template_file_path( 'widget-basket' ) );
			$output = ob_get_contents();		
			return $output;
		}		
	}
	
	/**
	 * Функция обновления количества, используется через AJAX и в нормальном загрузки страницы.
	 */
	function controller_update_item_quantity() 
	{
		global $usam_cart;

		if ( isset( $_REQUEST['key'] ) && isset($_REQUEST['usam_quantity']) ) 
		{
			$key = (int)$_REQUEST['key'];
			$quantity = (int)$_REQUEST['usam_quantity'];
			$usam_cart->update_quantity( $key, $quantity );				
		}		
		if ( $this->ajax ) 
		{
			ob_start();

			include_once( usam_get_template_file_path( 'widget-basket' ) );
			$output = ob_get_contents();			
			return $output;
		}
	}	
	
	function controller_buy_product()
	{
		global $usam_cart, $type_price;
				
		if ( !isset($_REQUEST['product_id']) )
			return;
				
		$product_id = apply_filters( 'usam_add_to_cart_product_id', (int)$_REQUEST['product_id'] );
		$product = get_post( $product_id );	
				
		$post_type_object = get_post_type_object( 'usam-product' );
		$permitted_post_statuses = current_user_can( $post_type_object->cap->edit_posts ) ? array( 'private', 'draft', 'pending', 'publish' ) : array( 'publish' );
		
		$parameters = array();			

		if ( ! in_array( $product->post_status, $permitted_post_statuses ) || 'usam-product' != $product->post_type )
			return false;		
		
		if ( isset($_REQUEST['usam_quantity']) && $_REQUEST['usam_quantity'] > 0 )		
			$parameters['quantity'] = (int)$_REQUEST['usam_quantity'];		
		
		$usam_cart->empty_cart( );
		$usam_cart->add_product_basket( $product_id, $parameters ); 

		$this->sendback	= usam_get_url_system_page('basket'); 
	}

	/**
	 * функция добавление в корзину используется через AJAX и в нормальном загрузки страницы.
	 */
	function controller_add_to_cart()
	{
		global $usam_cart, $type_price;
		
		$variation_values = array();		
		
		$post_type_object = get_post_type_object( 'usam-product' );
		$permitted_post_statuses = current_user_can( $post_type_object->cap->edit_posts ) ? array( 'private', 'draft', 'pending', 'publish' ) : array( 'publish' );
		if ( isset($_REQUEST['products']) && is_array($_REQUEST['products']) )
		{
			$count = 0;		
			$usam_cart->empty_cart( );			
			foreach( $_REQUEST['products'] as $product_id => $post ) 
			{		
				if ( isset($post['quantity']) && $post['quantity'] == 0 )	
					continue;
				
				$parameters = array();
				$product_id = apply_filters( 'usam_add_to_cart_product_id', (int)$product_id );
				$product = get_post( $product_id );

				if ( ! in_array( $product->post_status, $permitted_post_statuses ) || 'usam-product' != $product->post_type )
					continue;
				
				if( isset($post['variation']))
				{
					foreach ( (array)$product['variation'] as $key => $variation )
						$variation_values[(int)$key] = (int)$variation;

					if ( count( $variation_values ) > 0 ) 
					{
						$variation_product_id = usam_get_child_object_in_terms( $product_id, $variation_values, 'usam-variation' );
						if ( $variation_product_id > 0 )
							$product_id = $variation_product_id;
					}
				}
				if ( isset($post['quantity']) )		
					$parameters['quantity'] = (int)$post['quantity'];						
				$result = $usam_cart->add_product_basket( $product_id, $parameters );	
				if ( $result ) 
				{
					$count++;
				} 				
			}
			if ( $count ) 							
				$cart_messages[] = str_replace( "[product_count]", $count, __( 'Вы добавили [product_count] товаров в корзину.', 'usam' ) );			
			else 	
				$cart_messages[] = __( 'Товары не добавленны в корзину.', 'usam' );			
		}			
		else
		{
			$parameters = array();
			
			if ( !isset($_REQUEST['product_id']) )
				return;
		
			$product_id = apply_filters( 'usam_add_to_cart_product_id', (int)$_REQUEST['product_id'] );
			$product = get_post( $product_id );

			if ( ! in_array( $product->post_status, $permitted_post_statuses ) || 'usam-product' != $product->post_type )
				return false;
			
			if( isset($_REQUEST['variation']))
			{
				foreach ( (array)$_REQUEST['variation'] as $key => $variation )
					$variation_values[(int)$key] = (int)$variation;

				if ( count( $variation_values ) > 0 ) 
				{
					$variation_product_id = usam_get_child_object_in_terms( $product_id, $variation_values, 'usam-variation' );
					if ( $variation_product_id > 0 )
						$product_id = $variation_product_id;
				}
			}
			if ( isset($_REQUEST['usam_quantity']) && $_REQUEST['usam_quantity'] > 0 )		
				$parameters['quantity'] = (int)$_REQUEST['usam_quantity'];	
						
			if ( $usam_cart->add_product_basket( $product_id, $parameters ) ) 									
				$cart_messages[] = str_replace( "[product_name]", $product->post_title, __( 'Вы только что добавили &laquo;[product_name]&raquo; в корзину.', 'usam' ) );
			else 	
				$cart_messages = $usam_cart->get_properties( 'errors' );			
		}				
		if ( $this->ajax ) 
		{
			$result = array();			
			if ( get_option( 'usam_show_fancy_notifications', 1 ) ) 
			{			
				$result['notification'] = usam_fancy_notification_content( $cart_messages );				
			}
			ob_start();				
			include( usam_get_template_file_path( 'widget-basket' ) );		// обновление виджета корзины	
			$output = ob_get_contents();
			ob_end_clean();
		
			$result['cart_widget'] = $output;
			$result['count'] = usam_get_basket_number_items() ._n(' товар', ' товаров', usam_get_basket_number_items(), 'usam');
			$result['price_cart'] = usam_get_basket_subtotal();			
			$result['slider_state'] = '';
			if ( get_option( 'usam_widget_show_sliding_cart' ) == 1 )
			{
				if ( (usam_get_basket_number_items() > 0) || (count( $cart_messages ) > 0) ) 
				{
					$_SESSION['slider_state'] = 1;
					$result['slider_state'] = USAM_CORE_IMAGES_URL.'/minus.png';
				} 
				else 
				{					
					$result['slider_state'] = USAM_CORE_IMAGES_URL.'/plus.png';					
					$_SESSION['slider_state'] = 0;					
				}
			}					
			return $result;
		}	
	}
	
	// Получить корзину
	function controller_get_cart()
	{
		ob_start();	
		include( usam_get_template_file_path( 'widget-basket' ) );		// обновление виджета корзины	
		$output = ob_get_contents();
		ob_end_clean();
	
		$result['cart_widget'] = $output;			
		$result['slider_state'] = '';
		$result['slider_state'] = '';
		if ( get_option( 'usam_widget_show_sliding_cart' ) == 1 )
		{
			if ( (usam_get_basket_number_items() > 0) ) 
			{
				$_SESSION['slider_state'] = 1;
				$result['slider_state'] = USAM_CORE_IMAGES_URL.'/minus.png';
			} 
			else 
			{					
				$result['slider_state'] = USAM_CORE_IMAGES_URL.'/plus.png';					
				$_SESSION['slider_state'] = 0;					
			}
		}	
		return $result;		
	}

	/**
	 * Функция очистки корзины, используемая через AJAX.
	 */
	function controller_empty_cart() 
	{
		global $usam_cart;
		$usam_cart->empty_cart( );
		if ( $this->ajax ) 
		{
			ob_start();	
			include( usam_get_template_file_path( 'widget-basket' ) );		// обновление виджета корзины	
			$output = ob_get_contents();
			ob_end_clean();
		
			$result['cart_widget'] = $output;			
			$result['count'] = usam_get_basket_number_items() ._n(' товар', ' товаров', usam_get_basket_number_items(), 'usam');
			$result['price_cart'] = usam_cart_total(true);		
			$result['slider_state'] = '';
			if ( get_option( 'usam_widget_show_sliding_cart' ) == 1 )
			{	
				$result['slider_state'] = USAM_CORE_IMAGES_URL.'/plus.png';					
				$_SESSION['slider_state'] = 0;	
			}
			return $result;			
		}
	}
	
	/**
	 * Потратить бонусы в корзине, используется через AJAX и в нормальной загрузки страницы.
	 */
	function controller_spend_bonuses( $id = '' ) 
	{
		global $usam_cart;
		$usam_cart->spend_bonuses();
	}

	/**
	 * Потратить бонусы в корзине, используется через AJAX и в нормальной загрузки страницы.
	 */
	function controller_return_bonuses(  ) 
	{
		global $usam_cart;
		$usam_bonus = new USAM_Work_Bonuses();	
		$usam_bonus->cancel_reserve_bonuses(); 
		
		$usam_cart->clear_cache();
	}

	function controller_shipping_same_as_billing()
	{
		if ( !empty($_REQUEST['shipping_same_as_billing']) )
			$shipping_same_as_billing = 1;
		else
			$shipping_same_as_billing = 0;
		usam_update_customer_meta( 'shipping_same_as_billing', $shipping_same_as_billing );
	}	
	
	// Обратные ответы от платежных шлюзов
	function controller_gateway_notification()
	{			
		if ( !empty($_REQUEST['gateway']) ) 
		{
			$gateway_name = sanitize_title($_REQUEST['gateway']);	
			$gateways = usam_get_payment_gateways( array('handler' => $gateway_name, 'ipn' => 1, 'number' => 1, 'cache' => true ) );	
			
			if (!empty($gateways) )
			{ 
				$merchant_instance = usam_get_merchant_class( $gateways->handler );
				$merchant_instance->process_gateway_notification();
			}			
		}				
	}
	
	function controller_modal_feedback()
	{			
		$output = '';
		if ( !empty($_REQUEST['modal']) ) 
		{
			$modal = sanitize_title($_REQUEST['modal']);				
			$feedback = new USAM_Feedback_Init();
			$output = $feedback->get_window_modal( $modal );			
		}		
		return $output;
	}
	
	function controller_send_message_feedback()
	{			
		if ( !empty($_REQUEST['data']) ) 
		{
			global $user_ID;
		
			if ( isset($_REQUEST['data']) )
				extract( $_REQUEST['data'] ); 
			
			$mail         = empty($mail)?'':sanitize_email($mail);
			$name         = empty($name)?'':sanitize_text_field(stripcslashes($name));
			$message      = empty($message)?'':sanitize_textarea_field(stripcslashes($message));
			$phone        = empty($phone)?'':(int)$phone;
			$type_message = empty($topic)?sanitize_title(str_replace("_", "-", $_REQUEST['type'])):sanitize_title($topic);
			$page_id = absint($_REQUEST['page_id']);				
		
			$status       = 0;				
			if ( $type_message == 'price-comparison' )
			{
				$url = sanitize_text_field($url);
		
				$data = array( 'user_id' => $user_ID, 'product_id' => $page_id, 'url' => $url, 'mail' => $mail, 'name' => $name, 'description' => $message, 'type' => 'C' ); 		
				usam_insert_price_comparison( $data );	
			}		
			else
			{							
				$data = array( 'user_id' => $user_ID, 'id_product' => $page_id, 'type_message' => $type_message, 'mail' => $mail, 'phone' => $phone, 'name' => $name, 'message' => $message, 'status' => $status );	
				$data['location_id'] = usam_get_customer_location( );				
				usam_insert_feedback( $data );				
			}
			return 	$type_message;
		}				
	}
	
	function controller_price_comparison()
	{
		global $user_ID;
		
		$mail    = empty($_REQUEST['mail'])?'':sanitize_email($_REQUEST['mail']);
		$name    = empty($_REQUEST['name'])?'':sanitize_text_field($_REQUEST['name']);
		$description = empty($_REQUEST['description'])?'':sanitize_textarea_field($_REQUEST['description']);
		
		$page_id = absint($_REQUEST['page_id']);
		$url = sanitize_text_field($_REQUEST['url']);
		
		$data = array( 'user_id' => $user_ID, 'id_product' => $page_id, 'url' => $url, 'mail' => $mail, 'name' => $name, 'description' => $description, 'type' => 'C' ); 		
		usam_insert_price_comparison( $data );		
	}
	
	function controller_quick_purchase()
	{			
		global $type_price;	
		$result = false;				
		if ( !empty($_REQUEST['checkout']) && !empty($_REQUEST['page_id']) )
		{						
			$product_id  = absint($_REQUEST['page_id']);
			$args = array( 'type_price' => $type_price, 'type_payer' => 1 );
			$cart = new USAM_CART( $args );	
			
			$parameters['quantity'] = 1;			
			if ( $cart->add_product_basket( $product_id, $parameters ) )
			{					
				$cart->set_properties( array( 'gateway' => '' ) );	
			
				$order_id = $cart->save_order();	
				if ( $order_id )
				{
					$order = new USAM_Order( $order_id );
			
					$customer_data = array();	
					$checkout = $_REQUEST['checkout'];			
					
					$type_payer = usam_get_customer_meta( 'type_payer' );					
					$order_properties = usam_get_order_properties( array( 'type_payer' => $type_payer, 'fields' => 'id=>data' ) );					
					foreach ( $checkout as $value )
					{
						preg_match('/\[(.+?)\]/s', $value['name'], $m);
						if ( !empty($m[1]) )					
							$customer_data[$order_properties[$m[1]]->unique_name] = sanitize_text_field($value['value']);
					}				
					$location = usam_get_customer_location();
					foreach ( $order_properties as $id => $value )
					{
						if ( $value->type == 'location' )
							$customer_data[$value->unique_name] = $location;
					}				
					$update = $order->save_customer_data( $customer_data );	
					$args =  array(	
						'status'          => 'received',
						'shipping'        => 0,
						'order_type'      => 'fast_order',					
					);					
					$order->set($args);	
					$update = $order->save();	
					$result = true;
				}
			}	
		}				
		return $result;	
	}		
			
//********************************************************* Социальные сети ****************************************************************//
	function controller_set_user_vk() 
	{
		global $user_ID;			
			
	/*	if ( get_option( "usam_vk_сollect_customer_data" ) )
		{			
			if ( !empty($_REQUEST['vk_user_id']) )
				update_user_meta( $user_ID, 'usam_vk_user_id', absint($_REQUEST['vk_user_id']) );				
			if ( isset($_REQUEST['vk_user_id_birthday']) )					
				update_user_meta( $user_ID, 'usam_vk_birthday', absint($_REQUEST['vk_user_id_birthday']) );	
		}*/
	}
	
//********************************************************* Оформление заказа ****************************************************************//
	
	function controller_printed_form()
	{				
		if ( isset($_REQUEST['form']) )
		{			
			$printed_form = sanitize_title($_REQUEST['form']);
			echo usam_get_printing_forms( $printed_form );
			exit;
		}
	}
	
	function controller_printed_form_to_pdf()
	{				
		if ( isset($_REQUEST['form']) )
		{			
			$id = absint($_REQUEST['id']);	
			$printed_form = sanitize_title($_REQUEST['form']);				
			$html = usam_get_printing_forms_to_pdf( $printed_form, $id );
			
			header('Content-Type: application/pdf');
			echo $html;
			exit;
		}	
	}
	
	function controller_terms_and_conditions()
	{
		echo wpautop( wp_kses_post( get_option( 'usam_terms_and_conditions' ) ) );
		die();
	}
	
	private function get_page_checkout() 
	{
		if( !empty($_REQUEST['checkout']) )
		{				
			$checkout = $_REQUEST['checkout'];					
			$meta_data = array();
			foreach ( $checkout as $value ) 
			{
				preg_match('/\[(.+?)\]/s', $value['name'], $m);
				$item_id = $m[1];				
				if ( isset($meta_data[$item_id]) )
				{
					$meta_data[$item_id] = array( $meta_data[$item_id] );
					$meta_data[$item_id][] = sanitize_text_field($value['value']);
				}
				else
					$meta_data[$item_id] = sanitize_text_field($value['value']);	
			}			
			usam_user_checkout_update( $meta_data );		
		}		
		return usam_get_page_content( 'checkout', "content-page-checkout");
	}
	
	/**
	 * Функция обновления местоположения, используется через AJAX и в нормальном загрузки страницы.
	 */
	function controller_update_location()
	{
		global $usam_cart;		
		$result = array( 'checkout' => '' );
		if ( ! empty( $_REQUEST['location_id'] ) )
		{	
			$location_id = (int)$_REQUEST['location_id'];		
		
			usam_update_customer_location( $location_id );				
			usam_update_customer_meta( 'checkout_location', array() );	
			
			$usam_cart->set_properties( array( 'location' => $location_id ) );
			
			$page_checkout = $this->get_page_checkout();
			$result['checkout'] = $page_checkout;
		}			
		return $result;
	}	
	
	/**
	 * Функция добавления местоположения
	 */
	function controller_change_location()
	{		
		$output = '';
		if ( ! empty($_REQUEST['id_change_location']) )
		{	
			$page_checkout = $this->get_page_checkout();
			
			$id_change_location = absint($_REQUEST['id_change_location']);
			$checkout_location = usam_get_customer_meta( 'checkout_location' );
			$checkout_location[$id_change_location] = 1;
			usam_update_customer_meta( 'checkout_location', $checkout_location );	
		}
		echo $page_checkout;
		exit();
	}
	
	/**
	 * Функция, используется через AJAX и в нормальном загрузки страницы. Обновление доставки когда выбран новый способ доставки
	 */
	function controller_update_shipping_method() 
	{	
		if( !empty($_REQUEST['method']) )
		{		
			global $usam_cart;

			$method = absint($_REQUEST['method']);
			$usam_cart->set_properties( array( 'selected_shipping' => $method ) );				
			
			$page_checkout = $this->get_page_checkout();			
		
			require_once( USAM_FILE_PATH . '/includes/modal.class.php'  );	
			$modal = new USAM_Modal( 'pickup_order' );	
			$html_modal = $modal->get_html_modal();		
			return array( 'page' => $page_checkout, 'modal' => $html_modal );
		}			
	}
	
	function controller_update_type_payer() 
	{
		// Сменить плательщика	
		if ( isset( $_REQUEST['type_payer'] ) && is_numeric($_REQUEST['type_payer']) )
		{			
			global $usam_cart, $type_payer;
			
			$type_payer = absint($_REQUEST['type_payer']);
			
			usam_update_customer_meta( 'type_payer', $type_payer );	
			$usam_cart->set_properties( array( 'type_payer' => $type_payer ) );
							
			return usam_get_page_content( 'checkout', "content-page-checkout");					
		}	
	}	
	
	/**
	 * Сохранение выбранного способа оплаты.
	 */
	function controller_update_gateway_method() 
	{	
		if( !empty($_REQUEST['method']) )
		{		
			global $usam_cart;	
			$args['selected_gateway'] = absint($_REQUEST['method']);
			$usam_cart->set_properties( $args );			
			
			$page_checkout = $this->get_page_checkout();	
			return $page_checkout;
		}			
	}
			
	/**
	 * Выбрать склад самовывоза
	 */
	function controller_selected_pickup_method() 
	{		
		if( !empty($_REQUEST['method']) && !empty($_REQUEST['storage']) )
		{		
			global $usam_cart;	
			$selected_shipping_method = (int)$_REQUEST['method'];
			$selected_storage = (int)$_REQUEST['storage'];
			$usam_cart->set_properties( array('selected_shipping' => $selected_shipping_method, 'storage_pickup' => $selected_storage) );
			
			ob_start();			
			usam_cart_get_select_storage_address(); 
			$output = ob_get_contents();
			ob_end_clean();
			
			return $output;
		}	
	}	
	
	function controller_get_modal()
	{			
		$type_modal = sanitize_title($_REQUEST['modal']);		
		require_once( USAM_FILE_PATH . '/includes/modal.class.php'  );	
		$modal = new USAM_Modal( $type_modal );	
		$html_modal = $modal->get_html_modal();			
		return $html_modal;
	}
	
//********************************************************* Кабинет пользователя ****************************************************************//
			
	//Отмена заказа покупателем
	function controller_cancel_order()
	{	
		global $user_ID;
			
		if ( !empty($_REQUEST['order_id']) && !empty($_REQUEST['cancellation_reason']))
		{
			$order_id = absint($_REQUEST['order_id']);
			$order = new USAM_Order( $order_id );					
			if ( $user_ID == $order->get('user_ID') && $order->get('status') != 'closed' )
			{		
				$args = array( 'status' => 'canceled', 'modified_customer' => 1, 'cancellation_reason' => sanitize_textarea_field($_REQUEST['cancellation_reason']) );			
				$order->set( $args );	
				$order->save( );				
			}
		}		
	}

	//Запрос на работу
	function controller_working()
	{	
		global $user_ID;
		update_user_meta( $user_ID, 'usam_job_request', 1 );
	}
	//********************************************************* Категории товаров ****************************************************************//
	
	
	// функция выводит продукты в соответствии с запрошенными сортировками
	function controller_sort_products()
	{	
		add_action( 'usam_start_the_query', 'usam_include_products_page_template' );		
		$this->exit = false;
	}
	
//********************************************************* Товар ****************************************************************//	
	function controller_get_desired_product()
	{							
		$product_id = !empty($_REQUEST['product_id'])?absint($_REQUEST['product_id']):0;
		$args = array( 'user_list' => 'desired', 'fields' => 'product_id' );
		
		if ( !empty($_REQUEST['max']) )
			$args['max'] = absint($_REQUEST['max']);
		
		$product_ids = usam_get_user_products( $args );	

		if ( empty($product_ids) )
			return __('Нет товаров в списке желаний...', 'usam');
		
		$output = usam_get_display_desired_products( $product_ids );
		return $output;
	}
	
	//Добавляет в список сравнений
	function controller_compare_product(  )
	{	
		global $user_ID;	
		
		if ( empty($_REQUEST['product_id']) )
			return array( 'text' => '', 'class' => '' );
		
		$product_id = absint($_REQUEST['product_id']);	
		$length_list = 10;
		
		$args = array( 'user_list' => 'compare', 'fields' => 'product_id' );
		$product_ids = usam_get_user_products( $args );	
		
		if ( count($product_ids) > $length_list )	
		{
			$output = __('Больше нельзя добавлять...', 'usam');	
			$class = '';	
		}		
		if ( in_array($product_id, $product_ids) )			
		{
			usam_delete_product_from_customer_list( $product_id, 'compare' );				
			$output = __('Товар удален из списка сравнения...', 'usam');	
			$class = 'no';	
		}	
		else
		{
			$data = array( 'user_list' => 'compare', 'product_id' => $product_id, 'user_id' => $user_ID, 'user_code' => usam_get_current_customer_id() );
			$insert = usam_insert_user_product( $data );	
			if ( $insert )
			{
				$output = __('Добавлено в список сравнения...', 'usam');	
				$class = 'yes';	
			}
			else
			{
				$output = __('Товар не добавлен.', 'usam');	
				$class = 'no';
			}			
		}		
		$output .= '<p class = "text2">'.sprintf( __( "Посмотреть %s", 'usam' ),'<a href="'.usam_get_url_system_page('compare').'">'.__('ваш список', 'usam').'</a>' ).'</p>';		
		return array( 'text' => $output, 'class' => $class );
	}

	//Добавляет в список желаний
	function controller_desired_product(  )
	{	
		global $user_ID;		
		
		$userlog_url = usam_get_url_system_page('your-account');
		if ( $user_ID && !empty($_REQUEST['product_id']) )
		{
			$product_id = absint($_REQUEST['product_id']);	
			$length_list = 20;				
			
			$args = array( 'user_list' => 'desired', 'fields' => 'product_id' );
			$product_ids = usam_get_user_products( $args );	
			
			if ( count($product_ids) > $length_list )	
			{
				$output = __('Больше нельзя добавлять...', 'usam');	
				$class = '';	
			}		
			if ( in_array($product_id, $product_ids) )	
			{
				usam_delete_product_from_customer_list( $product_id, 'desired' );				
				$output = __('Товар удален из избранного...', 'usam');	
				$class = 'no';	
			}							
			else
			{
				$data = array( 'user_list' => 'desired', 'product_id' => $product_id, 'user_id' => $user_ID, 'user_code' => usam_get_current_customer_id() );
				$insert = usam_insert_user_product( $data );	
				if ( $insert )
				{
					$output = __('Добавлено в избранное...', 'usam');	
					$class = 'yes';	
				}
				else
				{
					$output = __('Товар не добавлен.', 'usam');	
					$class = 'no';
				}						
			}				
			$output .= '<p class = "text2">'.sprintf( __( "Посмотреть избранное <a href='%s'>Вашем аккаунте</a>", 'usam' ), $userlog_url ).'</p>';
		}
		else
		{		
			$output = '<p class = "text1">'.sprintf( __('Недоступно для неавторизованных пользователей. Пожалуйста, <a href="%s">войдите</a>', "usam"), $userlog_url ).'</p>';		
			$class = 'no';
		}	
		return array( 'text' => $output, 'class' => $class );
	}
	
	/** обновление цены, артикула продукта в вариациях через скрипт
	 */
	function controller_get_data_product_variation()
	{
		$response = array(		
			'variation_found' => false,
		);
		if ( empty($_REQUEST['product_id']) || !is_numeric($_REQUEST['product_id']) )
			return $response;
		
		$from = '';
		$change_price = true;
		$product_id = (int) $_REQUEST['product_id'];		
		
		$response['product_id'] = $product_id;
		
		if ( ! empty( $_REQUEST['variation'] ) )
		{
			$variation = (int)$_REQUEST['variation'];
			do_action( 'usam_update_variation_product', $product_id, $variation );
			$variation_product_id = usam_get_id_product_variation( $product_id, $variation );			
			if ( !empty($variation_product_id) ) 
			{				
				$stock = usam_get_product_meta( $variation_product_id, "stock" );
				$response['variation_found'] = true;
				if ( $stock === 0 )
					$response += array(
						'product_msg'     =>  __( 'Извините, но этой вариации нет в наличии.', 'usam' ),
						'variation_msg'   => __( 'Вариации нет в наличии', 'usam' ),
						'stock_available' => false,
					);
				else
					$response += array(
						'variation_msg'   => __( 'Товар в наличии', 'usam' ),
						'stock_available' => true,
					);			
				if ( $change_price )
				{
					$old_price = usam_get_product_old_price( $variation_product_id );		
					if ( $old_price == 0 )
					{
						$old_price = 0;
						$you_save = '';	
						$old_price_currency = '';					
					}
					else
					{
						$old_price_currency = usam_currency_display( $old_price, array( 'display_as_html' => false ) );
						$you_save_amount = usam_you_save( array( 'product_id' => $variation_product_id, 'type' => 'amount' ) );
						$you_save_percentage = usam_you_save( array( 'product_id' => $variation_product_id ) );
						$you_save = usam_currency_display( $you_save_amount, array( 'display_as_html' => false ) ) . "! (" . $you_save_percentage . "%)";
					}				
					$price = usam_get_product_price( $variation_product_id );						
					$sku = usam_get_product_meta( $variation_product_id, 'sku' );			
					$response += array(
						'old_price'         => $old_price_currency,
						'numeric_old_price' => (float) number_format( $old_price ),
						'you_save'          => $you_save,						
						'price'             => $from . usam_currency_display( $price, array( 'display_as_html' => false ) ),
						'numeric_price'     => (float) number_format( $price ),
						'sku'    			=>  $sku,					
					);
				}
			}
		}
		return $response;		
	}	
	
	// Обновляет рейтинг товара
	function controller_product_rating() 
	{
		global $wpdb;	
		$nowtime = time();
		$product_id = absint( $_REQUEST['product_id'] );
		$rating = absint( $_REQUEST['rating'] );
		
		if ( !isset($_COOKIE['prating'][$product_id]) || (isset($_COOKIE['prating'][$product_id]) && $rating != $_COOKIE['prating'][$product_id]) ) 	
		{
			$rating_count = (int)usam_get_product_meta( $product_id, 'rating_count' );	
			$p_rating = (int)usam_get_product_meta( $product_id, 'rating' );
			
			$rating_sum = $p_rating*$rating_count;		
			if ( isset($_COOKIE['prating'][$product_id]) ) 	
				$rating_sum -= (int)$_COOKIE['prating'][$product_id];
			else 			
				++$rating_count;	
			
			$rating_sum += $rating;
			$rating_new = $rating_sum / $rating_count;		
		
			setcookie( "prating[$product_id]", $rating, time() + (60 * 60 * 24 * 360) );	
			usam_update_product_meta( $product_id, 'rating_count', $rating_count );	
			usam_update_product_meta( $product_id, 'rating', $rating_new );	
		}		
	}
	
	function controller_rss() 
	{
		global $wp_query, $usam_query;		
		if ( !empty($usam_query) && is_object($usam_query) )
		{
			list($wp_query, $usam_query) = array( $usam_query, $wp_query ); // swap the usam_query object	
			header( "Content-Type: application/xml; charset=UTF-8" );
			header( 'Content-Disposition: inline; filename="usam_product_list.rss"' );
			require_once(USAM_FILE_PATH . '/includes/theme/rss_template.php');
			list($wp_query, $usam_query) = array( $usam_query, $wp_query ); // swap the usam_query object
			exit();
		}
	}
		
	//********************************************************* Поиск ****************************************************************//	
	/**
	 * Потратить бонусы в корзине, используется через AJAX и в нормальной загрузки страницы.
	 */
	function controller_get_result_search_page( ) 
	{				
		USAM_Search_Shortcodes::get_result_search_page();
	}
	
	function controller_download_file() 
	{
		global $wpdb;

		$message = __( 'Данная загрузка больше не доступна, пожалуйста, свяжитесь с администратором сайта для получения дополнительной информации.', 'usam' );
		// strip out anything that isnt 'a' to 'z' or '0' to '9'
		ini_set('max_execution_time',10800);
		
		$downloadid = sanitize_title($_REQUEST['downloadid']);		
		
		$download_status = new USAM_PRODUCT_DOWNLOAD_STATUS( $downloadid, 'unique_id' );	
		$download_data = $download_status->get_data();	
		if ( (get_option( 'usam_ip_lock_downloads', 0 ) == 1) && ($_SERVER['REMOTE_ADDR'] != null) ) 
		{	
			$ip_number = $_SERVER['REMOTE_ADDR'];
			if ( $download_data['ip_number'] == '' ) 	
			{
				$update = array( 'ip_number' => $ip_number );
				$download_status->set( $update );
				$download_status->save();				
			}		
			elseif ( $ip_number != $download_data['ip_number'] ) 				
				exit( $message );		
		}
		$file_id = $download_data['fileid'];
		$file_data = usam_get_downloadable_file( $file_id );
		
		if ( $file_data == null ) 
		{
			exit( $message );
		}
		if ( $download_data != null ) 
		{
			if ( (int)$download_data['downloads'] >= 1 )
				$download_count = (int)$download_data['downloads'] - 1;
			else 
				$download_count = 0;
			
			$update = array( 'downloads' => $download_count );
			$download_status->set( $update );
			$download_status->save();
		
			do_action( 'usam_alter_download_action', $file_id );
			usam_force_download_file( $file_id );
		} 
		else 
			exit( $message );
	}
	
	//Письмо открыто
	function controller_email_open()
	{	
		$mail_id = (int)$_REQUEST['mail_id'];
		if ( $mail_id > 0)
		{
			usam_update_email( $mail_id, array('opened_at' => date( "Y-m-d H:i:s" ) ) );
		}
	}
	
	//Переход из рассылки. Рассылка открыта
	function controller_mailing_open()
	{	
		if ( !empty($_REQUEST['stat_id']) )
		{		
			$stat_id = (int)$_REQUEST['stat_id'];	
			$stat = usam_get_user_stat_mailing( $stat_id );			
			$mailing = usam_get_newsletter( $stat['newsletter_id'] );
			if ( !empty($mailing) )  
			{					
				$update_user_stat = array( 'opened_at' => date( "Y-m-d H:i:s" ), 'status' => 2,  );
				usam_update_user_stat_newsletter( $stat_id, $update_user_stat );
				
				$update['number_opened'] = $mailing['number_opened'] + 1;
				usam_update_newsletter( $stat['newsletter_id'], $update );
				
				header("Content-type: image/png");
				readfile(USAM_CORE_IMAGES_PATH.'/mailtemplate/picsel.gif');				
			}
		}		
	}
	
	// На ссылку в рассылке нажали
	function controller_m_click()
	{	
		if ( !empty($_REQUEST['stat_id']) )
		{	
			$stat_id = (int)$_REQUEST['stat_id'];
			$stat = usam_get_user_stat_mailing( $stat_id );			
			$mailing = usam_get_newsletter( $stat['newsletter_id'] );
			if ( !empty($mailing) )  
			{ 
				$stat_id = absint($_REQUEST['stat_id']);	
				
				$stat['status'] = 2;
				$stat['clicked']++;
				$stat['opened_at'] = !empty($stat['opened_at'])?$stat['opened_at']:date( "Y-m-d H:i:s" );
				usam_update_user_stat_newsletter( $stat_id, $stat );
				
				$update['number_clicked'] = $mailing['number_clicked'] + 1;		
				usam_update_newsletter( $stat['newsletter_id'], $update );	
				
				usam_update_location_subscriber( $stat['id_communication'] );
			}
		}	
	}
	
	// Отписаться в рассылке
	function controller_mailing_unsub()
	{	
		$this->sendback = get_bloginfo( 'url' ).'/your-subscribed';
		if ( !empty($_REQUEST['stat_id']) )
		{		
			$stat_id = (int)$_REQUEST['stat_id'];	
			$stat = usam_get_user_stat_mailing( $stat_id );			
			$mailing = usam_get_newsletter( $stat['newsletter_id'] );			
			if ( !empty($mailing) )  
			{				
				$update_user_stat = array( 'status' => 2, 'unsub' => 1 );
				$update_user_stat['opened_at'] = !empty($stat['opened_at'])?$stat['opened_at']:date( "Y-m-d H:i:s" );
				usam_update_user_stat_newsletter( $stat_id, $update_user_stat );
		
				$update['number_unsub'] = $mailing['number_unsub'] + 1;
				usam_update_newsletter( $stat['newsletter_id'], $update );
			
				$lists = usam_get_newsletter_list( $stat['newsletter_id'] );
				foreach ( $lists as $list )
					usam_set_subscriber_lists( array( 'id_communication' => $stat['id_communication'], 'status' => 2, 'id' => $list ));
					
				usam_update_location_subscriber( $stat['id_communication'] );
				$this->sendback = add_query_arg( array('comm' => $stat['id_communication'], 'subscribe' => 0), $this->sendback );	
			}
		}	
	}
	
	// Изменить подписку на странице подписок
	function controller_unsub_list()
	{	
		$this->sendback = get_bloginfo( 'url' ).'/your-subscribed';
		if( wp_verify_nonce( $_REQUEST['_usam_user_profile'], 'usam_user_profile') )
		{
			if ( !empty($_REQUEST['communication_id']) )
			{		
				$select_list = !empty($_REQUEST['mail_list'])?array_map('intval', $_REQUEST['mail_list']):array();	
				$communication_id = absint($_REQUEST['communication_id']);	
				$lists = usam_get_subscribers_list( );
				foreach ( $lists as $list )
				{
					if ( empty($select_list) || !in_array($list['id'], $select_list) )
						$status = 2;
					else
						$status = 1;					
					usam_set_subscriber_lists( array( 'id_communication' => $communication_id, 'status' => $status, 'id' => $list['id'] ));
					
					$this->sendback = add_query_arg( array('comm' => $communication_id, 'subscribe' => 0), $this->sendback );	
				}
			}	
		} 		
	}
		
	function controller_subscribe_for_newsletter()
	{		
		$this->sendback = get_bloginfo( 'url' ).'/your-subscribed';
		if ( !empty($_REQUEST['email']) )
		{			
			$email = sanitize_email($_REQUEST['email']);
			if ( is_email( $email ) )	
			{
				$lastname =  !empty($_REQUEST['lastname'])?trim(sanitize_text_field($_REQUEST['lastname'])):'';	
				$firstname =  !empty($_REQUEST['firstname'])?trim(sanitize_text_field($_REQUEST['firstname'])):'';
				
				global $user_ID;	
				$subscriber = array( 'lastname' => $lastname, 'firstname' => $firstname, 'contact_source' => 'email', 'user_id' => $user_ID, 'email' => $email );
				$communication_id = usam_subscribe_for_newsletter( $subscriber );		
				
				$this->sendback = add_query_arg( array('comm' => $communication_id, 'subscribe' => 1), $this->sendback );	
			}
		}		
	}
	
	// Добавить отзыв в базу
    function controller_add_review( )
	{		
		if ( empty($_REQUEST['review']) )
			return false;
		
		$review = $_REQUEST['review'];		
      
		$options = get_option( 'usam_reviews', array() );	
		$insert = array();
		if ( !empty($options['fields']) )
		{
			foreach ($options['fields'] as $type_field => $value ) 
			{						
				$type_field = sanitize_title($type_field);
				if ( !empty($value['ask']) && empty($review[$type_field]) && !empty($value['require']) )
				{				
					$this->error_code = 1;					
				}		
				else
				{
					if ( !empty($review[$type_field]) )
					{
						$field = sanitize_text_field($review[$type_field]);
						if ( stripos($type_field, 'custom_') === false) 					
							$insert[$type_field] = $field;				
						else
							$insert['custom_fields'][$type_field] = $field;	
					}
				}
			}
		}
		if ( !empty($review['email']) && !is_email($review['email']) )
			$this->error_code = 2;
		
		if ( !empty($review['website']) && !preg_match('/^\S+:\/\/\S+\.\S+.+$/', $review['website']) )
			$this->error_code = 3;
		
		if ( empty($_REQUEST['confirm']) )
			$this->error_code = 4;    
		
        if ( $review['rating'] < 1 || $review['rating'] > 5 ) 
            $this->error_code = 5; 
		else
			$insert['rating'] = absint($review['rating']);		
		
        if (strlen(trim($review['review_text'])) < 30) 
            $this->error_code = 6;  
		else
			$insert['review_text'] = sanitize_textarea_field($review['review_text']);	
		
		$insert['page_id'] = absint($review['page_id']);		
		if ( !$this->error_code )
		{
			usam_insert_review( $insert );   
			$this->sendback = add_query_arg( 'review', 1, $this->sendback );			
		}
		else
			$this->sendback = add_query_arg( array( 'review' => 'error'), $this->sendback );		
    }   
	
	//Удалить товар из списка пользователя
	function controller_delete_product_from_user_list( )
	{
		$product_id = absint( $_REQUEST['product_id'] );	
		$user_list = sanitize_title( $_REQUEST['list'] );		
		usam_delete_product_from_customer_list( $product_id, $user_list );	
	}
	
	// Скачать прайс лист
	function controller_download_price() 
	{		
		if ( isset($_REQUEST['file']) )
		{
			$file = sanitize_title($_REQUEST['file']);
			require_once( USAM_FILE_PATH . '/includes/product/price_list.class.php' );
			$class = new USAM_Price_List( $file );
			$class->download();
		}
	}
}
	
/**
 * функция получает изображение для вариации товара 
 * @since 2.0
 */
function usam_get_variation_product_image(  )
{
	$nonce = $_REQUEST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'image_swap' ) ) 
		 die ('Проверка не пройдена!');	
		
	if ( !empty( $_REQUEST['product_id'] ) )
	{		
		$product_id = absint( $_REQUEST['product_id'] );	
		$metadata = usam_get_product_meta( $product_id, 'product_metadata' );
		if ( !empty( $metadata['image_swap'] ) )
		{
			echo false;
			exit;	
		}		
	}
	else 
	{
		echo false;
		exit;	
	}			
	$var_id = absint($_REQUEST['var_id']);			
	$id = usam_get_id_product_variation( $product_id, $var_id );
	
	$product_thumbnail_full = usam_the_product_thumbnail( $id, array(1024, 768) );
	$product_thumbnail = usam_the_product_thumbnail($id, 'single' );		
	$gallery = usam_get_images_for_product( $id ); 

	$image_url = array( 'gallery' => $gallery, 'full' => $product_thumbnail_full['src'], 'thumbnail' => $product_thumbnail['src'] ); 
	echo json_encode( $image_url );		
	exit;
}
add_action( 'wp_ajax_nopriv_variation_image_swap', 'usam_get_variation_product_image' );
add_action( 'wp_ajax_variation_image_swap', 'usam_get_variation_product_image' );

function usam_affair_complete(  )
{	
	if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'affair_complete' ) ) 
		die ('Проверка не пройдена!');	
	 
	if ( isset($_REQUEST['id']) )
	{
		$id = absint($_REQUEST['id']);
		$data = array( 'end' => date( "Y-m-d H:i:s" ), 'status' => 3 );	
		$_event = new USAM_Event( $id );
		$_event->set( $data );
		$_event->save();		
	}
	exit;
}
add_action( 'wp_ajax_nopriv_affair_complete', 'usam_affair_complete' );
add_action( 'wp_ajax_affair_complete', 'usam_affair_complete' );
?>