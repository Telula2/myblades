<?php
// Порядок сортировки
function usam_product_order( $default = 'ASC' )
{
	$order = $default;
	if ( isset($_REQUEST['order']) )
		$order = sanitize_title($_REQUEST['order']);	
	switch ( $order ) 
	{
		case "DESC":
		case "desc":
			$query_vars = 'DESC';
		break;
		case "ASC":
		case "asc":
			$query_vars = 'ASC';
		break;		
		default:
			$query_vars = $default;
		break;
	}
	return $query_vars;
}

/**
 * Получить необходимый порядок сортировки продукта
 * Если никакой сортировки не указан, порядок сортировки настраивается в Настройки -> магазин -> Презентация -> "Сортировать продукт" » используется.
 */
function usam_product_sort_order_query_vars( $orderby = null )
{	
	global $type_price;	
	
	$query_vars = array();	
	switch ( $orderby ) 
	{
		case "dragndrop":
			$query_vars["orderby"] = 'menu_order';
		break;
		case "name":
			$query_vars["orderby"] = 'title';
			$query_vars["order"] = 'ASC';				
		break;			
		case "date":
			$query_vars["orderby"] = 'date';
			$query_vars["order"] = 'DESC';	
		break;			
		case "priceASC":	
			$query_vars["orderby"]  = 'meta_value_num';							
			$query_vars["meta_key"] = '_usam_price_'.$type_price;
			$query_vars["order"]    = 'ASC';			
		break;
		case "priceDESC":			
			$query_vars["orderby"]  = 'meta_value_num';							
			$query_vars["meta_key"] = '_usam_price_'.$type_price;
			$query_vars["order"]    = 'DESC';			
		break;
		case "percent":		// сортировать по проценту скидки			
			add_filter( 'posts_orderby', 'usam_sort_products_by_sale' );
			add_filter( 'posts_join', 'usam_sort_products_by_sale_join' );		
		break;		
		case "rating":
			$query_vars["orderby"]  = 'meta_value_num';							
			$query_vars["meta_key"] = '_usam_rating';
			$query_vars["order"]    = 'DESC';		
		break;
		case "popularity":
			$query_vars["orderby"]  = 'meta_value_num';							
			$query_vars["meta_key"] = '_usam_product_views';
			$query_vars["order"]    = 'DESC';		
		break;
		case "id":
			$query_vars["orderby"] = 'ID';			
		//	$query_vars["orderby"] = array( 'meta_value_num' => 'DESC', 'ID' => 'DESC' );		
		//	$query_vars["meta_key"] = '_usam_stock';
		break;		
	}	
	return $query_vars;
}

function usam_sort_products_by_sale( $orderby )
{
	global $wpdb;		
	$orderby = 'IF(pm_r2.meta_value=0,1,0), pm_r.meta_value / pm_r2.meta_value ASC';	
	return $orderby;
}
		
function usam_sort_products_by_sale_join( $join ) 
{
	global $wpdb, $type_price; 		
	$join .= " INNER JOIN $wpdb->postmeta AS pm_r ON ($wpdb->posts.ID = pm_r.post_id AND pm_r.meta_key = '_usam_price_".$type_price."') INNER JOIN $wpdb->postmeta AS pm_r2 ON ($wpdb->posts.ID = pm_r2.post_id AND pm_r2.meta_key = '_usam_old_price_".$type_price."')";
	return $join;
}

function usam_update_cache( $object_ids, $tables, $column )
{	
	global $wpdb;	
	
	if ( !is_array($object_ids) ) 
	{
		$object_ids = preg_replace('|[^0-9,]|', '', $object_ids);
		$object_ids = explode(',', $object_ids);
	}
	$object_ids = array_map('intval', $object_ids);		
	
	$out = array();
	foreach ( $tables as $table => $cache_key ) 
	{					
		$ids = array();
		$cache = array();
		foreach ( $object_ids as $id ) 
		{
			$cached_object = wp_cache_get( $id, $cache_key );
			if ( false === $cached_object )
				$ids[] = $id;
			else
				$cache[$id] = $cached_object;
		}

		if ( empty( $ids ) )
			return $cache;
			
		$id_list = join( ',', $ids );	
		$meta_list = $wpdb->get_results( "SELECT * FROM $table WHERE $column IN ($id_list) ORDER BY $column ASC" );	
		if ( !empty($meta_list) ) 
		{		
			foreach ( $meta_list as $metarow) 
			{				
				$cache[$metarow->$column][] = $metarow;
			}
		}		
		foreach ( $ids as $id ) 
		{
			if ( ! isset($cache[$id]) )
			{
				$cache[$id] = array();
			}
			wp_cache_add( $id, $cache[$id], $cache_key );
		}			
		$out[$cache_key] = $cache;
	}		
	return $out;
}

function usam_get_metadata($meta_type, $object_id, $table, $meta_key = '', $single = false) 
{ 
	if ( ! $meta_type || ! is_numeric( $object_id ) )
		return false;

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}

	$check = apply_filters( "usam_get_{$meta_type}_metadata", null, $object_id, $meta_key, $single );
	if ( null !== $check ) 
	{
		if ( $single && is_array( $check ) )
			return $check[0];
		else
			return $check;
	}
	$meta_cache = wp_cache_get($object_id, $meta_type . '_meta');
	if ( !$meta_cache ) 
	{ 
		$cache = usam_update_cache( array( $object_id ), array( $table => $meta_type. '_meta' ), $meta_type. '_id' );
		if ( isset($cache[$meta_type. '_meta']) )
			$meta_cache = $cache[$meta_type. '_meta'][$object_id];		
	}	
	if ( ! $meta_key ) {
		return $meta_cache;
	}	
	if ( !empty($meta_cache) ) 
	{ 
		if ( $single )
			return maybe_unserialize( $meta_cache[0]->meta_value );
		else
			return array_map('maybe_unserialize', $meta_cache);
	}
	if ( $single )
		return '';
	else
		return array();
}


function usam_update_metadata($meta_type, $object_id, $meta_key, $meta_value, $table, $prev_value = '') 
{
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
		return false;
	}
	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}
	$column = sanitize_key($meta_type . '_id');
	$id_column = 'user' == $meta_type ? 'umeta_id' : 'meta_id';
	
	$raw_meta_key = $meta_key;
	$meta_key = wp_unslash($meta_key);
	$passed_value = $meta_value;
	$meta_value = wp_unslash($meta_value);
	$meta_value = sanitize_meta( $meta_key, $meta_value, $meta_type );
	
	$check = apply_filters( "usam_update_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $prev_value );
	if ( null !== $check )
		return (bool) $check;

	if ( empty($prev_value) ) 
	{
		$old_value = usam_get_metadata($meta_type, $object_id, $table, $meta_key);
		if ( count($old_value) == 1 ) {
			if ( $old_value[0] === $meta_value )
				return false;
		}
	} 
	$meta_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $id_column FROM $table WHERE meta_key = %s AND $column = %d", $meta_key, $object_id ) );
	if ( empty( $meta_ids ) ) {
		return usam_add_metadata( $meta_type, $object_id, $raw_meta_key, $passed_value, $table );
	}

	$_meta_value = $meta_value;
	$meta_value = maybe_serialize( $meta_value );

	$data  = compact( 'meta_value' );
	$where = array( $column => $object_id, 'meta_key' => $meta_key );

	if ( !empty( $prev_value ) ) {
		$prev_value = maybe_serialize($prev_value);
		$where['meta_value'] = $prev_value;
	}

	foreach ( $meta_ids as $meta_id ) 
	{		
		do_action( "usam_update_{$meta_type}_meta", $meta_id, $object_id, $meta_key, $_meta_value );
	}
	$result = $wpdb->update( $table, $data, $where );
	if ( ! $result )
		return false;

	wp_cache_delete($object_id, $meta_type . '_meta');
	foreach ( $meta_ids as $meta_id ) 
	{
		do_action( "usam_updated_{$meta_type}_meta", $meta_id, $object_id, $meta_key, $_meta_value );
	}
	return true;
}

function usam_add_metadata($meta_type, $object_id, $meta_key, $meta_value, $table, $unique = false) 
{
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}
	$column = sanitize_key($meta_type . '_id');

	// expected_slashed ($meta_key)
	$meta_key = wp_unslash($meta_key);
	$meta_value = wp_unslash($meta_value);
	$meta_value = sanitize_meta( $meta_key, $meta_value, $meta_type );

	$check = apply_filters( "usam_add_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $unique );
	if ( null !== $check )
		return $check;

	if ( $unique && $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM $table WHERE meta_key = %s AND $column = %d", $meta_key, $object_id ) ) )
		return false;

	$_meta_value = $meta_value;
	$meta_value = maybe_serialize( $meta_value );

	do_action( "usam_add_{$meta_type}_meta", $object_id, $meta_key, $_meta_value );

	$result = $wpdb->insert( $table, array(	$column => $object_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value) );
	if ( ! $result )
		return false;

	$mid = (int) $wpdb->insert_id;
	wp_cache_delete($object_id, $meta_type . '_meta');	
	do_action( "usam_added_{$meta_type}_meta", $mid, $object_id, $meta_key, $_meta_value );
	return $mid;
}
?>