<?php
/**
 * Основные запрос в базу данных для шаблона
 * @since 3.8
 */
new USAM_Product_Query();
final class USAM_Product_Query
{	
	private $is_page_universam = false;
	private $is_edit_product = false;
	private $is_taxonomy_universam = false;
	private static $_this;
	private $query_vars = null;
	private $in_the_loop = false;

	static function this() 
	{	 
		return self::$_this;	 
	}	
	
	public function __construct() 
	{
		self::$_this = $this;
			
		add_filter( 'pre_get_posts', array($this, 'split_the_query'), 8 );
	//	add_filter( 'pre_get_posts', array($this, 'exclude_product'), 8 );		
		add_action( 'template_redirect', array($this, 'start_the_query'), 8 );
		add_filter( 'request', array($this, 'filter_query_request'),9 );	
		add_filter( 'query_vars', array($this, 'query_vars') );
		
		add_filter( 'parse_query', array($this, 'mark_product_query'), 12 );

		if ( get_option( 'usam_display_sold_products' ) == '' ) 
		{
			add_filter( 'posts_orderby', array($this, 'product_orderby'), 50, 2 );	
			add_filter( 'posts_join', array( $this, 'product_join' ), 50, 2 );			
		}
	}

	function product_orderby( $orderby, $wp_query ) 
	{
		global $wpdb;	
		if ( (isset($wp_query->query_vars['usam-brands']) || isset($wp_query->query_vars['usam-category']) || isset($wp_query->query_vars['usam-category_sale']) ) && $orderby != '' && $this->in_the_loop )
		{ 
			$orderby = "IF(pstock_sort.meta_value>0,1,0) DESC, ".$orderby;	
			//remove_filter( 'posts_orderby', array($this, 'product_orderby'), 50, 2 );	
		}
		return $orderby;
	}	
	
	function product_join( $sql, $wp_query ) 
	{	
		global $wpdb;		
		
		if ( (isset($wp_query->query_vars['usam-brands']) || isset($wp_query->query_vars['usam-category']) || isset($wp_query->query_vars['usam-category_sale'])) && $this->in_the_loop )
		{		
			$sql .= " INNER JOIN $wpdb->postmeta AS pstock_sort ON ($wpdb->posts.id = pstock_sort.post_id AND pstock_sort.meta_key = '_usam_storage')";
			//remove_filter( 'posts_join', array($this, 'product_join'), 50, 2 );			
		}			
		return $sql;	
	}		
	
	function mark_product_query( $query ) 
	{		
		if ( isset( $query->query_vars['post_type'] ) && ($query->query_vars['post_type'] == 'usam-product') )			
			$query->is_product = true;		
		return $query;	
	}

	function query_vars( $vars ) 
	{
		$vars[] = "post_type"; // post_type используется для указания того, что мы ищем продукты	
		$vars[] = "usam_page";
		$vars[] = "price_range";
		$vars[] = "usam_item"; // используется, чтобы найти элементы, которые могут быть либо продуктами или категорией продуктов.	
		$vars[] = "tabs";      // tabs используется, чтобы найти страницу в профиле пользователя
		$vars[] = "subtab";    // subtab используется, чтобы найти страницу в профиле пользователя			
		$vars[] = "gateway_id";		
		
		$vars[] = "keyword";    // является именем категории продуктов, как показано на URL
		$vars[] = "scat";
		$vars[] = "stag";	
		return $vars;
	}	
	
	/**
	 * Исправления для некоторых несоответствий $wp_query при просмотре страниц.
	 * Причины следующие URL-адреса для работы (с нумерацией страниц с поддержкой):
	 */
	function filter_query_request( $args ) 
	{ 	//print_r($args); exit;
		$product_list = basename(usam_get_url_system_page('products-list'));
		if ( get_option( 'usam_category_hierarchical_url', 0 ) )
		{				
			if ( !empty( $args['usam-product'] ) )
			{
				$components = explode( '/', $args['usam-product'] );
				$end_node = array_pop( $components );
				$parent_node = array_pop( $components );

				$posts = get_posts( array( 'post_type' => 'usam-product', 'name' => $end_node, ) );
				if ( !empty( $posts ) )
				{
					$args['usam-product'] = $args['name'] = $end_node;
					if ( !empty( $parent_node ) )
						$args['usam-category'] = $parent_node;
				} 
				else 
				{
					$args['usam-category'] = $end_node;
					unset( $args['name'] );
					unset( $args['usam-product'] );
				}			
			}
			elseif ( !empty($args['usam-category']) )
			{
				$components = explode( '/', $args['usam-category'] );
				$end_node = array_pop( $components );
				$parent_node = array_pop( $components );				
				$args['usam-category'] = $end_node;
			}			
		}
		else
		{		
			$is_sub_page = !empty( $args['usam-category'] ) && $args['usam-category'] != 'page' && ! term_exists( $args['usam-category'], 'usam-category' );
			// Убедитесь, что никакие ошибка 404 не создается для любых дополнительных страниц Товаров-странице
			if ( $is_sub_page )
			{						
				$pagename = "{$product_list}/{$args['usam-category']}";
				if ( isset($args['name']) )
					$pagename .= "/{$args['name']}";		
				$args = array();
				$args['pagename'] = $pagename;	
			} 
		}	
	// Когда странице продукта установлена для отображения всех продуктов или категории, а разбивку на страницы включена, $wp_query перепутались и is_home() равна истина. Это исправляет это.
		$needs_pagination_fix = isset( $args['post_type'] ) && !empty($args['usam-category']) && 'usam-product' == $args['post_type'] && ! empty( $args['usam-product'] ) && 'page' == $args['usam-category'];
		if ( $needs_pagination_fix ) 
		{ 
			$default_category = get_option( 'usam_default_category' );
			if ( $default_category == 'all' || $default_category != 'list' ) 
			{
				$page = $args['usam-product'];
				$args = array();
				$args['pagename'] = $product_list;
				$args['page'] = $page; 
			}
		}		
		if( isset($args['category_name']))
		{	
			if( strpos($args['category_name'], 'brands' ) !== false)
			{	
				$cat = explode("/", $args['category_name']);	
				$args['usam-brands'] = $cat[1];
				if ( is_numeric($cat[2]) )
					$args['paged'] = $cat[2];
				else
					$args['usam-category'] = $cat[2];
				if ( !empty($cat[3]))
					$args['paged'] = $cat[3];
				unset($args['category_name']);	
			}	
		}	
		return $args;
	}	
	
	function split_the_query( $query )
	{
		global $usam_query_vars, $type_price;
			
		$query->usam_in_the_loop = false;
		$this->query_vars = $query->query;	//сделать копию запроса WordPress		

		if( isset($_GET['range']) )
		{						
			if ( !isset( $_GET['orderby'] ) )	
			{
				$query->query_vars["orderby"]  = 'meta_value_num';							
				$query->query_vars["meta_key"] = '_usam_price_'.$type_price;		
			}
			if ( !isset( $_GET['order'] ) )	
				$query->query_vars["order"]    = 'ASC';	
			
			$str = $_GET['range'];
			$str = preg_replace('/[^0-9-]/', '', $str);					
			$range = explode('-', $str);		
			$range = array_map('intval', $range);			
			if( empty($range[0]) && !empty($range[1]) )			
				$query->query_vars['price_range'] = array( '>' => 0, '<=' => $range[1] );			
			elseif( empty($range[1]) && !empty($range[0])  )											
				$query->query_vars['price_range'] = array( '>=' => $range[0] );					
			elseif( !empty($range[1]) && !empty($range[0]))			
				$query->query_vars['price_range'] = array( '>=' => $range[0], '<=' => $range[1] );			
			else 
				$query->query_vars['price_range'] = array( '>' => 0 );	
		}	
		elseif ( !get_option('usam_show_zero_price', 1) ) 
			$query->query_vars['price_range'] = array( '>' => 0 );	

// Виртуальные страницы			
		$virtual_page = usam_get_virtual_page( $query->query_vars['pagename'] );
		if ( !empty($virtual_page) ) 
		{			 								
			require_once( USAM_FILE_PATH . '/includes/theme/virtual_page.class.php'  );		
			$pg = new USAM_Virtual_Page( $virtual_page );
		}	
		remove_filter( 'pre_get_posts', array( $this, 'split_the_query'), 8 );
	}
	
	function exclude_product( $query )
	{
	/*	$post_type_object = get_post_type_object( 'usam-product' );
		echo $post_type_object;
		$edit_product = current_user_can( $post_type_object->cap->edit_posts );
		
		$query->query_vars['meta_query'] = array();
		$query->query_vars['meta_query'][] = array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '!=' );			*/
	}
	
	function is_product_query()
	{
		global $wp_query;		

		if ( !empty($wp_query->query['pagename']) ) 
		{					
			$system_pages = get_option( 'usam_system_page', false );			
			foreach ($system_pages as $system_page )
			{
				if ( $system_page['name'] == $wp_query->query['pagename'] )
				{
					$wp_query->query['usam_page'] = $wp_query->query['pagename'];
					return true;
				}
			}				
			if ( usam_get_virtual_page( $wp_query->query['pagename'] ) ) 
			{ // если виртуальная страница
				$wp_query->query['usam_page'] = $wp_query->query['pagename'];
				return true;			
			}
		}				
		elseif ( !empty($wp_query->query['usam-category']) ) 
			return true;
		elseif ( !empty($wp_query->query['usam-brands']) ) 
			return true;
		elseif ( !empty($wp_query->query['usam-category_sale']) ) 
			return true;
		elseif ( !empty($wp_query->query['product_tag']) ) 
			return true;
		
		return false;
	}
			
	function start_the_query()
	{	
		global $wp_query, $usam_query, $usam_query_vars, $type_price;		
		if ( null == $usam_query && $this->is_product_query() ) 
		{		
			if( isset( $wp_query->query['pagename'] ) && $wp_query->query['pagename'] == 'products-list' && !isset($wp_query->post))
			{
				global $post;
				if( !isset( $this->query_vars['usam-category'] ) && ! isset( $this->query_vars['product_tag'] ) )
					$wp_query = new WP_Query('post_type=usam-product&name='.$this->query_vars['name']);
				
				$test_term = get_term_by( 'slug', $this->query_vars['name'], 'usam-category' );
				if ( empty($test_term) )
				{  // Узнаем существует ли термин
					$wp_query->is_404 = true;
				    $usam_query->is_404  = true;
				}					
			}		
			$usam_query_vars = array();		
			if ( count( $usam_query_vars ) <= 1 )
			{					
				if ( !isset($usam_query_vars['meta_query']))
					$usam_query_vars['meta_query'] = array();
					
				$post_type_object = get_post_type_object( 'usam-product' );				
				$edit_product = current_user_can( $post_type_object->cap->edit_posts );	
				$usam_query_vars = array(
					'post_status' => $edit_product ? 'private,draft,pending,publish' : 'publish',
					'post_parent' => 0		//Выведем продукты верхнего уровня, исключим все дочерние		
				);											
				if( isset($this->query_vars['preview']) && $this->query_vars['preview'] )
					$usam_query_vars['post_status'] = 'any';
				
				if ( isset($_GET['p_filter']) )
				{							
					$select_filters = $_GET['p_filter'];
					if ( !is_array($select_filters) )
						$select_filters = usam_url_array_decode($select_filters);
										
					if ( is_array($select_filters) )
					{															
						$filter_ids = array();							
						foreach ( $select_filters as $key => $value )
						{ 								
							if ( !empty($value) )											
								$filter_ids[] = absint($value);								
						}				
						$filters = usam_get_product_filters_value( array( 'id' => $filter_ids ) );
						foreach ( $filters as $filter )
							$usam_query_vars['meta_query'][] = array( 'key' => '_usam_filter_'.$filter->attribute_id, 'value' => $filter->id, 'compare' => '=' );	
					}
				}	
				if(!empty($this->query_vars['product_tag']))
				{
					$usam_query_vars['product_tag'] = $this->query_vars['product_tag'];
					$usam_query_vars['taxonomy'] = get_query_var( 'taxonomy' );
					$usam_query_vars['term'] = get_query_var( 'term' );
				}
				elseif( !empty($this->query_vars['usam-product']) )
				{	// Если открыт товар
					
				}				
				else
				{			
					if (isset( $_GET['order']))	
						$usam_query_vars['order'] = usam_product_order();
					else
						$usam_query_vars['order'] = get_option( 'usam_product_order' );									
					if( isset($this->query_vars['usam-category']) )
					{								
						if ( !empty($_GET['brand']) )
						{
							$usam_query_vars['usam-brands'] = sanitize_title($_GET['brand']);							
						}							
						$usam_query_vars['usam-category'] = $this->query_vars['usam-category'];								
						$usam_query_vars['taxonomy'] = get_query_var( 'taxonomy' );
						$usam_query_vars['term'] = get_query_var( 'term' );	
						
						if( isset($this->query_vars['usam-brands']))
							$usam_query_vars['usam-brands'] = $this->query_vars['usam-brands'];	
						if( isset($this->query_vars['usam-category_sale']))
							$usam_query_vars['usam-category_sale'] = $this->query_vars['usam-category_sale'];	
						
						if ( isset($this->query_vars['pagename']) )
						{						
							if ( $this->query_vars['pagename'] == 'sale' )				
								$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars('sale') );				
							elseif ($this->query_vars['pagename'] == 'new-arrivals' )					
								$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars('product_news') );					
						}
					}
					elseif( isset($this->query_vars['usam-brands']))
					{						
						$usam_query_vars['usam-brands'] = $this->query_vars['usam-brands'];				
						$usam_query_vars['taxonomy'] = get_query_var( 'taxonomy' );
						$usam_query_vars['term'] = get_query_var( 'term' );		
						if ( isset($this->query_vars['pagename']) )
						{
							if ( $this->query_vars['pagename'] == 'sale' )				
								$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars('sale') );				
							elseif ($this->query_vars['pagename'] == 'new-arrivals' )					
								$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars('product_news') );					
						}
					}
					elseif( isset($this->query_vars['usam-category_sale']))
					{						
						$usam_query_vars['usam-category_sale'] = $this->query_vars['usam-category_sale'];				
						$usam_query_vars['taxonomy'] = get_query_var( 'taxonomy' );
						$usam_query_vars['term'] = get_query_var( 'term' );
					}					
					else
					{									
						$usam_query_vars['post_type'] = 'usam-product';
						$usam_query_vars['pagename'] = 'products-list';
						if ( isset ($wp_query->query['usam_page']) )
						{ 
							if ( in_array( $wp_query->query['usam_page'], array('sale','special-offer','new-arrivals','compare') ) )
								$usam_query_vars['pagename'] = $wp_query->query['usam_page'];	
							$orderby = '';
							switch ( $wp_query->query['usam_page'] ) 
							{
								case 'sale':          //Если страница распродаж
									$usam_query_vars['meta_query'] = array( array( 'key' => '_usam_old_price_'.$type_price, 'value' => 0, 'type' => 'numeric', 'compare' => '>' ));				
								break;
								case 'popular':          //Если страница популярные товары
									global $wpdb;			
									$limit = get_option('usam_products_per_page', 24);	
									$limit = $limit*2;
									$products_ids = $wpdb->get_col("SELECT DISTINCT product_id FROM `". USAM_TABLE_PRODUCTS_ORDER ."` WHERE YEAR(`date_insert`) = YEAR(NOW()) ORDER BY `quantity` DESC LIMIT $limit");									
									if ( !empty($products_ids) )
										$usam_query_vars['post__in'] = $products_ids;	
									else
										$usam_query_vars['post__in'][] = 0;	
									
									$query_vars["orderby"]  = 'meta_value_num';							
									$query_vars["meta_key"] = '_usam_product_views';
									$query_vars["order"]    = 'DESC';	
									
									$usam_query_vars['meta_query'] = array( array( 'key' => '_usam_product_views', 'value' => 0, 'type' => 'numeric', 'compare' => '>' ));				
								break;								
								case 'special-offer':		//Если страница специальное предложение
									$orderby = 'priceASC';
									$products_ids = usam_get_active_products_day_id_by_codeprice();				
									if ( !empty($products_ids) )
										$usam_query_vars['post__in'] = $products_ids;	
									else
										$usam_query_vars['post__in'][] = 0;	
								break;		
								case 'new-arrivals':       //Если страница новинки
									$usam_query_vars['date_query'] = array( array( 'after' => '30 days ago') );							
								break;	
								case 'recommend':		//Если страница рекомендуемые товары
									$usam_query_vars['meta_query'][] = array( 'key' => '_usam_sticky_products', 'value' => 1, 'type' => 'numeric', 'compare' => '=' ); 
								break;
								case 'compare':		//Если страница сравнения товаров
									$ids = usam_get_products_compare( );								
									if ( !empty($ids) )									
										$usam_query_vars['post__in'] = $ids;								
									else
										$usam_query_vars['name'] = 'compare';
								break;
								case 'wish-list':		//Если страница специальное предложение
									$ids = usam_get_products_desired();															
									if ( !empty($ids) )
										$usam_query_vars['post__in'] = $ids;
									else
										$usam_query_vars['name'] = 'wish-list';								
								break;
							}				
						}								
						if (!empty($orderby)) 
							$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars( $orderby ) );	
					}
					add_filter( 'pre_get_posts', array($this, 'generate_product_query'), 11 );	
					if ( isset( $_GET['orderby'] ) )		
						$orderby =	sanitize_title($_GET['orderby']);
					else 			
						$orderby = get_option('usam_product_sort_by', 'id');   // сортировка по умолчанию			
						
					if ( get_option('usam_product_pagination',1) )
					{					
					//	$usam_query_vars['nopaging'] = 1;	
						$usam_query_vars['paged'] = get_query_var('paged');		
						if( isset($usam_query_vars['paged']) && empty($usam_query_vars['paged']))
							$usam_query_vars['paged'] = get_query_var('page');							
					}					
					$usam_query_vars = array_merge( $usam_query_vars, usam_product_sort_order_query_vars( $orderby ) );											
					// Исключить проданный товар из показа						
					switch ( get_option( 'usam_display_sold_products' ) ) 
					{
						case 1:
							$usam_query_vars['meta_query'][] = array( 'key' => '_usam_storage', 'value' => '0', 'compare' => '>','type' => 'numeric' );								
						break;
						case 2:
							$usam_query_vars['meta_query'][] = array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '>','type' => 'numeric' );							
						break;					
					}			
				}					
				$this->in_the_loop = true;					
				$usam_query = new WP_Query( $usam_query_vars ); 				
				$usam_query->usam_in_the_loop = true;				
				if ( (isset($usam_query->post_count) && $usam_query->post_count == 0) && isset($usam_query_vars['paged']) && $usam_query_vars['paged'] > 1 )
				{   // Если нет товара в категории или в бренде показать страницу товаров					
					$url = add_query_arg( array( 'paged' => 1 ) , stripslashes($_SERVER['REQUEST_URI']) );
					wp_redirect( $url );
					exit;
				}	
				do_action( 'usam_start_the_query', $usam_query_vars );					
			}						
		}
	}

	/** Событие срабатывает перед каждым запросом WP_Query. До того, как был сделан запрос в базу. Используется для изменения запроса.  */
	function generate_product_query( $query ) 
	{
		global $wp_query, $type_price;

		remove_filter( 'pre_get_posts', array($this, 'generate_product_query'), 11 );

		// Выбор продуктов по умолчанию
		if ( $query->query_vars['pagename'] != '' )
		{
			$query->query_vars['post_type'] = 'usam-product';
			$query->query_vars['pagename']  = '';
			$query->is_page     = false;
			$query->is_tax      = false;
			$query->is_archive  = true;
			$query->is_singular = false;
			$query->is_single   = false;
		}	
	
		// Если usam_item не является нулевым, мы ищем продукта или категории продукта, проверьте для категории
		if ( isset( $query->query_vars['usam_item'] ) && ($query->query_vars['usam_item'] != '') )
		{
			$test_term = get_term_by( 'slug', $query->query_vars['usam_item'], 'usam-category' );
			if ( $test_term->slug == $query->query_vars['usam_item'] )
			{	// если категория существует (slug matches slug), установить продукты в значение usam_item
				$query->query_vars['products'] = $query->query_vars['usam_item'];
			} 
			else 
			{	// в противном случае установить имя в значение usam_item
				$query->query_vars['name'] = $query->query_vars['usam_item'];
			}
		}
		if ( isset( $query->query_vars['products'] ) && ($query->query_vars['products'] != null) && ($query->query_vars['name'] != null) )
		{
			unset( $query->query_vars['taxonomy'] );
			unset( $query->query_vars['term'] );
			$query->query_vars['post_type'] = 'usam-product';
			$query->is_tax      = false;
			$query->is_archive  = true;
			$query->is_singular = false;
			$query->is_single   = false;
		}				
		if( isset($wp_query->query_vars['usam-category']) && !isset($wp_query->query_vars['usam-product']))
		{					
			$query->query_vars['usam-category'] = $wp_query->query_vars['usam-category'];
		}
		elseif( isset($wp_query->query_vars['usam-brands']) && !isset($wp_query->query_vars['usam-product']))
		{	
			$query->is_tax = true;		
			$query->is_archive = false;	
			$query->query_vars['usam-brands'] = $wp_query->query_vars['usam-brands']; 
			$query->query_vars['taxonomy'] = $wp_query->query_vars['taxonomy'];
			$query->query_vars['term'] = $wp_query->query_vars['term'];		
		}
		elseif( '' != ( $default_category = get_option('usam_default_category') ) && !isset($wp_query->query_vars['usam-product']))
		{   // Выберите, что показывать на странице товаров					
			if( empty($wp_query->query_vars['category_name']) && is_numeric( $default_category ) && ( $default_term = get_term($default_category,'usam-category')) != '' )
			{
				$query->query_vars['taxonomy'] = 'usam-category';
				$query->query_vars['term'] = $default_term->slug;
				$query->is_tax = true;
			}
			elseif( isset($wp_query->query_vars['name']) && $wp_query->is_404 )
			{	
				unset( $query->query_vars['taxonomy'] );
				unset( $query->query_vars['term'] );
				$query->query_vars['usam-product'] = $wp_query->query_vars['name'];
				$query->query_vars['name'] = $wp_query->query_vars['name'];				
			}
		}			
		//Если тег продукта таксономии
		if (isset($wp_query->query_vars['product_tag']) && $wp_query->query_vars['product_tag'])
		{
			$query->query_vars['product_tag'] = $wp_query->query_vars['product_tag'];
			$query->query_vars['term'] = $wp_query->query_vars['term'];
			$query->query_vars['taxonomy'] = 'product_tag';
			$query->is_tax      = true;
		}
		// количество продуктов на странице	
		if ( get_option('usam_product_pagination',1) )
		{				
			if( empty($_SESSION['number_products']) )
			{							
				$_SESSION['number_products'] = get_option('usam_products_per_page', 24);	
				$query->query_vars['posts_per_page'] = $_SESSION['number_products'];		
			}
			else 
			{		
				if(!empty($_GET['number_products']) && is_numeric($_GET['number_products']) && ($_SESSION['number_products'] != $_GET['number_products']) && $_GET['number_products'] <= 400 )
				{					
					$_SESSION['number_products'] = absint($_GET['number_products']);
					$query->query_vars['paged'] = 1;
					$query->query_vars['posts_per_page'] = $_SESSION['number_products'];
				}
				else 
					$query->query_vars['posts_per_page'] = $_SESSION['number_products'];
			}					
			if ( $_SESSION['number_products'] == 'all' )
			{
				$query->query_vars['posts_per_page'] = -1;
				$query->query_vars['nopaging'] = 1;
			}			
		} 
		else 
		{
			$query->query_vars['posts_per_page'] = -1;
			$query->query_vars['nopaging'] = 1;
		}
		if ( !empty($wp_query->query_vars['price_range']) )
		{  			
			if ( !isset($query->query_vars['meta_query']) )
				$query->query_vars['meta_query'] = array();
			foreach ( $wp_query->query_vars['price_range'] as $compare => $value )
				$query->query_vars['meta_query'][] = array( 'key' => '_usam_price_'.$type_price, 'value' => $value, 'type' => 'DECIMAL', 'compare' => $compare );						
		}
		return $query;
	}
}

/**
 * Проверить страница является продуктом
 * @since 3.8
 */
function usam_is_product() 
{
	return is_single() && get_post_type() == 'usam-product';
} 

function usam_is_transaction_results( $tab = '' ) 
{
	global $wp_query;
	return is_page('transaction-results') &&( $tab == '' || isset($wp_query->query['tabs']) && $wp_query->query['tabs'] == $tab ); 
} 

function usam_is_product_category() 
{
	return is_tax( 'usam-category' );
}

function usam_is_product_brand() 
{
	return is_tax( 'usam-brands' );
}

function usam_is_product_category_sale() 
{
	return is_tax( 'usam-category_sale' );
}

/**
 * Показывает только продукты из текущей категории, или из подкатегорий.
 */
class USAM_Hide_Subcatsprods_In_Cat
{
	private $q;

	function __construct( ) 
	{	
		add_action( 'pre_get_posts', array( $this, 'get_posts' ) );			
	}	
	
	function get_posts( &$q ) 
	{
		$this->q =& $q;
		if ( ( !isset($q->query_vars['taxonomy']) || ( "usam-category" != $q->query_vars['taxonomy'] )) )
			return false;

		add_action( 'posts_where', array( &$this, 'where' ) );
		add_action( 'posts_join', array( &$this, 'join' ) );
	}

	function where( $where )
	{
		global $wpdb;

		remove_action( 'posts_where', array( &$this, 'where' ) );
		$term_id = $wpdb->get_var($wpdb->prepare('SELECT term_id FROM '.$wpdb->terms.' WHERE slug = %s ', $this->q->query_vars['term']));

		if ( !is_numeric( $term_id ) || $term_id < 1 )
			return $where;
		$term_taxonomy_id = $wpdb->get_var($wpdb->prepare('SELECT term_taxonomy_id FROM '.$wpdb->term_taxonomy.' WHERE term_id = %d and taxonomy = %s', $term_id, $this->q->query_vars['taxonomy']));
		if ( !is_numeric($term_taxonomy_id) || $term_taxonomy_id < 1)
			return $where;
		$field = preg_quote( "$wpdb->term_relationships.term_taxonomy_id", '#' );
		$just_one = $wpdb->prepare( " AND $wpdb->term_relationships.term_taxonomy_id = %d ", $term_taxonomy_id );
		if ( preg_match( "#AND\s+$field\s+IN\s*\(\s*(?:['\"]?\d+['\"]?\s*,\s*)*['\"]?\d+['\"]?\s*\)#", $where, $matches ) )
			$where = str_replace( $matches[0], $just_one, $where );
		else
			$where .= $just_one;
		return $where;
	}

	function join($join)
	{
		global $wpdb;
		remove_action( 'posts_where', array( &$this, 'where' ) );
		remove_action( 'posts_join', array( &$this, 'join' ) );
		if( strpos($join, "JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)" ) ){
			return $join;
		}
		$join .= " JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id)";
		return $join;
	}
}

//Показать подкатегории товаров в родительской категории
if( ! get_option( 'usam_show_subcatsprods_in_cat', 0 ) )
	$hide_subcatsprods = new USAM_Hide_Subcatsprods_In_Cat;
?>