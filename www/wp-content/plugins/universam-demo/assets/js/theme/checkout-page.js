
//обновить итоги, когда методы доставки изменился.
function selected_shipping_method( method_id )
{ 		
	if ( jQuery("#element_shipping_"+method_id).hasClass('selected_method') )	
	{ 
		if ( jQuery("#element_shipping_"+method_id).hasClass('pickup_method') )	
		{
			usam_display_select_pickup( method_id );
		}
		return false;
	}		
	var data = {		
		checkout         : jQuery('.usam_checkout_table [name^="collected_data"]').serializeArray(),
		usam_ajax_action : 'update_shipping_method',	
		method           : method_id,		
	}
	jQuery.post( 'index.php', data, function( response )
	{		
		if ( response.obj )
		{				
			jQuery("#checkout_page").empty().append( jQuery(response.obj.page).html() );	
			if ( response.obj.modal != '' ) 
			{ 
				if ( jQuery("#pickup_"+method_id).length == 0 ) 
				{	
					jQuery('body').append( response.obj.modal );	
				}			
				usam_display_select_pickup( method_id );
			}				
		}			
	}, 'json');		
}

//Выбрать местоположение
function selected_location( location_id )
{			
	var parameters = {
		checkout         : jQuery('#checkout_page [name^="collected_data"]').serializeArray(),
		location_id      : location_id,	
		usam_ajax_action : 'update_location',
		},					
		response_handler = function( response ) 
		{				
		//	jQuery(".resultslocation ul").empty();	
			//jQuery(".table-shipping").empty().append( jQuery(response.obj.checkout).find(".table-shipping").html() );		
			jQuery("#checkout_page").empty().append( jQuery(response.obj.checkout).html() );				
			var stores_forms = jQuery('.select_stores_forms')
			if ( stores_forms.length > 0)
			{
				stores_forms.remove( );
			}				
		};					
	jQuery.post('index.php', parameters, response_handler, 'json');
}

//Выбрать тип плательщика
function selected_type_payer( type_payer )
{			
	data = {
		usam_ajax_action : 'update_type_payer',	
		type_payer       : type_payer,		
	}
	jQuery.post( 'index.php', data, function( response )
	{			
		if ( response.obj )
		{						
			var page = jQuery("#checkout_page").parent();
			page.empty();		
			page.append(response.obj);			
		}		
	}, 'json');
}

//Выбрать способ оплаты
function selected_gateway_method( gateway_method )
{		
	data = {	
		checkout         : jQuery('.usam_checkout_table [name^="collected_data"]').serializeArray(),
		usam_ajax_action : 'update_gateway_method',	
		method           : gateway_method,		
	}
	jQuery.post( 'index.php', data, function( response )
	{	
		if ( response.obj )
		{			
			var page = jQuery("#checkout_page").parent();
			page.empty();		
			page.append(response.obj);
		}		
	}, 'json');
}


// Действие при нажатии на кнопку сохранить в выборе склада самовывоза
function selected_pickup_method( id_method )
{	
	var modal_pickup = '#pickup_'+id_method;			
	if ( jQuery(".selected").data('store_id') != '' )
	{			
		var parameters = {
			method           : id_method,
			storage          : jQuery(".selected").data('store_id'),
			usam_ajax_action : 'selected_pickup_method',
			},					
			response_handler = function(response) 
			{				
				jQuery(".point_receipt .title").html(response.obj);
			//	$(modal_pickup).modal('hide');			
			};		
		jQuery.post('index.php', parameters, response_handler, 'json');	
	}
}	

function usam_selected_store( id_store )
{	
	if ( !jQuery("#store_"+id_store).hasClass('selected') || jQuery("#map").html() == '' )
	{				
		jQuery('tr').removeClass('selected');
		jQuery("#store_"+id_store).addClass("selected");	
		jQuery("#map").html('');
		var GPS_N = jQuery("#store_"+id_store).attr("data-GPS_N");
		var GPS_S = jQuery("#store_"+id_store).attr("data-GPS_S");
		if ( GPS_N != 0 )
		{								
			var title = '', description = '';
			if ( jQuery("#store_"+id_store+" .name strong").html() )
			{
				title = jQuery("#store_"+id_store+" .name").html();
			}						
			description = jQuery("#store_"+id_store+" label").html();	
			usam_yandex_map('map', GPS_N, GPS_S, title, description);						
		}
	}
}		

function usam_display_select_pickup( method_id )
{ 
	var data = { };			
	var modal_type = 'pickup_'+method_id;
	if ( jQuery('#'+modal_type).length ) 
	{					
		jQuery('#'+modal_type).modal();
	}
	else
	{ 			
		jQuery('body').append( '<div class="usam_backdrop"></div>');		
		data.usam_ajax_action = 'get_modal';
		data.modal = 'pickup_order';				
		jQuery.post(window.location.href, data, function(response) 
		{						
			if ( response.obj != '' )
			{
				jQuery('body').append( jQuery(response.obj) );									
				
				var storage_id = jQuery('#'+modal_type+' #store_table .selected').data('store_id');
				usam_selected_store( storage_id );
				
				jQuery('#'+modal_type).modal();	
			}			
			jQuery('.usam_backdrop').remove();	
		}, 'json');							
	}					
}


jQuery(document).ready(function($)
{		
	$('input[data-mask]').each(function () 
	{					
		$(this).mask( $(this).attr('data-mask') );		
	})	
			
	var id = jQuery("#checkout_page #location_box").data('id');
			
	jQuery(document).delegate( "#shippingSameBilling", "change", function() 	
	{					
		var shipping_same_as_billing = 0;
		if(jQuery(this).is(":checked"))
		{
			shipping_same_as_billing = 1;
		}		
		var parameters = {		
			shipping_same_as_billing : shipping_same_as_billing,			
			usam_ajax_action         : 'shipping_same_as_billing',
			},					
			response_handler = function( response ) 
			{		
							
			};					
		$.post('index.php', parameters, response_handler);
	});	
	
	$("form.usam_checkout_forms").submit(function()
	{
		if ( jQuery(".table-shipping .selected_method .validation-error").length > 0 )
		{			
			$('html, body').animate({ scrollTop: $(".table-shipping .selected_method").offset().top }, 500); 
			return false;
		}
	});
	
	jQuery(document).delegate( "#add_inhabited_locality", "click", function(e) 	
	{			
		e.preventDefault();			
		var parameters = {
			checkout           : jQuery('.checkout_item').serializeArray(),		
			id_change_location : $(this).data("location_id"),			
			usam_ajax_action   : 'change_location',
			},					
			response_handler = function( response ) 
			{		
				jQuery("#checkout_page").empty().append( response );
				jQuery(".chzn-select").chosen();				
			};					
		$.post('index.php', parameters, response_handler);
	});		
	
	jQuery(document).delegate( "#usam_select_pickup_buttom", "click", function(e) 	
	{	
		e.preventDefault();		
		var method_id =  $(this).closest('.element_shipping').data('id_method');		
		usam_display_select_pickup( method_id );
	});	
});