jQuery(document).ready(function($)
{	
	$( ".order_cancellation_reason form" ).submit(function() 
	{	  
		var cancellation_reason = $(this).find("#cancellation_reason");
		var length = cancellation_reason.val().trim().length;
		if ( length < 5 )
		{		
			cancellation_reason.css("border", "1px solid #a4286a");		
			return false;
		}						
		return true;	
	});			
	var myOptions =	
	{ 
	  collapsible: true,
	  heightStyle: "content",
	  active: $("#usam_accordion .current_menu").index(),
	}
	$( "#usam_accordion" ).accordion( myOptions );		
});