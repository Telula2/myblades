// эта функция для связывания действия для определенных событий и подменой их после того как они заменены AJAX, когда страница полностью загружена.
jQuery(document).ready(function ($) 
{		
	jQuery(".usam_quantity input").click(function ()
	{	
		var input = $(this).parent().find('input.quantity_update');
		var quantity = input.val();		
		// Нажата кнопка увеличить количество товара
		if ( $(this).hasClass('plus') )
		{
			var max_stock = $(this).data('stock');	
			var new_quantity = parseFloat(input.val()) + 1;		
			if ( max_stock >= new_quantity )		
			{
				input.val(new_quantity);
				input.change(); 
			}
		}
		if ( $(this).hasClass('minus') )
		{
			// Нажата кнопка уменьшить количество товара			                 
			var new_quantity = parseFloat(input.val().replace(/[^-0-9]/gim,'')) - 1;                
			new_quantity = new_quantity < 0 ? 0 : new_quantity;                 
			input.val(new_quantity);                 
			input.change(); 			
		}
		var product = $(this).parents('.product');		
		var price = product.find('.price').attr("data-price"); 		
		var product_sum = new_quantity*price;		
		product.find('.sum .price_value').html( product_sum );		
	
		var group = $(this).parents('.group');			
		var totalprice = group.find('.caption .sum .price_value').html().replace(/[^-0-9]/gim,'');
		var sum = totalprice - quantity*price + product_sum;
		group.find('.caption .sum .price_value').html( sum );
		
		total = 0;
		jQuery(".caption .sum .price_value").each(function () 
		{	
			total = total + parseFloat($(this).html().replace(/[^-0-9]/gim,''));			
		})				
		$('.total .sum .sale .price_value').html( total );		
		return false;
	});		
	
	jQuery('#goodslist .jcarousel').on('jcarousel:scrollend', next_product_sets );
	jQuery('#goodslist .jcarousel').on('jcarousel:createend', scrollend_product_sets );
	
	//jQuery('#goodslist .jcarousel').on('jcarousel:lastin', 'li', lastin_product_sets);//jcarousel:lastin		
//	jQuery('#goodslist .jcarousel').on('jcarousel:lastout', 'li', lastout_product_sets);
});


function lastin_product_sets( event, carousel ) 
{	
	alert(1);
	$(this).closest('.jcarousel-wrapper').find('.down-carousel').hide();
}

function lastout_product_sets( event, carousel ) 
{		
	alert(2);
	$(this).closest('.jcarousel-wrapper').find('.down-carousel').show();		
}



function scrollend_product_sets( event, carousel ) 
{	
	$(this).find('input').prop("disabled", true);
	carousel._target.find('input').prop("disabled", false);		
}

function next_product_sets(event, carousel, target, animate) 
{
	var input = $(this).find('input');	
	$(this).find('.product').removeClass('selected');	
	input.prop("disabled", true);
	
	carousel._target.find('input').prop("disabled", false);	
	carousel._target.find('.product').addClass( 'selected' );	
	
	var group_total = 0;
	var total = 0;
	var group = carousel._target.parents('.group');	
	
	group.find('.selected .sum .price_value').each(function () 
	{	
		group_total = group_total + parseFloat($(this).html().replace(/[^-0-9]/gim,''));		
	})	
	group.find('.caption .sum .price_value').html( group_total );		
	
	jQuery(".selected .sum .price_value").each(function () 
	{	
		total = total + parseFloat($(this).html().replace(/[^-0-9]/gim,''));		
	})	
	$('.total .sum .sale .price_value').html( total );	
}