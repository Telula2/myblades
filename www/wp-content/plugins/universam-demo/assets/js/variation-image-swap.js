jQuery(document).ready(function($)
{ 
	$.usam_variation_swap_scripts = 
	{
		init: function() 
		{			
			$("div.usam_variation_forms select.usam_select_variation").change(function() 
			{
				var productForm = $(this).parents("form.product_form"),
					productCol = $(this).parents(".productcol"),
					imageCol = productCol.prev(),
					allSelected,
					var_ids = new Array(),
					i = 0; //counter
				
				$(productForm).find("select.usam_select_variation").each(function() 
				{
					if ( $("option:selected", this).val() == 0) 
					{
						allSelected = false;
						return false;
					} 
					else 
					{
						var_ids[i] = $("option:selected", this).val();
						allSelected = true;
						i++;	
					}
				});						
				if ( allSelected ) 
				{					
					var product_id = $("input[name=product_id]", productForm).val(),
						image_element = $("img#product_image_" + product_id, imageCol),				
						image_src = image_element.attr('src'),
						$data = {
							action: "variation_image_swap",
							product_id: product_id,
							var_id: $("select.usam_select_variation").val(),
							image_src: image_src,
							nonce    : USAM_Variation_Image_Swap.image_swap_nonce
						};			
					$.post(USAM_Variation_Image_Swap.ajaxurl, $data, function(response) 
					{							
						if ( response != false )
						{ 		
							$("#thumbs").empty();							
							response = $.parseJSON(response);
							image_element.parent('a.preview_link').attr('href', response.full);
							image_element.fadeTo('fast', 0, function() 
							{
								var myImge = $("<img />").attr("src",response.thumbnail + "?" + new Date().getTime());
								$( myImge ).load( function() { 										
									var zoom = document.getElementById('Zoomer'); 
									ProductZoom.update(zoom, response.full, response.thumbnail, 'show-title: false');									
								//	image_element.attr("src", response.thumbnail).fadeTo('fast', 1);
								});								
							});	
							$(response.gallery).appendTo("#thumbs");	
							$.usam_variation_swap_scripts.change_product_image();		
						} 
						else 
						{					
							image_element.fadeTo('fast', 1);	
						}				
					});
		
				}
			});		
			$.usam_variation_swap_scripts.change_product_image();
		},

		change_product_image: function() 
		{
			$('#thumbs a').click(function(event)
			{			
				event.preventDefault();	
				var link = $(this).attr('href'),
					link_rev = $(this).attr('data-image'),
				image_element = $("img.product_image");
			
			//	image_element.attr("src", link).fadeTo('fast', 1);
				var zoom = document.getElementById('Zoomer'); 
				ProductZoom.update(zoom, link_rev, link, 'show-title: false');
			});  
		}			
		
	}; 
	$.usam_variation_swap_scripts.init();			
});