function get_filtered_products(event, ui) 
{			
	var slider_range = jQuery("#slider-range");
	var value1 = slider_range.slider('values', 0);
	var value2 = slider_range.slider('values', 1);					
	var value_str = value1.toString() + '-' + value2.toString();		
	var data = {
		usam_ajax_action : 'sort_products',	
		cat              : USAM_Product_Filter.cat,
		page             : USAM_Product_Filter.page,
		number_products  : jQuery('.number_products .active').html(),
		view_type        : jQuery('.display_products_options .active').data('view_type'),
		orderby          : jQuery('.pfilter #usam_products_sort').val(),					
	};
	var buffer = {};
	var filter_id = 0;	
	jQuery("#usam_filter_form select[name^='p_filter']").each(function () 
	{	
		var filter_id = jQuery(this).attr('filter_id');
		var value = this['value'];		
		if ( typeof value !== typeof undefined && value != '' ) 
		{   
			buffer[filter_id] = value;			
		}
	})		
	data["p_filter"] = buffer;	
	data["range"] = value_str;	
	jQuery(".products-catalog_list").empty().html("<div style='text-align:center; padding:30px;'><img src='"+USAM_Product_Filter.spinner+"'/></div>");	
		
	jQuery.ajax({ 
		type: "GET",
		url: document.location.href,
		data: data,			
		success: function(response) 
		{			
			jQuery("#catalog_list").html( jQuery(response).html() );
		/*	.animate({ height: jQuery(".products-catalog_list").height()+40 });
		
			jQuery(".product_grid_item").animate({ width: jQuery(".product_grid_item").width() + 10, height: jQuery(".product_grid_item").height() + 10}, 500 );
			jQuery(".product_grid_item").animate({ width: jQuery(".product_grid_item").width() - 10, height: jQuery(".product_grid_item").height() - 10}, 500 );*/
		}
	});	
}

function usam_change_number_products( event ) 
{
	event.preventDefault();		
}

jQuery(document).ready(function($)
{		
	jQuery("#usam_products_sort").change(get_filtered_products);
	jQuery("#usam_filter_form select").change(get_filtered_products);
	jQuery("#slider-range").slider({ stop: get_filtered_products	});		
	//jQuery(".number_products a").click(usam_change_number_products);	
});