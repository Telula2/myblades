(function($){
	$.extend(USAM_Feedback, 
	{
		init : function() {
			$(function()
			{	
				$('body').delegate('.usam_modal_feedback', 'click', USAM_Feedback.get_form_modal);				
			});
		},
		
		//	получить форму модального окна 
		get_form_modal : function() 
		{	
			var type = $(this).attr('href');	
			if ( $(type).length > 0 ) 
			{					
				$(type).modal();						
			}		
			else
			{
				var parameters = {
					modal            : type,
					usam_ajax_action : 'modal_feedback',
					page_id          : USAM_Feedback.page_id,					
					},					
					response_handler = function(response) 
					{					
						$('body').append( $(response.obj) );
						$(type).modal();		
						
						$('#feedback_button').delegate('#modal_action', 'click', USAM_Feedback.send_message);						
						$('#quick_purchase').delegate('#modal_action', 'click', USAM_Feedback.sending_quick_purchase);								
					};					
				USAM_Feedback.ajax( parameters, response_handler );		
			}				
		},	
		
		// Отправить сообщение
		send_message : function() 
		{		
			var send = true;
			var data = {};			
			var val = '';				
			var modal = $(this).parents('.modal').attr('id');
			jQuery('#'+modal+" .uneditable-input").each(function () 
			{	
				val = $(this).val();	
				if ( val == '' && $(this).attr('data-required') == 1 )
				{
					$(this).css("border-color", "red");		
					send = false;					
				}		
				else
				{
					$(this).css("border-color", "grey");
				}
				data[$(this).data('type')] = val;				
			});			
			if ( send )
			{	
				jQuery( '#'+modal + " .modal-body").html(USAM_Feedback.message);
				var parameters = {
					type               : modal,
					usam_ajax_action   : 'send_message_feedback',
					page_id            : USAM_Feedback.page_id,
					data               : data,										
					},
					response_handler = function(response) 
					{			
						$('#'+modal).modal('hide');
					};	
				USAM_Feedback.ajax( parameters, response_handler );		
			}
		},
		
		ajax : function( data, handler ) 
		{
			$.post(USAM_Feedback.url, data, handler, 'json');		
		},		
				
		sending_quick_purchase : function() 
		{			
			var div = '#quick_purchase';			
			var required  = true;			
			jQuery('.required').each(function() 
			{
				if ( this.value == '')
				{
					$(this).addClass('highlight');					
					required  = false;					
				}
				else
					$(this).removeClass('highlight');	
			});			
			if ( required )
			{			
				var parameters = {
					checkout         : jQuery('.checkout_item').serializeArray(),
					usam_ajax_action : 'quick_purchase',
					page_id          : USAM_Feedback.page_id,					
					},					
					response_handler = function(response) 
					{				
						if ( response.obj == true )
							jQuery( div + " .modal-body").html(USAM_Feedback.message_quick_purchase);
						else
							jQuery( div + " .modal-body").html(USAM_Feedback.message_quick_purchase_error);					
					};					
				USAM_Feedback.ajax( parameters, response_handler );						
			}
		},
	});
})(jQuery);
USAM_Feedback.init();