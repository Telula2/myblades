/**
 * Рейтинг
 */

/**
 * @example jQuery('.rating').rating();
 */ 
(function( $ )
{					
	var methods = 
	{			
		init : function( settings ) 
		{  
			var options = {
			  'selected' : null,
			  'star'     : 'star',
			};	
			options = $.extend( options, settings );
			
			return this.each(function()
			{         
				var $this = $(this),
					data = $this.data('rating');			 
				 // Если плагин ещё не инициализирован
				if ( ! data ) 
				{					
					$this.data('rating', options);						
					$this.find('.'+options.star).on("mouseover.rating", options, function( event ) 
					{  
						$(this).addClass('selected').prevAll('.'+event.data.star).addClass('selected');	
						$(this).nextAll().removeClass('selected');
					
					}).on("click.rating", options, function( event ) 
					{ 		
						if ( event.data.selected != null )
						{
							var $this = $(this);
							var rating = $this.siblings('.selected').length;
							rating = rating + 1;
							var callbacks = jQuery.Callbacks();
							callbacks.add( event.data.selected );
							callbacks.fire( rating, $this );	
						}		
					});						
				}					
		   });		   
		},
		
		close : function( )
		{
		   return this.each(function()
		   {
				var $this = $(this),
				data = $this.data('rating');
				
				$(this).find('.'+data.star).unbind("mouseover.rating");
		   })
		},		
	};	 
	 
	$.fn.rating = function( method )
	{ 	
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} 
		else if ( typeof method === 'object' || ! method ) 
		{
			return methods.init.apply( this, arguments );
		} 
		else 
		{
			$.error( 'Метод с именем ' +  method + ' не существует для jQuery.rating' );
		}   			
	};
})( jQuery );