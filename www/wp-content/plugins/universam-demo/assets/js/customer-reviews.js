(function($)
{	
	function usam_val_form() 
	{	
		var rating = jQuery("#usam_add_new_review #review_rating").val();			
		var data = {};	
		var send = true;	
		var val = '';
		var form = "usam_commentform";
		
		jQuery("#"+form+" .uneditable-input").each(function () 
		{	     
			var newid = jQuery(this).data('type');  
			var oldid = jQuery(this).attr('id');
			var t = jQuery(this);			
			val = t.val();	
			
			if ( val == '' && t.attr('data-required') == 1 )
			{
				t.css("border-color", "red");					
				send = false;				
			}		
			else
			{ 
				if ( newid === 'mail' ) 
				{			
					if (/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(val) == false) 
					{
						send = false;
						t.css("border-color", "red");		
					}
				}    
				else if ( newid === 'website' ) 
				{				
					if (/^\S+:\/\/\S+\.\S+.+$/.test(val) == false) 
					{
						send = false;
						t.css("border-color", "red");		
					}
				}    			
				else if (newid === "text" && val.length < 30) 
				{
					send = false;
					t.css("border-color", "red");
				}		
				else	
					t.css("border-color", "grey");
			}
			data[jQuery(this).data('type')] = val;					
		});
		if ( jQuery("#confirm").is(":checked") === false)
		{
			send = false;
			jQuery("#confirm").css("border-color", "red");					
		}  		
		if (rating < 1 || rating > 5) 
		{
			send = false;
			alert("Пожалуйста, выберите звездочный рейтинг от 1 до 5.");
		}		
		if ( !send ) 
		{        
			jQuery("#add_review_table").find("input:text:visible:first").focus();
			return false;
		}		
		var newact = document.location.pathname + document.location.search;
		jQuery("#"+form).attr("action",newact).removeAttr("onsubmit");
		return true;
	}	

	function usam_showform() 
	{
		jQuery("#usam_add_new_review").slideToggle();		
		jQuery("#add_review_table").find("input:text:visible:first").focus();
	}
	
	function usam_save_review_rating( rating, t ) 
	{   	
		jQuery(".your_review_rating #review_rating").val(rating);		
		jQuery(".your_review_rating").rating('close');
	}	
	
	function usam_init() 
	{    
		jQuery("#button_new_review").click(usam_showform);    
		jQuery("#usam_commentform").submit(usam_val_form);			
		jQuery(".your_review_rating").rating( {'selected': usam_save_review_rating } );
	}
	jQuery(document).ready(usam_init);
	
})(jQuery);

