(function($)
{
   /*
	* Обертка для $.post. Помогает в выполнении 'usam_ajax_action' и 'action'.
	*/
	$.usam_post = function(data, handler)
	{
		data['usam_ajax_action'] = data['action'];		
		data['action'] = 'usam_ajax';	
		$.post('index.php', data, handler, 'json');
	};	

   /*
	* Обертка для $.get. Заботится о «usam_action» и «action» аргументах данных.
	*/
	$.usam_get = function(data, handler) {
		data['usam_ajax_action'] = data['action'];
		data['action'] = 'usam_ajax';
		$.get('index.php', data, handler, 'json');
	};		

	$.usam_get_modal = function(modal_type, data)
	{ 						
		if ( jQuery('#'+modal_type).length ) 
		{					
			jQuery('#'+modal_type).modal();
		}
		else
		{ 			
			$('body').append( '<div class="usam_backdrop"></div>');		
			data.usam_ajax_action = 'get_modal';
			data.modal = modal_type;				
			jQuery.post(window.location.href, data, function(response) 
			{						
				jQuery('body').append( jQuery(response.obj) );		
				$('#'+modal_type).modal();					
				$('.usam_backdrop').remove();
			}, 'json');							
		}			
	}
})(jQuery);

function usam_get_products( data, html_id )
{ 	
	jQuery.ajax({ 
		type: "POST",
		url: "index.php",
		dataType: 'json',
		data: data,			
		success: function(response) 
		{					
			jQuery( '#'+html_id ).empty().append( response.obj );	
		}
	});		
}

//The following is all for Share this.
function usam_akst_share(id, url, title)
{
	if ((jQuery('#usam_akst_form').css("display") == 'block') && (jQuery('#usam_akst_post_id').attr("value") == id)) {
		jQuery('#usam_akst_form').css("display", "none");
		return;
	}

	var offset = {};
	new_container_offset = jQuery('#usam_akst_link_' + id).offset();

	if(offset['left'] == null) {
		offset['left'] = new_container_offset.left;
		offset['top'] = new_container_offset.top;
	}

	jQuery("#usam_akst_delicious").attr("href", usam_akst_share_url("http://del.icio.us/post?url={url}&title={title}", url, title));
	jQuery("#usam_akst_digg").attr("href", usam_akst_share_url("http://digg.com/submit?phase=2&url={url}&title={title}", url, title));
	jQuery("#usam_akst_furl").attr("href", usam_akst_share_url("http://furl.net/storeIt.jsp?u={url}&t={title}", url, title));
	jQuery("#usam_akst_netscape").attr("href", usam_akst_share_url(" http://www.netscape.com/submit/?U={url}&T={title}", url, title));
	jQuery("#usam_akst_yahoo_myweb").attr("href", usam_akst_share_url("http://myweb2.search.yahoo.com/myresults/bookmarklet?u={url}&t={title}", url, title));
	jQuery("#usam_akst_stumbleupon").attr("href", usam_akst_share_url("http://www.stumbleupon.com/submit?url={url}&title={title}", url, title));
	jQuery("#usam_akst_google_bmarks").attr("href", usam_akst_share_url("  http://www.google.com/bookmarks/mark?op=edit&bkmk={url}&title={title}", url, title));
	jQuery("#usam_akst_technorati").attr("href", usam_akst_share_url("http://www.technorati.com/faves?add={url}", url, title));
	jQuery("#usam_akst_blinklist").attr("href", usam_akst_share_url("http://blinklist.com/index.php?Action=Blink/addblink.php&Url={url}&Title={title}", url, title));
	jQuery("#usam_akst_newsvine").attr("href", usam_akst_share_url("http://www.newsvine.com/_wine/save?u={url}&h={title}", url, title));
	jQuery("#usam_akst_magnolia").attr("href", usam_akst_share_url("http://ma.gnolia.com/bookmarklet/add?url={url}&title={title}", url, title));
	jQuery("#usam_akst_reddit").attr("href", usam_akst_share_url("http://reddit.com/submit?url={url}&title={title}", url, title));
	jQuery("#usam_akst_windows_live").attr("href", usam_akst_share_url("https://favorites.live.com/quickadd.aspx?marklet=1&mkt=en-us&url={url}&title={title}&top=1", url, title));
	jQuery("#usam_akst_tailrank").attr("href", usam_akst_share_url("http://tailrank.com/share/?link_href={url}&title={title}", url, title));

	jQuery('#usam_akst_post_id').value = id;
	jQuery('#usam_akst_form').css("left", offset['left'] + 'px');
	jQuery('#usam_akst_form').css("top", (offset['top']+ 14 + 3) + 'px');
	jQuery('#usam_akst_form').css("display", 'block');
}

// submit the country forms.
function submit_change_country(){
	document.forms.change_country.submit();
}


function gotoexternallink(link, target)
{
	if (target == '')
	{
		target = '_self';
	}
	window.open(link, target);
	return false;
}

// submit the fancy notifications forms.
function usam_fancy_notification(parent_form)
{	
	if(typeof(USAM_SHOW_FANCY_NOTIFICATION) == 'undefined')
	{
		USAM_SHOW_FANCY_NOTIFICATION = true;
	}
	if((USAM_SHOW_FANCY_NOTIFICATION == true) && (jQuery('#fancy_notification') != null))
	{
		var options = {
			margin: 1 ,
			border: 1 ,
			padding: 1 ,
			scroll: 1
		};

		form_button_id = jQuery(parent_form).attr('id') + "_submit_button";
		var button_offset = jQuery('#'+form_button_id).offset();

		jQuery('#fancy_notification').css("left", (button_offset.left - 130) + 'px');
		jQuery('#fancy_notification').css("top", (button_offset.top + 40) + 'px');

		jQuery('#fancy_notification').css("display", 'block');
		jQuery('#loading_animation').css("display", 'block');
		jQuery('#fancy_notification_content').css("display", 'none');
	}
}

function usam_akst_share_url(base, url, title) 
{
	base = base.replace('{url}', url);
	return base.replace('{title}', title);
}

function number_of_unread_user_chat_messages() 
{				
	jQuery.ajax({ 
		type: "POST",
		url: "index.php",
		dataType: 'json',
		data: { usam_ajax_action : 'number_of_unread_user_chat_messages' },			
		success: function(response) 
		{				
			if ( response.obj )
			{									
				var numbers = jQuery('#start_dialog .numbers').text( );	
				if ( numbers < response.obj )
				{
					jQuery('#start_dialog .numbers').text( response.obj );	 			
					
					if ( jQuery('#chat_audio').length != 0 )
						jQuery('#chat_audio')[0].play();	
				}
			}
		}
	});		
}

function usam_save_product_rating( rating, t ) 
{ 
	var product_id = t.parent('.your_product_rating').data('product_id');
	var post_data = {
		'action'     : 'product_rating',	
		'product_id' : product_id,	
		'rating'     : rating,
	},
	response_handler = function(response){ };
	jQuery.usam_post(post_data, response_handler);		
}


function usam_yandex_map( id, GPS_N, GPS_S, title, description )
{     
	var myMap, myPlacemark;
	myMap = new ymaps.Map(id, {
		center: [GPS_N, GPS_S], 
		controls: ["zoomControl"],
		zoom: 15
	});
	myPlacemark = new ymaps.Placemark([GPS_N, GPS_S], { 
		hintContent: title, 
		balloonContent: description 
	}); 
	myMap.geoObjects.add( myPlacemark );				
}	

// эта функция для связывания действия для определенных событий и подменой их после того как они заменены AJAX, когда страница полностью загружена.
jQuery(document).ready(function ($) 
{	
	if ( jQuery("#usam_maps").length )
	{		
		$(window).load(function () 
		{
			jQuery("#usam_maps .usam_map").each(function()
			{
				var id = $(this).attr('id');
				var title = $(this).data('title');
				var description = $(this).data('description');
				var gps_s = $(this).data('gps_s');
				var gps_n = $(this).data('gps_n');
				if ( gps_s > 0 && gps_n > 0 )
				{								
					usam_yandex_map(id, gps_n, gps_s, title, description);
				}
			});
		});
	}
	
	// Нажата кнопка увеличить количество товара
	jQuery("#usam_quantity .plus").click(function ()
	{	
		var max_stock = $(this).data('stock');	
		if ( max_stock !== false )	
		{
			var input = $(this).parent().find('input.quantity_update');
			var new_quantity = parseInt(input.val()) + 1;			
			max_stock = parseInt(max_stock);
			
			if ( max_stock >= new_quantity )		
			{			
				input.val(new_quantity);
				input.change(); 
			}
		}
		return false;
	});	
	
// Нажата кнопка уменьшить количество товара
	jQuery("#usam_quantity .minus").click(function ()
	{		
		var input = $(this).parent().find('input.quantity_update');                 
		var count = parseInt(input.val()) - 1;                
		count = count < 1 ? 1 : count;                 
		input.val(count);                 
		input.change(); 
		return false;
	});	
	
	jQuery('#fancy_notification').appendTo('body');

	// добавить товар AJAX
	jQuery('body').delegate('form.product_form, .usam-add-to-cart-button-form', 'submit' , function()
	{				
		file_upload_elements = jQuery.makeArray(jQuery('input[type="file"]', jQuery(this)));
		if(file_upload_elements.length > 0) 
		{
			return true;
		} 
		else 
		{			
			form_values = jQuery(this).serialize();						
			jQuery.post( 'index.php?usam_ajax_action=add_to_cart', form_values, function(response) 
			{							
				if( jQuery('#fancy_notification_content') && response.obj.notification != '' ) 
				{
					jQuery('#fancy_notification_content').html(response.obj.notification);
					jQuery('#loading_animation').css("display", 'none');
					jQuery('#fancy_notification_content').css('display', 'block');
				}
				
				jQuery('div.shopping-cart-wrapper').html(response.obj.cart_widget);
				jQuery('span.basket_item').html(response.obj.count);
				jQuery('span.basket_total').html(response.obj.price_cart);						
				if( response.obj.slider_state != '' ) 
				{				
					jQuery('#sliding_cart').slideUp('fast',function(){
						  jQuery('#fancy_collapser').attr('src', (response.obj.slider_state));
					});
				}	
				
				jQuery('.cart_message').delay(3000).slideUp(500);								
			},'json');
			usam_fancy_notification(this);
			return false;
		}
	});	
		
	jQuery("div.usam_cart_loading").livequery(function()
	{
		form_values = "";
		jQuery.post( 'index.php?usam_ajax_action=get_cart', form_values, function(response) {
			jQuery('div.shopping-cart-wrapper').html(response.obj.cart_widget);
			if( response.obj.slider_state != '' ) 
			{				
				jQuery('#sliding_cart').slideUp('fast',function(){
					  jQuery('#fancy_collapser').attr('src', (response.obj.slider_state));
				});
			}	
		},'json');
	});
	
	$('.usam_products').delegate('.product_actions', 'mouseenter' , function(){
		$(this).css('z-index',99999);
		$(this).find('.product_actions_wrapper').show();		
	});		
	
	$('.usam_products').delegate('.product_actions', 'mouseleave' , function(){
		$(this).css('z-index',1);
		$(this).find('.product_actions_wrapper').hide();	
	});	
	
	
	if ( jQuery(".your_product_rating").length )
	{
		jQuery(".your_product_rating").rating( {'selected': usam_save_product_rating } );
	}
	
	jQuery('body').delegate('.emptycart', 'click' , function(e)
	{	
		e.preventDefault();	
		form_values = "usam_ajax_action=empty_cart";
		jQuery.post( 'index.php', form_values, function(response) 
		{			
			jQuery('div.shopping-cart-wrapper').html(response.obj.cart_widget);
			jQuery('span.basket_item').html(response.obj.count);
			jQuery('span.basket_total').html(response.obj.price_cart);						
			if( response.obj.slider_state != '' ) 
			{				
				jQuery('#sliding_cart').slideUp('fast',function(){
					  jQuery('#fancy_collapser').attr('src', (response.obj.slider_state));
				});
			}	
		},'json');
		return false;
	});


	jQuery('a.usam_category_link, a.usam_category_image_link').click(function()
	{
		product_list_count = jQuery.makeArray(jQuery('ul.category-product-list'));
		if(product_list_count.length > 0) {
			jQuery('ul.category-product-list', jQuery(this).parent()).toggle();
			return false;
		}
	});

	//  this is for storing data with the product image, like the product ID, for things like dropshop and the the ike.
	jQuery("form.product_form").livequery(function()
	{
		product_id = jQuery('input[name="product_id"]',this).val();
		image_element_id = 'product_image_'+product_id;
		jQuery("#"+image_element_id).data("product_id", product_id);
		parent_container = jQuery(this).parents('div.product_view_'+product_id);
		jQuery("div.item_no_image", parent_container).data("product_id", product_id);
	});
	//jQuery("form.product_form").trigger('load');

	// update the price when the variations are altered.
	
	jQuery('body').delegate('.usam_select_variation', 'change' , function()	
	{	
		var product_id = USAM_Theme.page_id;
		var variation = jQuery(this).val();
			
		var	post_data   = {				
				action     : 'get_data_product_variation',			    
				product_id : product_id,
				variation  : variation,			
			},
			ajax_callback = function(response)
			{				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
				var stock_display = jQuery('div#stock_display_' + product_id),				
					price_span = jQuery('#product_price_' + product_id ),			
					old_price = jQuery('#old_product_price_' + product_id),
					sku = jQuery('#product_sku_' + product_id),		
					save = jQuery('#yousave_' + product_id);
					
				if ( response.obj.variation_found ) 
				{
					if ( response.obj.stock_available ) {
						stock_display.removeClass('out_of_stock').addClass('in_stock');
					} else {
						stock_display.addClass('out_of_stock').removeClass('in_stock');
					}								 
				}								
				stock_display.html(response.obj.variation_msg);			
				if ( response.obj.price !== undefined ) 
				{							
					price_span.html(response.obj.price);
					old_price.html(response.obj.old_price);
					sku.html(response.obj.sku);						
					save.html(response.obj.you_save);					
					if ( response.obj.old_price != 0 ) 
					{
						old_price.parent().show();
						save.parent().show();
					} 
					else 
					{
						old_price.parent().hide();
						save.parent().hide();
					}	
				}						
			};						
		$.usam_post(post_data, ajax_callback);	
	});

	// Object frame destroying code.
	jQuery("div.shopping_cart_container").livequery(function(){
		object_html = jQuery(this).html();
		window.parent.jQuery("div.shopping-cart-wrapper").html(object_html);
	});

	//Shipping bug fix by James Collins
	var radios = jQuery(".productcart input:radio[name='shipping_method']");
	if (radios.length == 1) {
		// If there is only 1 shipping quote available during checkout, automatically select it
		jQuery(radios).click();
	} else if (radios.length > 1) {
		// There are multiple shipping quotes, simulate a click on the checked one
		jQuery(".productcart input:radio[name='shipping_method']:checked").click();
	}

	//карусель	
	jQuery(".jcarousel").each(function() 
	{
		var visible = parseInt($(this).attr("data-visible-items"));
		var total = parseInt($(this).attr("data-visible-total"));
		var scroll = parseInt($(this).attr("scroll-items"));
		var auto = true;
		var wrap = null;
		var interval = 3000;
		var itemFirstInCallback = '';

		if ( $(this).attr("scroll-auto") && $(this).attr("scroll-auto") == 'false' )
			auto = false;	

		if ( $(this).attr("scroll-interval") )
			interval = $(this).attr("scroll-interval");		
		
		if ( $(this).attr("data-wrap") )
			wrap = $(this).attr("data-wrap");	

		if ( $(this).attr("data-itemFirstInCallback") )
			itemFirstInCallback = $(this).attr("data-itemFirstInCallback");	
		
	
		if (visible <= total)
		{
			cir = true;	
		}
		else
		{
			cir = false;
			visible = total;
			scroll = 0;			
		}		
		var data = { wrap: wrap };		
        if ( auto == true )
		{			
			$(this).jcarousel( data ).jcarouselAutoscroll({
				interval: interval,
				target: '+=1',
				autostart: true
			});			
		}
		else
			$(this).jcarousel( data );	
		
		if ( $(this).attr("data-CallbackScroll") )
		{			
			var callbackscroll = $(this).attr("data-CallbackScroll");
	
			if ($.isFunction(callbackscroll) ) 
			{
				$(this).on('jcarousel:scroll', callbackscroll );
			}
		}
				
		$(this).siblings('.up-carousel')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $(this).siblings('.down-carousel')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
    });	
	
	//Javascript для вариаций: подергивание селекта с вариациями, когда ничего не выбрано и нажата кнопка добавить в корзину.
	jQuery('.productcol, .textcol, .product_grid_item, .product_list_item, .usam-add-to-cart-button').each(function()
	{
		jQuery('.button_buy', this).click(function()
		{
			var dropdowns = jQuery(this).closest('form').find('.usam_select_variation');
			var not_selected = false;
			dropdowns.each(function(){
				var t = jQuery(this);
				if(t.val() <= 0){
					not_selected = true;
					t.css('position','relative');
					t.animate({'left': '-=5px'}, 50, function(){
						t.animate({'left': '+=10px'}, 100, function(){
							t.animate({'left': '-=10px'}, 100, function(){
								t.animate({'left': '+=10px'}, 100, function(){
									t.animate({'left': '-=5px'}, 50);
								});
							});
						});
					});
				}
			});
			if (not_selected)
				return false;
		});
	});
	
	jQuery('.shopping_cart_container table tr td select').hide();
	var radio = jQuery(".productcart .usam_shipping_quote_radio input:checked").attr('id');
	jQuery('.shopping_cart_container table tr.'+radio+' td select').show();		
	
	jQuery('.shopping_cart_container table tr td.usam_shipping_quote_radio').click(function()
	{			
		jQuery('.shopping_cart_container table tr td select').hide();
		var radio = jQuery(".productcart .usam_shipping_quote_radio input:checked").attr('id');
		jQuery('.shopping_cart_container table tr.'+radio+' td select').show();
	});
	
	// Добавить в сравнение	
	$('body').delegate('#compare_product', 'click' , function(e)
	{			
		e.preventDefault();	
		var t = $(this);
		var compare_result = $(this).siblings('#compare_product_result');
	
		compare_result.show();
		jQuery.ajax({
			type: "POST",
			url: USAM_Theme.ajaxurl,
			dataType: 'json',
			data: { usam_ajax_action: 'compare_product', product_id: USAM_Theme.page_id},			
			success: function(response)
			{									
				t.removeClass('yes').removeClass('no').addClass(response.obj.class);
				compare_result.empty().append( response.obj.text ).delay(5000).slideUp();			
			}
		});	
	});
	
	
	if ( $('#start_dialog .numbers').length != 0 )		
	{		
		setInterval('number_of_unread_user_chat_messages()',10000); 
	}	
	
/**
 * Добавляет в список желаний
 */	
	$('body').delegate('#desired_product', 'click' , function(e)
	{	
		e.preventDefault();	
		var t = $(this);
		var desired_result = $(this).siblings('#desired_product_result');
	
		desired_result.show();
		jQuery.ajax({
			type: "POST",
			url: USAM_Theme.ajaxurl,
			dataType: 'json',
			data: { usam_ajax_action: 'desired_product', product_id: USAM_Theme.page_id},			
			success: function(response)
			{									
				t.removeClass('yes').removeClass('no').addClass(response.obj.class);
				desired_result.empty().append( response.obj.text ).delay(5000).slideUp();			
			}
		});			
	}); 

/**
 * Получить форму модального окна 
 */	
	jQuery('#regions_buttom').click(function(e) 
	{	
		e.preventDefault();		
		var data = { };			
		$.usam_get_modal('regions_search', data);
	});	
	
	jQuery('#warehouses_buttom').click(function(e) 
	{	
		e.preventDefault();		
		var data = { id: USAM_Theme.page_id };	
		$.usam_get_modal('availability_by_warehouses', data);
	});	
	
	jQuery('#bt_search_my_order').click(function(e) 
	{	
		$('#search_my_order_id-form').submit();
	});	
	
	if ( $("#countdown").length != 0 ) 
	{
		var note = jQuery("#note"),
			today = new Date,	
			newYear = true;		
			ts = new Date( today.getFullYear(), today.getMonth(), today.getDate() + 1 );
		var options = {
			timestamp	: ts,		
			callback	: function(hours, minutes, seconds)
			{
				
			},
		};
		$("#countdown").usam_countdown( options );
	}
});