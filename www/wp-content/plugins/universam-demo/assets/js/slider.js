(function($)
{
	$.fn.et_switcher = function(options)
	{
		var defaults =
		{
		   slides: '>div',
		   activeClass: 'active',
		   linksNav: '',
		   findParent: true, //use parent elements in defining lengths
		   lengthElement: 'li', //parent element, used only if findParent is set to true
		   useArrows: false,
		   arrowLeft: 'prevlink',
		   arrowRight: 'nextlink',
		   auto: false,
		   autoSpeed: 5000,
		   button_styling: 'mini',				   
		};

		var options = $.extend(defaults, options);

		return this.each(function()
		{
			var width_document = $(document).width();			
			var slidesContainer = jQuery(this);		
			var slides = slidesContainer.find(options.slides);					
			slides.hide();				
			var first_slide = slides.filter(':first');
		//	var slide_width = first_slide.width();			
			first_slide.css('display','block');
			
			if (options.linksNav != '') 
			{ 	
				var linkSwitcher = jQuery(options.linksNav);
				linkSwitcher.click(function()
				{
					var targetElement;

					if (options.findParent) 
						targetElement = jQuery(this).parent();
					else 
						targetElement = jQuery(this);
					if (targetElement.hasClass('active')) 
						return false;
							
					if ( targetElement.hasClass('raise') )
					{ 
						targetElement.siblings('.active').animate({marginTop: '-18px'},500,function(){
							jQuery(this).removeClass('active');
						});
						targetElement.animate({marginTop: '6px'},500,function(){
							jQuery(this).addClass('active');

						});
					}
					else
					{
						targetElement.siblings('.active').removeClass('active');
						targetElement.addClass('active');
					}
					var ordernum = targetElement.prevAll(options.lengthElement).length;

					slidesContainer.find(options.slides).filter(':visible').hide().end().end().find(options.slides).filter(':eq('+ordernum+')').stop().fadeIn(700);

					if (typeof interval != 'undefined') 
					{
						clearInterval(interval);
						auto_rotate();
					};	
					return false;
				});
			};

			jQuery('#'+options.arrowRight+', #'+options.arrowLeft).click(function()
			{
				var slideActive = slidesContainer.find(options.slides).filter(":visible"),
					nextSlide = slideActive.next(),
					prevSlide = slideActive.prev();

				if (jQuery(this).attr("id") == options.arrowRight) {
					if (nextSlide.length) {
						var ordernum = nextSlide.prevAll().length;
					} else { var ordernum = 0; }
				};

				if (jQuery(this).attr("id") == options.arrowLeft) {
					if (prevSlide.length) {
						var ordernum = prevSlide.prevAll().length;
					} else { var ordernum = slidesContainer.find(options.slides).length-1; }
				};

				slidesContainer.find(options.slides).filter(':visible').hide().end().end().find(options.slides).filter(':eq('+ordernum+')').stop().fadeIn(700);

				if (typeof interval != 'undefined') {
					clearInterval(interval);
					auto_rotate();
				};

				return false;
			});

			if (options.auto) 
			{
				auto_rotate();
			};

			function auto_rotate()
			{
				interval = setInterval(function()
				{ 
					var slideActive = slidesContainer.find(options.slides).filter(":visible"),
						nextSlide = slideActive.next();
					if (nextSlide.length) 
					{
						var ordernum = nextSlide.prevAll().length;
					} 
					else 
					{ 
						var ordernum = 0; 
					}
					if (options.linksNav === '')
						jQuery('#'+options.arrowRight).trigger("click");
					else
						linkSwitcher.filter(':eq('+ordernum+')').trigger("click");
				},options.autoSpeed);
			};
		});
	}
})(jQuery);