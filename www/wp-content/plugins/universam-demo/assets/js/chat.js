(function($){
	$.extend(USAM_Chat, 
	{
		timeout       : 10000,		
		iframe	      : '',
		rows	      : '',
		set_interval  : '',
		table_wrap_id : 'im_rows_wrap',
		table_id      : 'im_rows',
		
		init : function() 
		{
			$(function()
			{	
			/*	var defaults = { 
					dialog_id   : USAM_Chat.dialog_id,
					id_message : USAM_Chat.id_message,
					user       : 0, 
				};							
			*/	
			//	$.dialog( defaults );			
				
				USAM_Chat.Scroll();		
				USAM_Chat.rows = $('#im_rows');								
	
				$(document).delegate('#start_dialog', 'click', USAM_Chat.start_dialog);
				
				$('#dialogs tbody').delegate('tr td', 'click', USAM_Chat.go_dialogs);				
				$(document).delegate('#im_controls #message', 'keyup', USAM_Chat.event_sent_message_key)	
						   .delegate('#sent_message', 'click', USAM_Chat.event_sent_message);
									  
				$('#form_dialog').delegate('.switch', 'click', USAM_Chat.update_online_consultants);	
				$('#wpadminbar').delegate('#wp-admin-bar-usam-menu-online_consultant-item', 'click', USAM_Chat.get_form_dialog);
				
				if ( $('#im_rows').length != 0 && USAM_Chat.dialog_id != 0 )		
				{		
					setInterval('USAM_Chat.refresh()',USAM_Chat.timeout); 
					//$.dialog( 'start_refresh' );
				}
				if ( $('#tab_chat .wp-list-table tbody tr').length != 0 )	
				{					
					setInterval('USAM_Chat.refresh_dialogs()',USAM_Chat.timeout);					
				}				
				$('#usam_dialog_iframe').hide();
			});
		},
		
		// Загрузить диалог
		set_manager_dialog : function() 
		{					
			$('.dialogs_row').removeClass('current_sel');
			$(this).addClass("current_sel");
			
			USAM_Chat.rows = $(this).closest('#dialogs').siblings('#content-chat').find('#im_rows');
			USAM_Chat.rows.find('tr').remove();
			
			var spinner = USAM_Chat.rows.siblings('#ajax-loading');	
			spinner.addClass('ajax-loading-active');
			
			USAM_Chat.dialog_id = $(this).data('dialog_id');			
			jQuery.ajax({ 
				type: "POST",
				url: "index.php",
				dataType: 'json',
				data: { chat : 'dialog_message', dialog_id : USAM_Chat.dialog_id },			
				success: function( response ) 
				{												
					spinner.removeClass('ajax-loading-active');				
					USAM_Chat.display_message( response );		
				}
			});		
		},
		
		get_html_message : function( message ) 
		{	
			var html = '<td class = "message_body"><div class="message_header"><span class="message_user">'+message.user+'</span><span class="message_date">'+message.date+'</span></div><div class = "message_text">'+message.text+'</div></td>';
			return html;
		},
		
		dialog_wrapper : function( ) 
		{	
			var html = '<div class = "'+USAM_Chat.table_wrap_id+'"><table id="'+USAM_Chat.table_id+'"></table></div>';
			return html;
		},
		
		display_message : function( messages ) 
		{							
			USAM_Chat.rows.find('tr').removeClass("new_message");		
			if ( messages )
			{				
				jQuery.each(messages, function() 
				{	
					USAM_Chat.id_message = this['id'];
					var massage = { 'user': this['user'], 'date': this['date'], 'text': this['message']  } 
					var html = USAM_Chat.get_html_message( massage );					
					var $html = $("<tr id = 'im_rows-" + USAM_Chat.id_message + "'>"+ html + "</tr>");
					if ( this['read'] == 0 )
						$html.addClass('new_message');				
					
					USAM_Chat.rows.append( $html );
				});							
				USAM_Chat.Scroll();
			}
		},	
		
		get_form_dialog : function() 
		{ 
			$('#form_dialog').modal();			
			if ( !$('#form_dialog .modal-body').is('.chat') )			
			{
				jQuery.ajax({ 
					type: "POST",
					url: "index.php",
					dataType: 'json',
					data: { chat : 'modal_html' },			
					success: function(response) 
					{							
						$('#form_dialog .modal-body').html( response );	
						$('#form_dialog #dialogs').delegate('.dialogs_row', 'click', USAM_Chat.set_manager_dialog);	
						setInterval('USAM_Chat.refresh()',USAM_Chat.timeout);							
					}
				});		
			}
		},
		
		update_online_consultants : function(  ) 
		{
			var $status = 0;
			if ( $(this).find('input').prop( "checked" ) )			
				$status = 1;
					
			jQuery.ajax({ 
				type: "POST",
				url: "index.php",
				dataType: 'json',
				data: { chat : 'update_online_consultants', status : $status },			
				success: function(response) 
				{		
					$('.selector_status_consultant').html( response );
					if ( $status )
						$('.selector_status_consultant').addClass('active');
					else
						$('.selector_status_consultant').removeClass('active');
				}
			});				
		},		
		
		start_dialog : function() 
		{
			if( $('#usam_dialog_iframe').is(':visible') )
			{
				USAM_Chat.close_dialog();
			}
			else 
			{
				USAM_Chat.show_dialog();
			}
		},
		
		show_dialog : function() 
		{			
			$('#usam_dialog_iframe').show();
			
			USAM_Chat.iframe = $('#usam_dialog_iframe #usam_iframe').contents();
			USAM_Chat.rows = USAM_Chat.iframe.find('#im_rows');				

			USAM_Chat.iframe.delegate('#message', 'keyup', USAM_Chat.event_sent_message_key_iframe)	
					.delegate('#sent_message', 'click', USAM_Chat.event_sent_message_iframe);	
					
			USAM_Chat.iframe.delegate('.icon-close', 'click', USAM_Chat.close_dialog);
			
			USAM_Chat.set_interval = setInterval('USAM_Chat.refresh()',USAM_Chat.timeout); 
			
			//$.dialog( 'start_refresh' );	
		},
		
		close_dialog : function() 
		{
			$('#usam_dialog_iframe').hide();		
		//	$('#usam_form_dialog').animate({height: "hide"}, USAM_Chat.timeout);	
			clearInterval( USAM_Chat.set_interval );		
		},
		
		//скролл к низу окна
		Scroll : function() 
		{
			var win = $('.im_rows_wrap');			
			if ( win.length != 0 )
			{
				var height = win[0].scrollHeight;
				win.scrollTop(height);
			}
			if ( USAM_Chat.iframe != '' )
			{			
				var	iframe_rows = USAM_Chat.iframe.find(".im_rows_wrap");	
				if ( iframe_rows.length != 0 )
				{
					var height = iframe_rows[0].scrollHeight;
					iframe_rows.scrollTop(height);
				}
			}
		},
	
//	Отправить сообщение по нажатию CTRL + Enter		
		event_sent_message_key : function(e) 
		{		
			if(e.keyCode == 13 && e.ctrlKey)
			{			
				USAM_Chat.event_sent_message();
			}
		},		
		
		event_sent_message_key_iframe : function(e) 
		{				
			if( e.keyCode == 13 )
			{			
				USAM_Chat.event_sent_message_iframe();
			}
		},
		
		event_sent_message : function() 
		{	
			//$.dialog( 'sent_message' );
			var	blok_message = $(this).parents("#im_controls").find("#message");	
			var text_message = blok_message.val();				
			if (text_message != "" ) 
			{			
				blok_message.val('');
				USAM_Chat.sent_message( text_message );
			}	
		},
		
		event_sent_message_iframe : function() 
		{	
			var	blok_message = USAM_Chat.iframe.find("#message");	
			var text_message = blok_message.val();
		
			if (text_message != "" ) 
			{	
				blok_message.val('');
				USAM_Chat.sent_message( text_message );
			}	
		},
						
//	Отправить сообщение
		sent_message : function( text_message ) 
		{	
			if ( text_message.replace(/\r|\n/g, '') != '' ) 
			{		
				var d = new Date();			
				var year_num = d.getFullYear();
				var month_num = d.getMonth();
				var day = d.getDate();
				var hours = d.getHours();
				var minutes = d.getMinutes();				
				var curent_date = day + '.' + month_num + '.' + year_num + ' ' + hours + ':' + minutes;			
				
				var massage = { 'user': USAM_Chat.user, 'date': curent_date, 'text': text_message  } 
				var html_message = USAM_Chat.get_html_message( massage );		
				USAM_Chat.id_message = USAM_Chat.id_message + 1;
				USAM_Chat.rows.append( "<tr id = 'im_rows-" + USAM_Chat.id_message + "'>"+ html_message + "</tr>" );	
				USAM_Chat.Scroll();				
				jQuery.ajax({ // Отсылаем параметры	
					type: "POST",
					url: "index.php",
					data: { chat: 'sent_message', message: text_message, dialog_id: USAM_Chat.dialog_id, manager_id: USAM_Chat.manager_id},		
					success: function(response) // Выводим то что вернул PHP
					{							
						var obj = jQuery.parseJSON(response);					
						if ( response )
						{							
							USAM_Chat.dialog_id = obj.dialog_id;
							USAM_Chat.id_message = obj.id_message;	
						}
					}
				});	
			}	
		},
		
		refresh_dialogs : function() 
		{					
			jQuery.ajax({ 
				type: "POST",
				url: "index.php",
				dataType: 'json',
				data: { chat : 'refresh_dialogs' },			
				success: function(response) 
				{					
					if ( response.length != 0 )
					{		
						jQuery.each(response, function() 
						{	
							var count_new_box = $('#tab_chat tr#im_dialog-'+this['dialog_id']+' .column-count .count_new');
							var count_new = count_new_box.html();
							if ( count_new != "+"+this['count'] )
							{
								count_new_box.html( "+"+this['count'] );
							}													
						});	
					}
					else
					{
						var count_new_box = $('#tab_chat tr .column-count .count_new');
						count_new_box.html('');
					}
				}
			});				
		},		
		
		refresh : function() 
		{		
		//	$.dialog('refresh');		
			if ( USAM_Chat.dialog_id != 0 )
			{					
				jQuery.ajax({ 
					type: "POST",
					url: "index.php",
					dataType: 'json',
					data: { chat : 'refresh', dialog_id : USAM_Chat.dialog_id, id_message : USAM_Chat.id_message },			
					success: function( response ) 
					{					
						USAM_Chat.display_message( response );						
					}
				});	
			}			
		},
		
		go_dialogs : function() 
		{				
			var row = $(this).closest('tr');
			if ( !row.hasClass('no-items') && !row.hasClass('current_sel') )
			{
				var str = row.attr('id').split('-');				
				if ( str[1] > 0 )
				{
					var linkLocation = updateURLParameter(location.href, 'sel', str[1]);	
					$("body").fadeOut(1000, USAM_Chat.redirectPage(linkLocation) );
				}
			}
		},	

		redirectPage: function( linkLocation ) 
		{			
			window.location = linkLocation;
		},
				
	});
})(jQuery);
USAM_Chat.init();





/*
(function( $ )
{	
	var defaults = { 
			dialog_id   : 0,
			id_message : 0,
			user       : 0, 
		};	
	
	var methods = 
	{		
		init : function( options ) 
		{ 	
			methods.options = $.extend({}, defaults, options );
	
		
		 
			var make = function(){
			  // реализация работы метода с отдельным элементом страницы
			}; 
			return this.each(make);			
		},
		
		sent_message : function( ) 
		{	
			var user = methods.options.user;
			var dialog_id = methods.options.dialog_id;
			var id_message = methods.options.id_message;				
			
			var	blok_message = jQuery("#im_controls #message");	
			var text_message = blok_message.val();				
			if (text_message != "" ) 
			{	
				blok_message.val('');
				var im_rows = jQuery('#im_rows');				
				var d = new Date();			
				var year_num = d.getFullYear();
				var month_num = d.getMonth();
				var day = d.getDate();
				var hours = d.getHours();
				var minutes = d.getMinutes();	
				var curent_date = day + '.' + month_num + '.' + year_num + ' ' + hours + ':' + minutes;			
				var box_message = '<td class = "message_body"><div class="message_user">'+user+'</div><div class = "message_text">'+text_message+'</div></td><td class = "message_date">'+ curent_date +'</td>';	
				id_message = id_message + 1;
				im_rows.append( "<tr id = 'im_rows-" + id_message + "'>"+ box_message + "</tr>" );	
				methods.scroll();				
				jQuery.ajax({ // Отсылаем параметры	
					type: "POST",
					url: "index.php",
					data: { chat: 'sent_message', message: text_message, dialog_id: dialog_id},		
					success: function(response) // Выводим то что вернул PHP
					{							
						var obj = jQuery.parseJSON(response);
						methods.options.dialog_id = obj.dialog_id;
						methods.options.id_message = obj.id_message;	
						alert('refresh '+methods.options.dialog_id);	
					}
				});	
			}	
		},
		
		scroll : function( ) 
		{		  
			var win = $('.im_rows_wrap');
			if ( win.length != 0 )
			{
				var height = win[0].scrollHeight;
				win.scrollTop(height);
			}
		},
		
		start_refresh : function( ) 
		{		
			setInterval( methods.refresh, 5000);						
		},
			
		
		//dialog_id, id_message
		refresh : function(  ) 
		{											
			var dialog_id = methods.options.dialog_id;
		//	alert('refresh '+dialog_id);	
			var id_message = methods.options.id_message;	
		
			if ( dialog_id != 0 )
			{
				jQuery.ajax({ 
					type: "POST",
					url: "index.php",
					data: { chat : 'refresh', dialog_id : dialog_id, id_message : id_message },			
					success: function(response) 
					{					//	console.log(response);	
						$('#im_rows tr').removeClass("new_message");
						var massiv = JSON.parse(response);	
				
						if ( massiv != 0 )
						{											
							var im_rows = jQuery('#im_rows');						
							jQuery.each(massiv, function() 
							{	
								id_message = this['id'];
								var box_message = '<td class = "message_body"><div class="message_user">'+this['user']+'</div><div class = "message_text">'+this['message']+'</div></td><td class = "message_date">'+ this['date'] +'</td>';
								im_rows.append( "<tr id = 'im_rows-" + id_message + "' class = 'new_message'>"+ box_message + "</tr>" );							 					
							});										
							methods.scroll();
						}
					}
				});	
			}
		}
	};
		
	$.dialog = function( method ) 
	{  		
		if ( methods[method] ) 
		{
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}
		else if ( typeof method === 'object' || ! method ) 
		{
			return methods.init.apply( this, arguments );
		} 
		else 
		{
			$.error( 'Метод с именем ' +  method + ' не существует' );
		}        
	};		
})( jQuery );
*/








function updateURLParameter(url, param, paramVal)
{
	var TheAnchor = null;
	var newAdditionalURL = "";
	var tempArray = url.split("?");
	var baseURL = tempArray[0];
	var additionalURL = tempArray[1];
	var temp = "";

	if (additionalURL) 
	{
		var tmpAnchor = additionalURL.split("#");
		var TheParams = tmpAnchor[0];
			TheAnchor = tmpAnchor[1];
		if(TheAnchor)
			additionalURL = TheParams;

		tempArray = additionalURL.split("&");

		for (i=0; i<tempArray.length; i++)
		{
			if(tempArray[i].split('=')[0] != param)
			{
				newAdditionalURL += temp + tempArray[i];
				temp = "&";
			}
		}        
	}
	else
	{
		var tmpAnchor = baseURL.split("#");
		var TheParams = tmpAnchor[0];
			TheAnchor  = tmpAnchor[1];

		if(TheParams)
			baseURL = TheParams;
	}

	if(TheAnchor)
		paramVal += "#" + TheAnchor;

	var rows_txt = temp + "" + param + "=" + paramVal;
	return baseURL + "?" + newAdditionalURL + rows_txt;
}