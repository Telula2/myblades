var ProductZoomJS = {
    version: "1.2.1",
    $F: function() {},
    defined: function(a) {
        return (undefined != a)
    },
    exists: function(a) {
        return !!(a)
    },
    j1: function(a) {
        if (!$J.defined(a)) {
            return false
        }
        if ((a instanceof window.Object || a instanceof window.Function) && a.constructor === $J.Class) {
            return "class"
        }
        if (a instanceof window.Array) {
            return "array"
        }
        if (a instanceof window.Function) {
            return "function"
        }
        if (a instanceof window.String) {
            return "string"
        }
        if ($J.v.trident) {
            if ($J.defined(a.cancelBubble)) {
                return "event"
            }
        } else {
            if (a instanceof window.Event || a === window.event) {
                return "event"
            }
        }
        if (a instanceof window.Date) {
            return "date"
        }
        if (a instanceof window.RegExp) {
            return "regexp"
        }
        if (a === window) {
            return "window"
        }
        if (a === document) {
            return "document"
        }
        if (a.length && a.item) {
            return "collection"
        }
        if (a.length && a.callee) {
            return "arguments"
        }
        if (!!a.nodeType) {
            if (1 == a.nodeType) {
                return "element"
            }
            if (3 == a.nodeType) {
                return "textnode"
            }
        }
        return typeof(a)
    },
    extend: function(c, b) {
        if (!ProductZoomJS.defined(c)) {
            return c
        }
        for (var a in (b || {})) {
            c[a] = b[a]
        }
        return c
    },
    implement: function(f, d) {
        if (!(f instanceof window.Array)) {
            f = [f]
        }
        for (var c = 0, a = f.length; c < a; c++) {
            if (!ProductZoomJS.defined(f[c])) {
                continue
            }
            for (var b in (d || {})) {
                if (!f[c].prototype[b]) {
                    f[c].prototype[b] = d[b]
                }
            }
        }
        return f[0]
    },
    $try: function() {
        for (var b = 0, a = arguments.length; b < a; b++) {
            try {
                return arguments[b]()
            } catch (c) {}
        }
        return null
    },
    $A: function(d) {
        if (!ProductZoomJS.defined(d)) {
            return []
        }
        if (d.toArray) {
            return d.toArray()
        }
        if (d.item) {
            var c = d.length || 0,
                b = new Array(length);
            while (c--) {
                b[c] = d[c]
            }
            return b
        }
        return Array.prototype.slice.call(d)
    },
    now: function() {
        return new Date().getTime()
    },
    detach: function(f) {
        var c;
        switch ($J.j1(f)) {
            case "object":
                c = {};
                for (var d in f) {
                    c[d] = $J.detach(f[d])
                }
                break;
            case "array":
                c = [];
                for (var b = 0, a = f.length; b < a; b++) {
                    c[b] = $J.detach(f[b])
                }
                break;
            default:
                return f
        }
        return c
    },
    getPageSize: function() {
        var l, g, d, j, k, h;
        var c = (!$J.v.backCompat) ? document.documentElement : document.body;
        var f = document.body;
        l = (window.innerWidth && window.scrollMaxX) ? window.innerWidth + window.scrollMaxX : (f.scrollWidth > f.offsetWidth) ? f.scrollWidth : ($J.v.trident && $J.v.backCompat) ? f.scrollWidth : f.offsetWidth;
        g = (window.innerHeight && window.scrollMaxY) ? window.innerHeight + window.scrollMaxY : (f.scrollHeight > f.offsetHeight) ? f.scrollHeight : f.offsetHeight;
        var b, a;
        b = $J.v.trident ? c.scrollWidth : (document.documentElement.clientWidth || self.innerWidth), a = $J.v.trident ? c.clientHeight : (document.documentElement.clientHeight || self.innerHeight);
        k = (self.pageXOffset) ? self.pageXOffset : c.scrollLeft;
        h = (self.pageYOffset) ? self.pageYOffset : c.scrollTop;
        d = (g < a) ? a : g;
        j = (l < b) ? b : l;
        return {
            pageWidth: j,
            pageHeight: d,
            width: $J.v.trident ? c.clientWidth : (document.documentElement.clientWidth || self.innerWidth),
            height: $J.v.trident ? c.clientHeight : ($J.v.opera) ? self.innerHeight : (self.innerHeight || document.documentElement.clientHeight),
            scrollX: k,
            scrollY: h,
            viewWidth: l,
            viewHeight: g
        }
    },
    $: function(b) {
        switch ($J.j1(b)) {
            case "string":
                var a = document.getElementById(b);
                if ($J.defined(a)) {
                    return $J.$(a)
                }
                b = null;
                break;
            case "window":
            case "document":
                b = $J.extend(b, $J.Event.Functions);
                b = $J.extend(b, $J.Doc);
                break;
            case "element":
                b = $J.extend(b, $J.Element);
                b = $J.extend(b, $J.Event.Functions);
                break;
            case "event":
                b = $J.extend(b, $J.Event.Methods);
                break;
            case "function":
            case "array":
            case "date":
            default:
                break
        }
        return b
    },
    $new: function(a, c, b) {
        return $j(document.createElement(a)).setProps(c).p(b)
    }
};
window.$J = ProductZoomJS;
window.$j = ProductZoomJS.$;
ProductZoomJS.implement(Array, {
    indexOf: function(d, f) {
        var a = this.length;
        for (var b = this.length, c = (f < 0) ? Math.max(0, b + f) : f || 0; c < b; c++) {
            if (this[c] === d) {
                return c
            }
        }
        return -1
    },
    contains: function(a, b) {
        return this.indexOf(a, b) != -1
    },
    forEach: function(a, d) {
        for (var c = 0, b = this.length; c < b; c++) {
            if (c in this) {
                a.call(d, this[c], c, this)
            }
        }
    },
    filter: function(a, g) {
        var f = [];
        for (var d = 0, b = this.length; d < b; d++) {
            if (d in this) {
                var c = this[d];
                if (a.call(g, this[d], d, this)) {
                    f.push(c)
                }
            }
        }
        return f
    },
    map: function(a, f) {
        var d = [];
        for (var c = 0, b = this.length; c < b; c++) {
            if (c in this) {
                d[c] = a.call(f, this[c], c, this)
            }
        }
        return d
    }
});
Array.prototype.j14 = Array.prototype.forEach;
ProductZoomJS.implement(String, {
    j19: function() {
        return this.replace(/^\s+|\s+$/g, "")
    },
    trimLeft: function() {
        return this.replace(/^\s+/g, "")
    },
    trimRight: function() {
        return this.replace(/\s+$/g, "")
    },
    j20: function(a) {
        if ("string" != $J.j1(a)) {
            return false
        }
        return (this.toString() === a.toString())
    },
    ij20: function(a) {
        if ("string" != $J.j1(a)) {
            return false
        }
        return (this.toLowerCase().toString() === a.toLowerCase().toString())
    },
    k: function() {
        return this.replace(/-\D/g, function(a) {
            return a.charAt(1).toUpperCase()
        })
    },
    j22: function(a) {
        return parseInt(this, a || 10)
    },
    toFloat: function() {
        return parseFloat(this)
    },
    j23: function() {
        return !this.replace(/true/i, "").j19()
    },
    has: function(b, a) {
        a = a || "";
        return (a + this + a).indexOf(a + b + a) > -1
    }
});
ProductZoomJS.v = {
    features: {
        xpath: !!(document.evaluate),
        air: !!(window.runtime)
    },
    engine: (window.opera) ? "presto" : (window.attachEvent) ? "trident" : (!navigator.taintEnabled) ? "webkit" : (null != document.getBoxObjectFor) ? "gecko" : "unknown",
    version: "",
    platform: (ProductZoomJS.defined(window.orientation)) ? "ipod" : (navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase(),
    backCompat: document.compatMode && "backcompat" == document.compatMode.toLowerCase(),
    getDoc: function() {
        return (document.compatMode && "backcompat" == document.compatMode.toLowerCase()) ? document.body : document.documentElement
    },
    domLoaded: false
};
(function() {
    ProductZoomJS.v.version = ("presto" == ProductZoomJS.v.engine) ? ((document.getElementsByClassName) ? 950 : 925) : ("trident" == ProductZoomJS.v.engine) ? !!(window.XMLHttpRequest && window.postMessage) ? 6 : ((window.XMLHttpRequest) ? 5 : 4) : ("webkit" == ProductZoomJS.v.engine) ? ((ProductZoomJS.v.features.xpath) ? 420 : 419) : ("gecko" == ProductZoomJS.v.engine) ? ((document.getElementsByClassName) ? 19 : 18) : "";
    ProductZoomJS.v[ProductZoomJS.v.engine] = ProductZoomJS.v[ProductZoomJS.v.engine + ProductZoomJS.v.version] = true
})();
ProductZoomJS.Element = {
    j13: function(a) {
        return this.className.has(a, " ")
    },
    j2: function(a) {
        if (!this.j13(a)) {
            this.className += (this.className ? " " : "") + a
        }
        return this
    },
    j3: function(a) {
        this.className = this.className.replace(new RegExp("(^|\\s)" + a + "(?:\\s|$)"), "$1").j19();
        return this
    },
    j4: function(a) {
        return this.j13(a) ? this.j3(a) : this.j2(a)
    },
    j5: function(b) {
        b = b == "float" ? "cssFloat" : b.k();
        var c = this.style[b];
        if (!c && document.defaultView) {
            var a = document.defaultView.getComputedStyle(this, null);
            c = a ? a[b] : null
        } else {
            if (!c && this.currentStyle) {
                c = this.currentStyle[b]
            }
        }
        if ("opacity" == b) {
            return $J.defined(c) ? parseFloat(c) : 1
        }
        if (/^(border(Top|Bottom|Left|Right)Width)|((padding|margin)(Top|Bottom|Left|Right))$/.test(b)) {
            c = parseInt(c) ? c : "0px"
        }
        return ("auto" == c ? null : c)
    },
    p: function(b) {
        for (var a in b) {
            try {
                if ("opacity" == a) {
                    this.g(b[a]);
                    continue
                }
                if ("float" == a) {
                    this.style[("undefined" === typeof(this.style.styleFloat)) ? "cssFloat" : "styleFloat"] = b[a];
                    continue
                }
                this.style[a.k()] = b[a] + (("number" == $J.j1(b[a]) && !["zIndex", "zoom"].contains(a.k())) ? "px" : "")
            } catch (c) {}
        }
        return this
    },
    g: function(a) {
        a = parseFloat(a);
        if (a == 0) {
            if ("hidden" != this.style.visibility) {
                this.style.visibility = "hidden"
            }
        } else {
            if (a > 1) {
                a = parseFloat(a / 100)
            }
            if ("visible" != this.style.visibility) {
                this.style.visibility = "visible"
            }
        }
        if (!this.currentStyle || !this.currentStyle.hasLayout) {
            this.style.zoom = 1
        }
        if ($J.v.trident) {
            this.style.filter = (1 == a) ? "" : "alpha(opacity=" + a * 100 + ")"
        }
        this.style.opacity = a;
        return this
    },
    s: function() {
        return this.p({
            display: "none",
            visibility: "hidden"
        })
    },
    j27: function() {
        return this.p({
            display: "block",
            visibility: "visible"
        })
    },
    j7: function() {
        return {
            width: this.offsetWidth,
            height: this.offsetHeight
        }
    },
    j10: function() {
        return {
            top: this.scrollTop,
            left: this.scrollLeft
        }
    },
    j11: function() {
        var a = this,
            b = {
                top: 0,
                left: 0
            };
        do {
            b.left += a.scrollLeft || 0;
            b.top += a.scrollTop || 0;
            a = a.parentNode
        } while (a);
        return b
    },
    j8: function() {
        if ($J.defined(document.documentElement.getBoundingClientRect)) {
            var a = this.getBoundingClientRect(),
                d = $j(document).j10(),
                g = $J.v.getDoc();
            return {
                top: a.top + d.y - g.clientTop,
                left: a.left + d.x - g.clientLeft
            }
        }
        var f = this,
            c = t = 0;
        do {
            c += f.offsetLeft || 0;
            t += f.offsetTop || 0;
            f = f.offsetParent
        } while (f && !(/^(?:body|html)$/i).test(f.tagName));
        return {
            top: t,
            left: c
        }
    },
    j9: function() {
        var b = this.j8();
        var a = this.j7();
        return {
            top: b.top,
            bottom: b.top + a.height,
            left: b.left,
            right: b.left + a.width
        }
    },
    update: function(b) {
        try {
            this.innerHTML = b
        } catch (a) {
            this.innerText = b
        }
        return this
    },
    remove: function() {
        return (this.parentNode) ? this.parentNode.removeChild(this) : this
    }
};
ProductZoomJS.Element.j30 = ProductZoomJS.Element.j5;
ProductZoomJS.Element.j31 = ProductZoomJS.Element.p;
ProductZoomJS.Doc = {
    j7: function() {
        if ($J.v.presto || $J.v.webkit) {
            return {
                width: self.innerWidth,
                height: self.innerHeight
            }
        }
        return {
            width: $J.v.getDoc().clientWidth,
            height: $J.v.getDoc().clientHeight
        }
    },
    j10: function() {
        return {
            x: self.pageXOffset || $J.v.getDoc().scrollLeft,
            y: self.pageYOffset || $J.v.getDoc().scrollTop
        }
    },
    j12: function() {
        var a = this.j7();
        return {
            width: Math.max($J.v.getDoc().scrollWidth, a.width),
            height: Math.max($J.v.getDoc().scrollHeight, a.height)
        }
    }
};
ProductZoomJS.Event = {
    Methods: 
	{
        stop: function() {
            if (this.stopPropagation) {
                this.stopPropagation()
            } else {
                this.cancelBubble = true
            }
            if (this.preventDefault) {
                this.preventDefault()
            } else {
                this.returnValue = false
            }
            return this
        },
        j15: function() {
            return {
                x: this.pageX || this.clientX + $J.v.getDoc().scrollLeft,
                y: this.pageY || this.clientY + $J.v.getDoc().scrollTop
            }
        },
        getTarget: function() {
            var a = this.target || this.srcElement;
            while (a && a.nodeType == 3) {
                a = a.parentNode
            }
            return a
        },
        getRelated: function() {
            var a = null;
            switch (this.type) {
                case "mouseover":
                    a = this.relatedTarget || this.fromElement;
                    break;
                case "mouseout":
                    a = this.relatedTarget || this.toElement;
                    break;
                default:
                    return a
            }
            while (a && a.nodeType == 3) {
                a = a.parentNode
            }
            return a
        }
    },
    Functions: 
	{
        a: function(a, b) {
            if ((this === document || this === window) && "domready" == a) {
                if ($J.v.domLoaded) {
                    b.call(this);
                    return
                }
                $J.onDomReadyList.push(b);
                if ($J.onDomReadyList.length <= 1) {
                    $J.bindDomReady()
                }
            }
            if (this.addEventListener) {
                this.addEventListener(a, b, false)
            } else {
                this.attachEvent("on" + a, b)
            }
        },
        j26: function(a, b) {
            if (this.removeEventListener) {
                this.removeEventListener(a, b, false)
            } else {
                this.detachEvent("on" + a, b)
            }
        },
        raiseEvent: function(d, c) {
            var b = this;
            if (b === document && document.createEvent && !b.dispatchEvent) {
                b = document.documentElement
            }
            var a;
            if (document.createEvent) {
                a = document.createEvent(d);
                a.initEvent(c, true, true)
            } else {
                a = document.createEventObject();
                a.eventType = d
            }
            if (document.createEvent) {
                b.dispatchEvent(a)
            } else {
                b.fireEvent("on" + evName, a)
            }
            return a
        }
    }
};
ProductZoomJS.extend(ProductZoomJS, 
{
    onDomReadyList: [],
    onDomReadyTimer: null,
    onDomReady: function() {
        if ($J.v.domLoaded) {
            return
        }
        $J.v.domLoaded = true;
        if ($J.onDomReadyTimer) {
            clearTimeout($J.onDomReadyTimer);
            $J.onDomReadyTimer = null
        }
        for (var b = 0, a = $J.onDomReadyList.length; b < a; b++) {
            $J.onDomReadyList[b].apply(document)
        }
    },
    bindDomReady: function() {
        if ($J.v.webkit) {
            (function() {
                if (["loaded", "complete"].contains(document.readyState)) {
                    $J.onDomReady();
                    return
                }
                $J.onDomReadyTimer = setTimeout(arguments.callee, 50);
                return
            })()
        }
        if ($J.v.ie && window == top) {
            (function() {
                try {
                    $J.v.getDoc().doScroll("left")
                } catch (a) {
                    $J.onDomReadyTimer = setTimeout(arguments.callee, 50);
                    return
                }
                $J.onDomReady()
            })()
        }
        if ($J.v.presto) {
            $j(document).a("DOMContentLoaded", function() {
                for (var b = 0, a = document.styleSheets.length; b < a; b++) {
                    if (document.styleSheets[b].disabled) {
                        $J.onDomReadyTimer = setTimeout(arguments.callee, 50);
                        return
                    }
                    $J.onDomReady()
                }
            })
        }
        $j(document).a("DOMContentLoaded", $J.onDomReady);
        $j(window).a("load", $J.onDomReady)
    }
});
if (Function.prototype.hasOwnProperty("bind")) {
    Function.prototype._bind_ = Function.prototype.bind;
    delete Function.prototype.bind
}
ProductZoomJS.implement(Function, {
    bind: function() {
        var b = $J.$A(arguments),
            a = this,
            c = b.shift();
        if (this._bind_ && this.pass) {
            return this.pass(b, c)
        }
        return function() {
            return a.apply(c, b.concat($J.$A(arguments)))
        }
    },
    j18: function() {
        var b = $J.$A(arguments),
            a = this,
            c = b.shift();
        return function(d) {
            return a.apply(c, [d || window.event].concat(b))
        }
    },
    j32: function() {
        var b = $J.$A(arguments),
            a = this,
            c = b.shift();
        return window.setTimeout(function() {
            return a.apply(a, b)
        }, c || 0)
    },
    interval: function() {
        var b = $J.$A(arguments),
            a = this,
            c = b.shift();
        return window.setInterval(function() {
            return a.apply(a, b)
        }, c || 0)
    }
});
ProductZoomJS.Class = function() {
    var f = null,
        b = $J.$A(arguments);
    if ("class" == $J.j1(b[0])) {
        f = b.shift()
    }
    var a = function() {
        for (var j in this) {
            this[j] = $J.detach(this[j])
        }
        var h = (this.init) ? this.init.apply(this, arguments) : this;
        if (this.constructor.$parent) {
            this.$parent = {};
            var n = this.constructor.$parent;
            for (var l in n) {
                var g = n[l];
                switch ($J.j1(g)) {
                    case "function":
                        this.$parent[l] = $J.Class.wrap(this, g);
                        break;
                    case "object":
                        this.$parent[l] = $J.detach(g);
                        break;
                    case "array":
                        this.$parent[l] = $J.detach(g);
                        break
                }
            }
            delete this.constructor.$parent
        }
        delete this.caller;
        return h
    };
    if (!a.prototype.init) {
        a.prototype.init = $J.$F
    }
    if (f) {
        var d = function() {};
        d.prototype = f.prototype;
        a.prototype = new d;
        a.$parent = {};
        for (var c in f.prototype) {
            a.$parent[c] = f.prototype[c]
        }
    } else {
        a.$parent = null
    }
    a.constructor = $J.Class;
    a.prototype.constructor = a;
    $J.extend(a.prototype, b[0]);
    return a
};
ProductZoomJS.Class.wrap = function(a, b) {
    return function() {
        var d = this.caller;
        var c = b.apply(a, arguments);
        return c
    }
};
ProductZoomJS.FX = new $J.Class({
    defaults: {
        fps: 50,
        duration: 500,
        transition: function(a) {
            return -(Math.cos(Math.PI * a) - 1) / 2
        },
        onStart: $J.$F,
        onComplete: $J.$F,
        onBeforeRender: $J.$F
    },
    init: function(b, a) {
        this.el = $j(b);
        this.options = $J.extend($J.extend({}, this.defaults), a);
        this.timer = false
    },
    start: function(a) {
        this.styles = a;
        this.state = 0;
        this.curFrame = 0;
        this.startTime = $J.now();
        this.finishTime = this.startTime + this.options.duration;
        this.timer = this.loop.bind(this).interval(Math.round(1000 / this.options.fps));
        this.options.onStart();
        return this
    },
    stop: function(a) {
        a = $J.defined(a) ? a : false;
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = false
        }
        if (a) {
            this.render(1);
            setTimeout(this.options.onComplete, 10)
        }
        return this
    },
    calc: function(c, b, a) {
        return (b - c) * a + c
    },
    loop: function() {
        var b = $J.now();
        if (b >= this.finishTime) {
            if (this.timer) {
                clearInterval(this.timer);
                this.timer = false
            }
            this.render(1);
            setTimeout(this.options.onComplete, 10);
            return this
        }
        var a = this.options.transition((b - this.startTime) / this.options.duration);
        this.render(a)
    },
    render: function(a) {
        var c = {};
        for (var b in this.styles) {
            if ("opacity" === b) {
                c[b] = Math.round(this.calc(this.styles[b][0], this.styles[b][1], a) * 100) / 100
            } else {
                c[b] = Math.round(this.calc(this.styles[b][0], this.styles[b][1], a))
            }
        }
        this.options.onBeforeRender(c);
        this.el.p(c)
    }
});
ProductZoomJS.FX.Transition = {
    linear: function(a) {
        return a
    },
    sin: function(a) {
        return -(Math.cos(Math.PI * a) - 1) / 2
    },
    quadIn: function(a) {
        return Math.pow(a, 2)
    },
    quadOut: function(a) {
        return 1 - MagicTools.Transition.quadIn(1 - a)
    },
    cubicIn: function(a) {
        return Math.pow(a, 3)
    },
    cubicOut: function(a) {
        return 1 - MagicTools.Transition.cubicIn(1 - a)
    },
    backIn: function(b, a) {
        a = a || 1.618;
        return Math.pow(b, 2) * ((a + 1) * b - a)
    },
    backOut: function(b, a) {
        return 1 - MagicTools.Transition.backIn(1 - b)
    },
    elastic: function(b, a) {
        a = a || [];
        return Math.pow(2, 10 * --b) * Math.cos(20 * b * Math.PI * (a[0] || 1) / 3)
    },
    none: function(a) {
        return 0
    }
};
$J.$Ff = function() {
    return false
};


var ProductZoom = {
    version: "3.1",
    options: {},
    defaults: {
        opacity: 50,
        opacityReverse: false,
        smoothingSpeed: 40,
        fps: 25,
        zoomWidth: 300,
        zoomHeight: 300,
        zoomDistance: 15,
        zoomPosition: "right",
        dragMode: false,
        moveOnClick: false,
        alwaysShowZoom: false,
        preservePosition: false,
        x: -1,
        y: -1,
        clickToActivate: false,
        clickToInitialize: false,
        smoothing: true,
        showTitle: "true",
        thumbChange: "click",
        zoomFade: false,
        zoomFadeInSpeed: 400,
        zoomFadeOutSpeed: 200,
        hotspots: "",
        preloadSelectorsSmall: true,
        preloadSelectorsBig: false,
        showLoading: true,
        loadingMsg: "загрузка увеличенного изображения..",
        loadingOpacity: 75,
        loadingPositionX: -1,
        loadingPositionY: -1,
        selectorsMouseoverDelay: 200,
        selectorsEffect: "dissolve",
        selectorsEffectSpeed: 400,
        fitZoomWindow: true,
        entireImage: false
    },
    z40: [/^(opacity)(\s+)?:(\s+)?(\d+)$/i, /^(opacity-reverse)(\s+)?:(\s+)?(true|false)$/i, /^(smoothing\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(fps)(\s+)?:(\s+)?(\d+)$/i, /^(zoom\-width)(\s+)?:(\s+)?(\d+)(px)?/i, /^(zoom\-height)(\s+)?:(\s+)?(\d+)(px)?/i, /^(zoom\-distance)(\s+)?:(\s+)?(\d+)(px)?/i, /^(zoom\-position)(\s+)?:(\s+)?(right|left|top|bottom|custom|inner)$/i, /^(drag\-mode)(\s+)?:(\s+)?(true|false)$/i, /^(move\-on\-click)(\s+)?:(\s+)?(true|false)$/i, /^(always\-show\-zoom)(\s+)?:(\s+)?(true|false)$/i, /^(preserve\-position)(\s+)?:(\s+)?(true|false)$/i, /^(x)(\s+)?:(\s+)?([\d.]+)(px)?/i, /^(y)(\s+)?:(\s+)?([\d.]+)(px)?/i, /^(click\-to\-activate)(\s+)?:(\s+)?(true|false)$/i, /^(click\-to\-initialize)(\s+)?:(\s+)?(true|false)$/i, /^(smoothing)(\s+)?:(\s+)?(true|false)$/i, /^(show\-title)(\s+)?:(\s+)?(true|false|top|bottom)$/i, /^(thumb\-change)(\s+)?:(\s+)?(click|mouseover)$/i, /^(zoom\-fade)(\s+)?:(\s+)?(true|false)$/i, /^(zoom\-fade\-in\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(zoom\-fade\-out\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(hotspots)(\s+)?:(\s+)?([a-z0-9_\-:\.]+)$/i, /^(preload\-selectors\-small)(\s+)?:(\s+)?(true|false)$/i, /^(preload\-selectors\-big)(\s+)?:(\s+)?(true|false)$/i, /^(show\-loading)(\s+)?:(\s+)?(true|false)$/i, /^(loading\-msg)(\s+)?:(\s+)?([^;]*)$/i, /^(loading\-opacity)(\s+)?:(\s+)?(\d+)$/i, /^(loading\-position\-x)(\s+)?:(\s+)?(\d+)(px)?/i, /^(loading\-position\-y)(\s+)?:(\s+)?(\d+)(px)?/i, /^(selectors\-mouseover\-delay)(\s+)?:(\s+)?(\d+)$/i, /^(selectors\-effect)(\s+)?:(\s+)?(dissolve|fade|false)$/i, /^(selectors\-effect\-speed)(\s+)?:(\s+)?(\d+)$/i, /^(fit\-zoom\-window)(\s+)?:(\s+)?(true|false)$/i, /^(entire\-image)(\s+)?:(\s+)?(true|false)$/i],
    zooms: [],
    z1: function(b) 
	{        
		for (var a = 0; a < ProductZoom.zooms.length; a++) 
		{
            if (ProductZoom.zooms[a].z28) {
                ProductZoom.zooms[a].j17()
            } else {
                if (ProductZoom.zooms[a].options.clickToInitialize && ProductZoom.zooms[a].initMouseEvent) {
                    ProductZoom.zooms[a].initMouseEvent = b
                }
            }
        }
    },
    stop: function(a) {
        if (a.zoom) {
            a.zoom.stop();
            return true
        }
        return false
    },
    start: function(a) 
	{
        if (!a.zoom) 
		{
            var b = null;
            while (b = a.firstChild) {
                if (b.tagName == "IMG") {
                    break
                }
                a.removeChild(b)
            }
            while (b = a.lastChild) {
                if (b.tagName == "IMG") {
                    break
                }
                a.removeChild(b)
            }
            if (!a.firstChild || a.firstChild.tagName != "IMG") {
                throw "Ошибка плагина ProductZoom"
            }
            ProductZoom.zooms.push(new ProductZoom.zoom(a))
        } else {
            a.zoom.start()
        }
    },
    update: function(d, a, c, b) {
        if (d.zoom) {
            d.zoom.update(a, c, b)
        }
    },
    refresh: function() 
	{
        $J.$A(window.document.getElementsByTagName("A")).j14(function(a) 
		{
            if (/ProductZoom/.test(a.className)) 
			{				
                if (ProductZoom.stop(a)) 
				{
                    ProductZoom.start.j32(100, a)
                } 
				else 
				{
                    ProductZoom.start(a)
                }
            }
        }, this)
    },
    getXY: function(a) {
        if (a.zoom) {
            return {
                x: a.zoom.options.x,
                y: a.zoom.options.y
            }
        }
    },
    from_Char_Code: function(b) 
	{
        var a = "";
        for (i = 0; i < b.length; i++) {
            a += String.fromCharCode(14 ^ b.charCodeAt(i))
        }
        return a
    }
};
ProductZoom.z50 = function() {
    this.init.apply(this, arguments)
};
ProductZoom.z50.prototype = {
    init: function(a) {
        this.cb = null;
        this.z2 = null;
        this.onErrorHandler = this.onError.j18(this);
        this.z3 = null;
        this.width = 0;
        this.height = 0;
        this.border = {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        };
        this.padding = {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        };
        this.ready = false;
        if ("string" == $J.j1(a)) {
            this.self = $j(new Image);
            this.z4();
            this.self.src = a
        } else {
            this.self = $j(a);
            this.z4()
        }
    },
    onError: function(a) {
        if (a) {
            $j(a).stop()
        }
        if (this.cb) {
            this.cb.call(this, false)
        }
        this.unload()
    },
    z4: function() {
        this.z2 = null;
        if (!(this.self.src && (this.self.complete || this.self.readyState == "complete"))) {
            this.z2 = function(a) {
                this.ready = true;
                this.z5();
                if (this.cb) {
                    this.z6();
                    this.cb.call()
                }
            }.j18(this);
            this.self.a("load", this.z2);
            ["abort", "error"].j14(function(a) {
                this.self.a(a, this.onErrorHandler)
            }, this)
        } else {
            this.ready = true
        }
    },
    update: function(a) {
        this.unload();
        this.z3 = this.self;
        this.self = $j(new Image);
        this.z4();
        this.self.src = a
    },
    z6: function() {
        this.width = this.self.width;
        this.height = this.self.height;
        ["Left", "Right", "Top", "Bottom"].j14(function(a) {
            this.padding[a.toLowerCase()] = this.self.j30("padding" + a).j22();
            this.border[a.toLowerCase()] = this.self.j30("border" + a + "Width").j22()
        }, this);
        if ($J.v.presto || ($J.v.trident && !$J.v.backCompat)) {
            this.width -= this.padding.left + this.padding.right;
            this.height -= this.padding.top + this.padding.bottom
        }
    },
    getBox: function() {
        var a = null;
        a = this.self.j9();
        return {
            top: a.top + this.border.top,
            bottom: a.bottom - this.border.bottom,
            left: a.left + this.border.left,
            right: a.right - this.border.right
        }
    },
    z5: function() {
        if (this.z3) {
            this.z3.src = this.self.src;
            this.self = null;
            this.self = this.z3
        }
    },
    load: function(a) {
        if (this.ready) {
            if (!this.width) {
                this.z6()
            }
            a.call()
        } else {
            this.cb = a
        }
    },
    unload: function() {
        if (this.z2) {
            this.self.j26("load", this.z2)
        }["abort", "error"].j14(function(a) {
            this.self.j26(a, this.onErrorHandler)
        }, this);
        this.z2 = null;
        this.cb = null;
        this.width = null;
        this.ready = false
    }
};
ProductZoom.zoom = function() {
    this.construct.apply(this, arguments)
};

ProductZoom.zoom.prototype = 
{
    construct: function(b, a) 
	{
        this.z25 = -1;
        this.z28 = false;
        this.ddx = 0;
        this.ddy = 0;
        this.options = $J.detach(ProductZoom.defaults);
        if (b) {
            this.c = $j(b)
        }
        this.z37(this.c.rel);
        if (a) {
            this.z37(a)
        }
        if (b) 
		{
            this.z7 = this.mousedown.j18(this);
            this.z8 = this.mouseup.j18(this);
            this.z9 = this.j27.bind(this, false), this.z10 = this.z26.bind(this), this.z46Bind = this.z46.j18(this);
            this.c.a("click", function(c) {
                if (!$J.v.trident) {
                    this.blur
                }
                $j(c).stop();
                return false
            });
            this.c.a("mousedown", this.z7);
            this.c.a("mouseup", this.z8);
            this.c.unselectable = "on";
            this.c.style.MozUserSelect = "none";
            this.c.onselectstart = $J.$Ff;
            this.c.oncontextmenu = $J.$Ff;
            this.c.p({
                position: "relative",
                display: "inline-block",
                textDecoration: "none",
                outline: "0",
                cursor: "hand"
            });
            if (this.c.j5("textAlign") == "center") {
                this.c.p({
                    margin: "auto auto"
                })
            }
            this.c.zoom = this
        } else {
            this.options.clickToInitialize = false
        }
        if (!this.options.clickToInitialize) {
            this.z11()
        }
    },
    z11: function() {
        if (!this.q) {
            this.q = new ProductZoom.z50(this.c.firstChild);
            this.w = new ProductZoom.z50(this.c.href)
        } else {
            this.q.update(this.c.firstChild.src);
            this.w.update(this.c.href)
        }
        if (!this.e) 
		{
            this.e = {
                self: $j(document.createElement("DIV")).j2("ProductZoomBigImageCont").p({
                    overflow: "hidden",
                    zIndex: 100,
                    top: "-10000px",
                    position: "absolute",
                    width: this.options.zoomWidth + "px",
                    height: this.options.zoomHeight + "px"
                }),
                zoom: this,
                z17: "0px"
            };
            this.e.s = function() 
			{
                if (this.self.style.top != "-10000px" && !this.zoom.x.z39) {
                    this.z17 = this.self.style.top;
                    this.self.style.top = "-10000px"
                }
            };
            this.e.z18 = this.e.s.bind(this.e);
            if ($J.v.trident) {
                var b = $j(document.createElement("IFRAME"));
                b.src = "javascript:''";
                b.p({
                    left: "0px",
                    top: "0px",
                    position: "absolute"
                }).frameBorder = 0;
                this.e.z19 = this.e.self.appendChild(b)
            }
            this.e.z44 = $j(document.createElement("DIV")).j2("ProductZoomHeader").p({
                position: "relative",
                zIndex: 10,
                left: "0px",
                top: "0px",
                padding: "3px"
            }).s();
            var b = document.createElement("DIV");
            b.style.overflow = "hidden";
            b.appendChild(this.w.self);
            if (this.options.showTitle == "bottom") {
                this.e.self.appendChild(b);
                this.e.self.appendChild(this.e.z44)
            } else {
                this.e.self.appendChild(this.e.z44);
                this.e.self.appendChild(b)
            }
            if (this.options.zoomPosition == "custom" && $j(this.c.id + "-big")) {
                $j(this.c.id + "-big").appendChild(this.e.self)
            } else {
                this.c.appendChild(this.e.self)
            }
            if ("undefined" !== typeof(gd56)) {
                this.e.gd56 = $j(document.createElement("div")).p({
                    color: gd56[1],
                    fontSize: gd56[2] + "px",
                    fontWeight: gd56[3],
                    fontFamily: "Tahoma",
                    position: "absolute",
                    width: gd56[5],
                    textAlign: gd56[4],
                    left: "0px"
                }).update(ProductZoom.from_Char_Code(gd56[0]));
                this.e.self.appendChild(this.e.gd56)
            }
        }
        if (this.options.showTitle != "false" && this.c.title != "" && this.options.zoomPosition != "inner") {
            var a = this.e.z44;
            while (p = a.firstChild) {
                a.removeChild(p)
            }
            this.e.z44.appendChild(document.createTextNode(this.c.title));
            this.e.z44.j27()
        } else {
            this.e.z44.s()
        }
        if (this.c.z51 === undefined) {
            this.c.z51 = this.c.title
        }
        this.c.title = "";
        this.q.load(this.z12.bind(this))
    },
	
    z12: function(a) {
        if (!a && a != undefined) {
            return
        }
        if (!this.options.opacityReverse) {
            this.q.self.g(1)
        }
        this.c.p({
            width: this.q.width + "px"
        });
        if (this.options.showLoading) {
            this.z20 = setTimeout(this.z10, 400)
        }
        if (this.options.hotspots != "" && $j(this.options.hotspots)) {
            this.z21()
        }
        if (this.c.id != "") {
            this.z22()
        }
        this.w.load(this.z13.bind(this))
    },
    z13: function(a) {
        if (!a && a != undefined) {
            clearTimeout(this.z20);
            if (this.options.showLoading && this.o) {
                this.o.s()
            }
            return
        }
        var b = this.e.z44.j7();
        if (this.options.fitZoomWindow || this.options.entireImage) {
            if ((this.w.width < this.options.zoomWidth) || this.options.entireImage) {
                this.options.zoomWidth = this.w.width
            }
            if ((this.w.height < this.options.zoomHeight) || this.options.entireImage) {
                this.options.zoomHeight = this.w.height + b.height
            }
        }
        if (this.options.showTitle == "bottom") {
            this.w.self.parentNode.style.height = (this.options.zoomHeight - b.height) + "px"
        }
        this.e.self.p({
            height: this.options.zoomHeight + "px",
            width: this.options.zoomWidth + "px"
        }).g(1);
        if ($J.v.trident) {
            this.e.z19.p({
                width: this.options.zoomWidth + "px",
                height: this.options.zoomHeight + "px"
            })
        }
        var b = this.q.self.j9();
        switch (this.options.zoomPosition) {
            case "custom":
                break;
            case "right":
                this.e.self.style.left = b.right - b.left + this.options.zoomDistance + "px";
                this.e.z17 = "0px";
                break;
            case "left":
                this.e.self.style.left = "-" + (this.options.zoomDistance + this.options.zoomWidth) + "px";
                this.e.z17 = "0px";
                break;
            case "top":
                this.e.self.style.left = "0px";
                this.e.z17 = "-" + (this.options.zoomDistance + this.options.zoomHeight) + "px";
                break;
            case "bottom":
                this.e.self.style.left = "0px";
                this.e.z17 = b.bottom - b.top + this.options.zoomDistance + "px";
                break;
            case "inner":
                this.e.self.p({
                    left: "0px",
                    height: this.q.height + "px",
                    width: this.q.width + "px"
                });
                this.options.zoomWidth = this.q.width;
                this.options.zoomHeight = this.q.height;
                this.e.z17 = "0px";
                break
        }
        if (this.e.gd56) {
            this.e.gd56.p({
                top: this.options.showTitle == "bottom" ? "0px" : ((this.options.zoomHeight - 20) + "px")
            })
        }
        this.w.self.p({
            position: "relative",
            borderWidth: "0px",
            padding: "0px",
            left: "0px",
            top: "0px"
        });
        this.z23();
        if (this.options.alwaysShowZoom) {
            if (this.options.x == -1) {
                this.options.x = this.q.width / 2
            }
            if (this.options.y == -1) {
                this.options.y = this.q.height / 2
            }
            this.j27()
        } else {
            if (this.options.zoomFade) {
                this.r = new $J.FX(this.e.self)
            }
            this.e.self.p({
                top: "-10000px"
            })
        }
        if (this.options.showLoading && this.o) {
            this.o.s()
        }
        this.c.a("mousemove", this.z46Bind);
        this.c.a("mouseout", this.z46Bind);
        if (!this.options.clickToActivate) {
            this.z28 = true
        }
        if (this.options.clickToInitialize && this.initMouseEvent) {
            this.z46(this.initMouseEvent)
        }
        this.z25 = $J.now()
    },
    z26: function() {
        if (this.w.ready) {
            return
        }
        this.o = $j(document.createElement("DIV")).j2("ProductZoomLoading").g(this.options.loadingOpacity / 100).p({
            display: "block",
            overflow: "hidden",
            position: "absolute",
            visibility: "hidden",
            "z-index": 20,
            "max-width": (this.q.width - 4)
        });
        this.o.appendChild(document.createTextNode(this.options.loadingMsg));
        this.c.appendChild(this.o);
        var a = this.o.j7();
        this.o.p({
            left: (this.options.loadingPositionX == -1 ? ((this.q.width - a.width) / 2) : (this.options.loadingPositionX)) + "px",
            top: (this.options.loadingPositionY == -1 ? ((this.q.height - a.height) / 2) : (this.options.loadingPositionY)) + "px"
        });
        this.o.j27()
    },
    z22: function() 
	{
        this.selectors = [];
        $J.$A(document.getElementsByTagName("A")).j14(function(f) {
            var c = new RegExp("^" + this.c.id + "$");
            var b = new RegExp("zoom\\-id(\\s+)?:(\\s+)?" + this.c.id + "($|;)");
            if (c.test(f.rel) || b.test(f.rel)) {
                if (!$j(f).z36) {
                    f.z36 = function(g) {
                        if (!$J.v.trident) {
                            this.blur()
                        }
                        $j(g).stop();
                        return false
                    };
                    f.a("click", f.z36)
                }
                if (!f.z34) 
				{
                    f.z34 = function(h, g) {
                        if (h.type == "mouseout") {
                            if (this.z35) {
                                clearTimeout(this.z35)
                            }
                            this.z35 = false;
                            return
                        }
                        if (g.title != "") {
                            this.c.title = g.title
                        }
                        if (h.type == "mouseover") {
                            this.z35 = setTimeout(this.update.bind(this, g.href, g.rev, g.rel), this.options.selectorsMouseoverDelay)
                        } else {
                            this.update(g.href, g.rev, g.rel)
                        }
                    }.j18(this, f);
                    f.a(this.options.thumbChange, f.z34);
                    if (this.options.thumbChange == "mouseover") {
                        f.a("mouseout", f.z34)
                    }
                }
                f.p({
                    outline: "0"
                });
                if (this.options.preloadSelectorsSmall) {
                    var d = new Image();
                    d.src = f.rev
                }
                if (this.options.preloadSelectorsBig) {
                    var a = new Image();
                    a.src = f.href
                }
                this.selectors.push(f)
            }
        }, this)
    },
    stop: function(a) {
        try {
            this.j17();
            this.c.j26("mousemove", this.z46Bind);
            this.c.j26("mouseout", this.z46Bind);
            if (undefined == a) {
                this.x.self.s()
            }
            if (this.r) {
                this.r.stop()
            }
            this.y = null;
            this.z28 = false;
            this.selectors.j14(function(c) {
                if (undefined == a) {
                    c.j26(this.options.thumbChange, c.z34);
                    if (this.options.thumbChange == "mouseover") {
                        c.j26("mouseout", c.z34)
                    }
                    c.z34 = null;
                    c.j26("click", c.z36);
                    c.z36 = null
                }
            }, this);
            if (this.options.hotspots != "" && $j(this.options.hotspots)) {
                $j(this.options.hotspots).s();
                $j(this.options.hotspots).z30.insertBefore($j(this.options.hotspots), $j(this.options.hotspots).z31);
                if (this.c.z32) {
                    this.c.removeChild(this.c.z32)
                }
            }
            this.q.unload();
            this.w.unload();
            this.r = null;
            if (this.o) {
                this.c.removeChild(this.o)
            }
            if (undefined == a) {
                this.c.removeChild(this.x.self);
                this.e.self.parentNode.removeChild(this.e.self);
                this.x = null;
                this.e = null;
                this.w = null;
                this.q = null
            }
            if (this.z20) {
                clearTimeout(this.z20);
                this.z20 = null
            }
            this.z48 = null;
            this.c.z32 = null;
            this.o = null;
            if (this.c.title == "") {
                this.c.title = this.c.z51
            }
            this.z25 = -1
        } catch (b) {}
    },
    start: function(a) {
        if (this.z25 != -1) {
            return
        }
        this.construct(false, a)
    },
	
    update: function(a, g, f) 
	{
        if ($J.now() - this.z25 < 300 || this.z25 == -1 || this.ufx) {
            var b = 300 - $J.now() + this.z25;
            if (this.z25 == -1) {
                b = 300
            }
            this.z35 = setTimeout(this.update.bind(this, a, g, f), b);
            return
        }
        var d = (function() {
            if (undefined != a) {
                this.c.href = a
            }
            if (undefined != g) {
                this.c.firstChild.src = g
            }
            if (undefined == f) {
                f = ""
            }
            if (this.options.preservePosition) {
                f = "x: " + this.options.x + "; y: " + this.options.y + "; " + f
            }
        }).bind(this);
        this.stop(true);
        if (this.options.selectorsEffect != "false" && this.options.preloadSelectorsSmall) {
            this.ufx = true;
            if (this.options.selectorsEffect == "fade") {
                new $J.FX(this.c.firstChild, {
                    duration: this.options.selectorsEffectSpeed * 0.9
                }).start({
                    opacity: [1, 0]
                })
            }
            var c = $j(document.createElement("IMG")).p({
                opacity: 0,
                position: "absolute",
                left: "0px",
                top: "0px"
            });
            c.src = g;
            this.c.appendChild(c);
            new $J.FX(c, {
                duration: this.options.selectorsEffectSpeed,
                onComplete: (function() {
                    d.call();
                    $j(this.c.firstChild).p({
                        opacity: 1
                    });
                    this.c.removeChild(c);
                    this.ufx = false;
                    this.start(f)
                }).bind(this)
            }).start({
                opacity: [0, 1]
            })
        } else {
            d.call();
            this.start(f)
        }
    },
	
    z37: function(a) 
	{
        var c = null;
        var b = [];
        var d = a.split(";");
        d.j14(function(f) {
            ProductZoom.z40.j14(function(g) {
                c = g.exec(f.j19());
                if (c) {
                    switch ($J.j1(ProductZoom.defaults[c[1].k()])) {
                        case "boolean":
                            b[c[1].k()] = c[4] === "true";
                            break;
                        case "number":
                            b[c[1].k()] = parseFloat(c[4]);
                            break;
                        default:
                            b[c[1].k()] = c[4]
                    }
                }
            }, this)
        }, this);
        b = $J.extend(b, ProductZoom.options);
        if (b.dragMode && undefined === b.alwaysShowZoom) {
            b.alwaysShowZoom = true
        }
        this.options = $J.extend(this.options, b)
    },
	
    z23: function() 
	{
        if (!this.x) 
		{
            this.x = {
                self: $j(document.createElement("DIV")).j2("ProductZoomPup").p({
                    zIndex: 10,
                    position: "absolute",
                    overflow: "hidden"
                }).s(),
                width: 20,
                height: 20
            };
            this.c.appendChild(this.x.self)
        }
        if (this.options.entireImage) {
            this.x.self.p({
                "border-width": "0px"
            })
        }
        this.x.z39 = false;
        var b = this.e.z44.j7();
        this.x.height = (this.options.zoomHeight - b.height) / (this.w.height / this.q.height);
        this.x.width = this.options.zoomWidth / (this.w.width / this.q.width);
        if (this.x.width > this.q.width) {
            this.x.width = this.q.width
        }
        if (this.x.height > this.q.height) {
            this.x.height = this.q.height
        }
        this.x.width = Math.round(this.x.width);
        this.x.height = Math.round(this.x.height);
        this.x.borderWidth = this.x.self.j30("borderLeftWidth").j22();
        this.x.self.p({
            width: (this.x.width - 2 * ($J.v.backCompat ? 0 : this.x.borderWidth)) + "px",
            height: (this.x.height - 2 * ($J.v.backCompat ? 0 : this.x.borderWidth)) + "px"
        });
        if (!this.options.opacityReverse) {
            this.x.self.g(parseFloat(this.options.opacity / 100));
            if (this.x.z45) {
                this.x.self.removeChild(this.x.z45);
                this.x.z45 = null
            }
        } else {
            this.x.self.g(1);
            if (this.x.z45) {
                this.x.z45.src = this.q.self.src
            } else {
                var a = this.q.self.cloneNode(false);
                a.unselectable = "on";
                this.x.z45 = $j(this.x.self.appendChild(a)).p({
                    position: "absolute",
                    zIndex: 5
                })
            }
        }
    },
	
    z46: function(b, a) 
	{
        if (!this.z28 || b === undefined) {
            return false
        }
        $j(b).stop();
        if (a === undefined) {
            var a = $j(b).j15()
        }
        if (this.y == null) {
            this.y = this.q.getBox()
        }
        if (a.x > this.y.right || a.x < this.y.left || a.y > this.y.bottom || a.y < this.y.top) {
            this.j17();
            return false
        }
        if (b.type == "mouseout") {
            return false
        }
        if (this.options.dragMode && !this.z49) {
            return false
        }
        if (!this.options.moveOnClick) {
            a.x -= this.ddx;
            a.y -= this.ddy
        }
        if ((a.x + this.x.width / 2) >= this.y.right) {
            a.x = this.y.right - this.x.width / 2
        }
        if ((a.x - this.x.width / 2) <= this.y.left) {
            a.x = this.y.left + this.x.width / 2
        }
        if ((a.y + this.x.height / 2) >= this.y.bottom) {
            a.y = this.y.bottom - this.x.height / 2
        }
        if ((a.y - this.x.height / 2) <= this.y.top) {
            a.y = this.y.top + this.x.height / 2
        }
        this.options.x = a.x - this.y.left;
        this.options.y = a.y - this.y.top;
        if (this.z48 == null) {
            if ($J.v.trident) {
                this.c.style.zIndex = 1
            }
            this.z48 = setTimeout(this.z9, 10)
        }
        return true
    },
	
    j27: function() 
	{
        var f = this.x.width / 2;
        var j = this.x.height / 2;
        this.x.self.style.left = this.options.x - f + this.q.border.left + "px";
        this.x.self.style.top = this.options.y - j + this.q.border.top + "px";
        if (this.options.opacityReverse) {
            this.x.z45.style.left = "-" + (parseFloat(this.x.self.style.left) + this.x.borderWidth) + "px";
            this.x.z45.style.top = "-" + (parseFloat(this.x.self.style.top) + this.x.borderWidth) + "px"
        }
        var d = (this.options.x - f) * (this.w.width / this.q.width);
        var c = (this.options.y - j) * (this.w.height / this.q.height);
        if (this.w.width - d < this.options.zoomWidth) {
            d = this.w.width - this.options.zoomWidth;
            if (d < 0) {
                d = 0
            }
        }
        if (this.w.height - c < this.options.zoomHeight) {
            c = this.w.height - this.options.zoomHeight;
            if (c < 0) {
                c = 0
            }
        }
        if (document.documentElement.dir == "rtl") {
            d = (this.options.x + this.x.width / 2 - this.q.width) * (this.w.width / this.q.width)
        }
        d = Math.round(d);
        c = Math.round(c);
        if (this.options.smoothing == false || !this.x.z39) {
            this.w.self.style.left = (-d) + "px";
            this.w.self.style.top = (-c) + "px"
        } else {
            var h = parseInt(this.w.self.style.left);
            var g = parseInt(this.w.self.style.top);
            var b = (-d - h);
            var a = (-c - g);
            if (!b && !a) {
                this.z48 = null;
                return
            }
            b *= this.options.smoothingSpeed / 100;
            if (b < 1 && b > 0) {
                b = 1
            } else {
                if (b > -1 && b < 0) {
                    b = -1
                }
            }
            h += b;
            a *= this.options.smoothingSpeed / 100;
            if (a < 1 && a > 0) {
                a = 1
            } else {
                if (a > -1 && a < 0) {
                    a = -1
                }
            }
            g += a;
            this.w.self.style.left = h + "px";
            this.w.self.style.top = g + "px"
        }
        if (!this.x.z39)
		{
            if (this.r) 
			{
                this.r.stop();
                this.r.options.onComplete = $J.$F;
                this.r.options.duration = this.options.zoomFadeInSpeed;
                this.e.self.g(0);
                this.r.start({
                    opacity: [0, 1]
                })
            }
            if (this.options.zoomPosition != "inner") {
                this.x.self.j27()
            }
            this.e.self.style.top = this.e.z17;
            if (this.options.opacityReverse) {
                this.c.j2("ProductZoomPup").j31({
                    "border-width": "0px"
                });
                this.q.self.g(parseFloat((100 - this.options.opacity) / 100))
            }
            this.x.z39 = true
        }
        if (this.z48) {
            this.z48 = setTimeout(this.z9, 1000 / this.options.fps)
        }
    },
	
    j17: function() 
	{
        if (this.z48) {
            clearTimeout(this.z48);
            this.z48 = null
        }
        if (!this.options.alwaysShowZoom && this.x.z39) {
            this.x.z39 = false;
            this.x.self.s();
            if (this.r) {
                this.r.stop();
                this.r.options.onComplete = this.e.z18;
                this.r.options.duration = this.options.zoomFadeOutSpeed;
                var a = this.e.self.j30("opacity");
                this.r.start({
                    opacity: [a, 0]
                })
            } else {
                this.e.s()
            }
            if (this.options.opacityReverse) {
                this.c.j3("ProductZoomPup");
                this.q.self.g(1)
            }
        }
        this.y = null;
        if (this.options.clickToActivate) {
            this.z28 = false
        }
        if (this.options.dragMode) {
            this.z49 = false
        }
        if ($J.v.trident) {
            this.c.style.zIndex = 0
        }
    },
	
    mousedown: function(b) 
	{		
        $j(b).stop();
        if (this.options.clickToInitialize && !this.q) {
            this.initMouseEvent = b;
            this.z11();
            return
        }
        if (this.w && this.options.clickToActivate && !this.z28) {
            this.z28 = true;
            this.z46(b)
        }
        if (this.options.dragMode) {
            this.z49 = true;
            if (!this.options.moveOnClick) {
                var a = b.j15();
                this.ddx = a.x - this.options.x - this.y.left;
                this.ddy = a.y - this.options.y - this.y.top;
                if (Math.abs(this.ddx) > this.x.width / 2 || Math.abs(this.ddy) > this.x.height / 2) {
                    this.z49 = false;
                    return
                }
            }
        }
        if (this.options.moveOnClick) {
            this.z46(b)
        }
    },
	
    mouseup: function(a) 
	{		
        $j(a).stop();
        if (this.options.dragMode) {
            this.z49 = false
        }
    }
};
if ($J.v.trident) 
{
    try {
        document.execCommand("BackgroundImageCache", false, true)
    } catch (e) {}
}
$j(document).a("domready", ProductZoom.refresh);
$j(document).a("mousemove", ProductZoom.z1);