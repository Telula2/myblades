(function($){
	$.extend(USAM_Admin_Bar, 
	{		
		timeout    : 10000,		
		init : function() 
		{
			$(function()
			{				
				$('#wpadminbar')
					.delegate('#wp-admin-bar-usam-menu-working_day-item', 'click', USAM_Admin_Bar.get_form)				
					.delegate('#wp-admin-bar-usam-menu-task_manager-item .delete', 'click', USAM_Admin_Bar.delete_event_task_manager);
				
				if ( $('#wp-admin-bar-usam-menu-online_consultant-item .selector_status_consultant').hasClass('active') )		
				{		
					setInterval('USAM_Admin_Bar.refresh()',USAM_Admin_Bar.timeout); 
				}				
			});
		},
		
		delete_event_task_manager : function( ) 
		{	
			var	event_id = $(this).data('event_id'),
				li = $(this).closest('li'),
				post_data   = {
					action   : 'delete_event_task_manager',			    
					id       : event_id,
					nonce    : USAM_Admin_Bar.delete_event_task_manager_nonce
				},
				ajax_callback = function(response)
				{					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}										
				};		
			li.remove();			
			$.usam_post(post_data, ajax_callback);		
		},
		
		refresh : function() 
		{			
			var post_data = {
					'action'     : 'number_of_unread_menager_chat_messages',					
					'nonce'      : USAM_Admin_Bar.number_of_unread_menager_chat_messages_nonce
				},
				response_handler = function(response) 
				{
					if ( response.obj )
					{						
						var number = $('#wp-admin-bar-usam-menu-online_consultant-item .number_events .numbers').text( );	
						if ( number < response.obj )
						{ 
							$('#wp-admin-bar-usam-menu-online_consultant-item .number_events').removeClass( 'count-'+number );	
							$('#wp-admin-bar-usam-menu-online_consultant-item .number_events').addClass('count-'+response.obj);									
							$('#wp-admin-bar-usam-menu-online_consultant-item .number_events .numbers').text( response.obj );								
							if ( $('#chat_audio').length != 0 )
								$('#chat_audio')[0].play();	
						}
					}
				};
			jQuery.usam_post(post_data, response_handler);				
		},
		
		get_form : function() 
		{ 
			$('#form_dialog').modal();				
		},				
	});
})(jQuery);
USAM_Admin_Bar.init();