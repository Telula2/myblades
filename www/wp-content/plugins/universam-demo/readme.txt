﻿=== UNIVERSAM ===
Contributors: universam
Donate link: http://wp-universam.ru/buy/
Tags: ecommerce, e-commerce, crm, business, store, sales, sell, shop, cart, checkout, downloadable, downloads, storefront
Requires at least: 4.8
Tested up to: 4.9
Stable tag: 4.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Платформа для интернет-магазина и бизнеса UNIVERSAM c CRM предлагает потрясающие возможности для продажи через интернет-магазин. Множество цен на товары, потрясающие скидки на товар или корзину с множеством настроек, что даст тысячи комбинаций.
 
== Description ==

Бесплатная платформа для интернет-магазина и бизнеса с CRM, рассылкой, seo, аналитикой и другими функциями!

Мы с 2012 года помогаем интернет-магазинам продавать товары через интернет. Предлагаем вам посмотреть платформу для бизнеса UNIVERSAM, которая умеет управлять любыми каналами продаж. Платформа для интернет-магазина UNIVERSAM предлагает потрясающие возможности для продажи через интернет-магазин за очень привлекательную цену. Настройка не займет много времени.

= Почему выбирают нас =
1.	Все что нужно для ведения бизнеса есть в одной платформе
2.	Никакой абонентской платы
3.	Все данные хранятся в вашей базе никаких облачных сервисов
4.	Максимальная автоматизация бизнес процессов сокращает персонал
5.	Подключение и управление ко многим сервисам в стандартном пакете. Например,  к социальным сетям и рекламным площадкам, аналитическим системам и телекоммуникационным сервисам и др.
6.  Почти все включено

= Пример магазина =
Посмотрите пример магазина с использованием UNIVERSAM - [computer.ru](http://computer.wp-universam.ru). Здесь вы можете посмотреть как все работает и попробовать самим изменить что-то.

= Реальные магазины =
Посмотрите один из нескольких реальных магазинов с использованием UNIVERSAM - [radov39.ru](http://radov39.ru). 5000 товаров с разделения по зонам (регионам) продаж.
Полный список смотрите на официальном сайте

= Ваши пожелания =
По просьбе крупных интернет-магазинов мы сделали зоны продаж. Они пригодятся если вам нужны разные цены и скидки для разных регионов.

= Об комплексе UNIVERSAM =
В комплекс UNIVERSAM включено:
* Отправка СМС-сообщений или красивых писем на изменение статуса заказа
* Отправка красивых писем по триггеру (по событию). При регистрации, давно не покупал, брошенная корзина и другие...
* Оплата с карт PAYPAL, Сбербанк, Onpay, Оплата по счету, Оплата при получении - сотни комбинаций
* Отслеживание отправлений Почта России
* Определение позиции сайта
* Зоны продаж. Теперь вы можете указывать зоны продаж для разделения цен, акций, слайдеров по зонам.
* Добавления товаров с разными группировками и свойствами (бренды, категории, категории скидок, характеристики товаров)
* Множество цен на товары. Показывается та цена, которая соответствует настройкам цены. Например, можно указать регион или группу покупателей(цены для оптовиков) или то и другое.
* Автоматические сопутствующие продажи, что значительно экономит время заведения нового товара
* Перепродажа товара позволит продавать товар, которого у вас нет, а цена и остаток загружается с сайта постановщика. После продажи вы просите поставщика отправить товар по указанному вами адресу. 
* Множество складов с разными настройками
* Складской учет
* Потрясающие скидки на товар или корзину с множеством настроек, что даст тысячи комбинаций. Вам будут доступны купоны и правила автоматических купонов на основе правил. Планирование по времени.
* Бонусы за покупки
* Вариации товаров
* Управление ценами
* Налоги
* Заказы с документами отгрузки и оплаты.
* Настраиваемые бланки печатных форм заказа
* Возврат товаров
* Резервы товаров документами отгрузки
* Обратная связь при помощи настраиваемые форм
* Отзывы клиентов
* Чат с клиентами
* Подключение множество электронных ящиков прямо на сайт.
* Контроль прочтения писем
* СМС сообщения о событиях, вам только нужно указать логин и пароль
* Управления делами
* Контроль работы менеджера интернет-магазина
* Автоматические события и напоминания
* Полноценная CRM
* Коммерческие предложения
* Выставления счетов
* Избранное, просмотренное, сравнение для покупателей
* Слайдер товаров и акций
* Обмен данными (текстовый файл, exel, 1С, FTP, UNIVERSAM)
* Прайс-лист партнерам
* Отчеты
* Создание рассылки при помощи шаблонов
* Передача и вконтакт данных

Если вам что-то не удобно, сообщите нам и вы возможно переделаем в следующих версиях.



--------------------------------------------------------------------------------------------------------------------




Free platform for online store and business with CRM, mailing, seo, analytics and other functions!

Since 2012 we have been helping online stores sell products online. We suggest you look at the platform for business UNIVERSAM, which is able to manage any sales channels. The platform for the online store UNIVERSAM offers tremendous opportunities for sale through an online store for a very attractive price. The setup will not take long.

= Why choose us =
1. All you need to do business is in one platform
2. No monthly fee
3. All data is stored in your database, no cloud services
4. Maximum automation of business processes reduces staff
5. Connection and management to many services in a standard package. For example, to social networks and advertising platforms, analytical systems and telecommunication services, etc.
6. Almost everything included

= Example store =
Look at the example of the store using UNIVERSAM - [computer.ru] (http://computer.wp-universam.ru). Here you can see how everything works and try to change something yourself.

= Real Stores =
Look at one of several real stores using UNIVERSAM - [radov39.ru] (http://radov39.ru). 5000 goods from the division by zones (regions) of sales.
For a full list see the official website

= Your wishes =
At the request of major online stores, we made sales zones. They are useful if you need different prices and discounts for different regions.

= About the UNIVERSAM complex =
The UNIVERSAM complex includes:
* Sending SMS messages or beautiful letters to change the status of the order
* Sending beautiful letters to the trigger (by event). At registration, for a long time did not buy, the thrown basket and others ...
* Payment with PAYPAL cards, Sberbank, Onpay, Payment by account, Payment on receipt - hundreds of combinations
* Tracking of dispatches Mail of Russia
* Determine the position of the site
* Zones of sales. Now you can specify sales areas to separate prices, shares, sliders by zones.
* Adding goods with different groupings and properties (brands, categories, categories of discounts, product characteristics)
* A lot of prices for goods. It shows the price that corresponds to the price settings. For example, you can specify a region or group of buyers (prices for wholesalers) or both.
* Automatic accompanying sales, which significantly saves the time of the establishment of a new product
* Resale of the goods will allow you to sell goods that you do not have, and the price and the rest are downloaded from the website of the director. After the sale, you ask the supplier to send the goods to the address specified by you.
* Many warehouses with different settings
* Inventory control
* Stunning discounts on the product or basket with many settings, which will give thousands of combinations. Coupons and rules of automatic coupons based on rules will be available to you. Time planning.
* Bonus for purchases
* Variations of goods
* Price Management
* Taxes
* Orders with documents of shipment and payment.
* Customized forms of printed order forms
* Return of goods
* Goods reserves of shipping documents
* Feedback using custom forms
* Customer Testimonials
* Chat with customers
* Connect a lot of electronic boxes directly to the site.
* Control of reading letters
* SMS message about events, you only need to specify login and password
* Business management
* Control of the work of the manager of the online store
* Automatic events and reminders
* Full CRM
* Commercial offers
* Invoicing
* Favorites, viewed, comparison for buyers
* Goods and shares slider
* Data exchange (text file, exel, 1C, FTP, UNIVERSAM)
* Price-list to partners
* Reports
* Creating a distribution using templates
* Transfer and contact data

If something is not convenient for you, let us know and you will probably remake in the next versions.
 
== Installation ==
  
 Загрузите в папку /wp-content/plugins/ и активируйте. 
 
== Frequently Asked Questions ==
 
= Как его настроить? =
 
Во время первой активации запустится мастер установки, который вам поможет настроить UNIVERSAM. Основные настройки доступны в разделе "Настройки" wordpress дальше нажать вкладку "Магазин". Не забудьте скачать нашу тему, чтобы посмотреть как выглядит сайт для клиента.
 
= Как насчет документации? =
 
Она в процессе написания. Любые вопросы вы можете задать в форме обратной связи в "Центре поддержки" или по почте office@wp-universam.ru
 
== Screenshots ==
 
1. Список товаров для менеджера
2. Создание рассылки
3. Создание правил скидок в корзине
4. Вид заказа - отгрузка
5. Вид заказа - купленные товары
6. screenshot-6.(png|jpg|jpeg|gif)
7. screenshot-7.(png|jpg|jpeg|gif)
8. screenshot-8.(png|jpg|jpeg|gif)
9. screenshot-9.(png|jpg|jpeg|gif)
10. screenshot-10.(png|jpg|jpeg|gif)
11. screenshot-11.(png|jpg|jpeg|gif)
12. screenshot-12.(png|jpg|jpeg|gif)
13. screenshot-13.(png|jpg|jpeg|gif)
14. screenshot-14.(png|jpg|jpeg|gif)
15. screenshot-15.(png|jpg|jpeg|gif)
16. screenshot-16.(png|jpg|jpeg|gif)
17. Пример сайта с UNIVERSAM(http://computer.wp-universam.ru)
18. Пример сайта с UNIVERSAM
19. Варианты триггерной рассылки
 
== Changelog ==
= 5.27 =
* Строгий счет в crm
= 5.26 =
* Новая электронная почта. Массовое обновление свойств товаров и управления ценами.
= 5.01 =
* Добавленны новые типы свойств товаров.
= 5.0 =
* Переработанное ядро, множество улучшений.
= 4.82 =
* Новые улучшения для отображения на мобильных устройствах.
= 4.81 =
* Улучшенные уведомления о событиях, такие как низкий запас остатков, отсутствие товара. Новые возможности для публикаций вКонтакте. Новые подключения к платежным шлюзам.
= 4.72 =
* Поисковая аналитика и оптимизация
= 4.70 =
* Атол-онлайн
* Смс-рассылки
= 4.60 =
* Статистика отправленных рассылок
= 4.5.03 =
* Добавлена валюта Гривна
= 4.5 =
* Измененная визуализация списка товаров
* CRM в заказах
= 4.42 =
* Улучшенная интеграция с шаблонами. Теперь магазин стал еще красивее.
= 4.41 =
* Отправка сообщений по событию( забытая корзина, давно не покупал)
= 4.4 =
* Теперь бесплатная версия.
= 4.3 =
* Добавлен мастер установки.
* Добавлена бесплатная тема.
* Зоны продаж.

= 4.2 =
* Добавлены правила наценок.
* Кассы онлайн(Штрих-М).

= 4.1 =
* Добавлены правила скидок на товары.

= 4.0 =
* Новая версия рассылки.
* Документы оплаты.
* Возврат заказов.
* Редактируемые бланки.

= 3.9 =
* Загрузка и управление почтой на сайт.

== Upgrade Notice ==
= 4.4 =
Добавлено много мелких улучшений. 

= 4.3 =
Добавлен мастер установки. Добавлена бесплатная тема. Добавлены зоны продаж. Добавлено много мелких улучшений. 

= 4.2 =
Новая версия стала более функциональной. Она позволит более гибко  управлять ценами

== Arbitrary section ==
 Вы можете также посмотреть работу комплекса интернет-магазина на тестовом сайте или заказать разработку собственного интернет-магазина. 