<?php
/*
Printing Forms:Коммерческое предложение
type:crm
Description: Вы можете использовать %recipient_name% %recipient_inn% %recipient_ppc% %recipient_legalpostcode% %recipient_legalcity% %recipient_legaladdress% %recipient_bank_number% %recipient_bank_name%  %recipient_bank_bic% %recipient_bank_address% %recipient_bank_bank_ca%
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
$columns = array(
	'image'     => '%%edit_colum image ""%%',
    'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
	'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',	
	'barcode'  => '%%edit_colum barcode "'.__( 'Штрих-код','usam' ).'"%%',	 	
	'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',		
	'discount_price' => '%%edit_colum discount_price "'.__( 'Цена со скидкой','usam' ).'"%%',			
	'quantity' => '%%edit_colum quantity "'.__( 'Кол-во','usam' ).'"%%',	
	'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
);
if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">		
		@page {	margin: 0; }
		included unicode fonts:*  serif: 'dejavu serif'*  sans: 'devavu sans'
		h1, p, td, th, title, div, *{font-family: 'dejavu serif';}					
		td, th {font-size: 10px; white-space: normal}
		p {margin:2px; white-space: normal}
		#products-table{padding-bottom:1px;}
		#products-table th{border:0.1pt solid #606060; background-color: rgb(226, 226, 226); text-align: center; padding:5px}
		#products-table tbody td{text-align:right; border:0.1pt solid #606060; padding:3px 2px}
		
		#products-table tbody .column-image{text-align:center;}
		#products-table tbody .column-name{text-align:left;}
		#products-table tbody .column-sku{text-align:left;}
		#products-table tbody .column-barcode{text-align:center;}
		.totalprice{font-size:16px; font-weight: 700;}
			
		#invoice-content{vertical-align:top;}
		#invoice-footer{color:#444;}		
	</style>
	<title><?php echo esc_html__( 'Комерческое предложение', 'usam' ); ?>: %id% <?php esc_html_e( 'от', 'usam' ); ?> %date%</title>	
</head>
<body <?php echo $print; ?>>
	<div style="margin: 15pt; width: 565pt; background: #ffffff">
		<table id="logo-table" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100px">
					<p>%shop_logo%</p>
				</td>
				<td>					
					<h2>%%name_company input "<?php echo __( 'Интернет-магазин', 'usam' )." - ".get_bloginfo('name'); ?>"%%</h2>				
				</td>
			</tr>
		</table>
		<table id="content-table" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<h2 style="text-align: left;"><?php _e( 'Поставщик', 'usam' ); ?></h2>				
					<p style="text-align: left;">%%company_details input "<?php echo '%recipient_name% %recipient_inn% %recipient_ppc% %recipient_legalpostcode% %recipient_legalcity% %recipient_legaladdress%'; ?>" "<?php echo __( 'Вы можете использовать', 'usam' ).': %recipient_name% %recipient_inn% %recipient_ppc% %recipient_legalpostcode% %recipient_legalcity% %recipient_legaladdress%'; ?>"%%</p>	
					<p style="text-align: left;">%%company_bank_details input "<?php echo __('р/с','usam').' %recipient_bank_number% %recipient_bank_name% '.__( 'БИК', 'usam' ).' %recipient_bank_bic% %recipient_bank_address% '.__('кор/с','usam').' %recipient_bank_bank_ca%'; ?>" "<?php echo __( 'Вы можете использовать', 'usam' ).': %recipient_bank_number% %recipient_bank_name% %recipient_bank_bic% %recipient_bank_address% %recipient_bank_bank_ca%'; ?>"%%</p>				
				</td>			
			</tr>
			<tr>
				<td>
					<h2 style="text-align: left;"><?php _e( 'Контрагент', 'usam' ); ?></h2>				
					<p style="text-align: left;">%counterparty%</p>
				</td>
			</tr>
				[if description!= {		
			<tr>
				<td>
					<h2 style="text-align: left;"><?php _e( 'Описание', 'usam' ); ?></h2>				
					<p style="text-align: left;">%description%</p>
				</td>
			</tr>}]	
				[if conditions!= {		
			<tr>
				<td>
					<h2 style="text-align: left;"><?php _e( 'Условия', 'usam' ); ?></h2>				
					<p style="text-align: left;">%conditions%</p>
				</td>
			</tr>}]	
			<tr>
				<td>						
					<h1 style="text-align: center;"><?php esc_html_e( 'Коммерческое предложение', 'usam' ); ?>: %id% <?php esc_html_e( 'от', 'usam' ); ?> %date%</h1>			
					<p style="text-align: center;">%closedate%</p>	
				</td>
			</tr>
		</table>	
		<?php $this->display_table( $columns );	?>
		<div class ="invoice-footer" style="margin-top:20px">%%footer textarea "Главный бухгалтер ______________________" "Подписи"%%</div>
	</div>
</body>
</html>		