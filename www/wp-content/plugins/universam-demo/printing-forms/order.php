<?php
/*
Printing Forms: Заказ
type:order
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php printf( esc_html__( 'Заказ #%s', 'usam' ), $this->id ); ?></title>
	<style type="text/css">
		@page {	margin: 0; }
		@media print  {
			header { background: none; color: #000; }
			header img { -webkit-filter: invert(100%);	filter: invert(100%); }
		}			
		body {font-family:"dejavu serif", Helvetica, Arial, Verdana, sans-serif;}
		h1 {font-size:2.3em;}
		h1 span {font-size:0.8em;}
		h2 {font-size:1.0em; color: #333; margin: 0 0 0.4em 0;}
		h3 {margin: 0 0 0.4em 0;}
		h4 {margin: 0 0 0.4em 0;}
		#logo-table td{border:none}
		.usam_details_box {width:100%;border:none}	
		.usam_details_box td{border:none}	
		.usam_details_box h2{ font-size:1.5em; border-bottom: 2px solid maroon; font-weight: normal; padding-bottom: 5px;}	
		.usam_details_box .order_props_group table td {text-align: left;}
		.usam_details_box .order_props_group table td, 
		.usam_details_box .order_props_group table{ border:0px solid #ccc; }			
		#products-table{font-size:0.75em;}
		table#order,table#products-table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	}	
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px; font-size:10px;}
		table#order td,table#products-table td {text-align:center;}
		#products-table td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc;}		
		th.column-total { width:90px;}
		th.column-shipping { width:120px;}
		th.column-price { width:100px;}
		th.column-barcode { width:140px;}
		.column-barcode table{border: none; padding: 0; margin: 0}
		.column-barcode table td{border: none; padding: 0;}	
	
		#shipping_notes{margin: 5px 0;width:100%;display:inline-block;}
		#shipping_notes #notes{border:1px solid #000; padding: 5px;}		
	</style>
</head>
<?php
if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<body <?php echo $print; ?>>
	<div style="margin: 0pt; padding: 12pt 12pt 12pt 12pt; width: 571pt; background: #ffffff">
		<table id="logo-table" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100px"><p>%shop_logo%</p></td>
				<td><h1><?php echo get_bloginfo('name'); ?> <br /><span><?php printf( esc_html__( 'Заказ #%s', 'usam' ), $this->id ); ?></span></h1></td>
			</tr>
		</table>		
		<?php
		$this->display_customer_details(); 
		if ( $this->order_data['notes']	!= '' ) 
		{
			?>
			<div id="shipping_notes">	
				<h3><?php _e( 'Заметка', 'usam' ); ?></h3>
				<div id="notes">			
					<?php echo $this->order_data['notes']; ?>
				</div>
			</div>
			<?php 
		}
		$this->display_order_details(); 
		
		$columns = array(
			 'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
			 'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',		
			 'quantity' => '%%edit_colum quantity "'.__( 'Количество','usam' ).'"%%',
			 'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',	
			 'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
		);
		$this->display_table( $columns );
		?>	
	</div>
</body>
</html>