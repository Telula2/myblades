<?php
/*
Printing Forms: Акт приемки-передачи товара
type:shipped_document
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
$columns = array(
	 'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
	 'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',	
	 'barcode'  => '%%edit_colum barcode "'.__( 'Штрих-код','usam' ).'"%%',	 	 
	 'quantity' => '%%edit_colum quantity "'.__( 'Кол-во','usam' ).'"%%',
	 'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',	
	 'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
	);	

if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php printf( esc_html__( 'Акт приемки-передачи товара № %s', 'usam' ), $this->id ); ?></title>
	<style type="text/css">
		@page {	margin: 0; }
		included unicode fonts:*  serif: 'dejavu serif'*  sans: 'devavu sans'
		h1, p, td, th, title, div, *{font-family: 'dejavu serif';}	
		h1 {font-size:1.3em;}
		h1 span {font-size:0.75em;}
		h2 {color: #333;}
		#wrapper {margin:0 auto; width:95%;}
		#header {	}
		#usam_requisite_company{font-size:10px;}
		#customer {	overflow:hidden;}
		#customer .shipping, #customer .billing {float: left; width: 50%;}
		#order{font-size:10px;}
		#products-table{font-size:10px;}
		table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	table-layout: fixed;}		
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px; overflow: hidden; white-space: nowrap}
		td {text-align:center;}
		#products-table td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc; }	
		#products-table tbody .column-barcode{text-align:center;}
	
		.footer_purchase_receipt{margin-top: 15px}	
		.footer_purchase_receipt .terms_and_conditions{font-size: 9px; margin-bottom:15px}
		.footer_purchase_receipt .consent_date{ border:1px solid #ccc; padding: 3px 10px; margin 10px 0; font-weight:bold; text-align:center;}
		.footer_purchase_receipt .consent{font-size:11px;}
		.footer_purchase_receipt .date{font-size:11px;}
		.footer_purchase_receipt .customer_signature{margin-top: 10px; border: none;}
		.footer_purchase_receipt .customer_signature td{border: none; width: 25% font-size:10px;}
		.footer_purchase_receipt .customer_signature td.name{text-align:center;}
		.line{border-bottom:1px solid #000; margin: 0 10%; height:30px;}		
	</style>
</head>
<body <?php echo $print; ?>>
	<div style="margin: 0pt; padding: 12pt 12pt 12pt 12pt; width: 571pt; background: #ffffff">
		<div id="header">
			<h1>				
				<?php echo __( 'интернет-магазин', 'usam' ).' '.get_bloginfo('name'); ?> <br />
				<span><?php printf( esc_html__( 'Акт приемки-передачи товара № %s', 'usam' ), $this->id ); ?></span>
			</h1>
		</div>			
		<div id="usam_requisite_company"><?php _e( "Продавец:", 'usam' ); ?> %company_details% <?php _e( "ИНН:", 'usam' ); ?> %recipient_inn% <?php _e( "КПП:", 'usam' ); ?> %recipient_ppc%, <?php _e( 'Адрес', 'usam' ); ?>: %recipient_legaloffice%, %recipient_contactcity% %recipient_contactaddress%</div>
		<table id="order">
			<thead>
				<tr>
					<th><?php echo esc_html_e( 'Дата заказа', 'usam' ); ?></th>					
					<th><?php echo esc_html_e( 'Способ доставки', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Способ оплаты', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплата', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплачено', 'usam' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>%order_date%</td>					
					<td>%shipping_method_name%</td>
					<td>%gateway_name%</td>
					<td>%payment_status%</td>
					<td>%total_paid_currency%</td>					
				</tr>
			</tbody>
		</table>	
		<?php $this->display_table( $columns ); ?>
		<div class = "footer_purchase_receipt">		
			<h4><?php esc_html_e( 'Условия', 'usam' ); ?></h4>
			<p class = "terms_and_conditions">%%conditions_act_transferring textarea "Условия"%%</p>
			<div class = "consent_date">					
				<p class = "consent"><?php esc_html_e( 'Подтверждаю, что с условиями акта в том числе с правилами возврата товара, предъявлении претензий к продавцу в отношении товара, ОЗНАКОМЛЕН И СОГЛАСЕН', 'usam' ); ?></p>
				<p class = "date"><?php esc_html_e( 'Дата подписания акта', 'usam' ); ?>: &laquo;_________&raquo;__________________________</p>
			</div>
			<table class = "customer_signature">
				<tr>
					<td class = "name" colspan='2'><strong><?php esc_html_e( 'Выдал', 'usam' ); ?></strong></td>
					<td class = "name" colspan='2'><strong><?php esc_html_e( 'Покупатель', 'usam' ); ?></strong></td>
				</tr>				
				<tr>
					<td style ="width:35%;"><p class = "line">%%courier input "Фамилия продавца"%%</p></td>
					<td><p class = "line"></p></td>
					<td style ="width:35%;"><p class = "line"></p></td>
					<td><p class = "line"></p></td>
				</tr>
				<tr>
					<td><?php esc_html_e( 'ФИО', 'usam' ); ?></td>
					<td><?php esc_html_e( 'Подпись', 'usam' ); ?></td>
					<td><?php esc_html_e( 'ФИО', 'usam' ); ?></td>
					<td><?php esc_html_e( 'Подпись', 'usam' ); ?></td>
				</tr>				
			<table>		
		</div>
	</div>
</body>
</html>