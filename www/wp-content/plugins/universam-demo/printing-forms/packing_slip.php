<?php
/*
Printing Forms: Упаковочный лист
type:shipped_document
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
<?php	
$columns = array(
	 'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
	 'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',	
	 'barcode'  => '%%edit_colum barcode "'.__( 'Штрих-код','usam' ).'"%%',	 	 
	 'quantity' => '%%edit_colum quantity "'.__( 'Количество','usam' ).'"%%',
	 'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',	
	 'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
	);	

if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php printf( esc_html__( 'Упаковочный лист #%s', 'usam' ), $this->id ); ?></title>
	<style type="text/css">
		@page {	margin: 0; }
		@media print  {
			header { background: none; color: #000; }
			header img { -webkit-filter: invert(100%);	filter: invert(100%); }
		}				
		body {font-family:"dejavu serif", Helvetica, Arial, Verdana, sans-serif;}
		h1 {font-size:1.3em;}
		h1 span {font-size:0.8em;}
		h2 {font-size:1.0em; color: #333; margin: 0 0 0.4em 0;}
		h3 {margin: 0 0 0.4em 0;}
		h4 {margin: 0 0 0.4em 0;}
		.usam_details_box {width:100%;border:none}	
		.usam_details_box td{border:none}	
		.usam_details_box h2{ font-size:1.5em; border-bottom: 2px solid maroon; font-weight: normal; padding-bottom: 5px;}	
		.usam_details_box .order_props_group table td {text-align: left;}
		.usam_details_box .order_props_group table td, 
		.usam_details_box .order_props_group table{ border:0px solid #ccc; }	
		#products-table{font-size:0.75em;}
		
		table#order, 
		table#products-table { border:1px solid #000; border-collapse:collapse; margin-top:1em; width:100%;}		
		#products-table td.amount {	text-align:right; }
			
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px; white-space: normal; font-size:10px;}
		table#order td,table#products-table td {text-align:center;}
	
		td, tbody th { border-top:1px solid #ccc; }	
	
		#shipping_notes{margin: 8px 0;}
		#shipping_notes #notes{border:1px solid #000; padding: 5px;}		
	</style>
</head>
<body <?php echo $print; ?>>
	<div style="margin: 0pt; padding: 12pt 12pt 12pt 12pt; width: 571pt; background: #ffffff">
		<div id="header">
			<h1>
				<?php echo get_bloginfo('name'); ?> <br />
				<span><?php printf( esc_html__( 'Упаковочный лист #%s для заказа #', 'usam' ), $this->id ); ?>%order_id%</span>
			</h1>
		</div>
		<?php $this->display_customer_details(); ?>
		<table id="order">
			<thead>
				<tr>
					<th><?php echo esc_html_e( 'Заказ', 'usam' ); ?></th>		
					<th><?php echo esc_html_e( 'Дата заказа', 'usam' ); ?></th>				
					<th><?php echo esc_html_e( 'Способ доставки', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Способ оплаты', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплата', 'usam' ); ?></th>
					<th><?php echo esc_html_e( 'Оплачено', 'usam' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>%order_id%</td>	
					<td>%order_date%</td>					
					<td>%shipping_method_name%</td>
					<td>%gateway_name%</td>
					<td>%payment_status%</td>
					<td>%total_paid_currency%</td>					
				</tr>
			</tbody>
		</table>		
		[if shipped_notes != '' {<tr class='total_tax'>
			<div id="shipping_notes">	
				<h3><?php _e( 'Заметка для курьера', 'usam' ); ?></h3>
				<div id="notes">%shipped_notes%</div>
			</div>}]	
		<?php $this->display_table( $columns ); ?>	
	</div>
</body>
</html>