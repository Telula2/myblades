<?php
/*
Printing Forms: Складской лист
type:order
orientation:landscape
*/
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php printf( esc_html__( 'Складской лист для заказа #%s', 'usam' ), $this->id ); ?></title>	
	<style type="text/css">
		@page {size:landscape} 
		@page {	margin: 0; }
		body {font-family:"dejavu serif", Helvetica, Arial, Verdana, sans-serif;}
		h1 {font-size:1.3em;}
		h1 span {font-size:0.8em;}
		h2 {font-size:1.0em; color: #333; margin: 0 0 0.4em 0;}
		h3 {margin: 0 0 0.4em 0;}
		h4 {margin: 0 0 0.4em 0;}		
		#header {	}		
		#products-table{font-size:0.75em;}
		table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	table-layout: fixed;}
		table#products-table thead th, table#products-table thead td{ writing-mode: tb-rl; }
		th {background-color:#efefef; text-align:center;}
		th, td {padding:5px 2px; overflow: hidden; white-space: pre-line; font-size:10px;}
		td {text-align:center;}
		#products-table td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc; }	
		.column-n{ width:50px;}		
		.column-sku {width:120px;}
		.column-barcode {width:120px; text-overflow: ellipsis; overflow: hidden;}
		.column-barcode table{width:100%;}
		.column-barcode table{border: none; padding: 0; margin: 0}
		.column-barcode table td{border: none; padding: 0; }
		.column-storage{width:30px;}
	</style>
</head>
<?php
if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<body <?php echo $print; ?>>
	<div style="margin: 0pt; padding: 12pt 12pt 12pt 12pt; width: 871pt; background: #ffffff">
		<div id="header">
			<h1>
				<?php echo get_bloginfo('name'); ?> <br />
				<span><?php printf( esc_html__( 'Складской лист для заказа #%s', 'usam' ), $this->id ); ?></span>
			</h1>
		</div>	
		<?php
		$columns = array(
			'n'        => __( 'Номер','usam' ),
			'title'    => __( 'Название товара','usam' ),
			'sku'      => __( 'Артикул','usam' ),
			'barcode'  => __( 'Штрих-код','usam' ),
			'quantity' => __( 'Кол-во','usam' ),	
		);
		$storages = usam_get_stores();
		foreach ( $storages as $storage )
		{
			$columns[$storage->meta_key] = $storage->title;		
		}					
		register_column_headers( 'order_item_details', $columns );
		?>
		<table id="products-table" cellspacing="0">
			<thead>
				<tr>
					<?php print_column_headers( 'order_item_details' ); ?>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i = 0;
				foreach ( $this->cartcontent as $product ) 
				{
					$i++;
					?>
						<tr data-item-id="<?php echo $product->id; ?>" >
							<td class="column-n"><?php echo $i;	?></td>
							<td><?php echo $product->name;	?></td>
							<td><?php echo usam_get_product_meta( $product->product_id, 'sku', true ); ?></td>
							<td class='column-barcode'><?php echo usam_get_product_barcode( $product->product_id ); ?></td>
							<td><?php echo $product->quantity; ?></td>
							<?php
							foreach ( $storages as $storage )
							{
								$stock = (int)usam_get_product_meta( $product->product_id, $storage->meta_key );
								?><td class='column-storage'><?php echo $stock; ?></td><?php		
							}	
							?>							
					  </tr>
					  <?php
				}
				?>							
			</tbody>
		</table>
	</div>
</body>
</html>