<?php
/*
Printing Forms: Счет на оплату
type:payment
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php	
$columns = array(	 
	 'number'   => '%%edit_colum name "№"%%',
	 'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
	 'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',	
	 'barcode'  => '%%edit_colum barcode "'.__( 'Штрих-код','usam' ).'"%%',	 	 
	 'quantity' => '%%edit_colum quantity "'.__( 'Количество','usam' ).'"%%',
	 'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',	
	 'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
	);

if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo esc_html__( 'Счет', 'usam' ); ?>: %order_id% <?php esc_html_e( 'от', 'usam' ); ?> %order_date%</title>
	<style type="text/css">
		body {margin: 30px;font-family:"dejavu serif", Helvetica, Arial, Verdana, sans-serif;}
		included unicode fonts:*  serif: 'dejavu serif'*  sans: 'devavu sans'
		h1, h2, p, td, th, *{margin:2px; font-family: 'dejavu serif';}		
		td, th {font-size: 14px; white-space: nowrap}
		p {margin:2px;  }
		#products-table{width:100%; border-collapse:collapse;}
		#products-table th{border:0.2pt solid #606060; background-color: rgb(226, 226, 226); text-align: center; padding:5px}
		#products-table td{border:0.1pt solid #606060; padding:3px 3px}
		#products-table td.item_number{width:100px}
		#products-table .product_name{text-align:left;}
		#content-table{width:100%; margin-top:0;}
		#invoice-content{vertical-align:top;}
		#invoice-footer{color:#444; padding-top:10px;}		
		#invoice-footer{color:#444; padding-top:10px;}		
		
		table { border-collapse: collapse; }
		table.acc td { border: 1pt solid #000000; padding: 0pt 3pt; line-height: 21pt; }
		table.it td { border: 1pt solid #000000; padding: 0pt 3pt; }
		table.sign td { font-weight: bold; vertical-align: bottom; }
		table.header td { padding: 0pt; vertical-align: top; }
	</style>
</head>
<body <?php echo $print; ?> style="margin: 0pt; padding: 0pt; background: #ffffff">
	<div style="margin: 0pt; padding: 15pt; width: 565pt; background: #ffffff">
		<table id="logo-table" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100px">
					<p>%shop_logo%</p>
				</td>
				<td>					
					<h2><?php echo __( 'Интернет-магазин', 'usam' )." - ".get_bloginfo('name'); ?></h2>				
				</td>
			</tr>
		</table>
		<table class="acc" width="100%">
			<colgroup>
				<col width="29%">
				<col width="29%">
				<col width="10%">
				<col width="32%">
			</colgroup>
			<tr>
				<td>ИНН %recipient_inn%</td>
				<td>КПП %recipient_ppc%</td>
				<td rowspan="2">
					<br>
					<br>
					Сч. №		</td>
				<td rowspan="2">
					<br>
					<br>%recipient_bank_number%</td>
			</tr>
			<tr>
				<td colspan="2">
					Получатель<br>%recipient_full_company_name%</td>
			</tr>
			<tr>
				<td colspan="2">
					Банк получателя<br>%recipient_bank_name%
				</td>
				<td>
					БИК<br>
					Сч. №<br>
				</td>
				<td>%recipient_bank_bic%
					<br>%recipient_bank_bank_ca%
				</td>
			</tr>
		</table>
		<br>
		<br>

		<h1 style="font-size: 1.5em; font-weight: bold; text-align: center"><nobr>СЧЕТ № %document_number% от %date%</nobr></h1>
		
		<h2 style="text-align: left; margin: 0.4em 0;"><?php _e( 'Поставщик', 'usam' ); ?></h2>				
		
		<p style="width: 565pt;text-align: left;white-space:normal;">%recipient_full_company_name% ИНН %recipient_inn% КПП %recipient_ppc%, <?php _e( 'Адрес', 'usam' ); ?>: %recipient_legaloffice%, %recipient_contactcity% %recipient_contactaddress%</p>
		<h2 style="text-align: left; margin: 0.4em 0;"><?php _e( 'Заказчик', 'usam' ); ?></h2>		
		<p style="text-align: left;white-space:normal;">%counterparty%</p>	
		<br>
		<?php $this->display_table( $columns );	?>
		<br>
		Всего наименований %number_products%, на сумму %total_price_currency%<br>
		<b>%total_price_word%</b>
		<br>
		<br>
		<div class ="invoice-footer" style="margin-top:20px">%%conditions textarea "" "Условия"%%</div>
		<br>
		<br>
		<div style="position: relative">
			<table class="sign">
						<tr>
					<td style="width: 150pt; ">Генеральный директор</td>
					<td style="width: 160pt; border: 1pt solid #000000; border-width: 0pt 0pt 1pt 0pt; text-align: center; ">
																	</td>
					<td>
									</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
								<tr>
					<td style="width: 150pt; ">Главный бухгалтер</td>
					<td style="width: 160pt; border: 1pt solid #000000; border-width: 0pt 0pt 1pt 0pt; text-align: center; ">
																	</td>
					<td>
									</td>
				</tr>
					</table>
		</div>
	</div>	
</body>
</html>		