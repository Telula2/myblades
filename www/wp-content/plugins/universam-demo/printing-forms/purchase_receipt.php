<?php
/*
Printing Forms: Товарный чек
type:order
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php printf( esc_html__( 'Товарный чек № %s', 'usam' ), $this->id ); ?></title>
	<style type="text/css">
		@page {	margin: 0; }
		body {font-family:"dejavu serif", Helvetica, Arial, Verdana, sans-serif;}
		h1 {font-size:1.3em;}
		h1 span {font-size:0.75em;}		
		h2 {color: #333;}	
		#header {	}
		#customer {	overflow:hidden;}
		#customer .shipping, #customer .billing {float: left; width: 50%;}
		#products-table{font-size:0.75em;}
		table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	table-layout: fixed;}		
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px; overflow: hidden;}
		td {text-align:center;}
		#products-table td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc; }
		.column-barcode {width:120px; text-overflow: ellipsis; overflow: hidden;}
		th.column-total { width:90px;}
		th.column-shipping { width:120px;}
		th.column-price { width:100px;}
		.column-barcode table{border: none; padding: 0; margin: 0}
		.column-barcode table td{border: none; padding: 0;}
		.footer_purchase_receipt{margin-top: 15px}	
		.footer_purchase_receipt .terms_and_conditions{font-family: Verdana, sans-serif; font-size: 9px; margin-bottom:15px}
		.footer_purchase_receipt .consent_date{ border:1px solid #ccc; padding: 3px 10px; margin 10px 0; font-weight:bold; text-align:center;}
		.footer_purchase_receipt .consent{font-family: Verdana, sans-serif; font-size:11px;}
		.footer_purchase_receipt .date{font-family: Verdana, sans-serif; font-size:11px;}
		.footer_purchase_receipt .customer_signature{margin-top: 10px; border: none;}
		.footer_purchase_receipt .customer_signature td{border: none; width: 25% font-size:10px;}
		.footer_purchase_receipt .customer_signature td.name{text-align:center;}
		.line{border-bottom:1px solid #000; margin: 0 10%; height:30px;}		
	</style>
</head>
<?php
if ( $this->edit )
{
	$print = '';
}
else
	$print = 'onload="window.print()"';
?>	
<body <?php echo $print; ?>>
	<div style="margin: 0pt; padding: 12pt 12pt 12pt 12pt; width: 571pt; background: #ffffff">
		<div id="header">
			<h1>
				<?php echo __( 'интернет-магазин', 'usam' ).' '.get_bloginfo('name'); ?> <br />
				<span><?php printf( esc_html__( 'Товарный чек № %s от %s', 'usam' ), $this->id, date_i18n( 'd.m.Y' ) ); ?></span>
			</h1>
		</div>
		<?php echo $this->display_requisites_shop( ); ?>	
		<?php
		$columns = array(
			 'name'     => '%%edit_colum name "'.__( 'Название товара','usam' ).'"%%',
			 'sku'      => '%%edit_colum sku "'.__( 'Артикул','usam' ).'"%%',		
			 'quantity' => '%%edit_colum quantity "'.__( 'Количество','usam' ).'"%%',
			 'price'    => '%%edit_colum price "'.__( 'Цена','usam' ).'"%%',	
			 'total'    => '%%edit_colum total "'.__( 'Всего','usam' ).'"%%',
		);
		$this->display_table( $columns );
		?>				
		<div class = "footer_purchase_receipt">			
			<table class = "customer_signature">
				<tr>
					<td class = "name"><strong><?php esc_html_e( 'Продавец', 'usam' ); ?></strong></td>
					<td><p class = "line"></p></td>
					<td><p class = "line"></p></td>
					<td><p class = "line"></p></td>
				</tr>
				<tr>
					<td></td>
					<td><?php esc_html_e( 'Должность', 'usam' ); ?></td>
					<td><?php esc_html_e( 'ФИО', 'usam' ); ?></td>					
					<td><?php esc_html_e( 'Подпись', 'usam' ); ?></td>
				</tr>
				<tr>					
					<td></td>
					<td></td>					
					<td></td>
					<td><?php esc_html_e( 'М.П.', 'usam' ); ?></td>
				</tr>
				<tr>					
					<td><strong><?php esc_html_e( 'Покупатель', 'usam' ); ?></strong></td>
					<td></td>
					<td><p class = "line"></p></td>
					<td><p class = "line"></p></td>
				</tr>	
				<tr>					
					<td colspan='2'></td>
					<td><?php esc_html_e( 'ФИО', 'usam' ); ?></td>
					<td><?php esc_html_e( 'Подпись', 'usam' ); ?></td>
				</tr>				
			<table>		
		</div>
	</div>
</body>
</html>