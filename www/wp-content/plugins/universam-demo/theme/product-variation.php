<?php 
// Описание: вид выбора вариантов товара
?>
<div class="usam_variation_forms">
<?php 
if ( usam_have_variation_groups() ) 
{ ?>
	<fieldset><legend><?php _e('Варианты товара', 'usam'); ?></legend>							
		<table>
			<?php while (usam_have_variation_groups()) : usam_the_variation_group(); ?>
				<tr>
					<td class="col1"><label for="<?php echo usam_vargrp_form_id(); ?>"><?php echo usam_the_vargrp_name(); ?>:</label></td>
					<td class="col2">
						<select class="usam_select_variation" name="variation[<?php echo usam_vargrp_id(); ?>]" id="<?php echo usam_vargrp_form_id(); ?>">
							<?php while (usam_have_variations()) : usam_the_variation(); ?>
								<option value="<?php echo usam_the_variation_id(); ?>" <?php echo usam_the_variation_out_of_stock(); ?>><?php echo usam_the_variation_name(); ?></option>
							<?php endwhile; ?>
						</select>
					</td>
				</tr> 
			<?php endwhile; ?>
		</table>							
	</fieldset>
<?php } ?>
</div>