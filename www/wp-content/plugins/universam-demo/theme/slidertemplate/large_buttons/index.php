<div id="featured">
	<div id="slides">
		<?php		
		foreach ($slides as $key => $slide)		
		{					
			if( wp_is_mobile() )
				$size = 'medium';
			else
				$size = 'full';
			
			$object_id = wp_get_attachment_image_src($slide['object_id'], $size);		
			?>	
			<div class="slide <?php if ($key == 0) { echo "active";} ?>" style="background-color: <?php echo $slide['fon']; ?>">
				<div class="container" style="background-image: url('<?php echo $object_id[0]; ?>'); background-color: <?php echo $slide['fon']; ?>;">
					<?php						
					if (!empty($slide['link']))
					{
						?><a class="slide_link" href="<?php echo $slide['link']; ?>"></a><?php
					}	
					?>
				</div> 				
			</div>
			<?php						
		} 
		?>	
	</div>
	<?php
	if ( $slider['setting']['button']['show'] )
	{
		?>
		<div id="controllers">
			<div class="container">
				<div id="switcher">
					<?php
					$i = 0;
					$width = 160;
					$height = 120;	
					if ( !empty($slider['setting']['button']['border_color']) )
					{
						$border = 'border-width:1px; border-style: solid; border-color: '.$slider['setting']['button']['border_color'].';';			
					}
					$border = '';
					foreach ($slider['slides'] as $key => $slide) 			
					{																
						$img_mini = wp_get_attachment_image_src($slide['object_id'], array($width, $height) );				
						?>					
							<div class="item <?php if ($i == 0) { echo "active";} ?> raise" style="background-color: <?php echo $slider['setting']['button']['fon']; ?>; <?php echo $border; ?>;">
								<a href="<?php echo $slide['link']; ?>">
									<h2 class="title"><?php echo $slide['title']; ?></h2>
									<p><?php echo $slide['description']; ?></p>
									<img src="<?php echo $img_mini[0]; ?>" alt="<?php echo $slide['title']; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>">
								</a>
							</div> 				
						<?php			
						$i++;												
					} 		
					?>	
				</div> 
			</div>
		</div> 	
		<?php
	}
	?>
</div> 	