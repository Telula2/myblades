<div id="featured">
	<div id="slides">
		<?php
		foreach ($slides as $key => $slide)		
		{					
			if( wp_is_mobile() )
				$size = 'medium';
			else
				$size = 'full';
			
			$object_id = wp_get_attachment_image_src( $slide['object_id'], $size );	
			?>	
			<div class="slide <?php if ($key == 0) { echo "active";} ?>" style="background-color: <?php echo $slide['fon']; ?>">							
				<div class="container" style="background-image: url('<?php echo $object_id[0]; ?>');">
					<?php									
					if (!empty($slide['link']))
					{
						?><a class="slide_link" href="<?php echo $slide['link']; ?>"></a><?php
					}	
					if (!empty($slide['description']))
					{
						?>
						<div class="description">
							<div class="description_content">								
								<h2 class="title"><a href="<?php echo $slide['link']; ?>"><?php echo $slide['title']; ?></a></h2>
								<p><?php echo $slide['description']; ?></p>
								<?php
									if (!empty($slide['link']))
									{
										?><a class="more" href="<?php echo $slide['link']; ?>"><span><?php _e( 'Подробнее', 'usam' ) ?></span></a><?php
									}
								?>
							</div>
						</div> 
						<?php
					}
					?>
				</div> 				
			</div>
			<?php						
		} 
		?>	
	</div>	
	<?php
	if ( $slider['setting']['button']['show'] )
	{	
		?>
		<div id="controllers">
			<div class="container">
				<div id="switcher">
					<?php
					$i = 0;										
					foreach ($slider['slides'] as $key => $slide) 			
					{	
						?>					
							<div class="item <?php if ($i == 0) { echo "active";} ?>">
								<a href=""></a>	
							</div> 				
						<?php			
						$i++;										
					} 		
					?>	
				</div> 
			</div>
		</div> 	
		<?php
	}
	?>
</div> 	