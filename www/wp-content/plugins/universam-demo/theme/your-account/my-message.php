<?php
// Описание: Сообщения

?>
<?php
$chat = new USAM_Chat_Template();
if ( isset($_GET['sel']) && is_numeric($_GET['sel']) )
{				
	usam_go_back_user_account_tab();
	$dialog = $_GET['sel'];
	$chat->display_chat_dialogs( $dialog );
}
else
{
	$chat->display_dialogs();					
}
?>
