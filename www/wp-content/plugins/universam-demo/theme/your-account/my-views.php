<?php
// Описание: Просмотренные товары

?>
<div class='history_views_product usam_user_products'>
<?php
$width = 160;
$height = 160;
$output = '';	
$args = array( 'user_list' => 'view', 'fields' => 'product_id' );
$product_ids = usam_get_user_products( $args );	
$products = usam_get_products( array( 'post_status' => 'publish', 'post__in' => $product_ids, 'orderby' => 'post__in' ));
foreach ( $products as $product )
{				
	$price = usam_get_product_price_currency( $product->ID );
	$output .= "<div id='product-$product->ID' class='usam_product'>";
	$output .= "<a href='" . usam_product_url( $product->ID ) . "' class='preview_link'  rel='" . str_replace( " ", "_", $product->post_title ) . "'>";			
	$output .= usam_get_product_thumbnail( $product->ID, 'product-image', $product->post_title );	
	$output .= "<div class = 'product_title'>".$product->post_title."</div>";			
	if ( $price )
	{
		if( usam_is_product_discount( $product->ID ) )										
			$output .= '<div class = "price price_sale">'. $price .'</div>';				
		else 									
			$output .='<div class = "price">' .$price . '</div>';				
	}
	$output .= "</a>";
	$output .= "</div>";		
}		
echo $output;
?>				
</div>	