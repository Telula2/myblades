<?php
// Описание: Корзина

?>
<?php if(usam_get_basket_number_items() > 0): ?>
	<div class="box_style_1 shopping-cart-wrapper">
		<table class = "your_cart tstyle_1">
			<thead>
				<tr class="cart-widget-header">
					<th id="product" colspan='2'><?php _e('Название', 'usam'); ?></th>
					<th id="quantity"><?php _e('Количество', 'usam'); ?></th>
					<th id="price"><?php _e('Сумма', 'usam'); ?></th>
					<th id="remove">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<?php while(usam_have_cart_items()): usam_the_cart_item(); ?>
				<tr>
						<td colspan='2' class='product-name'><?php do_action ( "usam_before_cart_widget_item_name" ); ?><a href="<?php echo usam_cart_item_url(); ?>"><?php echo usam_cart_item_name(); ?></a><?php do_action ( "usam_after_cart_widget_item_name" ); ?></td>
						<td><?php echo usam_cart_item_quantity(); ?></td>
						<td class="pricedisplay"><?php echo usam_cart_item_price(); ?></td>
						<td class="cart-widget-remove">
							<input type="hidden" name="usam_action" value="remove_cart_item" />
							<input type="hidden" name="key" value="<?php echo usam_the_cart_item_key(); ?>" />
							<input class="remove_button" type="submit" />
						</td>
				</tr>	
			<?php endwhile; ?>
			</tbody>
			<tfoot>
				<tr class="cart-widget-total">
					<td class="textdisplay">
						<?php _e('Всего', 'usam'); ?>:					
					</td>
					<td class="cart-widget-count" colspan='2'>
						<?php printf( _n('%d товар', '%d товаров', usam_get_basket_number_items(), 'usam'), usam_get_basket_number_items() ); ?>
					</td>				
					<td class="pricedisplay" colspan='1'>
						<?php echo usam_cart_total_widget(false, false, false, false, false); ?><br />					
					</td>
					<td></td>
				</tr>
				<tr>
					<td id='cart-widget-links' colspan="5">
						<a target="_parent" href="<?php echo usam_get_url_system_page('basket'); ?>" title="<?php _e('Оформить заказ', 'usam'); ?>" class="gocheckout button button_buy"><?php _e('Оформить заказ', 'usam'); ?></a>
						
						<a target="_parent" href="<?php echo htmlentities(add_query_arg('usam_action', 'empty_cart', remove_query_arg('ajax')), ENT_QUOTES, 'UTF-8'); ?>" class="emptycart button" title="<?php _e('Очистить корзину', 'usam'); ?>"><?php _e('Очистить корзину', 'usam'); ?></a>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
<?php else: ?>
	<p class="empty-basket">
		<?php _e('Ваша корзина пуста', 'usam');?><br /><br />
		<?php _e("Мы надеемся, что это не на долго", 'usam');?><br /><br />
		<a target="_parent" href="<?php echo usam_get_url_system_page('products-list'); ?>" class="button" title="<?php _e('Посетите магазин', 'usam'); ?>"><?php _e('Посетите магазин', 'usam'); ?></a>	
	</p>
<?php endif; ?>