<?php
// Описание: Прайс-лист

?>
<table class = "usam_user_profile_table price_list">
<?php
global $user_ID;
$user = get_userdata( $user_ID );			
$options = get_option('usam_price_list_setting', array() );
foreach($options as $id => $option)	
{							
	if ( $option['status'] == 1 )
	{
		$result = array_intersect($option['roles'], $user->roles);
		if ( !empty($result) )
		{
			?>
			<tr>
				<td><?php echo $option['title']; ?>:</td>
				<td class ="action"><a href="<?php echo home_url(); ?>?usam_action=download_price&file=<?php echo $id; ?>"><?php _e('Скачать','usam'); ?></a></td>
			</tr>
			<?php	
		}
	}
}
?>
</table>
