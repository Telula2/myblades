<?php
// Описание: Список сравнения

?>
<?php $bonus_account = usam_get_bonus_account( ); ?>
<div class = "box_style_1">
	<table class = "bonus_account_table tstyle_1">
		<thead>
			<tr>
				<th><?php _e('Дата', 'usam'); ?></th>
				<th><?php _e('Описание', 'usam'); ?></th>
				<th><?php _e('Статус', 'usam'); ?></th>
				<th><?php _e('Бонусы', 'usam'); ?></th>											
			</tr>
		</thead>
		<tbody>
		<?php 
		$bonus_total = 0;
		$status_bonus = usam_get_statuses_bonus( );
		foreach ( $bonus_account as $bonus )
		{ 
			?>
			<tr>
				<td><?php echo usam_local_date( $bonus['date_insert'] ); ?></td>
				<td><?php echo usam_get_bonus_type( $bonus['type'] ); ?></td>
				<td><?php echo $status_bonus[$bonus['status']]['title']; ?></td>
				<td><?php echo $bonus['bonus']; ?></td>
			</tr>	
		<?php
			$bonus_total += $bonus['bonus'];							
		} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='3'><?php _e('Всего накопленно бонусов','usam'); ?></td>
				<td><?php echo $bonus_total; ?></td>							
			</tr>
		</tfoot>
	</table>
	<p><?php _e('Накопленные бонусы можно использовать как деньги при оплате покупок на сайте.', 'usam'); ?></p>
</div>
