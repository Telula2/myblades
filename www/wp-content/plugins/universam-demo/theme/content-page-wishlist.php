<?php
// Описание: Страница "Избранные товары"
?>
<div class = "wish_list products_grid">
	<?php
	if(usam_product_count() == 0)
	{
		?><h4><?php  _e('Нет ни одного товара в избранном', 'usam'); ?></h4><?php 
	}	
	else
	{
		global $post;			
		while (usam_have_products()) :  
			usam_the_product(); 			
			$product_id = $post->ID;				
			?>			
			<div class="product_grid_item"> 
				<a class = "del" href="?usam_action=delete_product_from_user_list&list=desired&product_id=<?php echo $product_id; ?>" title="<?php _e('Убрать из списка', 'usam'); ?>">
					<img src="<?php echo USAM_CORE_IMAGES_URL; ?>/cross.png"/>	
				</a>			
				<a class = "product_link" href="<?php echo usam_product_url();  ?>" data-title="<?php _e('Нажмите для просмотра', 'usam'); ?>">						
					<div class="image_box">									
						<?php usam_product_thumbnail( ); ?>			
						<?php 							
						if( usam_product_has_stock( $product_id ) )
						{
							if( usam_is_product_discount() )
							{								
								?>								
								<div class="percent_action"><?php echo '-'. usam_you_save().'%'; ?></div>
								<div class="transparent_box"><?php _e('Акция', 'usam'); ?></div>
								<?php									
							}							
						}
						else 
						{
							?><div class="product_sold">Все запасы проданы</div><?php
						}
						?>
					</div>							
					<div  class="ptitle">								
						<div class="title"><?php the_title(); ?></div>
						<div class="old_price"><?php usam_product_price_currency( true ); ?></div>
						<div class="price"><?php usam_product_price_currency( ); ?></div>						
					</div>	
				</a>
				<?php usam_add_to_cart_button( $product_id ); ?>
			</div>			
		<?php
		endwhile; 	
	}
	?>				
</div>