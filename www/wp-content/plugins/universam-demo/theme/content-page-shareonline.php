<?php
// Описание: Акции на сайте
?>
<div class="usam_stocks_in_store">
<?php 
$terms = usam_get_category_sale(); 
$i = 1;
foreach($terms as $term) 
{
	$start_date = get_term_meta($term->term_id, 'usam_start_date_stock', true);	
	$end_date = get_term_meta($term->term_id, 'usam_end_date_stock', true);					
	
	if ( empty($start_date) )
		$start_date = date_i18n("d.m.Y");
	else
		$start_date = date_i18n("d.m.Y", strtotime($start_date) ); 
	if ( empty($end_date) )
		$time_stock = __('бессрочная', 'usam'); 
	else		
		$time_stock = $start_date.' - '.date_i18n("d.m.Y", strtotime($end_date) ); 
	if ( $i % 2 == 0 )
	{
		$i = 0;
		$style = "style='margin-right:0px'";
	}
	else
		$style = "style='margin-right:20px'";
	$term_link = get_term_link($term->term_id, 'usam-category_sale');

	?>
	<div class="usam_category_sale" <?php echo $style; ?>>		
		<div class="category_image">
			<a href='<?php echo $term_link ?>'><img src="<?php echo usam_taxonomy_image($term->term_id, array(150,150) ) ?>" width='150px' height='150px' title ="<?php echo $term->name ?>"/></a>
		</div>
		<div class="text">
			<div class="category_name">
				<a href='<?php echo $term_link ?>'><?php echo $term->name ?></a>		
			</div>
			<div class="time_stock">
				<div class="title"><?php _e('Срок акции','usam') ?></div>
				<div class="time"><?php echo $time_stock; ?></div>
			</div>
		</div>
	</div>
	<?php
	$i++;
}
?>
</div>