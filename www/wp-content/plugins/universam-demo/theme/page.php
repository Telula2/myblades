<?php
// Описание: Страница корзины, оформления заказа и т. д.

if ( ! defined( 'ABSPATH' ) ) {
	exit; 
} 
get_header( 'shop' );
?>
<div class="wrap">
	<?php 
	do_action( 'usam_before_main_content' );
	usam_get_content( ); 
	do_action( 'usam_after_main_content' );
	?>
</div>
<?php
get_footer( 'shop' );