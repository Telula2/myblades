<?php	
	$curr_cat       = get_term( $category_id, 'usam-category', ARRAY_A );
	$category_list  = get_terms( 'usam-category', 'hide_empty=0&parent=' . $category_id );
	$link           = get_term_link((int)$category_id , 'usam-category');	
	$show_name      = $instance['show_name'];
	$subcategory    = FALSE;
	
	if ( $grid ) : ?>
		<a href="<?php echo $link; ?>" style="padding: 4px 4px 0 0; width:<?php echo $width; ?>px; height:<?php echo $height; ?>px;" title="<?php echo $curr_cat['name']; ?>" class="usam_category_grid_item">
			<?php usam_parent_category_image( $category_id, $show_thumbnails, $width, $height, true ,$show_name); ?>
		</a>
		<?php start_category_query( array( 'parent_category_id' => $category_id, 'show_thumbnails' => $show_thumbnails, 'show_name' => $show_name) ); ?>
		<a href="<?php usam_print_category_url(); ?>" style="width:<?php echo $width; ?>px; height:<?php echo $height; ?>px" class="usam_category_grid_item" title="<?php usam_print_category_name(); ?>">
			<?php usam_print_category_image( $width, $height ); ?>
		</a>
		<?php usam_print_subcategory( '', '' ); ?>
		<?php usam_end_category_query(); ?>
<?php else : 		
		?>
		<div class="usam_categorisation_group" id="categorisation_group_<?php echo $category_id; ?>">
			<ul class="usam_categories usam_top_level_categories">
				<li class="usam_category_<?php echo $curr_cat['term_id']; usam_print_category_classes($curr_cat);  ?>">					
					<a href="<?php echo $link; ?>" class="usam_category_image_link"><?php usam_parent_category_image( $category_id, $show_thumbnails, $width, $height, false, $show_name ); ?></a>						
					<a href="<?php echo $link; ?>"><?php echo esc_html( $curr_cat['name'] ); ?></a>
					<ul class="usam_categories usam_second_level_categories">
						<?php start_category_query( array( 'parent_category_id' => $category_id, 'show_thumbnails' => $show_thumbnails , 'show_name' => $show_name) ); ?>
							<a href="<?php usam_print_category_url(); ?>" class="usam_category_link">
								<li class="usam_category_<?php usam_print_category_id(); usam_print_category_classes_section();?>">
									<?php 
									if($show_thumbnails) usam_print_category_image( $width, $height ); 
									usam_print_category_name();								
									if ( 1 == get_option( 'show_category_count') ) usam_print_category_products_count( "(",")" ); 
									?>
								</li>
							</a>
							<?php
							usam_print_subcategory( '', '' ); 
							if ( $subcategory ) 
								usam_print_subcategory( '<ul>', '</ul>' ); 							
							?>
						<?php usam_end_category_query(); ?>
					</ul>
				</li>
			</ul>

			<div class="clear_category_group"></div>
		</div>
<?php endif; ?>