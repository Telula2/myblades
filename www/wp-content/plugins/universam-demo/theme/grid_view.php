<?php
// Описание: Модуль для страницы "каталог" и категорий товаров
if ( usam_display_products() )
{	
	?>
	<div id="catalog_list">
		<div class="products-catalog_list products_grid">
		<?php		
		$new_prod_date = 14;	
		while (usam_have_products()) :  	
			usam_the_product(); 			
			$product_id = $post->ID;	
			$stock = (int)usam_get_product_meta($product_id,'stock');
			?>			
			<div class="product_grid_item"> 			 
				<a class = "product_link" href="<?php echo usam_product_url(); ?>">	
					<div class="image_box">									
						<?php 					
						usam_product_thumbnail( );
						if( $stock > 0 )
						{						
							if( usam_is_product_discount() )
							{								
								?>								
								<div class="percent_action"><?php echo '-'. usam_you_save().'%'; ?></div>
								<div class="transparent_box"><?php _e('Акция', 'usam'); ?></div>
								<?php									
							}						
							$t = mktime(0,0,0,date('m'),date('d'),date('Y'));								
							$t = $t-$new_prod_date*24*60*60;						
							if (strtotime($post->post_date)>=$t) {
								?><div class="new_box"><?php _e('Новинка', 'usam'); ?></div><?php 
							} 
						}
						else
						{
							$storage = (int)usam_get_product_meta($product_id,'storage');
							if ( $storage > 0 ) 
							{
								?><div class="unavailable"><?php _e('Под заказ', 'usam'); ?></div><?php						
							}
							else
							{
								?><div class="product_sold"><?php _e('Все запасы проданы', 'usam'); ?></div><?php	
							}
						}
						?>
					</div>							
					<div  class="ptitle">								
						<div class="title"><?php the_title(); ?></div>
						<div class="rating"><?php usam_product_rating( ); ?></div>				
						<div class="all_price">
							<span class="price"><?php usam_product_price_currency( ); ?></span>
							<span class="sale"><?php usam_product_price_currency( true ); ?></span>
						</div>						
					</div>	
				</a>
				<?php usam_edit_the_product_link( ); ?>			
			</div>			
		<?php endwhile; ?>	
		<?php if( ! usam_product_count() )
		{
			?><h4 class = "products_found"><?php _e('Нет ни одного товара в этой группе.', 'usam'); ?></h4><?php 
		}
		?>		
		</div>
		<div class = "navigation_box">
			<?php
			usam_product_info();	
			usam_pagination();
			?>
		</div>
		<?php usam_product_taxonomy_description();	?>
	</div>
<?php
}
?>