<?php $items_body = 6; ?>
<?php $count_terms = count($products); ?>
<div class = "cross_sell_list">
	<div class = "jcarousel-wrapper cross_sell_body">
		<h2><?php _e('К Вашим покупкам прекрасно подойдут:', 'usam'); ?></h2>
		<section class='carousel'>
		<?php if ( $count_terms > 6 ) { ?>
			<button class='up-carousel'><span></span></button>
		<?php } ?>
		<div id='jcarousel' class='jcarousel' data-visible-items='<?php echo $items_body; ?>' data-visible-total='<?php echo $items_body; ?>' scroll-items='<?php echo $items_body; ?>' scroll-auto='true' scroll-interval ="6000">	
			<ul id='usam_the_cart_item'>		
			<?php
			$i = 0;
			$action = htmlentities(usam_this_page_url(), ENT_QUOTES, 'UTF-8' );	
			foreach ( $products as $product )
			{
				?>				
				<li id = "product-<?php echo $product->ID; ?>">									
					<form class="product_form"  enctype="multipart/form-data" action="" method="post" name="product_<?php echo $product->ID ?>">			
						<div class="image_box">	
							<?php echo usam_get_product_thumbnail( $product->ID, 'product-image' ); ?>
						</div>
						<div class="p_price"><?php echo usam_get_product_price_currency( $product->ID ); ?></div>		
						<div class="p_title"><?php echo get_the_title($product->ID); ?></div>
						<?php echo usam_addtocart_button( $product->ID ); ?>	
					</form>
				</li>
				<?php
				$i++;
				if ( $i >= 30 )
					break;
			}
			?>	
			</ul></div>
			<?php if ( $count_terms > 6 ) { ?>
			<button class='down-carousel'><span></span></button>
			<?php } ?>
		</section>
	</div>
</div>