<?php
/*
Template Name: Каталог
*/
?>
<?php 
get_header();
global $wp_query;
if ( isset($wp_query->query_vars['usam-category_sale']) )
{
	$term = get_term_by('slug', $wp_query->query_vars['usam-category_sale'], 'usam-category_sale');	
	?>
	<div id='product_category_sale_description' class='product_category_sale_description'>		
		<div class='description'><?php echo nl2br($term->description); ?></div>
	</div>
	<?php	
}
if( !empty($wp_query->post))
{	
	?>
	<div class="usam_columns2">
		<div class="usam_product_display">
			<?php usam_load_template("content-page-products"); ?>
		</div>
		<?php get_sidebar('product');?>
	</div>
	<?php
}
get_footer();
?>