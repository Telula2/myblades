<?php
// Описание: Станица "Личный кабинет пользователя"
if ( ! defined( 'ABSPATH' ) ) {
	exit; 
}
global $your_account;
	
?>		
<div class="usam_user_profile">
<?php		
	$your_account->display_header_tab();
	$your_account->display_content_tab();	
?>		
</div>