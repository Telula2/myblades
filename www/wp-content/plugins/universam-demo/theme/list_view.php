<?php
// Описание: Модуль для страницы "каталог" и категорий товаров
if (usam_display_products())
{
	?>
	<div id="catalog_list">
		<div class="products-catalog_list">
		<?php
		$new_prod_date = 14;	
		while (usam_have_products()) :  
			usam_the_product(); 		
			$product_id = $post->ID;				
			$product_has_stock = usam_product_has_stock($product_id);	
			$stock = usam_get_product_meta($product_id , 'stock' );			
			?>			
			<div class="product_list_item"> 							
				<a href="<?php echo usam_product_url(); ?>">		
					<div class="image_box">
						<?php usam_product_thumbnail( );
						if( $product_has_stock )
						{
							if( usam_is_product_discount() )
							{ ?>			
								<div class="percent_action"><?php echo '-'. usam_you_save().'%'; ?></div>					
								<div class="transparent_box"><?php _e('Акция', 'usam'); ?></div>						
							<?php 
							}						
						}
						else
						{
							$storage = usam_get_product_meta($product_id,'storage', true);
							if ( $storage > 0 ) 
							{
								?><div class="unavailable"><?php _e('Под заказ', 'usam'); ?></div><?php						
							}
							else
							{
								?><div class="product_sold"><?php _e('Все запасы проданы', 'usam'); ?></div><?php	
							}
						}
						$t = mktime(0,0,0,date('m'),date('d'),date('Y'));								
						$t = $t-$new_prod_date*24*60*60;
						if (strtotime($post->post_date)>=$t) {?>
							<div class="new_box">	
								<div class="rotated_text">	
									<?php _e('Новинка', 'usam'); ?>
								</div>	
							</div>
						<?php } ?>
					</div>
				</a>
				<div  class="product_data">								
					<a href="<?php echo usam_product_url(); ?>"><p class="title"><?php the_title(); ?></p></a>
					<?php usam_edit_the_product_link( ); ?>
					<p class="old_price"><?php usam_product_price_currency( true ); ?></p>
					<div class = "product_content"><?php the_content(); ?></div>
					<p class="price"><?php usam_product_price_currency( ); ?></p>
					
					<?php $action = htmlentities(usam_this_page_url(), ENT_QUOTES, 'UTF-8' ); ?>								
					<form class="product_form"  enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="product_<?php echo $product_id ?>" id="product_<?php echo $product_id ?>" >	
					
						<?php usam_select_product_variation(); ?>							
						<div id="usam_quantity" class="usam_quantity">	
							<input type="button" value="-" class="minus b_quantity"/>
							<input type="text" class="quantity_update" id="usam_quantity_update_<?php echo $product_id; ?>" name="usam_update" size="2" value="1" />						
							<input type="hidden" name="usam_action" value="update_item_quantity" />						
							<input type="button" value="+" class="plus b_quantity" data-title = "Увеличить количество" data-stock = "<?php echo $stock; ?>" />
						</div>	
						<?php usam_addtocart_button( ); ?>
					</form>				
				</div>
			</div>			
		<?php 
		endwhile;	
		if( !usam_product_count() )
		{
			?><h4 class = "products_found"><?php  _e('Нет ни одного товара в этой группе.', 'usam'); ?></h4><?php 
		}
		?>		
		</div>
		<div class = "navigation_box">
			<?php
			usam_product_info();	
			usam_pagination();
			?>
		</div>
		<?php usam_product_taxonomy_description();	?>
	</div>
	<?php
}?>