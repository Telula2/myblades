<div class = 'usam_carousel usam_products'>
	<?php
	if ( !empty($title_products_for_buyers) )
	{ ?>
		<h3 class='prodtitles'><?php echo $title_products_for_buyers ?></h3>
		<?php
	}
	?>
	<div class='usam_products_list'>
		<div class='jcarousel-wrapper carousel_products carousel_related_products'>	
			<section class='carousel'>				
				<button class='up-carousel'><span></span></button>
				<div id = 'jcarousel' class='jcarousel' scroll-items='1'>
					<ul>
						<?php			
						while (usam_have_products()) :  	
							usam_the_product(); 					
							?>
							<li>						
								<a href="<?php echo usam_product_url(); ?>">
									<div class="p_image"><?php usam_product_thumbnail(); ?>	</div>
									<h4 class="p_title"><?php the_title(); ?></h4>					
									<div class="p_price"><span><?php usam_product_price_currency( ); ?></span></div>
								</a>					
							</li>
						<?php endwhile; ?>	
					</ul>
				</div>
				<button class='down-carousel'><span></span></button>
			</section>
		</div>		
	</div>	
</div>	