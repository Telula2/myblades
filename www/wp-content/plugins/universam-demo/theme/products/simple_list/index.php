<div class = 'usam_simple_list usam_products'>
	<?php
	if ( !empty($title_products_for_buyers) )
	{ ?>
		<h3 class='prodtitles'><?php echo $title_products_for_buyers ?></h3>
		<?php
	}
	?>
	<div class='usam_products_list'>
	<?php 
	while (usam_have_products()) :  	
		usam_the_product(); 			
		?>
		<div class='usam_product'>
		<a href='<?php echo usam_product_url(); ?>' class='preview_link'>			
		<?php usam_product_thumbnail( ); ?>
		<div class = 'product_title'><?php the_title(); ?></div>			
		<?php 
		if( usam_is_product_discount() )
		{				
			?>
			<div class = "price_sale"><?php usam_product_price_currency( true ); ?></div>
			<div class = "price"><?php usam_product_price_currency( ); ?></div>
			<?php 
		} 
		else 
		{
			?>
			<div class = "price_sale"></div>
			<div class = "price"><?php usam_product_price_currency( ); ?></div>
			<?php 
		}
		?>
		</a>
		</div>
	<?php endwhile; ?>	
	</div>
</div>	