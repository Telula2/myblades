<div class = 'usam_full_product usam_products'>
	<?php
	if ( !empty($title_products_for_buyers) )
	{ ?>
		<h3 class='prodtitles'><?php echo $title_products_for_buyers ?></h3>
		<?php
	}
	?>
	<div class='usam_products_list'>
	<?php 
	while (usam_have_products()) :  	
		usam_the_product(); 			
		?>
		<div class='usam_product'>
			<div class='product_actions'>
				<a href='<?php echo usam_product_url(); ?>' class='preview_link'>			
				<?php usam_product_thumbnail( ); ?>
				<div class = 'product_title'><?php the_title(); ?></div>	
				</a>				
				<?php 
				if( usam_is_product_discount() )
				{				
					?>
					<div class = "price_sale"><?php usam_product_price_currency( true ); ?></div>
					<div class = "price"><?php usam_product_price_currency( ); ?></div>
					<?php 
				} 
				else 
				{
					?>
					<div class = "price_sale"></div>
					<div class = "price"><?php usam_product_price_currency( ); ?></div>
					<?php 
				}
				?>				
				<div class="product_actions_wrapper">
					<div class="rating"><?php usam_product_rating( ); ?></div>						
					<div class="button_product">
						<?php $class = usam_checks_product_from_customer_list( 'desired' )?'yes':'no'; ?>
						<a href="" title="<?php _e('Добавить в избранное', 'usam'); ?>" id="desired_product" data-product_id="<?php echo get_the_ID(); ?>" class = "site-icon-desired desired_product <?php echo $class; ?> feedback_img"></a>		
						<div id="desired_product_result" class = "result_action">	
							<img alt="<?php _e('Загрузка', 'usam'); ?>" src="<?php echo usam_loading_animation_url(); ?>" />
							<?php _e('Обработка...', 'usam'); ?>
						</div>
					</div>		
					<a href='<?php echo usam_product_url(); ?>' class='preview_link'><div class="see"><?php _e('Посмотреть', 'usam'); ?></div></a>	
				</div>		
			</div>
		</div>
	<?php endwhile; ?>	
	</div>
</div>	