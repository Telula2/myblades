<?php
/**
 * Name: Атол онлайн
 */
class USAM_Cashbox_atol_online extends USAM_Cashbox
{		
	private $delimiter = ';';
	private $api_version  = 'v3';	
	
	function __construct( $id ) 
	{
		parent::__construct( $id );		
		$this->api_url = "https://online.atol.ru/possystem/{$this->api_version}/";		
	}	
	
	private function get_token( )
	{	
		$result = $this->send_request( "getToken" );	
		if ( !isset( $result['code'] ) || $result['code'] >= 2 ) 
		{		
			$this->set_error( $result);	
			return false;
		}	
		if ( !empty($result['token']) )
		{
			$this->option['token'] = $result['token'];
			$this->option['time'] = time();
			usam_update_cashbox( $this->option, $this->id );
		}
		return $result;
	}
	
	/*
	operation: тип операции, которая должна быть выполнена. Возможные типы операция:
	* sell: чек «Приход»;
	* sell_refund: чек «Возврат прихода»;
	* sell_correction: чек «Коррекция прихода»;
	* buy: чек «Расход»;
	* buy_refund: чек «Возврат расхода»;
	* buy_correction: чек «Коррекция расхода»
	*/
	private function register_document( $operation, $params )
	{			
		$this->get_token();
		if ( empty($this->option['token']) )
			return false;
		$params['tokenid'] = $this->option['token'];
		
		$result = $this->send_request( "group_code/{$operation}", $params );
		if ( !isset( $result['code'] ) || $result['code'] >= 2 ) 
		{		
			$this->set_error( $result);	
			return false;
		}	
		return $result;
	}
	
	protected function send_sell( $params ) 
	{
		$result = $this->register_document( 'sell', $params );	
		return $result;
	}
	
	protected function send_sell_refund( $params ) 
	{
		return $this->register_document( 'sell_refund', $params );
	}	
	
	protected function get_data_refund( $id )
	{						
		$document = usam_get_return_purchases( $id );
		$products = usam_get_products_return_purchasess( $id );				
	
		$data = array( 'totalprice' => $document['sum'], 'date_insert' => $document['date_insert'], 'products' => array() );
		foreach ( $products as $product ) 
		{
			$sum = $product->price * $product->quantity;
			
			$data['products'][] = array( 
				"sum" => usam_string_to_float( $sum ),
				"tax" => $this->option['setting']['tax'],
				"tax_sum" => usam_string_to_float( $product->tax * $product->quantity ),
				"name" => $product->name,
				"price" => usam_string_to_float( $product->old_price ),
				"quantity" => $product->quantity,
			);
		}	
		$data['phone'] = usam_get_buyers_phone( $document['order_id'] );
		$data['email'] = usam_get_buyers_email( $document['order_id'] );
		return $data;
	}		

	protected function get_data_sell( $order_id )
	{						
		$order = new USAM_Order( $order_id );
		$order_data = $order->get_data();		
		$order_products = $order->get_order_products();				
		$customer_data = $order->get_customer_data();	
		
		$data = array( 'totalprice' => $order_data['totalprice'], 'date_insert' => $order_data['date_insert'], 'products' => array() );
		$data['products'] = array();
		foreach ( $order_products as $product ) 
		{
			$sum = $product->price * $product->quantity;
			
			$data['products'][] = array( 
				"sum" => usam_string_to_float( $sum ),
				"tax" => $this->option['setting']['tax'],
				"tax_sum" => usam_string_to_float( $product->tax * $product->quantity ),
				"name" => $product->name,
				"price" => usam_string_to_float( $product->old_price ),
				"quantity" => $product->quantity,
			);
		}	
		if ( usam_is_type_payer_company( $order_data['type_payer'] ) )
		{
			$data['email'] = !empty($customer_data['company_email'])?$customer_data['company_email']['value']:'';
			$data['phone'] = !empty($customer_data['company_phone'])?$customer_data['company_phone']['value']:'';		
		}
		else
		{
			$data['email'] = !empty($customer_data['billingemail'])?$customer_data['billingemail']['value']:'';		
			$data['phone'] = !empty($customer_data['billingphone'])?$customer_data['billingphone']['value']:'';
		}		
		return $data;
	}
	
	/*
	payment_type - Вид оплаты. Возможные значения:
	* «0» – наличными;
	* «1» – электронными,
	* «2» – предварительная оплата (аванс);
	* «3» – последующая оплата (кредит);
	* «4» – иная форма оплаты (встречное предоставление);
	* «5» – «9» – расширенные типы оплаты. Для каждого фискального типа оплаты можно указать расширенный тип оплаты.
	
	Устанавливает номер налога в ККТ. Перечисление со значениями:
	* «none» – без НДС;
	* «vat0» – НДС по ставке 0%;
	* «vat10» – НДС чека по ставке 10%;
	* «vat18» – НДС чека по ставке 18%;
	* «vat110» – НДС чека по расчетной ставке 10/110;
	* «vat118» – НДС чека по расчетной ставке 18/118.
	*/		
	protected function get_send_data( $data, $payment_type )
	{	
		$export = array( 
			"timestamp" => date( "d.m.Y H:i:s", strtotime($data['date_insert']) ),
			"total" => usam_string_to_float($data['totalprice']),
			"service" => array(
				"inn"             => $this->option['setting']['inn'],				
				"payment_address" => $this->option['setting']['address'],
				"callback_url"    => $this->callback_url,
			),
			"receipt" => array(
				"items" => $data['products'],
			),		
			"payments" => array(
				"sum"  => usam_string_to_float($data['totalprice']),
				"type" => $payment_type,
			),	
			"attributes" => array(				
				"sno"   => $this->option['setting']['sno'],
				"email" => $data['phone'],
				"phone" => $data['email'],
			),	
		);		
		return $export;
	}
		
	public function get_form() 
	{
		$default = array( 'login' => '', 'pass' => '', 'group_code' => '', 'sno' => '', 'tax' => '', 'inn' => '', 'address' => '' );				
		if ( !empty($this->option['setting']) )
			$option = array_merge ($default, $this->option['setting']);	
		else
			$option = $default;			
		?>	
		<table class="subtab-detail-content-table usam_edit_table" > 
			<tbody>
				<tr><td class="name"><?php esc_html_e( 'Логин', 'usam' ); ?>:</td><td><input type="text" name="cashbox[login]" value="<?php echo $option['login']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Пароль', 'usam' ); ?>:</td><td><input type="text" name="cashbox[pass]"   value="<?php echo $option['pass']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Код группы', 'usam' ); ?>:</td><td><input type="text" name="cashbox[group_code]"   value="<?php echo $option['group_code']; ?>"></td></tr>
				<tr>
					<td class="name"><?php esc_html_e( 'Код группы', 'usam' ); ?>:</td>
					<td>
						<select name="cashbox[sno]">
							<option value="osn" selected=""><?php esc_html_e( 'общая', 'usam' ); ?></option>
							<option value="usn_income"><?php esc_html_e( 'упрощенная', 'usam' ); ?></option>
							<option value="usn_income_outcome"><?php esc_html_e( 'пурощенная (доходы минус расходы)', 'usam' ); ?></option>
							<option value="envd"><?php esc_html_e( 'единый налог на вмененный доход', 'usam' ); ?></option>
							<option value="esn"><?php esc_html_e( 'единый сельскохозяйственный налог', 'usam' ); ?></option>
							<option value="patent"><?php esc_html_e( 'патентная', 'usam' ); ?></option>
						</select>
					</td>
				</tr>	
				<tr>
					<td class="name"><?php esc_html_e( 'Номер налога в ККТ', 'usam' ); ?>:</td>
					<td>
						<select name="cashbox[tax]">
							<option value="none" selected=""><?php esc_html_e( 'без НДС', 'usam' ); ?></option>
							<option value="vat0"><?php esc_html_e( 'НДС по ставке 0%', 'usam' ); ?></option>
							<option value="vat10"><?php esc_html_e( 'НДС чека по ставке 10%', 'usam' ); ?></option>
							<option value="vat18"><?php esc_html_e( 'НДС чека по ставке 18%', 'usam' ); ?></option>
							<option value="vat110"><?php esc_html_e( 'НДС чека по расчетной ставке 10/110', 'usam' ); ?></option>
							<option value="vat118"><?php esc_html_e( 'НДС чека по расчетной ставке 18/118', 'usam' ); ?></option>
						</select>
					</td>
				</tr>
				<tr><td class="name"><?php esc_html_e( 'ИНН организации', 'usam' ); ?>:</td><td><input type="text" name="cashbox[inn]"   value="<?php echo $option['inn']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Адрес места расчетов', 'usam' ); ?>:</td><td><input type="text" name="cashbox[address]"   value="<?php echo $option['address']; ?>"></td></tr>				
			</tbody>
		</table>
		<?php
	}
}