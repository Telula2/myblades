<?php
/**
 * Name: Штрих М 
 */
class USAM_Cashbox_shtrihm extends USAM_Cashbox
{		
	protected $delimiter = ';';
	protected $filename  = 'Pos.spr';
		/*
1	Число 13	Код	Код	
2	Строка 13	Штрихкод	Не используется	
3	Строка 100	Наименование	Наименование	
4	Строка 100	Текст для чека	Не используется	
5	Число 15.2	Цена	Не используется	
6	Число 17.3	Остаток товара 2)	Не используется	
7	Число 5	Код группы скидок	Не используется	К1/2
8	Число 1.0	Разрешить дробное количество	Не используется	
9	Число 2.0	Номер секции	Не используется	
10	Число 5.1	Макс. процент скидки	Не используется	
11	Число 2.0	Код группы налогов	Не используется	
12	Строка 12	Артикул	Не используется	
13	Число 1.0	Запрашивать марку	Не используется	Б 3)
14	—	Не используется	Не используется	
15	—	Не используется	Не используется	
16	Число 13	Код родительской группы	Код родительской группы	
17	Число 1.0	Товар или группа: для товара “1”	Товар или группа: для группы “0”	
18	Строка 255	Список кодов скидочных групп через «,»	Не используется	
19	Число 5.0	Категории блюд	Не используется	Б
20	Число 10.2	Количество модификаторов	Не используется	Б
21	Число 2.0	Комплект 4)	Не используется	Б
22	Число 5.0	Направления печати	Не используется	Б
23	Число 1.0	Отключить контроль остатка	Не используется	
24	Число 1.0	Учёт по доп. характеристикам	Не используется	
25	Число 5.1	Макс. процент бонуса	Не используется	
26	Число 5.0	Признак предмета расчета 5)	Не используется
	*/
	protected function get_export_data( )
	{		
		$this->delimiter = ';';		
	
		$export = array();
		$header = '##@@&&';
		if ( $this->option['setting']['clean'] )
			$header .= '$$$CLR';
		
		$export[] = $header;	
		$export[] = '#';		
		$export[] = '$$$ADD';  
		foreach ( $this->order_ids as $order_id )
		{			
			$order = new USAM_Order( $order_id );
			$order_data = $order->get_data();		
			$order_products = $order->get_order_products();				
			$customer_data = $order->get_customer_data();				
			
			if ( !$order->is_closed_order() )
			{				
				// Пройдите через все продукты в корзину для отображения количества и Артикул				
				foreach ( $order_products as $product ) 
				{
					$code = usam_get_product_meta( $product->product_id, 'code' );				
					$sku = usam_get_product_meta( $product->product_id, 'sku' );	
					$barcode = usam_get_product_meta( $product->product_id, 'barcode' );					
					
					$row['code'] = $product->product_id;
					$row['barcode'] = $barcode;
					$row['title'] = $product->name; // Название в подборе
					$row['text'] = $product->name; // Название в чеке
					$row['price'] = $product->price;
					$row['quantity'] = $product->quantity;
					$row['discont_code'] = '';
					$row['_quantity'] = '';
					$row['section'] = '';
					$row['discont_procent'] = '';
					$row['tax_code'] = '';
					$row['sku'] = $sku;
					$row['13'] = '';
					$row['14'] = '';
					$row['15'] = '';
					$row['cat_code'] = '';
					$row['17'] = 1;
					
					$row['18'] = '';
					$row['19'] = '';
					$row['20'] = '';
					$row['21'] = '';
					$row['22'] = '';
					$row['23'] = '';
					$row['24'] = '';
					$row['25'] = '';
					$row['26'] = '';
					$export[] = implode( $this->delimiter, $row );
				}			
			}
		}		
		$output = implode( chr(10), $export );	
		return $output;
	}
		
	public function get_form() 
	{
		$default = array( 'ftp_folder' => '', 'clean' => 1, 'variant' => 'ftp' );				
		if ( !empty($this->option['setting']) )
			$option = array_merge ($default, $this->option['setting']);	
		else
			$option = $default;			
		?>	
		<div id='shtrihm' class ="usam_tabs usam_tabs_style1">
			<div class='header_tab'>
				<ul>
					<li class='tab'><a href='#tab-general'><?php _e( 'Общие' , 'usam' ); ?></a></li>
					<li class='tab'><a href='#tab-ftp'><?php _e( 'Выгрузка на FTP' , 'usam' ); ?></a></li>
				</ul>
			</div>
			<div class='countent_tabs'>				
				<div id='tab-general' class='tab'>					
					<table class="subtab-detail-content-table usam_edit_table">
						<tr>				
							<td class ="name"><label for='mode_button'><?php _e( 'Очищать список товаров' , 'usam' ); ?>:</label></td>
							<td class ="option">	
								<input type='radio' value='0' name='cashbox[clean]' id='mode_button1' <?php checked($option['clean'],0); ?>/> 		
								<label for='mode_button1'><?php _e( 'Нет', 'usam' ); ?></label> &nbsp;
								<input type='radio' value='1' name='cashbox[clean]' id='mode_button2' <?php checked($option['clean'],1); ?>/>
								<label for='mode_button2'><?php _e( 'Да', 'usam' ); ?></label>	
							</td>
						</tr>
						<tr>				
							<td class ="name"><label for='mode_button'><?php _e( 'Куда выгружать' , 'usam' ); ?>:</label></td>
							<td class ="option">	
								<input type='radio' value='ftp' name='cashbox[variant]' id='mode_button1' <?php checked($option['variant'],'ftp'); ?>/> 		
								<label for='mode_button1'><?php _e( 'FTP', 'usam' ); ?></label> &nbsp;
								<input type='radio' value='file' name='cashbox[variant]' id='mode_button2' <?php checked($option['variant'],'file'); ?>/>
								<label for='mode_button2'><?php _e( 'Сохранить файл', 'usam' ); ?></label>	
							</td>
						</tr>												
					</table>				
				</div>	
				<div id='tab-ftp' class='tab'>					
					<table class="subtab-detail-content-table usam_edit_table">				
						<tr>				
							<td class ="name"><label for='ftp_folder'><?php _e( 'Папка для выгрузки' , 'usam' ); ?>: </label></td>
							<td class ="option"><input class ="width100" type="text" id = "ftp_folder" name="cashbox[ftp_folder]" value="<?php echo $option['ftp_folder']; ?>"/></td>
						</tr>
					</table>				
				</div>
			</div>		
		</div>
		<?php
	}
}