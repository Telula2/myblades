<?php
class USAM_Load_System_Default_Data
{		
	private $db_path;
	function __construct( $type_data ) 
	{			
		$this->db_path = USAM_FILE_PATH . "/admin/db/db-install/";
		if ( !is_array($type_data) )
			$type_data = array($type_data);
		
		foreach ( $type_data as $action )
		{
			$method = 'controller_'.$action;				
			if ( method_exists($this, $method) )
			{
				$this->$method();					
			}
		}
	}
	
	// Очистить таблицу
	private function truncase( $table ) 
	{
		global $wpdb;
		$wpdb->query("TRUNCATE TABLE $table");
	}
	
	// Типы местоположений
	function controller_location_type()
	{
		global $wpdb;	
		
		$this->truncase( USAM_TABLE_LOCATION_TYPE );
		$type_location = array( array( 'name' => __('Страна','usam'), 'code' => 'country', 'sort' => 700, 'level'=> 100 ),
									array( 'name' => __('Округ','usam'), 'code' => 'country_district', 'sort' => 600, 'level'=> 200 ),
									array( 'name' => __('Область','usam'), 'code' => 'region', 'sort' => 500, 'level'=> 300 ),
									array( 'name' => __('Район','usam'), 'code' => 'subregion', 'sort' => 400, 'level'=> 400 ),
									array( 'name' => __('Город','usam'), 'code' => 'city', 'sort' => 100, 'level'=> 500 ),								
									array( 'name' => __('Село','usam'), 'code' => 'village', 'sort' => 200, 'level'=> 600 ),
									array( 'name' => __('Городской район','usam'), 'code' => 'urban_area', 'sort' => 100, 'level'=> 700 ),
									array( 'name' => __('Улица','usam'), 'code' => 'street', 'sort' => 300, 'level'=> 800 ),
									);
		foreach ( $type_location as $value )
		{
			$wpdb->query("INSERT INTO `".USAM_TABLE_LOCATION_TYPE."` (`name`,`code`,`sort`,`level`) VALUES ('".$value['name']."', '".$value['code']."', '".$value['sort']."', '".$value['level']."')" );
		}		
	}
	
	// Группы свойств заказа
	function controller_order_props_group()
	{		
		$this->truncase( USAM_TABLE_ORDER_PROPS_GROUP );
		$group = array(     array( 'name' => __('Личные данные','usam'), 'type_payer_id' => 1, 'code' => 'billing', 'sort' => 100 ),
							array( 'name' => __('Данные для доставки','usam'), 'type_payer_id' => 1, 'code' => 'shipping', 'sort' => 200 ),
							array( 'name' => __('Данные компании','usam'), 'type_payer_id' => 2, 'code' => 'company', 'sort' => 300 ),
							array( 'name' => __('Контактная информация','usam'), 'type_payer_id' => 2, 'code' => 'company_contact_information', 'sort' => 400 ),
									);
		foreach ( $group as $value )
		{
			usam_insert_order_props_group( $value );
		}
	}
	
	//Заполняет и обновляет данные со списком валют
	function controller_currency()
	{		
		$this->truncase( USAM_TABLE_CURRENCY );
		require_once($this->db_path . "currency_list.php");
		
		foreach ( $list as $key => $currency )
		{
			usam_insert_currency( $currency );
		}
		
	}
	
	//Заполняет и обновляет данные со списком стран
	function controller_country()
	{			
		$this->truncase( USAM_TABLE_COUNTRY );
		require_once($this->db_path . "country_list.php");
		foreach ( $list as $key => $country )
		{
			usam_insert_country( $country );					
		}			
	}
	
	function controller_tables()
	{
		$tables = array( 
			USAM_TABLE_SEARCH_ENGINE_REGIONS => array( 'file_path' => "search_engine_regions.csv", 'column' => array( 'id', 'location_id', 'active', 'code', 'name', 'search_engine', 'sort'  ) ) 
		);		
		$this->load_tables( $tables );		
	}
		
	function controller_location()
	{
		global $wpdb;				
		$tables = array( 
			USAM_TABLE_LOCATION => array( 'file_path' => "location.csv", 'column' => array( 'id', 'id_type', 'parent', 'name', 'sort' ) ) 
		);		
		$this->load_tables( $tables );
		$countries = $wpdb->get_results( "SELECT * FROM " . USAM_TABLE_COUNTRY );
		$locations = usam_get_locations( array('type' => 'country', 'fields' => array('id', 'name') ) );
		
		foreach ( $locations as $location )
		{
			foreach ( $countries as $country )
			{
				if (strcasecmp($location->name, $country->name) == 0) 
				{
					$update = array( 'location_id' => $location->id );
					$result = usam_update_country( $country->code, $update );
				}
			}
		}				
	}
	
	function controller_order_status( ) 
	{
		$this->truncase( USAM_TABLE_ORDER_STATUS );
		$order_status = array(
			array(
				'internalname' => 'incomplete_sale',
				'name'         => __( 'Незаконченная продажа', 'usam' ),
				'short_name'   => __( 'Незаконченные', 'usam' ),
				'description'  => '',				
				'sort'         => 1,				
				'visibility'   => false,
				'close'        => false,			
			),
			array(
				'internalname' => 'received',
				'name'         => __( 'Заказ получен', 'usam' ),
				'short_name'   => __( 'Полученные', 'usam' ),				
				'color'        => '#99ccff',	
				'description'  => '',				
				'sort'         => 2,	
				'visibility'   => false,
				'close'        => false,			
			),
			array(
				'internalname'   => 'accepted_payment',
				'name'           => __( 'Оплата принята', 'usam' ),
				'short_name'     => __( 'Оплаченные', 'usam' ),	
				'color'          => '#ffcc66',
				'description'    => '',								
				'sort'           => 3,
				'visibility'     => false,
				'close'          => false,					
			),
			array(
				'internalname'   => 'job_dispatched',
				'name'           => __( 'Идет обработка', 'usam' ),
				'short_name'     => __( 'В обработке', 'usam' ),	
				'color'          => '#ffff99',				
				'description'    => '',				
				'sort'           => 4,	
				'visibility'     => true,
				'close'          => false,				
			),
			array(
				'internalname'   => 'closed',
				'name'           => __( 'Заказ закрыт', 'usam' ),
				'short_name'     => __( 'Закрытые', 'usam' ),	
				'color'          => '#ccff66',
				'description'    => '',								
				'sort'           => 20,			
				'visibility'     => true,
				'close'          => true,				
			),
			array(
				'internalname'   => 'waiting_payment',
				'name'           => __( 'Ожидание оплаты', 'usam' ),
				'short_name'     => __( 'Ожидание оплаты', 'usam' ),
				'description'    => '',								
				'sort'           => 6,				
				'visibility'     => true,
				'close'          => false,				
			),			
			array(
				'internalname'   => 'customer_waiting',
				'name'           => __( 'Готов к выдаче', 'usam' ),
				'short_name'     => __( 'Готов к выдаче', 'usam' ),		
				'description'    => __('Заказ находится в пункте выдачи и ожидает клиента','usam'),
				'color'          => '#ffcccc',					
				'sort'           => 7,
				'visibility'     => true,
				'close'          => false,
			),
			array(
				'internalname'   => 'canceled',
				'name'           => __( 'Заказ отменен', 'usam' ),				
				'short_name'     => __( 'Отмененные', 'usam' ),	
				'color'          => '#d5d8db',					
				'description'    => '',				
				'sort'           => 19,
				'close'          => true,
				'visibility'     => true,
			),
			array(
				'internalname'   => 'not_picked',
				'short_name'     => __( 'Не забрал', 'usam' ),
				'name'           => __( 'Клиент не забрал', 'usam' ),		
				'description'    => __( 'Клиент не забрал заказ со склада', 'usam' ),					
				'sort'           => 18,
				'visibility'     => true,
				'close'          => true,
			),
			array(
				'internalname'   => 'preparing_delivery',
				'short_name'     => __( 'Подготовка доставки', 'usam' ),
				'name'           => __( 'Подготовка доставки', 'usam' ),			
				'description'    => __( 'Заказ подготавливается к передаче курьеру или транспортной компании для отправки', 'usam' ),				
				'sort'           => 9,
				'visibility'     => true,
				'close'          => false,			
			),
			array(
				'internalname'   => 'sending',
				'short_name'     => __( 'Отправленные', 'usam' ),
				'name'           => __( 'Заказ отправлен', 'usam' ),		
				'description'    => __( 'Ваш заказ отправлен по указанному адресу', 'usam' ),					
				'sort'           => 10,
				'visibility'     => true,
				'close'          => false,
			),
			array(
				'internalname'   => 'courier',
				'short_name'     => __( 'Передан курьеру', 'usam' ),
				'name'           => __( 'Передан курьеру', 'usam' ),		
				'description'    => __( 'Ваш заказ передан курьеру', 'usam' ),					
				'sort'           => 5,
				'visibility'     => true,
				'close'          => false,
			),
			array(
				'internalname'   => 'problem',
				'short_name'     => __( 'Проблемные', 'usam' ),
				'name'           => __( 'Проблема с заказом', 'usam' ),		
				'description'    => __( 'С вашим заказом какая то проблема. Менеджер свяжется с вами.', 'usam' ),					
				'sort'           => 8,
				'visibility'     => true,
				'close'          => false,
			),
			array(
				'internalname'   => 'no_connection',
				'short_name'     => __( 'Не удалось связаться', 'usam' ),
				'name'           => __( 'Не удалось связаться', 'usam' ),		
				'description'    => __( 'Менеджеру не удалось связаться с клиентом, ожидаем когда клиент ответит.', 'usam' ),					
				'sort'           => 8,
				'visibility'     => true,
				'close'          => false,
			),
		);			
		foreach ( $order_status as $status )
		{ 
			usam_insert_order_status( $status );
		}				
	}
	
	/**
	 * преобразует значения в десятичные, чтобы удовлетворить строгому режиму MySQL
	 */
	function controller_order_props()
	{			
		$this->truncase( USAM_TABLE_ORDER_PROPS );
		$properties = array( 
		array( 'name' => __('Имя','usam'), 'unique_name' => 'billingfirstname', 'type' => 'text', 'group' => 1, 'mandatory' => 0, 'active' => 1, 'sort' => 10, 'profile' => 1 ),
		array( 'name' => __('Фамилия','usam'), 'unique_name' => 'billinglastname','type' => 'text', 'group' => 1, 'mandatory' => 0, 'active' => 1,'sort' => 20, 'profile' => 1 ),
		array( 'name' => __('Мобильный Телефон','usam'), 'unique_name' => 'billingmobilephone','type' => 'mobile_phone', 'group' => 1, 'mandatory' => 0, 'active' => 1,'sort' => 30, 'profile' => 1 ),
		array( 'name' => __('Телефон','usam'), 'unique_name' => 'billingphone','type' => 'phone', 'group' => 1, 'mandatory' => 0, 'active' => 1,'sort' => 35, 'profile' => 1 ),
		array( 'name' => __('Email','usam'), 'unique_name' => 'billingemail','type' => 'email', 'group' => 1, 'mandatory' => 0, 'active' => 1, 'sort' => 40, 'profile' => 1 ),		
		array( 'name' => __('Страна','usam'), 'unique_name' => 'billingcountry','type' => 'location_type', 'group' => 1, 'mandatory' => 0, 'active' => 0, 'sort' => 60, 'profile' => 1 ),
		array( 'name' => __('Область','usam'), 'unique_name' => 'billingregion', 'type' => 'location_type', 'group' => 1, 'mandatory' => 0, 'active' => 0, 'sort' => 70, 'profile' => 1 ),
		array( 'name' => __('Город','usam'), 'unique_name' => 'billingcity','type' => 'location_type', 'group' => 1, 'mandatory' => 0, 'active' => 0, 'sort' => 80, 'profile' => 1 ),
		array( 'name' => __('Местоположение','usam'), 'unique_name' => 'billinglocation','type' => 'location', 'group' => 1, 'mandatory' => 0, 'active' => 1, 'sort' => 81, 'profile' => 1 ),		
		array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'billingpostcode','type' => 'text', 'group' => 1, 'mandatory' => 0, 'active' => 0, 'sort' => 90, 'profile' => 1 ),
		array( 'name' => __('Адрес','usam'), 'unique_name' => 'billingaddress','type' => 'textarea', 'group' => 1, 'mandatory' => 0, 'active' => 0, 'sort' => 100, 'profile' => 1 ),
		
		array( 'name' => __('Имя','usam'), 'unique_name' => 'shippingfirstname', 'type' => 'text', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 110, 'profile' => 1 ),
		array( 'name' => __('Фамилия','usam'), 'unique_name' => 'shippinglastname','type' => 'text', 'group' => 2, 'mandatory' => 0, 'active' => 1,'sort' => 120, 'profile' => 1 ),
		array( 'name' => __('Местоположение','usam'), 'unique_name' => 'shippinglocation','type' => 'location', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 130, 'profile' => 1 ),
		array( 'name' => __('Страна','usam'), 'unique_name' => 'shippingcountry','type' => 'location_type', 'group' => 2, 'mandatory' => 0, 'active' => 0, 'sort' => 140, 'profile' => 1 ),
		array( 'name' => __('Область','usam'), 'unique_name' => 'shippingregion', 'type' => 'location_type', 'group' => 2, 'mandatory' => 0, 'active' => 0, 'sort' => 150, 'profile' => 1 ),
		array( 'name' => __('Город','usam'), 'unique_name' => 'shippingcity','type' => 'location_type', 'group' => 2, 'mandatory' => 0, 'active' => 0, 'sort' => 160, 'profile' => 1 ),
		array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'shippingpostcode','type' => 'text', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 170, 'profile' => 1 ),
		//array( 'name' => __('Населенный пункт','usam'), 'unique_name' => 'shippinginhabitedlocality','type' => 'location_text', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 175, 'profile' => 1 ),
		array( 'name' => __('Адрес','usam'), 'unique_name' => 'shippingaddress','type' => 'textarea', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 180, 'profile' => 1 ),
		array( 'name' => __('Заметки к заказу','usam'), 'unique_name' => 'shippingnotesclient','type' => 'textarea', 'group' => 2, 'mandatory' => 0, 'active' => 1, 'sort' => 190, 'profile' => 0 ),		
		array( 'name' => __('Название компании','usam'), 'unique_name' => 'company', 'type' => 'text', 'group' => 3, 'mandatory' => 0, 'active' => 1, 'sort' => 200, 'profile' => 1 ),
		array( 'name' => __('Юридический адрес','usam'), 'unique_name' => 'company_adr','type' => 'text', 'group' => 3, 'mandatory' => 0, 'active' => 1, 'sort' => 210, 'profile' => 1 ),
		array( 'name' => __('ИНН','usam'), 'unique_name' => 'inn', 'type' => 'text','group' => 3, 'mandatory' => 0, 'active' => 1, 'sort' => 220, 'profile' => 1 ),
		array( 'name' => __('КПП','usam'), 'unique_name' => 'kpp','type' => 'text', 'group' => 3, 'mandatory' => 0, 'active' => 1, 'sort' => 230, 'profile' => 1 ),
		
		array( 'name' => __('Контактное лицо','usam'), 'unique_name' => 'contact_person','type' => 'text', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 240, 'profile' => 1 ),
		array( 'name' => __('Email','usam'), 'unique_name' => 'company_email','type' => 'email', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 250, 'profile' => 1 ),
		array( 'name' => __('Факс','usam'), 'unique_name' => 'fax', 'type' => 'text', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 260, 'profile' => 1 ),
		array( 'name' => __('Телефон','usam'), 'unique_name' => 'company_phone', 'type' => 'phone', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 270, 'profile' => 1 ),		
		array( 'name' => __('Местоположение','usam'), 'unique_name' => 'company_shippinglocation','type' => 'location', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 280, 'profile' => 1 ),
		array( 'name' => __('Страна','usam'), 'unique_name' => 'company_shippingcountry','type' => 'location_type', 'group' => 4, 'mandatory' => 0, 'active' => 0, 'sort' => 290, 'profile' => 1 ),
		array( 'name' => __('Область','usam'), 'unique_name' => 'company_shippingregion', 'type' => 'location_type', 'group' => 4, 'mandatory' => 0, 'active' => 0, 'sort' => 300, 'profile' => 1 ),
		array( 'name' => __('Город','usam'), 'unique_name' => 'company_shippingcity','type' => 'location_type', 'group' => 4, 'mandatory' => 0, 'active' => 0, 'sort' => 310, 'profile' => 1 ),
		array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'company_shippingpostcode','type' => 'text', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 320, 'profile' => 1 ),
		array( 'name' => __('Адрес','usam'), 'unique_name' => 'company_shippingaddress','type' => 'textarea', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 330, 'profile' => 1 ),
		array( 'name' => __('Заметки к заказу','usam'), 'unique_name' => 'company_shippingnotesclient','type' => 'textarea', 'group' => 4, 'mandatory' => 0, 'active' => 1, 'sort' => 340, 'profile' => 0 ),
		
		array( 'name' => __('Как Вы узнали о нас','usam'), 'unique_name' => 'find_us','type' => 'find_us', 'group' => 0, 'mandatory' => 0, 'active' => 0, 'sort' => 350, 'profile' => 0 ),		
		);		
		foreach ( $properties as $property )
		{
			$properties_order = new USAM_Order_Property( $property );
			$properties_order->save();
		}
	}
	
	function controller_product_attributes( )
	{
		$new_product_attributes = array(
			array( 'title' => 'Основные', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	 'slug' => 'main' ), 'sub' => array(
				array( 'title' => 'Страна', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,'slug' => 'country' ), ),
				array( 'title' => 'Бренд', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,  'slug' => 'brand' ), ),
				array( 'title' => 'Коллекция', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,'slug' => 'collection' ), ),
				array( 'title' => 'Материал', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'material' ), ),	
				array( 'title' => 'Цвет', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	    'slug' => 'color' ), ),	
				array( 'title' => 'Гарантия', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'warranty ' ), ),	
				array( 'title' => 'Срок службы', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'lifetime' ), ),	
			)),
			array( 'title' => 'Габариты', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'dimensions' ), 'sub' => array(	
				array( 'title' => 'Длина', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'length' ), ),
				array( 'title' => 'Ширина', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'width' ), ),	
				array( 'title' => 'Высота', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'height' ), ),
				array( 'title' => 'Диаметр', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'diameter' ), ),
				array( 'title' => 'Объем', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'volume' ), ),	
				array( 'title' => 'Вес', 'img' => '', 'args' => array( 'description' => '', 'parent' => 0,	'slug' => 'weight' ), ),	
			)),	
		);
		foreach ( $new_product_attributes as $attribute ) 
		{
			$term = wp_insert_term( $attribute['title'], 'usam-product_attributes', $attribute['args'] );	
			if ( !is_wp_error($term) ) 		
			{				
				foreach ( $attribute['sub'] as $attr ) 
				{			
					$attr['args']['parent'] = $term['term_id'];
					$term2 = wp_insert_term( $attr['title'], 'usam-product_attributes', $attr['args'] );			
				}
			}	
		}	
	}
	
	function load_tables( $tables )
	{
		global $wpdb;
		foreach ( $tables as $table => $data )
		{
			$this->truncase( $table );
			$list = usam_read_txt_file( $this->db_path.$data['file_path'], ',' ); 				
			foreach ( $list as $value )
			{	
				$insert = array();
				foreach ( $data['column'] as $number => $key )
				{
					$insert[$key] = $value[$number];
				}			
				$result = $wpdb->insert( $table, $insert );
			}	
		}	
	}	
}
?>