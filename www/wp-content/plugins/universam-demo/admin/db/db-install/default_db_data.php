<?php
global $user_ID, $wpdb;

add_filter( 'http_request_host_is_external', create_function('', 'return true;'), 10, 3 );

$image_url = "http://wp-universam.ru/wp-content/uploads/sl/downloadables/fotos/universam/";

$new_brands = array(
	array( 'title' => 'Easy Life', 'img' => 'EazyLife.jpg', 'args' => array( 'description' => 'История компании Nuova R2S берет начало в пятидесятых годах прошлого века, когда основатели фабрики решили организовать производство небьющейся посуды и товаров для детей. Со временем запросы рынка способствовали расширению компании. Сегодня Nuova R2S предлагает стильную и яркую посуду, которая украсит любую трапезу. Дизайнеры бренда ежегодно разрабатывают и предлагают по несколько новых креативных коллекций. Кроме того Nuova R2S создает штучные экземпляры своей продукции, которые красиво упакованы отдельно и объединены одной темой. Таким образом, вы сами можете собрать свой особенный сервиз.', 'parent' => 0, )),
	array( 'title' => 'Metrot',  'img' => 'metrot.jpg','args' => array( 'description' => 'В России, на рынке эмалированной посуды, "МЕТРОТ" работает уже более 6 лет и на сегодняшний день является одним из крупнейших поставщиков металлической эмалированной посуды. Современные хозяйки все чаще обращают внимание на яркие, необычные и оригинальные новинки, и отдают им свое предпочтение.', 'parent' => 0 )),
	array( 'title' => 'Luminarc',  'img' => 'Luminarc.jpg','args' => array( 'description' => 'Благодаря совершенствованию производства, в 1948 году концерн ARC Int. наладил выпуск высококачественного сверкающего стекла под брендом LUMINARC. Этот бренд очень быстро завоевал сердца огромного количества домохозяек во всем мире. Марка Luminarc представлена широким спектром стеклянных изделий различного применения. Она с лучшей стороны зарекомендовала себя на отечественном рынке. В ассортименте представлена столовая посуда изо всех видов стекла (матового, прозрачного, декорированного). Всю посуду можно мыть в посудомоечной машине и использовать в микроволновой печи.', 'parent' => 0 )),
	array( 'title' => 'Pyrex', 'img' => 'Pyrex.jpg', 'args' => array( 'description' => 'Pyrex – ведущий бренд кухонной посуды, который всегда был известен своей линейкой посуды из жаропрочного стекла, включающей посуду для приготовления, посуду для духовки, для выпечки и для хранения. Сегодня Pyrex также предлагает огромный выбор продуктов разнообразного дизайна из любых материалов, удовлетворяя любые требования к кухонной посуде. Pyrex можно найти на кухне повсюду – от холодильника и морозильника до микроволновой печи и духовки!', 'parent' => 0)),
	array( 'title' => 'Berlinger Haus',  'img' => 'BerlingerHaus.jpg','args' => array( 'description' => '', 'parent' => 0)),
	array( 'title' => 'Ballarini', 'img' => 'Ballarini.jpg', 'args' => array( 'description' => 'Сегодня бренд Ballarini возглавляет сегмент рынка элитной посуды с противопригарным покрытием. В ассортименте продукции компании представлены серии от бюджетных до профессиональных. Производство, основанное на инновационных разработках, учитывает кулинарные традиции Италии. Компания постоянно совершенствует состав антипригарного покрытия, повышая его прочность и устойчивость к деформациям. Главный слоган бренда, которым управляет уже пятое поколение семьи Балларини, - «будущее традиции», а позиционируется компания как толкователь и популяризатор блюд высокого качества.', 'parent' => 0)),
	array( 'title' => 'Arcoroc', 'img' => 'Arcoroc.jpg', 'args' => array( 'description' => 'Бренд Arcoroc предлагает полную и разнообразную линию продуктов для профессионального рынка: столовая посуда, стекло и аксессуары. Ассортимент, производимый концерном, насчитывает более 8000 наименований Профессиональное стекло от ARC International обладает особой прочностью и подходит для мытья в посудомоечных машинах и обладает повышенной устойчивостью к царапинам. Благодаря постоянным разработкам новейших технологий в компании ARC International, продукция Arcoroc удовлетворяет требованиям профессионалов ресторанного бизнеса по функциональности, прочности и капиталовложениям.', 'parent' => 0)),
	array( 'title' => 'Барьер', 'img' => 'barier.jpg', 'args' => array( 'description' => 'БАРЬЕР — крупнейший производитель бытовых фильтров для воды в России. Компания начала производить бытовые фильтры для очистки питьевой воды еще в 1993 году. С тех пор прошло более 20 лет, и сегодня БАРЬЕР – это 4 производственных предприятия, собственная научно-исследовательская лаборатория, полностью автоматизированные и роботизированные немецкие производственные линии и более 300 квалифицированных сотрудников.', 'parent' => 0)),
	
);
$brands = array();
foreach ( $new_brands as $brand ) 
{
	$term = wp_insert_term( $brand['title'], 'usam-brands', $brand['args'] );
	if ( !is_wp_error($term) ) 
	{
		$url     = $image_url.'brands/'.$brand['img'];
		$desc    = "";
		
		$file_array = array();
		$tmp = download_url( $url );
			
		preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
		$file_array['name'] = basename( $matches[0] );
		$file_array['tmp_name'] = $tmp;	
		$thumbnail_id = media_handle_sideload( $file_array, 0, $desc );	
	
		update_term_meta( $term['term_id'], 'thumbnail', $thumbnail_id );
		
		$brands[] = $term['term_id'];
	}
	else
	{
		$term = get_term_by( 'name', $brand['title'], 'usam-brands' );	
		$brands[] = $term->term_id;
	}
}
$new_category_sale = array(
	array( 'title' => 'Скидка 20% на фарфор "Easy Life"', 'img' => '1.jpg', 'args' => array( 'description' => 'С 7 по 30 сентября скидка 20% на весь ассортимент торговых марок "Easy Life"', 'parent' => 0)),
	array( 'title' => 'Скидка новым покупателям!', 'img' => '2.jpg', 'args' => array( 'description' => 'Каждому новому покупателю, при оформлении заказа с доставкой курьером, мы дарим дополнительную СКИДКУ 5%!', 'parent' => 0)),
);
$category_sale = array();
foreach ( $new_category_sale as $category ) 
{
	$term = wp_insert_term( $category['title'], 'usam-category_sale', $category['args'] );	
	if ( !is_wp_error($term) ) 		
	{		
		$url     = $image_url.$category['img'];
		$desc    = "";
		
		$file_array = array();
		$tmp = download_url( $url );
			
		preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
		$file_array['name'] = basename( $matches[0] );
		$file_array['tmp_name'] = $tmp;	
		$thumbnail_id = media_handle_sideload( $file_array, 0, $desc );	
	
		update_term_meta( $term['term_id'], 'thumbnail', $thumbnail_id );
		
		$category_sale[] = $term['term_id'];
		update_term_meta($term['term_id'], 'usam_status_stock', 1 );
	}
	else
	{
		$term = get_term_by( 'name', $category['title'], 'usam-category_sale' );
		$category_sale[] = $term->term_id;
	}
}	
$new_categories = array(
	array( 'title' => 'Столовая посуда', 'args' => array( 'description' => 'Столовая посуда - предназначена для подачи и приема пищи, но она также является украшением Вашего стола.', 'parent' => 0, 'slug' => sanitize_title('Столовая посуда'))),
	array( 'title' => 'Посуда для приготовления', 'args' => array( 'description' => 'Для приготовления кулинарных шедевров, просто необходима отличная посуда.', 'parent' => 0)),
	array( 'title' => 'Кухонные принадлежности', 'args' => array( 'description' => '', 'parent' => 0 )),
	array( 'title' => 'Посуда для чая и кофе', 'args' => array( 'description' => 'Существует множество традиций чаепития или выпивания кофе. Разработано множество рецептов приготовления и заваривания этих напитков. Но для употребления напитков обязательно должна быть комфортная и красивая посуда для чая и кофе.', 'parent' => 0 )),
);
$categories = array();
foreach ( $new_categories as $category ) 
{
	$term = wp_insert_term( $category['title'], 'usam-category', $category['args'] );
	if ( !is_wp_error($term) ) 		
	{		
		$categories[] = $term['term_id'];
	}
	else
	{
		$term = get_term_by( 'name', $category['title'], 'usam-category' );
		$categories[] = $term->term_id;
	}
}	

$products_data = array(
	array(		
		'post_content'   => '',
		'post_excerpt'   => 'Этот велликолепный Кувшин для масла "OLIVES" 50cl прекрасно будет свотреться на вашем столе',
		'post_title'     => 'Кувшин для масла "OLIVES" 50cl',
		'post_status'    => 'publish',	
		'post_type'      => "usam-product",
		'tax_input' => array('usam-category' => array($categories[0]), 'usam-brands' => array($brands[0]), 'usam-category_sale' => array($brands[0]) ),
		'thumbnail' => $image_url.'products/OVS0730.jpg',
		
		'meta' => array(														
			'code' => '',											
			'sku' => 'OVS0730',
			'barcode' => '',									
			'product_views' => 670,										
			'rating' => 5,			
			'rating_count' => 3, //Количество проголосовавших за товар		
			'price_tp_1' => 850,		
			'storage_1' => 23,			
			'weight' => 0,		
			'product_metadata' =>	array( 											
				'bonuses' => array ( 'value' => 100, 'type' => 'p' ),	
				'webspy_link' => '',										
				'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
				'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),			
				'unit_measure' => 'thing',
				'unit' => 1,											
			),								
		),			
	),
	
	array(		
			'post_content'   => '',
			'post_excerpt'   => 'Оригинальная кружка OPTICAL D в картонной подарочной упаковке. Емкость 300 мл. Материал: фарфор.',
			'post_title'     => 'Кружка OPTICAL D',
			'post_status'    => 'publish',	
			'post_type'      => "usam-product",
			'tax_input' => array('usam-category' => array($categories[0]), 'usam-brands' => array($brands[0]) ),
			'thumbnail' => $image_url.'products/OPTD1380.jpg',
			
			'meta' => array(														
				'code' => '',											
				'sku' => 'OPTD1380',
				'barcode' => '',									
				'product_views' => 60,										
				'rating' => 5,		
				'rating_count' => 3, //Количество проголосовавших за товар		
				'price_tp_1' => 700,		
				'storage_1' => 23,		
				'weight' => 0,					
				'product_metadata' =>	array( 											
					'bonuses' => array ( 'value' => 100, 'type' => 'p' ),	
					'webspy_link' => '',										
					'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
					'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),
					'unit_measure' => 'thing',
					'unit' => 1,												
																		
				),								
			),			
		),
		
	array(		
			'post_content'   => '',
			'post_excerpt'   => 'Сковорода OPTIMA 26 см глубокая изготовлена из утолщенного кованного алюминия. Сверхпрочное антипригарное покрытие не содержит перфтороктановой кислоты, свинца и кадмия. Оно является безопасным для здоровья и экологически чистым. Подходит для использования на всех типах плит, в том числе и на индукционных. Удобная ручка с покрытием Soft Touch изготовлена из прочного бакелита, на конце силиконовая вставка. Сковороды серии OPTIMA отличаются увеличенным объемом и высотой бортов.',
			'post_title'     => 'Сковорода OPTIMA 26 см глубокая',
			'post_status'    => 'publish',	
			'post_type'      => "usam-product",
			'tax_input' => array('usam-category' => array($categories[1]), 'usam-brands' => array($brands[0]) ),
			'thumbnail' => $image_url.'products/OP26DF2.jpg',
			
			'meta' => array(														
				'code' => '',											
				'sku' => 'OP26DF2',
				'barcode' => '',									
				'product_views' => 60,										
				'rating' => 5,	
				'rating_count' => 3, //Количество проголосовавших за товар		
				'price_tp_1' => 1850,		
				'storage_1' => 23,			
				'weight' => 0,						
				'product_metadata' =>	array( 											
					'bonuses' => array ( 'value' => 100, 'type' => 'p' ),	
					'webspy_link' => '',										
					'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
					'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),						
					'unit_measure' => 'thing',
					'unit' => 1,																	
				),								
			),			
		),
		
	array(		
			'post_content'   => '',
			'post_excerpt'   => '',
			'post_title'     => 'Скороварка BERLINGER HAUS 6 л',
			'post_status'    => 'publish',	
			'post_type'      => "usam-product",
			'tax_input' => array('usam-category' => array($categories[1]), 'usam-brands' => array($brands[0]) ),
			'thumbnail' => $image_url.'products/BH-1080.jpg',
			
			'meta' => array(														
				'code' => '',											
				'sku' => 'BH-1080',
				'barcode' => '',									
				'product_views' => 60,										
				'rating' => 5,		
				'rating_count' => 3, //Количество проголосовавших за товар		
				'price_tp_1' => 3850,		
				'storage_1' => 23,				
				'weight' => 0,
				'product_metadata' =>	array( 											
					'bonuses' => array ( 'value' => 100, 'type' => 'p' ),	
					'webspy_link' => '',										
					'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
					'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),		
					'unit_measure' => 'thing',
					'unit' => 1,															
				),								
			),			
		),	
	array(		
			'post_content'   => '',
			'post_excerpt'   => 'Сотейник из коллекции PRIOR изготовлен из литого алюминия. Антипригарное покрытие TEFLON® CLASSIC внутри и снаружи, высокое качество без PFOA. Посуда PRIOR подходит для всех типов варочных панелей, включая индукционные. А так же для мытья в посудомоечной машине. Эргономичные литые ручки с силиконовыми прихватками обеспечивают удобный захват. Диаметр 28 см, объем 3,1 л.',
			'post_title'     => 'Сотейник PRIOR 28 см',
			'post_status'    => 'publish',	
			'post_type'      => "usam-product",
			'tax_input' => array('usam-category' => array($categories[0]), 'usam-brands' => array($brands[0]) ),		
			'thumbnail' => $image_url.'products/3560V728.jpg',	
			
			'meta' => array(														
				'code' => '',											
				'sku' => '3560V728',
				'barcode' => '',									
				'product_views' => 60,										
				'rating' => 5,		
				'rating_count' => 3, //Количество проголосовавших за товар						
				'price_tp_1' => 2350,	
				'storage_1' => 23,				
				'weight' => 0,	
				'product_metadata' =>	array( 											
					'bonuses' => array ( 'value' => 100, 'type' => 'p' ),	
					'webspy_link' => '',										
					'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
					'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),
					'unit_measure' => 'thing',
					'unit' => 1,	
				),								
			),			
		),
	);	
$product_ids = array();
foreach ( $products_data as $key => $product ) 
{
	$_product = new USAM_Product( $product );		
	$product_id = $_product->insert_product();	
	
	$product_ids[] = $product_id;
}
/* Создание заказов */
/* Заказ №1*/
$data = array( 'totalprice' => 0, 'type_price' => 'tp_1','paid' => 0,'date_paid' => '','status' => 'received', 'type_payer' => 1,'user_ID' => 0,'coupon_name' => 0, 'notes' => "Клиент не отвечает", 'order_type' => 'order');	

$order_new = new USAM_Order( $data );
$order_new->save();	
$order_id = $order_new->get('id');

$order_products = array( array( 'order_id' => $order_id, 'product_id' => $product_ids[0], 'name' => 'Кувшин для масла "OLIVES" 50cl', 'order_id' => 1,'old_price' => 0,'price' => 608, 'quantity' => 1 ) , 
						 array( 'order_id' => $order_id, 'product_id' => $product_ids[1], 'name' => 'Кружка OPTICAL D', 'order_id' => 1,'old_price' => 0,'price' => 308, 'quantity' => 10 ) );	
	
$new_document = array( 'method' => 2, 'storage' => 1, 'storage_pickup' => 1, 'order_id' => $order_id );
$order_new->add_order_products( $order_products );
$order_products = $order_new->get_order_products();
foreach ( $order_products as $product ) 
{						
	$new_document['products'][] = array( 'basket_id' => $product->id, 'quantity' => $product->quantity, 'reserve' => $product->quantity );
}		
$order_new->add_document_shipped( $new_document );

$customer_data = array( 'billingfirstname' => 'Иван', 'billinglastname' => 'Кузнецов', 'Иванович' => '', 'billingemail' => 'office@wp-universam.ru', 'shippinglocation'=> 981, 'shippingaddress' => 'г. Калининград ул. Куйбышева д.4', 'shippingnotesclient' => 'Сообщите когда будет доставка' );
$order_new->save_customer_data( $customer_data );


/* Заказ №2*/

$data = array( 'totalprice' => 0, 'type_price' => 'tp_1','paid' => 0,'date_paid' => '','status' => 'closed', 'type_payer' => 1,'user_ID' => 0,'notes' => "", 'order_type' => 'order');	

$order_new = new USAM_Order( $data );
$order_new->save();	
$order_id = $order_new->get('id');

$order_products = array( array( 'order_id' => $order_id, 'product_id' => $product_ids[0], 'name' => 'Кувшин для масла "OLIVES" 50cl', 'order_id' => 1,'old_price' => 0,'price' => 608, 'quantity' => 1 ) , 
						 array( 'order_id' => $order_id, 'product_id' => $product_ids[1], 'name' => 'Кружка OPTICAL D', 'order_id' => 1,'old_price' => 0,'price' => 308, 'quantity' => 10 ) );	
	
$new_document = array( 'method' => 2, 'storage' => 1, 'storage_pickup' => 1, 'order_id' => $order_id );
$order_new->add_order_products( $order_products );
$order_products = $order_new->get_order_products();
foreach ( $order_products as $product ) 
{						
	$new_document['products'][] = array( 'basket_id' => $product->id, 'quantity' => $product->quantity, 'reserve' => $product->quantity );
}		
$order_new->add_document_shipped( $new_document );

$customer_data = array( 'billingfirstname' => 'Иван', 'billinglastname' => 'Кузнецов', 'Иванович' => '', 'billingemail' => 'office@wp-universam.ru', 'shippinglocation'=> 981, 'shippingaddress' => 'г. Калининград ул. Куйбышева д.4', 'shippingnotesclient' => '' );
$order_new->save_customer_data( $customer_data );

	
$images = array( '1.jpg', '2.jpg');
$image_ids = array();
foreach ( $images as $key => $image ) 
{
	$url     = $image_url.$image;
	$desc    = "";
	
	$file_array = array();
	$tmp = download_url( $url );
		
	preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
	$file_array['name'] = basename( $matches[0] );
	$file_array['tmp_name'] = $tmp;	
	$image_ids[] = media_handle_sideload( $file_array, 0, $desc );	
}

$slider = array( 'name' => 'Слайдер акций и новинок', 'active' => 1, 'type' => 'I', 'template' => 'simple','template' => 'simple', 'setting' => array( 'button' => array( 'show' => true, 'styling' => 'mini', 'fon' => '', 'fon_active' => '', 'border_color' => '' ), 'show' => '', 'autospeed' => 6000, 'roles' => array(), 'sales_area' => array() ) );
$slides = array(
	array( 'object_id' => $image_ids[0], 'link' => '' ),
	array( 'object_id' => $image_ids[1], 'link' => '' ),
);

$slider = new USAM_Slider( $slider );				
$slider->save();
$slider->save_slides( $slides );


$feedback = array(
	array( 'user_id' => 1, 'id_product' => 1, 'type_message' => 'buy-product', 'mail' => 'office@wp-universam.ru', 'phone' => '', 'name' => 'Алексей', 'message' => 'Хотим купить ваш товар. Свяжитесь с нами пожалуйста!' ),
	array( 'user_id' => 1, 'id_product' => 1, 'type_message' => 'buy-product', 'mail' => 'office@wp-universam.ru', 'phone' => '', 'name' => 'Алексей', 'message' => 'Нам не понятно до скольки вы работаете?' ),
	array( 'user_id' => 1, 'id_product' => 1, 'type_message' => 'shipping-info', 'mail' => 'office@wp-universam.ru', 'phone' => '', 'name' => 'Игорь', 'message' => 'Нам за товаром самим приехать?' ),
	array( 'user_id' => 1, 'id_product' => 1, 'type_message' => 'contact-form', 'mail' => 'office@wp-universam.ru', 'phone' => '', 'name' => 'UNIVERSAM', 'message' => 'Магазин готов к работе. Будут вопросы свяжитесь с нами.' ),
);

foreach ( $feedback as $data ) 
{		
	usam_insert_feedback( $data );		
}


$review = array(
	array( 'user_id' => 1, 'page_id' => 1, 'rating' => 4, 'mail' => 'office@wp-universam.ru', 'name' => 'Миронов Сергей', 'title' => 'Очень удобный и красивый сайт.', 'review_text' => 'Поздравляем! У вас очень хороший сайт. Будем рады покупать у вас.' ),

);
foreach ( $review as $data ) 
{		
	usam_insert_review( $data );		
}

//Почта
//========================================================================================
$mailbox = array( 'name' => 'Интернет магазин - '.get_option( 'blogname' ), 'email' => 'customer@wp-universam.ru', 'pop3server' => 'pop.yandex.ru', 'pop3port' => 995, 'pop3user' => 'customer@wp-universam.ru', 'pop3pass' => '4nm21R4d6S','pop3ssl' => 1,'letter_id' => 0, 'sort' => 100 );

$result_insert = $wpdb->insert( USAM_TABLE_MAILBOX, $mailbox, array( 'name' => '%s', 'email' => '%s', 'pop3server' => '%s', 'pop3port' => '%d', 'pop3user' => '%s', 'pop3pass' => '%s','pop3ssl' => '%d','letter_id' => '%d', 'sort' => '%d' ) );
$users = get_users( array('role'   => 'administrator',	'fields' => 'ID') );

$mailbox_id = $wpdb->insert_id;
$sql = "INSERT INTO `".USAM_TABLE_MAILBOX_USERS."` (`id`,`user_id`) VALUES ('%d','%d') ON DUPLICATE KEY UPDATE `user_id`='%d'";	
foreach ( $users as $userid ) 
{
	$wpdb->query( $wpdb->prepare($sql, $mailbox_id, $userid, $userid ));	
}
	
//Валютные курсы
//========================================================================================
$date = date("Y-m-d H:i:s" );
$sql = "INSERT INTO `".USAM_TABLE_CURRENCY_RATES."` (`basic_currency`,`currency`,`rate`,`date_update`) VALUES ('%s','%s','%f','%s') ON DUPLICATE KEY UPDATE `rate`='%f', `date_update`='%s'";					
$insert = $wpdb->query( $wpdb->prepare($sql, 'RUB', 'USD', 62, $date, 62, $date ) );	
$insert = $wpdb->query( $wpdb->prepare($sql, 'RUB', 'EUR', 80, $date, 80, $date ) );	

//Задания
//========================================================================================

$tasks = array( 
	array( 
		'reminder'    => 0, 
		'importance'  => 0, 
		'calendar'    => 1, 
		'status'      => 1, 
		'title'       => 'Позвонить в компанию СБЕРБАНК', 
		'description' => 'Позвонить в компанию СБЕРБАНК и заказать подключение платежного шлюза к моему сайту', 		
		'user_id'     => $user_ID,
		'manager_id'  => 0, 
		'color'       => '', 			
		'type'        => 'call', 	
		'start'       => date("Y-m-d H:i:s", mktime(date("H"), 0, 0, date("m"), date("d")-2, date("Y"))), 
		'end'         => date("Y-m-d H:i:s", mktime(date("H"), 30, 0, date("m"), date("d")-1, date("Y"))), 
		'date_time'   => '',
	),	
	array( 
		'reminder'    => 0, 
		'importance'  => 1, 
		'calendar'    => 1, 
		'status'      => 1, 
		'title'       => 'Акция Весенняя распродажа', 
		'description' => 'Не забыть сделать акцию', 		
		'user_id'     => $user_ID,
		'manager_id'  => 0, 
		'color'       => '', 			
		'type'        => 'event', 	
		'start'       => date("Y-m-d H:i:s", mktime(date("H"), 0, 0, date("m"), date("d"), date("Y"))), 
		'end'         => date("Y-m-d H:i:s", mktime(date("H"), 30, 0, date("m"), date("d"), date("Y"))), 
		'date_time'   => '',
	),	
	array( 
		'reminder'    => 0, 
		'importance'  => 1, 
		'calendar'    => 1, 
		'status'      => 1, 
		'title'       => 'Составить отчет на месяц', 
		'description' => 'Составить отчет по продажам на месяц', 		
		'user_id'     => $user_ID,
		'manager_id'  => 0, 
		'color'       => '', 			
		'type'        => 'task', 	
		'start'       => date("Y-m-d H:i:s", mktime(11, 0, 0, date("m"), 30, date("Y"))), 
		'end'         => date("Y-m-d H:i:s", mktime(18, 0, 0, date("m"), 30, date("Y"))), 
		'date_time'   => '',
	),	
	array( 
		'reminder'    => 0, 
		'importance'  => 1, 
		'calendar'    => 1, 
		'status'      => 1, 
		'title'       => 'Встретится с руководителем компании Газпром', 
		'description' => 'Возможно оптовая поставка. Выясню на встрече.', 		
		'user_id'     => $user_ID,
		'manager_id'  => 0, 
		'color'       => '', 			
		'type'        => 'meeting', 	
		'start'       => date("Y-m-d H:i:s", mktime(11, 0, 0, date("m"), date("d")+2, date("Y"))), 
		'end'         => date("Y-m-d H:i:s", mktime(11, 30, 0, date("m"), date("d")+2, date("Y"))), 
		'date_time'   => '',
	),	
);
foreach ( $tasks as $task ) 
{
	$document['date_insert'] = date( "Y-m-d H:i:s");
	$insert = $wpdb->insert( USAM_TABLE_EVENTS, $task );
}	
	
//Коммерческие предложения
//========================================================================================

$documents = array( 
	array( 'name' => 'предложение для СБЕРБАНК', 'manager_id' => $user_ID, 'number' => 1, 'bank_account_id' => 1, 'customer_id' => 1, 'customer_type' => 'company', 'type_price' => '', 'description' => '', 'notes' => '', 'status' => 1, 'closedate' => date( "Y-m-d H:i:s"), 'type' => 'suggestion' ),		
	array( 'name' => 'Счет для СБЕРБАНК', 'manager_id' => $user_ID, 'number' => 1, 'bank_account_id' => 1, 'customer_id' => 1, 'customer_type' => 'company', 'type_price' => '', 'description' => '', 'notes' => '', 'status' => 1, 'closedate' => date( "Y-m-d H:i:s"), 'type' => 'invoice' ),
	array( 'name' => 'Договор с СБЕРБАНК', 'manager_id' => $user_ID, 'number' => 1, 'bank_account_id' => 1, 'customer_id' => 1, 'customer_type' => 'company', 'type_price' => '', 'description' => '', 'notes' => '', 'status' => 1, 'closedate' => date( "Y-m-d H:i:s"), 'type' => 'contract' ),
);
foreach ( $documents as $document ) 
{
	$document['date_insert'] = date( "Y-m-d H:i:s");
	$insert = $wpdb->insert( USAM_TABLE_DOCUMENTS, $document );
}	
	
//Компании
//========================================================================================

$companies = array( 
	array( 'name' => '', 'description' => '', 'manager_id' => $user_ID, 'logo' => 0, 'open' => 1, 'type' => '', 'group' => '', 'industry' => 'customer', 'employees' => '500', 'revenue' => '', 'name' => 'Сбербанк', 'date_insert' => date( "Y-m-d H:i:s") )
);
foreach ( $companies as $company ) 
	$insert = $wpdb->insert( USAM_TABLE_COMPANY, $company );	
	
//Контакты
//========================================================================================

$users = get_users( );
foreach ( $users as $user ) 
{	
	if ( empty($user->last_name) )
		$lastname = $user->display_name;
	else
		$lastname = $user->last_name;
	$contact = array( 'contact_source' => '', 'manager_id' => $user_ID, 'user_id' => $user->ID, 'open' => 1, 'sex' => '', 'lastname' => $lastname, 'firstname' => $user->first_name, 'patronymic' => '', 'birthday' => '', 'company_id' => 0, 'post' => '', 'address' => '', 'address2' => '', 'postal_code' => '', 'about_contact' => '', 'foto' => 0, 'location_id' => 1, 'date_insert' => date( "Y-m-d H:i:s") );
	$insert = $wpdb->insert( USAM_TABLE_CONTACTS, $contact );	
	
	$communication = array( 'value' => $user->user_email, 'value_type' => 'private', 'type' => 'email', 'customer_type' => 'contact', 'contact_id' => $wpdb->insert_id );	
	$communication_id = usam_insert_communication( $communication );	
}

//SEO
//========================================================================================
$keywords = array( 'интернет-магазин' );
foreach ( $keywords as $keyword ) 
{		
	$insert = $wpdb->insert( USAM_TABLE_KEYWORDS, array( 'keyword' => $keyword ) );	
}


$static = array( 
	array( 'date_insert' => date( "Y-m-d", strtotime('-120 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 98 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-90 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 76 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-65 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 54 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-40 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 45 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-35 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 56 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-22 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 34 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-21 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 87 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-11 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 23 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-10 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 4 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-9 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 59 ),
	array( 'date_insert' => date( "Y-m-d" ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'y', 'site_id' => 0, 'number' => 50 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-30 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'g', 'site_id' => 0, 'number' => 56 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-3 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'g', 'site_id' => 0, 'number' => 56 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-2 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'g', 'site_id' => 0, 'number' => 80 ),
	array( 'date_insert' => date( "Y-m-d", strtotime('-1 days') ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'g', 'site_id' => 0, 'number' => 99 ),
	array( 'date_insert' => date( "Y-m-d" ), 'keyword_id' => 1, 'location_id' => 981, 'url' => get_bloginfo('url'), 'search_engine' => 'g', 'site_id' => 0, 'number' => 43 ),	
);
foreach ( $static as $stat ) 
{		
	$insert = $wpdb->insert( USAM_TABLE_STATISTICS_KEYWORDS, $stat );	
}

?>