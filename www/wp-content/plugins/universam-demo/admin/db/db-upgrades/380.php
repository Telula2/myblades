<?php
global $wpdb;
$wpdb->query( "ALTER TABLE `".USAM_TABLE_PRODUCTS_BASKET."` DROP COLUMN `weight`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_DOCUMENTS."` DROP COLUMN `file_name`" );


$wpdb->query( "alter table `".USAM_TABLE_DOCUMENTS."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_CONTACTS."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_COMPANY."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_CHAT_DIALOGS."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_FEEDBACK."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_PRICE_COMPARISON."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_ORDERS."` CHANGE COLUMN `manager` `manager_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 



delete_option('usam_waiting_sending_subject');
delete_option('usam_waiting_sending_message');

USAM_Install::create_or_update_tables();

?>