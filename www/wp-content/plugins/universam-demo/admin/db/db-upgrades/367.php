<?php
global $wpdb;
$wpdb->query( "alter table `".USAM_TABLE_ORDERS."` drop index `user_ID`" ); 

$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam__edit_last'" );
$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam__edit_lock'" );
$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam__thumbnail_id'" );
$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam__yoast_wpseo_primary_usam-cat_rules_purchase'" );
$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam_custom_thumb'" );
$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam_selected_image_size'" );

$args = array (										
	'fields' => 'ids',			
	'post_status' => 'publish',
	'update_post_meta_cache' => true,
	'update_post_term_cache' => false,
	'cache_results' => true,
);
$products_ids = usam_get_products( $args );

$default_data = array( 'components' => array(),
											'bonuses' => array ( 'value' => 0, 'type' => 'p' ),	
											'webspy_link' => '',										
											'external' => array( 'type' => 'resale', 'title' => '', 'target' => '' ),	
											'dimensions' => array('height' => 0,'width'=> 0,'length'=> 0),
											'unit_measure' => '', //Коэффициент единицы измерения
											'unit' => 1,		 //единица измерения								
											'comment' => '', // Использовать комментарии										
											'image_swap' => 0,									
											'vkontakte' => array(),
											);
$products = usam_get_products( $args );	
if ( !empty($products) )
{					
	foreach ( $products as $product_id )
	{		
		$product_metadata = usam_get_product_meta( $product_id, 'product_metadata' );
		if ( !empty($product_metadata['weight']) )
			usam_update_product_meta( $product_id, 'weight', $product_metadata['weight'] );
		foreach ( $product_metadata as $key => $value )
		{
			if ( !isset($default_data[$key]) )
				unset($product_metadata[$key]);
		}			
		usam_update_product_meta( $product_id, 'product_metadata', $product_metadata );		
	}
}

delete_option('shippingsameasbilling');
delete_option('usam_shippingsameasbilling');
?>