<?php
global $wpdb;

$wpdb->query( "RENAME TABLE `wp_usam_tasks` TO `wp_usam_events`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_ORDERS."` DROP COLUMN `contact_status`" );

USAM_Install::create_or_update_tables();
		
$events = usam_get_tasks( ); 
foreach ( $events as $event ) 
{
	usam_set_event_object( array( 'event_id' => $event->id, 'object_id' => $event->object_id, 'object_type' => $event->object_type ) ); 
}


$wpdb->query( "ALTER TABLE `".USAM_TABLE_EVENTS."` DROP COLUMN `object_id`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_EVENTS."` DROP COLUMN `object_type`" );

$wpdb->query("UPDATE ".USAM_TABLE_DOCUMENTS." SET type='suggestion'");


$args = array( 'fields' => 'ids', 'numberposts' => -1, 'post_status' => null, 'date_query' => array(
		'after' => '5 months ago',
	), );		
$product_ids = usam_get_products( $args );
foreach ( $product_ids as $product_id ) 
{
	$post_thumbnail_id = get_post_thumbnail_id( $product_id );
	wp_update_post( array( 'ID' => $post_thumbnail_id, 'post_parent' => $product_id ) );		
}


$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_parent' => 0, 'post_status' => null, 'date_query' => array('after' => '5 months ago' ), );		
$post_thumbnail = get_posts( $args );
foreach ( $post_thumbnail as $thumbnail ) 
{		
	preg_match('/\[(.+)\]/', $thumbnail->post_title, $m);	
	if ( !empty($m) )
	{
		$product_id = usam_get_product_id_by_sku( $m[1] );	
		if ( !empty($product_id) )
			wp_update_post( array( 'ID' => $thumbnail->ID, 'post_parent' => $product_id ) );		
		else
		{
			$post_title = trim(str_replace( $m[0], "", $thumbnail->post_title ));
			$product_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '$post_title' AND post_type = 'usam-product'" );			
			
			if ( !empty($product_id) )
				wp_update_post( array( 'ID' => $thumbnail->ID, 'post_parent' => $product_id ) );		
		}		
	}	
}
?>