<?php
global $wpdb;

$wpdb->query( "RENAME TABLE `wp_usam_commercial_offers` TO `wp_usam_documents_crm`" );
		
$wpdb->query( "ALTER TABLE `".USAM_TABLE_SLIDES."` DROP COLUMN `setting`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_STORAGE_LIST."` DROP COLUMN `city`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_CONTACTS."` DROP COLUMN `city`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_CONTACTS."` DROP COLUMN `company_name`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_CONTACTS."` DROP COLUMN `status`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_ORDERS."` DROP COLUMN `comment_buyer`" );



$wpdb->query( "ALTER TABLE `".USAM_TABLE_DOCUMENTS."` DROP COLUMN `pay_up`" );

$wpdb->query( "ALTER TABLE `".USAM_TABLE_DOCUMENTS."` DROP COLUMN `contact_id`" );
$wpdb->query( "alter table `".USAM_TABLE_PRODUCTS_DOCUMENT."` drop index `document_type`" ); 
$wpdb->query( "ALTER TABLE `".USAM_TABLE_PRODUCTS_DOCUMENT."` DROP COLUMN `document_type`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_CUSTOMER_DOCUMENT."` DROP COLUMN `document_type`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_CUSTOMER_DOCUMENT."` DROP COLUMN `customer_type`" );


$wpdb->query("UPDATE ".USAM_TABLE_EVENTS." SET type='work' WHERE `type`='affair'");
$wpdb->query( "DROP TABLE `wp_usam_invoice`" );


$crm_company_fields = array( 
//Фактический адрес 
	array( 'name' => __('Местоположение','usam'), 'unique_name' => 'contactlocation','type' => 'location', 'group' => 'actual_address',  'sort' => 80,   ),	
	array( 'name' => __('Адрес','usam'), 'unique_name' => 'contactaddress','type' => 'address', 'group' => 'actual_address',  'sort' => 90,   ),
	array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'contactpostcode','type' => 'text', 'group' => 'actual_address',  'sort' => 100,   ),
	array( 'name' => __('Офис','usam'), 'unique_name' => 'contactoffice','type' => 'text', 'group' => 'actual_address',  'sort' => 100,   ),	
//Юридический адрес 
	array( 'name' => __('Местоположение','usam'), 'unique_name' => 'legallocation','type' => 'location', 'group' => 'legal_address',  'sort' => 80,   ),	
	array( 'name' => __('Адрес','usam'), 'unique_name' => 'legaladdress','type' => 'address', 'group' => 'legal_address',  'sort' => 90,   ),
	array( 'name' => __('Почтовый индекс','usam'), 'unique_name' => 'legalpostcode','type' => 'text', 'group' => 'legal_address',  'sort' => 100,   ),
	array( 'name' => __('Офис','usam'), 'unique_name' => 'legaloffice','type' => 'text', 'group' => 'legal_address',  'sort' => 100,   ),	
//Реквизиты компании
	array( 'name' => __('Сокращенное наименование компании','usam'), 'unique_name' => 'company_name', 'type' => 'text', 'group' => 'requisites',  'sort' => 200,   ),
	array( 'name' => __('Полное наименование компании','usam'), 'unique_name' => 'full_company_name', 'type' => 'text', 'group' => 'requisites',  'sort' => 200,   ),
	array( 'name' => __('ИНН','usam'), 'unique_name' => 'inn', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('КПП','usam'), 'unique_name' => 'ppc','type' => 'text', 'group' => 'requisites',  'sort' => 230,   ),
	array( 'name' => __('ОГРН','usam'), 'unique_name' => 'ogrn', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('Дата государственной регистрации','usam'), 'unique_name' => 'date_registration', 'type' => 'text','group' => 'requisites',  'sort' => 220,   ),
	array( 'name' => __('ОКПО','usam'), 'unique_name' => 'okpo','type' => 'text', 'group' => 'requisites',  'sort' => 240,   ),
	array( 'name' => __('ОКТМО','usam'), 'unique_name' => 'oktmo', 'type' => 'text', 'group' => 'requisites',  'sort' => 250,   ),
	array( 'name' => __('Ген. директор','usam'), 'unique_name' => 'gm', 'type' => 'text', 'group' => 'requisites',  'sort' => 250,   ),
	array( 'name' => __('Гл. бухгалтер','usam'), 'unique_name' => 'accountant', 'type' => 'text', 'group' => 'requisites',  'sort' => 260,   ),	
);
update_option( 'usam_crm_company_fields', $crm_company_fields );	



delete_option('usam_subscribe_to_newsletter_subject');
delete_option('usam_subscribe_to_newsletter_message');
delete_option('usam_subscribe_to_newsletter_sms_message');
?>