<?php
global $wpdb;
			
$wpdb->query( "RENAME TABLE wp_usam_email_user_stat TO ".USAM_TABLE_NEWSLETTER_USER_STAT );
$wpdb->query( "RENAME TABLE wp_usam_triggered_email TO ".USAM_TABLE_NEWSLETTER_TRIGGERED );
$wpdb->query( "RENAME TABLE wp_usam_templates_mailings TO ".USAM_TABLE_NEWSLETTER_TEMPLATES );
$wpdb->query( "RENAME TABLE wp_usam_email_lists TO ".USAM_TABLE_NEWSLETTER_LISTS );

$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` CHANGE COLUMN `email_id` `newsletter_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 
$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_LISTS."` CHANGE COLUMN `email_id` `newsletter_id` bigint(20) unsigned NOT NULL DEFAULT '0'" ); 

$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` DROP COLUMN `from_email`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` DROP COLUMN `from_name`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` DROP COLUMN `replyto_email`" );
$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` DROP COLUMN `replyto_name`" );
$wpdb->query( "DELETE FROM `".USAM_TABLE_NEWSLETTER_TEMPLATES."` WHERE email_id=0" );	

$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_TRIGGERED."` CHANGE COLUMN `email_id` `newsletter_id` bigint(20) unsigned NOT NULL DEFAULT '0'" );
$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_TEMPLATES."` CHANGE COLUMN `email_id` `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT" );

	
$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` DROP COLUMN `styles`" );

delete_option('usam_needs_update');
delete_option('usam_hide_update');
delete_option('usam_page_transaction-results');


$wpdb->query("UPDATE `".USAM_TABLE_PAYMENT_HISTORY."` SET external_document=transactid");
$wpdb->query( "DELETE FROM wp_options WHERE option_name LIKE 'usam_term_link_%'" );	


$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` ADD `t` bigint(20)" );
$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` ADD `n` bigint(20)" );
$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_USER_STAT."` SET t=id_communication");
$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_USER_STAT."` SET n=newsletter_id");

$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_USER_STAT."` DROP COLUMN `newsletter_id`, DROP COLUMN `id_communication`" );

$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` ADD `id_communication` bigint(20) unsigned NOT NULL DEFAULT '0'" );
$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` ADD `newsletter_id` bigint(20) unsigned NOT NULL DEFAULT '0'" );


$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_USER_STAT."` SET id_communication=t");
$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_USER_STAT."` SET newsletter_id=n");

$wpdb->query( "ALTER TABLE `".USAM_TABLE_NEWSLETTER_USER_STAT."` DROP COLUMN `t`, DROP COLUMN `n`" );


$wpdb->query( "alter table `".USAM_TABLE_NEWSLETTER_USER_STAT."` ADD `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST" );
 	
USAM_Install::create_or_update_tables();

$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` SET class=type");
$wpdb->query("UPDATE `".USAM_TABLE_NEWSLETTER_TEMPLATES."` SET type='mail'");


update_option('usam_list_of_subscribers', array( 'value' => array( array('title' => 'Рассылка', 'date_insert' => date("Y-m-d H:i:s", current_time('timestamp')), 'id' => 1 ) ) , 'autoload' => false ));
?>