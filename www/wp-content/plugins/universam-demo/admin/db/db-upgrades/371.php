<?php
global $wpdb;

$args = array (										
	'fields' => 'ids',			
	'post_status' => 'publish',
	'update_post_meta_cache' => true,
	'update_post_term_cache' => false,
	'cache_results' => true,
);
$products = usam_get_products( $args );	
if ( !empty($products) )
{					
	foreach ( $products as $product_id )
	{		
		$product_metadata = usam_get_product_meta( $product_id, 'product_metadata' );
		if ( !empty($product_metadata['rating_count']) )
		{	
			usam_update_product_meta( $product_id, 'rating_count', $product_metadata['rating_count'] );	
		}
	}
}
delete_option('usam_vk_place_publish');
delete_option('usam_vk_products_publish');
delete_option('usam_vk_group');


$wpdb->query( "DELETE FROM `$wpdb->postmeta` WHERE meta_key LIKE '_usam_options_price'" );


$option = get_option('usam_type_prices');
$prices = maybe_unserialize( $option );
foreach( $prices as $setting_price )
{						
	$new = $setting_price;
	$new['sort'] = 100;
	$new['base_type'] = isset($setting_price['base_type']) && $setting_price['base_type'] == '-1'?0:$setting_price['base_type'];	
	usam_edit_data( $new, $new['id'], 'usam_type_prices' );	
}	
?>