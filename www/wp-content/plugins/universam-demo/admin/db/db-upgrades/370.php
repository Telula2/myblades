<?php
global $wpdb;
$tracking_id = get_option('usam_ga_tracking_id');
$ecommerce = get_option('usam_google_analytics_ecommerce_active');

$google = array('analytics_id' => $tracking_id,'analytics_ecommerce' => $ecommerce);

update_option('usam_google', $google);

delete_option('usam_ga_tracking_id');
delete_option('usam_google_analytics_ecommerce_active');
delete_option('usam_seo_locations');
delete_option('usam_search_focus_enable');

$sites = get_option('usam_settings_sites_suppliers');
if ( !empty($sites) )
{
	delete_option('usam_settings_sites_suppliers');
	foreach ( $sites as $site ) 
	{
		 usam_add_data( $site, 'usam_settings_sites_suppliers' );
	}
}


include( USAM_FILE_PATH . '/admin/db/db-install/system_default_data.php' );	
new USAM_Load_System_Default_Data( array('tables') );	

global $wp_roles;
$delete_capabilities = array( 
	'order_page'         => array( 'shop_manager', 'administrator' ),
	
	'edit_storage'        => array( 'shop_manager', 'administrator' ),
	'edit_price'          => array( 'shop_manager', 'administrator' ),
	'exchange'            => array( 'shop_manager', 'administrator', ),
	'reports'             => array( 'shop_manager', 'administrator' ),
	
	'marketing'           => array( 'shop_seo', 'shop_manager', 'administrator' ),
);
foreach ( $delete_capabilities as $capability_id => $roles ) 
{	
	foreach ( $wp_roles->role_objects as $wp_role ) 
	{							
		$wp_role->remove_cap( $capability_id );
	}
}

$capabilities = array( 
	'store_section'       => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),
	'view_orders'         => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),		
	'edit_order'          => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),		
	'edit_payment'        => array( 'shop_manager', 'administrator', 'shop_crm' ),
	'edit_shipped'        => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),				
	'view_feedback'       => array( 'shop_manager', 'administrator', 'shop_crm' ),	
	'view_crm'            => array( 'shop_manager', 'administrator', 'shop_crm' ),				
	'manage_prices'       => array( 'shop_manager', 'administrator' ),				
	'view_storage'        => array( 'shop_manager', 'administrator', 'shop_crm' ),		
	'view_customers'      => array( 'shop_manager', 'administrator', 'shop_crm' ),		
	'view_interface'      => array( 'shop_manager', 'administrator' ),
	'view_exchange'       => array( 'shop_manager', 'administrator' ),
	'view_partners'       => array( 'shop_manager', 'administrator', 'shop_crm' ),
	'manage_discounts'    => array( 'shop_manager', 'administrator' ),				
	'view_reports'        => array( 'shop_manager', 'administrator', 'shop_crm' ),
	
	'marketing_section'    => array( 'shop_manager', 'administrator' ),	
	'view_social_networks' => array( 'shop_manager', 'administrator' ),	
	'view_newsletter'      => array( 'shop_manager', 'administrator' ),						
	'view_marketing'       => array( 'shop_manager', 'administrator' ),
	'view_seo'             => array( 'shop_seo', 'shop_manager', 'administrator' ),
	
	'shop_tools'           => array( 'administrator' ),
	'edit_shop_settings'   => array( 'administrator' ),
);			
			
foreach ( $capabilities as $capability_id => $roles ) 
{	
	foreach ( $wp_roles->role_objects as $wp_role ) 
	{				
		if ( in_array($wp_role->name, $roles) )
		{								
			if ( !$wp_role->has_cap( $capability_id ) )
			{						
				$wp_role->add_cap( $capability_id );						
			}	
		}		
	}
}									

add_role( 'shop_seo', __( 'SEO специалист', 'usam' ), array(
	'store_section'          => false,
	'view_orders'            => false,		
	'edit_order'             => false,		
	'edit_payment'           => false,
	'edit_shipped'           => false,	
	
	'view_seo'               => true,
	'edit_shop_settings'     => false,			
	
	'level_9'                => false,			
	'level_8'                => false,
	'level_7'                => false, // ниже 7 не загружаются js и css
	'level_6'                => true,
	'level_5'                => true,
	'level_4'                => true,
	'level_3'                => true,
	'level_2'                => true,
	'level_1'                => true,
	'level_0'                => true,
	'read'                   => true,
	'read_private_pages'     => true,
	'read_private_posts'     => true,
	'edit_users'             => true,		
	'edit_posts'             => true,
	'edit_pages'             => true,
	'edit_published_posts'   => true,
	'edit_published_pages'   => true,
	'edit_private_pages'     => true,
	'edit_private_posts'     => true,
	'edit_others_posts'      => true,
	'edit_others_pages'      => true,
	'publish_posts'          => true,
	'publish_pages'          => true,
	'delete_posts'           => false,
	'delete_pages'           => false,
	'delete_private_pages'   => false,
	'delete_private_posts'   => false,
	'delete_published_pages' => false,
	'delete_published_posts' => false,
	'delete_others_posts'    => false,
	'delete_others_pages'    => false,
	'manage_categories'      => true,
	'manage_links'           => true,
	'moderate_comments'      => true,
	'unfiltered_html'        => true,
	'upload_files'           => true,
	'export'                 => true,
	'import'                 => true,
	'list_users'             => false			
) );
?>