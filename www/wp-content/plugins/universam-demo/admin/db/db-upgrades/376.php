<?php
require_once( USAM_FILE_PATH .'/includes/feedback/email_query.class.php'  );

$system_folders = array( 'inbox' => __('Входящие', 'usam' ), 'drafts' => __('Черновики', 'usam' ), 'sent' => __('Отправленные', 'usam' ), 'spam' => __('Спам', 'usam' ), 'deleted' => __('Удаленные', 'usam' ), 'outbox' => __('Исходящие', 'usam' ) );

$email_folders = usam_get_email_folders( );	
$folders = array();
foreach ( $email_folders as $folder ) 
{
	$folders[$folder->mailbox_id][] = $folder->slug;								
}
			
$mailboxes = usam_get_mailboxes( );					
foreach ( $mailboxes as $mailbox )
{
	foreach ( $system_folders as $slug => $name )
	{
		if ( empty($folders[$mailbox->id]) || !in_array($slug, $folders[$mailbox->id]) )
		{
			$emails = usam_get_emails( array( 'mailbox' => $mailbox->id, 'folder' => $slug ) );	
			$not_read = 0;
			foreach ( $emails as $email )
			{
				if ( $email->read == 0 )
					$not_read++;
			}	
			$insert = array('name' => $name, 'mailbox_id' => $mailbox->id, 'slug' => $slug, 'not_read' => $not_read, 'count' => count($emails));			
			$id = usam_insert_email_folder( $insert );	
		}
	}
}
?>