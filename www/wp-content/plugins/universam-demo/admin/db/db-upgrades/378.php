<?php

global $wp_roles;		
		
		$capabilities = array( 
			'store_section'       => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),
			'view_orders'         => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),		
			'edit_order'          => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),		
			'edit_payment'        => array( 'shop_manager', 'administrator', 'shop_crm' ),
			'edit_shipped'        => array( 'shop_manager', 'courier', 'administrator', 'shop_crm' ),				
			'view_feedback'       => array( 'shop_manager', 'administrator', 'shop_crm' ),	
			'view_crm'            => array( 'shop_manager', 'administrator', 'shop_crm' ),				
			'manage_prices'       => array( 'shop_manager', 'administrator' ),				
			'view_storage'        => array( 'shop_manager', 'administrator', 'shop_crm' ),		
			'view_customers'      => array( 'shop_manager', 'administrator', 'shop_crm' ),		
			'view_interface'      => array( 'shop_manager', 'administrator' ),
			'view_exchange'       => array( 'shop_manager', 'administrator' ),
			'view_partners'       => array( 'shop_manager', 'administrator', 'shop_crm' ),
			'manage_discounts'    => array( 'shop_manager', 'administrator' ),				
			'view_reports'        => array( 'shop_manager', 'administrator', 'shop_crm' ),
			
			'marketing_section'    => array( 'shop_manager', 'administrator' ),	
			'view_social_networks' => array( 'shop_manager', 'administrator' ),	
			'view_newsletter'      => array( 'shop_manager', 'administrator' ),						
			'view_marketing'       => array( 'shop_manager', 'administrator' ),
			'view_seo'             => array( 'shop_seo', 'shop_manager', 'administrator' ),
			
			'shop_tools'           => array( 'administrator' ),
			'edit_shop_settings'   => array( 'administrator' ),
		);	
		
		foreach ( $capabilities as $capability_id => $roles ) 
		{	
			foreach ( $wp_roles->role_objects as $wp_role ) 
			{				
				if ( in_array($wp_role->name, $roles) )
				{								
					if ( !$wp_role->has_cap( $capability_id ) )
					{						
						$wp_role->add_cap( $capability_id );						
					}	
				}
				else
					$wp_role->remove_cap( $capability_id );
			}
		}			

?>