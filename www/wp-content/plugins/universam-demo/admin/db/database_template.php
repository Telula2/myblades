<?php
/**
 * Шаблон базы данных магазина
 * Это шаблон базы данных Universam. Это многомерный ассоциативный массив используется для создания и обновления таблиц базы данных.
 */ 
$table_name = USAM_TABLE_ADVERTISING_BANNER;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']             = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['type']             = "varchar(5) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['weight']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['contact_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['date_insert']      = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified']    = "datetime NOT NULL";
$database_template[$table_name]['columns']['description']      = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_PRODUCTS_DOCUMENT; 
$database_template[$table_name]['columns']['id']            = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['document_id']   = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['product_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']          = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['price']         = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['old_price']     = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['tax']           = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['quantity']      = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_insert']   = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']       = "PRIMARY KEY ( `id` )";
$database_template[$table_name]['indexes']['document_id']   = " KEY `document_id` ( `document_id` )";
 
$table_name = USAM_TABLE_CUSTOMER_DOCUMENT;
$database_template[$table_name]['columns']['document_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['customer_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`document_id`,`customer_id`)";

$table_name = USAM_TABLE_DOCUMENT_META;
$database_template[$table_name]['columns']['meta_id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['document_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['meta_key']         = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['meta_value']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['document_id']      = " KEY `document_id` ( `document_id` )";
$database_template[$table_name]['indexes']['meta_key']         = " KEY `meta_key` ( `meta_key` )";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `meta_id` )";

$table_name = USAM_TABLE_DOCUMENTS;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['number']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['customer_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['customer_type']    = "varchar(7) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['name']             = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['type_price']       = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['totalprice']       = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['shipping']         = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']           = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']      = "datetime NOT NULL";
$database_template[$table_name]['columns']['closedate']        = "datetime";//срок оплаты
$database_template[$table_name]['columns']['bank_account_id']  = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['manager_id']       = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['description']      = "text NULL";
$database_template[$table_name]['columns']['conditions']       = "text NULL";
$database_template[$table_name]['columns']['notes']   		   = "text NULL";
$database_template[$table_name]['columns']['type']       	   = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";
 
$table_name = USAM_TABLE_TAXES;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']             = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']      = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['value']            = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_update']      = "datetime NOT NULL";
$database_template[$table_name]['columns']['is_in_price']      = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['active']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type_payer']       = "int(8) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['setting']          = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']             = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";
 
$table_name = USAM_TABLE_PAYMENT_GATEWAY;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']             = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']      = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['active']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']             = "char(1) NOT NULL DEFAULT '' "; // эквайринговая операция
$database_template[$table_name]['columns']['debug']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['cashbox']          = "int(8) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['bank_account_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['handler']          = "varchar(100) NOT NULL DEFAULT '' "; // Обработчик
$database_template[$table_name]['columns']['img']              = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['setting']          = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']             = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['previous_names']              = USAM_META_PREFIX."payment_gateway";
 
$table_name = USAM_TABLE_DELIVERY_SERVICE;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']             = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']      = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['price']            = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['active']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['period_from']      = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['period_to']        = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['period_type']      = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['handler']          = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['img']              = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['setting']          = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']             = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_NEWSLETTER_USER_STAT;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['newsletter_id']     = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['id_communication']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['sent_at']           = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['opened_at']         = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['clicked']           = "smallint unsigned NOT NULL DEFAULT '0' ";// Количество нажатий
$database_template[$table_name]['columns']['unsub']             = "smallint unsigned NOT NULL DEFAULT '0' ";// отписался
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['newsletter_id']     = " KEY `newsletter_id` ( `newsletter_id` )";
$database_template[$table_name]['indexes']['id_communication']  = "KEY `id_communication` ( `id_communication` )";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_NEWSLETTER_TRIGGERED;
$database_template[$table_name]['columns']['id']             = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['newsletter_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['date_modified']  = "datetime NOT NULL";
$database_template[$table_name]['columns']['status']         = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['event_start']    = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['condition']      = "text NULL";
$database_template[$table_name]['columns']['data']           = "text NULL"; // Данные для работы
$database_template[$table_name]['indexes']['PRIMARY']        = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['newsletter_id']  = " KEY `newsletter_id` ( `newsletter_id` )";

$table_name = USAM_TABLE_NEWSLETTER_TEMPLATES;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['mailbox_id']        = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['subject']        	= "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['body']           	= "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['template']          = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']       = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['date_update']       = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['sent_at']           = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']              = "varchar(4) NOT NULL DEFAULT 'mail'";
$database_template[$table_name]['columns']['class']             = "varchar(1) NOT NULL DEFAULT 'S' ";
$database_template[$table_name]['columns']['number_sent']       = "int(10) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['number_opened']     = "int(10) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['number_clicked']    = "int(10) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['number_unsub']      = "int(10) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['number_bounce']     = "int(10) NOT NULL DEFAULT '0' "; // отказы
$database_template[$table_name]['columns']['number_forward']    = "int(10) NOT NULL DEFAULT '0' "; // в ожидании
$database_template[$table_name]['columns']['data']           	= "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['type']              = "KEY `type` ( `type` )";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
 
$table_name = USAM_TABLE_NEWSLETTER_LISTS;
$database_template[$table_name]['columns']['newsletter_id']     = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['list']              = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`newsletter_id`,`list`)";

$table_name = USAM_TABLE_SUBSCRIBER_LISTS;
$database_template[$table_name]['columns']['id_communication']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['list']              = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`id_communication`,`list`)";

$table_name = USAM_TABLE_MEANS_COMMUNICATION;
$database_template[$table_name]['columns']['id']                 = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['contact_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['type']               = "varchar(6) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['value_type']         = "varchar(8) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['value']              = "varchar(60) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['customer_type']      = "varchar(7) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']            = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['contact_id']         = " KEY `contact_id` ( `contact_id` )";
$database_template[$table_name]['indexes']['customer_type']      = " KEY `customer_type` ( `customer_type` )";
$database_template[$table_name]['indexes']['type']               = " KEY `type` ( `type` )";
 
$table_name = USAM_TABLE_CONTACTS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['foto']              = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['sex']               = "char(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['lastname']          = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['firstname']         = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['patronymic']        = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['birthday']          = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['company_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['post']              = "varchar(20) NOT NULL DEFAULT '' "; //Должность
$database_template[$table_name]['columns']['location_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['address']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['address2']          = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['postal_code']       = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['manager_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['open']              = "tinyint NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['about_contact']     = "text";
$database_template[$table_name]['columns']['contact_source']    = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_COMPANY;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']              = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['logo']              = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['manager_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";	
$database_template[$table_name]['columns']['open']              = "tinyint NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['group']             = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']              = "varchar(20) NOT NULL DEFAULT '' "; //Тип компании (своя или сторонняя )
$database_template[$table_name]['columns']['industry']          = "varchar(20) NOT NULL DEFAULT '' "; //Сфера деятельности
$database_template[$table_name]['columns']['employees']         = "varchar(20) NOT NULL DEFAULT '' "; //Кол-во сотрудников
$database_template[$table_name]['columns']['revenue']           = "int(11) NOT NULL DEFAULT '0'"; //Годовой оборот
$database_template[$table_name]['columns']['description']       = "text NULL";
$database_template[$table_name]['columns']['date_insert']        = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_COMPANY_META;
$database_template[$table_name]['columns']['unique_name'] = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['company_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['value']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['unique_key']  = "UNIQUE KEY `unique_key` (`company_id`,`unique_name`)";

$table_name = USAM_TABLE_COMPANY_ACC_NUMBER;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['company_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']             = "varchar(255) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['bic']              = "varchar(9) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['number']           = "varchar(50) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['bank_ca']          = "varchar(50) NOT NULL DEFAULT '' "; //кор счет
$database_template[$table_name]['columns']['currency']         = "varchar(3) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['address']          = "varchar(255) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['swift']            = "varchar(11) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['note']             = "varchar(255) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";	
 
$table_name = USAM_TABLE_EVENTS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['manager_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['calendar']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['title']             = "varchar(100) DEFAULT NULL";
$database_template[$table_name]['columns']['type']              = "varchar(20) DEFAULT NULL";
$database_template[$table_name]['columns']['description']       = "text";
$database_template[$table_name]['columns']['start']             = "datetime";
$database_template[$table_name]['columns']['end']               = "datetime";
$database_template[$table_name]['columns']['date_time']         = "datetime";// Напомнить дата
$database_template[$table_name]['columns']['reminder']          = "tinyint NOT NULL DEFAULT '0' "; // Напомнить
$database_template[$table_name]['columns']['importance']        = "tinyint NOT NULL DEFAULT '0' "; // Важное
$database_template[$table_name]['columns']['color']             = "varchar(7) DEFAULT NULL";
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '1' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_EVENT_META;
$database_template[$table_name]['columns']['meta_id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['event_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['meta_key']         = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['meta_value']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['event_id']         = " KEY `event_id` ( `event_id` )";
$database_template[$table_name]['indexes']['meta_key']         = " KEY `meta_key` ( `meta_key` )";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `meta_id` )";

$table_name = USAM_TABLE_EVENT_ACTION_LIST;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['event_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['name']              = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['made']              = "tinyint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['sort']              = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_EVENT_USERS;
$database_template[$table_name]['columns']['event_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`event_id`,`user_id`)";

$table_name = USAM_TABLE_EVENT_RELATIONSHIPS;
$database_template[$table_name]['columns']['event_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['object_type']       = "varchar(20) DEFAULT NULL";
$database_template[$table_name]['columns']['object_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`event_id`,`object_type`,`object_id`)";

$table_name = USAM_TABLE_COMMENTS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['object_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['object_type']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['message']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['object_id']         = " KEY `object_id` ( `object_id` )";
$database_template[$table_name]['indexes']['object_type']       = " KEY `object_type` ( `object_type` )";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_COMMENT_META;
$database_template[$table_name]['columns']['meta_id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['object_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['meta_key']         = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['meta_value']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['object_id']        = " KEY `object_id` ( `object_id` )";
$database_template[$table_name]['indexes']['meta_key']         = " KEY `meta_key` ( `meta_key` )";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `meta_id` )";

/*
$table_name = USAM_TABLE_TRANSACT;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['date_time']         = "datetime NOT NULL";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['type']              = "tinyint NOT NULL DEFAULT '0' "; // Тип записи занесение-списание
$database_template[$table_name]['columns']['sum']               = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['description']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
  
$table_name = USAM_TABLE_MONEY;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '0' "; // Заблокирован - активен
$database_template[$table_name]['columns']['sum']               = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
*/
// Код для создания или обновления бонусов клиентов
$table_name = USAM_TABLE_CUSTOMER_REVIEWS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['date_time']         = "datetime NOT NULL";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['name']              = "varchar(150) DEFAULT NULL";
$database_template[$table_name]['columns']['mail']              = "varchar(150) DEFAULT NULL";
$database_template[$table_name]['columns']['reviewer_ip']       = "varchar(15) DEFAULT NULL";
$database_template[$table_name]['columns']['title']             = "varchar(150) DEFAULT NULL";
$database_template[$table_name]['columns']['review_text']       = "text";
$database_template[$table_name]['columns']['review_response']   = "text";
$database_template[$table_name]['columns']['status']            = "tinyint DEFAULT '0'";
$database_template[$table_name]['columns']['rating']            = "tinyint(2) DEFAULT '0'";
$database_template[$table_name]['columns']['page_id']           = "int(11) NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['custom_fields']     = "text";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['user_id']           = "KEY `user_id` ( `user_id` )";
$database_template[$table_name]['indexes']['page_id']           = "KEY `page_id` ( `page_id` )";
$database_template[$table_name]['previous_names']               = USAM_META_PREFIX."customer_reviews"; 
 
// Код для создания или обновления бонусов клиентов
$table_name = USAM_TABLE_CUSTOMER_BONUSES;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['order_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['payment_order_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['bonus']            = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']             = "varchar(15) DEFAULT NULL";
$database_template[$table_name]['columns']['use_date']         = "datetime";
$database_template[$table_name]['columns']['date_insert']      = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_update']      = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";

// Таблицы для хранения складов
$table_name = USAM_TABLE_STORAGE_LIST;
$database_template[$table_name]['columns']['id']             = "mediumint(7) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['code']           = "varchar(45) NOT NULL DEFAULT '' ";// код
$database_template[$table_name]['columns']['title']          = "varchar(60) NOT NULL DEFAULT '' ";// название склада
$database_template[$table_name]['columns']['active']         = "tinyint NOT NULL DEFAULT '0' ";// активность
$database_template[$table_name]['columns']['issuing']        = "tinyint NOT NULL DEFAULT '0' ";// пункт выдачи
$database_template[$table_name]['columns']['shipping']       = "tinyint NOT NULL DEFAULT '0' ";// для отгрузки	
$database_template[$table_name]['columns']['description']    = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['location_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['address']        = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['phone']          = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['schedule']       = "varchar(255) NOT NULL DEFAULT '' "; // график работы
$database_template[$table_name]['columns']['date_modified']  = "datetime NOT NULL";
$database_template[$table_name]['columns']['email']          = "varchar(255) NOT NULL DEFAULT '' ";// электронная почта
$database_template[$table_name]['columns']['author']         = "bigint(20) unsigned NOT NULL DEFAULT '0' ";// id пользователя
$database_template[$table_name]['columns']['img']            = "bigint(20) unsigned NOT NULL DEFAULT '0' ";// изображение склада
$database_template[$table_name]['columns']['site_id']        = "mediumint(7) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['GPS_N']          = "float NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['GPS_S']          = "float NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['sort']           = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['columns']['meta_key']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']        = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['previous_names'] = USAM_META_PREFIX."storage_list"; 

// Таблицы для сохранения количества просмотров товаров по дням
$table_name = USAM_TABLE_PRODUCT_VIEWS;
$database_template[$table_name]['columns']['id']         = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['id_product'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['views']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']    = "PRIMARY KEY  ( `id` )";

// В таблице содержится сообщения чата
$table_name = USAM_TABLE_CHAT;
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'"; // номер сотрудника
$database_template[$table_name]['columns']['topic'] = "bigint(15) unsigned NOT NULL DEFAULT '0'"; // группировка сообщений по темам
$database_template[$table_name]['columns']['message'] = "varchar(255) NOT NULL DEFAULT '' ";	   // сообщение
$database_template[$table_name]['columns']['status'] = "tinyint NOT NULL DEFAULT '0' ";		// прочитано - не прочитано
$database_template[$table_name]['columns']['ip'] = "VARCHAR(39) NOT NULL";	
$database_template[$table_name]['columns']['date_insert'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['topic']   = "KEY `topic` ( `topic` )";
$database_template[$table_name]['indexes']['user_id'] = "KEY `user_id` ( `user_id` )";

// В таблице содержится темы чата
$table_name = USAM_TABLE_CHAT_DIALOGS;
$database_template[$table_name]['columns']['id']         = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['manager_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['sessionid']  = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['product_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['name']       = "varchar(100) NOT NULL DEFAULT '' "; // Название темы
$database_template[$table_name]['columns']['date_insert'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']    = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['user_id']    = "KEY `user_id` ( `user_id` )";

$table_name = USAM_TABLE_FEEDBACK;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['id_product']   = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['user_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['location_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type_message'] = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['manager_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['mail']         = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['phone']        = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['name']         = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['message']      = "text";
$database_template[$table_name]['columns']['status']       = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['description']  = "text";
$database_template[$table_name]['columns']['date_insert']  = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_MAILBOX_USERS;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['user_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']   = "UNIQUE KEY `unique_key` (`id`,`user_id`)";

$table_name = USAM_TABLE_MAILBOX;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']              = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['email']           	= "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['pop3server']        = "varchar(15) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['pop3port']          = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['pop3user']          = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['pop3pass']          = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['pop3ssl']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['delete_server_deleted'] = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['delete_server']     = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['delete_server_day'] = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['smtpserver']        = "varchar(15) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['smtpport']          = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['smtpuser']          = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['smtppass']          = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['smtp_secure']       = "varchar(4) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['letter_id']         = "bigint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['sort']              = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_EMAIL_FILTERS;
$database_template[$table_name]['columns']['id']        = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']   = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['mailbox_id'] = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['if']        = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['condition'] = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['value']     = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['action']    = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']   = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['user_id']   = "KEY `user_id` ( `user_id` )";

$table_name = USAM_TABLE_EMAIL_FOLDERS;
$database_template[$table_name]['columns']['id']         = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['mailbox_id'] = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']       = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['slug']       = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['count']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['not_read']   = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['PRIMARY']    = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['mailbox_id'] = "KEY `mailbox_id` ( `mailbox_id` )";

$table_name = USAM_TABLE_EMAIL;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['server_message_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['from_email']        = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['from_name']         = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['to_email']          = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['to_name']           = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['copy_email']        = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['reply_to_email']    = "varchar(250) NOT NULL DEFAULT '' "; // Ответить
$database_template[$table_name]['columns']['reply_to_name']     = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['subject']        	= "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['body']           	= "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']       = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['sent_at']           = "datetime DEFAULT NULL";          // отправлено
$database_template[$table_name]['columns']['read']              = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['folder']            = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['type']              = "varchar(1) NOT NULL DEFAULT '' "; // Полученные R отправленные S
$database_template[$table_name]['columns']['importance']        = "tinyint NOT NULL DEFAULT '0' "; // Важное
$database_template[$table_name]['columns']['opened_at']         = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['mailbox_id']        = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['object_type']       = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['object_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['reply_message_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0' ";// Ответ на сообщение
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_EMAIL_FILE;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['email_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['title']             = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['name']              = "varchar(250) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['type']              = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['mime_type']         = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_SMS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['phone']             = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['message']           = "text";
$database_template[$table_name]['columns']['number']            = "varchar(40) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']       = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['folder']            = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sent_at']           = "datetime DEFAULT NULL";    
$database_template[$table_name]['columns']['object_type']       = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['object_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_PRICE_COMPARISON;
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['product_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['user_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['price_found'] = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['url'] = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['mail'] = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['name'] = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description'] = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['status'] = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type'] = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['manager_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_insert'] = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_PRODUCT_ATTRIBUTES;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['attribute_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['value']        = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['code']         = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']         = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['attribute_id'] = " KEY `attribute_id` ( `attribute_id` )";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";
 
$table_name = USAM_TABLE_PRODUCT_FILTER;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['attribute_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['value']        = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";  
 
$table_name = USAM_TABLE_ASSOCIATED_PRODUCTS;
$database_template[$table_name]['columns']['product_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['associated_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['list']          = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['unique_key']    = "UNIQUE KEY `unique_key` (`product_id`,`associated_id`,`list`)";

$table_name = USAM_TABLE_DOWNLOAD_STATUS;
$database_template[$table_name]['columns']['id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['file_name']   = "varchar(255) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['fileid']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['order_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['basket_id']   = "bigint(20) unsigned NULL";
$database_template[$table_name]['columns']['unique_id']   = "varchar(64) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['downloads']   = "smallint unsigned NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['ip_number']   = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['active']      = "varchar(1) NOT NULL DEFAULT '0' "; //ENUM('1','0')
$database_template[$table_name]['columns']['date_modified'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['basket_id'] = " KEY `basket_id` ( `basket_id` )";
$database_template[$table_name]['indexes']['unique_id'] = "UNIQUE KEY `unique_id` ( `unique_id` )";

$table_name = USAM_TABLE_PRODUCTS_ORDER; 
$database_template[$table_name]['columns']['id']            = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['product_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']          = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['order_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['price']         = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['old_price']     = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['product_day']   = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['tax']           = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['quantity']      = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['bonus']         = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['subscribed']    = "varchar(1) NOT NULL DEFAULT '0' ";// подписка
$database_template[$table_name]['columns']['date_insert']   = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['order_id']      = " KEY `order_id` ( `order_id` )";
$database_template[$table_name]['indexes']['PRIMARY']       = "PRIMARY KEY ( `id` )";

// Код для создания или обновления
$table_name = USAM_TABLE_ORDER_PROPS; 
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name'] = "varchar(255) NOT NULL DEFAULT ''";
$database_template[$table_name]['columns']['type'] = "varchar(64) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['group'] = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['mandatory'] = "varchar(1) NOT NULL DEFAULT '0' ";// Обязательное заполнение
$database_template[$table_name]['columns']['active'] = "varchar(1) NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['fast_buy'] = "varchar(1) NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['unique_name'] = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['profile'] = "varchar(1) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['options'] = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['mask'] = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort'] = "tinyint NOT NULL DEFAULT '0' "; // Сортировка
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['sort'] = " KEY `sort` ( `sort` )";

$table_name = USAM_TABLE_ORDER_PROPS_GROUP; 
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name'] = "varchar(255) NOT NULL DEFAULT ''";
$database_template[$table_name]['columns']['code'] = "varchar(255) NOT NULL DEFAULT ''";
$database_template[$table_name]['columns']['type_payer_id'] = "int(5) unsigned NOT NULL DEFAULT '0' ";//Тип плательщика
$database_template[$table_name]['columns']['sort']    = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['type_payer_id'] = " KEY `type_payer_id` ( `type_payer_id` )";

$table_name = USAM_TABLE_RETURN_PURCHASES;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['sum']               = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['reason']            = "varchar(255) NOT NULL DEFAULT '' "; // причина
$database_template[$table_name]['columns']['status']            = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['storage']           = "mediumint(7) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['notes']             = "varchar(255) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['date_update']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['order_id']          = " KEY `order_id` ( `order_id` )";

$table_name = USAM_TABLE_RETURNED_PRODUCTS;
$database_template[$table_name]['columns']['documents_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['basket_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['quantity']          = "int(10) NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`documents_id`,`basket_id`)";

$table_name = USAM_TABLE_PAYMENT_HISTORY;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['document_type']     = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['document_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['bank_account_id']   = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']              = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['document_number']   = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['external_document'] = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_payed']        = "datetime";
$database_template[$table_name]['columns']['pay_up']            = "datetime"; //Оплать до
$database_template[$table_name]['columns']['sum']               = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['payment_type']      = "tinyint NOT NULL DEFAULT '1' "; //Расход 0 или приход 1
$database_template[$table_name]['columns']['gateway_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['transactid']        = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_update']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_time']         = "datetime NOT NULL";
$database_template[$table_name]['columns']['note']              = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['document_type']     = " KEY `document_type` ( `document_type` )";
$database_template[$table_name]['indexes']['document_id']       = " KEY `document_id` ( `document_id` )";
 
$table_name = USAM_TABLE_SHIPPED_PRODUCTS;
$database_template[$table_name]['columns']['documents_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['basket_id']         = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['quantity']          = "int(10) NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['reserve']           = "int(10) NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`documents_id`,`basket_id`)";

$table_name = USAM_TABLE_SHIPPED_DOCUMENTS;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['courier']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";// Курьер
$database_template[$table_name]['columns']['name']              = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['method']            = "varchar(50) NULL ";
$database_template[$table_name]['columns']['planned_date']      = "datetime";
$database_template[$table_name]['columns']['storage_pickup']    = "mediumint(7) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['storage']           = "mediumint(7) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['notes']             = "varchar(255) NOT NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['price']             = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['include_in_cost']   = "tinyint unsigned NOT NULL DEFAULT 1";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_update']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_allow_delivery'] = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['readiness_date']    = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['readiness']         = "tinyint(3) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['status']            = "varchar(10) NULL DEFAULT '' "; 
$database_template[$table_name]['columns']['track_id']          = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['doc_number']        = "varchar(50) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['doc_data']          = "datetime";
$database_template[$table_name]['columns']['export']            = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['order_id']          = " KEY `order_id` ( `order_id` )";
$database_template[$table_name]['indexes']['status']            = " KEY `status` ( `status` )";

// Код для создания или обновления таблицы с заказами
$table_name = USAM_TABLE_ORDERS; 
$database_template[$table_name]['columns']['id']                  = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['totalprice']          = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type_price']          = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['paid']                = "tinyint NOT NULL DEFAULT '0' ";        // Оплачен
$database_template[$table_name]['columns']['date_paid']           = "datetime DEFAULT NULL";
$database_template[$table_name]['columns']['sessionid']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['status']              = "varchar(20) NULL DEFAULT ''";
$database_template[$table_name]['columns']['type_payer']          = "smallint unsigned NOT NULL DEFAULT '0' ";//Тип плательщика
$database_template[$table_name]['columns']['user_ID']             = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['customer_id']         = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['manager_id']          = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['modified_customer']   = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['coupon_name']         = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['coupon_discount']     = "decimal(11,2) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['discount']            = "decimal(11,2) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['bonus']               = "decimal(11,2) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['notes']   			  = "text NULL";
$database_template[$table_name]['columns']['cancellation_reason'] = "varchar(255)  NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['shipping']            = "decimal(11,2) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['total_tax']           = "decimal(11,2) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['taxes_rate']          = "bigint(20) unsigned NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_insert']         = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified']       = "datetime NOT NULL";
$database_template[$table_name]['columns']['order_type']          = "varchar(20) NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']             = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['status']              = "KEY `status` ( `status` )";
$database_template[$table_name]['indexes']['user_ID']             = "KEY `user_ID` ( `user_ID` )";

$table_name = USAM_TABLE_ORDER_CHANGE_HISTORY;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['date']              = "datetime NOT NULL";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['operation']         = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['order_id']          = " KEY `order_id` ( `order_id` )";

$table_name = USAM_TABLE_ORDER_FEEDBACK;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['manager_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0'";     // Менеджер
$database_template[$table_name]['columns']['source']            = "varchar(10) NOT NULL DEFAULT '' ";             // Источник
$database_template[$table_name]['columns']['message']           = "text NULL";
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['order_id']          = " KEY `order_id` ( `order_id` )";

// Таблица статусы заказов
$table_name = USAM_TABLE_ORDER_STATUS; 
$database_template[$table_name]['columns']['internalname']  = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['name']          = "varchar(100) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']   = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['color']         = "varchar(7) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['short_name']    = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']          = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['columns']['visibility']    = "tinyint NOT NULL DEFAULT '0' "; // отображаемый
$database_template[$table_name]['columns']['close']         = "tinyint NOT NULL DEFAULT '0' "; // Заказ завершен
$database_template[$table_name]['indexes']['PRIMARY']       = "PRIMARY KEY  ( `internalname` )";	

// Таблица местоположений
$table_name = USAM_TABLE_LOCATION; 
$database_template[$table_name]['columns']['id']            = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['id_type']       = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['parent']        = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']          = "varchar(64) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']          = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']       = "PRIMARY KEY  ( `id` )";

// Таблица типы местоположений
$table_name = USAM_TABLE_LOCATION_TYPE; 
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['code'] = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['level'] = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name'] = "varchar(40) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']    = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
//$database_template[$table_name]['actions']['after']['all'] = "add_type_location";

//Таблица страны
$table_name = USAM_TABLE_COUNTRY;
$database_template[$table_name]['columns']['code'] = "char(2) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['numerical'] = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name'] = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['currency'] = "char(3) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['phone_code'] = "smallint unsigned NOT NULL DEFAULT '0' "; // телефонные коды
$database_template[$table_name]['columns']['continent'] = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['location_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `code` )";

//Таблица валют
$table_name = USAM_TABLE_CURRENCY;
$database_template[$table_name]['columns']['code']      = "char(3) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['numerical'] = "smallint unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']      = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['symbol']    = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['symbol_html'] = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['display_currency'] = "varchar(1) NOT NULL DEFAULT '1'";// показать другие валюты для товара
$database_template[$table_name]['indexes']['PRIMARY']     = "PRIMARY KEY  ( `code` )";

$table_name = USAM_TABLE_CURRENCY_RATES;
$database_template[$table_name]['columns']['basic_currency'] = "char(3) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['currency']       = "char(3) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['rate']           = "decimal(11,2) NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['date_update']    = "datetime NOT NULL";
$database_template[$table_name]['indexes']['unique_key']     = "UNIQUE KEY `unique_key` (`basic_currency`,`currency`)";

// Код для создания или обновления таблицы с данными клиентов
$table_name = USAM_TABLE_ORDER_PROPS_VALUE;
$database_template[$table_name]['columns']['id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['unique_name'] = "varchar(50) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['order_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['value']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']     = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['order_id']    = " KEY `order_id` ( `order_id`, `unique_name` )";

// Код для создания или обновления таблицы с купонами
$table_name = USAM_TABLE_COUPON_CODES;
$database_template[$table_name]['columns']['id'] = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['coupon_code'] = "varchar(255) NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description'] = "text NULL";
$database_template[$table_name]['columns']['action'] = "char(1) NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['value'] = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['is_percentage'] = "char(1) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['use_once'] = "char(1) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['is_used'] = "int(5) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['max_is_used'] = "int(5) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['active'] = "char(1) NOT NULL DEFAULT '1' ";
$database_template[$table_name]['columns']['customer'] = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['amount_bonuses_author'] = "int(6) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['start'] = "datetime NOT NULL";
$database_template[$table_name]['columns']['expiry'] = "datetime NOT NULL";
$database_template[$table_name]['columns']['condition'] = " text NULL";
$database_template[$table_name]['columns']['coupon_type'] = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert'] = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY'] = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['coupon_code'] = " KEY `coupon_code` ( `coupon_code` )";
$database_template[$table_name]['indexes']['active'] = " KEY `active` ( `active` )";
$database_template[$table_name]['indexes']['start'] = " KEY `start` ( `start` )";
$database_template[$table_name]['indexes']['expiry'] = " KEY `expiry` ( `expiry` )";

$table_name = USAM_TABLE_USERS_BASKET;
$database_template[$table_name]['columns']['id']            = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['user_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['shipping_method'] = "bigint(10) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['storage_pickup']  = "mediumint(7) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['gateway_method']  = "bigint(10) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['coupon_name']     = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['quantity']      = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['totalprice']    = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_insert']   = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified'] = "datetime NOT NULL";
$database_template[$table_name]['columns']['errors']        = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']       = "PRIMARY KEY  ( `id` )";
 
$table_name = USAM_TABLE_DISCOUNT_BASKET;
$database_template[$table_name]['columns']['discount_order_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['cart_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']        = "UNIQUE KEY `unique_key` (`discount_order_id`,`cart_id`)";

$table_name = USAM_TABLE_PRODUCT_DISCOUNT_RELATIONSHIPS;
$database_template[$table_name]['columns']['product_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['code_price']  = "varchar(20) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['discount_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['indexes']['unique_key']  = "UNIQUE KEY `unique_key` (`product_id`,`code_price`,`discount_id`)";

$table_name = USAM_TABLE_ORDER_DISCOUNT;
$database_template[$table_name]['columns']['id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['discount_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['name']        = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['priority']    = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']        = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['value']       = "int(8) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['value_type']  = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['condition']   = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['order_id']    = " KEY `order_id` ( `order_id` )";
$database_template[$table_name]['indexes']['PRIMARY']     = "PRIMARY KEY  ( `id` )"; 

$table_name = USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER;
$database_template[$table_name]['columns']['id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['order_id']    = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['discount_id'] = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['product_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['name']        = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['priority']    = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['discount']    = "int(8) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['dtype']       = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['conditions']  = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['order_id']    = " KEY `order_id` ( `order_id` )";
$database_template[$table_name]['indexes']['PRIMARY']     = "PRIMARY KEY  ( `id` )"; 

$table_name = USAM_TABLE_PRODUCTS_BASKET; 
$database_template[$table_name]['columns']['id']             = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['cart_id']        = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['product_id']     = "bigint(20) UNSIGNED NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sku']            = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['price']          = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['old_price']      = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['tax']            = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['gift']           = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['quantity']       = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['date_insert']    = "datetime NOT NULL";
$database_template[$table_name]['columns']['date_modified']  = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']        = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_TAXONOMY_RELATIONSHIPS;
$database_template[$table_name]['columns']['term_id1']   = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['term_id2']   = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['taxonomy']   = "varchar(40) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['unique_key'] = "UNIQUE KEY `unique_key` (`term_id1`,`term_id2`)";

$table_name = USAM_TABLE_SLIDER;
$database_template[$table_name]['columns']['id']             = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['name']           = "varchar(60) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['active']         = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['type']           = "char(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['template']       = "char(15) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['setting']        = "longtext NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']        = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_SLIDES;
$database_template[$table_name]['columns']['id']             = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['slider_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['object_id']      = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['title']          = "varchar(60) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']    = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['link']           = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['fon']            = "varchar(8) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['interval_to']    = "datetime";
$database_template[$table_name]['columns']['interval_from']  = "datetime";
$database_template[$table_name]['columns']['sort']           = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['PRIMARY']        = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_SITES;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['domain']       = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['description']  = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['type']         = "varchar(5) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']  = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_KEYWORDS;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['keyword']      = "varchar(60) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_SEARCH_ENGINE_REGIONS;
$database_template[$table_name]['columns']['id']           = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['location_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['active']       = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['code']         = "int(10) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['name']         = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['search_engine']= "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['sort']         = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['indexes']['unique_key']   = "UNIQUE KEY `unique_key` (`location_id`,`search_engine`)";
$database_template[$table_name]['indexes']['PRIMARY']      = "PRIMARY KEY  ( `id` )";

$table_name = USAM_TABLE_STATISTICS_KEYWORDS;
$database_template[$table_name]['columns']['date_insert']  = "DATE NOT NULL";
$database_template[$table_name]['columns']['keyword_id']   = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['location_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['url']          = "varchar(255) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['search_engine']= "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['site_id']      = "mediumint(7) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['number']       = "tinyint NOT NULL DEFAULT '0' ";
$database_template[$table_name]['indexes']['unique_key']   = "UNIQUE KEY `unique_key` (`date_insert`,`keyword_id`,`location_id`,`search_engine`,`site_id`)";

$table_name = USAM_TABLE_SUPPORT_MESSAGE;
$database_template[$table_name]['columns']['id']                = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['user_id']           = "bigint(20) unsigned NOT NULL DEFAULT '0'";    
$database_template[$table_name]['columns']['subject']           = "varchar(6) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['message']           = "text NULL";
$database_template[$table_name]['columns']['read']              = "tinyint NOT NULL DEFAULT '0' "; //прочитано
$database_template[$table_name]['columns']['outbox']            = "tinyint NOT NULL DEFAULT '0' "; //исходящие
$database_template[$table_name]['columns']['date_insert']       = "datetime NOT NULL";
$database_template[$table_name]['indexes']['PRIMARY']           = "PRIMARY KEY  ( `id` )";
$database_template[$table_name]['indexes']['outbox']            = " KEY `outbox` ( `outbox` )";

$table_name = USAM_TABLE_PRODUCT_DAY;
$database_template[$table_name]['columns']['id']          = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['rule_id']     = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['product_id']  = "bigint(20) unsigned NOT NULL DEFAULT '0'";
$database_template[$table_name]['columns']['value']       = "decimal(11,2) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['dtype']       = "varchar(1) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['status']      = "varchar(1) NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['sort']        = "tinyint unsigned NOT NULL DEFAULT '100' ";
$database_template[$table_name]['columns']['date_time']   = "datetime NULL";
$database_template[$table_name]['indexes']['product_id']  = " KEY `product_id` ( `product_id` )";
$database_template[$table_name]['indexes']['PRIMARY']     = "PRIMARY KEY  ( `id` )"; 

$table_name = USAM_TABLE_USER_PRODUCTS;
$database_template[$table_name]['columns']['id']               = "bigint(20) unsigned NOT NULL AUTO_INCREMENT";
$database_template[$table_name]['columns']['product_id']       = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['user_id']          = "bigint(20) unsigned NOT NULL DEFAULT '0' ";
$database_template[$table_name]['columns']['user_code']        = "varchar(13) NOT NULL DEFAULT '' ";
$database_template[$table_name]['columns']['date_insert']      = "datetime NOT NULL";
$database_template[$table_name]['columns']['user_list']        = "varchar(10) NOT NULL DEFAULT '' ";
$database_template[$table_name]['indexes']['user_id']          = " KEY `user_id` ( `user_id` )";
$database_template[$table_name]['indexes']['user_code']        = " KEY `user_code` ( `user_code` )";
$database_template[$table_name]['indexes']['PRIMARY']          = "PRIMARY KEY  ( `id` )";
?>