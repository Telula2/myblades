<?php
/*
 * Отображение страницы "Обмен данных"
 */

 
$default_tabs = array(
	array( 'id' => 'file',  'title' => __( 'С компьютера', 'usam' ) ),
	array( 'id' => 'ftp',  'title' => __( 'FTP', 'usam' ) ),
	array( 'id' => 'product_importer',  'title' => __( 'Импорт товаров', 'usam' ) ),	
	array( 'id' => 'mysql',  'title' => 'УНИВЕРСАМ', 'level' => array(
			array( 'id' => 'mysql_products',  'title' => __( 'Список товаров', 'usam' ) ),
		),
	),
//	array( 'id' => '1c',  'title' => __( '1С', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{			
	protected function localize_script_tab()
	{
		return array(			
			
		);
	}	
} 