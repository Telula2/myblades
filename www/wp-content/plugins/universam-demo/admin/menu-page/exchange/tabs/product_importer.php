<?php
class USAM_Tab_Product_Importer extends USAM_Tab
{
	private $file_path = false;	
	private $step = 1;
	protected $delimiter = ';';
	protected $encoding = ';';	
	
	protected $display_save_button = false;	

	public function __construct()
	{
		$this->step = empty( $_REQUEST['step'] ) ? 1 : (int) $_REQUEST['step'];
		if ( $this->step < 1 || $this->step > 3 )
			$this->step = 1;
		
		parent::__construct();

		$this->delimiter = isset($_SESSION['delimiter'])?$_SESSION['delimiter']:$this->delimiter;	
		$this->encoding = isset($_SESSION['encoding'])?$_SESSION['encoding']:$this->encoding;	
		
		$file_path = get_transient( 'usam_settings_tab_import_file' );	
		if ( $file_path )
			$this->file_path = $file_path;
		$this->header = array( 'title' => __('Импорт товаров', 'usam'), 'description' => __("Вы можете импортировать товары, загрузив их из файла.","usam") );				
	}	
	
	public function callback_submit() 
	{ 
		$this->redirect	= true;		
		switch ( $this->step ) 
		{
			case 1:				
				if ( isset( $_FILES['csv_file'] ) && isset( $_FILES['csv_file']['name'] ) && ($_FILES['csv_file']['name'] != '') ) 
				{				
					$_SESSION['delimiter'] = isset($_POST['delimiter'])?$_POST['delimiter']:$this->delimiter;	
					$_SESSION['encoding'] = isset($_POST['encoding'])?$_POST['encoding']:$this->encoding;	
					ini_set( 'auto_detect_line_endings', 1 );
					$file = $_FILES['csv_file'];
					$file_path = USAM_FILE_DIR . $file['name'];
					if ( move_uploaded_file( $file['tmp_name'], USAM_FILE_DIR . $file['name'] ) ) 
					{
						set_transient( 'usam_settings_tab_import_file', $file_path );				
						$this->sendback = add_query_arg( array( 'step' => 2 ), $this->sendback );
						return;
					}
				}			
			break;					
		}			
		switch( $this->current_action )
		{
			case 'start_import':
				$this->import_data( false );
				$this->sendback = add_query_arg( array( 'step' => 1 ), $this->sendback );
			break;
		}
	}

	private function prepare_import_columns() 
	{ 		
		ini_set( 'auto_detect_line_endings', 1 );
		$data = usam_read_txt_file($this->file_path, $this->delimiter, $this->encoding );			
		if ( empty($data[0]) ) 
		{ 
			$this->reset_state();
			return array();
		}			
		return $data[0];		
	}

	private function reset_state()
	{
		delete_transient( 'usam_settings_tab_import_file' );
		$this->file_path = false;	
	}

	private function import_data() 
	{	
		global $wpdb;
		ini_set( 'auto_detect_line_endings', 1 );				
		
		$column_map = array_flip( $_POST['value_name'] );
		extract( $column_map, EXTR_SKIP );
		
		$data = usam_read_txt_file($this->file_path, $this->delimiter, $this->encoding );			
		if ( empty($data) ) 
		{
			$this->reset_state();
			return;
		}		
		$type_price = sanitize_title($_POST['type_price']);		
		$post_status = sanitize_title($_POST['post_status']);		
		$storages = usam_get_stores();
		foreach($data as $key => $row )		
		{
			$product = array(
				'post_title'             => isset($column_name) && isset( $row[$column_name] ) ? $row[$column_name] : '',
				'post_status'            => $post_status,
				'post_content'           => isset($column_description) && isset( $row[$column_description] ) ? $row[$column_description] : '',
				'post_excerpt'           => isset($column_additional_description) && isset( $row[$column_additional_description] ) ? $row[$column_additional_description] : '',
				'thumbnail'              => isset($column_thumbnail) && isset( $row[$column_thumbnail] ) ? $row[$column_thumbnail] : '',
				'meta'                   => array(
					'price_'.$type_price => isset($column_price) && isset( $row[$column_price] ) ? str_replace( '$', '', $row[$column_price] ) : 0,
					'sku'               => isset($column_sku) && isset( $row[$column_sku] ) ? $row[$column_sku] : '',
					'weight'            => isset($column_weight) && isset( $row[$column_weight] ) ? $row[$column_weight] : '',
					'barcode'           => isset($column_barcode) && isset( $row[$column_barcode] ) ? $row[$column_barcode] : '',	
				)
			);
			foreach ( $storages as $storage )
			{
				$column = "column_".$storage->meta_key;
				$product['meta'][$storage->meta_key] = isset($column_map[$column]) && isset( $row[$column_map[$column]] ) ? $row[$column_map[$column]] : 0;				
			}
			if ( isset($column_category) && isset($row[$column_category]) && is_numeric($row[$column_category]))
			{
				$category = get_term_by( 'name', $row[$column_category], 'usam-category', $output, $filter );
				$product['tax_input']['usam-category'][] = (int)$category->id;
			}
			else
				$product['tax_input']['usam-category'] = array( (int)$_POST['category'] );
					
			$result = true;			
			if ( !empty($product['sku']) )
			{
				$product_id = usam_get_product_id_by_sku( $product['sku'] );				
				if ( !empty($product_id) ) 
					$result = false;
			}
			$_product = new USAM_Product( $product );	
			if ( $result )	
				$_product->insert_product();
			else
				$_product->save_product_meta( );
		}	
		$this->reset_state();		
		add_settings_error( 'shop_settings', 'settings_updated', __( 'CSV файл импортирован.', 'usam' ), 'updated' );
	}	
	
	private function display_imported_columns()
	{
		$columns = $this->prepare_import_columns();		
		$storages = usam_get_stores();
		$categories = get_terms( 'usam-category', 'hide_empty=0' );
		?>
			<p><?php _e( 'Для каждого столбца, выберите соответствующие поле. Вы можете загрузить столько продуктов, сколько вы хотите.', 'usam' ); ?></p>
			<p><?php _e( 'Если в вашем файле есть колонка с категориями, то сначала создайте их, а потом нажмите Импортировать.', 'usam' ); ?></p>
			<div class='metabox-holder' style='width:90%'>
				<div style='width:100%;' class='postbox'>
					<h3 class='hndle'><?php _e('Статус товара' , 'usam' ); ?></h3>
					<div class='inside'>
						<table class ="subtab-detail-content-table usam_edit_table">
							<tr>
								<td class ="name"><?php _e( 'Выберите, статус для импортируемых товаров' , 'usam' ); ?>:</td>
								<td>
									<select name='post_status'>										
										<option value='draft'  ><?php _e( 'Черновик'  , 'usam'); ?></option>
										<option value='publish'><?php _e( 'Публиковать', 'usam'); ?></option>
									</select>
								</td>							
							</tr>
							<tr>
								<td class ="name"><?php _e( 'Тип загружаемой цены' , 'usam' ); ?>:</td>								
								<td><?php usam_get_select_prices( ); ?></td>
							</tr>
						</table>
					</div>
				</div>						
				<?php foreach ( $columns as $key => $datum ): ?>
					<div style='width:100%;' class='postbox'>
						<h3 class='hndle'><?php printf(__('Столбец (%s)', 'usam'), ($key + 1)); ?></h3>
						<div class='inside'>
							<table>
								<tr>
									<td style='width:80%;'><?php echo $datum; ?></td>
									<td>
										<select  name='value_name[<?php echo $key; ?>]'>
											<option <?php selected( $key, 0 ); ?> value='column_name'                  ><?php _e('Имя товара'          , 'usam'); ?></option>
											<option <?php selected( $key, 1 ); ?> value='column_thumbnail'             ><?php _e('Ссылка на миниатюру', 'usam'); ?></option>
											<option <?php selected( $key, 2 ); ?> value='column_description'           ><?php _e('Описание'           , 'usam'); ?></option>
											<option <?php selected( $key, 3 ); ?> value='column_additional_description'><?php _e('Дополнительное описание', 'usam'); ?></option>
											<option <?php selected( $key, 4 ); ?> value='column_price'                 ><?php _e('Цена'                 , 'usam'); ?></option>
											<option <?php selected( $key, 5 ); ?> value='column_sku'                   ><?php _e('Артикул'               , 'usam'); ?></option>
											<option <?php selected( $key, 6 ); ?> value='column_weight'                ><?php _e('Вес'                , 'usam'); ?></option>
											<option <?php selected( $key, 7 ); ?> value='column_weight_unit'           ><?php _e('Весовая единица'    , 'usam'); ?></option>
											<option <?php selected( $key, 8 ); ?> value='column_quantity'              ><?php _e('Колличество'        , 'usam'); ?></option>
											<option <?php selected( $key, 9 ); ?> value='column_barcode'               ><?php _e('Штрихкод'  , 'usam'); ?></option>
											<option <?php selected( $key, 10 ); ?> value='column_category'              ><?php _e('Категория'  , 'usam'); ?></option>
											<?php
											$i = 9;
											foreach ( $storages as $storage )
											{
												$i++;
												?><option <?php selected( $key, $i ); ?> value='column_<?php echo $storage->meta_key; ?>'><?php echo _e('Склад', 'usam')." - ".$storage->title; ?></option><?php
											}
											?>
										</select>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<?php endforeach; ?>
				<label for='category'><?php _e('Пожалуйста, выберите категорию, которую вы хотели бы разместить все продукты из этого CSV' , 'usam' ); ?>:</label>
				<select id='category' name='category'>
					<?php foreach ( $categories as $category ): ?>
						<option value="<?php echo $category->term_id; ?>"><?php echo esc_html( $category->name ); ?></option>
					<?php endforeach; ?>
				</select>				
				<input type="hidden" name="action" value="start_import" />				
				<input type='submit' value='<?php echo esc_html_x( 'Импортировать', 'import csv', 'usam' ); ?>' class='button-primary'>
			</div>
		<?php
		$this->nonce_field();
	}
			
	public function display_default()
	{		
		?>
		<table class='subtab-detail-content-table usam_edit_table'>		
			<tbody>
				<tr>
					<td class='name'><?php _e( 'Разделить:', 'usam' ); ?></td>
					<td><input type="text" size='10' maxlength="1" name="delimiter" value="<?php echo $this->delimiter; ?>" /></td></tr>
				<tr>
					<td class='name'><?php _e( 'Кодировка файла:', 'usam' ); ?></td>
					<td>
						<select name='encoding'>						
							<option value='utf-8' <?php selected($this->encoding, 'utf-8') ?>>utf-8</option>
							<option value='windows-1251' <?php selected($this->encoding, 'windows-1251') ?>>windows-1251</option>
						</select>
					</td>
				</tr>		
				<tr><td></td><td><input type='file' name='csv_file'/></td></tr>
			</tbody>
		</table>
		<input type="hidden" name="action" value="import" />
		<?php 
		$this->nonce_field();
		submit_button( __( 'Загрузить', 'usam' ) ); 
	}	

	public function display() 
	{		
		?>
		<form method='POST' action='' id='usam-tab_form' enctype='multipart/form-data'>		
		<?php
		switch ( $this->step ) 
		{
			case 1:
				usam_add_box( 'usam_product_importer', __( 'Импортер товаров', 'usam' ), array( $this, 'display_default' ) );	
				break;
			case 2:				
				$this->display_imported_columns();
				break;
			default:
				$this->display_default();
				break;
		}
		?>
		</form>
		<?php
	}
}