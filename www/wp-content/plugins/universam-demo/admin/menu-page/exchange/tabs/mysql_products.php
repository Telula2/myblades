<?php
class USAM_Tab_mysql_products extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Обмен данными с MYSQL', 'usam'), 'description' => 'Здесь Вы можете загружать товары из другой базы wordpress, например, у вас есть поддомены и вы хотите перекачать информацию между ними.' );
		
		global $db_export;

		$settings_mysql = get_option( 'usam_exchange_mysql_settings', array() );			
		if ( !empty($settings_mysql) )			
		{			
			$db_export = new wpdb( $settings_mysql['user'], $settings_mysql['pass'], $settings_mysql['db'], $settings_mysql['host'] );	
		}
	}
	
	protected function load_tab()
	{			
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $db_export, $wpdb;					
		if ( isset($_GET['insert_post']) && !empty($_GET['cb']) && is_object($db_export) )
		{						
			$post_id = implode( "', '",  $_GET['cb'] );
			$sql = "SELECT * FROM {$wpdb->posts} WHERE ID IN ('". $post_id ."')";
			$table_data = $db_export->get_results( $sql );	
		
			$i = 0;
			foreach ( $table_data as $post ) 
			{					
				$i++;
				$new_post_type = $post->post_type;
				$post_content = str_replace( "'", "''", $post->post_content );
				$post_content_filtered = str_replace( "'", "''", $post->post_content_filtered );
				$post_excerpt = str_replace( "'", "''", $post->post_excerpt );
				$post_title = str_replace( "'", "''", $post->post_title );
				$post_name = str_replace( "'", "''", $post->post_name );
				$comment_status = str_replace( "'", "''", $post->comment_status );
				$ping_status = str_replace( "'", "''", $post->ping_status );

				$terms = array();
				$terms_data = $db_export->get_results( "SELECT term_id, name FROM $wpdb->terms" );				
				foreach ( $terms_data as $term )
					$terms[$term->term_id] = $term->name;
				
				$meta = array();
				$product_attributes = array();
				$post_meta_infos = $db_export->get_results( $wpdb->prepare( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = %d", $post->ID ) );				
				if ( count( $post_meta_infos ) ) 
				{							
					foreach ( $post_meta_infos as $meta_info )
					{						
						$meta_key = $meta_info->meta_key;
						$meta_value = addslashes( $meta_info->meta_value );						
						switch( $meta_key )
						{
							case '_edit_lock':		
							case '_edit_last':		
								$save_action = false;
							break;	
							case '_thumbnail_id':									
								$thumbnail_id = $meta_value;								
								$save_action = false;
							break;
							case '_usam_reserve':	
							case '_usam_product_views':					
								$meta_value = 0;
								$save_action = true;
							break;																
							case '_usam_product_metadata':
								$meta_value = maybe_unserialize( $meta_info->meta_value );								
								$save_action = true;
							break;
							default:
								if ( stripos($meta_key, '_usam_product_attributes_') !== false) 
								{
									$id = str_replace("_usam_product_attributes_", "", $meta_key);
									$term = get_term_by('name', $terms[$id], 'usam-product_attributes');	
									if ( !empty($term) )
									{ 	
										$product_attributes[$term->term_id] = $meta_info->meta_value;
									}
									$save_action = false;
								}															
								elseif ( stripos($meta_key, '_usam_filter_') !== false) 
									$save_action = true;
								elseif ( stripos($meta_key, '_usam_price_') !== false) 
									$save_action = true;
								elseif ( stripos($meta_key, '_usam_underprice_') !== false) 
									$save_action = true;
								elseif ( stripos($meta_key, '_usam_old_price_') !== false) 
									$save_action = true;
								elseif ( stripos($meta_key, '_usam_') !== false) 
									$save_action = true;
								else
									$save_action = false;
							break;
						}						
						if ( $save_action )
						{
							$meta_key = str_replace("_usam_", "", $meta_key);
							$meta[$meta_key] = $meta_value;
						}
					}					
				}					
				$product = array(
					'post_status'           => 'draft',
					'post_type'             => $new_post_type,
					'ping_status'           => $ping_status,
					'post_parent'           => $post->post_parent,
					'menu_order'            => $post->menu_order,
					'to_ping'               => $post->to_ping,
					'pinged'                => $post->pinged,
					'post_excerpt'          => $post_excerpt,
					'post_title'            => $post_title,
					'post_content'          => $post_content,
					'post_content_filtered' => $post_content_filtered,
					'post_mime_type'        => $post->post_mime_type,
					'meta'                  => $meta,
					);					
				$_product = new USAM_Product( $product );		
				$new_post_id = $_product->insert_product();
				$_product->calculate_product_attributes( $product_attributes );
				
				$attach_id = $this->set_image( $thumbnail_id, $new_post_id );
				set_post_thumbnail( $new_post_id, $attach_id );		
				
				$id_image = $db_export->get_col( $wpdb->prepare("SELECT `ID` FROM `{$wpdb->posts}` WHERE `post_parent` = '%d' AND `post_type` = 'attachment' AND `ID` != '$thumbnail_id'",$post->ID));
				foreach ( $id_image as $id )
				{	
					$attach_id = $this->set_image( $id, $new_post_id );	
				}
				if ( !empty($_GET["usam-category"]))
				{					
					$category = sanitize_title($_GET["usam-category"]);
					wp_set_object_terms( $new_post_id, $category, "usam-category", true );
				}
				//usam_duplicate_children( $post->ID, $new_post_id );// Ноходит дочерние записи (в том числе файлы продуктов и изображения продуктов), их значения мета, и дублирует их.
			} 
			$this->sendback = add_query_arg( array( 'insert_post' => $i ), $this->sendback );			
			$this->redirect = true;		
		}		
	}	
	
	private function set_image( $meta_value, $new_post_id )
	{
		global $db_export, $wpdb;
		
		$url = $db_export->get_var( "SELECT guid FROM {$wpdb->posts} WHERE ID = '$meta_value'" );
		$upload = wp_upload_dir(date('m'));   // Получим путь до директории загрузок.
		$file = usam_download_image_in_url( $url, $upload['path'] );	
		$filename =	$file['path'];
		// Проверим тип поста, который мы будем использовать в поле 'post_mime_type'.
		$filetype = wp_check_filetype( basename( $filename ), null );								
		// Подготовим массив с необходимыми данными для вложения.
		$attachment = array(
			'guid'           => $upload['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);
		// Вставляем запись в базу данных.
		$attach_id = wp_insert_attachment( $attachment, $filename, $new_post_id );	
		// wp_generate_attachment_metadata() зависит от этого файла.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		// Создадим метаданные для вложения и обновим запись в базе данных.		
		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		return $attach_id;
	}

	public function get_message()
	{		
		$message = '';					
		if( isset($_REQUEST['insert_post']) )
		{
			$insert_post = absint( $_REQUEST['insert_post'] );	
			$message = sprintf( _n( '%s товар добавлен в базу данных.', '%s товаров добавлено в базу данных.', $insert_post, 'usam' ), $insert_post );			
		}
		return $message;
	} 		
}