<?php
class USAM_Tab_1c extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Отчет по корзинам', 'usam'), 'description' => 'Здесь вы можете посмотреть отчет по корзинам.' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}
}