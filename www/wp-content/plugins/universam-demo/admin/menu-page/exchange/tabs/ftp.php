<?php
class USAM_Tab_ftp extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Обмен данными с FTP', 'usam'), 'description' => 'Здесь Вы можете загружать цены и остатков с FTP из определенных файлов и выгружать заказы.' );	
	}	
	
	protected function callback_submit()
	{		
		if ( !empty($_POST)  )
		{			
			if ( isset($_POST['save']) || isset($_POST['price_update']) )
			{		
				$ftp = get_option( 'usam_ftp_settings', '' );			
				
				$new_ftp_settings = $_POST['ftp_settings'];	
				if ( !empty($new_ftp_settings['pass']) && $new_ftp_settings['pass'] == '******' )
				{
					$new_ftp_settings['pass'] = $ftp['pass'];
				}		
				else
				{					
					$new_ftp_settings['pass'] = ds_crypt( $new_ftp_settings['pass'] );
				}
				update_option( 'usam_ftp_settings', $new_ftp_settings );
				
				$new_ftp = stripslashes_deep($_POST['ftp']);			
				
				update_option( 'exchange_ftp_settings', $new_ftp );						
				
				update_option( 'usam_ftp_download_balances', absint($_POST['ftp_download_balances']) );
				update_option( 'usam_ftp_export_order', absint($_POST['ftp_export_order']) );	
				update_option( 'usam_ftp_download_prices', absint($_POST['ftp_download_prices']) );				
				
				$this->sendback = add_query_arg( array( 'update' => '1' ), $this->sendback );	
				$this->redirect = true;				
			}		
			if ( isset($_POST['price_update']) ) 
			{			
				$exchange_FTP = new USAM_Exchange_FTP();
				$result = $exchange_FTP->update_price_all_products( );				
				$this->set_user_screen_error( $exchange_FTP->get_error() );	
			
				if ( $result == 'set_task' )					
					$this->sendback = add_query_arg( array( 'set_task' => 'price' ), $this->sendback );	
				elseif ( $result == 'completed' )
					$this->sendback = add_query_arg( array( 'completed' => 'price' ), $this->sendback );	
				else
					$this->sendback = add_query_arg( array( 'error' => '1' ), $this->sendback );					
				$this->redirect = true;				
			}	

			if ( isset($_POST['ftp_test']) ) 
			{			
				$ftp = new USAM_FTP();
				$result	= $ftp->ftp_open();	
				if ( !$result )	
				{								
					foreach( $ftp->get_errors() as $error ) 			
						$this->set_user_screen_error( $error );
				}
				else
					$this->set_user_screen_message( __('Соединение установлено успешно!','usam') );	
				$this->redirect = true;				
			}				
			if ( isset($_POST['stock_update']) ) 
			{	
				$exchange_FTP = new USAM_Exchange_FTP();
				$result = $exchange_FTP->update_product_all_quantity( );
				
				if ( $result == 'set_task' )					
					$this->sendback = add_query_arg( array( 'set_task' => 'stock' ), $this->sendback );	
				elseif ( $result == 'completed' )
					$this->sendback = add_query_arg( array( 'completed' => 'stock' ), $this->sendback );	
				else
					$this->sendback = add_query_arg( array( 'error' => '1' ), $this->sendback );		
				
				$this->set_user_screen_error( $exchange_FTP->get_error() );							
				$this->redirect = true;
			}
			if ( isset($_POST['upload_transactions']) ) 
			{			
				$exchange_FTP = new USAM_Exchange_FTP();
				$exchange_FTP->upload_transactions( );
				
				$this->set_user_screen_message( $exchange_FTP->get_message() );	
				$this->set_user_screen_error( $exchange_FTP->get_error() );	
				
				$this->sendback = add_query_arg( array( 'completed' => 'transactions' ), $this->sendback );						
				$this->redirect = true;
			}			
			if ( isset($_POST['unload_data']) ) 
			{			
				$exchange_FTP = new USAM_Exchange_FTP();
				$exchange_FTP->update_all_product_data( );
								
				$this->set_user_screen_message( $exchange_FTP->get_message() );	
				$this->set_user_screen_error( $exchange_FTP->get_error() );	
				
				$this->sendback = add_query_arg( array( 'completed' => 'data' ), $this->sendback );						
				$this->redirect = true;
			}	
			if ( isset($_POST['print_product']) ) 
			{			
				$exchange_FTP = new USAM_Exchange_FTP();
				$product = $exchange_FTP->find_the_missing_products( );	
				$this->set_user_screen_error( $exchange_FTP->get_error(), 'ftp' );	
				include( USAM_FILE_PATH . '/admin/menu-page/exchange/include/print-product.php' );			
				exit;
			}	
			if ( isset($_POST['insert_product']) ) 
			{			
				$exchange_FTP = new USAM_Exchange_FTP();
				$product_ids = $exchange_FTP->insert_the_missing_products( );	
				$this->set_user_screen_error( $exchange_FTP->get_error(), 'ftp' );	

				$this->sendback = add_query_arg( array( 'add_product' => count($product_ids) ), $this->sendback );	
			}			
			
		}
	}	
	
	public function display() 
	{			
		$default = array( 'type_price' => '', 'file_price' => '', 'column_price' => 'code_product,type_price,price', 'file_price_pereozenka' => '', 'file_all_stock' => '', 'column_stock' => "code_product,code_warehouse,stock", 'file_stock' => '', 'min_stock' => '', 'products_not_found' => '', 'where_load_stock' => '', 'file_reserve' => '', 'file_receipt' => '', 'column_order' => 'code_product,price,code_warehouse,quantity', 'code_warehouse' => '', 'file_sale_encoding' => '', 'file_upload_transactions' => '', 'column_transactions' => "order,transactid,date", 'file_data' => '', 'column_data' => "code_product,barcode", 'search_products' => array( 'stock' => '', 'columns' => "code_product,stock" ) );
		
		$exchange_ftp = get_option( 'exchange_ftp_settings', array() );	
		$exchange_ftp = array_merge( $default, $exchange_ftp );			
				
		$ftp_settings = get_option( 'usam_ftp_settings', array( 'host' => '', 'port' => '', 'timeout' => '', 'user' => '', 'pass' => '', 'mode' => 1 ) );
		if ( !empty($ftp_settings['pass'])  )
		{
			$pass = '******';
		}			
		else
			$pass = '';
		?>			
		<form method='post' action='' id='usam-page-tabs-form'>
			<?php $this->nonce_field();	?>
			<table class = "export_table">	
				<tr>	
					<td>	
						<h3><?php _e( 'Настройка подключения к FTP' , 'usam' ); ?></h3>
						<table class = "t_data">
							<tr>				
								<td><label for='host'><?php _e( 'FTP' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "host" name="ftp_settings[host]" value="<?php echo $ftp_settings['host'] ?>"/></td>
							</tr>
							<tr>				
								<td><label for='ftp_port'><?php _e( 'Порт' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_port" name="ftp_settings[port]" value="<?php echo $ftp_settings['port'] ?>"/></td>
							</tr>
							<tr>				
								<td><label for='ftp_timeout'><?php _e( 'Таймаут соединения' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_timeout" name="ftp_settings[timeout]" value="<?php echo $ftp_settings['timeout'] ?>"/></td>
							</tr>
							<tr>				
								<td><label for='ftp_user'><?php _e( 'Имя пользователя' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_user" name="ftp_settings[user]" value="<?php echo $ftp_settings['user'] ?>"/></td>
							</tr>
							<tr>				
								<td><label for='ftp_pass'><?php _e( 'Пароль' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_pass" name="ftp_settings[pass]" value="<?php echo $pass; ?>"/></td>
							</tr>
							<tr>				
								<td><label for='mode_button'><?php _e( 'Режим' , 'usam' ); ?>:</label></td>
								<td>	
									<input type='radio' value='0' name='ftp_settings[mode]' id='mode_button1' <?php checked($ftp_settings['mode'],0); ?>/> 		
									<label for='mode_button1'><?php _e( 'Активный', 'usam' ); ?></label> &nbsp;
									<input type='radio' value='1' name='ftp_settings[mode]' id='mode_button2' <?php checked($ftp_settings['mode'],1); ?>/>
									<label for='mode_button2'><?php _e( 'Пассивный', 'usam' ); ?></label>	
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="ftp_test" class="button button-primary" value="<?php _e( 'Проверить соединение', 'usam' ) ?>"/></td>																	
							</tr>
						</table>
					</td>
					<td>	
						<h3><?php _e( 'Настройка файлов обмена', 'usam' ); ?></h3>					
						<table class = "t_data">
							<tr>
								<td><label for='file_export_encoding'><?php esc_html_e( 'Кодировка файла экспорта' , 'usam' ); ?>: </label></td>
								<td>
									<?php
										$encoding1 = '';
										$encoding2 = '';
										if ( empty($exchange_ftp['file']['export_encoding']) || $exchange_ftp['file']['export_encoding'] == 'windows-1251') 
											$encoding1 = 'selected="selected"';
										else
											$encoding2 = 'selected="selected"';
									?>
									<select id='file_export_encoding' name='ftp[file][export_encoding]'>						
										<option value='utf-8' <?php echo $encoding2 ?>>utf-8</option>
										<option value='windows-1251' <?php echo $encoding1 ?>>windows-1251</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label for='file_import_encoding'><?php esc_html_e( 'Кодировка файла импорта' , 'usam' ); ?>: </label></td>
								<td>
									<?php
										$encoding1 = '';
										$encoding2 = '';
										if ( empty($exchange_ftp['file']['import_encoding']) || $exchange_ftp['file']['import_encoding'] == 'windows-1251') 
											$encoding1 = 'selected="selected"';
										else
											$encoding2 = 'selected="selected"';
									?>
									<select id='file_import_encoding' name='ftp[file][import_encoding]'>						
										<option value='utf-8' <?php echo $encoding2 ?>>utf-8</option>
										<option value='windows-1251' <?php echo $encoding1 ?>>windows-1251</option>
									</select>
								</td>
							</tr>
							<tr>				
								<?php $separator = !empty($exchange_ftp['file']['separator'])?$exchange_ftp['file']['separator']:","; ?>
								<td><label for='separator'><?php _e( 'Разделитель' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "separator" size="1" style = "width: 20px;" maxlength="1" name="ftp[file][separator]" value="<?php echo $separator; ?>"/></td>
							</tr>												
						</table>
					</td>
				</tr>
				<tr>	
					<td>	
						<h3><?php _e('Загрузка цен','usam'); ?></h3>						
						<table>
						<tr>
							<td><?php _e('В какой тип цены загружать','usam'); ?>:</td>
							<td><?php usam_get_select_prices( $exchange_ftp['type_price'],  array( 'name' => 'ftp[type_price]' ) ); ?></td>
						</tr>			
						<tr>							
							<td><label for='ftp_price'><?php esc_html_e( 'Путь к файлу с ценами' , 'usam' ); ?>: </label></td>
							<td><input class = "ftp_parent" type="text" id = "ftp_price" name="ftp[file_price]" value="<?php echo $exchange_ftp['file_price'] ?>"/></td>
						</tr>
						<tr>							
							<td><label for='ftp_column'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
							<td><input class = "ftp_parent" type="text" id = "ftp_column" name="ftp[column_price]" value="<?php echo $exchange_ftp['column_price']; ?>"/></td>
						</tr>
						<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: code_product - внешний код товара, type_price - код цены, price - цена', 'usam'); ?></p></td></tr>
						<tr>
							<td><label for='file_price_pereozenka'><?php esc_html_e( 'Путь к папке с ценами измененными в течении дня' , 'usam' ); ?>: </label></td>
							<td><input class = "ftp_parent" type="text" id = "file_price_pereozenka" name="ftp[file_price_pereozenka]" value="<?php echo $exchange_ftp['file_price_pereozenka'] ?>"/></td>
						</tr>						
						<tr>		
							<td><span><?php _e('Опубликовать товар после обновления?','usam') ?> </span></td>
							<td class = "checked_button">
								<input type='radio' value='0' name='publish' id='hide_checked_button1'/> 		
								<label for='hide_checked_button1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
								<input type='radio' value='1' name='publish' id='hide_checked_button2' checked ='checked'/>
								<label for='hide_checked_button2'><?php _e( 'Нет', 'usam' ); ?></label>
							</td>
						</tr>
						<tr>					
							<td colspan='2'>
								<input type="submit" name="price_update" id="upload_price" class="button button-primary" value="<?php _e( 'Обновить цены', 'usam' ) ?>"/>
							</td>
						</tr>									
						</table>						
					</td>
					<td>	
						<h3><?php esc_html_e('Загрузка остатков', 'usam'); ?></h3>
						<table>
							<tr>
								<td><label for='ftp_stock_all'><?php esc_html_e( 'Путь к файлу с остатками' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_stock_all" name="ftp[file_all_stock]" value="<?php echo $exchange_ftp['file_all_stock'] ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Путь к файлу со всеми остатками', 'usam'); ?></p></td></tr>
							<tr>								
								<td><label for='ftp_column'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_column" name="ftp[column_stock]" value="<?php echo $exchange_ftp['column_stock']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: code_product - внешний код товара, code_warehouse - внешний код склада, stock - остаток', 'usam'); ?></p></td></tr>
							<tr>
								<td><label for='file_stock'><?php esc_html_e( 'Путь к папке с остатками' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "file_stock" name="ftp[file_stock]" value="<?php echo $exchange_ftp['file_stock'] ?>"/></td>	
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Если необходим обмен остатков в течении дня укажите папку', 'usam'); ?></p></td></tr>
							<tr>
								<td><label for='min_stock'><?php esc_html_e( 'Минимальный остаток' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "min_stock" name="ftp[min_stock]" value="<?php echo $exchange_ftp['min_stock']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Если необходимо загрузить остаток более определенного количества, то укажите количество здесь', 'usam'); ?></p></td></tr>
							<tr>
								<td>					
								<?php
								$encoding1 = '';
								$encoding2 = '';
								if ( $exchange_ftp['products_not_found'] == 'skip') 
									$encoding1 = 'selected="selected"';
								else
									$encoding2 = 'selected="selected"';
								?>
								<label for='products_not_found'><?php esc_html_e( 'Обработка товаров' , 'usam' ); ?>: </label></td>
								<td>
									<select name='ftp[products_not_found]' id='products_not_found'>						
										<option value='skip' <?php echo $encoding1 ?>><?php esc_html_e('Пропустить', 'usam'); ?></option>
										<option value='out_of_stock' <?php echo $encoding2 ?>><?php esc_html_e('Снять с продажи', 'usam'); ?></option>
									</select>
								</td>						
							</tr>						
							<tr>
								<td><label for='where_load_stock'><?php esc_html_e( 'Куда загружать остатки' , 'usam' ); ?>: </label></td>
								<td>
									<select name='ftp[where_load_stock]' id='where_load_stock'>											
										<option value='any' <?php echo ('any' == $exchange_ftp['where_load_stock']?'selected="selected"':'') ?>>-<?php esc_html_e('На склады как в файле', 'usam'); ?>-</option>
										<?php											
										$stores = usam_get_stores( array( 'fields' => 'id, title' ) );	
										foreach ($stores as $storage)
										{														
											?>
											<option value="<?php echo $storage->id; ?>" <?php echo ($storage->id == $exchange_ftp['where_load_stock']?'selected="selected"':'') ?>><?php echo $storage->title; ?></option>
											<?php
										}
										?>		
									</select>
								</td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Для того чтобы загружать на склады разные склады, вам нужно создать необходимые склады. Указать код склада как в загружаемом файле. Т. е. все остатки найденные в файле с кодом 12345 будут занесены на склад вашего интернет магазина с таким же кодом', 'usam'); ?></p></td></tr>
							<tr>
								<td colspan="2"><input type="submit" name="stock_update" id="Upload_p" class="button button-primary" value="<?php _e( 'Обновить остатки', 'usam' ) ?>"/></td>
							</tr>
						</table>
					</td>	
				</tr>
				<tr>	
					<td>	
						<h3><?php esc_html_e('Заказы', 'usam'); ?></h3>
						<table class = "t_data">
							<tr>				
								<td><label for='file_reserve'><?php esc_html_e( 'Путь к файлу с резервами' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "file_reserve" name="ftp[file_reserve]" value="<?php echo $exchange_ftp['file_reserve'] ?>"/></td>
							</tr>
							<tr>
								<td><label for='file_receipt'><?php esc_html_e( 'Путь к файлу с продажами' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "file_receipt" name="ftp[file_receipt]" value="<?php echo $exchange_ftp['file_receipt'] ?>"/></td>
							</tr>
							<tr>								
								<td><label for='ftp_column'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_column" name="ftp[column_order]" value="<?php echo $exchange_ftp['column_order']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: code_product - внешний код товара, code_warehouse - внешний код склада, price - цена, quantity - количество', 'usam'); ?></p></td></tr>
							<tr>	
								<td><label for='code_warehouse'><?php esc_html_e( 'Склад, для выгрузки продаж' , 'usam' ); ?>: </label></td>
								<td>
									<select name='ftp[code_warehouse]'>
									<?php
									$storages = usam_get_stores( );		
									foreach ( $storages as $storage )
									{	
										?><option value='<?php echo $storage->code; ?>' <?php echo $exchange_ftp['code_warehouse']==$storage->code ? 'selected="selected"':''; ?>><?php echo $storage->title; ?></option><?php
									}
									?>	
									</select>	
								</td>
							</tr>
							<tr>
								<td><label for='file_sale_encoding'><?php esc_html_e( 'Кодировка выходного файла' , 'usam' ); ?>: </label></td>
								<td>
									<?php
										$encoding1 = '';
										$encoding2 = '';
										if ( $exchange_ftp['file_sale_encoding'] == 'windows-1251') 
											$encoding1 = 'selected="selected"';
										else
											$encoding2 = 'selected="selected"';
									?>
									<select name='ftp[file_sale_encoding]'>						
										<option value='utf-8' <?php echo $encoding2 ?>>utf-8</option>
										<option value='windows-1251' <?php echo $encoding1 ?>>windows-1251</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label for='upload_transactions'><?php esc_html_e( 'Путь к файлам с документами оплаты' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "upload_transactions" name="ftp[file_upload_transactions]" value="<?php echo $exchange_ftp['file_upload_transactions'] ?>"/></td>
							</tr>	
							<tr>								
								<td><label for='ftp_column_transactions'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_column_transactions" name="ftp[column_transactions]" value="<?php echo $exchange_ftp['column_transactions']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: order - номер заказа, transactid - номер транзакции, date - дата транзакции', 'usam'); ?></p></td></tr>
						</table>
						<input type="submit" name="upload_transactions" id="upload_transactions_submit" class="button button-primary" value="<?php _e( 'Загрузить документы оплаты', 'usam' ) ?>"/>
					</td>		
					<td>	
						<h3><?php esc_html_e( 'Загрузка данных товаров' , 'usam' ); ?></h3>
						<table class = "t_data">
							<tr>				
								<td><label for='file_data'><?php esc_html_e( 'Путь к файлу с данными' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "file_data" name="ftp[file_data]" value="<?php echo $exchange_ftp['file_data'] ?>"/></td>
							</tr>
							<tr>
								<td><label for='ftp_column_data'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_column_data" name="ftp[column_data]" value="<?php echo $exchange_ftp['column_data']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: code_product - внешний код товара, barcode - штрих-код', 'usam'); ?></p></td></tr>	
							<tr>
								<td><input type="submit" name="unload_data" id="Upload_p" class="button button-primary" value="<?php _e( 'Обновить', 'usam' ) ?>"/></td>
								<th><?php esc_html_e( 'Обновить штрих-код товара' , 'usam' ); ?></th>									
							</tr>								
						</table>
					</td>							
				</tr>
				<tr>
					<td>	
						<h3><?php _e( 'Автоматически загрузка', 'usam' ); ?></h3>					
						<ul>
						<li  class = "checked_button">	
							<span><?php _e('Автоматически загружать цены с FTP?','usam') ?> </span>
							<input type='radio' value='1' name='ftp_download_prices' id='price_button1' <?php checked( get_option('usam_ftp_download_prices', 0) ,1); ?>/> 		
							<label for='price_button1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
							<input type='radio' value='0' name='ftp_download_prices' id='price_button2'<?php checked( get_option('usam_ftp_download_prices', 0) ,0); ?>/>
							<label for='price_button2'><?php _e( 'Нет', 'usam' ); ?></label>						
						</li>					
						<li  class = "checked_button">								
							<span><?php _e('Автоматически загружать остатки с FTP?','usam') ?> </span>
							<input type='radio' value='1' name='ftp_download_balances' id='ftp_download_balances1' <?php checked( get_option('usam_ftp_download_balances', 0) ,1); ?>/> 		
							<label for='ftp_download_balances1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
							<input type='radio' value='0' name='ftp_download_balances' id='ftp_download_balances2' <?php checked( get_option('usam_ftp_download_balances', 0),0); ?>/>
							<label for='ftp_download_balances2'><?php _e( 'Нет', 'usam' ); ?></label>
						</li><li  class = "checked_button">		
							<span><?php _e('Автоматически выгружать резервы?','usam') ?> </span>
							<input type='radio' value='1' name='ftp_export_order' id='sale_button1' <?php checked( get_option('usam_ftp_export_order', 0),1); ?>/> 		
							<label for='sale_button1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
							<input type='radio' value='0' name='ftp_export_order' id='psale_button2' <?php checked( get_option('usam_ftp_export_order', 0),0); ?>/>
							<label for='sale_button2'><?php _e( 'Нет', 'usam' ); ?></label>		
						</li>		
						</ul>
					</td>
					<td>	
						<h3><?php esc_html_e( 'Поиск товаров, которых нет на сайте' , 'usam' ); ?></h3>
						<table class = "t_data">										
							<tr>				
								<td><label for='search_products_stock'><?php esc_html_e( 'Минимальный остаток' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "search_products_stock" name="ftp[search_products][stock]" value="<?php echo $exchange_ftp['search_products']['stock'] ?>"/></td>
							</tr>
							<tr>
								<td><label for='ftp_search_products_columns'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label></td>
								<td><input class = "ftp_parent" type="text" id = "ftp_search_products_columns" name="ftp[search_products][columns]" value="<?php echo $exchange_ftp['search_products']['columns']; ?>"/></td>
							</tr>
							<tr><td class ="description" colspan="2"><p><?php esc_html_e('Укажите порядок колонок в файле. Возможные: code_product - внешний код товара, stock - остаток', 'usam'); ?></p></td></tr>	
							<tr>
								<td><input type="submit" name="print_product" id="Upload_p" class="button button-primary" value="<?php _e( 'Распечатать', 'usam' ) ?>"/></td>
								<th></th>	
							</tr>	
							<tr>
								<td><input type="submit" name="insert_product" id="Upload_p" class="button button-primary" value="<?php _e( 'Добавить товары на сайт', 'usam' ) ?>"/></td>
								<th></th>	
							</tr>								
						</table>
					</td>		
				</tr>
			</table>
			<div class = "submit_box">
				<input type="submit" name="save" id="Upload_p" class="button button-primary" value="<?php _e( 'Сохранить', 'usam' ) ?>"/>		
			</div>
		</form>
		<?php			
	}
	
	public function get_message()
	{		
		$message = '';
		switch( $this->tab )
		{			
			case 'ftp':				
				if( isset($_REQUEST['completed']) )
					switch( $_REQUEST['completed'] )
					{			
						case 'price':	
							$message = esc_html__( 'Цены обновлены.', 'usam' );
						break;								
						case 'stock':	
							$message = esc_html__( 'Запас обновлен.', 'usam' );
						break;
						case 'transactions':	
							$message = esc_html__( 'Документы оплаты обновлены.', 'usam' );
						break;
						case 'data':	
							$message = esc_html__( 'Данные обновлены.', 'usam' );
						break;
					}	
				if( isset($_REQUEST['set_task']) )
					$message = esc_html__( 'Задача добавлена. Результаты смотри в диспетчере задач', 'usam' );
			break;		
		}		
		return $message;
	} 	
}