<?php
class USAM_Tab_mysql extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Загрузка товаров с UNIVERSAM', 'usam'), 'description' => 'Здесь Вы можете загружать товары из другой базы UNIVERSAM.' );	
	}	
	
	public function display()
	{		
		$settings_mysql = get_option( 'usam_exchange_mysql_settings', array('user' => '', 'pass' => '', 'db' => '', 'prefix' => 'wp', 'host' => '') );	
		?>		
		<form method='post'>			
			<table  class = "export_table">	
				<tr>	
					<td><label for='mysql_user'><?php esc_html_e( 'Имя пользователя' , 'usam' ); ?>: </label></td>
					<td><input class = "mysql_parent" type="text" id = "mysql_user" name="user" value="<?php echo $settings_mysql['user'] ?>"/></td>
				</tr>
				<tr>				
					<td><label for='mysql_pass'><?php esc_html_e( 'Пароль' , 'usam' ); ?>: </label></td>
					<td><input class = "mysql_parent" type="text" id = "mysql_pass" name="pass" value="<?php echo $settings_mysql['pass'] ?>"/></td>
				</tr>
				<tr>				
					<td><label for='db'><?php esc_html_e( 'Имя базы' , 'usam' ); ?>: </label></td>
					<td><input class = "mysql_parent" type="text" id = "db" name="db" value="<?php echo $settings_mysql['db'] ?>"/></td>
				</tr>
				<tr>				
					<td><label for='mysql_host'><?php esc_html_e( 'Префикс' , 'usam' ); ?>: </label></td>
					<td><input class = "mysql_parent" type="text" id = "mysql_host" name="prefix" value="<?php echo $settings_mysql['prefix'] ?>"/></td>
				</tr>
				<tr>				
					<td><label for='mysql_host'><?php esc_html_e( 'Хост' , 'usam' ); ?>: </label></td>
					<td><input class = "mysql_parent" type="text" id = "mysql_host" name="host" value="<?php echo $settings_mysql['host'] ?>"/></td>					
				</tr>						
			</table>
			<?php
			global $current_screen;	
			wp_nonce_field('usam-'.$current_screen->id,'usam_name_nonce_field');
			?>
			<div class = "submit_box">
				<input type="submit" name="save" id="Upload_p" class="button button-primary" value="<?php _e( 'Сохранить', 'usam' ) ?>"/>		
			</div>
		</form>
		<?php
	}
	
	protected function callback_submit()
	{		
		if ( !empty($_POST['user']) )
		{	
			$mysql['user'] = sanitize_text_field($_POST['user']);
			$mysql['pass'] = sanitize_text_field($_POST['pass']);
			$mysql['db']   = sanitize_title($_POST['db']);
			$mysql['prefix'] = sanitize_title($_POST['prefix']);
			$mysql['host']  = sanitize_text_field($_POST['host']);
			
			update_option( 'usam_exchange_mysql_settings', $mysql );		
			$this->sendback = add_query_arg( array( 'update' => '1' ), $this->sendback );			
			$this->redirect = true;			
		}			
	}		
}