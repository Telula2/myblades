<?php
class USAM_Tab_file extends USAM_Tab
{
	private $tables;
	private $step;
	private $import_settings;
	private $file_path;
	private $format_file = '';
	private $header_action = array( );
	private $url   = "";
	private $count_update = 0;
	private $error;
	private $message;	
	private $type_exchange = '';	
	
	public function __construct() 
	{	
		$this->url = admin_url( "admin.php?page=exchange&tab=file" );		
		
		if ( isset($_POST['file']) )
		{
			$this->import_settings = stripslashes_deep($_POST['file']);
			update_option( 'usam_file_import_settings',  $this->import_settings );
		}
		else
			$this->import_settings = get_option( 'usam_file_import_settings',  array() );	
		
		$this->header = array( 'title' => __('Обмен данными при помощи файлов', 'usam'), 'description' => __('Здесь Вы можете загружать цены и остатков из файлов с вашего компьютера.', 'usam') );			
		$this->header_action = array( 1=>__('Выбор файла','usam'), 2 =>__('Просмотр файла','usam'), 4=>__('Импорт и результат','usam') );		
		
		$this->step = empty( $_REQUEST['step'] ) ? 1 : (int) $_REQUEST['step'];
		if ( $this->step < 1 || $this->step > 4 )
			$this->step = 1;
		if ( isset($_REQUEST['export_product']) )
		{
			$column_name  = array( 'title' => __('Название товара','usam'), 'price' => __('Цена','usam'), 'url' => __('Ссылка','usam'), 'discont' => __('Скидка','usam'),'sku' => __('Артикул','usam'),'stock' => __('Запас','usam'), 'image' => __('Изображение','usam'), 'cat' => __('Категория','usam') );
			$this->export_products( 'universam', $column_name );
		}
		if ( isset($_REQUEST['yandex_market']) )
		{
			$column_name = array( 'id','available','url','price','currencyId','category','picture','name');
			$this->export_products( 'yandex_market', $column_name );
		}
		switch ( $this->step ) 
		{
			case 2:							
				if ( isset($_FILES['file_price']) )
				{					
					$file = $_FILES['file_price'];	
					$extension = usam_get_extension( $file['name'] );					
					$filename = sanitize_title( basename( $file['name'],'.'.$extension ) );						
					$this->file_path = USAM_FILE_DIR . $filename.'.'.$extension;
					if ( move_uploaded_file( $file['tmp_name'], $this->file_path ) )
						set_transient( 'usam_settings_loading_prices_file', $this->file_path );					
				}	
				else
					$this->file_path = get_transient( 'usam_settings_loading_prices_file');
			break;
			case 3:
				$this->file_path = get_transient( 'usam_settings_loading_prices_file');							
			break;
			case 4:
				$this->file_path = get_transient( 'usam_settings_loading_prices_file');							
			break;
		}	
		if ( !empty($_REQUEST['type']) )
			$this->type_exchange = $_REQUEST['type'];		
		else
			$this->step = 1;
		if ( file_exists( $this->file_path ) )				
		{
			$this->url = add_query_arg( array('step' => $this->step, 'type' => $this->type_exchange) );					
			$extension = usam_get_extension( $this->file_path ); // узнать формат файла		
			switch ( $extension ) 
			{
				case 'xls':	
				case 'xlsx':
					$this->format_file = 'xls';
				break;
				case 'txt':
				case 'csv':					
					$this->format_file = 'txt';
				break;			
				default:				
				break;
			}	
			if ( $this->step == 3 )	
				$this->update();
		}	
		if ( !isset($_GET['step']) && $this->step != 1 || isset($_GET['step']) && $this->step != $_GET['step'] )
			$this->redirect = true;
				
		if ( $this->redirect )
		{ 
			wp_redirect( $this->url );
			exit;
		}	
	}
	
	private function update()
	{	
		$method = $this->format_file.'_update_'. $this->type_exchange;
		$i = 0;
		if ( method_exists( $this, $method ) )		
			$i = $this->$method();			
	
		delete_transient( 'usam_settings_loading_prices_file' );
		unlink($this->file_path); // удалить файл с загруженными ценами
		
		$this->url = add_query_arg( array('step' => 4, 'updated' => $i), $this->url );		
		$this->redirect = true;
	}
	
	private function xls_update_price()
	{			
		$this->tables = usam_read_exel_file( $this->file_path, $this->import_settings['price']['excel']['name_list'] );		
		
		$column_sku = $this->import_settings['price']['excel']['column_sku']; // номер столбца с артикулом
		$column_price = $this->import_settings['price']['excel']['column_price']; // номер столбца с ценой		
		$row_start = empty($this->import_settings['price']['excel']['row_start'])?0:$this->import_settings['price']['excel']['row_start']; // скакой строки начинать		
		
		$price_key = 'price_'. $this->import_settings['price']['type_price'];				
		$publish = !empty($_POST['publish']) ?true:false;
		if ( !empty($this->import_settings['price']['excel']['column_end']) )
			$count = $this->import_settings['price']['excel']['column_end'];
		else
			$count = count( $this->tables );
		
		$j = 0;			
		for ( $i = $row_start; $i <= $count; $i++ )
		{			
			if ( !empty($this->tables[$i][$column_sku] ) )
			{	
				$product_id = usam_get_product_id_by_sku( $this->tables[$i][$column_sku] );
				if ( !empty($product_id) )
				{					
					$prices = array();
					if ( isset($this->tables[$i][$column_price]) ) // колонки с ценой не пуста
						$prices[$price_key] = usam_string_to_float($this->tables[$i][$column_price]);					
					
					if ( $publish )
						wp_publish_post( $product_id );						
					
					usam_edit_product_prices( $product_id, $prices );					
					$j ++;
				}
			}			
		}		
		return $j;
	}
	
	private function xls_update_stock()
	{			
		$this->tables = usam_read_exel_file( $this->file_path, $this->import_settings['price']['excel']['name_list'] );		
		
		$column_sku = $this->import_settings['stock']['excel']['column_sku']; // номер столбца с артикулом
		$column_code = $this->import_settings['stock']['excel']['column_code']; 
		$column_stock = $this->import_settings['stock']['excel']['column_stock'];		
		$row_start = empty($this->import_settings['stock']['excel']['row_start'])?0:$this->import_settings['stock']['excel']['row_start']; // скакой строки начинать	
		$count = !empty($this->import_settings[$this->type_exchange]['excel']['column_end'])?$this->import_settings[$this->type_exchange]['excel']['column_end']: count( $this->tables );		
				
		$publish = !empty($_POST['publish']) ?true:false;		
		
		$j = 0;			
		for ( $i = $row_start; $i < $count; $i++ )
		{	
			if ( !empty($this->tables[$i][$column_sku] ) )
			{	
				$product_id = usam_get_product_id_by_sku( $this->tables[$i][$column_sku] );
				if ( !empty($product_id) )
				{					
					$storage = usam_get_storage($this->tables[$i][$column_code], 'code');
					if ( empty($storage['meta_key']) )
						continue;
					
					usam_update_product_meta($product_id, $storage['meta_key'], $this->tables[$i][$column_stock] );	
					$product_ids[] = $product_id;
					
					if ( $publish )
						wp_publish_post( $product_id );		
					$j ++;
				}
			}			
		}			
		usam_recalculate_stock_products( $product_ids );	
		return $j;
	}
	
	function txt_update_price( $echo_result = false ) 
	{
		ini_set( 'auto_detect_line_endings', 1 );	

		$publish = !empty($_POST['publish']) ?true:false;
		$price_key = 'price_'.$this->import_settings['price']['type_price'];	
	
		$columns = explode(',',$this->import_settings['price']['txt']['column']);
		$columns = array_flip($columns);
					
		$i = 0;
		$data = usam_read_txt_file($this->file_path, $this->import_settings['delimiter'], $this->import_settings['encoding']);			
		foreach($data as $key => $row )		
		{
			if ( !empty($row[$columns['sku']]) )
			{			
				$product_id = usam_get_product_id_by_sku( $row[$columns['sku']] );
				if ( !empty($product_id) )
				{
					$prices = array();
					$prices[$price_key] = $row[$columns['price']];	
										
					if ( $publish )
						wp_publish_post( $product_id );					
					
					usam_edit_product_prices( $product_id, $prices );					
					$i++;
				}
			}		
		}
		return $i;
	}	
	
	private function txt_update_stock()
	{
		ini_set( 'auto_detect_line_endings', 1 );	

		$publish = !empty($_POST['publish']) ?true:false;		
	
		$columns = explode(',',$this->import_settings['stock']['txt']['column']);
		$columns = array_flip($columns);
					
		$i = 0;
		$data = usam_read_txt_file($this->file_path, $this->import_settings['delimiter'], $this->import_settings['encoding']);		
		$product_ids = array();
		foreach($data as $key => $row )		
		{
			if ( !empty($row[$columns['sku']]) )
			{			
				$product_id = usam_get_product_id_by_sku( $row[$columns['sku']] );
				if ( !empty($product_id) )
				{
					$storage = usam_get_storage($row[$columns['code']], 'code');
					if ( empty($storage['meta_key']) )
						continue;
			
					usam_update_product_meta($product_id, $storage['meta_key'], $row[$columns['stock']] );	
					$product_ids[] = $product_id;
					
					if ( $publish )
						wp_publish_post( $product_id );						
					
					$i++;
				}
			}		
		}
		usam_recalculate_stock_products( $product_ids );		
		return $i;			
	}		
	
	private function display_result()
	{			
		if ( !empty($_GET['updated']) )
		{
			$i = absint( $_GET['updated']);
			?>
			<div class="result_update">					
				<h3><?php _e('Результаты работы','usam'); ?></h3>
				<p><?php printf( _n( 'Обновлен %s товар ', 'Обновленно %s товаров', $i, 'usam' ), $i); ?></p>
			</div>
			<?php	
		}
	}	
	
	
	private function print_file()
	{	
		ini_set( 'auto_detect_line_endings', 1 );	 
		
		$method = $this->format_file.'_display_file_to_view';
		if ( method_exists( $this, $method ) )	
			$content = $this->$method();
		else
			$content = '';
		$out  = 'Файл: '.$this->file_path.'<br>';
		$out .= "<form id='purchase-logs-filter' method='POST' action='".$this->url."'>
		<ul>
		<li>			
		<div class = 'submit_box'>
			<input type='submit' name='price_update' id='Upload_p' class='button button-primary' value='".__( 'Продолжить', 'usam' )."'/>
		</div>
		<input type='hidden' name='step' value='3'/>
		<input type='hidden' name='type' value='".$this->type_exchange."'/>";			
		if ( isset($_POST['publish']) && $_POST['publish'] == 0 )
			$out .="<input type='hidden' name='publish' value='0'/>";	
		$out .="</form>";		
		$out .= "<p>".__( 'Посмотрите правильно ли вы указали столбцы. Столбцы, которые вы выбрали, выделены.', 'usam' )."</p>		
		<table class = 'print_file_table' >";
		$out .= $content;
		$out .= "</table>";			
		echo $out;
	}	
	
	//Вывести содержимое файла для просмотра
	private function txt_display_file_to_view( ) 
	{
		ini_set("max_execution_time", "60");		
		
		$columns = !empty($this->import_settings[$this->type_exchange]['txt']['column'])?$this->import_settings[$this->type_exchange]['txt']['column']:'';		
		$columns = explode(',',$columns);
		
		$data = usam_read_txt_file($this->file_path, $this->import_settings['delimiter'], $this->import_settings['encoding']);					
		
		$out = '<thead>';
		$column_name  = array( 'title' => __('Название товара','usam'), 'price' => __('Цена','usam'), 'sku' => __('Артикул','usam'),'stock' => __('Запас','usam'), 'cat' => __('Категория','usam'), 'code' => __('Код склада','usam') );	
		$out .= '<tr>';		
		foreach($columns as $column )
		{			
			if ( !empty($column_name[$column]) )
				$name = $column_name[$column];	
			else
				$name = '';
			$out .= '<th>'.$name.'</th>';		
		}
		$out .= '</tr></thead>
		<tbody>';
		$i = 0;
		$count = count($columns);
		foreach($data as $key => $row )		
		{							
			$out .= "<tr>";
			$j = 0;
			foreach($row as $value )
			{				
				$j++;
				$out  .= "<td>".$value."</td>";
				if ( $j == $count )
					break;
			}
			$out .= "</tr>";
			$i++;	
			if ( $i > 20 )
				break;
		}
		$out .= "</tbody>";
		return $out;
	}	
	
	//Вывести содержимое файла excel для просмотра
	private function xls_display_file_to_view( ) 
	{		
		$this->tables = usam_read_exel_file( $this->file_path, $this->import_settings['price']['excel']['name_list'] );		
	
		$row_start = empty($this->import_settings[$this->type_exchange]['excel']['row_start'])?0:$this->import_settings[$this->type_exchange]['excel']['row_start']; // скакой строки начинать	
		$count = !empty($this->import_settings[$this->type_exchange]['excel']['column_end'])?$this->import_settings[$this->type_exchange]['excel']['column_end']: count( $this->tables );

		$colums = array();
		foreach($this->import_settings[$this->type_exchange]['excel'] as $key => $data )
		{
			if ( stripos($key, 'column_' ) !== false )
			{
				$str = explode('_',$key);
				$value = !empty($data)?$data:0;
				$colums[$value] = $str[1];
			}
		}	
		$out = '<thead><tr>';		
		for ( $i = $row_start; $i == $row_start; $i++ )
		{	
			$out .='<th></th>';
			foreach($this->tables[$i] as $key => $data )
			{							
				$out .='<th>'.sprintf( __( 'Колонка №%s', 'usam' ), $key );
				if ( isset( $colums[$key]) )		
				{
					if ( 'sku' == $colums[$key] )				
						$out .= '<br>'.__( 'Артикул', 'usam' );					
					elseif ( 'price' == $colums[$key] )
						$out .= '<br>'.__( 'Цена', 'usam' );		
					elseif ( 'code' == $colums[$key] )
						$out .= '<br>'.__( 'Код склада', 'usam' );		
					elseif ( 'stock' == $colums[$key] )
						$out .= '<br>'.__( 'Остаток', 'usam' );	
				}
				$out .='</th>';
			}			
		}
		$out .=' </tr></thead><tbody>';				
		for ( $i = $row_start; $i < $count; $i++ )
		{	
			$out .= "<tr>";
			$out .='<th>'.sprintf( __( 'Строка №%s', 'usam' ), $i ).'</th>';
			foreach($this->tables[$i] as $key => $data )
			{	
				$out .=  "<td>".$data."</td>";		
			}
			$out .= "</tr>";
		}
		$out .= "</tbody>";
		return $out;
	}	
	
	private function display_default() 
	{	
		$price = array( 'excel' => array( 'name_list' => '', 'column_sku' => '', 'column_price' => '', 'row_start' => '', 'column_end' => '' ), 'txt' => array('column' => 'sku,price'), 'type_price' => '', );
		$stock = array( 'excel' => array( 'name_list' => '', 'column_sku' => '', 'column_code' => '', 'column_stock' => '', 'row_start' => '', 'column_end' => '' ), 'txt' => array('column' => 'sku,code,stock') );
		$default = array('price' => $price, 'stock' => $stock, 'delimiter' => ';', 'encoding' => 'utf-8' );		
		$this->import_settings = array_merge( $default, $this->import_settings );
		$import_price = $this->import_settings['price'];
		$import_stock = $this->import_settings['stock'];
		?>
		<table class = "export_table">
			<tr>
			<td>
				<h3><?php _e( 'Обновление цены из файла с вашего компьютера.', 'usam' ); ?></h3>		
				<form class="file_price_update" method="POST" enctype="multipart/form-data" action="<?php echo $this->url; ?>">			
					<div class = "excel">		
					<h4><?php _e( 'Загрузка из файла excel.', 'usam' ); ?></h4>
					<ul>
					<li>
					<label class ="name" for='Excel_name_list'><?php esc_html_e( 'Название листа' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" id = "Excel_name_list" name="file[price][excel][name_list]" value="<?php echo $import_price['excel']['name_list'] ?>"/><?php echo usam_help_tip( __( 'Укажите название листа. Если будет не заполнено, то возмет первый лист' , 'usam' ) ); ?>					
					</li><li>
					<label class ="name" for='Excel_column_sku'><?php esc_html_e( 'Номер колонки с артиклом' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" name="file[price][excel][column_sku]" value="<?php echo $import_price['excel']['column_sku'] ?>"/><br>
					</li><li>
					<label class ="name" for='excel_column_price'><?php esc_html_e( 'Номер колонки с ценой' , 'usam' ); ?>: </label>
					<input class = "excel_parent" id='excel_column_margin' type="text" name="file[price][excel][column_price]" value="<?php echo $import_price['excel']['column_price'] ?>"/>
					</li><li>					
					<label class ="name" for='excel_column_number'><?php esc_html_e( 'Номер строки откуда начинать' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" id='excel_column_number' name="file[price][excel][row_start]" value="<?php echo $import_price['excel']['row_start'] ?>"/>
					</li>		
					<li>
					<label class ="name" for='column_end'><?php esc_html_e( 'Номер строки где закончить' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" id='column_end' name="file[price][excel][column_end]" value="<?php echo $import_price['excel']['column_end'] ?>"/>
					</li>						
					</ul>
					</div>		
					<div class = "excel">		
					<h4><?php _e( 'Загрузка из текстового файла', 'usam' ); ?></h4>
					<ul>
					<li>					
					<label class ="name" for='excel_column_price'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label>
					<input class = "excel_parent" id='excel_column_margin' type="text" name="file[price][txt][column]" value="<?php echo $import_price['txt']['column'] ?>"/>
					</li><li>
					<label class ="name" for='Excel_column_sku'><?php esc_html_e( 'Разделитель' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" name="file[delimiter]" size='10' maxlength="1" value="<?php echo $this->import_settings['delimiter'] ?>"/><br>
					</li><li>
					<label class="name" for='file_sale_encoding'><?php esc_html_e( 'Кодировка выходного файла' , 'usam' ); ?>: </label>
						<select name='file[encoding]'>						
							<option value='utf-8' <?php selected($this->import_settings['encoding'], 'utf-8') ?>>utf-8</option>
							<option value='windows-1251' <?php selected($this->import_settings['encoding'], 'windows-1251') ?>>windows-1251</option>
						</select>
					</li>
					</ul>
					</div>					
					<table>
						<tr>
							<td><?php _e('Тип цены','usam'); ?>:</td>
							<td><?php usam_get_select_prices( $import_price['type_price'], array( 'name' => "file[price][type_price]") ); ?></td>
						</tr>
						<tr>
							<td><?php _e('Опубликовать товар после обновления?','usam'); ?>:</td>
							<td>
								<input type='radio' value='0' name='publish' id='hide_checked_button1'/> 		
								<label for='hide_checked_button1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
								<input type='radio' value='1' name='publish' id='hide_checked_button2' checked ='checked'/>
								<label for='hide_checked_button2'><?php _e( 'Нет', 'usam' ); ?></label>
							</td>
						</tr>
					</table>					
					<div class = "excel_name_list">
						<label class ="name" for='excel_name_list'><?php esc_html_e( 'Выберете файл excel, txt или csv' , 'usam' ); ?>: </label>	
						<input class = "file_price" type="file" name="file_price" value=""/><br>			
					</div>
					<div class = "submit_box">
						<input type="submit" name="price_update" id="Upload_p" class="button button-primary" value="<?php _e( 'Продолжить', 'usam' ) ?>"/>
					</div>
					<input type="hidden" name="step" value="2"/>
					<input type="hidden" name="type" value="price"/>
				</form>	
			</td>
			<td>			
			<h3><?php _e( 'Обновление остатков из файла с вашего компьютера.', 'usam' ) ?></h3>
			<form class="file_price_update" method="POST" enctype="multipart/form-data" action="<?php echo $this->url; ?>">			
				<div class = "excel">		
				<h4>Настройки для файла excel</h4>
				<ul>
				<li>
				<label class ="name" for='Excel_name_list'><?php esc_html_e( 'Название листа' , 'usam' ); ?>: </label>
				<input class = "excel_parent" type="text" id = "Excel_name_list" name="file[stock][excel][name_list]" value="<?php echo $import_stock['excel']['name_list'] ?>"/><br>
				</li><li>
				<label class ="name" for='Excel_column_sku'><?php esc_html_e( 'Номер колонки с артиклом' , 'usam' ); ?>: </label>
				<input class = "excel_parent" type="text" name="file[stock][excel][column_sku]" value="<?php echo $import_stock['excel']['column_sku'] ?>"/><br>
				</li><li>
				<label class ="name" for='Excel_column_price'><?php esc_html_e( 'Код склада' , 'usam' ); ?>: </label>
				<input class = "excel_parent" type="text" name="file[stock][excel][column_code]" value="<?php echo $import_stock['excel']['column_code'] ?>"/><br>	
				</li><li>
				<label class ="name" for='Excel_column_price_sale'><?php esc_html_e( 'Номер колонки с остатком' , 'usam' ); ?>: </label>
				<input class = "excel_parent" type="text" name="file[stock][excel][column_stock]" value="<?php echo $import_stock['excel']['column_stock'] ?>"/>
				<br>	
				</li><li>
				<label class ="name" for='Excel_column_number'><?php esc_html_e( 'Номер сторки откуда начинать' , 'usam' ); ?>: </label>
				<input class = "excel_parent" type="text" id='Excel_column_number' name="file[stock][excel][row_start]" value="<?php echo $import_stock['excel']['row_start'] ?>"/><br>						
				</ul>
				</div>
				<div class = "excel">		
					<h4><?php _e( 'Загрузка из текстового файла', 'usam' ); ?></h4>
					<ul>
					<li>					
					<label class ="name" for='excel_column_price'><?php esc_html_e( 'Колонки' , 'usam' ); ?>: </label>
					<input class = "excel_parent" id='excel_column_margin' type="text" name="file[stock][txt][column]" value="<?php echo $import_stock['txt']['column'] ?>"/>
					</li><li>
					<label class ="name" for='Excel_column_sku'><?php esc_html_e( 'Разделитель' , 'usam' ); ?>: </label>
					<input class = "excel_parent" type="text" name="file[delimiter]" value="<?php echo $this->import_settings['delimiter'] ?>"/><br>
					</li><li>
					<label class="name" for='file_sale_encoding'><?php esc_html_e( 'Кодировка выходного файла' , 'usam' ); ?>: </label>
						<select name='file[encoding]'>						
							<option value='utf-8' <?php selected($this->import_settings['encoding'], 'utf-8') ?>>utf-8</option>
							<option value='windows-1251' <?php selected($this->import_settings['encoding'], 'windows-1251') ?>>windows-1251</option>
						</select>
					</li>
					</ul>
					</div>	
				<div class = "checked_button">
					<span><?php _e('Опубликовать товар после обновления?','usam') ?> </span>
					<input type='radio' value='0' name='publish' id='hide_checked_button1'/> 		
					<label for='hide_checked_button1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
					<input type='radio' value='1' name='publish' id='hide_checked_button2' checked ='checked'/>
					<label for='hide_checked_button2'><?php _e( 'Нет', 'usam' ); ?></label>
				</div>
				<div class = "excel_name_list">
					<label for='excel_name_list'><?php esc_html_e( 'Выберете файл excel, txt или csv' , 'usam' ); ?>: </label>	
					<input class = "file_price" type="file" name="file_price" value=""/><br>			
				</div>
				<div class = "submit_box">
					<input type="submit" name="price_update" id="Upload_p" class="button button-primary" value="<?php _e( 'Продолжить', 'usam' ) ?>"/>
				</div>
				<input type="hidden" name="step" value="2"/>			
				<input type="hidden" name="type" value="stock"/>
			</form>			
			</td>
			</tr>
			<tr>
			<td>
				<h3><?php _e( 'Выгрузка товаров для Яндекс маркет.', 'usam' ); ?></h3>		
				<form method="POST" action="">
					<table>
						<tr>
							<td><?php _e('Тип цены','usam') ?>:</td>
							<td><?php usam_get_select_prices( ); ?></td>
						</tr>
					</table>				
					<input type="hidden" name="yandex_market" value="true"/>						
					<?php submit_button( esc_html_x( 'Выгрузить для Яндекс маркет', 'Экспорт в файл', 'usam' ) ); ?>			
				</form>	
			</td>
			<td>			
			<h3><?php _e( 'Выгрузить товары.', 'usam' ) ?></h3>
			<form class="file_price_update" method="POST" action="">			
				<table>
					<tr>
						<td><?php _e('Тип цены','usam') ?>:</td>
						<td><?php usam_get_select_prices( ); ?></td>
					</tr>
				</table>
				<?php submit_button( esc_html_x( 'Выгрузить товары', 'Экспорт в файл', 'usam' ) ); ?>
				<input type="hidden" name="export_product" value="2"/>				
			</form>			
			</td>
			</tr>
		</table>
	<?php
	}
		
	private function export_products( $type, $column ) 
	{	
		if ( isset($_POST['type_price']) )
		{
			$type_price = sanitize_title($_POST['type_price']);
			$price_key = 'price_'.$type_price;				
		}
		else
		{
			$this->message .= "<br>".__( 'Тип цены не указан', 'usam' );
			return;
		}	
		$callback = "export_products_".$type;
		if ( !method_exists($this, $callback) )
		{
			$this->message .= "<br>".__( 'Тип цены не указан', 'usam' );
			return;
		}		
		ini_set("max_execution_time", "600");
		
		$chunk_size = 500;	
		$args = array( 'post_status' => 'publish', 'offset' => 0, 'posts_per_page' => $chunk_size );
		$products = usam_get_products( $args, true );			
		
		$output	= array();
		while ( count ( $products ) ) 
		{			
			foreach ( $products as $product )
			{			
				$result = $this->$callback( $product, $type_price );
				if ( !empty($result) )
					$output[] = $result;
			}
			$args['offset'] += $chunk_size;
			$products = usam_get_products( $args, true );			
		
		}			
		usam_write_exel_file( 'export_products', $output, $column );	
	}	
		
	private function export_products_universam( $product, $type_price ) 
	{			
		$price_key = 'price_'.$type_price;		
		$cat = wp_get_post_terms( $product->ID , 'usam-category' );					
		$category = '';
		if ( isset($cat[0]) )
		{
			$i = 1;					
			$categories[$i] = $cat[0]->name;			
			$terms = get_ancestors( $cat[0]->term_id, 'usam-category' );			
			foreach ($terms as $term) 
			{	
				$term_data = get_term_by('id', $term, 'usam-category');		
				$i++;
				$categories[$i] = $term_data->name;				
			}
			$category = implode(',', $categories);
			unset($categories);
		}				
		$stock = usam_get_product_meta( $product->ID, 'stock' );	
		$sku = usam_get_product_meta( $product->ID, 'sku' );	
		$price = usam_get_product_meta( $product->ID, $price_key );	
		
		$thumbnail = usam_get_product_thumbnail_src($product->ID, 'single' );		
		$output = array( 'title' => $product->post_title,						
						 'stock' => $stock, 							 
						 'url' => $product->guid,
						 'price' => $price,
						 'sku' => $sku,
						 'cat' => $category,
						 'image' => $thumbnail,												
		);		
		return $output;
	}	
	
//  Выгрузка для яндекс маркет	
	private function export_products_yandex_market( $product, $type_price ) 
	{	
		$price_key = 'price_'.$type_price;		
		$output = array();
		$price = usam_get_product_meta( $product->ID, $price_key );	
		if ( $price )
		{
			$thumbnail = usam_get_product_thumbnail_src($product->ID, 'single' );		
			$categorys = wp_get_object_terms( $product->ID , 'usam-category' );	
			$category = isset($cat[0])?$cat[0]->name:'';						
			$stock = usam_get_product_meta( $product->ID, 'stock' );					
			$output = array( 'id' => $product->ID,
							 'available' => $stock>0?'true':'false', 
							 'url' => $product->guid,
							 'price' => $price,
							 'currencyId' => 'RUR',
							 'category' => $category,
							 'picture' => $thumbnail,
							 'name' => $product->post_title,
			);	
		}	
		return $output;		
	}
	
	public function header_action()
	{		
		$out = '';
		if ( $this->step != 1 )
		{
			$step = $this->step - 1;
			$out .='<div class = "go_back"><a href="' . admin_url( 'admin.php?page=exchange&tab=file&step='.$step ) . '">' . __( 'Вернуться назад', 'usam' ) . '</a></div>';
		}
		$out .= '<div class = "header_step"><h4>Текущие действие:</h4><div>';
		foreach ($this->header_action as $key => $title)
		{
			if ( $this->step == $key )
				$class = "action";
			else
				$class = "no_action";
			$out .= "<span class = '$class'>".$title."</span>";
		}
		$out .= '</div></div>';	
		echo $out; 
	}
	
	public function display( ) 
	{				
		$this->header_action();
		switch ( $this->step ) 
		{
			case 2:				
				$this->print_file();
			break;
			case 4:						
				$this->display_result();
			break;				
			default:
				$this->display_default();
			break;
		}	
	}	
}