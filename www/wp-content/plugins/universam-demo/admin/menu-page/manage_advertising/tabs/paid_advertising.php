<?php
class USAM_Tab_paid_advertising extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Банерная реклама', 'usam'), 'description' => __('Здесь Вы можете размещать банерную рекламу.', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}