<?php
class USAM_Tab_menu extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Реклама в меню', 'usam'), 'description' => __('Здесь Вы можете размещать рекламу в меню.', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}		
}