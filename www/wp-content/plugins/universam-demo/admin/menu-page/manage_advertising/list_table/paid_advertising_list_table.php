<?php
class USAM_List_Table_paid_advertising extends USAM_List_Table
{	
	protected $orderby = 'id';
	protected $order   = 'asc'; 
		
    function __construct( $args = array() )
	{	
		parent::__construct( $args );	
    }
		
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item )
	{	
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );
	}
		  
	function get_sortable_columns() 
	{
		$sortable = array(
			'id'       => array('id', false),		
			'name'    => array('name', false),		
			'active'   => array('active', false),				
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',
			'id'          => __( 'ID', 'usam' ),
			'name'        => __( 'Название', 'usam' ),			
			'type'        => __( 'Тип', 'usam' ),	
			'active'      => __( 'Активность', 'usam' ),			
			'weight'      => __( 'Вес', 'usam' ),	
			'views'       => __( 'Просмотров', 'usam' ),
        );
        return $columns;
    }
	
	public function get_hidden_columns()
    {
        return array('address', 'schedule');
    }
	
	public function get_number_columns_sql()
    {       
		return array('id', 'sort');
    }
	
	function prepare_items() 
	{			
		global $wpdb;
	
		$this->get_standart_query_parent( );

		$search_terms = $this->search != '' ? explode( ' ', $this->search ): array();
		$search_sql = array();			
		foreach ( $search_terms as $term )
		{
			$search_sql[$term][] = "title LIKE '%".esc_sql( $term )."%'";
			if ( is_numeric( $term ) )
				$search_sql[$term][] = 'id = ' . esc_sql( $term );
			$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
		}
		$search_sql = implode( ' AND ', array_values( $search_sql ) );
		if ( $search_sql )
		{
			$this->where[] = $search_sql;
		}		
		$where = implode( ' AND ', $this->where );			
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_ADVERTISING_BANNER." WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query );	
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );
	
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );	
	}
}