<?php
/*
 * Отображение страницы Рассылка
 */
$default_tabs = array(
	array( 'id' => 'sending_email',  'title' => __( 'Email-рассылки', 'usam' ) ),
	array( 'id' => 'sending_sms',  'title' => __( 'СМС-рассылки', 'usam' ) ),
	array( 'id' => 'lists',  'title' => __( 'Списки', 'usam' ) ), 
	array( 'id' => 'subscriber',  'title' => __( 'Подписчики', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{		
	public function __construct( ) 
	{	
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );
	}	
	
	protected function localize_script_tab()
	{
		return array(					
			'sending_control_nonce' => usam_create_ajax_nonce( 'sending_control' ),				
		);
	}		
} 

?>