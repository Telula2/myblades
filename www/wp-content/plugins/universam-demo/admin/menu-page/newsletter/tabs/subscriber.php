<?php
class USAM_Tab_subscriber extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Подписчики', 'usam'), 'description' => 'Здесь Вы можете управлять Вашими подписчиками.' );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	public function get_tables() 
	{ 
		$tables = array( 'subscriber' => array( 'title' => __('Контакты','usam') ),  'company' => array( 'title' => __('Компании','usam') ) );		
		return $tables;
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{
			case 'delete':			
							
			break;
			case 'save':	
				if( isset($_REQUEST['subscriber']) )
				{							
					foreach ( $_REQUEST['subscriber'] as $id => $select_lists )
					{			
						foreach ( $select_lists['lists'] as $select_list_id => $active )
						{				
							$select_list_id = absint($select_list_id);
							$id = absint($id);
							$status = absint($select_lists['status'][$select_list_id]);									
							if ( $active )
								usam_set_subscriber_lists( array( 'id_communication' => $id, 'status' => $status, 'id' => $select_list_id ) );	
							else
								usam_delete_subscriber_lists( array($id => $select_list_id) );	
						}
					}	
				}
			break;				
			default:			
				if ( !empty($this->current_action) )
				{					
					$args = explode('-',$this->current_action);				
					if ( isset($args[1]) )
					{ 
						$list_id = $args[1];
						foreach ( $this->records as $key => $id )
						{							
							if ( isset($_GET['table']) && $_GET['table'] == 'company' )
								$customer_type = 'company';
							else
								$customer_type = 'contact';	
							$communications = usam_get_customer_communication( $id, $customer_type );	
							foreach ( array('email', 'phone') as $value )
							{						
								foreach ( $communications[$value] as $id_communication => $communication ) 
								{
									switch( $args[0] )
									{
										case 'movetolist':			
											usam_delete_subscriber_list( $id_communication );				
											usam_set_subscriber_lists( array( 'id_communication' => $id_communication, 'id' => $list_id ) );			
										break;	
										case 'copytolist':							
											usam_set_subscriber_lists( array( 'id_communication' => $id_communication, 'id' => $list_id ) );
										break;
										case 'removefromlist':			
											$lists[$id_communication] = $list_id;								
											usam_delete_subscriber_lists( $lists );		
										break;
									}
								}
							}						
						}	
						$this->sendback = add_query_arg( array( 'update' => count($this->records) ), $this->sendback );								
						$this->redirect = true;					
					}
				}
			break;	
		}
	}
}