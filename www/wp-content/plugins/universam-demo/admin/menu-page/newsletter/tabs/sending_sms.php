<?php
class USAM_Tab_sending_sms extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('СМС-рассылки', 'usam'), 'description' => __('Здесь Вы можете создавать СМС-рассылки.','usam') );		
		$this->buttons = array( 'add' => __('Добавить рассылку', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{
			case 'delete':
				foreach ( $this->records as $id ) 
					usam_delete_newsletter( $id );
				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'sending':				
				if ( isset($_GET['id']) )
				{
					$id = absint($_GET['id']);								
					$newsletter = usam_get_newsletter( $id );		
					
					require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
					$_newsletter = new USAM_Send_Newsletter();		
					$result = $_newsletter->send_newsletter( $newsletter );
				//	$this->sendback = add_query_arg( array( 'send' => 1 ), $this->sendback );						
				}
				$this->redirect = true;
			break;			
			case 'view':				
				if ( isset($_GET['id']) )
				{
					$this->sendback = add_query_arg( array( 'table' => 'report_subscriber', 'n' => absint($_GET['id']) ), $this->sendback );
					wp_redirect( $this->sendback );
					exit;				
				}
				else
					$this->redirect = true;
			break;	
			case 'save':			
				if ( !empty($_POST['name']) && !empty($_POST['message']) )
				{
					$update['subject'] = sanitize_text_field($_POST['name']);
					$update['body'] = sanitize_text_field($_POST['message']);	
					$update['type'] = 'sms';
					if ( isset($_POST['send']) )	
					{				
						$update['status'] = 5;					
					}													
					if ( $this->id != null )
					{						
						usam_update_newsletter( $this->id, $update );		
					}
					else
					{
						$this->id = usam_insert_newsletter( $update );	
					}
					if ( !empty($_POST['lists']) && $this->id )
					{
						$lists = array_map('intval', $_POST['lists']);		
						usam_update_newsletter_lists( $this->id, $lists );	
					}				
					if ( $update['status'] == 5 )
					{			
						usam_add_list_newsletter_user_stat( $this->id, 'phone' );						
						$this->sendback = remove_query_arg( array( 'id', 'action' ), $this->sendback  );	
						wp_redirect( $this->sendback );
						exit;
					}	
				}
			break;
			case 'copy':
				$sms = usam_get_newsletter( $this->id );	
				$sms['status'] = 0;
				$this->id = usam_insert_newsletter( $sms );	
			break;
		}
	}	
}