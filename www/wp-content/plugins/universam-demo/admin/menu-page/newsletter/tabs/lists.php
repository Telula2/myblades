<?php
class USAM_Tab_lists extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Списки', 'usam'), 'description' => 'Здесь Вы можете управлять Вашими списками для рассылок.' );			
		$this->buttons = array( 'add' => __('Добавить список', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{
			case 'delete':			
				usam_delete_data( $this->records, 'usam_list_of_subscribers' );
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;
			case 'save':
				if( !empty($_REQUEST['name']) )
				{					
					$list['title'] = sanitize_text_field(stripcslashes($_REQUEST['name']));
					$list['description'] = sanitize_textarea_field(stripcslashes($_REQUEST['description']));
			
					if ( $this->id != null )		
						usam_edit_data( $list, $this->id, 'usam_list_of_subscribers' );	
					else			
					{
						$list['date_insert'] = date( 'Y-m-d H:i:s' );	
						$this->id = usam_add_data( $list, 'usam_list_of_subscribers' );
					}		
				}
			break;			
		}
	}		
}