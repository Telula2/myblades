<?php
class USAM_Tab_sending_email extends USAM_Tab
{
	public function __construct()
	{ 
		if ( !empty($_GET['table']) && $_GET['table'] == 'report_subscriber' )
		{
			$id = !empty($_GET['n'])?absint($_GET['n']):0;		
			$newsletter = usam_get_newsletter( $id );			
			if ( empty($newsletter) )
			{
				$this->sendback = remove_query_arg( array( 'id', 'table' ) );	
				wp_redirect( $this->sendback );
				exit;
			}
			$this->header = array( 'title' => sprintf( __('Статистика рассылки &laquo;%s&raquo;', 'usam'), $newsletter['subject']), 'description' => __('Здесь Вы можете посмотреть статистику вашей рассылки.', 'usam') );		
		}
		else
		{
			$this->header = array( 'title' => __('Email-рассылки', 'usam'), 'description' => 'Здесь Вы можете создавать Email-рассылки.' );		
			$this->buttons = array( 'add' => __('Добавить', 'usam') );			
		}
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
	
	protected function callback_submit()
	{		
		global $wpdb;	
		switch( $this->current_action )
		{
			case 'delete':				
				foreach ( $this->records as $id ) 
					usam_delete_newsletter( $id );
					
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'sending':				
				if ( $this->id != null )	
				{							
					$newsletter = usam_get_newsletter( $this->id );		
				
					require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
					$_newsletter = new USAM_Send_Newsletter();		
					$result = $_newsletter->send_newsletter( $newsletter );					
					$this->sendback = add_query_arg( array( 'send' => 1 ), $this->sendback );						
				}
				$this->redirect = true;
			break;	
			case 'copy':
				$mailing = usam_get_newsletter( $this->id );	
				$mailing['status'] = 0;
				$this->id = usam_insert_newsletter( $mailing );		
			break;	
			case 'view_report':				
				if ( $this->id != null )
				{
					$this->sendback = add_query_arg( array( 'table' => 'report_subscriber', 'n' => $this->id ), $this->sendback );
					wp_redirect( $this->sendback );
					exit;				
				}
				else
					$this->redirect = true;
			break;	
			case 'save':
				if ( isset($_POST['mailing']) )
				{
					$newsletter = $_POST['mailing'];
					if ( $this->id != null )	
					{
						if ( !empty($_POST['mailing_lists']) )
						{
							$mailing_lists = array_map('intval', $_POST['mailing_lists']);		
							usam_update_newsletter_lists( $this->id, $mailing_lists );			
						}						
						if ( isset($_POST['send']) )			
							$newsletter['status'] = 5;				
						
						usam_update_newsletter( $this->id, $newsletter );				
					
						if ( isset($_POST['event_start']) )
						{					
							$trigger = usam_get_trigger( $this->id, 'newsletter_id' );			
						
							$trigger_update['newsletter_id'] = $this->id;
							$trigger_update['event_start'] = sanitize_title($_POST['event_start']);
							if ( isset($_POST['trigger_condition']) )			
								$trigger_update['condition'] = stripslashes_deep($_POST['trigger_condition']);
							
							if ( isset($_POST['send']) )
								$trigger_update['status'] = 1;	
					
							if ( empty($trigger) )
								usam_insert_trigger( $trigger_update );
							else
								usam_update_trigger( $trigger['id'], $trigger_update );	
						}
					}
					else
					{
						if( empty($newsletter['template']) )
							$newsletter['template'] = get_option('usam_mailtemplate');		
						if( empty($newsletter['mailbox_id']) )
							$newsletter['mailbox_id'] = get_option("usam_return_email");			
						if ( empty($newsletter['data']) )
						{				
							$image = array( 'src' => '', 'width' => '', 'height' => '', 'alignment' => '', 'static' => '' );
							$newsletter['data'] = array( 'body' => array( array( 'text' => array( 'value' => "<h2><strong>Шаг 1:</strong> нажмите на этот текст!</h2><br/><p>Для редактирования, просто нажмите на эту часть текста.</p>" ), 'image' => $image, 'position' => 1, 'type' => 'content' ) ), );			
						}			
						$this->id = usam_insert_newsletter( $newsletter );	
					}
					if ( isset($_POST['send']) )
					{			
						usam_add_list_newsletter_user_stat( $this->id, 'email' );							
						$this->sendback = remove_query_arg( array( 'id', 'step', 'action' ), $this->sendback  );	
						wp_redirect( $this->sendback );
						exit;
					}	
				}				
			break;
		}
	}	
}