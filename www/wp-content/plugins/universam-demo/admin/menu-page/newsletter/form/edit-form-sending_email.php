<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_sending_email extends USAM_Edit_Form
{	
	protected $step = 1;
	
	protected function get_data_tab(  )
	{		
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );			
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_style' ), 20 );	
		
		if ( $this->id != null )							
		{
			$this->data = usam_get_newsletter( $this->id );	
			$this->data['lists'] = usam_get_newsletter_list( $this->id );
		}
		else
			$this->data = array( 'lists' => array( ), 'type' => 'mail', 'class' => 'S', 'subject' => '' );					
	}

	private function back_submit( $screen )
	{ 
		$url = wp_get_referer();
		echo "<a class='back_submit secondary' href='".$url."'>&laquo; ".__('Назад', 'usam')."</a>";	
	}	
	
	public function scripts_and_style()
	{   
		wp_enqueue_script( 'jquery-ui-draggable' );	
		wp_enqueue_script( 'jquery-ui-droppable' );	
		wp_enqueue_script('usam-mail-editor');		
		wp_enqueue_style( 'usam-mail-editor', USAM_URL . '/admin/css/mail/mail-editor.css', false, USAM_VERSION, 'all' );		
		wp_localize_script( 'usam-mail-editor', 'Universam_Mail_Editor', array(			
			'mail_id'                => $this->id,
			'text_insert_block_here' => __('Вставьте блок здесь', 'usam'),	
			'text_media_upload'      => __('Добавление картинки в рассылку','usam'),	
			'text_alignment_left'    => __('Выравнять по левому краю', 'usam'),	
			'text_alignment_center'  => __('Выравнять по центру', 'usam'),	
			'text_alignment_right'   => __('Выравнять по правому краю', 'usam'),	
			'text_add_link'          => __('Добавить ссылку / Альтернативный текст', 'usam'),
			'text_remove_link'       => __('Удалить ссылку', 'usam'),
			'text_remove'            => __('Удалить картинку', 'usam'),					
			'get_form_mail_editor_iframe_nonce' => usam_create_ajax_nonce( 'get_form_mail_editor_iframe' ),			
			'save_mailtemplate_nonce'           => usam_create_ajax_nonce( 'save_mailtemplate' ),
			'get_selection_products_mail_editor_iframe_nonce' => usam_create_ajax_nonce( 'get_selection_products_mail_editor_iframe' ),
			'set_divider_mail_editor_nonce' => usam_create_ajax_nonce( 'set_divider_mail_editor' ),
			'insert_blok_mail_editor_iframe_nonce' => usam_create_ajax_nonce( 'insert_blok_mail_editor_iframe' ),
			'insert_post_blok_mail_editor_iframe_nonce' => usam_create_ajax_nonce( 'insert_post_blok_mail_editor_iframe' ),			
			'insert_products_blok_mail_editor_iframe_nonce' => usam_create_ajax_nonce( 'insert_products_blok_mail_editor_iframe' ),
			'get_post_type_nonce'     => usam_create_ajax_nonce( 'get_post_type' ),				
			'send_preview_mail_nonce' => usam_create_ajax_nonce( 'send_preview_mail' ),		
		) );		
		wp_enqueue_media();
		wp_enqueue_script( 'hc-sticky' );
		wp_enqueue_script( 'jquery-ui-accordion' );	
	//	wp_enqueue_script( 'media-upload' );
		add_thickbox();	
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );	
		
		wp_enqueue_script( 'jquery-ui-resizable' );	

		 require_once ABSPATH . '/wp-includes/class-wp-editor.php';		
		_WP_Editors::print_tinymce_scripts();	
	}
		
    public function screen_1()
	{           
        ?>
		<div class = "screen_start">
		<h2><?php _e('Первый шаг: основные сведения', 'usam') ?></h2>
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>
				<tr>
					<td class="header"><h4><?php _e('Название рассылки', 'usam') ?>:</h4></td>
					<td>
						<div id="titlediv">
							<div id="titlewrap">
								<input type="text" name="mailing[subject]" value="<?php echo $this->data['subject']; ?>" class="titlebox"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td class="header"><h4><?php _e('Какой тип этой рассылки?', 'usam'); ?></h4></td>
					<td>
						<label for="user-type-S"><input type="radio" name="mailing[class]" value="S" <?php echo ($this->data['class'] == 'S')?'checked="checked"':''; ?> id="user-type-S"><?php _e('Стандартная', 'usam'); ?></label>
						<label for="user-type-T"><input type="radio" name="mailing[class]" value="T" <?php echo ($this->data['class'] == 'T')?'checked="checked"':''; ?>id="user-type-T" ><?php _e('Триггерная', 'usam'); ?></label>						
					</td>					
				</tr>									
			</tbody>
		</table>
		</div>					
		<?php		
		submit_button( __('Следующий шаг &raquo;','usam'), 'primary', 'next', false, array( 'id' => 'submit' ) ); 
	} 

	public function screen_2( )
	{   		
		$mailtemplate_list = usam_get_templates( 'mailtemplate' );	
		?>
		<div class = "screen_template">
		<h2><?php _e('Второй шаг: дизайн', 'usam') ?></h2>		
			<div class ="theme-browser content-filterable rendered" >
				<div class ="themes wp-clearfix" >
				<?php						
				foreach ($mailtemplate_list as $template => $data ) 
				{
					$class = $template == $this->data['template']?'active':'';			
					?>				
					<div class="theme <?php echo $class; ?>" tabindex="0" aria-describedby="<?php echo $template; ?>-action <?php echo $template; ?>-name" data-mailtemplate ="<?php echo $template; ?>">
			
						<div class="theme-screenshot">
							<img src="<?php echo $data['screenshot']; ?>" alt="">
						</div>				
						<span class="more-details"><?php echo __('Автор', 'usam').": ".$data['author']; ?></span>				
						<h3 class="theme-name"><?php echo $template; ?></h3>

						<div class="theme-actions">
							<a id ="select_mailtemplate" class="button button-primary" href=""><?php _e('Выбрать', 'usam'); ?></a>						
						</div>
					</div>
					<?php
				}
				?>
				</div>
			</div>
		</div>
		<input type='hidden' name='mailing[template]' value='<?php echo $this->data['template']; ?>' id ="mailtemplate"/>		
		<div class="footer_panel">
			<div class="footer_wrap">
				<div class="control_buttons_div">
					<?php
					$this->back_submit( 'start' );
					submit_button( __('Следующий шаг &raquo;','usam'), 'primary', 'next', false, array( 'id' => 'next-submit' ) );
					?>
				</div>	
			</div>	
		</div>		
		<?php
	} 
	
	public function screen_3( )
	{   		
		$mail = new USAM_Edit_Newsletter( $this->id );
		?>		
		<h2><?php _e('Третий шаг: создать письмо', 'usam') ?></h2>	
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="post-body-content">				
					<div id = "usam_mailtemplate_editor">
						<?php if ( empty($_GET['edit_mailtemplate']) ) { 			
							//<a id="edit_mailtemplate"><?php _e('Редактировать все письмо', 'usam') </a> ?>
							<?php echo $mail->get_mail_edit(  ); 
						} 
						else 
						{ 													
							$mail_html = $mail->get_mail( ); 
							wp_editor(stripslashes(str_replace('\\&quot;','',$mail_html)),'edit_mail',array(
								'textarea_name' => 'edit_mail',
								'media_buttons' => false,
								'textarea_rows' => 50,
								'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
								)	
							);	
						}
						?>
					</div>
				</div>  
				<div id="postbox-container-1" class="postbox-container">	
					<div class = 'menu_fixed_right'>	
						<div class = "postbox">
							<div id ="usam_editor_sidebar" class="inside">
								<div id='usam_accordion' class ="mailtemplate_editor_tab">									
									<h3><?php _e('Содержание', 'usam') ?><div class="handlediv"><br></div></h3>
									<div class="widget_content"><?php $this->display_content(); ?></div>									
									<h3><?php _e('Колонки', 'usam') ?><div class="handlediv"><br></div></h3>
									<div class="widget_content"><?php $this->display_columns(); ?></div>	
									<h3><?php _e('Колонки с товарами', 'usam') ?><div class="handlediv"><br></div></h3>
									<div class="widget_content"><?php $this->display_columns_products(); ?></div>										
									<h3><?php _e('Стили', 'usam') ?><div class="handlediv"><br></div></h3>
									<div class="widget_styles"><?php $this->display_styles(); ?></div>
									<h3><?php _e('Шорт-коды', 'usam') ?><div class="handlediv"><br></div></h3>
									<div class="widget_content widget_short_codes"><?php $this->display_short_codes(); ?></div>
								</div>
							</div>							
						</div>						
					</div>	
				</div>
			</div>		
		</div>							
		<div class="footer_panel">
			<div class="footer_wrap">
				<div class="control_buttons_div">
					<?php $this->back_submit( 'template' );	?>
					<?php submit_button( __('Следующий шаг &raquo;','usam'), 'primary', 'next', false, array( 'id' => 'submit' ) ); ?>
					<?php usam_loader(); ?>
				</div>
				<?php $this->send_preview( ); ?>			
				<div class="save_buttons_div">
					<button type="button" id="save-submit" class="button button-primary"><?php _e( 'Сохранить изменения', 'usam' ); ?></button>
					<?php usam_loader(); ?>
				</div>
			</div>
		</div>				
		<?php
	} 
	
	private function display_content(  )
	{ 
		?>		
		<div id ="content" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="143.342 252.486 325.183 286.798" enable-background="new 143.342 252.486 325.183 286.798" xml:space="preserve">
				<path d="M337.154,398.353h-70.667l23.409-59.823c5.202-13.12,9.983-26.622,13.445-41.788h0.86
					c2.161,15.166,6.503,29.089,11.265,41.788L337.154,398.353z M372.708,497.496h-24.71v41.788h120.526v-41.788h-24.289
					l-100.139-245.01h-69.806l-106.66,245.01h-24.289v41.788h112.723v-41.788h-29.051l21.688-54.907H353.2L372.708,497.496z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">Текст</div>

		</div>
		<div id ="fotos" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="38.25 185.625 535.5 420.75" enable-background="new 38.25 185.625 535.5 420.75" xml:space="preserve">
				<path d="M459,606.375H38.25v-306v-38.25v-76.5h535.5v420.75h-76.5H459z M535.5,223.875h-459v344.25h459V223.875z M114.75,529.875
					v-229.5l382.5,229.5H114.75z M420.75,415.125c-20.903,0-38.881-7.516-53.933-22.567c-15.052-15.051-22.567-33.01-22.567-53.933
					c0-20.904,7.516-38.881,22.567-53.933s33.029-22.567,53.933-22.567s38.881,7.516,53.933,22.567s22.567,33.029,22.567,53.933
					c0,20.923-7.516,38.881-22.567,53.933C459.631,407.609,441.653,415.125,420.75,415.125z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">Картинка</div>
		</div>
		<div id ="button" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 195.188 573.75 446.946" enable-background="new 0 195.188 573.75 446.946" xml:space="preserve">
				<path d="M535.5,482.062h-20.655c-12.967,90.468-90.574,160.071-184.618,160.071c-94.068,0-171.65-69.604-184.646-160.071H38.25
					c-21.133,0-38.25-17.117-38.25-38.25V233.438c0-21.133,17.117-38.25,38.25-38.25H535.5c21.114,0,38.25,17.117,38.25,38.25v210.375
					C573.75,464.945,556.614,482.062,535.5,482.062z M463.889,382.016c0-11.045-9.259-20.009-20.703-20.009
					c-11.419,0-19.677,8.964-19.677,20.009v43.34c0,3.682-4.095,6.67-7.884,6.67c-3.814,0-7.909-2.988-7.909-6.67v-50.01
					c0-11.045-8.258-20.009-19.676-20.009s-18.677,8.964-18.677,20.009v50.01c0,3.682-5.097,6.67-8.909,6.67
					c-3.815,0-9.671-2.988-9.671-6.67v-56.681c0-11.046-8.258-20.01-19.703-20.01c-11.418,0-18.702,8.964-18.702,20.01v56.681
					c0,3.682-6.27,6.67-10.084,6.67c-3.815,0-11.458-2.988-11.458-6.67V288.64c0-11.046-8.258-20.01-19.677-20.01
					s-17.715,8.964-17.715,20.01v173.413c0,3.685-11.419,6.67-15.233,6.67c-3.815,0-3.335-2.985-3.335-6.67v-85.532
					c-2.16-0.747-15.794-2.269-18.222-2.269c-11.419,0-20.09,10.085-20.09,21.131v73.34c0,66.297,74.622,120.058,143.187,120.058
					s124.138-53.761,124.138-120.058V382.016L463.889,382.016z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">Кнопка</div>
		</div>
		<div id ="divider" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="124.312 348.188 382.5 114.75" enable-background="new 124.312 348.188 382.5 114.75" xml:space="preserve">
				<rect x="124.312" y="348.188" width="382.5" height="38.25"></rect>
				<rect x="124.312" y="424.688" width="382.5" height="38.25"></rect>
				</svg>
			</div>
			<div class="usam_widget_title">Разделитель</div>
		</div>
		<div id="indentation" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="239.062 263.438 153 284.25" enable-background="new 239.062 263.438 153 284.25" xml:space="preserve">
				<path d="M315.563,263.438l76.5,76.538h-153L315.563,263.438z M315.563,547.688l-76.5-76.537h153L315.563,547.688z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">Отступ</div>
		</div>		
		<div id="posts" class="usam_widget usam_droppable_widget">
			<div class="usam_widget_icon usam_widget_icon_droppable">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="133.875 223.875 344.25 344.25" enable-background="new 133.875 223.875 344.25 344.25" xml:space="preserve">
				<path d="M133.875,223.875v344.25h229.5l114.75-114.75v-229.5H133.875z M358.518,391.161l-11.647,11.647
					c-7.038,7.152-11.054,15.721-12.049,25.704c-0.976,10.002,1.224,19.087,6.599,27.272L312.98,484.07l-37.447-37.446l-43.509,43.356
					c-5.584,5.584-16.142,13.617-31.613,24.078c-15.492,10.461-24.633,14.286-27.425,11.494c-2.792-2.792,1.033-11.991,11.494-27.578
					s18.437-26.125,23.925-31.613l43.356-43.356l-37.447-37.6l28.439-28.286c8.07,5.393,17.136,7.592,27.195,6.598
					c10.06-0.994,18.59-5.049,25.628-12.202l11.646-11.647c7.153-7.038,11.208-15.587,12.202-25.627
					c0.976-10.041-1.224-19.164-6.598-27.349l28.285-28.286l98.819,98.666l-28.439,28.286c-8.07-5.393-17.136-7.592-27.195-6.598
					C374.257,379.935,365.67,384.009,358.518,391.161z M363.375,539.438v-86.063h86.063L363.375,539.438z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">Статьи</div>
		</div>		
		<?php
	}
	
	private function display_columns( )
	{ 	
		?>	
		<div id ="column" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="2" data-line ="1">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="86.063 85.506 440.854 612" enable-background="new 86.063 85.506 440.854 612" xml:space="preserve">
				<path d="M264.938,85.506h-159.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h159.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C284.062,94.074,275.494,85.506,264.938,85.506z"></path>
				<path d="M507.792,85.506h-160.75c-10.558,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.567,19.125,19.125,19.125h160.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C526.917,94.074,518.349,85.506,507.792,85.506z"></path>
				</svg>
			</div>			
		</div>
		<div id ="column" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="3" data-line ="2">
				<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="210mm" height="297mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 21000 29700" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 86.063 85.506 439.937 612">
					<path class="fil0" d="M5102 0l-4189 0c-504,0 -913,184 -913,411l0 12324c0,227 409,411 913,411l4189 0c504,0 912,-184 912,-411l0 -12324c0,-227 -409,-411 -912,-411z"/>
					<path class="fil0" d="M5102 16554l-4189 0c-504,0 -913,184 -913,411l0 12324c0,227 409,411 913,411l4189 0c504,0 912,-184 912,-411l0 -12324c0,-227 -409,-411 -912,-411z"/>
					<path class="fil0" d="M20087 0l-4189 0c-504,0 -912,184 -912,411l0 12324c0,227 409,411 912,411l4189 0c504,0 913,-184 913,-411l0 -12324c0,-227 -409,-411 -913,-411z"/>
					<path class="fil0" d="M20017 16554l-4189 0c-504,0 -913,184 -913,411l0 12324c0,227 409,411 913,411l4189 0c503,0 912,-184 912,-411l0 -12324c0,-227 -409,-411 -912,-411z"/>
					<path class="fil0" d="M12594 0l-4188 0c-504,0 -913,184 -913,411l0 12324c0,227 409,411 913,411l4188 0c504,0 913,-184 913,-411l0 -12324c0,-227 -409,-411 -913,-411z"/>
					<path class="fil0" d="M12559 16554l-4189 0c-504,0 -913,184 -913,411l0 12324c0,227 409,411 913,411l4189 0c504,0 913,-184 913,-411l0 -12324c0,-227 -409,-411 -913,-411z"/>
				</svg>
			</div>			
		</div>
		<div id ="column" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="3" data-line ="1">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="86.063 85.506 439.937 612" enable-background="new 86.063 85.506 439.937 612" xml:space="preserve">
				<path d="M192.938,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C212.062,94.074,203.494,85.506,192.938,85.506z"></path>
				<path d="M349.875,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C369,94.074,360.432,85.506,349.875,85.506z"></path>
				<path d="M506.875,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C526,94.074,517.432,85.506,506.875,85.506z"></path>
				</svg>
			</div>			
		</div>
		<div id ="column" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="3" data-line ="3">
				<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="210mm" height="297mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 21000 29700" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 86.063 85.506 439.937 612">
				  <path d="M5102 0l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M5102 20936l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M5102 10468l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M20087 0l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M20087 20936l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M20087 10468l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 0l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 20936l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 10468l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				</svg>
			</div>			
		</div>
		<?php
	}	
	
	private function display_columns_products( )
	{ 	
		?>	
		<div id ="column_product" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="2" data-line ="1">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="86.063 85.506 440.854 612" enable-background="new 86.063 85.506 440.854 612" xml:space="preserve">
				<path d="M264.938,85.506h-159.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h159.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C284.062,94.074,275.494,85.506,264.938,85.506z"></path>
				<path d="M507.792,85.506h-160.75c-10.558,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.567,19.125,19.125,19.125h160.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C526.917,94.074,518.349,85.506,507.792,85.506z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">2 колонки</div>
		</div>
		<div id ="column_product" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="3" data-line ="1">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="86.063 85.506 439.937 612" enable-background="new 86.063 85.506 439.937 612" xml:space="preserve">
				<path d="M192.938,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C212.062,94.074,203.494,85.506,192.938,85.506z"></path>
				<path d="M349.875,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C369,94.074,360.432,85.506,349.875,85.506z"></path>
				<path d="M506.875,85.506h-87.75c-10.557,0-19.125,8.568-19.125,19.125v573.75c0,10.557,8.568,19.125,19.125,19.125h87.75
					c10.557,0,19.125-8.568,19.125-19.125v-573.75C526,94.074,517.432,85.506,506.875,85.506z"></path>
				</svg>
			</div>
			<div class="usam_widget_title">3 колонки</div>
		</div>		
		<div id ="column_product" class="usam_widget usam_droppable_widget ">
			<div class="usam_widget_icon usam_widget_icon_droppable" data-column ="3" data-line ="3">
				<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="210mm" height="297mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 21000 29700" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 86.063 85.506 439.937 612">
				  <path d="M5102 0l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M5102 20936l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M5102 10468l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 912,-123 912,-274l0 -8216c0,-151 -409,-274 -912,-274z"/>
				  <path d="M20087 0l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M20087 20936l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M20087 10468l-4189 0c-504,0 -912,123 -912,274l0 8216c0,151 409,274 912,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 0l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 20936l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				  <path d="M12542 10468l-4189 0c-504,0 -913,123 -913,274l0 8216c0,151 409,274 913,274l4189 0c504,0 913,-123 913,-274l0 -8216c0,-151 -409,-274 -913,-274z"/>
				</svg>
			</div>
			<div class="usam_widget_title">3 колонки</div>
		</div>
		<?php
	}
	
	private function style_tag( $tag, $title )
	{ 
		$styles = !empty($this->data['data']['styles'][$tag])?$this->data['data']['styles'][$tag]:array();
		$default = array( 'color' => '#ffffff', 'font_family' => '', 'font_size' => '','line_height' => '' );
		$styles = array_merge( $default, $styles );	
		
		$fonts_family = array( 'Arial', 'Comic Sans MS', 'Courier New', 'Georgia', 'Lucida', 'Tahoma', 'Times New Roman', 'Trebuchet MS', 'Verdana'  );				
		?>		
		<div id="usam_tag_<?php echo $tag; ?>" class="usam_form_field usam_form_field_tag">			
			<p><strong><?php echo $title; ?></strong></p>
			<span>
				<input type="text" class="usam_color" size="6" maxlength="6" name="text-color" value="<?php echo $styles['color']; ?>" id="usam_<?php echo $tag; ?>_font_color" style="display: none;">
			</span>
			<select id="usam_<?php echo $tag; ?>_font_family" class="usam_font_family usam_select">
				<?php				
				foreach( $fonts_family as $font )
				{						
					?>               
					<option value="<?php echo $font; ?>" <?php selected($styles['font_family'], $font); ?> ><?php echo $font; ?></option>
					<?php
				}
				?>				
			</select>
			<select id="usam_<?php echo $tag; ?>_font_size" class="usam_font_size usam_select">
				<?php				
				for ($i=8; $i<24; $i++)
				{						
					?>               
					<option value="<?php echo $i; ?>px" <?php selected($styles['font_size'], $i.'px'); ?> ><?php echo $i; ?>px</option>
					<?php
				}
				?>				
			</select>
			<select id="usam_<?php echo $tag; ?>_line_height" class="usam_line_height usam_select">
				<?php			
				for ($i=8; $i<24; $i++)
				{						
					?>               
					<option value="<?php echo $i; ?>px" <?php selected($styles['line_height'], $i.'px'); ?> ><?php echo $i; ?>px</option>
					<?php
				}
				?>				
			</select>				
		</div>
		<?php
	}
	
	private function display_styles( )
	{ 		
		$this->style_tag( 'p', __('Текст', 'usam') ); 
		$this->style_tag( 'h1', __('Заголовок 1', 'usam') ); 
		$this->style_tag( 'h2', __('Заголовок 2', 'usam') ); 
		$this->style_tag( 'h3', __('Заголовок 3', 'usam') ); 		
		?>			
		<div id="usam_tag_a" class="usam_form_field">
			<p><strong><?php _e('Ссылки', 'usam') ?></strong></p>
			<span>
				<?php $color = !empty($this->data['data']['styles']['a']['color'])?$this->data['data']['styles']['a']['color']:'#21759B'; ?>
				<input type="text" class="usam_color" size="6" maxlength="6" value="<?php echo $color; ?>" id="usam_a_font_color" style="display: none;">
			</span>
			<label><input type="checkbox" name="underline" value="underline" id="usam_a_font_underline" checked="" class="usam_option_offset_left_small"><?php _e('Подчеркивание', 'usam') ?></label>
		</div>
		<hr>
		<div id="usam_newsletter" class="usam_form_field">
			<p><strong><?php _e('Рассылка', 'usam') ?></strong></p>
			<span>
				<?php $color = !empty($this->data['data']['styles']['newsletter']['color'])?$this->data['data']['styles']['newsletter']['color']:'#ffffff'; ?>
				<input type="text" class="usam_color background_color" size="6" maxlength="6" value="<?php echo $color; ?>" id="usam_table_background_color" style="display: none;">
			</span>
		</div>
		<div id="usam_fon" class="usam_form_field">
			<p><strong><?php _e('Фон', 'usam') ?></strong></p>
			<span>
				<?php $color = !empty($this->data['data']['styles']['fon']['color'])?$this->data['data']['styles']['fon']['color']:'#ffffff'; ?>
				<input type="text" class="usam_color background_color" size="6" maxlength="6" value="<?php echo $color; ?>" id="usam_fon_background_color" style="display: none;">
			</span>
		</div>
		<?php
	}
	
	public function display_short_codes( )
	{
		?>			
		<div class="short_codes_content">			
			<div id ="short_codes_name" class="usam_widget">
				<div class="usam_widget_icon" data-code ="name">
					<?php _e('Обращение', 'usam') ?>
				</div>			
			</div>
		</div>
		<h4><?php _e('Контакт', 'usam') ?></h4>
		<div class="short_codes_content">		
			<div id ="short_codes_name" class="usam_widget">
				<div class="usam_widget_icon" data-code ="lastname">
					<?php _e('Имя', 'usam') ?>
				</div>			
			</div>
			<div id ="short_codes_name" class="usam_widget">
				<div class="usam_widget_icon" data-code ="firstname">
					<?php _e('Фамилия', 'usam') ?>
				</div>			
			</div>		
			<div id ="short_codes_name" class="usam_widget">
				<div class="usam_widget_icon" data-code ="patronymic">
					<?php _e('Отчество', 'usam') ?>
				</div>			
			</div>	
		</div>	
		<h4><?php _e('Компания', 'usam') ?></h4>		
		<div class="short_codes">
			<div id ="short_codes_name" class="usam_widget">
				<div class="usam_widget_icon" data-code ="company_name">
					<?php _e('Название', 'usam') ?>
				</div>			
			</div>		
		</div>			
		<?php
	}	
	
	public function screen_4( )
	{   	
		global $user_ID;
		?>
		<div class = "screen_end">
			<h2><?php _e('Финальный шаг: последние детали', 'usam') ?></h2>
			<table class="subtab-detail-content-table usam_edit_table" >
				<tbody>					
					<tr>
						<td class="header"><h4><?php _e('Название рассылки', 'usam') ?>:</h4></td>
						<td>
							<div id="titlediv">
								<div id="titlewrap">
									<input type="text" name="mailing[subject]" id="subject" value="<?php echo $this->data['subject']; ?>" class="titlebox"/>
								</div>
							</div>
						</td>
					</tr>
					<?php if ( $this->data['class'] == 'S')
					{ 						
					?>
					<tr>
						<td class="header"><h4><?php _e('Списки подписчиков', 'usam'); ?>:</h4></td>
						<td id ="list_of_subscribers"><?php								
								$lists = usam_get_subscribers_list( );
								foreach ( $lists as $list )
								{
									?>
									<label for="user-list-<?php echo $list['id']; ?>">							
									<input type="checkbox" name="mailing_lists[]" value="<?php echo $list['id']; ?>" <?php echo ( in_array( $list['id'], $this->data['lists'] ))?"checked='checked'":'' ?>><?php echo $list['title']; ?>
									</label></br>
									<?php
								}
							?>						
						</td>					
					</tr>	
					<?php
					}					
					?>					
					<tr>
						<td class="header"><h4><?php _e('Отправитель', 'usam') ?>:</h4></td>
						<td>
							<select name="mailing[mailbox_id]">
							<?php
							$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name','email'), 'user_id' => $user_ID ) );
							foreach( $mailboxes as $mailbox )
							{
								?>	<option value="<?php echo $mailbox->id; ?>" <?php selected($mailbox->id, $this->data['mailbox_id']) ?>><?php echo $mailbox->name.' ('.$mailbox->email.')'; ?></option><?php
							}		
							?>	
							</select>
						</td>
					</tr>										
				</tbody>
			</table>
			<?php if ( $this->data['class'] == 'T')
			{ 
				$trigger = usam_get_trigger( $this->id, 'newsletter_id' );						
				if ( empty($trigger) )
				{
					$trigger = array( 'event_start' => '' , 'condition' => array('run_for_old_data' => 0, 'days_dont_buy' => 90, 'time_start' => '13:00', 'days_basket_forgotten' => 5 ) );
				}//colspan="			
				?>
				<div class="start_conditions">
					<h3><?php _e('Условия запуска и остановки', 'usam') ?></h3>					
					<table class="subtab-detail-content-table usam_edit_table">
						<tbody>										
							<tr>
								<td class="header"><?php _e('Выберите условие', 'usam') ?>:</td>					
								<td>
									<select id="event_start" name="event_start">
										<option value="sender_user_dontauth" <?php echo $trigger['event_start'] == 'sender_user_dontauth' ?'selected="selected"':''; ?>><?php _e('Давно не заходил на сайт', 'usam') ?></option>
										<option value="sale_dont_buy" <?php echo $trigger['event_start'] == 'sale_dont_buy' ?'selected="selected"':''; ?>><?php _e('Давно не покупал', 'usam') ?></option>
										<option value="basket_forgotten" <?php echo $trigger['event_start'] == 'basket_forgotten' ?'selected="selected"':''; ?>><?php _e('Забытая корзина', 'usam') ?></option>
										<option value="sender_user_auth" <?php echo $trigger['event_start'] == 'sender_user_auth' ?'selected="selected"':''; ?>><?php _e('Заход на сайт', 'usam') ?></option>
										<option value="order_status_change" <?php echo $trigger['event_start'] == 'order_status_change' ?'selected="selected"':''; ?>><?php _e('Изменение статуса заказа', 'usam') ?></option>
										<option value="order_status" <?php echo $trigger['event_start'] == 'order_status' ?'selected="selected"':''; ?>><?php _e('Статус заказа', 'usam') ?></option>
										<option value="order_paid" <?php echo $trigger['event_start'] == 'order_paid' ?'selected="selected"':''; ?>><?php _e('Оплата заказа', 'usam') ?></option>
										<option value="subscribe_to_newsletter" <?php echo $trigger['event_start'] == 'subscribe_to_newsletter' ?'selected="selected"':''; ?>><?php _e('Новому подписчику', 'usam') ?></option>
										<option value="adding_newsletter" <?php echo $trigger['event_start'] == 'adding_newsletter' ?'selected="selected"':''; ?>><?php _e('При добавлении в рассылку', 'usam') ?></option>										
									</select>
								</td>
								<td><?php _e('Что должен сделать пользователь, чтобы запустилась рассылка', 'usam') ?></td>	
							</tr>		
							<tr id ="trigger_condition">
								<td colspan="3">	
									<table id ="order_status">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php
													$times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>		
											<tr>								
												<td class="header"><?php _e('Статус заказа', 'usam') ?>:</td>					
												<td>
													<select class="chzn-select-nosearch" name = "trigger_condition[status]">
														<?php									
														$order_statuses = usam_get_order_statuses();
														foreach ( $order_statuses as $status ) 
														{
															if ( $status->internalname == $trigger['condition']['status'])
																$select = 'selected="selected"';
															else
																$select = '';
															?><option value='<?php echo $status->internalname; ?>'<?php echo $select; ?>><?php echo $status->name; ?></option><?php
														}
														?>
													</select>	
												</td>
												<td></td>
											</tr>									
										</tbody>
									</table>
									<table id ="basket_forgotten">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php $times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>	
											<tr>
												<td class="header"><?php _e('Обработать старые данные', 'usam') ?>:</td>					
												<td>
													<input type="hidden" id="run_for_old_data" name="trigger_condition[run_for_old_data]" value="0" />
													<input type="checkbox" id="run_for_old_data" name="trigger_condition[run_for_old_data]" <?php echo ( $trigger['condition']['run_for_old_data'] == 1)?"checked='checked'":'' ?> value="1" />
												</td>
												<td></td>				
											</tr>	
											<tr>
												<td class="header"><?php _e('Забыл более(дней)', 'usam') ?>:</td>					
												<td><input type="text" name="trigger_condition[days_basket_forgotten]" value="<?php echo $trigger['condition']['days_basket_forgotten']; ?>" /></td>		
												<td></td>													
											</tr>							
										</tbody>
									</table>										
									<table id ="sale_dont_buy">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php $times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>	
											<tr>
												<td class="header"><?php _e('Обработать старые данные', 'usam') ?>:</td>					
												<td>
													<input type="hidden" id="run_for_old_data" name="trigger_condition[run_for_old_data]" value="0" />
													<input type="checkbox" id="run_for_old_data" name="trigger_condition[run_for_old_data]" <?php echo ( $trigger['condition']['run_for_old_data'] == 1)?"checked='checked'":'' ?> value="1" />
												</td>
												<td></td>				
											</tr>	
											<tr>
												<td class="header"><?php _e('Сколько дней не покупал', 'usam') ?>:</td>					
												<td><input type="text" name="trigger_condition[days_dont_buy]" value="<?php echo $trigger['condition']['days_dont_buy']; ?>" /></td>		
												<td></td>													
											</tr>							
										</tbody>
									</table>
									<table id ="adding_newsletter">
										<tbody>											
											<tr>
												<td class="header"><?php _e('Обработать старые данные', 'usam') ?>:</td>					
												<td>
													<input type="hidden" id="run_for_old_data" name="trigger_condition[run_for_old_data]" value="0" />
													<input type="checkbox" id="run_for_old_data" name="trigger_condition[run_for_old_data]" <?php checked( $trigger['condition']['run_for_old_data'], 1) ?> value="1" />
												</td>
												<td></td>				
											</tr>	
											<tr>
												<td class="header"><?php _e('Списки', 'usam') ?>:</td>
												<td><?php 
													$lists = usam_get_subscribers_list( );
													foreach ( $lists as $list )
													{
														?>
														<label for="user-list-<?php echo $list['id']; ?>">							
														<?php echo $list['title']; ?> <input type="checkbox" name="trigger_condition[lists][]" value="<?php echo $list['id']; ?>" <?php echo ( !empty($trigger['condition']['lists']) && in_array( $list['id'], $trigger['condition']['lists'] ))?"checked='checked'":'' ?>>
														</label></br>
														<?php
													}	
													?>
												</td>	
												<td></td>													
											</tr>							
										</tbody>
									</table>														
								</td>			
							</tr>						
						</tbody>
					</table>	
				</div>
			<?php } ?>			
			<div class="footer_panel">
				<div class="footer_wrap">
					<div class="control_buttons_div">
						<?php
						$this->back_submit( 'editor' );		
						submit_button( __('Отправить','usam'), 'primary', 'send', false, array( 'id' => 'send-submit' ) );					
						?>
					</div>			
					<?php $this->send_preview( ); ?>
					<div class="save_buttons_div">
						<?php submit_button( __('Сохранить и закрыть','usam'), 'primary', 'save-close', false, array( 'id' => 'submit' ) ); ?>
						<?php submit_button( __('Сохранить','usam'), 'primary', 'save', false, array( 'id' => 'submit' ) ); ?>									
					</div>
				</div>
			</div>
		</div>
		<?php
	}   

	public function send_preview( )
	{			
		$mailbox = usam_get_primary_mailbox();
		?>
		<div class="send_preview_div">
			<label for="send_preview">
				<strong><?php _e('Просмотр', 'usam') ?>:</strong>
				<input type="text" id="from_email" value="<?php echo $mailbox['email']; ?>" />
			</label>
			<input type="hidden" id="mail_id" value="<?php echo $this->id; ?>"/>		
			<?php submit_button( __('Отправить просмотр','usam'), 'secondary', 'send_preview', false, array( 'id' => 'send_preview-submit' ) ); ?>
			<?php usam_loader(); ?>
		</div>
		<?php
				
    }	
}
?>