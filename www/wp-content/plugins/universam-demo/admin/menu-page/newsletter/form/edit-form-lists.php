<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_lists extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить cписок','usam');
		else
			$title = __('Добавить cписок', 'usam');	
		return $title;
	}
	
	protected function get_data_tab()
	{ 
		if ( $this->id != null )
		{		
			$this->data = usam_get_data( $this->id, 'usam_list_of_subscribers' );	
		}
		else
			$this->data = array( 'title' => '', 'description' => '', );
	}
	
	function display_left()
	{			
		$this->titlediv( $this->data['title'] );		
		$this->add_box_description( $this->data['description'] );			
    }	
}
?>