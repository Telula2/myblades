<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_sending_sms extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить СМС рассылку','usam');
		else
			$title = __('Добавить СМС рассылку', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{		
		if ( $this->id != null )	
		{
			$this->data = usam_get_newsletter( $this->id );	
			$this->data['lists'] = usam_get_newsletter_list( $this->id );
		}
		else
			$this->data = array( 'lists' => array(), 'subject' => '', 'body' => '', 'type' => 'S' );				
	}
	
	public function display_left( )
	{   				
		$this->titlediv( $this->data['subject'] );		
		?>		
		<div class = "usam_sms_editor usam_sms">
			<textarea rows='20' autocomplete='off' cols='60' name='message' id='message_editor'><?php echo $this->data['body']; ?></textarea>
		</div>		
		<div class = "usam_sms_settings usam_sms">
			<table class="subtab-detail-content-table usam_edit_table" >
				<tr>
					<td class="name"><h4><?php _e('Списки подписчиков', 'usam'); ?>:</h4></td>
					<td id ="list_of_subscribers"><?php
							$lists = usam_get_subscribers_list( );
							foreach ( $lists as $list )
							{
								?>
								<label for="user-list-<?php echo $list['id']; ?>">							
								<input type="checkbox" name="lists[]" value="<?php echo $list['id']; ?>" <?php echo ( in_array( $list['id'], $this->data['lists'] ))?"checked='checked'":'' ?>><?php echo $list['title']; ?>
								</label></br>
								<?php
							}
						?>						
					</td>					
				</tr>			
			</table>
		</div>	
		<?php 
	} 	
	
	public function screen_end( )
	{   	
		?>
		<div class = "screen_end">
			<h2><?php _e('Финальный шаг: последние детали', 'usam') ?></h2>
			<?php 
			if ( $this->data['type'] == 'T')
			{ 
				$trigger = usam_get_trigger( $this->id, 'newsletter_id' );						
				if ( empty($trigger) )
				{
					$trigger = array( 'event_start' => '' , 'condition' => array('run_for_old_data' => 0, 'days_dont_buy' => 90, 'time_start' => '13:00' ) );
				}//colspan="			
				?>
				<div class="start_conditions">
					<h3><?php _e('Условия запуска и остановки', 'usam') ?></h3>					
					<table class="subtab-detail-content-table usam_edit_table">
						<tbody>										
							<tr>
								<td class="header"><?php _e('Выберите условие', 'usam') ?>:</td>					
								<td>
									<select id="event_start" name="event_start">
										<option value="sender_user_dontauth" <?php echo $trigger['event_start'] == 'sender_user_dontauth' ?'selected="selected"':''; ?>><?php _e('Давно не заходил на сайт', 'usam') ?></option>
										<option value="sale_dont_buy" <?php echo $trigger['event_start'] == 'sale_dont_buy' ?'selected="selected"':''; ?>><?php _e('Давно не покупал', 'usam') ?></option>
										<option value="basket_forgotten" <?php echo $trigger['event_start'] == 'basket_forgotten' ?'selected="selected"':''; ?>><?php _e('Забытая корзина', 'usam') ?></option>
										<option value="sender_user_auth" <?php echo $trigger['event_start'] == 'sender_user_auth' ?'selected="selected"':''; ?>><?php _e('Заход на сайт', 'usam') ?></option>
										<option value="order_status_change" <?php echo $trigger['event_start'] == 'order_status_change' ?'selected="selected"':''; ?>><?php _e('Изменение статуса заказа', 'usam') ?></option>
										<option value="order_status" <?php echo $trigger['event_start'] == 'order_status' ?'selected="selected"':''; ?>><?php _e('Статус заказа', 'usam') ?></option>
										<option value="order_paid" <?php echo $trigger['event_start'] == 'order_paid' ?'selected="selected"':''; ?>><?php _e('Оплата заказа', 'usam') ?></option>
									</select>
								</td>
								<td><?php _e('Что должен сделать пользователь, чтобы запустилась рассылка', 'usam') ?></td>	
							</tr>		
							<tr id ="trigger_condition">
								<td colspan="3">	
									<table id ="order_status">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php
													$times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>		
											<tr>								
												<td class="header"><?php _e('Статус заказа', 'usam') ?>:</td>					
												<td>
													<select class="chzn-select-nosearch" name = "trigger_condition[status]">
														<?php									
														$order_statuses = usam_get_order_statuses();
														foreach ( $order_statuses as $status ) 
														{
															if ( $status->internalname == $trigger['condition']['status'])
																$select = 'selected="selected"';
															else
																$select = '';
															?><option value='<?php echo $status->internalname; ?>'<?php echo $select; ?>><?php echo $status->name; ?></option><?php
														}
														?>
													</select>	
												</td>
												<td></td>
											</tr>									
										</tbody>
									</table>
									<table id ="basket_forgotten">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php $times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>	
											<tr>
												<td class="header"><?php _e('Обработать старые данные', 'usam') ?>:</td>					
												<td>
													<input type="hidden" id="run_for_old_data" name="trigger_condition[run_for_old_data]" value="0" />
													<input type="checkbox" id="run_for_old_data" name="trigger_condition[run_for_old_data]" <?php echo ( $trigger['condition']['run_for_old_data'] == 1)?"checked='checked'":'' ?> value="1" />
												</td>
												<td></td>				
											</tr>	
											<tr>
												<td class="header"><?php _e('Забыл более(дней)', 'usam') ?>:</td>					
												<td><input type="text" name="trigger_condition[days_basket_forgotten]" value="<?php echo $trigger['condition']['days_basket_forgotten']; ?>" /></td>		
												<td></td>													
											</tr>							
										</tbody>
									</table>					
									<table id ="sale_dont_buy">
										<tbody>										
											<tr>								
												<td class="header"><?php _e('Время запуска', 'usam') ?>:</td>					
												<td>
													<?php $times = array( '00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30', ); ?>
													<select id="endpoint_start_closed_trigger_time" name="trigger_condition[time_start]">
														<?php									
														foreach ( $times as $time )
														{ 
															?><option value="<?php echo $time; ?>" <?php echo $trigger['condition']['time_start'] == $time ?'selected="selected"':''; ?>><?php echo $time; ?></option><?php									
														}
														?>
													</select>								
												</td>
												<td></td>
											</tr>	
											<tr>
												<td class="header"><?php _e('Обработать старые данные', 'usam') ?>:</td>					
												<td>
													<input type="hidden" id="run_for_old_data" name="trigger_condition[run_for_old_data]" value="0" />
													<input type="checkbox" id="run_for_old_data" name="trigger_condition[run_for_old_data]" <?php echo ( $trigger['condition']['run_for_old_data'] == 1)?"checked='checked'":'' ?> value="1" />
												</td>
												<td></td>				
											</tr>	
											<tr>
												<td class="header"><?php _e('Сколько дней не покупал', 'usam') ?>:</td>					
												<td><input type="text" name="trigger_condition[days_dont_buy]" value="<?php echo $trigger['condition']['days_dont_buy']; ?>" /></td>		
												<td></td>													
											</tr>							
										</tbody>
									</table>														
								</td>			
							</tr>						
						</tbody>
					</table>	
				</div>
			<?php } ?>				
		</div>
		<?php
	}   

	public function buttons(  )
	{			
		if ( $this->id != null )
		{			
			?>			
			<div class="submit_div">		
				<?php
				submit_button( __('Сохранить и закрыть', 'usam'), 'primary button_save_close', 'save-close', false, array( 'id' => 'submit-save-close' ) ); 
				submit_button( __('Сохранить', 'usam'), 'secondary button_save', 'save', false, array( 'id' => 'submit-save' ) ); 
					?>	
			</div>		
			<div class="send_submit_div">
				<?php submit_button( __('Отправить','usam'), 'primary', 'send', false, array( 'id' => 'send-submit' ) ); ?>
			</div>	
			<?php
		}
		else
		{
			submit_button( __('Добавить', 'usam').' &#10010;', 'primary button_save_close', 'add', false, array( 'id' => 'submit-add' ) ); 
		}		
    }
}
?>