<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_subscriber extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->data['lastname'] == '' && $this->data['firstname'] == '' )
			$name = __('неизвестного подписчика', 'usam');
		else
			$name = $this->data['lastname'].' '.$this->data['firstname'];
		
		if ( $this->id != null )
			$title = sprintf( __('Изменить подписку для &laquo;%s&raquo;','usam'), $name );
		else
			$title = __('Добавить подписку', 'usam');	
		
		return $title;
	}
	
	protected function get_data_tab()
	{ 
		if ( $this->id != null )
		{		
			$contact = usam_get_contact( $this->id );	
			$this->data =  usam_get_subscriber_data( $contact );			
		}
		else
		{
			$this->redirect = true;
			return;
		}
	}
	
	function display_wrapper( $communications )
	{	
		foreach ( $communications as $id_communication => $communication )
		{
		?>
			<div class ="subscription-wrapper">
			<h2><?php echo $communication['value']; ?></h2>			
			<table class="subtab-detail-content-table usam_edit_table" >				
				<tbody>
					<?php 
					$lists = usam_get_subscribers_list( );
					foreach ( $lists as $list )
					{
						$status = isset($communication['list'][$list['id']]['status'])?$communication['list'][$list['id']]['status']:'';
						?>						
					<tr>
						<td>	
							<label for="user-list-<?php echo $list['id']; ?>">													
							<input type='hidden' value='0' name='subscriber[<?php echo $id_communication; ?>][lists][<?php echo $list['id']; ?>]' />				
							<input type="checkbox" name="subscriber[<?php echo $id_communication; ?>][lists][<?php echo $list['id']; ?>]" value="1" <?php echo isset($communication['list'][$list['id']])?"checked='checked'":'' ?>><?php echo $list['title']; ?>
							</label>
							<select class="subscriber_status" name="subscriber[<?php echo $id_communication; ?>][status][<?php echo $list['id']; ?>]">
								<option value="0" <?php echo ($status == 0?'selected':''); ?>><?php _e('Неподтвержден', 'usam'); ?></option>
								<option value="1" <?php echo ($status == 1?'selected':''); ?>><?php _e('Подписан', 'usam'); ?></option>
								<option value="2" <?php echo ($status == 2?'selected':''); ?>><?php _e('Отписан', 'usam'); ?></option>
							</select>
						</td>
					</tr>
						<?php
					}
				?>				
				</tbody>
			</table>
			</div>
		<?php
		}
    }	
		
	function email_settings( )
	{	
		$this->display_wrapper( $this->data['email'] );
    }	

	function sms_settings( )
	{		
		$this->display_wrapper( $this->data['phone'] );
	}
	
	function display_left()
	{				
		if ( !empty($this->data['email']) )
			usam_add_box( 'usam_email_settings', __('Подписка на рассылку по электронной почте','usam'), array( $this, 'email_settings' ) );
		if ( !empty($this->data['phone']) )
			usam_add_box( 'usam_sms_settings', __('Подписка на СМС рассылку','usam'), array( $this, 'sms_settings' ) );
    }	
}
?>