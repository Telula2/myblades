<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_subscriber extends USAM_Edit_Form
{	
	protected $statistics = array();
	protected $contact;
	
	protected function get_data_tab(  )
	{	
		if ( empty($this->id) )				
			return;
		
		$this->contact = usam_get_contact( $this->id );
		$keys = array_keys($this->contact['email']) + array_keys($this->contact['phone']);	
		
		global $wpdb;
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." WHERE id_communication IN (".implode(',',$keys).")";
		$this->statistics = $wpdb->get_results( $sql );	
	}	 
	
	protected function get_title_tab()
	{ 	
		if ( !empty( $this->contact['lastname']) || !empty( $this->contact['firstname']) )
			$name = '&laquo;'.$this->contact['lastname'].' '.$this->contact['firstname'].'&raquo;';
		else
			$name = '';
		$title = sprintf( __('Статистика отправлений подписчику %s','usam'), $name );
		return $title;
	}
	
	protected function toolbar_buttons( ) { }

	public function display()
	{					
		$this->display_toolbar();	
		usam_add_box( 'usam_statistics', __('Сводная cтатистика','usam'), array( $this, 'statistics_meta_box' ) );	
		usam_add_box( 'usam_sending', __('Отправления клиенту','usam'), array( $this, 'sending_meta_box' ) );	
    }	
	
	public function statistics_meta_box()
	{					
		$clicked = 0;
		$open = 0;
		foreach ( $this->statistics as $statistic )
		{
			$clicked += $statistic->clicked;
			if ( !empty($statistic->opened_at) )
				$open++;
		}		
		?>			
		<table class ='usam_table'>
			<tbody>	
				<tr><td><?php _e('Отправлено', 'usam'); ?>:</td><td><?php echo count($this->statistics); ?></td></tr>
				<tr><td><?php _e('Открыто', 'usam'); ?>:</td><td><?php echo $open; ?></td></tr>
				<tr><td><?php _e('Нажатий', 'usam'); ?>:</td><td><?php echo $clicked; ?></td></tr>
			</tbody>
		</table>		
		<?php	
    }		
		   
	public function sending_meta_box()
	{			
		?>
		<table class ='usam_list_table'>
			<thead>
				<tr>
					<td><?php _e('Отправлено', 'usam'); ?></td>
					<td><?php _e('Открыто', 'usam'); ?></td>
					<td><?php _e('Нажатий', 'usam'); ?></td>											
				</tr>
			</thead>
			<tbody>	
				<?php			
				$i = 0;
				foreach ( $this->statistics as $item )
				{		
				?>				
					<tr>
						<td><?php echo usam_local_date( $item->sent_at ); ?></td>						
						<td>
							<?php 
							if ( !empty($item->opened_at) )
								echo usam_local_date( $item->opened_at );
							else
								_e('Не открыто', 'usam'); 
							?>
						</td>
						<td><?php echo $item->clicked; ?>						
						</td>											
					</tr>
				<?php			
				}	
				?>		
			</tbody>
		</table>
		<?php	
    }	
}
?>