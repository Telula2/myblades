<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/newsletter/includes/newsletter_list_table.php' );
class USAM_List_Table_sending_sms extends USAM_List_Table_Newsletter
{
	protected $type = 'sms';		
	
	function column_subject( $item ) 
    { 
		if ( $item->status == 0)
		{
			$actions = array(
				'edit' => $this->add_row_actions( $item->id, 'edit', __( 'Изменить', 'usam' ) ),					
			);	
		}
		elseif( $item->status == 5 || $item->status == 6 )
		{
			$actions = array(
				'view'      => $this->add_row_actions( $item->id, 'view', __( 'Посмотреть', 'usam' ) ),		
				'copy'      => $this->add_row_actions( $item->id, 'copy', __( 'Скопировать', 'usam' ) ),		
			);			
		}
		$actions['delete'] = $this->delete_url( $item->id );
		$this->row_actions_table( $item->subject, $actions );	
    }	
	
	function get_columns()
	{
        $columns = array(           
			'cb'          => '<input type="checkbox" />',
			'action'      => '<span class="usam-dashicons-icon" title="' . esc_attr__( 'Действия' ) . '">'.__( 'Действия' ).'</span>',
			'subject'     => __( 'Название рассылки', 'usam' ),
			'body'        => __( 'Сообщение', 'usam' ),
			'status'      => __( 'Статус', 'usam' ),		
			'opened'      => __( 'Открыли, нажали, отписались', 'usam' ),			
			'lists'       => __( 'Списки рассылок', 'usam' ),
			'date_update' => __( 'Дата изменения', 'usam' ),	
            'sent_at'     => __( 'Дата отправки', 'usam' ),				
        );		
        return $columns;
    }	
}
?>