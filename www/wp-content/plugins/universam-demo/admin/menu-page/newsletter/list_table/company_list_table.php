<?php
class USAM_List_Table_company extends USAM_List_Table
{
	private $lists;
	private $user_lists = array();
	protected $order = 'DESC';

	function __construct( $args = array() )
	{			
		parent::__construct( $args );
		
		$this->lists = usam_get_subscribers_list( );
    }
	
	protected function bulk_actions( $which = '' ) 
	{
		if ( is_null( $this->_actions ) ) {
			$no_new_actions = $this->_actions = $this->get_bulk_actions();			
			$this->_actions = apply_filters( "bulk_actions-{$this->screen->id}", $this->_actions );
			$this->_actions = array_intersect_assoc( $this->_actions, $no_new_actions );
			$two = '';
		} 
		else 
		{
			$two = '2';
		}

		if ( empty( $this->_actions ) )
			return;
		
		echo '<label for="bulk-action-selector-' . esc_attr( $which ) . '" class="screen-reader-text">' . __( 'Select bulk action' ) . '</label>';
		echo '<select name="action' . $two . '" id="bulk-action-selector-' . esc_attr( $which ) . "\">\n";
		echo '<option value="-1">' . __( 'Bulk Actions' ) . "</option>\n";		
		foreach ( $this->_actions as $name => $title ) 
		{
			$class = 'edit' === $name ? ' class="hide-if-no-js"' : '';
			echo "\t" . '<option value="' . $name . '"' . $class . '>' . $title . "</option>\n";
		}	
		echo '<optgroup label="' . __( 'Переместить в список', 'usam' ) . '">';
		foreach ( $this->lists as $key => $item )
		{
			echo "\t" . '<option value="movetolist-'.$item['id'].'">' . $item['title'] . "</option>\n";
		}		
		echo '</optgroup>';
		echo '<optgroup label="' . __( 'Добавить в список', 'usam' ) . '">';
		foreach ( $this->lists as $key => $item )
		{
			echo "\t" . '<option value="copytolist-'.$item['id'].'">' . $item['title'] . "</option>\n";
		}
		echo '</optgroup>';
		echo '<optgroup label="' . __( 'Удалить из списока', 'usam' ) . '">';
		foreach ( $this->lists as $key => $item )
		{
			echo "\t" . '<option value="removefromlist-'.$item['id'].'">' . $item['title'] . "</option>\n";
		}
		echo '</optgroup>';			
		echo "</select>\n";

		submit_button( __( 'Apply' ), 'action', '', false, array( 'id' => "doaction$two" ) );
		echo "\n";
	}
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'     => __( 'Удалить из всех списков', 'usam' ),				
		);
		return $actions;
	}
	
	public function extra_tablenav( $which ) 
	{		
		global $type_price;
		if ( 'top' == $which )
		{					
			$select = isset($_GET['list'])?$_GET['list']:'all'; 
			?>				
			<select name="list">				
				<option value="all" <?php selected( $select, 'all' ); ?>><?php echo esc_html__( 'Все', 'usam'); ?></option>	
				<option value="0" <?php selected( $select, 0 ); ?>><?php echo esc_html__( 'Не добавленны', 'usam'); ?></option>	
				<?php
				foreach ( $this->lists as $key => $item )
				{
					?><option value="<?php echo $item['id']; ?>" <?php selected( $select, $item['id'] ); ?>><?php echo $item['title']; ?></option>	
				<?php } ?>
			</select>		
			<?php
			submit_button( __( 'Выбрать', 'usam' ), 'secondary', '', false, array( 'id' => 'filter-submit' ) );				
		}		
	}
	
	public function get_views() 
	{	
		global $wpdb;	
		$current_status = isset($_GET['status'])?$_GET['status']:'all';				
		
		$query = "SELECT status, COUNT( * ) AS num FROM ".USAM_TABLE_SUBSCRIBER_LISTS." WHERE 1=1 GROUP BY status";
		$count_status = (array) $wpdb->get_results( $query );	
		
		$total_items = 0;
		foreach ( $count_status as $item )
			$total_items += $item->num;	
		
		$all_text = sprintf(__( 'Всего <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $total_items ) );
		$all_href = remove_query_arg( array('status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );
		$all_class = ( $current_status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );

		foreach ( $count_status as $item )
		{				
			$text = '';			
			switch( $item->status )
			{							
				case 0:		
					$text = __( 'Неподтвержден', 'usam' );			
				break;
				case 1:			
					$text = __( 'Подписан', 'usam' );			
				break;
				case 2:	
					$text = __( 'Отписан', 'usam' );			
				break;
			}			
			$text = $text." <span class='count'>($item->num)</span>";
			$href = add_query_arg( 'status', $item->status );
			$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's','orderby','order'), $href );
			$class = ( $current_status == $item->status ) ? 'class="current"' : '';
			$views[$item->status] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}	
		return $views;
	}
	
	function column_status( $item )
    {					
		switch( $item->status )
		{							
			case 0:		
				_e( 'Неподтвержден', 'usam' );			
			break;
			case 1:			
				_e( 'Подписан', 'usam' );			
			break;
			case 2:	
				_e( 'Отписан', 'usam' );			
			break;
		}
    }
	
	function column_subscriber( $item )
    {					
		$actions = $this->standart_row_actions( $item->id );	
		$actions['statistics'] = $this->add_row_actions( $item->id, 'statistics', __( 'Статистика', 'usam' ) );
		
		$this->row_actions_table( $item->name, $actions );
    }
	
	function column_email( $item )
    {	
		$communication = usam_get_customer_communication( $item->id, 'company' );	
		$communication = usam_get_subscriber_data( $communication );	
		if ( isset($communication['email']) )
		{
			$i = 0;		
			foreach( $communication['email'] as $data)
			{	
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo $data['value']."<br>";				
				if ( isset($data['list']) )				
					foreach( $data['list'] as $list)
					{					
						echo $this->lists[$list['list']]['title'].' ';
					}				
				$i++;
			}		
		}
    }   
	
	function column_phone( $item )
    {	
		$communication = usam_get_customer_communication( $item->id, 'company' );	
		$communication = usam_get_subscriber_data( $communication );	
		if ( isset($communication['phone']) )
		{
			$i = 0;			
			foreach( $communication['phone'] as $data)
			{	
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo $data['value']."<br>";				
				if ( isset($data['list']) )				
					foreach( $data['list'] as $list)
					{					
						echo $this->lists[$list['list']]['title'].' ';
					}				
				$i++;
			}		
		}
    }   
	 
	function get_sortable_columns()
	{
		$sortable = array(
			'email'      => array('value', false),		
			'phone'      => array('value', false),	
			'status'     => array('status', false),	
			'date'       => array('date_insert', false),				
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',			
			'subscriber' => __( 'Подписчик', 'usam' ),
			'email'      => __( 'Список для email', 'usam' ),			
			'phone'      => __( 'Список для телефонов', 'usam' ),			
			'opened'     => __( 'Последнее открытое', 'usam' ),
			'clicked'    => __( 'Последнее нажатое', 'usam' ),
			'date'       => __( 'Дата создания', 'usam' ),			
        );		
        return $columns;
    }
	
	function prepare_items() 
	{		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 		
			'search' => $this->search, 	
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'cache_results' => true,	
			'cache_communication' => true,	
			'is_communication' => true,				
		);			
		if ( !empty($this->records) )
			$query['include'] = $this->records;	
		
		if ( isset($_REQUEST['status']) )
			$query['status_subscriber'] = array($_REQUEST['status']);
		
		if ( isset($_REQUEST['list']) && $_REQUEST['list'] != 'all' )
		{
			if ( $_REQUEST['list'] > 0 )
				$query['list_subscriber'] = array($_REQUEST['list']);	
			elseif ( $_REQUEST['list'] == 0 )
				$query['not_subscriber'] = 'not';		
		}		
		$_contacts = new USAM_Companies_Query( $query );
		$this->items = $_contacts->get_results();		
		$this->total_items = $_contacts->get_total();
		
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>