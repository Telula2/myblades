<?php
class USAM_List_Table_report_subscriber extends USAM_List_Table
{	
	private $user_stats = array();	
	private $email_id;
	private $customer_type = 'contact';	

	function __construct( $args = array() )
	{			
		parent::__construct( $args );	

		$this->customer_type = isset( $_REQUEST['customer_type'] ) ? sanitize_title($_REQUEST['customer_type']) : 'contact';		
		if ( !empty( $_REQUEST['n']) )
			$this->email_id = absint($_REQUEST['n']);
		else
			return;		
    }
	
	public function get_views() 
	{	
		global $wpdb;
		$current_link_filter = isset($_GET['link_filter'])?$_GET['link_filter']:'all';	
		
		$count_open = $wpdb->get_var( "SELECT COUNT(*) AS num FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat INNER JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS com ON ( com.id = stat.id_communication AND com.customer_type = '$this->customer_type') WHERE stat.newsletter_id=$this->email_id AND stat.opened_at IS NOT NULL" );	
		$count_clicked = $wpdb->get_var( "SELECT COUNT(*) AS num FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat INNER JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS com ON ( com.id = stat.id_communication AND com.customer_type = '$this->customer_type') WHERE stat.newsletter_id=$this->email_id AND stat.clicked > 0" );	
		$count_total = $wpdb->get_var( "SELECT COUNT(*) AS num FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat INNER JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS com ON ( com.id = stat.id_communication AND com.customer_type = '$this->customer_type') WHERE stat.newsletter_id=$this->email_id" );		
		$count_not_open = $count_total - $count_open;	

		$all_text = sprintf(__( 'Все отправленные <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $count_total ) );
		$all_href = remove_query_arg( array('link_filter', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );
		$all_class = ( $current_link_filter == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );
		
		$array = array( array( 'text' => __( 'Нажато', 'usam' ), 'count' => $count_clicked, 'link_filter' => 'clicked' ), 
						array( 'text' => __( 'Открыто', 'usam' ), 'count' => $count_open, 'link_filter' => 'open' ), 
						array( 'text' => __( 'Не открыто', 'usam' ), 'count' => $count_not_open, 'link_filter' => 'not_open' ) 
					);

		$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's','orderby','order') );
		foreach ( $array as $item )
		{		
			$text = $item['text']." <span class='count'>(".$item['count'].")</span>";
			$href = add_query_arg( 'link_filter', $item['link_filter'] );		
			$class = ( $current_link_filter == $item['link_filter'] ) ? 'class="current"' : '';
			$views[$item['link_filter']] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}		
		return $views;
	}
	
	function column_subscriber( $item ) 
    { 
		$title = $item->lastname.' '.$item->firstname;		
		$url = $this->get_nonce_url( add_query_arg(array('id' => $item->id, 'action' => 'statistics', 'tab' => 'subscriber', 'page' => 'newsletter'), admin_url( 'admin.php' ) ) );
		$actions = array(				
			'statistics' => '<a class="usam-statistics-link" href="'.$url.'">'.__( 'Статистика', 'usam' ).'</a>'
		);		
		$this->row_actions_table( $title, $actions );	
	}
	
	function column_opened_at( $item ) 
    {				
		if ( !empty($item->opened_at) )
		{		
			echo usam_local_date( $item->opened_at );	
		}
	}
	
	function column_sent_at( $item ) 
    {				
		if ( !empty($item->sent_at) )
		{		
			echo usam_local_date( $item->sent_at );	
		}
	}
	
	function column_email( $item ) 
	{		
		echo $item->value;
	}	
	
	function column_unsub( $item ) 
	{		
		if ( $item->unsub )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}
	
	function column_contact_source( $item ) 
    {		
		echo usam_get_name_contact_source( $item->contact_source );
	}
		
	function column_list( $item )
    {	
		if ( isset($this->user_lists[$item->id]) ) 
			foreach ( $this->user_lists[$item->id] as $item )
			{			
				echo $this->lists[$item->list]['title'];
			}
    }   
	
	public function type_dropdown() 
	{				
		?>	
		<select name="customer_type">			
			<option <?php selected( "contact", $this->customer_type ); ?> value="contact"><?php _e('Контакт', 'usam'); ?></option>		
			<option <?php selected( "company", $this->customer_type ); ?> value="company"><?php _e('Компания', 'usam'); ?></option>				
		</select>
		<?php
	}	
	
	public function source_dropdown() 
	{	
		$selected = isset( $_REQUEST['source'] ) ? absint($_REQUEST['source']) : 0;		
		?>	
		<select name="source">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Источник', 'usam'); ?></option>
			<?php	
			$contact_source = get_option('usam_crm_contact_source',array());
			foreach ( $contact_source as $value )
			{
				?><option <?php selected( $value['id'], $selected ); ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'type' => array(), 'source' => array() );		
	}	
	
	 
	function get_sortable_columns()
	{
		$sortable = array(					
			'opened_at'  => array('opened_at', false),
			'clicked'    => array('clicked', false),	
			'sent_at'    => array('sent_at', false),				
			'contact_source'    => array('contact_source', false),	
			'unsub'    => array('unsub', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           			
			'subscriber' => __( 'Подписчик', 'usam' ),
			'contact_source' => __( 'Источник', 'usam' ),
			'email'      => __( 'Email', 'usam' ),		
			'sent_at'  => __( 'Отправлено', 'usam' ),			
			'opened_at'  => __( 'Дата открытия', 'usam' ),
			'clicked'    => __( 'Нажатий', 'usam' ),							
			'unsub'      => __( 'Отписался', 'usam' ),	
        );		
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;		
				
		$this->get_standart_query_parent( );	
		
		$where = array( "stat.newsletter_id=$this->email_id" );
		if ( $this->search != '' )		
		{
			$search_terms = explode( ' ', $this->search );
			$search_sql = array();		
			$search_sql[] = "value LIKE '%{$this->search}%'";				
			foreach ( $search_terms as $term )
			{
				$search_sql[$term][] = "value LIKE '%".esc_sql( $term )."%'";
				if ( is_numeric( $term ) )
					$search_sql[$term][] = 'id = ' . esc_sql( $term );
				$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
			}
			$search_sql = implode( ' OR ', array_values( $search_sql ) );
			if ( $search_sql )
			{
				$where[] = "($search_sql)";
			}	
		}			
		if ( !empty($_REQUEST['link_filter']) )
		{
			
			switch ($_REQUEST['link_filter'] ) 
			{
				case 'clicked' :
					$where[] = "stat.clicked > 0";
				break;
				case 'open' :
					$where[] = "stat.opened_at IS NOT NULL";
				break;
				case 'not_open' :
					$where[] = "stat.opened_at IS NULL";
				break;
			}
		}				
		$_where = implode( ' AND ', $where );	
		
		switch ( $this->orderby ) 
		{			
			case 'contact_source' :			
			case 'id' :
				$orderby = $this->customer_type.".$this->orderby";
			break;			
			case 'opened_at' :				
			case 'clicked' :
			case 'unsub' :
			default:
				$orderby = "stat.$this->orderby";
			break;
		}	
		if ( $this->customer_type == 'contact' )
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS *
			FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat 
			INNER JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS com ON ( com.id = stat.id_communication AND com.customer_type = 'contact') 
			LEFT JOIN " . USAM_TABLE_CONTACTS . " AS contact ON ( contact.id = com.contact_id )			
			WHERE $_where ORDER BY {$orderby} {$this->order} {$this->limit}";		
		}
		else
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS *
			FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." AS stat 
			INNER JOIN " . USAM_TABLE_MEANS_COMMUNICATION . " AS com ON ( com.id = stat.id_communication AND com.customer_type = 'company') 
			LEFT JOIN " . USAM_TABLE_COMPANY . " AS company ON ( company.id = com.contact_id)		
			WHERE $_where ORDER BY {$orderby} {$this->order} {$this->limit}";	
		}	
		$this->items = $wpdb->get_results( $sql );
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		

		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>