<?php
class USAM_List_Table_lists extends USAM_List_Table
{
	function __construct()
	{	
       parent::__construct( array(
            'singular'  => 'record',      
            'plural'    => 'records',   
            'ajax'      => true       //поддерживать Ajax для таблицы
		) );	
    }	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'     => __( 'Удалить', 'usam' ),				
		);
		return $actions;
	}
		
	function column_title( $item )
	{
		$actions = $this->standart_row_actions( $item['id'] );			
		$this->row_actions_table( $item['title'], $actions );
    }
	
	function get_sortable_columns()
	{
		$sortable = array(
			'title'            => array('title', false),		
			'subscribed'       => array('subscribed', false),		
			'unconfirmed'      => array('unconfirmed', false),
			'daunsubscribedta' => array('unsubscribed', false),
			'data'             => array('data', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'            => '<input type="checkbox" />',			
			'title'         => __( 'Название списка', 'usam' ),
			'subscribed'    => __( 'Подписан', 'usam' ),					
			'unconfirmed'   => __( 'Неподтвержден', 'usam' ),
			'unsubscribed'  => __( 'Отписан', 'usam' ),		
			'description'   => __( 'Описание', 'usam' ),						
			'date'          => __( 'Дата создания', 'usam' ),			
        );		
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;		
		$lists = usam_get_subscribers_list( );
		$total_items = 0;	
		foreach ( (array)$lists as $item )
		{
			if ( empty($this->records) || in_array($item['id'], $this->records) )
			{
				$this->items[$total_items] = $item;			
				$results = $wpdb->get_results("SELECT COUNT(*) AS count, status FROM ".USAM_TABLE_SUBSCRIBER_LISTS." WHERE list = '".$item['id']."' GROUP BY status" );
			
				$subscribed = 0;
				$unconfirmed = 0;
				$unsubscribed = 0;
				foreach ( $results as $result )
				{
					switch ( $result->status ) 
					{
						case 1 :
							$subscribed = $result->count;
						break;
						case 2 :
							$unconfirmed = $result->count;
						break;
						case 3 :
							$unsubscribed = $result->count;
						break;	
					}
				}				
				$this->items[$total_items]['subscribed'] = $subscribed;
				$this->items[$total_items]['unconfirmed'] = $unconfirmed;			
				$this->items[$total_items]['unsubscribed'] = $unsubscribed;		
				$total_items++;
			}
		}		
		$this->total_items = count($total_items);			
		$this->forming_tables();		
	}
}
?>