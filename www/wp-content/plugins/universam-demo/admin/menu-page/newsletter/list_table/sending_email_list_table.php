<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/newsletter/includes/newsletter_list_table.php' );

class USAM_List_Table_sending_email extends USAM_List_Table_Newsletter
{
	protected $type = 'mail';	
	
	function column_subject( $item ) 
    {		
		if ( $item->status == 0 || $item->class == 'T' )
		{
			$actions = array(
				'edit' => $this->add_row_actions( $item->id, 'edit', __( 'Изменить', 'usam' ) ),					
			);	
		}
		if( $item->status == 5 || $item->status == 6 )
		{
			$actions = array(
				'view_report'    => $this->add_row_actions( $item->id, 'view_report', __( 'Посмотреть', 'usam' ) ),	
				'ya'      => $this->add_row_actions( $item->id, 'ya', __( 'Яндекс', 'usam' ) ),					
				'copy'    => $this->add_row_actions( $item->id, 'copy', __( 'Скопировать', 'usam' ) ),						
			);			
		}		
		$actions['delete'] = $this->delete_url( $item->id );
		$this->row_actions_table( $item->subject, $actions );	
    }	
	
	function column_opened( $item )
    {		
		switch ( $item->status ) 
		{			
			case 0 :
			break;
			case 1 :		
			break;
			case 2 :
	
			break;
			case 4 :
			case 5 :
			case 6 :
				if ( $item->number_sent > 0 )
				{
					$rate_opened = round($item->number_opened*100/$item->number_sent,1);
					$rate_clicked = round($item->number_clicked*100/$item->number_sent,1);
					$rate_unsub = round($item->number_unsub*100/$item->number_sent,1);
				}
				else
				{
					$rate_opened = 0;	
					$rate_clicked = 0;	
					$rate_unsub = 0;	
				}
				?>
				<a href="<?php echo admin_url('admin.php?page=newsletter&tab=sending_email&table=report_subscriber&n='.$item->id) ?>" class="stats" title="<?php echo $item->number_opened.' - '.$item->number_clicked.' - '.$item->number_unsub; ?>">
					<?php echo $rate_opened . '% - ' . $rate_clicked . '% - ' . $rate_unsub . '%'; ?>
				</a>
				<?php
				
				if ( $item->type == 'T' )
					echo "<span class ='stat'>".$this->get_stat_triger($item->id)."</span>";
				else
					echo "<span class ='stat'>".$this->get_stat($item->id)."</span>";
			break;			
		}	
	}	

	function get_columns()
	{
        $columns = array(           
			'cb'          => '<input type="checkbox" />',
			'action'      => '<span class="usam-dashicons-icon" title="' . esc_attr__( 'Действия' ) . '">'.__( 'Действия' ).'</span>',
			'subject'     => __( 'Название рассылки', 'usam' ),
			'status'      => __( 'Статус', 'usam' ),			
			'class'       => __( 'Тип', 'usam' ),	
			'lists'       => __( 'Списки рассылок', 'usam' ),
			'opened'      => __( 'Открыли, нажали, отписались', 'usam' ),
			'date_update' => __( 'Дата изменения', 'usam' ),	
            'sent_at'     => __( 'Дата отправки', 'usam' ),				
        );		
        return $columns;
    }
}
?>