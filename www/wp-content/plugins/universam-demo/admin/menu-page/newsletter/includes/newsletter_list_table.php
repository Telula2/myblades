<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/newsletter/includes/newsletter_list_table.php' );
require_once( USAM_FILE_PATH . '/includes/mailings/newsletter_query.class.php' );

class USAM_List_Table_Newsletter extends USAM_List_Table
{
	private $status  = 'all';
	protected $order = 'asc'; 
	
	protected $type = 'mail';	
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }	
	
	public function get_views() 
	{	
		global $wpdb;
		
		$sql = "SELECT COUNT(*) AS count, status FROM ".USAM_TABLE_NEWSLETTER_TEMPLATES." WHERE type ='$this->type' GROUP BY status";
		$num_stat = $wpdb->get_results( $sql );
		
		$current_status = isset($_GET['post_status'])?$_GET['post_status']:'all';
		$total_count = 0;
		if ( ! empty( $num_stat ) )
		{		
			foreach ( $num_stat as $value )
			{
				$total_count = $value->status;			
			}
		}
		$all_text = sprintf(__( 'Всего <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $total_count ) );
		$all_href = remove_query_arg( array('post_status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );
		$all_class = ( $current_status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );

		foreach ( $num_stat as $value )
		{
			if ( $value->count == 0 )
				continue;
			switch ( $value->status ) 
			{			
				case 0 :
					$label = __('Черновик','usam');				
				break;				
				case 4 :
					$label = __('В паузе','usam');						
				break;
				case 5 :
					$label = __('Отправляются','usam');							
				break;
				case 6 :
					$label = __('Отправленые','usam');					
				break;
			}					
			$text = $label." <span class='count'>($value->count)</span>";
			$href = add_query_arg( 'post_status', $value->status );
			$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's','orderby','order'), $href );
			$class = ( $current_status == $value->status ) ? 'class="current"' : '';
			$views[$value->status] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}
		return $views;
	}

	function get_bulk_actions_display() 
	{			
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	protected function get_stat( $id )
    {		
		global $wpdb;
		$sql = "SELECT COUNT(*) AS count, status FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." WHERE newsletter_id = '$id' GROUP BY status";
		$stat = $wpdb->get_results( $sql );
		
		$not_sent = $sent = 0;		
		foreach ( $stat as $value )
		{			
			if ( $value->status == 0 )			
				$not_sent = (int)$value->count;			
			elseif ( $value->status == 1 )			
				$sent = (int)$value->count;				
		}				
		$total = $sent + $not_sent;
		if ( $sent > 0 )
			$p = " (".round($sent/$total*100,2)."%)";
		else
			$p = '';
		return '<br>'.sprintf( __( 'Отправлено %s из %s', 'usam' ), $sent, $total ).$p;			
	}
	
	protected function get_stat_triger( $id )
    {		
		global $wpdb;		
		$total = $wpdb->get_var( "SELECT COUNT(*) AS count FROM ".USAM_TABLE_NEWSLETTER_USER_STAT." WHERE newsletter_id = '$id'" );		
		return '<br>'.sprintf( __( 'Отправлено %s', 'usam' ), $total );			
	}
	
	function column_opened( $item )
    {		
		switch ( $item->status ) 
		{			
			case 0 :
			break;
			case 1 :		
			break;
			case 2 :
	
			break;
			case 4 :
			case 5 :
			case 6 :
				if ( $item->number_sent > 0 )
				{
					$rate_opened = round($item->number_opened*100/$item->number_sent,1);
					$rate_clicked = round($item->number_clicked*100/$item->number_sent,1);
					$rate_unsub = round($item->number_unsub*100/$item->number_sent,1);
				}
				else
				{
					$rate_opened = 0;	
					$rate_clicked = 0;	
					$rate_unsub = 0;	
				}
				?>
				<a href="<?php echo wp_nonce_url(admin_url('admin.php?page=newsletter&tab=sending_email&action=view&id='.$item->id)) ?>" class="stats" title="<?php echo $item->number_opened.' - '.$item->number_clicked.' - '.$item->number_unsub; ?>">
					<?php echo $rate_opened . '% - ' . $rate_clicked . '% - ' . $rate_unsub . '%'; ?>
				</a>
				<?php
				
				if ( $item->type == 'T' )
					echo "<span class ='stat'>".$this->get_stat_triger($item->id)."</span>";
				else
					echo "<span class ='stat'>".$this->get_stat($item->id)."</span>";
			break;			
		}	
	}
	
	function column_lists( $item )
    {
		$lists = usam_get_newsletter_list( $item->id );
		foreach ( $lists as $list )
		{
			echo usam_get_name_list_of_subscribers( $list )."<br>";			
		}	
	}	
	
	function column_action( $item )
    {				
		$title = '';
		$send = '';
		switch ( $item->status ) 
		{					
			case 5 :
				$title = __('Остановить','usam');
				
				if ( $item->type !== 'T' )
				{
					$url = add_query_arg( array( 'action' => 'sending' ), $this->item_url( $item->id ) );					
					$send = "<br><a href='".$url."' class='action-send-editor' title ='".__( 'Отправить партию', 'usam' )."'><span class ='usam-dashicons-icon'></span></a>";				
				}
			break;
			case 4 :
				$title = __('Запустить','usam');
			break;
		}		
		echo "<span id ='status-{$item->status}' class ='action usam-dashicons-icon' title ='$title' data-mail_id='$item->id'></span>";
		echo $send;
	}
		
	function column_status( $item )
    {				
		if ( $item->type == 'T' )
		{			
			switch ( $item->status ) 
			{			
				case 0 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('Черновик','usam')."</span>";
				break;			
				case 5 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('Отправляю','usam')."</span>";					
				break;
				case 4 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('В паузе','usam')."</span>";					
				break;
			}
		}
		else
		{
			switch ( $item->status ) 
			{			
				case 0 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('Черновик','usam')."</span>";
				break;
				case 1 :
			//		_e('Черновик','usam');
				break;
				case 4 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('В паузе','usam')."</span>";
				break;
				case 5 :
					echo "<span id ='status-{$item->status}' class ='status'>".__( 'Отправляю', 'usam' )."</span>";
				break;
				case 6 :
					echo "<span id ='status-{$item->status}' class ='status'>".__('Отправлено','usam')."</span>";				
				break;
			}
		}
    }
	
	function column_class( $item )
    { 
		switch ( $item->class ) 
		{			
			case 'T' :
				_e('Тригирная','usam');
			break;
			case 'S':
				_e('Стандартная','usam');
			break;				
		}
	}
	
	function column_sent_at( $item )
    {
		if ( !empty($item->sent_at) )
			echo usam_local_date( $item->sent_at );	
		else
			_e('Еще не отправлено','usam');
	}
   	  
	function get_sortable_columns()
	{
		$sortable = array(
			'title'       => array('name', false),		
			'status'      => array('status', false),		
			'class'       => array('class', false),		
			'date_update' => array('date_update', false),		
			'sent_at'     => array('sent_at', false),		
			);
		return $sortable;
	}
		
	function prepare_items() 
	{		
		$args = array( 'cache_results' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'type' => $this->type, 'orderby' => $this->orderby );	
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );			
			if ( $this->status != 'all' ) 
			{			
				$args['status'] = $this->status;
			}			
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => $_REQUEST['selectc'], 'compare' => $_REQUEST['compare'], 'value' => $_REQUEST['condition_v'] ) );				
			}
		}
		$query_orders = new USAM_Newsletters_Query( $args );
		$this->items = $query_orders->get_results();
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}
?>