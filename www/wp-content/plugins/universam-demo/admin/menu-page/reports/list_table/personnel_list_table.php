<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_personnel extends USAM_Main_Report_List_Table
{	
    public $groupby_date = 'week';
	function __construct( $args = array() )
	{			
		$this->filters = array( 'interval_date' => '','date' => '','report_manager' => '', 'weekday' => '' );		
		parent::__construct( $args );				
    }
	
	public function column_date( $item ) 
	{	
		$timestamp = (int) $item['date'];
		echo date( 'd.m.Y', $timestamp );			
	}
	
	function get_columns()
	{
        $columns = array(   			
			'date'      => __( 'Дата', 'usam' ),			
			'products'  => __( 'Опубликованные товары', 'usam' ),
			'orders'    => __( 'Обработанные заказы', 'usam' ),
			'order_sum' => __( 'Сумма заказов', 'usam' ),
			'messages'  => __( 'Обработанные сообщения', 'usam' ),		
			'quantity_suggestion' => __( 'Предложения (кол-во)', 'usam' ),
			'cost_suggestion'     => __( 'Предложения (Рубль)', 'usam' ),				
			'quantity_invoice'    => __( 'Счета (кол-во)', 'usam' ),
			'cost_invoice'        => __( 'Счета (Рубль)', 'usam' ),
			'meeting'   => __( 'Встречь', 'usam' ),
			'call'      => __( 'Звонков', 'usam' ),
			'task'      => __( 'Заданий', 'usam' ),
			'event'     => __( 'Событий', 'usam' ),				
        );
        return $columns;
    }
	
	function prepare_items() 
	{	
		require_once( USAM_FILE_PATH . '/includes/crm/documents_query.class.php' );	
		
		$this->_column_headers = $this->get_column_info();	

		$records = array();				
		$weekday = !empty($_REQUEST['weekday'])?array_map( 'intval', $_REQUEST['weekday'] ):array( );		
		
		$start_date = strtotime($this->end_date_interval);
		$end_date = strtotime($this->start_date_interval);						
		
		$date_query = array( 
			array(
				'after'     => array( 'year'  => date('Y',$end_date),	'month' => date('m',$end_date), 'day'   => date('d',$end_date) ),
				'before'    => array( 'year'  => date('Y',$start_date),	'month' => date('m',$start_date), 'day'   => date('d',$start_date) ),
				'inclusive' => true,
				'dayofweek' => $weekday,
				'compare'   => 'IN',
			),
		);		
		
		$query = array(
			'date_query' => $date_query,		
	//		'status' => 'closed', 
			'fields' => array('totalprice','date_insert'),	
			'order' => 'DESC', 					
		);		
		if ( !empty($_REQUEST['manager']) )
			$query['manager_id'] = (int)$_REQUEST['manager'];
		$data_orders = usam_get_orders( $query );	
	
		$query = array(
			'date_query' => $date_query,	
			'fields' => array('date_insert'),	
			'order' => 'DESC', 					
			'status' => 2, 
		);			
		if ( !empty($_REQUEST['manager']) )
			$query['manager_id'] = (int)$_REQUEST['manager'];			
		$data_feedback = usam_get_feedback_query( $query );		
		
		$query = array(
			'date_query' => $date_query,	
			'cache_results' => false, 		
			'update_post_meta_cache' => false, 	
			'update_post_term_cache' => false, 			
		);	
		if ( !empty($_REQUEST['manager']) )
			$query['author'] = (int)$_REQUEST['manager'];			
		$data_products = usam_get_products( $query );
		
		$query = array(
			'date_query' => $date_query,
			'fields' => array('totalprice','date_insert', 'type'),	
			'order' => 'DESC', 					
		);			
		if ( !empty($_REQUEST['manager']) )
			$query['manager_id'] = (int)$_REQUEST['manager'];		
		$documents = usam_get_documents( $query );	
			
		$query = array(
			'date_query' => $date_query,
			'fields' => array('type','date_insert'),	
			'object_type' => 'contact', 
			'order' => 'DESC', 					
		);		
		if ( !empty($_REQUEST['manager']) )
			$query['user_id'] = (int)$_REQUEST['manager'];		
		$events = usam_get_tasks( $query );	
		
		$i = 0;	
		for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
		{			
			$records[$i]['date'] = $j;	
			$records[$i]['orders'] = 0;	
			$records[$i]['messages'] = 0;	
			$records[$i]['products'] = 0;			
			$records[$i]['order_sum'] = 0;	
			$records[$i]['quantity_suggestion'] = 0;	
			$records[$i]['cost_suggestion'] = 0;	
			$records[$i]['quantity_invoice'] = 0;	
			$records[$i]['cost_invoice'] = 0;		
			$records[$i]['meeting'] = 0;	
			$records[$i]['call'] = 0;	
			$records[$i]['task'] = 0;	
			$records[$i]['event'] = 0;					
			foreach ( $data_orders as $key => $item )
			{						
				if ( $j >= strtotime($item->date_insert) )
				{	
					break;
				}
				else
				{					
					$records[$i]['orders']++;	
					$records[$i]['order_sum'] += $item->totalprice;							
					unset($data_orders[$key]);					
				}
			}	
			foreach ( $data_feedback as $key => $item )
			{						
				if ( $j >= strtotime($item->date_insert) )
				{	
					break;
				}
				else
				{					
					$records[$i]['messages']++;											
					unset($data_feedback[$key]);					
				}
			}				
			foreach ( $data_products as $key => $item )
			{			
				if ( $j >= strtotime($item->post_date) )
				{	
					break;
				}
				else
				{	
					$records[$i]['products'] ++;	
					unset($data_products[$key]);					
				}
			}
			foreach ( $documents as $key => $item )
			{						
				if ( $j >= strtotime($item->date_insert) )
				{	
					break;
				}
				else
				{					 
					if ($item->type == 'suggestion' )
					{
						$records[$i]['quantity_suggestion']++;	
						$records[$i]['cost_suggestion'] += $item->totalprice;							
					}
					elseif ( $item->type == 'invoice' )
					{
						$records[$i]['quantity_invoice']++;	
						$records[$i]['cost_invoice'] += $item->totalprice;												
					}
					unset($documents[$key]);					
				}
			}
			foreach ( $events as $key => $item )
			{						
				if ( $j >= strtotime($item->date_insert) )
				{	
					break;
				}
				else
				{					
					if ( empty($records[$i][$item->type]) )
						$records[$i][$item->type] = 1;
					else
						$records[$i][$item->type]++;		
					unset($events[$key]);					
				}
			}
			$i++;
			$j = strtotime("-1 ".$this->groupby_date, $j);		
		}
		$this->items = $records;

		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['products'], 'label' => __('Количество', 'usam').': '.$this->currency_display( $item['products'] ) ) );
		}		
		usam_set_data_graph( $data_graph, __('Опубликованные товары','usam') );		
	}
}