<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_contact extends USAM_Main_Report_List_Table
{		
	private $status = 'all';
	public $groupby_date = 'month';
	private $total_amount = 0;
		
	public function __construct( $args = array() ) 
	{
		$this->filters = array( 'interval_date' => '','date' => 'month', 'report_company' => '', 'weekday' => '', 'manager' => '', );	
		parent::__construct( $args );
	}
	
	public function filter_groups_by_report_company()
	{			
		$contact = !empty($_REQUEST['contact'])?$_REQUEST['contact']:0;
		?>
		<td><label><?php _e( 'Выберите компанию' ); ?>:</label></td>
		<td><?php
				$autocomplete = new USAM_Autocomplete_Forms( );
				$autocomplete->get_form_contact( $contact );
			?>	
		</td>
	<?php	
	}	
	
	function get_columns()
	{	
        $columns = array(  		
			'date'                      => __( 'Дата', 'usam' ),
			'quantity_order'            => __( 'Заказов (кол-во)', 'usam' ),	
			'cost_order'                => __( 'Заказов (Рубль)', 'usam' ),	
			'quantity_pending'          => __( 'Заказов в работе (кол-во)', 'usam' ),
			'cost_pending'              => __( 'Заказов в работе (Рубль)', 'usam' ),
			'percent_pending'           => __( 'В ожидании (%)', 'usam' ),			
			'quantity_suggestion'       => __( 'Предложения (кол-во)', 'usam' ),
			'cost_suggestion'           => __( 'Предложения (Рубль)', 'usam' ),				
			'quantity_invoice'          => __( 'Счета (кол-во)', 'usam' ),
			'cost_invoice'              => __( 'Счета (Рубль)', 'usam' ),
			'meeting'                   => __( 'Встречь', 'usam' ),
			'call'                      => __( 'Звонков', 'usam' ),
			'task'                      => __( 'Заданий', 'usam' ),
			'event'                     => __( 'Событий', 'usam' ),	
        );
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->_column_headers = $this->get_column_info();	
		$records = array();	
		if ( !empty($_REQUEST['contact']) )
		{			
			require_once( USAM_FILE_PATH . '/includes/crm/documents_query.class.php' );	
			
			$contact_id = $_REQUEST['contact'];
			$weekday = !empty($_REQUEST['weekday'])?array_map( 'intval', $_REQUEST['weekday'] ):array( );		
			
			$start_date = strtotime($this->end_date_interval);
			$end_date = strtotime($this->start_date_interval);						
			
			$date_query = array( 
				array(
					'after'     => array( 'year'  => date('Y',$end_date),	'month' => date('m',$end_date), 'day'   => date('d',$end_date) ),
					'before'    => array( 'year'  => date('Y',$start_date),	'month' => date('m',$start_date), 'day'   => date('d',$start_date) ),
					'inclusive' => true,
					'dayofweek' => $weekday,
					'compare'   => 'IN',
				),
			);			
			$query = array(
				'date_query' => $date_query,	
				'fields' => array('totalprice','date_insert', 'status'),	
				'contacts' => $contact_id,	
				'order' => 'DESC', 					
			);				
			if ( !empty($_REQUEST['manager']) )
				$query['manager_id'] = (int)$_REQUEST['manager'];
			
			$data_orders = usam_get_orders( $query );
						
			$query = array(
				'date_query' => $date_query,
				'fields' => array('totalprice','date_insert', 'type'),	
				'contacts' => $contact_id,	
				'order' => 'DESC', 					
			);				
			$documents = usam_get_documents( $query );	
			
			
			$query = array(
				'date_query' => $date_query,
				'fields' => array('type','date_insert'),	
				'object_type' => 'contact', 
				'object_id' => $contact_id,
				'order' => 'DESC', 					
			);				
			$events = usam_get_tasks( $query );	

			$all_in_work = array();
			$order_statuses = usam_get_order_statuses( );	
			foreach ( $order_statuses as $key => $status )		
				if ( !$status->close )
					$all_in_work[] = $key;			
			
			$i = 0;	
			for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
			{			
				$records[$i]['date'] = $j;
				$records[$i]['quantity_order'] = 0;		
				$records[$i]['cost_order'] = 0;							
				$records[$i]['quantity_pending'] = 0;		
				$records[$i]['cost_pending'] = 0;	
				$records[$i]['percent_pending'] = 0;					
				$records[$i]['quantity_suggestion'] = 0;	
				$records[$i]['cost_suggestion'] = 0;	
				$records[$i]['quantity_invoice'] = 0;	
				$records[$i]['cost_invoice'] = 0;		
				$records[$i]['meeting'] = 0;	
				$records[$i]['call'] = 0;	
				$records[$i]['task'] = 0;	
				$records[$i]['event'] = 0;							
				foreach ( $data_orders as $key => $item )
				{						
					if ( $j >= strtotime($item->date_insert) )
					{	
						break;
					}
					else
					{					
						$records[$i]['quantity_order']++;	
						$records[$i]['cost_order'] += $item->totalprice;		

						if ( in_array($item->status, $all_in_work) )
						{
							$records[$i]['quantity_pending']++;	
							$records[$i]['cost_pending'] += $item->totalprice;	
							$records[$i]['cost_order'] += $item->totalprice;		
						}
						unset($data_orders[$key]);					
					}
				}	
				foreach ( $documents as $key => $item )
				{						
					if ( $j >= strtotime($item->date_insert) )
					{	
						break;
					}
					else
					{					 
						if ($item->type == 'suggestion' )
						{
							$records[$i]['quantity_suggestion']++;	
							$records[$i]['cost_suggestion'] += $item->totalprice;							
						}
						elseif ( $item->type == 'invoice' )
						{
							$records[$i]['quantity_invoice']++;	
							$records[$i]['cost_invoice'] += $item->totalprice;												
						}
						unset($documents[$key]);					
					}
				}
				foreach ( $events as $key => $item )
				{						
					if ( $j >= strtotime($item->date_insert) )
					{	
						break;
					}
					else
					{					
						if ( empty($records[$i][$item->type]) )
							$records[$i][$item->type] = 1;
						else
							$records[$i][$item->type]++;		
						unset($events[$key]);					
					}
				}
				$i++;
				$j = strtotime("-1 ".$this->groupby_date, $j);		
			}
			$this->items = $records;
	
			$data_graph = array();
			foreach ( $this->items as $item )
			{			
				array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['cost_order'], 'label' => __('Сумма', 'usam').': '.$this->currency_display( $item['cost_order'] ) ) );
			}		
			usam_set_data_graph( $data_graph, __('Продажи','usam') );	
		}
	}
}
?>