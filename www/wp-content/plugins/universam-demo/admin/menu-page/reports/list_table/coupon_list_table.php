<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_Coupon extends USAM_Main_Report_List_Table
{	    
	public $groupby_date = 'month';
	function __construct( $args = array() )
	{			
		$this->filters = array( 'interval_date' => '','date' => '','report_product' => '' );		
		 parent::__construct( $args );			
    }
	  
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'          => array('name', false),
			'sales_amount'  => array('sales_amount', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   			
			'date'            => __( 'Дата', 'usam' ),		
			'total'           => __( 'Количество', 'usam' ),
			'discount_min'    => __( 'Минимальная скидка', 'usam' ),		
			'discount_max'    => __( 'Максимальная скидка', 'usam' ),		
			'using'           => __( 'Использованых', 'usam' ),
			'unused'          => __( 'Не использованных', 'usam' ),
			'percent'         => __( '%', 'usam' ),		
			'coupon_discount' => __( 'Сумма скидки', 'usam' ),			
			'cost'            => __( 'Оборот', 'usam' ),			
        );
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->_column_headers = $this->get_column_info();
		
		$where = array( "c.coupon_type='coupon'" );
		$join  = array();
		
		$where[] = "c.start<='" . esc_sql(  date('Y-m-d H:i:s', strtotime($this->end_date_interval)) )."'";
		$where[] = "c.start>='" . esc_sql( date('Y-m-d H:i:s', strtotime($this->start_date_interval)) )."'";	
		
		if ( isset( $_REQUEST['groupby_user_role'] ) && $_REQUEST['groupby_user_role'] != '-1' )
		{
			$where[] = "p.user_role='".sanitize_title($_REQUEST['groupby_user_role'])."'";
		}		
		if ( ! empty( $_REQUEST['shipping'] ) && is_numeric($_REQUEST['shipping']) || ! empty( $_REQUEST['storage'] ) && is_numeric($_REQUEST['storage']) )
		{
			$join[] = " LEFT JOIN ( SELECT order_id, method, storage_pickup, storage FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE 1=1 ORDER BY date_insert DESC LIMIT 1) AS doc_ship ON (doc_ship.order_id=p.id)";			
			if ( isset($_REQUEST['shipping']) )
				$where[] = "doc_ship.method='".absint($_REQUEST['shipping'])."'";
			if ( !empty( $_REQUEST['storage'] ) )
				$where[] = "doc_ship.storage='".absint($_REQUEST['storage'])."'";
		}			
		$join[] = " LEFT JOIN ".USAM_TABLE_ORDERS." AS p ON (c.coupon_code=p.coupon_name AND p.status='closed' AND p.coupon_name != '')";			
		
		$selects = array( 'c.start, c.is_used, IFNULL(p.coupon_discount,0) AS coupon_discount, IFNULL(p.totalprice,0) AS totalprice');	
	//	$selects[] = ' ( SELECT COUNT(*) FROM '.USAM_TABLE_PRODUCTS_ORDER.' AS c WHERE c.order_id = p.id ) AS item_count';			

		$selects = implode( ', ', $selects );		
		$join = implode( ' ', $join );
		$where = implode( ' AND ', $where );
		
		$coupons = $wpdb->get_results("SELECT $selects FROM ".USAM_TABLE_COUPON_CODES." AS c $join WHERE $where ORDER BY c.id DESC");	
	//	$t_start_date = $wpdb->get_var("SELECT start FROM ".USAM_TABLE_COUPON_CODES." AS c WHERE $where ORDER BY c.start ASC LIMIT 1");	

		$start_period = true;	
		$format_price = array(
						'display_currency_symbol' => false,
						'decimal_point'   		  => true,
						'display_currency_code'   => false,
						'display_as_html'         => false,
						'code'                 => false,
					);		
		$records = array();	
		$i = 0;			
		for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
		{	
			$i++;			
			$records[$i]['date'] = $j;
			$records[$i]['total'] = 0; //Общее количество
			$records[$i]['cost'] = 0;				
			$records[$i]['percent'] = 0;		
			$records[$i]['coupon_discount'] = 0;
			$records[$i]['using'] = 0;	
			$records[$i]['unused'] = 0;	
			
			$min = 999999999999;			

			$records[$i]['discount_max'] = 0;		
			$records[$i]['discount_min'] = 0;
			foreach ( $coupons as $key => $item )
			{			
				if ( date('Y-m-d H:i:s',$j) > $item->start )
				{					
					if ( $records[$i]['coupon_discount'] > 0 && $records[$i]['cost'] > 0) 
						$records[$i]['percent'] = round($records[$i]['coupon_discount'] / $records[$i]['cost'] *100, 1);
					break;
				}
				else
				{	
					$records[$i]['total']++;
					if ( $item->is_used ) 
						$records[$i]['using']++;
					else
						$records[$i]['unused']++;
					
					$records[$i]['cost'] += $item->totalprice;						
					$records[$i]['coupon_discount'] += $item->coupon_discount;
					if ( $item->coupon_discount > $records[$i]['discount_max']) 
						$records[$i]['discount_max'] = $item->coupon_discount;					
					if ( $item->coupon_discount < $min && $item->coupon_discount > 0) 						
						$min = $item->coupon_discount;
				
					unset($coupons[$key]);					
				}
			}			
			if ( $min !== 999999999999 ) 
				$records[$i]['discount_min'] = $min;				
			
			$records[$i]['cost'] = usam_currency_display( $records[$i]['cost'], $format_price );			
			$j = strtotime("-1 ".$this->groupby_date, $j);	
		}		
		$this->items = $records;
		
		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['total'], 'label' => __('Количество', 'usam').': '.$this->currency_display( $item['total'] ) ) );
		}		
		usam_set_data_graph( $data_graph, __('Количество созданных','usam') );
	}
}