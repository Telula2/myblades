<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_Products extends USAM_Main_Report_List_Table
{	
    protected $orderby = "name";
	protected $order = "ASC";
	protected $prefix = '';
	protected $search_box = true;
	
	function __construct( $args = array() )
	{			
		$this->filters = array( 'interval_date' => '' );		
		parent::__construct( $args  );				
    }  
	
	public function column_name( $item ) 
	{	
		echo "<a href ='".add_query_arg( array('table' => 'product', 'product' => $item['id'] ), $this->url )."' >".$item['name']."</a>";
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'quantity' => array('quantity', false),	
			'total'    => array('total', false),		
			'id'    => array('id', false),	
			'name'    => array('name', false),	
			'min'    => array('min', false),	
			'max'    => array('max', false),						
			'views'    => array('views', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(   		
			'id'       => __( 'Идентификатор', 'usam' ),			
			'name'     => __( 'Название товара', 'usam' ),			
			'quantity' => __( 'Количество', 'usam' ),			
			'total'    => __( 'Сумма', 'usam' ),	
			'min'               => __( 'Минимальная цена', 'usam' ),
			'max'               => __( 'Максимальная цена', 'usam' ),				
			'views'             => __( 'Просмотры', 'usam' ),				
        );
        return $columns;
    }
	
	public function get_number_columns_sql()
    {       
		return array('shipping', 'quantity_shipping' );
    }
	
	public function date_interval_toolbar(  ) { }
	
	public function extra_tablenav( $which ) {	}
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->get_standart_query_parent();
		
		
		$this->where[] = "pl.date_insert <= '".date("Y-m-d H:i:s",strtotime($this->end_date_interval))."'";
		$this->where[] = "pl.date_insert >= '".date("Y-m-d H:i:s",strtotime($this->start_date_interval))."'";
		
		if ( $this->search != '' )
			$this->where[] = "po.name LIKE LOWER ('%".$this->search."%')";				
		
		$this->where[] = "pl.status = 'closed'";		
		$where = implode( ' AND ', $this->where );	

		switch ( $this->orderby ) 
		{					
			case 'quantity':	
				$this->orderby = 'SUM(po.quantity)';
			break;
			case 'total':	
				$this->orderby = 'SUM(po.quantity)*price';
			break;			
			case 'min':	
				$this->orderby = 'MIN(price)';
			break;
			case 'max':	
				$this->orderby = 'MAX(price)';
			break;
			case 'views':	
				$this->orderby = "pm.meta_value";
			break;
			case 'id':	
				$this->orderby = "po.product_id";
			break;			
		}		
		$select = "name, MIN(price) AS min, MAX(price) AS max, SUM(po.quantity)*price AS total, SUM(po.quantity) AS quantity, po.product_id AS id, IFNULL(pm.meta_value,0) AS views";
		$sql = "SELECT SQL_CALC_FOUND_ROWS $select 
		FROM ".USAM_TABLE_PRODUCTS_ORDER." AS po 
		LEFT JOIN ".USAM_TABLE_ORDERS." AS pl ON (po.order_id=pl.id)
		LEFT JOIN ( SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '".USAM_META_PREFIX."product_views' GROUP BY post_id ) AS pm ON (po.product_id = pm.post_id)
		WHERE $where GROUP BY po.product_id ORDER BY {$this->orderby} {$this->order} {$this->limit}";		
		
		$this->items = $wpdb->get_results($sql, ARRAY_A);	
		$this->total_items =  $total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		
		
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );		
	}
}