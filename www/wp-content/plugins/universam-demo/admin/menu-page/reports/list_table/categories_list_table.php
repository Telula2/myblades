<?php
require_once(ABSPATH . 'wp-admin/includes/class-wp-terms-list-table.php');
class USAM_List_Table_categories extends WP_Terms_List_Table
{	
	private $per_page;	
	public function __construct( $args = array() ) 
	{
		global $post_type, $taxonomy, $action, $tax;
		
		parent::__construct( array(
			'plural' => 'tags',
			'singular' => 'tag',
			'screen' => isset( $args['screen'] ) ? $args['screen'] : null,
		) );	
		
		$action    = $this->screen->action;
		$post_type = $this->screen->post_type;
		$taxonomy  = $this->screen->taxonomy = 'usam-category';
		
		if ( empty( $taxonomy ) )
			$taxonomy = 'post_tag';

		if ( ! taxonomy_exists( $taxonomy ) )
			wp_die( __( 'Invalid taxonomy' ) );

		$tax = get_taxonomy( $taxonomy );

		// @todo Still needed? Maybe just the show_ui part.
		if ( empty( $post_type ) || !in_array( $post_type, get_post_types( array( 'show_ui' => true ) ) ) )
			$post_type = 'post';
	}
	
	function get_bulk_actions() 
	{	
		return array();
	}	
	
	public function set_per_page( $per_page )
	{		
		$this->per_page = (int) $per_page;		
	}
	
	function column_views( $item ) 
	{		
		global $wpdb;	
		
		$children = get_term_children( $item->term_id, 'usam-category' );	
		$children[] = $item->term_id;		
		$cat_ids = implode( ',', $children);
						
		$query = "SELECT SUM(DISTINCT pm.meta_value) FROM ".$wpdb->posts." AS p INNER JOIN {$wpdb->prefix}term_relationships AS tr ON (p.ID = tr.object_id) INNER JOIN {$wpdb->prefix}term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) INNER JOIN ".$wpdb->postmeta." AS pm ON (p.ID = pm.post_id) WHERE p.post_parent = '0' AND tt.term_id IN ($cat_ids) AND p.post_type = 'usam-product' AND pm.meta_key = '_usam_product_views'";
		$count_views_prod = $wpdb->get_var($query);	
    }
	
	function column_product( $item ) 
	{		
		global $wpdb;	
		
		$children = get_term_children( $item->term_id, 'usam-category' );	
		$children[] = $item->term_id;
		$cat_ids = implode( ',', $children);		
		
		$query = "SELECT COUNT(DISTINCT p.ID) FROM ".$wpdb->posts." AS p INNER JOIN {$wpdb->prefix}term_relationships AS tr ON (p.ID = tr.object_id) INNER JOIN {$wpdb->prefix}term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) WHERE p.post_parent = '0' AND tt.term_id IN ($cat_ids) AND p.post_type = 'usam-product'";
		echo $wpdb->get_var($query);	
    }
	
	function column_stock( $item ) 
	{		
		global $wpdb;	
		
		$children = get_term_children( $item->term_id, 'usam-category' );
		$children[] = $item->term_id;				
		$cat_ids = implode( ',', $children);
		
		$query = "SELECT COUNT(DISTINCT p.ID) FROM ".$wpdb->posts." AS p INNER JOIN {$wpdb->prefix}term_relationships AS tr ON (p.ID = tr.object_id) INNER JOIN {$wpdb->prefix}term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) INNER JOIN ".$wpdb->postmeta." AS pm ON (p.ID = pm.post_id) WHERE p.post_parent = '0' AND tt.term_id IN ($cat_ids) AND p.post_type = 'usam-product' AND p.post_status = 'publish' AND pm.meta_key = '_usam_stock' AND pm.meta_value = '0'";
		echo $wpdb->get_var($query);
    }
	
	
	function column_publish( $item ) 
	{		
		global $wpdb;	
		
		$children = get_term_children( $item->term_id, 'usam-category' );	
		$children[] = $item->term_id;		
		$cat_ids = implode( ',', $children);
		
		$query = "SELECT COUNT(DISTINCT p.ID) FROM ".$wpdb->posts." AS p INNER JOIN {$wpdb->prefix}term_relationships AS tr ON (p.ID = tr.object_id) INNER JOIN {$wpdb->prefix}term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) WHERE p.post_parent = '0' AND tt.term_id IN ($cat_ids) AND p.post_type = 'usam-product' AND p.post_status = 'publish'";
		echo $wpdb->get_var($query);	
    }
	
	function column_draft( $item ) 
	{		
		global $wpdb;	
		$children = get_term_children( $item->term_id, 'usam-category' );	
		$children[] = $item->term_id;		
		$cat_ids = implode( ',', $children);
		
		$query = "SELECT COUNT(DISTINCT p.ID) FROM ".$wpdb->posts." AS p INNER JOIN {$wpdb->prefix}term_relationships AS tr ON (p.ID = tr.object_id) INNER JOIN {$wpdb->prefix}term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) WHERE p.post_parent = '0' AND tt.term_id IN ($cat_ids) AND p.post_type = 'usam-product' AND p.post_status = 'draft'";
		
		echo $wpdb->get_var($query);		
    }
	
	function column_purchase( $item ) 
	{		
		global $wpdb;	
		
    }
		   	 
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'   => array('name', false),
			'date'  => array('date', false),			
			);
		return $sortable;
	}
		
	function get_columns(){
        $columns = array(   
			'name'         => __( 'Название', 'usam' ),	
		//	'views'        => __( 'Просмотры', 'usam' ),			
		//	'purchase'     => __( 'Покупки', 'usam' ),	
			'product'      => __( 'Количество товаров', 'usam' ),	
			'publish'      => __( 'Опубликовано', 'usam' ),
			'draft'        => __( 'В черновиках', 'usam' ),
			'stock'        => __( 'Нет в наличии', 'usam' ),
        );
        return $columns;
    }
	
	public function prepare_items() 
	{		
		$args = array(
			'search' => $this->search,
			'page' => $this->get_pagenum(),
			'number' => $this->per_page,
			'orderby' => $this->orderby,
			'order' => $this->order,
		);		
		$this->callback_args = $args;
		$this->set_pagination_args( array(
			'total_items' => wp_count_terms( $this->screen->taxonomy, compact( 'search' ) ),
			'per_page' => $this->per_page,
		) );
	}
	
	public function display_table()
    {
		if ( $this->search )
			printf( '<h3 class="search_title">' . __( 'Результаты поиска &#8220;%s&#8221;' ) . '</h3>', esc_html( $this->search ) );
			
		$this->prepare_items();	
		?>				
		<div class='usam_tab_table'>		
			<form method='' action='' id='usam-tab_form'>
				<input type='hidden' value='<?php echo $this->page; ?>' name='page' />
				<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />			
				<div class='usam_list_table_wrapper'>
					<?php $this->display(); ?>
				</div>
			</form>
		</div>
		<?php
	}	
}