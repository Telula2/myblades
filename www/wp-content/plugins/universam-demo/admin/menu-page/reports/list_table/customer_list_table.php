<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_Customer extends USAM_Main_Report_List_Table
{	
    function __construct( $args = array() )
	{			
		$this->filters = array( 'interval_date' => '','date' => '' );		
		parent::__construct( $args );				
    }
	
	function no_items() 
	{
		_e( 'Клиенты не найдены', 'usam' );
	}	
		
	function column_total_orders( $item ) 
    {	
		echo usam_currency_display($item['total_orders'] ); 
    }  
	     
	function get_sortable_columns()
	{
		$sortable = array(
			'billingfirstname'    => array('billingfirstname', true),
			'billinglastname'     => array('billinglastname', false),
			'billingemail'        => array('billingemail', false),
			'total_orders'        => array('total_orders', false),
			'count_orders'        => array('count_orders', false),		
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(  
			'billingfirstname' => __( 'Имя', 'usam' ),
			'billinglastname'  => __( 'Фамилия', 'usam' ),	
			'billingemail'     => __( 'Электронная почта', 'usam' ),						
			'Last_Order'       => __( 'Последний заказ', 'usam' ),			
			'_order_total'     => __( 'Последняя сумма', 'usam' ),	
			'count_orders'     => __( 'Количество заказов', 'usam' ),	
			'total_orders'     => __( 'Всего куплено', 'usam' ),					
        );
        return $columns;
    }
	
	function prepare_items() 
	{			
		require_once( USAM_FILE_PATH . '/includes/customer/customer.class.php'    );
		
		$customer = new USAM_Customer();
		$this->items = $customer->get_data();
		$this->total_items = count( $this->items );
		
		$this->forming_tables();
	}
}
?>