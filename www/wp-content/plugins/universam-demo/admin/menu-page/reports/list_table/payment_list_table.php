<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_payment extends USAM_Main_Report_List_Table
{		
	private $status = 'all';
	public $groupby_date = 'month';
	private $total_amount = 0;
		
	public function __construct( $args = array() ) 
	{
		$this->filters = array( 'interval_date' => '','date' => 'month' );		
		parent::__construct( $args );
	}
	
	function no_items() 
	{
		_e( 'Оплаты не найдены', 'usam' );
	}	
		
	function get_columns()
	{
        $columns = array(  		
			'date'                      => __( 'Дата', 'usam' ),
			'total'                     => __( 'Количество', 'usam' ),	
			'cost'                      => __( 'Стоимость всех (Рубль)', 'usam' ),			
			'quantity_payment'          => __( 'Оплаченые(кол-во)', 'usam' ),
			'percent_payment'           => __( 'Оплаченые (%)', 'usam' ),
			'payment'                   => __( 'Оплаченые (Рубль)', 'usam' ),			
			'quantity_unpaid'           => __( 'Не оплаченные (кол-во)', 'usam' ),
			'cost_unpaid'               => __( 'Не оплаченные (Рубль)', 'usam' ),			
			'percent_unpaid'            => __( 'Не оплаченные (%)', 'usam' ),		
			'quantity_pending'          => __( 'В ожидании (кол-во)', 'usam' ),
			'cost_pending'              => __( 'В ожидании (Рубль)', 'usam' ),
			'percent_pending'           => __( 'В ожидании (%)', 'usam' ),
			'quantity_refunded'         => __( 'Платеж возвращен (кол-во)', 'usam' ),
			'cost_refunded'             => __( 'Платеж возвращен (Рубль)', 'usam' ),		
			'percent_refunded'          => __( 'Платеж возвращен (%)', 'usam' ),		
						
        );
        return $columns;
    }
	
	function prepare_items() 
	{			
		$start_date = strtotime($this->end_date_interval);
		$end_date = strtotime($this->start_date_interval);
		
		$query = array( 'groupby' => array( $this->groupby_date, 'status' ), 		
				'date_query' => array(
				array(
					'after'     => array( 'year'  => date('Y',$end_date), 'month' => date('m',$end_date), 'day' => date('d',$end_date) ),
					'before'    => array( 'year'  => date('Y',$start_date), 'month' => date('m',$start_date), 'day' => date('d',$start_date) ),
					'inclusive' => true,
				),
			),
			'fields' => array('date_time','count','total_sum', 'status'),	
			'order' => 'DESC', 
			'count_total' => true,			
		);		
		$payments = new USAM_Payments_Query( $query );	
		$result_payments = $payments->get_results();
		$this->total_items = $payments->get_total();

		$start_period = true;	
		$format_price = array(
						'display_currency_symbol' => false,
						'decimal_point'   		  => true,
						'display_currency_code'   => false,
						'display_as_html'         => false,
						'code'                 => false,
					);		
		$records = array();	
		$i = 0;			
	
		for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
		{	
			$i++;
			$records[$i]['date'] = $j;
			$records[$i]['total'] = 0;	
			$records[$i]['cost'] = 0;	
			$records[$i]['quantity_payment'] = 0; 
			$records[$i]['percent_payment'] = 0;		
			$records[$i]['payment'] = 0;
			$records[$i]['quantity_unpaid'] = 0;
			$records[$i]['cost_unpaid'] = 0;			
			$records[$i]['percent_unpaid'] = 0;
			$records[$i]['quantity_pending'] = 0;
			$records[$i]['cost_pending'] = 0;
			$records[$i]['percent_pending'] = 0;		
			$records[$i]['quantity_refunded'] = 0;	
			$records[$i]['cost_refunded'] = 0;	
			$records[$i]['percent_refunded'] = 0;		
				
			foreach ( $result_payments as $key => $item )
			{			
				if ( $j > strtotime($item->date_time) )
				{							
					break;					
				}
				else
				{	
					$records[$i]['total']+= $item->count;
					$records[$i]['cost'] += $item->sum;	
					switch ( $item->status ) 
					{					
						case 1: //Не оплачено
							$records[$i]['quantity_unpaid'] += $item->count;
							$records[$i]['cost_unpaid'] += $item->sum;
						break;			
						case 2: //Отклонено
						
						break;						
						case 3:	 //Оплачено
							$records[$i]['quantity_payment'] += $item->count;
							$records[$i]['payment'] += $item->sum;
						break;						
						case 4: //Платеж возвращен
							$records[$i]['quantity_refunded'] ++;
							$records[$i]['cost_refunded'] += $item->sum;
						break;
						case 5://Ошибка оплаты
							
						break;		
						case 6: //В ожидании 
							$records[$i]['quantity_pending'] += $item->count;
							$records[$i]['cost_pending'] += $item->sum;
						break;							
					}					
					unset($result_payments[$key]);							
				}
			}	
			if ( $records[$i]['total'] > 0 )
			{
				if ( $records[$i]['quantity_payment'] > 0 ) 		
					$records[$i]['percent_payment'] = round($records[$i]['quantity_payment'] / $records[$i]['total']*100, 1);					
				if ( $records[$i]['quantity_unpaid'] > 0 )
					$records[$i]['percent_unpaid'] = round($records[$i]['quantity_unpaid'] / $records[$i]['total'] *100, 1);
				if ( $records[$i]['quantity_refunded'] > 0 )
					$records[$i]['percent_refunded'] = round($records[$i]['quantity_refunded'] / $records[$i]['total'] *100, 1);
				if ( $records[$i]['quantity_pending'] > 0 )
					$records[$i]['percent_pending'] = round($records[$i]['quantity_pending'] / $records[$i]['total'] *100, 1);
			}				
			$j = strtotime("-1 ".$this->groupby_date, $j);		
		}				
		$this->_column_headers = $this->get_column_info();		
		$this->items = $records;
		
		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['total'], 'label' => __('Стоимость', 'usam').': '.$this->currency_display( $item['total'] ) ) );
		}		
		usam_set_data_graph( $data_graph, __('Стоимость всех оплат','usam') );
	}
}
?>