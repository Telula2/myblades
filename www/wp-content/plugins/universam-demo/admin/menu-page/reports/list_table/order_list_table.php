<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_Order extends USAM_Main_Report_List_Table
{	
	public  $orderby = 'date';	
	public  $order   = 'DESC';	
	
	public  $groupby_date = 'month';
	private $total_amount = 0;
	private $order_status = 'closed';	
	private $statuses;	
		
	public function __construct( $args = array() ) 
	{
		if ( isset( $_REQUEST['order_status'] ))
			$this->order_status = $_REQUEST['order_status'];
		
		$this->filters = array('paid' => '','weekday' => '','order_status' => $this->order_status, 'type_price' => '', 'coupon' => '', 'storage' => '','shipping' => '', 'payment_gateway' => '', 'type_price' => '', 'manager' => '', 'discount' => '', 'discount_product' => '', 'product_day' => '');	
		
		$this->statuses = usam_get_order_statuses( array( 'active' => 'all' ) );
		parent::__construct( $args );
	}
	
	function no_items() 
	{
		_e( 'Заказы не найдены', 'usam' );
	}	
	
	function get_columns()
	{
        $currency = usam_get_currency_sign();		
		$columns = array(  		
			'date'                      => __( 'Дата', 'usam' ),
			'quantity'                  => __( 'Количество', 'usam' ),
			'totalprice'                => sprintf(__( 'Стоимость(%s)', 'usam' ), $currency ),
			'item_count'                => __( 'Товаров', 'usam' ),			
			'quantity_payment'          => __( 'Кол-во оплаченных', 'usam' ),
			'percent_payment'           => __( 'Оплаченных (%)', 'usam' ),
			'payment'                   => sprintf(__( 'Оплаченных (%s)', 'usam'), $currency ),	
			'total_shipping'            => sprintf(__( 'Доставка (%s)', 'usam' ), $currency ),
			'total_taxes'               => sprintf( __( 'Налог (%s)', 'usam' ), $currency ),
			'average_check'             => __( 'Средний чек', 'usam' ),				
        );
				
		foreach ( $this->statuses as $status )
		{
			if ( $this->order_status == $status->internalname || $this->order_status == 'all' )
			{
				$key = $status->internalname.'_quantity';
				$columns[$key] = $status->short_name.' '.__( '(кол-во)', 'usam' );
				
				$key = $status->internalname."_currency";
				$columns[$key] = $status->short_name." ($currency)";
				
				$key = $status->internalname."_percent";
				$columns[$key] = $status->short_name." (%)";
			}
        }
		return $columns;
    }
	
	function prepare_items() 
	{	
		global $wpdb;
		
		$where = array( '1=1' );
		$join  = array();
			
		$selects = array( 'p.id, p.totalprice, p.shipping, p.total_tax, p.status, p.paid' );			
		if ( isset($_REQUEST['paid']) && $_REQUEST['paid'] == 'paid' )
			$date_colum = 'date_paid';		
		else
			$date_colum = 'date_insert';
			
		$where[] = "p.{$date_colum}<='".$this->end_date_interval."'";
		$where[] = "p.{$date_colum}>='".$this->start_date_interval."'";
		$selects[] = "p.{$date_colum} AS date";	
		$orderby = "p.{$date_colum}";
	
		if ( isset( $_REQUEST['code_price'] ) && $_REQUEST['code_price'] != '-1' )
		{
			$where[] = "p.type_price IN ('".implode( "','", $_REQUEST['code_price'] )."')";
		}				
		if ( !empty( $_REQUEST['storage'] ) && $_REQUEST['storage'] != 'all' )
		{
			$storage = array_map('intval', (array)$_REQUEST['storage']);
			$in = implode( ',',  $storage );		
			$where[] = "p.id IN (SELECT order_id FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE storage IN ($in))";
		}
		if ( !empty( $_REQUEST['shipping'] ) && $_REQUEST['shipping'] != 'all' )
		{
			$shipping = array_map('intval', (array)$_REQUEST['shipping']);
			$in = implode( ',',  $shipping );		
			$where[] = "p.id IN (SELECT order_id FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." WHERE method IN ($in))";
		}
		if ( !empty($_REQUEST['discount']) )
		{			
			$discount = (int)$_REQUEST['discount'];		
			$where[] = "p.id IN (SELECT order_id FROM ".USAM_TABLE_ORDER_DISCOUNT." WHERE discount_id IN ($discount))";
		}
		if ( !empty($_REQUEST['discount_product']) )
		{			
			$discount = (int)$_REQUEST['discount_product'];		
			$where[] = "p.id IN (SELECT order_id FROM ".USAM_TABLE_DISCOUNTS_PRODUCTS_ORDER." WHERE discount_id IN ($discount))";
		}
		if ( !empty($_REQUEST['product_day']) )
		{			
			$product_day = array_map('intval', (array)$_REQUEST['product_day']);	
			$in = implode( ',',  $product_day );					
			$where[] = "p.id IN (SELECT order_id FROM ".USAM_TABLE_PRODUCTS_ORDER." WHERE product_day IN ($in))";
		}
		if ( !empty( $_REQUEST['pgateway'] ) && $_REQUEST['pgateway'] != 'all' )
		{
			$pgateway = array_map('intval', (array)$_REQUEST['pgateway']);
			$in = implode( ',', $pgateway );	
			$where[] = "p.id IN (SELECT document_id FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE gateway_id IN ($in) AND document_type='order')";
		}		
		if ( ! empty( $_REQUEST['weekday'] ) )
		{
			$dayofweek = array();
			foreach ( $_REQUEST['weekday'] as $day )
			{
				$dayofweek[] = $wpdb->prepare("DAYOFWEEK({$date_colum}) = %d", sanitize_title($day) );
			}
			$where[] = implode( ' OR ', $dayofweek );		
		}	
		if ( usam_check_current_user_role('shop_crm') )
		{
			$where[] = "p.manager_id=$user_ID";	
		}	
		else
		{
			$view_group = usam_get_user_order_view_group( );
			if ( !empty($view_group) )
			{ 
				if ( !empty($view_group['type_prices']) )
					$where[] = "p.type_price IN ('".implode("','", $view_group['type_prices'])."')";
			}
		}
		$selects[] = ' IFNULL((SELECT COUNT(*) FROM '.USAM_TABLE_PRODUCTS_ORDER.' AS c WHERE c.order_id = p.id ),0) AS item_count';			
	
		$where = implode( ' AND ', $where );	
		$selects = implode( ', ', $selects );		
		$join = implode( ' ', $join );		
				
		$all_orders = $wpdb->get_results("SELECT $selects FROM ".USAM_TABLE_ORDERS." AS p $join WHERE $where ORDER BY $orderby DESC");	
		$records = array();	
		$i = 0;	
		for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
		{				
			$records[$i]['date'] = $j;
			$records[$i]['quantity'] = 0; //Общее количество
			$records[$i]['item_count'] = 0;
			$records[$i]['totalprice'] = 0;			
			$records[$i]['total_shipping'] = 0; //Стоимость доставки (Рубль)
			$records[$i]['total_taxes'] = 0;		
			$records[$i]['average_check'] = 0;	
			$records[$i]['quantity_payment'] = 0;
			$records[$i]['percent_payment'] = 0;
			$records[$i]['payment'] = 0;			
			foreach ( $this->statuses as $status )
			{ 				
				if ( $this->order_status == $status->internalname || $this->order_status == 'all' )
				{
					$colum_key = $status->internalname.'_currency';
					$records[$i][$colum_key] = 0;
					
					$colum_key = $status->internalname."_quantity";
					$records[$i][$colum_key] = 0;
					
					$colum_key = $status->internalname."_percent";
					$records[$i][$colum_key] = 0;					
				}
			}			
			foreach ( $all_orders as $key => $item )
			{		
				if ( $j > strtotime($item->date) )
				{						
					if ( $records[$i]['quantity'] > 0 && $records[$i]['quantity_payment'] > 0) 
						$records[$i]['percent_payment'] = round($records[$i]['quantity_payment'] / $records[$i]['quantity'] *100, 1);						
					break;					
				}
				else
				{	
					$records[$i]['quantity']++;					
					$records[$i]['totalprice'] += $item->totalprice;				
					$records[$i]['total_shipping'] += $item->shipping;
					$records[$i]['total_taxes'] += $item->total_tax;	
					
					if ( $item->paid == 2 )
					{
						$records[$i]['quantity_payment']++; //Кол-во оплаченных
						$records[$i]['payment'] += $item->totalprice;	
					}							
					$records[$i]['item_count'] += $item->item_count;		//Кол-во товаров		
					
					if ( $this->order_status == $item->status || $this->order_status == 'all' )
					{ 
						if ( $item->status != '' )
						{
							$colum_key = $item->status.'_currency';
							$records[$i][$colum_key] += $item->totalprice;					
							$colum_key = $item->status."_quantity";
							$records[$i][$colum_key]++;			
						}
					}					
					unset($all_orders[$key]);					
				}
			}						
			foreach ( $this->statuses as $status )
			{
				if ( ( $this->order_status == $status->internalname || $this->order_status == 'all') && $records[$i]['quantity'] > 0 && $records[$i][$status->internalname."_quantity"] > 0 )
				{					
					$colum_key = $status->internalname."_percent";					
					$records[$i][$colum_key] = round($records[$i][$status->internalname."_quantity"]/$records[$i]['quantity']*100, 1);	
				}
			}		
			if ( $records[$i]['quantity'] > 0 && $records[$i]['totalprice'] > 0) 
				$records[$i]['average_check'] = round($records[$i]['totalprice'] / $records[$i]['quantity'], 1);	
			
			if ( $records[$i]['quantity'] > 0 && $records[$i]['quantity_payment'] > 0) 
				$records[$i]['percent_payment'] = round($records[$i]['quantity_payment'] / $records[$i]['quantity'] *100, 1);		
			
			$j = strtotime("-1 ".$this->groupby_date, $j);	
			$i++;			
		}		
		$this->items = $records;	
		
		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['totalprice'], 'label' => __('Стоимость', 'usam').': '.$this->currency_display( $item['totalprice'] ) ) );			
		}		
		usam_set_data_graph( $data_graph, __('Стоимость всех заказов','usam') );
	}
}
?>