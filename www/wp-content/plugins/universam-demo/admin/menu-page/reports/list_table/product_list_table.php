<?php
include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
class USAM_List_Table_Product extends USAM_Main_Report_List_Table
{	
    public $groupby_date = 'month';
	function __construct( $args )
	{			
		$this->filters = array( 'interval_date' => '','date' => '','report_product' => '' );		
		parent::__construct( $args );				
    }
	
	public function filter_groups_by_report_product()
	{			
		$product = !empty($_REQUEST['product'])?$_REQUEST['product']:0;
		?>
		<td><label><?php _e( 'Выберите товар' ); ?>:</label></td>
		<td><?php
				$autocomplete = new USAM_Autocomplete_Forms( );
				$autocomplete->get_form_product( $product );
			?>	
		</td>
	<?php	
	}		
	
	public function column_date( $item ) 
	{	
		$timestamp = (int) $item->date;
		echo date( 'd.m.Y', $timestamp );			
	}
	
	function get_columns()
	{
        $columns = array(   			
			'date'     => __( 'Дата', 'usam' ),			
			'quantity' => __( 'Количество', 'usam' ),
			'total'    => __( 'Сумма', 'usam' ),	
		//	'views'    => __( 'Просмотры', 'usam' ),				
        );
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->_column_headers = $this->get_column_info();	

		$records = array();	
		if ( !empty($_REQUEST['product']) )
		{			
			$product_id = $_REQUEST['product'];
			$where = array( '1 = 1' );	
					
			$where[] = "pl.date_insert<='" . date("Y-m-d H:i:s",strtotime($this->end_date_interval)) ."'";
			$where[] = "pl.date_insert>='" . date("Y-m-d H:i:s", strtotime($this->start_date_interval) )."'";
		
			$where = implode( ' AND ', $where );		
					
			$sql = "SELECT pl.id, pl.date_insert AS date, cc.quantity, cc.price FROM ".USAM_TABLE_ORDERS." AS pl LEFT JOIN ".USAM_TABLE_PRODUCTS_ORDER." AS cc ON cc.order_id = pl.id  WHERE cc.product_id = '$product_id' AND $where ORDER BY pl.date_insert DESC";
			$data_report = $wpdb->get_results($sql);
	
		/*	$sql2 = "SELECT pl.id, pl.date, cc.quantity, cc.price FROM ".USAM_TABLE_ORDERS." AS pl LEFT JOIN ".USAM_TABLE_PRODUCTS_ORDER." AS cc ON cc.order_id = pl.id  WHERE cc.product_id = '$product_id' AND $where GROUP BY DATE(FROM_UNIXTIME(date)) ORDER BY pl.date DESC";
			$data_report1 = $wpdb->get_results($sql2);*/
		//	$sql = "SELECT UNIX_TIMESTAMP(date) AS date, views FROM ".USAM_TABLE_PRODUCT_VIEWS." WHERE id_product = '$product_id' ORDER BY date DESC ";	//GROUP BY DATE(FROM_UNIXTIME(date)) 
		//	$views = $wpdb->get_results($sql);					
					
			$views = array();
			$i = 0;					
			for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
			{			
				$records[$i] = new stdClass();		
				$records[$i]->date = $j;
				$records[$i]->quantity = 0;
				$records[$i]->total = 0;
				$records[$i]->views = 0;			
				foreach ( $data_report as $key => $item )
				{			
					if ( $j >= strtotime($item->date) )
					{	
						break;
					}
					else
					{	
						$records[$i]->quantity += $item->quantity;
						$records[$i]->total += $item->price*$item->quantity;					
						unset($data_report[$key]);					
					}
				}
				foreach ( $views as $key => $item )
				{			
			//		echo date("F j, Y, H:i a",$j),' ',$j,' >=',$item->date,'<br>';
					if ( $j >= $item->date )
					{	
						break;
					}
					else
					{						
						$records[$i]->views += $item->views;	
						unset($views[$key]);					
					}
				}			
				$i++;
				$j = strtotime("-1 ".$this->groupby_date, $j);		
			}
		}	
		$this->items = $records;
	
		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item->date ), 'x_data' => $item->total, 'label' => __('Количество', 'usam').': '.$this->currency_display( $item->total ) ) );
		}		
		usam_set_data_graph( $data_graph, __('Продажи','usam') );	
	}
}