<?php
class USAM_Tab_Products extends USAM_Tab
{	
	public function get_tables() 
	{ 
		$tables = array( 'products' => array( 'title' => __('Отчет по товарам','usam') ),  'product' => array( 'title' => __('Отчет по товару','usam') ) );		
		return $tables;
	}
	
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по проданным товарам', 'usam'),  'description' => 'Здесь Вы можете посмотреть какие товары были проданны за выбранный период.' );	
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}
}