<?php
class USAM_Tab_Coupon extends USAM_Tab
{
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по купонам', 'usam'), 'description' => 'Здесь вы можете посмотреть отчет по купонам.' );		
		$this->list_table();
	}	
}