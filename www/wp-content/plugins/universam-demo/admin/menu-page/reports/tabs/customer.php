<?php
class USAM_Tab_Customer extends USAM_Tab
{	
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по клиентам', 'usam'), 'description' => 'Здесь Вы можете сделать и посмотреть отчет по клиентам.' );		
		$this->list_table();
	}
}