<?php
class USAM_Tab_payment extends USAM_Tab
{	
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по документам оплаты', 'usam'), 'description' => 'Здесь вы можете посмотреть отчет по документам оплаты.' );		
		$this->list_table();
	}
}