<?php
class USAM_Tab_Reports extends USAM_Tab
{		
	protected function get_file_name_table( )
    {
		$this->table = $this->tab;
		$filename = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/list_table/'.$this->table.'_list_table.php';
		if ( !empty($_GET['table']) )
		{ 
			$this->table = sanitize_title($_GET['table']);
			$filename = USAM_FILE_PATH .'/admin/menu-page/reports/finished_reports/'.$this->table.'.php';	
		}			
		return $filename;
	}
	
	protected function load_tab()
	{			
		require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );	
		require_once( USAM_FILE_PATH . '/admin/menu-page/reports/includes/main_report_list_table.php' );
		$path_report = USAM_FILE_PATH .'/admin/menu-page/reports/finished_reports/'; 
		if ( isset($_REQUEST['table']) &&  file_exists( $path_report.$_REQUEST['table'].'.php' ) )
		{
			$file_data = get_file_data( $path_report.$_REQUEST['table'].'.php', array('ver'=>'Version', 'author'=>'Author', 'date'=>'Date', 'description'=>'Description', 'name'=>'Name' ) );
			$this->header = array( 'title' => $file_data['name'], 'description' => $file_data['description'] );
			$this->list_table();
		}
		else
		{		
			$this->header = array( 'title' => __('Готовые отчеты', 'usam'), 'description' => __('Здесь Вы можете посмотреть готовые отчеты.', 'usam') );
			$this->list_table();
		}
	}
	
	protected function callback_submit()
	{		
		
	}
	
	public function get_message()
	{		
		$message = '';		
		if( isset($_REQUEST['error']) && $_REQUEST['error'] == 'yandex' )
		{
			$message = __('Подключите Яндекс Метрика','usam');	
		}		
		return $message;
	} 	
}