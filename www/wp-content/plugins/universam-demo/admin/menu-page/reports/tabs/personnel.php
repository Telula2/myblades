<?php
class USAM_Tab_personnel extends USAM_Tab
{	
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по выполненой работе персоналом', 'usam'),  'description' => 'Здесь Вы можете посмотреть какую работу выполнил Ваш персанал за выбранный период.' );	
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}
}