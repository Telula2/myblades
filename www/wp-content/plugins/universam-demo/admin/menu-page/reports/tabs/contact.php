<?php
class USAM_Tab_contact extends USAM_Tab
{		
	protected function load_tab()
	{		
		$this->header = array( 'title' => __('Отчет по клиентам', 'usam'), 'description' => __('Здесь Вы можете посмотреть отчет по клиентам.', 'usam') );	
		$this->list_table();		
	}	
}