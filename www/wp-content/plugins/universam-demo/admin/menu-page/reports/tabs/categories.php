<?php
class USAM_Tab_Categories extends USAM_Tab
{
	protected function load_tab()
	{
		$this->header = array( 'title' => __('Отчет по категориям', 'usam'), 'description' => 'Здесь вы можете посмотреть отчет по категориям.' );
		$this->list_table();
	}		
}