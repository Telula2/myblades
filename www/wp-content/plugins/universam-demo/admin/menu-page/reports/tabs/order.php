<?php
class USAM_Tab_Order extends USAM_Tab
{		
	protected function load_tab()
	{		
		$this->header = array( 'title' => __('Отчет по заказам', 'usam'), 'description' => 'Здесь Вы можете посмотреть отчеты по заказам.' );	
		$this->list_table();		
	}	
}