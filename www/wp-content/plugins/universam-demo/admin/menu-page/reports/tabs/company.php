<?php
class USAM_Tab_company extends USAM_Tab
{		
	protected function load_tab()
	{		
		$this->header = array( 'title' => __('Отчет по компании', 'usam'), 'description' => __('Здесь Вы можете посмотреть отчет по компании.', 'usam') );	
		$this->list_table();		
	}	
}