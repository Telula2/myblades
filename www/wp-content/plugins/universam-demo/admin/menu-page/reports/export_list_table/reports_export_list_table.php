<?php
class USAM_Export_List_Table_reports extends USAM_Export_List_Table
{	
	public function __construct( $args ) 
	{				
		parent::__construct( $args );		
	}	

	public function column_date( $item ) 
	{	
		$timestamp = (int) $item['date'];
		return date( 'd.m.Y', $timestamp );			
	}
}