<?php
/*
 * Отображение страницы покупатели
 */ 
$default_tabs = array(
	array( 'id' => 'reports',  'title' => __( 'Готовые отчеты', 'usam' ) ),	
	array( 'id' => 'order',  'title' => __( 'Заказы', 'usam' ) ),	
	array( 'id' => 'payment',  'title' => __( 'Оплаты', 'usam' ) ),	
	array( 'id' => 'products',  'title' => __( 'Товары', 'usam' ) ), 
//	array( 'id' => 'customer',  'title' => __( 'Клиенты', 'usam' ) ),
	array( 'id' => 'coupon',  'title' => __( 'Купоны', 'usam' ) ),
	array( 'id' => 'categories',  'title' => __( 'Категории', 'usam' ) ), 
	array( 'id' => 'personnel',  'title' => __( 'Персонал', 'usam' ) ),
	array( 'id' => 'company',  'title' => __( 'Компании', 'usam' ) ),
	array( 'id' => 'contact',  'title' => __( 'Контакты', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{		
	public function __construct() 
	{		 
		parent::__construct( );
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_style' ) );		
	}	
	
	public function scripts_and_style( ) 
	{	
		wp_enqueue_script('d3');		
	}
	
	protected function print_file_table()
	{	
		$this->list_table->prepare_items();
		$columns_data = $this->list_table->items;	
		list( $columns, $hidden ) = $this->list_table->get_column_info();
		$displayed_columns = array_diff_key($columns, $hidden);
	
		$result_report = $this->list_table->result_report;		
		
		$title_tab = $this->get_title_tab( );	
				
		include( USAM_FILE_PATH . '/admin/menu-page/reports/includes/print-report.php' );	
		exit;
		$this->redirect = false;
	}		
} 
?>