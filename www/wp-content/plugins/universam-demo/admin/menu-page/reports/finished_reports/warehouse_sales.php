<?php
/*
Name:Продажи по складам
Description:Продажи по складам
Date:11.08.2015
Author:universam
Version:1.0
*/
class USAM_List_Table_warehouse_sales extends USAM_Main_Report_List_Table
{
	protected $orderby = 'date';
	private $storages;	
	
	public function __construct() 
	{
		$this->filters = array( 'interval_date' => '','date' => 'month', 'paid' => '','weekday' => '','type_price' => '','coupon' => '', 'storage' => '','shipping' => '', 'shipping_status' => 'shipped' );		
				
		$this->storages = usam_get_stores( array( 'active' => 'all' ) );
		parent::__construct( );
	}

	public function column_export_date( $item )
	{	
		$timestamp = (int) $item['date'];		
		return date( 'd.m.Y', $timestamp );
	}
	
    public function column_date( $item ) 
	{	
		$timestamp = (int) $item['date'];
		echo date( 'd.m.Y', $timestamp );			
	}

	function get_sortable_columns() 
	{
		$sortable = array(
			'name'   => array('name', false),
			'date'  => array('date', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{       
		$columns = array(   	
			'date'    => __( 'Дата', 'usam' ),			
        );				
		foreach ( $this->storages as $storage )		
			$columns[$storage->meta_key] = $storage->title;
		$columns['no_storage'] = __( 'Склад не указан', 'usam' );		
		$columns['del_storage'] = __( 'Склад не найден', 'usam' );
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->_column_headers = $this->get_column_info();
		$current_page = $this->get_pagenum();
		$offset = ( $current_page - 1 ) * $this->per_page;	
		
		$where = array( '1=1' );
		$join  = array();			
		
		if ( isset($_REQUEST['paid']) && $_REQUEST['paid'] == 'paid' )
			$data = 'p.date_paid';
		else
			$data = 'sd.date_insert';
		
		$where[] = "$data<='" .date("Y-m-d H:i:s", strtotime($this->end_date_interval))."'";
		$where[] = "$data>='" .date("Y-m-d H:i:s", strtotime($this->start_date_interval))."'";		
		
		if ( ! empty( $_REQUEST['storage'] ) && is_numeric($_REQUEST['storage']) )
		{			
			$storage = absint($_REQUEST['storage']);
			$where[] = "sd.storage='$storage'";
		}
		if ( ! empty( $_REQUEST['shipping'] ) && is_numeric($_REQUEST['storage']) )
		{
			$shipping = absint($_REQUEST['shipping']);
			$where[] = "sd.method='$shipping'";
		}	
		if ( ! empty( $_REQUEST['shipping_status'] ) && is_numeric($_REQUEST['shipping_status']) )
		{
			$status = absint($_REQUEST['shipping_status']);
			$where[] = "sd.status='".$status."'";
		}		
		else
			$where[] = "sd.status='shipped'";
		if ( !empty( $_REQUEST['code_price'] ) )
		{
			$code_prices = stripslashes_deep($_REQUEST['code_price']);
			$where[] = "p.type_price IN ('".implode("','",$ode_prices)."')";
		}			
		$join[] = " INNER JOIN ".USAM_TABLE_ORDERS." AS p ON (sd.order_id=p.id AND p.status='closed')";		
		
		$selects = array( 'sd.id, sd.order_id, sd.status, sd.storage, t_sp.totalprice' );
		
		$selects[] = "$data AS date";
		
	
	//	$join[] = "LEFT JOIN `".USAM_TABLE_SHIPPED_PRODUCTS."` AS sp ON (sd.id=sp.documents_id)
	//	LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc	ON ( cc.id=sp.basket_id)";	
			
			
		$join[] = "LEFT OUTER JOIN (SELECT sp.documents_id, SUM(cc.quantity*cc.price) AS totalprice 
			FROM `".USAM_TABLE_SHIPPED_PRODUCTS."` AS sp
			LEFT JOIN `".USAM_TABLE_PRODUCTS_ORDER."` AS cc	ON (cc.id=sp.basket_id) GROUP BY sp.documents_id) AS t_sp ON (t_sp.documents_id = sd.id)";
							
		$selects = implode( ', ', $selects );	
		$where = implode( ' AND ', $where );
		$join = implode( ' ', $join );				
		
		$documents_data = $wpdb->get_results("SELECT $selects FROM ".USAM_TABLE_SHIPPED_DOCUMENTS." AS sd $join WHERE $where GROUP BY sd.id ORDER BY sd.date_insert DESC");			
		
		$start_period = true;	
		$format_price = array(
						'display_currency_symbol' => false,
						'decimal_point'   		  => true,
						'display_currency_code'   => false,
						'display_as_html'         => false,
						'code'                 => false,
					);		
		$records = array();	
		$i = 0;		
		foreach ( $this->storages as $storage )	
		{
			$new_records[$storage->meta_key] = 0;		
			$_storage[$storage->id] = $storage->meta_key;		
		}			
		$new_records['no_storage'] = 0;		
		$new_records['del_storage'] = 0;		
		for ( $j = $this->time_calculation_interval_start; $j >= $this->time_calculation_interval_end; )
		{		
			$i++;				
			$records[$i] = $new_records;
			$records[$i]['date'] = $j;				
			foreach ( $documents_data as $key => $item )
			{					
				if ( $j > strtotime($item->date) )
				{					
					break;
				}
				else
				{					
					if ( empty($item->storage) )
						$records[$i]['no_storage'] += $item->totalprice;	
					elseif ( empty($_storage[$item->storage]) )		
						$records[$i]['del_storage'] += $item->totalprice;
					else						
						$records[$i][$_storage[$item->storage]] += $item->totalprice;					
					unset($documents_data[$key]);
				}				
			}		
			$j = strtotime("-1 ".$this->groupby_date, $j);			
		}			
		$this->total_items = $i;			

		if ( $this->per_page == 0 )
			$this->items = $records;
		else
			$this->items = array_slice( $records, $offset, $this->per_page);	
		
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page  ) );
	}
}