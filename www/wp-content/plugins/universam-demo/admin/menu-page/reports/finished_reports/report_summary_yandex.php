<?php
/*
Name:Отчет о конверсии и обороте
Description:Данные о посещениях беруться из Яндекс Метрики. Подключение к Яндекс Метрики должно быть настроено
Date:25.07.2014
Author:universam
Version:1.0
*/
class  USAM_List_Table_report_summary_yandex extends USAM_Main_Report_List_Table
{		
	function __construct( $args = array() )
	{	
		$this->filters = array( 'interval_date' => '','date' => 'month', 'weekday' => '','type_price' => '', );	
		
		parent::__construct( $args );
		
		require_once( USAM_FILE_PATH . '/includes/seo/yandex/metrika.class.php' );
	}		
		
	function get_columns()
	{
        $columns = array(   	
			'date'          => __( 'Дата', 'usam' ),									
			'visitors'      => __( 'Визиты', 'usam' ),	
			'views'         => __( 'Просмотры', 'usam' ),	
			'total_order'   => __( 'Количество заказов', 'usam' ),			
			'conversion'    => __( 'Конверсия', 'usam' ),	
			'item_count'    => __( 'Количество товаров', 'usam' ),
			'sum'          => __( 'Сумма заказов', 'usam' ),		
			'average_order' => __( 'Средний заказ', 'usam' ),					
        );
        return $columns;
    }
	
	function prepare_items() 
	{				
		$page = $this->get_pagenum();		
		$this->get_standart_query_parent( );	
		
		$start_date = strtotime($this->end_date_interval);
		$end_date = strtotime($this->start_date_interval);
		
		$start_date_day   = date('d',$start_date);	
		$start_date_month = date('m',$start_date);
		$start_date_year  = date('Y',$start_date);	
		
		$end_date_day   = date('d',$end_date);	
		$end_date_month = date('m',$end_date);
		$end_date_year  = date('Y',$end_date);		

		if ( !empty( $_REQUEST['weekday'] ) )			
			$weekday = sanitize_title($_REQUEST['weekday']);	
		else
			$weekday = array();
		
		$query = array( 'groupby' => array( $this->groupby_date ), 		
				'date_query' => array(
				array(
					'after'     => array( 'year'  => $end_date_year,	'month' => $end_date_month, 'day'   => $end_date_day ),
					'before'    => array( 'year'  => $start_date_year,	'month' => $start_date_month, 'day'   => $start_date_day ),
					'inclusive' => true,	
					'dayofweek' => $weekday,
					'compare'   => 'IN',					
				),				
			),
			'fields' => array('date_insert','count','sum','product_count','average_order'),
			'order' => 'DESC', 
			'status__in' => array('closed'), 		
		);			
		if ( !empty( $_REQUEST['code_price'] ) )
		{		
			$query['type_prices'] = sanitize_title($_REQUEST['code_price']);
		}	
		$all_orders = usam_get_orders( $query );	
		
		/*Получение данных из Яндекс Метрика*/				
		$metrika = new USAM_Yandex_Metrika();
		if ( !$metrika->auth() )
		{	
			$sendback = add_query_arg( array( 'error' => 'yandex' ) );
			$sendback = remove_query_arg( array('_wpnonce', '_wp_http_referer', 'action', 'action2', 'n' ),$sendback  );
			wp_redirect( $sendback );
			exit;
		}		
		$args = array( 
			'date1' => date('Y-m-d', $end_date), 
			'date2' => date('Y-m-d', $start_date), 
			'group' => $this->groupby_date, 	
			'limit' => 10000,					
		);		
		$result_yandex = $metrika->get_statistics( $args );
		$records = array();
		$format_price = array(
					'display_currency_symbol' => false,
					'decimal_point'   		  => true,
					'display_currency_code'   => false,
					'display_as_html'         => false,
					'code'                 => false,
				);	
				
		$format = array(
					'display_currency_symbol' => false,
					'decimal_point'   		  => false,
					'display_currency_code'   => false,
					'display_as_html'         => false,
					'code'                 => false,
				);		
		$j = $this->time_calculation_interval_start;	
		$ok = true;
		$i = 0; 
		while( $ok )
		{					
			$i++;
			$records[$i]['date'] = $j;			
			$records[$i]['total_order'] = 0;
			$records[$i]['sum'] = 0;				
			$records[$i]['average_order'] = 0;
			$records[$i]['visitors'] = 0;
			$records[$i]['views'] = 0;
			$records[$i]['conversion'] = 0;
			$records[$i]['item_count'] = 0; 
			$to = strtotime("-1 ".$this->groupby_date, $j);	
			foreach ( $result_yandex as $key => $item )
			{		
				if ( $j >= strtotime($item['dimensions'][0]['from']) && $to <= strtotime($item['dimensions'][0]['to']) )
				{	
					$records[$i]['visitors'] = $item['statistic']['visits'];
					$records[$i]['views'] = $item['statistic']['page_views'];
					unset($result_yandex[$key]);	
				}		
				else
					break;
			}			
			foreach ( $all_orders as $key => $item )
			{		
				if ( $j < strtotime($item->date_insert) )
				{
					$records[$i]['average_order'] = $item->average_order;				
					$records[$i]['sum'] = $item->sum;	
					$records[$i]['total_order'] = $item->count;
					$records[$i]['item_count'] = $item->product_count;		//Кол-во товаров	
				
					if ($records[$i]['visitors'] > 0 && $item->count > 0)
						$records[$i]['conversion'] = round($item->count/$records[$i]['visitors']*100,2);
					
					unset($all_orders[$key]);		
					break;
				}				
			}			
			$records[$i]['sum'] = usam_currency_display($records[$i]['sum'], $format_price);
			$records[$i]['average_order'] = usam_currency_display($records[$i]['average_order'], $format_price);		
			
			$records[$i]['visitors'] = $records[$i]['visitors'];
			$records[$i]['views'] = $records[$i]['views'];	
			
			$j = strtotime("-1 ".$this->groupby_date, $j);		
			if ( $end_date >= $j)
			{
				$j = $end_date;			
				$ok = false;
			}			
		}					
		$this->items = $records;
		$this->total_items = count($records);
		
		$data_graph = array();
		foreach ( $this->items as $item )
		{			
			array_unshift($data_graph, array( 'date' => date_i18n( "d.m.y", $item['date'] ), 'x_data' => $item['visitors'], 'label' => __('Визиты', 'usam').': '.$this->currency_display( $item['visitors'] ) ) );			
		}		
		usam_set_data_graph( $data_graph, __('Визиты','usam') );
	}
}