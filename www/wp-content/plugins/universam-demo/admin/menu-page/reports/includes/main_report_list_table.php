<?php
class USAM_Main_Report_List_Table extends USAM_List_Table
{
	protected $filters = array();
	protected $order = 'desc';
	
	protected $search_box = false;
	
	public function __construct( $args = array() ) 
	{
		$this->set_date_period();
		parent::__construct( $args );
	}
	
	protected function handle_row_actions( $item, $column_name, $primary ) {}
	
	public function return_post()
	{
		return array( 'groupby_date', 'groupby_customer', 'by_coupon', 'product', 'start_date', 'end_date', 'storage', 'storage_pickup', 'shipping', 'paid', 'n', 'm');
	}
	
	function currency_display( $price ) 
	{
		$format_price = array( 'display_currency_symbol' => false, 'decimal_point' => false, 'display_currency_code' => false, 'display_as_html' => false, 'code' => false );
		$result = explode('.', $price);		
		$format_price['decimal_point'] = isset($result[1]) ? true : false;		
		return usam_currency_display( $price, $format_price );		
	}
	
	function column_default( $item, $column_name ) 
	{		
		if ( is_object($item) )
			$data = $item->$column_name;
		else
			$data = $item[$column_name];		
		
		if ( is_numeric($data) )
			echo $this->currency_display( $data );
		else
			echo $data;			
	}
	
	public function column_date( $item ) 
	{		
		echo date_i18n('d.m.y', $item['date'] );
	}	
	
	public function extra_tablenav( $which ) 
	{				
		if ( 'top' == $which )
		{			
			?>			
			<div class = "report_page_toolbar">
				<?php $this->date_interval_toolbar(); ?>	
			</div>	
			<div class = "report_page_filter_toolbar">
				<div class = "filter_box postbox usam_box closed">	
					<div class="handlediv" title="<?php _e( 'Нажмите, чтобы переключить', 'usam' ) ?>"><br></div>
					<h3 class="hndle ui-sortable-handle"><span><?php _e('Фильтр', 'usam') ?></span></h3>
					<div class = "container inside">
						<div class = "container_bulkactions">			
							<table>							
								<?php
								foreach ( $this->filters as $filter => $value ) 
								{
									$method = "filter_groups_by_{$filter}";										
									if ( method_exists($this, $method) )
									{ ?><tr><?php $this->$method( $value ); ?></tr><?php }
								}
								?>					
							</table>								
						</div>
						<div class = "button_filter">
							<?php submit_button( __( 'Найти', 'usam' ), 'secondary', '', false, array( 'id' => 'filter-query-submit' ) );	?>		
							<a id="cancel" class="button control_button" href="<?php echo $this->url; ?>"><?php _e( 'Отменить', 'usam' ) ?></a>							
						</div>
					</div>	
				</div>						
			</div>	
			<div class = "_graph">
				<svg id ="graph" width="900" height="500"></svg>
			</div>			
			<?php
			echo '<div class="alignleft actions">';	
				$this->print_button();	
				$this->excel_button();				
			echo '</div>';				
		}
	}		
	
	protected function display_tablenav( $which ) 
	{
		?>
		<div class="tablenav <?php echo esc_attr( $which ); ?>">		
			<?php 
			$this->extra_tablenav( $which );
			$this->pagination( $which );
			?>
			<br class="clear" />
		</div>
	<?php
	}
	
	public function filter_groups_by_report_manager()
	{		
		$select = !empty( $_REQUEST['manager'])? $_REQUEST['manager']: '';
		?>		
		<td><label><?php _e( 'Выберите сотрудника', 'usam' ); ?>:</label></td>
		<td><select name="manager">
			<option value="0" <?php selected($select, 0); ?> ><?php _e('Нет','usam'); ?></option>
				<?php	
				$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator'), 'fields' => array( 'ID','display_name') );
				$users = get_users( $args );
				foreach( $users as $user )
				{						
					?>               
					<option value="<?php echo $user->ID; ?>" <?php echo ($select  == $user->ID) ?'selected="selected"':''?> ><?php echo $user->display_name; ?></option>
					<?php
				}
				?>
		</select></td>
		<?php	
	}		
	
	protected function filter_groups_by_type_price( $default = array() )
	{			
		$select = isset( $_REQUEST['code_price'] ) ? sanitize_title($_REQUEST['code_price']) : $default;			
		$type_prices = usam_get_prices( array('type' => 'R') );	
		$results = array();
		foreach ( $type_prices as $type_price )
		{
			$results[$type_price['code']] = $type_price['title'];
		}
		?>
		<td><label><?php _e( 'Цены', 'usam' ); ?>:</label></td>
		<td><?php echo $this->get_checklist( 'code_price', $results, $select ); ?></td>
		<?php	
	}
	
	protected function filter_groups_by_manager( $default = array() )
	{			
		$select = isset( $_REQUEST['groupby_manager'] ) ? $_REQUEST['groupby_manager'] : $default;	
		$results = array();
		$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator'), 'fields' => array( 'ID','user_nicename') );
		$users = get_users( $args );
		foreach( $users as $user )
		{				
			$results[$user->ID] = $user->user_nicename;;
		}
		?>
		<td><label><?php _e( 'Менеджеры', 'usam' ); ?>:</label></td>
		<td><?php echo $this->get_checklist( 'groupby_manager', $results, $select ); ?></td>
		<?php	
	}
	
	protected function filter_groups_by_discount( $default = array() )
	{			
		$select = isset( $_REQUEST['discount'] ) ? $_REQUEST['discount'] : $default;	
		?>  
		<td><label><?php _e( 'Выберите скидку на корзину', 'usam' ); ?>:</label></td>
		<td><select name="discount">
			<option value="0" <?php selected($select, 0); ?> ><?php _e('Нет','usam'); ?></option>
				<?php	
				$option = get_option('usam_rules_discounts_shopping_cart', array() );
				$rules = maybe_unserialize( $option );	
				foreach( $rules as $rule )
				{						
					?>               
					<option value="<?php echo $rule['id']; ?>" <?php echo ($select == $rule['id']) ?'selected="selected"':''?> ><?php echo $rule['title']; ?></option>
					<?php
				}
				?>
		</select></td>
		<?php	
	}
	
	protected function filter_groups_by_discount_product( $default = array() )
	{			
		$select = isset( $_REQUEST['discount_product'] ) ? $_REQUEST['discount_product'] : $default;	
		?>  
		<td><label><?php _e( 'Выберите скидку на товар', 'usam' ); ?>:</label></td>
		<td><select name="discount_product">
			<option value="0" <?php selected($select, 0); ?> ><?php _e('Нет','usam'); ?></option>
				<?php	
				$option = get_option('usam_product_discount_rules', array() );
				$rules = maybe_unserialize( $option );	
				foreach( $rules as $rule )
				{						
					?>               
					<option value="<?php echo $rule['id']; ?>" <?php echo ($select == $rule['id']) ?'selected="selected"':''?> ><?php echo $rule['name']; ?></option>
					<?php
				}
				?>
		</select></td>
		<?php	
	}
	
	protected function filter_groups_by_product_day( $default = array() )
	{			
		$select = isset( $_REQUEST['product_day'] ) ? $_REQUEST['product_day'] : $default;	
		
		$option = get_option('usam_product_day_rules', array() );
		$rules = maybe_unserialize( $option );			
				
		$results = array();
		if ( !empty($rules) )
		{
			foreach( $rules as $rule )		
			{			
				$results[$rule['id']] = $rule['name'];
			}
		}
		?>
		<td><label><?php _e( 'Товар дня', 'usam' ); ?>:</label></td>
		<td><?php echo $this->get_checklist( 'product_day', $results, $select ); ?></td>
		<?php	
	}
	
	protected function filter_groups_by_storage( $default = 'all' )
	{		
		$select = isset( $_REQUEST['storage'] ) ? sanitize_title($_REQUEST['storage']) : $default;			
		?>
		<td><label><?php _e( 'Cклад списания', 'usam' ); ?>:</label></td>
		<td><select name='storage'>
			<option value='all' <?php echo $select == 'all' ? 'selected="selected"':''; ?>><?php _e( 'Все склады', 'usam' ); ?></option>
			<?php
			$storages = usam_get_stores( array( 'active' => 'all' ) );				
			foreach ( $storages as $storage )
			{				
				?><option value='<?php echo $storage->id; ?>' <?php echo $select == $storage->id ? 'selected="selected"':''; ?>><?php echo $storage->title; ?></option><?php
			}
			?>	
		</select></td>
		<?php	
	}
	
	protected function filter_groups_by_order_status( $default = 'all' )
	{		
		$select = isset( $_REQUEST['order_status'] ) ? $_REQUEST['order_status'] : $default;	
		?>
		<td><label><?php _e( 'Статусы заказа' ); ?>:</label></td>
		<td><select name='order_status'>
			<option value='all' <?php selected( 'all', $select ); ?>><?php _e( 'Все статусы' ); ?></option>
			<?php			
			$statuses = usam_get_order_statuses( array( 'active' => 'all' ) );				
			foreach ( $statuses as $status )
			{				
				$style = $status->color != ''?'style="color:'.$status->color.'"':''
				?><option <?php echo $style; ?> value='<?php echo $status->internalname; ?>' <?php selected( $status->internalname, $select ); ?>><?php echo $status->name; ?></option><?php
			}
			?>	
		</select></td>
		<?php	
	}
	
	protected function filter_groups_by_shipping_status( $default = 'all' )
	{		
		$select = isset( $_REQUEST['shipping_status'] ) ? $_REQUEST['shipping_status'] : $default;
		$shipped_document_status = usam_get_shipped_document_status();
		?>
		<td><label><?php _e( 'Статусы доставки' ); ?>:</label></td>
		<td><select name="shipping_status">
			<option <?php selected( 0, $select ); ?> value="all"><?php _e( 'Все' ); ?></option>
			<?php												
			foreach ($shipped_document_status as $status)
			{					
			?>
				<option value="<?php echo $status['code']; ?>"  <?php echo ( $select == $status['code']?'selected="selected"':'') ?>><?php echo $status['name']; ?></option>
			<?php
			}
			?>				
		</select></td>	
		<?php
										
	}								
	
	protected function filter_groups_by_shipping( $default = 0 )
	{		
		$shipping = isset( $_REQUEST['shipping'] ) ? $_REQUEST['shipping'] : $default;
		$delivery_service = usam_get_delivery_services();	
		?>
		<td><label><?php _e( 'Способ доставки' ); ?>:</label></td>
		<td><select name="shipping">
			<option <?php selected( 0, $shipping ); ?> value="all"><?php _e( 'Все способы' ); ?></option>
			<?php
			foreach ( $delivery_service as $method ) 
			{					
				?>
				<option <?php selected( $method->id, $shipping ); ?> value="<?php echo $method->id; ?>"><?php echo $method->name; ?></option>
				<?php
			}		
			?>				
		</select></td>		
		<?php	
	}	
	
	protected function filter_groups_by_paid( $default = 'insert' )
	{		
		$paid = isset( $_REQUEST['paid'] ) ? $_REQUEST['paid'] : $default;		
		?>
		<td><label><?php _e( 'Дата группировки' ); ?>:</label></td>
		<td><select name="paid">
			<option <?php selected( 'insert', $paid ); ?> value="insert"><?php _e( 'Создание заказ' ); ?></option>
			<option <?php selected( 'paid', $paid ); ?> value="paid"><?php _e( 'Дата оплаты' ); ?></option>			
		</select></td>	
		<?php	
	}
	
	protected function filter_groups_by_weekday( $default = array() )
	{		
		$select_weekday = isset( $_REQUEST['weekday'] ) ? $_REQUEST['weekday'] : $default;		
		
		$weekday = array( '2' => __('Понедельник','usam'), '3' => __('Вторник','usam'), '4' => __('Среда','usam'), '5' => __('Четверг','usam'), '6' => __('Пятница','usam'), '7' => __('Суббота','usam'), '1' => __('Воскресение','usam') );
		?>
		<td><label><?php _e( 'Дни недели' ); ?>:</label></td>
		<td><?php echo $this->get_checklist( 'weekday', $weekday, $select_weekday ); ?></td>
		<?php
	}
	
	protected function filter_groups_by_payment_gateway( $default = 0 )
	{		
		$select = isset( $_REQUEST['pgateway'] ) ? $_REQUEST['pgateway'] : $default;
		$payment_gateways = usam_get_payment_gateways();	
		?>
		<td><label><?php _e( 'Способы оплаты' ); ?>:</label></td>
		<td><select name="pgateway">
			<option <?php selected( 0, $select ); ?> value="all"><?php _e( 'Все способы' ); ?></option>
			<?php
			foreach ( $payment_gateways as $method ) 
			{					
				?>
				<option <?php selected( $method->id, $select ); ?> value="<?php echo $method->id; ?>"><?php echo $method->name; ?></option>
				<?php
			}		
			?>				
		</select></td>		
		<?php	
	}
	
	protected function filter_groups_by_customer()
	{	
		$mail = '';
		$family  = '';			
		if (isset($_REQUEST['groupby_customer']))
		{				
			switch ( $_REQUEST['groupby_customer'] ) 
			{					
				case 'family':
					$family = 'selected=selected';
				break;				
				case 'mail':					
				default:
					$mail = 'selected=selected';
				break;
			}
		}
		else
		{
			$mail = 'selected=selected';
		}
		?>
		<td><label><?php _e( 'Группировать по' ); ?>:</label></td>
		<td><select name="groupby_customer">
			<option <?php echo $mail; ?> value='mail'><?php _e( 'По почте' ); ?></option>
			<option <?php echo $family; ?> value='family'><?php _e( 'По фамилии' ); ?></option>			
		</select></td>
		<?php	
	}	
	
	public function filter_groups_by_coupon() 
	{
		global $wpdb;
		
		$report_couponby_all = '';
		$report_couponby_yes = '';
		$report_couponby_no = '';			
		
		if (isset($_GET['by_coupon']))
		{				
			switch ( $_GET['by_coupon'] ) 
			{					
				case 'yes':
					$report_couponby_yes = 'selected=selected';
				break;
				case 'no':
					$report_couponby_no = 'selected=selected';
				break;
				case 'all':						
				default:
					$report_couponby_all = 'selected=selected';
				break;
			}
		}
		else
		{
			$report_couponby_all = 'selected=selected';
		}
		?>
		<td><label><?php _e( 'Купоны' ); ?>:</label></td>
		<td><select name="by_coupon">
			<option <?php echo $report_couponby_all; ?> value='all'><?php _e( 'Все' ); ?></option>
			<option <?php echo $report_couponby_yes; ?> value='yes'><?php _e( 'Использовали купон' ); ?></option>
			<option <?php echo $report_couponby_no;  ?> value='no'><?php _e( 'Не использовали купон' ); ?></option>
			<?php
			$coupons = $wpdb->get_results( "SELECT * FROM `" . USAM_TABLE_COUPON_CODES . "` ", ARRAY_A );					
			foreach ( $coupons as $coupon )
			{
				?>
				<option <?php if (isset($_GET['by_coupon']) && $_GET['by_coupon'] == $coupon['coupon_code'] ) { echo 'selected=selected'; } ?> value='<?php echo $coupon['coupon_code']; ?>'><?php echo 'Купон №',$coupon['coupon_code']; ?></option>
				<?php
			}
			?>
		</select></td>
		<?php
	}	
	
	
	protected function get_checklist ( $name, $array, $checked_list = NULL )
	{               
        $output = '<ul>';
		foreach ($array as $id => $title) 
		{       
            $output  .= '<li id='.$id.'>' ;
            $output  .= '<label class="selectit">' ;
            $output  .= '<input id="'.$id.'" ';
            $output  .= 'type="checkbox" name="'.$name.'[]" ';
            $output  .= 'value="'.$id.'" ';
            if ( $checked_list ) 
			{
                if (in_array($id, $checked_list))
                   $output  .= 'checked="checked"';                     
            }
            $output  .= '>';
            $output  .= '&nbsp;<span class="name">'.$title;
            $output  .= '</span></label>';            
            $output  .= '</li>';             
        }
		$output .= '</ul>';
		return $output;
    } 
}
?>