<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $title_tab['title']; ?></title>
	<style type="text/css">
		body {font-family:"Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;}
		h1 span {font-size:0.75em;}
		h2 {color: #333;}
		#wrapper {margin:0 auto; width:95%;}
		#header {	}
		#customer {	overflow:hidden;}
		#customer .shipping, #customer .billing {float: left; width: 50%;}
		table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	}
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px;}
		td {text-align:center;}
		#print-items td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc; }
		th.column-total { width:90px;}
		th.column-shipping { width:120px;}
		th.column-price { width:100px;}
		tfoot{background-color:#efefef;}
	</style>
</head>
<body onload="window.print()">
	<div id="wrapper">
		<div id="header">
			<h1>
				<?php echo get_bloginfo('name'); ?> <br />
				<span><?php echo $title_tab['title']; ?></span>
			</h1>
		</div>		
		<table id="product">
			<thead>
				<tr>
					<th>№</th>	
					<?php				
					foreach( $displayed_columns as $key => $column )
					{		
						?>
						<th><?php echo esc_html( $column ); ?></th>						
						<?php
					}					
					?>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 0;				
				foreach( $columns_data as $row )
				{
					$i++;
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<?php	
						foreach( $displayed_columns as $key => $title )
						{									
							if ( isset($row[$key]) )
							{
								if ( $key == 'date' )
								{						
									$value = date( 'd.m.Y', $row[$key] );
								}
								else
									$value = $row[$key];
								?>
								<td><?php echo $value; ?></td>								
								<?php
							}
						}
					?>
					</tr>
					<?php
				}				
				?>
			</tbody>
			<tfoot>	
				<tr>					
					<td></td>
					<?php					
					foreach( $displayed_columns as $key => $row )
					{					
						if ( isset($result_report[$key]) )						
							$value = $result_report[$key];
						else
							$value = '';
						?>															
						<td><?php echo $value; ?></td>	
						<?php	
					}
					?>	
				</tr>				
			</tfoot>
		</table>		
	</div>
</body>
</html>