<?php
/*
 * Отображение страницы SEO
 */  
$default_tabs = array(	
	array( 'id' => 'dashboard',  'title' => __( 'Консоль', 'usam' ), ),
	array( 'id' => 'positions',  'title' => __( 'Позиция сайта', 'usam' ) ),
	array( 'id' => 'sites',  'title' => __( 'Сайты', 'usam' ) ),
	array( 'id' => 'title',      'title' => __( 'Редактор товаров', 'usam' ) ),	
	array( 'id' => 'analysis',  'title' => __( 'Анализ контента', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{	
	protected function localize_script_tab()
	{
		return array(			
			'seo_title_product_save_nonce' => usam_create_ajax_nonce( 'seo_title_product_save' ),
		);
	}
}