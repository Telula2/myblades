<?php
class USAM_Positions_Table extends USAM_List_Table 
{	
	protected $search_engine='y';
	protected $region = 0;
	protected $site_id = 0;	
	
	protected function get_table_classes() {
		return array( 'widefat', 'striped', $this->_args['plural'] );
	}
	
	function column_keyword( $item ) 
    {			
		$actions['delete'] = $this->add_row_actions( $item['id'], 'delete', __( 'Удалить', 'usam' ) );
		static $i = 0;
		$i++;
		
		$keyword = $i.'. '.$item['keyword'];	
		$keyword .= "<input type='hidden' name='cb[]' value='".$item['id']."' />";		
		$this->row_actions_table( $keyword, $actions );	
	}
	
	function colorate( $int )
	{
		global $row_item;
		
		$color = 'position_red';
		if($int <= 10)
			$color = 'position_grin';
		if($int > 10  && $int <= 20)
			$color = 'position_yellow';
				
		$pointer = '';
		$go = '';
		if ( $row_item !== false ) 
		{
			if ( !is_numeric($int) )
			{
				if ( $row_item != $int )
					$pointer = "<span class = 'dashicons pointer_down'></span>";
			}
			elseif ( !is_numeric($row_item) )
			{
				
			}
			elseif ( $row_item > $int )
			{
				$pointer = "<span class = 'dashicons pointer_up'></span>";
				$g = $row_item-$int;
				$go = "<span class ='go_position'>(+$g)</span>";
			}
			elseif ( $row_item < $int )
			{
				$pointer = "<span class = 'dashicons pointer_down'></span>";
				$g = $row_item-$int;
				$go = "<span class ='go_position'>($g)</span>";
			}
		}		
		$row_item = $int;
		
		return $pointer.'<span class = "position_number '.$color.'">'.$int.'</span>'.$go.'<br />'; 
	}
	
	public function single_row( $item )
	{
		global $row_item;		
		$row_item = false;
		
		echo '<tr>';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'keyword'  => array('keyword', false),				
		);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(        	
			'keyword'      => __( 'Ключевые слова', 'usam' ),
        );		
		$start_date_interval = strtotime($this->start_date_interval);
		$end_date_interval = strtotime($this->end_date_interval);		
		$start = true;
		for ( $j = $start_date_interval; $j <= $end_date_interval; )
		{	
			if ( $this->groupby_date != 'day' )
			{
				$date_from = strtotime("+1 ".$this->groupby_date, $j);
				$date_from = mktime(0, 0, 0, date("m", $date_from) , date("d", $date_from), date("Y", $date_from));		
				if ( $start )
					$date_from = $this->data_interval_start( $date_from );	
				
				$column = date( 'd.m.y', $j ).' - '.date( 'd.m.y', $date_from-1 );					
			}
			else
				$column = date( 'd.m.y', $j );	
			
			$columns[$j] = $column;
			
			if ( $start )
				$j = $this->data_interval_start( $j );	
			
			$j = strtotime("+1 ".$this->groupby_date, $j);			
			$start = false;
		}			
		return $columns;
    }
	
	function data_interval_start( $date ) 
	{
		switch ( $this->groupby_date ) 
		{					
			case 'day':	
				$result = $date;				
			break;
			case 'week':	
				$w = date("w", $date);	
				switch ( $w ) 
				{					
					case 0:
						$result = $date - 6*86400;							
					break;	
					case 1:		
						$result = $date - 604800;										
					break;				
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
						$result = $date - ($w-1)*86400;												
					break;
				}	
			break;
			case 'month':						
				$result = mktime(0,0,0,date('m',$date),1, date('Y',$date));						
			break;
			case 'year':				
				$result = mktime(0,0,0,1,1, date('Y',$date));				
			break;				
		}
		return $result;
	}	
	
	public function regions_dropdown( ) 
	{	
		$location_ids = usam_get_search_engine_regions( array('fields' => 'location_id', 'search_engine' => $this->search_engine) );	
		if ( !empty($location_ids) )
		{
			$locations = usam_get_locations( array('fields' => 'id','include' => $location_ids, 'orderby' => 'include') );
			?>
			<select name="region">
				<?php 
				foreach ( $locations as $location_id ) { ?>
					<option <?php selected($this->region, $location_id); ?> value='<?php echo $location_id ?>'><?php echo usam_get_full_locations_name( $location_id ); ?></option>
				<?php 
				} 
				?>
			</select>
			<?php 
		} 
		else
			_e('Не указаны регионы','usam');
	}
	
	public function sites_dropdown(  ) 
	{	
		$sites = usam_get_sites( array('fields' => array('domain', 'id'),'type' => 'C', 'location_id' => $this->region, 
		'date_query_statistic' => array( 'after' => $this->start_date_interval, 'before' => $this->end_date_interval, 'inclusive' => true ),));
		?>
		<select name="site_id">
			<option <?php selected($this->site_id, 0); ?> value='0'><?php echo __('ВАШ САЙТ','usam').' - '.parse_url( get_site_url(), PHP_URL_HOST); ?></option>
			<?php 
			foreach ( $sites as $site ) { ?>
				<option <?php selected($this->site_id, $site->id); ?> value='<?php echo $site->id ?>'><?php echo $site->domain; ?></option>
			<?php 
			} 
			?>
		</select>
		<?php 
	}
	
	public function search_engine_dropdown( )
	{		
		?>
		<select name="se">
			<option <?php selected($this->search_engine, 'y'); ?> value='y'><?php _e( 'Яндекс', 'usam' ); ?></option>
			<option <?php selected($this->search_engine, 'g'); ?> value='g'><?php _e( 'Гугол', 'usam' ); ?></option>				
		</select>
		<?php
	}	
}