<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_positions extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		$title = __('Добавить ключевые слова','usam');		
		return $title;
	}

	protected function toolbar_buttons( ) 
	{	
	
	}
	
	function display_left()
	{			
		$this->data = array( 'keyword' => '' );			
		?>		
		<table class = "keyword">							
			<tr>				
				<td><textarea rows="30" cols="100" id="keywords" name="keywords"><?php echo $this->data['keyword']; ?></textarea></td>				
			</tr>	
		</table>			
		<?php			
	}	
}
?>