<?php
class USAM_Tab_analysis extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Анализ контента', 'usam'), 'description' => __('Для успешного продвижения сайта в поисковых системах необходимо иметь релевантный контент и оптимальную плотность ключевых слов. Данный сервис позволяет провести максимально полный анализ контента на Вашем сайте.

Будет определен вес главной страницы сайта, релевантность заголовка (title), процент релевантности ключевых слов (keywords) к тексту страницы. Так же будут проверены на релевантность слова, заключенные в тег H1. По завершению анализа будет составлена таблица, в которой будут представлены все слова, встречающиеся в тексте. Синим цветом будут выделены наиболее частотные слова и словоформы.','usam') );
	}
	
	protected function load_tab()
	{
		//$this->list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{
			case __( 'Сохранить','usam' ):			
				
			break;			
		}			
	}
	
	protected function get_message()
	{		
		$message = '';				
		if( isset($_REQUEST['update']) )
		{
			$message = sprintf( _n( '%s товар обновлен.', '%s товаров обновлено.', $_REQUEST['update'], 'usam' ), $_REQUEST['update'] );
		}			
		return $message;
	} 	
	
	public function display() 
	{		
		$url = !empty($_REQUEST['url'])?$_REQUEST['url']:'';
		?>
		<form method="post" class="">			
			<table>
				<tr>
					<td class="title"><label><b>Введите страницу для проверки</b></label></td>
					<td><input name="url" type="text" class="form-control" placeholder="<?php _e( 'Введите страничку для проверки…', 'usam' ); ?>" value="<?php echo $url; ?>"></td>
				</tr>
			</table>
			<?php submit_button( __( 'Проверить' ), 'primary', false, false, array( 'id' => "submit" ) ); ?>
		</form>
		<?php		
		if ( $url != '' )
		{
			require_once( USAM_FILE_PATH . '/includes/seo/seo-analysis.class.php'   );				
			$analysis = new USAM_SEO_Link_Analysis( $url );
			$result = $analysis->get_results();	
			if ( !$result )
				return false;
			?>
			<div class="table_tag">	
				<h2><?php _e( 'Тег - title', 'usam' ); ?></h2>
				<p><?php echo $result['tag']['seo']['title']; ?></p>
			</div>			
			<div class="table_tag">	
				<h2><?php _e( 'Заголовки', 'usam' ); ?></h2>
				<table class ="wp-list-table widefat fixed striped">
					<thead>
						<tr>					
							<td><?php _e( 'Заголовок', 'usam' ); ?></td>
							<td><?php _e( 'Тип', 'usam' ); ?></td>			
							<td><?php _e( 'Плотность', 'usam' ); ?></td>	
							<td><?php _e( 'Релевантность', 'usam' ); ?></td>							
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($result['tag']['header'] as $tag => $tags) 
					{						
						foreach ($tags as $title) 
						{	?>
							<tr>
								<td><?php echo $title ; ?></td>
								<td><?php echo $tag; ?></td>
								<td><?php // echo $keyword['weight']; ?></td>
								<td><?php //echo $keyword['occurrence']; ?></td>
							</tr>
						<?php
						}
					}	?>
					</tbody>
				</table>				
			</div>
			<div class="table_tag">	
				<h2><?php _e( 'Мета теги', 'usam' ); ?></h2>
				<table class ="wp-list-table widefat fixed striped">
					<thead>
						<tr>							
							<td><?php _e( 'Тип', 'usam' ); ?></td>			
							<td><?php _e( 'Значение', 'usam' ); ?></td>									
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($result['meta_tags'] as $tag => $title) 
					{						
						?>
						<tr>
							<td><?php echo $tag ; ?></td>
							<td><?php echo $title; ?></td>								
						</tr>
						<?php
					}	?>
					</tbody>
				</table>				
			</div>
			<div class="table_words">	
				<h2><?php _e( 'Контент сайт', 'usam' ); ?></h2>
				<table class ="wp-list-table widefat fixed striped">
					<thead>
						<tr>
							<td><?php _e( 'Слова', 'usam' ); ?></td>
							<td><?php _e( 'Теги', 'usam' ); ?></td>
							<td><?php _e( 'Вес', 'usam' ); ?></td>
							<td><?php _e( 'Вхождений', 'usam' ); ?></td>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($result['words'] as $key => $keyword) {	?>
						<tr>
							<td><?php echo $keyword['name']; ?></td>
							<td><?php echo implode(',',$keyword['tag']); ?></td>
							<td><?php echo $keyword['weight']; ?></td>
							<td><?php echo $keyword['occurrence']; ?></td>
						</tr>
					<?php }	?>
					</tbody>
				</table>			
			</div>
			<?php	
		}
	}	
}