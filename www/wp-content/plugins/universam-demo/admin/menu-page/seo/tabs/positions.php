<?php
class USAM_Tab_positions extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Позиция сайта', 'usam'), 'description' => __('Здесь Вы можете посмотреть позицию вашего сайта в поисковых системах.', 'usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
		require_once( USAM_FILE_PATH .'/includes/seo/sites_query.class.php' );			
		
		$this->buttons = array( 'add' => __('Добавить ключевые слова', 'usam') );		
	}
	
	public function get_tables() 
	{ 
		$tables = array( 'positions' => array( 'title' => __('Позиция сайта','usam') ),  'links' => array( 'title' => __('Ссылки сайта','usam') ),  'view' => array('title' => __('Конкуренты сайта','usam') ) );		
		return $tables;
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}	
	
	protected function callback_submit()
	{		 		
		switch( $this->current_action )
		{
			case 'start':
				usam_query_position_site();				
			break;			
			case 'delete':
				global $wpdb;
				$ids = array_map( 'intval', $this->records );					
				$in = implode( "','", $ids );
				
				$wpdb->query( "DELETE FROM ".USAM_TABLE_STATISTICS_KEYWORDS." WHERE keyword_id IN ('$in')" );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_KEYWORDS." WHERE  id IN ('$in')" );					
				
				$this->sendback = add_query_arg( array( 'deleted' => count( $ids ), ), $this->sendback );
				$this->redirect = true;	
			break;	
			case 'save':
				if ( !empty($_POST['keywords']) )
				{			
					$keywords = explode("\r\n", $_POST['keywords']);
					usam_insert_keywords( $keywords );
				}
			break;			
		}		
	}
}