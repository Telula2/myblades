<?php
class USAM_Tab_dashboard extends USAM_Tab
{
	private $webmaster;
	public function __construct()
	{
		if ( !empty($_GET['table']) )
		{
			switch ( $_GET['table'] )
			{
				case "popular":
					$this->header = array( 'title' => __('Популярные запросы', 'usam'), 'description' => __('Здесь вы можете посмотреть популярные запросы.','usam') );	
				break;    
				case "external":
					$this->header = array( 'title' => __('Внешнии ссылки на сайт', 'usam'), 'description' => __('Здесь вы можете посмотреть внешнии ссылки на сайт.','usam') );	
				break;
			}
		}
		else
			$this->header = array( 'title' => __('Сводная информация', 'usam'), 'description' => __('Здесь вы можете посмотреть сводную информацию о индексирование вашего магазина.','usam') );			
	}
	
	protected function load_tab()
	{	
		require_once( USAM_FILE_PATH . '/includes/seo/yandex/webmaster.class.php' );		
		if ( !empty($_GET['table']) )
		{
			$this->list_table();
		}
		else
		{
			$this->webmaster = new USAM_Yandex_Webmaster();
		}	
	}
	
	protected function callback_submit()
	{		
		
	}		
	
	public function tab_structure() 
	{		
		if ( !empty($_GET['table']) )
		{
			$this->list_table->display_table(); 
		}
		else
		{		
			if ( $this->webmaster->is_token() )
			{
				if ( $this->webmaster->ready() )
				{
					usam_add_box( 'usam_statistics', __( 'Сводная информация', 'usam' ), array( $this, 'statistics' ) );
					usam_add_box( 'usam_yandex_popular', __( 'Популярные запросы в Яндексе', 'usam' ), array( $this, 'yandex_popular' ) );	
					usam_add_box( 'usam_yandex_external', __( 'Внешние ссылки на сайт', 'usam' ), array( $this, 'yandex_external' ) );
					
					$this->webmaster_get_error();
				}
				else
				{
					printf( __('Для выполнения действий на Яндекс.Вебмастере вы должны выбрать сайт в <a href="%s">настройках магазина</a> в разделе Яндекс Вебмастер.', 'usam'), admin_url("admin.php?page=shop_settings&tab=search_engines") );
				}
			}
			else			
			{
				printf( __('Для выполнения действий на Яндекс.Вебмастере от имени определенного пользователя клиентское приложение должно быть зарегистрировано на сервисе Яндекс.OAuth и токен сохранен в <a href="%s">настройках магазина</a> в разделе Паспорт в Яндексе.', 'usam'), admin_url("admin.php?page=shop_settings&tab=search_engines") );
			}
		}
	}
	
	public function webmaster_get_error()
	{       
		$errors = $this->webmaster->get_errors();								
		foreach ( $errors as $error )
		{ 
			echo "<div class=\"error\"><p>{$error}</p></div>";									
		}
	}	

	
	public function statistics() 
	{	
		$statistics = $this->webmaster->get_statistics_site( );		
		if ( empty($statistics) )
		{			
			return '';
		}
		$fatal = !empty($statistics['site_problems']['FATAL'])?$statistics['site_problems']['FATAL']:0;
		$critical = !empty($statistics['site_problems']['CRITICAL'])?$statistics['site_problems']['CRITICAL']:0;
		$possible_problem = !empty($statistics['site_problems']['POSSIBLE_PROBLEM'])?$statistics['site_problems']['POSSIBLE_PROBLEM']:0;
			
		$out = "<div class='yandex'>";
		$out .= "<div class='usam_container-table-container caption border'>";
		$out .= "<div class='usam_container-table-caption-title'><a href='https://webmaster.yandex.ru/site/dashboard/' target='_blank'>".__( 'Яндекс Вебмастер', 'usam' )."</a></div>";
		$out .= "<p>".sprintf( __('Войти в свой кабинет <a href="%s" target="_blank">Яндекс Вебмастер</a>.', 'usam'), "https://webmaster.yandex.ru/site/dashboard/" )."</p>";		
		$out .= "<h4>".__('Проблемы сайта', 'usam')."</h4>";
		$out .= "<table class ='usam_table site_problems'>";
		$out .= "<tr><td class='name'>".__('Фатальные проблемы', 'usam')."</td><td>".$fatal."</td></tr>";
		$out .= "<tr><td class='name'>".__('Критичные проблемы', 'usam')."</td><td>".$critical."</td></tr>";
		$out .= "<tr><td class='name'>".__('Возможные проблемы', 'usam')."</td><td>".$possible_problem."</td></tr>";
		if ( !empty($statistics['site_problems']['RECOMMENDATION']) )
		{
			$out .= "<tr><td class='name'>".__('Рекомендация', 'usam')."</td><td><a href='https://webmaster.yandex.ru/site/http:radov39.ru:80/diagnosis/checklist/#recommendation' target='_blank'>".__('Смотреть рекомендации', 'usam')." (".$statistics['site_problems']['RECOMMENDATION'].")</a></td></tr>";							
		}
		$out .= "</table>";
		echo $out;
		
		$out = "<h4>".__('Обработка страниц', 'usam')."</h4>";
		$out .= "<table class ='usam_table processing_pages'>";
		$out .= "<tr><td class='name'>".__('Загруженные страницы', 'usam')."</td><td>".$statistics['downloaded_pages_count']."</td></tr>
				<tr><td class='name'>".__('Исключенные страницы', 'usam')."</td><td>".$statistics['excluded_pages_count']."</td></tr>
				<tr><td class='name'>".__('Страницы в поиске', 'usam')."</td><td>".$statistics['searchable_pages_count']."</td></tr>";						
		$out .= "</table>";
		echo $out;	
		
		$out = "<h4>".__('Индекс цитирования', 'usam')."</h4>";
		$out .= "<table class ='usam_table processing_pages'>";
		$out .= "<tr><td class='name'>".__('тИЦ', 'usam')."</td><td>".$statistics['tic']."</td></tr>";					
		$out .= "</table>";
		$out .= "</div>";
		$out .= "</div>";
		echo $out;	
		
		$out = "<div class='google'>";
		$out .= "<div class='usam_container-table-container caption border'>";		
		$out .= "<div class='usam_container-table-caption-title'><a href='https://www.google.com/webmasters/tools/home/' target='_blank'>".__( 'Google Вебмастер', 'usam' )."</a></div>";		
		$out .= "<p>".sprintf( __('Войти в свой кабинет <a href="%s" target="_blank">Google Вебмастер</a>.', 'usam'), "https://www.google.com/webmasters/tools/home" )."</p>";
		$out .= "</div>";
		$out .= "</div>";
		echo $out;	
	}

	public function yandex_popular() 
	{				
		$count = 10;
		$popular = $this->webmaster->get_popular( 'TOTAL_SHOWS', array( 'TOTAL_SHOWS','TOTAL_CLICKS','AVG_SHOW_POSITION','AVG_CLICK_POSITION') );
		if ( empty($popular) )
		{			
			return '';
		}
		$out = "<table class ='usam_list_table'>";
		$out .= "<thead>";
		$out .= "<tr>
				<td>".__('Популярные запросы', 'usam')."</td>
				<td>".__('Показы', 'usam')."</td>
				<td>".__('Клики', 'usam')."</td>
				<td>".__('Средняя позиция', 'usam')."</td>
				<td>".__('Средняя позиция клика', 'usam')."</td>
				</tr>
				</thead><tbody>";			
				$i = 0;
				foreach ( $popular as $value )
				{							
					$out .= "<tr><td>".$value['query_text']."</td><td>".$value['indicators']['TOTAL_SHOWS']."</td><td>".$value['indicators']['TOTAL_CLICKS']."</td><td>".round($value['indicators']['AVG_SHOW_POSITION'],1)."</td><td>".round($value['indicators']['AVG_CLICK_POSITION'],1)."</td></tr>";
					$i++;
					if ( $i >= $count )
						break;
				}	 				
			$out .= "</tbody></table>";
		echo $out;
		?>
		<p><?php _e( 'Посмотреть', 'usam' ); ?> <a href="<?php echo admin_url("admin.php?page=seo&tab=dashboard&table=popular"); ?>" class=""><?php _e( '500 популярных запросов', 'usam'); ?></a></p>
		<?php
	}
	
	public function yandex_external() 
	{			
		$external = $this->webmaster->get_external( );
		if ( empty($external['links']) )
		{			
			return '';
		}		
		$out = "<table class ='usam_list_table'>";
		$out .= "<thead>";
		$out .= "<tr>
				<td>".__('Откуда', 'usam')."</td>
				<td>".__('Куда', 'usam')."</td>
				<td>".__('Дата обнаружения', 'usam')."</td>
				</tr>
				</thead><tbody>";			
				foreach ( $external['links'] as $value )
				{							
					$out .= "<tr><td>".$value['source_url']."</td><td>".$value['destination_url']."</td><td>".$value['discovery_date']."</td></tr>";					
				}	 				
			$out .= "</tbody></table>";
		echo $out;
		?>
		<p><a href="<?php echo admin_url("admin.php?page=seo&tab=dashboard&table=external"); ?>" class=""><?php printf( __( 'Посмотреть все %s внешних ссылок', 'usam' ), $external['count']); ?></a></p>
		<?php
	}	
}