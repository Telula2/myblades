<?php
class USAM_Tab_title extends USAM_Tab
{
	protected $form_method = "POST";
	
	public function __construct()
	{
		$this->header = array( 'title' => __('Массовый редактор товаров', 'usam'), 'description' => 'Здесь Вы можете массово отредактировать названия и описания товаров.' );
	}
	
	protected function load_tab()
	{
		$this->product_list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{
			case __( 'Сохранить','usam' ):			
				if ( !empty($_POST['post_title']) )
				{
					$i = 0;								
					foreach($_POST['post_title'] as $id => $post_title)
					{								
						$product_data = array();
						$product_data['ID'] = (int)$id;
						$product_data['post_title'] = $post_title;
						if ( isset($_POST['p_excerpt'][$id]) )
							$product_data['post_excerpt'] = $_POST['p_excerpt'][$id];									
						wp_update_post( $product_data );
						$i++;
					}		
					$this->sendback = add_query_arg( array( 'update' => $i ), $this->sendback );		
					$this->redirect = true;
				}
			break;			
		}			
	}
	
	protected function get_message()
	{		
		$message = '';				
		if( isset($_REQUEST['update']) )
		{
			$message = sprintf( _n( '%s товар обновлен.', '%s товаров обновлено.', $_REQUEST['update'], 'usam' ), $_REQUEST['update'] );
		}			
		return $message;
	} 
	
	
	public function tab_structure() 
	{
		usam_add_box( 'usam_box_replace', __('Глобальная замена','usam'), array( $this, 'box_replace' ) );	
		$this->list_table->display_table();
	}	
	
	public function box_replace() 
	{
		?>
		<p><?php _e('Если Вы хотите изменить одинаковое название или описание в нескольких товаров, то можете воспользоваться этой формой. Не забудте нажать кнопку сохранить, или изменения не сохраняться.', 'usam') ?></p>
		<table>			
			<tr>							
				<td><?php _e('Где заменить?', 'usam') ?></td>
				<td>								
					<select name="where_replace" id="where_replace" >
						<option value="title"><?php _e('В названии','usam'); ?></option>
						<option value="charact"><?php _e('В характеристиках','usam'); ?></option>
						<option value="desc"><?php _e('В описании','usam'); ?></option>
					</select>
				</td>							
			</tr>
			<tr>							
				<td><?php _e('Что заменить', 'usam') ?></td>
				<td><input type="text" id = "what_replaced" name="what_replaced" value="" /></td>							
			</tr>	
			<tr>							
				<td><?php _e('Чем заменить', 'usam') ?></td>
				<td><input type="text" id = "how_to_replace" name="how_to_replace" value="" /></td>
			</tr>							
			<tr>							
				<td colspan='2'><input type="button" id="button_replaced" class="secondary" value="<?php _e("Заменить", 'usam'); ?>"/></td>
			</tr>						
		</table>					
		<?php 		
	}	
}