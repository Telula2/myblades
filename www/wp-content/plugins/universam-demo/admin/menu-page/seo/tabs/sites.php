<?php
class USAM_Tab_sites extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Позиция сайта', 'usam'), 'description' => __('Список сайтов ваших конкурентов. Они будут добавляться автоматически.', 'usam') );		
		
		require_once( USAM_FILE_PATH .'/includes/seo/sites_query.class.php' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		 		
		switch( $this->current_action )
		{		
			case 'delete':
				global $wpdb;
				$ids = array_map( 'intval', $this->records );					
				$in = implode( "','", $ids );
							
				$wpdb->query( "DELETE FROM ".USAM_TABLE_SITES." WHERE  id IN ('$in')" );					
				
				$this->sendback = add_query_arg( array( 'deleted' => count( $ids ), ), $this->sendback );
				$this->redirect = true;	
			break;			
		}		
	}
}