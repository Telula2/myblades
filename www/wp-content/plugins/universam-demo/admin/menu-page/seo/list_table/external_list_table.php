<?php
class USAM_List_Table_external extends USAM_List_Table
{	
	function get_columns()
	{
        $columns = array(   			
			'source_url'      => __( 'Откуда', 'usam' ),	
			'destination_url' => __( 'Куда', 'usam' ),	
			'discovery_date'  => __( 'Дата обнаружения', 'usam' ),	
        );
        return $columns;
    }
	
	function prepare_items() 
	{		
		$offset =($this->get_pagenum() - 1)*$this->per_page;
		
		$webmaster = new USAM_Yandex_Webmaster();		
		$external = $webmaster->get_external( $offset, $this->per_page );
		
		$this->items = $external['links'];
		$this->total_items = $external['count'];				
		
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}