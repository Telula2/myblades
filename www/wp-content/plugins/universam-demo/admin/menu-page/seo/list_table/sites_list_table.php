<?php
class USAM_List_Table_sites extends USAM_List_Table 
{		
	private $keywords;
	private $statistics_keywords = array();
	function __construct( $args = array() )
	{	
		parent::__construct( $args );	
		
		global $wpdb;
		$sql_query = "SELECT * FROM ".USAM_TABLE_KEYWORDS."";		
		$this->keywords = $wpdb->get_results($sql_query);
    }
		
	// массовые действия 
	public function get_bulk_actions() 
	{
		if ( ! $this->bulk_actions )
			return array();

		$actions = array(
			'delete' => _x( 'Удалить', 'bulk action', 'usam' ),	
		);	
		return $actions;
	}	
	
	function column_domain( $item ) 
    {	
		$actions = $this->standart_row_actions( $item->id );
		$actions['statistics'] = '<a class="usam-statistics" href="'.add_query_arg( array('page' => 'seo','tab' => 'site_positions', 'site_id' => $item->id), admin_url('admin.php')).'">'. __( 'Статистика', 'usam' ).'</a>';	
		$name = "<a href='http://$item->domain' target='_blank'>$item->domain</a>";
		$this->row_actions_table( $name, $actions );	
	}
	
	function column_type( $item ) 
    {	
		switch ( $item->type ) 
		{
			case 'C' :
				_e( 'Конкуренты', 'usam' );
			break;
			case '' :
	
			break;			
		}
	}
	
	function column_keyword( $item ) 
    {	
		if ( isset($this->statistics_keywords[$item->id]) )
			echo count($this->statistics_keywords[$item->id]);
	}
	
	function column_rating( $item ) 
    {	
		if ( isset($this->statistics_keywords[$item->id]) )
			echo round(count($this->statistics_keywords[$item->id])*100/count($this->keywords),2);
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'domain'   => array('domain', false),
			'type'     => array('type', false),
			'status'   => array('status', false),	
			'date'     => array('date', false),			
			);
		return $sortable;
	}
       
	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',				
			'domain'      => __( 'Сайт', 'usam' ),
			'description' => __( 'Описание', 'usam' ),		
			'keyword'     => __( 'Позиция', 'usam' ),				
			'rating'      => __( 'Рейтинг', 'usam' ),
			'type'        => __( 'Тип', 'usam' ),	
			'date'        => __( 'Дата', 'usam' ),				
        );
        return $columns;
    }	
	
	public function prepare_items() 
	{
		global $wpdb, $user_ID;	
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby );		
		if ( $this->search != ''  )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );					
			if ( !empty( $_REQUEST['type'] ) )
				$args['type'] = $_REQUEST['type'];
		}		
		$query_orders = new USAM_Sites_Query( $args );
		$this->items = $query_orders->get_results();
		if ( $this->per_page )
		{
			$site_ids = array();
			foreach($this->items as $item )
			{			
				$site_ids[] = $item->id;
			}
			if ( !empty($site_ids) )
			{				
				$sql_query = "SELECT * FROM ".USAM_TABLE_STATISTICS_KEYWORDS." WHERE site_id IN (".implode(',',$site_ids).")";		
				$statistics_keywords = $wpdb->get_results($sql_query);	
				foreach($statistics_keywords as $keyword )
				{
					$this->statistics_keywords[$keyword->site_id][] = $keyword;
				}
			}
			$this->total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}		
	}
	
	//Фильтр по доставке
	public function method_dropdown() 
	{
		$type = isset( $_REQUEST['type'] ) ? absint($_REQUEST['type']) : 0;			
		$delivery_service = usam_get_delivery_services();
		?>
		<select name="type">
			<option <?php selected( 0, $method ); ?> value="0"><?php _e( 'Способы доставки' ); ?></option>
			<option <?php selected( 0, $method ); ?> value="0"><?php _e( 'Способы доставки' ); ?></option>					
		</select>		
		<?php
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'type9' => array() );		
	}
}