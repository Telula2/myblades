<?php
class USAM_List_Table_popular extends USAM_List_Table
{	
	protected $orderby = 'show';
	protected $order   = 'desc';		
	
	function column_query_text( $item )
	{	
		echo $item['query_text'];
		//$this->row_actions_table( $item['query_text'], $this->standart_row_actions( $item['id'] ) );
	}
			
	function get_sortable_columns() 
	{
		$sortable = array(
			'query_text' => array('query_text', false),
			'show'       => array('show', false),
			'click'      => array('click', false),
			'avg_show'   => array('avg_show', false),
			'avg_click'   => array('avg_click', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   			
			'query_text'=> __( 'Популярные запросы', 'usam' ),	
			'show'      => __( 'Показы', 'usam' ),	
			'click'     => __( 'Клики', 'usam' ),				
			'avg_show'  => __( 'Средняя позиция', 'usam' ),
			'avg_click' => __( 'Средняя позиция клика', 'usam' ),
        );
        return $columns;
    }

	public function get_number_columns_sql()
    {       
		return array('show', 'click', 'avg_show', 'avg_click');
    }
	
	function prepare_items() 
	{		
		$webmaster = new USAM_Yandex_Webmaster();		
		$popular = $webmaster->get_popular( 'TOTAL_SHOWS', array( 'TOTAL_SHOWS','TOTAL_CLICKS','AVG_SHOW_POSITION','AVG_CLICK_POSITION') );
				
		$items = array();
		foreach ( $popular as $value )
		{
			$items[] = array( 'id' => $value['query_id'], 'query_text' => $value['query_text'], 'show' => $value['indicators']['TOTAL_SHOWS'], 'click' => $value['indicators']['TOTAL_CLICKS'], 'avg_show' => round($value['indicators']['AVG_SHOW_POSITION'],2), 'avg_click' => round($value['indicators']['AVG_CLICK_POSITION'],2) );
		}	
		unset($popular);
		$search_terms = empty( $this->search ) ? array() : explode( ' ', $this->search );			
		if ( !empty($search_terms) )
		{
			foreach ( $items as $item )
			{
				foreach ( $search_terms as $value )
				{
					if ( stripos($item['query_text'], $value) !== false)
					{
						$this->items[] = $item;
					}
				}
			}
		}
		else
			$this->items = $items;
		
		$this->total_items = count($this->items);			
		$this->forming_tables();
	}
}