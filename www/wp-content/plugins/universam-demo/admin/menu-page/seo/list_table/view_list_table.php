<?php
class USAM_List_Table_view extends USAM_List_Table
{	
	public $orderby = 'ID';
	public $order   = 'desc';

	protected $search_engine='y';
	protected $region = 0;
	protected $keyword = '';	
	protected $search_box = false;
	protected $host = '';
	
	protected $groupby_date = 'day';	
	protected $period = 'month';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
				
		$this->host = parse_url( get_site_url(), PHP_URL_HOST);	
		$this->set_date_period();	
		if ( !empty($_REQUEST['se']) )
		{			
			$this->search_engine = $_REQUEST['se'];			
		}	
		if ( !empty($_REQUEST['keyword']) )
		{			
			$this->keyword = absint($_REQUEST['keyword']);	
		}			
		else
		{
			global $wpdb;
			$this->keyword = $wpdb->get_var("SELECT id FROM ".USAM_TABLE_KEYWORDS." LIMIT 1");;	
		}		
		if ( !empty($_REQUEST['region']) )
			$this->region = $_REQUEST['region'];
		else
		{
			$location_ids = usam_get_search_engine_regions( array('fields' => 'location_id', 'search_engine' => $this->search_engine, 'number' => 1) );	
			$this->region = $location_ids[0];
		}
    }	
	
	public function extra_tablenav_display( $which ) 
	{		
		if ( 'top' == $which )
		{
			echo '<div class="alignleft actions">';	
			$url = $this->get_nonce_url( add_query_arg( array('action' => 'start'), $_SERVER['REQUEST_URI'] ) );
			?>		
			<a href="<?php echo $url; ?>" class = "button button-primary"><?php _e( 'Проверить сейчас', 'usam' ); ?></a>
			<?php 									
			echo '</div>';		
			echo '<div class="alignleft actions">';					
				$this->standart_button();									
			echo '</div>';						
		}
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'interval' => array(), 'search_engine' => array(), 'regions' => array(), 'keywords' => array() );		
	}	
	
	public function regions_dropdown( ) 
	{	
		$location_ids = usam_get_search_engine_regions( array('fields' => 'location_id', 'search_engine' => $this->search_engine) );	
		if ( !empty($location_ids) )
		{
			$locations = usam_get_locations( array('fields' => 'id','include' => $location_ids) );
			?>
			<select name="region">
				<?php 
				foreach ( $locations as $location_id ) { ?>
					<option <?php selected($this->region, $location_id); ?> value='<?php echo $location_id ?>'><?php echo usam_get_full_locations_name( $location_id ); ?></option>
				<?php 
				} 
				?>
			</select>
			<?php 
		} 
		else
			_e('Не указаны регионы','usam');
	}
	
	public function search_engine_dropdown( )
	{		
		?>
		<select name="se">
			<option <?php selected($this->search_engine, 'y'); ?> value='y'><?php _e( 'Яндекс', 'usam' ); ?></option>
			<option <?php selected($this->search_engine, 'g'); ?> value='g'><?php _e( 'Гугол', 'usam' ); ?></option>				
		</select>
		<?php
	}
	
	public function keywords_dropdown( )
	{		
		global $wpdb;
		$keywords = $wpdb->get_results("SELECT * FROM ".USAM_TABLE_KEYWORDS."");
		?>
		<select name="keyword">
			<?php
			foreach ( $keywords as $keyword ) 
			{
			?>
			<option <?php selected($this->keyword, $keyword->id); ?> value='<?php echo $keyword->id; ?>'><?php echo $keyword->keyword; ?></option>
			<?php
			}
			?>
		</select>
		<?php
	}

	protected function get_table_classes() {
		return array( 'widefat', 'striped', $this->_args['plural'] );
	}
	
	function column_number( $item ) 
    {			
		echo $item['number'];
	}
		
	public function column_default( $item, $column_name ) 
	{
		if ( is_numeric($column_name) && isset($item[$column_name]) )
		{							
			$class = stripos($item[$column_name], $this->host) !== false ? "class='active'":"";				
			$domain = parse_url( $item[$column_name], PHP_URL_HOST);	
			$domain = str_replace("www.","",$domain);	
			echo "<span class='domain'>$domain</span><br><a href='".$item[$column_name]."' $class target='_blank'>".$item[$column_name]."</a>";
		}		
	}
	
	public function single_row( $item )
	{
		global $row_item;		
		$row_item = false;
		
		echo '<tr>';
		$this->single_row_columns( $item );
		echo '</tr>';
	}	
			
	function get_columns()
	{
        $columns = array(        	
			'number'      => __( 'Номер', 'usam' ),
        );		
		for ( $j = $this->time_calculation_interval_end; $j <= $this->time_calculation_interval_start; )
		{	
			$columns[$j] = date( 'd.m.Y', $j );
			$j = strtotime("+1 day", $j);		
		}				
	return $columns;
    }	

	function prepare_items() 
	{	
		global $wpdb;
				
		$from = date( 'Y-m-d', $this->time_calculation_interval_end );
		$to = date( 'Y-m-d', $this->time_calculation_interval_start );

		$sql_query = "SELECT * FROM ".USAM_TABLE_STATISTICS_KEYWORDS." WHERE search_engine='$this->search_engine' AND keyword_id='$this->keyword' AND location_id='$this->region' AND date_insert>='$from' AND date_insert<='$to' AND number<='10'ORDER BY number, date_insert ASC";
		$statistics_keywords = $wpdb->get_results( $sql_query );	
		if ( !empty($statistics_keywords) )
		{
			$i = 1;			
			foreach ( $statistics_keywords as $value ) 
			{							
				if ( $value->number != $i )
				{
					$item['number'] = $i;
					$this->items[] = $item;
					$i = $value->number;
					$item = array();
				}
				$item[strtotime($value->date_insert)] = $value->url;	
			}	
			$item['number'] = $i;
			$this->items[] = $item;
		}	
		$this->total_items = 10;
	}	
}