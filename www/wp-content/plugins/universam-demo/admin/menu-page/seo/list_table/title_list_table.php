<?php
class USAM_List_Table_title extends USAM_Product_List_Table
{	
	protected $orderby = 'ID';
	protected $order   = 'desc';
	
	public function extra_tablenav_display( $which ) 
	{		
		echo '<div class="alignleft actions">';	
		submit_button( __( 'Сохранить', 'usam' ), 'primary','action', false, array( 'id' => 'save-seo-submit' ) );
		echo '<img src="'.esc_url( admin_url( 'images/wpspin_light.gif' ) ).'" id = "all-ajax-loading" class="ajax-loading"/></div>';
	}
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'terms' => array() );		
	}

	function column_product_title( $item ) 
	{		
		?>		
		<input type="text" id="product_title_<?php echo $item->ID; ?>" data-product_id="<?php echo $item->ID; ?>" name="post_title[<?php echo $item->ID; ?>]" value="<?php echo htmlspecialchars($item->post_title, ENT_QUOTES); ?>" class = "width100 show_change"/></br>
		<?php	
		echo __('Артикул','usam').': '.usam_get_product_meta($item->ID, 'sku', true )."</br>";
		echo __('Статус','usam').': '.$item->post_status;
		?>	
		</br></br>
		<a href="<?php echo $item->guid; ?>"><?php _e( 'Посмотреть', 'usam' ); ?></a>
		| <a href="<?php echo admin_url('post.php'); ?>?post=<?php echo $item->ID; ?>&action=edit"><?php _e( 'Изменить', 'usam' ); ?></a>		
		<?php
	}
	
	function column_row_action( $item ) 
	{		
		?>	
		<img src="<?php echo esc_url( admin_url( 'images/wpspin_light.gif' ) ); ?>" id = "ajax-loading-<?php echo $item->ID; ?>" class="ajax-loading"/>		
		<?php
	}	
	
	function column_post_excerpt( $item ) 
	{			
		?><textarea cols="" rows="" class="overflow_y show_change" data-product_id="<?php echo $item->ID; ?>" id="product_excerpt_<?php echo $item->ID; ?>" name="p_excerpt[<?php echo $item->ID; ?>]"><?php echo htmlspecialchars($item->post_excerpt, ENT_QUOTES); ?></textarea>	
		<?php
	}
	
	function column_post_content( $item ) 
	{			
		?><textarea cols="" rows="" class="overflow_y show_change" data-product_id="<?php echo $item->ID; ?>" id="product_content_<?php echo $item->ID; ?>" name="p_content[<?php echo $item->ID; ?>]"><?php echo $item->post_content; ?></textarea>	
		<?php
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'product_title' => array('title', false),			
			'price'         => array('product_price', false),		
			'date'          => array('date', false),					
			'stock'         => array('product_stock', false),
			'sku'           => array('product_sku', false),		
			);
		return $sortable;
	}
			
	function get_columns()
	{
        $columns = array(           
			'cb'    => '<input type="checkbox" />',			
			'image' => '',
			'product_title' => __( 'Название', 'usam' ),
			'post_content'  => __( 'Содержание', 'usam' ),
			'post_excerpt'  => __( 'Описание', 'usam' ), 		
			'stock'         => __( 'Запас', 'usam'),				
			'date'          => __( 'Дата', 'usam' ),	
			'row_action'    => '',				
        );		
        return $columns;
    }		
}