<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/seo/includes/positions_list_table.php' );	
class USAM_List_Table_links extends USAM_Positions_Table
{	
	public $orderby = 'ID';
	public $order   = 'desc';
	
	protected $period = 'month';
	protected $groupby_date = 'week';
	protected $search_box = false;
	
	function __construct( $args = array() )
	{	
		$this->set_date_period();	
		if ( !empty($_REQUEST['se']) )
		{			
			$this->search_engine = $_REQUEST['se'];			
		}
		if ( !empty($_REQUEST['site_id']) )
		{			
			$this->site_id = $_REQUEST['site_id'];			
		}	
		if ( !empty($_REQUEST['region']) )
			$this->region = $_REQUEST['region'];
		else
		{
			$location_ids = usam_get_search_engine_regions( array('fields' => 'location_id', 'search_engine' => $this->search_engine, 'number' => 1) );	
			$this->region = $location_ids[0];
		}
		parent::__construct( $args );
    }	
	
	public function extra_tablenav_display( $which ) 
	{		
		if ( 'top' == $which )
		{
			echo '<div class="alignleft actions">';	
			$url = $this->get_nonce_url( add_query_arg( array('action' => 'start'), $_SERVER['REQUEST_URI'] ) );
			?>		
			<a href="<?php echo $url; ?>" class = "button button-primary"><?php _e( 'Проверить сейчас', 'usam' ); ?></a>
			<?php 									
			echo '</div>';		
			echo '<div class="alignleft actions">';					
				$this->standart_button();									
			echo '</div>';						
		}
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'interval' => array(), 'groups_by_date' => array(), 'search_engine' => array(), 'regions' => array(), 'sites' => array() );		
	}		
		
	public function column_default( $item, $column_name ) 
	{ 
		if ( is_array($item[$column_name]) )
		{			
			foreach ( $item[$column_name] as $value ) 
			{			
				$host = parse_url($value['url'], PHP_URL_PATH);	
				$host = $host=='/'?$value['url']:$host;
				echo $this->colorate( $value['number'] )."<a href='".$value['url']."' target='_blank'>$host</a><br>";
			}
		}
	}	

	function prepare_items() 
	{	
		global $wpdb;
		
		$this->get_standart_query_parent();		
			
		if ( $this->search != '' )
		{			
			$this->where[] = "name='{$this->search}'";			
		}
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_KEYWORDS." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";		
		$keywords = $wpdb->get_results($sql_query);

		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );

		$sql_query = "SELECT * FROM ".USAM_TABLE_STATISTICS_KEYWORDS." WHERE search_engine='$this->search_engine' AND location_id='$this->region' AND site_id='$this->site_id' AND date_insert>='$this->start_date_interval' AND date_insert<='$this->end_date_interval' ORDER BY date_insert ASC";
		$statistics_keywords = $wpdb->get_results($sql_query);	
		foreach($keywords as $keyword )
		{
			$item = array( 'id' => $keyword->id, 'keyword' => $keyword->keyword );	
			$start_cycle = true;
			for ( $j = strtotime($this->start_date_interval); $j <= strtotime($this->end_date_interval); )
			{						
				$item[$j] = array();			
				$infinity = false;
				$k = 0;					
				$sum = 0;				
				$max = 1;
				$min = 99;	
				$current = array();
				foreach($statistics_keywords as $key => $statistic )
				{	
					if ( $keyword->id == $statistic->keyword_id )
					{
						if ( $j < strtotime($statistic->date_insert) )												
							break;							
						else
						{				
							if ( empty($current) || $current['url'] != $statistic->url || $current['number'] != $statistic->number )			
							{
								$current = array( 'url' => $statistic->url, 'number' => $statistic->number);
								$item[$j][] = array( 'url' => $statistic->url, 'number' => $statistic->number);
							}
							unset($statistics_keywords[$key]);			
						}
					}
				}			
				if ( $start_cycle )
					$j = $this->data_interval_start( $j );	
				
				$j = strtotime("+1 ".$this->groupby_date, $j);		
				$start_cycle = false;				
			}		
			$this->items[] = $item;	
		}	
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}	
}