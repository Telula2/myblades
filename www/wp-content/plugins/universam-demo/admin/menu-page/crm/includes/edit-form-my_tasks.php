<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_My_Tasks extends USAM_Edit_Form
{		
	protected function get_data_tab()
	{ 
		global $user_ID;
		if ( $this->id != null )	
		{
			$_event = new USAM_Event( $this->id );
			$this->data = $_event->get_data();			
			if ( $this->data['manager_id'] != $user_ID && $this->data['user_id'] != $user_ID )
			{ 				
				$this->sendback = remove_query_arg( array( 'action', 'id' ), $this->sendback );
				$this->sendback = add_query_arg( array( 'error' => 1 ), $this->sendback );				
				$this->redirect = true;	
			}
		}		
		else
			$this->data = array( 
				'reminder'    => 1, 
				'importance'  => 0, 
				'calendar'    => '', 
				'status'      => 1, 
				'title'       => '', 
				'description' => '', 		
				'user_id'     => $user_ID,
				'manager_id'  => 0, 
				'color'       => '', 			
				'type'        => 'task', 	
				'start'       => date("Y-m-d H:i:s", mktime(date("H"), 0, 0, date("m"), date("d")+1, date("Y"))), 
				'end'         => date("Y-m-d H:i:s", mktime(date("H"), 0, 0, date("m"), date("d")+2, date("Y"))), 
				'date_time'   => '',	
			);
		$this->statuses = array( 1 => __( 'Выполняется', 'usam'),  2 => __( 'Остановлена', 'usam'),  4 => __( 'Отменена', 'usam') , 3 => __( 'Завершена', 'usam') );
		add_action( 'admin_footer', array(&$this, 'admin_footer') );		
	}
	
	function admin_footer()
	{
		$html = "			
			<div class='action_buttons'>				
				<button type='button' class='action_confirm button-primary button' data-dismiss='modal' aria-hidden='true'>".__( 'Удалить', 'usam' )."</button>
				<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
			</div>";	
		echo usam_get_modal_window( __('Подтвердите','usam'), 'operation_confirm', $html, 'small' );
		
		echo usam_get_modal_window( __('Добавить','usam').usam_get_loader(), 'event_object_add', $this->get_add_data_modal_window() );		
	}
	
	function get_add_data_modal_window()
	{		
		$out = "<div class='product_bulk_actions'><textarea name='description'></textarea></div>";
		$out .= "<div class='popButton'>
				<button id = 'modal_action' type='button' class='button-primary button'>".__( 'Добавить', 'usam' )."</button>
				<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
			</div>";
		return $out;
	}
	
	function object_type( )
	{		
		$objects = usam_get_event_objects_all( $this->id );
		echo "<div class='event_objects_container'>";
		if ( !empty($objects) )
		{
			foreach ( $objects as $object )
			{		
				echo "<div class='event_object'>
					<div class='event_object_title'><a id='delete'><span class='dashicons dashicons-no-alt'></span></a></div><div class='event_object_content'>".usam_get_display_event_object( $object )."</div>
					<input type='hidden' value='$object->object_id' name='object_id[]' />	
					<input type='hidden' value='$object->object_type' name='object_type[]' />	
				</div>";
			}
		}
		else
		{
			_e( 'Нет объектов', 'usam' );
		}
		echo "</div>";
    }	
	
	function event_action_lists()
	{	
		?>		
		<div class='action_lists usam_edits'>
			<?php
			$actions = usam_get_event_action_lists( array( 'event_id' => $this->id ) );
			foreach ( $actions as $action )
			{
				?>		
				<div class='action-box <?php echo $action->made?'made_action':''; ?>' data-id="<?php echo $action->id; ?>">			
					<div class='action_wrapper'>					
						<div class='status_action'></div>	
						<div class='event_action_name'>
							<div id = "edit_name_action" class='usam_display_data '><?php echo $action->name ; ?></div>
							<div class='usam_edit_data'>
								<textarea class="event_input_text" tabindex="1" dir="auto"><?php echo $action->name; ?></textarea>							
								<button id='save_name_action' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>
							</div>		
						</div>					
						<a class="js_delete_action" href="#"></a>
					</div>	
				</div>	
				<?php
			}
			?>	
		</div>	
		<input type="text" class="event_action_input js_new_event_action" value =""/>
		<button id='add_event_action' type="button" class="button"><span class="dashicons dashicons-plus"></span></button>
		<?php		
	}	
	
	function display_left()
	{
		$this->titlediv( $this->data['title'] );					
		$this->add_box_description( $this->data['description'] );	
		
		usam_add_box( 'usam_task_settings', __('Настройки задания','usam'), array( $this, 'task_settings' ) );		
		if ( $this->id )
		{
			usam_add_box( 'usam_comments', __('Комментарии','usam'), array( $this, 'display_comments' ), 'event' );		
			usam_add_box( 'usam_event_object_type', __('Прикрепленные объекты','usam'), array( $this, 'object_type' ) );						
		}
    }	
	
	function select_users()
	{
		usam_select_manager('', array('id' => 'users') );
		?>	
		<button id='add_participant' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>			
		<div class='participants'>
			<?php
			$users = usam_get_event_users( $this->id );
			foreach ( $users as $user_id )
			{				
				?>		
				<div class='user-box' data-user_id="<?php echo $user_id; ?>">			
					<div class='user_wrapper'>	
						<div class='user_name'>
							<?php echo usam_get_manager_name($user_id); ?>
						</div>					
						<a class="js_delete_action" href="#"></a>
					</div>	
				</div>	
				<?php
			}
			?>	
		</div>	
		<?php
	}
	
	function display_file_upload()
	{
		?>	
		<div id = 'email_attachments' class = 'attachments'>
			<div class ='attachments-content'>
				<ul>											
					<li>
						<div id="drop">
							<a>
								<?php _e('Прикрепить', 'usam') ?><br />
								<?php _e('файл', 'usam') ?>
							</a>			
							<input id="file_upload" type="file" name="upl" multiple />
						</div> 
					</li>
				<?php 	
				$out = '';	
				$attachments = usam_get_event_metadata( $this->id, 'attachments' ); 
				if ( !empty($attachments) )
				{
					foreach ( $attachments as $key => $attachment ) 
					{ 						
						$file_path = USAM_UPLOAD_DIR.'events/'.$this->id.'/'.$attachment['name'];
						$file_url = USAM_UPLOAD_URL.'events/'.$this->id.'/'.$attachment['name'];									
						if ( file_exists($file_path) )
							$size = size_format( filesize($file_path) );
						else
							$size = '';
								
						$out .= "<li data-id='$key'>
								<a class='js_delete_action'></a>
								<a href='".$file_url."' title ='".$attachment['title']."' target='_blank'><div class='attachment_icon' style='background-image:url(".usam_get_icon_by_file_extension( $file_path ).");'></div></a>
								<div class='filename'>".usam_get_attachment_title( $attachment['title'], $file_path )."</div>
								<div class='filesize'><a download href='".$file_url."' title ='".__('Сохранить этот файл себе на компьютер','usam')."' target='_blank'>".__('Скачать','usam')."</a>".$size."</div>											
							</li>";
					}	
				}
				$delete_attachment = array( 'id' => $this->id, 'action' => 'delete_event_file', 'nonce' => usam_create_ajax_nonce('delete_event_file') );
				$out .= "<script>
				var fileupload_url ='". add_query_arg( array('usam_ajax_action' => 'event_file_upload', 'id' => $this->id, 'action' => 'usam_ajax', 'nonce' => usam_create_ajax_nonce('event_file_upload') ), admin_url( 'admin-ajax.php' ))."';				
				var delete_attachment=". json_encode( $delete_attachment ).";	
				</script>";
				echo $out;
				?>								
				</ul>
			</div>
		</div>	
		<?php
	}	
}
?>