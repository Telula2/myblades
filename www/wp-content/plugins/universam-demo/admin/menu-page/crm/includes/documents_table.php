<?php
class USAM_Documents_Table extends USAM_List_Table
{	
	protected $order = 'DESC';
	protected $document_type = '';
	
	function __construct( $args = array() )
	{	
		require_once( USAM_FILE_PATH . '/includes/crm/documents_query.class.php' );		
		parent::__construct( $args );
    }
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),			
		);
		return $actions;
	}	
	
	function column_number( $item ) 
    {
		$number = '<a class="row-title" href="'.esc_url( add_query_arg( array('action' => 'edit', 'id' => $item->id), $this->url ) ).'">'.$item->number.'</a>';
		$this->row_actions_table( $number, $this->standart_row_actions( $item->id ) );	
	}
		 
	function get_sortable_columns()
	{
		$sortable = array(
			'number'      => array('number', false),
			'name'        => array('name', false),
			'manager'     => array('manager', false),		
			'status'      => array('status', false),		
			'totalprice'  => array('totalprice', false),	
			'closedate'   => array('closedate', false),	
			'date'  => array('date_insert', false),				
			);
		return $sortable;
	}
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'number'         => __( 'Номер', 'usam' ),				
			'name'           => __( 'Предложение', 'usam' ),		
			'status'         => __( 'Статус', 'usam' ),				
			'totalprice'     => __( 'Сумма', 'usam' ),		
			'closedate'      => __( 'Срок', 'usam' ),	
			'date'           => __( 'Дата', 'usam' ),	
			'manager'        => __( 'Менеджер', 'usam' ),				
        );		
        return $columns;
    }
		
	function prepare_items() 
	{			
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 
			'search' => $this->search, 			
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'type' => $this->document_type,
		);			
		if ( !empty($this->records) )
			$query['include'] = $this->records;
	
		$_contacts = new USAM_Documents_Query( $query );		
		$this->items = $_contacts->get_results();		
		$total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page ) );
	}
}
?>