<?php
class USAM_List_Table_My_Tasks extends USAM_List_Table 
{	
	protected $statuses;
	protected $status;	
	protected $type_task;
	protected $orderby = 'start';
	protected $order = 'ASC';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		$this->statuses = array( 1 => __( 'Выполняется', 'usam'),  2 => __( 'Остановлена', 'usam') , 3 => __( 'Завершена', 'usam') );	
		
		if ( isset($_GET['status']) && is_numeric($_GET['status']) )
			$this->status = $_GET['status'];
		elseif ( isset($_GET['status']) && $_GET['status'] == 'all' )
		{
			$this->status = 'all';
		}
		else
			$this->status = 1;			
    }	

	public function get_views() 
	{
		global $wpdb, $user_ID;
		
		$url = remove_query_arg( array('post_status', 'paged', 'action', 'action2', 'm', 'deleted', 'updated', 'paged', 's', 'orderby','order') );		
		
		if ( $this->type_task == 'schedule' )
		{
			$where = "manager_id = '$user_ID' AND type = 'task'";		
		}
		elseif( $this->type_task == 'affairs' )
		{
			$user = !empty($_GET['user'])?$_GET['user']:$user_ID;		
			$where = "type = 'affair' AND user_id = '$user'";
		}
		elseif( $this->type_task == 'my' )
		{
			$user = !empty($_GET['user'])?$_GET['user']:$user_ID;	
			$type = array('task','assigned_task','affair', 'meeting', 'call', 'event');				
			$where = "user_id = '$user' AND type IN ('".implode("','",$type)."')";
		}
		else
		{
			$user = !empty($_GET['user'])?$_GET['user']:$user_ID;		
			$where = "type = '{$this->type_task}' AND user_id = '$user'";
		}
		$sql = "SELECT DISTINCT status, COUNT(*) AS count FROM " . USAM_TABLE_EVENTS . " WHERE $where GROUP BY status ORDER BY status";
		$results = $wpdb->get_results( $sql );
		
		$statuses = array();		
		$total_count = 0;	
		if ( ! empty( $results ) )
		{			
			foreach ( $results as $status )
			{				
				$statuses[$status->status] = $status->count;						
			}
			$total_count = array_sum( $statuses );
		}
		$all_text = sprintf(_nx( 'Всего <span class="count">(%s)</span>', 'Всего <span class="count">(%s)</span>', $total_count, 'purchase logs', 'usam' ),	number_format_i18n( $total_count ) );	
		$all_class = $this->status == 'all' && $this->search == '' ? 'class="current"' : '';	
		$href = add_query_arg( 'status', 'all', $url );
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $all_class, $all_text ), );				
	
		foreach ( $this->statuses as $key => $title )		
		{			
			$number = !empty($statuses[$key])?$statuses[$key]:0;
			if ( !$number )
				continue;
			
			$text = $text = sprintf( $title.' <span class="count">(%s)</span>', number_format_i18n( $number )	);
			$href = add_query_arg( 'status', $key, $url );
			$class = $this->status == $key ? 'class="current"' : '';
			$views[$key] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );			
		}
		return $views;
	}
		
	public function type_dropdown() 
	{	
		$selected = isset( $_REQUEST['type'] ) ? sanitize_title($_REQUEST['type']) : 0;		
		$types = array( 'affair' => __('Дело','usam'), 'meeting' => __('Встреча','usam'), 'call' => __('Звонок','usam'), 'event' => __('Событие','usam'), 'email' => __('Сообщение','usam'), 'task' => __('Задание','usam') );
		?>	
		<select name="type">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Тип', 'usam'); ?></option>
			<?php	
			foreach ( $types as $id => $name )
			{
				?><option <?php selected( $id, $selected ); ?> value="<?php echo $id; ?>"><?php echo $name; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	public function object_dropdown() 
	{	
		$selected = isset( $_REQUEST['object'] ) ? sanitize_title($_REQUEST['object']) : 0;		
		$objects = array( 'product' => __('Товар','usam'), 'order' => __('Заказ','usam'), 'contact' => __('Контакт','usam'), 'company' => __('Компания','usam') );
		?>	
		<select name="object">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Тип объекта', 'usam'); ?></option>
			<?php				
			foreach ( $objects as $id => $name )
			{
				?><option <?php selected( $id, $selected ); ?> value="<?php echo $id; ?>"><?php echo $name; ?></option><?php
			}
			?>				
		</select>
		<?php
	}
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'type' => array(), 'object' => array() );		
	}	
	
	function column_title( $item )
	{	
		$title = '<a class="row-title" href="'.esc_url( add_query_arg( array('action' => 'edit', 'id' => $item->id), $this->url ) ).'">'.$item->title.'</a>';
		$this->row_actions_table( $title, $this->standart_row_actions( $item->id ) );	
	}

	function column_manager( $item ) 
	{	
		$user = get_user_by('id', $item->user_id );			
		echo $user->display_name;	
	}	
	
	function column_user( $item )
	{
		if ( $item->user_id != 0)
		{	
			$user = get_user_by('id', $item->user_id);
			$manager = isset($user->display_name)?"$user->display_name ({$user->user_login})":'';
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo $manager; ?></span>
			</div>
			<?php 
		}				
	} 
	
	function column_object_type( $item )
	{
		$objects = usam_get_event_objects_all( $item->id ) ;
		foreach ( $objects as $object )
		{		
			echo usam_get_display_event_object( $object );
			echo "<br>";
		}
	} 
	
	function column_participants( $item )
	{
		$users = usam_get_event_users( $item->id );
		foreach ( $users as $user_id )
		{				
			echo usam_get_manager_name( $user_id );
			echo "<br>";
		}
	} 
	
	function column_time_work( $item )
	{
		if ( !empty($item->start) && !empty($item->end) )
		{
			$time = round((strtotime($item->end) - strtotime($item->start)) / 60);
			if ( $time == 0 )
				_e( 'менее минуты', 'usam' );
			else
				echo $time." ".__("мин","usam");
		}
	} 	
	
	function column_time( $item )
	{		
		if ( !empty($item->start) )
			echo usam_local_date( $item->start, get_option( 'date_format', 'Y/m/d' ).' H:i' );
		if ( !empty($item->end) )
			echo " - ".usam_local_date( $item->end, get_option( 'date_format', 'Y/m/d' ).' H:i' );
	} 
	
	function column_reminder( $item )
	{
		if ( !empty($item->date_time) )
			echo usam_local_date( $item->date_time, get_option( 'date_format', 'Y/m/d' ).' H:i' );
	} 
	
	function column_calendar_id( $item )
	{ 
		echo usam_get_calendar_name_by_id( $item->calendar );
	} 
	
	function column_color( $item )
	{ 
		if ( $item->importance )
			echo '<span class="dashicons dashicons-star-filled"></span>';
		else
			echo '<span class="dashicons dashicons-star-empty"></span>';
		
		$color = $item->color != ''?'color_'.$item->color:'';
		echo '<span class="dashicons dashicons-image-filter '.$color.'"></span>';
	} 
	
	function column_status( $item ) 
	{		
		$out ='<select data-id = "'.$item->id.'" class = "select_status_record">';
		foreach( $this->statuses as $key => $value )
		{
			if ( $item->status == $key )
				$selected = 'selected="selected"';
			else
				$selected = '';
			$out.="<option $selected value='$key'>$value</option>";
		}	
		$out.='</select>';			
		echo $out;
		usam_loader();		
	}
	
	function column_type( $item ) 
	{	
		global $user_ID;
		if ( $item->type == 'assigned_task' )
			echo '<span class="dashicons dashicons-arrow-left-alt" title="'.__( 'Назначенное задание', 'usam' ).'"></span>';
		elseif ( $item->type == 'task' )
		{
			if ( $item->user_id == $user_ID )
				echo '<span class="dashicons dashicons-flag" title="'.__( 'Задание', 'usam' ).'"></span>';
			else
				echo '<span class="dashicons dashicons-arrow-right-alt" title="'.__( 'Назначенное вам задание', 'usam' ).'"></span>';
		}
		elseif ( $item->type == 'affair' )
		{
			echo '<span class="dashicons dashicons-flag" title="'.__( 'Задание', 'usam' ).'"></span>';
		}
		elseif ( $item->type == 'meeting' )
		{
			echo '<span class="dashicons dashicons-groups" title="'.__( 'Встреча', 'usam' ).'"></span>';
		}
		elseif ( $item->type == 'call' )
		{
			echo '<span class="dashicons dashicons-phone" title="'.__( 'Звонок', 'usam' ).'"></span>';
		}
		elseif ( $item->type == 'event' )
		{
			echo '<span class="dashicons dashicons-calendar-alt" title="'.__( 'Событие', 'usam' ).'"></span>';
		}
	}
	   
    function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
 
	function get_sortable_columns() 
	{
		$sortable = array(			
			'reminder'     => array('reminder', false),
			'date_time'    => array('date_time', false),		
			'date_insert'  => array('date_insert', false)
			);
		return $sortable;
	}
	
	public function single_row( $item ) 
	{		
		if ( empty($item->start) )
		{
			$row_class = '';		
		}
		elseif ( date('d', strtotime($item->start)) == date_i18n('d') )
		{
			$row_class = 'event_day';
		}
		elseif ( date('d',strtotime($item->start)) <= date_i18n('d') )
		{
			$row_class = 'event_overdue';
		}		
		else
			$row_class = '';		
		echo '<tr class ="row '.$row_class.'" id = "row-'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}	
		
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',	
			'color'         => '',		
			'type'          => '',				
			'title'         => __( 'Название', 'usam' ),			
			'description'   => __( 'Описание', 'usam' ),
			'status'        => __( 'Статус', 'usam' ),	
			'time'          => __( 'Срок', 'usam' ),	
			'manager'       => __( 'Менеджер', 'usam' ),	
			'participants'  => __( 'Участники', 'usam' ),			
			'reminder'      => __( 'Напомнить', 'usam' ),
			'calendar_id'   => __( 'Календарь', 'usam' ),					
        );
        return $columns;	
    }	
	
	function prepare_items() 
	{		
		global $user_ID;
		
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'search' => $this->search, 'orderby' => $this->orderby, 'order' => $this->order );		
			
		if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
		$args['status'] = $this->status;		
	
		if ( $this->type_task == 'schedule' )
		{
			$args['manager_id'] = $user_ID;
			$args['type'] = 'task';	
		}			
		elseif( $this->type_task == 'my' )
		{
			$args['user_id'] = $user_ID;	
			$args['type'] = array('task','assigned_task','affair', 'meeting', 'call', 'event');	
		}
		elseif( $this->type_task == 'work' )
		{
			$users_ids = get_users( array( 'fields' => 'ID', 'meta_key' => 'usam_chief', 'meta_value' => $user_ID ) );			
			$args['user_id'] = $users_ids;	
			$args['type'] = 'work';	
		}		
		else
		{			
			$args['type'] = $this->type_task;	
			$args['user_id'] = $user_ID;				
		}	
		$args = array_merge ($args, $this->get_date_for_query() );		
		
		$query = new USAM_Tasks_Query( $args );
		$this->items = $query->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}