<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/documents_table.php' );	
class USAM_List_Table_Documents_View_Form extends USAM_Documents_Table 
{	
	protected $search_box = false;	

	function get_bulk_actions() { }	

	function prepare_items() 
	{			
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 
			'search' => $this->search, 			
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'type' => $this->document_type,
			'contacts' => $this->id,
		);							
		$_contacts = new USAM_Documents_Query( $query );		
		$this->items = $_contacts->get_results();		
		$total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page ) );
	}	
}