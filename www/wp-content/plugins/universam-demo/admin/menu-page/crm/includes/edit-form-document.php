<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
class USAM_Form_Document extends USAM_Edit_Form
{	
	private $default;	
	private $total = 0;
	private $discount = 0;
	private $tax = 0;
	private $shipping = 0;	
	
	protected $text1;	
	protected $text2;
	
	public function section_customers( )
	{				
		$display_text = __('Еще не выбрано','usam');
		if ( $this->data['customer_type'] == 'contact' )
		{
			$contact_id = $this->data['customer_id'];
			$company_id = null;
			$company_class = 'class="hide"';
			$contact_class = '';
			
			$contact = usam_get_contact( $this->data['customer_id'] );			
			if ( !empty($contact) )	
			{	
				$url = admin_url("admin.php?page=crm&tab=contacts&id=".$this->data['customer_id']."&action=edit");
				$display_text = "<a href='$url' target='_blank'>".$contact['firstname']." ".$contact['lastname']."</a>";	
			}
		}
		else
		{
			$this->data['customer_type'] = 'company';
			$company_id = $this->data['customer_id'];
			$contact_id = null;
			$company_class = '';
			$contact_class = 'class="hide"';
			
			$company = usam_get_company( $this->data['customer_id'] );			
			if ( !empty($company) )	
			{	
				$url = admin_url("admin.php?page=crm&tab=company&id=".$this->data['customer_id']."&action=edit");
				$display_text = "<a href='$url' target='_blank'>".$company['name']."</a>";	
			}
		}
		?>	
		<div class ="usam_customer">			
			<div class ="usam_edit_customer usam_edit_data">		
				<div class="colum">		
					<strong><?php _e('Клиент','usam'); ?>:</strong>				
					<select id="customer_type" class="chzn-select-nosearch-load" name = "customer_type">
						<option value='contact' <?php selected( $this->data['customer_type'], "contact") ?>><?php _e('Контакт','usam'); ?></option>	
						<option value='company' <?php selected( $this->data['customer_type'], "company") ?>><?php _e('Компания','usam'); ?></option>	
					</select>
				</div>				
				<div class="autocomplete_forms colum">						
					<div id="customer_type_contact" <?php echo $contact_class; ?>>			
						<?php
							$autocomplete = new USAM_Autocomplete_Forms( );
							$autocomplete->get_form_contact( $contact_id, array('id' => 2) );
						?>	
					</div>		
					<div id="customer_type_company" <?php echo $company_class; ?>>			
						<?php
							$autocomplete = new USAM_Autocomplete_Forms( );
							$autocomplete->get_form_company( $company_id );
						?>	
					</div>					
				</div>							
			</div> 	
			<div class ="usam_select_customer usam_display_data">			
				<?php echo $this->text1; ?>: <strong><?php echo $display_text; ?></strong>
			</div>	
		</div>	
		<strong><?php echo $this->text2; ?>:</strong>				
		<?php
		$this->table_contacts( );
		if ( $this->data['status'] < 2 ) 
		{				
			?>						
			<button id='add_contact' type="button" class="button"><?php _e( 'Добавить контакт', 'usam' ); ?></button>
			<?php				
		}
	}		
		
	protected function table_contacts( )
	{	
		?>	
		<div class = 'table_contacts'>				
			<table class="widefat">				
				<tbody>				
					<?php
					if ( empty($this->data['contacts']) )
					{
						?>
							<tr class = "items_empty"><td colspan = '2'><?php _e( 'Нет контактов', 'usam' ); ?></td></tr>
						<?php
					}
					else
					{								
						$i = 0;
						$this->total = 0;
						foreach ( $this->data['contacts'] as $key => &$contact ) 
						{
							?>
							<tr id = "contact_<?php echo $contact->id; ?>">									
								<td>
									<?php echo $contact->firstname." ".$contact->lastname; ?>
									<input type='hidden' name='contacts_ids[]' value='<?php echo $contact->id; ?>'/>									
									<a class='button_delete'></a>
								</td>								
							</tr>
							<?php	
						}
					}	
					?>
				</tbody>				
			</table>							
		</div>	
		<?php		
	}
	
	public function section_products( )
	{		
		?>	
		<div class = 'header_product'>
			<strong><?php esc_html_e( 'Цены:', 'usam' ); ?> </strong><span class ="usam_display_data"><?php echo usam_get_name_price_by_code( $this->data['type_price'] ); ?></span>				
			<span class ="usam_edit_data"><?php usam_get_select_prices( $this->data['type_price'] ); ?></span>				
		</div>			
		<?php			
		$this->table_products( );
		if ( $this->data['status'] < 2 ) 
		{				
			$this->add_product_button();
		}
	}
	
	public function table_products( )
	{		
		$columns = array(
		 'n'         => __( '№', 'usam' ),
		 'title'     => __( 'Имя', 'usam' ),
		 'sku'       => __( 'Артикул', 'usam' ),			 
		 'edit_price' => __( 'Цена', 'usam' ),
		 'discount'  => __( 'Скидка', 'usam' ).'<input size="4" type="text" name="_discount_cart" id="_discount_cart" value="" class = "usam_edit_data">',
		 'discount_price' => __( 'Цена со скидкой', 'usam' ),	
		 'tax'        => __( 'Налог', 'usam' ),
		 'quantity'   => __( 'Количество','usam' ),	 		 
		 'total'     => __( 'Всего', 'usam'),
		 'action'    => __( 'Действие', 'usam'),
		);			
		$this->display_products_table( $this->data['products'], $columns );
	}
	
	protected function display_table_products( $products, $columns )
	{		
		if ( empty($products) )
		{
			?>
				<tr class = "items_empty"><td colspan = '<?php echo count($columns); ?>'><?php _e( 'Нет товаров', 'usam' ); ?></td></tr>
			<?php
		}
		else
		{				
			$i = 0;
			$this->total = 0;
			foreach ( $products as $key => $product ) 
			{ 
				$product = (object)$product;	
				$post = get_post($product->product_id, ARRAY_A);						
				$i++;
				
				$discount = 0;
				if ( $product->old_price > 0)
					$discount = $product->old_price - $product->price;
				
				$this->discount += $discount;
				$this->tax      += $product->tax;	
				?>
				<tr id ="product_<?php echo $product->id; ?>" data-item-id="<?php echo $product->id; ?>" >					
					<?php
					foreach ( $columns as $column => $name ) 
					{						
						$data = '';					
						switch ( $column ) 
						{
							case 'n' :
								$data = $i;
							break;
							case 'title' :
								$url = admin_url('post.php?post='.$product->product_id.'&action=edit');
								$data = "<a id = 'details_product' title='".$post['post_title']."' href='$url'>".$post['post_title']."</a>";								
							break;
							case 'sku' :
								$data = usam_get_product_meta( $product->product_id, 'sku', true );
							break;
							case 'quantity' :
								$data = "<span class='usam_display_data'>$product->quantity</span><input size='4' type='text' name='products[$product->id][quantity]' class='usam_edit_data' value='$product->quantity'>";
							break;
							case 'edit_price' :
								$price = $product->old_price>0?$product->old_price:$product->price;			
								$data = "<span class='usam_display_data'>{$price}</span><input size='4' type='text' name='products[$product->id][price]' class='usam_edit_data' value='$price'>";
							break;							
							case 'discount_price' :
								$data = $product->price;									
							break;								
							case 'tax' :							
								$data = "<span class='usam_display_data'>{$product->tax}</span><input size='4' type='text' name='products[$product->id][tax]' class='usam_edit_data' value='$product->tax'>";
							break;							
							case 'discount' :
								$discount = $product->old_price>0?round(100 - $product->price*100/$product->old_price,2):0;
								$data = "<span class = 'usam_display_data'>$discount %</span>		
								<span class = 'usam_edit_data'>				
									<input size='4' type='text' name='products[$product->id][discount]' value='$discount'>
									<select name='products[$product->id][type]'>
										<option value='p'>%</option>
										<option value='f'>-</option>
									</select>	
								</span>";
							break;
							case 'total' :
								$total = ($product->price + $this->tax) * $product->quantity;		
								$data = usam_currency_display( $total, array( 'decimal_point' => true ) ); 
								$this->total += $total;
							break;							
							case 'action' :													
								$data = "<input type='hidden' name='products_ids[]' value='$product->id'/>";
								$data .= "<a class='button_delete'></a>";								
							break;
						}
						echo "<td class = 'column-$column'>$data</td>";
					}
					?>	
				</tr>
				<?php											
			}
		}		
	}	

	protected function total_table_product( $products, $columns )
	{		
		$columns_count = count( $columns ) - 3;	
		?>	
		<tr class="cart_total">
			<td colspan='<?php echo $columns_count; ?>'></td>
			<th><strong><?php esc_html_e( 'Сумма', 'usam' ); ?></strong></th>
			<th><strong><?php echo usam_currency_display($this->total); ?></strong></th>				
			<td></td>
		</tr>	
		<?php if ( $this->discount > 0 ) { ?>
		<tr>
			<td colspan='<?php echo $columns_count; ?>'></td>
			<th><?php esc_html_e( 'Скидка', 'usam' ); ?> </th>
			<th><?php echo usam_currency_display($this->discount); ?></th>				
			<td></td>
		</tr>		
		<?php }
		if ( $this->tax > 0 ) { ?>
		<tr>
			<td colspan='<?php echo $columns_count; ?>'></td>
			<th><?php esc_html_e( 'Налог', 'usam' ); ?> </th>
			<th><?php echo usam_currency_display($this->tax); ?></th>
			<td></td>
		</tr>	
		<?php }
		if ( $this->data['shipping'] > 0 ) { ?>		
		<tr>
			<td colspan='<?php echo $columns_count; ?>'></td>
			<th><?php esc_html_e( 'Доставка', 'usam' ); ?> </th>
			<th><?php echo usam_currency_display($this->data['shipping']); ?></th>
			<td></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan='<?php echo $columns_count; ?>'></td>
			<th><strong><?php esc_html_e( 'Итог', 'usam' ); ?></strong></th>
			<th><strong><?php echo usam_currency_display($this->data['totalprice']); ?></strong></th>
			<td></td>
		</tr>			
		<?php
	}
	
	public function actions_buttom( $links )
	{								
		?>		
		<div id = "actions_buttom">
			<ul>
			<?php 
			do_action( 'usam_this_links_start' ); 
			foreach ( $links as $link )
			{					
				$target = '';
				if ( !empty($link['target']) )
					$target = 'target="'.$link['target'].'"';
				
			
				?>		
				<li>
				<img src='<?php echo $link['icon_url']; ?>' alt='<?php echo $link['title']; ?>' />&ensp;				
				<a id ="<?php echo $link['id']; ?>" href='<?php echo $link['action_url']; ?>' <?php echo $target; ?>><?php echo $link['title']; ?></a>	
				</li>
				<?php				
			}		
			?>	
			</ul>
		</div>		
	<?php		
	}	
	
	public function customer()
	{
		$links = array( 			
			array( 'id' => 'open_send_email', 'action_url' => usam_url_admin_action( 'send_email_to_document', admin_url('admin.php'), array( 'document' => $this->data['type'], 'id' => $this->id ) ), 'title' => esc_html__( 'Отправить на почту', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ), 
			array( 'id' => 'printed_form_to_pdf','action_url' => usam_url_admin_action( 'printed_form_to_pdf', admin_url('admin.php'), array( 'form' => $this->data['type'], 'id' => $this->id ) ), 'title' => esc_html__( 'pdf', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/pdf_download.png', 'target' => '_blank' ), 	
			array( 'id' => 'printed_form', 'action_url' => usam_url_admin_action( 'printed_form', admin_url('admin.php'), array( 'form' => $this->data['type'], 'id' => $this->id ) ), 'title' => esc_html__( 'Распечатать', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/printer.png', 'target' => '_blank' ), 				
		);							
		$this->actions_buttom( $links );
	}	
  
	function display_right()
	{	
		usam_add_box( 'usam_customer', __('Клиенту','usam'), array( $this, 'customer' ) );
	}
	
	function admin_footer()
	{ 
		echo usam_get_modal_window( __('Отправить счет на почту','usam'), 'send_mail', $this->get_modal_window() );	
		echo usam_get_modal_window( __('Выбрать контакт', 'usam'), 'select_contacts', $this->select_contacts_windows() );				
	}		
	
	function select_contacts_windows( )
	{
		global $user_ID;	
		ob_start();	
		?>		
		<div class="usam_select_list">						
			<iframe id="usam_select_list_iframe" src="<?php echo usam_url_admin_action( 'display_contacts_list', admin_url('admin.php') ); ?>"></iframe>
		</div>	
		<?php 
		$out = ob_get_contents();
		ob_end_clean();		
			
		return $out;
	}
	
	public function get_to_email()
	{
		$to_email = array();	
		foreach ( $this->data['contacts'] as $contact ) 
		{
			$communication = usam_get_contact_means_communication( $contact->id ); 					
			if ( !empty($communication['email']) )
				foreach ( $communication['email'] as $email ) 			
				{								
					$to_email[] = $email['value'];
				}	
		}				
		if ( $this->data['customer_type'] == 'company' )
			$communication = usam_get_company_means_communication( $this->data['customer_id'] );
		else
			$communication = usam_get_contact_means_communication( $this->data['customer_id'] );
		if ( !empty($communication['email']) )
			foreach ( $communication['email'] as $email ) 			
			{								
				$to_email[] = $email['value'];
			}	
			
		return $to_email;
	}	
	
	public function get_modal_window()
	{			
		$to_email = $this->get_to_email();
		
		if ( $this->data['type'] == 'invoice' )
			$subject = 	sprintf(__( 'Счет №%s', 'usam' ), $this->data['number']).' - '.$this->data['name'];
		elseif ( $this->data['type'] == 'suggestion' )
			$subject = 	__( 'Коммерческое предложение', 'usam' ).' - '.$this->data['name'];	
		else
			$subject = '';
			
		$args = array( 'form_url' => usam_url_admin_action( 'send_email', admin_url('admin.php'), array( 'do_action_send_email' => 'document', 'id' => $this->id ) ), 'object_id' => $this->data['id'], 'object_type' => $this->data['type'], 'customer_type' => $this->data['customer_type'], 'customer_id' => $this->data['customer_id'], 'to_email' => $to_email, 'subject' => $subject, 'message' => '' );
		
		$file_name = usam_get_document_metadata( $this->data['id'], 'file_name');
		if ( !empty($file_name) )
		{
			$attachments = array( array( 'url' => USAM_DOCUMENTS_URL.$this->data['type'].'/'.$file_name, 'path' => USAM_DOCUMENTS_DIR.$this->id.'/'.$file_name, 'title' => $file_name ) );
		}
		else
			$attachments = array();	
		
		$out = usam_get_form_send_message( $args, $attachments );
		return $out;
	}	
}
?>