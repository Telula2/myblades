<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/my_tasks_list_table.php' );		
class USAM_List_Table_Affairs_View_Form extends USAM_List_Table_My_Tasks 
{	
	protected $search_box = false;	
	protected $customer = '';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		$this->customer = isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'contacts'?'contact':'company';
    }	
	
	function get_bulk_actions() 
	{	
		
	}		
	
	protected function get_filter_tablenav( ) 
	{		
		return array( );		
	}
	
	function column_title( $item )
	{	
		$url = add_query_arg( array( 'id' => $item->id ), admin_url('admin.php?page=crm&tab=tasks&action=edit') );	
		echo "<a href='$url'>{$item->title}</a>";	
	}	
	
	public function get_views() 
	{
		
	}	
}