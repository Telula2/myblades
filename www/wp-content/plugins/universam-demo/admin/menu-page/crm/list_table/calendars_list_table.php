<?php
class USAM_List_Table_calendars extends USAM_List_Table
{	   
	function __construct()
	{	
       parent::__construct( array(         
            'plural'    => 'contact_status',            
		) );
    }	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item )
	{		
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );
	}
				   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'id'        => array('id', false),		
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',				
			'name'             => __( 'Название', 'usam' ),	
			'sort'             => __( 'Сортировка', 'usam' ),			
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		$calendars = usam_get_user_calendars();	

		if ( empty($calendars) )
			$this->items = array();	
		else
			foreach( $calendars as $key => $item )
			{						
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}		
		$this->total_items = count($this->items);	
		$this->forming_tables();	
	}
}
?>