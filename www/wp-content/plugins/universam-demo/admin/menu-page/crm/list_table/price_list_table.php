<?php
class USAM_List_Table_price extends USAM_List_Table
{	
	public function column_date( $item ) 
	{			
		echo date( 'd.m.Y', strtotime($item['date']) );			
	}
	
	public function column_date_modified( $item ) 
	{	
		echo date( 'd.m.Y', strtotime($item['date_modified']) );			
	}
	
	function get_sortable_columns()
	{
		$sortable = array(
			'title'  => 'title',		
			'date'   => 'date',
			);
		return $sortable;
	}
	
	function column_title( $item ) 
    {
		$this->row_actions_table( $item['title'], $this->standart_row_actions( $item['id'] ) );	
	}	
	
	function column_action( $item ) 
    {	
		?>
		<a href="<?php echo admin_url(); ?>?price_list=download&file=<?php echo $item['id']; ?>"><?php _e('Скачать','usam'); ?></a>		
		<?php
	}
		
	function get_columns()
	{
        $columns = array(  				
			'cb'             => '<input type="checkbox" />',
			'title'          => __( 'Название', 'usam' ),	
			'date_insert'    => __( 'Дата', 'usam' ),	
			'date_modified'  => __( 'Дата изменения', 'usam' ),				
			'action'         => __( 'Действие', 'usam' ),			
        );
        return $columns;
    }
	
	function prepare_items() 
	{	
		$option = get_option('usam_price_list_setting');
		$items = maybe_unserialize( $option );		
		if ( !empty($items) )
		{
			foreach( $items as $id => $item)	
			{
				if ( empty($this->record) || in_array($item['id'], $this->records) )
				{	
					$this->items[] = $item;
				}
			}
		}
		$this->total_items = count($this->items);		
		$this->forming_tables();
	}
}
?>