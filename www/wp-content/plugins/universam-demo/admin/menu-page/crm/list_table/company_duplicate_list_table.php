<?php
class USAM_List_Table_company_duplicate extends USAM_List_Table
{	
	private $results = array();
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'combine'    => __( 'Объединить', 'usam' ),			
		);
		return $actions;
	}	

	public function extra_tablenav( $which ) 
	{ 
		if ( 'top' == $which && $this->filter_box ) 
		{					
			$checked_phone = isset($_REQUEST['phone'])?1:"";
			$checked_email = isset($_REQUEST['email'])?1:"";
			?>
				<div class = "usam_manage usam_manage-contacts_duplicate">							
					<input type='checkbox' id ="phone" name='phone' <?php checked( $checked_phone ); ?> value = '1' />
					<label for="phone"><?php esc_html_e( 'Телефон', 'usam' ); ?></label>
					<input type='checkbox' id ="email" name='email' <?php checked( $checked_email ); ?> value = '1' />
					<label for="email"><?php esc_html_e( 'E-mail', 'usam' ); ?></label>
					<?php submit_button( __( 'Найти дубликаты', 'usam' ), 'button-secondary', 'find_duplicates', false ); ?>
				</div>
			<?php			
		}			
	}		
		
	function column_ID( $item ) 
    {			
		$out = "<a href='".add_query_arg( array('page' => 'crm', 'tab' => 'company', 'action' => 'view', 'id' => $item['id'] ), admin_url('admin.php') )."'>".$item['id']."</a>";	
		foreach( $item['items'] as $id )
		{				
			$out .= "<hr size='1' width='100%'><a href='".add_query_arg( array('page' => 'crm', 'tab' => 'company', 'action' => 'view', 'id' => $id ), admin_url('admin.php') )."'>$id</a>";	
			$out .= "<input type='hidden' name='company_duplicate[".$item['id']."][]' value = '$id' />"	;	
		}	
		echo $out;
	}	
	
	function column_company( $item ) 
    {		
		$count = 0;	
		$out = '';
		foreach( $item['items'] as $id )
		{			
			if ( $this->results[$id]->name != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;<i><a href="'.esc_url( admin_url('admin.php?page=crm&tab=company&action=view&id=').$this->results[$id]->id ).'" target="_blank">'.$this->results[$id]->name.'</a></i>';			
		}		
		echo '<i><a href="'.esc_url( admin_url('admin.php?page=crm&tab=company&action=view&id=').$item['id'] ).'" target="_blank">'.$this->results[$item['id']]->name.'</a></i>'.$this->get_message( $count ).$out;
	}	

	function column_email( $item ) 
    {		
		$count = 0;	
		$out = '';		
		foreach( $item['items'] as $id )
		{
			if ( $this->results[$id]->email != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;'.$this->results[$id]->email;			
		}		
		echo $this->results[$item['id']]->email.$this->get_message( $count ).$out;	
	}	
	
	function column_phone( $item ) 
    {		
		$count = 0;	
		$out = '';
		foreach( $item['items'] as $id )
		{
			if ( $this->results[$id]->phone != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;'.$this->results[$id]->phone;			
		}	
		echo $this->results[$item['id']]->phone.$this->get_message( $count ).$out;	
	}
	
	private function get_message( $count ) 
    {
		return $count>0?' '.'<b>'.sprintf( _n('Найдено %s совпадений','Найдено %s совпадений', $count, 'usam'), $count ).'</b>':'';	
	}
	 	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'id'             => __( 'ID', 'usam' ),		
			'company'        => __( 'Компания', 'usam' ),				
			'email'          => __( 'Почта', 'usam' ),	
			'phone'          => __( 'Телефон', 'usam' ),	
        );		  
        return $columns;
    }	
	
	function prepare_items() 
	{		
		global $wpdb;					
	
		if ( !isset($_REQUEST['phone']) && !isset($_REQUEST['email'])	)			
			return;
				
		$this->where = '';
		if ( isset($_REQUEST['name']) )	
		{
		//	$this->where[] = "CONCAT( company.name, company.firstname ) IN ( SELECT CONCAT( company.lastname, company.firstname ) AS x FROM `".USAM_TABLE_COMPANY."` WHERE company.lastname != '' AND company.firstname != '' GROUP BY x HAVING COUNT( x )>1 )";				
		}	
		if ( isset($_REQUEST['phone']) )	
		{
			$this->where[] = "( com_phone.value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` WHERE customer_type = 'company' AND value != '' GROUP BY value HAVING COUNT( value ) >1 ) )";
		}		
		if ( isset($_REQUEST['email']) )	
		{
			$this->where[] .= "com_email.value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` WHERE customer_type = 'company' AND value != '' GROUP BY value HAVING COUNT( value ) >1 )";
		}	
		$where = implode( ' OR ', $this->where );			
		$this->joins[] = "INNER JOIN ".USAM_TABLE_MEANS_COMMUNICATION." AS com_email ON (com_email.customer_type = 'company' AND com_email.contact_id=company.id AND com_email.type='email')";
		$this->joins[] = "INNER JOIN ".USAM_TABLE_MEANS_COMMUNICATION." AS com_phone ON (com_phone.customer_type = 'company' AND com_phone.contact_id=company.id AND com_phone.type='phone')";
		$joins = implode( ' ', $this->joins );
			
		$sql = "SELECT company.name, company.id, company.date_insert, com_email.value AS email, com_phone.value AS phone FROM ".USAM_TABLE_COMPANY." AS company $joins WHERE $where ORDER BY `company`.`name` LIMIT 100";
	//	$sql = "SELECT * FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE	value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` GROUP BY value HAVING COUNT( value ) >1 ) LIMIT 0 , 30 ";
		$results = $wpdb->get_results( $sql );			
		
		foreach(  $results as $data )
		{
			$this->results[$data->id] = $data;
		}		
		$results = $this->results;
		foreach( $this->results as $key1 => $data1 )
		{
			$items = array();
			unset($results[$key1]);
			foreach( $results as $key2 => $data2 )
			{				
				if ( isset($_REQUEST['name']) )	
				{
					if ( $data1->name == $data2->name )
					{
						$items[] = $data2->id;
						unset($results[$key2]);
						continue;
					}				
				}
				if ( isset($_REQUEST['phone']) )	
				{
					if ( $data1->phone == $data2->phone )
					{
						$items[] = $data2->id;
						unset($results[$key2]);
						continue;
					}				
				}
				if ( isset($_REQUEST['email']) )	
				{
					if ( $data1->email == $data2->email )
					{
						$items[] = $data2->id;
						unset($results[$key2]);
						continue;
					}				
				}
			}
			if ( !empty($items) )
			{					
				$this->items[] = array( 'id' => $data1->id, 'items' => $items);
			}
		} 
		$total_items = 0;	
		$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page ) );
	}
}
?>