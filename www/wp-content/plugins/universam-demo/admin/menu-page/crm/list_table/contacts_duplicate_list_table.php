<?php
class USAM_List_Table_contacts_duplicate extends USAM_List_Table
{	
	private $contacts = array();
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'combine'    => __( 'Объединить', 'usam' ),			
		);
		return $actions;
	}	

	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{		
			$checked_name = isset($_REQUEST['name'])?1:"";
			$checked_phone = isset($_REQUEST['phone'])?1:"";
			$checked_email = isset($_REQUEST['email'])?1:"";
			?>
				<div class="alignleft actions bulkactions">
				<div class = "usam_manage usam_manage-contacts_duplicate">			
					<input type='checkbox' id ="name" name='name' <?php checked( $checked_name ); ?> value = '1' />
					<label for="name"><?php esc_html_e( 'ФИО', 'usam' ); ?></label>
					<input type='checkbox' id ="phone" name='phone' <?php checked( $checked_phone ); ?> value = '1' />
					<label for="phone"><?php esc_html_e( 'Телефон', 'usam' ); ?></label>
					<input type='checkbox' id ="email" name='email' <?php checked( $checked_email ); ?> value = '1' />
					<label for="email"><?php esc_html_e( 'E-mail', 'usam' ); ?></label>
					<?php submit_button( __( 'Найти дубликаты', 'usam' ), 'button-secondary', 'find_duplicates', false ); ?>
				</div>
				</div>
			<?php			
		}			
	}		
		
	function column_ID( $item ) 
    {			
		$out = "<a href='".add_query_arg( array('page' => 'crm', 'tab' => 'contacts', 'action' => 'view', 'id' => $item['id'] ), admin_url('admin.php') )."' target='_blank'>".$item['id']."</a>";
		foreach( $item['items'] as $id )
		{				
			$out .= "<hr size='1' width='100%'><a href='".add_query_arg( array('page' => 'crm', 'tab' => 'contacts', 'action' => 'view', 'id' => $id ), admin_url('admin.php') )."' target='_blank'>$id</a>";;	
			$out .= "<input type='hidden' name='contact_duplicate[".$item['id']."][]' value = '$id' />"	;	
		}			
		echo $out;
	}	
	
	function column_contact( $item ) 
    {		
		$count = 0;	
		$out = '';
		foreach( $item['items'] as $id )
		{			
			if ( $this->contacts[$id]->lastname != '' && $this->contacts[$id]->firstname != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;<i>'.$this->contacts[$id]->lastname.' '.$this->contacts[$id]->firstname.'</i>';			
		}		
		echo '<i>'.$this->contacts[$item['id']]->lastname.' '.$this->contacts[$item['id']]->firstname.'</i>'.$this->get_message( $count ).$out;
	}	

	function column_email( $item ) 
    {		
		$count = 0;	
		$out = '';		
		foreach( $item['items'] as $id )
		{
			if ( $this->contacts[$id]->email != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;'.$this->contacts[$id]->email;			
		}		
		echo $this->contacts[$item['id']]->email.$this->get_message( $count ).$out;	
	}	
	
	function column_phone( $item ) 
    {		
		$count = 0;	
		$out = '';
		foreach( $item['items'] as $id )
		{
			if ( $this->contacts[$id]->phone != '' )
				$count++;
			$out .= '<br><hr size="1" width="100%">&nbsp;&nbsp;&nbsp;&nbsp;'.$this->contacts[$id]->phone;			
		}	
		echo $this->contacts[$item['id']]->phone.$this->get_message( $count ).$out;	
	}
	
	private function get_message( $count ) 
    {
		return $count>0?' '.'<b>'.sprintf( _n('Найдено %s совпадений','Найдено %s совпадений', $count, 'usam'), $count ).'</b>':'';	
	}
	 	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'id'             => __( 'ID', 'usam' ),		
			'contact'        => __( 'Контакт', 'usam' ),				
			'email'          => __( 'Почта', 'usam' ),	
			'phone'          => __( 'Телефон', 'usam' ),	
        );		  
        return $columns;
    }	
	
	function prepare_items() 
	{		
		global $wpdb;					
	
		if ( !isset($_REQUEST['name']) && !isset($_REQUEST['phone']) && !isset($_REQUEST['email'])	)			
			return;
		
		$this->get_standart_query_parent( );
		
		$this->where = '';
		if ( isset($_REQUEST['name']) )	
		{
			$this->where[] = "CONCAT( contact.lastname, contact.firstname ) IN ( SELECT CONCAT( contact.lastname, contact.firstname ) AS x FROM `".USAM_TABLE_CONTACTS."` WHERE contact.lastname != '' AND contact.firstname != '' GROUP BY x HAVING COUNT( x )>1 )";				
		}	
		if ( isset($_REQUEST['phone']) )	
		{
			$this->where[] = "( com_phone.value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` WHERE customer_type = 'contact' AND value != '' GROUP BY value HAVING COUNT( value ) >1 ) )";
		}		
		if ( isset($_REQUEST['email']) )	
		{
			$this->where[] .= "com_email.value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` WHERE customer_type = 'contact' AND value != '' GROUP BY value HAVING COUNT( value ) >1 )";
		}	
		$where = implode( ' OR ', $this->where );			
		$this->joins[] = "INNER JOIN ".USAM_TABLE_MEANS_COMMUNICATION." AS com_email ON (com_email.customer_type = 'contact' AND com_email.contact_id=contact.id AND com_email.type='email')";
		$this->joins[] = "INNER JOIN ".USAM_TABLE_MEANS_COMMUNICATION." AS com_phone ON (com_phone.customer_type = 'contact' AND com_phone.contact_id=contact.id AND com_phone.type='phone')";
		$joins = implode( ' ', $this->joins );
			
		$sql = "SELECT contact.lastname, contact.id, contact.firstname, contact.date_insert, com_email.value AS email, com_phone.value AS phone FROM ".USAM_TABLE_CONTACTS." AS contact $joins WHERE $where ORDER BY `contact`.`lastname` $this->limit";
	//echo $sql;	
	//	$sql = "SELECT * FROM ".USAM_TABLE_MEANS_COMMUNICATION." WHERE	value IN ( SELECT value FROM `".USAM_TABLE_MEANS_COMMUNICATION."` GROUP BY value HAVING COUNT( value ) >1 ) LIMIT 0 , 30 ";
		$contacts = $wpdb->get_results( $sql );		
		foreach(  $contacts as $data )
		{
			$this->contacts[$data->id] = $data;
		}	
	//	print_r($this->contacts);
		$contacts = $this->contacts;
		foreach( $this->contacts as $key1 => $data1 )
		{
			$items = array();
			unset($contacts[$key1]);
			foreach( $contacts as $key2 => $data2 )
			{				
				if ( isset($_REQUEST['name']) )	
				{
					if ( $data1->lastname == $data2->lastname && $data1->firstname == $data2->firstname )
					{
						$items[] = $data2->id;
						unset($contacts[$key2]);
						continue;
					}				
				}
				if ( isset($_REQUEST['phone']) )	
				{
					if ( $data1->phone == $data2->phone )
					{
						$items[] = $data2->id;
						unset($contacts[$key2]);
						continue;
					}				
				}
				if ( isset($_REQUEST['email']) )	
				{
					if ( $data1->email == $data2->email )
					{
						$items[] = $data2->id;
						unset($contacts[$key2]);
						continue;
					}				
				}
			}
			if ( !empty($items) )
			{					
				$this->items[] = array( 'id' => $data1->id, 'items' => $items);
			}
		}
		$total_items = 0;	
		$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page ) );
	}
}
?>