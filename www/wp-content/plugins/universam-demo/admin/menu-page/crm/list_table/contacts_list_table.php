<?php
class USAM_List_Table_contacts extends USAM_List_Table
{	
	protected $order = 'DESC';
	private $my_count = 0;
	private $window = 'my';
	
	function __construct( $args = array() )
	{			
		parent::__construct( $args );	

		require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );	
		
		global $user_ID;
		$query = array( 'manager_id' => $user_ID, 'fields' => 'count', 'number' => 1 );
		$this->my_count = usam_get_contacts( $query );
		
		if ( isset($_GET['window']) )
			$this->window = $_GET['window'];
		elseif ( $this->my_count == 0 )
			$this->window = 'open';		
    }
	
	public function return_post()
	{
		return array( 'window' );
	}

	public function source_dropdown() 
	{	
		$selected = isset( $_REQUEST['source'] ) ? sanitize_title($_REQUEST['source']) : 0;		
		?>	
		<select name="source">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Источник', 'usam'); ?></option>
			<?php 
			$option = get_option('usam_crm_contact_source', array() );
			$contact_sources = maybe_unserialize( $option );
			foreach( $contact_sources as $key => $item )
			{
				?>
				<option value="<?php echo $item['id']; ?>" <?php selected($selected, $item['id'] ); ?> ><?php echo $item['name']; ?></option>
				<?php 
			} ?>
		</select>
		<?php
	}		
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'source' => array() );		
	}
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),			
		);
		return $actions;
	}	
	
	public function get_views() 
	{	
		global $user_ID;
				
		$href = remove_query_arg( array('post_status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );	

		if ( $this->my_count )
		{
			$all_text = sprintf(__( 'Мои <span class="count">(%s)</span>', 'usam' ), number_format_i18n( $this->my_count ) );		
			$href = add_query_arg( 'window', 'my', $href );			
			$class = ( $this->window == 'my' ) ? 'class="current"' : '';
			$views = array(	'window' => sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text ), );
		}		
		$query = array( 'open' => 1, 'fields' => 'count', 'number' => 1 );
		$count = usam_get_contacts( $query );
		
		if ( $count )
		{
			$all_text = sprintf(__( 'Доступные <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $count ) );				
			$href = add_query_arg( 'window', 'open', $href );		
			$class = ( $this->window == 'open' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
			$views['open'] = sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text );
		}
		
		$query = array( 'manager_id' => 0, 'fields' => 'count', 'number' => 1 );
		$count = usam_get_contacts( $query );			
		if ( $count )
		{				
			$all_text = sprintf(__( 'Не занятые <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $count ) );				
			$href = add_query_arg( 'window', 'all', $href );		
			$class = ( $this->window == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
			$views['all'] = sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text );	
		}
		return $views;
	}
	
	function column_affairs( $item ) 
    {					
		$actions = array(
			'send_message'   => $this->add_row_actions( $item->id, 'send_message', __( 'Отправить сообщение', 'usam' ) ),		
			'add_meeting'    => $this->add_row_actions( $item->id, 'add_meeting', __( 'Встреча', 'usam' ) ),	
			'add_phone'      => $this->add_row_actions( $item->id, 'add_phone', __( 'Звонок', 'usam' ) ),	
			'add_reminder'   => $this->add_row_actions( $item->id, 'add_reminder', __( 'Событие', 'usam' ) ),	
		);			
		$this->row_actions_table( usam_get_form_customer_case( $item->id, 'contact' ), $actions );	
	}	
	
	function column_contact( $item ) 
    {
		$name = '<a class="row-title" href="'.esc_url( $url = add_query_arg( array('page' => 'crm','tab' => 'contacts', 'action' => 'view', 'id' => $item->id), admin_url('admin.php') ) ).'"><span id="customer_name">'.$item->lastname.' '.$item->firstname.'</span></a>';
		$actions = $this->standart_row_actions( $item->id );				
		$actions['view'] = $this->add_row_actions( $item->id, 'view', __( 'Посмотреть', 'usam' ) );	
		$actions['email'] = '<a class="usam-email-link" href="'.add_query_arg( array('page' => 'feedback','tab' => 'email', 'contact' => $item->id), admin_url('admin.php')).'">'. __( 'Письма', 'usam' ).'</a>';			
		$this->row_actions_table( $name, $actions );	
	}
	
	function column_contact_source( $item ) 
    {		
		echo usam_get_name_contact_source( $item->contact_source );
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "contact-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
		
	function column_communication( $item ) 
	{
		$communication = usam_get_contact_means_communication( $item->id );		
		
		if ( isset($communication['email']) )
		{
			echo "<div class = 'email'>";
			foreach( $communication['email'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
		if ( isset($communication['phone']) )
		{
			echo "<div class = 'phone'>";
			foreach( $communication['phone'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
	}
	
	function get_sortable_columns()
	{
		$sortable = array(
			'contact'     => array('lastname', false),			
			'status'      => array('status', false),		
			'affairs'     => array('affairs', false),	
			'date'        => array('date_insert', false),				
			);
		return $sortable;
	}
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'contact'        => __( 'Контакт', 'usam' ),		
			'affairs'        => __( 'Дела', 'usam' ),	
			'communication'  => __( 'Связаться', 'usam' ),				
			'contact_source' => __( 'Источник', 'usam' ),	
			'date'           => __( 'Создан', 'usam' ),				
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		global $user_ID;			
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 		
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'search' => $this->search,
			'cache_case' => true,			
		);				
		if ( $this->search == '' )
		{
			$query = array_merge ($query, $this->get_date_for_query() );
			if ( isset($_REQUEST['action']) && $_GET['action'] == 'delete' )
				$query['manager_id'] = array( 0, $user_ID);		
			elseif ( $this->window == 'all' )
				$query['manager_id'] = 0;
			elseif ( $this->window == 'open' )
				$query['open'] = 1;	
			else	
				$query['manager_id'] = $user_ID;				
			
			if ( !empty($_REQUEST['source']) )
				$query['source'] = sanitize_title($_REQUEST['source']);
			
			if ( !empty($this->records) )
				$query['include'] = $this->records;			
		}		
		$_contacts = new USAM_Contacts_Query( $query );
		$this->items = $_contacts->get_results();		
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>