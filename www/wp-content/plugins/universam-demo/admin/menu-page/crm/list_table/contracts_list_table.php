<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/documents_table.php' );
class USAM_List_Table_contracts extends USAM_Documents_Table
{	
	protected $document_type = 'contract';
	
	function column_status( $item ) 
    {
		echo usam_get_status_name_document_contract( $item->status );	
	}	
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',		
			'number'         => __( 'Номер', 'usam' ),				
			'name'           => __( 'Название', 'usam' ),		
			'status'         => __( 'Статус', 'usam' ),				
			'totalprice'     => __( 'Сумма', 'usam' ),		
			'closedate'      => __( 'Срок', 'usam' ),	
			'date'           => __( 'Дата', 'usam' ),	
			'manager'        => __( 'Менеджер', 'usam' ),				
        );		
        return $columns;
    }
}
?>