<?php
class USAM_List_Table_company extends USAM_List_Table
{	
	private $company_groups = array();
	private $emails;
	private $my_count = 0;
	private $window = 'my';
	
	protected $order = 'DESC';
	function __construct( $args = array() )
	{				
		parent::__construct( $args );
		
		$option = get_option('usam_crm_company_group');
		$company_groups = maybe_unserialize( $option );	
		if (!empty($company_groups) )
		{
			foreach( $company_groups as $item )
				$this->company_groups[$item['id']] = $item['name'];
		}				
		
		global $user_ID;
		$query = array( 'manager_id' => $user_ID, 'fields' => 'count', 'number' => 1 );
		$this->my_count = usam_get_companies( $query );	
		
		if ( isset($_GET['window']) )
			$this->window = $_GET['window'];
		elseif ( $this->my_count == 0 )
			$this->window = 'open';
    }
	
	public function return_post()
	{
		return array( 'window' );
	}		
	
	public function group_dropdown() 
	{	
		$selected = isset( $_REQUEST['group'] ) ? sanitize_title($_REQUEST['group']) : 0;		
		?>	
		<select name="group">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Группа', 'usam'); ?></option>
			<?php 
			$option = get_option('usam_crm_company_group', array() );
			$company_groups = maybe_unserialize( $option );	
			foreach( $company_groups as $key => $item )
			{
				?>
				<option value="<?php echo $item['id']; ?>" <?php selected($selected, $item['id'] ); ?> ><?php echo $item['name']; ?></option>
				<?php 
			} ?>
		</select>
		<?php
	}	
	
	
	public function industry_dropdown() 
	{	
		$selected = isset( $_REQUEST['industry'] ) ? sanitize_title($_REQUEST['industry']) : 0;		
		?>	
		<select name="industry">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Сфера деятельности', 'usam'); ?></option>
			<?php	
			$industry = usam_get_companies_industry();
			foreach ( $industry as $id => $name )
			{
				?><option <?php selected( $id, $selected ); ?> value="<?php echo $id; ?>"><?php echo $name; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	
	public function type_dropdown() 
	{	
		$selected = isset( $_REQUEST['type'] ) ? sanitize_title($_REQUEST['type']) : 0;		
		?>	
		<select name="type">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Тип компании', 'usam'); ?></option>
			<?php	
			$types = usam_get_companies_types();
			foreach ( $types as $id => $name )
			{
				?><option <?php selected( $id, $selected ); ?> value="<?php echo $id; ?>"><?php echo $name; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'type' => array(), 'industry' => array(), 'group' => array() );		
	}	
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),			
		);
		return $actions;
	}	
	
	public function get_views() 
	{	
		global $user_ID;
				
		$href = remove_query_arg( array( 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );	
								
		if ( $this->my_count )
		{
			$all_text = sprintf(__( 'Мои <span class="count">(%s)</span>', 'usam' ), number_format_i18n( $this->my_count ) );		
			$href = add_query_arg( 'window', 'my', $href );			
			$class = ( $this->window == 'my' ) ? 'class="current"' : '';
			$views = array(	'window' => sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text ), );
		}
		$query = array( 'open' => 1, 'fields' => 'count', 'number' => 1 );
		$count = usam_get_companies( $query );
		
		if ( $count )
		{
			$all_text = sprintf(__( 'Доступные <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $count ) );				
			$href = add_query_arg( 'window', 'open', $href );		
			$class = ( $this->window == 'open' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
			$views['open'] = sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text );
		}
		
		$query = array( 'manager_id' => 0, 'fields' => 'count', 'number' => 1 );
		$count = usam_get_companies( $query );
		
		if ( $count )
		{
			$all_text = sprintf(__( 'Не занятые <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $count ) );				
			$href = add_query_arg( 'window', 'all', $href );		
			$class = ( $this->window == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
			$views['all'] = sprintf('<a href="%s" %s>%s</a>', esc_url( $href ), $class, $all_text );
		}
		return $views;
	}
	
	function column_name( $item ) 
    {
		$name = '<a class="row-title" href="'.esc_url( $url = add_query_arg( array('page' => 'crm','tab' => 'company', 'action' => 'view', 'id' => $item->id), admin_url('admin.php') ) ).'"><span id="customer_name">'.$item->name.'</span></a>';
		$actions = $this->standart_row_actions( $item->id );				
		$actions['email'] = '<a class="usam-email-link" href="'.add_query_arg( array('page' => 'feedback','tab' => 'email', 'company' => $item->id), admin_url('admin.php')).'">'. __( 'Письма', 'usam' ).'</a>';		
		$this->row_actions_table( $name, $actions );	
	}	

	function column_group( $item ) 
    {
		if ( isset($this->company_groups[$item->group]) )
			echo $this->company_groups[$item->group];	
	}	

	function column_affairs( $item ) 
    {		
		$actions = array(
			'send_message'   => $this->add_row_actions( $item->id, 'send_message', __( 'Отправить сообщение', 'usam' ) ),		
			'add_meeting'    => $this->add_row_actions( $item->id, 'add_meeting', __( 'Встреча', 'usam' ) ),	
			'add_phone'      => $this->add_row_actions( $item->id, 'add_phone', __( 'Звонок', 'usam' ) ),	
			'add_reminder'   => $this->add_row_actions( $item->id, 'add_reminder', __( 'Событие', 'usam' ) ),	
		);			
		$this->row_actions_table( usam_get_form_customer_case( $item->id, 'company' ), $actions );	
	}	
		
	function column_industry( $item ) 
    {
		$industry = array( 
			'it' => __('Информационные технологии','usam'), 
			'telecom' => __('Телекоммуникации и связь','usam'), 
			'manufacture' => __('Производство','usam'),
			'banking' => __('Банковские услуги','usam'),
			'consulting' => __('Консалтинг','usam'),
			'finance' => __('Финансы','usam'),
			'government' => __('Правительство','usam'),
			'delivery' => __('Доставка','usam'),
			'entertainmet' => __('Развлечения','usam'),
			'notprofit' => __('Не для получения прибыли','usam'),
			'other' => __('Другое','usam'),		
		); 		
		if ( !empty($industry[$item->industry]) )
			echo $industry[$item->industry];		
	}
	
	function column_type( $item ) 
    {
		$types = array( 
			'customer' => __('Клиент','usam'), 
			'prospect' => __('Будующий клиент','usam'), 
			'partner' => __('Партнер','usam'),
			'reseller' => __('Реселлер','usam'),
			'competitor' => __('Конкурент','usam'),
			'investor' => __('Инвестор','usam'),
			'integrator' => __('Интегратор','usam'),
			'press' => __('СМИ','usam'),
			'own' => __('Своя','usam'),			
			'other' => __('Другое','usam'),		
		);
		echo $types[$item->type];		
	}
	
	function column_communication( $item ) 
	{
		$communication = usam_get_company_means_communication( $item->id );		
		
		if ( isset($communication['email']) )
		{
			echo "<div class = 'email'>";
			foreach( $communication['email'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
		if ( isset($communication['phone']) )
		{
			echo "<div class = 'phone'>";
			foreach( $communication['phone'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "company-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
	 
	function get_sortable_columns()
	{
		$sortable = array(
			'name'     => array('name', false),		
			'status'      => array('status', false),					
			'date'  => array('date_insert', false),	
			'type'  => array('type', false),
			'industry'  => array('industry', false),
			'group'  => array('group', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'name'           => __( 'Компания', 'usam' ),						
			'affairs'        => __( 'Дела', 'usam' ),	
			'communication'  => __( 'Связаться', 'usam' ),		
			'date'           => __( 'Создана', 'usam' ),			
			'type'           => __( 'Тип компании', 'usam' ),	
			'industry'       => __( 'Сфера деятельности', 'usam' ),	
			'group'          => __( 'Группа', 'usam' ),				
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		global $user_ID;			
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'cache_case' => true,		
		);	
		if ( isset($_REQUEST['action']) && $_GET['action'] == 'delete' )
			$query['manager_id'] = array( 0, $user_ID);		
		elseif ( $this->window == 'all' )
			$query['manager_id'] = 0;
		elseif ( $this->window == 'open' )
			$query['open'] = 1;	
		else	
			$query['manager_id'] = $user_ID;	
			
		if ( $this->search != '' )
		{
			$query['search'] = $this->search;					
		}
		else
		{		
			$query = array_merge ($query, $this->get_date_for_query() );		
			
			if ( !empty($this->records) )
				$query['include'] = $this->records;
			
			if ( !empty($_REQUEST['industry']) )
				$query['industry'] = sanitize_title($_REQUEST['industry']);	
			
			if ( !empty($_REQUEST['type']) )
				$query['type'] = sanitize_title($_REQUEST['type']);	
			
			if ( !empty($_REQUEST['group']) )
				$query['group'] = sanitize_title($_REQUEST['group']);	
		} 
		$_contacts = new USAM_Companies_Query( $query );
		$this->items = $_contacts->get_results();		
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>