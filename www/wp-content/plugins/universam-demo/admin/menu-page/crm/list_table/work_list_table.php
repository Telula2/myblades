<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/my_tasks_list_table.php' );		
class USAM_List_Table_work extends USAM_List_Table_My_Tasks 
{	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		
		$this->type_task = 'work';			
		$this->status = isset($_GET['status']) && is_numeric($_GET['status'])?$_GET['status']:'all';
    }	
		
	function column_user( $item )
	{
		if ( $item->user_id != 0)
		{	
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo usam_get_manager_name( $item->user_id ); ?></span>
			</div>
			<?php 
		}		
	} 	
	
	function column_status( $item ) 
	{		
		echo $this->statuses[$item->status];
	}
	
	function column_time_work( $item )
	{
		if ( !empty($item->start) && !empty($item->end) )
		{
			$time = round((strtotime($item->end) - strtotime($item->start)) / 60);
			if ( $time == 0 )
				_e( 'менее минуты', 'usam' );
			else
				echo $time." ".__("мин","usam");
		}
	} 
	
	public function user_dropdown() 
	{	
		global $user_ID;
		$selected = isset( $_REQUEST['user'] ) ? absint($_REQUEST['user']) : 0;	
		?>	
		<select name="user">			
			<option <?php selected( 0, $selected ); ?> value="0"><?php _e('Моя работа', 'usam'); ?></option>
			<?php	
			$users = get_users( array('meta_key' => 'usam_chief', 'meta_value' => $user_ID ) );
			foreach( $users as $user )
			{
				echo "<option value='$user->ID' ".selected($selected, $user->ID).">{$user->display_name} {$user->user_login})</option>";
			}
			?>				
		</select>
		<?php
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'user' => array() );		
	}
	
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which )
		{					
			echo '<div class="alignleft actions">';	
				$this->standart_button();									
			echo '</div>';
		}
	}   
	
	public function get_sortable_columns() 
	{
		if ( ! $this->sortable )
			return array();
		
		return array(
			'date'       => 'id',
			'user' => 'user',
			'title'   => 'title',		
			'status'     => 'status',		
		);
	}
	
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',
			'user'          => __( 'Менеджер', 'usam' ),					
			'title'         => __( 'Тема', 'usam' ),	
			'description'   => __( 'Описание', 'usam' ),
			'object_type'   => __( 'Объект', 'usam' ),	
			'status'        => __( 'Статус', 'usam' ),	
			'time'          => __( 'Срок', 'usam' ),			
			'time_work'     => __( 'Время', 'usam' ),	
			'calendar_id'   => __( 'Календарь', 'usam' ),					
        );
        return $columns;	
    }	
	
	function prepare_items() 
	{				
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'search' => $this->search, 'orderby' => $this->orderby, 'order' => $this->order );		
			
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;
		
		$args['status'] = $this->status;
	
		if ( isset($_REQUEST['user']) )
		{
			$args['user_id'][] = absint( $_REQUEST['user'] );				
		}
		else
		{
			global $user_ID;
			$users_ids = get_users( array( 'fields' => 'ID', 'meta_key' => 'usam_chief', 'meta_value' => $user_ID ) );				
			$users_ids[] = $user_ID;
			$args['user_id'] = $users_ids;	
		}
		$args['type'] = 'work';			
		$args = array_merge ($args, $this->get_date_for_query() );		
		
		$query = new USAM_Tasks_Query( $args );
		$this->items = $query->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}