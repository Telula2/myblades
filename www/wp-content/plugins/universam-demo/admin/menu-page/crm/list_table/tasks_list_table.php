<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/my_tasks_list_table.php' );		
class USAM_List_Table_Tasks extends USAM_List_Table_My_Tasks 
{	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		
		$this->type_task = 'my';
    }	
}