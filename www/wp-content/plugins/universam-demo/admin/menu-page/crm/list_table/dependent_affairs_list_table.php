<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/my_tasks_list_table.php' );		
class USAM_List_Table_Affairs extends USAM_List_Table_My_Tasks 
{	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		
		$this->type_task = 'affairs';
    }	
	
	function column_user( $item )
	{
		if ( $item->user_id != 0)
		{	
			$user = get_user_by('id', $item->user_id);
			$manager = isset($user->display_name)?"$user->display_name ({$user->user_login})":'';
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo $manager; ?></span>
			</div>
			<?php 
		}		
	} 
	
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',
			'user'          => __( 'Менеджер', 'usam' ),			
			'title'         => __( 'Тема', 'usam' ),	
			'description'   => __( 'Описание', 'usam' ),
			'status'        => __( 'Статус', 'usam' ),	
			'time'          => __( 'Срок', 'usam' ),			
			'calendar_id'   => __( 'Календарь', 'usam' ),					
        );
        return $columns;	
    }	
}