<?php
/*
 * Отображение страницы CRM
 */ 
$default_tabs = array(	
	array( 'id' => 'tasks',       'title' => __( 'Задания', 'usam' ) ),		
	array( 'id' => 'contacts',    'title' => __( 'Контакты', 'usam' ) ),	
	array( 'id' => 'company',     'title' => __( 'Компании', 'usam' ) ),
	array( 'id' => 'suggestions', 'title' => __( 'Предложения', 'usam' ) ),
	array( 'id' => 'invoice',     'title' => __( 'Счета', 'usam' ) ),
	array( 'id' => 'contracts',   'title' => __( 'Договора', 'usam' ) ),	
	array( 'id' => 'price',       'title' => __( 'Прайс-лист', 'usam' ) ),
	array( 'id' => 'work',        'title' => __( 'Работа', 'usam' ) ),
	array( 'id' => 'calendar',    'title' => __( 'Календарь', 'usam' )), 	
);


class USAM_Tab extends USAM_Page_Tab
{			
	protected function localize_script_tab()
	{ 	
		wp_enqueue_script( 'knob' );				
		wp_enqueue_script( 'iframe-transport' );	
		wp_enqueue_script( 'fileupload' );	
		
		return array(
			'add_product_to_document_nonce'  => usam_create_ajax_nonce( 'add_product_to_document' ),
			'add_event_nonce'  => usam_create_ajax_nonce( 'add_event' ),
			'edit_event_nonce'  => usam_create_ajax_nonce( 'edit_event' ),
			'form_customer_case_nonce' => usam_create_ajax_nonce( 'form_customer_case' ),				
			'view_form_tab_nonce'  => usam_create_ajax_nonce( 'view_form_tab' ),	
			'delete_event_file_nonce'  => usam_create_ajax_nonce( 'delete_event_file' ),				
			
			'add_event_action_nonce'  => usam_create_ajax_nonce( 'add_event_action' ),
			'edit_event_action_nonce'  => usam_create_ajax_nonce( 'edit_event_action' ),
			'delete_event_action_nonce'  => usam_create_ajax_nonce( 'delete_event_action' ),				
			'add_participant_nonce'  => usam_create_ajax_nonce( 'add_participant' ),
			'delete_event_participant_nonce'  => usam_create_ajax_nonce( 'delete_event_participant' ),		
			
			'id' => isset($_GET['id'])?$_GET['id']:0,		
			'add_button_text' => __( 'Добавить', 'usam' ),			
		);
	}
	
	function set_event_windows()
	{
		echo usam_get_modal_window( __('Отправить сообщение','usam'), 'quick_response', usam_get_form_send_message() );	
		echo usam_get_modal_window( __('Добавить событие','usam'), 'event_windows', $this->event_windows() );	
	}		
	
	function event_windows( )
	{	
		global $user_ID;	
		
		if ( $this->tab == 'company' )
		{
			$object_type = 'company';
			$title = __('Компания','usam');
		}
		else
		{
			$object_type = 'contact';
			$title = __('Контакт','usam');
		}
		ob_start();	
		?>		
		<div class="usam_popup usam_event_edit">
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_name"><?php echo $title; ?>:</label></span>
				<input type="hidden" name="event_object_id" id ="event_object_id" value=""/>
				<input type="hidden" name="event_object_type" id ="event_object_type" value="<?php echo $object_type; ?>"/>
				<input type="hidden" name="event_type" id ="event_type" value=""/>								
				<div class="usam_txt"></div>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_date"><?php _e('Дата', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner"><?php usam_display_datetime_picker( 'start', mktime(date('H')+1, 0, 0, date('m'), date('d'), date('Y')) ); ?></span>
			</div>					
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_name"><?php _e('Название', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner">						
				<input type="text" class ="event_name" id="event_name" value =""></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_description"><?php _e('Описание', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner"><textarea class ="event_description" id="event_description"></textarea></span>
			</div>		
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_importance"><?php _e('Напомнить', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner"><input type="checkbox" id = "usam_remind" name = "remind" value="1"/><span class ="hide" id ="date_remind"><?php usam_display_datetime_picker( 'date_time', mktime(date('H')+1, 0, 0, date('m'), date('d'), date('Y')) ); ?></span></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_importance"><?php _e('Важность', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner"><input type="checkbox" name="event_importance" id="event_importance" value="1"/></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_description"><?php _e('Календарь', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner">
					<?php 	
					$calendars = usam_get_user_calendars();
					if ( !empty($calendars) )
					{
						?>
						<select id = "event_calendar">
							<?php 		
							foreach( $calendars as $key => $item )
							{	
								?> 
								<option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>								
								<?php 	
							}
							?>
						</select><?php 							
					}
					?>
				</span>
			</div>
			<div class="popButton">
				<button id = "save_action" type="button" class="button-primary button"><?php _e( 'Добавить', 'usam' ); ?></button>				
				<button type="button" class="button" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
			</div>
		</div>		
		<?php 
		$out = ob_get_contents();
		ob_end_clean();		
			
		return $out;
    }	
	
	function process_save_document( $new ) 
	{		
		if ( !empty($_POST['customer_type']) )
		{
			if ( $_POST['customer_type'] == 'company' )
				$new['customer_id'] = !empty($_POST['company'])?$_POST['company']:'';			
			else	
				$new['customer_id'] = !empty($_POST['contact'])?$_POST['contact']:'';	
			$new['customer_type'] = sanitize_title($_POST['customer_type']);		
		}		
		if ( !empty($_POST['description']) )
			$new['description'] = sanitize_textarea_field($_POST['description']);		
		if ( !empty($_POST['type_price']) )
			$new['type_price'] = sanitize_title($_POST['type_price']);	
		if ( !empty($_POST['number']) )
			$new['number'] = sanitize_title($_REQUEST['number']);			
		$new['name'] = sanitize_text_field($_POST['name']);		
		$new['conditions'] = sanitize_textarea_field($_POST['conditions']);		
		$new['notes'] = sanitize_textarea_field($_POST['notes']);	
		if ( isset($_POST['manager_id']) )
			$new['manager_id'] = is_numeric($_POST['manager_id'])?$_POST['manager_id']:0;	
		
		if ( isset($_POST['status']) )
			$new['status'] = $_POST['status'];
		
		if ( isset($_POST['bank_account_id']) )
			$new['bank_account_id'] = $_POST['bank_account_id'];
		
		$products = !empty($_POST['products'])?$_POST['products']:array();
		$new['closedate'] = usam_get_datepicker('closedate');					
		if ( $this->id != null )	
		{										
			$document = new USAM_Document( $this->id );				
			$closedate = $document->get( 'closedate' );	
			$manager_id = $document->get( 'manager_id' );
			
			$document->set( $new );				
			$document->edit_products( $products );			

			$document->edit_products( $products );	
			$products = $document->get_products();	
			if ( isset($_POST['products_ids']) )
			{
				$products_ids = array_map('intval', $_POST['products_ids']);		
				foreach($products as $product)	
				{
					if ( !in_array( $product['id'], $products_ids )	)
						$result_deleted = $document->delete_product( $product['id'] );
				}
			}	
			else
			{					
				foreach($products as $product)	
				{
					$result_deleted = $document->delete_product( $product['id'] );
				}
			}				
			$contacts = $document->get_contacts();	
			if ( isset($_POST['contacts_ids']) )
			{ 				
				$contacts_ids = array_map('intval', $_POST['contacts_ids']);						
				foreach($contacts as $contact)	
				{ 
					if ( !in_array( $contact->id, $contacts_ids )	)
					{					
						$result_deleted = $document->delete_contact( $contact->id );
					}
				}
				foreach($contacts_ids as $customer_id)	
					$result = $document->add_contact( $customer_id );				
			}	
			else
			{					
				foreach($contacts as $contact)	
				{
					$result_deleted = $document->delete_contact( $contact->id );
				}
			}				
		}
		else			
		{				
			$document = new USAM_Document( $new );
			$closedate = '';
			$manager_id = $new['manager_id'];
			$this->id = $this->get('id');
		}			
		$update = $document->save();	
		if ( !empty($new['closedate']) && $closedate != $new['closedate'] && $manager_id )
		{	
			$data = $document->get_data();	
			
			$task['title'] = __('Нужны дальнейшие действия','usam');
			$task['description'] = __('Время рассмотрения коммерческого предложения завершилось','usam');	
			$task['date_time'] = $new['closedate'];		
			usam_insert_system_event( $task,  array( array('object_type' => 'suggestion', 'object_id' => $data['id'] ), array('object_type' => $data['customer_type'], 'object_id' => $data['customer_id'])) );						
		}	
	}
} 