<?php
class USAM_Tab_suggestions extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Коммерческие предложения', 'usam'), 'description' => 'Здесь вы можете посмотреть ваши коммерческие предложения.' );	
		$this->buttons = array( 'new' => __('Добавить', 'usam') ); 
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{			
			case 'delete':				
				foreach ( $this->records as $id )
				{				
					$doc = new USAM_Document( $id );
					$doc->delete();	
				} 
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );		
			break;
			case 'new':
				global $type_price, $user_ID;	
			
				$insert = array( 'name' => '', 'type_price' => $type_price, 'manager_id' => $user_ID, 'type' => 'suggestion' );
				if ( isset($_REQUEST['contact']) )
				{
					$insert['customer_type'] = 'contact';
					$insert['customer_id'] = absint($_REQUEST['contact']);
				}
				elseif ( isset($_REQUEST['company']) )
				{
					$insert['customer_type'] = 'company';
					$insert['customer_id'] = absint($_REQUEST['company']);
				}
				$doc = new USAM_Document( );
				$doc->set( $insert );	
				$doc->save();			
				
				$this->id = $doc->get('id');
				$this->sendback = add_query_arg( array( 'action' => 'edit', 'id' => $this->id ), $this->sendback );					
				wp_redirect( $this->sendback );
				exit;
			break;
			case 'save':				
				$new['type'] = 'suggestion';	
				$this->process_save_document( $new );
			break;
		}		
		
	}	
}
