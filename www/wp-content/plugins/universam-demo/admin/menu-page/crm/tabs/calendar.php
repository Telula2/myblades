<?php
class USAM_Tab_calendar extends USAM_Tab
{	
	protected $year;
	protected $month;
	protected $day;	
	
	private $days_in_month;
	private $days_in_previous_month;
	
	private $windows;
	
	public function __construct()
	{		
		$this->windows = isset($_GET['table'])? $_GET['table']:'calendar';
		
		if ( $this->windows == 'calendars' )
		{
			$this->header = array( 'title' => __('Календари', 'usam'), 'description' => __('Здесь Вы можете Ваш календари.', 'usam') );	
			$this->buttons = array( 'add' => __('Добавить календарь', 'usam') );						
		}
		else
		{
			$this->month = isset($_GET['month']) && (int)$_GET['month']<=12?(int)$_GET['month']:date('n');
			$this->day = isset($_GET['day']) && (int)$_GET['day']<=31?(int)$_GET['day']:date('j');
			$this->year = isset($_GET['year'])?(int)$_GET['year']:date('Y');	
			
			$this->header = array( 'title' => __('Календарь', 'usam'), 'description' => __('Здесь Вы можете создавать задания.', 'usam') );
		}			
	}
	
	protected function load_tab()
	{		
		if ( $this->windows == 'calendars' )
		{
			$this->list_table();
		}
		else
		{
			global $user_ID;
			
			add_action( 'admin_footer', array($this, 'show_event_window') );			
			$this->days_in_month = cal_days_in_month( CAL_GREGORIAN ,$this->month, $this->year );
			if ( $this->month == 1 )
			{
				$previous_month = 12;
				$previous_year = $this->year -1;
			}
			else
			{
				$previous_month = $this->month-1;
				$previous_year = $this->year;
			}
			$this->days_in_previous_month = cal_days_in_month( CAL_GREGORIAN , $previous_month, $previous_year );
		}
	}
	
	public function show_event_window()
	{
		ob_start();		
		?>	
		<div class="usam_popup usam_event_edit">			
			<div class="usam_popup-row">				
				<span class="usam_edit_data">
					<span class="usam_field-label"><label for="add_ed_description"><?php _e('Название', 'usam'); ?>:</label></span>
					<span class="usam_field-val usam_field-title-inner"><input type="text" class ="event_name" id="event_name" value =""></span>					
				</span>
				<h3 class="usam_display_data" id="event_name_display"></h3>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_description"><?php _e('Описание', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_description_display"></span>
				<span class="usam_field-val usam_field-title-inner usam_edit_data"><textarea class ="event_description" id="event_description"></textarea></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_name"><?php _e('Дата начала', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner usam_edit_data"><?php usam_display_datetime_picker( 'start', '' ); ?></span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_date_from_display"></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_name"><?php _e('Срок', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner usam_edit_data"><?php usam_display_datetime_picker( 'end', '' ); ?></span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_date_to_display"></span>
			</div>	
			<div class="usam_popup-row">				
				<span class="usam_field-val usam_field-title-inner usam_edit_data">
				<span class="usam_field-label"><label for="add_ed_name"><?php _e('Напомнить', 'usam'); ?>:</label></span>
				<input type="checkbox" id = "usam_remind" name = "remind" value="1"/><span class ="hide" id ="date_remind"><?php usam_display_datetime_picker( 'date_time', '' ); ?></span>
				</span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_date_remind_display"></span>
			</div>	
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_name"><?php _e('Важность', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner usam_edit_data"><input type="checkbox" id="event_importance" name="task[importance]" value="1"/></span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_importance_display"></span>
			</div>				
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="add_ed_description"><?php _e('Календарь', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner usam_edit_data">
					<?php 	
					$calendars = usam_get_user_calendars();
					if ( !empty($calendars) )
					{
						?>
						<select id = "event_calendar">
							<?php 		
							foreach( $calendars as $key => $item )
							{	
								?> 
								<option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>								
								<?php 	
							}
							?>
						</select><?php 							
					}
					?>
				</span>
				<span class="usam_field-val usam_field-title-inner usam_display_data" id="event_calendar_display"></span>
			</div>
		</div>
		<div id = 'event_add' class="popButton">
			<button id = "save_action" type="button" class="button-primary button"><?php _e( 'Добавить', 'usam' ); ?></button>				
			<button type="button" class="button" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
		</div>
		<div id = 'event_edit' class="popButton">
			<div class="usam_display_data">				
				<button id = "edit_action_view" type="button" class="button-primary button change_field"><?php _e( 'Изменить', 'usam' ); ?></button>
				<button id = "delete_action" type="button" class="button-primary button"><?php _e( 'Удалить', 'usam' ); ?></button>				
				<button type="button" class="button" data-dismiss="modal" aria-hidden="true"><?php _e( 'Закрыть', 'usam' ); ?></button>
			</div>
			<div class="usam_edit_data">
				<button id = "edit_action" type="button" class="button-primary button change_field"><?php _e( 'Сохранить', 'usam' ); ?></button>
				<button type="button" class="button" data-dismiss="modal" aria-hidden="true"><?php _e( 'Закрыть', 'usam' ); ?></button>
			</div>
		</div>
		<input type="hidden" id ="event_id" id="event_id" value ="">
		<?php	
		$html = ob_get_contents();
		ob_end_clean();
	
		echo usam_get_modal_window( __('Добавить задание', 'usam'), 'show_details_event', $html );		
	}	
		
	protected function callback_submit()
	{		
		global $user_ID;	
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;
				
				$option = get_option('usam_calendars');
				$calendars = maybe_unserialize( $option );
				foreach ( $this->records as $key => $id )
				{						
					if ( $calendars[$id]['user_id'] == $user_ID )
					{
						unset($calendars[$id]);					
						$i++;
					}
				}	
				update_option('usam_calendars', serialize($calendars) );					
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'save':			
				if ( !empty($_POST['calendar']) )
				{		
					global $user_ID;
					$new = stripslashes_deep($_POST['calendar']);		
					$new['name'] =  sanitize_text_field(stripcslashes($_POST['name']));
					if ( $this->id != null )	
					{				
						$this->data = usam_get_data($this->id, 'usam_calendars');
						
						if ( $this->data['user_id'] !== $user_ID )
							return;	
						
						usam_edit_data( $new, $this->id, 'usam_calendars' );								
					}
					else			
					{								
						$new['user_id'] = $user_ID;				
						$this->id = usam_add_data( $new, 'usam_calendars' );		
					}
				}
			break;
		}		
	}

	public function controller_month() 
	{			
		$back_day = mktime( 0,0,0, $this->month-1, 1, $this->year);
		$back_link = add_query_arg( array('day' => date('d', $back_day), 'month' => date('m', $back_day), 'year' => date('Y', $back_day)), $this->sendback );	
		
		$next_day = mktime( 0,0,0, $this->month+1, 1, $this->year);
		$next_link = add_query_arg( array('day' => date('d', $next_day), 'month' => date('m', $next_day), 'year' => date('Y', $next_day)), $this->sendback );
		?>
		<div id='tab-month' class='tab'>
			<div class='views_interval'>
				<a href="<?php echo $back_link; ?>#calendar_tab-tab-month" class="usam_month_back" title="<?php _e( 'Предыдущий месяц', 'usam' ); ?>">&larr;</a>
				<?php echo date_i18n('F Y', mktime( 0,0,0, $this->month, 1, $this->year)); ?>				
				<a href="<?php echo $next_link; ?>#calendar_tab-tab-month" class="usam_next_month" title="<?php _e( 'Следующий месяц', 'usam' ); ?>">&rarr;</a>
			</div>
			<div class="tab_calendar_title"><?php echo date_i18n('F Y', mktime( 0,0,0, $this->month, $this->day, $this->year)); ?></div>
			<table class="usam_table_month_calendar usam_table_calendar">
				<tbody>
					<tr class="usam_days-title">
						<td>		
							<div id="days_title" class="usam_month-title" style="visibility: visible;">
								<b id="mo" title="<?php _e( 'Понедельник', 'usam' ); ?>"><i>Пн</i></b>
								<b id="tu" title="<?php _e( 'Вторник', 'usam' ); ?>"><i>Вт</i></b>
								<b id="we" title="<?php _e( 'Среда', 'usam' ); ?>"><i>Ср</i></b>
								<b id="th" title="<?php _e( 'Четверг', 'usam' ); ?>"><i>Чт</i></b>
								<b id="fr" title="<?php _e( 'Пятница', 'usam' ); ?>"><i>Пт</i></b>
								<b id="sa" title="<?php _e( 'Суббота', 'usam' ); ?>"><i>Сб</i></b>
								<b id="su" title="<?php _e( 'Воскресенье', 'usam' ); ?>"><i>Вс</i></b>
							</div>
						</td>
					</tr>
					<tr>
						<td class="usam_days-grid-td"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php 
	}	
	
	
	/* НЕДЕЛЬНЫЙ ГРАФИК*/	
	public function controller_week() 
	{				
		$w = date('w', mktime( 0,0,0, $this->month, $this->day, $this->year));
		if ( $w == 0 )
			$day_from = $this->day - 6;	
		else
			$day_from = $this->day - $w+1;	
		
		$date_from = mktime( 0,0,0, $this->month, $day_from, $this->year);		
		$back_day = mktime( 0,0,0, $this->month, $day_from-7, $this->year);
		$back_link = add_query_arg( array('day' => date('d', $back_day), 'month' => date('m', $back_day), 'year' => date('Y', $back_day)), admin_url('admin.php?page=tasks&tab=calendar') );	
		
		$date_to = mktime( 0,0,0, $this->month, $day_from+6, $this->year);		
		$next_day = mktime( 0,0,0, $this->month, $day_from+7, $this->year);
		$next_link = add_query_arg( array('day' => date('d', $next_day), 'month' => date('m', $next_day), 'year' => date('Y', $next_day)), admin_url('admin.php?page=tasks&tab=calendar') );
		?>
		<div id='tab-week' class='tab'>
			<div class='views_interval'>
				<a href="<?php echo $back_link; ?>" class="usam_month_back" title="<?php _e( 'Предыдущая неделя', 'usam' ); ?>">&larr;</a>
				<?php echo date_i18n('d F', $date_from ); ?> - 			
				<?php echo date_i18n('d F', $date_to ); ?>						
				<a href="<?php echo $next_link; ?>" class="usam_next_month" title="<?php _e( 'Следующая неделя', 'usam' ); ?>">&rarr;</a>
			</div>
			<div class="tab_calendar_title"><?php echo sprintf( __('%s неделя %s года','usam'), date_i18n('W', mktime( 0,0,0, $this->month, $this->day, $this->year)), $this->year ); ?></div>
			<table class="usam_table_week_calendar usam_table_calendar">
				<tbody>					
					<tr class="usam_days-title">						
						<td>		
							<div id="days_title" class="usam_month-title" style="visibility: visible;">
								<b id="mo" title="<?php _e( 'Понедельник', 'usam' ); ?>"><i>Пн</i></b>							
								<b id="tu" title="<?php _e( 'Вторник', 'usam' ); ?>"><i>Вт</i></b>
								<b id="we" title="<?php _e( 'Среда', 'usam' ); ?>"><i>Ср</i></b>
								<b id="th" title="<?php _e( 'Четверг', 'usam' ); ?>"><i>Чт</i></b>
								<b id="fr" title="<?php _e( 'Пятница', 'usam' ); ?>"><i>Пт</i></b>
								<b id="sa" title="<?php _e( 'Суббота', 'usam' ); ?>"><i>Сб</i></b>
								<b id="su" title="<?php _e( 'Воскресенье', 'usam' ); ?>"><i>Вс</i></b>
							</div>
						</td>
					</tr>
					<tr>
						<td class="usam_days-grid-td"></td>
					</tr>
				</tbody>
			</table>
		</div>		
		<?php 
	}
	
	public function controller_day() 
	{				
		$back_day = mktime( 0,0,0, $this->month, $this->day-1, $this->year);
		$back_day_link = add_query_arg( array('day' => date('d', $back_day), 'month' => date('m', $back_day), 'year' => date('Y', $back_day)), $this->sendback );	
		
		$next_day = mktime( 0,0,0, $this->month, $this->day+1, $this->year);
		$next_day_link = add_query_arg( array('day' => date('d', $next_day), 'month' => date('m', $next_day), 'year' => date('Y', $next_day)), $this->sendback );
		?>
		<div id='tab-day' class='tab'>
			<div class='views_interval'>
				<a href="<?php echo $back_day_link; ?>#calendar_tab-tab-day" class="usam_month_back" title="<?php _e( 'Предыдущий месяц', 'usam' ); ?>">&larr;</a>
				<?php echo date_i18n('l, d F Y', mktime( 0,0,0, $this->month, $this->day, $this->year)); ?>				
				<a href="<?php echo $next_day_link; ?>#calendar_tab-tab-day" class="usam_next_month" title="<?php _e( 'Следующий месяц', 'usam' ); ?>">&rarr;</a>
			</div>	
			<div class="tab_calendar_title"><?php echo date_i18n('l, d', mktime( 0,0,0, $this->month, $this->day, $this->year)); ?></div>
			<table class="usam_table_day_calendar usam_table_calendar">
				<tbody>					
					<tr><td class="usam_days-grid-td"></td></tr>
				</tbody>
			</table>
		</div>
		<?php 	
	}
	
	function display() 
	{
		if ( $this->windows == 'calendars' )
		{
			$this->list_table->display_table();
		}
		else
		{		
			?>		
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-2">
					<div id = 'post-body-content'>				
						<?php $this->display_left(); ?>
					</div>	
					<div id ="postbox-container-1" class = 'postbox-container'>				
						<div class = 'menu_fixed_right'>
							<?php						
							$this->display_right(); 
							?>
						</div>
					</div>						
				</div>	
			</div>			
			<?php
		}
	}	
	
	public function display_left() 
	{		
		?>			
		<div id='calendar_tab' class = "calendar_tab">
			<div class='header_tab'>
				<ul>
					<li id = 'tab-month' class='tab'><a href=''><?php _e( 'Месяц', 'usam' ); ?></a></li>
					<li id = 'tab-week' class='tab'><a href=''><?php _e( 'Неделя', 'usam' ); ?></a></li>
					<li id = 'tab-day' class='tab'><a href=''><?php _e( 'День', 'usam' ); ?></a></li>
				</ul>
			</div>
			<div class='countent_tabs'>
				<?php 						
				$this->controller_month();
				$this->controller_week();
				$this->controller_day();
				?>
			</div>
		</div>		
		<?php 	
	}	
	
	public function display_right() 
	{
		$url_manage_calendars = add_query_arg( array( 'table' => 'calendars' ) );
		$url_add_calendar = add_query_arg( array( 'action' => 'add' ), $url_manage_calendars );
		?>		
		<div class = "categorydiv calendars_box">						
			<div id='calendars' class = "tabs-panel">			
				<h4><a href="<?php echo $url_manage_calendars; ?>" class="manage_calendars" title="<?php _e( 'Управление календарями', 'usam' ); ?>"><?php _e( 'Ваши календари', 'usam' ); ?></a><a href="<?php echo $url_add_calendar; ?>" class="add_calendar" title="<?php _e( 'Добавить календарь', 'usam' ); ?>"><?php _e( 'Добавить', 'usam' ); ?></a></h4>
				<ul id="usam-brandschecklist" class="categorychecklist form-no-clear">
					<?php 
					$calendars = usam_get_user_calendars();					
					$user_calendars = usam_get_user_select_calendars();
					if ( !empty($calendars) )
						foreach( $calendars as $key => $item )
						{	
							?> 
							<li>
								<label class="selectit">
									<input id="calendar-<?php echo $item['id']; ?>" type="checkbox" name="calendar[]" value="<?php echo $item['id']; ?>"
									<?php echo ( in_array($item['id'],$user_calendars)?'checked="checked"':''); ?>>
									&nbsp;<span class="name"><?php echo $item['name']; ?></span>
								</label>          
							</li>
							<?php 	
						}		
					?>
				</ul>
			</div>
		</div>	
		<div class = "categorydiv calendars_box">						
			<div id='calendars' class = "tabs-panel">			
				<h4><?php _e( 'Системные календари', 'usam' ); ?></h4>
				<ul id="usam-brandschecklist" class="categorychecklist form-no-clear">
					<?php 				
					$system_calendars = usam_get_system_calendars();					
					$user_calendars = usam_get_user_select_calendars();
					if ( !empty($system_calendars) )
						foreach( $system_calendars as $key => $item )
						{	
							?> 
							<li>
								<label class="selectit">
									<input id="calendar-<?php echo $item['id']; ?>" type="checkbox" name="calendar[]" value="<?php echo $item['id']; ?>"
									<?php echo ( in_array($item['id'],$user_calendars)?'checked="checked"':''); ?>>
									&nbsp;<span class="name"><?php echo $item['name']; ?></span>
								</label>          
							</li>
							<?php 	
						}		
					?>
				</ul>
			</div>
		</div>	
		<?php 	
	}
}