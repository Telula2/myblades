<?php
class USAM_Tab_tasks extends USAM_Tab
{		
	protected function load_tab()
	{		
		$this->header = array( 'title' => __('Задания', 'usam'), 'description' => __('Здесь Вы можете создавать задания, назначать задания сотрудникам и просматривать выполнение.', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить задание', 'usam'), 'assigned_task' => __('Назначить задание', 'usam') );	
		$this->list_table();
	}
		
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;
				foreach ( $this->records as $key => $id )
				{						
					if ( usam_delete_event( $id ) >= 1 )
						$i++;
				}				
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;
			break;		
			case 'save':
				global $user_ID; 
				if( !empty($_REQUEST['name']) ) 
				{					
					$task['title'] = sanitize_text_field(stripcslashes($_REQUEST['name']));	
					$task['description'] = sanitize_textarea_field( stripcslashes($_REQUEST['description']));										
					if ( isset($_REQUEST['importance']) )
						$task['importance'] = $_REQUEST['importance']?1:0;	
					
					if ( isset($_REQUEST['status']) )
						$task['status'] = absint($_REQUEST['status']);	
					
					if ( isset($_REQUEST['calendar']) )
						$task['calendar'] = absint($_REQUEST['calendar']);	
					
					if ( isset($_REQUEST['color']) )
						$task['color'] = sanitize_text_field(stripcslashes($_REQUEST['color']));
									
					if ( $this->id )
					{
						$_event = new USAM_Event( $this->id );
						
						if( !isset($_REQUEST['screen']) || $_REQUEST['screen'] != 'assigned_task' )
						{							
							if ( !empty($_REQUEST['remind']) )
							{
								$task['date_time']  = usam_get_datepicker('date_time');
								$task['reminder'] = 1;
							}
							else
							{
								$task['reminder'] = 0;
								$task['date_time']  = null;
							}
						}
						$type = $_event->get('type');		
						$userid = $_event->get('user_id');			
						if ( $type == 'task' || $userid == $user_ID && $type == 'assigned_task' )		
						{ 
							$task['start']  = usam_get_datepicker('start');					
							$task['end']  = usam_get_datepicker('end');	
						}	
						$_event->set( $task );	

						if ( isset($_REQUEST['object_id']) && isset($_REQUEST['object_type']) )
						{
							$object_ids = stripslashes_deep($_REQUEST['object_id']);						
							$object_types = stripslashes_deep($_REQUEST['object_type']);								
							$save_objects = array();
							foreach ( $object_ids as $key => $object_id )
							{	
								$object_id = absint($object_id);
								if ( isset($object_types[$key]) )
								{
									$object_type = sanitize_title($object_types[$key]);	
									$save_objects[$object_id] = $object_type;
								}
							}												
						}
						$objects = usam_get_event_objects_all( $this->id );
						foreach ( $objects as $object )
						{	
							if ( isset($save_objects[$object->object_id]) && $save_objects[$object->object_id] == $object->object_type )
							{
								
							}
							else
								usam_delete_event_object( (array)$object );
						}	
					}
					else
					{						
						$task['start']  = usam_get_datepicker('start');					
						$task['end']  = usam_get_datepicker('end');	
						if( isset($_REQUEST['screen']) && $_REQUEST['screen'] == 'assigned_task' )
						{							
							$task['type'] = 'assigned_task';							
						}
						else
						{
							$task['type'] = 'task';								
						}
						$_event = new USAM_Event( $task );
					}			
					$_event->save();
					
					$this->id = $_event->get('id');					
					if ( $_event->get('type') == 'assigned_task' )
					{
						$this->sendback = add_query_arg( array( 'action' => 'assigned_task' ), $this->sendback );	
					}
				}				
			break;
		}		
	}
	
	public function get_message_error()
	{			
		$message = '';
		if( isset($_REQUEST['error']) && $_REQUEST['error'] == 1 )		
			$message = __( 'Вы не можете редактировать это задание', 'usam' );		
		
		return $message;
	} 
}