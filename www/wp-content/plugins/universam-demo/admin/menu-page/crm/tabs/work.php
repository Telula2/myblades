<?php
class USAM_Tab_work extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Работа менеджера', 'usam'), 'description' => __('Здесь вы можете посмотреть, что вы или ваши подчиненные делали за день.', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}