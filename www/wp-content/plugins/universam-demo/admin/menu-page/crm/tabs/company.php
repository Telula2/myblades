<?php
class USAM_Tab_company extends USAM_Tab
{	
	private $data_filter;
	
	public function __construct()	
	{
		if ( !empty($_GET['table']) )
		{
			$this->header = array( 'title' => __('Поиск дубликатов', 'usam'), 'description' => 'Здесь Вы можете удалить дубликаты ваших компаний.' );
		}
		else
		{
			$this->header = array( 'title' => __('Компании', 'usam'), 'description' => 'Здесь Вы можете работать с вашими компаниями.' );
			$this->buttons = array( 'add' => __('Добавить', 'usam'), 'import' => __('Импорт', 'usam'), 'duplicate' => __('Найти дубликаты', 'usam') );
			add_action( 'admin_footer', array(&$this, 'set_event_windows') );	
		}
	}
	
	protected function load_tab()
	{		
		$this->list_table();
	}
	
	public function array_filter_communication( $data )
	{		
		return ( $data['value'] == $this->data_filter );
	}
	
	protected function callback_submit()
	{ 
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;
				foreach ( $this->records as $key => $value )
				{				
					$company = new USAM_Company( $value );
					$company->delete();					
					$i++;
				}
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );		
			break;
			case 'duplicate':			
				$this->sendback = add_query_arg( array( 'table' => 'contacts_duplicate' ), $this->sendback );
			break;
			case 'combine': 
				$i = 0;		 				
				foreach ( $this->records as $key => $id )
				{						
					if ( isset($_GET['company_duplicate'][$id]) )
					{						
						$contact = new USAM_Company( $id );		
						$contact_data = $contact->get_data();						
						foreach ( $_GET['company_duplicate'][$id] as $duplicat_id )
						{
							$duplicat_contact = new USAM_Company( $duplicat_id );		
							$duplicat_contact_data = $duplicat_contact->get_data();
							if ( empty($duplicat_contact_data) )
								continue;				
							
							foreach ( $duplicat_contact_data as $key => $data )
							{
								if ( empty($contact_data[$key]) )
									$contact_data[$key] = $data;
							}
							$communications2 = usam_get_company_means_communication( $duplicat_id );
							foreach ( $communications2 as $type => $communications )
							{									
								if ( !empty($communications1[$type]) )
								{
									foreach ( $communications as $communication )
									{										
										$this->data_filter = $communication['value'];											
										if ( !array_filter( $communications1[$type], array($this, 'array_filter_communication') ) )
										{
											$data = array( 'value' => $communication['value'], 'value_type' => $communication['value_type'], 'type' => $type, 'contact_id' => $id, 'customer_type' => 'company' );
											$communication_id = usam_insert_communication( $data );										
										}									
									}
								}		
								else
								{
									foreach ( $communications as $communication )
									{
										$data = array( 'value' => $communication['value'], 'value_type' => $communication['value_type'], 'type' => $type, 'contact_id' => $id, 'customer_type' => 'company' );
										$communication_id = usam_insert_communication( $data );
									}
								}
							}
							$duplicat_contact->delete( );	
						}	
						$contact->set( $contact_data );							
						$contact->save( );
					}									
					$i++;
				} 
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );
			break;
			case 'save':					
				if ( !empty($_POST['company']) )
				{		
					$new_company = stripslashes_deep($_POST['company']);			
					$new_company['name'] = sanitize_text_field($_POST['name']);	
					$new_company['description'] = sanitize_textarea_field($_POST['description']);	
					$new_company['logo'] = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;			
					
					$new_communication = !empty($_POST['new_communication'])?stripslashes_deep($_POST['new_communication']):array();			
					$communications = !empty($_POST['communication'])?stripslashes_deep($_POST['communication']):array();	
					$fields = !empty($_POST['fields'])?stripslashes_deep($_POST['fields']):array();		
					$acc = !empty($_POST['acc'])?stripslashes_deep($_POST['acc']):array();		
					$acc_ids = !empty($_POST['acc_ids'])?stripslashes_deep($_POST['acc_ids']):array();				
					
					$new_acc = array();					
					
					foreach ($acc as $colum => $data)
					{
						if ( $colum == 0 )
						{ 
							$new_acc[] = $data;
						}
					}						
					if ( $this->id != null )	
					{								
						$_company = new USAM_Company( $this->id );	
						$bank_accounts = $_company->get_bank_accounts();
						
						$update_ids = array();
						foreach ($communications as $type_key => $array )
						{
							foreach ($array['value'] as $communication_id => $value)
							{
								$value = trim($value);
								if ( $value != '' )
								{
									$update_ids[] = $communication_id;
									$communication = array( 'value' => $value, 'value_type' => $array['type'][$communication_id] );							
									usam_update_communication($communication_id, $communication);
								}
							}					
						}		
						global $wpdb;
								
						$communications_id = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE contact_id = '%d' AND customer_type='company'", $this->id ) );
						$result = array_diff($communications_id, $update_ids);		

						if ( !empty($result) )
						{
							foreach ($result as $id )
							{
								$args = array( 'id' => $id, 'customer_type' => 'company' );			
								usam_delete_means_communication( $args );
							}
						}									
						$new_company['id'] = $this->id;
						$_company->set( $new_company );						
						foreach ($acc as $id => $data )
						{							
							$_company->update_bank_accounts( $id, $data );				
						}
						foreach ($bank_accounts as $data )
						{
							if ( !in_array( $data->id, $acc_ids ) )								
								$_company->delete_bank_accounts( $data->id );		
						}
					}
					else			
					{							
						$_company = new USAM_Company( $new_company );
					}			
					$update = $_company->save();	
					$this->id = $_company->get('id');
					$_company->update_meta( $fields );			
					foreach ($new_acc as $data )
					{		
						$_company->insert_bank_accounts( $data );	
					} 
					foreach ($new_communication as $type_key => $array )
					{ 
						foreach ($array['value'] as $communication_id => $item)
						{						
							$data = array( 'contact_id' => $this->id, 'customer_type' => 'company', 'type' => $type_key, 'value' => $item, 'value_type' => $array['type'][$communication_id] );	
							usam_insert_communication( $data );						
						}					
					}					
				}				
			break; 
			case 'start_import':	
				$default_fields = array( 'type', 'group', 'manager', 'contactlocation', 'contactaddress', 'contactpostcode', 'contactoffice', 'legallocation', 'legaladdress', 'legalpostcode', 'company_name', 'full_company_name', 'inn', 'ppc', 'ogrn', 'date_registration', 'okpo', 'oktmo', 'gm', 'accountant', 'bank_name', 'bank_bic', 'bank_number', 'bank_ca', 'bank_currency', 'bank_address', 'bank_swift', 'bank_note' );
				
				
				require_once( USAM_FILE_PATH . '/includes/data_exchange/import.class.php'         );
				
				$companys = array();
				if( !empty($_FILES['import_file']) && $_FILES['import_file']['name'] != '' )
				{		
					$import = new USAM_Import();
					$companys = $import->import_file( $default_fields, 'import_file' );
				}		
				elseif( !empty($_POST['import_text']) )
				{				
					$import = new USAM_Import();
					$companys = $import->import_text( $default_fields, $_POST['import_text'] );			
				} 
				if ( !empty($companys) )
				{
					ini_set("max_execution_time", "3660");
					foreach ( $companys as $key => $company )
					{			
						$new_company = array();
						$new_company_fields = array();
						$new_company_acc = array();
						
						if ( isset($company['email']) )
							$new_company['email']['new'] = array( 'value' => $company['email'], 'value_type' => 'private' );
						
						if ( isset($company['phone']) )
							$new_company['phone']['new'] = array( 'value' => $company['phone'], 'value_type' => 'private' );
						
						if ( isset($company['site']) )
							$new_company['site']['new'] = array( 'value' => $company['site'], 'value_type' => 'private' );
						
						if ( isset($company['group']) )
							$new_company['group'] = (int)$company['group'];
						
						if ( isset($company['type']) )
							$new_company['type'] = $company['type'];
						else
							$new_company['type'] = 'customer';
						
						if ( isset($company['company_name']) )
							$new_company['name'] = $company['company_name'];				
						elseif ( isset($company['full_company_name']) )
							$new_company['name'] = $company['full_company_name'];
							
						if ( isset($company['manager']) && is_numeric($company['manager']) )
							$new_company['manager_id'] = $company['manager'];
						
						if ( isset($company['contactlocation']) )
							$new_company_fields['contactlocation'] = $company['contactlocation'];						
						
						if ( isset($company['contactaddress']) )
							$new_company_fields['contactaddress'] = $company['contactaddress'];
						
						if ( isset($company['contactpostcode']) )
							$new_company_fields['contactpostcode'] = $company['contactpostcode'];
						
						if ( isset($company['contactoffice']) )
							$new_company_fields['contactoffice'] = $company['contactoffice'];
						
						if ( isset($company['legallocation']) )
							$new_company_fields['legallocation'] = $company['legallocation'];
						
						if ( isset($company['legaladdress']) )
							$new_company_fields['legaladdress'] = $company['legaladdress'];
						
						if ( isset($company['legalpostcode']) )
							$new_company_fields['legalpostcode'] = $company['legalpostcode'];
						
						if ( isset($company['company_name']) )
							$new_company_fields['company_name'] = $company['company_name'];
						
						if ( isset($company['full_company_name']) )
							$new_company_fields['full_company_name'] = $company['full_company_name'];
						
						if ( isset($company['inn']) )
							$new_company_fields['inn'] = $company['inn'];
						
						if ( isset($company['ppc']) )
							$new_company_fields['ppc'] = $company['ppc'];				
						
						if ( isset($company['ogrn']) )
							$new_company_fields['ogrn'] = $company['ogrn'];
						
						$date_format = get_option( 'date_format', 'Y/m/d' );				
						if ( isset($company['date_registration']) )
							$new_company_fields['date_registration'] = date( $date_format, strtotime( $company['date_registration'] ));
						
						if ( isset($company['okpo']) )
							$new_company_fields['okpo'] = $company['okpo'];				
						
						if ( isset($company['oktmo']) )
							$new_company_fields['oktmo'] = $company['oktmo'];
						
						if ( isset($company['gm']) )
							$new_company_fields['gm'] = $company['gm'];
						
						if ( isset($company['accountant']) )
							$new_company_fields['accountant'] = $company['accountant'];
						
						if ( isset($company['bank_name']) )
							$new_company_acc[0]['name'] = $company['bank_name'];
						
						if ( isset($company['bank_bic']) )
							$new_company_acc[0]['bic'] = $company['bank_bic'];
						
						if ( isset($company['bank_number']) )
							$new_company_acc[0]['number'] = $company['bank_number'];
						
						if ( isset($company['bank_ca']) )
							$new_company_acc[0]['bank_ca'] = $company['bank_ca'];
						
						if ( isset($company['bank_currency']) )
							$new_company_acc[0]['currency'] = $company['bank_currency'];
						
						if ( isset($company['bank_address']) )
							$new_company_acc[0]['address'] = $company['bank_address'];
						
						if ( isset($company['bank_swift']) )
							$new_company_acc[0]['swift'] = $company['bank_swift'];
						
						if ( isset($company['bank_note']) )
							$new_company_acc[0]['note'] = $company['bank_note'];
					
						if ( !empty($new_company['name']) )
						{
							$_company = new USAM_Company( $new_company );			
							$update = $_company->save();	
							
							$this->id = $_company->get('id');
						
							$_company->update_meta( $new_company_fields );		
							$_company->insert_bank_accounts( $new_company_acc );
						}
					}	
					$this->sendback = add_query_arg( 'import' , count($companys) );			
					$this->redirect = true;
				}
			break;
		}		
	}
	
	protected function get_message()
	{		
		$message = '';
		if( isset($_REQUEST['import']) )
		{			
			$import = absint( $_REQUEST['import'] );	
			$message = sprintf( _n( '%s компании загруженно.', '%s компаний загруженно.', $import, 'usam' ), $import );					
		}	
		return $message;
	} 
}
?>