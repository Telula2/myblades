<?php
class USAM_Tab_contacts extends USAM_Tab
{	
	private $data_filter;
	
	public function __construct()	
	{
		if ( !empty($_GET['table']) )
		{
			$this->header = array( 'title' => __('Поиск дубликатов', 'usam'), 'description' =>  __('Здесь Вы можете удалить дубликаты ваших контактов.', 'usam') );			
		}
		else
		{
			$this->header = array( 'title' => __('Контакты', 'usam'), 'description' => __('Здесь Вы можете работать с вашими контактами.', 'usam') );
			$this->buttons = array( 'add' => __('Добавить', 'usam'), 'import' => __('Импорт', 'usam'), 'duplicate' => __('Найти дубликаты', 'usam') );
			add_action( 'admin_footer', array(&$this, 'set_event_windows') );	
		}		
	}
	
	protected function load_tab()
	{		
		$this->list_table();
	}
	
	public function array_filter_communication( $data )
	{		
		return ( $data['value'] == $this->data_filter );
	}
	
	protected function callback_submit()
	{  
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;
				foreach ( $this->records as $id )
				{				
					usam_delete_contact( $id );					
					$i++;
				}
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );		
			break;
			case 'duplicate':			
				$this->sendback = add_query_arg( array( 'table' => 'contacts_duplicate' ), $this->sendback );
			break;
			case 'combine':
				$i = 0;		 				
				foreach ( $this->records as $contact_id )
				{							
					if ( isset($_GET['contact_duplicate'][$contact_id]) )
					{						
						$contact = new USAM_Contact( $contact_id );		
						$contact_data = $contact->get_data();	
						$communications1 = usam_get_contact_means_communication( $contact_id );						
						foreach ( $_GET['contact_duplicate'][$contact_id] as $duplicat_id )
						{
							$duplicat_contact = new USAM_Contact( $duplicat_id );		
							$duplicat_contact_data = $duplicat_contact->get_data();
							foreach ( $duplicat_contact_data as $key => $data )
							{
								if ( empty($contact_data[$key]) )
									$contact_data[$key] = $data;
							}
							$communications2 = usam_get_contact_means_communication( $duplicat_id );
							foreach ( $communications2 as $type => $communications )
							{									
								if ( !empty($communications1[$type]) )
								{
									foreach ( $communications as $communication )
									{										
										$this->data_filter = $communication['value'];											
										if ( !array_filter( $communications1[$type], array($this, 'array_filter_communication') ) )
										{
											$data = array( 'value' => $communication['value'], 'value_type' => $communication['value_type'], 'type' => $type, 'contact_id' => $contact_id, 'customer_type' => 'contact' );
											$communication_id = usam_insert_communication( $data );										
										}									
									}
								}		
								else
								{
									foreach ( $communications as $communication )
									{
										$data = array( 'value' => $communication['value'], 'value_type' => $communication['value_type'], 'type' => $type, 'contact_id' => $contact_id, 'customer_type' => 'contact' );
										$communication_id = usam_insert_communication( $data );
									}
								}
							}
							$duplicat_contact->delete( );	
						}	
						$contact->set( $contact_data );							
						$contact->save( );						
					}						
					$i++;
				}
				$this->sendback = add_query_arg( array( 'updated' => $i ), $this->sendback );		
			break;	
			case 'save':				
				$new_contact = stripslashes_deep($_POST['contact']);	
				$new_contact['foto'] = absint($_POST['thumbnail']);			
				
				$new_communication = !empty($_POST['new_communication'])?stripslashes_deep($_POST['new_communication']):array();			
				$communications = !empty($_POST['communication'])?stripslashes_deep($_POST['communication']):array();			
				$new_contact['about_contact'] = sanitize_textarea_field($_POST['about_contact']);	
				$new_contact['location_id']   = absint($_POST['location']);			
				
				$new_contact['birthday'] = usam_get_datepicker('birthday');				
				if ( $this->id != null )	
				{			
					$update_ids = array();
					foreach ($communications as $type_key => $array )
					{
						foreach ($array['value'] as $communication_id => $value)
						{
							$value = trim($value);
							if ( $value != '' )
							{
								$update_ids[] = $communication_id;
								$communication = array( 'value' => $value, 'value_type' => $array['type'][$communication_id] );							
								usam_update_communication($communication_id, $communication);
							}
						}					
					}		
					global $wpdb;
							
					$communications_id = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM " . USAM_TABLE_MEANS_COMMUNICATION . " WHERE contact_id = '%d' AND customer_type='contact'", $this->id ) );
					$result = array_diff($communications_id, $update_ids);		

					if ( !empty($result) )
					{
						foreach ($result as $id )
						{
							$args = array( 'id' => $id, 'customer_type' => 'contact' );			
							usam_delete_means_communication( $args );
						}
					}									
					$new_contact['id'] = $this->id;
					usam_update_contact( $new_contact );	
				}
				else
				{
						$this->id = usam_insert_contact( $new_contact );			
				}		
				foreach ($new_communication as $type_key => $array )
				{ 
					foreach ($array['value'] as $communication_id => $item)
					{						
						$data = array( 'contact_id' => $this->id, 'customer_type' => 'contact', 'type' => $type_key, 'value' => $item, 'value_type' => $array['type'][$communication_id] );		
						usam_insert_communication( $data );						
					}					
				}					
			break;
			case 'start_import':				
				ini_set( 'auto_detect_line_endings', 1 );	
				
				if( !empty($_FILES['import_file']) && $_FILES['import_file']['name'] != '' )
				{		
					$file_path = USAM_FILE_DIR . $_FILES['import_file']['name'];
					if ( !move_uploaded_file( $_FILES['import_file']['tmp_name'], $file_path ) )
						return;
					
					$extension = usam_get_extension( $file_path ); // узнать формат файла
					switch ( $extension ) 
					{
						case 'xls':	
						case 'xlsx':
							$results = usam_read_exel_file( $file_path );	
						break;
						case 'txt':
						case 'csv':			
							$results = usam_read_txt_file($file_path, ',' );
						break;			
						default:				
						break;
					}					
				}		
				elseif( !empty($_POST['import_text']) )
				{				
					$import = trim(stripslashes($_POST['import_text']));
					$import = str_replace(array("\r", "\n\n", "\n\t\t\n\t\n\t\n\t\n", "\n\t\t\n\t\n\t\n", "\xEF\xBB\xBF", "\n\t\n", "\n(+1)"), array("\n", "\n", "\n ;", "\n", '', ';', ''), $import);
					$arraylines = explode("\n", $import);
					
					$results = array();
					foreach ( $arraylines as $str )
					{	
						$results[] = explode(",", $str);
					}
				}	
				$default = array( 'email', 'lastname', 'firstname', 'phone' );
				$keys = array( );
				$contacts = array();
				foreach ( $results as $row )
				{							
					if ( empty($keys) )
					{			
						$count = count($row);			
						for ( $i = 0; $i < $count; $i++ )
						{				
							$key = trim($row[$i]);
							if ( in_array( $key, $default) )
							{					
								$keys[$key] = $i;
							}	
						}							
						if ( empty($keys) )
						{
							$this->sendback = add_query_arg( array('error' => 'keys'), $this->sendback );	
							return false;
						}								
					}
					else
					{														
						foreach ( $keys as $key => $value )
						{				 
							$contact[$key] = trim($row[$value]);
						}
						if ( !empty($contact) )
							$contacts[] = $contact;
					}			
				}		
				if ( !empty($contacts) )
				{
					ini_set("max_execution_time", "3660");
					foreach ( $contacts as $key => $contact )
					{						
						if ( empty($contact) )
							continue;
				
						if ( isset($contact['lastname']) )
							$new_contact['lastname'] = $contact['lastname'];
						
						if ( isset($contact['firstname']) )
							$new_contact['firstname'] = $contact['firstname'];
												
						$new_contact['contact_source'] = 'import';
				
						$contact_id = usam_insert_contact( $new_contact );
						if ( isset($contact['email']) )
						{					
							$data = array( 'value' => $contact['email'], 'value_type' => 'private', 'type' => 'email', 'contact_id' => $contact_id, 'customer_type' => 'contact' );
							$communication_id = usam_insert_communication( $data );	
						}
						if ( isset($contact['phone']) )
						{					
							$data = array( 'value' => $contact['phone'], 'value_type' => 'private', 'type' => 'phone', 'contact_id' => $contact_id, 'customer_type' => 'contact' );
							$communication_id = usam_insert_communication( $data );		
						}
					}	
					$this->sendback = add_query_arg( 'import' , count($contacts) );		
				}				
				$this->redirect = true;					
			break;
		}		
	}	
	
	protected function get_message()
	{		
		$message = '';					
		if( isset($_REQUEST['import']) )
		{
			$import = absint( $_REQUEST['import'] );	
			$message = sprintf( _n( '%s контакта загруженно.', '%s контактов загруженно.', $import, 'usam' ), $import );					
		}						
		return $message;
	} 
}
?>