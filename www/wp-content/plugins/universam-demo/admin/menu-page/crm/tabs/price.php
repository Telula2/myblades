<?php
class USAM_Tab_price extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Прайс-лист', 'usam'), 'description' =>  __('Здесь Вы можете настроить Прайс лист.', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{
			case 'delete':			
				usam_delete_data( $this->records, 'usam_price_list_setting' );								
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;	
			break;
			case 'save':					
				if ( !empty($_POST['price_list']) )
				{
					$new = $_POST['price_list'];		
					
					$new['title'] = sanitize_text_field(stripcslashes($_POST['name']));	
					$new['date_modified'] = date( "Y-m-d H:i:s" );
					$new['active']        = !empty($_POST['active'])?1:0;
					$new['tax']['category'] =  isset($_POST['tax_input']['usam-category'])?array_map('intval', $_POST['tax_input']['usam-category']):array();
					$new['tax']['brands'] = isset($_POST['tax_input']['usam-brands'])?array_map('intval', $_POST['tax_input']['usam-brands']):array();			
					$new['roles'] = isset($_POST['input-roles'])?$_POST['input-roles']:array();	
					if ( $this->id != null )	
					{					
						usam_edit_data( $new, $this->id, 'usam_price_list_setting' );					
					}
					else			
					{				
						$new['date_insert'] = date( "Y-m-d H:i:s" );					
						$this->id = usam_add_data( $new, 'usam_price_list_setting' );				
					}
				}
			break;
		}		
	}
}