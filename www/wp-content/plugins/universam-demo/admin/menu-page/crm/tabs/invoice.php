<?php
class USAM_Tab_invoice extends USAM_Tab
{
	public function __construct()
	{ 
		$this->header = array( 'title' => __('Счета', 'usam'), 'description' => 'Здесь вы можете посмотреть ваши выставленные счета.' );		
		$this->buttons = array( 'new' => __('Выставить счет', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{			
		switch( $this->current_action )
		{			
			case 'delete':
				$i = 0;
				foreach ( $this->records as $key => $value )
				{				
					$invoice = new USAM_Document( $value );
					$invoice->delete();					
					$i++;
				}
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );		
			break;
			case 'new':
				global $type_price, $user_ID;	
				
				$insert = array( 'name' => '', 'type_price' => $type_price, 'manager_id' => $user_ID, 'customer_type' => 'company', 'type' => 'invoice' );
				if ( isset($_REQUEST['contact']) )
				{
					$insert['customer_type'] = 'contact';
					$insert['customer_id'] = absint($_REQUEST['contact']);
				}
				elseif ( isset($_REQUEST['company']) )
				{
					$insert['customer_type'] = 'company';
					$insert['customer_id'] = absint($_REQUEST['company']);
				}				
				$_invoice = new USAM_Document( );
				$_invoice->set( $insert );	
				$_invoice->save();			
				
				$this->id = $_invoice->get('id');
				$this->sendback = add_query_arg( array( 'action' => 'edit', 'id' => $this->id ), $this->sendback );					
				wp_redirect( $this->sendback );
				exit;
			break;
			case 'save':
				$new['type'] = 'invoice';	
				$this->process_save_document( $new );		
				
				if ( isset($_REQUEST['contract']) )
				{
					$constant_id = absint($_REQUEST['contract']);
					usam_update_document_metadata($this->id, 'contract', $constant_id ); 		
				}
			break;
		}
	}		
}
