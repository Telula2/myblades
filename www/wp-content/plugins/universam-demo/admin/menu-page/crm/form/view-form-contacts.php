<?php		
require_once( USAM_FILE_PATH .'/admin/includes/view_form.class.php' );	
class USAM_Form_contacts extends USAM_View_Form
{	
	protected function get_title_tab()
	{ 	
		return __('Контакт', 'usam').': <span id="customer_name">'. $this->data['lastname'].' '.$this->data['firstname'].' '.$this->data['patronymic'].'</span>';
	}
	
	protected function get_data_tab()
	{ 	
		$this->data = usam_get_contact( $this->id );	
		$this->tabs = array( 
			array( 'slug' => 'tape', 'title' => __('Лента','usam') ), 
			array( 'slug' => 'affairs', 'title' => __('Дела','usam') ), 
			array( 'slug' => 'orders', 'title' => __('Заказы','usam') ), 
			array( 'slug' => 'suggestions', 'title' => __('Предложения','usam') ), 
			array( 'slug' => 'invoice', 'title' => __('Счета','usam') ), 
		);
		
		$this->header_title = __('Описание', 'usam');
		$this->header_content = $this->data['about_contact'];
	}	
	
	protected function main_content_cell_1( ) 
	{		
		$company = usam_get_company( $this->data['company_id'] );
		?>		
		<table>
			<?php if ( $this->data['contact_source'] !='' ) { ?>			
			<tr><td><strong><?php _e('Источник', 'usam'); ?>:</strong></td><td><?php echo usam_get_name_contact_source( $this->data['contact_source'] ); ?></td></tr>
			<?php } ?>		
			<?php if ( isset($company['name']) ) { ?>
				<tr><td><strong><?php _e('Название компании', 'usam'); ?>:</strong></td><td><?php echo $company['name']; ?></td></tr>
			<?php } ?>		
			<?php if ( $this->data['post'] !='' ) { ?>			
				<tr><td><strong><?php _e('Должность', 'usam'); ?>:</strong></td><td><?php echo $this->data['post']; ?></td></tr>
			<?php } ?>		
		</table>	
		<?php	
	}	
	
	protected function main_content_cell_2( ) 
	{ 		
		$keys = array( 'email' => __('E-mail', 'usam'), 'phone' => __('Телефон', 'usam'), 'site' => __('Сайт', 'usam'), 'social' => __('Социальная сеть', 'usam') );
		?>		
		<table>
			<?php
			foreach( $keys as $key => $name )
			{ 
				if ( !empty($this->data[$key]) )
				{ 
					
					?>			
						<tr><td><strong><?php echo $name; ?>:</strong></td>
						<td>
							<?php
							$values = array();
							foreach( $this->data[$key] as $id => $data)
							{ 
								$values[] = "<span class='communication-$key'>".$data['value']."</span>";
							}
							echo implode( ', ', $values );
							?>
						</td>
						</tr>
					<?php
				}
			}
			?>
		</table>	
		<?php	
	}	
	
	protected function main_content_cell_3( ) 
	{ 
		?>		
		<table>			
			<tr><td><strong><?php _e('Ответственный', 'usam'); ?>:</strong></td><td><?php echo usam_get_manager_name( $this->data['manager_id'] ); ?></td></tr>		
		</table>	
		<?php	
	}
	
	function display_tab_tape()
	{
		$this->list_table( 'tape' );			
	}
	
	function display_tab_affairs()
	{
		$this->list_table( 'affairs' );	
	}
	
	function display_tab_orders()
	{
		$this->list_table( 'orders' );			
	}	
		
	function display_tab_suggestions()
	{		
		$this->list_table( 'suggestions' );			
	}	
	
	function display_tab_invoice()
	{ 
		$this->list_table( 'invoice' );			
	}	
}
?>