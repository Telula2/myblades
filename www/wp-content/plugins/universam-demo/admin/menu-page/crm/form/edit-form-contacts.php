<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_contacts extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить контакт %s','usam'), $this->data['lastname'].' '.$this->data['firstname'].' '.$this->data['patronymic'] );
		else
			$title = __('Добавить новый контакт', 'usam');	
		return $title;
	}
	
	protected function get_data_tab()
	{ 	
		if ( $this->id != null )
		{			
			$this->data = usam_get_contact( $this->id );
		}
		else
		{ 
			$this->data = array( 'contact_source' => '', 'manager_id' => '', 'user_id' => '', 'open' => 1, 'sex' => '', 'lastname' => '', 'firstname' => '', 'patronymic' => '', 'birthday' => '', 'company_id' => 0, 'post' => '', 'address' => '', 'address2' => '', 'postal_code' => '', 'about_contact' => '', 'foto' => 0, 'location_id' => 0, 'email' => array(), 'phone' => array(),'site' => array(),'social' => array() );			
		}
	}
		
    public function box_contact( )
	{       	
		$option = get_option('usam_crm_contact_source', array() );
		$contact_sources = maybe_unserialize( $option );
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="contact_table">
			<tbody>						
				<tr>
					<td class="name"><?php _e('Фамилия','usam'); ?>:</td>
					<td><input class= "contact_name" type="text" name="contact[lastname]" value="<?php echo $this->data['lastname']; ?>" size="20" /></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Имя','usam'); ?>:</td>
					<td><input class= "contact_name" type="text" name="contact[firstname]" value="<?php echo $this->data['firstname']; ?>" size="20" /></td>
				</tr>		
				<tr>
					<td class="name"><?php _e('Отчество','usam'); ?>:</td>
					<td><input class= "contact_name" type="text" name="contact[patronymic]" value="<?php echo $this->data['patronymic']; ?>" size="20" /></td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Пользователь','usam'); ?>:</td>
					<td>						
						<select class="chzn-select"  name = "contact[user_id]">
							<option value="0" <?php echo ($this->data['user_id'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
							<?php	
							$args = array( 'orderby' => 'nicename', 'fields' => array( 'ID','user_nicename') );
							$users = get_users( $args );
							foreach( $users as $user )
							{						
								?>               
								<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['user_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->user_nicename; ?></option>
								<?php
							}
							?>
						</select>				
					</td>	
				</tr>					
				<tr>				
					<td class="name"><?php _e('Пол','usam'); ?>:</td>
					<td>
						<select class="chzn-select" name = "contact[sex]">
							<option value="m" <?php echo ($this->data['sex'] == 'male') ?'selected="selected"':''?> ><?php _e('Мужской','usam'); ?></option>
							<option value="f" <?php echo ($this->data['sex'] == 'female') ?'selected="selected"':''?> ><?php _e('Женский','usam'); ?></option>
						</select>				
					</td>	
				</tr>				
				<tr>
					<td class="name"><?php _e('Дата рождения','usam'); ?>:</td>
					<td><?php usam_display_datetime_picker( 'birthday', $this->data['birthday'] ); ?></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Источник','usam'); ?>:</td>
					<td>
						<select class="chzn-select" name = "contact[contact_source]">
							<?php 
							foreach ( $contact_sources as $contact_source )
							{	 ?>	
								<option value='<?php echo $contact_source['id']; ?>' <?php echo ( $contact_source['id'] == $this->data['contact_source']?'selected="selected"':'') ?>><?php echo $contact_source['name']; ?></option>	
							<?php 
							}
							?>	
						</select>
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td>						
						<select class="chzn-select" name = "contact[manager_id]">
							<option value="0" <?php echo ($this->data['manager_id'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
							<?php	
							$args = array( 'orderby' => 'nicename', 'role__in' => array('editor','shop_manager','administrator'), 'fields' => array( 'ID','user_nicename') );
							$users = get_users( $args );
							foreach( $users as $user )
							{						
								?>               
								<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['manager_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->user_nicename; ?></option>
								<?php
							}
							?>
						</select>
					</td>	
				</tr>
				<tr>
					<td class="name"><?php _e('Доступный всем','usam'); ?>:</td>
					<td>
						<input type="hidden" name="contact[open]" value="0"/>
						<input type="checkbox" name="contact[open]" value="1" <?php checked( $this->data['open'] ); ?> />
					</td>
				</tr>				
			</tbody>
		</table>
      <?php
	}     
	
	
	public function section_place_of_work( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="contact_table">
			<tbody>					
				<tr>
					<td class="name"><?php _e('Название компании','usam'); ?>:</td>
					<td>
						<?php 
							$autocomplete = new USAM_Autocomplete_Forms( );
							$autocomplete->get_form_company( $this->data['company_id'], array( 'name' => 'contact[company_id]' ) );
						?>	
					</td>
				</tr>				
				<tr>
					<td class="name"><?php _e('Должность','usam'); ?>:</td>
					<td><input type="text" name="contact[post]" value="<?php echo $this->data['post']; ?>"/></td>
				</tr>						
			</tbody>
		</table>
      <?php
	}     	
	
	public function section_residence_address( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="contact_table">
			<tbody>					
				<tr>
					<td class="name"><?php _e('Местоположение','usam'); ?>:</td>
					<td>
						<?php 
							$autocomplete = new USAM_Autocomplete_Forms( );
							$autocomplete->get_form_position_location( $this->data['location_id'] );
						?>	
					</td>
				</tr>				
				<tr>
					<td class="name"><?php _e('Улица, дом, корпус, строение','usam'); ?>:</td>
					<td><textarea class="usam_crm_edit_text_area" name="contact[address]" cols="40" rows="3"><?php echo $this->data['address']; ?></textarea></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Квартира / офис','usam'); ?>:</td>
					<td><input type="text" name="contact[address2]" value="<?php echo $this->data['address2']; ?>"/></td>
				</tr>											
				<tr>
					<td class="name"><?php _e('Почтовый индекс','usam'); ?>:</td>
					<td><input type="text" name="contact[postal_code]" value="<?php echo $this->data['postal_code']; ?>" /></td>
				</tr>							
			</tbody>
		</table>
      <?php
	}	
	
	public function section_means_communication( )
	{   
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="contact_table">
			<tbody>										
				<tr>
					<td class="name"><?php _e('E-mail','usam'); ?>:</td>
					<td>
<table class ="subtab-detail-content-table usam_edit_table fixed">	
<?php 
$row_display = true;
foreach( $this->data['email'] as $id => $data)
{ 					
	$row_display = false;
	?>
	<tr id = 'email'>
		<td>
			<input type="text" name="communication[email][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>"/>
			<select class="usam_select" name = "communication[email][type][<?php echo $id; ?>]">				
				<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>								
				<option value='private' <?php echo ( $data['value_type'] == 'private'?'selected="selected"':'') ?>><?php _e( 'Личный', 'usam' ); ?></option>	
				<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
			<a class="button_delete" href="#"></a>
		</td>
	</tr>
<?php } ?>	
<?php
$class = $row_display?'':'hide';
?>				
<tr id = 'new_email' class ="<?php echo $class; ?>">		
	<td>
		<input type="text" name="new_communication[email][value][]" value=""/>
		<select class="usam_select" name = "new_communication[email][type][]">				
			<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>								
			<option value='private'><?php _e( 'Личный', 'usam' ); ?></option>	
			<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
		</select>
	</td>
</tr>		
<tr>	
	<td><a id = "add_row" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>	
					
					<tr>
						<td class="name"><?php _e('Телефон','usam'); ?>:</td>
						<td>
<table class ="subtab-detail-content-table usam_edit_table fixed">	
<?php 
$row_display = true;
foreach( $this->data['phone'] as $id => $data)
{ 
	$row_display = false;
	?>					
	<tr>		
		<td>
			<input type="text" name="communication[phone][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
			<select class="usam_select" name = "communication[phone][type][<?php echo $id; ?>]">
				<option value='mobile' <?php echo ( $data['value_type'] == 'mobile'?'selected="selected"':'') ?>><?php _e( 'Мобильный', 'usam' ); ?></option>	
				<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>								
				<option value='home' <?php echo ( $data['value_type'] == 'home'?'selected="selected"':'') ?>><?php _e( 'Домашний', 'usam' ); ?></option>	
				<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
			<a class="button_delete" href="#"></a>
		</td>
	</tr>	
<?php } ?>			
	<?php
	$class = $row_display?'':'hide';
	?>
	<tr class ="<?php echo $class; ?>">	
		<td>
			<input type="text" name="new_communication[phone][value][]" value="" />
			<select class="usam_select" name = "new_communication[phone][type][]">
				<option value='mobile'><?php _e( 'Мобильный', 'usam' ); ?></option>	
				<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>								
				<option value='home'><?php _e( 'Домашний', 'usam' ); ?></option>	
				<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
		</td>
	</tr>
	<tr>		
		<td><a id = "add_row" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
	</tr>
</table>
						</td>
					</tr>	
					<tr>
						<td class="name"><?php _e('Сайт','usam'); ?>:</td>
						<td>
<table class ="subtab-detail-content-table usam_edit_table fixed">	
				<?php 
				$row_display = true;
				foreach( $this->data['site'] as $id => $data)
				{ 
					$row_display = false;
					?>
					<tr>
						<td>
							<input type="text" name="communication[site][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
							<select class="usam_select" name = "communication[site][type][<?php echo $id; ?>]">				
								<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>								
								<option value='private' <?php echo ( $data['value_type'] == 'private'?'selected="selected"':'') ?>><?php _e( 'Личный', 'usam' ); ?></option>	
								<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
							</select>
							<a class="button_delete" href="#"></a>							
						</td>
					</tr>
				<?php } ?>
				<?php
				$class = $row_display?'':'hide';
				?>
				<tr class ="<?php echo $class; ?>">						
					<td>
						<input type="text" name="new_communication[site][value][]" value="" />
						<select class="usam_select" name = "new_communication[site][type][]">				
							<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>								
							<option value='private'><?php _e( 'Личный', 'usam' ); ?></option>	
							<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
						</select>					
					</td>
				</tr>
				<tr>	
	<td><a id = "add_row" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>
					<tr>
						<td class="name"><?php _e('Социальная сеть','usam'); ?>:</td>
						<td>
<table class ="subtab-detail-content-table usam_edit_table fixed">					
				<?php 
				$row_display = true;
				foreach( $this->data['social'] as $id => $data)
				{ 		
					$row_display = false;
					?>
					<tr>						
						<td>
							<input type="text" name="communication[social][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
							<select class="usam_select" name = "communication[social][type][<?php echo $id; ?>]">
								<option value='vk' <?php echo ( $data['value_type'] == 'vk'?'selected="selected"':'') ?>><?php _e( 'ВКонтакте', 'usam' ); ?></option>	
								<option value='facebook' <?php echo ( $data['value_type'] == 'facebook'?'selected="selected"':'') ?>><?php _e( 'Facebook', 'usam' ); ?></option>	
								<option value='telegram' <?php echo ( $data['value_type'] == 'telegram'?'selected="selected"':'') ?>><?php _e( 'Telegram', 'usam' ); ?></option>								
								<option value='skype' <?php echo ( $data['value_type'] == 'skype'?'selected="selected"':'') ?>><?php _e( 'skype', 'usam' ); ?></option>	
								<option value='icq' <?php echo ( $data['value_type'] == 'icq'?'selected="selected"':'') ?>><?php _e( 'ICQ', 'usam' ); ?></option>	
								<option value='msn' <?php echo ( $data['value_type'] == 'msn'?'selected="selected"':'') ?>><?php _e( 'MSN', 'usam' ); ?></option>	
								<option value='jabber' <?php echo ( $data['value_type'] == 'jabber'?'selected="selected"':'') ?>><?php _e( 'Jabber', 'usam' ); ?></option>	
								<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другое', 'usam' ); ?></option>	
							</select>	
							<a class="button_delete" href="#"></a>							
						</td>
					</tr>	
				<?php } ?>				
				<?php
				$class = $row_display?'':'hide';
				?>
				<tr class ="<?php echo $class; ?>">					
					<td>
						<input type="text" name="new_communication[social][value][]" value="" />
						<select class="usam_select" name = "new_communication[social][type][]">
							<option value='vk'><?php _e( 'ВКонтакте', 'usam' ); ?></option>	
							<option value='facebook'><?php _e( 'Facebook', 'usam' ); ?></option>	
							<option value='telegram'><?php _e( 'Telegram', 'usam' ); ?></option>								
							<option value='skype'><?php _e( 'skype', 'usam' ); ?></option>	
							<option value='icq'><?php _e( 'ICQ', 'usam' ); ?></option>	
							<option value='msn'><?php _e( 'MSN', 'usam' ); ?></option>	
							<option value='jabber'><?php _e( 'Jabber', 'usam' ); ?></option>	
							<option value='other'><?php _e( 'Другое', 'usam' ); ?></option>	
						</select>					
					</td>
				</tr>	
				<tr>	
	<td><a id = "add_row" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>		
			</tbody>
		</table>
      <?php
	}   
	
	function display_right()
	{		
		usam_add_box( 'usam_description', __('Фотография','usam'), array( $this, 'imagediv' ), array('thumbnail_id' => $this->data['foto'], 'title' => __('Фотография клиента', 'usam'), 'button_text' => __( 'Задать фотографию', 'usam' )) );
	}  
	
	function display_left()
	{		
		usam_add_box( 'usam_contact', __('Параметры','usam'), array( $this, 'box_contact' ) );
		usam_add_box( 'usam_means_communication', __('Связаться','usam'), array( $this, 'section_means_communication' ) );
		usam_add_box( 'usam_place_of_work', __('Место работы','usam'), array( $this, 'section_place_of_work' ) );		
		usam_add_box( 'usam_residence_address', __('Адрес проживания','usam'), array( $this, 'section_residence_address' ) );
		$this->add_box_description( $this->data['about_contact'], 'about_contact' );
    }	
}
?>