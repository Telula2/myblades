<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/documents_view_form_table.php' );
class USAM_List_Table_suggestions extends USAM_List_Table_Documents_View_Form
{	
	protected $document_type = 'suggestion';
	
	public function extra_tablenav( $which ) 
	{
		?><div class="alignleft actions"><?php
		if ( 'top' == $which ) 
		{
			global $current_screen;					
			$customer = isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'contacts'?'contact':'company';
			?>
			<ul class ="list_button">		
				<?php $sendback = wp_nonce_url( add_query_arg( array('page' => 'crm', 'tab' => 'suggestions', $customer => $this->id, 'action' => 'new' ), admin_url('admin.php') ), 'usam-'.$current_screen->id); ?>
				<li><a href="<?php echo $sendback; ?>" target="_blank" class="button"><?php _e('Новое предложение','usam'); ?></a></li>				
			</ul>
			<?php	
		}
		?></div><?php
	}
	
	function column_id( $item )
	{	
		$url = add_query_arg( array( 'id' => $item->id ), admin_url('admin.php?page=crm&tab=suggestions&action=edit') );	
		echo "<a href='$url'>{$item->number}</a>";	
	}	
	
	function column_status( $item ) 
    {
		echo usam_get_status_name_document_suggestion( $item->status );	
	}	
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'id'             => __( 'Номер', 'usam' ),				
			'name'           => __( 'Предложение', 'usam' ),		
			'status'         => __( 'Статус', 'usam' ),				
			'totalprice'     => __( 'Сумма', 'usam' ),		
			'closedate'      => __( 'Срок', 'usam' ),	
			'date'           => __( 'Дата', 'usam' ),	
			'manager'        => __( 'Менеджер', 'usam' ),				
        );		
        return $columns;
    }	
}
?>