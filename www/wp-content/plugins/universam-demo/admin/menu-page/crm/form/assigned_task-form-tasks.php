<?php	
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/edit-form-my_tasks.php' );	
class USAM_Form_tasks extends USAM_Form_My_Tasks
{			
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить назначенное задание','usam');
		else
			$title = __('Назначить задание', 'usam');	
		return $title;
	}
		
	function task_settings( )
	{	
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>							
				<tr>
					<td class = "name"><?php esc_html_e( 'Дата начала', 'usam' );  ?>:</td>
					<td class = "option"><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?></td>
				</tr>	
				<tr>
					<td class = "name"><?php esc_html_e( 'Срок', 'usam' );  ?>:</td>
					<td class = "option"><?php usam_display_datetime_picker( 'end', $this->data['end'] ); ?></td>
				</tr>											
				</tr>									
				<tr>						
					<td class = "name"><?php _e('Важность','usam'); ?>:</td>
					<td class = "option">
						<input type="checkbox" name="importance" value="1" <?php checked( $this->data['importance'], 1 ); ?>/>
					</td>
				</tr>								
			</tbody>
		</table>		
		<?php 		
    }
}
?>