<?php		
class USAM_List_Table_employees extends USAM_List_Table 
{		
	
	function column_contact( $item ) 
    {
		echo '<a class="row-title" href="'.esc_url( $url = add_query_arg( array('page' => 'crm','tab' => 'contacts', 'action' => 'view', 'id' => $item->id), admin_url('admin.php') ) ).'"><span id="customer_name">'.$item->lastname.' '.$item->firstname.'</span></a>';			
	}
	
	function column_contact_source( $item ) 
    {		
		echo usam_get_name_contact_source( $item->contact_source );
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "contact-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
		
	function column_communication( $item ) 
	{
		$communication = usam_get_contact_means_communication( $item->id );		
		
		if ( isset($communication['email']) )
		{
			echo "<div class = 'email'>";
			foreach( $communication['email'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
		if ( isset($communication['phone']) )
		{
			echo "<div class = 'phone'>";
			foreach( $communication['phone'] as $data)
				echo "<p>".$data['value']."</p>";
			echo "</div>";
		}
	}
	
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',
			'contact'        => __( 'Контакт', 'usam' ),		
			'affairs'        => __( 'Дела', 'usam' ),	
			'communication'  => __( 'Связаться', 'usam' ),				
			'contact_source' => __( 'Источник', 'usam' ),	
			'date'           => __( 'Создан', 'usam' ),	
			'manager'        => __( 'Менеджер', 'usam' ),								
        );
        return $columns;	
    }	
	
	function prepare_items() 
	{		
		require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );		
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 		
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'search' => $this->search,
			'company_id' => $this->id,			
		);			
		$_contacts = new USAM_Contacts_Query( $query );
		$this->items = $_contacts->get_results();
		
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}

}