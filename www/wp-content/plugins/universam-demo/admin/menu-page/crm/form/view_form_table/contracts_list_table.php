<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/documents_table.php' );
class USAM_List_Table_contracts extends USAM_Documents_Table
{	
	protected $document_type = 'contract';
	
	function column_status( $item ) 
    {
		echo usam_get_status_name_document_contract( $item->status );	
	}	
	
	public function extra_tablenav( $which ) 
	{
		?><div class="alignleft actions"><?php
		if ( 'top' == $which ) 
		{
			global $current_screen;					
			$customer = isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'contacts'?'contact':'company';
			?>
			<ul class ="list_button">		
				<?php $sendback = wp_nonce_url( add_query_arg( array('page' => 'crm', 'tab' => 'contracts', $customer => $this->id, 'action' => 'new' ), admin_url('admin.php') ), 'usam-'.$current_screen->id); ?>
				<li><a href="<?php echo $sendback; ?>" target="_blank" class="button"><?php _e('Заключить договор','usam'); ?></a></li>				
			</ul>
			<?php	
		}
		?></div><?php
	}
	
	function column_id( $item )
	{	
		$url = add_query_arg( array( 'id' => $item->id ), admin_url("admin.php?page=crm&tab={$this->document_type}&action=edit") );	
		echo "<a href='$url'>{$item->number}</a>";	
	}	
	
	function get_columns()
	{		
        $columns = array(           
			'cb'             => '<input type="checkbox" />',		
			'id'             => __( 'Номер', 'usam' ),				
			'name'           => __( 'Название', 'usam' ),		
			'status'         => __( 'Статус', 'usam' ),				
			'totalprice'     => __( 'Сумма', 'usam' ),		
			'closedate'      => __( 'Срок', 'usam' ),	
			'date'           => __( 'Дата', 'usam' ),	
			'manager'        => __( 'Менеджер', 'usam' ),				
        );		
        return $columns;
    }
}
?>