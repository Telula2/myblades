<?php	
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/edit-form-my_tasks.php' );	
class USAM_Form_Tasks extends USAM_Form_My_Tasks
{	
	protected function get_title_tab()
	{ 	
		global $user_ID;
		if ( $this->data['type'] == 'assigned_task' )		
		{			
			if ( $this->data['user_id'] == $user_ID )
			{
				if ( $this->id != null )
					$title = sprintf( __('Назначенное задание &#8220;%s&#8221;','usam'),$this->data['title']);
				else
					$title = __('Добавить задание', 'usam');	
			}
			else
			{
				$title = sprintf( __('Назначенное Вам задание &#8220;%s&#8221;','usam'),$this->data['title']);
			}
		}
		else
		{
			if ( $this->id != null )
				$title = sprintf( __('Задание &#8220;%s&#8221;','usam'),$this->data['title']);
			else
				$title = __('Добавить задание', 'usam');	
		}
		return $title;
	}	
	
	function task_settings( )
	{			
		global $user_ID;	
		
		if ( $this->data['type'] == 'task' )
		{
		?>	
		<div class="usam_line_menu">
			<div class="usam_line_menu-wrapper">			
				<div class="usam_line">
					<div class="usam_cell_line">
						<span class = "name"><?php esc_html_e( 'Дата начала', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?></span>	
					</div>
					<div class="usam_cell_line">					
						<span class = "name"><?php esc_html_e( 'Срок', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'end', $this->data['end'] ); ?></span>	
					</div>							
				</div>			
				<div class="usam_line">
					<div class="usam_cell_line">
						<select name="status">
							<?php 		
							foreach( $this->statuses as $key => $status )
							{	
								?> 
								<option value="<?php echo $key; ?>" <?php selected($this->data['status'], $key); ?>><?php echo $status; ?></option>								
								<?php 	
							}
							?>
						</select>
					</div>
					<div class="usam_cell_line">
						<?php 
						$calendars = usam_get_user_calendars();
						if ( !empty($calendars) )
						{
							?>
							<select name="calendar">
								<?php 		
								foreach( $calendars as $key => $item )
								{	
									?> 
									<option value="<?php echo $item['id']; ?>" <?php selected($this->data['calendar'],$item['id']); ?>><?php echo $item['name']; ?></option>
									<?php 	
								}
								?>
							</select><?php 							
						}
						?>
					</div>		
				</div>	
				<div class="usam_line">
					<div class="usam_cell_line">
						<?php $colors = array( 'blue', 'brown', 'yellow' , 'green' , 'purple' , 'gray' ); ?>						
						<ul id="color_select">
							<?php 		
							foreach( $colors as $color )
							{	
								?> 
								<li class="color_<?php echo $color; ?> <?php echo $this->data['color']==$color?'select':''; ?>" data-color="<?php echo $color; ?>"><span class='color_block'></span></li>
								<?php 	
							}
							?>
						</ul>
						<input type="hidden" name="color" class="input_color" value="<?php echo $this->data['color']; ?>"/>
					</div>
					<div class="usam_cell_line usam_cell_line_importance">
						<?php 	
						if ( $this->data['importance'] )
							echo '<span class="dashicons dashicons-star-filled"></span>';
						else
							echo '<span class="dashicons dashicons-star-empty"></span>';
						?>
						<input type="hidden" name="importance" value="<?php echo $this->data['importance']; ?>"/>
					</div>		
				</div>			
				<div class="usam_line">
					<div class="usam_cell_line">
						<span class = "name"><?php _e('Напомнить','usam'); ?>:</span>
						<span class = "option">
							<input type="checkbox" id = "usam_remind" name = "remind" <?php echo (empty($this->data['date_time']))?'':'checked'; ?> value="1"/><span class ="<?php echo (!empty($this->data['date_time']))?'':'hide'; ?>" id ="date_remind"><?php usam_display_datetime_picker( 'date_time', $this->data['date_time'] ); ?></span>
						</span>
					</div>						
				</div>			
			</div>
		</div>		
		<?php 
		}
		else
		{
			if ( $this->data['user_id'] == $user_ID )
			{
				?>	
				<div class="usam_line_menu">
					<div class="usam_line_menu-wrapper">			
						<div class="usam_line">
							<div class="usam_cell_line">
								<span class = "name"><?php esc_html_e( 'Дата начала', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?></span>	
							</div>
							<div class="usam_cell_line">					
								<span class = "name"><?php esc_html_e( 'Срок', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'end', $this->data['end'] ); ?></span>	
							</div>							
						</div>			
						<div class="usam_line">
							<div class="usam_cell_line">
								<select name="status">
									<?php 		
									foreach( $this->statuses as $key => $status )
									{	
										?> 
										<option value="<?php echo $key; ?>" <?php selected($this->data['status'], $key); ?>><?php echo $status; ?></option>								
										<?php 	
									}
									?>
								</select>
							</div>							
						</div>	
						<div class="usam_line">
							<div class="usam_cell_line">
								<?php $colors = array( 'blue', 'brown', 'yellow' , 'green' , 'purple' , 'gray' ); ?>						
								<ul id="color_select">
									<?php 		
									foreach( $colors as $color )
									{	
										?> 
										<li class="background_<?php echo $color; ?> <?php echo $this->data['color']==$color?'select':''; ?>" data-color="<?php echo $color; ?>"><span class='color_block'></span></li>
										<?php 	
									}
									?>
								</ul>
								<input type="hidden" name="color" class="input_color" value="<?php echo $this->data['color']; ?>"/>
							</div>
							<div class="usam_cell_line usam_cell_line_importance">
								<?php 	
								if ( $this->data['importance'] )
									echo '<span class="dashicons dashicons-star-filled"></span>';
								else
									echo '<span class="dashicons dashicons-star-empty"></span>';
								?>
								<input type="hidden" name="importance" value="<?php echo $this->data['importance']; ?>"/>
							</div>		
						</div>										
					</div>
				</div>
				<?php 	
			}
			else
			{	
			?>		
				<div class="usam_line_menu">
					<div class="usam_line_menu-wrapper">					
						<div class="usam_line">
							<div class="usam_cell_line">
								<span class = "name"><?php esc_html_e( 'Дата начала', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?></span>	
							</div>
							<div class="usam_cell_line">					
								<span class = "name"><?php esc_html_e( 'Срок', 'usam' );  ?>:</span><span class = "option"><?php usam_display_datetime_picker( 'end', $this->data['end'] ); ?></span>	
							</div>							
						</div>			
						<div class="usam_line">
							<div class="usam_cell_line">
								<select name="status">
									<?php 		
									foreach( $this->statuses as $key => $status )
									{	
										?> 
										<option value="<?php echo $key; ?>" <?php selected($this->data['status'], $key); ?>><?php echo $status; ?></option>								
										<?php 	
									}
									?>
								</select>
							</div>								
						</div>	
					</div>
				</div>	
			<?php 	
			}  	
		}
    }	
	
	function display_left()
	{
		$this->task_settings();			
		$this->titlediv( $this->data['title'] );					
		$this->add_box_description( $this->data['description'], 'description', __('Описание задания','usam') );	
		
		if ( $this->id )
		{		
			usam_add_box( 'usam_event_file_upload', __('Вложения','usam'), array( $this, 'display_file_upload' ) );	
			usam_add_box( 'usam_comments', __('Комментарии','usam'), array( $this, 'display_comments' ), 'event' );					
			usam_add_box( 'usam_event_object_type', __('Прикрепленные объекты','usam'), array( $this, 'object_type' ) );						
		}
    }	
	
	function display_right()
	{	
		if ( $this->id )
		{
			global $user_ID;
			if ( $this->data['type'] != 'assigned_task' || $this->data['user_id'] == $user_ID )			
			{
				if ( $this->data['type'] == 'assigned_task' )			
					$title = __('Кому назначить','usam');
				else
					$title = __('Участники','usam');
				
				usam_add_box( 'usam_participants', $title, array( $this, 'select_users' ) );			
			}
			usam_add_box( 'usam_action_list', __('Список действий','usam'), array( $this, 'event_action_lists' ) );			
		}
	}
}
?>