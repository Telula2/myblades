<?php		
require_once( USAM_FILE_PATH .'/admin/includes/view_form.class.php' );	
class USAM_Form_company extends USAM_View_Form
{	
	protected function get_title_tab()
	{ 	
		return __('Компания', 'usam').': <span id="customer_name">'.$this->data['name'].'</span>';
	}
	
	protected function get_data_tab()
	{ 	
		$this->data = usam_get_company( $this->id );	
		$this->tabs = array( 
			array( 'slug' => 'affairs', 'title' => __('Дела','usam') ), 			 	
			array( 'slug' => 'email', 'title'   => __('Письма','usam') ), 	
			array( 'slug' => 'orders', 'title'  => __('Заказы','usam') ), 
			array( 'slug' => 'suggestions', 'title' => __('Предложения','usam') ), 
			array( 'slug' => 'invoice', 'title' => __('Счета','usam') ), 
			array( 'slug' => 'employees', 'title' => __('Сотрудники','usam') ), 
			array( 'slug' => 'contracts', 'title' => __('Договора','usam') ),		
		);
		
		if ( !empty($this->data) )
		{
			$this->header_title = __('Описание', 'usam');
			$this->header_content = $this->data['description'];
		}
	}	
	
	protected function main_content_cell_1( ) 
	{		
		$employees = array( '-50' => __('менее 50','usam'), '250' => __('50-250','usam'), '500' => __('250-500','usam'), '+500' => __('более 500','usam') );				
		?>		
		<table class="header_main_content_table">
			<?php if ( !empty($this->data['type']) ) { ?>
				<tr><td><strong><?php _e('Тип компании', 'usam'); ?>:</strong></td><td><?php echo usam_get_name_type_company( $this->data['type'] ); ?></td></tr>
			<?php } ?>		
			<?php if ( !empty($this->data['industry']) ) { ?>			
				<tr><td><strong><?php _e('Сфера деятельности', 'usam'); ?>:</strong></td><td><?php echo usam_get_name_industry_company( $this->data['industry'] ); ?></td></tr>
			<?php } ?>		
			<?php if ( !empty($this->data['group']) ) { ?>			
				<tr><td><strong><?php _e('Группа', 'usam'); ?>:</strong></td><td><?php echo usam_get_name_group_company( $this->data['group'] ); ?></td></tr>
			<?php } ?>	
			<?php if ( !empty($this->data['employees']) ) { ?>			
				<tr><td><strong><?php _e('Кол-во сотрудников', 'usam'); ?>:</strong></td><td><?php echo $employees[$this->data['employees']]; ?></td></tr>
			<?php } ?>	
			<?php if ( !empty($this->data['revenue']) ) { ?>			
				<tr><td><strong><?php _e('Годовой оборот', 'usam'); ?>:</strong></td><td><?php echo $this->data['revenue']; ?></td></tr>
			<?php } ?>				
		</table>	
		<?php	
	}	
	
	protected function main_content_cell_2( ) 
	{ 		
		$keys = array( 'email' => __('E-mail', 'usam'), 'phone' => __('Телефон', 'usam'), 'site' => __('Сайт', 'usam'), 'social' => __('Социальная сеть', 'usam') );
		?>		
		<table class="header_main_content_table">
			<?php
			foreach( $keys as $key => $name )
			{ 
				if ( !empty($this->data[$key]) )
				{ 
					
					?>			
						<tr><td><strong><?php echo $name; ?>:</strong></td>
						<td>
							<?php
							$values = array();
							foreach( $this->data[$key] as $id => $data)
							{ 
								$values[] = "<span class='communication-$key'>".$data['value']."</span>";
							}
							echo implode( ', ', $values );
							?>
						</td>
						</tr>
					<?php
				}
			}
			?>
		</table>	
		<?php	
	}	
	
	protected function main_content_cell_3( ) 
	{ 
		?>		
		<table class="header_main_content_table">	
			<tr><td><strong><?php _e('Ответственный', 'usam'); ?>:</strong></td><td><?php echo usam_get_manager_name( $this->data['manager_id'] ); ?></td></tr>		
		</table>	
		<?php	
	}
		
	function display_tab_tape()
	{
		$this->list_table( 'tape' );			
	}
	
	function display_tab_email()
	{ 
		$this->list_table( 'email' );			
	}
	
	function display_tab_affairs()
	{
		$this->list_table( 'affairs' );	
	}
	
	function display_tab_orders()
	{
		$this->list_table( 'orders' );			
	}	
		
	function display_tab_suggestions()
	{		
		$this->list_table( 'suggestions' );			
	}	
	
	function display_tab_invoice()
	{ 
		$this->list_table( 'invoice' );			
	}	
	
	function display_tab_employees()
	{ 
		$this->list_table( 'employees' );			
	}	
	
	function display_tab_contracts()
	{ 
		$this->list_table( 'contracts' );			
	}	
}
?>