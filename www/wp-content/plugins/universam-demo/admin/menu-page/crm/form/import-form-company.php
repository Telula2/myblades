<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_company extends USAM_Edit_Form
{	
	protected $formtype = true;
	protected $action = 'start_import';
	
	protected function get_title_tab()
	{ 	
		return __('Импортировать компании', 'usam');	
	}	
	
	function display()
	{		
		?>		
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row"><label for="redirect"><?php _e('Как Вы хотите импортировать?', 'usam'); ?></label></th>
					<td>
						<p>
							<label for="copy-paste"><input type="radio" class="validate" id="copy-paste" checked="checked" value="copy" name="import_type">Скопировать и вставить в текстовое поле</label>
							<label for="upload-file"><input type="radio" class="validate" id="upload-file" value="upload" name="import_type">Загрузить файл</label>
						</p>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="redirect"><?php _e('Первая строка должна содержать следующие метки', 'usam'); ?></label></th>
					<td><p>type, group, manager, contactlocation, contactaddress, contactpostcode, contactoffice, legallocation, legaladdress, legalpostcode, company_name, full_company_name, inn, ppc, ogrn, date_registration, okpo, oktmo, gm, accountant, bank_name, bank_bic, bank_number, bank_ca, bank_currency, bank_address, bank_swift, bank_note</p></td>
				</tr>				
				<tr class="csvmode copy-paste" style="display: table-row;">
					<th scope="row">
						<label for="csvtext"><?php _e('Затем вставьте список здесь', 'usam'); ?></label>
						<p class="description">Это должен быть CSV стиль</p>
					</th>
					<td>
						<textarea type="text" style="width:500px;" cols="130" rows="10" class="validate[required] empty" id="csvtext" name="import_text"></textarea>
						<p class="fieldsmatch"></p>
					</td>
				</tr>
				<tr class="csvmode upload-file" style="display: none;">
					<th scope="row">
						<label for="csvfile"><?php _e('Загрузить файл', 'usam'); ?></label>
						<p class="description"><?php _e('Файл может быть CSV или txt, разделенный запятыми. Или xls, в котором есть столбцы с метками', 'usam'); ?></p>
					</th>
					<td>
					<input type="file" name="import_file" size="50">( общий максимальный размер загружаемого файла: 2000M )
					<p class="fieldsmatch"></p>
					</td>
				</tr>				
			</tbody>
		</table>
		<?php
		submit_button( __('Импортировать','usam').' &#10010;', 'primary', 'save-import', false, array( 'id' => 'save-import' ) ); 
	}	
}
?>