<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_company extends USAM_Edit_Form
{	
	protected $formtype = true;
	
	private $meta = array();	
	private $bank_accounts = array();	
	private $communications = array();		
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить компанию %s','usam'), $this->data['name'] );
		else
			$title = __('Добавить компанию', 'usam');	
		return $title;
	}
	
	protected function get_data_tab()
	{ 
		require_once( USAM_FILE_PATH . '/includes/data_exchange/import.class.php'         );	
		if ( $this->id != null )
		{				
			$_company = new USAM_Company( $this->id );	
			$this->data = $_company->get_data();
			$this->communications = usam_get_company_means_communication( $this->data['id'] );			
			$this->meta = $_company->get_meta();
			$this->bank_accounts = $_company->get_bank_accounts();
		}
		else
		{
			$this->data = array( 'name' => '', 'description' => '', 'manager_id' => '', 'logo' => 0, 'open' => 1, 'type' => '', 'group' => '', 'industry' => '', 'employees' => '', 'revenue' => '', 'company_name' => '' );
			$this->bank_accounts = array();
			$this->meta = array();
			$this->communications = array( 'email' => array(), 'phone' => array(),'site' => array(), 'social' => array() );			
		}
	}
		
    public function box_company( )
	{        
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="company_table">
			<tbody>								
				<tr>				
					<td class="name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td>
						<select class="chzn-select" name = "company[manager_id]">
							<option value="0" <?php echo ($this->data['manager_id'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
							<?php	
							$args = array( 'orderby' => 'nicename', 'role__in' => array('editor','shop_manager','administrator'), 'fields' => array( 'ID','user_nicename') );
							$users = get_users( $args );
							foreach( $users as $user )
							{						
								?>               
								<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['manager_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->user_nicename; ?></option>
								<?php
							}
							?>
						</select>				
					</td>						
				</tr>	
				<tr>
					<td class="name"><?php _e('Доступный всем','usam'); ?>:</td>
					<td>
						<input type="hidden" name="company[open]" value="0"/>
						<input type="checkbox" name="company[open]" value="1" <?php checked( $this->data['open'] ); ?> />
					</td>
				</tr>									
				<tr>				
					<td class="name"><?php _e('Тип компании','usam'); ?>:</td>
					<td>
						<select class="usam_select" name = "company[type]">
							<?php
							$types = usam_get_companies_types();
							foreach( $types as $id => $name )
							{
								?>
								<option value="<?php echo $id; ?>" <?php selected ($this->data['type'], $id ); ?> ><?php echo $name; ?></option>
								<?php 
							} 
							?>										
						</select>				
					</td>	
				</tr>
				<tr>				
					<td class="name"><?php _e('Сфера деятельности','usam'); ?>:</td>
					<td>
						<select class="usam_select" name = "company[industry]">
							<?php
							$industry = usam_get_companies_industry();
							foreach( $industry as $id => $name )
							{
								?>
								<option value="<?php echo $id; ?>" <?php selected ($this->data['industry'], $id ); ?> ><?php echo $name; ?></option>
								<?php 
							} 
							?>
						</select>				
					</td>	
				</tr>	
				<tr>				
					<td class="name"><?php _e('Группа','usam'); ?>:</td>
					<td>
						<select class="usam_select" name = "company[group]">
							<option value="" <?php  selected ($this->data['group'], ''); ?> ><?php _e('Не выбрано','usam'); ?></option>
							<?php							
							$option = get_option('usam_crm_company_group', array() );
							$company_groups = maybe_unserialize( $option );	
							foreach( $company_groups as $key => $item )
							{
								?>
								<option value="<?php echo $item['id']; ?>" <?php selected ($this->data['group'], $item['id'] ); ?> ><?php echo $item['name']; ?></option>
								<?php 
							} ?>
						</select>				
					</td>	
				</tr>				
				<tr>				
					<td class="name"><?php _e('Кол-во сотрудников','usam'); ?>:</td>
					<td>
						<select class="usam_select" name = "company[employees]">
							<option value="-50" <?php  selected ($this->data['employees'], '-50'); ?> ><?php _e('менее 50','usam'); ?></option>
							<option value="250" <?php  selected ($this->data['employees'], '250'); ?> ><?php _e('50-250','usam'); ?></option>
							<option value="500" <?php  selected ($this->data['employees'], '500'); ?> ><?php _e('250-500','usam'); ?></option>
							<option value="+500" <?php  selected ($this->data['employees'], '+500'); ?> ><?php _e('более 500','usam'); ?></option>
						</select>				
					</td>	
				</tr>				
				<tr>
					<td class="name"><?php _e('Годовой оборот','usam'); ?>:</td>
					<td><input type="text" name="company[revenue]" value="<?php echo $this->data['revenue']; ?>" size="20" /></td>
				</tr>	
			</tbody>							
		</table>
		<?php			
	}  
	
	
				
	public function section_means_communication( )
	{  				
		?>
		<table class="subtab-detail-content-table usam_edit_table" id="company_table">
			<tbody>	
				<tr>
					<td class="name"><?php _e('E-mail','usam'); ?>:</td>
					<td>
<table>
<?php 
$row_display = true;
foreach( $this->communications['email'] as $id => $data)
{ 					
	$row_display = false;
	?>
	<tr id = 'email'>
		<td>
			<input type="text" name="communication[email][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>"/>
			<select class="usam_select" name = "communication[email][type][<?php echo $id; ?>]">				
				<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>				
				<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
			<a class="button_delete" href="#"></a>
		</td>
	</tr>
<?php } ?>	
<?php
$class = $row_display?'':'hide';
?>				
<tr id = 'new_email' class ="<?php echo $class; ?>">		
	<td>
		<input type="text" name="new_communication[email][value][]" value=""/>
		<select class="usam_select" name = "new_communication[email][type][]">				
			<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>				
			<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
		</select>
	</td>
</tr>		
<tr>	
	<td><a id = "add_row" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>	
					
				<tr>
					<td class="name"><?php _e('Телефон','usam'); ?>:</td>
					<td>
<table>
<?php 
$row_display = true;
foreach( $this->communications['phone'] as $id => $data)
{ 
	$row_display = false;
	?>					
	<tr>		
		<td>
			<input type="text" name="communication[phone][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
			<select class="usam_select" name = "communication[phone][type][<?php echo $id; ?>]">
				<option value='mobile' <?php echo ( $data['value_type'] == 'mobile'?'selected="selected"':'') ?>><?php _e( 'Мобильный', 'usam' ); ?></option>	
				<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>								
				<option value='home' <?php echo ( $data['value_type'] == 'home'?'selected="selected"':'') ?>><?php _e( 'Домашний', 'usam' ); ?></option>	
				<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
			<a class="button_delete" href="#"></a>
		</td>
	</tr>	
<?php } ?>			
	<?php
	$class = $row_display?'':'hide';
	?>
	<tr class ="<?php echo $class; ?>">	
		<td>
			<input type="text" name="new_communication[phone][value][]" value="" />
			<select class="usam_select" name = "new_communication[phone][type][]">
				<option value='mobile'><?php _e( 'Мобильный', 'usam' ); ?></option>	
				<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>								
				<option value='home'><?php _e( 'Домашний', 'usam' ); ?></option>	
				<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
			</select>
		</td>
	</tr>
	<tr>		
		<td><a id = "add_row"  class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
	</tr>
</table>
						</td>
					</tr>	
					<tr>
						<td class="name"><?php _e('Сайт','usam'); ?>:</td>
						<td>
<table>
				<?php 
				$row_display = true;
				foreach( $this->communications['site'] as $id => $data)
				{ 
					$row_display = false;
					?>
					<tr>
						<td>
							<input type="text" name="communication[site][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
							<select class="usam_select" name = "communication[site][type][<?php echo $id; ?>]">				
								<option value='work' <?php echo ( $data['value_type'] == 'work'?'selected="selected"':'') ?>><?php _e( 'Рабочий', 'usam' ); ?></option>								
								<option value='private' <?php echo ( $data['value_type'] == 'private'?'selected="selected"':'') ?>><?php _e( 'Личный', 'usam' ); ?></option>	
								<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другой', 'usam' ); ?></option>	
							</select>
							<a class="button_delete" href="#"></a>						
						</td>
					</tr>
				<?php } ?>
				<?php
				$class = $row_display?'':'hide';
				?>
				<tr class ="<?php echo $class; ?>">						
					<td>
						<input type="text" name="new_communication[site][value][]" value="" />
						<select class="usam_select" name = "new_communication[site][type][]">				
							<option value='work'><?php _e( 'Рабочий', 'usam' ); ?></option>								
							<option value='private'><?php _e( 'Личный', 'usam' ); ?></option>	
							<option value='other'><?php _e( 'Другой', 'usam' ); ?></option>	
						</select>					
					</td>
				</tr>
				<tr>	
	<td><a id = "add_row"  class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>
					<tr>
						<td class="name"><?php _e('Социальная сеть','usam'); ?>:</td>
						<td>
<table>				
				<?php 
				$row_display = true;
				foreach( $this->communications['social'] as $id => $data)
				{ 		
					$row_display = false;
					?>
					<tr>						
						<td>
							<input type="text" name="communication[social][value][<?php echo $id; ?>]" value="<?php echo $data['value']; ?>" />
							<select class="usam_select" name = "communication[social][type][<?php echo $id; ?>]">
								<option value='vk' <?php echo ( $data['value_type'] == 'vk'?'selected="selected"':'') ?>><?php _e( 'ВКонтакте', 'usam' ); ?></option>	
								<option value='facebook' <?php echo ( $data['value_type'] == 'facebook'?'selected="selected"':'') ?>><?php _e( 'Facebook', 'usam' ); ?></option>	
								<option value='telegram' <?php echo ( $data['value_type'] == 'telegram'?'selected="selected"':'') ?>><?php _e( 'Telegram', 'usam' ); ?></option>								
								<option value='skype' <?php echo ( $data['value_type'] == 'skype'?'selected="selected"':'') ?>><?php _e( 'skype', 'usam' ); ?></option>	
								<option value='icq' <?php echo ( $data['value_type'] == 'icq'?'selected="selected"':'') ?>><?php _e( 'ICQ', 'usam' ); ?></option>	
								<option value='msn' <?php echo ( $data['value_type'] == 'msn'?'selected="selected"':'') ?>><?php _e( 'MSN', 'usam' ); ?></option>	
								<option value='jabber' <?php echo ( $data['value_type'] == 'jabber'?'selected="selected"':'') ?>><?php _e( 'Jabber', 'usam' ); ?></option>	
								<option value='other' <?php echo ( $data['value_type'] == 'other'?'selected="selected"':'') ?>><?php _e( 'Другое', 'usam' ); ?></option>	
							</select>	
							<a class="button_delete" href="#"></a>						
						</td>
					</tr>	
				<?php } ?>				
				<?php
				$class = $row_display?'':'hide';
				?>
				<tr class ="<?php echo $class; ?>">					
					<td>
						<input type="text" name="new_communication[social][value][]" value="" />
						<select class="usam_select" name = "new_communication[social][type][]">
							<option value='vk'><?php _e( 'ВКонтакте', 'usam' ); ?></option>	
							<option value='facebook'><?php _e( 'Facebook', 'usam' ); ?></option>	
							<option value='telegram'><?php _e( 'Telegram', 'usam' ); ?></option>								
							<option value='skype'><?php _e( 'skype', 'usam' ); ?></option>	
							<option value='icq'><?php _e( 'ICQ', 'usam' ); ?></option>	
							<option value='msn'><?php _e( 'MSN', 'usam' ); ?></option>	
							<option value='jabber'><?php _e( 'Jabber', 'usam' ); ?></option>	
							<option value='other'><?php _e( 'Другое', 'usam' ); ?></option>	
						</select>					
					</td>
				</tr>	
				<tr>	
	<td><a id = "add_row"  class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a></td>
</tr>
</table>
</td>
</tr>
			</tbody>
		</table>
      <?php
	}    	
	
	function company_fields( $group_id )
	{			
		?>
		<table class ="subtab-detail-content-table usam_edit_table company_fields_table">
		<?php		
		$fields = get_option( 'usam_crm_company_fields' );			
		foreach( $fields as $key => $field )
		{
			if ( isset($this->meta[$field['unique_name']]) )
				$value = $this->meta[$field['unique_name']];
			else
				$value = '';
			if ( $field['group'] == $group_id )
			{
				echo "<tr><td class='name'>".$field['name']."</td><td class='value'>";
				switch ( $field['type'] ) 
				{
					case "location":			
						$t = new USAM_Autocomplete_Forms();				
						$t->get_form_position_location( $value, array( 'id' => $key, 'name' => 'fields['.$field['unique_name'].']') );
					break;									
					case "address":			
					case "textarea":
						echo "<textarea id='field-".$field['unique_name']."' class='company_fields text' name='fields[".$field['unique_name']."]' rows='3' cols='40' >".esc_html( $value )."</textarea>";
					break;				
					case "phone":	
					case "text":		
					case "email":				
					default:				
						echo "<input type='text' id='field-".$field['unique_name']."' class='company_fields' value='".esc_attr( $value )."' name='fields[".$field['unique_name']."]' />";
					break;
				}
				echo "</td></tr>";
			}		
		}
		?>
		</table>
		<?php			
	}

	function display_document_sidebar( )
	{	
	
	}
	
	function acc_numbers( $account  )
	{
		?>
		<div id="acc_number-<?php echo $account->id; ?>" class="usam_document_container" data-item-id="<?php echo $account->id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Реквизит счета', 'usam' ); ?></div>
				<div class="usam_document_container-action-block">					
					<div class="usam_document_container-action"><a id = "bitton_delete" title='<?php _e( 'Удалить документ', 'usam' ); ?>'><?php _e( 'Удалить', 'usam' ); ?></a></div>
					<div class="usam_document_container-action"><a href="" id="button_edit"><?php _e( 'Редактировать', 'usam' ); ?></a></div>
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>			
			<div class="usam_document_container-content">
				<?php $this->display_document_sidebar(); ?>
				<div class="usam_document_container-right">				
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Банк', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
							<tbody>						
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'Наименование банка', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option">
										<span class = "usam_display_data"><?php echo htmlspecialchars($account->name); ?></span><input maxlength='255' size="255" type="text" name="acc[<?php echo $account->id; ?>][name]" class = "usam_edit_data" id="acc_bank_name" value="<?php echo $account->name; ?>">
									</td>
								</tr>							
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'БИК', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option">
										<span class = "usam_display_data"><?php echo $account->bic; ?></span><input maxlength='9' size="9" type="text" name="acc[<?php echo $account->id; ?>][bic]" class = "usam_edit_data" id="acc_bank_bik" value="<?php echo $account->bic; ?>">
										</td>
								</tr>	
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'Адрес банка', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option">
										<span class = "usam_display_data"><?php echo htmlspecialchars($account->address); ?></span><textarea maxlength='255' size="255" type="text" name="acc[<?php echo $account->id; ?>][address]" class = "usam_edit_data" id="acc_bank_address"><?php echo $account->address; ?></textarea>
									</td>
								</tr>
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'SWIFT', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option">
										<span class = "usam_display_data"><?php echo $account->swift; ?></span><input size="11" maxlength='11' type="text" name="acc[<?php echo $account->id; ?>][swift]" class = "usam_edit_data" id="acc_bank_swift" value="<?php echo $account->swift; ?>">
									</td>
								</tr>							
							</tbody>
						</table>
					</div>			
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Счёт', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
							<tbody>
								<tr>
									<td class="usam_table_cell_name"><?php _e( 'Расчетный счёт', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option tal">
										<span class = "usam_display_data"><?php echo $account->number; ?></span><input size="50" maxlength='50' type="text" name="acc[<?php echo $account->id; ?>][number]" class = "usam_edit_data <?php echo $account->id?'required':''; ?>" id="acc_bank_number" value="<?php echo $account->number; ?>"
									</td>
								</tr>
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'Кор. счёт', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option">
										<span class = "usam_display_data"><?php echo $account->bank_ca; ?></span><input size="50" maxlength='50' type="text" name="acc[<?php echo $account->id; ?>][bank_ca]" class = "usam_edit_data" id="acc_bank_ca" value="<?php echo $account->bank_ca; ?>">
									</td>
								</tr>								
								<tr>
									<td class="usam_table_cell_name" class = "name"><?php _e( 'Валюта', 'usam' ); ?>:</td>
									<td class="usam_table_cell_option tal">
										<span class = "usam_display_data"><?php echo usam_get_currency_name( $account->currency ); ?></span>
										<?php usam_select_currencies( $account->currency, array( "name" => "acc[$account->id][currency]", "class" => "usam_edit_data", "id" =>"acc_bank_currency" ) ); ?>
									</td>
								</tr>																
							</tbody>
						</table>
					</div>	
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Комментарий', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
							<tbody>					
								<tr>								
									<td class="usam_table_cell_option tal">
										<span class = "usam_display_data"><?php echo htmlspecialchars($account->note); ?></span><textarea maxlength='255' size="255" type="text" name="acc[<?php echo $account->id; ?>][note]" class = "usam_edit_data" id="acc_bank_note"><?php echo $account->note; ?></textarea>
									</td>
								</tr>														
							</tbody>
						</table>
					</div>	
					<input type="hidden" name="acc_ids[]" value="<?php echo $account->id; ?>"/>				
				</div>				
			</div>	
		</div>
		<?php
	}
	
	function company_acc_numbers(  )
	{
		if( !empty($this->bank_accounts) )
		{
			foreach( $this->bank_accounts as $account )
			{
				$this->acc_numbers( $account );
			}			
		} 
		$account = array( 'id' => 0, 'name' => '', 'bic' => '', 'address' => '', 'swift' => '', 'number' => '', 'bank_ca' => '', 'currency' => get_option('usam_currency_type'), 'note' => '' );
		$this->acc_numbers( (object)$account );
		?>	
		<div class ="buttons">
			<button id='show_add' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>
			<button id='hide_add' type="button" class="button" style="display: none;"><?php _e( 'Отменить', 'usam' ); ?></button>
		</div>
		<?php			
	}  	
	
	function display_right()
	{			
		$this->display_imagediv( $this->data['logo'] );
	}
	
	function display_left()
	{	
		$this->titlediv( $this->data['name'] );
		$this->add_box_description( $this->data['description'] );
		usam_add_box( 'usam_company', __('Параметры','usam'), array( $this, 'box_company' ) );
		usam_add_box( 'usam_means_communication', __('Связаться','usam'), array( $this, 'section_means_communication' ) );
		
		$groups = get_option( 'usam_crm_company_fields_group' );		
		foreach( $groups as $group )
			usam_add_box( 'usam_company_fields', $group['name'], array( $this, 'company_fields' ), $group['id'] );
			
		usam_add_box( 'usam_company_acc_number', __('Банковские счета','usam'), array( $this, 'company_acc_numbers' ), '', true );
    }	
}
?>