<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/edit-form-document.php' );
class USAM_Form_suggestions extends USAM_Form_Document
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить коммерческое предложение №%s','usam'), $this->data['number'] );
		else
			$title = __('Добавить коммерческое предложение', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		global $user_ID;
		$this->text1 = __('Коммерческое предложение для','usam');		
		$this->text2 = __('Контакты чувствующие в коммерческом предложении','usam');
		if ( $this->id != null )
		{
			$document = new USAM_Document( $this->id );	
			$this->data = $document->get_data();	
			$this->data['products'] = $document->get_products();	
			$this->data['contacts'] = $document->get_contacts();
			
			add_action( 'admin_footer', array(&$this, 'admin_footer') );
		}
		else
			$this->data = array( 'name' => '', 'number' => $this->id, 'bank_account_id' => '', 'customer_id' => 0, 'customer_type' => 'company', 'manager_id' => $user_ID, 'type_price' => '', 'description' => '', 'notes' => '', 'status' => 1, 'closedate' => date( "Y-m-d H:i:s") );	
	}	  
	
	public function setting( )
	{  	
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>										
				<tr>					
					<td class="name"><?php _e('Номер','usam'); ?>:</td>
					<td>
						<span class ="usam_display_data"><?php echo $this->data['number']; ?></span>
						<span class ="usam_edit_data"><input type='text' name='number' value="<?php echo $this->data['number']; ?>"/></span>
					</td>
				</tr>	
				<tr>
					<td class="name"><?php _e('Ваша фирма','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_display_company_by_acc_number( $this->data['bank_account_id'] ); ?></span>
					<span class ="usam_edit_data"><?php usam_select_bank_accounts( $this->data['bank_account_id'], array('name' => 'bank_account_id') ) ?></span></td>
				</tr>	
				<tr>
					<td class="name"><?php _e('Статус','usam'); ?>:</td>					
					<td><span class ="usam_display_data"><?php echo usam_get_status_name_document_suggestion( $this->data['status'] ); ?></span>
						<select class="chzn-select-nosearch-load usam_edit_data" name = "status">
							<?php 
							$status = usam_get_status_suggestion();	
							foreach ( $status as $key => $name )
							{	 ?>	
								<option value='<?php echo $key; ?>' <?php echo ( $key == $this->data['status']?'selected="selected"':'') ?>><?php echo $name; ?></option>	
							<?php 
							}
							?>	
						</select>
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_customer_name( $this->data['manager_id'] ); ?></span>
						<?php usam_select_manager( $this->data['manager_id'], array( 'name' => "manager_id",'class' => "usam_edit_data" ) ); ?>
					</td>	
				</tr>					
				<tr>
					<?php $date = empty($this->data['closedate'])?'':date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($this->data['closedate'], "Y-m-d H:i:s"))); ?>
					<td class="name"><?php _e('Срок','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo $date; ?></span><span class ="usam_edit_data"><?php usam_display_datetime_picker( 'closedate', $this->data['closedate'] ); ?></span></td>
				</tr>						
			</tbody>
		</table>
      <?php
	} 
	
	function display_left()
	{				
		$this->titlediv( $this->data['name'] );		
		usam_add_box( 'usam_section_customers', __('Клиент','usam'), array( $this, 'section_customers' ), '', true );	
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'setting' ), '', true );
		usam_add_box( 'usam_products', __('Товары','usam'), array( $this, 'section_products' ), '', true  );	
		$this->add_box_description( $this->data['description'] );
		$this->add_box_description( $this->data['conditions'], 'conditions', __('Условия','usam') );					
		$this->add_box_description( $this->data['notes'], 'notes', __('Комментарий','usam') );				
    }
}
?>