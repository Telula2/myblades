<?php
class USAM_List_Table_Orders extends USAM_List_Table 
{		
	private $filter = true;		
	private $status = 'all_in_work';
	private $total_amount = 0;	
	private $order_statuses = array();
	protected  $order = 'desc';	

	protected $search_box = false;		

	public function __construct( $args = array() ) 
	{		
		parent::__construct( $args );

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			$_SERVER['REQUEST_URI'] = wp_get_referer();	
		
		$statuses = usam_get_order_statuses( );	
		foreach ( $statuses as $status )					
			$this->order_statuses[$status->internalname] = $status;	

		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;			
	}		
	
	public function prepare_items() 
	{
		global $user_ID;		
	
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
  
		$args = array( 'cache_results' => true, 'cache_order_documents' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby);	
		$view_group = usam_get_user_order_view_group( );
		if ( !empty($view_group) )
		{ 
			if ( !empty($view_group['type_prices']) )
				$args['type_prices'] = $view_group['type_prices'];			
		}	
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{					
			$args = array_merge ($args, $this->get_date_for_query() );
			
			$args['customer_id'] = $this->id;			
			$args['date_group'] = isset( $_REQUEST['dgo'] ) ? $_REQUEST['dgo'] : 0;					
			if ( isset( $_REQUEST['manager'] ) )
				$args['manager_id'] = absint($_REQUEST['manager']);
			
			if ( isset($_REQUEST['paid']) && $_REQUEST['paid'] !== '' )
				$args['paid'] = absint($_REQUEST['paid']);
			
			if ( !empty( $_REQUEST['t_price'] ) )
				$args['type_prices'] = sanitize_title($_REQUEST['t_price']);			
			
			if ( !empty( $_REQUEST['payer'] ) )
				$args['type_payer'] = absint($_REQUEST['payer']);
			
			if ( !empty( $_REQUEST['payment'] ) )
				$args['payment'] = absint($_REQUEST['payment']);
			
			if ( !empty( $_REQUEST['shipping'] ) )
				$args['shipping_method'] = absint($_REQUEST['shipping']);
			
			if ( !empty( $_REQUEST['storage_pickup'] ) )
				$args['storage_pickup'] = absint($_REQUEST['storage_pickup']);			
			
			if ( isset( $_REQUEST['user'] ) && $_REQUEST['user'] != '' )
			{
				if ( is_numeric( $_REQUEST['user'] ) )			
					$args['user_ID'] = absint($_REQUEST['user']);
				else								
					$args['user_login'] = sanitize_title($_REQUEST['user']);				
			}				
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => $_REQUEST['selectc'], 'compare' => $_REQUEST['compare'], 'value' => $_REQUEST['condition_v'] ) );				
			}
		}			
		$query_orders = new USAM_Orders_Query( $args );
		$this->items = $query_orders->get_results();
		$this->total_amount = $query_orders->get_total_amount();		
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}			
	}	
/* 
	Описание: Заголовок таблицы в журнале продаж
*/
	public function get_columns()
	{
		return array(
			'cb'       => '<input type="checkbox" />',
			'id'       => __( 'Номер заказа', 'usam' ),			
			'totalprice' => __( 'Сумма', 'usam' ),			
			'status'   => __( 'Статус', 'usam' ),			
			'date'     => __( 'Дата', 'usam' ),				
			'shipping' => __( 'Доставка', 'usam' ),				
			'method'   => __( 'Способ оплаты', 'usam' ),
			'date_paid' => __( 'Дата оплаты', 'usam' ),	
			'address'   => __( 'Адрес доставки', 'usam' ),		
			'manager'   => __( 'Менеджер', 'usam' ),			
		);
	}

	public function get_sortable_columns() 
	{
		if ( ! $this->sortable )
			return array();
		return array(
			'date_paid' => 'date_paid',
			'date'      => 'id',
			'date_modified' => 'date_modified',
			'id'        => 'id',
			'status'    => 'status',
			'totalprice' => 'totalprice',	
			'manager'   => 'manager',						
		);
	}
	
	public function extra_tablenav( $which ) 
	{
		?><div class="alignleft actions"><?php
		if ( 'top' == $which ) 
		{
			global $current_screen;					
			$customer = isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'contacts'?'contact':'company';
			?>
			<ul class="list_button">				
				<?php $sendback = wp_nonce_url( add_query_arg( array('page' => 'orders', 'tab' => 'orders', $customer => $this->id, 'action' => 'new' ), admin_url('admin.php') ), 'usam-'.$current_screen->id); ?>
				<li><a href="<?php echo $sendback; ?>" target="_blank" class="button"><?php _e('Новый заказ','usam'); ?></a></li>				
			</ul>
			<?php	
		}
		?></div><?php
	}
	
	public function pagination( $which )
	{
		ob_start();
		parent::pagination( $which );
		$output = ob_get_clean();
		if ( $this->status == 'all' )
			$string = _x( 'Всего (без учета неполных и отклоненных): %s', 'Сумма страницы журнала продаж', 'usam' );
		else
			$string = _x( 'Всего: %s', 'Сумма страницы журнала продаж', 'usam' );
		$total_amount = ' - ' . sprintf( $string, usam_currency_display( $this->total_amount ) );
		$total_amount = str_replace( '$', '\$', $total_amount );
		$output = preg_replace( '/(<span class="displaying-num">)([^<]+)(<\/span>)/', '${1}${2}'.' '.$total_amount.'${3}', $output );
		echo $output;
	}
	
	private function item_link( $id, $text )
	{	
		$result = '<a class="row-title" href="'.esc_url( usam_get_url_order( $id ) ).'" title="'.esc_attr__( 'Посмотреть детали заказа', 'usam' ).'">'.$text.'</a>';
		return $result;
	}
	
	public function column_address( $item )
	{			
		$shippingaddress = '';
		$customer_data = $this->order->get_customer_data();
		if ( isset($customer_data['shippingaddress']) )
			$shippingaddress = !empty($customer_data['shippingaddress'])?$customer_data['shippingaddress']['value'].' ':'';
		elseif ( isset($customer_data['company_shippingaddress']) )
			$shippingaddress = !empty($customer_data['company_shippingaddress'])?$customer_data['company_shippingaddress']['value'].' ':'';
			
		?><a target="_blank" href="https://maps.yandex.ru/?text=<?php echo urlencode($shippingaddress); ?>"><?php echo $shippingaddress; ?></a><?php			
	}
	
	public function column_id( $item ) 
	{
		echo $this->item_link( $item->id, $item->id );	
	}
	
	public function column_date_paid( $item ) 
	{			
		if ( $item->date_paid )
			echo usam_local_date( $item->date_paid, get_option( 'date_format', 'Y/m/d' ) );
	}	
	
	public function column_totalprice( $item ) 
	{
		echo $this->item_link( $item->id, usam_currency_display( $item->totalprice ) );
	}
	
	public function column_status( $item ) 
	{				
		if ( usam_check_order_is_completed( $item->status ) )
		{
			echo '<span class="order_closed">'.$this->order_statuses[$item->status]->name.'</span>';		
			if ( $item->status == 'canceled' && !empty($item->cancellation_reason) )
			{
				echo '<hr size="1" width="90%">';
				echo "<p>$item->cancellation_reason</p>";
			}
		}
		else
		{			
			echo '<select class="usam-order-status" data-log-id="'.$item->id.'">';
			foreach ( $this->order_statuses as $status ) 
			{						
				if ( $status->internalname == 'closed' && $item->paid != 2 )
					continue;
					
				$style = $status->color != ''?'style="color:'.$status->color.'"':'';
				
				if ( $status->visibility || $status->internalname == $item->status )
					echo '<option '.$style.' value="'.esc_attr( $status->internalname ).'" '.selected($status->internalname, $item->status, false) . '>'.esc_html( $status->name ). '</option>';
			}
			echo '</select>';
			usam_loader(); 
		}
	}
	
	public function column_shipping( $item )
	{			
		$shipped_documents = $this->order->get_shipped_documents();	
		$i = 0;		
		foreach ( $shipped_documents as $document )
		{	
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			if ( !empty($document->storage_pickup) )
			{	
				$storage = usam_get_store_field( $document->storage_pickup, 'title' );
				$storage_pickup = "<br>".$storage;							
			}		
			else
				$storage_pickup = '';
			$date_allow_delivery = !empty($document->date_allow_delivery)?"<br>".usam_local_date( $document->date_allow_delivery ):'';
			
			
			echo "<strong>".$document->name."</strong>$storage_pickup<br>".usam_currency_display($document->price).$date_allow_delivery."<br><strong>".usam_get_shipped_document_status_name( $document->status )."</strong>";			
		}		
	}
	
	public function column_method( $item )
	{	
		$payment_documents = $this->order->get_payment_history();			
		$i = 0;
		foreach ( $payment_documents as $document )
		{
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			$date_payed = empty($document['date_payed'])?'':' - '.usam_local_date( $document['date_payed'], get_option( 'date_format', 'Y/m/d' ) );
			echo "<strong>".$document['name']."</strong><br>".usam_currency_display($document['sum'])."<br>(".usam_get_payment_document_status_name($document['status'])."$date_payed)";			
		}
	}
		
	public function single_row( $item ) 
	{
		$this->order = new USAM_Order( $item->id );
		echo '<tr>';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
}