<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_calendars extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить календарь %s','usam'), $this->data['name'] );
		else
			$title = __('Добавить календарь', 'usam');	
		return $title;
	}	
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )					
			$this->data = usam_get_data($this->id, 'usam_calendars');
		else	
			$this->data = array( 'name' => '', 'sort' => 100 );
	}	
		
    public function settings( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>							
				<tr>				
					<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
					<td>
						<input type="text" name="calendar[sort]" autocomplete="off" value="<?php echo $this->data['sort']; ?>"/>
					</td>
				</tr>									
			</tbody>
		</table>
      <?php
	}      
	
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );	
		usam_add_box( 'usam_calendar', __('Параметры','usam'), array( $this, 'settings' ) );		
    }
}
?>