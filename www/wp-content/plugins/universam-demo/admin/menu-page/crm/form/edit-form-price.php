<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Price extends USAM_Edit_Form
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить прайс-лист № %s','usam'), $this->id );
		else
			$title = __('Добавить прайс-лист', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
			$this->data = usam_get_data($this->id, 'usam_price_list_setting');
		else
			$this->data = array('title' => '', 'description' => '', 'start_date' => '', 'end_date' => '', 'active' => 1, 'roles' => array(), 'tax' => array( 'category' => array(), 'brand' => array() ), 'product_group' => 'all');	
	}	
	
	public function box_column_price_list(  )
	{   		
		$columns = array( 'title' => __('Название товара','usam'), 'category' => __('Название категории','usam'), 'brands' => __('Бренд','usam'), 'price' => __('Цена','usam'), 'sku' => __('Артикул','usam'), 'stock' => __('Остаток','usam'), 'stock_title' => __('Статус остатка','usam'), 'link' => __('Ссылка','usam'), 'image' => __('Ссылка на основную фотографию','usam') );
		?>
	<div class="container_column">			
		<div class="column1" id="rule-descrip">  
			<p><?php _e('Колонки в прайс-листе в файле.', 'usam'); ?></p>
		</div>
		<div class="column2" id="rule-choice">
			<h3><?php _e('Выберите колонки', 'usam'); ?></h3>
			<div id="rule-radio">
				<span id="rule-selected">	
					<?php
					$str = "";
					foreach ( $columns as $key => $column )
					{		
						?> 
						<input type="checkbox" id = "column_<?php echo $key; ?>" class = "show_help" name="price_list[column][<?php echo $key; ?>]" value="<?php echo $key; ?>" <?php if ( isset($this->data['column'][$key]) ) { echo "checked='checked'"; } ?> /><?php echo $column; ?><br/>
						<?php
					}		
					?>						
				</span>
			</div>                
		</div>		
	</div>		
	   <?php
	}	
	
	public function box_product_group( ) 
	{
		$checked = 'checked="checked"';	
		?>    
	<div class="container_column">			
		<div class="column1" id="inpop_descrip">
			<h4><?php _e('Выберите, группы для включения в прайс-лист', 'usam') ?></h4>
			<p><?php _e('Выберите, какие группы включать в Ваш прайс-лист.', 'usam') ?></p>
		</div>
		
		<div class="column2" id="type_search">       
		  <h3><?php _e('Выберите, группы', 'usam')?></h3>
		  <div id="inpopRadio">
				<input type="radio" name="price_list[product_group]" id = "type_search_all" class = "show_help" value="all" <?php if ( $this->data['product_group'] == 'all' ) { echo "checked='checked'"; } ?> /><?php _e('Распространить на все продукты в корзине', 'usam'); ?><br />
				<input type="radio" name="price_list[product_group]" id = "type_search_group" class = "show_help" value="group" <?php if ( $this->data['product_group'] == 'group' ) { echo "checked='checked'"; } ?> /><?php _e('Используя группы выбора', 'usam'); ?><br />
		  </div>			  
		</div>			
		
		<div class="column3">
			<div class="box_help_setting <?php if ( $this->data['product_group'] == 'group' ) { echo "hidden"; } ?>" id="type_search_all">
				<h4><?php _e('Применить ко всем продуктам', 'usam')?><span> - <?php _e('объяснение', 'usam')?></span></h4>
				<p><?php _e('Начальное логическое соотношение относится ко всем продуктам, которые можно найти.', 'usam')?></p>
			</div>
			<div class="box_help_setting <?php if ( $this->data['product_group'] == 'all' ) { echo "hidden"; } ?>" id="type_search_group">
				<h4><?php _e('Использование группы выбора', 'usam')?><span> - <?php _e('объяснение', 'usam')?></span></h4> 
				<p><?php _e('Использовуя группы выбора, вы можете задать группы выбора, сосредоточив внимание на некоторых товарах.', 'usam')?> </p>
			</div>
		</div> 	
		
	
		<div id="all_taxonomy" class="all_taxonomy <?php if ( $this->data['product_group'] == 'all' ) { echo "1hidden"; } ?>">	
			<div id="prodcat-in" class="taxonomy_box">
				<h3><?php _e('Категории товаров', 'usam')?></h3>
				<?php  $this->checklist_meta_box( 'usam-category', $this->data['tax']['category'] ); ?>
			</div>
			
			<div id="prodcat-in" class="taxonomy_box">
				<h3><?php _e('Бренды', 'usam')?></h3>
				<?php  $this->checklist_meta_box( 'usam-brands', $this->data['tax']['brands'] ); ?>
			</div>
		</div>			
	</div>		
	   <?php   
	}	
	
	public function box_customers_roles( )
	{   			
		?>
		<div class="container_column">			
			<div class="column1" id="rule-descrip">  
				<p><?php _e('Выберете группы покупателей, которые смогут просматривать прайс лист.', 'usam'); ?></p>
			</div>
			<div class="column2" id="rule-choice">			
				<div id="all_taxonomy" class="all_taxonomy">	
					<?php  $this->display_meta_box_group( 'roles', $this->data['roles'] ); ?>
				</div>	     
			</div>
		</div>
	   <?php
	}	
	
	function display_left()
	{		
		$this->titlediv( $this->data['title'] );


		$help = array( array( 'title' => __('Статус файла доступен', 'usam'), 'description' => __('Файл доступен для скачивания выбранными ниже покупателями.', 'usam') ),
				array( 'title' => __('Статус файла не доступен', 'usam'), 'description' => __('Файл не доступен для скачивания выбранными ниже покупателями.', 'usam') ));
				
		$title = __('Файл может быть доступен для скачивания или недоступен. Вы можете файл сделать недоступным тогда, когда Вы не хотите потерять настройки и хотите чтобы его не могли скачивать сейчас.', 'usam');				
		
		$this->add_box_status_active( $this->data['active'], $title, $help );
		usam_add_box( 'usam_customers_roles', __('Группы покупателей','usam'), array( $this, 'box_customers_roles' ) );
		usam_add_box( 'usam_product_group', __('Группы товаров','usam'), array( $this, 'box_product_group' ) );	
		usam_add_box( 'usam_column_price_list', __('Колонки прайс-листа','usam'), array( $this, 'box_column_price_list' ) );		
    }	
}
?>