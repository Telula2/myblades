<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/crm/includes/affairs_view_form_table.php' );		
class USAM_List_Table_affairs extends USAM_List_Table_Affairs_View_Form 
{	
	public function extra_tablenav( $which ) 
	{
		?><div class="alignleft actions"><?php
		if ( 'top' == $which ) 
		{
			?>
			<ul class ="list_button">
				<li><a href="" target="_blank" class="usam-send_message-link button"><?php _e('Отправить сообщение','usam'); ?></a></li>					
				<li><a href="" target="_blank" class="usam-add_meeting-link button"><?php _e('Добавить встречу','usam'); ?></a></li>					
				<li><a href="" target="_blank" class="usam-add_phone-link button"><?php _e('Добавить звонок','usam'); ?></a></li>		
				<li><a href="" target="_blank" class="usam-add_reminder-link button"><?php _e('Добавить событие','usam'); ?></a></li>					
			</ul>
			<?php				
		}
		?></div><?php
	}	
	
	function prepare_items() 
	{		
		global $user_ID;
		
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		
		$args = array( 'object_type' => $this->customer, 'object_id' => $this->id, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'search' => $this->search, 'orderby' => $this->orderby, 'order' => $this->order );		
		
		$query = new USAM_Tasks_Query( $args );
		$this->items = $query->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}