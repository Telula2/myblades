<?php			
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/edit-form-document.php' );
class USAM_Form_invoice extends USAM_Form_Document
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить счет №%s от %s','usam'), $this->data['number'], usam_local_date( $this->data['date_insert'], "d.m.Y" ) );
		else
			$title = __('Добавить счет', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{			
		$this->text1 = __('Счет для','usam');		
		$this->text2 = __('Контакты чувствующие в счете','usam');		
		if ( $this->id != null )
		{
			$_invoice = new USAM_Document( $this->id );	
			$this->data = $_invoice->get_data();	
			$this->data['products'] = $_invoice->get_products();	
			$this->data['contacts'] = $_invoice->get_contacts();
			
			add_action( 'admin_footer', array(&$this, 'admin_footer') );
		}
		else
			$this->data = array( 'name' => '', 'manager_id' => '', 'type_price' => '', 'contact_id' => 0, 'conditions' => '', 'notes' => '', 'status' => '', 'closedate' => date( "Y-m-d H:i:s") );		
	}	
	
	public function setting( )
	{  	
		$contract_id = usam_get_document_metadata( $this->id, 'contract' ); 	
		$contract_display = '';
		if ( $contract_id )
		{ 
			$contract = usam_get_document( $contract_id );		
			if ( $contract )
				$contract_display = sprintf( __('№%s от %s','usam'), $contract['number'], usam_local_date( $contract['date_insert'], "d.m.Y" ) );
		}		
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>										
				<tr>					
					<td class="name"><?php _e('Номер','usam'); ?>:</td>
					<td>
						<span class ="usam_display_data"><?php echo $this->data['number']; ?></span>
						<span class ="usam_edit_data"><input type='text' name='number' value="<?php echo $this->data['number']; ?>"/></span>
					</td>
				</tr>								
				<tr>
					<td class="name"><?php _e('Ваша фирма','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_display_company_by_acc_number( $this->data['bank_account_id'] ); ?></span>
					<span class ="usam_edit_data"><?php usam_select_bank_accounts( $this->data['bank_account_id'], array('name' => 'bank_account_id') ) ?></span></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Договор','usam'); ?>:</td>					
					<td>
						<?php
						if ( $this->data['customer_id'] )
						{
							if ( $this->data['customer_type'] == 'company' )		
								$args = array( 'companies' => $this->data['customer_id'] );		
							else 
								$args = array( 'contacts' => $this->data['customer_id'] );
							?>
							<span class ="usam_display_data"><?php echo $contract_display; ?></span>
							<span class ="usam_edit_data"><?php usam_select_contracts( $contract_id, array('name' => 'contract'), $args ) ?></span>
							<?php
						}
						else
							_e('Выбирете фирму','usam');
						?>						
					</td>
				</tr>					
				<tr>
					<td class="name"><?php _e('Статус','usam'); ?>:</td>					
					<td><span class ="usam_display_data"><?php echo usam_get_status_name_document_invoice( $this->data['status'] ); ?></span>
						<select class="chzn-select-nosearch-load usam_edit_data" name = "status">
							<?php 
							$status = usam_get_status_invoice();	
							foreach ( $status as $key => $name )
							{	 ?>	
								<option value='<?php echo $key; ?>' <?php echo ( $key == $this->data['status']?'selected="selected"':'') ?>><?php echo $name; ?></option>	
							<?php 
							}
							?>	
						</select>
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_customer_name( $this->data['manager_id'] ); ?></span>
						<?php usam_select_manager( $this->data['manager_id'], array( 'name' => "manager_id",'class' => "usam_edit_data" ) ); ?>
					</td>	
				</tr>					
				<tr>
					<?php $date = empty($this->data['closedate'])?'':date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($this->data['closedate'], "Y-m-d H:i:s"))); ?>
					<td class="name"><?php _e('Срок','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo $date; ?></span><span class ="usam_edit_data"><?php usam_display_datetime_picker( 'closedate', $this->data['closedate'] ); ?></span></td>
				</tr>									
			</tbody>
		</table>
      <?php
	} 	

	function display_left()
	{						
		$this->titlediv( $this->data['name'] );	
		usam_add_box( 'usam_section_customers', __('Клиент','usam'), array( $this, 'section_customers' ), '', true  );
		usam_add_box( 'usam_invoice', __('Параметры','usam'), array( $this, 'setting' ), '', true );		
		usam_add_box( 'usam_products', __('Товары','usam'), array( $this, 'section_products' ), '', true  );
		$this->add_box_description( $this->data['conditions'], 'conditions', __('Условия и комментарии','usam') );				
		$this->add_box_description( $this->data['notes'], 'notes', __('Примечание менеджера','usam') );							
    }	
}
?>