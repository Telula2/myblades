<?php			
require_once( USAM_FILE_PATH . '/admin/menu-page/crm/includes/edit-form-document.php' );
class USAM_Form_contracts extends USAM_Form_Document
{		
	protected $formtype = true;
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить договор №%s от %s','usam'), $this->data['number'], usam_local_date( $this->data['date_insert'], "d.m.Y" ) );
		else
			$title = __('Добавить договор', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{			
		$this->text1 = __('Договор с','usam');		
		$this->text2 = __('Контакты учавствующие в договоре','usam');		
		if ( $this->id != null )
		{
			$_invoice = new USAM_Document( $this->id );	
			$this->data = $_invoice->get_data();			
			$this->data['contacts'] = $_invoice->get_contacts();
			
			add_action( 'admin_footer', array(&$this, 'admin_footer') );
		}
		else
			$this->data = array( 'name' => '', 'manager_id' => '', 'type_price' => '', 'contact_id' => 0, 'conditions' => '', 'notes' => '', 'status' => '', 'closedate' => date( "Y-m-d H:i:s") );		
	}	
	
	public function customer()
	{
		$links = array( 			
			array( 'id' => 'open_send_email', 'action_url' => usam_url_admin_action( 'send_email_to_document', admin_url('admin.php'), array( 'document' => $this->data['type'], 'id' => $this->id ) ), 'title' => esc_html__( 'Отправить на почту', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ), 			
		);							
		$this->actions_buttom( $links );
	}	
	
	public function display_file( )
	{  		
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>										
				<tr>					
					<td class="name"><?php _e('Файл договора','usam'); ?>:</td>
					<td>
						<span class ="usam_display_data"><?php 
						$file = usam_get_document_metadata($this->id, 'file'); 
						if ( $file )
							echo usam_get_form_attachments( array( array( 'title' => $file['name'], 'url' => USAM_DOCUMENTS_URL.'/'.$this->id.'/'.$file['file'], 'path' => USAM_DOCUMENTS_DIR.$this->id.'/'.$file['file'])) );						
						else
							_e('не загружен файл договора','usam')
						?>
						</span>
						<span class ="usam_edit_data"><input type="file" name="document_file" value=""></span>
					</td>
				</tr>							
			</tbody>
		</table>
      <?php
	} 
	
	public function setting( )
	{  	
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>										
				<tr>					
					<td class="name"><?php _e('Номер','usam'); ?>:</td>
					<td>
						<span class ="usam_display_data"><?php echo $this->data['number']; ?></span>
						<span class ="usam_edit_data"><input type='text' name='number' value="<?php echo $this->data['number']; ?>"/></span>
					</td>
				</tr>								
				<tr>
					<td class="name"><?php _e('Ваша фирма','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_display_company_by_acc_number( $this->data['bank_account_id'] ); ?></span>
					<span class ="usam_edit_data"><?php usam_select_bank_accounts( $this->data['bank_account_id'], array('name' => 'bank_account_id') ) ?></span></td>
				</tr>							
				<tr>
					<td class="name"><?php _e('Статус','usam'); ?>:</td>					
					<td><span class ="usam_display_data"><?php echo usam_get_status_name_document_contract( $this->data['status'] ); ?></span>
						<select class="chzn-select-nosearch-load usam_edit_data" name = "status">
							<?php 
							$status = usam_get_status_contract();	
							foreach ( $status as $key => $name )
							{	 ?>	
								<option value='<?php echo $key; ?>' <?php echo ( $key == $this->data['status']?'selected="selected"':'') ?>><?php echo $name; ?></option>	
							<?php 
							}
							?>	
						</select>
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo usam_get_customer_name( $this->data['manager_id'] ); ?></span>
						<select class="chzn-select-nosearch-load usam_edit_data" name = "manager_id">
							<option value="0" <?php echo ($this->data['manager_id'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
							<?php	
							$args = array( 'orderby' => 'nicename', 'role__in' => array('editor','shop_manager','administrator'), 'fields' => array( 'ID','display_name') );
							$users = get_users( $args );
							foreach( $users as $user )
							{						
								?>               
								<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['manager_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->display_name; ?></option>
								<?php
							}
							?>
						</select>				
					</td>	
				</tr>					
				<tr>
					<?php $date = empty($this->data['closedate'])?'':date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($this->data['closedate'], "Y-m-d H:i:s"))); ?>
					<td class="name"><?php _e('Срок','usam'); ?>:</td>
					<td><span class ="usam_display_data"><?php echo $date; ?></span><span class ="usam_edit_data"><?php usam_display_datetime_picker( 'closedate', $this->data['closedate'] ); ?></span></td>
				</tr>									
			</tbody>
		</table>
      <?php
	} 

	function display_left()
	{						
		$this->titlediv( $this->data['name'] );	
		usam_add_box( 'usam_section_customers', __('Клиент','usam'), array( $this, 'section_customers' ), '', true  );
		usam_add_box( 'usam_setting', __('Параметры','usam'), array( $this, 'setting' ), '', true );		
		usam_add_box( 'usam_file', __('Файл договора','usam'), array( $this, 'display_file' ), '', true );	
		$this->add_box_description( $this->data['conditions'], 'conditions', __('Условия и комментарии','usam') );				
		$this->add_box_description( $this->data['notes'], 'notes', __('Примечание менеджера','usam') );							
    }	
	
	public function get_modal_window()
	{			
		$to_email = $this->get_to_email();
		
		$subject = 	__( 'Договор', 'usam' ).' - '.$this->data['name'];		
			
		$args = array( 'form_url' => usam_url_admin_action( 'send_email', admin_url('admin.php'), array( 'do_action_send_email' => 'document', 'id' => $this->id ) ), 'object_id' => $this->data['id'], 'object_type' => $this->data['type'], 'customer_type' => $this->data['customer_type'], 'customer_id' => $this->data['customer_id'], 'to_email' => $to_email, 'subject' => $subject, 'message' => '' );
		
		$file = usam_get_document_metadata( $this->data['id'], 'file');		 
		if ( !empty($file) )
		{
			$attachments = array( array( 'url' => USAM_DOCUMENTS_URL.$this->data['type'].'/'.$file['file'], 'path' => USAM_DOCUMENTS_DIR.$this->data['type'].'/'.$file['file'], 'title' => $file['name'] ) );
		}
		else
			$attachments = array();	
		
		$out = usam_get_form_send_message( $args, $attachments );
		return $out;
	}	
}
?>