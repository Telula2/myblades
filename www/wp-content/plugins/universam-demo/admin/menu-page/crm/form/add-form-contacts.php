<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_contacts extends USAM_Edit_Form
{	
	protected $formtype = true;
	private $default;
	private $communications = array();	
	
	protected function get_title_tab()
	{ 			
		return __('Добавить новый контакт', 'usam');
	}
	
	protected function get_data_tab()
	{ 	
		$this->default = array( 'contact_source' => '', 'manager_id' => '', 'user_id' => '', 'open' => 1, 'sex' => '', 'lastname' => '', 'firstname' => '', 'patronymic' => '', 'birthday' => '', 'post' => '', 'address' => '', 'address2' => '', 'postal_code' => '', 'about_contact' => '', 'foto' => 0, 'location_id' => 0  );
		$this->communications = array( 'email' => array(), 'phone' => array(),'site' => array(),'social' => array() );	
	}
}
?>