<?php
class USAM_Export_List_Table_affairs extends USAM_Export_List_Table
{		
	function column_user( $item )
	{
		if ( $item->user_id != 0)
		{	
			$user = get_user_by('id', $item->user_id);
			$manager = isset($user->display_name)?"$user->display_name ({$user->user_login})":'';
			echo $manager;
		}		
	}
	
	function column_object_type( $item )
	{
		$objects = usam_get_event_objects_all( $item->id ) ;
		foreach ( $objects as $object )
		{		
			echo usam_get_name_type_event_object ( $object->object_type );
			echo usam_get_display_event_object( $object );
		}
	} 

	function column_time_work( $item )
	{
		if ( !empty($item->start) && !empty($item->end) )
		{
			$time = round((strtotime($item->end) - strtotime($item->start)) / 60);
			if ( $time == 0 )
				_e( 'менее минуты', 'usam' );
			else
				echo $time." ".__("мин","usam");
		}
	} 	
	
	function column_time( $item )
	{		
		if ( !empty($item->start) )
			echo usam_local_date( $item->start, get_option( 'date_format', 'Y/m/d' ).' H:i' );
		if ( !empty($item->end) )
			echo " - ".usam_local_date( $item->end, get_option( 'date_format', 'Y/m/d' ).' H:i' );
	} 
	
	function column_status( $item )
	{ 
		if ( $item->status == 1 )
			_e('Выполняется','usam');
		elseif ( $item->status == 2 )
			_e('Остановлена','usam');
		elseif ($item->status == 3 )
			_e('Завершена','usam');
	} 
	
	function column_reminder( $item )
	{
		if ( !empty($item->date_time) )
			echo usam_local_date( $item->date_time, get_option( 'date_format', 'Y/m/d' ).' H:i' );
	} 
	
	function column_calendar_id( $item )
	{
		echo usam_get_calendar_name_by_id( $item->calendar );
	}    
}