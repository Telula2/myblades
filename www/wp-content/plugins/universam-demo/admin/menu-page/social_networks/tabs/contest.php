<?php
class USAM_Tab_contest extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Управление конкурсами', 'usam'), 'description' => __('Здесь вы можете создавать и управлять вашими конкурсами.', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}

	
	public function get_message()
	{		
		$message = '';
		
		if( isset($_REQUEST['uploaded']) && $_REQUEST['uploaded'] > 0 )
			$message = sprintf( _n( 'Загружена %s фотография.', 'Загружена %s фотография.', $_REQUEST['uploaded'], 'usam' ), $_REQUEST['uploaded'] );	
		if( isset($_REQUEST['public']) && $_REQUEST['public'] > 0 )
			$message .= sprintf( _n( 'Опубликован %s товар.', 'Опубликовано %s товаров.', $_REQUEST['public'], 'usam' ), $_REQUEST['public'] );
			
		return $message;
	} 
	
	protected function callback_submit()
	{			
		switch( $this->current_action )
		{			
			case 'delete':	
				usam_delete_data( $this->records, 'usam_vk_contest' );			
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records) ), $this->sendback );
			break;	
			case 'save':				
				if ( !empty($_POST['contest']) )
				{			
					$new = $_POST['contest'];		
					$new['date_modified'] =  date( "Y-m-d H:i:s" );	
					$new['title'] = sanitize_text_field(stripcslashes($_POST['name']));	
					$new['message'] = sanitize_textarea_field($_POST['message']);	
					$new['active'] = !empty($_POST['active'])?1:0;			
					
					$new['start_date'] = usam_get_datepicker('start');
					$new['end_date'] = usam_get_datepicker('end');	
					
					if ( $this->id != null )					
						usam_edit_data( $new, $this->id, 'usam_vk_contest' );			
					else			
					{				
						$new['date_insert'] = date( "Y-m-d H:i:s" );
						$this->id = usam_add_data( $new, 'usam_vk_contest' );	
					}
				}
			break;
		}			
	}
}