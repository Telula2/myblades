<?php
class USAM_Tab_vk_users extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Участники группы', 'usam'), 'description' => __('Здесь вы можете просматривать участников группы.','usam') );		
	}
	
	protected function load_tab()
	{
		global $profile_id;	
		
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();								
		$profiles = $vkontakte->user_and_group('group');
				
		if ( !empty($profiles) )
		{ 
			$profile_id = !empty($_REQUEST['profile_id'])?$_REQUEST['profile_id']:'';
			if ( $profile_id=='' )
			{
				$profile_id = $profiles[0]['page_id'];
			}			
		}
		else
			$profile_id = '';		
		$this->list_table();
	}	
	
	public function get_message()
	{		
		$message = '';
		if( isset($_REQUEST['birthday']) && $_REQUEST['birthday'] > 0 )
			$message .= sprintf( _n( 'Поздравлено %s участника группы.', 'Поздравлено %s участников группы.', $_REQUEST['birthday'], 'usam' ), $_REQUEST['birthday'] );	
			
		return $message;
	} 
	
	protected function callback_submit()
	{	
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();
		if ( !empty($_REQUEST['publish_birthday'] ) )
		{				
			$result = $vkontakte->publish_birthday();			
	
			$this->sendback = add_query_arg( array( 'birthday' => $result ), $this->sendback );
			$this->redirect = true;
		}	
		$errors = $vkontakte->get_errors();
		foreach ( $errors as $error ) 
			$this->set_user_screen_error( $error );
	}		
	
	public function tab_structure() 
	{			
		global $profile_id;
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();									
		$profiles = $vkontakte->user_and_group('group');
		?>
		<div class="profiles_panel">
			<div class="profiles_content">		
				<span class="name"><?php _e( 'Ваши анкеты и группы', 'usam' ); ?>:</span>
				<div class="select_profile">	
					<?php
					if ( empty($profiles) )
					{
						echo "<h4>".__( 'Все возможности скрыты, потому что Вы не настроили ни группы, ни анкеты. Сделайте это сейчас!', 'usam' )."</h4>";
					}
					else
					{
					?>
					<form method='GET' action=''>
					<input type='hidden' value='<?php echo $this->page_name; ?>' name='page' />
					<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />
					<select name ="profile_id" id ="profile_id" onchange="this.form.submit()">
						<?php
						foreach ( $profiles as $profile ) 
						{				
							$select = ($profile_id > 0 && $profile_id==$profile['page_id'])|| ( $profile_id < 0 && abs($profile_id)==$profile['page_id'])?1:0;					
							$sendback = add_query_arg( array( 'profile_id' => $profile['page_id'] ), $this->sendback );
							?>					
								<option <?php selected($select); ?> value="-<?php echo $profile['page_id']; ?>"><img src="<?php echo $profile['photo']; ?>"><?php echo $profile['name']; ?></option>
							<?php
						}
						?>
					</select>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php
		$this->list_table->display_table(); 
	}	
}