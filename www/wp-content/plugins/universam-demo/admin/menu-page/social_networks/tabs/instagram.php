<?php
class USAM_Tab_instagram extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Записи', 'usam'), 'description' => __('Здесь вы можете просматривать первые 20 записей.','usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
}