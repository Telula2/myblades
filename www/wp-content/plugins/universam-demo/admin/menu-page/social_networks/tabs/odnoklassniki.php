<?php
class USAM_Tab_odnoklassniki extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Управление сообществом и анкетами в одноклассниках', 'usam'), 'description' => 'Здесь вы можете публиковать товар на стены сообщества и анкет, добавлять фото в альбомы.' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}