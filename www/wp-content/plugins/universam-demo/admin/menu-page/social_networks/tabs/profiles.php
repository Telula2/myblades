<?php
class USAM_Tab_profiles extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Управление анкетами', 'usam'), 'description' => 'Здесь вы можете публиковать товар на стены сообщества и анкет, добавлять фото в альбомы.' );		
	}
	
	protected function load_tab()
	{
	//	$this->list_table();
	}
	
	
	public function get_message()
	{		
		$message = '';		
		if( isset($_REQUEST['add']) )
			$message = sprintf( _n( 'Добавлен %s друг.', 'Добавлено %s друзей.', $_REQUEST['add'], 'usam' ), $_REQUEST['add'] );
		return $message;
	} 
	
	protected function callback_submit()
	{				
		switch( $this->current_action )
		{		
			case 'add-friends':					
				$option = get_option('usam_vk_profile', '' );
				$vk_profile_new = $vk_profile = maybe_unserialize( $option );	
				if ( !empty($vk_profile) )
				{				
					require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
					$vkontakte = new USAM_VKontakte_API();	
					$i = 0;
					foreach ( $vk_profile as $key => $profile ) 
					{ 	
						$search = $_POST['search'];			
						$params = array_merge ($search, $profile);
						$result = $vkontakte->users_search( $params );
					
						if ( $result != 1 )
							usam_set_user_screen_error( $result, 'users_search' );	
							
						$vk_profile_new[$key]['offset'] += 5;
						$i++;
					}
					update_option( 'usam_vk_profile', serialize($vk_profile_new) );
					$this->sendback = add_query_arg( array( 'add' => $i ), $this->sendback );			
					$this->redirect = true;						
				}
			break;		
		}	
	}		
	
	public function display() 
	{	
		?>			
		<form action="" method="post">
			<table>
				<tr>
					<td><?php _e('Сколько добавить','usam'); ?></td>
					<td><input value='1' type='text' name='search[count]' /></td>
				</tr>
			</table>
			<input type="hidden" name="action" value="add-friends" />
			<?php 
			$this->nonce_field(); 				
			submit_button( __( 'Добавить друзей', 'usam' ), 'primary','action-submit', false, array( 'id' => 'add-friends-submit' ) );
			?>
		</form>
		<?php
	}	
}