<?php
class USAM_Tab_vk_products extends USAM_Tab
{
	protected  $form_method = 'POST';		
	public function __construct()
	{
		$this->header = array( 'title' => __('Управление публикацией вКонтакте', 'usam'), 'description' => __('Здесь вы можете публиковать товар, на стены сообщества и анкет, публиковать как товар вКонтакте, указывать расписание публикации, добавлять фото в альбомы и многое другое.','usam') );	
	}
	
	public function get_tables() 
	{ 
		$tables = array( 'vk_products' => array( 'title' => __('Товары','usam') ),  'vk_posts' => array( 'title' => __('Записи','usam') ) );		
		return $tables;
	}
	
	protected function load_tab()
	{
		global $profile_id;	
		
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();								
		$profiles = $vkontakte->user_and_group();
				
		if ( !empty($profiles) )
		{  
			$profile_id = !empty($_REQUEST['profile_id'])?$_REQUEST['profile_id']:'';
			if ( $profile_id=='' )
			{
				$profile_id = $profiles[0]['type_page'] == 'group'?'-'.$profiles[0]['page_id']:$profiles[0]['page_id'];
			}			
		}
		else
			$profile_id = '';	 
		$this->list_table();
	}
	
	public function get_message()
	{		
		$message = '';
		
		if( isset($_REQUEST['uploaded']) && $_REQUEST['uploaded'] > 0 )
			$message = sprintf( _n( 'Загружена %s фотография.', 'Загружена %s фотография.', $_REQUEST['uploaded'], 'usam' ), $_REQUEST['uploaded'] );	
		if( isset($_REQUEST['public']) && $_REQUEST['public'] > 0 )
			$message .= sprintf( _n( 'Опубликован %s товар.', 'Опубликовано %s товаров.', $_REQUEST['public'], 'usam' ), $_REQUEST['public'] );
		
		if( !empty($_REQUEST['error']) )
		{
			switch( $_REQUEST['error'] )
			{			
				case 1:	
					$message = __( 'Нет товаров для публикации.', 'usam' );	
				break;
				case 2:	
					$message = __( 'Неправильный номер группы вКонтакте. Возможно это номер анкеты?', 'usam' );	
				break;
				case 3:	
					$message = __( 'Группа вКонтакте не найдена.', 'usam' );	
				break;
			}
		}		
		return $message;
	} 
	
	protected function callback_submit()
	{					
		global $profile_id;		
		
		$profile_type = $profile_id>0?'profiles':'group';	

		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();	
		$profiles = $vkontakte->user_and_group( $profile_type );		
		$current_profile = array();
		foreach ( $profiles as $profile ) 
		{
			if ( $profile['page_id'] == abs($profile_id) )
			{							
				$current_profile = $profile;
				break;
			}
		}	
				
		$i = 0;
		switch( $this->current_action )
		{			
			case 'delete':				
				foreach ( $this->records as $product_id )
				{
					foreach ( $profiles as $profile ) 
					{						
						if ( $vkontakte->delete_product( $product_id, $profile ) )
							$i++;
					}
				}				
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );			
				$this->redirect = true;
			break;		
			case 'update_all_products':				
				$vk = new USAM_VKontakte();
				$vk->update_products();
			break;
			case 'update':									
				$profiles = $vkontakte->user_and_group('group');
				foreach ( $this->records as $product_id )
				{
					foreach ( $profiles as $profile ) 
					{						
						if ( $vkontakte->edit_product( $product_id, $profile ) )
							$i++;
					}
				}				
				$this->sendback = add_query_arg( array( 'update' => $i ), $this->sendback );			
				$this->redirect = true;
			break;
			case 'add_post_users':
			case 'add_post_groups':					
				if ( $this->current_action == 'add_post_users' )
					$profiles = $vkontakte->user_and_group('profiles');			
				else
					$profiles = $vkontakte->user_and_group('group');	
				
				foreach ( $this->records as $product_id )
				{					
					$post = get_post( $product_id );
					$publish_date = strtotime( usam_get_datepicker($product_id) );	
					foreach ( $profiles as $profile ) 
					{						
						$args = array();
						if ( $publish_date )
							$args['publish_date'] = $publish_date;	
						
						if( $vkontakte->publish_post( $post, $profile, $args ) ) 
							$i++;
						break;
					}
				}				
				$this->sendback = add_query_arg( array( 'public' => $i ), $this->sendback );			
				$this->redirect = true;
			break;				
			case 'add_post':								
				foreach ( $this->records as $product_id )
				{					
					$post = get_post( $product_id );
					$publish_date = strtotime( usam_get_datepicker($product_id) );	
					
					$args = array();					
					if ( $publish_date )
						$args['publish_date'] = $publish_date;	
					
					if( $vkontakte->publish_post( $post, $current_profile, $args ) )
						$i++;
				}				
				$this->sendback = add_query_arg( array( 'public' => $i ), $this->sendback );			
				$this->redirect = true;
			break;			
		}				
		if ( stripos($this->current_action, 'category') !== false) 
		{				
			$category_id = str_replace( 'category-', '', $this->current_action );
			foreach ( $this->records as $product_id )
			{									
				$post = get_post( $product_id );						
				if ( $vkontakte->upload_photo_album( $post, $profile, $thumb_id ) )							
					$i++;	
			}				
			$this->sendback = add_query_arg( array( 'public' => $i ), $this->sendback );				
			$this->redirect = true;
		}
		elseif ( stripos($this->current_action, 'album') !== false) 
		{	
			$thumb_id = str_replace( 'album-', '', $this->current_action );
			foreach ( $this->records as $product_id )
			{									
				$post = get_post( $product_id );							
				if ( $vkontakte->upload_photo_album( $post, $current_profile, $thumb_id ) )							
					$i++;
			}			
			$this->sendback = add_query_arg( array( 'uploaded' => $i ), $this->sendback );				
			$this->redirect = true;
		}
		$errors = $vkontakte->get_errors();
		foreach ( $errors as $error ) 
			$this->set_user_screen_error( $error );
			
		if ( $this->redirect )
		{ 
			foreach ( $this->records as $product_id )
				$this->sendback = remove_query_arg( array( 'date_'.$product_id, 'date_hour_'.$product_id, 'date_minute_'.$product_id ), $this->sendback  );					
		}	
	}
	
	public function tab_structure() 
	{			
		global $profile_id;
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();									
		$profiles = $vkontakte->user_and_group();
		
		$this->display_title();	
		$this->display_help_center();	
		$this->display_tables_select();				
		
		?>
		<div class="profiles_panel">
			<div class="profiles_content">		
				<span class="name"><?php _e( 'Ваши анкеты и группы', 'usam' ); ?>:</span>
				<div class="select_profile">	
					<?php
					if ( empty($profiles) )
					{
						echo "<h4>".__( 'Все возможности скрыты, потому что Вы не настроили ни группы, ни анкеты. Сделайте это сейчас!', 'usam' )."</h4>";
					}
					else
					{
					?>
					<form method='GET' action=''>
					<input type='hidden' value='<?php echo $this->page_name; ?>' name='page' />
					<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />
					<select name ="profile_id" id ="profile_id" onchange="this.form.submit()">
						<?php
						foreach ( $profiles as $profile ) 
						{				
							$select = ($profile_id > 0 && $profile_id==$profile['page_id'])|| ( $profile_id < 0 && abs($profile_id)==$profile['page_id'])?1:0;					
							$sendback = add_query_arg( array( 'profile_id' => $profile['page_id'] ), $this->sendback );
							if ( $profile['type_page'] == 'group' )
							{					
								?>					
									<option <?php selected($select); ?> value="-<?php echo $profile['page_id']; ?>"><img src="<?php echo $profile['photo']; ?>"><?php echo $profile['name']; ?></option>
								<?php
							}
							else
							{
								?>					
									<option <?php selected($select); ?> value="<?php echo $profile['page_id']; ?>"><img src="<?php echo $profile['photo_50']; ?>"><?php echo $profile['last_name'].' '.$profile['first_name']; ?></option>
								<?php
							}
						}
						?>
					</select>
					</form>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php
		$this->list_table->display_table(); 
	}	
}