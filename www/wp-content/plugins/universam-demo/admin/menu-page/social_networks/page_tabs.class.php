<?php
/*
 * Отображение страницы "Инструменты магазина"
 */
 
$default_tabs = array(	
	array( 'id' => 'vk_products',  'title' => __( 'вКонтакте', 'usam' ), 'level' => array(
		array( 'id' => 'vk_users',  'title' => __( 'Участники', 'usam' ) ),
	//	array( 'id' => 'profiles',  'title' => __( 'Управление анкетами', 'usam' ) ),
		array( 'id' => 'contest',  'title' => __( 'Конкурсы', 'usam' ) ),			
		),	
	),		 
	 array( 'id' => 'instagram',  'title' => __( 'Instagram', 'usam' ) ),	
//	'odnoklassniki'    => __( 'Одноклассники' , 'usam' ),
//	'facebook'         => __( 'facebook' , 'usam' ),
	
);


class USAM_Tab extends USAM_Page_Tab
{		
	protected function localize_script_tab()
	{	
		return array(			
			'add_products_vk_nonce' => usam_create_ajax_nonce( 'add_products_vk' ),	
		);
	}	
}