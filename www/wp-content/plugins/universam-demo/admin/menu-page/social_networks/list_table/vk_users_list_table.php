<?php
require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );
class USAM_List_Table_vk_users extends USAM_List_Table
{	
	protected $pimary_id = 'uid';		
		
	function column_image( $item ) 
	{	
		?><img src="<?php echo $item['photo_50']; ?>"><?php
    }	
	
	function column_name( $item ) 
	{		
		echo '<a href="https://vk.com/id'.$item['uid'].'" target="_blank">' .$item['last_name']." ".$item['first_name'].'</a>';
    }		
	
	function column_bdate( $item ) 
	{	
		if ( isset($user['bdate']) )
		{
			$day = explode(".",$user['bdate']);	
			
		}
    }	
	
	function column_country( $item ) 
	{
		
	}
	
	function column_city( $item ) 
	{
		
	}
	
	function column_online( $item ) 
	{
		echo $item['online']?__('Онлайн','usam'):'';
	}
	
	function column_has_mobile( $item ) 
	{
		echo !empty($item['has_mobile'])?__('Мобильное устройство','usam'):'';
	}
	
	function column_can_see_all_posts( $item ) 
	{
		echo !empty($item['can_see_all_posts'])?'<span class="yes">'.__('Видит','usam')."</span>":"<span class='no'>".__('Не видит','usam')."</span>";
	}
	
	function column_can_post( $item ) 
	{
		echo !empty($item['can_post'])?'<span class="yes">'.__('Можно оставлять','usam')."</span>":"<span class='no'>".__('Нельзя оставлять','usam')."</span>";
	}
	
	public function extra_tablenav_display( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			submit_button( __( 'Поздравить с ДР', 'usam' ), 'secondary', 'publish_birthday', false, array( 'id' => 'publish_birthday' ) );
		}
	}	
		    	
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',
			'image'          => '',				
			'name'           => __( 'Имя', 'usam' ),	
			'bdate'          => __( 'Возраст', 'usam' ),				
			'online'          => __( 'Статус', 'usam' ),		
			'has_mobile'     => __( 'Устройство', 'usam' ),	
			'can_see_all_posts' => __( 'Чужие записи на стене', 'usam' ),	
			'can_post' => __( 'Записи на стене', 'usam' ),
			
        );		
        return $columns;
    }	
	
	function prepare_items() 
	{		
		global $profile_id;		
		
		$offset = ($this->get_pagenum() - 1) * $this->per_page;
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();
		
		$params = array( 'group_id' => $profile_id, 'offset' => $offset, 'count' => $this->per_page, 'fields' => 'sex, city, country, has_mobile, online, bdate, photo_50, can_see_all_posts, can_post' );		
		$results = $vkontakte->send_request( $params, 'groups.getMembers' );	
		
		$this->items = $results['users']; 
		$this->set_pagination_args( array('total_items' => $results['count'], 'per_page' => $this->per_page) );
	}
}