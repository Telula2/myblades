<?php	
class USAM_List_Table_vk_posts extends USAM_List_Table
{
	private $albums_group = array();
	private $albums = array();
	private $vk_group = array();
	private $users = array();
		
	function __construct( $args )
	{	
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();
		$this->users = $vkontakte->user_and_group('profiles');					
		$this->vk_group = $vkontakte->user_and_group('group');
		parent::__construct( $args );				
    }
	
	protected function bulk_actions( $which = '' ) 
	{ 
		if ( is_null( $this->_actions ) ) {
			$no_new_actions = $this->_actions = $this->get_bulk_actions();			
			$this->_actions = apply_filters( "bulk_actions-{$this->screen->id}", $this->_actions );
			$this->_actions = array_intersect_assoc( $this->_actions, $no_new_actions );
			$two = '';
		} 
		else 
		{
			$two = '2';
		}			
		echo '<label for="bulk-action-selector-' . esc_attr( $which ) . '" class="screen-reader-text">' . __( 'Select bulk action' ) . '</label>';
		echo '<select name="action' . $two . '" id="bulk-action-selector-' . esc_attr( $which ) . "\">\n";
			echo '<option value="-1">' . __( 'Bulk Actions' ) . "</option>\n";
			if ( empty( $this->_actions ) )
			{					
				foreach ( $this->_actions as $name => $title ) 
				{
					$class = 'edit' === $name ? ' class="hide-if-no-js"' : '';
					echo "\t" . '<option value="' . $name . '"' . $class . '>' . $title . "</option>\n";
				}	
			}	
			if ( !empty($this->users) )
			{				
				echo '<optgroup label="' . __( 'Выгрузить в анкету как запись', 'usam' ) . '">';
				echo "\t" . '<option value="user_post-0">'.__( 'Во все группы', 'usam' )."</option>\n";
				foreach( $this->users as $user )
				{	
					echo "\t" . '<option value="user_post-'.$user['page_id'].'">'.$user['first_name'].' '.$user['last_name']."</option>\n";
				}	
				echo '</optgroup>';			
			}							
			if ( !empty($this->vk_group) )
			{
				echo '<optgroup label="' . __( 'Выгрузить в группу как запись', 'usam' ) . '">';
				echo "\t" . '<option value="group_post-0">'.__( 'Во все группы', 'usam' )."</option>\n";
				foreach( $this->vk_group as $group )
				{	
					echo "\t" . '<option value="group_post-'.$group['page_id'].'">'.$group['name']."</option>\n";
				}	
				echo '</optgroup>';					
			}			
		echo "</select>\n";

		submit_button( __( 'Apply' ), 'action', '', false, array( 'id' => "doaction$two" ) );
		echo "\n";
	}	
	
	function column_new_publish_date( $item ) 
	{
		usam_display_datetime_picker( $item->ID );
	}	
			
	function column_publish_date( $item ) 
	{	
		$publish_date = usam_get_product_meta( $item->ID, 'vk_publish_date' );
		echo usam_local_date( $publish_date );
    }	
		
	function get_sortable_columns()
	{
		$sortable = array(
			'post_title'     => array('post_title', false),				
			'publish_date'   => array('vk_publish_date', false),	
			'vk_post_id'     => array('vk_post_id', false),				
			'views'          => array('views', false),				
			'post_date_gmt'  => array('post_date_gmt', false)
			);
		return $sortable;
	}

	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'image'          => '',
			'post_title'     => __( 'Имя', 'usam' ),
			'vk_post_id'     => __( 'ID в контакте', 'usam' ),			
			'publish_date'   => __( 'Дата публикации', 'usam' ),	
			'new_publish_date' => __( 'Задать дату публикации', 'usam' ),	
			'views'          => '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Просмотры' ) . '">'.__( 'Просмотры' ).'</span>',					
			'post_date_gmt'  => __( 'Дата', 'usam' )			
        );		
        return $columns;
    }
	
	public function has_items() 
	{
		return have_posts();
	}
		
	public function display_rows( $posts = array(), $level = 0 ) 
	{
		global $wp_query;

		if ( empty( $posts ) )
			$posts = $wp_query->posts;	
		if ( $this->hierarchical_display ) 
			$this->_display_rows_hierarchical( $posts, $this->get_pagenum(), $this->per_page );
		else 
			$this->_display_rows( $posts, $level );
	}


	private function _display_rows( $posts, $level = 0 ) 
	{
		$post_ids = array();

		foreach ( $posts as $a_post )
			$post_ids[] = $a_post->ID;
		
		foreach ( $posts as $post )
			$this->single_row( $post, $level );
	}

	function prepare_items() 
	{			
		global $wp_query;				
	
		$offset = ($this->get_pagenum() - 1) * $this->per_page;
		$query_vars = array(
			'post_status'    => 'publish',
			'post_type'      => 'post',			
			'posts_per_page' => $this->per_page,
			'offset'         => $offset,		
			's'              => $this->search,	
			'post_parent'    => 0				
		);					
		if ( !empty($this->records) )
			$query_vars['post__in'] = $this->records;
		
		$wp_query = new WP_Query( $query_vars );	
		$this->_column_headers = $this->get_column_info();

		$total_items = $this->hierarchical_display ? $wp_query->post_count : $wp_query->found_posts;	

		if ( $this->hierarchical_display )
			$total_pages = ceil( $total_items / $this->per_page );
		else
			$total_pages = $wp_query->max_num_pages;			

		$this->is_trash = isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] == 'trash';
	
		$this->set_pagination_args( array('total_items' => $total_items, 'total_pages' => $total_pages,	'per_page' => $this->per_page) );
	}	
}