<?php
require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );
class USAM_List_Table_vk_products extends USAM_Product_List_Table
{
	private $profile = array();
		
	function __construct( $args )
	{		
		global $profile_id;				

		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();		
		
		$profiles = $vkontakte->user_and_group();
		foreach ( $profiles as $profile ) 	
		{ 
			$page_id = $profile['type_page'] == 'group'?'-'.$profile['page_id']:$profile['page_id'];
			if ( $page_id == $profile_id )
			{
				$this->profile = $profile;	
				break;
			}
		}		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );
		parent::__construct( $args );				
	}
	
	protected function bulk_actions( $which = '' ) 
	{ 
		global $profile_id;
		
		if ( empty($this->profile) )
			return false;
	
		if ( !$this->bulk_actions )
			return false;
		
		$params = array(
			'access_token' => $this->profile['access_token'],
			'owner_id' => $profile_id,			
			'need_system' => 1			
		); 
		$albums = usam_vkontakte_send_request( $params, 'photos.getAlbums' );	
		
		if ( is_null( $this->_actions ) ) {
			$no_new_actions = $this->_actions = $this->get_bulk_actions();			
			$this->_actions = apply_filters( "bulk_actions-{$this->screen->id}", $this->_actions );
			$this->_actions = array_intersect_assoc( $this->_actions, $no_new_actions );
			$two = '';
		} 
		else 
		{
			$two = '2';
		}			
		echo '<label for="bulk-action-selector-' . esc_attr( $which ) . '" class="screen-reader-text">' . __( 'Select bulk action' ) . '</label>';
		echo '<select name="action' . $two . '" id="bulk-action-selector-' . esc_attr( $which ) . "\">\n";
			echo '<option value="-1">' . __( 'Bulk Actions' ) . "</option>\n";
			echo '<option value="delete">' . __( 'Удалить', 'usam' ) . "</option>\n";
			echo '<option value="update">' . __( 'Обновление', 'usam' ) . "</option>\n";
			echo "\t" . '<option value="add_product">'.__( 'Добавить товар', 'usam' )."</option>\n";
			if ( empty( $this->_actions ) )
			{					
				foreach ( $this->_actions as $name => $title ) 
				{
					$class = 'edit' === $name ? ' class="hide-if-no-js"' : '';
					echo "\t" . '<option value="' . $name . '"' . $class . '>' . $title . "</option>\n";
				}	
			}					
			echo '<optgroup label="' . __( 'Добавить запись', 'usam' ) . '">';
				echo "\t" . '<option value="add_post_users">'.__( 'Во все анкеты', 'usam' )."</option>\n";
				echo "\t" . '<option value="add_post_groups">'.__( 'Во все группы', 'usam' )."</option>\n";
				echo "\t" . '<option value="add_post">'.__( 'В выбранный профиль', 'usam' )."</option>\n";
			echo '</optgroup>';			
			echo '<optgroup label="'.__( 'Фото в альбом группы', 'usam' ).'">';							
			foreach( $albums as $albums )
			{			
				if ( $albums['aid'] > 0 )
					echo "\t" . '<option value="album-'.$albums['aid'].'">' .$albums['title'] . "</option>\n";
			}				
			echo '</optgroup>';				
		echo "</select>\n";

		submit_button( __( 'Apply' ), 'action', '', false, array( 'id' => "doaction$two" ) );
		echo "\n";
	}	
	
	public function extra_tablenav_display( $which ) 
	{		
		if ( 'top' == $which )
		{
			echo '<div class="alignleft actions">';	
			$url = $this->get_nonce_url( add_query_arg( array('action' => 'update_all_products'), $_SERVER['REQUEST_URI'] ) );
			?>		
			<a href="<?php echo $url; ?>" class = "button button-primary"><?php _e( 'Обновить товары', 'usam' ); ?></a>
			<?php 									
			echo '</div>';								
		}
	}	
	
	function column_new_publish_date( $item ) 
	{
		usam_display_datetime_picker( $item->ID );
	}	
			
	function column_publish_date( $item ) 
	{	
		$publish_date = usam_get_product_meta( $item->ID, 'vk_market_publish_date' );
		echo usam_local_date( $publish_date );
    }	
	
	function column_vk_market_id( $item ) 
	{	
		$market_id = usam_get_product_meta( $item->ID, 'vk_market_id' );
		if ( $market_id )
			_e('Опубликован','usam');
		else
			_e('Не опубликован','usam');
    }	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'terms' => array() );		
	}
	    
	function get_sortable_columns()
	{
		$sortable = array(
			'product_title'  => array('product_title', false),	
			'price'          => array('price', false),	
			'stock'          => array('stock', false),		
			'views'          => array('views', false),				
			'date'           => array('date', false),
			'publish_date'   => array('vk_publish_date', false),	
			'vk_market_id'   => array('vk_market_id', false),		
			);
		return $sortable;
	}
	
	function admin_footer()
	{
		if ( !empty($this->profile) )		
			echo usam_get_modal_window( __('Добавить товары','usam').usam_get_loader(), 'add_products_vk', $this->get_modal_window() );	
	}	
	
	function get_modal_window()
	{
		global $profile_id;
		
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();		
		$params = array(
			'access_token' => $this->profile['access_token'],
			'owner_id' => $profile_id,	
			'count' => 100,
			'offset' => 0,	
		); 					
		$categories = $vkontakte->send_request( $params, 'market.getCategories' );
	
		$params = array(
			'access_token' => $this->profile['access_token'],
			'owner_id' => $profile_id,	
		); 		
		$market_albums = $vkontakte->send_request( $params, 'market.getAlbums' ); 
		if ( $market_albums )
			unset($market_albums[0]);
		
		$out = "<div class='colums'><div class='colum1'>		
		<table>
			<tr><td><label>".__( 'Куда', 'usam' ).":</label></td><td><select id='group' class ='chzn-select'>";
					$profiles = $vkontakte->user_and_group('group');
					foreach ( $profiles as $profile ) 
					{															
						$out .= "<option value='".$profile['page_id']."' ".selected( $profile_id, $profile['page_id'], false ).">".$profile['name']."</option>";
					}		
					$out .= "<option value='all'>".__('Во все группы', 'usam')."</option>
			</select></td></tr>";			
			$out .= "<tr class ='colum-category'><td><label id='title'>".__( 'Категории товаров', 'usam' ).":</label></td><td><select id='category' class ='chzn-select'>";
			$out .= "<option value=''>".__( 'Выберите', 'usam' )."</option>";				
				$section = '';
				if (!empty( $categories) )
				{
					foreach ( $categories as $category )
					{
						if ( empty($category['name']) )
							continue;
						
						if ( $section !== $category['section']['id'] )
						{
							if ( $section !== '' )
								$out .= '</optgroup>';
							$out .= '<optgroup label="' . $category['section']['name'] . '">';
						}
						$out .= "<option value='".$category['id']."'>".$category['name']."</option>";					
						$section = $category['section']['id'];
					}
				}
		$out .= "</optgroup></select></td></tr>";		
		$out .= "<tr><td><label>".__( 'Подборки', 'usam' ).":</label></td><td><select id='market_album' class ='chzn-select'>";
			$out .= "<option value=''>".__( 'Выберите', 'usam' )."</option>";				
				$section = '';
				if (!empty( $market_albums) )
				{
					foreach ( $market_albums as $album )
					{					
						$out .= "<option value='".$album['id']."'>".$album['title']."</option>";
					}
				}
		$out .= "</optgroup></select></td></tr>";	
		$out .= "</table></div>
		<div class='colum2'><div class='title'><strong>".__('Выбранные товары','usam')."</strong></div><div class='products'>".__( 'Товары не выбраны. Публикация не возможна.', 'usam' )."</div></div></div>";
		$out .= "<div class='popButton'>
			<button id = 'modal_action' type='button' class='button-primary button'>".__( 'Отправить', 'usam' )."</button>
			<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
		</div>";
		
		$errors = $vkontakte->get_errors();
		foreach ( $errors as $error ) 
			$this->set_user_screen_error( $error );
			
		return $out;
	}
	
	function no_items() 
	{
		global $profile_id;
		
		if ( $profile_id )
			_e( 'Не найдено ни одного товара. Пожалуйста добавьте и вернитесь, чтобы начать управлять товарами.', 'usam' );
		else
			_e( 'Не найдено ни одного профиля или группы вКонтакте. Пожалуйста добавьте и вернитесь, чтобы начать управлять товарами.', 'usam' );
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'image'          => '',
			'product_title'  => __( 'Имя', 'usam' ),	
			'sku'            => __( 'Артикул', 'usam' ),				
			'price'          => __( 'Цена', 'usam' ),		
			'vk_market_id'   => __( 'Статус', 'usam' ),					
			'publish_date'   => __( 'Дата публикации', 'usam' ),	
			'new_publish_date' => __( 'Задать дату публикации записи', 'usam' ),					
			'stock'          => __( 'Запас', 'usam'),
			'views'          => '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Просмотры' ) . '">'.__( 'Просмотры' ).'</span>',					
			'date'           => __( 'Дата', 'usam' )			
        );		
        return $columns;
    }			

	function query_vars( $query_vars ) 
	{	
		$query_vars['meta_query'] = array( array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '!=','type' => 'numeric' ));	
		return $query_vars;
	}
}