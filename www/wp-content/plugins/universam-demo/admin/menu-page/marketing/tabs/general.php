<?php
class USAM_Tab_General extends USAM_Tab
{
	private $day;
	public function __construct()
	{
		$this->header = array( 'title' => __('Сводная страница конверсии за поледние 7 дней', 'usam'), 'description' => '' );
	}
	
	public function display() 
	{
		global $wpdb;				
		
		$query = array( 'date_query' => array( 'after' => '1 weeks ago', ), 'fields' => array('date_insert','count','sum','sum_paid','count_paid','product_count') );		
	
		$orders = new USAM_Orders_Query( $query );	
		$results = $orders->get_results();
		if ( !empty($results) )
		{			
			$count_paid = (int)$results[0]->count_paid;
			$sum_paid = $results[0]->sum_paid;
			$sum_order = $results[0]->sum;
			$count_order = (int)$results[0]->count;				
		}
		else
		{
			$count_paid = 0;
			$sum_paid = 0;
			$sum_order = 0;
			$count_order = 0;			
		}		
		$baskert = $wpdb->get_row( "SELECT SUM(price) AS total_price, COUNT(*) AS total_items FROM ".USAM_TABLE_PRODUCTS_BASKET." WHERE WEEK(`date_insert`, 1) = WEEK(NOW(), 1) " );		
		/*Получение данных из Яндекс Метрика*/				
		require_once( USAM_FILE_PATH . '/includes/seo/yandex/metrika.class.php' );
		$metrika = new USAM_Yandex_Metrika();
		if ( $metrika->auth() )
		{	
			$date = new DateTime();
			$date->modify('-1 week');		

			$args = array('date1' =>$date->format('Y-m-d'), 'date2' => date('Y-m-d'), 'group' => 'week','limit' => 10000 );		
			$result_yandex = $metrika->get_statistics( $args );	
		}				
		if ( empty($result_yandex) )
		{
			$visits = 0;	
			$data['paid']['conversion'] = 0;
			$data['all']['conversion'] = 0;
			$data['cart']['conversion'] = 0;
		}
		else
		{
			$visits = $result_yandex[0]['statistic']['visits'];	
			$data['paid']['conversion'] = round($count_paid/$visits*100,2);
			$data['all']['conversion'] = round($count_order/$visits*100,2);
			$data['cart']['conversion'] = round($baskert->total_items/$visits*100,2);
		}
		
		
		$data['paid']['sum'] = $sum_paid;
		$data['paid']['count'] = $count_paid;
	
		$data['all']['sum'] = $sum_order;
		$data['all']['count'] = $count_order;		
		$data['cart']['sum'] = $baskert->total_price;
		$data['cart']['count'] = $baskert->total_items;		
		
		usam_add_box( 'usam_conversion', __('Конверсия','usam'), array( $this, 'box_conversion' ), $data );	
	//	usam_add_box( 'usam_devices', __('Устройства с которых заходили','usam'), array( $this, 'box_devices' ), $data );	
	//	usam_add_box( 'usam_conversion', __('Конверсия','usam'), array( $this, 'box_graph' ) );	
	}
	
	function box_graph( $data )
	{
		
	}
	
	function box_conversion( $data )
	{		
		?>	
		<div class="stat-item">
			<span class="stat-item-subtitle"><?php _e('Оплачено заказов','usam') ?></span>
			<div class="stat-item-block stat-item-block-conversion">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Конверсия','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['paid']['conversion']; ?><span>%</span></span>
				</span>
			</div>
			<div class="stat-item-block stat-item-block-first">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('На сумму','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo usam_currency_display($data['paid']['sum']); ?></span>
				</span>
			</div>
			<div class="stat-item-block">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Количество','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['paid']['count']; ?></span>
				</span>
			</div>
		</div>
		<div class="stat-item">
			<span class="stat-item-subtitle"><?php _e('Оформлено заказов','usam') ?></span>
			<div class="stat-item-block stat-item-block-conversion">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Конверсия','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['all']['conversion']; ?><span>%</span></span>
				</span>
			</div>
			<div class="stat-item-block stat-item-block-first">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('На сумму','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo usam_currency_display($data['all']['sum']); ?></span>
				</span>
			</div>
			<div class="stat-item-block">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Количество','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['all']['count']; ?></span>
				</span>
			</div>
		</div>
		<div class="stat-item">
			<span class="stat-item-subtitle"><?php _e('Добавлено в корзину','usam') ?></span>
			<div class="stat-item-block stat-item-block-conversion">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Конверсия','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['cart']['conversion']; ?><span>%</span></span>
				</span>
			</div>
			<div class="stat-item-block stat-item-block-first">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('На сумму','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo usam_currency_display($data['cart']['sum']); ?></span>
				</span>
			</div>
			<div class="stat-item-block">
				<span class="stat-item-block-inner">
					<span class="stat-item-block-title"><?php _e('Количество','usam') ?></span>
					<span class="stat-item-block-digit"><?php echo $data['cart']['count']; ?></span>
				</span>
			</div>
		</div>	
		<?php		
	}
	
	
	
	function box_devices( )
	{	
		?>
		
		<?php	
	}
}