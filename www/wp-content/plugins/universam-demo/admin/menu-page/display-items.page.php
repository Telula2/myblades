<?php
/**
 * Функции для редактирования и добавления товаров на странице ТОВАРОВ
 * @since 3.7
 */
class USAM_Display_Product_Page
{
	function __construct( ) 
	{	
		$columns_products = new USAM_Manage_Columns_Products( );
		
		if ( !isset( $_REQUEST['no_minors_allowed']))
			add_filter( 'request', array(&$this, 'no_minors_allowed'), 10, 1 );

		add_filter( 'request', array(&$this, 'column_sql_orderby') );
		add_filter( 'posts_fields', array( &$this, 'fields_sql' ) );	
		
		add_filter( 'posts_where', array(&$this, 'where_product_filter_description') ); // Фильтр описаний	
		
		add_action( 'restrict_manage_posts', array(&$this, 'restrict_manage_product') );	
		add_filter( 'manage_edit-usam-product_sortable_columns', array(&$this, 'additional_sortable_column_names') ); // Какие колонки можно будет сортировать
		add_filter( 'manage_edit-usam-product_columns', array(&$this, 'additional_column_names') );
		add_filter( 'manage_usam-product_posts_columns', array(&$this, 'additional_column_names') );		

		add_filter( 'page_row_actions', array(&$this, 'action_product_in_row'), 10, 2 );
		add_action( 'quick_edit_custom_box', array(&$this, 'quick_edit_boxes'), 10, 2 );
		add_action( 'bulk_edit_custom_box', array(&$this, 'quick_edit_boxes'), 10, 2 );	
		
		add_action('admin_notices',         array(&$this, 'custom_bulk_admin_notices'));
		
		add_action( 'handle_bulk_actions-edit-usam-product', array(&$this, 'bulk_action_handler'), 10, 3);
		add_filter( 'bulk_actions-edit-usam-product',  array(&$this, 'register_bulk_actions') );
		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );			
		add_filter( 'views_edit-usam-product', array( $this, 'product_views' ) );
	}	
	
	public function product_views( $views ) 
	{
		global $wp_query;		
		unset( $views['mine'] );	
		if ( current_user_can( 'edit_others_pages' ) ) 
		{
			$class            = ( isset( $wp_query->query['orderby'] ) && 'menu_order title' === $wp_query->query['orderby'] ) ? 'current' : '';
			$query_string     = remove_query_arg( array( 'orderby', 'order' ) );
			$query_string     = add_query_arg( 'orderby', rawurlencode( 'menu_order title' ), $query_string );
			$query_string     = add_query_arg( 'order', rawurlencode( 'ASC' ), $query_string );
			$views['byorder'] = '<a href="' . esc_url( $query_string ) . '" class="' . esc_attr( $class ) . '">' . __( 'Сортировка', 'usam' ) . '</a>';
		}
		return $views;
	}
	
	function register_bulk_actions( $bulk_actions ) 
	{
		global $current_screen;
		
		//$allowed_actions = array("export", 'publish', 'draft', 'print', 'archive');
		
		if ( !empty($_GET['post_status']) && $_GET['post_status'] != 'all' )						
		{
			switch ( $_GET['post_status'] ) 
			{
				case 'draft' :
					$bulk_actions['publish'] = __( 'Опубликовать', 'usam');
					$bulk_actions['archive'] = __( 'В архив', 'usam');
				break;
				case 'publish' :
					$bulk_actions['draft'] = __( 'В черновик', 'usam');
					$bulk_actions['archive'] = __( 'В архив', 'usam');
				break;
				case 'archive' :
					$bulk_actions['draft'] = __( 'В черновик', 'usam');
					$bulk_actions['publish'] = __( 'Опубликовать', 'usam');
				break;								
			}			
		}	
		else
		{						
			$bulk_actions['draft'] = __( 'В черновик', 'usam');
			$bulk_actions['archive'] = __( 'В архив', 'usam');
			$bulk_actions['publish'] = __( 'Опубликовать', 'usam');
		}		
		$bulk_actions['category'] = __( 'Изменить категорию', 'usam');
		$bulk_actions['brand'] = __( 'Изменить бренд', 'usam');		
		$bulk_actions['category_sale'] = __( 'Изменить категорию скидок', 'usam');	
		$bulk_actions['product_attribute'] = __( 'Изменить свойства у товаров', 'usam');	
		return $bulk_actions;
	}	
	
	function admin_footer()
	{
		echo usam_get_modal_window( __('Изменить термин у товаров','usam').usam_get_loader(), 'bulk_actions_terms', $this->get_terms_modal_window() );	
		echo usam_get_modal_window( __('Изменить свойства у товаров','usam').usam_get_loader(), 'bulk_actions_product_attribute', $this->get_product_attribute_modal_window() );
	}	
	
	function get_terms_modal_window()
	{
		$out = "
		<div class='product_bulk_actions'>
			<div class='operation_box'>
					<label>".__( 'Выберете операцию', 'usam' ).": </label><select id='operation' name='operation'>
				<option value='1'>".__('Добавить', 'usam')."</option>
				<option value='0'>".__('Перенести', 'usam')."</option>
				<option value='del'>".__('Удалить', 'usam')."</option>
			</select></div>";		
			$out .= "<div class='colums'><div class='colum1'>
			<div class='title'><strong>".__('Термины для изменения','usam')."</strong></div>
			<table><tr><td><label>".__( 'Категории товаров', 'usam' ).":</label></td><td><select id='category' name='usam-category' class ='chzn-select'>";
			$out .= "<option value=''>".__( 'Выберите', 'usam' )."</option>";
					$args = array(
								'descendants_and_self' => 0,						
								'popular_cats'         => false,
								'walker'               => new Walker_Category_Select(),
								'taxonomy'             => 'usam-category',
								'checked_ontop'        => false, 
								'echo'                 => false,
							);
					$out .= wp_terms_checklist( 0, $args );
			$out .= "</select></td></tr>";
			$out .= "<tr><td><label>".__( 'Бренд товаров', 'usam' ).":</label></td><td><select id='brands' name='brands' class ='chzn-select'>";
			$out .= "<option value=''>".__( 'Выберите', 'usam' )."</option>";
					$args = array(
								'descendants_and_self' => 0,						
								'popular_cats'         => false,
								'walker'               => new Walker_Category_Select(),
								'taxonomy'             => 'usam-brands',
								'checked_ontop'        => false, 
								'echo'                 => false,
							);
					$out .= wp_terms_checklist( 0, $args );
			$out .= "</select></td></tr>";
			$out .= "<tr><td><label>".__( 'Категории скидок товаров', 'usam' ).":</label></td><td><select id='category_sale' name='usam-category_sale' class ='chzn-select'>";
			$out .= "<option value=''>".__( 'Выберите', 'usam' )."</option>";
					$args = array(
								'descendants_and_self' => 0,						
								'popular_cats'         => false,
								'walker'               => new Walker_Category_Select(),
								'taxonomy'             => 'usam-category_sale',
								'checked_ontop'        => false, 
								'echo'                 => false,
							);
					$out .= wp_terms_checklist( 0, $args );
			$out .= "</select></td></tr></table></div>
			<div class='colum2'><div class='title'><strong>".__('Выбранные товары','usam')."</strong></div><div class='products'></div></div></div>";
			$out .= "<div class='popButton'>
				<button id = 'modal_action' type='button' class='button-primary button'>".__( 'Отправить', 'usam' )."</button>
				<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
			</div>
		</div>";
		return $out;
	}
	
	function get_product_attribute_modal_window()
	{					
		$html_table = '';	
		$args = array( 'hide_empty' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sort_order' );
		$product_attributes = get_terms('usam-product_attributes', $args);	
		
		$ids = array();				
		foreach( $product_attributes as $term )
		{						
			$ids[] = $term->term_id;					
		}	
		$ready_options_attributes = usam_get_product_attribute_values( array( 'attribute_id__in' => $ids ) );	
		$ready_options = array();
		foreach( $ready_options_attributes as $option )
		{
			$ready_options[$option->attribute_id][$option->id] = $option->value;
		}			
		$html_table = '<table class="t_characteristics">';
		foreach( $product_attributes as $term )
		{							
			if ( $term->parent == 0 )
			{						
				$out = '';
				foreach( $product_attributes as $attr )
				{
					if ( $term->term_id == $attr->parent )
					{
						$id = $attr->term_id;
						$disabled = '';									
						$type_attribute = get_term_meta($id, 'usam_type_attribute', true);					
						$class = "show_change";
						$meta_key = 'product_attributes_'.$id;			
						if ( $attr->slug == 'brand' )
						{
							$disabled = 'disabled="disabled"';							
						}							
						$fled_name = "data-id='$id'";
						$fled_id = "id='product_attribute_$id'";						
						$out .= "<tr class ='attribute'>
								<td class='name'><label for='product_attribute_$id'>".$attr->name.":</label></td>
								<td class='value'>";
									switch ( $type_attribute ) 
									{
										case 'C' ://Один																									
											$out .= "<input type='hidden' value='0' $disabled $fled_name>";
											$out .= "<input type='checkbox' value='1' $disabled class='$class' $fled_name $fled_id>";
										break;
										case 'M' ://Несколько																					
											if ( !empty($ready_options[$attr->term_id]) )
											{											
												$out .= "<select class='$class' multiple='multiple' $fled_name $fled_id>";
												foreach ( $ready_options[$attr->term_id] as $option_id => $option_name )
												{		
													$out .= "<option value='$option_id'>$option_name</option>";
												}	
												$out .= "</select>";	
											}
										break;
										case 'N' ://Число
										case 'S' ://Текст											
											if ( !empty($ready_options[$attr->term_id]) )
											{
												$out .= "<select class='$class' $fled_name $fled_id>";														
												foreach ( $ready_options[$attr->term_id] as $option_id => $option_name )
												{					
													$out .= "<option value='$option_id'>$option_name</option>";
												}				
												$out .= "</select>";
											}
										break;																	
										case 'D' ://Дата											
											$class = 'pattributes_date_picker';
											$out .= "<input type='text' class='$class' $fled_name $fled_id maxlength='10' style='width:90px' pattern='(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}' />";
										break;
										case 'O' ://Число
											$out .= "<input $disabled class='$class' type='text' $fled_name $fled_id>";
										break;
										case 'U' ://Менеджер	
											$args = array( 'orderby' => 'nicename', 'role__in' => array('editor','shop_manager','administrator','author','contributor'), 'fields' => array( 'ID','display_name') );
											$users = get_users( $args );
											if ( !empty($users) )
											{
												$out .= "<select class='$class' $fled_name $fled_id>";														
												foreach ( $users as $user )
												{					
													$out .= "<option value='$user->ID'>$user->display_name</option>";
												}				
												$out .= "</select>";
											}
										break;		
										case 'A' ://Агенты	
											$args = array( 'orderby' => 'nicename', 'role__in' => array('remote_agents'), 'fields' => array( 'ID','display_name') );
											$users = get_users( $args );
											if ( !empty($users) )
											{
												$out .= "<select class='$class' $fled_name $fled_id>";														
												foreach ( $users as $user )
												{					
													$out .= "<option value='$user->ID'>$user->display_name</option>";
												}				
												$out .= "</select>";
											}
										break;											
										case 'T' ://Текст
										default:			
											$out .=  "<input $disabled class='$class' type='text' $fled_name $fled_id>";
										break;
									}									
								$out .= "</td>
							</tr>";
					}
				}
				if ( $out != '' )
					$html_table .= "<tr><td colspan='2' class='group_name'><strong>".$term->name."</strong></td></tr>".$out;
			}
		}		
		$html_table .= "</table>";		
		$out = "
		<div class='product_bulk_actions'>
			<div class='colums'>
				<div class='colum1'>
					<div class='title'><strong>".__('Характеристики товара','usam')."</strong></div>
					<div class='product_attributes'>$html_table</div>	
				</div>
				<div class='colum2'><div class='title'><strong>".__('Выбранные товары','usam')."</strong></div><div class='products'></div></div></div>
				<div class='popButton'>
					<button id = 'modal_action' type='button' class='button-primary button'>".__( 'Отправить', 'usam' )."</button>
					<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
				</div>		
			</div>					
		</div>";
		return $out;
	}
	
	/**
	 * Обработка массовых действий
	 */
	function bulk_action_handler( $redirect_to, $action, $post_ids )
	{ 
		global $typenow, $wpdb, $type_price;
		switch ( $action ) 
		{
			case 'export':	
				//if ( !current_user_can($post_type_object->cap->export_post, $post_id) )
				//	wp_die( __('You are not allowed to export this post.') );

				$columns = array( 'title' => 'Название товара', 'price' => 'Цена', 'code' => 'Код', 'sku' => 'Артикул', 'stock' => 'Запас', 'cat' => 'Категория');
				$writer_i++;
				$export_data = array();
				foreach( $post_ids as $post_id )
				{	
					$meta = get_post_custom( $post_id );
					$data = array();
					foreach( $columns as $key => $column )
					{							
						switch ( $key ) 
						{
							case 'title':									
								$data[$key] = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE ID = '$post_id'");
							break;								
							case 'price':
								$data[$key] = $meta['_usam_price_'.$type_price][0];
							break;	
							case 'oldprice':
								$data[$key] = $meta['_usam_old_price_'.$type_price][0];
							break;	
							case 'sku':
								$data[$key] = $meta['_usam_sku'][0];
							break;								
							case 'stock':
								$data[$key] = $meta['_usam_stock'][0];
							break;								
							case 'cat':
								$cat = wp_get_object_terms( $post_id , 'usam-category' );
								$data[$key] = $cat[0]->name;
							break;								
						}
					}
					$export_data[] = $data;
				}	
				require_once( USAM_FILE_PATH . '/admin/includes/exported.class.php' );		
				$exported = new USAM_Exported_Table();	
				$columns = array ( 0 => $columns, 1 => array());			
				$exported->excel( $export_data, $columns );
			break;				
			case 'publish':
			case 'archive':
			case 'draft':	
			case 'to_order':	// Под заказ			
				$update = 0;
				foreach( $post_ids as $post_id )
				{	
					$update_post = array( 'ID' => $post_id, 'post_status' => $action );
					$update_status = wp_update_post( $update_post );						
					if ( $update_status >= 1 ) 
						$update++;
				}						
				$key = $action.'_product';
				$redirect_to = add_query_arg( array( $key => $update ), $redirect_to );
			break;						
			case 'print':						
				global $type_price;
				$product_count = 0;				
				foreach( $post_ids as $post_id )
				{					
					$product[$post_id]['title'] = $wpdb->get_var( "SELECT post_title FROM $wpdb->posts WHERE ID = '$post_id'" );
					$meta = get_post_custom( $post_id );
					$product[$post_id]['sku'] = $meta['_usam_sku'][0];
					$product[$post_id]['price'] = $meta['_usam_price_'.$type_price][0];				
					$product[$post_id]['stock'] = $meta['_usam_stock'][0];
					$terms = wp_get_object_terms( $post_id , 'usam-category' );		
					$product[$post_id]['cat'] = '';
					foreach ($terms as $term) 												
						$product[$post_id]['cat'] .= $term->name;
					$product_count++;
				}
				require_once( USAM_FILE_PATH . '/admin/includes/product/printing_products.php' );	
				exit();
			break;
			default: return;
		}			
		$redirect_to = remove_query_arg( array('action', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status',  'post', 'bulk_edit', 'post_view'), $redirect_to );	
		return $redirect_to;
	}
	/**
	 * отображать администратора уведомление на странице сообщений после действия
	 */
	function custom_bulk_admin_notices() 
	{
		global $post_type, $pagenow;
		
		if($pagenow == 'edit.php' && $post_type == 'usam-product')
		{
			$message = '';
			if( isset($_REQUEST['exported']) )
			{
				$exported = absint( $_REQUEST['exported'] );	
				$message = sprintf( _n( 'Элементы выгруженны в Excel.', '%s элемента экспортировано.', $exported ), $exported );
			}
			if( isset($_REQUEST['publish_product']) )	
			{
				$publish_product = absint( $_REQUEST['publish_product'] );	
				$message = sprintf( _n( 'Выбранные элементы опубликованны.', '%s элемента опубликованно.', $publish_product ), $publish_product );	
			}
			if( isset($_REQUEST['draft_product']) )		
			{
				$draft_product = absint( $_REQUEST['draft_product'] );	
				$message = sprintf( _n( 'Выбранные элементы перемещены в черновик.', '%s элемента перемещено в черновик.', $draft_product ), $draft_product );	
			}
			if ( $message != '' )
				echo "<div class=\"updated\"><p>{$message}</p></div>";
		}
	}	

	function fields_sql( $sql )
	{
		global $wpdb;
		$sql .= ",(SELECT COUNT(*) FROM `".USAM_TABLE_CUSTOMER_REVIEWS."` WHERE page_id=$wpdb->posts.id) AS `total`";	
		return $sql;
	}

	function additional_column_names( $columns )
	{
		$columns = array();
		$columns['cb']            = '<input type="checkbox" />';
		$columns['image']         = '';
		$columns['title']         = __('Название товара', 'usam');	
		$columns['price']         = __('Цена', 'usam');		
		$columns['sku']           = __('Артикул', 'usam');
		$columns['stock']         = '<span class="usam-dashicons-icon" title="' . esc_attr__( 'Доступный запас' ) . '">'.__( 'Доступный запас' ).'</span>';
		$columns['storage']       = '<span class="usam-dashicons-icon" title="' . esc_attr__( 'Запас' ) . '">'.__( 'Запас' ).'</span>';
		$columns['cats']          = __('Категории', 'usam');
		$columns['featured']      = '<span class="usam-dashicons-icon" title="' . esc_attr__( 'Избранное' ) . '">'.__( 'Избранное' ).'</span>';
		$columns['pdesc']         = '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Наличие описания' ) . '">'.__( 'Наличие описания' ).'</span>';
		$columns['views']         = '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Просмотры' ) . '">'.__( 'Просмотры' ).'</span>';		
		$columns['comment']       = '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Отзывы' ) . '">'.__( 'Отзывы' ).'</span>';
		$columns['prating']       = '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Рейтинг' ) . '">'.__( 'Рейтинг' ).'</span>';
		$columns['weight']        = __('Вес', 'usam');			
		$columns['date']          = __('Дата', 'usam');
		$columns['author']        = __('Автор', 'usam');
		$columns['hidden_alerts'] = '';		
		return $columns;
	}

	function additional_sortable_column_names( $columns )
	{
		$columns['stock']         = 'stock';
		$columns['price']         = 'price';
		$columns['sku']           = 'sku';
		$columns['views']         = 'views';
		$columns['prating']       = 'rating';	
		$columns['weight']        = 'weight';	
		$columns['storage']       = 'storage';		
		$columns['author']        = 'post_author';	
		return $columns;
	}

	/**
	 * Ограничить на странице продуктов к показу только родительские продукты и убрать вариации.
	 * @since 3.8
	 */
	function no_minors_allowed( $vars ) 
	{		
		$vars['post_parent'] = 0;
		return $vars;
	}
	
	//устанавливает порядок сортировки по умолчанию	
	/*
	[draft] => Черновик
	[pending] => На утверждении
	[private] => Личное
	[publish] => Опубликовано
	*/
	function column_sql_orderby( $vars ) 
	{				
		unset($vars['posts_per_archive_page']);		
		unset($vars['fields']);	
		$vars['posts_per_page'] = (int) get_user_option( "edit_usam-product_per_page" );		// Так как товары являются древовидными нужен лимит иначе загрузятся все.
		if ( !isset($_GET['orderby']) )
		{		
			$vars['order']   = 'DESC';	
			$vars['orderby']  = 'ID';		
		}	
		return $vars;		
	}
	
	// Выбирает товары с описанием или без описания
	function where_product_filter_description( $where )
	{
		if ( !empty($_GET['d']) )
		{					
			switch ( $_GET['d'] ) 
			{			
				case 'excerpt_yes':	
					$where .= " AND wp_posts.post_excerpt != ''";	
				break;
				case 'excerpt_no':					
					$where .= " AND wp_posts.post_excerpt = ''";	
				break;
			}
		}			
		return $where;
	}
		
	//Действие над продуктами в строке
	function action_product_in_row( $actions, $post ) 
	{
		if ( $post->post_type != "usam-product" )
			return $actions;
		
		$first_action['id'] = "<span class='product_id'>ID: $post->ID";
		
		$actions = array_merge( $first_action, $actions );	

		$url = admin_url( 'edit.php' );		
		if ( ($post->post_status == "publish" || $post->post_status == "draft") && $post->post_parent == 0 )
		{
			if ( ($post->post_status != "archive") )
			{	
				$actions['duplicate'] = '<a href="'.esc_url(usam_url_admin_action('duplicate_product', $url, array( 'id' => $post->ID ))).'">' . esc_html_x( 'Дублировать', 'row-actions', 'usam' ) . '</a>';
			}					
			$actions['archive'] = '<a href="'.esc_url(usam_url_admin_action('set_product_status_archive', $url, array( 'id' => $post->ID ))).'">' . esc_html_x( 'В архив', 'row-actions', 'usam' ) . '</a>';
		}
		return $actions;
	}		
	
	//Менеджер фильтров
	function restrict_manage_product( ) 
	{		
		$filter_manage = new USAM_Product_Filter_Manage();			 
		$filter_manage->filter_print('products_selection');
		$filter_manage->filter_print('products_type_price');	
		$checked = !empty($_REQUEST['no_minors_allowed']) == 1 ?1:0;
		?>
		</br>
		</br>		
		<div class = "usam_manage usam-manage_product_term">
			<?php $filter_manage->terms_products_search( array( 'usam-category', 'usam-brands', 'usam-category_sale' ) ); ?>
		</div>
		<div id = "usam_search_field_box" class = "usam_manage usam-manage_product_meta">			
			<?php $filter_manage->filter_print( 'meta' ); ?>
		</div>		
		<div id = "usam_search_field_box" class = "usam_manage usam-manage_product_price">			
			<?php $filter_manage->filter_print( 'price' ); ?>
		</div>	
		<?php			
	}		
		
	/**
	 * Создает поля для различных мета в быстром полях редактирования. Post_id не могут быть доступны здесь, вводят в рамках соответствующих строк, используя JavaScript.
	 */
	function quick_edit_boxes( $col_name, $_screen_post_type = null ) 
	{
		// See http://core.trac.wordpress.org/ticket/16392#comment:9
		if ( current_filter() == 'quick_edit_custom_box' && $_screen_post_type == 'edit-tags' )
			return;
		global  $wpdb, $post;
		
		?>
		<fieldset class="inline-edit-col-left usam-cols">
			<div class="inline-edit-col">
				<div class="inline-edit-group">
		<?php
		switch ( $col_name ) :
			case 'sku' :
				$sku = usam_get_product_meta( $post->ID, 'sku' );
				?>
				<label style="max-width: 85%" class="alignleft">
					<span class="checkbox-title usam-quick-edit"><?php esc_html_e( 'Артикул', 'usam' ); ?>: </span>
					<input type="text" name="sku" class="usam_ie_sku" value ="<?php echo $sku; ?>"/>						
				</label>
				<?php
			break;
			case 'weight' :
				$weight = usam_get_product_meta( $post->ID, 'weight' );
				?>
				<label style="max-width: 85%" class="alignleft">
					<span class="checkbox-title usam-quick-edit"><?php esc_html_e( 'Вес', 'usam' ); ?>: </span>
					<input type="text" name="weight" class="usam_ie_weight" value ="<?php echo $weight; ?>"/>							
				</label>
				<?php
			break;			
		endswitch;
		?>
				 </div>
			</div>
		</fieldset>
		<?php
	}
} 

/**
 * Вывод столбцов. Получает название столбца $column и подключает его к хуку "usam_manage_products_column__{$column}"
 */
class USAM_Manage_Columns_Products
{	
	function __construct( ) 
	{		
		add_action( 'manage_usam-product_posts_custom_column', array($this, 'column_display'), 10, 2 );			
		add_filter('usam_product_alert', array(&$this, 'check_stock'), 10, 2); 		
	}		
	
	public function column_display( $column, $product_id ) 
	{		
		$product = get_post( $product_id );		
		$function_column = "column_".strtolower( $column );	
		$this->$function_column( $product );
	}		
		
	
	//Колонка "Изображение" товара	 
	public function column_image( $product ) 
	{	
		echo usam_get_product_thumbnail( $product->ID, 'manage-products' );
	}
	
	//Колонка "Дата изменения" товара	 
	public function column_post_modified( $product ) 
	{	
		echo date( "d.m.Y", strtotime($product->post_modified) );	
	}	
	
	
	//Колонка "Цена" товара	 	
	public function column_price( $product ) 
	{					
		$out = ''; 
		$product_type = usam_get_product_type( $product->ID );	
		if ( $product_type == 'variable' ) 				
			$out .= __("от", 'usam').' ';
		$price = usam_get_product_price( $product->ID );
		$old_price = usam_get_product_old_price( $product->ID );			
		if( $old_price != 0 ) 					
			$out .= '<span class = "price"><strong>'.usam_currency_display( $price ).'</strong></span><br><span class = "old_price"><strike>'.usam_currency_display( $old_price )."</strike></span>";
		else
			$out .= "<strong>".usam_currency_display( $price )."<strong>";
		echo $out;
	}
	
	//Колонка "Артикул" товара	
	public function column_sku( $product ) 
	{ 
		$sku = usam_get_product_meta( $product->ID, 'sku' );
		if( $sku == '' )
			$sku = __('Не заполнено', 'usam');
		echo $sku;	
	}	
	
	private function display_term( $product_id, $taxonomies ) 
	{
		$terms = get_the_terms( $product_id, $taxonomies );	
		if ( !empty( $terms ) )
		{
			$out = array();
			foreach ( $terms as $term )
				$out[] = "<a href='".admin_url("edit.php?post_type=usam-product&amp;$taxonomies={$term->slug}")."'> ".$term->name."</a>";
			echo join( ', ', $out );
		} 		
	}
	
	//Колонка "Категории" товара	
	public function column_cats( $product ) 
	{
		$this->display_term( $product->ID, 'usam-category' );
	}
	
	//Колонка "Категории" товара	
	public function column_brand( $product ) 
	{		
		$this->display_term( $product->ID, 'usam-brands' );
	}
	
	/**
	 * Колонка "Избранный" в таблице продуктов админского интерфейса.
	 * @since  3.8.9
	 */
	public function column_featured( $product )
	{
		$status = usam_get_product_meta( $product->ID, 'sticky_products', true );			
		?>
			<a id="featured_toggle_<?php echo $product->ID; ?>" data-product_id="<?php echo $product->ID; ?>" class="usam_featured_product_toggle" 
			href='<?php echo usam_url_admin_action('update_featured_product', '', array( 'product_id' => $product->ID )); ?>' >
				<?php if ( !empty($status) ) : ?>
					<span class="usam-dashicons-icon usam_featured"><?php _e( 'Убрать из Избранного', 'usam' ); ?></span>					
				<?php else: ?>
					<span class="usam-dashicons-icon usam_featured not-featured"><?php _e( 'Добавить в Избранное', 'usam' ); ?></span>					
				<?php endif; ?>
			</a>
		<?php
	}	

	/**
	 * Колонка "Описание" в таблице продуктов админского интерфейса.	
	 */
	public function column_pdesc( $product )
	{
		if ( empty($product->post_excerpt) )
			$class = "no-description-icon";
		else
			$class = "yes-description-icon";
		?>	
		<div class = "usam-description-icon <?php echo $class; ?>"></div>
		<?php
	}	

	/**
	 * Колонка "Просмотры" в таблице продуктов админского интерфейса.	
	 */
	public function column_views( $product ) 
	{
		echo usam_get_product_meta( $product->ID, "product_views" );	
	}	
	
	/**
	 * Колонка "Вес" в таблице продуктов админского интерфейса.
	 */
	public function column_weight( $post ) 
	{
		$product_type = usam_get_product_type( $post->ID );	
		if ( $product_type == 'variable' ) 	
		{
			esc_html_e( 'N/A', 'usam' );
			return;
		}	
		$weight = usam_get_product_meta( $post->ID, 'weight' );			
		echo $weight;		
	}	

	/**
	 * Колонка "Запас" в таблице продуктов админского интерфейса.
	 */
	public function column_stock( $product ) 
	{
		$stock = usam_get_product_meta( $product->ID, "stock" );
		if( $stock == USAM_UNLIMITED_STOCK )
			$out = __('Неограничен', 'usam');		
		elseif ( empty($stock) )
			$out = "<span class = 'no_stock'>0<span>";
		else 		
			$out = sprintf( _n( '%s товар', '%s товаров', $stock, 'usam' ), $stock);			
		echo $out;	
	}	
	
	/**
	 * Колонка "Резерв" в таблице продуктов админского интерфейса.
	 */
	public function column_reserve( $product ) 
	{
		$reserve = (int)usam_get_product_meta( $product->ID, "reserve" );
	
		$out = sprintf( _n( '%s шт', '%s шт', $reserve, 'usam' ), $reserve);			
		echo $out;	
	}		
	
	/**
	 * Колонка "Запас" в таблице продуктов админского интерфейса.
	 */
	public function column_storage( $product ) 
	{
		$storage = (int)usam_get_product_meta( $product->ID, "storage" );
		if( $storage == USAM_UNLIMITED_STOCK )
			$out = __('Неограничен', 'usam');		
		elseif ($storage == '0')
			$out = "<span class = 'no_stock'>$storage<span>";
		else 			
			$out = sprintf( _n( '%s товар', '%s товаров', $storage, 'usam' ), $storage);			
		echo $out;		
	}	
	
	
	/**
	 * Колонка "Коментарии" в таблице продуктов админского интерфейса.
	 */
	public function column_comment( $post ) 
	{
		echo '<div class="post-com-count-wrapper"><a href="'.admin_url('admin.php?page=feedback&tab=reviews&id='.$post->ID).'" title="0 ожидающих" class="post-com-count"><span class="comment-count">'.$post->total.'</span></a></div>';
	}
	
		/**
	 * Колонка "Рейтинг" в таблице продуктов админского интерфейса.
	 */
	public function column_prating( $product ) 
	{
		echo usam_get_product_rating( $product->ID );
	}
	
	
	/**
	 * Колонка оповещение о продукте на страницы "Управление продуктами"
	 */
	public function column_hidden_alerts( $product ) 
	{
		$product_alert = apply_filters( 'usam_product_alert', array( false, '' ), $product );	
		if( !empty( $product_alert['messages'] ) )
			$product_alert['messages'] = implode( "\n",( array )$product_alert['messages'] );
		if( $product_alert['state'] === true ) 
		{
			?>
				<img alt='<?php echo $product_alert['messages'];?>' title='<?php echo $product_alert['messages'];?>' class='product-alert-image' src='<?php echo  USAM_CORE_IMAGES_URL;?>/product-alert.jpg' alt='' />
			<?php
		}
		// Если предупреждение продукт имеет материал для отображения, показать его.
		if ( !empty( $product_alert['display'] ) )
			echo $product_alert['display'];
	}
	
	/*
	 * эта функция проверяет каждый продукт на странице продуктов, чтобы узнать имеет он запас. Выполняется через usam_product_alert фильтр
	 */
	function check_stock($state, $product)
	{
		$state['state'] = false;
		$state['messages'] = array();	
		$stock_count = usam_get_product_meta( $product->ID, 'stock', true );	
		if( $stock_count == 0 )
		{			
			$state['state'] = true;
			$state['messages'][] = __( 'Этот продукт не имеет доступных запасов', 'usam' );
		}	
		return $state;
	}
}

// Вывод фильтров товаров
class USAM_Product_Filter_Manage
{		
	public function __construct() 
	{			
			
	}

	public function filter_print( $filter_name )
	{
		$filter = $filter_name."_filter";
		$this->$filter();
	}

	// Вывод сортировки по терминам
	public function terms_products_search( $filters ) 
	{
		foreach ( $filters as $tax_slug )
		{		
			$tax_obj = get_taxonomy( $tax_slug );		
			$tax_name = $tax_obj->labels->menu_name;	
			$selected_cats = !empty($_REQUEST[$tax_slug]) ? $_REQUEST[$tax_slug]:'';				
			echo "<select name='$tax_slug' id='$tax_slug' class='term_manager_product chzn-select' data-placeholder='".__('Выберите продукты', 'usam')."'>";
				echo "<option value=''>" . esc_html( sprintf( __( 'Все %s', 'usam' ), $tax_name ) ) . "</option>";
				$args = array(
							'descendants_and_self' => 0,
							'selected_cats'        => array( $selected_cats ),
							'popular_cats'         => false,
							'walker'               => new Walker_Category_Select(),
							'taxonomy'             => $tax_slug,
							'list_only'            => 'slug',							
							'checked_ontop'        => false, 
							'echo'                 => true,
						);
				wp_terms_checklist( 0, $args );				
			echo "</select>";
		}   
	}	
	
	private function price_filter()
	{
		$price_from = isset($_REQUEST['price_from']) && $_REQUEST['price_from'] != ''? absint($_REQUEST['price_from']):'';
		$price_to = isset($_REQUEST['price_to']) && $_REQUEST['price_to'] != ''? absint($_REQUEST['price_to']):'';		
		?>	
		<label for="usam_product_price"><?php _e( 'Цена', 'usam' ) ?>:</label>	
		<input id="usam_product_price" type="text" size = '9' name="price_from" value="<?php echo $price_from; ?>"/> - 
		<input type="text" name="price_to" size = '9' value="<?php echo $price_to; ?>"/>		
		<?php
	}
											   
	private function meta_filter()
	{
		$columns['stock']     = __('Доступный остаток', 'usam');
		$columns['storage']   = __('Общий остаток', 'usam');
		$columns['price']     = __('Цена', 'usam');	
		$columns['oldprice']  = __('Старая цена', 'usam');		
		$columns['sku']       = __('Артикул', 'usam');
		$columns['code']      = __('Код', 'usam');	
		$columns['barcode']   = __('Штрихкод', 'usam');			
		$columns['vkid']      = __('Код товара вКонтакте', 'usam');		
		$columns['weight']    = __('Вес', 'usam');		
		$columns['views']     = __('Просмотры', 'usam');		
		$columns['rating']    = __('Рейтинг', 'usam');	
				
		$current_v = isset($_REQUEST['av'])? sanitize_text_field($_REQUEST['av']):'';
		
		$compare_columns['r']    = __('Равно', 'usam');
		$compare_columns['notr'] = __('Не равно', 'usam');
		$compare_columns['b']    = __('Больше', 'usam');
		$compare_columns['m']    = __('Меньше', 'usam');  
		$compare_columns['contains']  = __('Содержит', 'usam');  		
		$compare_columns['not_contain']  = __('Не содержит', 'usam');
		$compare_columns['NOT_EXISTS']  = __('Не существует', 'usam');
		?>
		<select name="a">
			<option value=""><?php _e('Выберите...', 'usam'); ?></option>
			<?php
			$current = isset($_REQUEST['a'])? sanitize_text_field($_REQUEST['a']):'';			
			foreach ($columns as $key => $value) 
			{				
				printf('<option value="%s"%s>%s</option>', $key, $key == $current ? ' selected="selected"':'', $value);	
			}
			?>
		</select> 		
		<select name="ac">			
			<?php
			$current = isset($_REQUEST['ac'])? sanitize_text_field($_REQUEST['ac']):'';			
			foreach ($compare_columns as $key => $value) 
			{				
				printf('<option value="%s"%s>%s</option>', $key, $key == $current ? ' selected="selected"':'', $value);	
			}
			?>
		</select> 				
		<input type="text" name="av" value="<?php echo $current_v; ?>" class = "show_internal_text" data-internal_text = "<?php _e('Введите значение...', 'usam') ?>"/>		
		<?php
	}
	
	private function products_type_price_filter()
	{		
		global $type_price;
		
		$current = isset($_REQUEST['type_price'])? sanitize_text_field($_REQUEST['type_price']):$type_price;	
		usam_get_select_prices( $current );
	}

	private function products_selection_filter()
	{		 
		$columns['image_yes']     = __('Показать с миниатюрами', 'usam');
		$columns['image_no']      = __('Показать без миниатюр', 'usam');
		$columns['webspy_yes']    = __('Товары с ссылками на поставщика', 'usam');
		$columns['webspy_no']     = __('Товары без ссылок на поставщика', 'usam');
		$columns['excerpt_yes']   = __('Показать с описанием', 'usam');
		$columns['excerpt_no']    = __('Показать без описание', 'usam');		
		$columns['variant_prod']  = __('Товары имеющие вариации', 'usam');
		?>
		<select name="d">
			<option value=""><?php _e('Выберите...', 'usam'); ?></option>
			<?php
			$current = isset($_REQUEST['d'])? sanitize_text_field($_REQUEST['d']):'';			
			foreach ($columns as $key => $value) 
			{				
				printf('<option value="%s"%s>%s</option>', $key, $key == $current ? ' selected="selected"':'', $value);	
			}
			?>
		</select> 
		<?php
	}
	
	private function products_sale_selection_filter()
	{		
		$columns['image_yes']     = __('Показать ', 'usam');
		$columns['image_no']      = __('Показать без миниатюр', 'usam');
		$columns['webspy_yes']    = __('Товары с ссылками на поставщика', 'usam');
		$columns['webspy_no']     = __('Товары без ссылок на поставщика', 'usam');
		$columns['excerpt_yes']   = __('Показать с описанием', 'usam');
		$columns['excerpt_no']    = __('Показать без описание', 'usam');		
		$columns['variant_prod']  = __('Товары имеющие вариации', 'usam');
		?>
		<select name="d">
			<option value=""><?php _e('Выберите...', 'usam'); ?></option>
			<?php
			$current = isset($_REQUEST['d'])? sanitize_text_field($_REQUEST['d']):'';			
			foreach ($columns as $key => $value) 
			{				
				printf('<option value="%s"%s>%s</option>', $key, $key == $current ? ' selected="selected"':'', $value);	
			}
			?>
		</select> 
		<?php
	}
}
?>