<?php
class USAM_Tab_back_call extends USAM_Tab
{	
	public function __construct()
	{				
		$this->header = array( 'title' => __('Обратный отзыв', 'usam'), 'add' => false, 'description' => __('Здесь показываются запросы ваших клиентов на обратный звонок.','usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{
			case 'delete':				
				$this->delete_record_feedback();	
			break;								
		}			
	}	
}