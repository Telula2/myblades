<?php
class USAM_Tab_Questions extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Сообщение от клиентов', 'usam'), 'description' => __('Здесь Вы можете отвечать Вашим клиентам на их вопросы и сообщения.','usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{					
		global $user_ID;
		switch( $this->current_action )
		{
			case 'delete':	
				$this->delete_record_feedback();				
			break;	
			case 'order':			
				add_filter( 'usam_prevent_notification_change_order_status', create_function('', 'return false;'));		
				foreach ( $this->records as $id ) 
				{						
					$row = usam_get_feedback( $id );
					$type_payer = usam_get_customer_meta( 'type_payer', $row['user_id'] );			
					$location = $row->location_id;	
					if ( empty($location) )
					{		
						$location = get_option( 'usam_shop_location' );	
					}					
					$args['type_price'] = usam_get_customer_price_code( $row['user_id'] );					
					$args['type_payer'] = $type_payer;
					$args['location'] = $location;					
			
					$cart = new USAM_CART();
					$cart->set_properties( $args );		
							
					$parameters['any_balance'] = true;
					$parameters['quantity'] = 1;
								
					$result = $cart->add_product_basket( $row['id_product'], $parameters );
					if ( $result )
					{					
						$order_id = $cart->save_order();						
						if ( !$order_id )
							continue;
						
						$order = new USAM_Order( $order_id );											
						$order_properties = usam_get_order_properties( array('type_payer' => $type_payer, 'fields' => 'unique_name=>data') );		
						if ( usam_is_type_payer_company( $type_payer ) )
						{
							if ( isset($order_properties['contact_person']) )
								$customer_data['contact_person'] = $row['name'];			
							if ( isset($order_properties['company_shippingnotesclient']) )
								$customer_data['company_shippingnotesclient'] = $row['message'];							
						}	
						else
						{
							if ( isset($order_properties['billinglastname']) )
								$customer_data['billinglastname'] = $row['name'];							
							if ( isset($order_properties['shippinglastname']) )
								$customer_data['shippinglastname'] = $row['name'];	
							if ( isset($order_properties['shippingnotesclient']) )
								$customer_data['shippingnotesclient'] = $row['message'];			
						}					
						foreach ( $order_properties as $property )
						{ 
							switch( $property->type )
							{
								case 'location':
									$customer_data[$property->unique_name] = $location;
								break;
								case 'email':
									$customer_data[$property->unique_name] = $row['mail'];
								break;
								case 'phone':
									$customer_data[$property->unique_name] = $row['phone'];
								break;
							}							
						}								
						$update = $order->save_customer_data( $customer_data );	
						$args =  array(	
							'status'     => 'received',	
							'shipping'   => 0,		
							'user_ID'    => $row['user_id'],	
							'manager_id'    => $user_ID,								
						);					
						$order->set($args);	
						$update = $order->save();	
						
						$payment['sum'] = $order->get( 'totalprice' );		
						$document_number = $order->add_payment_history( $payment );
				
						wp_redirect( usam_get_url_order( $order_id ) );
						exit;
					}
				}
			break;			
			case 'task':			
				foreach ( $this->records as $id ) 
				{
					$row = usam_get_feedback( $id );
					
					$task['title'] = __('Напоминание из обратной связи','usam');
					$task['description'] = $row['name']." ".$row['phone']." ".$row['mail']." ".$row['message']." ".$row['description'];				
					$task['reminder'] = 1;				
					$task['calendar'] = usam_get_id_system_calendar();	
					$task['date_time'] = date("Y-m-d H:i:s", mktime( date('H'),0,0,date('m'),date('d')+1,date('Y')));
									
					$id = usam_insert_event( $task );	
					usam_set_event_object( array( 'event_id' => $id, 'object_id' => $row['id'], 'object_type' => 'questions' ) ); 
				
					$this->sendback = add_query_arg( array( 'id' => $id ), admin_url('admin.php?page=crm&tab=tasks&action=edit') );			
					wp_redirect( $this->sendback );
					exit;		
				}
			break;	
			case 'save':	
				if( isset($_REQUEST['message']) )
				{	
					$data = stripslashes_deep($_REQUEST['message']);
					if ( $this->id != null )
						$result = usam_update_feedback( $this->id, $data );	
					else
						$result = usam_insert_feedback( $data );
				}
			break;	
		}	
		foreach ( $this->records as $key => $id )
			usam_work_on_task( array( 'title' => __('Консультация посетителя','usam') ),  array( 'object_type' => 'question', 'object_id' => $id ) );		
	}	
}