<?php
class USAM_Tab_Chat extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Общение с клиентами', 'usam'), 'add' => false, 'description' => 'Здесь Вы можете общаться онлайн с клиентами.' );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;		
		switch( $this->current_action )
		{
			case 'delete':				
				$in = implode( ', ', $this->records );	
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_CHAT_DIALOGS." WHERE id IN ($in)");
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_CHAT." WHERE topic IN ($in)");	
				if ($result >= 1 )
					$i = count($this->records);				
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;
			break;			
			default:
				
			break;
		}		
	}		
	
	public function tab_structure() 
	{
		$this->display_title();	
		$this->display_help_center();	
		$chat = new USAM_Chat_Template();
		if ( isset($_GET['sel']) ) 
		{
		?>		
			<div class = "current_dialog_messages">
				<h3><?php _e('Диалог', 'usam') ?></h3>
				<?php $chat->display_chat( 'dialogs' ); ?>			
			</div>
		<?php }	?>	
		<div class = "dialogs_box">
			<div id = "dialogs" class = "dialogs_table">
				<?php 							
				$this->list_table->display_table();
				?>
			</div>
		</div>
		<?php	
	}
}