<?php
class USAM_Tab_reviews extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Отзывы посетителей', 'usam'), 'description' => 'Здесь Вы можете просматривать отзывы Ваших посетителей.' );				
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;		
		switch( $this->current_action )
		{
			case 'delete':
				foreach ( $this->records as $key => $id )
				{
					usam_delete_review($id);							
				}
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;			
			case 'trashreview':				
				foreach ( $this->records as $key => $id )
				{
					usam_update_review( $id, array('status' => 2) );						
				}
				$this->sendback = add_query_arg( array( 'updated' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;
			case 'approvereview':
				foreach ( $this->records as $key => $id )
				{					
					usam_update_review( $id, array('status' => 1) );
				}
				$this->sendback = add_query_arg( array( 'updated' => count($this->records) ), $this->sendback );	
				$this->redirect = true;				
			break;
			case 'unapprovereview':				
				foreach ( $this->records as $key => $id )
				{
					usam_update_review( $id, array('status' => 0) );
				}
				$this->sendback = add_query_arg( array( 'updated' => count($this->records) ), $this->sendback );	
				$this->redirect = true;					
			break;		
			case 'vk':					
				require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
				$vkontakte = new USAM_VKontakte_API();
				foreach ( $this->records as $key => $id )
				{
					$vkontakte->customer_reviews( $id );				
				}				
			break;
			default:
				
			break;
		}
		foreach ( $this->records as $key => $id )
			usam_work_on_task( array( 'title' => __('Работа с отзывом','usam') ),  array( 'object_type' => 'review', 'object_id' => $id ) );				
	}	
}
