<?php
class USAM_Tab_email extends USAM_Tab
{	
	public function __construct()
	{
		global $user_ID;
		require_once( USAM_FILE_PATH . '/includes/mailings/email_filter.class.php' );		
		
		if ( isset($_GET['table']) && $_GET['table'] == 'email_filters' )	
		{
			$this->header = array( 'title' => __('Фильтры', 'usam'), 'add' => false, 'description' => __('С помощью фильтров вы сможете раскладывать входящую почту по папкам, а также отмечать письма как важные или прочитанные..', 'usam') );		
			$this->buttons = array( 'add' => __('Добавить фильтр', 'usam') );			
		}
		else
		{
			$this->header = array( 'title' => __('Почтовые ящики', 'usam'), 'add' => false, 'description' => __('Здесь Вы можете отправлять сообщения и просматривать почту.', 'usam') );			
			$this->buttons = array( 'new' => __('Новое сообщение', 'usam'), 'download' => __('Проверить почту', 'usam'), 'clearing' => __('Очистка', 'usam'), 'filters' => __('Фильтры', 'usam') );
			
			global $email_id, $email_folder, $mailbox_id;
			
			if ( !empty($_REQUEST['f']) )
				$email_folder = sanitize_title($_REQUEST['f']);
			else
				$email_folder = 'inbox';
			
			if ( !empty($_REQUEST['m']) )
				$mailbox_id = sanitize_title($_REQUEST['m']);
			else
			{
				$mailbox_ids = usam_get_mailboxes( array('fields' => 'id', 'user_id' => $user_ID ) );	
				if ( isset($mailbox_ids[0]) )
					$mailbox_id = $mailbox_ids[0];
				else
					$mailbox_id = 0;
			}		
			if ( !empty($_GET['email_id']) )
				$email_id = absint($_GET['email_id']);		
			else	
				$email_id = 0;
			
			if ( $email_folder != 'deleted' )
				add_filter( 'usam_action_delete_items_table', create_function('', 'return true;'));	

			add_action( 'admin_footer', array(&$this, 'add_folder') );	
		}
	}
	
	function add_folder( )
	{
		echo usam_get_modal_window( __('Добавить папку', 'usam'), 'add_folder_window', $this->add_folder_windows() );	
		$html = "			
			<div class='action_buttons'>				
				<button type='button' class='action_confirm button-primary button' data-dismiss='modal' aria-hidden='true' id='clear_folder'>".__( 'Очистить', 'usam' )."</button>
				<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
			</div>";	
		echo usam_get_modal_window( __('Подтвердите','usam'), 'operation_confirm', $html, 'small' );		
	}
	
	function add_folder_windows( )
	{
		global $user_ID;	
		ob_start();	
		?>		
		<div class="usam_popup usam_event_edit">							
			<div class="usam_popup-row">
				<span class="usam_field-label"><label for="folder_name"><?php _e('Название', 'usam'); ?>:</label></span>
				<span class="usam_field-val usam_field-title-inner"><input type="text" id="folder_name" value =""></span>
			</div>				
			<div class="popButton">
				<button id = "save_action" type="button" class="button-primary button"><?php _e( 'Добавить', 'usam' ); ?></button>				
				<button type="button" class="button" data-dismiss="modal" aria-hidden="true"><?php _e( 'Отменить', 'usam' ); ?></button>
			</div>
		</div>		
		<?php 
		$out = ob_get_contents();
		ob_end_clean();		
			
		return $out;
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $email_folder, $mailbox_id, $user_ID;		
		
		switch( $this->current_action )
		{
			case 'delete':
				if ( $email_folder == 'deleted' )
				{
					foreach ( $this->records as $id )	
					{
						usam_delete_email( $id );
					}
				}
				else
				{ 
					foreach ( $this->records as $id )	
					{  
						usam_update_email( $id, array('folder' => 'deleted' ) );
					}					
					if ( isset($_REQUEST['s']) )					
						$this->sendback = add_query_arg( array( 's' => $_REQUEST['s'] ), $this->sendback );	
				}				
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records), 'm' => $mailbox_id, 'f' => $email_folder ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'email_print': 		
				require_once( USAM_FILE_PATH . '/admin/includes/mail/print.php' );
			break;	
			case 'clear': 
				$ids = usam_get_emails( array( 'fields' => 'id', 'folder' => 'deleted', 'mailbox_id' => $mailbox_id ) );
				foreach ( $ids as $id )	
				{
					usam_delete_email( $id );
				}			
				$this->sendback = add_query_arg( array( 'deleted' => count($ids), 'm' => $mailbox_id, 'f' => 'deleted' ), $this->sendback );
				$this->redirect = true;				
			break;			
			case 'spam':								
				foreach ( $this->records as $id )	
				{
					$email = usam_get_email( $id );
					
					$insert['if'] = 'sender';
					$insert['condition'] = 'equal';
					$insert['value'] = $email['from_email'];
					$insert['action'] = 'delete';
					$insert['mailbox_id'] = $mailbox_id;	
					usam_insert_email_filter( $insert );
					usam_update_email( $id, array('folder' => 'deleted' ) );
				}
			break;
			case 'filters':				
				$this->sendback = add_query_arg( array( 'table' => 'email_filters' ), $this->sendback );				
				$this->redirect = true;
			break;
			case 'read':				
				foreach ( $this->records as $id )	
				{
					usam_update_email( $id, array('read' => 1) );
				}
			break;
			case 'not_read':				
				foreach ( $this->records as $id )	
				{
					usam_update_email( $id, array('read' => 0) );
				}
			break;	
			case 'important':				
				foreach ( $this->records as $id )	
				{
					usam_update_email( $id, array('importance' => 1) );
				}
			break;							
			case 'not_important':				
				foreach ( $this->records as $id )	
				{
					usam_update_email( $id, array('importance' => 0) );
				}
			break;	
			case 'save':			
				if ( isset($_GET['table']) && $_GET['table'] == 'email_filters' )	
				{ 				
					$insert['if'] = sanitize_title($_REQUEST['if']);
					$insert['condition'] = sanitize_title($_REQUEST['condition']);
					$insert['value'] = sanitize_text_field($_REQUEST['value']);
					$insert['action'] = sanitize_title($_REQUEST['action']);
					$insert['mailbox_id'] = absint($_REQUEST['mailbox_id']);					
					
					if ( $this->id != null )	
					{
						$_email = new USAM_Email_Filter( $this->id );						
						$_email->set( $insert );	
						$_email->save();	
					}
					else
					{ 		
						$_email = new USAM_Email_Filter( $insert );
						$_email->save();
						$this->id = $_email->get('id');					
					}							
				}
				else
				{
					if ( isset($_REQUEST['from_mailbox_id']) )
					{ 
						$object_id  = absint($_POST['object_id']);		
						$object_type  = sanitize_title($_POST['object_type']);		
						$reply_message_id  = absint($_POST['reply_message_id']);		
						
						$from_mailbox_id  = absint($_POST['from_mailbox_id']);					
						$address = trim($_REQUEST['to']);	
						$to_name = '';	
						$to_email = $address;
						
						if ( stripos($address, ',') === false)
						{
							if( preg_match('/<[ ]?(([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6})>$/i', $address, $regs) )	
							{				
								$to_email = $regs[1];
							}											
							if( preg_match('/^(.*?)</i', $address, $regs) )	
							{		
								$to_name = $regs[1];
							}							
						}						
						$copy_email = trim($_REQUEST['copy_to']);										
						$subject = sanitize_text_field($_REQUEST['subject']);
						
						$message = $_REQUEST['message'];				
						$message = stripslashes( $message );
						$message = str_replace( array( "\n\r" ), '<br>', $message );				
						$message = htmlspecialchars_decode( $message );	
						$insert_email = array('body' => $message, 'subject' => $subject, 'to_email' => $to_email, 'to_name' => $to_name, 'copy_email' => $copy_email, 'read' => 1, 'folder' => 'drafts', 'mailbox_id' => $from_mailbox_id, 'object_id' => $object_id, 'object_type' => $object_type, 'reply_message_id' => $reply_message_id );	
					}	
					else
						return false;
					
					if ( $this->id != null && (!isset($_REQUEST['screen']) || $_REQUEST['screen'] != 'reply' && $_REQUEST['screen'] != 'forward') )	
					{
						$_email = new USAM_Email( $this->id );						
						$_email->set( $insert_email );	
						$_email->save();	
					}
					else
					{ 		
						$_email = new USAM_Email( $insert_email );
						$_email->save();
						$this->id = $_email->get('id');					
					}	
					$attachments = array();
					if ( !empty($_REQUEST['fileupload']) )	
					{ // Загруженные пользователем файлы				
						$directory = USAM_FILE_DIR.$user_ID.'/';
						foreach ($_REQUEST['fileupload'] as $attachment ) 	
						{
							$attachments[] = array( 'file_path' => $directory.stripcslashes($attachment), 'delete' => 1 );						
						}
					} 		
					if ( !empty($_REQUEST['attachments']) )	
					{   // Документы
						foreach ($_REQUEST['attachments'] as $attachment ) 				
							$attachments[] = array( 'file_path' => stripcslashes($attachment), 'delete' => 0 );
					}			
					$_email->set_attachments( $attachments );			
					if ( isset($_REQUEST['send']) )
					{				
						$email_sent = $_email->send_mail();	
						$this->sendback = remove_query_arg( array('action', 'id' ),$this->sendback  );		
						wp_redirect( $this->sendback );
						exit;
					}
				}
			break;		
			case 'clearing_email':	// Удалить письма по заданному периоду								
				$day = !empty($_REQUEST['auto_clearing_day'])?sanitize_text_field($_REQUEST['auto_clearing_day']):'';	
	
				if ( empty($day) )
					return false;			
				
				$args = array();
				$args['date_query'] = array( 'before' => date('Y-m-d H:i:s', strtotime('-'.$day.' days')), 'inclusive' => true );		
				$args['fields'] = 'id';	
				$args['folder_not_in'] = array('drafts', 'outbox');	
				$args['mailbox'] = $mailbox_id;
				
				$ids = usam_get_emails( $args );			
				foreach ( $ids as $id ) 
				{
					usam_delete_email( $id );
				}					
				$this->sendback = add_query_arg( array( 'action' => 'clearing' ), $this->sendback );
				wp_redirect( $this->sendback );
				exit;
			break;			
		/*	$_email = new USAM_Email( $insert_email );
			$_email->save();	
			$email_sent = $_email->send_mail();	
				
			$this->sendback = remove_query_arg( array('action', 'id' ),$this->sendback  );
			$this->sendback = add_query_arg( array( 'send_email' => $email_sent ), $this->sendback );								
			$this->redirect = true;		*/	
			case 'send':			
				if ( $this->id )
				{	
					$_email = new USAM_Email( $this->id );
					$email_sent = $_email->send_mail();			
				}				
				$this->sendback = remove_query_arg( array('action', 'id' ),$this->sendback  );
				$this->sendback = add_query_arg( array( 'send_email' => $email_sent, 'f' => 'drafts' ), $this->sendback );	
				$this->redirect = true;
			break;
			case 'download':				
				$mailbox_ids = usam_get_mailboxes( array( 'fields' => 'id' ) );	
				$i = 0;
				foreach ( $mailbox_ids as $mailbox_id ) 
				{
					$mailboxes = new USAM_POP3( $mailbox_id );	
					if ( $mailboxes->connect_open() )
					{
						$number = $mailboxes->get_download_number_emails();
						$i += $number;
					}
				}
				usam_create_system_process( __("Загрузить письма", "usam" ), 1, 'download_email_pop3_server', $i, 'download_email_pop3_server' );
				
				$this->sendback = remove_query_arg( array('action' ),$this->sendback  );	
				$this->redirect = true;	
			break;
			case 'add_contact':
				
				foreach ( $this->records as $id )	
				{
					$email = usam_get_email( $id );
					
					$from_name = explode( ' ', $email['from_name'] );
					$lastname = isset($from_name[0])?$from_name[0]:'';
					$firstname = isset($from_name[1])?$from_name[1]:'';
					
					$data = array( 'lastname' => $lastname, 'firstname' => $firstname, 'contact_source' => 'email', 'manager_id' => $user_ID );		
					
					$users = get_users( array('search_columns' => array('email'), 'search' => $email['from_email'] ) );
					if ( !empty($users) )
						$data['user_id'] = $users[0]->ID;
					
					$contact_id = usam_insert_contact( $data );
					$data = array( 'value' => $email['from_email'], 'value_type' => 'private', 'type' => 'email', 'contact_id' => $contact_id, 'customer_type' => 'contact' );
					$communication_id = usam_insert_communication( $data );	
				}
				$this->sendback = remove_query_arg( array('action' ),$this->sendback  );	
				$this->redirect = true;	
			break;
		}			
	}	

	public function tab_structure() 
	{	
		if ( is_object($this->item_table) )
		{
			$this->item_table->display_form();
		}		
		else
		{
			$this->display_table_email();
		}
	}
	
	public function display_table_email() 
	{			
		global $email_id, $email_folder, $mailbox_id, $user_ID;		
		
		$this->display_title();				
		if ( isset($_GET['table']) && $_GET['table'] == 'email_filters' )	
		{
			$this->list_table->display_table();
		}
		else
		{			
			if ( isset($_GET['action']) && ( $_GET['action'] != 'delete' ) && $_GET['action'] != '-1' )
			{
				$this->list_table->display_table();
			}
			else
			{		
			if ( isset($_GET['company']) )
			{
				$company_id = absint($_GET['company']);	
				$company = usam_get_company( $company_id );
				?>
				<h3 class="search_title"><?php printf( __( 'Поиск общений с компанией &#8220;%s&#8221;' ), esc_html( stripslashes( $company['name'] ) ) ); ?></h3>
				<?php 
			}
			if ( isset($_GET['contact']) )
			{
				$contact_id = absint($_GET['contact']);	
				$contact = usam_get_contact( $contact_id );
				?>
				<h3 class="search_title"><?php printf( __( 'Поиск общений с &#8220;%s&#8221;' ), esc_html( stripslashes( $contact['lastname'].' '.$contact['firstname'] ) ) ); ?></h3>
				<?php 
			}
			?>
			<div id = "emails" class="messaging_management">
				<div class = "list_folders">
					<div class = "mailboxes">
						<?php 						
						$mailboxes = usam_get_mailboxes( array('user_id' => $user_ID) );								
						if ( !empty($mailboxes) )
						{							
							$system_folders = usam_get_system_folders();
							$system_folders = array_keys($system_folders);
							
							$ids = array();
							foreach ( $mailboxes as $mailbox ) 
								$ids[] = $mailbox->id;
								
							$email_folders = usam_get_email_folders( array( 'mailbox_id' => $ids ) );	
							$folders = array();
							foreach ( $email_folders as $folder ) 
							{
								$folders[$folder->mailbox_id][] = $folder;								
							}							
							foreach ( $mailboxes as $mailbox ) 
							{ 
								?>
								<div id ="folders_mailbox_<?php echo $mailbox->id; ?>" class = "folders" data-mailbox_id="<?php echo $mailbox->id; ?>">
									<span class='name_email'><?php echo $mailbox->email; ?></span>
									<ul>					
										<?php
										if ( !isset($folders[$mailbox->id]) )
											continue;
										$url = add_query_arg( array( 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox->id ), admin_url('admin.php') );
										foreach ( $folders[$mailbox->id] as $folder ) 
										{
											$link = add_query_arg( array( 'f' => $folder->slug ), $url );											
											$class = in_array($folder->slug, $system_folders)?'folder_system':'';
											if ( $folder->slug == $email_folder && $mailbox_id == $mailbox->id )
												$class .= " folder_current";
											
											if ( $folder->slug == 'inbox' && $folder->not_read )
												$title = $folder->name." <strong id='new_email_numbers'>(<span class='numbers'>".$folder->not_read."</span>)</strong>";	
											else
												$title = $folder->name;
											
											echo "<li class = 'folder $class' data-folder='$folder->id'><a href='$link'>$title</a>
												<div class='usam_menu'><span class='menu_name dashicons dashicons-arrow-down-alt2'></span>
													<div class='menu_content email_menu'>														
														<ul>														
															<li id='read_folder'>".__('Прочитано', 'usam')."</li>															
															<li id='add_folder'>".__('Добавить папку', 'usam')."</li>
															<li id='open_clear_folder'>".__('Очистить папку', 'usam')."</li>";															
															if ( !in_array($folder->slug, $system_folders) )
																echo "<li id='remove_folder'>".__('Удалить папку', 'usam')."</li>";
															echo "
														</ul>
													</div>
												</div>
											</li>";
										}
										?>										
									</ul>
								</div>	
							<?php 
							} 					
						} 
						else 
						{
							printf(__('У вас еще не подключено ни одного почтового ящика. Пожалуйста подключите ящик на <a href="%s">этой странице</a>!','usam'), admin_url('admin.php?page=shop_settings&tab=mailboxes'));					
						}
						?>						
					</div>
				</div>
				<div class = "list_email">
					<?php $this->list_table->display_table(); ?>
				</div>		
				<div class = "display_email">									
					<div class = 'menu_fixed_right'>
						<div class = "email_body">
							<div class = "email_header"><?php $this->get_email_html( $email_id ) ?></div>	
							<div class="message"><iframe src="<?php echo usam_url_admin_action( 'display_mail_body', '', array('id' => $email_id) ); ?>"></iframe></div>
						</div>	
					</div>					
				</div>	
			</div>			
			<?php
			}
		}			 
	}
	
	public function get_email_html( $id ) 
	{ 
		global $mailbox_id;
		
		if ( empty($id) )
			return '';
		
		$email = usam_get_email( $id );		 
		if ( !empty($email) ) 
		{ 
			$url = USAM_UPLOAD_URL."e-mails/$id/";
			$email_attachments = usam_get_attachments( $id );
			$related_messages = usam_get_emails( array( 'reply_message_id' => $id ) );
			
		/*	if ( $email['read'] != 1 )
			{
				usam_update_email( $id, array('read' => 1) );
				$email['read'] = 1;
			}
			*/
			$body = $email['body'];											
			$body =  preg_replace_callback("/\n>+/u", 'usam_email_replace_body', $body );
			$from = !empty($email['from_name'])?$email['from_name'].' - '.$email['from_email']:$email['from_email'];
			$to = !empty($email['to_name'])?$email['to_name'].' - '.$email['to_email']:$email['to_email'];
			?>
			<div class = "message_subject"><?php echo $email['subject']; ?></div>
			<div class = "message_header_contacts contacts_row_from">
				<div class = "message_header_label"><?php _e('От', 'usam') ?>:</div>
				<div class = "message_header_text"><?php echo $from; ?></div>
			</div>			
			<div class = "message_header_contacts contacts_row_to">		
				<div class = "message_header_label"><?php _e('Кому', 'usam') ?>:</div>
				<div class = "message_header_text"><?php echo $to; ?></div>			
			</div>
			<?php if ( !empty($email['copy_email']) ) { ?>
			<div class = "message_header_contacts contacts_row_to">		
				<div class = "message_header_label"><?php _e('Копия', 'usam') ?>:</div>
				<div class = "message_header_text"><?php echo $email['copy_email']; ?></div>			
			</div>
			<?php } ?>
			<?php 
			if ( $email['server_message_id'] ) 
			{
				$title =  __('Получено', 'usam');
				$date =  $email['date_insert'];
			} 
			else 
			{ 	
				if ( !empty($email['sent_at']) ) 
				{ 
					$title =  __('Отправлено', 'usam');
					$date =  $email['sent_at'];
				}
				else
				{
					$title =  __('Создано', 'usam');
					$date =  $email['date_insert'];
				}
			}
			?>
			<div class = "message_header_contacts contacts_row_to">		
				<div class = "message_header_label"><?php echo $title ?>:</div>				
				<div class = "message_header_text"><?php echo usam_local_date( $date ); ?></div>
			</div>						
			<?php 
			if ( !empty($email_attachments)) 
			{ 
				$attachments = array();
				foreach ( $email_attachments as $attachment ) 				 
					$attachments[] = array( 'url' => USAM_UPLOAD_URL."e-mails/$id/".$attachment['name'], 'path' => USAM_UPLOAD_DIR."e-mails/$id/".$attachment['name'], 'title' => $attachment['name'] );
				
				echo usam_get_form_attachments( $attachments, false );		
			} 
			if ( !empty($related_messages)) 
			{ 
				?>
				<div class = "related_messages email_block">
					<div class = "email_block-content">
						<h4><?php _e('Уже есть ответ на это письмо', 'usam') ?>:</h4>
						<ul>					
						<?php							
						foreach ( $related_messages as $related_message ) 
						{ 
							echo "<li><a href='".wp_nonce_url( add_query_arg( array('page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'email_id' => $related_message->id, 'f' => $related_message->folder ), admin_url('admin.php') ))."'><span class='dashicons dashicons-undo'></span> <strong>&laquo;$related_message->subject&raquo;</strong></br>".__('от', 'usam')." ".usam_local_date( $related_message->date_insert )." </a></li>";
						}
						?>
						</ul>
					</div>
				</div>
				<?php
			}	
			?>			
			<div class = "mail_button email_block">
				<ul>						
					<?php 
					if ( $id && $email['server_message_id'] )
					{ 
						?>	
						<li><a class="usam-reply-link" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'reply', 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-undo"></span><?php _e('Ответить', 'usam') ?></a></li>
						<li><a class="usam-forward-link" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'forward', 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-undo"></span><?php _e('Переслать', 'usam') ?></a></li>
						<?php 
						if ( $email['read'] )
						{
						?>
							<li>
								<a class="usam-not_read-link" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'not_read', 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-email-alt"></span><?php _e('Не прочитано', 'usam') ?></a>
							</li>	
						<?php 
						}
						else
						{
							?>	
							<li>
								<a class="usam-read-link" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'read', 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-email-alt"></span><?php _e('Прочитано', 'usam') ?></a>
							</li>
							<?php 					
						}						
					}
					?>
					<li><a class="usam-email_print-link" target="_blank" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'email_print', 'page' => $this->page_name, 'tab' => $this->tab, 'm' => $mailbox_id, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-media-text"></span><?php _e('Распечатать', 'usam') ?></a></li>
					<li><a class="usam-delete-link" href="<?php echo wp_nonce_url( add_query_arg( array('action' => 'delete', 'page' => $this->page_name, 'tab' => $this->tab, 'id' => $id ), admin_url('admin.php') )); ?>"><span class="dashicons dashicons-no-alt"></span><?php _e('Удалить', 'usam') ?></a></li>
				</ul>
			</div>
			<?php
		} 
	}
}