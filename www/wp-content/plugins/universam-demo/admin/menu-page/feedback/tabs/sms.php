<?php
class USAM_Tab_sms extends USAM_Tab
{	
	public function __construct()
	{		
		require_once( USAM_FILE_PATH .'/includes/feedback/sms_query.class.php'  );
		$this->header = array( 'title' => __('СМС сообщения', 'usam'), 'add' => false, 'description' => __('Здесь Вы можете просматривать отправленные смс сообщения.','usam') );		
		global $email_folder;
		
		if ( !empty($_REQUEST['f']) )
			$email_folder = sanitize_title($_REQUEST['f']);
		else
			$email_folder = 'sent';	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $email_folder;
		
		switch( $this->current_action )
		{
			case 'delete':
				if ( $email_folder == 'deleted' )
				{
					foreach ( $this->records as $id => $value )	
					{
						usam_delete_sms( $id );
					}
				}
				else
				{ 
					foreach ( $this->records as $id => $value )	
					{
						usam_update_sms( $id, array('folder' => 'deleted' ) );
					}
				}
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'read':				
				foreach ( $this->records as $id => $value )	
				{
					usam_update_sms( $id, array('read' => 1) );
				}
			break;						
		}			
	}		
	
	public function tab_structure() 
	{				
		global $email_folder, $user_ID;		
		
		if ( isset($_GET['action']) && ( $_GET['action'] != 'delete' ) && $_GET['action'] != '-1' )
		{
			$this->list_table->display_table();
		}
		else
		{
			if ( !empty($_GET['email_id']) )
			{
				$id = absint($_GET['email_id']);		
				$sms = usam_get_sms( $id );			
				$message = $sms['message'];
			}
			else
			{
				$message = '';
			}
			$folders = array( 'sent' => __('Отправленные', 'usam' ), 'outbox' => __('Исходящие', 'usam' ), 'drafts' => __('Черновики', 'usam' ) );
			?>			
			<div id = "sms_list" class = "messaging_management">
				<div class = "list_folders">
					<div class = "mailboxes">
						<?php $numbers = usam_get_number_sms_messages();	?>
						<div class = "folders system">							
							<ul>					
								<?php
								$url = remove_query_arg( array( 'email_id' ) );										
								foreach ( $folders as $key => $name ) 
								{
									$link = add_query_arg( array( 'page' => $this->page_name, 'tab' => $this->tab, 'f' => $key ), admin_url('admin.php') );
									if ( $key == $email_folder )
										$current = "folder_current";
									else
										$current = "";
									if ( $key == 'inbox' && isset($numbers[$key]) )
										$name = $name." <strong>(".$numbers[$key].")</strong>";	
									
									echo "<li class = '$current'><a href='$link' data-folder='$key' >$name</a></li>";
								}
								?>
							</ul>	
						</div>	
					</div>
				</div>
				<div class = "list_email">
					<?php $this->list_table->display_table(); ?>
				</div>		
				<div class = "display_email">									
					<div class = 'menu_fixed_right'>
						<div class = "email_body">
							<div class = "email_header">
								<div class = "message_header_contacts contacts_row_to">		
									<div class = "message_header_label"><?php _e('Кому', 'usam') ?>:</div>
									<div class = "message_header_text"></div>				
								</div>
							</div>	
							<div class="message"><?php echo $message; ?></div>							
						</div>	
					</div>					
				</div>	
			</div>			
			<?php			
		}		
	}
}