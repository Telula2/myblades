<?php
class USAM_Tab_Price extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Цены для сравнения, отправленные посетителями', 'usam'), 'description' => 'Здесь Вы можете получать информацию о цена у конкурентов, отправленную нашими покупателями.' );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;		
		switch( $this->current_action )
		{			
			case 'delete':				
				$in = implode( ', ', $this->records );
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE id IN ($in)");
				if ($result >= 1 )
					$i = count($this->records);							
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;				
			break;
			case 'save':	
				if( isset($_REQUEST['price_found']) )
				{	
					$data  = array( 'status' => sanitize_textarea_field($_REQUEST['status']), 'price_found' => sanitize_textarea_field($_REQUEST['price_found']) );
					if ( $this->id != null )
						$result = usam_update_price_comparison( $this->id, $data );	
				}
			break;	
		}		
	}		
}