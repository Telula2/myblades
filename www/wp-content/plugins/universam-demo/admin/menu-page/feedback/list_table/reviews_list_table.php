<?php
class USAM_List_Table_reviews extends USAM_List_Table
{
	private $status = array( '0' => 'Не утвержденные', '1' => 'Утвержденные', '2' => 'В корзине' );	
	
	protected $orderby = 'id';
	protected $order   = 'desc';	
	private $display_status = '-1';
	
    function __construct( $args = array() )
	{	
		parent::__construct( $args );		
		
		if ( isset($_GET['status']) && $_GET['status'] != '-1' )
			$this->display_status = $_GET['status'];
    }	
	
	function output_rating( $rating ) 
	{
        $out = '';
        $out .= '<div class="your_review_rating">';		
		for ($i = 1; $i <= 5; $i++) 
		{
			$selected = $rating >= $i?'selected':'';
			$out .= "<span class='star $selected'></span>";
		}		
        $out .= '</div>';
        return $out;
    }	

	function get_views() 
	{	
		global $wpdb;	
		$sendback = remove_query_arg( array('status') );
		
		$total = 0;
		$results = $wpdb->get_results("SELECT COUNT(*) AS count, status FROM ".USAM_TABLE_CUSTOMER_REVIEWS." GROUP BY status");			
		foreach ( $results as $key => $value )
		{		
			$total	+= $value->count;
		}			
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['status'])) ?  'class="current"' : '' ).">". __('Все записи','usam')." <span class='count'> ($total)</span></a>";
		foreach ( $results as $key => $value )
		{			
			if ( $value->count != 0 )
			{
				$sendback = add_query_arg( array( 'status' => $value->status ) );
				$views[$value->status] = "<a href='$sendback' ". (( isset ($_GET['status']) && $_GET['status'] == $value->status ) ?  'class="current"' : '' ).">".$this->status[$value->status]." <span class='count'> (".$value->count.")</span></a>";
			}
		}	
		return $views;
	}

	function url() 
	{	
		return "?page=feedback&tab=reviews";
	}
	
	function column_author( $item ) 
	{			
		$update_path = get_admin_url()."admin-ajax.php?page=feedback&tab=reviews&id={$item->id}&action=update_field";
		$hash = md5( strtolower( trim( $item->mail ) ) );		
		
		if ( !empty($item->user_id) )
		{		
			$user = get_user_by('id', $item->user_id);
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo $user->user_login; ?></span>
			</div>
			<?php 
		}
		echo usam_get_fast_data_editing( $item->name, $item->id, 'name', 'review_edit', 'input' ); ?><br />
		<?php 		
		if ( !empty($item->mail) )
		{		
			?> 
			<a href="mailto:<?php echo $item->mail; ?>"><?php echo $item->mail; ?></a><br />
			<?php 
		}		
		if ( !empty($item->reviewer_ip) )
		{		
			?> 
			<a href="<?php echo $this->url(); ?>&amp;s=<?php echo $item->reviewer_ip; ?>"><?php echo $item->reviewer_ip; ?></a><br />
			<?php
		}		
		$options = get_option( 'usam_reviews' );    
		
		$custom_unserialized = @unserialize($item->custom_fields);												
		if ( !empty($custom_unserialized) )
		{	
			?> 
			<div class="custom_fields">
			<?php
			foreach ($custom_unserialized as $name => $value ) 
			{				
				echo "<strong>".$options['fields'][$name]['title'].":</strong> ";
				echo usam_get_fast_data_editing( $value, $item->id, 'custom_fields', 'review_edit', 'input', '', '', $name );
				echo "<br />";
			}
			?> 
			</div>
			<?php
		}
		$str = '[[1,"'. __("Одна звезда","usam").'"],[2,"'.__("Две звезды","usam").'"],[3,"'.__("Три звезды","usam").'"],[4,"'.__("Четыре звезды","usam").'"],[5,"'.__("Пять звезд","usam").'"]]'; 
		echo usam_get_fast_data_editing( $this->output_rating($item->rating), $item->id, 'rating', 'review_edit', 'select', 'make_stars_from_rating', $str ); 
	}
	
	function column_page( $item ) 
	{		
		$product = get_post( $item->page_id );	
		if ( $product->post_type == "usam-product" )
		{
			echo usam_get_product_thumbnail( $item->page_id, 'manage-products' );
		}
		?> 
		<div class="submitted-on">
			<a target="_blank" href="<?php echo usam_reviews_url($item, $this->page); ?>" title = "<?php _e("Посмотреть отзыв на странице","usam") ?>"><?php echo $product->post_title; ?></a>
		</div>
		<?php
	}
	
	function url_status( ) 
	{		
		return $this->get_nonce_url( $this->url()."&amp;status=".$this->display_status );
	}
	
	function column_review( $item ) 
	{			
		echo "<h4>".usam_get_fast_data_editing( stripslashes($item->title), $item->id, 'title', 'review_edit', 'input' )."</h4>"; 
		
		echo usam_get_fast_data_editing( stripslashes($item->review_text), $item->id, 'review_text', 'review_edit', 'textarea' ); ?>		  
		<br />
		<div style="font-size:13px;font-weight:bold;">
			<?php _e("Официальный ответ","usam") ?>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span style="font-size:11px;font-style:italic;"><?php _e("Оставьте это поле пустым, если вы не хотите, чтобы это было опубликовано","usam") ?></span>
		</div>
		<?php echo usam_get_fast_data_editing( stripslashes($item->review_response), $item->id, 'review_response', 'review_edit', 'textarea' ); ?>	
		<?php	
		if ( $item->status == 0 ) 
		{
			$actions = array(
				'approvereview' => $this->add_row_actions( $item->id, 'approvereview', __( 'Утвердить', 'usam' ) ),	
				'trashreview' => $this->add_row_actions( $item->id, 'trashreview', __( 'В корзину', 'usam' ) ),					
			);	
		}
		elseif ( $item->status == 1 ) 
		{
			$actions = array(
				'unapprovereview' => $this->add_row_actions( $item->id, 'unapprovereview', __( 'Не утвердить', 'usam' ) ),	
				'trashreview' => $this->add_row_actions( $item->id, 'trashreview', __( 'В корзину', 'usam' ) ),	
				'vk' => $this->add_row_actions( $item->id, 'vk', __( 'Опубликовать в контакте', 'usam' ) ),						
			);	
		}
		elseif ( $item->status == 2 ) 
		{			
			$actions = array(
				'delete' => $this->add_row_actions( $item->id, 'delete', __( 'Удалить навсегда', 'usam' ) ),		
			);	
		}			
		$this->row_actions_table( '', $actions );		
	}
	
	function column_date( $item ) 
	{
		echo usam_get_fast_data_editing( usam_local_date( $item->date_time ), $item->id, 'date_time', 'review_edit', 'input' );		
	}
	   
   
    function get_bulk_actions_display() 
	{	
		if ( $this->display_status != 1 )
			$actions['approvereview'] = __( 'Утвердить', 'usam' );
		if ( $this->display_status != 0 )
			$actions['unapprovereview'] = __( 'Не утвердить', 'usam' );
		if ( $this->display_status != 2 )
			$actions['trashreview'] = __( 'Переместить в корзину', 'usam' );
		if ( $this->display_status == 2 )
			$actions['delete'] = __( 'Удалить навсегда', 'usam' );
		if ( $this->display_status == 1 )
			$actions['vk'] = __( 'Опубликовать в контакте', 'usam' );
		return $actions;
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'author'         => array('name', false),
			'page'           => array('page_id', false),		
			'date'           => array('date_time', false),		
			'status'         => array('status', false)
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',		
			'author'        => __( 'Автор', 'usam' ),		
			'review'        => __( 'Отзыв', 'usam' ),		
			'page'          => __( 'Страница', 'usam' ),		
			'date'          => __( 'Дата', 'usam' ),			
        );
        return $columns;
    }	
	
	function prepare_items() 
	{		
		global $wpdb;		
		$this->get_standart_query_parent( );
		
		$search_terms = $this->search != '' ? explode( ' ', $this->search ): array();
		
		$search_sql = array();			
		foreach ( $search_terms as $term )
		{
			$search_sql[$term][] = "name LIKE '%".esc_sql( $term )."%'";		
			$search_sql[$term][] = "mail = '".esc_sql( $term )."'";				
			$search_sql[$term][] = "reviewer_ip = '".esc_sql( $term )."'";
			$search_sql[$term][] = "review_text LIKE '%".esc_sql( $term )."%'";
			$search_sql[$term][] = "review_response LIKE '%".esc_sql( $term )."%'";		
			$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
		}		
		if ( $search_sql )
		{
			$this->where[] = implode( ' AND ', array_values( $search_sql ) );
		}	
		if ( $this->display_status != '-1' )
		{
			$this->where[] = 'status='.$this->display_status;			
		}
			
		$where = implode( ' AND ', $this->where );
		$sql_query = "SELECT * FROM ".USAM_TABLE_CUSTOMER_REVIEWS." WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results( $sql_query );
		
		$post_ids = array();
		foreach ( $this->items as $key => $item )
		{
			if ( $item->page_id != 0 )
				$post_ids[] = $item->page_id;
		}
		if ( !empty($post_ids) )
			$products = usam_get_products( array( 'post__in' => $post_ids, 'update_post_term_cache' => false ), true);
		
		$sql_query = "SELECT COUNT(*) FROM ".USAM_TABLE_CUSTOMER_REVIEWS." WHERE {$where}";
		$this->total_items = $wpdb->get_var( $sql_query );
		
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}