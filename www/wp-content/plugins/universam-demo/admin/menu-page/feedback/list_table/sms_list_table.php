<?php
class USAM_List_Table_sms extends USAM_List_Table 
{
	public $status = array( '0' => 'Прочитанные', '1' => 'Не прочитанные' );
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }			

	function column_from( $item ) 
    {
		global $email_folder, $mailbox_id;
		
		if ( $email_folder == 'drafts' ) 
		{
			$actions['edit'] = $this->add_row_actions( $item->id, 'edit', __( 'Редактировать', 'usam' ) );
			$actions['send'] = $this->add_row_actions( $item->id, 'send', __( 'Отправить', 'usam' ) );	
		}
		else
			$actions['reply'] = $this->add_row_actions( $item->id, 'reply', __( 'Ответить', 'usam' ) );	
				
		$actions['delete'] = $this->add_row_actions( $item->id, 'delete', __( 'Удалить', 'usam' ) );
		
		$from = !empty($item->from_name)?$item->from_name:$item->from_email;
		$this->row_actions_table( $from, $actions );	
	}	
	
	function column_date( $item ) 
    {				
		echo usam_local_date( $item->sent_at, 'd.m.Y H:i:s' );
	}		
		   
    function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),		
		);
		return $actions;
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(	
			'phone'      => array('phone', false),					
			'date'       => array('date', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        global $email_folder;		
		$columns = array(   
			'cb'            => '<input type="checkbox" />',	
			'phone'         => __( 'Получатель', 'usam' ),			
			'message'       => __( 'Сообщение', 'usam' ),			
        );
		
		if ( $email_folder != 'outbox' )
			$columns['date'] = __( 'Дата', 'usam' );		
	
        return $columns;
    }
	
	public function single_row( $item ) 
	{		
		global $email_id;	
		
		if ( $item->id == $email_id )
			$message_current = "message_current";
		else
			$message_current = "";
		
		echo "<tr data-id = '$item->id' class = '$message_current'>";
		$this->single_row_columns( $item );
		echo '</tr>';
	}
			
	function prepare_items() 
	{			
		global $email_folder, $email_id;
				
		$args = array( 'cache_results' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'search' => $this->search, 'order' => $this->order, 'orderby' => $this->orderby );		
		
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;		
		
		$args['folder'] = $email_folder;			
		$query = new USAM_SMS_Query( $args );
		$this->items = $query->get_results();
		
		if ( !empty($_GET['email_id']) )
			$email_id = absint($_GET['email_id']);	
		elseif ( isset($this->items[0]) )		
			$email_id = $this->items[0]->id;
		
		if ( $this->per_page )
		{
			$this->total_items = $query->get_total();			
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}