<?php
class USAM_List_Table_Chat extends USAM_List_Table 
{	
	protected $orderby = 'date_insert';		
	protected $order = 'desc';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );		
		add_action( 'admin_head', array( $this, 'admin_header' ) );
    }	
	
	function admin_header() 
	{	
		echo '<style type="text/css">';		
		echo '.wp-list-table .column-datetime{ width: 90px; }';
		echo '.wp-list-table .column-status{width: 190px; }'; 
		echo '.wp-list-table .column-status select{font-size: 12px;}';
		echo '.wp-list-table .column-description{ width: 10%;}';
		echo '.wp-list-table .column-description textarea{height: 186px;}';
		echo '.wp-list-table .column-response{width: 30%; }';	
		echo '</style>';		
	}

	function column_count( $item ) 
    {			
		if ( $item->new > 0)		
			echo '<div class="count_new">+'.$item->new.'</div>';
		else
			echo '<div class="count_new"></div>';
    }	
   
    function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}		
	
	function get_sortable_columns() 
	{
		$sortable = array(			
			'status'     => array('status', false),
			'user_ID'    => array('user_ID', false),		
			'date'       => array('date_insert', false)
			);
		return $sortable;
	}
		
	function get_columns(){
        $columns = array(   
			'cb'            => '<input type="checkbox" />',				
			'customer'      => __( 'Клиент', 'usam' ),		
			'manager'       => __( 'Менеджер', 'usam' ),				
			'name'          => __( 'Тема', 'usam' ),
			'count'         => '',
			'date'          => __( 'Дата', 'usam' ),			
        );
        return $columns;
    }
	
	public function single_row( $item ) 
	{
		static $row_class = '';
		$row_class = ( isset($_GET['sel']) && $item->id == $_GET['sel'] ? 'current_sel' : '' );
		$new_message_class = ( $item->new > 0 ? 'new_message' : '' );
		
		
		echo '<tr class ="row '.$row_class.' '.$new_message_class.'" id = "im_dialog-'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}	
	
	function prepare_items() 
	{		
		global $wpdb, $user_ID;
				
		$this->get_standart_query_parent( );
	
		$join = array();	
	
		$selects = array( USAM_TABLE_CHAT_DIALOGS.'.*',  );	
		$selects[] = " ( SELECT COUNT(*) FROM ".USAM_TABLE_CHAT." AS chat WHERE chat.topic=".USAM_TABLE_CHAT_DIALOGS.".id AND chat.status = 0 AND user_id != '$user_ID') AS new";				
		$selects = implode( ', ', $selects );		
		$join = implode( ' ', $join );	
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS $selects FROM ".USAM_TABLE_CHAT_DIALOGS." $join WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);
	
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );

		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}