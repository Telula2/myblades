<?php
require_once( USAM_FILE_PATH .'/admin/menu-page/feedback/includes/customer_requests_list_table.php' );
class USAM_List_Table_back_call extends USAM_List_Table_Customer_Requests 
{
	protected $type_message = 'back-call';	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }	
	
	function column_user( $item ) 
	{	
		if ( $item->user_id != 0)
		{	
			$user = get_user_by('id', $item->user_id);
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo $user->user_login; ?></span>
			</div>
			<?php 
		}		
		$actions['delete'] = $this->delete_url( $item->id );		
		$this->row_actions_table( '', $actions );	
	}	
	
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',	
			'user'          => __( 'Отправитель', 'usam' ),			
			'phone'         => __( 'Телефон', 'usam' ),						
			'location'      => __( 'Город', 'usam' ),			
			'manager'       => __( 'Менеджер', 'usam' ),			
			'status'        => __( 'Статус обработки', 'usam' ),							
			'description'   => __( 'Заметка', 'usam' ),			
			'date'          => __( 'Дата', 'usam' )
        );
        return $columns;
    }	
}