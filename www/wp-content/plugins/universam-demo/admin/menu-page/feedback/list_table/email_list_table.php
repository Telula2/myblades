<?php
class USAM_List_Table_email extends USAM_List_Table 
{
	public $status = array( '0' => 'Прочитанные', '1' => 'Не прочитанные' );
	public $orderby = 'id';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }			

	public function return_post()
	{
		return array( 'm', 'f' );
	}	
	
	function column_name( $item ) 
    {
		global $email_folder, $mailbox_id;
		
		if ( $email_folder == 'drafts' ) 
		{
			$actions['edit'] = $this->add_row_actions( $item->id, 'edit', __( 'Редактировать', 'usam' ), array( 'm' => $mailbox_id, 'f' => $email_folder ) );
			$actions['send'] = $this->add_row_actions( $item->id, 'send', __( 'Отправить', 'usam' ), array( 'm' => $mailbox_id, 'f' => $email_folder ) );	
		}
		else
		{
			$actions['reply'] = $this->add_row_actions( $item->id, 'reply', __( 'Ответить', 'usam' ), array( 'm' => $mailbox_id, 'f' => $email_folder ) );	
			$actions['spam'] = $this->add_row_actions( $item->id, 'spam', __( 'Спам', 'usam' ), array( 'm' => $mailbox_id, 'f' => $email_folder ) );
		}		
		$actions['delete'] = $this->add_row_actions( $item->id, 'delete', __( 'Удалить', 'usam' ), array( 'm' => $mailbox_id, 'f' => $email_folder ) );
			
		if ( $item->server_message_id )
			$text = !empty($item->from_name)?$item->from_name:$item->from_email;
		else					
			$text = !empty($item->to_name)?$item->to_name:$item->to_email;
		if ( !empty($item->object_type) && !empty($item->object_id) )
		{
			switch ( $item->object_type ) 
			{
				case 'order' :
					$order = usam_get_order( $item->object_id );
					if ( !empty($order) )
						$text .= '<br><a class="object_link" href="'.usam_get_url_order($order['id']).'" target="blank">'.__( 'Заказ', 'usam' ).' - №'.$order['id'].'</a>';
				break;
				case 'company' :
					$company = usam_get_company( $item->object_id );
					if ( !empty($company) )
						$text .= '<br><a class="object_link" href="'.add_query_arg( array('company' => $item->object_id)).'">'.$company['name'].'</a>';
				break;
				case 'contact' :
					$contact = usam_get_contact( $contact_id );
					if ( !empty($contact) )
						$text .= '<br><a class="object_link" href="'.add_query_arg( array('contact' => $item->object_id)).'">'.$contact['lastname'].' '.$contact['firstname'].'</a>';
				break;
			}			
		}
		$title = !empty($item->subject)?$item->subject:usam_limit_words(strip_tags($item->body),50);	
		$text .= '<hr size="1" width="90%">'.$title;			
		$this->row_actions_table( $text, $actions );	
	}	
	
	public function extra_tablenav_display( $which ) 
	{
		global $email_folder, $mailbox_id;
		if ( 'top' == $which ) 
		{			
			if ( $email_folder == 'deleted' )
			{
				$url =  wp_nonce_url( add_query_arg( array( 'action' => 'clear', 'page' => $this->page, 'tab' => $this->tab, 'm' => $mailbox_id ), admin_url('admin.php') ));
				echo "<a href='$url' class='button secondary clear'>".__('Очистить','usam')."</a>"; 
			}
			?> 
			<select id="set_folder">
				<option value=""><?php _e('В папку', 'usam' ); ?></option>		
				<?php 	
				$email_folders = usam_get_email_folders( array( 'mailbox_id' => $mailbox_id ) );					
				$folders = array();
				foreach ( $email_folders as $folder ) 
				{
					if ( $email_folder != $folder->slug )
						$folders[$folder->slug] = $folder->name;								
				}				
				foreach( $folders as $key => $folder_name )
				{	
					?> 
					<option value="<?php echo $key; ?>"><?php echo $folder_name; ?></option>								
					<?php 	
				}
				?>
			</select>
			<?php 	
		}	
	}
	
	function column_date( $item ) 
    {		
		global $email_folder;
		
		if ( $item->type == 'R' )
			$date = $item->date_insert;
		else
		{
			if ( empty($item->sent_at) )
				$date = $item->date_insert;
			else
				$date = $item->sent_at;
		}
		echo usam_local_date( $date, 'd.m.y H:i' );
		if ( $email_folder == 'sent' )
		{
			echo '<hr size="1" width="90%">';
			if ( !empty($item->opened_at) )
				echo "<strong>".usam_local_date( $item->opened_at, 'd.m.y H:i' )."</strong>";
			else
				_e( 'Не открыли', 'usam' );
		}		
		if ( $item->importance )			
			echo '<span class="dashicons dashicons-star-filled importance important"></span>';
		else
			echo '<span class="dashicons dashicons-star-empty importance"></span>';
		
		$attachment = usam_get_attachments($item->id);
		if ( count($attachment) > 0 )
			echo "<img class='attachment' src='".USAM_URL."/admin/images/paperclip.png' height='16' width='16'>";
	}
	
    function get_bulk_actions_display() 
	{
		global $email_folder;	
		$actions = array(
			'delete'       => __( 'Удалить', 'usam' ),
			'read'         => __( 'Прочитано', 'usam' ),
			'not_read'      => __( 'Не прочитано', 'usam' ),
			'important'    => __( 'Важное', 'usam' ),
			'not_important' => __( 'Не важное', 'usam' ),		
		);
		if ( $email_folder == 'inbox' )
			$actions['add_contact'] = __( 'Добавить контакт', 'usam' );
		return $actions;
	}
	
	function get_sortable_columns() 
	{
		$sortable = array(			
			'subject'    => array('subject', false),				
			'date'       => array('date', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        global $email_folder;		
		$columns = array(   
			'cb'            => '<input type="checkbox" />',	
			'name'          => '',		
        );	
		if ( $email_folder != 'outbox' )
			$columns['date'] = __( 'Дата', 'usam' );
        return $columns;
    }
	
	public function single_row( $item ) 
	{		
		global $email_id;
	
		if ( $item->read || $item->server_message_id == 0 )
			$message_unread = "";
		else
			$message_unread = "message_unread";	
		
		if ( $item->id == $email_id )
			$message_current = "message_current";
		else
			$message_current = "";
		
		echo "<tr data-id = '$item->id' class = '$message_current $message_unread'>";
		$this->single_row_columns( $item );
		echo '</tr>';
	}
			
	function prepare_items() 
	{			
		global $email_folder, $mailbox_id, $email_id;		
		
		$args = array( 'cache_results' => true, 'cache_attachments' => true, 'cache_object' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'search' => $this->search, 'orderby' => $this->orderby );		
		
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;
			
		if ( !empty($_GET['company']) )
		{
			$args['object_id'] = absint($_GET['company']);	
			$args['object_type'] = 'company';
		} 
		elseif ( !empty($_GET['contact']) )
		{
			$args['object_id'] = absint($_GET['contact']);	
			$args['object_type'] = 'contact';
		}
		$args['folder'] = $email_folder;		
		$args['mailbox'] = $mailbox_id;	
		$query_emails = new USAM_Email_Query( $args );
		$this->items = $query_emails->get_results();
		if ( !empty($_GET['email_id']) )
			$email_id = absint($_GET['email_id']);	
		elseif ( isset($this->items[0]) )		
			$email_id = $this->items[0]->id;
		
		if ( $this->per_page )
		{
			$this->total_items = $query_emails->get_total();			
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}