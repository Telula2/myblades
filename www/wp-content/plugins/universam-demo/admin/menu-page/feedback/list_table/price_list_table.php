<?php
class USAM_List_Table_Price extends USAM_List_Table 
{
	protected $statuses = array( '0' => 'Не обработанные', '1' => 'В обработке', '2' => 'Обработка завершена'  );	
	protected $orderby = 'date_insert';		
	protected $order = 'desc';
	protected $status = '';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
			
		add_action( 'admin_head', array( $this, 'admin_header' ) );
		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;			
    }

	function admin_header() 
	{			
		echo usam_get_modal_window( __('Быстрый ответ','usam'), 'quick_response', usam_get_form_send_message() );	
	}
	
	function get_views() 
	{	
		global $wpdb;	
		$sendback = remove_query_arg( array('status') );
		$results = $wpdb->get_results("SELECT COUNT(id) AS count, status FROM ".USAM_TABLE_PRICE_COMPARISON." GROUP BY status");		
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['status'])) ? 'class="current"' : '' ).">". __('Все записи','usam')." <span class='count'> ($this->total_items)</span></a>";		
		foreach( $results as $result )
		{		
			$sendback = add_query_arg( array( 'status' => $result->status ) );
			$views[$result->status] = "<a href='$sendback' ". (( isset ($_GET['status']) && $_GET['status'] == $result->status ) ?  'class="current"' : '' ).">".$this->statuses[$result->status]." <span class='count'> ($result->count)</span></a>";
		}	
		return $views;
	}
		
	function column_image( $item ) 
	{
		echo usam_get_product_thumbnail( $item->product_id, 'manage-products' );
	}	
		
	function column_status( $item ) 
	{		
		$out ='<select data-id = "'.$item->id.'" class = "select_status_record">';
		foreach( $this->statuses as $key => $value )
		{
			$out.="<option ".selected($key, $item->status, false)." value='$key'>$value</option>";
		}	
		$out.='</select>';			
		echo $out;
		usam_loader();	
	}
	
	public function column_mail( $item ) 
	{
		?><a id="open_quick_response" data-email="<?php echo $item->mail; ?>" data-subject="" href=""><?php echo $item->mail; ?></a><?php		
	}	
	
	function column_title( $item ) 
    {			
		$post = get_post( $item->product_id );
		$title = "<a href='".$post->guid."'>".$post->post_title."</a>";
		
		$actions = $this->standart_row_actions( $item->id );				
		$this->row_actions_table( $title, $actions );	
    }
	
	function column_url( $item ) 
    {	
		$host = parse_url($item->url, PHP_URL_HOST);
		echo "<a href='".$item->url."'>".$host."</a>";
	}
		
	function column_price( $item ) 
    {	
		echo usam_get_product_price_currency($item->product_id);
    }
	
	function column_price_found( $item ) 
    {	
		echo usam_currency_display($item->price_found);
    }
   
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
  
	function get_sortable_columns() 
	{
		$sortable = array(
			'title'          => array('title', false),
			'price_found'    => array('price_found', false),		
			'status'         => array('status', false),
			'name'           => array('name', false),
			'mail'           => array('mail', false),
			'date_insert'    => array('date_insert', false)
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',
			'image'         => '',
			'title'         => __( 'Имя товара', 'usam' ),		
			'price_found'   => __( 'Найденная цена', 'usam' ),
			'status'        => __( 'Статус обработки', 'usam' ),
			'url'           => __( 'Ссылка', 'usam'),
			'name'          => __( 'Имя отправителя', 'usam' ),			
			'mail'          => __( 'Email отправителя', 'usam' ),
			'message'       => __( 'Сообщение', 'usam' ),
			'date_insert'   => __( 'Дата', 'usam' )
        );
        return $columns;
    }
	
	function prepare_items() 
	{	
		require_once( USAM_FILE_PATH . '/includes/product/price_comparison_query.class.php'      );
		$args = array( 'cache_results' => true, 'cache_product' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby, 'search' => $this->search );		
		
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;
	
		$args = array_merge ($args, $this->get_date_for_query() );
		
		$args['date_group'] = isset( $_REQUEST['dgo'] ) ? $_REQUEST['dgo'] : 0;			
		if ( $this->status != 'all' ) 
		{			
			$args['status'] = $this->status;
		}
		else
			$args['status__not_in'] = '';		
		
		$query = new USAM_Price_Comparison_Query( $args );
		$this->items = $query->get_results();	
		if ( $this->per_page )
		{
			$this->total_items = $query->get_total();			
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}