<?php
/*
 * Отображение страницы Обратная связь
 */ 
$default_tabs = array(
	array( 'id' => 'questions',  'title' => __( 'Сообщения', 'usam' ) ),
	array( 'id' => 'back_call',  'title' => __( 'Обратный звонок', 'usam' ) ),
	array( 'id' => 'price',  'title' => __( 'Цены', 'usam' ) ),
	array( 'id' => 'reviews',  'title' => __( 'Отзывы', 'usam' ) ),
	array( 'id' => 'chat',  'title' => __( 'Чат', 'usam' ) ),
	array( 'id' => 'email',  'title' => __( 'Почта', 'usam' ) ),
	array( 'id' => 'sms',  'title' => __( 'СМС', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{	
	protected function localize_script_tab()
	{
		wp_enqueue_script( 'knob' );				
		wp_enqueue_script( 'iframe-transport' );	
		wp_enqueue_script( 'fileupload' );	
	
		return array(			
			'change_status_feedback_nonce'  => usam_create_ajax_nonce( 'change_status_feedback' ),	
			'display_message_nonce'         => usam_create_ajax_nonce( 'display_message' ),	
			'add_email_folder_nonce'        => usam_create_ajax_nonce( 'add_email_folder' ),	
			'read_email_folder_nonce'       => usam_create_ajax_nonce( 'read_email_folder' ),	
			'clear_email_folder_nonce'      => usam_create_ajax_nonce( 'clear_email_folder' ),	
			'remove_email_folder_nonce'     => usam_create_ajax_nonce( 'remove_email_folder' ),			
			'change_email_folder_nonce'     => usam_create_ajax_nonce( 'change_email_folder' ), 
			'delete_email_nonce'            => usam_create_ajax_nonce( 'delete_email' ),				
			'change_importance_email_nonce' => usam_create_ajax_nonce( 'change_importance_email' ),			
			'display_sms_nonce'             => usam_create_ajax_nonce( 'display_sms' ),					
		);		
	}		
	
	protected function delete_record_feedback()
    {	
		global $wpdb;
		$in = implode( ', ', $this->records );	
		$result = $wpdb->query("DELETE FROM ".USAM_TABLE_FEEDBACK." WHERE id IN ($in)");
		if ($result >= 1 )
			$i = count($this->records);				
		$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
		$this->redirect = true;
	}
} 	