<?php
class USAM_List_Table_Customer_Requests extends USAM_List_Table 
{
	public $status = array( '0' => 'Не обработанные', '1' => 'В обработке', '2' => 'Обработка завершена' );	
	public $topic;
	protected $type_message = '';
	protected $orderby = 'date_insert';		
	protected $order = 'desc';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		
		add_action( 'admin_head', array( &$this, 'admin_header' ) );		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );	
		
		$this->topic = usam_get_feedback_topic();	
    }	
	
	function admin_footer()
	{
		echo usam_get_modal_window( __('Ответ кленту','usam'), 'quick_response', usam_get_form_send_message() );	
	}	
		
	function admin_header() 
	{	
		echo '<style type="text/css">';		
		echo '.wp-list-table .column-status{width: 190px; }'; 
		echo '.wp-list-table .column-status select{font-size: 12px;}';
		echo '.wp-list-table .column-description{ width: 10%;}';
		echo '.wp-list-table .column-description textarea{height: 186px;}';
		echo '.wp-list-table .column-response{width: 30%; }';	
		echo '</style>';		
	}
	
	function get_views() 
	{	
		global $wpdb;		
		
		$where = '';
		if ( $this->type_message != '' )
			$where = "WHERE type_message = '$this->type_message'";
			
		$status_db = $wpdb->get_results("SELECT COUNT(*) AS count, status FROM ".USAM_TABLE_FEEDBACK." $where GROUP BY status");
		$total_sum = 0;
		$status_sum = array();
		foreach ( $status_db as $value )
		{
			$status_sum[$value->status] = $value->count;
			$total_sum += $status_sum[$value->status];
		}
		
		$sendback = remove_query_arg( array('status') );
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['status'])) ?  'class="current"' : '' ).">". __('Все записи','usam')." <span class='count'> ( $total_sum )</span></a>";	
		foreach( $this->status as $key => $value )
		{		
			if ( !empty($status_sum[$key]) )
			{
				$sendback = add_query_arg( array( 'status' => $key ) );
				$views[$key] = "<a href='$sendback' ". (( isset ($_GET['status']) && $_GET['status'] == $key ) ?  'class="current"' : '' ).">". $value." <span class='count'> (". $status_sum[$key].")</span></a>";
			}
		}	
		return $views;
	}
		
	function column_type_message( $item ) 
	{		
		if ( isset($this->topic[$item->type_message]) )
			$topic = $this->topic[$item->type_message];	
		else
			$topic = $this->topic[''];	

		echo '<a class="row-title" href="'.esc_url( add_query_arg( array('action' => 'edit', 'id' => $item->id), $this->url ) ).'">'.$topic.'</a>';		
	}
	
	function column_user( $item ) 
	{	
		if ( $item->user_id != 0)
		{	
			$user = get_user_by('id', $item->user_id);
			?> 
			<div class="user">
				<img width="32" height="32" class="avatar avatar-32 photo" src="<?php echo get_avatar_url( $item->user_id, array('size' => 32, 'default'=>'mystery' ) ); ?>" alt="" />&nbsp;
				<span><?php echo $user->user_login; ?></span>
			</div>
			<?php 
		}		
		$name = $item->name."<br>";	
		if ( $item->mail != '' )
			$name .= '</br>'.__('Почта','usam').': '.$item->mail;
		if ( $item->phone != '' )
			$name .= '</br>'.__('Телефон','usam').': '.$item->phone;		
		
		$actions = array(				
			'edit'      => $this->add_row_actions( $item->id , 'edit', __( 'Изменить', 'usam' ) ),
			'task'      => $this->add_row_actions( $item->id , 'task', __( 'Задача', 'usam' ) ),
		);
		
		$product = get_post( $item->id_product );	
		if ( !empty($product->ID) )
			$actions['order'] = $this->add_row_actions( $item->id , 'order', __( 'Заказ', 'usam' ) );
		
		$actions['delete'] = $this->delete_url( $item->id );
		
		$this->row_actions_table( $name, $actions );	
	}	
			
	function column_status( $item ) 
	{		
		$out ='<select data-id = "'.$item->id.'" class = "select_status_record">';
		foreach( $this->status as $key => $value )
		{
			if ( $item->status == $key )
				$selected = 'selected="selected"';
			else
				$selected = '';
			$out.="<option $selected value='$key'>$value</option>";
		}	
		$out.='</select>';			
		echo $out;
		usam_loader();		
	}	
	
	function column_message( $item ) 
	{			
		echo $this->format_description( $item->message );
		if ( !empty($item->mail) )
		{			
			$subject = !empty($this->topic[$item->type_message])?$this->topic[$item->type_message]." - ".get_bloginfo('name'):get_bloginfo('name');			
			?><br><br><a id="open_quick_response" data-email="<?php echo $item->mail; ?>" data-subject="<?php echo $subject; ?>" href=""><?php _e( 'Ответить', 'usam' ) ?></a><?php
		}
	}	

	function column_page( $item ) 
	{	
		$post = get_post( $item->id_product );	
		if ( empty($post->post_title) )
			return;
		
		if ( $post->post_type == 'usam-product' )
		{					
			echo usam_get_product_thumbnail( $post->ID, 'manage-products' );			
			echo "</br><a href='".$post->guid."' title='".__('Посмотреть товар','usam')."'>".$post->post_title."</a></br>";	
			echo __('Артикул','usam').": ".usam_get_product_meta( $post->ID, 'sku' );					
		}
		else
		{			
			echo "<a href='".$post->guid."' title='".__('Посмотреть страницу','usam')."'>".$post->post_title."</a>";
		}
	}	

	function column_location( $item ) 
	{	
		if ( $item->location_id )
		{
			$location = usam_get_location( $item->location_id );
			echo $location['name'];
		}
	}
		   
    function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
		  
	function get_sortable_columns() 
	{
		$sortable = array(			
			'status'         => array('status', false),
			'user'           => array('user_ID', false),
			'type_message'   => array('type_message', false),			
			'date_insert'    => array('date_insert', false),
			'manager'       => array('manager', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'            => '<input type="checkbox" />',						
			'user'          => __( 'Отправитель', 'usam' ),	
			'type_message'  => __( 'Тема', 'usam' ),			
			'message'       => __( 'Сообщение', 'usam' ),
			'location'      => __( 'Город', 'usam' ),
			'manager'       => __( 'Менеджер', 'usam' ),	
			'page'          => __( 'Отправлено со страницы', 'usam' ),					
			'status'        => __( 'Статус обработки', 'usam' ),							
			'description'   => __( 'Заметка', 'usam' ),			
			'date'          => __( 'Дата', 'usam' )
        );
        return $columns;
    }	
	
	function prepare_items() 
	{		
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'search' => $this->search, 'order' => $this->order, 'orderby' => $this->orderby );		
		
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;		
		
		if ( !empty( $_GET['status'] ) )
			$args['status'] = absint($_GET['status']);	
		
		if ( !empty( $_GET['manager'] ) )
			$args['manager'] = absint($_GET['manager']);

		if ( $this->type_message != '' )
			$args['type_message'] = $this->type_message;
		else
			$args['type_message__not_in'] = 'back-call';
		
		$query = new USAM_Feedback_Query( $args );
		$this->items = $query->get_results();		
			
		if ( $this->per_page )
		{
			$this->total_items = $query->get_total();			
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}