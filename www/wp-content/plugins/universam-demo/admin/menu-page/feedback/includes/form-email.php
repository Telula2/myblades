<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Display_Email_Form extends USAM_Edit_Form
{		
	protected $formtype = true;
	protected $attachments = array();
	public function toolbar_buttons( ) 
	{
		?>
		<ul>
			<li><?php submit_button( __('Отправить сообщение','usam'), 'primary button', 'send', false, array( 'id' => 'submit-send' ) ); ?></li>
			<li><?php submit_button( __('Сохранить','usam'), 'button', 'save', false, array( 'id' => 'submit-save' ) ); ?></li>
		</ul>	
		<?php
	}	
	
	protected function get_user_response(  )
	{ 
		return stripslashes('<br><br><br>'.usam_local_date($this->data['date_insert'], 'd M Yг., H:i').' '.__('пользователь','usam').' '.$this->data['from_name'].' '.__('написал','usam').':<br><blockquote>'.$this->data['body']."</blockquote>");
	}
		
	protected function get_data_tab(  )
	{
		global $user_ID, $mailbox_id;
		
		wp_enqueue_script( 'knob' );				
		wp_enqueue_script( 'iframe-transport' );	
		wp_enqueue_script( 'fileupload' );		
	
		if ( $this->id )
		{
			$_email = new USAM_Email( $this->id );
			if ( $this->screen != 'reply' )
				$this->attachments = $_email->get_attachments();
			$this->data = $_email->get_data();
		}
		else
			$this->data = array( 'from_name' => '', 'from_email' => '', 'subject' => '', 'body' => '', 'folder' => 'drafts', 'to_name' => '','to_email' => '', 'copy_email' => '', 'server_message_id' => 0, 'object_id' => 0, 'object_type' => 0, 'reply_message_id' => 0, 'date_insert' => '' );	
				
		$signature_email = usam_get_manager_signature_email();			
		if ( $this->data['folder'] == 'drafts' && $this->data['body'] != '' )
			$this->data['body'] = $this->data['body'];		
		elseif ( !empty($signature_email) )
		{ 
			if ( !empty($this->data['body']) )
				$this->data['body'] = $signature_email.$this->get_user_response();
			else
				$this->data['body'] = $signature_email;	
		}	
		else
			$this->data['body'] = $this->get_user_response();
		
		if ( !$this->id )
		{
			$style = new USAM_Mail_Styling();
			$this->data['body'] = $style->process_plaintext_args( $this->data['body'] );
		}
		if ( $this->data['server_message_id'] )
		{
			if ( $this->id )
				$this->data['reply_message_id'] = $this->id;
			$this->data['from'] = !empty($this->data['from_name'])? $this->data['from_name']." <".$this->data['from_email'].">":$this->data['from_email'];	
		}
		else
		{
			$this->data['from'] = !empty($this->data['to_name'])? $this->data['to_name']." <".$this->data['to_email'].">":$this->data['to_email'];			
		}		
		add_action( 'admin_footer', array(&$this, 'select_email') );	
	}
	
	function select_email( )
	{
		echo usam_get_modal_window( __('Выбрать адрес электронной почты', 'usam'), 'select_email', $this->select_email_windows() );				
	}		
		
	function select_email_windows( )
	{
		global $user_ID;	
		ob_start();	
		?>		
		<div class="usam_select_list">						
			<iframe id="usam_select_list_iframe" src="<?php echo usam_url_admin_action( 'display_address_book', admin_url('admin.php') ); ?>"></iframe>
		</div>	
		<?php 
		$out = ob_get_contents();
		ob_end_clean();		
			
		return $out;
	}
	
	public function display( ) 
	{	
		global $mailbox_id, $user_ID;
			
		$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name','email'), 'user_id' => $user_ID ) );
		$this->display_toolbar();		
		?>		
		<input type='hidden' name='object_id' value='<?php echo $this->data['object_id']; ?>' />
		<input type='hidden' name='object_type' value='<?php echo $this->data['object_type']; ?>' />
		<input type='hidden' name='reply_message_id' value='<?php echo $this->data['reply_message_id']; ?>' />
		<div class = "mailing">
			<table class="table_email_form">
				<tr>
					<td class="name"><?php _e('От кого', 'usam') ?>:</td>
					<td>
						<select name="from_mailbox_id" id="from_mailbox_id">';				
						<?php
						foreach ( $mailboxes as $mailbox )
						{							
							?><option value='<?php echo $mailbox->id ?>' <?php selected($mailbox->id, $mailbox_id); ?>><?php echo "$mailbox->name ($mailbox->email)"; ?></option><?php
						}		
						?>						
						</select>
					</td>
				</tr>
				<tr>
					<td class="name"><a href="" id="open_select_email" class="button button-secondary"><?php _e('Кому', 'usam') ?></a></td>
					<td><input type="text" name="to" id ="to_email_adress" class ="email_adress" value="<?php echo $this->data['from']; ?>"/></td>
				</tr>
				<tr>
					<td class="name"><a href="" id="open_select_email" class="button button-secondary"><?php _e('Копия', 'usam') ?></a></td>
					<td><input type="text" name="copy_to" id ="copy_to_email_adress" class ="email_adress" value="<?php echo $this->data['copy_email']; ?>"/></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Тема', 'usam') ?>:</td>
					<td><input type="text" name="subject" value="<?php echo $this->data['subject']; ?>"/></td>
				</tr>
				<tr>
					<td colspan='2'>						
						<?php 
						$attachments = array();
						foreach ( $this->attachments as $attachment ) 				 
							$attachments[] = array( 'url' => USAM_UPLOAD_URL."e-mails/$this->id/".$attachment['name'], 'path' => USAM_UPLOAD_DIR."e-mails/$this->id/".$attachment['name'], 'title' => $attachment['name'] );
						
						echo usam_get_form_attachments( $attachments ); 
						?>								
					</td>
				</tr>				
				<tr>
					<td colspan="2">
					<?php 				
					add_editor_style( USAM_URL . '/admin/css/email-editor-style.css' );		
					
					wp_editor( $this->data['body'], 'content', array(
						'textarea_name' => 'message',
						'media_buttons'=>false,
						'textarea_rows' => 50,	
						'wpautop' => 0,							
						'tinymce' => array(
							'theme_advanced_buttons3' => 'invoicefields,checkoutformfields',
							)
						)
					 ); 
					?>
					</td>						
				</tr>
			</table>				
		</div>
		<?php			
	}		
}
?>