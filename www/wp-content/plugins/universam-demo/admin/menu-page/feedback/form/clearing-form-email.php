<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_email extends USAM_Edit_Form
{		
	public function manual_clearing() 
	{		
		$clearing = array( 'day' => 14 );
		?>
		<form method='POST' action=''>			
			<input type='hidden' value='clearing_email' name='action' />			
			<div class = "clearing">
				<table>
					<tr>
						<td><?php _e('Удалять старше', 'usam') ?></td>
						<td><input type='text' name='manual_clearing_day' size='3' maxlength='3' value="<?php echo $clearing['day']; ?>"/> <?php _e('дней', 'usam') ?></td>
					</tr>													
				</table>				
			</div>
			<div class="subtab-detail-toolbar">
				<?php submit_button( __( 'Очистить','usam' ), 'primary','manual_clearing', false, array( 'id' => 'clearing-submit' ) );	?>							
			</div>
			<?php	
			global $current_screen;	
			wp_nonce_field('usam-'.$current_screen->id,'usam_name_nonce_field');
			?>
		</form> 
		<?php	
	
	}

	public function display_form() 
	{						
		?><h2><?php _e( 'Очистка почты','usam' ); ?></h2><?php
		$this->display_toolbar();			
		usam_add_box( 'usam_manual_clearing', __('Ручная очистка почты','usam'), array( $this, 'manual_clearing' ) );			
	}	
}
?>