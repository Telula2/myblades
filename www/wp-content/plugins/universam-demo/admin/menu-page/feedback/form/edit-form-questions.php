<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_questions extends USAM_Edit_Form
{	
	private $topic;	
	
	
	function admin_footer()
	{
		$args = array( 'object_id' => $this->data['id'], 'object_type' => 'questions', 'to_email' => $this->data['mail'], 'subject' => get_bloginfo('name') );
		echo usam_get_modal_window( __('Ответ кленту','usam'), 'send_mail', usam_get_form_send_message( $args ) );	
	}
	
	public function actions_buttom( $links )
	{								
		?>		
		<div id = "actions_buttom">
			<ul>
			<?php 
			do_action( 'usam_this_links_start' ); 
			foreach ( $links as $link )
			{					
				$target = '';
				if ( !empty($link['target']) )
					$target = 'target="'.$link['target'].'"';
				
			
				?>		
				<li>
					<img src='<?php echo $link['icon_url']; ?>' alt='<?php echo $link['title']; ?>' />&ensp;				
					<a id ="<?php echo $link['id']; ?>" href='<?php echo $link['action_url']; ?>' <?php echo $target; ?>><?php echo $link['title']; ?></a>	
				</li>
				<?php				
			}		
			?>	
			</ul>
		</div>		
	<?php		
	}	
	
	public function customer()
	{
		$links = array( 			
			array( 'id' => 'open_send_email', 'action_url' => '', 'title' => esc_html__( 'Отправить письмо', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ),
		);							
		$this->actions_buttom( $links );
	}	
  
	function display_right()
	{	
		usam_add_box( 'usam_customer', __('Клиенту','usam'), array( $this, 'customer' ) );
	}
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __( 'Сообщение от посетителя', 'usam' );
		else
			$title = '';	
		return $title;
	}
	
	public function get_data_tab() 
	{		
		if ( $this->id != null )
		{				
			$this->data = usam_get_feedback( $this->id );	
			add_action( 'admin_footer', array(&$this, 'admin_footer') );			
		}
		else
		{
			$this->data = array();		
		}
		$this->topic = usam_get_feedback_topic();	
	}
	
	public function display_message() 
	{				
		?>
		<div class="usam_document_container-sidebar">
			<div class="usam-document-logo"></div>										
			<div class ="usam_container-action-document-block">
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Отправлено со страницы', 'usam' ); ?></div>
					<?php
					$product = get_post( $this->data['id_product'] );	
					if ( $product->post_type == 'usam-product' )
					{					
						echo usam_get_product_thumbnail( $product->ID, 'product-image' );			
						echo "</br><a href='".$product->guid."' title='".__('Посмотреть товар','usam')."'>".$product->post_title."</a>";
					}
					elseif ( isset($product->post_title) )
					{	
						printf( __( 'Отправлено со странице %s', 'usam' ), '&laquo;'.$product->post_title.'&raquo;' );
					}
					?>
				</div>				
			</div>
		</div>
					
		<div class="usam_document_container-right">
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Посетитель', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
					<tbody>					
						<tr>
							<td class ="usam_table_cell_name"><?php _e('Дата','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo usam_local_date( $this->data['date_insert'] ); ?></span><span class = "usam_edit_data"><?php usam_display_datetime_picker( 'date_insert', $this->data['date_insert'] ); ?></span></td>
						</tr>
						<tr>				
							<td class ="usam_table_cell_name"><?php _e('Пользователь','usam'); ?>:</td>
							<td class ="usam_table_cell_option">
								<?php 
								$user = get_user_by('id', $this->data['user_id'] );
								$display_user = isset($user->display_name)?"$user->display_name ({$user->user_login})":__('Гость','usam');
								?>
								<span class = "usam_display_data"><?php echo $display_user; ?></span>
								<span class = "usam_edit_data">
									<select class="chzn-select" style = "width: 200px;"  name = "message[user_id]">
										<option value="0" <?php echo ($this->data['user_id'] == 0) ?'selected="selected"':''?> ><?php _e('Гость','usam'); ?></option>
										<?php	
										$args = array( 'orderby' => 'nicename', 'fields' => array( 'ID','user_nicename') );
										$users = get_users( $args );
										foreach( $users as $user )
										{						
											?>               
											<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['user_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->user_nicename; ?></option>
											<?php
										}
										?>
									</select>
								</span>								
							</td>	
						</tr>
						<tr>
							<td class ="usam_table_cell_name"><?php _e('Имя','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->data['name']; ?></span><input class="usam_edit_data" type="text" name="message[name]" value="<?php echo $this->data['name']; ?>"/></td>
						</tr>
						<tr>
							<td class ="usam_table_cell_name"><?php _e('Электронная почта','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->data['mail']; ?></span><input class="usam_edit_data" type="text" name="message[mail]" value="<?php echo $this->data['mail']; ?>"/></td>
						</tr>
						<tr>
							<td class ="usam_table_cell_name"><?php _e('Телефон','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->data['phone']; ?></span><input class="usam_edit_data" type="text" name="message[phone]" value="<?php echo $this->data['phone']; ?>"/></td>
						</tr>											
					</tbody>
				</table>
			</div>
		</div>				
		<div class="usam_document_container-right">
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Сообщение', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
					<tbody>							
						<tr>				
							<td class ="usam_table_cell_name"><?php _e('Тема сообщения','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->topic[$this->data['type_message']]; ?></span>
								<select class="usam_select usam_edit_data" name = "message[type_message]">
									<?php							
									foreach( $this->topic as $key => $title )
									{								
										?><option <?php selected($this->data['type_message'], $key ); ?> value='<?php echo $key; ?>'><?php echo $title; ?></option><?php
									}	
									?>
								</select>				
							</td>	
						</tr>	
						<tr>
							<td class ="usam_table_cell_name"><?php _e('Сообщение','usam'); ?>:</td>
							<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->data['message']; ?></span><textarea class ="usam_edit_data" name="message[message]" rows="6"/><?php echo $this->data['message']; ?></textarea></td>
						</tr>								
					</tbody>
				</table>
			</div>
		</div>		
		<?php			
	}
	
	public function display_manager() 
	{		
		$statuses = array( '0' => 'Не обработанные', '1' => 'В обработке', '2' => 'Обработка завершена' );	
		
		$user = get_user_by('id', $this->data['manager_id'] );
		$display_user = isset($user->display_name)?"$user->display_name ({$user->user_login})":'';							
		?>	
		<table class="subtab-detail-content-table usam_edit_table" id="message_table">
			<tbody>						
				<tr>				
					<td class ="usam_table_cell_name"><?php _e('Ответственный менеджер','usam'); ?>:</td>
					<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $display_user; ?></span>
						<span class = "usam_edit_data"><?php usam_select_manager( $this->data['manager_id'], array( 'name' => "message[manager_id]" ) ); ?></span>
					</td>	
				</tr>	
				<tr>				
					<td class ="usam_table_cell_name"><?php _e('Статус','usam'); ?>:</td>
					<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $statuses[$this->data['status']]; ?></span>
						<span class = "usam_edit_data">
						<select class="usam_select" name = "message[status]">
							<?php							
							foreach( $statuses as $key => $title )
							{								
								?><option <?php selected($this->data['status'], $key ); ?> value='$key'><?php echo $title; ?></option><?php
							}	
							?>
						</select>
						</span>								
					</td>	
				</tr>		
				<tr>
					<td class ="usam_table_cell_name"><?php _e('Комментарий','usam'); ?>:</td>
					<td class ="usam_table_cell_option"><span class = "usam_display_data"><?php echo $this->data['description']; ?></span><textarea class = "usam_edit_data" name="message[description]" rows="6"/><?php echo $this->data['description']; ?></textarea></td>
				</tr>
			</tbody>						
		</table>
		<?php		
	}	

	public function display_left() 
	{			
		usam_add_box( 'usam_message', __('Содержание сообщения','usam'), array( $this, 'display_message'), array(), true );	
		usam_add_box( 'usam_manager', __('Менеджер','usam'), array( $this, 'display_manager' ), array(), true );						
	}	
}
?>