<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_email_filters extends USAM_Edit_Form
{		
	public function get_data_tab() 
	{		
		if ( $this->id != null )
		{				
			require_once( USAM_FILE_PATH . '/includes/mailings/email_filter.class.php' );
			$this->data = usam_get_email_filter( $this->id );				
		}
		else
		{
			$this->data = array( 'mailbox_id' => '', 'if' => '', 'condition' => '', 'value' => '', 'action' => '' );		
		}		
	}
		
	function display_filter()
	{			
		global $user_ID;
		$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name','email'), 'user_id' => $user_ID ) );
		?>		
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>				
				<tr>
					<td><?php _e( 'Для ящика','usam' ); ?>:</td>
					<td>
						<select name="mailbox_id" id="mailbox_id">';				
							<option value='0' <?php selected(0, $this->data['mailbox_id'] ); ?>><?php _e('все мои ящики','usam'); ?></option>
							<?php
							foreach ( $mailboxes as $mailbox )
							{							
								?>
								<option value='<?php echo $mailbox->id ?>' <?php selected($mailbox->id, $this->data['mailbox_id'] ); ?>><?php echo "$mailbox->name ($mailbox->email)"; ?></option>
								<?php
							}		
							?>						
						</select>
					</td>	
				</tr>	
				<tr>					
					<td><?php _e( 'Если','usam' ); ?>:</td>
					<td>
						<select name="if">
							<?php							
							$if = array( 'sender' => __('Адрес отправителя','usam'), 'recipient' => __('Адрес получателя','usam'), 'copy' => __('Копия','usam'), 'subject' => __('Тема','usam'), 'size' => __('Размер письма','usam') );
							foreach( $if as $key => $value )
							{								
								echo "<option ".selected( $this->data['if'], $key, false)." value='$key'>$value</option>";
							}
							?>					
						</select>		
					</td>	
					<td>
						<select name="condition">
							<?php							
							$condition = array( 'equal' => __('совпадает','usam'), 'not_equal' => __('не совпадает','usam') );
							foreach( $condition as $key => $value )
							{								
								echo "<option ".selected( $this->data['condition'], $key, false)." value='$key'>$value</option>";
							}
							?>					
						</select>		
					</td>	
					<td><input type="text" name="value" value="<?php echo $this->data['value']; ?>" /></td>
				</tr>				
				<tr>
					<td><?php _e( 'Действие','usam' ); ?>:</td>
					<td>
						<select name="action">
							<?php							
							$actions = array( 'read' => __('Пометить письмо прочитанным','usam'), 'important' => __('Пометить письмо важным','usam'), 'delete' => __('Удалить на всегда','usam'), 'folder' => __('Переместить в папку','usam') );
							foreach( $actions as $key => $value )
							{								
								echo "<option ".selected( $this->data['action'], $key, false)." value='$key'>$value</option>";
							}
							?>					
						</select>		
					</td>	
				</tr>											
			</tbody>
		</table>	
		<?php
	}
	
	function display_left()
	{			
		usam_add_box( 'usam_message', __('Настройка фильтра','usam'), array( $this, 'display_filter'), array(), true );			
    }	
}
?>