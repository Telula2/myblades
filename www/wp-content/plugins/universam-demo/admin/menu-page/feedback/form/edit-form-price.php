<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Price extends USAM_Edit_Form
{		
	public function get_data_tab() 
	{		
		if ( $this->id != null )
		{				
			$this->data = usam_get_price_comparison( $this->id );				
		}
		else
		{
			$this->data = array();		
		}		
	}
	
	function display_message()
	{	
		$post = get_post( $this->data['product_id'] );
		?>
		<h2><?php echo "<a href='".$post->guid."'>".$post->post_title."</a>"; ?></h2>
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>				
				<tr>					
					<td><?php _e( 'Текущая цена','usam' ); ?>:</td>
					<td><?php echo usam_get_product_price_currency($this->data['product_id']); ?></td>
				</tr>		
				<tr>					
					<td><?php _e( 'Найденная цена','usam' ); ?>:</td>
					<td><input type="text" name="price_found" value="<?php echo $this->data['price_found']; ?>" /></td>
				</tr>				
				<tr>
					<td><?php _e( 'Статус','usam' ); ?>:</td>
					<td>
						<select class = "select_status_record" name="status">
							<?php							
							$status = array( '0' => 'Не обработанные', '1' => 'В обработке', '2' => 'Обработка завершена' );
							foreach( $status as $key => $value )
							{
								if ( $this->data['status'] == $key )
									$selected = 'selected="selected"';
								else
									$selected = '';
								$out.="<option $selected value='$key'>$value</option>";
							}
							echo $out;
							?>					
						</select>		
					</td>
				</tr>											
			</tbody>
		</table>	
		<?php
	}
	
	function display_left()
	{			
		usam_add_box( 'usam_message', __('Содержание сообщения','usam'), array( $this, 'display_message'), array(), true );			
    }	
}
?>