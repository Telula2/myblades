<?php
class USAM_Tab_Payment extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Документы оплаты', 'usam'), 'description' => 'Здесь Вы можете просматривать и изменять документы списания.' );
		$this->buttons = array( 'expenditure' => __('Добавить расходный кассовый ордер', 'usam'), 'parish' => __('Добавить приходный кассовый ордер', 'usam') );	
	}
	
	function help_tabs() 
	{	
		$help = array( 'capabilities' => __( 'Возможности', 'usam' ), 'search' => __( 'Поиск', 'usam' ), 'panel' => __( 'Контекстная панель', 'usam' ) );
		return $help;
	}
	
	protected function callback_submit()
	{
		global $wpdb;	
		switch( $this->current_action )
		{
			case 'delete':
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_PAYMENT_HISTORY." WHERE id IN ($in)" );				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'expenditure':			
				if ( !empty($_POST['_payment'][$this->id]) )
				{
					$document = $_POST['_payment'][$this->id];
					$document['payment_type'] = 0;
					$this->id = $this->save_payment_document( $this->id, $document ); 
				}			
			break;		
			case 'save':			
				if ( !empty($_POST['_payment'][$this->id]) )
				{
					$document = $_POST['_payment'][$this->id];
					if ( $document['payment_type'] )
						$payment_type = 'parish';
					else
						$payment_type = 'expenditure';
				
					$this->id = $this->save_payment_document( $this->id, $document );
					$this->sendback = add_query_arg( array( 'action' => $payment_type, 'id' => $this->id ), $this->sendback );				
				}			
			break;				
		}
		if ( is_numeric( $this->current_action ) && $this->current_action < 4 && !empty( $this->records ) ) 
		{			
			foreach ( $this->records as $payment_id )
			{	
				$order_id = (int)$_REQUEST['order_id'][$payment_id];
				$purchase_log = new USAM_Order( $order_id );					
				$payment['status'] = $this->current_action;	
				$payment['id'] = $payment_id;				
				$purchase_log->edit_payment_history( $payment );				
			}			
			$this->sendback = add_query_arg( array( 'updated' => count( $this->records ), ), $this->sendback );
			$this->redirect = true;	
		}			
	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}	
}