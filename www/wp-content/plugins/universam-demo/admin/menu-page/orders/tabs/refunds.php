<?php
class USAM_Tab_refunds extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Возвраты покупателей', 'usam'), 'description' => __('Здесь Вы можете работать с документами возвратов.', 'usam') );	
	//	$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}
	
	protected function callback_submit()
	{		 
		global $wpdb;	
		
		switch( $this->current_action )
		{
			case 'delete':					
				foreach( $this->records as $document_id )
				{
					$shipped = new USAM_Shipped_Document( $document_id );
					$shipped->remove_all_product_from_reserve();
					wp_cache_delete( $document_id, 'usam_document_shipped' );					
				}
				$ids = implode( ',', $this->records );
				
				$wpdb->query( "DELETE FROM `".USAM_TABLE_SHIPPED_PRODUCTS."` WHERE documents_id IN ($ids)" );				
				$wpdb->query( "DELETE FROM " . USAM_TABLE_SHIPPED_DOCUMENTS." WHERE id IN ($ids)" );	
				
				$this->sendback = add_query_arg( array( 'deleted' => count( $ids ), ), $this->sendback );
				$this->redirect = true;	
			break;				
			case 'save':					
				if ( isset($_POST['_refunds']) )
				{
					$data = $_POST['_refunds'];
					if ( $this->id )
						$return = new USAM_Return_Purchases( $this->id );
					else
						$return = new USAM_Return_Purchases(  );					
					$return->set( $data );	
					$return->save( );	
					$this->id = $return->get('id');	
				} 	
				if ( $this->id )
				{
					$return = new USAM_Return_Purchases( $this->id );
					if ( isset($_POST['products']) )
					{
						$products = $_POST['products'];
						$new_products = array();
						foreach($products as $basket_id => $value)
							$new_products[] = array('basket_id' => $basket_id, 'quantity' => $value['quantity']);
							
						$return->set_products( $new_products );				
					}				
					if ( isset($_POST['payment_ids']) )	
					{
						$ids = $_POST['payment_ids'];							
						$payments = usam_get_payments( array('fields' => 'id', 'document_id' => $this->id) );	
						foreach($payments as $id)	
						{
							if ( !in_array( $id, $ids )	)
							{			
								$result_deleted = usam_delete_payment_document( $id );
							}
						}						
						if ( isset($_POST['_payment']) )
						{		
							$payments = $_POST['_payment']; 
							foreach($payments as $document_id => $document)
							{  
								$document = $_POST['_payment'][$document_id];	
								$document['document_type'] = 'refund';
								$document['document_id'] = $this->id;								
								$this->save_payment_document( $document_id, $document );
							}
						}		
					}
					else
					{
						$payments = usam_get_payments( array('fields' => 'id', 'document_type' => 'refund', 'document_id' => $this->id) );	
						foreach($payments as $id)	
						{
							$result_deleted = usam_delete_payment_document( $id );
						}	
					}	
				}			
			break;				
		}		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
}