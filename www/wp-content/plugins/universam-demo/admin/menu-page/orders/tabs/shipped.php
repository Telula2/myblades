<?php
class USAM_Tab_Shipped extends USAM_Tab
{	
	public function __construct()
	{		
		$this->header = array( 'title' => __('Документы отгрузки', 'usam'), 'description' => 'Здесь Вы можете просматривать ваши отгрузки.' );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function callback_submit()
	{		 
		global $wpdb;	
		
		switch( $this->current_action )
		{
			case 'delete':					
				foreach( $this->records as $document_id )
				{
					$shipped = new USAM_Shipped_Document( $document_id );
					$shipped->remove_all_product_from_reserve();
					wp_cache_delete( $document_id, 'usam_document_shipped' );					
				}
				$ids = implode( ',', $this->records );
				
				$wpdb->query( "DELETE FROM `".USAM_TABLE_SHIPPED_PRODUCTS."` WHERE documents_id IN ($ids)" );				
				$wpdb->query( "DELETE FROM " . USAM_TABLE_SHIPPED_DOCUMENTS." WHERE id IN ($ids)" );	
				
				$this->sendback = add_query_arg( array( 'deleted' => count( $ids ), ), $this->sendback );
				$this->redirect = true;	
			break;				
			case 'save':	
				if ( !empty($_POST['_document_shipped'][$this->id]) )
				{ 
					$document = $_POST['_document_shipped'][$this->id];
					$this->id = $this->save_shipped_document( $this->id, $document ); 
				}	
			break;				
		}		
	}
	
	protected function load_tab()
	{		
		$this->list_table();
	}	
}