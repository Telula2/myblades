﻿<?php
class USAM_Tab_route_list extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Маршрутные листы', 'usam'), 'description' => 'Здесь Вы можете работать с маршрутными листами.' );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}	
}