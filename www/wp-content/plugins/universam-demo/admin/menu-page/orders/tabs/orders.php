<?php
class USAM_Tab_orders extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Заказы', 'usam'), 'description' => 'Здесь Вы можете просматривать и работать с Вашими заказами.' );	
		$this->buttons = array( 'new' => __('Добавить', 'usam') );			
	}
	
	function help_tabs() 
	{	
		$help = array( 'capabilities' => __( 'Возможности', 'usam' ), 'search' => __( 'Поиск', 'usam' ), 'panel' => __( 'Контекстная панель', 'usam' ) );
		return $help;
	}
	
	public function notification_change_order_status( $active ) 
	{
		if ( !empty($_POST['prevent_notification']) )
			return false;
		return true;
	}
	
	protected function callback_submit()
	{		 					
		add_filter( 'usam_prevent_notification_change_order_status', array($this, 'notification_change_order_status') );	
		switch( $this->current_action )
		{				
			case 'download_csv':				
				$this->list_table->prepare_items();	
				require_once( USAM_FILE_PATH . '/admin/menu-page/orders/includes/purchase_logs_exported.php' );
				$exported = new USAM_Exported_Purchase_Logs();				
				$exported->purchase_log_csv( $this->list_table->items );
			break;			
			case 'download_dx_csv': //Выгрузка в формате dx.com				
				$this->list_table->prepare_items();	
				require_once( USAM_FILE_PATH . '/admin/menu-page/orders/includes/purchase_logs_exported.php' );
				$exported = new USAM_Exported_Purchase_Logs();					
				$exported->purchase_log_dx_csv( $this->list_table->items );				
			break;
			case 'return_purchases':					
				foreach ( $this->records as $order_id )
				{
					$return_purchases = array( 'order_id' => $order_id );
					
					$_return_purchases = new USAM_Return_Purchases();
					$_return_purchases->set( $return_purchases );
					$_return_purchases->save( );
					$id = $_return_purchases->get( 'id' );
					
					$order = new USAM_Order( $order_id );
					$order_products = $order->get_order_products();
				
					$products_refunds = array();
					foreach( $order_products as $product )
					{
						$products_refunds[] = array('basket_id' => $product->id, 'quantity' => $product->quantity);
					}						
					$_return_purchases->set_products( $products_refunds );
					
					$url = add_query_arg( array('page' => 'orders', 'tab' => 'refunds', 'action' => 'edit', 'id' => $id), admin_url('admin.php') );	
					wp_redirect( $url );
					exit;				
				}	
			break;
			case 'new':	
				global $type_price;
			
				$types_payers = usam_get_group_payers();	
				$type_payer = $types_payers[0];
				$args =  array(
					'totalprice'       => 0,	
					'type_payer'       => $type_payer,				
					'user_ID'          => 0,		
					'coupon_discount'  => 0,	
					'bonus'            => 0,		
					'type_price'       => $type_price,		
					'order_type'       => 'manager'
				);
				$purchase_log = new USAM_Order( $args );	
				$purchase_log->save();			
							
				$id  = $purchase_log->get('id');
		
				$this->sendback = add_query_arg( array( 'action' => 'add', 'id' => $id ), $this->sendback  );
			break;	
			case 'order_copy': 
				foreach ( $this->records as $id )
				{
					$new_order_id = usam_order_copy( $id );			
					$this->sendback = usam_get_url_order( $new_order_id );	
					wp_redirect( $this->sendback );
					exit;				
				}
			break;		
			case 'delete':
				$ids = array_map( 'intval', $this->records );					
				usam_delete_order( $ids );				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ), ), $this->sendback );
				$this->redirect = true;	
			break;	
			case 'save':	
				global  $user_ID;	
		
				$redirect = false;										
				$purchase_log = new USAM_Order( $this->id );	
				
				$order_cart_update = false;
				$order_messages = array();			
				$args = array();			
				if ( isset($_POST['_cart_product']) )
				{
					$products = array();
					$setting_price = usam_get_setting_price_by_code();
					$rounding = $setting_price['rounding'];	
					foreach($_POST['_cart_product'] as $id => $product)		
					{						
						if ( $product['discount'] > 0 )	
						{
							$old_price = $product['price'];						
							if ( $product['type'] == 'p' )						
								$price = $product['price'] - round($product['discount']*$product['price']/100, $rounding);							
							else
								$price = $product['price'] - $product['discount'];					
						}
						else
						{
							$old_price = 0;
							$price = $product['price'];
						}						
						$products[$id] = array( 'price' => $price, 'old_price' => $old_price, 'quantity' => $product['quantity'] );				
					}				
					if ( !empty($products) )
						$purchase_log->edit_order_products( $products );				
					
					$order_cart_update = true;
				}		
				$order_products = $purchase_log->get_order_products();
				if ( isset($_POST['basket_products_ids']) )
				{
					$basket_products_ids = array_map('intval', $_POST['basket_products_ids']);					
					foreach($order_products as $product)	
					{
						if ( !in_array( $product->id, $basket_products_ids )	)
						{					
							$purchase_log->delete_order_product( $product->id );
						}
					}
				}
				else
				{
					foreach($order_products as $product)	
					{									
						$purchase_log->delete_order_product( $product->id );
					}
				}
				if ( isset($_POST['purchlog_notes']) )
					$args['notes'] = sanitize_textarea_field( $_POST['purchlog_notes'] );					
				
				if ( isset($_POST['order_user_id']) && is_numeric($_POST['order_user_id']) )
					$args['user_ID'] = absint($_POST['order_user_id']);	
				if ( isset($_POST['type_price']) )
					$args['type_price'] = sanitize_title($_POST['type_price']);					
				if ( isset($_POST['cancellation_reason']) )
					$args['cancellation_reason'] = sanitize_textarea_field($_POST['cancellation_reason']);			
				if ( isset($_POST['order_user_role']) && is_numeric($_POST['order_user_role']) )
					$args['user_role'] = $_POST['order_user_role'];		
				if ( isset($_POST['order_manager']) && is_numeric($_POST['order_manager']) )
					$args['manager_id'] = absint($_POST['order_manager']);					
				if ( isset($_POST['order_status']) )				
					$args['status'] = $_POST['order_status'];		
				if ( isset($_POST['order_type_payer']) && is_numeric($_POST['order_type_payer']) )				
					$args['type_payer'] = $_POST['order_type_payer'];	
					
				if ( isset($_POST['_coupon_code_order']) )
					$args['coupon_name'] = sanitize_title($_POST['_coupon_code_order']);			
				if ( isset($_POST['_coupon_discount_order']) && is_numeric($_POST['_coupon_discount_order']) )
					$args['coupon_discount'] = (float)$_POST['_coupon_discount_order'];						
				if ( isset($_POST['_bonuses_order']) && is_numeric($_POST['_bonuses_order']) )
					$args['bonus'] = absint($_POST['_bonuses_order']);				
				
				$manager = $purchase_log->get('manager_id');
				if ( empty($manager) && empty($args['manager_id']) )
					$args['manager_id'] = $user_ID;			
				
				if ( !empty($args) )
				{					
					$args['modified_customer'] = 0;				
					$purchase_log->set($args);
					$update = $purchase_log->save();
					if ( $update >= 1 )
						$order_messages['order'] = true;	
				}						
				if ( !empty($_POST['collected_data']) )	
				{
					$data = stripslashes_deep($_POST['collected_data']);	
					if ( !empty($data) )	
					{
						$order_properties = usam_get_order_properties( array( 'active' => 'all', 'fields' => 'id=>unique_name' ) );
						$customer_data = array();
						foreach( $data as $id => $value )	
						{
							$customer_data[$order_properties[$id]] = $value;
						}
						$result = $purchase_log->save_customer_data( $customer_data );
						if ( $result >= 1 )
							$order_messages['users_data'] = $result;	
					}				
				}										
				if ( isset($_POST['payment_ids']) )	
				{
					$ids = array_map('intval', $_POST['payment_ids']);
					$payments = usam_get_payments( array('fields' => 'id', 'document_type' => 'order', 'document_id' => $this->id) );					
					foreach($payments as $id)	
					{
						if ( !in_array( $id, $ids )	)
						{			
							$result_deleted = usam_delete_payment_document( $id );						
						}
					} 					
				}
				else
				{
					$payments = usam_get_payments( array('fields' => 'id', 'document_type' => 'order', 'document_id' => $this->id) );	
					foreach($payments as $id)	
					{
						$result_deleted = usam_delete_payment_document( $id );
					}	
				}	
				if ( isset($_POST['_payment']) )	
				{
					$payments = stripslashes_deep($_POST['_payment']); 
					foreach($payments as $document_id => $document)
					{  
						$document['document_type'] = 'order';
						$document['document_id'] = $this->id;
						$this->save_payment_document( $document_id, $document );	
					}				
				}				
				if ( isset($_POST['shipped_ids']) )	
				{			
					$ids = array_map('intval', $_POST['shipped_ids']);
					$shippeds_document = usam_get_shipping_documents( array('fields' => 'id', 'order_id' => $this->id) );	
					foreach($shippeds_document as $id)						
					{
						if ( !in_array( $id, $ids )	)
						{			
							$result_deleted = usam_delete_shipped_document( $id );						
						}
					}					
				}
				else
				{
					$shippeds_document = usam_get_shipping_documents( array('fields' => 'id', 'order_id' => $this->id) );	
					foreach($shippeds_document as $id)	
					{
						$result_deleted = usam_delete_shipped_document( $id );
					}	
				}	
				if ( isset($_POST['_document_shipped']) )	
				{
					$documents = stripslashes_deep($_POST['_document_shipped']); 
					foreach($documents as $document_id => $document)
					{  								
						$document['order_id'] = $this->id;
						$this->save_shipped_document( $document_id, $document );	
					}					
				}		
				$files = $purchase_log->get_downloadable_files( );
				if ( !empty($_POST['_downloadable_edit']) )
				{
					$files_update = stripslashes_deep($_POST['_downloadable_edit']);	
					$purchase_log->edit_downloadable_files( $files_update );				
				}				
				$redirect = true;
				if ( !empty($order_messages) )
					usam_set_user_screen_message( $order_messages, 'order' );	
			break;	
			default:
				if (strpos($this->current_action, 'cashbox') !== false) 
				{					
					$str = explode("-",$this->current_action);
					$cashbox = usam_get_cashbox( $str[1] );
					if ( !empty($cashbox) ) 
					{
						$class = usam_get_cashbox_class( $cashbox['id'] );
						$class->export( $this->records );
						
						$this->set_user_screen_error( $class->get_error() );		
						
						$this->sendback = add_query_arg( array( 'export' => count( $this->records ), ), $this->sendback );		
						$this->redirect = true;	
					}
				}
				elseif (strpos($this->current_action, 'order_status') !== false) 
				{
					$str = explode("-",$this->current_action);
					$order_status = usam_get_order_status( $str[1] );	
					// изменить статус заказа ( при изменении его на странице Журнала продаж)		
					if ( !empty( $this->records ) && !empty($order_status) ) 
					{			
						foreach ( $this->records as $order_id )
						{
							$result = usam_update_order( $order_id, array('status' => $order_status['internalname']) );
						}	
						$this->sendback = add_query_arg( array( 'updated' => count( $this->records ), ), $this->sendback );		
						$this->redirect = true;	
					}					
				}		
			break;
		}		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	public function get_message()
	{			
		$message = array();
		if ( isset( $_GET['id'] ) )
			$messages = usam_get_user_screen_message( 'order' );		
		else
		{
			$messages = usam_get_user_screen_message( 'purchase-logs' );			
			if ( ! empty( $_REQUEST['unload_receipt'] ) )
				$message[] = sprintf( _n( '%s заказ выгружен для продажи.', '%s заказов выгружены для продажи.', $_REQUEST['unload_receipt'] ), number_format_i18n( $_REQUEST['unload_receipt'] ) );
			if ( ! empty( $_REQUEST['unload_reserve'] ) )
				$message[] = sprintf( _n('%s заказ выгружен для резерва.', '%s заказов выгружены для резерва.', $_REQUEST['unload_reserve']), number_format_i18n( $_REQUEST['unload_reserve'] ) );				
		}
		if ( $messages )
		{
			foreach ( $messages as $key => $value )
			{
				switch( $key )
				{							
					case 'users_data':		
						$message[] = __( 'Данные покупателя сохранены.', 'usam' );			
					break;					
					case 'date':			
						$message[] = __( 'Дата сохранена.', 'usam' );			
					break;
					case 'user':			
						$message[] = __( 'Покупатель изменен.', 'usam' );			
					break;
					case 'shipped_edit':			
						$message[] = __( 'Документы доставки сохранены.', 'usam' );	
					break;	
					case 'shipped_add':			
						$message[] = __( 'Документы доставки создан.', 'usam' );	
					break;					
					case 'payment_add':			
						$message[] = __( 'Документы оплаты создан.', 'usam' );			
					break;
					case 'payment_edit':			
						$message[] = __( 'Документы оплаты сохранены.', 'usam' );			
					break;
					case 'order':			
						$message[] = __( 'Заказ сохранен.', 'usam' );			
					break;
					case 'export_reserve':			
						if ( $value == 1 )
							$message[] = __( 'Резерв выгружен на ftp.', 'usam' );						
					break;
					case 'export_receipt':			
						if ( $value == 1 )
							$message[] = __( 'Чек выгружен на ftp.', 'usam' );						
					break;
				}	
			}				
		}			
		return $message;
	} 
	
	protected function get_message_error()
	{	
		$errors = array();
		if ( isset( $_GET['id'] ) )
			$errors = usam_get_user_screen_error( 'order' );			
		return $errors;
	}
}