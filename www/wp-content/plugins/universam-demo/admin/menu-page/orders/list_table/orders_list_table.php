<?php
class USAM_List_Table_Orders extends USAM_List_Table 
{		
	private $filter = true;		
	private $status = 'all_in_work';
	private $total_amount = 0;	
	private $order_statuses = array();
	protected  $order = 'desc';		

	public function __construct( $args = array() ) 
	{		
		parent::__construct( $args );

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			$_SERVER['REQUEST_URI'] = wp_get_referer();	
		
		$statuses = usam_get_order_statuses( );	
		foreach ( $statuses as $status )					
			$this->order_statuses[$status->internalname] = $status;	

		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;			
	}	
	
	// массовые действия Журнала продаж
	public function get_bulk_actions() 
	{
		if ( ! $this->bulk_actions )
			return array();

		$actions = array(			
			'delete' => _x( 'Удалить', 'bulk action', 'usam' ),	
		);		
		return $actions;
	}
	
	protected function bulk_actions( $which = '' ) 
	{
		if ( is_null( $this->_actions ) ) 
		{
			$no_new_actions = $this->_actions = $this->get_bulk_actions();			
			$this->_actions = apply_filters( "bulk_actions-{$this->screen->id}", $this->_actions );
			$this->_actions = array_intersect_assoc( $this->_actions, $no_new_actions );
			$two = '';
		} 
		else 
		{
			$two = '2';
		}

		if ( empty( $this->_actions ) )
			return;
		
		echo '<label for="bulk-action-selector-' . esc_attr( $which ) . '" class="screen-reader-text">' . __( 'Select bulk action' ) . '</label>';
		echo '<select name="action' . $two . '" id="bulk-action-selector-' . esc_attr( $which ) . "\">\n";
		echo '<option value="-1">' . __( 'Bulk Actions' ) . "</option>\n";		
		foreach ( $this->_actions as $name => $title ) 
		{
			$class = 'edit' === $name ? ' class="hide-if-no-js"' : '';
			echo "\t" . '<option value="' . $name . '"' . $class . '>' . $title . "</option>\n";
		}	
		echo '<optgroup label="' . __( 'Статусы заказа', 'usam' ) . '">';
				
		foreach ( $this->order_statuses as $status )	
		{
			$style = $status->color != ''?'style="color:'.$status->color.'"':'';
			echo "\t" . '<option '.$style.' value="order_status-'.$status->internalname.'">'.$status->name."</option>\n";
		}		
		echo '</optgroup>';		
		echo '<optgroup label="' . __( 'Выгрузить для касс', 'usam' ) . '">';		
		$option = get_option('usam_cashbox');
		$cashbox = maybe_unserialize( $option );
		if ( !empty($cashbox) )
		{
			foreach ( $cashbox as $value )
			{
				echo "\t" . '<option value="cashbox-'.$value['id'].'">' . $value['name'] . "</option>\n";
			}
		}
		echo '</optgroup>';			
		echo "</select>\n";

		submit_button( __( 'Apply' ), 'action', '', false, array( 'id' => "doaction$two" ) );
		echo "\n";
	}
	
	public function prepare_items() 
	{
		global $user_ID;
	
		$args = array( 'cache_results' => true, 'cache_order_documents' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby,
		'fields' => array( 'product_count', 'user_login', 'all_order'),
		);					
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );
			
			$args['date_group'] = isset( $_REQUEST['dgo'] ) ? $_REQUEST['dgo'] : 0;					
			if ( isset( $_REQUEST['manager'] ) && is_numeric($_REQUEST['manager']) )
				$args['manager_id'] = absint($_REQUEST['manager']);
			
			if ( isset($_REQUEST['paid']) && $_REQUEST['paid'] !== '' )
				$args['paid'] = absint($_REQUEST['paid']);
					
			if ( $this->status != 'all' ) 
			{			
				if ( $this->status == 'all_in_work' )
				{
					$args['status__in'] = array();				
					foreach ( $this->order_statuses as $key => $status )		
						if ( !$status->close )
							$args['status__in'][] = $key;
				}
				else
				{				
					$args['status'] = $this->status;
				}
			}
			else
				$args['status__not_in'] = '';
			
			if ( !empty( $_REQUEST['t_price'] ) )
				$args['type_prices'] = sanitize_title($_REQUEST['t_price']);			
			
			if ( !empty( $_REQUEST['payer'] ) )
				$args['type_payer'] = absint($_REQUEST['payer']);
			
			if ( !empty( $_REQUEST['payment'] ) )
				$args['payment'] = absint($_REQUEST['payment']);
			
			if ( !empty( $_REQUEST['shipping'] ) )
				$args['shipping_method'] = absint($_REQUEST['shipping']);
			
			if ( !empty( $_REQUEST['storage_pickup'] ) )
				$args['storage_pickup'] = absint($_REQUEST['storage_pickup']);			
			
			if ( isset( $_REQUEST['user'] ) && $_REQUEST['user'] != '' )
			{
				if ( is_numeric( $_REQUEST['user'] ) )			
					$args['user_ID'] = absint($_REQUEST['user']);
				else								
					$args['user_login'] = sanitize_title($_REQUEST['user']);				
			}				
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => $_REQUEST['selectc'], 'compare' => $_REQUEST['compare'], 'value' => $_REQUEST['condition_v'] ) );				
			}
		}	
//Ограничения просмотра		
		if ( usam_check_current_user_role('shop_crm') )
		{
			$args['manager_id'] = $user_ID;	
		}	
		else
		{
			$view_group = usam_get_user_order_view_group( );
			if ( !empty($view_group) )
			{ 
				if ( !empty($view_group['type_prices']) )
					$args['type_prices'] = $view_group['type_prices'];			
			}
		}
		$query_orders = new USAM_Orders_Query( $args );
		$this->items = $query_orders->get_results();
		$this->total_amount = $query_orders->get_total_amount();		
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}			
	}	
/* 
	Описание: Заголовок таблицы в журнале продаж
*/
	public function get_columns()
	{
		return array(
			'cb'       => '<input type="checkbox" />',
			'id'       => __( 'Номер заказа', 'usam' ),			
			'totalprice' => __( 'Сумма', 'usam' ),			
			'status'   => __( 'Статус', 'usam' ),
			'customer' => __( 'Покупатель', 'usam' ),
			'date'     => __( 'Дата', 'usam' ),		
			'notes'    => __( 'Заметки', 'usam' ),
			'shipping' => __( 'Доставка', 'usam' ),				
			'method'   => __( 'Способ оплаты', 'usam' ),
			'date_paid' => __( 'Дата оплаты', 'usam' ),	
			'address'   => __( 'Адрес доставки', 'usam' ),		
			'manager'   => __( 'Менеджер', 'usam' ),			
		);
	}

	public function get_sortable_columns() 
	{
		if ( ! $this->sortable )
			return array();
		return array(
			'date_paid' => 'date_paid',
			'date'      => 'id',		
			'id'        => 'id',
			'status'    => 'status',
			'totalprice' => 'totalprice',	
			'manager'   => 'manager',						
		);
	}
	
	public function get_views() 
	{
		global $wpdb, $user_ID;
		
		$url = admin_url( 'admin.php?page=orders' );
		$url = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's', 'condition_v', 'payment', 'shipping', 'storage_pickup', 'paid', 'compare', 'numerical' ), $url );
				
		$where = '';
		if ( usam_check_current_user_role('shop_crm') )
		{
			$where = "WHERE manager_id=$user_ID";	
		}	
		else
		{
			$view_group = usam_get_user_order_view_group( );
			if ( !empty($view_group) )
			{ 
				if ( !empty($view_group['type_prices']) )
					$where = "WHERE type_price IN ('".implode("','",$view_group['type_prices'])."')";			
			}
		}
		$sql = "SELECT DISTINCT status, COUNT(*) AS count FROM " . USAM_TABLE_ORDERS . " $where GROUP BY status ORDER BY status";
		$results = $wpdb->get_results( $sql );
		$statuses = array();
		$total_count = 0;
		$all_in_work_total_count = 0;
		if ( ! empty( $results ) )
		{			
			foreach ( $results as $status )
			{
				$statuses[$status->status] = $status->count;	
				if ( isset($this->order_statuses[$status->status]) && !$this->order_statuses[$status->status]->close )
					$all_in_work_total_count += $status->count;					
			}
			$total_count = array_sum( $statuses );
		}
		$all_text = sprintf(_nx( 'Всего <span class="count">(%s)</span>', 'Всего <span class="count">(%s)</span>', $total_count, 'purchase logs', 'usam' ),	number_format_i18n( $total_count ) );
		$all_in_work_text = sprintf(_nx( 'Всего в работе <span class="count">(%s)</span>', 'Всего в работе <span class="count">(%s)</span>', $all_in_work_total_count, 'purchase logs', 'usam' ),	number_format_i18n( $all_in_work_total_count ) );
		$all_class = ( $this->status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$all_in_work = ( $this->status == 'all_in_work' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( add_query_arg( 'status', 'all', $url ) ), $all_class, $all_text ),
						'all_in_work' => sprintf('<a href="%s" %s>%s</a>', esc_url( add_query_arg( 'status', 'all_in_work', $url ) ), $all_in_work, $all_in_work_text ),
		);				
		foreach ( $this->order_statuses as $status )		
		{			
			if ( empty($statuses[$status->internalname]) )
				continue;		
			$text = $text = sprintf( $status->short_name.' <span class="count">(%s)</span>', number_format_i18n( $statuses[$status->internalname] )	);
			$href = add_query_arg( 'status', $status->internalname, $url );			
			$class = ( $this->status == $status->internalname ) ? 'class="current"' : '';
			$views[$status->internalname] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );			
		}
		return $views;
	}
	
	public function payment_dropdown() 
	{			
		$selected = !empty( $_REQUEST['payment'])?absint($_REQUEST['payment']):'';
		usam_get_payment_gateway_dropdown( $selected, array('not_selected_text' => __('Способы оплаты', 'usam')) );	
	}
	
	//Фильтр по доставке
	public function shipping_dropdown() 
	{
		$selected = !empty( $_REQUEST['shipping'])?absint($_REQUEST['shipping']):'';
		usam_get_delivery_service_dropdown( $selected, array('not_selected_text' => __('Способы доставки', 'usam')) );	
	}
		
	//Дата группировки
	public function date_group_order_dropdown() 
	{
		$dgo = isset( $_REQUEST['dgo'] ) ? sanitize_text_field($_REQUEST['dgo']) : 0;			
		?>
		<select name="dgo">
			<option <?php selected( 'insert', $dgo ); ?> value="insert"><?php _e( 'Создание заказа' ); ?></option>
			<option <?php selected( 'update', $dgo ); ?> value="update"><?php _e( 'Изменения заказа' ); ?></option>
			<option <?php selected( 'paid', $dgo ); ?> value="paid"><?php _e( 'Дата оплаты' ); ?></option>					
		</select>		
		<?php
	}
	
	//Дата группировки
	public function paid_dropdown() 
	{
		$paid = isset( $_REQUEST['paid'] ) ? sanitize_text_field($_REQUEST['paid']) : '';			
		?>
		<select name="paid">
			<option <?php selected( '', $paid ); ?> value=""><?php _e( 'По статусу оплату' ); ?></option>
			<option <?php selected( 2, $paid ); ?> value="2"><?php _e( 'Оплаченные' ); ?></option>
			<option <?php selected( 1, $paid ); ?> value="1"><?php _e( 'Частично оплачен' ); ?></option>
			<option <?php selected( 0, $paid ); ?> value="0"><?php _e( 'Не оплаченные' ); ?></option>					
		</select>		
		<?php
	}
		
	//Фильтр количества, сумме
	public function numerical_dropdown() 
	{
		$selectc = isset( $_REQUEST['selectc'] ) ? sanitize_text_field($_REQUEST['selectc']) : 'sum';		
		$compare = isset( $_REQUEST['compare'] ) ? sanitize_text_field($_REQUEST['compare']) : '>';
		$condition_v = isset( $_REQUEST['condition_v'] ) ? sanitize_text_field($_REQUEST['condition_v'] ): '';		
		?>		
		<div class = "numerical">
			<select name="selectc">
				<option <?php selected( 'sum', $selectc ); ?> value="sum"><?php _e('Сумма', 'usam'); ?></option>
				<option <?php selected( 'prod', $selectc ); ?> value="prod"><?php _e('Количество товаров', 'usam'); ?></option>		
				<option <?php selected( 'coupon', $selectc ); ?> value="coupon"><?php _e('Код купона', 'usam'); ?></option>	
				<option <?php selected( 'coupon_v', $selectc ); ?> value="coupon_v"><?php _e('Скидка по купону', 'usam'); ?></option>					
				<option <?php selected( 'bonus', $selectc ); ?> value="bonus"><?php _e('Количество бонусов', 'usam'); ?></option>		
				<option <?php selected( 'taxes', $selectc ); ?> value="taxes"><?php _e('Налог', 'usam'); ?></option>						
				<option <?php selected( 'shipping', $selectc ); ?> value="shipping"><?php _e('Доставка', 'usam'); ?></option>					
			</select>
			<select name="compare">
				<option <?php selected( "<", $compare ); ?> value="<"><?php _e('Меньше', 'usam'); ?></option>				
				<option <?php selected( "=", $compare ); ?> value="="><?php _e('Равно', 'usam'); ?></option>	
				<option <?php selected( ">", $compare ); ?> value=">"><?php _e('Больше', 'usam'); ?></option>			
			</select>
			<input type="text" name="condition_v" value="<?php echo $condition_v; ?>" size = "7" />
		</div>
		<?php
	}	
	
	public function storage_dropdown() 
	{	
		$selected = isset( $_REQUEST['storage_pickup'] ) ? absint($_REQUEST['storage_pickup']) : 0;		
		usam_get_storage_dropdown( $selected, array('name' => 'storage_pickup', 'not_selected_text' => __('Склад для выдачи', 'usam') ) );
	}
	
	public function payer_dropdown() 
	{	
		$payer_selected = isset( $_REQUEST['payer'] ) ? absint($_REQUEST['payer']) : 0;		
		?>	
		<select name="payer">			
			<option <?php selected( 0, $payer_selected ); ?> value="0"><?php _e('Тип плательщика', 'usam'); ?></option>
			<?php	
			$option = get_option('usam_types_payers',array());
			$types_payers = maybe_unserialize($option);	
			foreach ( $types_payers as $payer )
			{
				?><option <?php selected( $payer['id'], $payer_selected ); ?> value="<?php echo $payer['id']; ?>"><?php echo $payer['name']; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	public function type_price_dropdown() 
	{	
		$selected = isset( $_REQUEST['t_price'] ) ? sanitize_title($_REQUEST['t_price']) : 0;
		usam_get_select_prices( $selected,  array( 'name' => 't_price'), true );
	}

	protected function get_filter_tablenav( ) 
	{		
		return array( 'date_group_order' => array(), 'calendar' => array(), 'paid' => array(), 'type_price' => array(), 'payer' => array(),'payment' => array(), 'shipping' => array(), 'storage' => array(), 'numerical' => array() );		
	}	
	
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			$this->excel_button();
			$this->print_button();	
		//	$this->download_csv_button();
			if ( false )
			{
				$url = add_query_arg( array('action' => 'download_dx_csv'), $_SERVER['REQUEST_URI'] );			
				$url = $this->get_nonce_url( $url );
				?>		
				<a target='blank' href="<?php echo $url; ?>" class = "button button-export" title="<?php esc_attr_e( 'Скачать CSV для сайта dx.com', 'usam' ) ?>"><?php _e( 'CSV DX.COM', 'usam' ); ?></a>
				<?php 
			}
		}
		do_action( 'usam_sales_log_extra_tablenav' );			
	}	
	
	public function pagination( $which )
	{
		ob_start();
		parent::pagination( $which );
		$output = ob_get_clean();
		if ( $this->status == 'all' )
			$string = _x( 'Всего (без учета неполных и отклоненных): %s', 'Сумма страницы журнала продаж', 'usam' );
		else
			$string = _x( 'Всего: %s', 'Сумма страницы журнала продаж', 'usam' );
		$total_amount = ' - ' . sprintf( $string, usam_currency_display( $this->total_amount ) );
		$total_amount = str_replace( '$', '\$', $total_amount );
		$output = preg_replace( '/(<span class="displaying-num">)([^<]+)(<\/span>)/', '${1}${2}'.' '.$total_amount.'${3}', $output );
		echo $output;
	}
	
	private function item_link( $id, $text )
	{	
		$result = '<a class="row-title" href="'.esc_url( usam_get_url_order( $id ) ).'" target="_blank" title="'.esc_attr__( 'Посмотреть детали заказа', 'usam' ).'">'.$text.'</a>';
		return $result;
	}
	
	public function column_customer( $item )
	{
		if ( $item->modified_customer == 1 )
		{
			echo "<div class = 'modified_customer'>!";
		}		
		$customer_data = $this->order->get_customer_data();		
		$text = '';
		$email = '';
		if ( usam_is_type_payer_company( $item->type_payer ) )			
		{
			$text = !empty($customer_data['company'])?$customer_data['company']['value'].' ':'';
			$email = !empty($customer_data['company_email'])?$customer_data['company_email']['value']:'';
		}
		else
		{
			$firstname = !empty($customer_data['billingfirstname'])?$customer_data['billingfirstname']['value'].' ':'';
			$lastname = !empty($customer_data['billinglastname'])?$customer_data['billinglastname']['value']:'';
			$text = $firstname.$lastname;
			
			$email = !empty($customer_data['billingemail'])?$customer_data['billingemail']['value']:'';
		}				
		?>
		<strong><?php echo '<a class="row-title" href="'.admin_url( 'admin.php?page=orders&tab=orders&s='.$text ).'" title="'.esc_attr__( 'Найти заказы клиента', 'usam' ).'">'.$text.'</a>'; ?></strong><br />
		<small><?php echo make_clickable( $email ); ?></small><br />
		
		<?php			
		if ( !empty($item->user_login) )
		{
			?><small><?php printf( __('Логин: %s','usam'), $item->user_login ) ; ?></small><?php	
		}
		else
		{
			?><small><?php _e('Гость','usam'); ?></small><?php	
		}
		if ( $item->modified_customer == 1 )
		{
			echo "</div>";
		}
	}
	
	public function column_address( $item )
	{			
		$shippingaddress = '';
		$customer_data = $this->order->get_customer_data();
		if ( isset($customer_data['shippingaddress']) )
			$shippingaddress = !empty($customer_data['shippingaddress'])?$customer_data['shippingaddress']['value'].' ':'';
		elseif ( isset($customer_data['company_shippingaddress']) )
			$shippingaddress = !empty($customer_data['company_shippingaddress'])?$customer_data['company_shippingaddress']['value'].' ':'';
			
		?><a target="_blank" href="https://maps.yandex.ru/?text=<?php echo urlencode($shippingaddress); ?>"><?php echo $shippingaddress; ?></a><?php			
	}

	public function column_id( $item ) 
	{
		$title = $this->item_link( $item->id, $item->id );	
		$actions = $this->standart_row_actions( $item->id );	
		$actions['order_copy'] = $this->add_row_actions( $item->id, 'order_copy', __( 'Копировать', 'usam' ) );		
		if ( $item->status == 'closed' )
		{			
			$actions['return_purchases'] = $this->add_row_actions( $item->id, 'return_purchases', __( 'Возврат товара', 'usam' ) );	 
		}
		$this->row_actions_table( $title, $actions );	
	}
	
	public function column_date_paid( $item ) 
	{			
		if ( $item->date_paid )
			echo usam_local_date( $item->date_paid, get_option( 'date_format', 'Y/m/d' ) );
	}	
	
	function column_manager( $item ) 
	{		
		$user = get_user_by('id', $item->manager_id );			
		if ( !empty($user) )
		{
			$url = add_query_arg( array( 'manager' => $item->manager_id, 'page' => $this->page, 'tab' => $this->tab ), wp_get_referer() );	
			?>
			<a href="<?php echo $url; ?>"><?php echo $user->display_name; ?></a>		
			<?php
		}
		if ( !usam_check_order_is_completed( $item->status ) )
		{ 
			$date = human_time_diff( time(), strtotime($item->date_insert) );	
			echo "<br><strong>".__( 'Обработка', 'usam' ).": $date</strong>";
		}		
	}

	public function column_totalprice( $item ) 
	{
		$text = usam_currency_display( $item->totalprice )."<br /><small>".sprintf( _n( '1 товар', '%s товаров', $item->product_count, 'usam' ), number_format_i18n( $item->product_count ) ).'</small>';
		
		echo $this->item_link( $item->id, $text );
	}
	
	public function column_status( $item ) 
	{				
		if ( usam_check_order_is_completed( $item->status ) )
		{
			$style = !empty($this->order_statuses[$item->status]->color)?'style="background-color:'.$this->order_statuses[$item->status]->color.'; color:#fff;"':'';
			echo '<div class="status_container"><span class="order_closed" '.$style.'>'.$this->order_statuses[$item->status]->name.'</span></div>';		
			if ( $item->status == 'canceled' && !empty($item->cancellation_reason) )
			{
				echo '<div class="cancellation_reason">';
				echo "<strong>".__( 'Причина', 'usam' ).":</strong>";
				echo "<p>$item->cancellation_reason</p>";
				echo '</div>';
			}
		}
		else
		{			
			echo '<select class="usam-order-status" data-log-id="'.$item->id.'">';
			foreach ( $this->order_statuses as $status ) 
			{						
				if ( $status->internalname == 'closed' && $item->paid != 2 )
					continue;
					
				$style = $status->color != ''?'style="color:#fff; background-color:'.$status->color.'"':'';
				
				if ( $status->visibility || $status->internalname == $item->status )
					echo '<option '.$style.' value="'.esc_attr( $status->internalname ).'" '.selected($status->internalname, $item->status, false) . '>'.esc_html( $status->name ). '</option>';
			}
			echo '</select>';
			usam_loader(); 
		}
	}
	
	protected function column_notes( $item ) 
	{		
		echo $this->format_description( $item->notes );
	}
		
	public function column_shipping( $item )
	{			
		$shipped_documents = $this->order->get_shipped_documents();	
		$i = 0;		
		foreach ( $shipped_documents as $document )
		{	
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			if ( !empty($document->storage_pickup) )
			{	
				$storage = usam_get_store_field( $document->storage_pickup, 'title' );
				$storage_pickup = "<br>".$storage;							
			}		
			else
				$storage_pickup = '';
			$date_allow_delivery = !empty($document->date_allow_delivery)?"<br>".usam_local_date( $document->date_allow_delivery ):'';
			
			
			echo "<strong>".$document->name."</strong>$storage_pickup<br>".usam_currency_display($document->price).$date_allow_delivery."<br><strong>".usam_get_shipped_document_status_name( $document->status )."</strong>";			
		}		
	}
	
	public function column_method( $item )
	{	
		$payment_documents = $this->order->get_payment_history();			
		$i = 0;
		foreach ( $payment_documents as $document )
		{
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			$date_payed = empty($document['date_payed'])?'':' - '.usam_local_date( $document['date_payed'], get_option( 'date_format', 'Y/m/d' ) );
			echo "<strong>".$document['name']."</strong><br>".usam_currency_display($document['sum'])."<br>(".usam_get_payment_document_status_name($document['status'])."$date_payed)";			
		}
	}
		
	public function single_row( $item ) 
	{
		$this->order = new USAM_Order( $item->id );
		echo '<tr>';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
}