<?php
class USAM_List_Table_Shipped extends USAM_List_Table 
{		
	private $status = 'all_in_work';
	private $storages = array();
	function __construct( $args = array() )
	{	
		parent::__construct( $args );	
		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;
		$storages = usam_get_stores( array( 'active' => 'all' ) );
		
		foreach ( $storages as $storage )		
			$this->storages[$storage->id] = $storage;
    }
	
	// массовые действия 
	public function get_bulk_actions() 
	{
		if ( ! $this->bulk_actions )
			return array();

		$actions = array(
			'delete' => _x( 'Удалить', 'bulk action', 'usam' ),	
		);	
		return $actions;
	}		
 
	function get_sortable_columns() 
	{
		$sortable = array(
			'id'       => array('id', false),
			'order_id' => array('order_id', false),
			'status'   => array('status', false),
			'track_id' => array('track_id', false),	
			'method'   => array('method', false),	
			'date'     => array('date', false),			
			);
		return $sortable;
	}
	
	function column_id( $item )
	{
		?>
		<a href="<?php echo admin_url('index.php?page=orders&tab=shipped&action=edit&id='.$item->id); ?>" title="<?php _e( 'Открыть отгрузку', 'usam' ); ?>"><?php echo $item->id; ?></a>
		<?php
	}
	
	function column_storage( $item )
	{
		if ( !empty($this->storages[$item->storage]) )
			echo $this->storages[$item->storage]->title;
	}
	
	function column_storage_pickup( $item )
	{
		if ( !empty($this->storages[$item->storage_pickup]) )
			echo $this->storages[$item->storage_pickup]->title;
	}
	
	function column_order_id( $item )
	{		
		echo usam_get_link_order( $item->order_id );	
	}
       
	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',				
			'id'          => __( 'Номер', 'usam' ),
			'name'        => __( 'Метод', 'usam' ),			
			'storage'     => __( 'Склад списания', 'usam' ),		
			'storage_pickup' => __( 'Офис получения', 'usam' ),
			'planned_date'=> __( 'Дата отгрузки', 'usam' ),				
			'order_id'    => __( 'Номер заказа', 'usam' ),
			'status'      => __( 'Статус', 'usam' ),				
			'price'       => __( 'Стоимость', 'usam' ),	
			'date'        => __( 'Дата', 'usam' ),
			'doc_number'  => __( 'Номер документа', 'usam' ),
			'doc_data'    => __( 'Дата документа', 'usam' ),	
			'track_id'    => __( 'Номер отслеживания', 'usam' ),						
        );
        return $columns;
    }
	
	public function get_status_in_work() 
	{
		$statuses = usam_get_shipped_document_status();
		$result = array();
		foreach( $statuses as $key => $status )
		{
			if ( $status['work'] )
				$result[] = $status['code'];
		}
		return $result;
	}
	
	public function prepare_items() 
	{
		global $user_ID;
	
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby );		
		if ( $this->search != ''  )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );
			
			//$args['date_group'] = isset( $_REQUEST['dgo'] ) ? $_REQUEST['dgo'] : 0;		
			if ( $this->status != 'all' ) 
			{		
				if ( $this->status == 'all_in_work' )			
					$args['status__in'] = $this->get_status_in_work();	
				else	
					$args['status'] = $this->status;
			}
			else
				$args['status__not_in'] = '';	
			
			if ( !empty( $_GET['method'] ) && is_numeric($_GET['method']) )
				$args['method'] = $_GET['method'];
			
			if ( !empty( $_GET['storage'] ) && is_numeric($_GET['storage']) )
				$args['storage'] = $_GET['storage'];	
			
			if ( !empty( $_GET['storage_pickup'] ) && is_numeric($_GET['storage_pickup']) )
				$args['storage_pickup'] = $_GET['storage_pickup'];			
			
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => sanitize_title($_REQUEST['selectc']), 'compare' => sanitize_title($_REQUEST['compare']), 'value' => $_REQUEST['condition_v'] ) );				
			}
		}		
		$query_orders = new USAM_Shippeds_Document_Query( $args );
		$this->items = $query_orders->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}		
	}	
	
	public function get_views() 
	{
		global $wpdb;
		
		$sql = "SELECT DISTINCT status, COUNT(*) AS count FROM " . USAM_TABLE_SHIPPED_DOCUMENTS . " GROUP BY status ORDER BY status";
		$results = $wpdb->get_results( $sql );
		$statuses = array();	
		$total_count = 0;
		$all_in_work_total_count = 0;
		if ( ! empty( $results ) )
		{			
			foreach ( $results as $status )
			{
				$statuses[$status->status] = $status->count;
				if ( in_array( $status->status, $this->get_status_in_work() ) )
					$all_in_work_total_count += $status->count;									
			}
			$total_count = array_sum( $statuses );
		}	
		$url = admin_url( 'admin.php?page=orders&tab=shipped' );	
		
		$all_text = sprintf(_nx( 'Всего <span class="count">(%s)</span>', 'Всего <span class="count">(%s)</span>', $total_count, 'purchase logs', 'usam' ),	number_format_i18n( $total_count ) );
		$all_in_work_text = sprintf(_nx( 'Всего в работе <span class="count">(%s)</span>', 'Всего в работе <span class="count">(%s)</span>', $all_in_work_total_count, 'purchase logs', 'usam' ),	number_format_i18n( $all_in_work_total_count ) );
		$all_class = ( $this->status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$all_in_work = ( $this->status == 'all_in_work' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( add_query_arg( 'status', 'all', $url ) ), $all_class, $all_text ),
						'all_in_work' => sprintf('<a href="%s" %s>%s</a>', esc_url( add_query_arg( 'status', 'all_in_work', $url ) ), $all_in_work, $all_in_work_text ),
		);				
		
		$shipped_document_status = usam_get_shipped_document_status();			
		foreach ( $shipped_document_status as $key => $status )		
		{						
			if ( empty($statuses[$status['code']]) )
				continue;
			
			$text = $text = sprintf( $status['name'].' <span class="count">(%s)</span>', number_format_i18n( $statuses[$status['code']] )	);
			$href = add_query_arg( 'status', $status['code'], $url );		
			$class = ( $this->status === (string)$status['code'] ) ? 'class="current"' : '';
			$views[$status['code']] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );			
		}
		return $views;
	}
	
	//Фильтр по доставке
	public function method_dropdown() 
	{
		$selected = !empty( $_REQUEST['method'])?absint($_REQUEST['method']):'';
		usam_get_delivery_service_dropdown( $selected, array('not_selected_text' => __('Способы доставки', 'usam'),'name' => 'method') );	
	}
	
	public function storage_pickup_dropdown() 
	{		
		$selected = isset( $_REQUEST['storage_pickup'] ) ? absint($_REQUEST['storage_pickup']) : 0;		
		usam_get_storage_dropdown( $selected, array('name' => 'storage_pickup', 'not_selected_text' => __('Склад для выдачи', 'usam') ) );
	}
	
	public function storage_dropdown() 
	{		
		$selected = isset( $_REQUEST['storage'] ) ? absint($_REQUEST['storage']) : 0;		
		usam_get_storage_dropdown( $selected, array('name' => 'storage', 'not_selected_text' => __('Склад списания', 'usam') ) );
	}

	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'method' => array(), 'storage' => array(), 'storage_pickup' => array() );		
	}
		
	public function column_planned_date( $item ) 
	{	
		if ( !empty($item->planned_date) )
			echo usam_local_date( $item->planned_date );
	}
		
	public function column_status( $item ) 
	{				
		echo usam_get_shipped_document_status_name( $item->status );
	}
}