<?php
class USAM_List_Table_Payment extends USAM_List_Table
{
	protected $orderby = 'id';
	protected $order   = 'desc'; 
	private $status = 'all';
	protected $prefix = 'ph';		
	private $total_amount;
	
	public function __construct( $args = array() ) 
	{		
		parent::__construct( $args );

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			$_SERVER['REQUEST_URI'] = wp_get_referer();	

		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;					
	}	

	public function prepare_items() 
	{
		$this->get_standart_query_parent();		
	
		$args = array( 'cache_results' => true, 'cache_order_documents' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby );	
			
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );
			
			$args['date_group'] = isset( $_REQUEST['dgo'] ) ? $_REQUEST['dgo'] : 0;			
			if ( $this->status != 'all' ) 					
				$args['status'] = $this->status;							
			
			if ( !empty( $_GET['payment'] ) )
				$args['gateway'] = absint($_GET['payment']);			
			
			if ( isset( $_REQUEST['user'] ) && $_REQUEST['user'] != '' )
			{
				if ( is_numeric( $_REQUEST['user'] ) )			
					$args['user_ID'] = absint($_REQUEST['user']);
				else								
					$args['user_login'] = sanitize_title($_REQUEST['user']);				
			}				
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => sanitize_title($_REQUEST['selectc']), 'compare' => sanitize_title($_REQUEST['compare']), 'value' => $_REQUEST['condition_v'] ) );				
			}
		}		
		$query = new USAM_Payments_Query( $args );
		$this->items = $query->get_results();
		$this->total_amount = $query->get_total_amount();	
		
		if ( $this->per_page )
		{
			$total_items = $query->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}	
	
	public function get_number_columns_sql()
	{		
		return array('document_id', 'status', 'sum' );
	}
	
/* 
	Описание: Заголовок таблицы в журнале продаж
*/
	public function get_columns()
	{
		return array(
			'cb'         => '<input type="checkbox" />',		
			'document'   => __( 'Документ', 'usam' ),			
			'document_number' => __( 'Номер документа', 'usam' ),
			'linked'     => __( 'Связан', 'usam' ),		
			'name'       => __( 'Способ оплаты', 'usam' ),
			'transactid' => __( 'Номер транзакции', 'usam' ),		
			'external_document' => __( 'Учетный документ', 'usam' ),				
			'sum'        => __( 'Сумма', 'usam' ),
			'status'     => __( 'Статус', 'usam' ),			
			'date_payed' => __( 'Дата оплаты', 'usam' ),				
			'date'       => __( 'Дата', 'usam' ),				
		);
	}

	public function get_sortable_columns() 
	{
		if ( ! $this->sortable )
			return array();
		
		return array(
			'date'       => 'id',
			'date_payed' => 'date_payed',
			'linked'     => 'document_id',		
			'document'   => 'payment_type',		
			'document_number'     => 'document_number',				
			'transactid' => 'transactid',
			'external_document' => 'external_document',		
			'status'     => 'status',
			'sum'        => 'sum',
		);
	}

	public function get_views() 
	{
		global $wpdb;

		$status_payment_documents = usam_get_status_payment_documents();
		
		$results = $wpdb->get_results( "SELECT DISTINCT status, COUNT(*) AS count FROM ".USAM_TABLE_PAYMENT_HISTORY." GROUP BY status ORDER BY status" );
		$statuses = array();	
		$total_count = 0;

		if ( ! empty( $results ) )
		{
			foreach ( $results as $status ) 
				$statuses[$status->status] = $status->count;			
			$total_count = array_sum( $statuses );
		}
		$all_text = sprintf(_nx( 'Всего <span class="count">(%s)</span>', 'Всего <span class="count">(%s)</span>', $total_count, 'purchase logs', 'usam' ),	number_format_i18n( $total_count ) );
		$all_href = remove_query_arg( array('status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', ) );
		$all_class = ( $this->status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );

		foreach ( $statuses as $status => $count )
		{
			if ( ! isset( $status_payment_documents[$status] ) )
				continue;
			$text = sprintf( $status_payment_documents[$status].' <span class="count">(%s)</span>', number_format_i18n( $count ) );
			$href = add_query_arg( 'status', $status );
			$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's', ), $href );
			$class = ( $this->status == $status ) ? 'class="current"' : '';
			$views[$status] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}
		return $views;
	}

	//Фильтр количества, сумме
	public function numerical_dropdown() 
	{
		$numerical = isset( $_REQUEST['numerical'] ) ? sanitize_text_field($_REQUEST['numerical']) : 0;		
		$compare = isset( $_REQUEST['compare'] ) ? sanitize_text_field($_REQUEST['compare']) : 0;
		$condition_v = isset( $_REQUEST['condition_v'] ) ? sanitize_text_field($_REQUEST['condition_v']) : '';		
		?>	
		<select name="numerical">
			<option <?php selected( 0, $numerical ); ?> value="0"><?php _e('Сумма', 'usam'); ?></option>				
		</select>
		<select name="compare">
			<option <?php selected( 0, $compare ); ?> value="0"><?php _e('Меньше', 'usam'); ?></option>
			<option <?php selected( 1, $compare ); ?> value="1"><?php _e('Равно', 'usam'); ?></option>	
			<option <?php selected( 2, $compare ); ?> value="2"><?php _e('Больше', 'usam'); ?></option>			
		</select>
		<input type="text" name="condition_v" value="<?php echo $condition_v; ?>" size = "7" />		
		<?php
	}
			
	public function status_dropdown() 
	{	
		$status_selected = isset( $_REQUEST['status'] ) ? sanitize_text_field($_REQUEST['status']) : 'all';	
		?>	
		<select name="status">			
			<option <?php selected( 'all', $status_selected ); ?> value="all"><?php _e('Все', 'usam'); ?></option>
			<option <?php selected( 1, $status_selected ); ?> value="1"><?php _e('Не оплачено', 'usam'); ?></option>
			<option <?php selected( 2, $status_selected ); ?> value="2"><?php _e('Отклонено', 'usam'); ?></option>
			<option <?php selected( 3, $status_selected ); ?> value="3"><?php _e('Оплачено', 'usam'); ?></option>	
			<option <?php selected( 4, $status_selected ); ?> value="4"><?php _e('Возвращен', 'usam'); ?></option>
			<option <?php selected( 5, $status_selected ); ?> value="5"><?php _e('Ошибка оплаты', 'usam'); ?></option>			
		</select>
		<?php
	}	
	
	public function payment_gateway_dropdown() 
	{
		$selected = !empty( $_REQUEST['gateway'])?sanitize_text_field($_REQUEST['gateway']):'';
		usam_get_payment_gateway_dropdown( $selected, array('not_selected_text' => __('Способы оплаты', 'usam')) );	
	}		
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'status' => array('title' => __( 'По статусам', 'usam' )), 'payment_gateway' => array('title' => __( 'По методу оплаты', 'usam' )), 'numerical' => '' );		
	}	
	
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			$this->print_button();
		}
	}	

	public function pagination( $which )
	{
		ob_start();
		parent::pagination( $which );
		$output = ob_get_clean();
		$string = _x( 'Всего: %s', 'Сумма страницы документов оплаты', 'usam' );
		$total_amount = ' - ' . sprintf( $string, usam_currency_display( $this->total_amount ) );
		$total_amount = str_replace( '$', '\$', $total_amount );
		$output = preg_replace( '/(<span class="displaying-num">)([^<]+)(<\/span>)/', '${1}${2}'.' '.$total_amount.'${3}', $output );
		echo $output;
	}	
	
	public function column_document( $item ) 
	{ 	
		if ( $item->payment_type == 1 )
		{			
			$title = __( 'Приходный кассовый ордер', 'usam' );			
		}
		elseif ( $item->payment_type == 0 )
		{
			$title = __( 'Расходный кассовый ордер', 'usam' );	
		}
		$this->row_actions_table( $title, $this->standart_row_actions( $item->id ) );		
	}

	public function column_linked( $item ) 
	{ 
		if ( $item->document_type == 'order' )
		{						
			?>		
			<a href="<?php echo esc_url( usam_get_url_order( $item->document_id ) ); ?>" title="<?php esc_attr_e( 'Посмотреть детали', 'usam' ) ?>"><?php echo __( 'Заказ', 'usam' )." №".$item->document_id ; ?></a>
			<?php			
		}
		elseif ( $item->document_type == 'refund' )
		{
			?>		
			<a href="<?php echo esc_url( usam_get_url_edit_refunds_purchases( $item->document_id ) ); ?>" title="<?php esc_attr_e( 'Посмотреть детали', 'usam' ) ?>"><?php echo __( 'Возврат товаров', 'usam' )." №".$item->document_id ; ?></a>
			<?php			
		}			
	}
	
	public function column_date_payed( $item ) 
	{		
		echo usam_local_date( $item->date_payed, __( get_option( 'date_format', 'Y/m/d' ) )." H:i" );
	}	
	
	public function column_date( $item ) 
	{
		$format = get_option( 'date_format', 'Y/m/d' )." H:i";
		$timestamp = strtotime( $item->date_time );		
		$full_time = date_i18n( $format, $timestamp );
		$time_diff = time() - $timestamp;		
		if ( $time_diff > 0 && $time_diff < 86400 ) // 24 * 60 * 60
			$h_time = sprintf( __( '%s назад' ), human_time_diff( $timestamp, time() ) );
		else
			$h_time = $full_time;

		echo '<abbr title="'.$full_time.'">' . $h_time . '</abbr>';
	}

	public function column_sum( $item ) 
	{
		echo usam_currency_display( $item->sum );
	}
	
	public function column_external_document( $item ) 
	{				
		if ( empty($item->external_document) )
		{
		?>
			<div data-payment-id="<?php echo esc_attr( $item->id ); ?>">				
				<input id = "external_document" type="text" value="<?php echo $item->external_document; ?>" />
				<a class="button save" href="#"><?php _e( 'Добавить', 'usam' ); ?></a>		
				<?php usam_loader(); ?>			
			</div>
		<?php		
		}
		else
			echo $item->external_document;
	}	
	
	public function column_status( $item ) 
	{					
		$statuses = usam_get_status_payment_documents();
		foreach ( $statuses as $key => $status ) 
		{	
			if (  $key == $item->status )
			{				
				echo $status;
				break;
			}			
		}	
	}
	
// массовые действия
	public function get_bulk_actions_display() 
	{		
		$actions = array(
			'delete' => __( 'Удалить', 'usam' ),
			'1'      => __( 'Не оплачено', 'usam' ),
			'2'      => __( 'Отклонено', 'usam' ),
			'3'      => __( 'Оплачено', 'usam' )			
		);		
		return $actions;
	}	
}