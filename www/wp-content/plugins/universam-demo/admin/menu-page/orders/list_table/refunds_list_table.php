<?php
class USAM_List_Table_refunds extends USAM_List_Table
{
	protected $orderby = 'id';
	protected $order   = 'desc'; 
	private $status = 'all';
	protected $prefix = 'ph';		
	private $total_amount;
	
	public function __construct( $args = array() ) 
	{		
		parent::__construct( $args );

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			$_SERVER['REQUEST_URI'] = wp_get_referer();	

		$this->status = isset($_REQUEST['status'])?$_REQUEST['status']:$this->status;			
	}	

	public function prepare_items() 
	{
		$args = array( 'orderby' => 'date_insert', 'status' => $this->status );
		
		$args = array_merge ($args, $this->get_date_for_query() );
		
		$query = new USAM_Return_Purchases_Query( $args );
		$this->items = $query->get_results();		
		if ( $this->per_page )
		{
			$total_items = $query->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}					
	}	
	
	public function get_number_columns_sql()
	{		
		return array('order_id', 'status', 'sum' );
	}
	
/* 
	Описание: Заголовок таблицы в журнале продаж
*/
	public function get_columns()
	{
		return array(
			'cb'         => '<input type="checkbox" />',			
			'id'         => __( 'Номер документа', 'usam' ),
			'order_id'   => __( 'Заказ', 'usam' ),		
			'customer'   => __( 'Покупатель', 'usam' ),
			'sum'        => __( 'Сумма', 'usam' ),
			'status'     => __( 'Статус', 'usam' ),					
			'date'       => __( 'Дата', 'usam' ),				
		);
	}

	public function get_sortable_columns() 
	{
		if ( ! $this->sortable )
			return array();
		
		return array(
			'date'       => 'id',	
			'order_id'   => 'order_id',					
			'status'     => 'status',
			'sum'        => 'sum',
		);
	}

	public function get_views() 
	{
		global $wpdb;

		$status_payment_documents = usam_get_status_return_purchases_document();
		
		$results = $wpdb->get_results( "SELECT DISTINCT status, COUNT(*) AS count FROM ".USAM_TABLE_RETURN_PURCHASES." GROUP BY status ORDER BY status" );
		$statuses = array();	
		$total_count = 0;

		if ( ! empty( $results ) )
		{
			foreach ( $results as $status ) 
				$statuses[$status->status] = $status->count;			
			$total_count = array_sum( $statuses );
		}
		$all_text = sprintf(_nx( 'Всего <span class="count">(%s)</span>', 'Всего <span class="count">(%s)</span>', $total_count, 'purchase logs', 'usam' ),	number_format_i18n( $total_count ) );
		$all_href = remove_query_arg( array('status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', ) );
		$all_class = ( $this->status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );

		foreach ( $statuses as $status => $count )
		{
			if ( ! isset( $status_payment_documents[$status] ) )
				continue;
			$text = sprintf( $status_payment_documents[$status].' <span class="count">(%s)</span>', number_format_i18n( $count ) );
			$href = add_query_arg( 'status', $status );
			$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's', ), $href );
			$class = ( $this->status == $status ) ? 'class="current"' : '';
			$views[$status] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}
		return $views;
	}	
	
	//Фильтр количества, сумме
	public function numerical_dropdown() 
	{
		$numerical = isset( $_REQUEST['numerical'] ) ? sanitize_text_field($_REQUEST['numerical']) : 0;		
		$compare = isset( $_REQUEST['compare'] ) ? sanitize_text_field($_REQUEST['compare']) : 0;
		$condition_v = isset( $_REQUEST['condition_v'] ) ? sanitize_text_field($_REQUEST['condition_v']) : '';		
		?>	
		<select name="numerical">
			<option <?php selected( 0, $numerical ); ?> value="0"><?php _e('Сумма', 'usam'); ?></option>				
		</select>
		<select name="compare">
			<option <?php selected( 0, $compare ); ?> value="0"><?php _e('Меньше', 'usam'); ?></option>
			<option <?php selected( 1, $compare ); ?> value="1"><?php _e('Равно', 'usam'); ?></option>	
			<option <?php selected( 2, $compare ); ?> value="2"><?php _e('Больше', 'usam'); ?></option>			
		</select>
		<input type="text" name="condition_v" value="<?php echo $condition_v; ?>" size = "7" />		
		<?php
	}
			
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array(), 'numerical' => array() );		
	}	
	
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			$this->print_button();
		}
	}	

	public function pagination( $which )
	{
		ob_start();
		parent::pagination( $which );
		$output = ob_get_clean();
		$string = _x( 'Всего: %s', 'Сумма страницы документов оплаты', 'usam' );
		$total_amount = ' - ' . sprintf( $string, usam_currency_display( $this->total_amount ) );
		$total_amount = str_replace( '$', '\$', $total_amount );
		$output = preg_replace( '/(<span class="displaying-num">)([^<]+)(<\/span>)/', '${1}${2}'.' '.$total_amount.'${3}', $output );
		echo $output;
	}

	public function column_customer( $item )
	{
		$user = get_user_by('id', $item->user_id);			
		if ( isset($user->user_login) )
		{
			if ( !empty($user->first_name) || !empty($user->first_name) )
				echo " $user->first_name $user->last_name";	
			else
				echo $user->user_login;
		}
	}	
	
	public function column_id( $item ) 
	{	
		$this->row_actions_table( $item->id, $this->standart_row_actions( $item->id ) );		
	}

	public function column_order_id( $item ) 
	{
		?><input type="hidden" name="order_id[<?php echo $item->id; ?>]" value="<?php echo $item->order_id; ?>" />
		<a href="<?php echo esc_url( usam_get_url_order( $item->order_id ) ); ?>" title="<?php esc_attr_e( 'Посмотреть детали заказа', 'usam' ) ?>"><?php echo esc_html( $item->order_id ); ?></a> <?php echo sprintf( __( 'от %s' ), date_i18n( __( get_option( 'date_format', 'Y/m/d' ) ),strtotime($item->date_insert) ) ); ?>
		<br/>
		<?php
		echo '<span class="order_status">'.usam_get_order_status_name( $item->status ).'</span>';	
	}	

	public function column_date( $item ) 
	{
		$format = __( 'Y/m/d H:i:s A' );
		$timestamp = strtotime($item->date_insert);
		$full_time = date_i18n( $format, $timestamp );
		$time_diff = current_time('timestamp') - $timestamp;
		if ( $time_diff > 0 && $time_diff < 24 * 60 * 60 )
			$h_time = $h_time = sprintf( __( '%s назад' ), human_time_diff( $timestamp, current_time('timestamp') ) );
		else
			$h_time = date_i18n( __( get_option( 'date_format', 'Y/m/d' ) ), $timestamp );

		echo '<abbr title="' . $full_time . '">' . $h_time . '</abbr>';
	}

	public function column_sum( $item ) 
	{
		echo usam_currency_display( $item->sum );
	}
		
	public function column_status( $item ) 
	{				
		echo usam_get_title_status_return_purchases_document( $item->status );
	}
	
// массовые действия
	public function get_bulk_actions_display() 
	{		
		$actions = array(
			'delete' => __( 'Удалить', 'usam' ),
			'1'      => __( 'Не оплачено', 'usam' ),
			'2'      => __( 'Отклонено', 'usam' ),
			'3'      => __( 'Оплачено', 'usam' )			
		);		
		return $actions;
	}	
}