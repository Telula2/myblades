<?php
class USAM_List_Table_route_list extends USAM_List_Table 
{		
   	function get_sortable_columns() 
	{
		$sortable = array(
			'id'       => array('id', false),
			'order_id' => array('order_id', false),
			'status'   => array('status', false),
			'track_id' => array('track_id', false),	
			'method'   => array('method', false),	
			'date'     => array('date', false),			
			);
		return $sortable;
	}

	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',			
			'id'          => __( 'Номер документа', 'usam' ),			
			'order_id'    => __( 'Номер заказа', 'usam' ),
			'status'      => __( 'Статус', 'usam' ),			
			'date'        => __( 'Дата', 'usam' ),
        );
        return $columns;
    }
	
	public function prepare_items() 
	{
		
	}
}