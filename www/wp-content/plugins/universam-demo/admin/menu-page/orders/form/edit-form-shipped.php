<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_shipped extends USAM_Edit_Form
{	
	private $purchase_log;	
	private $cartcontent = array();
	private $form_key;	
		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить документ отгрузки №%s','usam'), $this->id );
		else
			$title = __('Добавить документ отгрузки', 'usam');	
		return $title;
	}
	
	public function include_css_and_js(  )
	{
		wp_enqueue_style('usam-order-admin');			
	}

	protected function get_data_tab(  )
	{			
		add_action( 'admin_enqueue_scripts', array($this, 'include_css_and_js') );		
		
		$default = array( 'id' => 0, 'order_id' => '', 'date_insert' => '', 'date_update' => '', 'courier' => '', 'name' => '', 'method' => '', 'planned_date' => '', 'storage_pickup' => '', 'storage' => '', 'notes' => '', 'price' => 0, 'date_allow_delivery' => '', 'readiness_date' => '', 'readiness' => '', 'status' => '', 'track_id' => '', 'doc_number' => '', 'doc_data' => '', 'export' => '' );
		if ( $this->id != null )
		{				
			$this->title_action = sprintf( __( 'Отгрузка №%s', 'usam' ), $this->id );	
			$this->data = usam_get_shipped_document( $this->id );				
		}
		else
		{
			$this->id = 0;
			$this->title_action = sprintf( __( 'Добавить новую отгрузку', 'usam' ) );		
			$this->data = $default;	
		}	
//$this->data = array_merge( $default, $data );

		$this->purchase_log = new USAM_Order( $this->data['order_id'] );		
		$this->cartcontent = $this->purchase_log->get_order_products();	
		
		$this->form_key = '_document_shipped['.$this->id.']';
	}	
	
	public function url_action( $action, $args )
	{
		return usam_url_admin_action( $action, '', $args );
	}	
	
	private function form_readiness( $number )
    {		
		if ( $number < 100 && $number >= 50 )
			$class = 'readiness_yellow';
		elseif ( $number >= 100 )
			$class = 'readiness_green';
		else
			$class = 'readiness_read';		
		?>
		<div class="form_readiness">		
			<div class="readiness <?php echo $class; ?>" style ="width:<?php echo $number; ?>px;">			
				<?php echo $number; ?>%
			</div>					
		</div>			
		<?php		
	}
	
	private function disabled() 
	{ 
		echo $this->id?'disabled = "disabled"':'';
	}
	
	public function display_document()
    {				
		$this->data['products'] = usam_get_products_shipped_documents( $this->id, 'ARRAY_A' );			

		$post_ids = array();
		foreach( $this->cartcontent as $item )
			$post_ids[] = $item->product_id;
		
		update_meta_cache('post', $post_ids);			
		?>
		<div class="usam_document_container-content">
			<div class="usam_document_container-sidebar">
				<div class="usam-document-logo"></div>				
				<?php
		//		if ( !$this->is_closed )
		//		{						
				$printed_form = array( 
					array( 'form' => 'packing_slip', 'title' => esc_html__( 'Упаковочный лист', 'usam' ) ), 
					array( 'form' => 'act_acceptance', 'title' => esc_html__( 'Акт сдачи-приемки', 'usam' ) ), 
				);					
				$printed_form = apply_filters( 'usam_printed_form_shipped_document', $printed_form );	
				
				?>						
				<div class ="usam_container-action-document-block">
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Комплект документов', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
							<tbody>
								<?php															
								foreach ( $printed_form as $link )
								{					
									$url = usam_url_admin_action( 'printed_form', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) );
									?>		
									<tr>										
										<td class="usam_table_cell_option">
											<img src='<?php echo USAM_CORE_IMAGES_URL; ?>/printer.png' alt='<?php echo $link['title']; ?>' />&ensp;				
											<a href='<?php echo $url; ?>' target="_blank"><?php echo $link['title']; ?></a>	
										</td>
									</tr>	
									<?php				
								}		
								?>	
							</tbody>								
						</table>
					</div>
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Комплект документов в pdf', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
							<tbody>
								<?php															
								foreach ( $printed_form as $link )
								{					
									$url = usam_url_admin_action( 'printed_form_to_pdf', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) );
									?>		
									<tr>										
										<td class="usam_table_cell_option">
											<img src='<?php echo USAM_CORE_IMAGES_URL; ?>/printer.png' alt='<?php echo $link['title']; ?>' />&ensp;				
											<a href='<?php echo $url; ?>' target="_blank"><?php echo $link['title']; ?></a>	
										</td>
									</tr>	
									<?php				
								}		
								?>	
							</tbody>								
						</table>
					</div>
					
					<?php if ( $this->data['track_id'] != '' ) { ?>		
					
					<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Cообщения клиенту', 'usam' ); ?></div>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
							<tbody>
								<tr>										
									<td class="usam_table_cell_option">
										<?php $action_url = $this->url_action( 'send_tracking_email', array( 'item_id' => $this->id ) ); ?>	
										<a id = "send_tracking_email" data-order_id="<?php echo $this->data['order_id']; ?>" data-item-id="<?php echo $this->id; ?>" href='<?php echo $action_url; ?>'>						
											<img src='<?php echo USAM_CORE_IMAGES_URL . "/email_go.png"; ?>'/>	
											<?php _e( 'Отправить трек-номер', 'usam' ); ?>							
										</a>	
									</td>
								</tr>	
								
							</tbody>								
						</table>
					</div>	
					
					<?php } ?>		
																					
					<?php
					if ( get_option('usam_ftp_export_order') )
					{		
						$action_url = $this->url_action( 'export_reserve_export', array( 'item_id' => $this->id ) ); 								
						?>	
						<div class="usam_container-table-container caption border">
							<div class="usam_container-table-caption-title"><?php _e( 'Выгрузка на FTP', 'usam' ); ?></div>
							<?php								
							if ( $this->data['storage'] != 0 )
							{							
								if ( !empty($this->data['products']) ) 
								{
								?>	
							<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
								<tbody>
									<tr>										
										<td class="usam_table_cell_option">
											<a id='export_reserve_export' data-documents_id = "<?php echo $this->id; ?>" title='<?php echo esc_attr(__( 'Выгрузить на FTP резерв', 'usam' )); ?>' href='<?php echo $action_url; ?>'>
												<img src='<?php echo USAM_CORE_IMAGES_URL . "/plus_icon.jpg"; ?>' />
												<?php echo esc_attr(__( 'Резерв', 'usam' )); ?>
											</a>
										</td>												
									</tr>								
								</tbody>								
							</table>
							<?php	
								}	
								else
								{
									echo "<p>".__( 'Выберете товары', 'usam' )."</p>";
								}								
							}	
							else
							{
								echo "<p>".__( 'Выберете склад списания', 'usam' )."</p>";
							}
							?>
						</div>														
						<?php									
					}	
					usam_loader();
					?>					
				</div>
				<?php
			//	}						
				?>	
			</div>
			<div class="usam_document_container-right">
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Служба', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form ">
						<tbody>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Служба доставки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data"><?php echo $this->data['name']; ?></span>									
									<select class="usam_edit_data chzn-select-nosearch-load" <?php $this->disabled(); ?> name = "<?php echo $this->form_key; ?>[method]">
										<?php
										$calculated_data = $this->purchase_log->get_calculated_data();
										$order = $this->purchase_log->get_data();
										$location = usam_get_buyers_location( $order['id'] );
										$restrictions = array( 'price' => $calculated_data['order_final_basket'] - ($order['coupon_discount'] + $order['bonus']), 'weight' => $calculated_data['weight'] );
										if ( !empty($location) )
										{
											$restrictions['locations'] = usam_get_array_locations_up( $location, 'id' ); 
										}
										$ds = new USAM_Delivery_Services( $restrictions );										
										$delivery_service = $ds->get_delivery_services();
										$delivery_services_disabled = $ds->get_delivery_services_disabled();	
										
										foreach ($delivery_service as $value)
										{
										?>
											<option value="<?php echo $value->id; ?>" <?php echo ( $value->id == $this->data['method']?'selected="selected"':'') ?>><?php echo $value->name; ?></option>
										<?php
										}
										foreach ($delivery_services_disabled as $value)
										{
										?>
											<option class ="delivery_services_disabled" value="<?php echo $value->id; ?>" <?php echo ( $value->id == $this->data['method']?'selected="selected"':'') ?>><?php echo $value->name; ?></option>
										<?php
										}
										?>										
									</select>										
								</td>
							</tr>								
						</tbody>
					</table>
				</div>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Информация о процессе', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form ">
						<tbody>								
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Готовность', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php $this->form_readiness( $this->data['readiness'] ); ?></span>
									<select <?php $this->disabled(); ?> class="usam_edit_data chzn-select-nosearch-load" name="<?php echo $this->form_key; ?>[readiness]">
										<option value="">---</option>
										<?php											
										for ($j = 0; $j <= 100; $j+10)											
										{					
											?>
												<option value="<?php echo $j; ?>"  <?php echo ( ($j <= $this->data['readiness'] && $j+10 > $this->data['readiness'])?'selected="selected"':'') ?>><?php echo $j; ?></option>
											<?php
											$j = $j + 10;										
										}
										?>										
									</select>
								</td>
							</tr>
							<tr id ="readiness_date">
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Предполагаемая дата готовности', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<?php 												
									$display_readiness_date = '';
									if ( !empty($this->data['readiness_date']) )
									{											
										$readiness_date = get_date_from_gmt($this->data['readiness_date'], "d.m.Y H:i");
										$time = strtotime($readiness_date);										
										$num_days = date('d', $time) - date('d');								
										$message = '';
										if ( $num_days > 1 ) 
											$message = ' ( '.sprintf( _n( 'Остался %s день', 'Осталось %s дней', $num_days, 'usam' ), $num_days ).' )';
										elseif ( $num_days == 1 ) 
											$message = ' ( '. __('Заканчивается завтра','usam').' )';												
										elseif ( $num_days == 0 ) 
											$message = ' ( '. __('Заканчивается сегодня','usam').' )';											
										
										$display_readiness_date = $readiness_date.$message; 
										
										$hur = date('H') - date('H', $time);	
									}										
									?>
									<span class = "usam_display_data"><?php echo $display_readiness_date; ?></span>								
									<div class = "usam_edit_data"><?php usam_display_datetime_picker( 'readiness_date_'.$this->id, $this->data['readiness_date'] ); ?></div>
								</td>
							</tr>	
							<tr>
								<td class="usam_table_cell_name vat" class = "name"><?php _e( 'Статус', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data">
									<?php 
										echo usam_get_shipped_document_status_name( $this->data['status'] );										
										$shipped_document_status = usam_get_shipped_document_status();
									?>
									</span>
									<div class = "usam_edit_data">
										<select <?php $this->disabled(); ?> id = "document_status-<?php echo $this->id; ?>" class="chzn-select-nosearch-load" name = "<?php echo $this->form_key; ?>[status]">
											<?php												
											foreach ($shipped_document_status as $status)
											{					
											?>
												<option value="<?php echo $status['code']; ?>"  <?php echo ( $status['code'] == $this->data['status']?'selected="selected"':'') ?>><?php echo $status['name']; ?></option>
											<?php
											}
											?>										
										</select>
									</div>										
								</td>
							</tr>										
						</tbody>							
					</table>
				</div>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Стоимость', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
						<tbody>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Стоимость доставки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['price']; ?></span>
									<input size = "10" maxlength = "10" <?php $this->disabled(); ?> type="text" name="<?php echo $this->form_key; ?>[price]" class = "usam_edit_data" value="<?php echo $this->data['price']; ?>">
									<?php $action_url = $this->url_action( 'recalculate_document_shipped', array( 'item_id' => $this->id ) ); ?>
									<a href='<?php echo $action_url; ?>'><?php esc_html_e( 'Пересчитать стоимость', 'usam' ); ?></a>
								</td>
							</tr>		
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Включить в стоимость заказа', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['include_in_cost']?_e('Да', 'usam'):_e('Нет', 'usam'); ?></span>									
									<span class = "usam_edit_data">
										<input type="radio" name="<?php echo $this->form_key; ?>[include_in_cost]" <?php $this->disabled(); ?> value="0" <?php checked($this->data['include_in_cost'], 0) ?> /><?php _e('Нет', 'usam'); ?>&nbsp;
										<input type="radio" name="<?php echo $this->form_key; ?>[include_in_cost]" <?php $this->disabled(); ?> value="1" <?php checked($this->data['include_in_cost'], 1) ?> /><?php _e('Да', 'usam'); ?>
									</span>
								</td>
							</tr>								
						</tbody>
					</table>
				</div>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Отгрузка', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form ">
						<tbody>							
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Склад списания', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data">
										<?php 
										if ( empty($this->data['storage']) )
											_e( 'Не выбрано', 'usam' );
										else
										{
											$storage = usam_get_storage( $this->data['storage'] ); 
											echo isset($storage['title'])?$storage['title']:''; 
										}
										?>
									</span>
									<div class = "usam_edit_data"><?php usam_get_storage_dropdown( $this->data['storage'], array( 'name' => $this->form_key."[storage]", 'class' => "chzn-select-nosearch-load" ) ); ?></div>
								</td>
							</tr>
							<tr id = "storage_pickup">
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Офис получения', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<?php 
									if ( empty($this->data['storage_pickup']) )
									{
										$storage_pickup_name = __( 'Не выбрано', 'usam' );
										$storage_pickup_address = ''; 
										$storage_pickup_phone = ''; 
									}
									else
									{
										$storage = usam_get_storage( $this->data['storage_pickup'] ); 
										$storage_pickup_name = isset($storage['title'])?$storage['title']:''; 
										$storage_pickup_address = isset($storage['address'])?$storage['address']:''; 
										$storage_pickup_phone = isset($storage['phone'])?$storage['phone']:''; 
									}
									?>
									<span class = "usam_display_data"><span class = "name"><?php echo $storage_pickup_name; ?></span> (<span class = "address"><?php echo $storage_pickup_address; ?></span> - <span class = "phone"><?php echo $storage_pickup_phone; ?></span>)</span>
									<div class = "usam_edit_data"><?php usam_get_storage_dropdown( $this->data['storage_pickup'], array( 'name' => $this->form_key."[storage_pickup]", 'class' => "chzn-select-nosearch-load" ) ); ?></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>				
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Информация об отгрузке', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
						<tbody>					
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер документа отгрузки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['doc_number']; ?></span>
									<input <?php $this->disabled(); ?> type="text" name="<?php echo $this->form_key; ?>[doc_number]" class = "usam_edit_data" value="<?php echo $this->data['doc_number']; ?>">
								</td>
							</tr>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата документа отгрузки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">							
									<span class = "usam_display_data"><?php echo empty($this->data['doc_data'])?'':get_date_from_gmt($this->data['doc_data'], "d.m.Y H:i"); ?></span>
									<div class = "usam_edit_data"><?php usam_display_datetime_picker( 'doc_data_'.$this->id, $this->data['doc_data'] ); ?></div>
								</td>
							</tr>						
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер отслеживания', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data">										
										<?php 
										$delivery = usam_get_delivery( $this->data['method'] );									
										if ( $this->data['method'] != '' && !empty($delivery['handler']) ) { ?>	
											<a title='<?php _e( 'Посмотреть историю почтового отправления', 'usam' ); ?>' href='<?php echo add_query_arg( array( 'action' => 'view', 'id' => $this->id, 'page' => 'orders', 'screen' => 'tracking' ), admin_url('admin.php') ); ?>'><?php echo $this->data['track_id']; ?></a>
										<?php } else { ?>
											<?php echo $this->data['track_id']; ?>
										<?php } ?>
									</span>		
									<span class = "usam_edit_data">
										<input <?php $this->disabled(); ?> type="text" name="<?php echo $this->form_key; ?>[track_id]" value="<?php echo $this->data['track_id']; ?>">
									</span>		
								</td>
							</tr>
						</tbody>							
					</table>
				</div>					
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Информация для курьера', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
						<tbody>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Курьер', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<?php
									$user = get_user_by('id', $this->data['courier']);
									$courier = isset($user->user_login)?$user->user_login:'';
									$args = array( 'orderby' => 'nicename', 'role__in' => array('courier'), 'fields' => array( 'ID','user_nicename','display_name') );
									$users = get_users( $args );			
									?>
									<span class = "usam_display_data"><?php echo $courier; ?></span>										
									<select class="chzn-select-nosearch-load usam_edit_data" name = "<?php echo $this->form_key; ?>[courier]" <?php $this->disabled(); ?>>
										<option value="0" <?php echo ($this->data['courier'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
										<?php	
										foreach( $users as $user )
										{						
											?>               
											<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['courier'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->display_name ." ($user->user_nicename)"; ?></option>
											<?php
										}
										?>
									</select>				
								</td>
							</tr>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Заметки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['notes']; ?></span>
									<textarea rows="5" <?php $this->disabled(); ?> type="text" name="<?php echo $this->form_key; ?>[notes]" class = "usam_edit_data"><?php echo $this->data['notes']; ?></textarea>
								</td>
							</tr>
							<tr id ="date_allow_delivery">
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата и время доставки', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">										
									<span class = "usam_display_data"><?php echo empty($this->data['date_allow_delivery'])?'':get_date_from_gmt($this->data['date_allow_delivery'], "d.m.Y H:i"); ?></span>
									<div class = "usam_edit_data"><?php usam_display_datetime_picker( 'date_allow_delivery_'.$this->id, $this->data['date_allow_delivery'] ); ?></div>
								</td>
							</tr>								
						</tbody>							
					</table>
				</div>					
				<?php 										
				if ( get_option('usam_ftp_export_order', 0) )
				{
				?>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Информация о выгрузке', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
						<tbody>								
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Резерв', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data">
										<?php 													
										if ( $this->data['export'] == 1 )
										{
											_e( 'выгружен на FTP', 'usam' );
											$checked = "checked='checked'";
										}
										else
										{
											_e( 'не выгружен на FTP', 'usam' ); 
											$checked = "";
										}
										?>
									</span>										
									<div class = "usam_edit_data">
										<input <?php $this->disabled(); ?> type="checkbox" <?php echo $checked; ?> name="<?php echo $this->form_key; ?>[export]" value="1">
									</div>										
								</td>
							</tr>								
						</tbody>							
					</table>
				</div>
				<?php 
				}
				?>
			</div>				
			<div class="adm-s-order-shipment-basket-structure">
				<?php
				if ( !empty($this->cartcontent) ) 
				{
				?>				
				<h4><?php _e( 'Состав отгрузки', 'usam' ); ?></h4>
				<div class="usam_table_document_container">
					<table class="usam_table_document" style="width: 100%;">
						<thead>
							<tr>
								<td class="number">№</td>									
								<td class="name"><?php _e( 'Название', 'usam' ); ?></td>											
								<td class="dimensions"><?php _e( 'Размеры', 'usam' ); ?></td>										
								<td class="weight"><?php _e( 'Вес', 'usam' ); ?></td>			
								<td class="order_quantity"><?php _e( 'В заказе', 'usam' ); ?></td>								
								<td class="quantity"><?php _e( 'К отгрузке', 'usam' ); ?></td>								
								<td class="reserve"><?php _e( 'Резерв', 'usam' ); ?></td>
								<td class="stock"><?php _e( 'На складе', 'usam' ); ?></td>
								<td class="storage"><?php _e( 'Всего', 'usam' ); ?></td>
								<td class="barcode"><?php _e( 'Штрих-код', 'usam' ); ?></td>
								<td class="sku"><?php _e( 'Артикул', 'usam' ); ?></td>
								<td class="action"></td>
							</tr>
						</thead>
						<tbody>
							<?php	
							if ( !empty($this->data['storage']) )
							{
								$storage = usam_get_storage( $this->data['storage'] ); 															
								$storage_code = $storage['meta_key'];								
							}
							else
								$storage_code = null;
							$products = array();
							$i = 0;
							$q = 0;		
							if ( !empty($this->data['products']) )	
							{
								foreach ( $this->data['products'] as $product )
								{					
									$dimensions = '';									
									if ( $storage_code != null )
										$q = (int)usam_get_product_meta( $product['product_id'], $storage_code );
								
									$prod_meta = maybe_unserialize( usam_get_product_meta($product['product_id'] , 'product_metadata' ) );
									if ( !empty($prod_meta) && isset($prod_meta['dimensions']) && !empty($prod_meta['dimensions']['width']) )
										$dimensions = $prod_meta['dimensions']['width'] .' х '. $prod_meta['dimensions']['height'] .' х '. $prod_meta['dimensions']['length'];
									$products[$product['product_id']] = array( 'quantity' => $product['quantity_shipment'], 'reserve' => $product['reserve'] );
									$i++;		
									
									$weight = (float)usam_get_product_meta( $product['product_id'], 'weight' );									
								?>								
								<tr class = "usam_display_data">
									<td class="number"><?php echo $i; ?></td>								
									<td class="name"><a href="<?php echo admin_url("post.php?post=".$product['product_id']."&action=edit"); ?>" target="_blank"><?php echo $product['name']; ?></a></td>
									<td class="dimensions"><?php echo $dimensions; ?></td>
									<td class="weight"><?php echo $weight; ?></td>
									<td class="order_quantity"><?php printf( __('%s шт','usam'),$product['quantity']); ?></td>	
									<td class="quantity"><?php printf( __('%s шт','usam'),$product['quantity_shipment']); ?></td>	
									<td class="reserve"><?php printf( __('%s шт','usam'),$product['reserve']); ?></td>	
									<td class="stock"><?php printf( __('%s шт','usam'),$q ); ?></td>
									<td class="storage"><?php printf( __('%s шт','usam'),usam_get_product_meta( $product['product_id'], 'storage' ) ); ?></td>
									<td class="barcode"><?php echo usam_get_product_meta( $product['product_id'], 'barcode' ); ?></td>
									<td class="sku"><?php echo usam_get_product_meta( $product['product_id'], 'sku' ); ?></td>
									<td class="action"></td>
								</tr>
								<?php
								}
							}	
							else
							{
								?>								
								<tr class = "no-items usam_display_data">
									<td colspan="2"><?php _e( 'нет товаров', 'usam' ); ?></td>
								</tr>
								<?php									
							}
							$i = 0;								
							foreach ( $this->cartcontent as $product )
							{				
								if ( $storage_code != null )
									$q = (int)usam_get_product_meta( $product->product_id, $storage_code );
								$quantity = isset($products[$product->product_id])?$products[$product->product_id]['quantity']:'';
								$reserve = isset($products[$product->product_id])?$products[$product->product_id]['reserve']:'';
								$dimensions = '';	
								$prod_meta = maybe_unserialize( usam_get_product_meta($product->product_id , 'product_metadata' ) );
								if ( !empty($prod_meta) && isset($prod_meta['dimensions']) && !empty($prod_meta['dimensions']['width']) )
									$dimensions = $prod_meta['dimensions']['width'] .' х '. $prod_meta['dimensions']['height'] .' х '. $prod_meta['dimensions']['length'];
								
								$weight = (float)usam_get_product_meta( $product->product_id, 'weight' );			
								$i++;
								?>	
								<tr class = "usam_edit_data">
									<td class="number" style="text-align: center; width: 30px;"><?php echo $i; ?></td>								
									<td class="name"><?php echo $product->name; ?></td>
									<td class="dimensions"><?php echo $dimensions; ?></td>
									<td class="order_quantity"><?php echo $product->quantity; ?></td>
									<td class="weight"><?php echo $weight; ?></td>
									<td class="quantity">
										 <input <?php $this->disabled(); ?> type='text' size = '3' maxlength='3' value='<?php echo $quantity; ?>' name='<?php echo $this->form_key; ?>[products][<?php echo $product->id; ?>][quantity]'/>
									</td>	
									<td class="reserve">
										 <input <?php $this->disabled(); ?> type='text' size = '3' maxlength='3' value='<?php echo $reserve; ?>' name='<?php echo $this->form_key; ?>[products][<?php echo $product->id; ?>][reserve]'/>
									</td>
									<td class="stock"><?php printf( __('%s шт','usam'),$q ); ?></td>
									<td class="storage"><?php printf( __('%s шт','usam'),usam_get_product_meta( $product->product_id, 'stock' ) ); ?></td>
									<td class="barcode"><?php echo usam_get_product_meta( $product->product_id, 'barcode' ); ?></td>
									<td class="sku"><?php echo usam_get_product_meta( $product->product_id, 'sku' ); ?></td>
									<td class="action">
										<a class='button_delete'></a>
									</td>
								</tr>
								<?php				
							}
							?>
						</tbody>
					</table>
				</div>
				<?php				
				}
				?>
			</div>
			<?php
			if ( !empty($this->id) ) 
			{
			?>	
			<input type="hidden" name="shipped_ids[]" value="<?php echo $this->id; ?>"/>		
			<?php				
			}
			?>			
		</div>		
		<?php
	}
	
	public function display_attached() 
	{    
		?>		
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_edit_table">
			<tbody>						
				<tr>
					<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер заказа', 'usam' ); ?>:</td>							
					<td class="usam_table_cell_option">
						<span class="usam_display_data"><a href="<?php echo usam_get_url_order($this->data['order_id']); ?>"><?php echo $this->data['order_id']; ?></a></span>
						<input type="hidden" name="<?php echo $this->form_key; ?>[order_id]" value="<?php echo $this->data['order_id']; ?>">
						<input type="text" name="<?php echo $this->form_key; ?>[order_id]" class = "usam_edit_data" value="<?php echo $this->data['order_id']; ?>">
					</td>
				</tr>													
			</tbody>
		</table>
			
	<?php
	}	
		
	function display_left()
	{			
		usam_add_box( 'usam_display_attached', __( 'Прикреплен', 'usam' ), array( $this, 'display_attached' ), array(), true );	
		usam_add_box( 'usam_payment_general_information', __( 'Общая информация', 'usam' ), array( $this, 'display_document' ), null, true );	
	}	
}
?>