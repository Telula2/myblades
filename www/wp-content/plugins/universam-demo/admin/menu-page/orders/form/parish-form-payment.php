<?php		
require_once(USAM_FILE_PATH . '/admin/menu-page/orders/includes/form-payment.php');				
class USAM_Form_Payment extends USAM_Form_Payment_Edit
{
	protected $payment_type = 1;
	
	protected function display_document_sidebar() 
	{ 
		?>	
		<div class="usam_document_container-sidebar">
			<div class="usam-document-logo"></div>				
			<?php					
			$printed_form = array( 
				array( 'form' => 'payment_invoice', 'title' => esc_html__( 'Счет', 'usam' ) ), 					
			);					
			$printed_form = apply_filters( 'usam_printed_form_payment_document', $printed_form );	
				
			if ( $this->data['bank_account_id'] )
			{
			?>						
			<div class ="usam_container-action-document-block">
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Комплект документов', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
						<tbody>
							<?php															
							foreach ( $printed_form as $link )
							{					
								$url = usam_url_admin_action( 'printed_form', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) );
								?>		
								<tr>										
									<td class="usam_table_cell_option">
										<img src='<?php echo USAM_CORE_IMAGES_URL; ?>/printer.png' alt='<?php echo $link['title']; ?>' />&ensp;				
										<a href='<?php echo $url; ?>' target="_blank"><?php echo $link['title']; ?></a>	
									</td>
								</tr>	
								<?php				
							}		
							?>	
						</tbody>								
					</table>
				</div>
			</div>
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Комплект документов в pdf', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
					<tbody>
						<?php															
						foreach ( $printed_form as $link )
						{					
							$url = usam_url_admin_action( 'printed_form_to_pdf', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) );
							?>		
							<tr>										
								<td class="usam_table_cell_option">
									<img src='<?php echo USAM_CORE_IMAGES_URL; ?>/printer.png' alt='<?php echo $link['title']; ?>' />&ensp;				
									<a href='<?php echo $url; ?>' target="_blank"><?php echo $link['title']; ?></a>	
								</td>
							</tr>	
							<?php				
						}		
						?>	
					</tbody>								
				</table>
			</div>
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Отправить счет клиенту', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form">
					<tbody>
						<tr>										
							<td class="usam_table_cell_option">
								<?php $action_url = usam_url_admin_action( 'send_payment_invoice_email', admin_url('admin.php'), array( 'item_id' => $this->id ) ); ?>	
								<a id = "send_payment_invoice_email" href='<?php echo $action_url; ?>'>						
									<img src='<?php echo USAM_CORE_IMAGES_URL . "/email_go.png"; ?>'/>	
									<?php _e( 'Отправить счет клиенту', 'usam' ); ?>							
								</a>	
							</td>
						</tr>	
						
					</tbody>								
				</table>
			</div>	
			<?php 
			} else { 
				
			}?>	
		</div>	
		<?php 
	}
}
?>