<?php		
//http://rupost.info/api
require_once( USAM_FILE_PATH .'/admin/includes/view_form.class.php' );	
class USAM_Form_tracking extends USAM_View_Form
{		
	private $shipped_document;	
	
	protected function get_title_tab()
	{ 	
		return sprintf(__('Отслеживание посылки № %s', 'usam'), $this->shipped_document['track_id']);
	}
	
	protected function title_tab_none( ) 
	{ 
		esc_attr_e('Почтовое отправление не найдено.','usam');
	}
	
	protected function toolbar_buttons( ) 
	{
		
	}
	
	protected function get_url_go_back( ) 
	{ 
		return add_query_arg( array( 'action' => 'edit', 'id' => $this->shipped_document['id'], 'page' => 'orders', 'tab' => 'shipped' ), admin_url('admin.php') );
	}
	
	protected function get_data_tab()
	{ 	
		$this->shipped_document = usam_get_shipped_document( $this->id );

		$shipped_instance = usam_get_shipping_class( $this->shipped_document['method'] );
		$this->data = $shipped_instance->get_history_mail( $this->shipped_document['track_id'] );		
		$error = $shipped_instance->get_errors( );		
		if ( !empty($error) ) 
		{ 
			usam_set_user_screen_error( $error );
		} 			
		$this->tabs = array( 
			array( 'slug' => 'history', 'title' => __('История','usam') ), 		
		);
	}
	
	protected function main_content_cell_1( ) 
	{ 	
		?>		
		<h4><?php _e('Отгрузка', 'usam'); ?></h4>
		<table class="header_main_content_table">
			<tr><td><strong><?php _e('Отгрузка', 'usam'); ?>:</strong></td><td><?php echo $this->shipped_document['id']; ?></td></tr>		
			<tr><td><strong><?php _e('Статус', 'usam'); ?>:</strong></td><td><?php echo usam_get_shipped_document_status_name( $this->shipped_document['status'] );	?></td></tr>	
		</table>	
		<?php	
	}		
	
	protected function main_content_cell_2( ) 
	{ 
		$order = new USAM_Order( $this->shipped_document['order_id'] );	
		$order_data = $order->get_data();
		$customer_data = wp_unslash( $order->get_customer_data() );				
		$list_properties = usam_get_order_properties( array( 'type_payer' => $order_data['type_payer'] ) );				
		?>	
		<h4><?php _e('Данные для доставки', 'usam'); ?></h4>
		<table class="header_main_content_table">	
			<?php 			
			foreach( $list_properties as $data )		
			{  
				if ( stripos($data->unique_name,'shipping') !== false && !empty($customer_data[$data->unique_name]['value']) )
				{
					?><tr><td><strong><?php echo $data->name; ?>:</strong></td><td><?php echo usam_get_display_order_property( $customer_data[$data->unique_name]['value'], $data->type ); ?></td></tr><?php	
				}
			}
			?>	
		</table>	
		<?php	
	}
	
	protected function main_content_cell_3( ) 
	{				
		?>		
		<table class="header_main_content_table">
			<?php if ( !empty($this->data['sender_name']) ) { ?>
				<tr><td><strong><?php _e('Имя отправителя', 'usam'); ?>:</strong></td><td><?php echo $this->data['sender_name']; ?></td></tr>
			<?php } ?>		
			<?php if ( !empty($this->data['destination_address']) ) { ?>			
				<tr><td><strong><?php _e('Адрес назначения', 'usam'); ?>:</strong></td><td><?php echo $this->data['destination_address']['country'].' '.$this->data['destination_address']['address'].' '.$this->data['destination_address']['index']; ?></td></tr>
			<?php } ?>		
			<?php if ( !empty($this->data['weight']) ) { ?>			
				<tr><td><strong><?php _e('Вес', 'usam'); ?>:</strong></td><td><?php echo $this->data['weight']; ?></td></tr>
			<?php } ?>	
			<?php if ( !empty($this->data['departure_type']) ) { ?>			
				<tr><td><strong><?php _e('Тип отправления', 'usam'); ?>:</strong></td><td><?php echo $this->data['departure_type']; ?></td></tr>
			<?php } ?>	
			<?php if ( !empty($this->data['payment']) ) { ?>			
				<tr><td><strong><?php _e('Стоимость отправления', 'usam'); ?>:</strong></td><td><?php echo $this->data['payment']; ?></td></tr>
			<?php } ?>				
		</table>	
		<?php	
	}
	
	function display_tab_history()
	{
		?>	
		<table class = "wp-list-table widefat fixed striped">
			<thead>
				<tr>
					<td><?php esc_html_e( 'Дата', 'usam' ); ?></td>
					<td><?php esc_html_e( 'Место', 'usam' ); ?></td>
					<td><?php esc_html_e( 'Описание', 'usam' ); ?></td>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $this->data['operations'] as $value )
			{
				?>
				<tr>
					<td><?php echo $value['date']; ?></td>
					<td><?php echo $value['description']; ?></td>
					<td><?php echo $value['name']; ?></td>						
				</tr>
				<?php				
			}
			?>				
			</tbody>
		</table>
		<?php			
	}
}