<?php		
require_once( USAM_FILE_PATH .'/admin/includes/view_form.class.php' );	
class USAM_Form_product extends USAM_View_Form
{			
	protected function get_title_tab()
	{ 	
		return sprintf(__('Товар %s', 'usam'), $this->data->post_title );
	}
	
	protected function title_tab_none( ) 
	{ 
		esc_attr_e('Товар не найден.','usam');
	}
	
	protected function toolbar_buttons( ) {	}
	
	protected function get_url_go_back( ) 
	{ 
		$order_id = (int) $_REQUEST['order_id'];
		return add_query_arg( array( 'action' => 'edit', 'id' => $order_id, 'page' => 'orders', 'tab' => 'orders' ), admin_url('admin.php') );
	}
	
	protected function get_data_tab()
	{ 					
		$this->data = get_post( $this->id );
		
		$this->tabs = array( 
			array( 'slug' => 'crosssell', 'title' => __('Перекрестные продажи','usam') ), 		
		);
	}
	
	protected function header_view()
	{		
		$p_data = get_product_custom( $this->id );
		$p_meta = $p_data["_usam_product_metadata"];				
		unset($p_data["_usam_product_metadata"]);
		?>
		<div id="header">	
			<div class = "product_colum_2">			
				<div class='product_header_left'>		
					<h1>
						<?php echo $this->data->post_title; ?>
					</h1>
					<div class = "box_link">
						<a  href="<?php echo $this->data->guid;  ?>"><?php _e('Посмотреть товар', 'usam'); ?></a> | 
						<a  href="<?php echo get_edit_post_link($product->ID);  ?>"><?php _e('Посмотреть карточку товара', 'usam'); ?></a>
					</div>			
					<div class = "product_description product_description_box">
						<?php echo $this->data->post_content;  ?>
					</div>
					<div class = "single_additional_description product_description_box">
						<?php echo $this->data->post_excerpt;  ?>
					</div>		
				</div>
				<div class='product_image_container'><?php usam_single_image( $this->data->ID, $this->data->post_title ); ?></div>
			</div>
		</div>
		
	<div id="content">		
	<div class='profit_list'>
			<h3><?php esc_attr_e('Информация о цене','usam'); ?></h3>
			<?php $type_prices = usam_get_prices( ); ?>			
		<div id='price_info' class ="usam_tabs usam_tabs_style1">
			<div class='header_tab'>
				<ul>
				<?php 			
				foreach ( $type_prices as $value )
				{		
					echo "<li class='tab'><a href='#tab-".$value['id']."'>".$value['title']."</a></li>";	
				}				
				?>
				</ul>
			</div>
			<div class='countent_tabs'>
				<?php 						
				foreach ( $type_prices as $value )
				{	
					$price_key = "_usam_price_".$value['code'];
					$old_price_key = "_usam_old_price_".$value['code'];					
					?>
<div id='tab-<?php echo $value['id']; ?>' class='tab'>
		<div class='block_price'>
			<table>
				<tr>
					<td><label><?php _e( 'Цена', 'usam' ); ?>:</label></td>
					<td><?php echo number_format( (float)$p_data[$price_key], 2, '.', '' ); ?></td>
				</tr>
				<tr>
					<td><label for='add_form_special'><?php _e( 'Старая цена', 'usam' ); ?>:</label></td>
					<td><?php echo number_format( (float)$p_data[$old_price_key], 2, '.', '' ); ?></td>
				</tr>		
			</table>	
		</div>
</div>
		<?php 			
		}	
		?>	
			</div>	
		</div>
</div>		
	<div class='storage_list prod_details_box'>
			<h3><?php esc_attr_e('Остаток товара по складам','usam'); ?></h3>
			<table class = "storage_table">
				<thead>
					<tr>
						<td><?php esc_html_e( 'Название', 'usam' ); ?></td>
						<td><?php esc_html_e( 'Остаток', 'usam' ); ?></td>	
					</tr>
				</thead>
				<tbody>
				<?php				
				$storages = usam_get_stores();				
				foreach ( $storages as $storage )
				{
					$pmeta_key = USAM_META_PREFIX.$storage->meta_key;
					if ( isset($p_data[$pmeta_key]) )
						$stock = $p_data[$pmeta_key];
					else
						$stock = 0;
					?>
					<tr>
						<td><?php echo $storage->title; ?></td>
						<td><?php echo $stock; ?></td>						
					</tr>
					<?php				
				}
				?>
				<tr>
					<td><?php esc_html_e( 'Общий доступный остаток', 'usam' ); ?></td>
					<td><?php echo $p_data['_usam_stock']; ?></td>
				</tr>
				<tr>
					<td><?php esc_html_e( 'Общий остаток', 'usam' ); ?></td>
					<td><?php echo $p_data['_usam_storage']; ?></td>
				</tr>
				</tbody>
			</table>			
		</div>	
		
		<div class='prod_details_box'>
			<h3><?php esc_attr_e('Общая информация','usam'); ?></h3>				
			<table class = "price_table">
				<?php
				if (!empty($p_meta['webspy_link']))
				{
					$host = parse_url($p_meta['webspy_link'], PHP_URL_HOST);					
					?>	
					<tr>
						<td><?php esc_html_e( 'Поставщик товара', 'usam' ); ?>:</td>
						<td><a href='<?php echo $p_meta['webspy_link']; ?>' target='_blank'><?php echo $host; ?></a></td>
					</tr>					
					<?php
				}
				?>	
				<tr>
					<td><?php esc_html_e( 'Артикул', 'usam' ); ?>:</td>
					<td><?php echo $p_data['_usam_sku']; ?></td>
				</tr>
				<tr>
					<td><?php esc_html_e( 'Штрих-код товара', 'usam' ); ?>:</td>
					<td><?php echo $p_data['_usam_barcode']; ?></td>
				</tr>			
			
			</table>
		</div>
	</div>	
		<?php
	}
	
	function display_tab_crosssell()
	{
		?>	
		<div class='product_footer'>
		<?php
		$product_ids = usam_get_associated_products( $this->id, 'crosssell' );
		if ( !empty( $product_ids ))
		{					
			?>
			<div class='product_crosssell'>
				<h3><?php esc_attr_e('Перекрестные продажи (Cross-Sells). Посоветуйте клиентам эти товары.','usam'); ?></h3>
				<div class='product_crosssell_container'>		
					<?php				
					$args = array( 
						'post__in' => $product_ids, 
						'update_post_meta_cache' => true, 
						'update_post_term_cache' => false, 
						'cache_results' => true, 
						'post_status' => 'publish',
						'meta_query'=> array( array( 'key' => '_usam_stock', 'value' => '0', 'compare' => '!=' ) ) 
					);
					$products = usam_get_products( $args );
					if ( !empty($products) ) 
					{
						?>
						<ul>	
						<?php	
						foreach ($products as $product) 
						{
							?>
							<li>
								<?php echo usam_get_product_thumbnail($product->ID, 'product-image', $this->data->post_title); ?> 
								<div class = "title"><?php echo $this->data->post_title; ?></div>
								<div class = "price"><?php echo usam_currency_display( usam_get_product_price( $product_id ), array( 'display_as_html' => false ) );?></div>
							</li>
							<?php					
						}
						?>
						</ul>	
						<?php	
					}
					?>
				 </div>
			</div>
		<?php
		}
		?>
	</div>
		<?php			
	}
}