<?php
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );			
class USAM_Form_refunds extends USAM_Edit_Form
{	
	public function __construct()
	{       		
		parent::__construct();		
		$this->actions = array( 'edit'  => array( 'button_title' => '', 'title' => __('Изменить документ возврата', 'usam'), 'display' => false ), );		
	}	

	protected function get_title_tab()
	{ 	
		
		if ( $this->id != null )
		{
			if ( empty($this->data) )	
			{				
				$title = sprintf( __( 'Возврат №%s не существует', 'usam' ), $this->id );		
			}
			else
			{
				$title = sprintf( __('Изменить возврат %s','usam'), '<span class="number">№'.$this->data['id'].'</span> <span class="subtitle_date">'.esc_html__( 'от', 'usam' ).' '.date_i18n( get_option('date_format'),  strtotime($this->data['date_insert']) ).'</span>');
			}
		}
		else
			$title = __('Добавить новый возврат', 'usam');	
		return $title;
	}	
	
	protected function get_data_tab()
	{	
		if ( $this->id != null )
		{	
			$this->data = usam_get_return_purchases( $this->id );
			$this->data['products'] = usam_get_products_return_purchasess( $this->id , 'ARRAY_A' );	
		}
		else
		{		
			$this->data = array( 'id' => '', 'date_insert' => '', 'date_update' => '', 'order_id' => '', 'sum' => '', 'status' => '', 'storage' => '', 'reason' => '', 'notes' => '', 'products' => array() );
		}
	}
	
	public function general_information()
	{      	
		?>
		<div class="usam_document_container-right">
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Документ', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
					<tbody>
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер документа', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option"><?php echo $this->data['id']; ?></td>
						</tr>											
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата создания документа', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option"><?php echo $this->data['date_insert']; ?></td>
						</tr>	
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата обновления документа', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option"><?php echo $this->data['date_update']; ?></td>
						</tr>								
					</tbody>
				</table>
			</div>			
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Основание', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table">
					<tbody>						
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер заказ', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option"><?php echo $this->data['order_id']; ?></td>
						</tr>		
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Стоимость', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option tal"><?php echo $this->data['sum']; ?></td>
						</tr>
					</tbody>
				</table>
			</div>		
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Свойство', 'usam' ); ?></div>
				<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table">
					<tbody>				
						<tr>
							<td class="usam_table_cell_name vat" class = "name"><?php _e( 'Статус', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option">
								<span class = "usam_display_data"><?php echo usam_get_title_status_return_purchases_document( $this->data['status'] ); ?></span>
								<div class = "usam_edit_data">
									<?php 
									$args = array(
										'name'                  => '_refunds[status]',
										'id'                    => "usam-refunds_status",
										'class'                 => 'chzn-select-nosearch-load',
										'additional_attributes' => '',
									);
									echo usam_get_refunds_purchases_status_dropdown($this->data['status'] , $args ) 
									?>												
								</div>										
							</td>
						</tr>
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Склад возврата', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option">
								<span class = "usam_display_data">
									<?php 
									$stores = usam_get_stores();
									if ( empty($this->data['storage']) )
										_e( 'Не выбрано', 'usam' );
									else
									{
										$storage = usam_get_storage( $this->data['storage'] ); 
										echo isset($storage['title'])?$storage['title']:''; 
									}
									?>
								</span>
								<div class = "usam_edit_data">
									<select disabled = "disabled" class="chzn-select-nosearch-load" name = "_refunds[storage]">
										<option value="">---</option>
										<?php												
										foreach ($stores as $value)
										{					
										?>
											<option value="<?php echo $value->id; ?>"  <?php echo ( $value->id == $this->data['storage']?'selected="selected"':'') ?>><?php echo $value->title; ?></option>
										<?php
										}
										?>										
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Причина', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option tal">
								<span class = "usam_display_data"><?php echo $this->data['reason']; ?></span>
								<textarea rows="5" disabled = "disabled" type="text" name="_refunds[reason]" class = "usam_edit_data"><?php echo $this->data['reason']; ?></textarea>
							</td>
						</tr>		
						<tr>
							<td class="usam_table_cell_name" class = "name"><?php _e( 'Заметки', 'usam' ); ?>:</td>
							<td class="usam_table_cell_option tal">
								<span class = "usam_display_data"><?php echo $this->data['notes']; ?></span>
								<textarea rows="5" disabled = "disabled" type="text" name="_refunds[notes]" class = "usam_edit_data"><?php echo $this->data['notes']; ?></textarea>
							</td>
						</tr>						
					</tbody>
				</table>
			</div>
		</div>	
		<input type='hidden' value='<?php echo $this->data['order_id']; ?>' name='order_id' />
		<?php
	}	
	
	function display_products()
	{	
		$order = new USAM_Order( $this->data['order_id'] );		
		$order_products = $order->get_order_products();	
		if ( !empty($order_products) ) 
		{
		?>	
			<table class="usam_table widefat">
				<thead>
					<tr>
						<td>№</td>									
						<td><?php _e( 'Название', 'usam' ); ?></td>		
						<td><?php _e( 'Штрих-код', 'usam' ); ?></td>
						<td><?php _e( 'Артикул', 'usam' ); ?></td>								
						<td><?php _e( 'Количество', 'usam' ); ?></td>								
					</tr>
				</thead>
				<tbody id="shipment_basket_1sale-order-basket-product-25" data-basket-id="25">
					<?php	
					if ( !empty($this->data['storage']) )
					{
						$storage = usam_get_storage( $this->data['storage'] ); 															
						$storage_code = $storage['meta_key'];									
					}
					else
						$storage_code = null;
					
					$products = array();
					$i = 0;
					$q = 0;							
					foreach ( $this->data['products'] as $product )
					{													
						$products[$product['product_id']] = array( 'quantity' => $product['quantity_shipment'] );
						$i++;								
					?>								
					<tr class = "usam_display_data">
						<td class="number" style="text-align: center; width: 30px;"><?php echo $i; ?></td>								
						<td class="name"><a href="<?php echo admin_url("post.php?post=".$product['product_id']."&action=edit"); ?>" target="_blank"><?php echo $product['name']; ?></a></td>	
						<td class="sku"><?php echo usam_get_product_meta( $product['product_id'], 'sku' ); ?></td>
						<td class="barcode"><?php echo usam_get_product_meta( $product['product_id'], 'barcode' ); ?></td>									
						<td class="quantity"><?php printf( __('%s шт','usam'),$product['quantity_shipment']); ?></td>	
					</tr>
					<?php
					}							
					$i = 0;								
					foreach ( $order_products as $product )
					{										
						$quantity = isset($products[$product->product_id])?$products[$product->product_id]['quantity']:'';											
						$i++;
						?>	
						<tr class = "usam_edit_data">
							<td class="number" style="text-align: center; width: 30px;"><?php echo $i; ?></td>								
							<td class="name"><?php echo $product->name; ?></td>									
							<td class="sku"><?php echo usam_get_product_meta( $product->product_id, 'sku' ); ?></td>
							<td class="barcode"><?php echo usam_get_product_meta( $product->product_id, 'barcode' ); ?></td>
							<td class="quantity">
								 <input disabled = "disabled" type='text' size = '3' maxlength='3' value='<?php echo $quantity; ?>' name='products[<?php echo $product->id; ?>][quantity]'/>
							</td>									
						</tr>
						<?php				
					}
					?>
				</tbody>
			</table>
		<?php				
		}
	}
	
	public function display_payment_documents()
    { 
		if ( $this->id )
			$payments = usam_get_payments( array('document_type' => 'refund', 'document_id' => $this->id ) );				
		else
			$payments = array();
		?>	
		<div id="usam_shipped_products_box" class="set_row_element">
			<?php 
			if ( empty($payments) )
			{
				?>
				<p class ="items_empty"><?php _e( 'Нет документов оплаты', 'usam' ); ?></p>
				<?php	
			}
			else
			{
				foreach($payments as $document)
				{
					$this->display_payment_document( $document->id );
				}
			}	
			$this->display_payment_document( 0 ); 
?>	
		</div>			
		<div class="button_box">			
			<button id='add_button' type="button" class="button" data-id = "<?php echo $this->id; ?>"><?php _e( 'Добавить', 'usam' ); ?></button>
		</div>		
		<?php		
	}	
	
	public function display_payment_document( $document_id = 0 )
	{		
		?>
		<div id="document_shipped-<?php echo $document_id; ?>" class="usam_document_container <?php echo $document_id == 0?"add_new":''; ?>" data-item-id="<?php echo $document_id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Расходный кассовый ордер', 'usam' ); ?> <span id="shipment_19">#<?php echo $document_id; ?></span></div>
				<div class="usam_document_container-action-block">					
					<div class="usam_document_container-action"><a id = "bitton_delete" title='<?php _e( 'Удалить документ', 'usam' ); ?>'><?php _e( 'Удалить', 'usam' ); ?></a></div>
					<div class="usam_document_container-action"><a href="" id="button_edit"><?php _e( 'Редактировать', 'usam' ); ?></a></div>
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>			
			<?php			
			require_once( USAM_FILE_PATH ."/admin/menu-page/orders/form/expenditure-form-payment.php");		
			require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
			
			$display_payment = new USAM_Form_Payment( $document_id );
			$display_payment->display_document();
			
			
			//$display = new USAM_Form_Payment( $document_id, array('document_type' => 'refund', 'document_id' => $this->id, 'sum' => $this->data['sum']) );
		
			?>			
		</div>
		<?php
	}
		
	function display_left()
	{	
		usam_add_box( 'usam_refunds_general_information', __( 'Общая информация', 'usam' ), array( $this, 'general_information' ), array(), true );		
		usam_add_box( 'usam_display_products', __( 'Возвращаемые товары', 'usam' ), array( $this, 'display_products' ), array(), true );

		usam_add_box( 'usam_payment_document', __( 'Документы оплаты', 'usam' ), array( $this, 'display_payment_documents' ) );		
	}	
}
?>