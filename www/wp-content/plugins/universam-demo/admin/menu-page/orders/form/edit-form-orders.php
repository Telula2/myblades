<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Orders extends USAM_Edit_Form
{		
	private $payment_history;	

	private $type = 'item_details';
	private $purchase_log;	
	private $sale_basket;
	private $customer_data;
	
	private $purchitem;
	private $order_url;	

	private $is_closed = false;
	private $display = array();
	
	protected function get_data_tab(  )
	{			
		$this->order_url = usam_get_url_order( $this->id );
		$this->purchase_log = new USAM_Order( $this->id );	
		$this->data = $this->purchase_log->get_data();		
		if ( !$this->purchase_log->exists() )	
		{
			$this->not_exist = true;
			return false;
		}					
		usam_work_on_task( array( 'title' => sprintf(__('Работа на заказом %s','usam'), $this->id) ), array( 'object_type' => 'order', 'object_id' => $this->id ) );
		
		$this->data = $this->purchase_log->get_data();		
		
		$this->sale_basket = $this->purchase_log->get_order_products();	
		$calculated_data = $this->purchase_log->get_calculated_data();	

		$this->data = array_merge( $this->data,  $calculated_data );				
		$this->customer_data = wp_unslash($this->purchase_log->get_customer_data());			
		
		$this->is_closed = usam_check_order_is_completed( $this->data['status'] );		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );	
		add_action( 'admin_enqueue_scripts', array(&$this, 'print_scripts_style') );
	}
	
	protected function get_title_tab()
	{ 	
		if ( !$this->purchase_log->exists() )	
		{				
			$title = sprintf( __( 'Заказ №%s не существует', 'usam' ), $this->id );		
		}
		elseif ( $this->id != null )
			$title = sprintf( __('Изменить заказ %s','usam'), '<span class="number">#'.$this->data['id'].'</span> <span class="subtitle_date">'.esc_html__( 'от', 'usam' ).' '.date_i18n( get_option('date_format'),  strtotime($this->data['date_insert']) ).'</span>');
		else
			$title = __('Добавить новый заказ', 'usam');	
		return $title;
	}
	
	protected function toolbar_buttons( ) 
	{
		if ( $this->id != null )
		{
			global $current_screen;		
			$delete_url = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'delete' )), 'usam-'.$current_screen->id);
			$view_url = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'view' )), 'usam-'.$current_screen->id);
			?>
			<ul>
				<li><a href="<?php echo $delete_url; ?>" class="button"><?php _e('Удалить','usam'); ?></a></li>
				<li><a href="<?php echo $view_url; ?>" class="button"><?php _e('Посмотреть','usam'); ?></a></li>				
			</ul>
			<?php		
		}
	}
	
	function print_scripts_style()
	{
		wp_enqueue_script( 'usam-edit-order', USAM_URL.'/admin/js/form-display/edit-order.js', array( 'jquery' ), USAM_VERSION );			
		$data = array(					
			'order_id'                               => $this->id,
			'get_form_edit_customer_details_nonce'   => usam_create_ajax_nonce( 'get_form_edit_customer_details' ),
			'change_purchase_log_item_status_nonce'  => usam_create_ajax_nonce( 'change_purchase_log_item_status' ),
			'add_order_item_nonce'                   => usam_create_ajax_nonce( 'add_order_item' ),
			'recalculate_prices_nonce'               => usam_create_ajax_nonce( 'recalculate_prices' ),
			'add_product_box_nonce'                  => usam_create_ajax_nonce( 'add_product_box' ),
			'delete_order_item_nonce'                => usam_create_ajax_nonce( 'delete_order_item' ),
			'add_payment_nonce'                      => usam_create_ajax_nonce( 'add_payment' ),		
			'purchaselog_message_add_payment'        => __( 'Введите номер', 'usam'),
			'delete_payment_item_nonce'              => usam_create_ajax_nonce( 'delete_payment_item' ),
			'add_shipped_product_nonce'              => usam_create_ajax_nonce( 'add_shipped_product' ),		
			'send_tracking_email_nonce'              => usam_create_ajax_nonce( 'send_tracking_email' ),
			'billing_same_as_shipping_nonce'         => usam_create_ajax_nonce( 'billing_same_as_shipping' ),
			'purchaselog_message_and_id'             => __( 'Добавить номер', 'usam'),				
			'add_document_payment_nonce'             => usam_create_ajax_nonce( 'add_document_payment' ),
			'add_document_shipped_nonce'             => usam_create_ajax_nonce( 'add_document_shipped' ),
			'upload_customer_data_in_order_nonce'    => usam_create_ajax_nonce( 'upload_customer_data_in_order' ),					
			'sending_message'                        => __( 'отправка...', 'usam' ),
			'delete_order_item_message'              => __( "Вы собираетесь удалить позицию из заказа.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'delete_payment_item_message'            => __( "Вы собираетесь удалить способ оплаты.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'delete_shipped_item_message'            => __( "Вы собираетесь удалить способ доставки.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'sent_message'                           => __( 'Письмо отправлено!', 'usam' ),			
		);	
		wp_localize_script( 'usam-edit-order', 'USAM_Edit_Order', $data );	
		wp_enqueue_style('usam-order-admin'); 
	}
	
	function admin_footer()
	{	
		echo usam_get_modal_window( __('Отправить сообщение','usam'), 'form_send_message', $this->get_form_send_message() );
		echo usam_get_modal_window( __('Отправить сообщение','usam'), 'form_send_sms', $this->get_form_send_sms() );
	}	

	function get_form_send_message( )
	{	
		$list_properties = usam_get_order_properties( );		
		$to_email = array();
		foreach ( $list_properties as $property )
		{	 
			if ( $property->type == 'email' && !empty($this->customer_data[$property->unique_name]['value']) )
			{
				$to_email[] = $this->customer_data[$property->unique_name]['value'];
			}
		}			
		if ( usam_is_type_payer_company($this->data['type_payer']) ) 		
			$customer_type = 'company';		
		else
			$customer_type = 'contact';
		
		$args = array( 'object_id' => $this->data['id'], 'object_type' => 'order', 'customer_type' => $customer_type, 'customer_id' => $this->data['customer_id'], 'to_email' => $to_email, 'subject' => sprintf( __( 'Сообщение о вашем заказе №%s','usam' ),$this->data['id'])." - ".get_bloginfo('name'), 		
		'insert_text' => array( 
			'_order_id' => __( 'Номер заказа','usam'), 
			'_readiness_date' => __( 'Дата готовности','usam'), 
			'_storage_pickup' => __( 'Офис получения','usam'), 
			'_storage_pickup_address' => __( 'Адрес склада отгрузки','usam') ,
			'_storage_pickup_phone' => __( 'Телефон склада отгрузки','usam'),
			'_date_allow_delivery' => __( 'Дата и время доставки','usam')  
		) );		 
		return usam_get_form_send_message( $args );	
	}	
	
	function get_form_send_sms( )
	{	
		global $user_ID;						
		
		$list_properties = usam_get_order_properties( );
		
		$out  = "<div class='mailing'>";	
		$out .= '<form method="post" action="'.usam_url_admin_action( 'send_sms', admin_url('admin.php') ).'">';		
		$out .= "<input type='hidden' name='object_id' value='$this->id' />";
		$out .= "<input type='hidden' name='object_type' value='order' />";
		$out .= "<div class='mailing-mailing_wrapper'>";
		$out .= "<table class ='widefat'>";
		$out .= "<tr>
					<td>".__('Кому', 'usam')."</td>
					<td><select name = 'phone'>
					";
					foreach ( $list_properties as $property )
					{	 
						if ( $property->type == 'mobile_phone' && !empty($this->customer_data[$property->unique_name]['value']) )
						{
							$out .= "<option value='".$this->customer_data[$property->unique_name]['value']."'>".$this->customer_data[$property->unique_name]['value']."</option>";
						}
					}						
				$out .= "</select></td></tr>							
				<tr>
					<td colspan='2'><textarea rows='10' autocomplete='off' cols='40' name='message' id='message_editor' ></textarea></td>						
				</tr>
			</table>";		
		$out .= "</div>";
		$out .= '<div class="popButton">'.get_submit_button( __( 'Отправить','usam' ), 'primary','action_send_email', false, array( 'id' => 'send-email-submit' ) ).'</div>';	
		$out .= '</form>';
		$out .= "</div>"; 
		return $out;
	}	
	
	public function get_form( $form )
    {				
		switch ( $form ) 
		{			
			case 'order_products':				
				$this->sale_basket = $this->purchase_log->get_order_products();	
				$this->cart_items_table( );		
			break;				
			case 'payment_document':				
				$this->display_payment_document( );		
			break;	
			case 'document_shipped':				
				$this->sale_basket = $this->purchase_log->get_order_products();	
				$this->display_shipped_document( );		
			break;			
			case 'payment_history':	
				$this->display_payment_documents( );		
			break;
			case 'shipped_documents':
				$this->sale_basket = $this->purchase_log->get_order_products();			
				$this->display_shipped_documents( );				
			break;			
			case 'edit_details_order':					
				$this->edit_details_order_box( );				
			break;	
			case 'customer_details':	
				$this->customer_data = $this->purchase_log->get_customer_data();
				$this->type	= 'new_item';	
				$this->customer( );						
			break;			
		}			
	}
	
	function order_url_action( $arg, $action )
	{
		return usam_url_admin_action( $action, $this->order_url, $arg );
	}
	
	function currency_display( $price ) 
	{				
		$args['decimal_point'] = true;
		$args['currency'] = usam_get_currency_price_by_code($this->data['type_price'] );
		return usam_currency_display( $price, $args );
	}	
	
	function location_list_by_code( $property, $value ) 
	{			
		$code = str_replace("shipping", "", $property->unique_name);
		$code = str_replace("billing", "", $code);	
		$code = str_replace("company_", "", $code);	
		
		$locations = usam_get_locations_by_code( $code );			
		$output = "<select id ='$property->unique_name' name='collected_data[$property->id]' data-props_id = '$property->id' class = 'location_list chzn-select' data-placeholder='".__('Выберете из списка...','usam')."'>\n\r 
		<option value=''>--".__('Не выбрано','usam')."--</option>";	
		foreach ( $locations as $item )
		{			
			$output .= "<option value='".$item->id."' ".selected($value, $item->id, false).">".esc_html( $item->name )."</option>\n\r";
		}
		$output .= "</select>\n\r";		
		return $output;
	}
	
	function location_list( $code, $value ) 
	{			
		$locations = usam_get_locations_by_code( $code );		
		$output = "<select id = 'location-$code' name='location_list[$code]' class = 'chzn-select' data-placeholder='".__('Выберете из списка...','usam')."'>\n\r 
		<option value=''>--".__('Не выбрано','usam')."--</option>";	
		foreach ( $locations as $item )
		{
			if ( $value == $item->id )
				$selected = "selected='selected'";
			else
				$selected = "";				
			$output .= "<option value='".$item->id."' $selected>".esc_html( $item->name )."</option>\n\r";
		}
		$output .= "</select>\n\r";		
		return $output;
	}
	
	private function form_field( $property, $value ) 
	{			
		$output = '';	
		$name_submit = "collected_data[$property->id]";		
		switch ( $property->type ) 
		{
			case "location":			
				$t = new USAM_Autocomplete_Forms();				
				$t->get_form_position_location( $value, array( 'id' => $property->id, 'name' => $name_submit ) );		
			break;
			case "location_type":		
				if ( $value != '' && !is_numeric($value) )
				{					
					if ( $value == '#' )
						$value = '';
					$output = "<input type='text' id='checkout_item-$property->id' class='checkout_item' value='".esc_attr( $value )."' name='$name_submit' />";
				}
				else
				{
					$output = $this->location_list_by_code( $property, $value );				
				}
			break;	
			case "location_text":			
				$output = "<input id='checkout_item-$property->id' type='text' class='checkout_item' value='".esc_attr( $value )."' name='$name_submit' />";
			break;			
			case "address":			
			case "textarea":
				$output .= "<textarea id='checkout_item-$property->id' class='checkout_item text' name='$name_submit' rows='3' cols='40' >".esc_html( (string) $value )."</textarea>";
			break;
			case "checkbox":
				if ( !empty($property->options) ) 
				{	
					$options = maybe_unserialize($property->options);	
					$values = maybe_unserialize($value);						
					foreach ( $options as $option ) 
					{
						if ( is_array($values) && in_array($option['value'], $values) )
							$checked = checked( 1, 1, false );
						else
							$checked = '';
						?>
						<label>
							<input id='checkout_item-<?php echo $property->id; ?>' <?php echo $checked; ?> type="checkbox" class='checkout_item' name="<?php echo $name_submit; ?>[]" value="<?php echo esc_attr($option['value']); ?>"  />
							<?php echo esc_html( $option['name'] ); ?>
						</label>
						<?php
					}
				}
			break;					
			case "select":					
				if ( !empty($property->options) ) 
				{
					$output = "<select id='checkout_item-$property->id' name='$name_submit' class='checkout_item'>";
					$output .= "<option value='-1'>" . _x( 'Выберите вариант',' Выпадающее меню при вызове на странице оформления заказа' , 'usam' ) . "</option>";
					$options = maybe_unserialize($property->options);
					foreach ( $options as $option ) 
					{
						$value = esc_attr(str_replace( ' ', '', $value ) );
						$output .="<option " . selected( $option['value'], $value, false )." value='".esc_attr( $option['value'] )."'>".esc_html( $option['name'] )."</option>\n\r";
					}
					$output .="</select>";
				}
			break;
			case "radio":					
				if ( !empty($property->options) ) 
				{
					$options = maybe_unserialize($property->options);
					foreach ( $options as $option ) 
					{
						?>
						<label>
							<input id='checkout_item-<?php echo $property->id; ?>' type="radio" <?php checked( $option['value'], $value ); ?> name="<?php echo $name_submit; ?>" value="<?php echo esc_attr( $option['value'] ); ?>" class='checkout_item' />
							<?php echo esc_html( $option['name'] ); ?>
						</label>
						<?php
					}
				}
			break;			
			case "phone":	
			case "text":		
			case "email":
			case "coupon":
			default:				
				$output = "<input type='text' id='checkout_item-$property->id' class='checkout_item' value='".esc_attr( $value )."' name='$name_submit' />";
			break;
		}
		return $output;
	}
	
	private function display_details_properties_order( $properties_order )
	{		
		$row = '';		
		foreach( $properties_order as $property )			
		{
			if ( $property->active || !empty($this->customer_data[$property->unique_name]['value']) )
			{
				$display = isset($this->customer_data[$property->unique_name]['value'])? usam_get_display_order_property( $this->customer_data[$property->unique_name]['value'], $property->type ):__('Нет данных','usam');
				$row .= '<tr><td class="name"><strong>'.$property->name.':</strong></td><td><span id = "item-'.$property->unique_name.'">'.$display.'</span></td></tr>';	
			}
		}		
		$out = "<table class = 'subtab-detail-content-table'>";
		if ( $row != '' )
			$out .= $row;
		else
			$out .= "<tr><td>".__('Нет данных','usam')."</td></tr>";		
		$out .= "</table>";
		return $out;
	}
	   
	// Вывести данные покупателя (сведения о доставке, имя, телефон ...)
	public function edit_customer_details( $properties_order )
	{		
		do_action( 'usam_customer_details_top', $properties_order );		
		?>			
		<table class = 'subtab-detail-content-table'>
			<?php						
			foreach( $properties_order as $property )
			{		
				$value = isset($this->customer_data[$property->unique_name]['value'])?$this->customer_data[$property->unique_name]['value']:'';
				if ( $property->active || !empty($value) )
				{
					?>
					<tr> 
					   <td class='name'><label for='checkout_item-<?php echo $property->id; ?>'><?php echo $property->name; ?></label></td>
					   <td id = "_<?php echo $property->unique_name; ?>"><?php echo $this->form_field( $property, $value ); ?></td>
					</tr>			
					<?php 				
				}
			} ?>			
		</table>
		<?php	
		do_action( 'usam_customer_details_bottom', $properties_order );
	}

	private function display_crm_customer( $customer )
	{	
		if ( empty($customer) )
			return false;
		
		$image_attributes = wp_get_attachment_image_src( $customer['thumbnail'], array(100, 100) );						
		if ( empty($image_attributes[0]) )	
			$thumbnail = USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif';			
		else
			$thumbnail = $image_attributes[0];	
		?>				
		<table class ="crm_customer_table">
			<tr>
				<td><strong><?php esc_html_e( 'CRM контакт:', 'usam' ); ?></strong></td>
				<td><img src="<?php echo esc_url( $thumbnail ); ?>" alt=""></td>
				<td><p><a href='<?php echo $customer['link']; ?>' target="_blank"><span class="customer_name" ><?php echo $customer['name']; ?></span></a></p>						
					<p class ="communications">
					<?php 
					if ( !empty($customer['email']) )
					{
						$emails = array();
						foreach ( $customer['email'] as $email ) 
							$emails[] = $email['value'];	
							
						echo __( 'E-mail', 'usam' ).': '.implode(', ',$emails);									
					}
					?>				
					<?php 
					if ( !empty($customer['phone']) )
					{
						$phones = array();
						foreach ( $customer['phone'] as $phone ) 
							$phones[] = $phone['value'];		
							
						echo __( 'т.', 'usam' ).': '.implode(', ',$phones);		
					}
					?>						
					</p>
				</td>
			</tr>
		</table>
		<?php 	
	}
	
	public function customer( )
	{	
		$user = get_user_by('id', $this->data['user_ID']);
		if ( isset($user->user_login) )
		{
			$user_login = $user->user_login;		
			if ( !empty($user->first_name) || !empty($user->first_name) )
				$user_login .= " ($user->first_name $user->last_name)";						
		}
		else
		{
			$user_login = __('Гость','usam');				
		}		
		$name_type_payer = usam_get_name_payer( $this->data['type_payer'] );
		?>
		<div class = "crm_customer">				
				<?php 						
				if ( !empty($this->data['customer_id']) ) 
				{ 					
					if ( usam_is_type_payer_company($this->data['type_payer']) ) 
					{ 
						$customer = usam_get_company( $this->data['customer_id'] );			
						if ( !empty($customer) )
						{
							$customer['link'] = admin_url("admin.php?page=crm&tab=company&action=edit&id=".$customer['id']);	
							$customer['thumbnail'] = $customer['logo'];
						}
					}	
					else					
					{
						$customer = usam_get_contact( $this->data['customer_id'] );
						if ( !empty($customer) )
						{
							$customer['thumbnail'] = $customer['foto'];
							$customer['link'] = admin_url("admin.php?page=crm&tab=contacts&action=view&id=".$customer['id']);
							$customer['name'] = $customer['lastname'].' '.$customer['firstname'].' '.$customer['patronymic'];						
						}
					}					
					$this->display_crm_customer( $customer );					
				} 
			?>
		</div>
		<div class = "usam_details_box">	
			<table class = 'subtab-detail-content-table'>				
				<tr> 
				   <td class="name"><label for='order_user_id'><strong><?php esc_html_e( 'Клиент', 'usam' ); ?>:</strong></label></td>
				   <td>
						<?php 
						$args = array( 'fields' => array( 'id','user_nicename','user_email'), 'sorderby' => 'user_nicename' );
						$users = get_users( $args );						
						 ?>
						<select id = "order_field_userid" class="chzn-select" name = "order_user_id">
							<option value='0' <?php echo ($this->data['user_ID'] == 0 ? 'selected="selected"' :"") ?>><?php esc_html_e( 'Гость', 'usam' ); ?></option>
							<?php				
							foreach ( $users as $user ) 
							{					
								?><option value='<?php echo $user->id; ?>' <?php echo ($this->data['user_ID'] == $user->id ? 'selected="selected"':""); ?>><?php echo $user->user_nicename; ?></option><?php
							}
							?>
						</select>				
					</td>
				</tr>					
				<tr> 
				   <td class="name"><label for='order_user_role'><strong><?php esc_html_e( 'Тип плательщика', 'usam' ); ?>:</strong></label></td>
				   <td>
						<select class="chzn-select-nosearch-load" name = "order_type_payer" id = "order_field_type_payer">						
							<?php				
							$types_payers = usam_get_group_payers();
							foreach( $types_payers as $value )
							{						
								?>               
								<option value="<?php echo $value['id']; ?>" <?php echo ($this->data['type_payer'] == $value['id']) ?'selected="selected"':''?> ><?php echo $value['name']; ?></option>
								<?php
							}
							?>
						</select>
					</td>			
				</tr>	
			</table>						
			<?php 
			$list_properties = usam_get_order_properties( );		
			$order_props_group = usam_get_order_props_groups( );	

			$select_type_payer_data = array( );	
			$others_type_payer_data = array( );			
			foreach( $order_props_group as $group )
			{							
				if ( $this->data['type_payer'] == $group->type_payer_id )
				{			
					foreach( $list_properties as $data )			
					{ 							
						if ( $data->group == $group->id )
						{
							$select_type_payer_data[$group->id][$data->unique_name] = $data;
						}
					}
				}
				else
				{
					foreach( $list_properties as $data )			
					{ 			
						if ( $data->group == $group->id && !empty($this->customer_data[$data->unique_name]) )
						{						
							$others_type_payer_data[$group->id][$data->unique_name] = $data;
						}
					}
				}
			}								
			foreach( $order_props_group as $group )
			{							
				if ( $this->data['type_payer'] == $group->type_payer_id && !empty($select_type_payer_data[$group->id]) )
				{								
					$display_details = $this->display_details_properties_order( $select_type_payer_data[$group->id] );				
					?>
					<div id='order_props_group-<?php echo $group->id; ?>' class = "usam_details usam_details_border">							
						<div class="usam_container-table-container caption border">
							<div class="usam_container-table-caption-title"><?php echo $group->name; ?></div>
							<div id='order_props_group-edit'><?php echo $this->edit_customer_details( $select_type_payer_data[$group->id] ); ?></div>
						</div>		
					</div>								
					<?php 						
				}
			}
			if ( !empty($others_type_payer_data) )
			{
				?>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Другие заполненные группы', 'usam' ); ?></div>
				<?php		
				foreach( $order_props_group as $group )
				{			
					if ( $this->data['type_payer'] != $group->type_payer_id && !empty($others_type_payer_data[$group->id]) )
					{							
						$display_details = $this->display_details_properties_order( $others_type_payer_data[$group->id] );				
						?>	
						<div id='order_props_group-<?php echo $group->id; ?>' class = "usam_details usam_details_border">
							<h3><?php echo $group->name; ?></h3>						
							<div id='order_props_group-edit'><?php echo $this->edit_customer_details( $others_type_payer_data[$group->id] ); ?></div>							
						</div>													
						<?php 						
					}
				}
				?>					
				</div>															
				<?php 		
			}
			?>					
		</div>													
		<?php 		
		if ( $this->type != 'item_details' ) 
		{ 
		?>
		<p>
			<button class="button load_customer_details" data-user_id = "<?php echo $this->data['user_ID']; ?>"><?php esc_html_e( 'Загрузить', 'usam' ); ?></button>
		</p>
		<?php 	
		}		
	}
	
//Вывод таблицы с заказанными товарами
	private function product_table( )
	{		
		if ( empty($this->sale_basket) )
		{
			?>
				<tr><td colspan = '7' class = "cart_items_empty"><?php _e( 'Нет товаров в этом заказе', 'usam' ); ?> </td></tr>
			<?php	
		}
		else
		{	
			$sendback = add_query_arg( array( 'action' => 'view', 'page' => 'orders', 'screen' => 'product', 'order_id' => $this->id ), admin_url('admin.php') );
			foreach ( $this->sale_basket as $key => $product ) 
			{
				$total = ( $product->price + $product->tax ) * $product->quantity;			
				$discount = $product->old_price>0?round(100 - $product->price*100/$product->old_price,2):0;					
				?>
				<tr id ="product_<?php echo $product->id; ?>" data-item-id="<?php echo $product->id; ?>" >					
					<td class="column-n"><?php echo $key+1; ?></td>
					<td class="column-image">
						<?php echo usam_get_product_thumbnail( $product->product_id, 'manage-products' ); ?>
					</td>
					<td class="column-title">
						<a id = "details_product" href="<?php echo get_edit_post_link($product->product_id); ?>"><?php echo $product->name; ?></a>						
					</td>
					<td class="column-sku"><?php echo usam_get_product_meta( $product->product_id, 'sku', true ); ?></td>					
					<td class="column-quantity">
						<span class = "usam_display_data"><?php echo $product->quantity; ?></span><input size="4" type="text" name="_cart_product[<?php echo $product->id; ?>][quantity]" class = "usam_edit_data" id="_cart_product_quantity" value="<?php echo $product->quantity; ?>">
					</td>
					<td class ="column-old_price">
						<span class = "usam_display_data"><?php echo $product->old_price; ?></span><input size="4" type="text" name="_cart_product[<?php echo $product->id; ?>][price]" class = "usam_edit_data" id="_cart_product_price" value="<?php echo $product->old_price; ?>">
					</td>
					<td class="column-discount">
						<span class = "usam_display_data"><?php echo $discount; ?>%</span>		
						<span class = "usam_edit_data">				
							<input size="4" type="text" name="_cart_product[<?php echo $product->id; ?>][discount]" class = "_cart_product_discount" id="_cart_product_discount-<?php echo $product->id; ?>" value="<?php echo $discount; ?>">
							<select name="_cart_product[<?php echo $product->id; ?>][type]">
								<option value="p"><?php echo esc_html__( '%', 'usam'); ?></option>
								<option value="f"><?php echo esc_html__( '-', 'usam'); ?></option>
							</select>	
						</span>	
					</td>
					<td class="column-price"><?php echo $product->price; ?></td>	
				<?php if ( $this->data['total_tax'] > 0 ) { ?>						
					<td class="column-tax"><?php echo $product->tax; ?></td>	
				<?php }	?>
					<td class="column-total"><?php echo $this->currency_display( $total ); ?></td>
					<td class='column-delete'>
						<input type="hidden" name="basket_products_ids[]" value="<?php echo $product->id; ?>"/>
						<?php
						if ( !$this->is_closed ) 
						{
							$action_url = $this->order_url_action( array( 'item_id' => $product->id ), 'delete_order_item' ); ?>
							<a class='button_delete' href='<?php echo $action_url; ?>'></a><?php
						}
						?>
					</td>	
				</tr>
			<?php			
			}
		}		
	}	
	
	public function get_format_discount( $discount )
	{
		$p = '';
		if ( $discount > 0 )
		{
			$p = round( $discount/ $this->data['order_basket']*100, 2 );
			$p = " ( -$p% )";
		}
		echo $this->currency_display( $discount ).$p;	
	}
			
	public function cart_total()
	{		
		?>	
		<tr class="usam_order_basket">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Стоимость товаров', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['order_basket']); ?></th>			
			<th></th>
		</tr>	
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Скидка', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->get_format_discount($this->data['order_basket_discount']); ?></th>				
			<th></th>
		</tr>	
		<tr class="usam_order_final_basket">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><strong><?php esc_html_e( 'Стоимость товаров с учетом скидки', 'usam' ); ?>:</strong></th>
			<th class = "order_total_value"><strong><?php echo $this->currency_display($this->data['order_final_basket']); ?></strong></th>			
			<th></th>
		</tr>		
		<tr>
			<td colspan="<?php echo $this->columns_count; ?>">
				<?php 
				if ( !empty($this->data['coupon_name']) )
				{ 
					$coupon = new USAM_Coupon( $this->data['coupon_name'], 'coupon_code' );
					$coupon_data = $coupon->get_data();
					if ( $coupon_data['action'] == 'b' )
						$message = " (".__('Изменить стоимость корзины','usam').")";
					elseif ( $coupon_data['action'] == 's' )
						$message = " (".__('Изменить стоимость доставки','usam').")";	
					else
						$message = '';
					?>
					<span class = "usam_display_data"><?php esc_html_e( 'Код купона', 'usam' ); ?>: <a href='<?php echo admin_url('admin.php?page=manage_discounts&tab=coupons&action=edit&id='.$coupon_data['id']); ?>'><?php echo $this->data['coupon_name']."</a>$message"; ?></span>
				<?php } ?>
				<p class = "usam_edit_data"><?php esc_html_e( 'Код купона', 'usam' ); ?>: <input size="20" type="text" name="_coupon_code_order" id="_coupon_code_order" value="<?php echo $this->data['coupon_name']; ?>"></p>
			</td>				
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Скидка по купону', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->get_format_discount($this->data['coupon_discount']); ?></th>
			<th></th>
		</tr>	
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Используемые бонусы', 'usam' ); ?>:</th>
			<th class = "order_total_value"><span class = "usam_display_data"><?php echo $this->get_format_discount($this->data['bonus']); ?></span><input size="10" type="text" name="_bonuses_order" class = "usam_edit_data" id="_bonuses_order" value="<?php echo $this->data['bonus']; ?>"></th>				
			<th></th>
		</tr>			
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Налог', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['total_tax']); ?></th>
			<th></th>
		</tr>		
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Доставка', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['shipping']); ?></th>
			<th></th>
		</tr>
		<tr class ="usam_order_price">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class='order_total_names usam_name_row'><strong><?php esc_html_e( 'Итог', 'usam' ); ?>:</strong></th>
			<th class='order_total_value usam_value_row'><strong><?php echo $this->currency_display($this->data['totalprice']); ?></strong></th>
			<th></th>
		</tr>			
		<?php
	}
		
	public function cart_items( $id )
	{			
		?>	
		<div class ="usam_order_type_price">
			<strong><?php esc_html_e( 'Цены:', 'usam' ); ?> </strong><span class ="usam_display_data"><?php echo usam_get_name_price_by_code( $this->data['type_price'] ); ?></span>
			<select class = "usam_edit_data" id = "order_type_price" name="type_price">
				<?php					
					$type_prices = usam_get_prices( array('type' => 'R') );
					foreach ( $type_prices as $value )
					{					
						if ( $this->data['type_price'] == $value['code'] )
							$selected = 'selected="selected"';
						else
							$selected = '';
						?><option value="<?php echo $value['code']; ?>" <?php echo $selected; ?>><?php echo $value['title']; ?></option><?php
					}				
				?>
			</select>
			<?php usam_loader(); ?>	
		</div>	
		<?
		$args = array( 'order_id' => $this->id );
		$order_discounts = usam_get_order_discounts( $args );
		if ( !empty($order_discounts))
		{
		?>
		<div id="order_discount" class = 'order_discount'>
			<strong><?php esc_html_e( 'Акции заказа', 'usam' ); ?></strong>
			<table>								
				<?php		
				$url = admin_url('admin.php?page=manage_discounts&tab=basket&action=edit');		
				foreach ( $order_discounts as $order_discount )
				{
					$discount_url = add_query_arg( array( 'id' => $order_discount->discount_id ), $url );	
					?>
					<tr>
						<td><a href='<?php echo $discount_url; ?>'><?php echo $order_discount->name; ?></a></td>
					</tr>
				<?php }	?>
			</table>								
		</div>	
		<?php
		}
		?>		
		<div id="usam_display_data" class = 'cart_order_box'>
			<?php $this->cart_items_table( ); ?>									
		</div>			
		<div id = "add_product_button_box">
			
		<div id="order-add-item" class="order-add-item">			
			<?php
			if ( !$this->is_closed ) 
			{
				$this->add_product_box();		
			}
			?>	
			</div>						
		</div>			
		<?php
	}
	
	
	private function cart_items_table( )
	{
		$columns = array(
		 'n'         => __( '№', 'usam' ),
		 'image'     => '',
		 'title'     => __( 'Имя', 'usam' ),
		 'sku'       => __( 'Артикул', 'usam' ),		 
		 'quantity'  => __( 'Количество','usam' ),	 
		 'old_price' => __( 'Цена', 'usam' ),
		 'discount'  => __( 'Скидка', 'usam' ).'<input size="4" type="text" name="_discount_cart" id="_discount_cart" value="" class = "usam_edit_data">',
		 'price'     => __( 'Цена со скидкой', 'usam' ),
		);		
	
		if ( $this->data['total_tax'] > 0 )
			$columns['tax'] = __( 'Налог', 'usam' );
		
		$columns['total'] = __( 'Всего', 'usam' );
		$columns['delete'] = '';
		
		register_column_headers( 'order_item_details', $columns );	
		$this->columns_count = count( $columns ) - 4;	
		
		?>		
		<div class="usam_table_container">
			<table class="usam_table order_products_table" cellspacing="0">
				<thead>
					<tr>
						<?php print_column_headers( 'order_item_details' ); ?>
					</tr>
				</thead>
				<tbody>
					<?php $this->product_table(); ?>				
				</tbody>
				<tfoot>
					<?php $this->cart_total(); ?>	
				</tfoot>			
			</table>
		</div>
		<?php
	}	
	
	public function create_notes()
	{		
		?><textarea name="purchlog_notes" rows="3" wrap="virtual" id ="purchlog_notes" style="width:100%;"><?php echo $this->data['notes']; ?></textarea><?php 		
	}	
	
	//Добавить товар в заказ
	public function add_product_box()
	{	
		if ( $this->type == 'new_item' )
		{
			$class = '';		
			$class_button = 'hide';	
		}
		else
		{
			$class = 'hide';
			$class_button = '';	
		}
		?>	
		<button id='show_block_add_products' type="button" class="button <?php echo $class_button; ?>" data-order_id = "<?php echo $this->id; ?>"><?php _e( 'Добавить товар', 'usam' ); ?></button>
		<?php usam_loader(); ?>	
		<div class = "item_add_box <?php echo $class; ?>">	
			<table>
				<thead>
					<tr>
						<td><?php _e('Выберите товары', 'usam'); ?>:</td>				
						<td><?php _e('Количество', 'usam'); ?>:</td>					
						<td><?php _e('Добавить в отгрузку', 'usam'); ?>:</td>
						<td><?php _e('Добавить в оплату', 'usam'); ?>:</td>
						<td></td>
					</tr>				
				</thead>
				<tbody>
					<tr id = "item_add">
						<td>
							<?php
								$autocomplete = new USAM_Autocomplete_Forms( );
								$autocomplete->get_form_product( null, array( 'query' => array( 'storage' => 0 ) ) );
							?>	
						</td> 					
						<td><input id = "select_item_quantity" type="text" size="6" name="quantity" value=""/></td>					
						<td>	
							<select id = "document_shipped" name="document_shipped">
								<?php
									$selected = '';
									$documents_shipped = $this->purchase_log->get_full_details_of_shipment();		
									foreach ( $documents_shipped as $document )
									{										
										?><option value="<?php echo $document['id']; ?>" <?php echo $selected; ?>><?php printf( __( 'В отгрузку №%s', 'usam' ), $document['id'] ); ?></option><?php
									}				
								?>
								<option value="0"><?php _e("Не добавлять","usam"); ?></option>
							</select>
						</td>
						<td>	
							<select id = "documents_payment" name="documents_payment">
								<?php
									$selected = '';
									$documents_payment = $this->purchase_log->get_payment_history();		
									foreach ( $documents_payment as $document )
									{										
										if (  $document['status'] == 1 )
										{
										?><option value="<?php echo $document['id']; ?>" <?php echo $selected; ?>><?php printf( __( 'В оплату №%s', 'usam' ), $document['id'] ); ?></option><?php
										}
									}				
								?>
								<option value="0"><?php _e("Не добавлять","usam"); ?></option>
							</select>
						</td>						
						<td>
							<button id = "add_order_item" type="button" data-order_id = "<?php echo $this->id; ?>" class="button"><?php _e('Добавить', 'usam'); ?></button>
							<?php usam_loader(); ?>	
							<button id = "close_block_add_products" type="button" class="button"><?php _e('Отменить', 'usam'); ?></button>
						</td>						
					</tr>
				</tbody>
			</table>
		</div>
	<?php	
	}		
		
	public function display_payment_documents()
    { 
		$payments = usam_get_payments( array('document_type' => 'order', 'document_id' => $this->id ) );				
		?>	
		<div id="usam_payment_documents_box" class="set_row_element">
			<?php 
			if ( empty($payments) )
			{
				?>
				<p class ="items_empty"><?php _e( 'Нет документов оплаты', 'usam' ); ?></p>
				<?php	
			}
			else
			{
				foreach($payments as $document)
				{
					$this->display_payment_document( $document->id );
				}
			}	 
			?>	
		</div>	
		<?php	
		if ( !$this->is_closed ) 
		{
			?>		
			<div class="button_box">			
				<button id='add_payment_button' type="button" class="button" data-id = "<?php echo $this->id; ?>"><?php _e( 'Добавить', 'usam' ); ?></button>
			</div>		
			<?php	
		}
	}	
	
	public function display_payment_document( $document_id = 0 )
	{		
		?>
		<div id="document_payment-<?php echo $document_id; ?>" class="usam_document_container" data-item-id="<?php echo $document_id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Приходный кассовый ордер', 'usam' ); ?> <span id="shipment_<?php echo $document_id ?>">#<?php echo $document_id; ?></span></div>
				<div class="usam_document_container-action-block">					
					<div class="usam_document_container-action"><a id = "bitton_delete" title='<?php _e( 'Удалить документ', 'usam' ); ?>'><?php _e( 'Удалить', 'usam' ); ?></a></div>
					<div class="usam_document_container-action"><a href="" id="button_edit"><?php _e( 'Редактировать', 'usam' ); ?></a></div>
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>			
			<?php	
			$args = array('document_type' => 'order', 'document_id' => $this->id);
			if ( $document_id == 0 )
				$args['sum'] = $this->data['totalprice'];
			
			require_once( USAM_FILE_PATH ."/admin/menu-page/orders/form/parish-form-payment.php");		
			require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
			
			$display_payment = new USAM_Form_Payment( $document_id );
			$display_payment->display_document();
			?>			
		</div>
		<?php
	}	
		
	public function display_shipped_documents()
    {
		$documents_shipped = $this->purchase_log->get_full_details_of_shipment();		
		?>	
		<div id="usam_shipped_documents_box" class="set_row_element">
			<?php 
			if ( empty($documents_shipped) )
			{
				?>
					<p class ="no_documents"><?php _e( 'Не было никаких отгрузок для этого заказа', 'usam' ); ?></p>
				<?php	
			}
			else
			{
				foreach($documents_shipped as $document)
				{
					$this->display_shipped_document( $document['id'] );
				}
			}	
			?>
		</div>
		<?php	
		if ( !$this->is_closed ) 
		{
			?>
			<div class="button_box">			
				<button id='add_shipped_button' type="button" class="button" data-id = "<?php echo $this->id; ?>"><?php _e( 'Добавить', 'usam' ); ?></button>
			</div>
			<?php		
		}
	}	
	
	
	public function display_shipped_document( $document_id = 0 )
	{
		?>
		<div id="document_shipped-<?php echo $document_id; ?>" class="usam_document_container" data-item-id="<?php echo $document_id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Отгрузка', 'usam' ); ?> <span id="shipment_19">#<?php echo $document_id; ?></span></div>
				<div class="usam_document_container-action-block">						
					<div class="usam_document_container-action"><a id = "bitton_delete" title='<?php _e( 'Удалить документ', 'usam' ); ?>'><?php _e( 'Удалить', 'usam' ); ?></a></div>
					<div class="usam_document_container-action"><a href="" id="button_edit"><?php _e( 'Редактировать', 'usam' ); ?></a></div>
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>
			<?php
			require_once( USAM_FILE_PATH ."/admin/menu-page/orders/form/edit-form-shipped.php");		
			require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
			
			$display_shipped = new USAM_Form_shipped( $document_id );
			$display_shipped->display_document();
			?>
		</div>
		<?php
	}	
	
	public function downloadable_files()
    {		
		$downloadable_files = $this->purchase_log->get_downloadable_files();	
		$downloads_locked = false;
		?>	
		<div class="usam_table_container">
		<table id = "downloadable_products_table" class="widefat usam_table" cellspacing="0">
			<thead>
				<tr>
					<th class="column_title"><?php _e( 'Имя товара', 'usam' ); ?></th>	
					<th class="column_available"><?php _e( 'Доступно', 'usam' ); ?></th>					
					<th class="column_ip"><?php _e( 'IP', 'usam' ); ?></th>
					<th class="column_status"><?php _e( 'Статус', 'usam' ); ?></th>
					<th class="column_date"><?php _e( 'Изменен', 'usam' ); ?></th>		
				</tr>
			</thead>
			<tbody>
				<?php				
				if ( empty($downloadable_files) )
				{
					?>
						<tr><td colspan = '6' class = "no-items"><?php _e( 'Нет загружаемых заказов', 'usam' ); ?> </td></tr>
					<?php	
				}
				else
				{						
					foreach($downloadable_files as $file)
					{						
						if ( $file->ip_number != '' && !$downloads_locked )
						{
							$downloads_locked = true;						
						}
						?>
						<tr data-item-id="<?php echo $file->id; ?>">
						
							<td><?php echo $file->file_name; ?></td>
							<td><span class = "usam_display_data"><?php echo $file->downloads; ?></span><input type="text" name="_downloadable_edit[<?php echo $file->id; ?>][downloads]" class = "usam_edit_data" value="<?php echo $file->downloads; ?>" size = '5' maxlength='5'></td>
							<td><?php echo $file->ip_number; ?></td>							
							<td><span class = "usam_display_data"><?php 
									switch ( $file->active ) 
									{
										case 0:											
											_e( 'Не доступно', 'usam' );
										break;
										case 1:										
											_e( 'Доступно', 'usam' );	
										break;	
									}														
								?></span>
								<div class = "usam_edit_data">
									<select class="chzn-select-nosearch-load" name = "_downloadable_edit[<?php echo $file->id; ?>][active]">
										<option value='0' <?php selected( $file->active, 0 ) ?>><?php _e( 'Не доступно', 'usam' ); ?></option>	
										<option value='1' <?php selected( $file->active, 1) ?>><?php _e( 'Доступно', 'usam' ); ?></option>										
									</select>
								</div>
							</td>							
							<td><?php echo usam_local_date( $file->date_modified ); ?></td>
						</tr>
					<?php					
					} 
				}
				?>	
			</tbody>			
		</table>
		</div>
		<div id="order-downloads" class="button_box">			
			<?php if ( $downloads_locked ): ?>
				<?php $action_url = $this->order_url_action( array( ), 'downloadable_file_clear_locks' ); ?>
				<img src='<?php echo USAM_CORE_IMAGES_URL; ?>/lock_open.png'/>&ensp;<a href='<?php echo $action_url; ?>'><?php _e( 'Разблокировать', 'usam' ); ?></a>
			<?php endif; ?>	
		</div>				
		<?php		
	}	
	
	protected function get_fastnav( ) 
	{											
		return array( 			
			'usam_order_products' => __( 'Товары', 'usam' ), 
			'usam_create_notes' => __( 'Заметки', 'usam' ),
			'usam_payment_history' => __( 'Оплаты', 'usam' ),
			'usam_shipped_products' => __( 'Отгрузки', 'usam' ),
			'usam_downloadable_files' => __( 'Загружаемые', 'usam' ),			
		);
	}
	
	public function display_actions( )
	{
		$payment_sum = $this->purchase_log->get_payment_status_sum();
		if ( $this->data['paid'] == 2 )
			$class = 'order_paid';
		elseif ( $this->data['paid'] == 1 )
			$class = 'order_partially_paid';
		else
			$class = 'order_is_not_paid';
		?>
		<div id = 'order-actions' class = "postbox">						
			<h3 class="hndle"><span><?php esc_html_e( 'Номер заказа', 'usam' ); ?>:</strong> № <?php echo $this->id; ?></span></h3>
			<div class="inside">	
			<table class='table_order_details'>							
				<tr><td><?php esc_html_e( 'Тип заказа', 'usam' ); ?>:</td><td><?php echo usam_get_order_type($this->data['order_type']); ?></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Дата', 'usam' ); ?>:</td><td><?php echo get_date_from_gmt( $this->data['date_insert'], 'd-m-Y H:i'); ?></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Сумма', 'usam' ); ?>:</td><td><strong><?php echo $this->currency_display($this->data['totalprice']); ?></strong></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Статус', 'usam' ); ?>:</td>
					<td>
					<select class="chzn-select-nosearch" data-log-id="<?php echo $this->id; ?>" name = "order_status">
						<?php									
						$order_statuses = usam_get_order_statuses();
						foreach ( $order_statuses as $status ) 
						{
							if ( $status->visibility || $status->internalname == $this->data['status'] )
							{												
								$style = $status->color != ''?'style="color:'.$status->color.'"':'';												
								?><option value='<?php echo $status->internalname; ?>' <?php echo $style; ?> <?php echo $this->data['status'] != $status->internalname && $this->data['paid'] != 2 && $status->internalname == 'closed'?'disabled':'' ?><?php selected($this->data['status'], $status->internalname); ?>><?php echo $status->name; ?></option><?php
							}
						}
						?>
					</select>	
					</td>
				</tr>
				<tr><td class="name"><?php esc_html_e( 'Не уведомлять', 'usam' ); ?>:</td><td><input type='checkbox' name='prevent_notification' value='1' id="usam_prevent_notification" /></td></tr>				
				<tr> 
				   <td><label for='order_user_role'><?php esc_html_e( 'Менеджер', 'usam' ); ?>:</label></td>
				   <td>
						<select class="chzn-select-nosearch-load" name = "order_manager" id = "order_field_manager">
							<option value="0" <?php echo ($this->data['manager_id'] == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
							<?php	
							$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator','editor'), 'fields' => array( 'ID','display_name') );
							$users = get_users( $args );
							foreach( $users as $user )
							{						
								?>               
								<option value="<?php echo $user->ID; ?>" <?php echo ($this->data['manager_id'] == $user->ID) ?'selected="selected"':''?> ><?php echo $user->display_name; ?></option>
								<?php
							}
							?>
						</select>				
					</td>			
				</tr>				
				<tr><td><?php esc_html_e( 'Статус оплаты', 'usam' ); ?>:</td><td><span class="order_payment_status <?php echo $class; ?>"><?php echo usam_get_order_payment_status_name( $this->data['paid'] ); ?></span></td></tr>
				<?php if ( !empty($this->data['date_paid']) ) {  ?>
					<tr><td><?php esc_html_e( 'Дата оплаты', 'usam' ); ?>:</td><td><?php echo get_date_from_gmt( $this->data['date_paid'], 'd-m-Y H:i'); ?><td></tr>
				<?php } ?>	
				<tr><td><?php esc_html_e( 'Оплачено', 'usam' ); ?>:</td><td><?php echo $this->currency_display($payment_sum['total_paid']); ?></td></tr>
				<?php if ( $this->data['paid'] != 2 ) {  ?>
					<tr><td><?php esc_html_e( 'Требуется оплата', 'usam' ); ?>:</td><td><?php echo $this->currency_display($payment_sum['payment_required']); ?></td></tr>		
				<?php } ?>	
				<?php if ( $payment_sum['payment_required'] < 0 ) {  ?>
					<tr><td><?php esc_html_e( 'Переплата', 'usam' ); ?>:</td><td><?php echo $this->currency_display($payment_sum['payment_required']); ?></td></tr>		
				<?php } ?>					
			</table>								
			</div>
			<div id="major-publishing-actions">
				<div class="div-actions">
					<?php $this->buttons(); ?>
				</div>
			</div>							
		</div>
		<?php
	}
		
	public function display_right( )
	{					
		if ( $this->data['status'] == 'canceled' )
		{
			?>
			<div id='usam_thiss_links' class = "postbox">
				<div class="handlediv" title="Щелкните чтобы переключить"></div>
				<h3 class="hndle"><span><?php _e( 'Причина отмены заказа', 'usam' ); ?></span></h3>
				<div class="inside">							
					<textarea class='cancellation_reason' name='cancellation_reason' rows='3' cols='40' ><?php echo $this->data['cancellation_reason']; ?></textarea>
				</div>
			</div>		
			<?php 	
		}		
	}
	
		/**
	 * Вывод конструкции заказа
	 */
	public function display_left( )
	{		
		if ( $this->data['modified_customer'] == 1 )
		{
			?>
			<div id="modified_customer">
				<h2>
					<?php printf( __( 'Обратите внимание заказ был изменен покупателем %s', 'usam' ), date_i18n( get_option('date_format'), strtotime($this->data['date_modified']) ) ); ?>
					<a href="#nav-usam_order_change_history"><?php _e( 'Посмотреть изменения', 'usam' ); ?></a>
					<span class="hide_modified_customer"><?php _e( 'Скрыть', 'usam' ); ?></span>
				</h2>
			</div>
			<?php
		}								
		usam_add_box( 'usam_create_notes', __( 'Заметки заказа', 'usam' ), array( $this, 'create_notes' ) );
		usam_add_box( 'usam_order_products', __('Заказанные позиции', 'usam'), array($this, 'cart_items'), array(), !$this->is_closed );
		usam_add_box( 'usam_downloadable_files', __('Загружаемые товары', 'usam'), array($this, 'downloadable_files'), array(), !$this->is_closed );	
		
		usam_add_box( 'usam_order_customer', __('Покупатель', 'usam'), array($this, 'customer') );							
	
		usam_add_box( 'usam_shipped_products', __('Отгрузки', 'usam'), array($this, 'display_shipped_documents') );
		usam_add_box( 'usam_payment_history', __('История оплаты', 'usam'), array( $this, 'display_payment_documents' ) );		
	}
}