<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Orders extends USAM_Edit_Form
{		
	private $payment_history;	

	private $type = 'item_details';
	private $purchase_log;	
	private $sale_basket;
	private $customer_data;
	
	private $purchitem;
	private $order_url;	

	private $is_closed = false;
	private $display = array();
	
	protected function get_data_tab(  )
	{			
		$this->order_url = usam_get_url_order( $this->id );
		$this->purchase_log = new USAM_Order( $this->id );	

		if ( !$this->purchase_log->exists() )	
		{
			$this->not_exist = true;
			return false;
		}	
		$this->data = $this->purchase_log->get_data();		
		
		usam_work_on_task( array( 'title' => sprintf(__('Работа на заказом %s','usam'), $this->id) ), array( 'object_type' => 'order', 'object_id' => $this->id ) );
		
		$this->data = $this->purchase_log->get_data();		
		
		$this->sale_basket = $this->purchase_log->get_order_products();	
		$calculated_data = $this->purchase_log->get_calculated_data();	

		$this->data = array_merge( $this->data,  $calculated_data );				
		$this->customer_data = wp_unslash($this->purchase_log->get_customer_data());			
		
		$this->is_closed = usam_check_order_is_completed( $this->data['status'] );		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );	
		add_action( 'admin_enqueue_scripts', array(&$this, 'print_scripts_style') );	
	}	
	
	protected function get_title_tab()
	{ 	
		if ( !$this->purchase_log->exists() )	
		{				
			$title = sprintf( __( 'Заказ №%s не существует', 'usam' ), $this->id );		
		}
		elseif ( $this->id != null )
			$title = sprintf( __('Подробная информация по заказу %s','usam'), '<span class="number">#'.$this->data['id'].'</span> <span class="subtitle_date">'.esc_html__( 'от', 'usam' ).' '.date_i18n( get_option('date_format'),  strtotime($this->data['date_insert']) ).'</span>');
		else
			$title = __('Добавить новый заказ', 'usam');	
		return $title;
	}
	
	protected function toolbar_buttons( ) 
	{
		if ( $this->id != null )
		{
			global $current_screen;		
			$delete_url = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'delete' )), 'usam-'.$current_screen->id);
			$edit_url = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'edit' )), 'usam-'.$current_screen->id);
			
			$printed_form = array( 
				array( 'form' => 'order', 'title' => esc_html__( 'Заказ', 'usam' ) ), 
				array( 'form' => 'purchase_receipt', 'title' => esc_html__( 'Товарный чек', 'usam' ) ), 
				array( 'form' => 'storage_list', 'title' => esc_html__( 'Складской лист', 'usam' ) ), 			
				array( 'form' => 'waybill', 'title' => esc_html__( 'Накладная ТОРГ-12', 'usam' ) ), 				
			);					
			$printed_form = apply_filters( 'usam_printed_form_order', $printed_form );			
			?>
			<ul>
				<li><a href="<?php echo $delete_url; ?>" class="button"><?php _e('Удалить','usam'); ?></a></li>
				<li><a href="<?php echo $edit_url; ?>" class="button"><?php _e('Изменить','usam'); ?></a></li>		
				<li><div class = "usam_menu">
						<div class="menu_name button"><img src='<?php echo USAM_CORE_IMAGES_URL; ?>/printer.png'/>&ensp;<?php _e('Печать','usam'); ?></div>
						<div class="menu_content menu_content_form">
							<span class="popup-menu-angle"></span>
							<ul>
							<?php 		
							foreach ( $printed_form as $link )
							{		
								?>		
								<li><a href='<?php echo usam_url_admin_action( 'printed_form', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) ); ?>' target="_blank"><?php echo $link['title']; ?></a></li>
								<?php				
							}		
							?>	
							</ul>
						</div>
					</div>
			</li>	
			<li>
				<div class = "usam_menu">
					<div class="menu_name button"><img src='<?php echo USAM_CORE_IMAGES_URL; ?>/pdf_download.png'/>&ensp;<?php _e('PDF','usam'); ?></div>
					<div class="menu_content menu_content_form">
						<span class="popup-menu-angle"></span>
						<ul>
						<?php 		
						foreach ( $printed_form as $link )
						{		
							?>		
							<li><a href='<?php echo usam_url_admin_action( 'printed_form_to_pdf', admin_url('admin.php'), array( 'form' => $link['form'], 'id' => $this->id ) ); ?>' target="_blank"><?php echo $link['title']; ?></a></li>
							<?php				
						}		
						?>	
						</ul>
					</div>
				</div>
			</li>			
			</ul>
			<?php		
		}
	}
	
	function print_scripts_style()
	{
		wp_enqueue_script( 'usam-edit-order', USAM_URL.'/admin/js/form-display/edit-order.js', array( 'jquery' ), USAM_VERSION );			
		$data = array(					
			'order_id'                               => $this->id,			
			'change_purchase_log_item_status_nonce'  => usam_create_ajax_nonce( 'change_purchase_log_item_status' ),
			'add_order_item_nonce'                   => usam_create_ajax_nonce( 'add_order_item' ),
			'recalculate_prices_nonce'               => usam_create_ajax_nonce( 'recalculate_prices' ),
			'add_product_box_nonce'                  => usam_create_ajax_nonce( 'add_product_box' ),
			'delete_order_item_nonce'                => usam_create_ajax_nonce( 'delete_order_item' ),
			'add_payment_nonce'                      => usam_create_ajax_nonce( 'add_payment' ),		
			'purchaselog_message_add_payment'        => __( 'Введите номер', 'usam'),
			'delete_payment_item_nonce'              => usam_create_ajax_nonce( 'delete_payment_item' ),
			'add_shipped_product_nonce'              => usam_create_ajax_nonce( 'add_shipped_product' ),		
			'send_tracking_email_nonce'              => usam_create_ajax_nonce( 'send_tracking_email' ),
			'billing_same_as_shipping_nonce'         => usam_create_ajax_nonce( 'billing_same_as_shipping' ),
			'purchaselog_message_and_id'             => __( 'Добавить номер', 'usam'),				
			'add_document_payment_nonce'             => usam_create_ajax_nonce( 'add_document_payment' ),
			'add_document_shipped_nonce'             => usam_create_ajax_nonce( 'add_document_shipped' ),
			'upload_customer_data_in_order_nonce'    => usam_create_ajax_nonce( 'upload_customer_data_in_order' ),					
			'sending_message'                        => __( 'отправка...', 'usam' ),
			'delete_order_item_message'              => __( "Вы собираетесь удалить позицию из заказа.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'delete_payment_item_message'            => __( "Вы собираетесь удалить способ оплаты.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'delete_shipped_item_message'            => __( "Вы собираетесь удалить способ доставки.\n Нажмите 'Отмена', чтобы остановить удаление или 'OK', чтобы удалить.",'usam'),
			'sent_message'                           => __( 'Письмо отправлено!', 'usam' ),			
		);	
		wp_localize_script( 'usam-edit-order', 'USAM_Edit_Order', $data );	
		wp_enqueue_style('usam-order-admin'); 
	}
	
	function admin_footer()
	{	
		$html = "			
			<div class='action_buttons'>				
				<button type='button' class='action_confirm button-primary button' data-dismiss='modal' aria-hidden='true'>".__( 'Удалить', 'usam' )."</button>
				<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
			</div>";	
		echo usam_get_modal_window( __('Подтвердите','usam'), 'operation_confirm', $html, 'small' );
		echo usam_get_modal_window( __('Отправить сообщение','usam'), 'send_mail', $this->get_form_send_message() );
		echo usam_get_modal_window( __('Отправить сообщение','usam'), 'form_send_sms', $this->get_form_send_sms() );
	}	

	function get_form_send_message( )
	{	
		$list_properties = usam_get_order_properties( );		
		$to_email = array();
		foreach ( $list_properties as $property )
		{	 
			if ( $property->type == 'email' && !empty($this->customer_data[$property->unique_name]['value']) )
			{
				$to_email[] = $this->customer_data[$property->unique_name]['value'];
			}
		}			
		if ( usam_is_type_payer_company($this->data['type_payer']) ) 		
			$customer_type = 'company';		
		else
			$customer_type = 'contact';
		
		$args = array( 'object_id' => $this->data['id'], 'object_type' => 'order', 'customer_type' => $customer_type, 'customer_id' => $this->data['customer_id'], 'to_email' => $to_email, 'subject' => sprintf( __( 'Сообщение о вашем заказе №%s','usam' ),$this->data['id'])." - ".get_bloginfo('name'), 		
		'insert_text' => array( 
			'_order_id' => __( 'Номер заказа','usam'), 
			'_readiness_date' => __( 'Дата готовности','usam'), 
			'_storage_pickup' => __( 'Офис получения','usam'), 
			'_storage_pickup_address' => __( 'Адрес склада отгрузки','usam') ,
			'_storage_pickup_phone' => __( 'Телефон склада отгрузки','usam'),
			'_date_allow_delivery' => __( 'Дата и время доставки','usam')  
		) );		 
		return usam_get_form_send_message( $args );	
	}	
	
	function get_form_send_sms( )
	{	
		global $user_ID;						
		
		$list_properties = usam_get_order_properties( );
		
		$out  = "<div class='mailing'>";	
		$out .= '<form method="post" action="'.usam_url_admin_action( 'send_sms', admin_url('admin.php') ).'">';		
		$out .= "<input type='hidden' name='object_id' value='$this->id' />";
		$out .= "<input type='hidden' name='object_type' value='order' />";
		$out .= "<div class='mailing-mailing_wrapper'>";
		$out .= "<table class ='widefat'>";
		$out .= "<tr>
					<td>".__('Кому', 'usam')."</td>
					<td><select name = 'phone'>
					";
					foreach ( $list_properties as $property )
					{	 
						if ( $property->type == 'mobile_phone' && !empty($this->customer_data[$property->unique_name]['value']) )
						{
							$out .= "<option value='".$this->customer_data[$property->unique_name]['value']."'>".$this->customer_data[$property->unique_name]['value']."</option>";
						}
					}						
				$out .= "</select></td></tr>							
				<tr>
					<td colspan='2'><textarea rows='10' autocomplete='off' cols='40' name='message' id='message_editor' ></textarea></td>						
				</tr>
			</table>";		
		$out .= "</div>";
		$out .= '<div class="popButton">'.get_submit_button( __( 'Отправить','usam' ), 'primary','action_send_email', false, array( 'id' => 'send-email-submit' ) ).'</div>';	
		$out .= '</form>';
		$out .= "</div>"; 
		return $out;
	}	
		
	function order_url_action( $arg, $action )
	{
		return usam_url_admin_action( $action, $this->order_url, $arg );
	}
	
	function currency_display( $price ) 
	{				
		$args['decimal_point'] = true;
		$args['currency'] = usam_get_currency_price_by_code($this->data['type_price'] );
		return usam_currency_display( $price, $args );
	}	
	
	private function order_info()
	{	
		?>	
		<div id='usam_details_order_box' class = "usam_details_box">
			<div id='general_information_order' class = "usam_details usam_details_border">
				<h3><?php esc_html_e( 'Основная информация', 'usam' ); ?></h3>
				<?php $this->details_info_order(); ?>									
			</div>	
		</div>
		<div id='usam_details_order_box' class = "usam_details_box">
			<div id='general_information_order' class = "usam_details usam_details_border">
				<h3><?php esc_html_e( 'Финансовая информация', 'usam' ); ?></h3>
				<?php $this->financial_information(); ?>									
			</div>	
		</div>
		<div id='usam_details_order_box' class = "usam_details_box">
			<div id='general_information_order' class = "usam_details">
				<h3><?php esc_html_e( 'Детали заказа', 'usam' ); ?></h3>
				<?php $this->details_order(); ?>									
			</div>	
		</div>	
		<?php	
	}							
	
	private function details_info_order()
	{	
		?>						
		<table class = "table_basic_information_order">				
			<tr>
				<td><strong><?php esc_html_e( 'Статус заказа', 'usam' ); ?>:</strong></td><td><?php echo usam_get_order_status_name($this->data['status']); ?></td>
			</tr>
			<tr>
				<td><strong><?php esc_html_e( 'Стоимость заказа', 'usam' ); ?>:</strong></td><td><?php echo $this->currency_display($this->data['totalprice']); ?></td>
			</tr>
			<tr>
				<td><strong><?php esc_html_e( 'Количество товаров', 'usam' ); ?>:</strong></td><td><?php echo count($this->sale_basket); ?></td>
			</tr>			
			<tr>
				<td><strong><?php esc_html_e( 'Вес заказа', 'usam' ); ?>:</strong></td><td><?php echo $this->data['weight']; ?></td>
			</tr>	
			<tr>
				<td><strong><?php esc_html_e( 'Объем заказа', 'usam' ); ?>:</strong></td><td><?php echo $this->data['volume']; ?></td>
			</tr>				
		</table>		
		<?php	
	}
	
	private function financial_information()
	{	
		$payment_sum = $this->purchase_log->get_payment_status_sum();
		if ( $this->data['paid'] == 2 )
			$class = 'order_paid';
		elseif ( $this->data['paid'] == 1 )
			$class = 'order_partially_paid';
		else
			$class = 'order_is_not_paid';
		
		?>						
		<table class = "table_basic_information_order">			
			<tr>
				<td><strong><?php esc_html_e( 'Статус оплаты', 'usam' ); ?>:</strong></td><td><span class="order_payment_status <?php echo $class; ?>"><?php echo usam_get_order_payment_status_name( $this->data['paid'] ); ?></span></td>
			</tr>
			<?php if ( !empty($this->data['date_paid']) ) {  ?>
				<tr><td><strong><?php esc_html_e( 'Дата оплаты', 'usam' ); ?>:</strong></td><td><?php echo get_date_from_gmt( $this->data['date_paid'], 'd-m-Y H:i'); ?><td></tr>
			<?php } ?>	
			<tr>
				<td><strong><?php esc_html_e( 'Оплачено', 'usam' ); ?>:</strong></td><td><?php echo $this->currency_display($payment_sum['total_paid']); ?></td>
			</tr>
			<tr>
				<td><strong><?php esc_html_e( 'Требуется оплата', 'usam' ); ?>:</strong></td><td><?php echo $this->currency_display($payment_sum['payment_required']); ?></td>
			</tr>			
			<?php 
			if ( $this->data['paid'] != 2 ) 
			{ 		
				?>		
				<tr>
					<td><strong><?php esc_html_e( 'Можно оплатить на сумму', 'usam' ); ?>:</strong></td><td><?php echo $this->currency_display($payment_sum['can_be_paid']); ?></td>
				</tr>
			<?php } ?>				
		</table>		
		<?php	
	}
	
	private function details_order()
	{	
		?>	
		<div id='details_order'>					
			<table class = "table_basic_information_order">					
				<tr>
					<td><strong><?php esc_html_e( 'Тип заказа', 'usam' ); ?>:</strong></td><td><?php echo usam_get_order_type($this->data['order_type']); ?></td>
				</tr>
				<tr>
					<td><strong><?php esc_html_e( 'Менеджер', 'usam' ); ?>:</strong></td><td><?php echo usam_get_manager_name( $this->data['manager_id'] ); ?></td>
				</tr>	
				<?php 
				if ( $this->data['cancellation_reason'] != '' ) 
				{ 		
				?>	
				<tr>
					<td><strong><?php esc_html_e( 'Причина отказа', 'usam' ); ?>:</strong></td><td><?php echo $this->data['cancellation_reason']; ?></td>
				</tr>	
				<?php 
				} 		
				?>					
			</table>										
		</div>		
		<?php	
	}
	
	function location_list_by_code( $property, $value ) 
	{			
		$code = str_replace("shipping", "", $property->unique_name);
		$code = str_replace("billing", "", $code);	
		$code = str_replace("company_", "", $code);	
		
		$locations = usam_get_locations_by_code( $code );			
		$output = "<select id ='$property->unique_name' name='collected_data[$property->id]' data-props_id = '$property->id' class = 'location_list chzn-select' data-placeholder='".__('Выберете из списка...','usam')."'>\n\r 
		<option value=''>--".__('Не выбрано','usam')."--</option>";	
		foreach ( $locations as $item )
		{			
			$output .= "<option value='".$item->id."' ".selected($value, $item->id, false).">".esc_html( $item->name )."</option>\n\r";
		}
		$output .= "</select>\n\r";		
		return $output;
	}
	
	function location_list( $code, $value ) 
	{			
		$locations = usam_get_locations_by_code( $code );		
		$output = "<select id = 'location-$code' name='location_list[$code]' class = 'chzn-select' data-placeholder='".__('Выберете из списка...','usam')."'>\n\r 
		<option value=''>--".__('Не выбрано','usam')."--</option>";	
		foreach ( $locations as $item )
		{
			if ( $value == $item->id )
				$selected = "selected='selected'";
			else
				$selected = "";				
			$output .= "<option value='".$item->id."' $selected>".esc_html( $item->name )."</option>\n\r";
		}
		$output .= "</select>\n\r";		
		return $output;
	}
	
	private function display_details_properties_order( $properties_order )
	{		
		$row = '';		
		foreach( $properties_order as $property )			
		{
			if ( $property->active || !empty($this->customer_data[$property->unique_name]['value']) )
			{
				$display = isset($this->customer_data[$property->unique_name]['value'])? usam_get_display_order_property( $this->customer_data[$property->unique_name]['value'], $property->type ):__('Нет данных','usam');
				$row .= '<tr><td class="name"><strong>'.$property->name.':</strong></td><td><span id = "item-'.$property->unique_name.'">'.$display.'</span></td></tr>';	
			}
		}		
		$out = "<table class = 'subtab-detail-content-table'>";
		if ( $row != '' )
			$out .= $row;
		else
			$out .= "<tr><td>".__('Нет данных','usam')."</td></tr>";		
		$out .= "</table>";
		return $out;
	}
	   
	private function display_crm_customer( $customer )
	{	
		if ( empty($customer) )
			return false;
		
		$image_attributes = wp_get_attachment_image_src( $customer['thumbnail'], array(100, 100) );						
		if ( empty($image_attributes[0]) )	
			$thumbnail = USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif';			
		else
			$thumbnail = $image_attributes[0];	
		?>				
		<table class ="crm_customer_table">
			<tr>
				<td><strong><?php esc_html_e( 'CRM контакт:', 'usam' ); ?></strong></td>
				<td><img src="<?php echo esc_url( $thumbnail ); ?>" alt=""></td>
				<td><p><a href='<?php echo $customer['link']; ?>' target="_blank"><span class="customer_name" ><?php echo $customer['name']; ?></span></a></p>						
					<p class ="communications">
					<?php 
					if ( !empty($customer['email']) )
					{
						$emails = array();
						foreach ( $customer['email'] as $email ) 
							$emails[] = $email['value'];	
							
						echo __( 'E-mail', 'usam' ).': '.implode(', ',$emails);									
					}
					?>				
					<?php 
					if ( !empty($customer['phone']) )
					{
						$phones = array();
						foreach ( $customer['phone'] as $phone ) 
							$phones[] = $phone['value'];		
							
						echo __( 'т.', 'usam' ).': '.implode(', ',$phones);		
					}
					?>						
					</p>
				</td>
			</tr>
		</table>
		<?php 	
	}
	
	public function customer( )
	{	
		$user = get_user_by('id', $this->data['user_ID']);
		if ( isset($user->user_login) )
		{
			$user_login = $user->user_login;		
			if ( !empty($user->first_name) || !empty($user->first_name) )
				$user_login .= " ($user->first_name $user->last_name)";						
		}
		else
		{
			$user_login = __('Гость','usam');				
		}		
		$name_type_payer = usam_get_name_payer( $this->data['type_payer'] );
		?>
		<div class = "crm_customer">				
				<?php 						
				if ( !empty($this->data['customer_id']) ) 
				{ 					
					if ( usam_is_type_payer_company($this->data['type_payer']) ) 
					{ 
						$customer = usam_get_company( $this->data['customer_id'] );			
						if ( !empty($customer) )
						{
							$customer['link'] = admin_url("admin.php?page=crm&tab=company&action=edit&id=".$customer['id']);	
							$customer['thumbnail'] = $customer['logo'];
						}
					}	
					else					
					{
						$customer = usam_get_contact( $this->data['customer_id'] );
						if ( !empty($customer) )
						{
							$customer['thumbnail'] = $customer['foto'];
							$customer['link'] = admin_url("admin.php?page=crm&tab=contacts&action=view&id=".$customer['id']);
							$customer['name'] = $customer['lastname'].' '.$customer['firstname'].' '.$customer['patronymic'];						
						}
					}					
					$this->display_crm_customer( $customer );					
				} 
			?>
		</div>
		<div class = "usam_details_box">	
			<table class = 'subtab-detail-content-table'>			
				<tr> 
					<td class="name"><strong><?php esc_html_e( 'Логин покупателя:', 'usam' ); ?> </strong></td><td><a href="<?php echo  admin_url("user-edit.php?user_id=".$this->data['user_ID']); ?>"><?php echo $user_login; ?></a></td>
				</tr> 
				<tr> 
					<td class="name"><strong><?php esc_html_e( 'Тип плательщика:', 'usam' ); ?> </strong></td><td><?php echo $name_type_payer; ?></td>
				</tr> 
			</table>						
			<?php 
			$list_properties = usam_get_order_properties( );		
			$order_props_group = usam_get_order_props_groups( );	

			$select_type_payer_data = array( );	
			$others_type_payer_data = array( );			
			foreach( $order_props_group as $group )
			{							
				if ( $this->data['type_payer'] == $group->type_payer_id )
				{			
					foreach( $list_properties as $data )			
					{ 							
						if ( $data->group == $group->id )
						{
							$select_type_payer_data[$group->id][$data->unique_name] = $data;
						}
					}
				}
				else
				{
					foreach( $list_properties as $data )			
					{ 			
						if ( $data->group == $group->id && !empty($this->customer_data[$data->unique_name]) )
						{						
							$others_type_payer_data[$group->id][$data->unique_name] = $data;
						}
					}
				}
			}								
			foreach( $order_props_group as $group )
			{							
				if ( $this->data['type_payer'] == $group->type_payer_id && !empty($select_type_payer_data[$group->id]) )
				{								
					$display_details = $this->display_details_properties_order( $select_type_payer_data[$group->id] );				
					?>
					<div id='order_props_group-<?php echo $group->id; ?>' class = "usam_details usam_details_border">							
						<div class="usam_container-table-container caption border">
							<div class="usam_container-table-caption-title"><?php echo $group->name; ?></div>							
							<div id='order_props_group-view'><?php echo $display_details; ?></div>										
						</div>		
					</div>								
					<?php 						
				}
			}
			if ( !empty($others_type_payer_data) )
			{
				?>
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Другие заполненные группы', 'usam' ); ?></div>
				<?php		
				foreach( $order_props_group as $group )
				{			
					if ( $this->data['type_payer'] != $group->type_payer_id && !empty($others_type_payer_data[$group->id]) )
					{							
						$display_details = $this->display_details_properties_order( $others_type_payer_data[$group->id] );				
						?>	
						<div id='order_props_group-<?php echo $group->id; ?>' class = "usam_details usam_details_border">
							<h3><?php echo $group->name; ?></h3>							
							<div id='order_props_group-view'><?php echo $display_details; ?></div>											
						</div>													
						<?php 						
					}
				}
				?>					
				</div>															
				<?php 		
			}
			?>					
		</div>													
		<?php 		
		if ( $this->type != 'item_details' ) 
		{ 
		?>
		<p>
			<button class="button load_customer_details" data-user_id = "<?php echo $this->data['user_ID']; ?>"><?php esc_html_e( 'Загрузить', 'usam' ); ?></button>
		</p>
		<?php 	
		}		
	}
	
//Вывод таблицы с заказанными товарами
	private function product_table( )
	{		
		if ( empty($this->sale_basket) )
		{
			?>
				<tr><td colspan = '7' class = "cart_items_empty"><?php _e( 'Нет товаров в этом заказе', 'usam' ); ?> </td></tr>
			<?php	
		}
		else
		{	
			$sendback = add_query_arg( array( 'action' => 'view', 'page' => 'orders', 'screen' => 'product', 'order_id' => $this->id ), admin_url('admin.php') );
			$discounts = usam_get_disconts_products_order( array( 'order_id' => $this->id ) );
			$order_discounts = array();
			foreach ( $discounts as $discount ) 
			{
				$order_discounts[$discount->product_id][] = $discount;
			}
			foreach ( $this->sale_basket as $key => $product ) 
			{
				$total = ( $product->price + $product->tax ) * $product->quantity;			
				$discount = $product->old_price>0?round(100 - $product->price*100/$product->old_price,2):0;					
				?>
				<tr id ="product_<?php echo $product->id; ?>" data-item-id="<?php echo $product->id; ?>" >					
					<td class="column-n"><?php echo $key+1; ?></td>
					<td class="column-image">
						<?php echo usam_get_product_thumbnail( $product->product_id, 'manage-products' ); ?>
					</td>
					<td class="column-title">
						<a id = "details_product" href="<?php echo get_edit_post_link($product->product_id); ?>"><?php echo $product->name; ?></a>
						<?php 						
						if ( $product->product_day )
						{
							$discount_url = add_query_arg( array( 'id' => $product->product_day ), admin_url('admin.php?page=manage_discounts&tab=product_day&action=edit') );	
							?>
							<div class = 'product_order_discount'>							
								<p><a href='<?php echo $discount_url; ?>'><strong><?php esc_html_e( 'Товар дня', 'usam' ); ?></strong></a></p>						
							</div>		
						<?php }	?>	
						<?php 						
						if ( !empty($order_discounts[$product->product_id]))
						{
						?>
						<div class = 'order_discount product_order_discount'>
							<strong><?php esc_html_e( 'Акции на товар', 'usam' ); ?></strong>
							<table>								
								<?php		
								$url = admin_url('admin.php?page=manage_discounts&tab=discount&action=edit');								
								foreach ( $order_discounts[$product->product_id] as $order_discount )
								{									
									$discount_url = add_query_arg( array( 'id' => $order_discount->discount_id ), $url );	
									?>
									<tr>
										<td><a href='<?php echo $discount_url; ?>'><?php echo $order_discount->name; ?></a></td>
									</tr>
							<?php }	?>
							</table>								
						</div>		
						<?php }	?>												
					</td>
					<td class="column-sku"><?php echo usam_get_product_meta( $product->product_id, 'sku', true ); ?></td>					
					<td class="column-quantity"><?php echo $product->quantity; ?></td>
					<td class ="column-old_price"><?php echo $product->old_price; ?></td>
					<td class="column-discount"><?php echo $discount; ?>%</td>
					<td class="column-price"><?php echo $product->price; ?></td>	
				<?php if ( $this->data['total_tax'] > 0 ) { ?>						
					<td class="column-tax"><?php echo $product->tax; ?></td>	
				<?php }	?>
					<td class="column-total"><?php echo $this->currency_display( $total ); ?></td>
				</tr>
			<?php			
			}
		}		
	}	
	
	public function get_format_discount( $discount )
	{
		$p = '';
		if ( $discount > 0 )
		{
			$p = round( $discount/ $this->data['order_basket']*100, 2 );
			$p = " ( -$p% )";
		}
		echo $this->currency_display( $discount ).$p;	
	}
			
	public function cart_total()
	{		
		?>	
		<tr class="usam_order_basket">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Стоимость товаров', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['order_basket']); ?></th>	
		</tr>	
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Скидка', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->get_format_discount($this->data['order_basket_discount']); ?></th>	
		</tr>	
		<tr class="usam_order_final_basket">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><strong><?php esc_html_e( 'Стоимость товаров с учетом скидки', 'usam' ); ?>:</strong></th>
			<th class = "order_total_value"><strong><?php echo $this->currency_display($this->data['order_final_basket']); ?></strong></th>	
		</tr>		
		<tr>
			<td colspan="<?php echo $this->columns_count; ?>">
				<?php 
				if ( !empty($this->data['coupon_name']) )
				{ 
					$coupon = new USAM_Coupon( $this->data['coupon_name'], 'coupon_code' );
					$coupon_data = $coupon->get_data();
					if ( $coupon_data['action'] == 'b' )
						$message = " (".__('Изменить стоимость корзины','usam').")";
					elseif ( $coupon_data['action'] == 's' )
						$message = " (".__('Изменить стоимость доставки','usam').")";	
					else
						$message = '';
					?>
					<?php esc_html_e( 'Код купона', 'usam' ); ?>: <a href='<?php echo admin_url('admin.php?page=manage_discounts&tab=coupons&action=edit&id='.$coupon_data['id']); ?>'><?php echo $this->data['coupon_name']."</a>$message"; ?>
				<?php } ?>				
			</td>				
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Скидка по купону', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->get_format_discount($this->data['coupon_discount']); ?></th>
		</tr>	
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Используемые бонусы', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->get_format_discount($this->data['bonus']); ?></th>	
		</tr>			
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Налог', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['total_tax']); ?></th>
		</tr>		
		<tr>
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class = "order_total_names"><?php esc_html_e( 'Доставка', 'usam' ); ?>:</th>
			<th class = "order_total_value"><?php echo $this->currency_display($this->data['shipping']); ?></th>
		</tr>
		<tr class ="usam_order_price">
			<td colspan='<?php echo $this->columns_count; ?>'></td>
			<th colspan='2' class='order_total_names usam_name_row'><strong><?php esc_html_e( 'Итог', 'usam' ); ?>:</strong></th>
			<th class='order_total_value usam_value_row'><strong><?php echo $this->currency_display($this->data['totalprice']); ?></strong></th>
		</tr>			
		<?php
	}
		
	public function cart_items( $id )
	{			
		?>	
		<div class ="usam_order_type_price">
			<strong><?php esc_html_e( 'Цены:', 'usam' ); ?> </strong><span class ="usam_display_data"><?php echo usam_get_name_price_by_code( $this->data['type_price'] ); ?></span>
		</div>	
		<?php
		$args = array( 'order_id' => $this->id );
		$order_discounts = usam_get_order_discounts( $args );
		if ( !empty($order_discounts))
		{
		?>
		<div id="order_discount" class = 'order_discount'>
			<strong><?php esc_html_e( 'Акции заказа', 'usam' ); ?></strong>
			<table>								
				<?php						
				$url = admin_url('admin.php?page=manage_discounts&tab=basket&action=edit');
				foreach ( $order_discounts as $order_discount )
				{
					$discount_url = add_query_arg( array( 'id' => $order_discount->discount_id ), $url );	
					?>
					<tr>
						<td><a href='<?php echo $discount_url; ?>'><?php echo $order_discount->name; ?></a></td>
					</tr>
				<?php }	?>
			</table>								
		</div>	
		<?php
		}
		?>		
		<div id="usam_display_data" class = 'cart_order_box'>
			<?php $this->cart_items_table( ); ?>									
		</div>		
		<?php
	}
	
	
	private function cart_items_table( )
	{
		$columns = array(
		 'n'         => __( '№', 'usam' ),
		 'image'     => '',
		 'title'     => __( 'Имя', 'usam' ),
		 'sku'       => __( 'Артикул', 'usam' ),		 
		 'quantity'  => __( 'Количество','usam' ),	 
		 'old_price' => __( 'Цена', 'usam' ),
		 'discount'  => __( 'Скидка', 'usam' ),
		 'price'     => __( 'Цена со скидкой', 'usam' ),
		);		
	
		if ( $this->data['total_tax'] > 0 )
			$columns['tax'] = __( 'Налог', 'usam' );
		
		$columns['total'] = __( 'Всего', 'usam' );
		
		register_column_headers( 'order_item_details', $columns );	
		$this->columns_count = count( $columns ) - 3;	
		
		?>		
		<div class="usam_table_container">
			<table class="usam_table order_products_table" cellspacing="0">
				<thead>
					<tr>
						<?php print_column_headers( 'order_item_details' ); ?>
					</tr>
				</thead>
				<tbody>
					<?php $this->product_table(); ?>				
				</tbody>
				<tfoot>
					<?php $this->cart_total(); ?>	
				</tfoot>			
			</table>
		</div>
		<?php
	}	
	
	
	//Кнопки внизу экрана заказа
	private function thiss_links()
	{
		$links = array( 
			array( 'action_url' => $this->order_url_action( array( ), 'recalculate' ), 'title' => esc_html__( 'Пересчитать заказ', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/calculator_16x16.png' ), 			
			array( 'action_url' => $this->order_url_action( array( ), 'order_copy' ), 'title' => esc_html__( 'Копировать заказ', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/plus_icon.jpg' ), 
			array( 'action_url' => $this->order_url_action( array( ), 'order_resend_email' ), 'title' => esc_html__( 'Отправить на почту', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ), 
			array( 'action_url' => $this->order_url_action( array( ), 'motify_order_status_mail' ), 'title' => esc_html__( 'Уведомить о статусе на почту', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ), 
			array( 'action_url' => $this->order_url_action( array( ), 'motify_order_status_sms' ), 'title' => esc_html__( 'Уведомить о статусе в смс', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/email_go.png' ),
		);		
		if ( get_option('usam_ftp_settings') )
		{	
			$links[] = array( 'action_url' => $this->order_url_action( array( ), 'export_order_ftp' ), 'title' => esc_html__( 'Выгрузить на FTP чек', 'usam' ), 'icon_url' => USAM_CORE_IMAGES_URL.'/download.gif' );
		}
		$links = apply_filters( 'usam_thiss_links_order', $links );		
		?>		
		<div id = "actions_buttom">
			<ul>
				<?php 
				do_action( 'usam_this_links_start' ); 
				foreach ( $links as $link )
				{					
					$target = '';
					if ( !empty($link['target']) )
						$target = 'target="'.$link['target'].'"';
					?>		
					<li><img src='<?php echo $link['icon_url']; ?>' alt='<?php echo $link['title']; ?>' />&ensp;<a href='<?php echo $link['action_url']; ?>' <?php echo $target; ?>><?php echo $link['title']; ?></a></li>
					<?php				
				}		
				?>	
			</ul>
		</div>		
	<?php		
	}		
	
	public function create_notes()
	{		
		echo $this->data['notes'];
	}
	
	public function suggest_customer()
	{			
		global $wpdb, $type_price;
		$customer_product = array( 'usam_desired_product' => __('Избранное', 'usam'), 'history_views_product' => __('Просмотренное', 'usam') );		
		?>
		<div id='price_tab' class = "usam_tabs usam_tabs_style1 price_tab">
			<div class='header_tab'>
				<ul>
				<?php 			
				foreach ( $customer_product as $key => $title )
				{		
					echo "<li class='tab'><a href='#tab-".$key."'>".$title."</a></li>";	
				}				
				?>
				</ul>
			</div>
			<div class='countent_tabs'>
				<?php 						
				foreach ( $customer_product as $key => $title )
				{						
					$profile = usam_get_all_customer_meta( $this->data['user_ID'] );	
					if ( isset($profile[$key]) )
						$details = maybe_unserialize( $profile[$key] );	
					else
						$details = array();					
					?>
					<div id='tab-<?php echo $key; ?>' class='tab'>
						<div class="usam_table_container">
						<table class="widefat usam_table">
						<thead>
							<tr>
								<th></th>
								<th><?php _e('Название товара','usam'); ?></th>
								<th><?php _e('Цена','usam'); ?></th>
								<th><?php _e('Остаток','usam'); ?></th>
								<th></th>
							</tr>
						</thead>								
						<tbody>
						<?php 		
						if ( !empty($details['product']) )
						{
							$in = implode( ', ', $details['product'] );	
						//	$args = array( 'post_type' => 'usam-product', 'post_status' => 'publish', 'include' => $in, 'update_post_term_cache' => false, );
						//	$products = get_posts( $args );
							
							$products = $wpdb->get_results("SELECT p.ID, p.post_title, p.guid, pm.meta_value AS stock, pm2.meta_value AS price FROM `$wpdb->posts` AS p 
							LEFT JOIN `$wpdb->postmeta` AS pm ON ( pm.post_id = p.ID AND pm.meta_key = '_usam_stock')
							LEFT JOIN `$wpdb->postmeta` AS pm2 ON ( pm2.post_id = p.ID AND pm2.meta_key = '_usam_price_".$type_price."')
							WHERE p.ID IN ($in) AND p.post_status = 'publish' AND p.post_type = 'usam-product'");
						}
						else
							$products = array();
						if ( empty($products) )
						{
							?>
								<tr><td colspan = '5'><?php _e( 'Ничего не найдено', 'usam' ); ?> </td></tr>
							<?php	
						}
						else
						{							
							foreach ( $products as $product )
							{	
								?>									
									<tr>
										<td><?php echo usam_get_product_thumbnail( $product->ID, 'manage-products' ); ?></td>
										<td><?php echo $product->post_title; ?></td>
										<td><?php echo $product->price; ?></td>
										<td><?php echo $product->stock; ?></td>	
										<td>
											<a id = "add_product_to_order" data-product_id = "<?php echo $product->ID; ?>" data-order_id = "<?php echo $this->id; ?>" href=''><?php _e('Добавить в заказ','usam'); ?></a>
											<?php usam_loader(); ?>	
										</td>	
									</tr>									
								<?php 						
							}								
						}
						?>
						</tbody>
						</table>
						</div>
					</div>
					<?php 	
				}				
				?>
			</div>		
		</div>	
		<?php 		
	}
		
	public function client_mail()
	{			
		?>	
		<div class="order_form_send">
			<ul>
				<li><a id="open_send_email" data-target="#form_send_message" href=""><?php _e( 'Отправить сообщение на почту','usam' ); ?></a></li>
				<li><a id="open_form_send_sms" data-toggle="modal" data-target="#form_send_sms" href=""><?php _e( 'Отправить смс','usam' ); ?></a></li>
			</ul>		
		</div>
		<div class="order_feedback_history">
			<div class="usam_container-table-container caption border">
				<div class="usam_container-table-caption-title"><?php _e( 'Общения с клиентом о заказе', 'usam' ); ?></div>
				<table>
				<?php				
				$args = array( 'cache_attachments' => true, 'order' => 'DESC', 'orderby' => 'id', 'object_id' => $this->id, 'object_type' => 'order' );
				$mails = usam_get_emails( $args );		
					
				require_once( USAM_FILE_PATH .'/includes/feedback/sms_query.class.php'  );
				$args = array( 'order' => 'DESC', 'orderby' => 'id', 'object_id' => $this->id, 'object_type' => 'order' );
				$sms = usam_get_sms_query( $args );		
				
				$messages = array();
				foreach ( $mails as $value )
				{				
					$messages[] = array( 'type' => 'email', 'id' => $value->id, 'folder' => $value->folder, 'send'=> $value->sent_at, 'subject' => $value->subject, 'date_insert' => $value->date_insert );
				}
				foreach ( $sms as $value )
				{
					$messages[] = array( 'type' => 'sms', 'id' => $value->id, 'folder' => $value->folder, 'send' => $value->sent_at, 'subject' => $value->message, 'date_insert' => $value->date_insert );
				}					
				if ( empty($messages) )
				{
					?><tr><td><?php _e( 'Сообщений не найдено', 'usam' ); ?></td></tr><?php
				}					
				else
				{
					?>
					<thead>
						<tr>
							<td></td>
							<td></td>
							<td><?php _e( 'Дата', 'usam' ); ?></td>
							<td></td>
							<td><?php _e( 'Сообщение', 'usam' ); ?></td>
						</tr>
					</thead>
					<tbody>
					<?php							
					usort($messages, function($a, $b){  return ($a['date_insert'] < $b['date_insert']); });	
					
					foreach ( $messages as $value )
					{							
						$type = $value['folder'] == 'sent' || $value['folder'] == 'outbox'?'send':'received';					
						$title = $type == 'send'?__('Исходящие', 'usam'):__('Входящее', 'usam');		
						
						$source = '';
						switch ($value['type'] ) 
						{
							case 'email':											
								$source = __( 'Электронное письмо', 'usam' );
							break;
							case 'phone':										
								$source = __( 'По телефону', 'usam' );
							break;
							case 'sms':										
								$source = __( 'СМС сообщение', 'usam' );
							break;					
							default:
								$source = __( 'Неизвестным способом', 'usam' );
							break;
						}				
						$class = 'row-'.$type;
						?>	
							<tr class ="folder-<?php echo $value['folder']; ?> <?php echo $class; ?>">
								<td class ="error_send">
									<?php if ( $value['folder']=='outbox' ) { ?>
										<span class='outbox' title ="<?php _e('Не отправлено', 'usam'); ?>">!</span>
									<?php } ?>									
								</td>
								<td class ="send">									
									<span class='dashicons' title ="<?php echo $title; ?>"></span>
								</td>
								<td class ="date"><?php echo usam_local_date( $value['date_insert'] ); ?></td>
								<td class ="source"><?php echo $source; ?></td>									
								<td class ="message">
									<div class="subject"><?php echo $value['subject']; ?>
										<?php if ( $value['type']=='email' ) { ?>
										<div class="opendiv" title="<?php _e('Нажмите, чтобы открыть', 'usam'); ?>"></div>
										<?php } ?>
									</div>
									<?php if ( $value['type']=='email' ) { ?>
									<div class="message_display">
										<iframe src="<?php echo usam_url_admin_action( 'display_mail_body', '', array('id' => $value['id']) ); ?>"></iframe>
									</div>
									<?php } ?>
								</td>					
							</tr>				
						<?php
					}	
					?></tbody><?php
				}
				?>	
				</table>					
			</div>		
		</div>	
		<?php
	}		
			
	public function display_payment_documents()
    { 
		$payments = usam_get_payments( array('document_type' => 'order', 'document_id' => $this->id ) );				
		?>	
		<div id="usam_payment_documents_box" class="set_row_element">
			<?php 
			if ( empty($payments) )
			{
				?>
				<p class ="items_empty"><?php _e( 'Нет документов оплаты', 'usam' ); ?></p>
				<?php	
			}
			else
			{
				foreach($payments as $document)
				{
					$this->display_payment_document( $document->id );
				}
			}	 
			?>	
		</div>	
		<?php	
	}	
	
	public function display_payment_document( $document_id = 0 )
	{		
		?>
		<div id="document_payment-<?php echo $document_id; ?>" class="usam_document_container" data-item-id="<?php echo $document_id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Приходный кассовый ордер', 'usam' ); ?> <span id="shipment_<?php echo $document_id ?>">#<?php echo $document_id; ?></span></div>
				<div class="usam_document_container-action-block">						
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>			
			<?php	
			$args = array('document_type' => 'order', 'document_id' => $this->id);
			if ( $document_id == 0 )
				$args['sum'] = $this->data['totalprice'];
			
			require_once( USAM_FILE_PATH ."/admin/menu-page/orders/form/parish-form-payment.php");		
			require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
			
			$display_payment = new USAM_Form_Payment( $document_id );
			$display_payment->display_document();
			?>			
		</div>
		<?php
	}	
		
	public function display_shipped_documents()
    {
		$documents_shipped = $this->purchase_log->get_full_details_of_shipment();		
		?>	
		<div id="usam_shipped_documents_box" class="set_row_element">
			<?php 
			if ( empty($documents_shipped) )
			{
				?>
					<p class ="no_documents"><?php _e( 'Не было никаких отгрузок для этого заказа', 'usam' ); ?></p>
				<?php	
			}
			else
			{
				foreach($documents_shipped as $document)
				{
					$this->display_shipped_document( $document['id'] );
				}
			}	
			?>
		</div>
		<?php			
	}	
	
	
	public function display_shipped_document( $document_id = 0 )
	{
		?>
		<div id="document_shipped-<?php echo $document_id; ?>" class="usam_document_container" data-item-id="<?php echo $document_id; ?>">
			<div class="usam_document_container-title-container">
				<div class="usam_document_container-title"><?php _e( 'Отгрузка', 'usam' ); ?> <span id="shipment_19">#<?php echo $document_id; ?></span></div>
				<div class="usam_document_container-action-block">						
					<div class="usam_document_container-action" id="button_toggle"><?php _e( 'Свернуть', 'usam' ); ?></div>					
				</div>
			</div>
			<?php
			require_once( USAM_FILE_PATH ."/admin/menu-page/orders/form/edit-form-shipped.php");		
			require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );		
			
			$display_shipped = new USAM_Form_shipped( $document_id );
			$display_shipped->display_document();
			?>
		</div>
		<?php
	}
	
	public function order_change_history( )
    {
		$order_change_history = $this->purchase_log->get_order_change_history();		
		?>
		<div class="usam_table_container">
		<table class="widefat usam_table">
			<thead>
				<tr>
					<th><?php _e( 'Дата', 'usam' ); ?></th>
					<th><?php _e( 'Пользователь', 'usam' ); ?></th>						
					<th><?php _e( 'Операция', 'usam' ); ?></th>
					<th><?php _e( 'Описание', 'usam' ); ?></th>										
				</tr>
			</thead>
			<tbody>								
				<?php 													
					if ( empty($order_change_history) )
					{
						?>
							<tr><td colspan = '4'><?php _e( 'Нет истории', 'usam' ); ?> </td></tr>
						<?php	
					}
					else
					{
						foreach($order_change_history as $history)
						{
							$user = get_user_by('id', $history->user_id);
							if ( isset($user->user_login) )
								$user_name = $user->user_login;
							else
								$user_name = __('Гость', 'usam');
							?>
							<tr>
								<td><?php echo get_date_from_gmt( $history->date, 'd.m.Y G:i' ); ?></td>
								<td><?php echo $user_name; ?></td>
								<td><?php echo get_order_operation_message($history->operation); ?></td>
								<td><?php echo $history->description; ?></td>
							</tr>
							<?php 
							
							
						}
					}
				?>
			<tbody>
		</table>
		</div>
		<?php		
	}	
	
	public function downloadable_files()
    {		
		$downloadable_files = $this->purchase_log->get_downloadable_files();	
		$downloads_locked = false;
		?>	
		<div class="usam_table_container">
		<table id = "downloadable_products_table" class="widefat usam_table" cellspacing="0">
			<thead>
				<tr>
					<th class="column_title"><?php _e( 'Имя товара', 'usam' ); ?></th>	
					<th class="column_available"><?php _e( 'Доступно', 'usam' ); ?></th>					
					<th class="column_ip"><?php _e( 'IP', 'usam' ); ?></th>
					<th class="column_status"><?php _e( 'Статус', 'usam' ); ?></th>
					<th class="column_date"><?php _e( 'Изменен', 'usam' ); ?></th>		
				</tr>
			</thead>
			<tbody>
				<?php				
				if ( empty($downloadable_files) )
				{
					?>
						<tr><td colspan = '6' class = "no-items"><?php _e( 'Нет загружаемых заказов', 'usam' ); ?> </td></tr>
					<?php	
				}
				else
				{						
					foreach($downloadable_files as $file)
					{
						?>
						<tr data-item-id="<?php echo $file->id; ?>">						
							<td><?php echo $file->file_name; ?></td>
							<td><?php echo $file->downloads; ?></td>
							<td><?php echo $file->ip_number; ?></td>							
							<td><?php 
									switch ( $file->active ) 
									{
										case 0:											
											_e( 'Не доступно', 'usam' );
										break;
										case 1:										
											_e( 'Доступно', 'usam' );	
										break;	
									}														
								?>
							</td>							
							<td><?php echo usam_local_date( $file->date_modified ); ?></td>
						</tr>
					<?php					
					} 
				}
				?>	
			</tbody>			
		</table>
		</div>				
		<?php		
	}		
	
	/**
	 * Вывод конструкции заказа
	 */
	public function display_left( )
	{		
		if ( $this->data['modified_customer'] == 1 )
		{
			?>
			<div id="modified_customer">
				<h2>
					<?php printf( __( 'Обратите внимание заказ был изменен покупателем %s', 'usam' ), date_i18n( get_option('date_format'), strtotime($this->data['date_modified']) ) ); ?>
					<a href="#nav-usam_order_change_history"><?php _e( 'Посмотреть изменения', 'usam' ); ?></a>
					<span class="hide_modified_customer"><?php _e( 'Скрыть', 'usam' ); ?></span>
				</h2>
			</div>
			<?php
		}
		?>									
		<div class = "order_details_header">				
			<?php $this->order_info(); ?>																
		</div>		
		<?php			
		if ( $this->data['notes'] != '' )
			usam_add_box( 'usam_create_notes', __( 'Заметки заказа', 'usam' ), array( $this, 'create_notes' ) );		

		usam_add_box( 'usam_order_products', __('Заказанные позиции', 'usam'), array($this, 'cart_items') );
		if ( !$this->is_closed )
			usam_add_box( 'usam_contact_customer', __( 'Контакты с клиентом', 'usam' ), array( $this, 'client_mail' ) );	
				
		usam_add_box( 'usam_order_customer', __('Покупатель', 'usam'), array($this, 'customer') );		
	
		usam_add_box( 'usam_shipped_products', __('Отгрузки', 'usam'), array($this, 'display_shipped_documents') );
		usam_add_box( 'usam_payment_history', __('История оплаты', 'usam'), array( $this, 'display_payment_documents' ) );
	
		if ( $this->data['user_ID'] && ( !$this->is_closed ) )
			usam_add_box( 'usam_suggest_customer', __( 'Предложите покупателю', 'usam' ), array( $this, 'suggest_customer' ) );
									
		usam_add_box( 'usam_downloadable_files', __('Загружаемые товары', 'usam'), array($this, 'downloadable_files') );								
		usam_add_box( 'usam_order_change_history',  __( 'История изменений заказа', 'usam' ), array( $this, 'order_change_history' ) );		
	}
	
	protected function get_fastnav( ) 
	{											
		return array( 
			'usam_suggest_customer' => __( 'Предложите покупателю', 'usam' ), 
			'usam_order_products' => __( 'Товары', 'usam' ), 
			'usam_contact_customer' => __( 'Контакты с клиентом', 'usam' ),
			'usam_create_notes' => __( 'Заметки', 'usam' ),
			'usam_payment_history' => __( 'Оплаты', 'usam' ),
			'usam_shipped_products' => __( 'Отгрузки', 'usam' ),
			'usam_downloadable_files' => __( 'Загружаемые', 'usam' ),
			'usam_order_change_history' => __( 'История', 'usam' ),
		);
	}
	
	public function display_actions( )
	{
		?>		
		<?php
	}
		
	public function display_right( )
	{			
		if ( $this->type != 'new_item' && !$this->is_closed )
		{
		?>
			<div id='usam_thiss_links' class = "postbox">							
				<h3 class="hndle"><span><?php _e( 'Действия над заказом', 'usam' ); ?></span></h3>
				<div class="inside"><?php $this->thiss_links(); ?></div>
			</div>	
		<?php 						
		}	
		if ( $this->type != 'new_item' )
			usam_add_box( 'usam_comments', __('Комментарии','usam'), array( $this, 'display_comments' ), 'order' );			
	}
}