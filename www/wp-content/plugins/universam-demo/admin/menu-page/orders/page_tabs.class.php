<?php
/*
 * Отображение страницы Документы
 */ 
$default_tabs = array(		
	array( 'id' => 'orders',  'title' => __( 'Заказы', 'usam' ) ),
	array( 'id' => 'shipped',  'title' => __( 'Отгрузки', 'usam' ) ),
	array( 'id' => 'payment',  'title' => __( 'Оплаты', 'usam' ) ),
	array( 'id' => 'refunds',  'title' => __( 'Возвраты', 'usam' ) ),
//	array( 'id' => 'route_list',  'title' => __( 'Маршрутные листы', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{	
	protected function localize_script_tab()
	{
		wp_enqueue_script( 'knob' );				
		wp_enqueue_script( 'iframe-transport' );	
		wp_enqueue_script( 'fileupload' );	
		
		return array(
			'change_purchase_log_status_nonce'      => usam_create_ajax_nonce( 'change_purchase_log_status' ),
			'change_payment_documents_status_nonce' => usam_create_ajax_nonce( 'change_payment_documents_status' ),
			'payment_save_external_document_nonce'  => usam_create_ajax_nonce( 'payment_save_external_document' ),
			'message_payment'                       => __( 'Номер документа', 'usam' ),			
			'current_view'                     => empty( $_REQUEST['status'] ) ? 'all' : sanitize_title($_REQUEST['status']),
			'current_page'                     => empty( $_REQUEST['paged']) ? '' : sanitize_title($_REQUEST['paged']),
		);
	}	
	
	protected function save_shipped_document( $document_id, $document )
	{ 
		$document['date_allow_delivery'] = usam_get_datepicker('date_allow_delivery_'.$document_id);
		if ( !$document['date_allow_delivery'] )					
			$document['date_allow_delivery'] = NULL;	
		
		$document['doc_data'] = usam_get_datepicker('doc_data_'.$document_id);
		if ( !$document['doc_data'] )					
			$document['doc_data'] = NULL;					
		
		$document['readiness_date'] = usam_get_datepicker('readiness_date_'.$document_id);	
		if ( empty($document['readiness_date']) )					
			$document['readiness_date'] = NULL;	
		else
		{						
			if ( $document_id )
				$shipped_document = usam_get_shipped_document( $document_id );
			
			$current_time = time(); 
			if ( ( empty($shipped_document['readiness_date']) || $shipped_document['readiness_date'] != $document['readiness_date']) && strtotime($document['readiness_date']) > $current_time )
			{				
				$notification = new USAM_Order_Notification_Willingness( $shipped_document['order_id'], $shipped_document['id'] );
				$email_sent = $notification->send_mail();
				
				$order = usam_get_order( $shipped_document['order_id'] );
				
				$task['title'] = sprintf( __( 'Готовность отгрузки заказа %s', 'usam' ), $shipped_document['order_id'] );
				$task['description'] = sprintf( __( 'Отгрузка заказа №%s будет готова в %s', 'usam' ), $shipped_document['order_id'], usam_local_date( $document['readiness_date'] ) );								
				$task['date_time'] = $document['readiness_date'];	
				
				$objects = array( array('object_type' => 'shipped_document', 'object_id' => $document_id));
				
				if ( usam_is_type_payer_company($order['type_payer']) ) 		
					$customer_type = 'company';		
				else
					$customer_type = 'contact';
				
				if ( !empty($order['customer_id']) )
					$objects[] = array('object_type' => $order['customer_type'], 'object_id' => $order['customer_id'] ); 
				
				usam_insert_system_event( $task, $objects );			
			} 
		}
		if ( empty($document['products']) )	
		{ // Удалить товар
			if ( $document_id )
			{
				$shipped = new USAM_Shipped_Document( $document_id );	
				$products_shipping = $shipped->get_products();						
				if ( !empty($products_shipping) )
				{
					$delete_products = array();
					foreach( $products_shipping as $product )
					{
						$delete_products[] = array( 'basket_id' => $product->basket_id, 'quantity' => 0 );								
					}
					$shipped->set_products( $delete_products );
				}
			}
		}	
		else
		{   
			$update_products = array();
			foreach( $document['products'] as $basket_id => $product )
			{						
				$update_products[] = array( 'basket_id' => $basket_id, 'quantity' => $product['quantity'], 'reserve' => $product['reserve'] );			
			}
			$document['products'] = $update_products;		
		}					
		$order = new USAM_Order( $document['order_id'] );	
		if ( $document_id )
			$order->edit_document_shipped( $document_id, $document );
		else
			$document_id = $order->add_document_shipped( $document );
	
		return $document_id;
	}
	
	protected function save_payment_document( $document_id, $payment )
	{ 
		if ( !empty($payment['sum']) )
		{			
			$payment['date_payed'] = usam_get_datepicker('date_payed-'.$document_id);	
			$payment['pay_up'] = usam_get_datepicker('pay_up-'.$document_id);	
			if ( $document_id )
			{				
				$_payment = new USAM_Payment_Document( $document_id );			
				$_payment->set( $payment );					
			}
			else
			{		
				$_payment = new USAM_Payment_Document( $payment );		
			} 
			$_payment->save();	
			return $_payment->get('id');
		}
		return false;
	}
} 