 <?php		
 require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
 class USAM_Form_Payment_Edit extends USAM_Edit_Form
{		
	protected $payment_type = '';
	
	protected function get_title_tab()
	{ 	
		
		if ( $this->data['payment_type'] == 0 )
		{
			if ( $this->id != null )
				$title = sprintf( __('Изменить расходный кассовый ордер №%s','usam'), $this->id );
			else
				$title = __('Добавить расходный кассовый ордер', 'usam');	
		}
		else
		{		
			if ( $this->id != null )
				$title = sprintf( __('Изменить приходный кассовый ордер №%s','usam'), $this->id );
			else
				$title = __('Добавить приходный кассовый ордер', 'usam');	
		}
		return $title;
	}	
	
	protected function get_data_tab(  )
	{		
		if ( $this->id )
		{ 
			$this->data = usam_get_payment_document( $this->id );				
		}
		else
		{
			$this->id = 0;
			
			$day = get_option( 'usam_number_days_delay_payment', 3 );
			$pay_up = date( "Y-m-d H:i:s", mktime(date('H'), date('i'), 0, date('m'), date('d') + $day, date('Y')));	
				
			$this->data = array( 'id' => 0, 'date_time' => '', 'date_update' => '', 'date_payed' => '', 'bank_account_id' => '', 'name' => '', 'document_number' => '', 'document_type' => '', 'document_id' => 0, 'gateway_id' => '', 'pay_up' => $pay_up, 'sum' => 0, 'status' => '', 'transactid' => '', 'payment_type' => $this->payment_type, 'external_document' => '' );			
		}	
	}	
	
	public function display_attached() 
	{    		
		$documents = array( '' => __('Не прикреплен','usam'), 'order' => __('Заказ','usam'), 'refund' => __('Возврат','usam') );	
		?>		
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_edit_table">
			<tbody>							
				<tr>
					<td class="usam_table_cell_name" class = "name"><?php _e( 'Документ', 'usam' ); ?>:</td>
					<td class="usam_table_cell_option">
						<span class = "usam_display_data"><?php echo $documents[$this->data['document_type']]; ?></span>
						<div class = "usam_edit_data">
							<select class="chzn-select-nosearch-load" name = "_payment[<?php echo $this->id; ?>][document_type]">
								<?php										
								foreach ($documents as $key => $title )
								{
								?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected($this->data['document_type'], $key ); ?>><?php echo $title; ?></option>
								<?php
								}
								?>						
							</select>
						</div>
					</td>
				</tr>	
				<tr>
					<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер заказа', 'usam' ); ?>:</td>							
					<td class="usam_table_cell_option">
						<span class = "usam_display_data">
							<?php 
							if ( $this->data['document_type'] == 'order' ) 
								echo usam_get_link_order( $this->data['document_id'] ); 
							else
								$this->data['document_id'];
							?>							
						</span>
						<input disabled = "disabled" type="text" name="_payment[<?php echo $this->id; ?>][document_id]" class = "usam_edit_data" value="<?php echo $this->data['document_id']; ?>">
					</td>
				</tr>													
			</tbody>
		</table>
			
	<?php
	}	
	
	private function disabled() 
	{ 
		echo $this->id?'disabled = "disabled"':'';
	}
	
	protected function display_document_sidebar() 
	{ 
	
	}
	
	public function display_document() 
	{    				
		?>
		<div class="usam_document_container-content">
			<?php $this->display_document_sidebar(); ?>
			<div class="usam_document_container-right">				
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Документ', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
						<tbody>						
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер документа', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data"><?php echo $this->data['document_number']; ?></span>
									<input <?php $this->disabled(); ?> type="text" name="_payment[<?php echo $this->id; ?>][document_number]" class = "usam_edit_data" value="<?php echo $this->data['document_number']; ?>">
								</td>
							</tr>							
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата создания', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option"><?php echo usam_local_date( $this->data['date_time'] ); ?></td>
							</tr>	
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата обновления', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option"><?php echo usam_local_date( $this->data['date_update'] ); ?></td>
							</tr>								
						</tbody>
					</table>
				</div>			
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Свойство', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
						<tbody>
							<tr>
								<td class="usam_table_cell_name"><?php _e( 'Расчетный счет', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo usam_get_display_company_by_acc_number( $this->data['bank_account_id'] ); ?></span>
									<span class ="usam_edit_data"><?php usam_select_bank_accounts( $this->data['bank_account_id'], array('name' => "_payment[$this->id][bank_account_id]") ) ?></span>
								</td>
							</tr>
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Способ оплаты', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data"><?php echo $this->data['name']; ?></span>
									<div class = "usam_edit_data"><?php usam_get_payment_gateway_dropdown( $this->data['gateway_id'], array('name' => "_payment[$this->id][gateway_id]") ) ?></div>
								</td>
							</tr>	
							<tr>	
								<?php $date_pay_up = empty($this->data['pay_up'])?'':date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($this->data['pay_up'], "Y-m-d H:i:s"))); ?>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Оплать до', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $date_pay_up; ?></span>
									<div class = "usam_edit_data"><?php usam_display_datetime_picker( 'pay_up-'.$this->id, $this->data['pay_up'] ); ?></div>
								</td>
							</tr>	
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Стоимость', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['sum']; ?></span>
									<input size = "10" maxlength = "10" <?php $this->disabled(); ?> type="text" name="_payment[<?php echo $this->id; ?>][sum]" class = "usam_edit_data" value="<?php echo $this->data['sum']; ?>">
								</td>
							</tr>
							<tr>
								<td class="usam_table_cell_name vat" class = "name"><?php _e( 'Статус', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option">
									<span class = "usam_display_data"><?php echo usam_get_payment_document_status_name( $this->data['status'] ); ?></span>
									<div class = "usam_edit_data">
										<?php 
										$args = array(
											'name'                  => "_payment[$this->id][status]",
											'id'                    => "usam-payment_status",
											'class'                 => 'chzn-select-nosearch-load',
											'additional_attributes' => '',
										);
										echo usam_get_payment_status_dropdown($this->data['status'] , $args ) 
										?>												
									</div>										
								</td>
							</tr>	
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер транзакции', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['transactid']; ?></span>
									<input maxlength = "50" <?php $this->disabled(); ?> type="text" name="_payment[<?php echo $this->id; ?>][transactid]" class = "usam_edit_data" value="<?php echo $this->data['transactid']; ?>">
								</td>
							</tr>	
							<tr>									
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Дата оплаты', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo usam_local_date( $this->data['date_payed'] ); ?></span>
									<div class = "usam_edit_data"><?php usam_display_datetime_picker( 'date_payed-'.$this->id, $this->data['date_payed'] ); ?></div>
								</td>
							</tr>									
						</tbody>
					</table>
				</div>	
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Внешняя программа учета', 'usam' ); ?></div>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" class="usam_detail_content_table_form usam_edit_table ">
						<tbody>					
							<tr>
								<td class="usam_table_cell_name" class = "name"><?php _e( 'Номер документа', 'usam' ); ?>:</td>
								<td class="usam_table_cell_option tal">
									<span class = "usam_display_data"><?php echo $this->data['external_document']; ?></span>
									<input maxlength = "50" <?php $this->disabled(); ?> type="text" name="_payment[<?php echo $this->id; ?>][external_document]" class = "usam_edit_data" value="<?php echo $this->data['external_document']; ?>">
								</td>
							</tr>														
						</tbody>
					</table>
				</div>	
				<input type="hidden" name="payment_ids[]" value="<?php echo $this->id; ?>"/>					
				<input type="hidden" name="_payment[<?php echo $this->id; ?>][payment_type]" value="<?php echo $this->data['payment_type']; ?>"/>					
			</div>				
		</div>	
		<?php
	}	
			
	function display_left()
	{			
		usam_add_box( 'usam_display_attached', __( 'Прикреплен', 'usam' ), array( $this, 'display_attached' ), array(), true );	
		usam_add_box( 'usam_payment_general_information', __( 'Общая информация', 'usam' ), array( $this, 'display_document' ), array(), true );		
	}
}
?>