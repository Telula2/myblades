<?php
class USAM_Exported_Purchase_Logs
{
	private $csv_name = 'exported';
	private $delimiter = ';';
	
    public function __construct()
	{	
	
    }
	
	public function exported( $exported, $orders )
	{
		$callback = 'controller_'.$exported;
		$result = false;
		if ( method_exists( $this, $callback )) 
			$result = $this->$callback( $orders );	
	}
	
	private function get_headers_exported( $form_headers_array, $type = 'str' )
	{
		$headers_array = array(	__('Номер заказа', 'usam'), __('Сумма заказа', 'usam'),  __('Способ оплаты', 'usam'), __('Статус оплаты', 'usam'), __( 'Дата заказа', 'usam' ));	
		$headers2_array = array( __( 'Количество', 'usam' ), __( 'Название товара', 'usam' ), __( 'Артикул', 'usam' ), );
		$form_headers_array = array_values($form_headers_array);		
		if ( $type == 'str' )
		{	
			$headers      = '"' . implode( '","', $headers_array ) . '"';
			$form_headers = '"' . implode( '","', $form_headers_array ) . '"';
			$headers2     = '"' . implode( '","', $headers2_array ) . '"';
			$out = $headers .','. $form_headers .','. $headers2;
		}
		else
		{
			$out = array_merge ( $headers_array, $form_headers_array, $headers2_array );
		}
		return $out;
	}
	
	// Выгрузка в Журнале Продаж в csv-файл
	function purchase_log_csv( $data )
	{							
		$output = '';
		foreach ( (array)$data as $purchase ) 
		{
			$purchase_log = new USAM_Order($purchase->id);
			$order = $purchase_log->get_data();		
			$cart_contents = $purchase_log->get_order_products();				
			$customer_data = $purchase_log->get_customer_data();	
			
			$output .= "\"" . $order['id'] . "\","; 				 			 //Номер заказа
			$output .= "\"" . $order['totalprice'] ."\","; 		  //Сумма заказа							
			$form_headers_array = array();
			foreach ( $customer_data as $key => $userinfo ) 
			{
				if ( stristr( $key, 'shipping' ) || stristr( $key, 'billing' ) )
				{
					$form_headers_array[] = $userinfo['unique_name'];
					$output .= $userinfo['value'] . "\",";
				}
			}			
			$output .= "\"".usam_get_order_status_name( $order['status'] )."\",";   //Статус оплаты
			$output .= "\"".usam_local_date( $order['date_insert'] )."\","; //Дата		
			
			// Корзина товаров
			foreach ( (array)$cart_contents as $item ) 
			{
				$sku = usam_get_product_meta( $item->product_id, 'sku' );
				if( empty( $sku ) )
					$sku = __( 'Нет данных', 'usam' );
				$output .= "\"" . $item->quantity . "\",";
				$output .= "\"" . str_replace( '"', '\"', $item->name ) . "\"";
				$output .= "," . $sku."," ;
			}
			$output .= "\n"; // Конец строки
		}	
		$headers	  = $this->get_headers_exported( $form_headers_array );
		$headers      = apply_filters( 'usam_order_csv_headers', $headers, $data );
		$output       = apply_filters( 'usam_order_csv_output', $output, $data );
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: inline; filename="'.$this->csv_name.'.csv"' );
		echo $headers . "\n". $output;
		exit;
	}
	
	function purchase_log_excel( $data )
{		
	
		$output = array();
		$form_headers_array = array();
		$i = 0;
		foreach ( (array)$data as $purchase ) 
		{				
			$purchase_log = new USAM_Order( $purchase->id );
			$order = $purchase_log->get_data();		
			$cart_contents = $purchase_log->get_order_products();				
			$customer_data = $purchase_log->get_customer_data();
		
			$output[$i][] = $purchase->id; 				                 //Номер заказа
			$output[$i][] = $order['totalprice']; 		  //Сумма заказа				
			$output[$i][] = usam_get_order_status_name( $order['status'] );   //Статус оплаты			
			$output[$i][] = date( "d M Y", $order['date'] );                        //Дата						
			
			foreach ( $customer_data as $key => $userinfo ) 
			{
				if ( stristr( $key, 'shipping' ) || stristr( $key, 'billing' ) )
				{
					$form_headers_array[$userinfo['unique_name']] = $userinfo['name'];
					$output[$i][] = $userinfo['value'];
				}
			}				
			$buf = $output[$i];
			$j = 1;
			$order_item_count = count($cart_contents);
			// Корзина товаров
			foreach ( (array)$cart_contents as $item ) 
			{	
				$sku = usam_get_product_meta( $item->product_id, 'sku' );
				if( empty( $sku ) )
					$sku = __( 'Нет данных', 'usam' );
				$output[$i][] = $item->quantity;
				$output[$i][] = str_replace( '"', '\"', $item->name );
				$output[$i][] = $sku;
				if ( $j < $order_item_count )
				{	
					$i++;
					$output[$i] = $buf;
				}
				$j++;
			}
			$i++;
		}				
		$headers	  = $this->get_headers_exported( $form_headers_array, 'array' );
		$headers      = apply_filters( 'usam_order_excel_headers', $headers, $data );
		$output       = apply_filters( 'usam_order_excel_output', $output, $data );	
		usam_write_exel_file( 'row', $output, $headers );	
	}

	// Описание: Выгрузка заказов в файл csv для импорта на севис dx.com
	function purchase_log_dx_csv( $data ) 
	{					
		$headers_array = array( 'InvoiceNum','TranscationNum','FirstName','LastName','Phone','ShipmentMethod','CountryCode','State','City','Address1','Address2','ZipCode','Coupon','Reserved','Reserved','Reserved');	
		
		$count_max = 0;
		$i = 0;
		$output_array = array();
		foreach ( (array)$data as $purchase )
		{			
			$purchase_log = new USAM_Order($purchase->id);
			$order = $purchase_log->get_data();		
			$cart_contents = $purchase_log->get_order_products();				
			$customer_data = $purchase_log->get_customer_data();				
			
			if ( $order['status'] == 'accepted_payment')
			{	
				$output = '';								
				$output .= $purchase->id . ","; //Номер накладной
				$output .= "OB00" . $purchase->id . ","; //Номер сделки
				$output .= $customer_data['billingfirstname']['value'] . ","; //Имя
				$output .= $customer_data['billinglastname']['value'] . ","; //Фамилия		
				$output .= $customer_data['billingphone']['value'] . ","; //телефон
				$output .= "STANDARD". ","; //Метод доставки
				$output .= $customer_data['shippingcountry']['value'] . ","; //Страна 	
				$output .= $customer_data['shippingregion']['value'] . ","; //Область  	
				$output .= $customer_data['shippingcity']['value'] . ","; //Город 				
				$output .= str_replace( array( "\n", "\r","," ), ' ', $customer_data['shippingaddress']['value']). ","; //Адрес
				$output .= ","; //Адрес
				$output .= $customer_data['shippingpostcode']['value'] . ","; //Индекс
				$output .= ","; //Купон			
				$output .= ","; //Зарезервировано
				$output .= ","; //Зарезервировано
				$output .= ""; //Зарезервировано					

				$count = count( $cart );				
				if( $count_max < $count )
					$count_max = $count;
				// Пройдите через все продукты в корзину для отображения количества и Артикул				
				foreach ( (array)$cart_contents as $item ) 
				{
					$sku = usam_get_product_meta( $item->product_id, 'sku' );							
					$output .= "," . $sku."," ;
					$output .= $item->quantity;				
				}			
				$output_array[$i]['str'] = $output;
				$output_array[$i]['count'] = $count;
				$i++;
			}
		}
		if ( empty($output_array) )
			return;
		$output ='';
		foreach ($output_array as $item ) 
		{
			$str = "";
			if 	($item['count']< $count_max)
			{				
				for ($i = $item['count']; $i < $count_max; $i++)							
					$str .= ",,";	
			}			
			$output .= $item['str']. $str . "\n";					
		}	
		// Получить наибольшее количество продукции и создать заголовок для них
		$headers_array2 = "";
		for( $i = 0; $i < $count_max; $i++ )
		{
			$headers_array2[] = "SKU";
			$headers_array2[] = "QTY";
		}
		$headers  = implode( ',', $headers_array );
		$headers2 = ',' . implode( ',', $headers_array2 );

		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: inline; filename="' . $this->csv_name . '"' );
		echo $headers . $headers2 . "\n". $output;
		exit;	
	}
}