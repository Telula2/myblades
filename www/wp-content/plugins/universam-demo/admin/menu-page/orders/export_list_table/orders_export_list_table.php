<?php
class USAM_Export_List_Table_orders extends USAM_Export_List_Table
{	
	public $order;
	
	public function column_customer( $item )
	{			
		$customer_data = $this->order->get_customer_data();		
		$text = '';
		$email = '';
		if ( usam_is_type_payer_company( $item->type_payer ) )			
		{
			$text = !empty($customer_data['company'])?$customer_data['company']['value'].' ':'';
			$email = !empty($customer_data['company_email'])?$customer_data['company_email']['value']:'';
		}
		else
		{
			$firstname = !empty($customer_data['billingfirstname'])?$customer_data['billingfirstname']['value'].' ':'';
			$lastname = !empty($customer_data['billinglastname'])?$customer_data['billinglastname']['value']:'';
			$text = $firstname.$lastname;
			
			$email = !empty($customer_data['billingemail'])?$customer_data['billingemail']['value']:'';
		}				
		?>
		<strong><?php echo $text; ?></strong><br />
		<small><?php echo $email; ?></small><br />
		<?php			
		if ( !empty($item->user_login) )
		{
			?><small><?php printf( __('Логин: %s','usam'), $item->user_login ) ; ?></small><?php	
		}
		else
		{
			?><small><?php _e('Гость','usam'); ?></small><?php	
		}
	}
	
	public function column_address( $item )
	{			
		$shippingaddress = '';
		$customer_data = $this->order->get_customer_data();
		if ( isset($customer_data['shippingaddress']) )
			$shippingaddress = !empty($customer_data['shippingaddress'])?$customer_data['shippingaddress']['value'].' ':'';
		elseif ( isset($customer_data['company_shippingaddress']) )
			$shippingaddress = !empty($customer_data['company_shippingaddress'])?$customer_data['company_shippingaddress']['value'].' ':'';
			
		echo $shippingaddress;
	}

	public function column_date_paid( $item ) 
	{			
		if ( $item->date_paid )
			echo usam_local_date( $item->date_paid, get_option( 'date_format', 'Y/m/d' ) );
	}	
	
	public function column_date_modified( $item ) 
	{		
		echo usam_local_date( $item->date_modified );
	}

	public function column_totalprice( $item ) 
	{
		$text = usam_currency_display( $item->totalprice )."<br /><small>".sprintf( _n( '1 товар', '%s товаров', $item->product_count, 'usam' ), number_format_i18n( $item->product_count ) ).'</small>';
		
		echo $text;
	}
	
	public function column_status( $item ) 
	{		
		echo usam_get_order_status_name( $item->status );	
	}
	
	public function column_notes( $item ) 
	{			
		echo $item->notes;
	}	
	
	public function column_shipping( $item )
	{			
		$shipped_documents = $this->order->get_shipped_documents();	
		$i = 0;		
		foreach ( $shipped_documents as $document )
		{	
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			if ( !empty($document->storage_pickup) )
			{	
				$storage = usam_get_store_field( $document->storage_pickup, 'title' );
				$storage_pickup = "<br>".$storage;							
			}		
			else
				$storage_pickup = '';
			$date_allow_delivery = !empty($document->date_allow_delivery)?"<br>".usam_local_date( $document->date_allow_delivery ):'';
			
			
			echo "<strong>".$document->name."</strong>$storage_pickup<br>".usam_currency_display($document->price).$date_allow_delivery."<br><strong>".usam_get_shipped_document_status_name( $document->status )."</strong>";			
		}		
	}
	
	public function column_method( $item )
	{	
		$payment_documents = $this->order->get_payment_history();			
		$i = 0;
		foreach ( $payment_documents as $document )
		{
			$i++;
			if ( $i > 1 )
				echo '<hr size="1" width="90%">';
			$date_payed = empty($document['date_payed'])?'':' - '.usam_local_date( $document['date_payed'], get_option( 'date_format', 'Y/m/d' ) );
			echo "<strong>".$document['name']."</strong><br>".usam_currency_display($document['sum'])."<br>(".usam_get_payment_document_status_name($document['status'])."$date_payed)";			
		}
	}
	
	public function single_row( $item ) 
	{
		$this->order = new USAM_Order( $item->id );
		echo '<tr>';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
}