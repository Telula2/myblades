<?php
class USAM_List_Table_analysis extends USAM_List_Table
{
	protected $prefix = 'pc';
	function __construct( $args )
	{	
		parent::__construct( $args );
		$columns_products = new USAM_Display_Product_Page( );				
    }	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_title( $item )
	{	
		$title = "<a href='".$item->guid."'>".$item->post_title."</a>";		
		$this->row_actions_table( $title, $this->standart_row_actions( $item->id ) );	
	}	
	
	
   function column_default( $item, $column_name ) 
   {				
		switch( $column_name ) 
		{
			case 'image': 
			case 'cats':					
				return do_action( "manage_usam-product_posts_custom_column", $column_name, $item->product_id );
			break;
			case 's_price': 
			case 'r_price':
				echo usam_currency_display($item->$column_name);	
			break;	
			case 'url':
				echo "<a href='".$item->url."'>".parse_url($item->url, PHP_URL_HOST)."</a>";	
			break;						
			case 'date_insert':
				echo date( "d.m.Y H:i:s", strtotime($item->date_insert) );
			break;	
			case 'r_price':
				echo usam_currency_display($item->$column_name);	
			break;				
			default:
				echo $item->$column_name;			
			break;
		}	
    }
	  
	function get_sortable_columns()
	{
		$sortable = array(
			'title'          => array('title', false),				
			'r_price'        => array('r_price', false),			
			'price'          => array('price', false),		
			'manager'        => array('manager', false),				
			'date_insert'    => array('date_insert', false)
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',			
			'image'          => '',			
			'title'          => __( 'Имя товара', 'usam' ),
			'r_price'        => __( 'Цена', 'usam' ),
			's_price'        => __( 'Найденная цена', 'usam' ),			
			'url'            => __( 'Ссылка', 'usam'),			
			'description'    => __( 'Описание', 'usam' ),
			'manager'        => __( 'Автор', 'usam' ),		
			'date_insert'    => __( 'Дата', 'usam' )
        );		
        return $columns;
    }	
	
	function prepare_items() 
	{			
		global $wpdb, $type_price;
		
		$this->get_standart_query_parent( );		
		$search_terms = $this->search != '' ? explode( ' ', $this->search ): array();
		
		$this->select[] = "pc.id,pc.product_id, pc.url, pc.price_found AS s_price, pc.description, pc.manager_id, pc.date_insert, p.post_title, p.guid, pm.meta_value AS r_price";
		
		$search_sql = array();			
		foreach ( $search_terms as $term )
		{
			$search_sql[$term][] = "p.title LIKE '%".esc_sql( $term )."%'";			
			$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
		}		
		if ( $search_sql )
		{
			$this->where[] = implode( ' AND ', array_values( $search_sql ) );
		}						
		if ( isset($_GET['status']) && $_GET['status'] != '-1' )		
			$this->where[] = 'status='.$_GET['status'];	
		
		$this->joins[] = "LEFT JOIN `".$wpdb->posts."` AS p ON pc.product_id = p.ID ";
		$this->joins[] = "LEFT JOIN `".$wpdb->postmeta."` AS pm ON (pm.post_id =  p.ID AND pm.meta_key = '_usam_price_$type_price') ";
			
		$where = implode( ' AND ', $this->where );
		$select = implode( ' AND ', $this->select );
		$joins = implode( ' ', $this->joins );
		
		$sql = "SELECT $select FROM `".USAM_TABLE_PRICE_COMPARISON."` AS pc $joins WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";		
		$this->items = $wpdb->get_results( $sql );
		
		$this->total_items = $wpdb->get_var( "SELECT COUNT(*) FROM `".USAM_TABLE_PRICE_COMPARISON."` AS pc $joins WHERE {$where}" );
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}