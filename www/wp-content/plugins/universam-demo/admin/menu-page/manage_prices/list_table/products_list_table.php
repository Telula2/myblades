<?php
require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );
class USAM_List_Table_products extends USAM_Product_List_Table
{
	function __construct( $args )
	{		
		global $type_price;		
		
		$is_price = true;
		$prices = usam_get_prices( array( 'base_type' => '0' ) );
		if ( !empty($prices) )
		{
			foreach ( $prices as $price )
				if ( $price['code'] == $type_price )
				{
					$is_price = false;
					break;
				}
			if ( $is_price )
				$type_price = $prices[0]['code'];
		}
		parent::__construct( $args );				
	}	
	
	function column_price( $item ) 
	{	
		$price = usam_get_product_price( $item->ID );
		echo "<input class ='show_change' type='text' name='product_price[$item->ID]' data-product_id='$item->ID' value='$price'/>";
	}	
				
	public function extra_tablenav_display( $which ) 
	{		
		if ( 'top' == $which )
		{			
			global $type_price;		
		
			$currency = usam_get_currency_sign();			
			?>	
			<div class="usam_global_operation">	
				<h2><?php _e('Глобальное изменение цены', 'usam'); ?></h2>					
				<div class="global_operation">
					<div class="content">	
						<select id="price_global_operation">				
							<option value='+'><?php _e('Увеличить', 'usam'); ?></option>			
							<option value='-'><?php _e('Уменьшить', 'usam'); ?></option>						
						</select>
						<?php
						echo usam_get_select_type_md( '', array( 'id' => 'price_global_type_operation' ));
						echo "<input type='text' id='price_global_value' value=''/>";
						submit_button( __( 'Apply' ), 'action', '', false, array( 'id' => "apply" ) );
					echo '</div>';	
				echo '</div>';	
			echo '</div>';	
			echo '<div class="alignleft actions">';				
				submit_button( __('Сохранить цены', 'usam'), 'primary', 'save', false, array( 'id' => 'save-submit' ) ); 									
			echo '</div>';								
		}
	}	
	
	protected function price_dropdown( ) 
	{
		$filter_manage = new USAM_Product_Filter_Manage();			 
		$filter_manage->filter_print( 'price' );
	}
	
	protected function meta_dropdown( ) 
	{
		$filter_manage = new USAM_Product_Filter_Manage();			 
		$filter_manage->filter_print( 'meta' );
	}
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'price' => array(), 'terms' => array(), 'meta' => array() );		
	}
	    
	function get_sortable_columns()
	{
		$sortable = array(
			'product_title'  => array('product_title', false),	
			'price'          => array('price', false),	
			'stock'          => array('stock', false),		
			'views'          => array('views', false),				
			'date'           => array('date', false),		
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           									
			'image'          => '',
			'product_title'  => __( 'Имя', 'usam' ),		
			'price'          => __( 'Цена', 'usam' ),
			'cats'           => __( 'Категории', 'usam' ),		
			'sku'            => __( 'Артикул', 'usam' ),								
			'stock'          => __( 'Запас', 'usam'),
			'views'          => '<span class = "usam-dashicons-icon" title="' . esc_attr__( 'Просмотры' ) . '">'.__( 'Просмотры' ).'</span>',					
			'date'           => __( 'Дата', 'usam' )			
        );		
        return $columns;
    }
}