<?php
class USAM_List_Table_Rates extends USAM_List_Table
{	
    protected $pimary_id = 'basic_currency';

	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}					
	
	function column_basic_currency( $item ) 
    {					
		$this->row_actions_table( $item->basic_currency, $this->standart_row_actions($item->basic_currency.'-'.$item->currency) );	
	}		
	
	function column_official_rate( $item ) 
	{				
		$rates = new USAM_ExchangeRatesCBRF( );	
		echo $rates->GetCrossRate( $item->basic_currency, $item->currency );
	}	
		
	protected function column_cb( $item ) 
	{				
		$checked = in_array( sanitize_title($item->basic_currency.'-'.$item->currency), $this->records )?"checked='checked'":"";
		echo "<input type='checkbox' name='cb[]' value='".$item->basic_currency.'-'.$item->currency."' ".$checked."/>";	
    }	
  
	function get_sortable_columns()
	{
		$sortable = array(
			'basic_currency'   => array('basic_currency', false),
			'currency'         => array('currency', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',	
			'basic_currency'   => __( 'Базовая валюта', 'usam' ),
			'currency'         => __( 'Валюта', 'usam' ),
			'rate'             => __( 'Курс', 'usam' ),	
			'date_update'      => __( 'Дата обновления', 'usam' ),	
			'official_rate'    => __( 'Курс ЦБ', 'usam' ),				
        );		
        return $columns;
    }	
	
	function prepare_items() 
	{	
		global $wpdb;
		
		$offset = ($this->get_pagenum() - 1) * $this->per_page;		
		$this->limit = ( $this->per_page !== 0 ) ? "LIMIT {$offset}, {$this->per_page}" : '';		
						
		if ( !empty( $this->records ) )
		{
			$basic_currency = array();
			$currency = array();
			foreach ( $this->records as $id )
			{
				$string = explode( '-' , $id );
				$basic_currency[] = $string[0];
				$currency[] = $string[1];
			}
			$this->where[] = "basic_currency IN ('" . implode( "','", $basic_currency ) . "') AND currency IN ('" . implode( "','", $currency ) . "')";			
		}
		else
			$this->where = array("1 = '1'");	
	
		$where = implode( ' AND ', $this->where );
		
		$this->items = $wpdb->get_results( "SELECT * FROM ". USAM_TABLE_CURRENCY_RATES ." WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}");
		$this->total_items = $wpdb->get_var( "SELECT COUNT(*) FROM `".USAM_TABLE_CURRENCY_RATES."` WHERE {$where}" );	
		
		$this->_column_headers = $this->get_column_info();	
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );		
	}
}