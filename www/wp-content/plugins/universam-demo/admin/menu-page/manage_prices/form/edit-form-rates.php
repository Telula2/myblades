<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Rates extends USAM_Edit_Form
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить валютный курс','usam');
		else
			$title = __('Добавить валютный курс', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$string = explode( '-' , $this->id );
			$rate = usam_get_currency_rate( $string[0], $string[1] );
			
			$this->data = array( 'basic_currency' => mb_strtoupper($string[0]), 'currency' => mb_strtoupper($string[1]), 'rate' => $rate );
		}
		else
		{
			$this->data = array( 'basic_currency' => '', 'currency' => '', 'rate' => 1 );
		} 				
	}	
	
	function show_setting()
	{			
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>				
				<tr>
					<td><?php _e( 'Базовая валюта', 'usam' ) ?>:</td>
					<td><?php usam_select_currencies( $this->data['basic_currency'], array('name' => 'basic', 'class' => 'chzn-select') ); ?></td>
				</tr>		
				<tr>
					<td><?php _e( 'Валюта', 'usam' ) ?>:</td>
					<td><?php usam_select_currencies( $this->data['currency'], array('name' => 'currency', 'class' => 'chzn-select') ); ?></td>
				</tr>					
				<tr>
					<td><?php _e( 'Курс', 'usam' ) ?>:</td>
					<td><input type="text" style="width:100px" name="rate" value="<?php echo $this->data['rate']; ?>" /></td>
				</tr>
				</tr>				
			</tbody>
		</table>		
		<?php	
	}

	function display_left()
	{			
		usam_add_box( 'usam_show_setting', __('Настройки','usam'), array( $this, 'show_setting' ) );			
    }
}
?>