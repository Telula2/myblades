<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_underprice extends USAM_Edit_Form
{			
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_underprice_rules');
		}
		else
		{
			$this->data = array( 'title' => '', 'value' => 0, 'category' => array(), 'brands' => array(), 'category_sale' => array(), 'type_prices' => array() );
		} 				
	}	
	
	function display_options( )
	{			
		?>		
		<table class="subtab-detail-content-table usam_edit_table" >	
			<tr>
				<td class="name"><?php esc_html_e( 'Наценка', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['value']; ?>" name="value"/></td>				
			</tr>						
		</table>		
		<?php
	}		
	
	public function terms_settings( ) 
	{		
		?>		  
		<div class="container_column">		
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите группы', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие группы установить налог.', 'usam') ?></p>
			</div>			
			<div id="all_taxonomy" class="all_taxonomy">	
				<?php
				$this->display_meta_box_group( 'category', $this->data['category'] ); 
				$this->display_meta_box_group( 'brands', $this->data['brands'] ); 
				$this->display_meta_box_group( 'category_sale', $this->data['category_sale'] ); 				
				?>						
			</div>					
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>	
		</div>			
	   <?php   
	}
	  
	
	function display_left()
	{	
		$this->titlediv( $this->data['title'] );			
		usam_add_box( 'usam_options', __('Настройки','usam'), array( $this, 'display_options' ) );	
		usam_add_box( 'usam_prices', __('Типы цен','usam'), array( $this, 'selecting_type_prices' ), $this->data['type_prices'] );		
		usam_add_box( 'usam_terms_settings', __('Группы товаров','usam'), array( $this, 'terms_settings' ) );
    }	
}
?>