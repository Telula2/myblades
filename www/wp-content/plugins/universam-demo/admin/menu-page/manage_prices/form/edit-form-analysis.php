<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_analysis extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить информацию','usam');
		else
			$title = __('Добавить новую информацию', 'usam');	
		return $title;
	}

	function setting()
	{			
		if ( $this->id != null )
		{
			global $wpdb;
			$record = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE id = '".$this->id."'", ARRAY_A );	
			$record['sku'] = usam_get_product_meta( $record['product_id'], 'sku' );
		}		
		else	
			$record = array( 'sku' => '', 'price_found' => '', 'url' => '', 'description' => '' );	

		?>		
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>
				<tr>
					<td class="subtab-detail-name"></td>
					<td>
						<?php			
						if ( $this->id == null )
						{	
								$autocomplete = new USAM_Autocomplete_Forms( );
								$autocomplete->get_form_product( );
						}
						else
						{
							echo get_the_title( $record['product_id'] );
						}
						?>				
						
					</td>
				</tr>			
				<tr>
					<td class="subtab-detail-name"><?php _e('Найденная цена','usam'); ?>:</td>
					<td>				
						<input type="text" style="width:100px" name="record[price_found]" value="<?php echo $record['price_found']; ?>"/>
					</td>
				</tr>			
				<tr>
					<td class="subtab-detail-name"><?php _e('Ссылка на сайт','usam'); ?>:</td>
					<td>
						<input type="text" style="width:100%" name="record[url]" value="<?php echo $record['url']; ?>"/>
					</td>
				</tr>
				<tr>
					<td  class="subtab-detail-name"><?php _e('Описание','usam'); ?>:</td>
					<td>
						<textarea cols="35" rows="3" class="typearea" name="record[description]" wrap="virtual"><?php echo $record['description']; ?></textarea>
					</td>
				</tr>			
			</tbody>
		</table>			
		<?php	
    }	
	
	function display_left()
	{
		usam_add_box( 'usam_setting', __('Информация о товаре','usam'), array( $this, 'setting' ) );	
	}	
}
?>