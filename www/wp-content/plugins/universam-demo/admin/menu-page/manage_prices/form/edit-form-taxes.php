<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Taxes extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить налог %s','usam'), $this->data['name'] );
		else
			$title = __('Добавить налог', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{
			$_tax = new USAM_Tax( $this->id );
			$this->data = $_tax->get_data();			
		}
		else	
		{
			$this->data = array( 'name' => '', 'description' => '', 'active' => 0, 'sort' => 10, 'type_payer' => '', 'is_in_price' => 0, 'value' => 0, 'setting' => array( 'locations' => array(), 'category' => array(),'brands' => array() ));	
		}
	}
	
	function display_left()
	{							
		$this->titlediv( $this->data['name'] );
		$this->add_box_description( $this->data['description'] );		
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_settings', __('Основные параметры','usam'), array( $this, 'settings' ) );			
		usam_add_box( 'usam_terms_settings', __('Группы товаров','usam'), array( $this, 'terms_settings' ) );			
		usam_add_box( 'usam_locations', __('Местоположение','usam'), array( $this, 'selecting_locations' ), $this->data['setting']['locations'] );
    }	
	
	public function terms_settings( ) 
	{		
		?>		  
		<div class="container_column">		
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите группы', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие группы установить налог.', 'usam') ?></p>
			</div>			
			<div id="all_taxonomy" class="all_taxonomy">	
				<?php
				$this->display_meta_box_group( 'category', $this->data['setting']['category'] ); 
				$this->display_meta_box_group( 'brands', $this->data['setting']['brands'] ); 
				?>						
			</div>					
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>	
		</div>			
	   <?php   
	}
		
	function settings()
	{
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>															
			<tr>							
				<td class ="title_option"><?php _e( 'Тип плательщика', 'usam' ); ?>:</td>
				<td class ="value_option">					
					<select name="tax[type_payer]">				
						<option value="" <?php echo ($this->data['type_payer'] == '') ?'selected="selected"':''?> ><?php _e( 'Любой', 'usam' ); ?></option>
						<?php				
						$types_payers = usam_get_group_payers();	
						foreach( $types_payers as $value )
						{						
							?>               
							<option value="<?php echo $value['id']; ?>" <?php echo ($this->data['type_payer'] == $value['id']) ?'selected="selected"':''?> ><?php echo $value['name']; ?></option>
							<?php
						}
						?>
					</select>	
				</td>
			</tr>		
			<tr>							
				<td class ="title_option"><?php _e( 'Ставка', 'usam' ); ?>:</td>
				<td class ="value_option">					
					<input type="text" name="tax[value]" maxlength = "12" size = "12" value="<?php echo $this->data['value']; ?>"/>
				</td>
			</tr>	
			<tr style = "display:none">							
				<td class ="title_option"><?php _e( 'Входит в цену', 'usam' ); ?>:</td>
				<td class ="value_option">					
					<select name="tax[is_in_price]">
						<option value="1"><?php _e( 'Да', 'usam' ); ?></option>
						<option value="0"><?php _e( 'Нет', 'usam' ); ?></option>						
					</select>								
				</td>			
			</tr>			
			</tbody>
		</table>	
		<?php
    }
}
?>