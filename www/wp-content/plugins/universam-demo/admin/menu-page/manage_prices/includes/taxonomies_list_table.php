<?php
require_once(ABSPATH . 'wp-admin/includes/class-wp-terms-list-table.php');
class USAM_List_Table_Taxonomies extends WP_Terms_List_Table
{	
	private $per_page;	
	public function __construct( $args = array() ) 
	{
		global $post_type, $taxonomy, $action, $tax;
		
		parent::__construct( array(	
			'plural' => 'tags',
			'singular' => 'tag',
			'screen' => isset( $args['screen'] ) ? $args['screen'] : null,
		) );
		
		$action    = $this->screen->action;
		$post_type = $this->screen->post_type;
		$taxonomy  = $this->screen->taxonomy = $args['taxonomy'];
		
		if ( empty( $taxonomy ) )
			$taxonomy = 'post_tag';

		if ( ! taxonomy_exists( $taxonomy ) )
			wp_die( __( 'Invalid taxonomy' ) );

		$tax = get_taxonomy( $taxonomy );

		// @todo Still needed? Maybe just the show_ui part.
		if ( empty( $post_type ) || !in_array( $post_type, get_post_types( array( 'show_ui' => true ) ) ) )
			$post_type = 'post';	
	}	
	
	public function set_per_page( $per_page )
	{		
		$this->per_page = (int) $per_page;		
	}
	
	public function extra_tablenav( $which ) 
	{		
		if ( 'top' == $which )
		{
			global $type_price;
			
			submit_button( __( 'Сохранить', 'usam' ), 'primary','action', false, array( 'id' => 'save-submit' ) );
			echo '<img src="'.esc_url( admin_url( 'images/wpspin_light.gif' ) ).'" id = "all-ajax-loading" class="ajax-loading"/>';	

			usam_get_select_prices( $type_price );
			submit_button( __( 'Выбрать', 'usam' ), 'secondary','action', false, array( 'id' => 'filter-submit' ) );							
		}		
	}
	
	function column_margins( $item ) 
	{				
		?>
		<div id="discount_terms-<?php echo $item->term_id; ?>">
			<?php				
			$trade_margins = get_term_meta( $item->term_id, 'trade_margins', true );
			$type_prices = usam_get_prices( );
			foreach ( $type_prices as $type )			
			{			
				if ( !isset($trade_margins[$type['code']]) )
					$margin = '';
				else
					$margin = $trade_margins[$type['code']];
				echo $type['title']." - $margin<br>";
			}
			?>
		</div>
		<?php		
    }
	
	function column_action( $item ) 
	{		
		global $type_price;		
		
		$trade_margins = get_term_meta( $item->term_id, 'trade_margins', true );
		$margin = '';
		if ( isset($trade_margins[$type_price]) )
			$margin = $trade_margins[$type_price];
		?>
		<input id = "vdiscount-<?php echo $item->term_id; ?>" item_id = "<?php echo $item->term_id; ?>" type="text" maxlength = "5" class="vdiscount show_change <?php echo ( $margin == 0 ?'':'input_selected'); ?>" name="trade_margins[<?php echo $item->term_id; ?>]" value="<?php echo $margin; ?>" />		
		<?php	
    }
		   	 
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'       => array('name', false),					
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'            => '<input type="checkbox" />',				
			'name'          => __('Название', 'usam' ),			
			'margins'       => __('Наценка', 'usam' ),				
			'action'        => '',					
        );		
        return $columns;
    }
	
	public function prepare_items() 
	{				
		$args = array(
			'search' => $this->search,
			'page' => $this->get_pagenum(),
			'number' => $this->per_page,
			'orderby' => $this->orderby,
			'order' => $this->order,
		);
		$this->callback_args = $args;
		$this->set_pagination_args( array(
			'total_items' => wp_count_terms( $this->screen->taxonomy, compact( 'search' ) ),
			'per_page' => $this->per_page,
		) );
	}
}