<?php
class USAM_Tab_underprice extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Наценки', 'usam'), 'description' => __('Здесь Вы можете установить наценки на товар.','usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
    {			
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;				
				$option = get_option('usam_underprice_rules',array());
				$rules = maybe_unserialize( $option );				
				foreach ( $this->records as $id )
				{
					unset($rules[$id]);
					$i++;													
				}
				update_option('usam_underprice_rules', $rules);		

				usam_recalculate_price_products();				
			break;			
			case 'save':
				if ( !empty($_POST['name']) )
				{						
					$new_rule['title'] =  sanitize_text_field(stripcslashes($_POST['name']));			
					$new_rule['value'] = (int)$_POST['value'];
					$new_rule['category'] = isset($_POST['tax_input']['usam-category'])?array_map('intval', $_POST['tax_input']['usam-category']):array();
					$new_rule['category_sale'] = isset($_POST['tax_input']['usam-category_sale'])?array_map('intval', $_POST['tax_input']['usam-category_sale']):array();
					$new_rule['brands'] = isset($_POST['tax_input']['usam-brands'])?array_map('intval', $_POST['tax_input']['usam-brands']):array();			
					$new_rule['type_prices'] = isset($_POST['input-type_prices'])?stripslashes_deep($_POST['input-type_prices']):array();	

					if ( $this->id != null )	
						usam_edit_data( $new_rule, $this->id, 'usam_underprice_rules' );	
					else			
					{							
						$this->id = usam_add_data( $new_rule, 'usam_underprice_rules' );				
					}						
					usam_recalculate_price_products();					
				}		
			break;			
		}				
	}
}