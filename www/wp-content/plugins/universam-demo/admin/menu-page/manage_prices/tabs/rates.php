<?php
class USAM_Tab_Rates extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Валютные курсы', 'usam'), 'description' => 'Здесь вы можете управлять валютными курсами.' );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;	
		switch( $this->current_action )
		{			
			case 'delete':					
				foreach ( $this->records as $id )
				{
					$string = explode( '-' , $id );					
					$result = $wpdb->delete( USAM_TABLE_CURRENCY_RATES, array('basic_currency' => $string[0], 'currency' => $string[1] ), array( '%s','%s' ) );
				}
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ), ), $this->sendback );
				$this->redirect = true;	
			break;
			case 'save':					
				if ( isset( $_POST['rate'] ) ) 
				{	
					global $wpdb;
					
					$basic = substr(sanitize_text_field($_POST['basic']), 0, 3);
					$currency = substr(sanitize_text_field($_POST['currency']), 0, 3);
					$rate = sanitize_text_field($_POST['rate']);
					$date = date("Y-m-d H:i:s" );
					
					$sql = "INSERT INTO `".USAM_TABLE_CURRENCY_RATES."` (`basic_currency`,`currency`,`rate`,`date_update`) VALUES ('%s','%s','%f','%s') ON DUPLICATE KEY UPDATE `rate`='%f', `date_update`='%s'";					
					$insert = $wpdb->query( $wpdb->prepare($sql, $basic, $currency, $rate, $date, $rate, $date ) );	
					
					usam_recalculate_price_products();					
				}
			break;
		}				
	}	
	
	public function tab_structure() 
	{			
		if ( is_object($this->item_table) )
		{
			$this->item_table->display_form();
		}		
		else
		{
			$this->display_title();	
			?>			
			<div class = "base_currency">
				<strong><?php _e('Базовая валюта', 'usam') ?>: </strong><span><?php echo usam_get_currency_name( ); ?></span>
			</div>
			<?php
			$this->list_table->display_table();		
		}	
	}	
	
	public function get_message()
	{		
		$message = '';
		
		if( isset($_REQUEST['update_currency']) )				
			$message = sprintf( _n( 'Обновлена %s валюта.', 'Обновлено %s валют.', $_REQUEST['update_currency'], 'usam' ), $_REQUEST['update_currency'] );				
		
		return $message;
	} 
}