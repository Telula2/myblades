<?php
class USAM_Tab_products extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Управление ценой', 'usam'), 'description' => __('Здесь Вы можете изменить цену у товаров в один клик.','usam') );
	}
	
	protected function load_tab()
	{ 
		$this->product_list_table();
	}
	
	public function select_product_price( ) 
	{				
		global $type_price;			
		$name_pice = usam_get_name_price_by_code( $type_price );
		?>
		<div class="usam_global_operation">	
			<h2><?php echo __('Цена для редактирования', 'usam').': <span class="edit_product_price">'.$name_pice; ?></span></h2>	
			<div class="global_operation">
				<div class="content">	
					<form method='GET' action='' id='usam_select_product_price_form'>
						<input type='hidden' value='<?php echo $this->page_name; ?>' name='page' />
						<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />					
						<table>
							<tr>
								<td><?php _e('Сменить тип цены', 'usam'); ?>:</td>
								<td><?php usam_get_select_prices( $type_price, array( 'onChange' => 'this.form.submit()' ), false, array( 'base_type' => '0' ) ); ?></td>
							</tr>
						</table>
					</form>	
				</div>	
			</div>
		</div>
		<?php
	}
	
	public function tab_structure() 
	{			
		$this->display_title();	
		$this->display_help_center();
		$this->select_product_price();			
		$this->display_tables_select();		
		$this->list_table->display_table();		
	}	
	
	protected function callback_submit()
    {			
		switch( $this->current_action )
		{
			case 'save':
				
			break;			
		}				
	}
}