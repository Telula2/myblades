<?php
class USAM_Tab_analysis extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Алализ цен конкурентов', 'usam'), 'description' => __('Здесь Вы можете собирать информацию о ценах у конкурентов.', 'usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;	
		switch( $this->current_action )
		{
			case 'delete':
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_PRICE_COMPARISON." WHERE id IN ($in)" );				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;		
			case 'save':
				if ( isset($_POST['record']) )
				{
					$record = stripslashes_deep($_POST['record']);	
					if ( $this->id != null )
					{
						$data  = array( 'price_found' => sanitize_text_field($record['price_found']), 'description' => sanitize_text_field($record['description']), 'url' => sanitize_text_field($record['url']) );
						usam_update_price_comparison( $this->id, $data );
					}	
					else				
					{								
						$data['status'] = 2;				
						$data['product_id'] = absint($_POST['product']);
						$data['type'] = 'M'; //сделан менеджером
						
						usam_insert_price_comparison( $data );
					}	
				}
			break;
		}
	}
}