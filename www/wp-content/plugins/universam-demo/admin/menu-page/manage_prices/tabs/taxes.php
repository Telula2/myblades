<?php
class USAM_Tab_Taxes extends USAM_Tab
{	
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Управление налогами', 'usam'), 'description' => 'Здесь Вы можете добавить налоговые ставки.' );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}

	public function callback_submit() 
	{		
		switch( $this->current_action )
		{
			case 'delete':	
				global $wpdb;
				
				$in = implode( ', ', $this->records );	
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_TAXES." WHERE id IN ($in)");
				if ($result >= 1 )
					$i = count($this->records);		
				
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;			
			break;
			case 'save':
				if( !empty($_POST['tax']) )
				{			
					$insert = $_POST['tax'];	
					$insert['name'] = sanitize_text_field(stripslashes($_POST['name']));		
					$insert['description'] = sanitize_textarea_field( stripcslashes($_POST['description']) );	
					$insert['active']     = !empty($_POST['active'])?1:0;			
				
					$insert['setting']['locations'] = isset($_POST['locations'])?array_map('intval', $_POST['locations']):array();
					$insert['setting']['category'] = isset($_POST['tax_input']['usam-category'])?array_map('intval', $_POST['tax_input']['usam-category']):array();
					$insert['setting']['brands'] = isset($_POST['tax_input']['usam-brands'])?array_map('intval', $_POST['tax_input']['usam-brands']):array();
					
					if ( $this->id != null )
					{
						$_tax = new USAM_Tax( $this->id );
						$_tax->set( $insert );
					}
					else
					{ 
						$_tax = new USAM_Tax( $insert );
					}
					$_tax->save( );			
					$this->id = $_tax->get('id');
				}
			break;
		}
	}
}