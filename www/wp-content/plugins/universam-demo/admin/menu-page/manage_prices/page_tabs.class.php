<?php
/*
 * Отображение страницы Управление ценой
 */ 
  
$default_tabs = array(	
	array( 'id' => 'products', 'title' => __( 'Управление ценой', 'usam' ) ),
	array( 'id' => 'rates',    'title' => __( 'Валютные курсы', 'usam' ) ),
	array( 'id' => 'underprice', 'title' => __( 'Наценки', 'usam' ) ),
	array( 'id' => 'taxes', 'title' => __( 'Налоги', 'usam' ) ),
	array( 'id' => 'analysis', 'title' => __( 'Анализ', 'usam' ) ),
);

class USAM_Tab extends USAM_Page_Tab
{		
	protected function localize_script_tab()
	{
		return array(	
			'change_product_price_nonce'  => usam_create_ajax_nonce( 'change_product_price' ),				
		);
	}		
} 
