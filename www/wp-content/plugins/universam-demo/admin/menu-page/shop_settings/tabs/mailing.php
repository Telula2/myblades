<?php
class USAM_Tab_mailing extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Рассылка', 'usam'), 'description' => __('Здесь вы можете настроить рассылку Вашего магазина.','usam') );
	}	
	
	public function display() 
	{			
		$select_list = get_option('usam_add_users_to_mailing_list', '' );			
		$lists = usam_get_subscribers_list( );
		?>
		<table class='usam_setting_table usam_edit_table'>	
			<tr>
				<td class = "name"><?php esc_html_e( 'Добавлять подписчиков', 'usam' ); ?>:</th>
				<td>
					<select name='usam_options[usam_add_users_to_mailing_list]' id = "usam_add_users_to_mailing_list">
						<option value='' <?php echo ('' == $select_list)?"selected='selected'":'' ?>><?php esc_html_e( 'Не добавлять', 'usam' ); ?></option>	
						<?php	
							foreach ( $lists as $list )
							{
								?>							
								<option value='<?php echo $list['id']; ?>' <?php echo ($list['id'] == $select_list)?"selected='selected'":'' ?> ><?php echo $list['title']; ?></option>		
								<?php
							}
						?>														
					</select>	
				</td>
				<td class = "description"><?php esc_html_e( 'Добавлять подписчиков в списки для рассылки', 'usam' ); ?></td>
			</tr>						
		</table>	
		<?php
	}		
}