<?php
// Подобные товары
class USAM_Tab_Reviews extends USAM_Tab
{
	private $options;	
	public function __construct(  )
	{		
		$this->options = get_option('usam_reviews');
		
		$this->header = array( 'title' => __('Отзывы клиентов', 'usam'), 'description' => __("Отзывы клиентов позволяет Вашим клиентам и посетителям оставить отзывы или рекомендации ваших услуг. Отзывы являются Microformat включен и может помочь сканерам, таких как Google Local Search и Google Places для индексации этих отзывов.","usam") );		
	}	
	
	public function display() 
	{
		usam_add_box( 'usam_using', __( 'Использование', 'usam' ), array( $this, 'using' ) );		
		usam_add_box( 'usam_fields_reviews', __( 'Настройки формы отзыва', 'usam' ), array( $this, 'fields_reviews' ) );	
		usam_add_box( 'usam_page_reviews', __( 'Настройка страницы отзывов', 'usam' ), array( $this, 'page_reviews' ) );	
	}

	public function using() 
	{	
		?>
		<table class = "usam_setting_table usam_edit_table">
			<tr>	
				<td class = "name"><?php _e('Показать отзывы','usam'); ?>:</td> 
				<td><?php printf( __( 'Вставте %s в содержании поста или странице, где Вы хотели бы, чтобы отображались отзывы.','usam'), '[reviews]'); ?>
			</tr>
			<tr>	
				<td class = "name"><?php _e('Показать последние отзывы','usam'); ?>:</td> 
				<td>
					[USAM_RECENT_REVIEWS 
					POSTID="<span style="color:#00c;">ALL</span>" 
					NUM="<span style="color:#00c;">3</span>" 
					SNIPPET="" 
					MORE="" 
					HIDECUSTOM="<span style="color:#00c;">0</span>" 
					HIDERESPONSE="<span style="color:#00c;">0</span>"] - <?php _e('Вставте в содержании поста или странице, где Вы хотели бы, чтобы отображались последние отзывы.','usam'); ?>						
					<br /><br />
					POSTID="ALL" , <?php _e('чтобы показать последние отзывы от всех постов / страниц или','usam'); ?> POSTID="123", <?php _e('чтобы показать последние отзывы от записи / страницы с ID','usam'); ?> #123<br /><br />
					NUM="3"  <?php _e('покажет максимум 3 отзыва (без разбиения на страницы)','usam'); ?>.<br /><br />
					SNIPPET="140" <?php _e('покажет только первые 140 символов из обзора','usam'); ?>.<br /><br />
					MORE="view more" <?php _e('покажет &laquo;... просмотреть еще&raquo; со ссылкой на сам обзор на соответствующей странице','usam'); ?>.<br /><br />
					HIDECUSTOM="1" <?php _e('скроет все пользовательские поля в короткий выход.','usam'); ?><br /><br />
					HIDERESPONSE="1" <?php _e('скроет ответ администратора на отзыв','usam'); ?>
					</small>
				</td>
			</tr>
		</table>
		<?php
	}
	
	public function page_reviews() 
	{		 	
		?>
		<table class='usam_setting_table usam_edit_table'>
			<tr>
				<td class = "name"><label for="goto_show_button"><?php _e('Показать форму отзыва','usam') ?> : </label></td>
				<td><input type="checkbox" id="goto_show_button" name="usam_options[usam_reviews][goto_show_button]" value="1" <?php echo checked( !empty($this->options['goto_show_button']) ); ?>  /></td>
			</tr>		
			<tr>
				<td class = "name"><label for="hreview_type"><?php _e('Формат отзыва','usam'); ?> : </label>
				<td>
					<select id="hreview_type" name="usam_options[usam_reviews][hreview_type]">
						<option <?php if ($this->options['hreview_type'] == 'business') { echo "selected"; } ?> value="business"><?php _e('Бизнес','usam'); ?></option>
						<option <?php if ($this->options['hreview_type'] == 'product') { echo "selected"; } ?> value="product"><?php _e('Продукт','usam'); ?></option>
					</select>
				</td>
				<td><small><?php _e('Если этот параметр установлен на "Бизнес", плагин будет представить все отзывы, как будто они отзыва вашего бизнеса, как перечисленные выше','usam')?> .</small></td>
			</tr>
			<tr>
				<td class = "name"><label for="reviews_per_page"><?php _e('Количество отзывов на странице','usam')?> : </label></td>
				<td><input style="width:40px;" type="text" id="reviews_per_page" name="usam_options[usam_reviews][reviews_per_page]" value="<?php echo $this->options['reviews_per_page']?> " /></td>
			</tr>
			<tr>
				<td class = "name"><label for="form_location"><?php _e('Расположение формы отзыва','usam')?> : </label></td>
				<td>
					<select id="form_location" name="usam_options[usam_reviews][form_location]">
						<option <?php if ($this->options['form_location'] == 0) { echo "selected"; } ?> value="0"><?php _e('Над отзывами','usam')?> </option>
						<option <?php if ($this->options['form_location'] == 1) { echo "selected"; } ?> value="1"><?php _e('Под отзывами','usam')?> </option>
					</select>
				</td>
			</tr>	
			<tr>
				<td class = "name"><label for="enable_pages_default"><?php _e('Включить для новых страниц','usam') ?>:</label></td>
				<td><input id="enable_pages_default" name="usam_options[usam_reviews][enable_pages_default]" type="checkbox" <?php checked(!empty($this->options['enable_pages_default'])); ?>  value="1" /></td>
				<td><small><?php _e('Включить по умолчанию для новых страниц','usam')?> .</small></td>
			</tr>	
			<tr>
				<td class = "name"><label for="enable_posts_default"><?php _e('Включить для новых сообщений','usam')?>:</label></td>
				<td><input id="enable_posts_default" name="usam_options[usam_reviews][enable_posts_default]" type="checkbox" <?php checked(!empty($this->options['enable_posts_default'])); ?>  value="1" /></td>
				<td><small><?php _e('Включить по умолчанию для новых сообщений','usam')?> .</small></td>
			</tr>	
			<tr>
				<td class = "name"><label for="show_hcard_on"><?php _e('Включить вывод Бизнес hCard на','usam'); ?> : </label></td>
				<td>
					<select id="show_hcard_on" name="usam_options[usam_reviews][show_hcard_on]">
						<option <?php if ($this->options['show_hcard_on'] == 4) { echo "selected"; } ?> value="1"><?php _e('всех постах, страницах и товарах','usam'); ?> </option>
						<option <?php if ($this->options['show_hcard_on'] == 1) { echo "selected"; } ?> value="1"><?php _e('всех постах и страницах','usam'); ?> </option>
						<option <?php if ($this->options['show_hcard_on'] == 2) { echo "selected"; } ?> value="2"><?php _e('главной и странице отзывов','usam')?> </option>
						<option <?php if ($this->options['show_hcard_on'] == 3) { echo "selected"; } ?> value="3"><?php _e('только на странице отзывов','usam')?> </option>
						<option <?php if ($this->options['show_hcard_on'] == 0) { echo "selected"; } ?> value="0"><?php _e('нигде','usam')?> </option>
					</select>
				</td>
				<td class='description'><?php _e('Это включит Микроформат hCard, который включает в себя вашу контактную информацию бизнеса. Это рекомендуется включить для всех сообщений, страниц и товаров','usam')?>.</td>
			</tr>	
			<tr>
				<td class = "name"><label for="show_hcard"><?php _e('Видимость Бизнес hCard','usam')?> : </label></td>
				<td>
					<select id="show_hcard" name="usam_options[usam_reviews][show_hcard]">
						<option <?php if ($this->options['show_hcard'] == 0) { echo "selected"; } ?> value="0"><?php _e('Скрыть для всех посетителей','usam'); ?></option>
						<option <?php if ($this->options['show_hcard'] == 1) { echo "selected"; } ?> value="1"><?php _e('Показывать для всех посетителей','usam')?> </option>
					</select>
				</td>
				<td class='description'><?php _e('На экране появится hCard для посетителей (и поисковые системы). Поисковые системы, как правило, игнорируют микроформат информации, которая скрыта, так что, как правило, хорошая идея, чтобы установить это "показ". "Показать" будет влиять только отдельные на включенные страницы / сообщения. Если вы хотите скрыть элементы hCard выходе индивидуальных стилей можно переопределить CSS вашей темы','usam'); ?>.</td>
			</tr>						
		</table>
		<?php
	}
	
	private function field( $tfield, $field ) 
	{			
		?>
		<tr>			
			<td><input type="text" value='<?php echo $field['title']; ?>' name='usam_options[usam_reviews][fields][<?php echo $tfield; ?>][title]'/></td>
			<td><input type='checkbox' value='1' name='usam_options[usam_reviews][fields][<?php echo $tfield; ?>][ask]' <?php echo checked( !empty($field['ask']) ); ?> /></td>	
			<td><input type='checkbox' value='1' name='usam_options[usam_reviews][fields][<?php echo $tfield; ?>][require]' <?php checked( !empty($field['require']) ); ?> /></td>
			<td><input type='checkbox' value='1' name='usam_options[usam_reviews][fields][<?php echo $tfield; ?>][show]' <?php echo checked( !empty($field['show']) ); ?> /></td>			
		</tr>
		<?php
	}
	
	public function fields_reviews() 
	{	  
		$default_fields = array( 
			'name' => array('title' => esc_html__( 'Имя', 'usam' ), 'ask' => 0, 'require' => 0, 'show' => 0 ), 
			'mail' => array('title' => esc_html__( 'Электронная почта', 'usam' ), 'ask' => 0, 'require' => 0, 'show' => 0 ), 			
			'title' => array('title' => esc_html__( 'Название отзыва', 'usam' ), 'ask' => 0, 'require' => 0, 'show' => 0 ),
			'custom_1' => array( 'title' => '', 'ask' => 0, 'require' => 0, 'show' => 0 ),
			'custom_2' => array( 'title' => '', 'ask' => 0, 'require' => 0, 'show' => 0 ),
			'custom_3' => array( 'title' => '', 'ask' => 0, 'require' => 0, 'show' => 0 ),
		);				
		$fields = !empty($this->options['fields'])?$this->options['fields']:$default_fields;		
		?> 
		<div id = "fields" class = "fields">
			<table class = "t_feedback_fields">				
				<thead>
					<tr>
						<th><?php echo esc_html_e( 'Название поля', 'usam' ); ?></th>
						<th><?php echo esc_html_e( 'Просить', 'usam' ); ?></th>
						<th><?php echo esc_html_e( 'Требовать', 'usam' ); ?></th>	
						<th><?php echo esc_html_e( 'Показывать', 'usam' ); ?></th>								
					</tr>
				</thead>
				<tbody>
				<?php	
					foreach( $fields as $type_field => $field )
					{					
						$this->field( $type_field, $field );
					}
				?>					
				</tbody>
			</table>						
		</div>	
		<?php
	}	
}