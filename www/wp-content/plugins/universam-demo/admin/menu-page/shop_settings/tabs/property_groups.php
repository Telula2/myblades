<?php
class USAM_Tab_property_groups extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Группы свойств', 'usam'), 'description' => __('Здесь вы можете настроить группы свойств заказа', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	

	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_ORDER_PROPS_GROUP." WHERE id IN ($in)" );		
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'save':			
				if ( !empty($_POST['name']) )
				{								
					$property_group['name'] = sanitize_text_field(stripcslashes($_POST['name']));		
					$property_group['type_payer'] = sanitize_text_field($_POST['type_payer']);
					$property_group['sort'] = (int)$_POST['sort'];					
					
					if ( $this->id != null )							
						usam_update_order_property_group( $property_group );		
					else										
						$this->id = usam_insert_order_props_group( $property_group );	
				}
			break;
		}	
	}
}