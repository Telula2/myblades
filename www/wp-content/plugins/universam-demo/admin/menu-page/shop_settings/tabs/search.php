<?php
class USAM_Tab_Search extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Поиск на сайте', 'usam'), 'description' => __('Здесь вы можете управлять параметрами поиска Вашего магазина.','usam') );
	}			

	public function display() 
	{		
		usam_add_box( 'usam_settings', __('Страница всех результатов','usam'), array( $this, 'display_settings' ) );
		usam_add_box( 'usam_settings', __('Раскрывающиеся поле поиска','usam'), array( $this, 'display_widget' ) );
	}
	
	public function display_settings() 
	{	
		_e('Для интеграции поиска в тему Вы можете использовать эту функцию', 'usam'); ?><code>&lt;?php usam_search_widget(); ?&gt;</code><?php
		
		$options = array( 
			array( 'title' => __( 'Количество символов описания', 'usam' ), 'default' => 100, 'attribute' => array( 'size' => "6" ), 'type' => 'input', 'option' => 'search_text_lenght', 'description' => __( "Число символов из описания продукта, которые показывать с каждым результатом поиска.", 'usam' ),),	
			array( 'title' => __( 'Артикул', 'usam' ), 'type' => 'checkbox', 'option' => 'search_sku_enable', 'description' => __( "Показать артикул продукта в результатах поиска", 'usam' ),),
			array( 'title' => __( 'В наличии', 'usam' ), 'type' => 'checkbox', 'option' => 'search_in_stock_enable', 'description' => __( "Показать наличие товара в результатах поиска.", 'usam' ),),
			array( 'title' => __( 'Цена', 'usam' ), 'type' => 'checkbox', 'option' => 'search_price_enable', 'description' => __( "Показать цену продукта в результатах поиска", 'usam' ),),		
			array( 'title' => __( 'Категория товара', 'usam' ), 'type' => 'checkbox', 'option' => 'search_categories_enable', 'description' => __( "Показать категории результатах поиска", 'usam' ),),	
		  );    
		$this->display_table_row_option( $options ); 	
	}
	
	public function display_widget() 
	{	
		$options = array( 
			array( 'title' => __( 'Текст окна поиска', 'usam' ), 'default' => 50, 'attribute' => array( 'size' => "50" ), 'type' => 'input', 'option' => 'search_box_text', 'description' => __( "Текст окна глобального поиска. &lt;empty&gt; ничего не показывать.", 'usam' ),),
			array( 'title' => __( 'Количество результатов', 'usam' ), 'default' => 5, 'attribute' => array( 'size' => "6" ), 'type' => 'input', 'option' => 'search_result_items', 'description' => __( "Количество результатов, которые нужно показать перед нажатием, чтобы увидеть все результаты.", 'usam' ),),	
			array( 'title' => __( 'Длина описания', 'usam' ), 'default' => 100, 'attribute' => array( 'size' => "6" ), 'type' => 'input', 'option' => 'search_text_lenght', 'description' => __( "Количество символов из описания продукта, которые показать в поле поиска в раскрывающемся списке. Значение по умолчанию 100.", 'usam' ),),
			array( 'title' => __( 'Длина названия', 'usam' ), 'default' => 50, 'attribute' => array( 'size' => "6" ), 'type' => 'input', 'option' => 'search_length_name_items', 'description' => __( "Число символов имени товара показать в раскрывающемся поле поиска.", 'usam' ),),
			array( 'title' => __( 'Глобальный поиск', 'usam' ), 'type' => 'checkbox', 'option' => 'search_global_search', 'description' => __( "Установить глобальный поиск или поиск в текущей категории продукта или текущего тега продукта. Отметьте, чтобы активировать глобальный поиск", 'usam' ),),	
		  );  
		$this->display_table_row_option( $options ); 	
	}
}