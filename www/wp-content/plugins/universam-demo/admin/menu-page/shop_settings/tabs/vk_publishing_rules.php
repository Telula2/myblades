<?php
class USAM_Tab_vk_publishing_rules extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Правила публикации вКонтакте', 'usam'), 'description' => __('Здесь Вы можете добавлять правила для автоматической публикации товаров как записи вКонтакте.','usam') );		
		$this->buttons = array( 'add' => __('Добавить группу', 'usam') );				
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_vk_publishing_rules' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;	
			case 'save':
				if ( !empty($_POST['name']) )
				{							
					$new['name'] =  sanitize_text_field(stripcslashes($_POST['name']));	
					$new['terms']['category'] = isset($_POST['tax_input']['usam-category'])?array_map('intval', $_POST['tax_input']['usam-category']):array();
					$new['terms']['category_sale'] = isset($_POST['tax_input']['usam-category_sale'])?array_map('intval', $_POST['tax_input']['usam-category_sale']):array();
					$new['terms']['brands'] = isset($_POST['tax_input']['usam-brands'])?array_map('intval', $_POST['tax_input']['usam-brands']):array();	
					$new['profiles'] = isset($_POST['input-vk_profiles'])?array_map('intval', $_POST['input-vk_profiles']):array();
					$new['groups'] = isset($_POST['input-vk_groups'])?array_map('intval', $_POST['input-vk_groups']):array();
					$new['active'] = !empty($_POST['active'])?1:0;						
					$new['start_date'] = usam_get_datepicker('start');
					$new['end_date'] = usam_get_datepicker('end');		
					$new['pricemin'] = (float)$_POST['pricemin'];	
					$new['pricemax'] = (float)$_POST['pricemax'];
					$new['minstock'] = absint($_POST['minstock']);		
					$new['quantity'] = absint($_POST['quantity']);	
					$new['exclude'] = absint($_POST['exclude']);					
					$new['periodicity'] = absint($_POST['periodicity']);	
					$new['from_hour'] = !empty($_POST['from_hour'])?absint($_POST['from_hour']):'';		
					$new['to_hour'] = !empty($_POST['from_hour'])?absint($_POST['to_hour']):'';
					
					if ( $this->id != null )	
					{				
						usam_edit_data( $new, $this->id, 'usam_vk_publishing_rules' );					
					}
					else			
					{							
						$this->id = usam_add_data( $new, 'usam_vk_publishing_rules' );		 
					}
				}				
			break;			
		}			
	}
}