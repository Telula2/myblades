<?php
class USAM_Tab_company_group extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Группы компаний', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить группы компаний.','usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );				
	}
		
	public function callback_submit() 
	{
		switch( $this->current_action )
		{
			case 'delete':						
				usam_delete_data( $this->records, 'usam_crm_company_group' );								
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':	
				if ( !empty($_POST['name']) )
				{		
					$new['name'] = sanitize_text_field( stripslashes($_POST['name']));		
					$new['sort'] = isset($_POST['sort'])?(int)$_POST['sort']:100;						
					if ( $this->id != null )	
					{				
						usam_edit_data( $new, $this->id, 'usam_crm_company_group' );	
					}
					else			
					{								
						$this->id = usam_add_data( $new, 'usam_crm_company_group' );				
					}
				}
			break;	
		}				
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
}