<?php
class USAM_Tab_payment_gateway extends USAM_Tab
{
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Платежные системы', 'usam'), 'description' => __('Здесь Вы можете добавить или изменить платежную систему.', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}

	public function callback_submit()
	{
		global $wpdb;
		
		switch( $this->current_action )
		{
			case 'delete':
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_PAYMENT_GATEWAY." WHERE id IN ($in)" );				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;
			case 'save':	
				if( !empty($_POST['name']) )
				{	
					$this->data = $_POST['payment_gateway'];	
					$this->data['name'] = sanitize_text_field(stripcslashes($_POST['name']));		
					$this->data['description'] = sanitize_textarea_field(stripcslashes($_POST['description']));	
					$this->data['active'] = !empty($_POST['active'])?1:0;	
					$this->data['img'] = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;	
					if ( $this->data['handler'] != '' && !empty($_POST['gateway_handler']) )
					{
						$this->data['setting']['gateway_option'] = $_POST['gateway_handler'];
					}	
					$this->data['setting']['condition']['shipping'] = !empty($_POST['input-selected_shipping'])?array_map('intval', $_POST['input-selected_shipping']):array();	
					$this->data['setting']['condition']['sales_area'] = !empty($_POST['input-sales_area'])?array_map('intval', $_POST['input-sales_area']):array();	
					$this->data['setting']['condition']['roles'] = !empty($_POST['input-roles'])?$_POST['input-roles']:array();	
					
					$formats = $this->get_formats();				
					
					$insert = $this->data;	
					$insert['setting'] = serialize($insert['setting']);	
					if ( $this->id != null )
					{
						$where  = array( 'id' => $this->id );
						$result_update = $wpdb->update( USAM_TABLE_PAYMENT_GATEWAY, $insert, $where, $formats );
					}
					else
					{
						$result_insert = $wpdb->insert( USAM_TABLE_PAYMENT_GATEWAY, $insert, $formats );
						$this->id = $wpdb->insert_id;
					}				
				}	
			break;			
		}
	}
	
	function get_formats( )
	{		
		$formats = array( 'name' => '%s', 'description' => '%s', 'active' => '%d', 'img' => '%d', 'sort' => '%d', 'debug' => '%d', 'type' => '%s','bank_account_id' => '%d', 'handler' => '%s', 'cashbox' => '%d', 'setting' => '%s' );
		$return_formats = array();
		foreach( $this->data as $key => $value) 				
		{
			if ( isset($formats[$key]) )
				$return_formats[] = $formats[$key];
			else
				unset($this->data[$key]);
		}
		return $return_formats;
	}	
}