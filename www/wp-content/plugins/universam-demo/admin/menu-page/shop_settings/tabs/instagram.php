<?php
class USAM_Tab_instagram extends USAM_Tab
{	
	private $instagram;
	public function __construct() 
	{
		$this->header = array( 'title' => __('Instagram', 'usam'), 'description' => __('Здесь вы можете настроить управление Вашей страницей в социальной сети Instagram.','usam') );
		
		$this->instagram = get_option('usam_instagram_api', array('client_id' => ''));			
		add_action('admin_footer', array(&$this, 'admin_footer'));	
	}
	
	function admin_footer( ) 
	{		
		$params = array( 'client_id' => $this->instagram['client_id'], 'redirect_uri' => admin_url('admin.php?unprotected_query=instagram_token'), 'response_type' => 'code' );
		
		$query = http_build_query($params);  	
		$url = 'https://api.instagram.com/oauth/authorize/?'.$query;	
		
		$html = "<div class='popButton'>
			<iframe src='$url' style='width:100%;'></iframe>
		</div>";
		echo usam_get_modal_window( __('Получить токин','usam'), 'open_window_get_token', $html );						
	}
	
	public function add_meta_options_help_center_tabs( ) 
	{
		$url = admin_url('admin.php?unprotected_query=instagram_token');
		$url_arr = explode(".", basename($url));
		$domain = $url_arr[count($url_arr)-2] . "." . $url_arr[count($url_arr)-1];
		
		$tabs[] = new USAM_Help_Center_Item('basic-help', __( 'Настройки API Instagram', 'usam' ), __( 'Настройки API Instagram', 'usam' ),
			array(
				'content' => '<p>' .sprintf(__( 'Чтобы получить доступ к API Instagram, вам нужно <b><a %s>создать приложение</a></b>', 'usam' ), 'href="https://www.instagram.com/developer/clients/manage" target="_blank"').':</p>		
		<p>В настройках приложения необходимо установить параметры в разделе <strong>Open API</strong>:</p>
		<ol>
			<li><strong>Базовый домен:</strong> '.$domain.'</li>
			<li><strong>Адрес сайта:</strong> '.$url.'</li>			
		</ol>',
			)
		);
		return $tabs;
	}	
	
	public function callback_submit() 
	{				
		parent::callback_submit( );		
		if (!empty($_POST['client_id']) )
		{
			$client_id = sanitize_text_field($_POST['client_id']);
			$client_secret = sanitize_text_field($_POST['client_secret']);			
			$instagram_api = array('client_id' => $client_id, 'client_secret' => $client_secret);
			update_option('usam_instagram_api', $instagram_api );
			$this->instagram = $instagram_api;				
		}	
		else
			$instagram = array();		
		
		update_option('usam_instagram', $instagram );	
	} 

	public function display() 
	{
		usam_add_box( 'usam_application', __( 'Приложение', 'usam' ), array( $this, 'application_meta_box' ) );	
	}	
	
	public function application_meta_box()
	{		
		?>			
		<table class = "usam_setting_table usam_edit_table">		
			<tr>				
				<td class="name"><label for='client_id'>Client ID:</label></td>
				<td><input type="text" name="client_id" id="client_id" value="<?php echo $this->instagram['client_id']; ?>"></td>
				<td class='description'><?php esc_html_e( 'Инструкции по созданию приложения смотри в помощи', 'usam' ); ?></td>
			</tr>	
			<tr>				
				<td class="name"><label for='client_secret'>Client Secret:</label></td>
				<td><input type="text" name="client_secret" id="client_secret" value="<?php echo $this->instagram['client_secret']; ?>"></td>
				<td class='description'><?php esc_html_e( 'Инструкции по созданию приложения смотри в помощи', 'usam' ); ?></td>
			</tr>
			<?php if ( !empty($this->instagram['client_id']) && empty($this->instagram['token'])){ ?>
			<tr>				
				<td class="name"></td>
				<td><button id="get_token" data-toggle="modal" data-target="#open_window_get_token" type='button' class='button-primary button'><?php _e( 'Получить токин', 'usam' ); ?></button></td>
				<td class='description'></td>
			</tr>
			<?php } ?>
			<?php if ( true || !empty($this->instagram['token'])){ ?>
			<tr>				
				<td class="name"><?php esc_html_e( 'Токин получен', 'usam' ); ?></td>
				<td></td>
				<td class='description'></td>
			</tr>
			<?php } ?>
		</table>		
		<?php		
	}	
}