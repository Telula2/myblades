<?php
class USAM_Tab_types_payers extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Типы плательщиков', 'usam'), 'description' => __('Здесь вы можете настроить типы плательщиков', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить правило', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	
	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_types_payers' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;		
			case 'save':
				$new['name'] =  sanitize_text_field(stripcslashes($_POST['name']));		
				$new['active'] = !empty($_POST['active'])?1:0;		
				$new['type'] = !empty($_POST['type']) && $_POST['type'] == 'I'?'I':'E';				
				$new['sort'] = isset($_POST['sort'])?(int)$_POST['sort']:100;	
					
				if ( $this->id != null )	
				{
					usam_edit_data( $new, $this->id, 'usam_types_payers' );	
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_types_payers' );				
				}			
			break;			
		}	
	}
}