<?php
class USAM_Tab_search_engine_region extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Регионы поисковых систем', 'usam'), 'description' => __('Регионы поисковых систем используются для определения позиции сайта в заданном регионе.','usam') );				
		$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}
			
	protected function load_tab()
	{		
		$this->list_table( );
	}
		
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				global $wpdb;
				$in = implode( ', ', $this->records );	
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_SEARCH_ENGINE_REGIONS." WHERE id IN ($in)");
				
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;	
			case 'save':		
				if( !empty($_POST['location']) )
				{	
					global $wpdb;			
				
					$this->data['location_id'] = absint($_POST['location']);	
					$this->data['name'] = sanitize_text_field(stripcslashes($_POST['name']));		
					$this->data['code'] = sanitize_textarea_field($_POST['code']);	
					$this->data['search_engine'] = sanitize_textarea_field($_POST['search_engine']);	
					$this->data['sort'] = absint($_POST['sort']);	
					$this->data['active'] = !empty($_POST['active'])?1:0;			
					
					$formats = $this->get_formats();					
					if ( $this->id != null )
					{
						$where  = array( 'id' => $this->id );	
						$result = $wpdb->update( USAM_TABLE_SEARCH_ENGINE_REGIONS, $this->data, $where, $formats );
					}
					else
					{
						$result = $wpdb->insert( USAM_TABLE_SEARCH_ENGINE_REGIONS, $this->data, $formats );
						$this->id = $wpdb->insert_id;
					}			
				}		
			break;				
		}			
	}	
	
	function get_formats( )
	{		
		$formats = array( 'location_id' => '%d', 'name' => '%s', 'code' => '%s', 'search_engine' => '%s', 'sort' => '%d', 'active' => '%d' );
		$return_formats = array();
		foreach( $this->data as $key => $value) 	
		{
			if ( isset($formats[$key]) )
				$return_formats[] = $formats[$key];
			else				
				unset($this->data[$key]);
		}
		return $return_formats;
	}
}