<?php
class USAM_Tab_search_engines extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Поисковые системы', 'usam'), 'description' => 'Здесь вы можете управлять параметрами поисковых систем.' );			
	}
	
	public function load_tab() 
	{
		if ( !empty( $_REQUEST['yandex_delete_token']) )
		{			
			$yandex = get_option('usam_yandex');
			$yandex['token'] = '';		
			update_option('usam_yandex', $yandex);			
			$this->sendback = remove_query_arg( array( 'yandex_delete_token' ), $this->sendback );	
			wp_redirect( $this->sendback );	
			exit;			
		}		
		if ( !empty( $_REQUEST['yandex_delete_client_id']) )
		{			
			$yandex = get_option('usam_yandex');
			$yandex['client_id'] = '';		
			$yandex['token'] = '';
			$yandex['metrika'] = array();
			update_option('usam_yandex', $yandex);			
			$this->sendback = remove_query_arg( array( 'yandex_delete_client_id' ), $this->sendback );	
			wp_redirect( $this->sendback );	
			exit;			
		}		
	}
		
	public function callback_submit() 
	{
		parent::callback_submit( );
		if ( !empty( $_POST['usam_yandex_metrika_active'] ) )
			update_option( 'usam_yandex_metrika_active', 1 );
		else
			update_option( 'usam_yandex_metrika_active', 0 );			
		
		$yandex = get_option('usam_yandex', '');	
		if (!empty( $_POST['yandex_client_id']))		
		{			
			$yandex['client_id'] = sanitize_text_field($_POST['yandex_client_id']);	
		}		
		if (!empty( $_POST['yandex_metrika']))		
		{		
			$yandex['metrika'] = array_map('intval', $_POST['yandex_metrika']);			
		}	
		if (!empty( $_POST['yandex_webmaster']))		
		{	
			$yandex['webmaster'] = array();
			$yandex['webmaster']['user_id'] = (int)$_POST['yandex_webmaster']['user_id'];			
			$yandex['webmaster']['site_id'] = sanitize_text_field($_POST['yandex_webmaster']['site_id']);
			$yandex['webmaster']['verify'] = sanitize_text_field($_POST['yandex_webmaster']['verify']);			
		}	
		if (!empty( $_POST['yandex_postoffice']))		
		{	
			$yandex['postoffice'] = sanitize_text_field($_POST['yandex_postoffice']);				
		}		
		if (!empty( $_POST['yandex_token']))		
		{			
			$yandex['token'] = sanitize_text_field($_POST['yandex_token']);			
		}	
		if (!empty( $_POST['yandex_maps']))		
		{			
			$yandex['maps']['api'] = sanitize_text_field($_POST['yandex_maps']);			
		}		
		if (!empty( $_POST['yandex_xml']))		
		{			
			$yandex['xml']['username'] = sanitize_text_field($_POST['yandex_xml']['username']);				
			$password = sanitize_text_field($_POST['yandex_xml']['password']);	
			if ( $password != '***' )
				$yandex['xml']['password'] = $password;	
		}					
		update_option('usam_yandex', $yandex);
	}

	public function display() 
	{		
		usam_add_box( 'usam_google', __( 'Google', 'usam' ), array( $this, 'google_meta_box' ) );
		usam_add_box( 'usam_yandex', __( 'Яндекс', 'usam' ), array( $this, 'yandex_meta_box' ) );		
		usam_add_box( 'usam_bing', __( 'Bing', 'usam' ), array( $this, 'bing_meta_box' ) );
	}

	public function yandex_meta_box() 
	{	
		$yandex = get_option('usam_yandex');
		if ( !empty($yandex['client_id']) )
		{				
			$params = array('response_type' => 'token', 'client_id' => $yandex['client_id'], 'device_id' => md5(uniqid(rand(),1)), 'device_name' => __("Модуль интернет-магазина Универсам","usam"), 'display' => 'popup', 'force_confirm' => 1);
			$query = http_build_query($params);  				
			?>	
			<script type="text/javascript">
				jQuery(document).ready(function() 
				{
					jQuery("#_yandex_token").hide();
				});			
				function makeAuth( )
				{			
					jQuery("#_yandex_token").show();
					jQuery("#_yandex_makeauth").hide();				
					window.open('https://oauth.yandex.ru/authorize?<?php echo $query;  ?>', '','height=500,width=500');			
				}
			</script>	
			<?php				
		}
		?>	
		
		<div id = "yandex" class = "usam_tabs usam_tabs_style1" >
			<div class = "header_tab">
				<ul>
					<li class = "tab"><a href='#yandex-passport'><?php _e('Паспорт в Яндексе','usam'); ?></a></li>
					<li class = "tab"><a href='#yandex-metrika'><?php _e('Яндекс Метрика','usam'); ?></a></li>	
					<li class = "tab"><a href='#yandex-webmaster'><?php _e('Яндекс Вебмастер','usam'); ?></a></li>
					<li class = "tab"><a href='#yandex-postoffice'><?php _e('Яндекс Почтовый офис','usam'); ?></a></li>
					<li class = "tab"><a href='#yandex-xml'><?php _e('Яндекс XML','usam'); ?></a></li>	
					<li class = "tab"><a href='#yandex-product-and-price'><?php _e('Яндекс Товары и цены','usam'); ?></a></li>		
				</ul>
			</div>	
			<div class = "countent_tabs">	
				
				<div id = "yandex-passport" class = "tab">						
					<table class = "usam_setting_table usam_edit_table">				
						<?php				
						if ( empty($yandex['client_id']) )
						{
							?>	
							<tr id ="_yandex_makeauth">
								<td class="name"><label for='yandex_client_id'><?php esc_html_e( 'Идентификатор приложения', 'usam' ); ?></label></td>
								<td><input id='yandex_client_id' value='' type='text' name='yandex_client_id' /></td>
								<td><span class='description'><?php printf( __( 'Идентификатор приложения. Доступен в %s', 'usam' ), '<a target="_blank" href="https://oauth.yandex.ru/">'.__( 'свойствах приложения', 'usam' ).'</a>'); ?></span></td>
							</tr>			
						<?php			
						}
						else
						{
						?>	
							<tr id ="_yandex_makeauth">
								<?php $url = add_query_arg( array( 'yandex_delete_client_id' => '1' ) ); ?>
								<td class="name"><label for='yandex_client_id'><?php esc_html_e( 'Идентификатор приложения', 'usam' ); ?></label></td>
								<td><?php echo $yandex['client_id']; ?></td>
								<td><span class='description'><a href="<?php echo esc_url( $url ); ?>"><?php _e( 'Сбросить', 'usam' ); ?></a></span></td>
							</tr>			
							<?php							
							if ( empty($yandex['token']) )
							{
						?>	
							<tr id ="_yandex_makeauth">
								<td class="name"><?php esc_html_e( 'Авторизоваться в Яндексе', 'usam' ); ?></td>
								<td><input type="button" onclick="makeAuth()" value="<?php esc_html_e( 'Авторизоваться в Яндексе', 'usam' ); ?>"></td>
								<td><span class='description'><?php esc_html_e( 'Вы должны авторизоваться, чтобы использовать сервисы Яндекса', 'usam' ); ?>.</span></td>
							</tr>
							<tr id ="_yandex_token">
								<td class="name"><label for='yandex_token'><?php esc_html_e( 'Код авторизации', 'usam' ); ?></label></td>
								<td><input id='yandex_token' value='' type='text' name='yandex_token' /></td>
								<td><span class='description'><?php _e( 'Введите полученный код авторизации', 'usam' ); ?>.</span></td>
							</tr>
				<?php				
							}
							elseif ( !empty($yandex['token']) )
							{
				 ?>
							<tr>
								<?php $url = add_query_arg( array( 'yandex_delete_token' => '1' ) ); ?>
								<td class="name"><?php _e( 'Токен Яндекс', 'usam' ); ?></td>					
								<td><?php echo $yandex['token']; ?></td>
								<td><?php _e( 'Чтобы интернет-магазин Универсам мог работать с сервисами Яндекса, необходим токен.', 'usam' ); ?> <a href="<?php echo esc_url( $url ); ?>"><?php _e( 'Сбросить токен', 'usam' ); ?></a></td>
							</tr>	
				<?php				
							}
						}
			 ?>			
					</table>					
				</div>	
				<div id = "yandex-webmaster" class = "tab">						
					<?php
					if ( !empty($yandex['token']) )
					{	
						require_once( USAM_FILE_PATH . '/includes/seo/yandex/webmaster.class.php' );		
						$site_id = empty($yandex['webmaster']['site_id'])?'':$yandex['webmaster']['site_id'];
						$verify = empty($yandex['webmaster']['verify'])?'':$yandex['webmaster']['verify'];
					
						$webmaster = new USAM_Yandex_Webmaster();
						$user_id = $webmaster->get_user();					
						$sites = $webmaster->get_sites( $user_id );		
						?>
						<input type="hidden" name="yandex_webmaster[user_id]" value="<?php echo $user_id; ?>"/>
						<table class = "usam_setting_table usam_edit_table">								
							<tr>
								<td class="name"><?php _e( 'Сайты', 'usam' ); ?></td>
								<td>						
								<?php 													
								if ( !empty($sites) ) 									
								{?>
									<table class="usam_edit_table">
										<?php 
										foreach ($sites as $site) 
										{?>
											<tr valign="top">
												<td><input id="site_id-<?php echo $site['host_id']; ?>" type="radio" name="yandex_webmaster[site_id]" value="<?php echo $site['host_id']; ?>" <?php checked( $site['host_id'], $site_id ); ?>/></td>
												<td><label for="site_id-<?php echo $site['host_id']; ?>"><?php echo $site['unicode_host_url']?></td>
											</tr>
										<?php 
										} ?>
									</table>
								<?php 
								}
								else 						
									_e( 'Нет доступных сайтов.', 'usam' );
								?>
								</td>
							</tr>	
							<tr>
								<td class="name"><label for='yandex_verify'><?php esc_html_e( 'Подтвердить права на сайт', 'usam' ); ?></label></td>
								<td><input id='yandex_verify' value='<?php echo $verify; ?>' type='text' name='yandex_webmaster[verify]' maxlength="50" size="50"/></td>
							</tr>										
						</table>
						<?php			
					}		
					?>
				</div>					
				<div id = "yandex-postoffice" class = "tab">						
					<?php
					if ( !empty($yandex['token']) )
					{	
						require_once( USAM_FILE_PATH . '/includes/seo/yandex/postoffice.class.php' );							
						$site_id = empty($yandex['postoffice'])?'':$yandex['postoffice'];					
					
						$postoffice = new USAM_Yandex_Postoffice();
						$sites = $postoffice->get_reg_list();
						?>					
						<table class = "usam_setting_table usam_edit_table">								
							<tr>
								<td class="name"><?php _e( 'Домены', 'usam' ); ?></td>
								<td>						
								<?php 													
								if ( !empty($sites) ) 									
								{?>
									<table class="usam_edit_table">
										<?php 
										foreach ($sites as $site) 
										{?>
											<tr valign="top">
												<td><input id="site_id-<?php echo $site['domain']; ?>" type="radio" name="yandex_postoffice" value="<?php echo $site['domain']; ?>" <?php checked( $site['domain'], $site_id ); ?>/></td>
												<td><label for="site_id-<?php echo $site['domain']; ?>"><?php echo $site['domain']?></td>
											</tr>
										<?php 
										} ?>
									</table>
								<?php 
								}
								else 						
									_e( 'Нет доступных сайтов.', 'usam' );
								?>
								</td>
							</tr>							
						</table>
						<?php			
					}		
					?>
				</div>					
				<div id = "yandex-metrika" class = "tab">						
					<?php
					if ( !empty($yandex['token']) )
					{	
						$counter_id = empty($yandex['metrika']['counter_id'])?'':$yandex['metrika']['counter_id'];
						?>
						<table class = "usam_setting_table usam_edit_table">					
							<tr>
								<td class="name"><label for='yandex_metrika_active'><?php esc_html_e( 'Включить Яндекс метрику', 'usam' ); ?></label></td>
								<td><input value='1' id='yandex_metrika_active' <?php checked( '1', get_option( 'usam_yandex_metrika_active' ) ); ?> type='checkbox' name='usam_yandex_metrika_active' /></td>
								<td><span class='description'><?php esc_html_e( 'Вы можете отправлять данные о посещениях с вашего сайта в яндекс метрику.', 'usam' ); ?></span></td>
							</tr>	
							<tr>
								<td class="name"><?php _e( 'Счетчик', 'usam' ); ?></td>
								<td>						
								<?php 								
								
								require_once( USAM_FILE_PATH . '/includes/seo/yandex/metrika.class.php' );
		
								$metrika = new USAM_Yandex_Metrika();
								$counters = $metrika->get_counters();						
								if ( !empty($counters) && is_array($counters) ) 									
								{?>
									<table class="usam_edit_table">
										<?php 
										foreach ($counters as $counter) 
										{?>
											<tr valign="top">
												<td><input type="radio" name="yandex_metrika[counter_id]" value="<?php echo $counter['id']; ?>" <?php checked( $counter['id'], $counter_id  ); ?>/></td>									
												<td><a href="https://metrika.yandex.ru/dashboard?id=<?php echo $counter['id']; ?>" target="_blank"><?php echo $counter['name']?></a></td>							
												<td><?php _e( 'Разрешение', 'usam' ); ?>:<?php echo $counter['permission']; ?></td>
											</tr>
										<?php 
										} ?>
									</table>
								<?php 
								}
								else 						
									_e( 'Нет доступных счетчиков.', 'usam' );
								?>
								</td>
							</tr>
							<tr>
								<td class="name"><label for='metrika_webvisor'><?php esc_html_e( 'Вебвизор, карта скроллинга, аналитика форм', 'usam' ); ?></label></td>
								<td>
									<input value='0' type='hidden' name='yandex_metrika[webvisor]' />
									<input value='1' id='metrika_webvisor' <?php checked('1', !empty($yandex['metrika']['webvisor'])); ?> type='checkbox' name='yandex_metrika[webvisor]' />
								</td>
								<td><span class='description'><?php esc_html_e( 'Запись и анализ поведения посетителей сайта.', 'usam' ); ?></span></td>
							</tr>	
							<tr>
								<td class="name"><label for='metrika_ecommerce'><?php esc_html_e( 'Электронная коммерция', 'usam' ); ?></label></td>
								<td>
									<input value='0' type='hidden' name='yandex_metrika[ecommerce]' />
									<input value='1' id='metrika_ecommerce' <?php checked('1', !empty($yandex['metrika']['ecommerce'])); ?> type='checkbox' name='yandex_metrika[ecommerce]' />
								</td>
								<td><span class='description'><?php esc_html_e( 'Отправка в Метрику данных электронной коммерции.', 'usam' ); ?></span></td>
							</tr>		
							<tr>
								<td class="name"><label for='metrika_asynchronous'><?php esc_html_e( 'Асинхронный код', 'usam' ); ?></label></td>
								<td>
									<input value='0' type='hidden' name='yandex_metrika[asynchronous]' />
									<input value='1' id='metrika_asynchronous' <?php checked('1', !empty($yandex['metrika']['asynchronous'])); ?> type='checkbox' name='yandex_metrika[asynchronous]' />
								</td>
								<td><span class='description'><?php esc_html_e( 'Асинхронный код не блокирует и не влияет на скорость загрузки страницы вашего сайта.', 'usam' ); ?></span></td>
							</tr>								
						</table>
						<?php			
					}		
					?>
				</div>	
				<?php 
				$username = !empty($yandex['xml']['username'])?$yandex['xml']['username']:'';
				$password = !empty($yandex['xml']['password'])?$yandex['xml']['password']:'';
				?>
				<div id = "yandex-xml" class = "tab">	
					<table class = "usam_setting_table usam_edit_table">
						<tr>
							<td class="name"><label for='yandex_xml_username'><?php esc_html_e( 'Имя пользователя', 'usam' ); ?></label></td>
							<td><input value='<?php esc_html_e($username); ?>' id='yandex_xml_username' type='text' name='yandex_xml[username]' /></td>
							<td><span class='description'><?php _e( 'Введите логин в Яндекс XML', 'usam' ); ?>.</span></td>
						</tr>
						<tr>
							<td class="name"><label for='yandex_xml_password'><?php esc_html_e( 'Пароль', 'usam' ); ?></label></td>
							<td><?php usam_get_password_input( $password, array( 'id' => 'yandex_xml_password', 'name' => 'yandex_xml[password]') ); ?></td>
							<td><span class='description'><?php _e( 'Введите пароль в Яндекс XML', 'usam' ); ?>.</span></td>
						</tr>
					</table>
				</div>	
				
				<div id = "yandex-product-and-price" class = "tab">	
					<?php $feed_url = add_query_arg( array( 'action' => 'feed', 'xmlformat' => 'yandex' ), home_url( '/' ) ); ?>				
					<p><?php printf( __( 'Чтобы импортировать товары в Яндекс, для отображения в результатах поиска, настройте %s. Потом добавьте URL', 'usam'), '<a href="https://webmaster.yandex.ru/site/info/market/" target="_blank">&laquo;Яндекс Товары и цены&raquo;</a>' ); ?>
					<a href="<?php echo esc_url( $feed_url ); ?>"><?php echo esc_url( $feed_url ); ?></a>
					</p>
				</div>	
				<div id = "yandex-maps" class = "tab">	
					<?php 
					$api = !empty($yandex['maps']['api'])?$yandex['maps']['api']:'';					
					?>
					<table class = "usam_setting_table usam_edit_table">
						<tr>
							<td class="name"><label for='yandex_xml_maps'><?php esc_html_e( 'API ключ', 'usam' ); ?></label></td>
							<td><input value='<?php esc_html_e($api); ?>' id='yandex_xml_maps' type='text' name='yandex_maps' /></td>
							<td><span class='description'><?php _e( 'Введите логин в Яндекс XML', 'usam' ); ?>.</span></td>
						</tr>						
					</table>
				</div>				
			</div>				
		</div>			
		<?php		
	}
	
	function google_meta_box()
	{	
		$google_feed_url = add_query_arg( array( 'action' => 'feed', 'xmlformat' => 'google' ), home_url( '/' ) ); 
		?>		
		<div id = "google" class = "usam_tabs usam_tabs_style1" >
			<div class = "header_tab">
				<ul>
					<li class = "tab"><a href='#google-webmaster'><?php _e('WebMasters','usam'); ?></a></li>		
					<li class = "tab"><a href='#merchant-centrer'><?php _e('Google Merchant Centrer','usam'); ?></a></li>
					<li class = "tab"><a href='#google-analytics'><?php _e('Google Analytics','usam'); ?></a></li>	
					<li class = "tab"><a href='#google-postmaster'><?php _e('Postmaster','usam'); ?></a></li>										
				</ul>
			</div>	
			<div class = "countent_tabs">	
				<div id = "google-webmaster" class = "tab">						
					<?php
					$options = array( 						
						array( 'key' => 'verify', 'type' => 'input', 'title' => __( 'Подтвердить права на сайт', 'usam' ), 'option' => 'google', 'attribute' => array( 'maxlength' => "50", 'size' => "50") ),	
					 );	   		
					?>	
					<?php $this->display_table_row_option( $options ); ?>	
				</div>	
				<div id = "merchant-centrer" class = "tab">						
					<p><?php printf( __( 'Чтобы импортировать товары в %s, чтобы они отображались в результатах Google Product Search, подпишитесь на Google Merchant Centrer учет и добавьте запланированный канал данных со следующим URL:', 'usam'), '<a href="http://www.google.com/merchants/" target="_blank">Google Product Search</a>' ); ?><a href="<?php echo esc_url( $google_feed_url ); ?>"><?php echo esc_url( $google_feed_url ); ?></a></p>	
				</div>	
				<div id = "google-analytics" class = "tab">						
					<?php 
					$options = array( 			
						array( 'type' => 'checkbox', 'title' => __( 'Включить', 'usam' ), 'option' => 'google_analytics_active', 'description' => __( "Вы можете отправлять данные о посещениях с вашего сайта в Google Analytics", 'usam' )),					
						array( 'key' => 'analytics_id', 'type' => 'input', 'title' => __( 'ID отслеживания', 'usam' ), 'option' => 'google', 'description' => __( 'Здесь введите Ваш идентификатор отслеживания', 'usam' ), 'attribute' => array( 'maxlength' => "50", 'size' => "50") ),					
						array( 'key' => 'analytics_ecommerce', 'type' => 'checkbox', 'title' => __( 'Электронная коммерция', 'usam' ), 'option' => 'google', 'description' => __( "Включить отслеживание электронной коммерции", 'usam' )),
					 );	 
					$this->display_table_row_option( $options ); 					
					?>						
				</div>	
				<div id = "google-postmaster" class = "tab">						
					<p><?php printf( __( 'Этот инструмент используется для отслеживания эффективности почтовых отправлений в домен gmail.com. Чтобы начать использовать Postmaster Вам понадобится актуальный аккаунт в приложении Google или почтовый ящик на gmail.com. Перейдите на %s, чтобы начать использовать .', 'usam'), '<a href="http://www.postmaster.google.com" target="_blank">Postmaster</a>' ); ?></p>	
				</div>				 
			</div>	
		</div>	
		<?php	
	}
	
	function bing_meta_box()
	{	
		$options = array( 						
			array( 'key' => 'verify', 'type' => 'input', 'title' => __( 'Подтвердить права на сайт', 'usam' ), 'option' => 'bing', 'attribute' => array( 'maxlength' => "50", 'size' => "50") ),	
		 );	  
		 $this->display_table_row_option( $options );
	}
}