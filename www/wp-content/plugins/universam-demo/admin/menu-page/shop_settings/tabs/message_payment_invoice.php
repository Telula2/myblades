<?php
class USAM_Tab_message_payment_invoice extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сообщение при получении счета на оплату', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить сообщения покупателю при получении счета на оплату.','usam') );	
	}	
	
	public function display() 
	{			
		?>   		
		<p><?php esc_html_e( 'Шорткоды, которые могут быть использованы', 'usam' );?>: <?php echo esc_html( '[current_day -1]' ); ?>, %date%, %closedate%, %document_number%, %totalprice%, %totalprice_currency%, %status%, %status_name%, %gateway_name%, %gateway_id%, %note%</p>
		<div class = "detailed_description">
			<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
			<div class = "description_box shortcodes">
				<span>%date%</span> - <?php esc_html_e( 'Дата документа', 'usam' ); ?></br>
				<span>%closedate%</span> - <?php esc_html_e( 'Срок оплаты', 'usam' ); ?></br>
				<span>%document_number%</span> - <?php esc_html_e( 'Номер документа', 'usam' ); ?></br>
				<span>%totalprice%</span> - <?php esc_html_e( 'Сумма документа', 'usam' ); ?></br>
				<span>%totalprice_currency%</span> - <?php esc_html_e( 'Сумма документа в валюте', 'usam' ); ?></br>
				<span>%status%</span> - <?php esc_html_e( 'Код статуса документа', 'usam' ); ?></br>
				<span>%status_name%</span> - <?php esc_html_e( 'Имя статкуса документа', 'usam' ); ?></br>
				<span>%note%</span> - <?php esc_html_e( 'Примечание', 'usam' ); ?></br>
				<span>%gateway_id%</span> - <?php esc_html_e( 'Номер шлюза оплаты', 'usam' ); ?></br>
				<span>%gateway_name%</span> - <?php esc_html_e( 'Имя шлюза оплаты', 'usam' ); ?></br>
				<span>[current_day +1]</span> - <?php esc_html_e( 'Текущая дата с разницей на указаное количество дней. Например, [current_day +1] - заменить на текущую дату плюс 1 день', 'usam' ); ?></br>
				<span>[if total_price=2000 {Показать этот текст}]</span> - <?php esc_html_e( 'Условный текст. Например, если сумма заказа равна 2000 покажет текст в фигурных скобках', 'usam' ); ?></br>						
			</div>
		</div>
		<table class='usam_setting_table usam_edit_table'>				
			<tr>
				<td colspan='2'><?php $this->display_order_labels(); ?></td>
			</tr>
			<?php $this->subject( 'payment_invoice_subject' ); ?>
			<?php $this->message( 'payment_invoice_message' ); ?>
		</table>
		<?php    
	}		
}