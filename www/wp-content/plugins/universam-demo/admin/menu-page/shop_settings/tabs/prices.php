<?php
class USAM_Tab_prices extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Цены', 'usam'), 'description' => __('Здесь Вы можете настроить цены, по которым вы хотите продавать товар.','usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':						
				global $wpdb;
				$prices = usam_get_prices( array('type' => 'all', 'ids' => $this->records) );	
				
				$codes = array();
				foreach ( $prices as $value )
				{
					$codes[] = USAM_META_PREFIX.'price_'.$value['code'];
					$codes[] = USAM_META_PREFIX.'old_price_'.$value['code'];
				}
				$meta_key = implode( "','", $codes); 				
				$deleted_rows = $wpdb->query( "DELETE FROM $wpdb->postmeta WHERE meta_key IN ('$meta_key')" );
				
				usam_delete_data( $this->records, 'usam_type_prices' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;	
			case 'save':			
				if ( !empty($_POST['price']['code']) )
				{		
					$new = $_POST['price'];
					$new['title'] = sanitize_text_field($_POST['name']);
					$new['base_type'] = isset($_POST['base_type'])?sanitize_title($_POST['base_type']):'0';			
														
					$new['locations'] = !empty($_POST['locations']) ? array_map('intval', $_POST['locations']) : array();
					$new['roles'] = !empty($_POST['input-roles']) ? stripslashes_deep($_POST['input-roles']) : array();													
					$new['underprice'] =  !empty($new['underprice'])?(int)$new['underprice']:0;						
					
					$new['date'] = date( "Y-m-d H:i:s" );			
					$new['available'] = !empty($new['available'])?1:0;		
					
					$prices = usam_get_prices( array('type' => 'all') );	
					foreach ( $prices as $value )
					{
						if ( $value['code'] == $new['code'] && $value['id'] != $this->id )
						{					
							global $current_screen;
							usam_set_user_screen_error( __('Код цены не уникальный. Измените код цены.','usam'), $current_screen->id );
							return false;				
						}
					}			
					if ( $this->id != null )	
					{ 			
						$result = usam_edit_data( $new, $this->id, 'usam_type_prices' );	
						if ( $result )
						{
							$this->redirect = true;
							return;
						}	
					}
					else			
					{								
						$this->id = usam_add_data( $new, 'usam_type_prices' );		
					}		
					usam_recalculate_price_products();					
				}
				else
				{
					global $current_screen;
					usam_set_user_screen_error( __('Код цены не указан.','usam'), $current_screen->id );
					return false;
				}	
			break;			
		}			
	}
}