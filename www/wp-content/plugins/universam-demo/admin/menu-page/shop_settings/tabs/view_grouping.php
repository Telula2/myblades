<?php
class USAM_Tab_view_grouping extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Группировка просмотра', 'usam'), 'description' => __('Здесь можно создать групировку просмотра заказов, чтобы разделить их обработку по отделам. Сами группы задаются в настройках пользователя.','usam') );		
		$this->buttons = array( 'add' => __('Добавить группировку', 'usam') );		
	}
		
	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data($this->records, 'usam_order_view_grouping');				
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;		
			case 'save':			
				$new = array();
				$new['name'] = sanitize_text_field(stripcslashes($_POST['name']));				
				$new['type_prices'] = isset($_POST['input-type_prices'])?stripslashes_deep($_POST['input-type_prices']):array();

				if ( $this->id != null )	
				{
					usam_edit_data( $new, $this->id, 'usam_order_view_grouping' );	
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_order_view_grouping' );				
				}	
			break;			
		}	
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
}