<?php
class USAM_Tab_crosssell extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Перекрестные продажи', 'usam'), 'description' => __('Здесь вы можете настроить автоматический подбор товаров.', 'usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}	
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	

	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_crosssell_conditions' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':	
				if ( !empty($_POST['crosssell']) )
				{		
					$crosssell = $_POST['crosssell'];		
					$crosssell['active'] = !empty($_POST['active'])?1:0;			
					$option = get_option('usam_crosssell_conditions', array() );						
					$crosssell_conditions = maybe_unserialize($option);		
					
					if ( !empty($_POST['conditions']) )
					{
						$conditions = array();				
						foreach($_POST['conditions']['type'] as $id => $type )
						{	
							if ( isset($_POST['conditions']['logic'][$id]) )
								$logic = $_POST['conditions']['logic'][$id];
							else
								continue;
							if ( isset($_POST['conditions']['value'][$id]) )
								$value = $_POST['conditions']['value'][$id];
							else
								continue;
							
							if ( isset($_POST['conditions']['logic_operator'][$id]) )
								$logic_operator = $_POST['conditions']['logic_operator'][$id];
							else
								$logic_operator = 'AND';
							$conditions[] = array( 'type' => $type, 'logic' => $logic, 'value' => $value, 'logic_operator' => $logic_operator );
						}		
					}
					if ( !empty($conditions) )
						$crosssell['conditions'] = $conditions;
					else
						$crosssell['active'] = 0;
					
					if ( $this->id != null )	
					{				
						usam_edit_data( $crosssell, $this->id, 'usam_crosssell_conditions' );				
					}
					else			
					{				
						$crosssell['date_insert'] = date( "Y-m-d H:i:s");	
						$this->id = usam_add_data( $crosssell, 'usam_crosssell_conditions' );							
					}			
				}
			break;			
		}	
	}
}