<?php
class USAM_Tab_vk_users_profiles extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Анкеты пользователей вКонтакте', 'usam'), 'description' => __('Здесь Вы можете добавлять анкеты пользователей вКонтакте для публикации товаров и записей.','usam') );		
		$this->buttons = array( 'add' => __('Добавить анкету', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_vk_profile' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
			break;	
			case 'save':
				if ( !empty($_POST['profile_id']) )
				{		
					$new['page_id'] = absint($_POST['profile_id']);	
					$new['access_token'] = sanitize_text_field($_POST['access_token']);	

					$params = array(
						'uids' => $new['page_id'],
						'fields' => 'photo_50',
						'name_case' => 'nom',
					); 			
					$users = usam_vkontakte_send_request( $params, 'users.get' );
								
					$new['last_name'] = $users[0]['last_name'];
					$new['photo_50'] = $users[0]['photo_50'];
					$new['first_name'] = $users[0]['first_name'];	
					
					if ( $this->id != null )	
					{				
						usam_edit_data( $new, $this->id, 'usam_vk_profile' );					
					}
					else			
					{							
						usam_add_data( $new, 'usam_vk_profile' );		 
					}				
				/*	$url = 'http://oauth.vk.com/authorize?client_id='.$this->vk_api['api_id'].'&scope=wall,photos,ads,offline,friends,notifications,market&display=page&response_type=token&state='.$this->id.'&redirect_uri='.get_bloginfo( 'url' ).'/vk';		

					wp_redirect( $url );			
					exit;*/
				}
			break;			
		}			
	}	
}