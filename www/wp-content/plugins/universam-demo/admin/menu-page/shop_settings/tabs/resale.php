<?php
class USAM_Tab_Resale extends USAM_Tab
{		
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сайты поставщиков товара', 'usam'), 'description' => 'Здесь Вы можете добавить или изменить сайты с которых Вы продаете товар.' );
		$this->buttons = array( 'add' => __('Добавить сайт', 'usam') );			
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}


	public function callback_submit() 
	{
		switch( $this->current_action )
		{
			case 'delete':
				usam_delete_data($this->records, 'usam_settings_sites_suppliers');
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;
			case 'save':
				$site['name'] = sanitize_text_field(stripcslashes($_POST['name']));		
				$site['type_price'] = sanitize_title($_POST['type_price']);				
				$site['setting'] = stripslashes_deep($_POST['site_setting_tag']);			
				$site['currency_code'] = mb_strtoupper(sanitize_title($_POST['currency_code']));
				$site['store'] = absint($_POST['store']);
				
				$domain = parse_url($_POST['link'], PHP_URL_HOST);		
				if ( !empty($domain) )
					$site['domain']	= $domain;
				else
					$site['domain'] = $this->data['domain'];
				
				if ( $this->id != null )	
				{   
					usam_edit_data( $site, $this->id, 'usam_settings_sites_suppliers' );	
				}
				else			
				{			
					$this->id = usam_add_data( $site, 'usam_settings_sites_suppliers' );				
				}					
			break;
		}
	}
}