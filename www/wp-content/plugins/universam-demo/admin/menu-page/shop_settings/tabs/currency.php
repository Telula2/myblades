<?php
class USAM_Tab_currency extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Валюты', 'usam'), 'description' => __('Здесь вы можете посмотреть список валют', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить валюту', 'usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	

	public function callback_submit() 
	{	
		global $wpdb;
		switch( $this->current_action )
		{		
			case 'delete':			
				$sql = $wpdb->query( "DELETE FROM " . USAM_TABLE_CURRENCY . " WHERE id IN (".implode(",", $this->records).")" );
			break;	
			case 'save':
				if( !empty($_POST['currency']) )
				{	
					$data = $_POST['currency'];	
					$data['name'] = sanitize_text_field(stripcslashes($_POST['name']));				
					if ( $this->id != null )
					{	
						$currencies = new USAM_Currency( $this->id );
						$currencies->set( $data );				
					}				
					else
					{							
						$currencies = new USAM_Currency( $data );		
					}
					$currencies->save( );	
					$this->id = $currencies->get('code');						
				}	
			break;		
		}	
	}
}