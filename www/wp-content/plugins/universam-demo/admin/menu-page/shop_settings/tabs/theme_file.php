<?php
class USAM_Tab_theme_file extends USAM_Tab
{
	protected  $display_save_button = false;
	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Файлы темы магазина', 'usam'), 'description' => __('Магазин предоставляет Вам возможность перемещать файлы темы в безопасное место для контроля тематизации. Если вы хотите изменить внешний вид вашего сайта, выбрите файлы, которые вы хотите отредактировать, из списка и нажмите кнопку перемещения. Это позволит скопировать файлы шаблонов в вашу активную тему WordPress.', 'usam') );		
		
		require_once( USAM_FILE_PATH . '/admin/includes/theming.class.php' ); // перенос шаблонов в тему
	}
	
	protected function callback_submit()
	{		 		
		switch( $this->current_action )
		{
			case 'backup_themes':		//Резервное копирование темы WordPress		
				$wp_theme_path = get_stylesheet_directory();
				usam_recursive_copy( $wp_theme_path, USAM_THEME_BACKUP_DIR );
				$this->sendback = add_query_arg( array( 'backup_themes' => 1 ), $this->sendback );
				$this->redirect = true;	
			break;	
			case 'usam_move_themes':			
				$templates_to_move = isset( $_POST['usam_templates_to_port'] ) ? stripslashes_deep($_POST['usam_templates_to_port']) : array();	
				$theming = new USAM_Theming( $templates_to_move ); 
				$error = $theming->get_error();
				if ( !empty($error) )
					$this->set_user_screen_error( $error );
				else
				{
					$this->sendback = add_query_arg( array( 'result_move_themes' => 1 ), $this->sendback );
					$this->redirect = true;	
				}
			break;				
		}	
	}
	
	public function get_message()
	{		
		$message = '';
		if ( ! empty( $_REQUEST['backup_themes'] ) )
			$message = __( 'Спасибо, вы сделали резервную копию темы. Копия доступна по ссылке ниже. Пожалуйста, обратите внимание, каждая новая резервная копия заменяет предыдущие резервную копию.', 'usam' )."<br>".__( 'Сссылка:', 'usam' ). "/" . str_replace( ABSPATH, "", USAM_THEME_BACKUP_DIR );
		if ( ! empty( $_REQUEST['result_move_themes'] ) )
			$message = __( 'Спасибо, файлы были скопированы.', 'usam' );
		
		return $message;
	} 
	
	public function display() 
	{
		usam_add_box( 'usam_theme_metabox', __( 'Управление файлами темы магазина', 'usam' ), array( $this, 'theme_metabox' ) );		
	}

	public function theme_metabox()
	{		
		$usam_templates    = usam_list_product_templates( USAM_CORE_THEME_PATH );	
		$current_theme_files = usam_list_product_templates( USAM_THEMES_PATH );	// Загрузите файлы из текущей темы	
		$themes_location = array_intersect( $current_theme_files, $usam_templates );	// Получает различия	
		if ( count( $themes_location ) == 0 ) 
			$themes_location = false;			
		?>
		<p>
		<?php if(false !== $themes_location)						
			_e( 'Некоторые файлы темы были перемещены в папку с темой WordPress.', 'usam' );
		else
			_e( 'Ни одного файла темы не было перемещено в папку с темой WordPress.', 'usam' );

		 ?>
		</p>	
		<form method='POST' action='' id='usam-tab_form'>		
			<ul>
			<?php
				foreach($usam_templates as $file)
				{
					$id = str_ireplace('.', '_', $file);
					$selected = '';							
					if(false !== array_search($file, (array)$themes_location))
						$selected = 'checked="checked"';
					?>
					<li><input type='checkbox' id='<?php echo $id; ?>' <?php echo $selected; ?> value='<?php echo esc_attr( $file ); ?>' name='usam_templates_to_port[]' />
					<label for='<?php echo $id; ?>'><?php echo esc_html( $file ); ?></label></li>
			<?php }	 ?>
			 </ul>		
			 <?php
			 if( false !== $themes_location )
			 {
				?> 
				<p> <?php _e( 'Для изменения внешнего вида вашего магазина, вы можете переместить и редактировать файлы, которые будут находятся здесь:', 'usam' ); ?> </p>
				<p class="howto"><?php echo USAM_THEMES_PATH; ?></p> 
				<?php 
			} ?>
			<p>
				<?php $this->nonce_field(); ?>
				<input type='hidden' value='usam_move_themes' name='action' />
				<input type='submit' value='<?php esc_attr_e( 'Переместить файлы шаблонов &rarr;' ); ?>' class="button" name='submit_move_themes' />
			</p>
		</form>
		<p><?php _e( 'Вы можете создать копию вашей темы WordPress, нажав на кнопку ниже резервного копирования. После копирования вы можете найти их здесь:' ,'usam' ); ?></p>
		<p class="howto"><?php echo esc_html( '/uploads/universam/theme_backup/' ); ?></p>
		<p><?php printf( __( '<a href="%s" class="button">Резервное копирование темы WordPress</a>', 'usam' ), $this->get_nonce_url( add_query_arg( array('action' => 'backup_themes'), $_SERVER['REQUEST_URI']) ) ); ?></p>
		<?php
	}	
}