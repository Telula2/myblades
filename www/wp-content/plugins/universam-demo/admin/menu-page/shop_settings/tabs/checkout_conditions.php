<?php
class USAM_Tab_checkout_conditions extends USAM_Tab
{
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сроки и условия', 'usam'), 'description' => __('Напишите сроки и условия, которые будут видеть покупатели при оформлении заказа', 'usam') );
		
	}	
	
	public function display_conditions()
	{	               
		$option_key = 'usam_terms_and_conditions';
		wp_editor(stripslashes(str_replace('\\&quot;','',get_option($option_key))),$option_key,array(
			'textarea_name' => 'usam_options['.$option_key.']',
			'media_buttons' => false,
			'textarea_rows' => 20,
			'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
			)	
		);
	}
	
	public function display()
	{		
		usam_add_box( 'usam_checkout_conditions', __( 'Сроки и условия', 'usam' ), array( $this, 'display_conditions' ) );	
	}
}