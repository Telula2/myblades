<?php
class USAM_Tab_template_messages extends USAM_Tab
{	
	protected $display_save_button = false;
	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Глобальный шаблон сообщений электронной почты', 'usam'), 'description' => __('Здесь вы можете выбрать шаблон сообщений электронной почты.','usam') );	
	}	
		
	public function display() 
	{
		$mailtemplate_list = usam_get_templates( 'mailtemplate' );	
		$active = get_option('usam_mailtemplate');	
		?>		
		<div class ="theme-browser content-filterable rendered" >
			<div class ="themes wp-clearfix" >
			<?php		
			$url = admin_url('options-general.php?page=shop_settings&tab=template_messages');
			foreach ($mailtemplate_list as $template => $data ) 
			{
				$class = $template == $active?'active':'';			
				?>				
				<div class="theme <?php echo $class; ?>" tabindex="0" aria-describedby="<?php echo $template; ?>-action <?php echo $template; ?>-name">
		
					<div class="theme-screenshot">
						<img src="<?php echo $data['screenshot']; ?>" alt="">
					</div>				
					<span class="more-details"><?php echo __('Автор', 'usam').": ".$data['author']; ?></span>				
					<h3 class="theme-name"><?php echo $template; ?></h3>

					<div class="theme-actions">
						<a class="button button-primary" href="<?php echo usam_url_admin_action('install_mailtemplate', $url, array( 'theme' => $template ) ); ?>
						"><?php _e('Установить', 'usam'); ?></a>						
					</div>
				</div>
				<?php
			}
			?>
			</div>
		</div>
		<?php
				
	/*	$mailtemplate = usam_get_email_template();		
		$mail =  str_replace('%mailcontent%','',$mailtemplate); 		
		?>	
		<p><?php _e('Используйте %mailcontent%, где вы хотите показать ваше содержание. Вы также можете показать тему сообщения используя {#mailsubject#}','usam'); ?></p>
		
		<textarea rows="30" cols="40" class="widefat" id="stylingmailtemplate" name="usam_options[usam_styling_mail_template]" style="font-family:'Courier New'"><?php echo stripslashes(str_replace('\\&quot;','',$mail)); ?></textarea>
		<br/><br/><a id="previewmail" class="button" href='#' ><?php _e('Посмотреть шаблон письма','usam'); ?></a><br/><br/>
		<iframe id="mailtemplatepreview" style="width:800px; height:480px; border:1px solid #ccc;" ></iframe>    
		<script>
			jQuery("#previewmail").click(function(){
				jQuery("#mailtemplatepreview").contents().find("html").html(jQuery("#stylingmailtemplate").val()); 
				return false;    
			});
		</script>
		<?php*/
	}
}
?>