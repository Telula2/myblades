<?php
class USAM_Tab_message_order_statuses extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Статус заказа', 'usam'), 'description' => 'Здесь вы можете посмотреть и изменить сообщения при изменение статуса заказа.' );	
	}	
	
	public function display() 
	{		
		$this->display_order_labels(); 
		?>
		<p><?php esc_html_e( 'Шорткоды, которые могут быть использованы', 'usam' );?>: <?php echo esc_html( '[current_day -1]' ); ?></p>
		<div class = "detailed_description">
			<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
			<div class = "description_box shortcodes">
				<span>[current_day +1]</span> - <?php esc_html_e( 'Текущая дата с разницей на указаное количество дней. Например, [current_day +1] - заменить на текущую дату плюс 1 день', 'usam' ); ?></br>
				<span>[if total_price=2000 {Показать этот текст}]</span> - <?php esc_html_e( 'Условный текст. Например, если сумма заказа равна 2000 покажет текст в фигурных скобках', 'usam' ); ?></br>						
			</div>
		</div>
		<div id = "order_statuses" class = "usam_tabs usam_tabs_style3">
			<div class = "header_tab">
				<ul>
					<?php 					
					$order_statuses = usam_get_order_statuses();					
					foreach ( $order_statuses as $status )
					{						
						?>			
						<li class = "tab"><a href="#status_<?php echo $status->internalname; ?>"><?php echo $status->name; ?></a></li>
						<?php									
					}	
					?>
				</ul>
			</div>	
			<div class = "countent_tabs">	
				<?php						
				foreach ( $order_statuses as $status )
				{						
					?>			
					<div id = "status_<?php echo $status->internalname; ?>" class="tab">				
						<table class='usam_setting_table usam_edit_table'>											
							<?php $this->subject( $status->internalname.'_subject' ); ?>
							<?php $this->message( $status->internalname.'_message' ); ?>
							<?php $this->sms_message( $status->internalname.'_sms' ); ?>
						</table>
					</div>
					<?php						
				}
				?>
			</div>
		</div>
		<?php
	}
}