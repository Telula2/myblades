<?php
class USAM_Tab_message_willingness_order extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сообщение о готовности отгрузки', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить сообщения покупателю о готовности отгрузки. Напишите сообщение если вы хотите отправлять сообщение, когда была указана дата отгрузки.','usam') );	
	}		
	
	public function display() 
	{
		?>   		
		<table class='usam_setting_table usam_edit_table'>		
			<tr>
				<td colspan='2'><?php $this->display_order_labels(); ?></td>
			</tr>			
			<?php $this->subject( 'willingness_order_subject' ); ?>
			<?php $this->message( 'willingness_order_message' ); ?>			
		</table>
		<?php  
	}		
}