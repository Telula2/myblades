<?php

class USAM_Tab_product_view extends USAM_Tab
{	
	private $pbuyers;
	private $default;
	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Внешний вид товара', 'usam'), 'description' => __('Здесь Вы можете настроить внешний вид товар', 'usam') );	
		
		$this->default = array( 
			array( 'type' => 'collection',  'title' => __( 'Товары из этой коллекции', 'usam' ), 'template' => 'carousel', 'active' => true, 'number' => 14 ),	
			array( 'type' => 'upsell',      'title' => __( 'Рекомендуют профессионалы', 'usam' ), 'template' => 'simple_list', 'active' => true, 'number' => 7 ),
			array( 'type' => 'also_bought',    'title' => __( 'С этим товаром покупали', 'usam' ), 'template' => 'simple_list', 'active' => true, 'number' => 7 ),			
			array( 'type' => 'related_products', 'title' => __( 'К этому товару подойдет', 'usam' ), 'template' => 'carousel', 'active' => true, 'number' => 14 ),
			array( 'type' => 'same_category', 'title' => __( 'Товары в той же категории', 'usam' ), 'template' => 'carousel', 'active' => true, 'number' => 14 ),
			array( 'type' => 'history_views', 'title' => __( 'История просмотра товаров', 'usam' ), 'template' => 'simple_list', 'active' => true, 'number' => 7 ),			
		);		
		$this->pbuyers = get_option('usam_products_for_buyers', $this->default);	
		foreach ( $this->default as $value )		
		{
			$ok = false;
			foreach ( $this->pbuyers as $pbuyers )
			{
				if ( $pbuyers['type'] == $value['type'] )
				{
					$ok = true;
					break;
				}
			}
			if ( !$ok )
			{
				$value['active'] = false;
				$this->pbuyers[] = $value;
			}
		}		
	}	
				
	/*public function add_meta_options_help_center_tabs( ) 
	{
		$tabs[] = new USAM_Help_Center_Item(
			'basic-help',
			__( 'IntenseDebate комментарии', 'usam' ), __( 'IntenseDebate комментарии', 'usam' ),
			array(
				'content' => "<p><a href='http://intensedebate.com/sitekey/' title='".__( 'Справка по поиску Account ID', 'usam' )."' target='_blank'>".__( 'Справка по поиску Account ID', 'usam' )."</a></small>
					<a href='http://intensedebate.com/' title='".__( 'IntenseDebate комментарии для повышения лояльности покупателей', 'usam' )."' target='_blank'><img src='".USAM_CORE_IMAGES_URL."/intensedebate-logo.png' title='".__( 'IntenseDebate', 'usam' )."' /></a></p>",
			)
		);
		return $tabs;
	}
	*/
	public function callback_submit() 
	{
		if (!empty($_POST['pbuyers']) )
		{
			$pbuyers = stripslashes_deep($_POST['pbuyers']);	
			$update = array();			
			foreach ( $pbuyers as $key => $values )
			{				
				foreach ( $values as $key2 => $value )
				{
					switch( $key )
					{
						case 'active':
							$update[$key2][$key] = (bool)$value;	
						break;
						case 'title':
							$update[$key2][$key] = sanitize_text_field($value);	
						break;						
						case 'number':
							$update[$key2][$key] = absint($value);	
						break;
						case 'type':
							$update[$key2][$key] = sanitize_title($value);	
						break;
						case 'template':
							$filename = usam_get_module_template_file( 'products', $value );
							$update[$key2][$key] = !empty($filename)?$value:'simple_list';	
						break;
					}						
				}
			}	
			update_option('usam_products_for_buyers', $update );
			$this->pbuyers = $update;	
			parent::callback_submit( );			
		}		
	}
	
	
	public function display() 
	{
		usam_add_box( 'usam_view_product', __('Настройка просмотра товара','usam'), array( $this, 'view_product' ) );	
		usam_add_box( 'usam_product_setting', __('Другие настройки','usam'), array( $this, 'product_setting' ) );	
	}	
	
	public function view_product() 
	{	
		$view_tab = get_option('usam_view_tab', array('characteristics' => 1, 'description' => 1, 'comment' => 1, 'brand' => 1) );
		
		$options = array( 		
			array( 'type' => 'radio', 'title' => __( 'Показать рейтинг товара', 'usam' ), 'option' => 'show_product_rating', ),	
			array( 'type' => 'radio', 'title' => __( 'Показать доступность на складе', 'usam' ), 'option' => 'show_availability_stock', ),
			array( 'type' => 'radio', 'title' => __( 'Добавить поле "количество"', 'usam' ), 'option' => 'show_multi_add',  ),
			array( 'type' => 'radio', 'title'=>__( 'Показать кнопку Share This', 'usam' ), 'option' => 'share_this' ),			
			array( 'type' => 'radio', 'title'=>__( 'Показывает кнопку Facebook Like', 'usam' ), 'option' => 'facebook_like',),
			array( 'type' => 'radio', 'title'=>__( 'Показывает мне нравится для вКонтакта', 'usam' ), 'option' => 'vk_like' ),						   
		);
		?>	
		<div class="column-2">
			<div class="sidebar">
				<div class="usam_container-table-container caption border">
					<div class="usam_container-table-caption-title"><?php _e( 'Фотография товара', 'usam' ); ?></div>
					<img id ="thumb_product" width="260" height="260" src="<?php echo USAM_CORE_THEME_URL.'images/noimage.png'; ?>"/>
				</div>	
			</div>	
			<div class="main_content">
				<div class="usam_container-table-container caption border">
						<div class="usam_container-table-caption-title"><?php _e( 'Главное содержимое', 'usam' ); ?></div>
					<?php $this->display_table_row_option( $options ); ?>
				</div>	
			</div>	
		</div>	
		<?php
		$options = array( 		
			array( 'type' => 'radio', 'title' => __( 'Показать характеристики', 'usam' ), 'option' => 'view_tab_characteristics', 'attribute' => array('name' => 'usam_view_tab[characteristics]','value' => $view_tab['characteristics']) ),		
			array( 'type' => 'radio', 'title' => __( 'Показать описание', 'usam' ), 'option' => 'view_tab_description', 'attribute' => array('name' => 'usam_view_tab[description]','value' => $view_tab['description']) ),
			array( 'type' => 'radio', 'title' => __( 'Включить комментарии', 'usam' ), 'option' => 'view_tab_comment', 'attribute' => array('name' => 'usam_view_tab[comment]','value' => $view_tab['comment']) ),		
			array( 'type' => 'radio', 'title' => __( 'Показать описание бренда', 'usam' ), 'option' => 'view_tab_brand', 'attribute' => array('name' => 'usam_view_tab[brand]','value' => $view_tab['brand']) ),			
		);
		?>	
		<div class="usam_container-table-container caption border">
			<div class="usam_container-table-caption-title"><?php _e( 'Вкладки товара', 'usam' ); ?></div>
			<?php $this->display_table_row_option( $options ); ?>
		</div>				
		<div class="usam_container-table-container caption border">
			<div class="usam_container-table-caption-title"><?php _e( 'Блоки с товарами', 'usam' ); ?></div>
				<table id="products_for_buyers">
					<?php	
					$title = array( 'collection' => __( 'Коллекция', 'usam' ), 'also_bought' => __( 'С этим товаром покупали', 'usam' ),'history_views' => __( 'История просмотра товаров', 'usam' ), 'related_products' => __( 'Перекрестные продажи', 'usam' ), 'same_category' => __( 'Товары в той же категории', 'usam' ), 'upsell' => __( 'UP SELL', 'usam' ), );	
					$templates = usam_get_templates('products');	
					foreach ( $this->pbuyers as $value )
					{						
						?>
						<tr>
							<td>
								<input type='hidden' class ="template_input" name='pbuyers[template][<?php echo $value['type']; ?>]' value='<?php echo $value['template']; ?>' />
								<input type='hidden' name='pbuyers[type][<?php echo $value['type']; ?>]' value='<?php echo $value['type']; ?>' />								
								<div id="<?php echo $value['type']; ?>" class="postbox <?php echo $value['active']?'active':''; ?>">
									<span class="sort" title="<?php _e('Перетащите, чтобы поменять местами блоки', 'usam'); ?>"></span>
									<h3 class="hndle ui-sortable-handle">
										<span><?php echo $title[$value['type']] ?></span> 										
										<input type='hidden' name='pbuyers[active][<?php echo $value['type']; ?>]' value='0'/>
										<input title="<?php _e('Изменить активность', 'usam'); ?>" type='checkbox' class ="template_active" name='pbuyers[active][<?php echo $value['type']; ?>]' value='1' <?php checked($value['active'], 1); ?>/>
										<input title="<?php _e('Количество товара', 'usam'); ?>" type='text' size="2" maxlength = "2" name='pbuyers[number][<?php echo $value['type']; ?>]' value='<?php echo $value['number']; ?>'/>
									</h3>
									<div class="inside">	
		<div id="titlediv">
			<div id="titlewrap">
				<input class="titlebox" title="<?php _e('Название блока на сайте', 'usam'); ?>" type='text' name='pbuyers[title][<?php echo $value['type']; ?>]' value='<?php echo $value['title']; ?>'/>
			</div>
		</div>	
									
				<div class='jcarousel-wrapper'>				
					<section class='carousel'>
						<button class='up-carousel'><span></span></button>
						<div id = 'jcarousel' class='jcarousel'>
							<ul>
						
											<?php													
											foreach ($templates as $template => $data ) 
											{
												?>		
												<li data-name_template ="<?php echo $template; ?>" class="<?php echo $template == $value['template']?'active':''; ?>">														
												<div class="template">								
													<div class="template-screenshot">
														<img src="<?php echo $data['screenshot']; ?>" alt="">
													</div>													
													<h3 class="template-name"><?php echo $template; ?></h3>											
												</div>
												</li>
												<?php
											}
											?>		
							</ul>
						</div>
						<button class='down-carousel'><span></span></button>
					</section>
				</div>				
									</div>
								</div>								
							</td>
						</tr>
					<?php } ?>
				</table>		
		</div>	
		<?php				
	}
	
	public function product_setting() 
	{		
		$options = array( 					
			array('type' => 'radio', 'title' => __( 'Показать уведомления о покупке', 'usam' ), 'option' => 'show_fancy_notifications', 			  
			   'description' => '' ),		
		/*	array( 'type' => 'radio', 'title' => __( 'Варианты системы комментариев', 'usam' ), 'option' => 'comment_system', 			  
			    'radio' => array( 'integrated' => __( 'Встроенная', 'usam' ), 'intensedebate' => __( 'IntenseDebate', 'usam' ) ),
			   'description' => ""),	
			array( 'type' => 'input', 'title' => __( 'IntenseDebate Account ID', 'usam' ), 'option' => 'intensedebate_account_id', 				  
			   'description' => "" ),	*/
			array( 'type' => 'radio', 'title' => __( 'Отображать вкладку с коментариями', 'usam' ), 'option' => 'show_comments_by_default', 
			   'radio' => array( 0 => __( 'Всех товаров', 'usam' ), 1 => __( 'Для каждого продукта', 'usam' ) ),
			   'description' => __( 'Настройка если в карточке товара выбрана опция показывать комментарии по умолчанию', 'usam' )),				   
		);
		$this->display_table_row_option( $options ); 
	}
}