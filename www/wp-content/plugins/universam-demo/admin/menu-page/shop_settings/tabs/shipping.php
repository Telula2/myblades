<?php
class USAM_Tab_Shipping extends USAM_Tab
{
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Службы доставки', 'usam'), 'description' => 'Здесь Вы можете добавить или изменить службу доставки.' );
		$this->buttons = array( 'add' => __('Добавить службу доставки', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}

	public function callback_submit()
	{
		global $wpdb;
		
		switch( $this->current_action )
		{
			case 'delete':
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_DELIVERY_SERVICE." WHERE id IN ($in)" );				
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'save':
				if( !empty($_POST['name']) )
				{						
					$this->data = $_POST['shipping_module'];	
					$this->data['name'] = sanitize_text_field(stripcslashes($_POST['name']));	
					$this->data['handler'] = sanitize_text_field(stripcslashes($_POST['handler']));				
					$this->data['sort'] = absint($_POST['sort']);				
					$this->data['description'] = sanitize_textarea_field(stripcslashes($_POST['description']));	
					$this->data['active'] = !empty($_POST['active'])?1:0;			
					$this->data['img'] = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;	
					
					$this->data['setting']['restrictions'] = array();
					
					if ( !empty($_POST['price_from']) && $_POST['price_from'] != 0.00 )
						$this->data['setting']['restrictions']['price_from'] = (float)$_POST['price_from'];
					if ( !empty($_POST['price_to'])&& $_POST['price_to'] != 0.00 )
						$this->data['setting']['restrictions']['price_to'] = (float)$_POST['price_to'];
					if ( !empty($_POST['weight_from']) && $_POST['weight_from'] != 0.00 )
						$this->data['setting']['restrictions']['weight_from'] = (float)$_POST['weight_from'];
					if ( !empty($_POST['weight_to']) && $_POST['weight_to'] != 0.00 )
						$this->data['setting']['restrictions']['weight_to'] = (float)$_POST['weight_to'];
					if ( !empty($_POST['locations']) )
						$this->data['setting']['restrictions']['locations'] = array_map('intval', $_POST['locations']);
							
					if( isset($_POST['stores']) && !empty($_POST['stores_show']) )
					{								
						$this->data['setting']['stores'] = array_map('intval', $_POST['stores']);	
					}		
					else
						$this->data['setting']['stores'] = array();				
					
					if( isset($_POST['handler_setting']) )
						$this->data['setting']['handler_setting'] = array_map( 'sanitize_text_field', wp_unslash( $_POST['handler_setting'] ) );
							
					$formats = $this->get_formats();
					
					$insert = $this->data;			
					$insert['setting'] = serialize($insert['setting']);	
					
					if ( $this->id != null )	
					{
						$where  = array( 'id' => $this->id );
						$result = $wpdb->update( USAM_TABLE_DELIVERY_SERVICE, $insert, $where, $formats );		
					}
					else
					{
						$result = $wpdb->insert( USAM_TABLE_DELIVERY_SERVICE, $insert, $formats );		
						$this->id = $wpdb->insert_id;
					}
				}	
			break;
		}
	}
	
	function get_formats( )
	{		
		$formats = array( 'name' => '%s', 'description' => '%s', 'active' => '%d', 'sort' => '%d', 'period_from' => '%d', 'period_to' => '%d', 'period_type' => '%s','handler' => '%s', 'price' => '%f', 'setting' => '%s', 'img' => '%d' );
		$return_formats = array();
		foreach( $this->data as $key => $value) 				
			$return_formats[] = $formats[$key];
		return $return_formats;
	}
}