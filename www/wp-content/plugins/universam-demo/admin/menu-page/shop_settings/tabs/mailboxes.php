<?php
class USAM_Tab_mailboxes extends USAM_Tab
{	
	private $data = array();
	public function __construct() 
	{
		$this->header = array( 'title' => __('Почтовые ящики', 'usam'), 'description' => __('Здесь вы можете посмотреть список подключенных почтовых ящиков. Не забудьте разрешить доступ по протоколу POP в вашем почтовом ящике.', 'usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}

	protected function callback_submit()
	{
		global $wpdb;		
		switch( $this->current_action )
		{
			case 'delete':
				foreach ( $this->records as $key => $id )
				{
					$wpdb->query("DELETE FROM `".USAM_TABLE_MAILBOX."` WHERE `id`={$id}");						
				}
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'save':		
				if( !empty($_POST['mailbox']) )
				{
					$this->data = stripslashes_deep($_POST['mailbox']);						
					$formats = $this->get_formats();	
					$this->data['email'] = trim($this->data['email']);			
					if ( !is_email($this->data['email']) )
					{
						$this->sendback = add_query_arg( array( 'error' => 1, 'action' => 'add' ), $this->sendback );	
						return false;
					}			
					$encryption  = new USAM_Encryption();	
					if ( $this->data['smtppass'] == '***' )
						unset($this->data['smtppass']);			
					elseif ( $this->data['smtppass'] != '' )
						$this->data['smtppass']	= $encryption->data_encrypt( trim($this->data['smtppass']) );
					else
						$this->data['smtppass']	= '';
					
					if ( $this->data['pop3pass'] == '***' )
						unset($this->data['pop3pass']);			
					elseif ( $this->data['pop3pass'] != '' )
						$this->data['pop3pass']	= $encryption->data_encrypt( trim($this->data['pop3pass']) );
					else
						$this->data['pop3pass']	= '';
					
					if ( $this->id )			
						usam_update_mailbox( $this->id, $this->data, $formats );				
					else
					{						
						$result_insert = $wpdb->insert( USAM_TABLE_MAILBOX, $this->data, $formats );	
						$this->id = $wpdb->insert_id;
						
						if ( $this->id )
						{							
							$system_folders = usam_get_system_folders();							
							foreach ( $system_folders as $slug => $name )
								usam_insert_email_folder( array('name' => $name, 'mailbox_id' => $this->id, 'slug' => $slug) );	
						}
					}
				}
			break;
		}
	}
	
	function get_formats( )
	{		
		$formats = array( 'name' => '%s', 'email' => '%s', 'pop3server' => '%s', 'pop3port' => '%d', 'pop3user' => '%s', 'pop3pass' => '%s','pop3ssl' => '%d', 'smtpserver' => '%s', 'smtpport' => '%d', 'smtpuser' => '%s', 'smtppass' => '%s','smtp_secure' => '%s','letter_id' => '%d', 'sort' => '%d', 'delete_server' => '%d', 'delete_server_day' => '%s', 'delete_server_deleted' => '%d' );
		$return_formats = array();
		foreach( $this->data as $key => $value) 	
		{
			if ( isset($formats[$key]) )
				$return_formats[] = $formats[$key];
			else				
				unset($this->data[$key]);
		}
		return $return_formats;
	}	
	
	protected function load_tab()
	{
		$this->list_table( );
	}	
		
	protected function get_message_error()
	{	
		$message = '';
		if( isset($_REQUEST['error']) && $_REQUEST['error'] == 1 )		
			$message = __( 'Не верно указали электронную почту', 'usam' );		
		
		return $message;
	}
}
?>