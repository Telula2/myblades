<?php

class USAM_Tab_Feedback extends USAM_Tab
{	
	private function print_editor( $type_message ) 
	{
		$feedback = usam_get_feedback_message( $type_message );
		wp_editor(stripslashes(str_replace('\\&quot;','',$feedback )),'usam_feedback_'.$type_message,array(
			'textarea_rows' => 10,
			'textarea_name' => 'usam_options[usam_feedback_'.$type_message.'][message]',
			'media_buttons' => false,
			'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
			)	
		);
	}
	
	private function field( $tmessage, $tfield, $name_field, $field ) 
	{	
		$show    = !empty($field['show']) ? "checked='checked'":'';	
		$require = !empty($field['require']) ? "checked='checked'":'';	
		$title = !empty($field['title'])?$field['title']:'';
		?>
		<tr>
			<td class="column_name"><label for='usam_feedback_<?php echo $tmessage; ?>_mail'><?php echo $name_field; ?></label></td>
			<td class="column_show"><input type='checkbox' value='1' name='usam_options[usam_feedback_<?php echo $tmessage; ?>][fields][<?php echo $tfield; ?>][show]' <?php echo $show; ?> /></td>	
			<td class="column_require"><input type='checkbox' value='1' name='usam_options[usam_feedback_<?php echo $tmessage; ?>][fields][<?php echo $tfield; ?>][require]' <?php echo $require; ?> /></td>
			<td class="column_signature"><input type="text" value='<?php echo $title; ?>' name='usam_options[usam_feedback_<?php echo $tmessage; ?>][fields][<?php echo $tfield; ?>][title]'/></td>				
		</tr>
		<?php
	}

	public function fields( $type_message ) 
	{	
		$option = 'usam_feedback_'.$type_message;
		$feedback = get_option( $option, array() );				
		
		$default_fields = array( 'name' => esc_html__( 'Ваше имя', 'usam' ), 'url' => esc_html__( 'Сcылка', 'usam' ), 'mail' => esc_html__( 'Электронная почта', 'usam' ), 'phone' => esc_html__( 'Телефон', 'usam' ), 'message' => esc_html__( 'Сообщение', 'usam' ) );		
		?>
		<div id = "fields" class = "fields">
			<table class = "usam_list_table t_feedback_fields">				
				<thead>
					<tr>
						<th class="column_name"><?php echo esc_html_e( 'Название поля', 'usam' ); ?></th>
						<th class="column_show"><?php echo esc_html_e( 'Показать', 'usam' ); ?></th>
						<th class="column_require"><?php echo esc_html_e( 'Требовать', 'usam' ); ?></th>	
						<th class="column_signature"><?php echo esc_html_e( 'Подпись поля', 'usam' ); ?></th>						
					</tr>
				</thead>
				<tbody>
				<?php	
					foreach( $default_fields as $type_field => $name_field )
					{					
						$field = !empty($feedback['fields'][$type_field])?$feedback['fields'][$type_field]:array();
						$this->field( $type_message, $type_field, $name_field, $field );
					}
				?>					
				</tbody>
			</table>						
		</div>
		<?php 
	}
	
	private function print_tab( $type_message, $title ) 
	{	
		?>
		<div id = "message_<?php echo $type_message; ?>" class="tab">	
			<h3 class="form_group"><?php echo $title; ?></h3>
			<table class='usam_setting_table usam_edit_table'>				
				<tr>
					<th colspan="2"><?php esc_html_e( 'Метки могут быть использованы', 'usam' );?>: <?php echo esc_html( '%shop_name%, %product_price%, %product_name%' ); ?></th>
				</tr>	
				<tr>					
					<td colspan="2"><?php $this->print_editor( $type_message ); ?></td>
				</tr>				
			</table>
			<?php usam_add_box( 'usam_settings', __('Получить данные','usam'), array( $this, 'fields' ), $type_message );	?>	
		</div>
		<?php 
	}
	
	public function display() 
	{					
		?>
		<div id = "feedback" class = "usam_tabs usam_tabs_style2">
			<div class = "header_tab">
				<ul>
					<li class = "tab"><a href="#message_ask_question"><?php _e( 'Вопрос покупателя', 'usam' ); ?></a>|</li>
					<li class = "tab"><a href="#message_notify_stock"><?php _e( 'Сообщить при поступлении товара', 'usam' ); ?></a>|</li>				
					<li class = "tab"><a href="#message_product_info"><?php _e( 'Вопрос о товаре', 'usam' ); ?></a>|</li>				
					<li class = "tab"><a href="#message_product_error"><?php _e( 'Сообщение об ошибке', 'usam' ); ?></a>|</li>	
					<li class = "tab"><a href="#message_price_comparison"><?php _e( 'Есть дешевле', 'usam' ); ?></a>|</li>
					<li class = "tab"><a href="#message_buy_product"><?php _e( 'Заказать товар', 'usam' ); ?></a></li>		
				</ul>
			</div>	
			<div class = "countent_tabs">					
				<?php 
				$this->print_tab( 'ask_question', esc_html__( 'Содержание сообщения - вопрос покупателя', 'usam' ) ); 
				$this->print_tab( 'notify_stock', esc_html__( 'Содержание сообщения - сообщить при поступлении товара', 'usam' ) ); 
				$this->print_tab( 'buy_product', esc_html__( 'Содержание сообщения - заказать товар', 'usam' ) ); 
				$this->print_tab( 'product_info', esc_html__( 'Содержание сообщения - вопрос о товаре', 'usam' ) ); 
				$this->print_tab( 'product_error', esc_html__( 'Содержание сообщения - ошибки в товаре', 'usam' ) ); 
				$this->print_tab( 'price_comparison', esc_html__( 'Содержание сообщения - есть дешевле', 'usam' ) );
				?>			
			</div>
		</div>
		<?php 
	}
}
?>