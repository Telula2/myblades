<?php
class USAM_Tab_social_networks extends USAM_Tab
{	
	private $vk_api;
	public function __construct() 
	{
		$this->header = array( 'title' => __('Однокласники', 'usam'), 'description' => 'Здесь вы можете настроить управление Вашими социальными сетями.' );
	}		
	
	public function callback_submit() 
	{				
// Одноклассники
		if ( !empty($_POST['odnoklassniki']) )
			update_option('usam_odnoklassniki_api',  $_POST['odnoklassniki'] );
		
		if (!empty($_POST['odnoklassniki_id']) )
		{	// Настройки анкет 'вконтакте'
			$vk_profile_id = $_POST['odnoklassniki_id'];
			$vk_profile_api = $_POST['odnoklassniki_api'];			
			$vk_profile_new = array();
			foreach( $vk_profile_id as $key => $page_id )
			{				
				if ( !empty($page_id) )
				{	
					if ( !empty($vk_profile_api[$key]) )
					{
						$result_seach = stripos($vk_profile_api[$key], 'http://api.vk.com');
						if ($result_seach !== false) 
						{		
							$url = explode('#', $vk_profile_api[$key]);
							$params = wp_parse_args($url[1]);
							$access_token = $params['access_token'];	
						}
						else 
							$access_token = $vk_profile_api[$key];							
					}
					else 
						$access_token = '';
					if ( !empty($_POST['odnoklassniki_friends_add'][$page_id]) )
						$friends_add = 1;
					else
						$friends_add = 0;								
					$vk_profile_new[] = array( 'page_id' => $page_id, 'type_page' => 'user', 'access_token' => $access_token, 'friends_add' => $friends_add );
				}
			}		
			update_option('usam_odnoklassniki_profile', $vk_profile_new );
		}		
	}

	public function display() 
	{	
		usam_add_box( 'usam_odnoklassniki_meta_box', __( 'Одноклассники', 'usam' ), array( $this, 'odnoklassniki_meta_box' ) );
	}
		
	public function odnoklassniki_meta_box()
	{	
		$odnoklassniki_api = get_option('usam_odnoklassniki_api', array());		
		?>
		<p>
			<?php esc_html_e( 'Создайте приложение в одноклассниках и Вам на почту придет письмо, в нем необходимые параметры.', 'usam' ); ?>
		</p>
		<table class="odnoklassniki_api">
			<thead>
				<tr>
					<th>ID приложения</th>		
					<th>Публичный ключ приложения</th>		
					<th>Секретный ключ приложения</th>							
				</tr>
			</thead>
			<tbody>
			<tr>				
				<td><input type="text" name="odnoklassniki[client_id]" value="<?php echo $odnoklassniki_api['client_id']; ?>"></td>	
				<td><input type="text" name="odnoklassniki[application_key]" value="<?php echo $odnoklassniki_api['application_key']; ?>"></td>
				<td><input type="text" name="odnoklassniki[client_secret]" value="<?php echo $odnoklassniki_api['client_secret']; ?>"></td>
			</tr>									
			</tbody>
		</table>		
		<?php
		$ok = new Social_APIClient_Odnoklassniki( $odnoklassniki_api);

		// закомментировать для получения нового
		//$token = '{"token_type":"session","refresh_token":"b...3","access_token":"7...9","expires":1374053503}';
		if($token)
		{
			$ok->setToken($token);
			//$ok->refreshToken();
		} 
		else 
		{
		//	global $wp; 
 
//$tekuchaya_ssilka = home_url(add_query_arg(array(), $wp->request));;

		//	echo $_SERVER["REQUEST_URI"] ;
			$ok->setRedirectUrl('http://order-book.ru/wp-admin/options-general.php?page=shop_settings&tab=marketing');			
			if(isset($_GET['code']))
			{
				$ok->getToken($_GET['code']);
				$ok->saveToken();
				print $ok->getTokenStr();
			} 
			else 
			{
				?>					
				<p>
					<?php print '<a href="' . $ok->getLoginUrl(array('VALUABLE ACCESS', 'SET STATUS')) . '">'.__( 'Нажмите, чтобы получить доступ','usam').'!</a>'; ?>
					<?php print '<a href="http://ok.ru/dk?st.cmd=appsInfoMyDevList">'.__( 'Ваши приложения','usam').'!</a>'; ?>
					
				</p>
				<?php					
			}
		}
		?>
		<h4 class = "header_settings"><?php _e('Анкеты пользователей в одноклассниках', 'usam' ); ?></h4>
		<table class = "table_odnoklassniki_profile">
			<thead>
				<tr>
					<th><?php _e('ID анкеты', 'usam' ); ?></th>		
					<th><?php _e('Имя', 'usam' ); ?></th>						
					<th><?php _e('Access Token', 'usam' ); ?></th>	
					<th><?php _e('Доступ', 'usam' ); ?></th>
					<th><?php _e( 'Добавить друзей', 'usam' ); ?></th>	
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$odnoklassniki_profile = get_option('usam_odnoklassniki_profile', array());					
				if ( ! empty( $odnoklassniki_profile ) ): 
					$users_id = '';
					$z = '';
					foreach( $odnoklassniki_profile as $profile )
					{							
						$users_id .= $z.$profile['page_id'];
						$z = ',';	
					}		
					foreach( $odnoklassniki_profile as $key => $value )
					{
						$this->odnoklassniki_row( $value );
					}
					?>
				<?php else: ?>
					<?php $this->odnoklassniki_row(); ?>
				<?php endif; ?>
			</tbody>
		</table>
		<?php
	}
	
	private function odnoklassniki_row( $value = '' ) 
	{				
		?>
			<tr>				
				<td>
					<div class="cell-wrapper">					
						<?php 
						if ( !empty( $this->users) )
							foreach( $this->users as $users )
							{
								if ( $users['uid'] == $value['page_id'] )
									echo $users['first_name'].' '.$users['last_name'];
							}
						?>	
					</div>
				</td>
				<td>
					<div class="cell-wrapper">					
						<input type="text" name="odnoklassniki_id[]" id="vk_profile_id" value="<?php echo $value['page_id']; ?>">			
					</div>
				</td>				
				<td>
					<div class="cell-wrapper">					
						<input type="text" name="odnoklassniki_api[]" id="vk_profile_access_token" value="<?php echo $value['access_token']; ?>">
					</div>
				</td>				
				<td>
					<div class="cell-wrapper">					
						<a href="<?php ?>" id = "getaccesstokenurl" target = "_blank">Подтвердить доступ</a>	
					</div>
				</td>
				<td>
					<div class="cell-wrapper friends_add">	
						<input type='checkbox' name='odnoklassniki_friends_add[<?php echo $value['page_id']; ?>]' <?php echo ( $value['friends_add'] == 1?'checked="checked"':''); ?> />
					</div>
				</td>						
				<td>
					<div class="cell-wrapper">	
						<div class="actions">
							<a tabindex="-1" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a>
							<a tabindex="-1" title="<?php _e( 'Удалить уровень', 'usam' ); ?>" class="action delete" href="#"><?php _e( 'Удалить', 'usam' ); ?></a>
						</div>
					</div>
				</td>
			</tr>
		<?php
	}	
}