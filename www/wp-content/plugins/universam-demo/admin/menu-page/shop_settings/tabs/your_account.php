<?php

class USAM_Tab_your_account extends USAM_Tab
{	
	private $pbuyers;
	private $default;
	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Кабинет пользователя', 'usam'), 'description' => __('Здесь Вы можете настроить кабинет пользователя', 'usam') );	
	}
	
	public function callback_submit() 
	{
		if (!empty($_POST['pbuyers']) )
		{
			$pbuyers = stripslashes_deep($_POST['pbuyers']);	
			$update = array();			
			foreach ( $pbuyers as $key => $values )
			{				
				foreach ( $values as $key2 => $value )
				{
					switch( $key )
					{
						case 'active':
							$update[$key2][$key] = (bool)$value;	
						break;
						case 'title':
							$update[$key2][$key] = sanitize_text_field($value);	
						break;						
						case 'number':
							$update[$key2][$key] = absint($value);	
						break;
						case 'type':
							$update[$key2][$key] = sanitize_title($value);	
						break;
						case 'template':
							$filename = usam_get_module_template_file( 'products', $value );
							$update[$key2][$key] = !empty($filename)?$value:'simple_list';	
						break;
					}						
				}
			}	
			update_option('usam_products_for_buyers', $update );
			$this->pbuyers = $update;	
			parent::callback_submit( );			
		}		
	}
	
	
	public function display() 
	{
		usam_add_box( 'usam_view_product', __('Вкладки в кабинете пользователя','usam'), array( $this, 'view' ) );		
	}	
	
	public function view() 
	{	
		$view_tab = get_option('usam_view_tab', array('characteristics' => 1, 'description' => 1, 'comment' => 1, 'brand' => 1) );
		
		$options = array( 		
			array( 'type' => 'radio', 'title' => __( 'Показать характеристики', 'usam' ), 'option' => 'view_tab_characteristics', 'attribute' => array('name' => 'usam_view_tab[characteristics]','value' => $view_tab['characteristics']) ),		
			array( 'type' => 'radio', 'title' => __( 'Показать описание', 'usam' ), 'option' => 'view_tab_description', 'attribute' => array('name' => 'usam_view_tab[description]','value' => $view_tab['description']) ),
			array( 'type' => 'radio', 'title' => __( 'Включить комментарии', 'usam' ), 'option' => 'view_tab_comment', 'attribute' => array('name' => 'usam_view_tab[comment]','value' => $view_tab['comment']) ),		
			array( 'type' => 'radio', 'title' => __( 'Показать описание бренда', 'usam' ), 'option' => 'view_tab_brand', 'attribute' => array('name' => 'usam_view_tab[brand]','value' => $view_tab['brand']) ),			
		);
		
		
		?>	
		<div class="usam_container-table-container caption border">
			<div class="usam_container-table-caption-title"><?php _e( 'Вкладки товара', 'usam' ); ?></div>
			<?php $this->display_table_row_option( $options ); ?>
		</div>
		
		
		
		<div class="usam_user_profile_header">
			<div class="usam_user_profile_header_content">		
			<ul id="usam_accordion" class="usam_your_account_menu">
			<?php		
			foreach ( $this->menu_group as $menu )
			{
				?>
					<li class="usam_menu <?php echo ($this->name_group_tab == $menu['slug'])?'current_menu':''; ?> usam_menu-<?php echo $menu['slug']; ?>">
						<span class="usam_menu_name"><?php echo $menu['menu_title']; ?></span>				
						<ul class="usam_your_account_sub_menu">
						<?php 
						foreach ( $this->tabs[$menu['slug']] as $sub_menu )
						{
							?>
							<li class="usam_submenu <?php echo ($this->current_tab_id == $sub_menu['slug'])?'current_submenu':''; ?> usam_submenu-<?php echo $sub_menu['slug']; ?>">
								<a class = "usam_submenu_link-<?php echo $sub_menu['slug']; ?>" href="<?php echo  usam_get_user_account_url( $sub_menu['slug'] ); ?>"><?php echo $sub_menu['menu_title']; ?></a>
							</li>
							<?php
						}
						?>
						</ul>
					</li>
				<?php
			}
			?>
			</ul>
			</div>
		</div>		
		<?php		
	}
}