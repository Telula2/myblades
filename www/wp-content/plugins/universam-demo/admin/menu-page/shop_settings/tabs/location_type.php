<?php
class USAM_Tab_location_type extends USAM_Tab
{	
	private $data;
	
	public function __construct() 
	{				
		$this->header = array( 'title' => __('Типы местоположений', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить типы местоположений.', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );				
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}

	public function callback_submit() 
	{
		global $wpdb;
		switch( $this->current_action )
		{
			case 'delete':						
				$ids = implode(',',$this->records);
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_LOCATION_TYPE." WHERE id IN ($ids)");						
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':	
				if( !empty($_POST['name']) )
				{			
					$this->data['name'] = sanitize_text_field(stripslashes($_POST['name']));
					$this->data['code'] = sanitize_text_field($_POST['code']);
					$this->data['sort'] = absint($_POST['sort']);					
					$this->save( );					
				}				
			break;	
		}	
	}	
		
	private function get_column_format( ) 
	{
		$formats = array( 'name' => '%s', 'code' => '%s', 'sort' => '%d' );		
		return $formats;
	}
		
	function get_data_format( ) 
	{
		$formats = array();
		$formats_db = $this->get_column_format( );	
		foreach ( $this->data as $key => $value ) 
		{						
			if ( isset($formats_db[$key]) )		
				$formats[$key] = $formats_db[$key];	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	function save()
	{
		global $wpdb;
		
		$result = false;	
		if ( $this->id != null )
		{		
			$where_val = $this->id;			
			$where_col = 'id';
			$where_format = self::get_column_format( $where_col );			
			$where = array( $where_col => $where_format);

			$this->data = apply_filters( 'usam_location_update_data', $this->data );			
			$format = $this->get_data_format( );
			
			$result = $wpdb->update( USAM_TABLE_LOCATION_TYPE, $this->data, $where, $format, array( $where_val ) );	
		} 
		else 
		{   
			$this->data = apply_filters( 'usam_location_insert_data', $this->data );			
			$format = $this->get_data_format(  );
				
			$result = $wpdb->insert( USAM_TABLE_LOCATION_TYPE, $this->data, $format );	
			$this->id = $wpdb->insert_id;
		} 		
		return $result;
	}
}