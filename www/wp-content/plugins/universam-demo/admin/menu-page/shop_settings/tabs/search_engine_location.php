<?php
class USAM_Tab_search_engine_location extends USAM_Tab
{		
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Местоположение для поисковых систем', 'usam'), 'description' => __('Местоположения для поисковых систем используются для указания местоположения, когда заходит бот поисковиковых роботов.','usam') );				
		$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}	
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_search_engine_location' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );	
			break;	
			case 'save':	
				$new['location'] = absint($_POST['location']);			
				$new['search_engine'] = sanitize_text_field($_POST['search_engine']);				
				if ( $this->id != null )	
				{
					usam_edit_data( $new, $this->id, 'usam_search_engine_location' );	
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_search_engine_location' );				
				}
			break;			
		}			
	}
}