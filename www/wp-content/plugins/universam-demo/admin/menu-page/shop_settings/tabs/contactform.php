<?php
class USAM_Tab_contactform extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Контактная форма', 'usam'), 'description' => 'Сообщения которые Вам отправляют пользователи через кнотактную форму.' );
	}	
	
	public function display() 
	{
		?>		
		<table class='usam_setting_table usam_edit_table'>								
			<tr>
				<td class="name"><?php esc_html_e( 'Копировать посетителю на почту', 'usam' );?>:</td>
				<td>
					<input type="hidden" name="usam_options[usam_email_contactform_copy]" id="usam_casesensitive" value="0">
					<input type="checkbox" name="usam_options[usam_email_contactform_copy]" id="usam_casesensitive" <?php echo get_option('usam_email_contactform_copy', 1) == 1 ?"checked='checked'":'' ?> value="1">						
				</td>
			</tr>		
		</table>
		<?php
	}	
}
?>