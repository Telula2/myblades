<?php
class USAM_Tab_property extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Список свойств', 'usam'), 'description' => __('Здесь вы можете настроить список свойств заказа', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );			
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	

	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_ORDER_PROPS." WHERE id IN ($in)" );		
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'save':		
				if ( !empty($_POST['property_order']) )
				{							
					$new_property_order = stripslashes_deep($_POST['property_order']);
					$new_property_order['name'] = sanitize_text_field(stripslashes($_POST['name']));
					if ( ($new_property_order['type'] == 'select' || $new_property_order['type'] == 'checkbox' || $new_property_order['type'] == 'radio') && !empty( $_POST['options']) )
					{ 
						$new_property_order['options'] = array();
						foreach ( $_POST['options']['name'] as $key => $name )	
							$new_property_order['options'][] = array( 'name' => $name, 'value' => $_POST['options']['value'][$key] );				
					}			
					else
						$new_property_order['options'] = array();
					
					$new_property_order['active'] = !empty($_POST['active'])?1:0;	
					
					if ( empty($_POST['property_order']['mandatory']) )
						$new_property_order['mandatory'] = 0;
					
					if ( empty($_POST['property_order']['profile']) )
						$new_property_order['profile'] = 0;
					
					if ( empty($_POST['property_order']['fast_buy']) )
						$new_property_order['fast_buy'] = 0;
					
					if ( $this->id != null )	
					{				
						$property = new USAM_Order_Property( $this->id );		
					}
					else			
					{								
						$property = new USAM_Order_Property( );				
					}
					$property->set( $new_property_order );		
					$property->save();		
					
					$this->id = $property->get('id');						
				}
			break;			
		}
	}
}