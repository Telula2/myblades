<?php
class USAM_Tab_seo extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('SEO', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить бланки.','usam') );		
	}

	public function display() 
	{				
		usam_add_box( 'usam_currency_options', __( 'Настройка определения позиции сайта', 'usam' ), array( $this, 'display_options' ) );
	}
	
	public function display_options() 
	{		
		$options = array( 			
			array( 'type' => 'radio', 'title' => __( 'Включить проверку позиции сайта', 'usam' ), 'option' => 'check_position_site', 'description' => "",),		
			array( 'type' => 'input', 'title' => __( 'Запросов к Яндекс XML за раз', 'usam' ), 'option' => 'yandex_query_limit', 'description' => __( 'Максимальное количество запросов за раз.', 'usam' ), 'attribute' => array( 'maxlength' => "3", 'size' => "3"), 'default' => 10 ),				
			array( 'type' => 'input', 'title' => __( 'Запросов к Google за раз', 'usam' ), 'option' => 'google_query_limit', 'description' => __( 'Максимальное количество запросов за раз.', 'usam' ), 'attribute' => array( 'maxlength' => "3", 'size' => "3"), 'default' => 10 ),	
		);
		$this->display_table_row_option( $options );
	}
}