<?php
class USAM_Tab_message_discount_favorites extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сообщение о скидке на избранное', 'usam'), 'description' => 'Здесь вы можете посмотреть и изменить сообщения покупателю о скидке на те товары, которые он добавил в избранное.' );	
	}	
	
	public function display() 
	{
		?>   		
		<table class='usam_setting_table usam_edit_table'>			
			<tr>
				<td colspan='2'><?php esc_html_e( 'Метки могут быть использованы', 'usam' );?>: %order_id%, %shop_name%, %total_price%, %products%
					<div class = "detailed_description">
						<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
						<div class = "description_box">
							%order_id% - <?php esc_html_e( 'Номер заказа', 'usam' ); ?></br>
							%shop_name% - <?php esc_html_e( 'Название сайта', 'usam' ); ?></br>
							%total_price% - <?php esc_html_e( 'Общая сумма заказ покупателя', 'usam' ); ?></br> 
							%products% - <?php esc_html_e( 'Товары из избраного, которых нет в заказе', 'usam' ); ?></br>
						</div>
					</div>
				</td>
			</tr>
			<?php $this->subject( 'discount_favorites_subject' ); ?>
			<?php $this->message( 'discount_favorites_message' ); ?>
		</table>
		<?php    
	}		
}