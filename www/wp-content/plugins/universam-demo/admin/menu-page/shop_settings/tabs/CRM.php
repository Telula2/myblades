<?php

class USAM_Tab_CRM extends USAM_Tab
{	

	public function display() 
	{		
		usam_add_box( 'usam_affairs_managers', __( 'Ведение дел менеджеров', 'usam' ), array( $this, 'affairs_managers' ) );
		//usam_add_box( 'usam_marketing_settings', __( 'Раздел маркетинга', 'usam' ), array( $this, 'marketing_meta_box' ) );
	}	
	
	public function affairs_managers() 
	{			
		$record = get_option('usam_record_affairs_managers');
		?>		
		<table class='usam_setting_table usam_edit_table'>			
			<tr>
				<td class="name"><?php esc_html_e( 'Запись дел', 'usam' ); ?>:</td>
				<?php				
				$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator','shop_crm'), 'fields' => array( 'ID','display_name') );
				$users = get_users( $args );
				$managers = array();
				foreach( $users as $user )
				{						
					$managers[$user->ID] = $user->display_name;
				}				
				?>			
				<td><?php echo $this->get_checklist( 'usam_options[usam_record_affairs_managers]', $managers, $record ); ?></td>				
			</tr>									
		</table>
		<?php		
	}
	
	public function marketing_meta_box() 
	{
		$options = array(		
		//	array( 'type'=>'radio', 'title'=>__( 'Собирать данные клиентов', 'usam' ), 'option' => 'vk_сollect_customer_data', 'description'=>__( 'Собирать данные клиентов, зашедших на сайт. Собирает из социальной сети Контакт.', 'usam' ),),
		 );
		$this->display_table_row_option( $options );
	}	
}
?>