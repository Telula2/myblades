<?php
class USAM_Tab_cashbox extends USAM_Tab
{	
	public function __construct() 
	{		
		$this->header = array( 'title' => __('Кассы', 'usam'), 'description' => __('Здесь вы можете настроить кассу в соответствии с 54-ФЗ. 54-ФЗ - федеральный закон, определяющий каким образом ваш бизнес должен осуществлять расчеты с покупателями.', 'usam') );			
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table( );
	}
	

	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_cashbox' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;	
			break;	
			case 'save':
				$new['name'] = sanitize_text_field(stripcslashes($_POST['name']));
				$new['active'] = !empty($_POST['active'])?1:0;
				$new['handler'] = sanitize_title($_POST['handler']);
				$new['number'] = sanitize_text_field(stripcslashes($_POST['number']));			
				$new['date'] = date( "Y-m-d H:i:s" );
				$new['setting'] = !empty($_POST['cashbox'])?stripslashes_deep($_POST['cashbox']):array();
				
				if ( $this->id != null )	
				{
					$result = usam_edit_data( $new, $this->id, 'usam_cashbox' );
					if ( !$result )
					{
						$this->redirect = true;
						return;
					}				
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_cashbox' );				
				}	
			break;
		}
	}	
}