<?php
class USAM_Tab_General extends USAM_Tab
{
	public function __construct() 
	{
		$this->header = array( 'title' => __('Основные настройки', 'usam'), 'description' => 'Здесь вы можете посмотреть и изменить основные настройки Вашего магазина.' );	
		add_action( 'update_option_usam_currency_type', array($this, 'save_dafault_currency'), 10, 3 );
	}	
	
	// Установка базового курса
	public function save_dafault_currency( $old_value, $value, $option ) 
	{
		global $wpdb;
		$update = $wpdb->query( "UPDATE `" .USAM_TABLE_CURRENCY. "` SET `rate` = '0.00'");
	
		$currencies = new USAM_Currency( $value );
		$currencies->set( array( 'rate' => 1 ) );
		$currencies->save( );	
	}	
	
	public function callback_submit() 
	{
		if ( isset( $_POST['location'] ) ) 
		{		
			$location = absint($_POST['location']);
			update_option( 'usam_shop_location', $location );
		}
		if ( isset( $_POST['shop_company'] ) ) 
		{		
			$shop_company = absint($_POST['shop_company']);
			update_option( 'usam_shop_company', $shop_company );
		}				
		parent::callback_submit( );
	}
	
	public function display() 
	{				
		?>	
		<div class="postbox">
			<div class="inside">
				<h2 class="usam_setup"><?php printf( __( 'Вы можете использовать <a href="%s" target="_blank"><strong>мастер установки</strong></a>', 'usam' ), admin_url('admin.php?page=usam-setup') ); ?></h2>
			</div>
		</div>
		<?php		
		usam_add_box( 'usam_general_settings', __( 'Общие настройки', 'usam' ), array( $this, 'general_settings' ) );			
		usam_add_box( 'usam_currency_options', __( 'Настройки валюты', 'usam' ), array( $this, 'currency_options' ) );
		usam_add_box( 'usam_units', __( 'Единица измерения веса', 'usam' ), array( $this, 'units_meta_box' ) );
		usam_add_box( 'usam_rss_address', __( 'RSS Address', 'usam' ), array( $this, 'rss_address_meta_box' ) );
		usam_add_box( 'usam_allow_tracking', __( 'Центр сбора информации', 'usam' ), array( $this, 'allow_tracking_meta_box' ) );
		
	}	
			
	public function rss_address_meta_box()
	{
		?>
			<p><?php esc_html_e( 'Люди могут использовать канал RSS, чтобы быть в курсе вашего списка товаров..', 'usam' ); ?></p>
			<p><?php esc_html_e( 'Адрес RSS ленты', 'usam' ) ?> : <?php echo get_bloginfo( 'url' )."/index.php?xmlformat=rss&amp;action=feed"; ?></p>
		<?php
	}
	
	public function general_settings() 
	{					
		$location = get_option( 'usam_shop_location' );
		?>		
		<table class='usam_setting_table usam_edit_table'>			
			<tr>
				<td class="name"><?php esc_html_e( 'Местоположение магазина:', 'usam' ); ?></td>
				<td class="option">
					<?php
					$autocomplete = new USAM_Autocomplete_Forms( );
					$autocomplete->get_form_position_location( $location );
					?>	
				</td>
			</tr>	
			<?php $shop_company = get_option( 'usam_shop_company' ); ?>
			<tr>
				<td class="name"><?php _e( 'Компания магазина', 'usam' ); ?>:</td>
				<td class="option">					
					<?php usam_select_bank_accounts( $shop_company, array('name' => "shop_company") );
					if ( empty($shop_company) )	
					{
						?><p><?php _e( 'Добавьте вашу фирму и банковский счет в разделе', 'usam' ); ?> &laquo;<a target="_blank" href="<?php echo admin_url("admin.php?page=crm&tab=company"); ?>"><?php _e( 'Компании', 'usam' ); ?></a>&raquo; <?php _e( 'и укажите тип компании &laquo;своя&raquo;', 'usam' ); ?></p><?php
					}
					?>					
				</td>
			</tr>			
		</table>
		<?php		
	}
	
	public function units_meta_box() 
	{				
		$options = array( 
			array( 'type' => 'select', 'title' => __( 'Единица измерения веса', 'usam' ), 'option' => 'weight_unit', 
			   'options' => usam_get_weight_units( ), 'description' => ''),		
			array( 'type' => 'select', 'title' => __( 'Единица измерения', 'usam' ), 'option' => 'dimension_unit', 
			   'options' => usam_get_dimension_units( ),  'description' => ''),					
			  );
		$this->display_table_row_option( $options );
	}
	
	public function allow_tracking_meta_box() 
	{				
		$options = array( 
			array( 'type' => 'radio', 'title' => __( 'Автоматическое местоположение посетителя', 'usam' ), 'option' => 'get_customer_location', 'default' => 1 ),  
			array( 'type' => 'radio', 'title' => __( 'Отслеживание поведения', 'usam' ), 'option' => 'allow_tracking')  
		);
		$this->display_table_row_option( $options );
	}	
		
	public function currency_options() 
	{			
		$currencies = usam_get_currencies( );			
		
		$select_currency = array();
		foreach ( $currencies as $currency ) 
		{
			$select_currency[$currency->code] = $currency->code." ($currency->name)";
		}	
		$currency_sign = usam_get_currency_sign();		
						
		$options = array( 
			array( 'type' => 'select', 'title' => __( 'Валюта по-умолчанию', 'usam' ), 'option' => 'currency_type', 
			   'options' => $select_currency,
			   'description' => ''),
			array( 'type' => 'radio', 'title' => __( 'Расположение валюты', 'usam' ), 'option' => 'currency_sign_location', 
				'radio' => array( '1' => "100{$currency_sign}", '2' => "100 $currency_sign", '3' => "{$currency_sign}100", '4' => "$currency_sign 100",),
			    'description' => "",),
			array( 'type' => 'input', 'title' => __( 'Разделитель тысяч', 'usam' ), 'option' => 'thousands_separator', 'description' => '', 'attribute' => array( 'maxlength' => "1", 'size' => "1") ),
			array( 'type' => 'input', 'title' => __( 'Десятичный разделитель', 'usam' ), 'option' => 'decimal_separator', 'description' => '', 'attribute' => array( 'maxlength' => "1", 'size' => "1") ),	
			array( 'type' => 'text', 'title' => __( 'Предварительный просмотр', 'usam' ), 'html' => "10".get_option( 'usam_thousands_separator' )."000".get_option( 'usam_decimal_separator' )."00", 'description' => '' ),				
			  );
		$this->display_table_row_option( $options );
	}
}