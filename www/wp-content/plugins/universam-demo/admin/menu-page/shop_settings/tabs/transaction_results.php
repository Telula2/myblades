<?php
class USAM_Tab_transaction_results extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Результаты транзакции', 'usam'), 'description' => __('Здесь вы можете настроить сообщения отображаемые после покупки.', 'usam') );	
	}	
	
	private function print_tab( $key, $message ) 
	{	
		?>
		<div id = "message_<?php echo $key; ?>" class="tab">
			<h3 class="form_group"><?php echo $message['title']; ?></h3>
			<?php 
			$transaction_results = get_option( 'usam_page_transaction_results' );
			if ( !empty($transaction_results[$key]) )
				$text = $transaction_results[$key];
			else
				$text = $message['text'];
			
			wp_editor(stripslashes(str_replace('\\&quot;','',$text )),'usam_message_transaction_'.$key,array(
				'textarea_rows' => 30,
				'textarea_name' => 'usam_options[usam_page_transaction_results]['.$key.']',
				'media_buttons' => false,
				'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
				)	
			);
			?>			
		</div>		
		<?php 
	}
	
	public function display() 
	{			
		$this->display_order_labels(); 
		
		$message_transaction = usam_get_message_transaction();			
		?>
		<p><?php esc_html_e( 'Шорткоды, которые могут быть использованы', 'usam' );?>: <?php echo esc_html( '[current_day -1]' ); ?></p>
		<div class = "detailed_description">
			<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
			<div class = "description_box shortcodes">
				<span>[current_day +1]</span> - <?php esc_html_e( 'Текущая дата с разницей на указаное количество дней. Например, [current_day +1] - заменить на текущую дату плюс 1 день', 'usam' ); ?></br>
				<span>[if total_price=2000 {Показать этот текст}]</span> - <?php esc_html_e( 'Условный текст. Например, если сумма заказа равна 2000 покажет текст в фигурных скобках', 'usam' ); ?></br>						
			</div>
		</div>
		<div id = "tabs_transaction_results" class = "usam_tabs usam_tabs_style3">
			<div class = "header_tab">
				<ul>
					<?php 									
					foreach ( $message_transaction as $key => $message )
					{						
						?>			
						<li class = "tab"><a href="#message_<?php echo $key; ?>"><?php echo $message['title']; ?></a></li>
						<?php									
					}	
					?>
				</ul>
			</div>	
			<div class = "countent_tabs">	
				<?php						
				foreach ( $message_transaction as $key => $message )
				{				
					$this->print_tab( $key, $message );					
				}
				?>
			</div>
		</div>
		<?php
	}
}
?>