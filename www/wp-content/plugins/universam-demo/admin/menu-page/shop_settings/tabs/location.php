<?php
class USAM_Tab_Location extends USAM_Tab
{		
	private $data = array();
	
	private static $string_cols = array(
		'name',	
	);
	// цифровые
	private static $int_cols = array(
		'id',
		'parent',		
		'sort',
		'id_type',
	);
	
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Местоположения', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить местоположения.','usam') );	
		$this->buttons = array( 'add' => __('Добавить местоположение', 'usam'), 'import' => __('Импорт местоположений', 'usam') );			
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
	
	protected function callback_submit()
    {
		global $wpdb;	
	
		switch( $this->current_action )
		{
			case 'delete':						
				$ids = implode(',',$this->records);
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_LOCATION." WHERE id IN ($ids)");						
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':	
				if( !empty($_POST['name']) )
				{			
					$this->data['name'] = sanitize_text_field($_POST['name']);
					$this->data['sort'] = absint($_POST['sort']);
					$this->data['id_type'] = absint($_POST['id_type']);			
					$this->data['parent'] = absint($_POST['location']);					
					$this->save( );
				}
			break;			
			case 'screen':				
				if ( isset($_REQUEST['delete_existing']) ) 
				{
					$SESSION['pre_import'] = !empty($_REQUEST['delete_existing'])?1:0;
				}
			break;	
			case 'start_import':	
				$this->sendback = add_query_arg( array( 'action' => 'import' ), $this->sendback );		
				$step = !empty($_REQUEST['step'])?$_REQUEST['step']:1;
				if( $step == 1 ) 
				{
					$delete = !empty($_REQUEST['delete_existing'])?1:0;					
					set_transient( 'usam_start_import_location', array( 'source' => $_REQUEST['source'], 'delete_existing' => $delete ), HOUR_IN_SECONDS );						
					$this->redirect = true;	
				}
				elseif( $step == 2 ) 
				{		
					if ( isset($_FILES['file_location']) )
					{					
						$file = $_FILES['file_location'];
						$file_path = USAM_FILE_DIR . $file['name'];
						if ( move_uploaded_file( $file['tmp_name'], $file_path ) )
							set_transient( 'usam_file_location_file', $file_path );			
					}	
				}
				elseif( $step == 3 ) 
				{			
					$i = 0;	
					$data = get_transient('usam_start_import_location');		
					if ( !empty($data['delete_existing']) ) 
					{				
						$wpdb->query("TRUNCATE TABLE `".USAM_TABLE_LOCATION."`");
					}	
					if ( $data['source'] == 'file' ) 
					{			
						if ( isset($_REQUEST['row']) ) 
						{						
							$row = array_flip($_REQUEST['row']);		
							
							$file_path = get_transient( 'usam_file_location_file');	
							require_once( USAM_FILE_PATH . '/includes/data_exchange/import.class.php'         );	

							$import = new USAM_Import();
							$tables = $import->import_file_path( $file_path );						
					
							$ids = array();
							foreach ( $tables as $key => $value )
							{				
								$name   = isset($value[$row['name']])?$value[$row['name']]:0;
								$parent = isset($value[$row['parent']])?$value[$row['parent']]:0;
								$type = isset($value[$row['type']])?$value[$row['type']]:0;
								$id_location = isset($value[$row['id']])?$value[$row['id']]:0;	
								$this->id = 0;								
								$parent = isset($ids[$parent])?$ids[$parent]:0;						
								$this->data = array( 'name' => $name, 'parent' => $parent, 'id_type' => $type, 'sort' => 100 );
						
								$this->save( );				
								$ids[$id_location] = $this->id;	
								
								$i++;
							}	
							delete_transient( 'usam_file_location_file' );
							unlink($file_path); 					
						}				
					}
					else
					{
						
					}
					$this->sendback = remove_query_arg( array( 'action', 'step' ), $this->sendback );		
					$this->sendback = add_query_arg( array( 'ready' => $i ), $this->sendback );	
				}		
				$this->redirect = true;	
			break;	
		}					
	}		
	
	function get_column_format( $col ) 
	{
		if ( in_array( $col, self::$string_cols ) )
			return '%s';

		if ( in_array( $col, self::$int_cols ) )
			return '%d';
		
		if ( in_array( $col, self::$float_cols ) )
			return '%f';
		return false;
	}
	
	
	function get_data_format( ) 
	{
		$formats = array();
		foreach ( $this->data as $key => $value ) 
		{			
			$format = $this->get_column_format( $key );
			if ( $format !== false )		
				$formats[$key] = $format;	
			else
				unset($this->data[$key]);
		}
		return $formats;
	}		
	
	function data_format( ) 
	{
		foreach ( $this->data as $key => $value ) 
		{			
			if ( in_array( $key, self::$string_cols ) && !is_array($value) )
				$this->data[$key] = stripcslashes($value);
		}
	}
	
	function save()
	{
		global $wpdb;
		
		$result = false;	
		if ( $this->id != null )
		{		
			$where_val = $this->id;			
			$where_col = 'id';
			$where_format = self::get_column_format( $where_col );			
			$where = array( $where_col => $where_val);

			$this->data = apply_filters( 'usam_location_update_data', $this->data );			
			$format = $this->get_data_format( );
			$this->data_format( );		
			$result = $wpdb->update( USAM_TABLE_LOCATION, $this->data, $where, $format, array( $where_format ) );				
		} 
		else 
		{   
			$this->data = apply_filters( 'usam_location_insert_data', $this->data );			
			$format = $this->get_data_format(  );
			$this->data_format( );				
			
			$result = $wpdb->insert( USAM_TABLE_LOCATION, $this->data, $format );	
			$this->id = $wpdb->insert_id;
		} 		
		return $result;
	}
}