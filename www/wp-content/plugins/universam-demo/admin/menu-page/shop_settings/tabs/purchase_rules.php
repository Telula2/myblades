<?php
class USAM_Tab_purchase_rules extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Правила покупки', 'usam'), 'description' => __('Здесь вы можете создавать разные ограничения для совершения покупки. Заказ будет выполнен только по выполнению всех ограничений.','usam') );		
		$this->buttons = array( 'add' => __('Добавить правило', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{
			case 'delete':
				usam_delete_data($this->records, 'usam_purchase_rules');
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;
			case 'save':
				$new_rule['name'] = sanitize_text_field(stripcslashes($_POST['name']));		
				$new_rule['active'] = !empty($_POST['active'])?1:0;				
				$new_rule['conditions'] = $this->get_rules_basket_conditions(  );	
				$new_rule['description'] = sanitize_textarea_field($_POST['description']);
				if ( $this->id )		
				{			
					usam_edit_data( $new_rule, $this->id, 'usam_purchase_rules' );			
				}
				else			
				{					
					$this->id = usam_add_data( $new_rule, 'usam_purchase_rules' );							
				}		
			break;
		}
	}
}