<?php
class USAM_Tab_sales_area extends USAM_Tab
{		
	public function __construct() 
	{	
		$this->header = array( 'title' => __('Зоны продаж', 'usam'), 'description' => __('Здесь вы можете настроить зоны продаж.', 'usam') );	
		$this->buttons = array( 'add' => __('Добавить зону', 'usam') );				
	}	
	
	protected function load_tab()
	{		
		$this->list_table( );
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_sales_area' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'save':	
				$new = array();
				$new['name'] = sanitize_text_field(stripcslashes($_POST['name']));				
				$new['locations'] = isset($_POST['locations']) ? array_map('intval', $_POST['locations']) : array();
				
				if ( empty($new['locations']) )
					return false;
						
				if ( $this->id != null )	
				{
					usam_edit_data( $new, $this->id, 'usam_sales_area' );	
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_sales_area' );				
				}	
			break;			
		}			
	}	
}