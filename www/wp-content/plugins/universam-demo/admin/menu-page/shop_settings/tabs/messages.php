<?php
class USAM_Tab_Messages extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сообщения', 'usam'), 'description' => 'Здесь вы можете настроить сообщения.' );		
	}	
	
	public function display() 
	{
		usam_add_box( 'usam_sms_settings', __('СМС шлюз','usam'), array( $this, 'sms_display' ) );
		usam_add_box( 'usam_email_settings', __('Настройки электронной почты','usam'), array( $this, 'email_display' ) );		
		//usam_add_box( 'usam_viber_settings', __('Настройки Viber','usam'), array( $this, 'viber_display' ) );		
	}
	
	public function viber_display() 
	{
		$sms_gateway = get_option( 'usam_viber' );
		?>
		<table class='usam_setting_table usam_edit_table'>				
			<tr>
				<td class = "name"><?php esc_html_e( 'Логин', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_viber][login]' type='text' size='40' value='<?php echo esc_attr($sms_gateway['login']); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Логин для смс шлюза.' ); ?></td>
			</tr>
			<tr>
				<td class = "name"><?php esc_html_e( 'Пароль', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_viber][password]' type='text' size='40' value='<?php echo esc_attr($sms_gateway['password']); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Пароль для смс шлюза.' ); ?></td>
			</tr>
			<tr>
				<td class = "name"><?php esc_html_e( 'Подпись', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_viber][password]' type='text' size='40' maxlength="11" value='<?php echo esc_attr($sms_gateway['password']); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Укажите имя от, которого будут приходить сообщения.' ); ?></td>
			</tr>				
		</table>
		<?php		
	}
	
	public function sms_display() 
	{
		$sms_gateway = get_option( 'usam_sms_gateway_option' );
		?>
		<table class='usam_setting_table usam_edit_table'>				
			<tr>
				<td class = "name"><?php esc_html_e( 'Логин', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_sms_gateway_option][login]' type='text' size='40' value='<?php echo esc_attr($sms_gateway['login']); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Логин для смс шлюза.' ); ?></td>
			</tr>
			<tr>
				<td class = "name"><?php esc_html_e( 'Пароль', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_sms_gateway_option][password]' type='text' size='40' value='<?php echo esc_attr($sms_gateway['password']); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Пароль для смс шлюза.' ); ?></td>
			</tr>
			<tr>
				<td class = "name"><?php esc_html_e( 'Подпись', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_sms_gateway_name]' type='text' size='40' maxlength="11" value='<?php echo get_option( 'usam_sms_gateway_name' ); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Укажите имя от, которого будут приходить сообщения.' ); ?></td>
			</tr>				
			<tr>
				<td class = "name"><?php esc_html_e( 'Шлюз', 'usam' );?>:</th>
				<td>
					<?php
					$files = usam_list_dir( USAM_FILE_PATH .'/includes/mcommunicator/gateway' );	
					$gateways = array();
					foreach ( $files as $file ) 
					{
						if ( stristr( $file, '.php' ) ) 		
						{
							$file_path = USAM_FILE_PATH .'/includes/mcommunicator/gateway/'. $file;			
							$data = implode( '', file( $file_path ) );
							if ( preg_match( '|Phone Gateway:(.*)$|mi', $data, $name ) ) 
							{					
								$parts = explode( '.', $file );
								$gateways[] = array( 'id' => $parts[0], 'title' => $name[1] );
							}
						}		
					}			
					?>
					<select name="usam_options[usam_sms_gateway]">
						<?php
						foreach ( $gateways as $gateway ) 
						{
							?>
							<option value="<?php echo $gateway['id']; ?>" <?php echo (get_option('usam_sms_gateway')==$gateway['id']?'selected':''); ?>><?php echo $gateway['title']; ?></option>
							<?php
						}
						?>
					</select>
				</td>
				<td class = "description"><?php esc_html_e( 'Выберете шлюз для смс отправлений.' ); ?></td>
			</tr>	
			<tr>
				<td class = "name"><?php esc_html_e( 'Количество', 'usam' );?>:</th>
				<td><input class='text' name='usam_options[usam_max_number_of_sms_month]' type='text' size='40' value='<?php echo get_option( 'usam_max_number_of_sms_month' ); ?>' /></td>
				<td class = "description"><?php esc_html_e( 'Максимальное количество сообщение в месяц, разрешенное для отправки через шлюз. Если не указано, ограничений нет.' ); ?></td>
			</tr>					
		</table>
		<?php		
	}
	
	public function email_display() 
	{		
		global $user_ID;
		$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name','email'), 'user_id' => $user_ID ) );
		$managers = array( 0 => __( 'Не выбрано', 'usam' ) );
		foreach( $mailboxes as $mailbox )
		{
			$managers[$mailbox->id] = $mailbox->name.' ('.$mailbox->email.')';
		}		
		$options = array( 				
			array( 'title' => __( 'Электронная почта', 'usam' ), 'type' => 'select', 'option' => 'return_email', 
			   'options' => $managers,
			   'description' => __( 'Выберете основную электронную почту', 'usam' )),			   
			  );
			  
		$this->display_table_row_option( $options );
	}		
}
?>