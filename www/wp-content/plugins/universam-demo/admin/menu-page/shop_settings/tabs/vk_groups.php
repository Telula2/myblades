<?php
class USAM_Tab_vk_groups extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Группы вКонтакте', 'usam'), 'description' => __('Здесь Вы можете добавлять группы вКонтакте для публикации товаров и записей.','usam') );		
		$this->buttons = array( 'add' => __('Добавить группу', 'usam') );				
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':			
				usam_delete_data( $this->records, 'usam_vk_groups' );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );
			break;	
			case 'save':
				if ( !empty($_POST['group_page_id']) )
				{		
					$new['page_id'] = absint($_POST['group_page_id']);					
					$new['birthday'] = !empty($_POST['group_birthday'])?1:0;	
					$new['publish_reviews'] = !empty($_POST['publish_reviews'])?1:0;				
					$new['access_token'] = !empty($_POST['group_access_token'])?$_POST['group_access_token']:'';			
					$new['name'] = '';
					$new['photo'] = '';	
						
					$params = array(
						'group_ids' => $new['page_id'],
					); 							
					require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
					$vkontakte = new USAM_VKontakte_API();
					$vk_group = $vkontakte->send_request( $params, 'groups.getById' );//Возвращает информацию о заданном сообществе или о нескольких сообществах.
					if (  !empty($vk_group[0]) )
					{
						$new['name'] = $vk_group[0]['name'];
						$new['photo'] = $vk_group[0]['photo'];
					}		
					if ( $this->id != null )	
					{				
						usam_edit_data( $new, $this->id, 'usam_vk_groups' );					
					}
					else			
					{							
						usam_add_data( $new, 'usam_vk_groups' );		 
					}		
					
				/*	$url = 'http://oauth.vk.com/authorize?client_id='.$this->vk_api['api_id'].'&scope=wall,photos,ads,offline,friends,notifications,market&display=page&redirect_uri='.admin_url('admin.php?unprotected_query=vk_token_group');			
				
					wp_redirect( $url );
					exit;*/
				}
			break;			
		}			
	}
}