<?php
class USAM_Tab_message_trackingid extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Статус заказа', 'usam'), 'description' => 'Здесь вы можете посмотреть и изменить сообщения о трек-номере почтового отправления.' );	
	}	
		
	public function display() 
	{		
		?>   		
		<table class='usam_setting_table usam_edit_table'>		
			<tr>
				<td colspan='2'><?php esc_html_e( 'Метки могут быть использованы', 'usam' );?>: '%order_date%, %current_date%, %order_id%, %shop_name%, %product_list%, %payment_list%, %total_price%, %total_shipping%, %total_tax%, %advertising_products%', '%storage_phone%', '%storage_address%', '%storage_city%', '%storage_schedule%, %track_id%
					<div class = "detailed_description">
						<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
						<div class = "description_box">
							%order_date% - <?php esc_html_e( 'Дата заказа', 'usam' ); ?></br>
							%current_date% - <?php esc_html_e( 'Текущая дата', 'usam' ); ?></br>
							%order_id% - <?php esc_html_e( 'Номер заказа', 'usam' ); ?></br>				
							%shop_name% - <?php esc_html_e( 'Название сайта', 'usam' ); ?></br>					
							%product_list% - <?php esc_html_e( 'Список купленных товаров', 'usam' ); ?></br> 
							%payment_list% - <?php esc_html_e( 'Документ оплаты', 'usam' ); ?></br>
							%total_price% - <?php esc_html_e( 'Общая сумма заказа', 'usam' ); ?></br>
							%total_shipping% - <?php esc_html_e( 'Доставка', 'usam' ); ?></br>						
							%total_tax% - <?php esc_html_e( 'Налог заказа', 'usam' ); ?></br>
							%track_id% - <?php esc_html_e( 'Трек-номер', 'usam' ); ?>						
						</div>
					</div>
				</td>
			</tr>
			<?php $this->subject( 'trackingid_subject' ); ?>
			<?php $this->message( 'trackingid_message' ); ?>	
		</table>
		<?php    
	}	
}