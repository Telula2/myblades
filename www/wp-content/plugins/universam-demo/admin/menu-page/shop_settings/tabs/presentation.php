<?php

class USAM_Tab_Presentation extends USAM_Tab
{
	public function __construct() 
	{
		$this->header = array( 'title' => __('Внешний вид', 'usam'), 'description' => __('Здесь Вы можете настроить внешний вид страниц Вашего интернет магазина.','usam') );	
		add_action( 'update_option_usam_category_hierarchical_url', array($this, 'update_option_product_category_hierarchical_url') );	
	}
	
	function update_option_product_category_hierarchical_url() 
	{ 
		flush_rewrite_rules( false );
	}
		
	public function display() 
	{
		usam_add_box( 'usam_settings_page_catalog', __('Настройки страниц и категорий товаров','usam'), array( $this, 'settings_page_catalog' ) );	
		
		usam_add_box( 'usam_settings_cart', __('Настройки корзины','usam'), array( $this, 'settings_cart' ) );	
		usam_add_box( 'usam_thumbnail_settings', __('Настройки изображений','usam'), array( $this, 'thumbnail_settings' ) );			
		usam_add_box( 'usam_settings_menu', __('Настройки меню','usam'), array( $this, 'settings_menu' ) );			
	}
	
	
	public function settings_menu() 
	{
		?>			
		<table class='usam_setting_table usam_edit_table'>
			<tr>
				<td class="name"><?php esc_html_e( 'Категория', 'usam' ); ?>:</td>
				<td>
					<?php
					$current_default   = esc_attr( get_option( 'usam_default_menu_category' ) );				
					if ( $current_default == 0 )
						$selected = "selected='selected'";
					else
						$selected = "";
					?>
					<select name='usam_options[usam_default_menu_category]'>
						<option value='0' <?php echo $selected ?> ><?php esc_html_e( 'Категории товаров', 'usam' ); ?></option>
							<?php
							$args = array(
										'descendants_and_self' => 0,
										'selected_cats'        => array( $current_default ),
										'popular_cats'         => false,
										'walker'               => new Walker_Category_Select(),
										'taxonomy'             => 'usam-category',
										'checked_ontop'        => false, 
										'echo'                 => true,
									);
							wp_terms_checklist( 0, $args );
							?>
					</select>				
				</td>
				<td class='description'><?php esc_html_e( 'Выберите, категорию для начала меню', 'usam' ); ?></td>
			</tr>		
		</table>
		<?php
	}
			
	public function settings_page_catalog() 
	{
		?>			
		<h4 class = "header_settings"><?php esc_html_e( 'Общие', 'usam' ); ?></h4>	
		<?php	
		$options = array( 
			array( 'type' => 'select', 'title' => __( 'Вариант отображения товара', 'usam' ), 'option' => 'product_view', 
			   'options' => array( 'default' => __( 'По умолчанию', 'usam' ), 'list' => __( 'Списком', 'usam' ), 'grid' => __( 'Сеткой', 'usam' ),),
			   'description' => ''),	
			array( 'type' => 'radio', 'title' => __( 'Показать путь', 'usam' ), 'option' => 'show_breadcrumbs', 			  
			   'description' => __( 'Показать путь до страницы (хлебные крошки)', 'usam' )),	
			array( 'type' => 'radio', 'title' => __( 'Показывать список категорий', 'usam' ), 'option' => 'display_categories', 			  
			   'description' => __( 'Показывать список категорий', 'usam' )),			
			array( 'type' => 'select', 'title' => __( 'Сортировка по', 'usam' ), 'option' => 'product_sort_by', 
				'options' => array( 'name' => __('имени', 'usam'), 'price' => __('цене', 'usam'), 'dragndrop' => __('Drag &amp; Drop', 'usam'), 'rating' => __('рейтингу', 'usam'), 'popularity' => __('популярности', 'usam'), 'id' => __('номеру', 'usam') ), 'description' => '' ),
			array( 'type' => 'select', 'title' => __( 'Направление сортировки по', 'usam' ), 'option' => 'product_order', 
				'options' => array( 'ASC' => __('возрастанию', 'usam'), 'DESC' => __('убыванию', 'usam') ), 'description' => '',					
			  ),				   
			array( 'type' => 'select', 'title' => __( 'Показать проданные и недоступные товары', 'usam' ), 'option' => 'display_sold_products', 
			   'options' => array( '' => __( 'недоступные и проданные', 'usam' ), 1 => __( 'недоступные', 'usam' ), 2 => __( 'только доступные', 'usam' ),),
			   'description' => __( 'Не доступные товары это товары, которые находятся на складе не доступном для выдачи и отгрузки', 'usam' )),	
			array( 'type' => 'radio', 'title' => __( 'Показывать с нулевой ценой', 'usam' ), 'option' => 'show_zero_price', 
			   'description' => __( 'Показывать ли товары у которых цена равна нулю?', 'usam' )),			   
						   
			  );
			  
		$this->display_table_row_option( $options );		
		
		$view_category = get_option( 'usam_view_category' );
		?>
		<h4 class = "header_settings"><?php esc_html_e( 'Настройки на странице товаров', 'usam' ); ?></h4>
		<table class='usam_setting_table usam_edit_table'>
			<tr>
				<td class="name"><?php esc_html_e( 'Выберите, что показывать на странице', 'usam' ); ?>:</td>
				<td><?php echo $this->category_list(); ?></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Показать Избранные товары', 'usam' ); ?>:</td>
				<td>				
					<input type='radio' value='1' name='usam_options[usam_view_category][featured_products]' id='usam_hide_featured_products1' <?php checked($view_category['featured_products'], 1); ?> /> <label for='usam_hide_featured_products1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
					<input type='radio' value='0' name='usam_options[usam_view_category][featured_products]' id='usam_hide_featured_products2' <?php checked($view_category['featured_products'], 0); ?> /> <label for='usam_hide_featured_products2'><?php _e( 'Нет', 'usam' ); ?></label>
				</td>
			</tr>
		</table>
		<h4 class = "header_settings"><?php esc_html_e( 'Настройки в категориях товаров', 'usam' ); ?></h4>
		<?php
		$options = array( 
			array( 'type' => 'radio', 'title' => __( 'Иерархический URL категории продукта', 'usam' ), 'option' => 'category_hierarchical_url', 
			   'radio' => array( '0' => __( 'Нет', 'usam' ), '1' => __( 'Да', 'usam' ),),
			   'description' => __( 'Когда иерархический URL категории продукта включен, родительские категории продукта также будут включены в URL продукта.<br />Например:', 'usam' ).' <strong>example.com/product-category/parent-cat/sub-cat/product-name</strong>',),
			array( 'type' => 'radio', 'title' => __( 'Показать подкатегории товаров', 'usam' ), 'option' => 'show_subcatsprods_in_cat', 
			   'radio' => array( '0' => __( 'Нет', 'usam' ), '1' => __( 'Да', 'usam' ),),
			   'description' => __( 'Показать подкатегории товаров в родительской категори', 'usam'),),
			array( 'type' => 'radio', 'title' => __( 'Показать название', 'usam' ), 'option' => 'display_category_name', 
			   'radio' => array( '0' => __( 'Нет', 'usam' ), '1' => __( 'Да', 'usam' ),),
			   'description' => __( 'Показать название категории товара', 'usam'),),
			array( 'type' => 'radio', 'title' => __( 'Показать описание', 'usam' ), 'option' => 'category_description', 
			   'radio' => array( '0' => __( 'Нет', 'usam' ), '1' => __( 'Да', 'usam' ),),
			   'description' => __( 'Показать описание категории товара', 'usam'),),	
			  );
		
		$this->display_table_row_option( $options );
		?>					
		<h4 class = "header_settings"><?php _e( 'Настройки пагинации', 'usam' ); ?></h4>		
		<?php
		$options = array( 
			array( 'type' => 'radio', 'title' => __( 'Использовать разбиения на страницы', 'usam' ), 'option' => 'product_pagination', 'description' =>'' ),
			array( 'type' => 'input', 'title' => __( 'Количество продуктов на странице', 'usam' ), 'option' => 'products_per_page', 'description' => "" ),	
			array( 'type' => 'radio', 'title' => __( 'Расположение на странице', 'usam' ), 'option' => 'page_number_position', 
			   'radio' => array( '1' => __( 'Сверху', 'usam' ), '2' => __( 'Снизу', 'usam' ), '3' => __( 'Сверху и снизу', 'usam' )), 'description' =>'' ),
		 );			
		$this->display_table_row_option( $options );
	}	
	
	public function settings_cart() 
	{		
		$options = array(		
			array('type' => 'radio', 'title' => __( 'Форма ввода купонов', 'usam' ), 'option' => 'uses_coupons', 
				  'description' => __( 'Показывать форму ввода кодов купонов', 'usam' ), ),
			array( 'type' => 'radio', 'title' => __( 'Скрыть кнопку "Добавить в корзину"', 'usam' ), 'option' => 'hide_addtocart_button', 			  
			   'description' => ''),
			);									
		$this->display_table_row_option( $options );
	}
		
	public function thumbnail_settings() 
	{
		$product_image = get_option( 'usam_product_image' );
		$brand_image = get_option( 'usam_brand_image' );
		$category_image = get_option( 'usam_category_image' );
		$single_view_image = get_option( 'usam_single_view_image' );
		?>				
		<p><em><?php esc_html_e( 'Примечание: Когда Вы обновляете любую из настроек миниатюр, модуль магазина автоматически изменит все свои эскизы для вас. Продолжительность загрузки зависит от того, сколько изображений у вас есть.', 'usam' ); ?></em></p>
		<table class='usam_setting_table usam_edit_table'>
			<tr>
				<td class="name"><?php esc_html_e( 'Миниатюра товара', 'usam' ); ?>:</td>
				<td>
					<?php esc_html_e( 'Ширина', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_product_image][width]' class='usam_prod_thumb_option' value='<?php esc_attr_e( $product_image['width'] ); ?>' />&#8195;
					<?php esc_html_e( 'Высота', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_product_image][height]' class='usam_prod_thumb_option' value='<?php esc_attr_e( $product_image['height'] ); ?>' />

				</td>
				<td class="description"><?php esc_html_e( 'Размер эскизов по-умолчанию', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Миниатюра бренда', 'usam' ); ?>:</td>
				<td>
					<?php esc_html_e( 'Ширина', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_brand_image][width]' class='usam_prod_thumb_option' value='<?php esc_attr_e( $brand_image['width'] ); ?>' />&#8195;
					<?php esc_html_e( 'Высота', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_brand_image][height]' class='usam_prod_thumb_option' value='<?php esc_attr_e( $brand_image['height'] ); ?>' />
				</td>
				<td class="description"><?php esc_html_e( 'Размер эскизов брендов по-умолчанию', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Миниатюра категории', 'usam' ); ?>:</td>
				<td>
					 <?php esc_html_e( 'Ширина', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_category_image][width]' value='<?php esc_attr_e( $category_image['width'] ); ?>' />&#8195;
					<?php esc_html_e( 'Высота', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_category_image][height]' value='<?php esc_attr_e( $category_image['height'] ); ?>' />
				</td>
				<td class="description"><?php esc_html_e( 'Размер эскизов категорий по-умолчанию', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Изображение товара', 'usam' ); ?>:	</td>
				<td>
					<?php esc_html_e( 'Ширина', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_single_view_image][width]' value='<?php esc_attr_e( $single_view_image['width'] ); ?>' />&#8195;
					<?php esc_html_e( 'Высота', 'usam' ); ?>:<input type='text' size='6' name='usam_options[usam_single_view_image][height]' value='<?php esc_attr_e( $single_view_image['height'] ); ?>' />
				</td>
				<td class="description"><?php esc_html_e( 'Размер изображения товара при просмотре', 'usam' ); ?></td>
			</tr>						
			<?php			
			$options = array(
			array( 'type' => 'radio', 'title' => __( 'Обрезать миниатюры', 'usam' ), 'option' => 'crop_thumbnails', 
				  'description' => __( 'Выбор "Да" означает, что эскизы обрезаются точно по размерам (обычно эскизы пропорциональны)', 'usam' ),),			
			array('type' => 'radio', 'title' => __( 'Lightbox эффект изображений', 'usam' ), 'option' => 'show_thumbnails_thickbox', 									
				 'description' => __( 'Использование lightbox означает, что при нажатии на изображение товара, увеличенная версия будет отображаться в "lightbox" окне. Если вы используете плагин, такой как Shutter Reloaded, вы можете отключить lightbox.', 'usam' ),),
									 
			array( 'type' => 'radio', 'title' => __( 'Lightbox скрипт для использования', 'usam' ), 'option' => 'lightbox', 
					'radio' => array('colorbox' => __( 'Colorbox', 'usam' ), 'thickbox' => __( 'Thickbox', 'usam' ) ),
					'description' => '',),
				 
					);									
			$this->row_option( $options );			
			?>			
		</table>
		<?php		
	}
	
	public function category_list() 
	{		
		$current_default = esc_attr( get_option( 'usam_default_category' ) );
		$group_data      = get_terms( 'usam-category', 'hide_empty=0&parent=0' );
		$categorylist    = "<select name='usam_options[usam_default_category]'>";
		if ( $current_default == 'all' )
			$selected = "selected='selected'";
		else
			$selected = '';

		$categorylist .= "<option value='all' " . $selected . " >" . __( 'Показать все товары', 'usam' ) . "</option>";

		if ( $current_default == 'list' )
			$selected = "selected='selected'";
		else
			$selected = '';

		$categorylist .= "<option value='list' " . $selected . " >" . __( 'Показывать список категорий товаров', 'usam' ) . "</option>";
		$categorylist .= "<optgroup label='" . __( 'Категории товаров', 'usam' ) . "'>";
		foreach ( $group_data as $group ) 
		{
			$selected = "";
			if ( $current_default == $group->term_id )
				$selected = "selected='selected'";
			else
				$selected = "";

			$categorylist .= "<option value='" . $group->term_id . "' " . $selected . " >" . $group->name . "</option>";
			$category_data = get_terms( 'usam-category', 'hide_empty=0&parent=' . $group->term_id );
			if ( $category_data != null ) {
				foreach ( $category_data as $category ) {
					if ( $current_default == $category->term_id )
						$selected = "selected='selected'";
					else
						$selected = "";
					$categorylist .= "<option value='" . $category->term_id . "' " . $selected . " >" . $category->name . "</option>";
				}
			}
		}
		$categorylist .= "</optgroup>";
		$categorylist .= "</select>";
		return $categorylist;
	}
}