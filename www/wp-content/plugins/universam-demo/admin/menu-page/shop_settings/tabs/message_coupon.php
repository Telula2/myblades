<?php
class USAM_Tab_message_coupon extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Сообщение о купоне', 'usam'), 'description' => 'Здесь вы можете посмотреть и изменить сообщение, когда был создан купон из правила купонов.' );	
	}	
	
	public function display() 
	{
		?>   		
		<table class='usam_setting_table usam_edit_table'>			
			<tr>
				<td colspan='2'><?php esc_html_e( 'Метки могут быть использованы', 'usam' );?>: %order_id%, %shop_name%, %total_price%, %coupon_code%, %coupon_sum%, %coupon_day%, %order_sum%, %procent%
				<div class = "detailed_description">
					<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
					<div class = "description_box">
						%order_id% - <?php esc_html_e( 'Номер заказа', 'usam' ); ?></br>
						%shop_name% - <?php esc_html_e( 'Название сайта', 'usam' ); ?></br>
						%total_price% - <?php esc_html_e( 'Общая сумма заказ покупателя', 'usam' ); ?></br> 
						%coupon_code% - <?php esc_html_e( 'Код купона', 'usam' ); ?></br>
						%coupon_sum% - <?php esc_html_e( 'Сумма скидки по купону', 'usam' ); ?></br>
						%coupon_day% - <?php esc_html_e( 'Количество дней активности купона', 'usam' ); ?></br>
						%order_sum% - <?php esc_html_e( 'Сумма заказа, от которой можно использовать купон', 'usam' ); ?></br>
						%procent% - <?php esc_html_e( 'Использование при сумме больше чем(%)', 'usam' ); ?>
					</div>
				</div>
				</td>
			</tr>
			<?php $this->subject( 'coupon_add_subject' ); ?>
			<?php $this->message( 'coupon_add_message' ); ?>
			<?php $this->sms_message( 'coupon_add_sms' ); ?>
		</table>
		<?php    
	}	
}