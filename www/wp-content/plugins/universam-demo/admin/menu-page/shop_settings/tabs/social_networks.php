<?php
class USAM_Tab_social_networks extends USAM_Tab
{		
	public function __construct() 
	{
		$this->header = array( 'title' => __('Социальные сети', 'usam'), 'description' => __('Здесь вы можете настроить управление Вашими социальными сетями.','usam') );
	}	

	public function display() 
	{
		usam_add_box( 'usam_pinterest', __( 'Pinterest', 'usam' ), array( $this, 'pinterest_meta_box' ) );
	}	
	
	function pinterest_meta_box()
	{	
		$options = array( 						
			array( 'key' => 'verify', 'type' => 'input', 'title' => __( 'Подтвердить права на сайт', 'usam' ), 'option' => 'pinterest', 'attribute' => array( 'maxlength' => "50", 'size' => "50") ),	
		 );	  
		 $this->display_table_row_option( $options );
	}	
}