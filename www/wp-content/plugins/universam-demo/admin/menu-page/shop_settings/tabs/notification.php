<?php
class USAM_Tab_notification extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Уведомления о событиях', 'usam'), 'description' => __('Здесь вы можете указать какие уведомления будут получать менеджеры вашего интернет магазина.', 'usam') );
		$this->buttons = array( 'add' => __('Добавить уведомление', 'usam') );
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':		
				usam_delete_data( $this->records, 'usam_notifications' );		
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;					
			break;	
			case 'save':							
				$phone = absint($_POST['phone']);
				
				$new['name'] = sanitize_text_field(stripcslashes($_POST['name']));
				$new['active'] = !empty($_POST['active'])?1:0;		
				$new['email'] = sanitize_email($_POST['email']);			
				$new['phone'] = !empty($phone)?$phone:'';
				$new['events'] = isset($_POST['events'])?$_POST['events']:array();	
				$event_conditions = isset($_POST['conditions'])?$_POST['conditions']:array();				
					
				if ( empty($new['email']) && empty($new['phone']) )
				{
					$this->sendback = add_query_arg( array( 'error' => 1 ), $this->sendback );	
					return false;		
				}				
				foreach ($event_conditions as $event => $conditions)	
				{
					$new['events'][$event]['conditions'] = array();
					foreach ($conditions['type'] as $key => $type)	
					{ 		
						$new['events'][$event]['conditions'][$type][] = $conditions['value'][$key];	
					}
				}	
				if ( $this->id != null )	
				{
					usam_edit_data( $new, $this->id, 'usam_notifications' );	
				}
				else			
				{			
					$this->id = usam_add_data( $new, 'usam_notifications' );				
				}		
			break;				
		}			
	}
	
	protected function get_message_error()
	{	
		$message = '';
		if( isset($_REQUEST['error']) && $_REQUEST['error'] == 1 )		
			$message = __( 'Не верно указали электронную почту или телефон', 'usam' );		
		
		return $message;
	}
}
?>