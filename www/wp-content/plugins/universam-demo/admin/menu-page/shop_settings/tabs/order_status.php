<?php
class USAM_Tab_order_status extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Статусы заказов', 'usam'), 'description' => __('Здесь вы можете посмотреть и изменить статусы заказов.', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{		
		$this->list_table( );
	}	
	
	public function callback_submit() 
	{
		switch( $this->current_action )
		{		
			case 'delete':			
				foreach ( $this->records as $internalname )	
					usam_delete_order_status( $internalname );
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'save':	
				if( !empty($_POST['name']) )
				{					
					$data['name'] = sanitize_text_field(stripcslashes($_POST['name']));			
					$data['description'] = sanitize_textarea_field(stripcslashes($_POST['description']));	
					$data['short_name'] = sanitize_text_field(stripcslashes($_POST['short_name']));	
					$data['internalname'] = sanitize_title($_POST['internalname']);	
					$data['sort'] = isset($_POST['sort'])?(int)$_POST['sort']:100;	
					$data['visibility'] = !empty($_POST['visibility'])?1:0;
					$data['close'] = !empty($_POST['close'])?1:0;	
					$data['color'] = sanitize_text_field(stripcslashes($_POST['color']));	
					if ( $this->id != null )
					{				
						usam_update_order_status( $this->id, $data );
					}
					else
					{
						$this->id = usam_insert_order_status( $data );
						if ( empty($this->id) )
							$this->sendback = add_query_arg( array( 'error' => 1, 'action' => 'add' ), $this->sendback );	
					}	
				
				}				
			break;				
		}	
	}	
	
	protected function get_message_error()
	{	
		$message = '';
		if( isset($_REQUEST['error']) && $_REQUEST['error'] == 1 )		
			$message = __( 'Не указали код статуса или код уже существует', 'usam' );		
		
		return $message;
	}
}