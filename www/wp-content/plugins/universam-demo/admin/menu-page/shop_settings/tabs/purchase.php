<?php
class USAM_Tab_purchase extends USAM_Tab
{	
	public function __construct() 
	{
		$this->header = array( 'title' => __('Заказ', 'usam'), 'description' => '' );
		
		add_filter( 'pre_update_option_usam_registration_require', array($this, 'update_option_registration_require'), 10, 3 );			
	}		
	
		/**
	 * Автоматически включить "Любой может зарегистрироваться", если регистрация перед проверкой требуется.
	 */
	function update_option_registration_require( $value, $old_value, $option ) 
	{		
		if ( $value == 1 && get_option( 'users_can_register' ) )
		{ 
			$value = 0;	
			$message = __( 'Вы хотели обязать своих клиентов войти перед покупкой. В настройках Wordpress отключите <a href="%s">"любой может зарегистрироваться"</a>.', 'usam' );
			$message = sprintf( $message, admin_url( 'options-general.php' ) );		
			$this->set_user_screen_error( $message );
		}	
		return $value;
	}	
	
	public function display() 
	{
		usam_add_box( 'usam_payment', __('Настройки платежных документов','usam'), array( $this, 'payment' ) );	
		usam_add_box( 'usam_inventory_control', __('Складской учет','usam'), array( $this, 'inventory_control' ) );	
		usam_add_box( 'usam_bonus_rules', __('Правило использование бонусов','usam'), array( $this, 'bonus_rules' ) );	
		usam_add_box( 'usam_download_file', __( 'Загрузка файлов', 'usam' ), array( $this, 'download_file' ) );	
		usam_add_box( 'usam_checkout_options', __( 'Оформления заказа', 'usam' ), array( $this, 'checkout_options' ) );				
	}	
	
	public function checkout_options()
	{
		$options = array( 
			array( 'type' => 'radio', 'title' => __( 'Обязательная регистрация', 'usam' )." ".usam_help_tip( __( 'Если да, то вы должны также включить опцию WordPress "Любой может зарегистрироваться"', 'usam' ) ), 'option' => 'registration_require', 			
			    'description' => __( 'Пользователи должны зарегистрироваться, прежде чем рассчитать заказ', 'usam' ) ),							  
			 array( 'type' => 'radio', 'title' => __( 'Использовать SSL', 'usam' ), 'option' => 'force_ssl', 			
			    'description' => __( 'Заставить пользователей использовать SSL', 'usam' ) ),						
			  );
		$this->display_table_row_option( $options );		
	}
	
	public function download_file() 
	{				
		$options = array( 
			array( 'type' => 'input', 'title' => __( 'Максимальное число загрузок для файла', 'usam' ), 'option' => 'max_downloads', 'description' => '', 'attribute' => array( 'maxlength' => "3", 'size' => "3") ),
			array( 'type' => 'radio', 'title' => __( 'Блокировка загрузки на IP-адрес', 'usam' ), 'option' => 'ip_lock_downloads', 'description' => "",),
			array( 'type' => 'radio', 'title' => __( 'Проверьте MIME типы на закачку файла', 'usam' ), 'option' => 'check_mime_types', 'description' => __( 'Предупреждение: отключение этого элемента подвергает ваш сайт большей опасности злонамеренных загрузок файлов. Мы не рекомендуем отключать его.', 'usam' ),),			
		 );
		$this->display_table_row_option( $options );		
	}
	
	public function payment() 
	{
		$options = array( 
			array( 'type' => 'input', 'title' => __( 'Дней отсрочки', 'usam' ), 'option' => 'number_days_delay_payment', 'default' => 3, 'description' => __( 'Количество дней отсрочки оплаты', 'usam' ), 'attribute' => array( 'maxlength' => "3", 'size' => "3") ),			
		 );
		$this->display_table_row_option( $options );	
	}
	
	public function bonus_rules() 
	{
		$rules = get_option('usam_bonus_rules', array('percent' => 0, 'bonus_coupon' => 0 ) );		
		?>			
		<table class='usam_setting_table usam_edit_table'>			
			<tr>
				<td class="name"><label for ="usam_bonus_rules"><?php esc_html_e( 'Оплата бонусами', 'usam' ); ?>:</td>
				<td>				
					<input type='text' name='usam_options[usam_bonus_rules][percent]' id='usam_bonus_rules' value ="<?php echo $rules['percent']; ?>" />
				</td>
				<td class='description'><?php  esc_html_e( 'Процент от заказа, который можно оплачивать бонусами', 'usam' ); ?></td>
			</tr>
			<tr>
					<td class = "name"><?php _e('Купоны вместе с бонусами', 'usam') ?>:</td>
					<td>
						<input type='radio' value='1' name='usam_options[usam_bonus_rules][bonus_coupon]' id='bonus_coupon1' <?php checked( $rules['bonus_coupon'], 1 ); ?> />
						<label for='bonus_coupon1'><?php _e( 'Да', 'usam' );?></label> &nbsp;
						<input type='radio' value='0' name='usam_options[usam_bonus_rules][bonus_coupon]' id='bonus_coupon2' <?php checked( $rules['bonus_coupon'], 0 ); ?> />
						<label for='bonus_coupon2'><?php _e( 'Нет', 'usam' );?></label>
					</td>
				<td class = "description"><?php _e( 'Можно ли использовать купоны и бонусы вместе', 'usam' ); ?>.</td>
			</tr>					
		</table>	
		<?php
	}
	
	public function inventory_control() 
	{		
		$storage = get_option('usam_default_reservation_storage', '' );
		$stores = usam_get_stores();
		
		$reservation_storage = array();
		foreach ($stores as $value)
		{					
			$reservation_storage[$value->id] = $value->title;
		}
		
		$options = array( 
			array( 'type' => 'input', 'title' => __( 'Хранение корзин', 'usam' ), 'option' => 'time_keeping_baskets', 'description' => __( "Установите время хранения товаров в корзине клиентов. Вы также можете указать десятичные, такие как '0.5 дней'. Обратите внимание, что минимальный интервал, который можно ввести, 1 день, т.е. вы не можете запланировать его для запуска каждые 0.5 дня", 'usam' ), 'attribute' => array( 'maxlength' => "5", 'size' => "5") ),
			array( 'type' => 'select', 'title' => __( 'Склад резервирования', 'usam' ), 'option' => 'default_reservation_storage', 
				'options' => $reservation_storage,
			    'description' =>  __( 'Склад резервирования по умолчанию', 'usam' ),),	
			array( 'type' => 'select', 'title' => __( 'Товар резервируется', 'usam' ), 'option' => 'product_reserve_condition', 
				'options' => array( 'o' => __( 'При оформлении заказа', 'usam' ), 'p' => __( 'При полной оплате заказа', 'usam' ), 'd' => __( 'При разрешении доставки', 'usam' ), 's' => __( 'При отгрузке', 'usam' ), ),
			    'description' =>  __( 'Настройка резервирования товара', 'usam' ),),
			array( 'type' => 'input', 'title' => __( 'Снятие резервов', 'usam' ), 'option' => 'product_reserve_clear_period', 'description' => __( "Установите время храниния резервов в днях", 'usam' ), 'attribute' => array( 'maxlength' => "5", 'size' => "5") ),
		 );
		$this->display_table_row_option( $options );
	}
}
?>