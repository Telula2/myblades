<?php
class USAM_Tab_vk extends USAM_Tab
{	
	private $vk_api;
	public function __construct() 
	{
		$this->header = array( 'title' => __('вКонтакте', 'usam'), 'description' => __('Здесь вы можете настроить управление Вашей страницей в социальной сети вКонтакте.','usam') );
		
		add_filter( 'pre_update_option_usam_vk_products_publish_time', array($this, 'products_publish_time'), 10, 3 );
		$this->vk_api = get_option('usam_vk_api', array('api_id' => ''));	
	}
	
	// Период публикации в часах
	public function products_publish_time( $value, $old_value, $option ) 
	{
		if ( !empty($value) )			
			$publish_time = (int)$value; // через сколько публиковать товар
		else
			$publish_time = 0;
		
		return $publish_time;
	}
	
	public function add_meta_options_help_center_tabs( ) 
	{
		$url = site_url();
		$url_arr = explode(".", basename($url));
		$domain = $url_arr[count($url_arr)-2] . "." . $url_arr[count($url_arr)-1];
		
		$tabs[] = new USAM_Help_Center_Item('basic-help', __( 'Настройки API ВКонтакте', 'usam' ), __( 'Настройки API ВКонтакте', 'usam' ),
			array(
				'content' => '<p>' .sprintf(__( 'Чтобы получить доступ к API ВКонтакте, вам нужно <b><a %s>создать приложение</a></b> со следующими настройками', 'usam' ), 'href="http://vk.com/editapp?act=create" target="_blank"').':</p>
		<ol>
			<li><strong>Название:</strong> любое</li>
			<li><strong>Тип:</strong> Standalone-приложение</li>				
		</ol>
		<p>В настройках приложения необходимо установить параметры в разделе <strong>Open API</strong>:</p>
		<ol>
			<li><strong>Адрес сайта:</strong> '.$url.'</li>
			<li><strong>Базовый домен:</strong> '.$domain.'</li>
		</ol>
		<p>'.sprintf(__( 'Если приложение с этими настройками у вас было создано ранее, вы можете найти его на <b><a %s>странице приложений</a></b> и, нажав "Редактировать", найти его ID.'), 'href="http://vk.com/apps?act=settings" target="_blank"').':</p>',
			)
		);
		return $tabs;
	}	
	
	public function callback_submit() 
	{				
		parent::callback_submit( );		
		if (!empty($_POST['vk_api']) )
		{
			$vk_api = stripslashes_deep($_POST['vk_api']);	
			$vk_api['api_id'] = (int)$vk_api['api_id'];							
		}	
		else
			$vk_api = array();
		
		update_option('usam_vk_api', $vk_api );	
	} 

	public function display() 
	{
		usam_add_box( 'usam_application', __( 'Приложение', 'usam' ), array( $this, 'application_meta_box' ) );			
		usam_add_box( 'usam_main_settings', __( 'Общие настройки', 'usam' ), array( $this, 'main_settings_meta_box' ) );	
		usam_add_box( 'usam_decor', __( 'Оформление', 'usam' ), array( $this, 'decor_meta_box' ) );	
	}	
	
	public function application_meta_box()
	{
		?>			
		<table class = "usam_setting_table usam_edit_table">		
			<tr>				
				<td class="name"><label for='vk_api_id'><?php esc_html_e( 'ID приложения', 'usam' ); ?>:</label></td>
				<td><input type="text" name="vk_api[api_id]" id="vk_api_id" value="<?php echo $this->vk_api['api_id']; ?>"></td>
				<td class='description'><?php esc_html_e( 'Инструкции по созданию приложения смотри в помощи', 'usam' ); ?></td>
			</tr>	
			<tr>				
				<td class="name"><label for='vk_service_token'><?php esc_html_e( 'Сервисный ключ доступа', 'usam' ); ?>:</label></td>
				<td><input type="text" name="vk_api[service_token]" id="vk_service_token" value="<?php echo $this->vk_api['service_token']; ?>"></td>
				<td class='description'><?php esc_html_e( 'Инструкции по созданию приложения смотри в помощи', 'usam' ); ?></td>
			</tr>			
		</table>		
		<?php		
	}
	
	public function main_settings_meta_box()
	{	
		$options = array(    	
			array( 'type' => 'checkbox', 'title' => __( 'Товар дня', 'usam' ), 'option' => 'vk_publish_product_day', 'description' => __( "Публиковать товары дня вКонтакте.", 'usam' ),),			
		 );   
		$this->display_table_row_option( $options ); 
	}
	
	public function decor_meta_box()
	{		
		$prices = usam_get_prices( );
		$select_prices = array();
		foreach ( $prices as $price ) 
		{
			$select_prices[$price['code']] = $price['code']." - ".$price['title'];
		}			
		$options = array( 							
			array('key' => 'from_group', 'type' => 'checkbox', 'title' => __( 'Имя кто, опубликовал', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Если отметить, то запись будет опубликована от имени группы. Иначе от имени пользователя.', 'usam' ) ),					
			array('key' => 'from_signed', 'type' => 'checkbox', 'title' => __( 'Добавить пользователя', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Добавить к сообщению пользователя, опубликовавшего пост.', 'usam' ) ),	
			array('key' => 'add_link', 'type' => 'checkbox', 'title' => __( 'Добавить cсылку', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Добавить ссылку на статью на сайте.', 'usam' ) ),	
			array('key' => 'fix_product_day', 'type' => 'checkbox', 'title' => __( 'Закреплять Товар Дня', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Закреплять Товар Дня.', 'usam' ) ),
			array('key' => 'upload_photo_count', 'type' => 'select', 'title' => __( 'Изображения', 'usam' ), 'option' => 'vk_autopost', 
				'options' => array( 0,1,2,3,4,5), 'description' => __('Сколько изображений из статьи прикрепить к сообщению.', 'usam') ),			
			array( 'key' => 'excerpt_length', 'type' => 'input', 'title' => __( 'Анонс', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Сколько слов из статьи опубликовать в качестве анонса.', 'usam' ), 'attribute' => array( 'maxlength' => "4", 'size' => "4") ),		
			array( 'key' => 'excerpt_length_strings', 'type' => 'input', 'title' => __( 'Анонс', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Сколько знаков из статьи опубликовать в качестве анонса. Не рекомендуется больше 2688.', 'usam' ), 'attribute' => array( 'maxlength' => "4", 'size' => "4") ),
			array( 'key' => 'post_message', 'type' => 'textarea', 'title' => __( 'Сообщение для записей', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %title% - заголовок статьи, %excerpt% - анонс статьи, %link% - ссылка на статью', 'usam' ) ),
			array( 'key' => 'product_message', 'type' => 'textarea', 'title' => __( 'Сообщение для товаров', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %title% - заголовок статьи, %price_currency% - цена с валютой, %price% - цена, %old_price% - старая цена, %old_price_currency% - старая цена с валютой, %price_and_discont% - цена и скидка, если есть, %discont% - скидка, %excerpt% - анонс статьи, %link% - ссылка на статью, %name% - название сайта.' ) ),
			array( 'key' => 'product_day_message', 'type' => 'textarea', 'title' => __( 'Сообщение для товара дня', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %title% - заголовок статьи, %price% - цена, %excerpt% - анонс статьи, %link% - ссылка на статью, %name% - название сайта.' ) ),
			array( 'key' => 'reviews_message', 'type' => 'textarea', 'title' => __( 'Сообщение для отзывов', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %header% - заголовок, %review_title% - заголовок отзыва, %review_rating% - рейтинг, %review_author% - имя покупателя,%review_response% - ответ, %review_excerpt% - отзыв, %link% - ссылка на товар, %link_catalog% - ссылка на каталог.', 'usam' ) ),			
			array( 'key' => 'product_review_message', 'type' => 'textarea', 'title' => __( 'Сообщение для отзывов товара', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %review_title% - заголовок отзыва, %review_rating% - рейтинг, %review_author% - имя покупателя,%review_response% - ответ, %review_excerpt% - отзыв, %link% - ссылка на товар, %link_catalog% - ссылка на каталог.', 'usam' ) ),			
			array( 'key' => 'birthday', 'type' => 'textarea', 'title' => __( 'Сообщение с поздравлением ДР', 'usam' ), 'option' => 'vk_autopost', 'description' => __( 'Маска сообщения для стены ВКонтакте: %user_link% - ссылка на страницу, %first_name% - имя, %last_name% - фамилия, %sex% - пол, %city% - город, %country% - страна, %photo_50% - фото, %photo_100% - фото. Все метки должны быть в {}, например {%user_link% %photo_100%}', 'usam' ) ),		
			array('key' => 'type_price', 'type' => 'select', 'title' => __( 'Цены', 'usam' ), 'option' => 'vk_autopost', 
				'options' => $select_prices, 'description' => __('Выберите цену для публикации.', 'usam') ),
		 );	 
		$this->display_table_row_option( $options ); 
	}	
}