<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_blanks extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		$title = sprintf( __('Изменить бланк &laquo;%s&raquo;','usam'), trim($this->data['title']) );
		return $title;
	}
	
	protected function toolbar_buttons( ) 
	{
		$url = usam_url_admin_action( 'printed_form', admin_url('admin.php'), array( 'form' => $this->id, 'type' => $this->data['type'] ) );
		?>
		<ul>
			<li><a href="<?php echo $url; ?>" target="_blank" class="button button-primary"><?php _e('Просмотр','usam'); ?></a></li>
		</ul>
		<?php	
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )		
			$this->data = usam_get_data_printing_forms( $this->id );	
	}
	
	function display_left()
	{			
		if ( $this->id == null )
			return;		
		
		$url = usam_url_admin_action('edit_blank', admin_url( 'admin.php' ), array( 'blank' => $this->id, 'type' => $this->data['type'] ));	
		?>	
		<script type="text/javascript">
		  function resizeIframe(obj){
			 obj.style.height = 0;
			 obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
		  }
		</script>
		<p><?php echo trim($this->data['description']) ?></p>
		<input type="hidden" name ="save-close" value="1" />
		<iframe id="edit_form_blanks" data-id = "<?php echo $this->id; ?>" style="width:800px; height:1100px; border:1px solid #ccc;" onload='resizeIframe(this)' src="<?php echo $url; ?>"></iframe> 			
        <?php
    }	
}
?>