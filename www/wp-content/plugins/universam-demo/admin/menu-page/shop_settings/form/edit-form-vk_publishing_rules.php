<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_vk_publishing_rules extends USAM_Edit_Form
{		
	protected function get_data_tab(  )
	{							
		if ( $this->id != null )
		{				
			$this->data = usam_get_data( $this->id, 'usam_vk_publishing_rules' );
		}
		else
		{					
			$this->data = array('name' => '', 'active' => 0, 'quantity' => 1, 'exclude' => 10, 'from_hour' => 7, 'to_hour' => 24, 'periodicity' => 4, 'quantity' => 1, 'start_date' => '', 'end_date' => '', 'pricemin' => '', 'pricemax' => '', 'minstock' => '', 'terms' => array( 'category' => array(), 'brands' => array(), 'category_sale' => array() ) );
		} 
	}	
	
    public function settings( )
	{  				
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>
					<td class ="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
					<td><?php usam_display_datetime_picker( 'start', $this->data['start_date'], array( 'hour' ) ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['end_date'],  array( 'hour' ) ); ?></td>
				</tr>	
				<tr>
					<td class ="name"><?php esc_html_e( 'Часы для публикации', 'usam' );  ?>:</td>
					<td>
						<span><?php esc_html_e( 'с', 'usam' );  ?><span>
						<input type='text' class='text' size='10' maxlength='10' style='width:170px;' name='from_hour' value='<?php echo !empty($this->data['from_hour'])?$this->data['from_hour']:'';  ?>' />
						<span><?php esc_html_e( 'по', 'usam' );  ?><span>
						<input type='text' class='text' size='10' maxlength='10' style='width:170px;' name='to_hour' value='<?php echo !empty($this->data['to_hour'])?$this->data['to_hour']:'';  ?>' />
					</td>				
				</tr>	
				<tr>
					<td class ="name"><?php esc_html_e( 'Периодичность', 'usam' );  ?>:</td>
					<td><input type='text' class='text' size='2' name='periodicity' value='<?php echo $this->data['periodicity'];  ?>' /></td>		
					<td class ="description"><?php esc_html_e( 'Задайте час для повторения публикации', 'usam' );  ?></td>					
				</tr>
				<tr>
					<td class ="name"><?php esc_html_e( 'Исключить', 'usam' );  ?>:</td>
					<td><input type='text' class='text' size='2' name='exclude' value='<?php echo $this->data['exclude'];  ?>' /></td>		
					<td class ="description"><?php esc_html_e( 'Исключить опубликованные товары из повторной публикации на указанное количество дней.', 'usam' );  ?></td>					
				</tr>
				<tr>
					<td class ="name"><?php esc_html_e( 'Диапазон цен', 'usam' );  ?>:</td>
					<td>
						<span><?php esc_html_e( 'От', 'usam' );  ?><span>
						<input type='text' class='text' size='10' maxlength='10' style='width:170px;' name='pricemin' value='<?php echo number_format( $this->data['pricemin'], 2, '.', '' );  ?>' />
						<span><?php esc_html_e( 'До', 'usam' );  ?><span>
						<input type='text' class='text' size='10' maxlength='10' style='width:170px;' name='pricemax' value='<?php echo number_format( $this->data['pricemax'], 2, '.', '' );  ?>' />
					</td>				
				</tr>	
				<tr>
					<td class ="name"><?php esc_html_e( 'Минимальный остаток', 'usam' );  ?>:</td>
					<td><input type='text' class='text' size='10' name='minstock' value='<?php echo $this->data['minstock'];  ?>' /></td>				
				</tr>						
				<tr>
					<td class ="name"><?php esc_html_e( 'Товаров для публикации', 'usam' );  ?>:</td>
					<td>
						<select name='quantity'>
							<option <?php selected( $this->data['quantity'], 1 ); ?> value="1">1</option>
							<option <?php selected( $this->data['quantity'], 2 ); ?> value="2">2</option>
							<option <?php selected( $this->data['quantity'], 3 ); ?> value="3">3</option>
							<option <?php selected( $this->data['quantity'], 4 ); ?> value="4">4</option>
						</select>	
					</td>				
				</tr>				
			</tbody>
		</table>
      <?php
	}       
	
	public function terms_settings( ) 
	{		
		?>		  
		<div class="container_column">		
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите группы', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие группы установить налог.', 'usam') ?></p>
			</div>			
			<div id="all_taxonomy" class="all_taxonomy">	
				<?php
				$this->display_meta_box_group( 'category', $this->data['terms']['category'] ); 
				$this->display_meta_box_group( 'brands', $this->data['terms']['brands'] ); 
				$this->display_meta_box_group( 'category_sale', $this->data['terms']['category_sale'] ); 				
				?>						
			</div>					
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>	
		</div>			
	   <?php   
	}
	
	protected function controller_get_condition_vk_profiles ( )
	{ 
		$option = get_option('usam_vk_profile', '' );
		$profiles = maybe_unserialize( $option );	
		$result = array();		
		foreach ( $profiles as $profile ) 
		{            
           $result[$profile['id']] = $profile['first_name'].' '.$profile['last_name'];
        }	
		return $result;
    }  
	
	protected function controller_get_condition_vk_groups ( )
	{ 
		$option = get_option('usam_vk_groups', '' );
		$groups = maybe_unserialize( $option );	
		$result = array();
		foreach ( $groups as $group ) 
		{            
           $result[$group['id']] = $group['name'];
        }	
		return $result;
    }    
	
	public function profiles( ) 
	{		
		?>		  
		<div class="container_column">		
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите группы', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие группы установить налог.', 'usam') ?></p>
			</div>			
			<div id="all_taxonomy" class="all_taxonomy">	
				<?php
				$this->display_meta_box_group( 'vk_profiles', $this->data['profiles'], __('Профили пользователей','usam') ); 		
				$this->display_meta_box_group( 'vk_groups', $this->data['groups'], __('Группы','usam') ); 					
				?>						
			</div>					
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>	
		</div>			
	   <?php   
	}
	  
	
	function display_left()
	{				
		$this->titlediv( $this->data['name'] );
		$this->add_box_status_active( $this->data['active'] );	
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'settings' ) );
		usam_add_box( 'usam_terms_settings', __('Группы товаров','usam'), array( $this, 'terms_settings' ) );
		usam_add_box( 'usam_profiles', __('Профили пользователей и группы','usam'), array( $this, 'profiles' ) );
	}
}
?>