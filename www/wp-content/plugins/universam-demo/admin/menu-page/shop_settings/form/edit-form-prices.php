<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_prices extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить цену &#171;%s&#187;','usam'), $this->data['title'] );
		else
			$title = __('Добавить цену', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{					
		$default = array( 'title' => '', 'code' => '', 'type' => 'R', 'currency' => get_option("usam_currency_type"), 'base_type' => 0, 'underprice' => 0, 'available' => 1, 'rounding' => '0.01', 'locations' => array(), 'roles' => array(), 'sort' => 100 );
		
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_type_prices');
			$this->data = array_merge ( $default, $this->data );	
		}
		else
		{			
			$prices = usam_get_prices( array('type' => 'all') );	
			$this->data = $default;	
			$price = array_pop($prices);
			$id = $price['id']+1;
			$this->data['code'] = 'tp_'.$id;		
		}
	}	
		
    public function box_prices( )
	{  			
		$prices = usam_get_prices( );
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>
					<td class="name"><?php _e('Код','usam'); ?>:</td>
					<td>
						<input type="text" name="price[code]" value="<?php echo $this->data['code']; ?>" size="45"  autocomplete="off"/>
					</td>
				</tr>
				<tr>
					<td class="name"><?php _e('Тип валюты','usam'); ?>:</td>
					<td><?php usam_select_currencies( $this->data['currency'], array( "name" => "price[currency]", "class" => "chzn-select" ) ); ?></td>
				</tr>
				<tr>
					<td class="name"><?php _e('Тип','usam'); ?>:</td>
					<td>									
						<select class="chzn-select" name="price[type]">
							<option value="R" <?php selected($this->data['type'], 'R'); ?>><?php _e('Розничная','usam'); ?></option>
							<option value="P" <?php selected($this->data['type'], 'P'); ?>><?php _e('Закупочная','usam'); ?></option>							
						</select>							
					</td>
				</tr>	
				<?php if ( $this->data['type'] == 'R' ) { ?>	
				<tr>
					<td class="name"><?php _e('Базовая цена','usam'); ?>:</td>
					<td>									
						<select class="chzn-select" name="base_type">
							<option value="0" <?php selected($this->data['base_type'], 0 ); ?>>
								<?php _e('Не наследуется','usam'); ?>
							</option>
							<?php												
							foreach ( $prices as $value )
							{					
								if  ( $value['base_type'] == 0 && $value['id'] != $this->data['id'] )
								{
									?><option value="<?php echo $value['code']; ?>" <?php selected($this->data['base_type'], $value['code']); ?>><?php echo $value['title']; ?></option><?php
								}
							}				
							?>
						</select>							
					</td>
				</tr>							
					<tr>				
						<td class="name"><?php _e('Наценка','usam'); ?>:</td>
						<td>
							<input type="text" name="price[underprice]" value="<?php echo $this->data['underprice']; ?>"  autocomplete="off"/>
						</td>
					</tr>													
					<tr>
						<td class="name"><?php _e('Доступна для покупателей','usam'); ?>:</td>
						<td>
							<input type='checkbox' name="price[available]" value="1" <?php checked( $this->data['available'], 1 ); ?>/>
						</td>
					</tr>	
				<?php } ?>	
				<tr>
					<td class="name"><?php _e('Порядок округления','usam'); ?>:</td>
					<td><input type="text" name="price[rounding]" value="<?php echo $this->data['rounding']; ?>" size="5" autocomplete="off" /></td>
				</tr>		
				<tr>
					<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
					<td><input type="text" name="price[sort]" value="<?php echo $this->data['sort']; ?>" size="5" autocomplete="off"/></td>
				</tr>					
			</tbody>
		</table>
      <?php
	}     

	public function box_roles( ) 
	{			
		?>  							
		<div class="container_column">			
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите роли', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие роли установить цену.', 'usam') ?></p>
			</div>			
				
			<div id="all_taxonomy" class="all_taxonomy">	
				<?php $this->display_meta_box_group( 'roles', $this->data['roles'] ); ?>
			</div>	
			
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>			
		</div>					
	   <?php   
	}	
  
	
	function display_left()
	{					
		$this->titlediv( $this->data['title'] );		
		usam_add_box( 'usam_type_price', __('Параметры','usam'), array( $this, 'box_prices' ) );
		if ( $this->data['type'] == 'R' ) 
		{ 
			usam_add_box( 'usam_roles', __('Роли покупателей','usam'), array( $this, 'box_roles' ) );
			usam_add_box( 'usam_locations', __('Местоположение','usam'), array( $this, 'selecting_locations' ), $this->data['locations'] );
		}
    }
}
?>