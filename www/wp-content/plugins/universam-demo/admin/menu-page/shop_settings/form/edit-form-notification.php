<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_notification extends USAM_Edit_Form
{	
	private $alt;	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить уведомление &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить уведомление', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{		
		if ( $this->id != null )							
			$this->data = usam_get_data( $this->id, 'usam_notifications' );
		else
			$this->data = array( 'name' => '', 'active' => 0, 'email' => '', 'phone' => '', 'events' => array( ) );				
	}	
		
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );	
		$this->add_box_status_active( $this->data['active'] );			
		
		usam_add_box( 'usam_events', __( 'О чем уведомлять', 'usam' ), array( $this, 'display_events' ) );	
		usam_add_box( 'usam_where_to_report', __( 'Куда уведомлять', 'usam' ), array( $this, 'display_where_to_report' ) );				
    }	
	
	public function display_events() 
	{		
		?>
		<div id = "feedback" class = "usam_tabs usam_tabs_style2">
			<div class = "header_tab">
				<ul>
					<li class = "tab"><a href="#order"><?php _e( 'Новый заказ', 'usam' ); ?></a>|</li>
					<li class = "tab"><a href="#feedback"><?php _e( 'Новые сообщения', 'usam' ); ?></a>|</li>				
					<li class = "tab"><a href="#webproduct"><?php _e( 'Нет у поставщика', 'usam' ); ?></a>|</li>				
					<li class = "tab"><a href="#low_stock"><?php _e( 'Низкий запас', 'usam' ); ?></a>|</li>	
					<li class = "tab"><a href="#no_stock"><?php _e( 'Нет в наличии', 'usam' ); ?></a></li>
				</ul>
			</div>	
			<div class = "countent_tabs">					
				<?php 
				$this->print_tab( 'order', esc_html__( 'Сообщать о новых заказах', 'usam' ) ); 
				$this->print_tab( 'feedback', esc_html__( 'Сообщать об обращениях через контактную форму', 'usam' ) ); 
				$this->print_tab( 'webproduct', esc_html__( 'Сообщать об отсутствии товара у поставщика', 'usam' ) ); 
				$this->print_tab( 'low_stock', esc_html__( 'Сообщать о малом запасе товара', 'usam' ) ); 
				$this->print_tab( 'no_stock', esc_html__( 'Сообщать об отсутствии товара на вашем сайте', 'usam' ) ); 				
				?>			
			</div>
		</div>
	<?php 
	}
	
	function print_tab( $type_message, $title ) 
	{		
		$events = !empty($this->data['events'][$type_message])?$this->data['events'][$type_message]:array('email' => 0, 'sms' => 0, 'condition' => array() );
		
		$email_checked = !empty($events['email'])?1:0;
		$sms_checked = !empty($events['sms'])?1:0;
		?>
		<div id = "<?php echo $type_message; ?>" class="tab">	
			<h4 class="form_group"><?php echo $title; ?></h3>	
			<table class="subtab-detail-content-table usam_edit_table">
				<tbody>					
					<tr>				
						<td class="name"><?php _e('Электронная почта','usam'); ?>:</td>
						<td>
							<input type='hidden' name='events[<?php echo $type_message; ?>][email]' value='0' />
							<input id="notification_events_<?php echo $type_message; ?>" value='1' <?php checked( '1', $email_checked ); ?> type='checkbox' name='events[<?php echo $type_message; ?>][email]' />
						</td>
					</tr>	
					<tr>				
						<td class="name"><?php _e('Телефон','usam'); ?>:</td>
						<td>
							<input type='hidden' name='events[<?php echo $type_message; ?>][sms]' value='0' />
							<input id="notification_events_<?php echo $type_message; ?>" value='1' <?php checked( '1', $sms_checked ); ?> type='checkbox' name='events[<?php echo $type_message; ?>][sms]' />
						</td>
					</tr>								
				</tbody>
			</table>
			<h4 class="form_group"><?php _e('Условия','usam'); ?></h3>	
			<div class="condition">				
				<?php 						
				$conditions = !empty($events['conditions'])?$events['conditions']:array();
				switch ( $type_message ) 
				{
					case 'order' :
						if ( empty($conditions) )
							$conditions = array( 'prices' => array( '' ) );
						$this->display_conditions( $type_message, $conditions );		
					break;
					case 'feedback' :												
						?>
						<p><?php _e('У этого события нет условий','usam'); ?></p>						
						<?php	
						
						if ( empty($conditions) )
							$conditions =  array( 'sales_area' => array( '' ) );
					//	$this->display_conditions( $type_message, $conditions );					
					break;
					case 'webproduct' :
						?>
						<p><?php _e('У этого события нет условий','usam'); ?></p>						
						<?php	
					break;			
					case 'low_stock' :
						if ( empty($conditions) )
							$conditions = array( 'category' => array( '' ) );						
					
						$stock = !empty($events['stock'])?$events['stock']:10;
						?>
						<p><label><?php _e('Запас от','usam'); ?>: <input type='text' name='events[<?php echo $type_message; ?>][stock]' value='<?php echo $stock; ?>' /></label></p>								
						<?php	
						$this->display_conditions( $type_message, $conditions );							
					break;
					case 'no_stock' :							
						if ( empty($conditions) )
							$conditions = array( 'category' => array( '' ) );	
						$this->display_conditions( $type_message, $conditions );						
					break;
				}			
				?>
			</div>	
		</div>		
		<?php
	}
	
	public function display_conditions( $type, $conditions )
	{			
		?>		
		<table class = "table_rate">
			<thead>
				<tr>
					<th><?php _e('Что проверить', 'usam' ); ?></th>
					<th><?php _e('Значение', 'usam' ); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>				
				<?php if ( ! empty( $conditions ) ): ?>
					<?php				
						foreach( $conditions as $id => $condition )
						{
							foreach( $condition as $value )
								$this->output_row_conditions( $type, $id, $value );							
						}
					?>
				<?php else: ?>
					<?php $this->output_row_conditions( $type ); ?>
				<?php endif ?>
			</tbody>
		</table>	
        <?php
	}     

	private function output_row_conditions( $type, $id = '', $value = '' ) 
	{		
		$class = ( $this->alt ) ? ' class="alternate"' : '';
		$this->alt = ! $this->alt;							
		?>
		<tr <?php echo $class; ?>>
			<td class="column_name center">
				<div class="cell-wrapper condition_type">					
					<select id ="check_type" name="conditions[<?php echo $type; ?>][type][]">
						<?php
						switch ( $type ) 
						{
							case 'order' :							
								?>	
								<option value="prices" <?php selected( $id, 'prices'); ?>><?php esc_html_e( 'Типы цен', 'usam'); ?></option>	
							<?php
							break;
							case 'feedback' :								
								?>	
								<option value="sales_area" <?php selected( $id, 'sales_area'); ?>><?php esc_html_e( 'Зоны продаж', 'usam'); ?></option>	
							<?php
							break;
							case 'low_stock' :	
							case 'no_stock' :									
								?>	
								<option value="brands" <?php selected( $id, 'brands'); ?>><?php echo esc_html__( 'Бренд', 'usam'); ?></option>		
								<option value="category" <?php selected($id, 'category'); ?>><?php echo esc_html__( 'Категория товара', 'usam'); ?></option>	
								<option value="category_sale" <?php selected($id, 'category_sale'); ?>><?php echo esc_html__( 'Категория скидок', 'usam'); ?></option>
							<?php
							break;
						}
						?>								
					</select>				
				</div>					
			</td>				
			<td class="td_condition_value">							
				<?php
				switch ( $type ) 
				{
					case 'order' :	
						?>	
						<div class="cell-wrapper">						
							<div id="check_price" class="check_blok <?php echo $id=='prices'?'show':'hidden'; ?>">	
								<select class ="condition_value" name="conditions[<?php echo $type; ?>][value][]" <?php echo $id=='prices'?'':'disabled = "disabled"'; ?>>
									<option value=""<?php selected($value, ''); ?>><?php esc_html_e( 'Все цены', 'usam'); ?></option>
									<?php 
									$prices = usam_get_prices( array('type' => 'R') );		
									foreach ($prices as $price)	
									{									
										?>
										<option value="<?php echo $price['code']; ?>"<?php selected($value, $price['code']); ?>><?php echo $price['title']; ?></option>
										<?php
									}	
									?>	
								</select>	
							</div>
						</div>
					<?php
					break;
					case 'feedback' :	
						?>	
						<div class="cell-wrapper">						
							<div id="check_sales_area" class="check_blok <?php echo $id=='sales_area'?'show':'hidden'; ?>">	
								<select class ="condition_value" name="conditions[<?php echo $type; ?>][value][]" <?php echo $id=='sales_area'?'':'disabled = "disabled"'; ?>>
									<option value=""<?php selected($value, ''); ?>><?php esc_html_e( 'Все зоны', 'usam'); ?></option>
									<?php 
									$option = get_option('usam_sales_area');
									$sales_area = maybe_unserialize( $option );
									foreach ($sales_area as $area)	
									{									
										?>
										<option value="<?php echo $area['id']; ?>"<?php selected($value, $area['id']); ?>><?php echo $area['name']; ?></option>
										<?php
									}	
									?>	
								</select>	
							</div>
						</div>
					<?php
					break;
					case 'low_stock' :	
					case 'no_stock' :	
						?>	
						<div class="cell-wrapper">						
							<div id="check_brands" class="check_blok <?php echo $id=='brands'?'show':'hidden'; ?>">	
								<select class ="condition_value" name="conditions[<?php echo $type; ?>][value][]" <?php echo $id=='brands'?'':'disabled = "disabled"'; ?>>								
								<?php 
								$terms = get_terms('usam-brands', array( 'hide_empty' => 0 ));	
								foreach ($terms as $term)	
								{									
									?><option value="<?php echo $term->term_id; ?>"<?php selected($value, $term->term_id); ?>><?php echo $term->name; ?></option><?php
								}	
								?>	
								</select>	
							</div>
							<div id="check_category" class="check_blok <?php echo $id=='category'?'show':'hidden'; ?>">	
								<select class ="condition_value" name="conditions[<?php echo $type; ?>][value][]" <?php echo $id=='category'?'':'disabled = "disabled"'; ?>>
								<option value=""<?php selected($value, ''); ?>><?php esc_html_e( 'Все категории', 'usam'); ?></option>
								<?php 
								$terms = get_terms('usam-category', array( 'hide_empty' => 0 ));	
								foreach ($terms as $term)	
								{									
									?><option value="<?php echo $term->term_id; ?>"<?php selected($value, $term->term_id); ?>><?php echo $term->name; ?></option><?php
								}	
								?>	
								</select>	
							</div>
							<div id="check_category_sale" class="check_blok <?php echo $id=='category_sale'?'show':'hidden'; ?>">	
								<select class ="condition_value" name="conditions[<?php echo $type; ?>][value][]" <?php echo $id=='category_sale'?'':'disabled = "disabled"'; ?>>
								<?php 
								$terms = get_terms('usam-category_sale', array( 'hide_empty' => 0 ));	
								foreach ($terms as $term)	
								{									
									?><option value="<?php echo $term->term_id; ?>"<?php selected($value, $term->term_id); ?>><?php echo $term->name; ?></option><?php
								}	
								?>	
								</select>	
							</div>						
						</div>
					<?php
					break;
				}
				?>						
			</td>
			<td class="td_condition_value">								
				<div class="actions">
					<a tabindex="-1" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a>
					<a tabindex="-1" title="<?php _e( 'Удалить уровень', 'usam' ); ?>" class="action delete" href="#"><?php _e( 'Удалить', 'usam' ); ?></a>
				</div>
			</td>
		</tr>
		<?php
	}	
	
	public function display_where_to_report() 
	{		
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>					
				<tr>				
					<td class="name"><?php _e('Электронная почта','usam'); ?>:</td>
					<td>
						<input type="text" name="email" value="<?php echo $this->data['email']; ?>"/>
					</td>
				</tr>	
				<tr>				
					<td class="name"><?php _e('Телефон','usam'); ?>:</td>
					<td>
						<input type="text" name="phone" value="<?php echo $this->data['phone']; ?>"/>
					</td>
				</tr>								
			</tbody>
		</table>
		<?php
	}			
}
?>