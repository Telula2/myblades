<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_search_engine_region extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить местоположение &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить местоположение', 'usam');	
		return $title;
	}	
	
	protected function get_data_tab(  )
	{		
		if ( $this->id != null )
		{				
			global $wpdb;
			$this->data = $wpdb->get_row( "SELECT * FROM ".USAM_TABLE_SEARCH_ENGINE_REGIONS." WHERE id = '".$this->id."'", ARRAY_A );			
		}
		else
			$this->data = array( 'location_id' => '', 'name' => '', 'code' => '', 'search_engine' => '', 'active' => 0, 'sort' => 100 );		
	}	     
	
	function display_left()
	{			
		?>
		<div class="location_site">
			<?php
			$autocomplete = new USAM_Autocomplete_Forms( );
			$autocomplete->get_form_position_location( $this->data['location_id'], array( 'type' => 'all' ) );			
			?>
		</div>
		<?php
		$this->add_box_status_active( $this->data['active'] );	
		usam_add_box( 'usam_locations', __('Параметры региона','usam'), array( $this, 'settings_locations' ) );		
    }		
	
	function settings_locations( )
	{			
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>					
			<tr>
				<td class ="name"><?php esc_html_e( 'Код региона в поисковой системе', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['code']; ?>" name="code"/></td>				
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Название региона в поисковой системе', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['name']; ?>" name="name"/></td>				
			</tr>
			<tr>
				<td class ="name"><?php esc_html_e( 'Поисковая система', 'usam' );  ?>:</td>
				<td>
					<select name = "search_engine">
						<option value='g' <?php selected('g',$this->data['search_engine']); ?>><?php esc_html_e( 'Google', 'usam' );  ?></option>
						<option value='y' <?php selected('y',$this->data['search_engine']); ?>><?php esc_html_e( 'Яндекс', 'usam' );  ?></option>
					</select>	
				</td>				
			</tr>
			<tr>
				<td class ="name"><?php esc_html_e( 'Сортировка', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['sort']; ?>" name="sort" autocomplete="off" /></td>				
			</tr>
		</table>		
		<?php
	}		
}
?>