<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_mailboxes extends USAM_Edit_Form
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить электронную почту','usam');
		else
			$title = __('Добавить электронную почту', 'usam');	
		return $title;
	}
	
	protected function get_data_tab() 	
	{		
		if ( $this->id != null )
		{
			$this->data = usam_get_mailbox( $this->id );
		}
		else	
		{ 
			$this->data =  array( 'name' => '', 'email' => '', 'user_id' => '', 'pop3server' => '', 'pop3port' => 995, 'pop3user' => '', 'pop3pass' => '', 'pop3ssl' => 1, 'smtpserver' => '', 'smtpport' => 465, 'smtpuser' => '', 'smtppass' => '', 'smtp_secure' => 'SSL', 'delete_server' => 0, 'delete_server_day' => 0, 'delete_server_deleted' => 0 );
		}		
		add_action( 'admin_footer', array(&$this, 'admin_footer') );				
	}	
	
	function admin_footer()
	{
		echo usam_get_modal_window( __('Проверить соединение','usam').usam_get_loader(), 'check_connection', $this->get_modal_window(), 'medium' );	
	}
	
	function get_modal_window()
	{				
		$out = "<div class='status_connection'></div>";	
		return $out;
	}
	
	public function box_settings()
	{	
		?>		
		<table class="subtab-detail-content-table usam_edit_table" >			
			<tbody>
				<tr>				
					<td class="name"><?php esc_html_e( 'Имя', 'usam' ); ?>:</td>
					<td><input type="text" name="mailbox[name]" value="<?php echo $this->data['name']; ?>"></td>
				</tr>
				<tr>
					<td class="name"><?php esc_html_e( 'Электронная почта', 'usam' ); ?>:</td>
					<td><input type="text" name="mailbox[email]" value="<?php echo $this->data['email']; ?>"></td>				
				</tr>								
			</tbody>
		</table>		
		<?php 
	}	
	
	public function pop3_settings()
	{		
		?>		
		<div class="usam_container-table-container caption border">
			<div class="usam_container-table-caption-title"><?php _e( 'POP3 сервер', 'usam' ); ?></div>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" class="subtab-detail-content-table usam_edit_table ">
				<tbody>					
					<tr><td class="name"><?php esc_html_e( 'POP3 сервер', 'usam' ); ?>:</td><td><input type="text" name="mailbox[pop3server]" value="<?php echo $this->data['pop3server']; ?>"></td></tr>
					<tr><td class="name"><?php esc_html_e( 'Порт', 'usam' ); ?>:</td><td><input type="text" name="mailbox[pop3port]"   value="<?php echo $this->data['pop3port']; ?>"></td></tr>
					<tr><td class="name"><?php esc_html_e( 'Пользователь', 'usam' ); ?>:</td><td><input type="text" name="mailbox[pop3user]"   value="<?php echo $this->data['pop3user']; ?>"></td></tr>
					<tr><td class="name"><?php esc_html_e( 'Пароль', 'usam' ); ?>:</td><td><?php usam_get_password_input( $this->data['pop3pass'], array( 'name' => 'mailbox[pop3pass]') ); ?></td></tr>
					<tr>
						<td class="name"><?php esc_html_e( 'Требуется шифрованное подключение (SSL)', 'usam' ); ?>:</td>
						<td><input type='hidden' value='0' name='mailbox[pop3ssl]' />
						<input value='1' <?php checked($this->data['pop3ssl'],1); ?> type='checkbox' name='mailbox[pop3ssl]' /></td>
					</tr>
					<tr>
						<td class="name"></td>
						<td><button id='test' type="button" class="button"><?php _e( 'Тест', 'usam' ); ?></button></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="usam_container-table-container caption border">
			<div class="usam_container-table-caption-title"><?php _e( 'Очистка ящика на сервере', 'usam' ); ?></div>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" class="subtab-detail-content-table usam_edit_table ">
				<tbody>						
					<tr>
						<td class="name"><?php esc_html_e( 'Удалять на сервере', 'usam' ); ?>:</td>
						<td><input type='hidden' value='0' name='mailbox[delete_server]' />
						<input value='1' <?php checked($this->data['delete_server'],1); ?> type='checkbox' name='mailbox[delete_server]' /></td>
					</tr>
					<tr>
						<td class="name"><?php esc_html_e( 'Удалять с сервера через', 'usam' ); ?>:</td>
						<td><input type="text" name="mailbox[delete_server_day]" value="<?php echo $this->data['delete_server_day']; ?>" size='10' maxlength='10' style="width:40px">&nbsp;<?php esc_html_e( 'дней', 'usam' ); ?></td>
					</tr>
					<tr>
						<td class="name"><?php esc_html_e( 'Удалять с сервера при удалении', 'usam' ); ?>:</td>
						<td><input type='hidden' value='0' name='mailbox[delete_server_deleted]' />
						<input value='1' <?php checked($this->data['delete_server_deleted'],1); ?> type='checkbox' name='mailbox[delete_server_deleted]' /></td>
					</tr>					
				</tbody>
			</table>
		</div>
		<?php 
	}	
	
	public function smtp_settings()
	{	
		?>		
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>
				<tr><td class="name"><?php esc_html_e( 'SMTP сервер', 'usam' ); ?>:</td><td><input type="text" name="mailbox[smtpserver]" value="<?php echo $this->data['smtpserver']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Порт', 'usam' ); ?>:</td><td><input type="text" name="mailbox[smtpport]"   value="<?php echo $this->data['smtpport']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Пользователь', 'usam' ); ?>:</td><td><input type="text" name="mailbox[smtpuser]"   value="<?php echo $this->data['smtpuser']; ?>"></td></tr>
				<tr><td class="name"><?php esc_html_e( 'Пароль', 'usam' ); ?>:</td><td><?php usam_get_password_input( $this->data['smtppass'], array( 'name' => 'mailbox[smtppass]') ); ?></td></tr>
				<tr>
					<td class="name"><?php esc_html_e( 'Тип шифрованного подключения', 'usam' ); ?>:</td>
					<td>
					<select name="mailbox[smtp_secure]">
						<option value="none" <?php selected($this->data['smtp_secure'], 'none'); ?>>None</option>
						<option value="SSL" <?php selected($this->data['smtp_secure'], 'SSL'); ?>>SSL</option>
						<option value="TLS" <?php selected($this->data['smtp_secure'], 'TLS'); ?>>TLS</option>	
					</select>
					</td>
				</tr>
			</tbody>
		</table>
		<?php 
	}	
	
	function select_users()
	{
		usam_select_manager('', array('id' => 'users') );
		?>	
		<button id='add_participant' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>			
		<div class='participants'>
			<?php
			$users = usam_get_mailbox_users( $this->id );
			foreach ( $users as $user_id )
			{				
				?>		
				<div class='user-box' data-user_id="<?php echo $user_id; ?>">			
					<div class='user_wrapper'>	
						<div class='user_name'>
							<?php echo usam_get_manager_name($user_id); ?>
						</div>					
						<a class="js_delete_action" href="#"></a>
					</div>	
				</div>	
				<?php
			}
			?>	
		</div>	
		<?php
	}
	
	function display_left()
	{						
		usam_add_box( 'usam_settings', __('Ваша электронная почта','usam'), array( $this, 'box_settings' ) );	
		usam_add_box( 'usam_pop3_settings', __('Настройки POP3','usam'), array( $this, 'pop3_settings' ) );	
		usam_add_box( 'usam_smtp_settings', __('Настройки SMTP','usam'), array( $this, 'smtp_settings' ) );			
    }	
	
	function display_right()
	{	
		if ( $this->id )
			usam_add_box( 'usam_users_mailbox', __('Доступна пользователям','usam'), array( $this, 'select_users' ) );		
	}
}
?>