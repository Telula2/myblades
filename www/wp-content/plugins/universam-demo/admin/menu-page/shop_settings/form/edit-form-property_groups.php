<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_property_groups extends USAM_Edit_Form
{	
	public $alt;
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить группу свойств','usam');
		else
			$title = __('Добавить группу свойств', 'usam');	
		return $title;
	}	

	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$this->data = usam_get_order_props_group( $this->id );	
		}
		else
		{
			$this->data = array( 'name' => '','type_payer' => '','sort' => '' );
		}
	}	
		
    public function settings( )
	{			
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>									
				<tr>				
					<td class="name"><?php _e('Тип плательщика','usam'); ?>:</td>
					<td>
						<select class="chzn-select-nosearch-load" name = "type_payer" id = "order_field_type_payer">						
							<?php				
							$types_payers = usam_get_group_payers();
							foreach( $types_payers as $value )
							{						
								?>               
								<option value="<?php echo $value['id']; ?>" <?php echo ($this->data['type_payer'] == $value['id']) ?'selected="selected"':''?> ><?php echo $value['name']; ?></option>
								<?php
							}
							?>
						</select>	
					</td>
				</tr>				
				<tr>				
					<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
					<td>
						<input type="text" name="sort" value="<?php echo $this->data['sort']; ?>" autocomplete="off"/>
					</td>
				</tr>				
			</tbody>
		</table>
      <?php
	}   
	
	function display_left()
	{		
		$this->titlediv( $this->data['name'] );		
		usam_add_box( 'usam_property_order', __('Параметры','usam'), array( $this, 'settings' ) );				
    }	
}
?>