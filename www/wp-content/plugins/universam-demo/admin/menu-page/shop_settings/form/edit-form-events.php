<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_events extends USAM_Edit_Form
{	
    public function settings( $event_handling )
	{		
		$events = array(
			'change_order_status' => __('Изменен статус заказа', 'usam')
		);
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>			
				<tr>				
					<td><?php _e('Событие','usam'); ?>:</td>
					<td>
						<select name="event">						
							<?php		
							$type_prices = usam_get_prices( );						
							foreach ( $events as $key => $event )
							{					
								?>			
								<option value="<?php echo $key; ?>"<?php echo ($selected == $key?'selected="selected"':''); ?>><?php echo $event['title']; ?></option>		
								<?php
							}				
							?>							
						</select>
					</td>
				</tr>
				<tr>				
					<td><?php _e('Выполнить','usam'); ?>:</td>
					<td>
						<?php
						foreach ( $actions as $key => $action )
						{					
							?>			
							<input <?php checked( in_array($key, $event_handling['actions']) ); ?> type='checkbox' value='1' name = "action[]" id="action_<?php echo $key; ?>"><label for = "action_<?php echo $key; ?>"><?php echo $action['title']; ?></label>
							<?php
						}				
						?>
					</td>
				</tr>									
			</tbody>
		</table>
      <?php
	}      
	
	function display_left()
	{			
		if ( $this->id != null )
		{				
			$option = get_option('usam_event_handling', '');
			$event_handlinges = maybe_unserialize( $option );
			$event_handling = $event_handlinges[$this->id];
			$event_handling = array_merge ($this->default, $event_handling);			
		}
		else
			$event_handling = array();		
		
	
		usam_add_box( 'usam_event_handling', __('Параметры','usam'), array( $this, 'settings' ), $event_handling );		
    }	
}
?>