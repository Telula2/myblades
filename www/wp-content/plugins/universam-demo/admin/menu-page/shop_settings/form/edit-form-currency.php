<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_currency extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить валюту','usam');
		else
			$title = __('Добавить валюту', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{			
			$this->data = usam_get_currency( $this->id );
		}
		else	
		{
			$this->data = array( 'name' => '', 'symbol' => '', 'symbol_html' => '', 'code' => '', 'numerical' => '', 'display_currency' => 0 );
		}						
	}	
	
	public function settings( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>				
					<td><label for='currency_isocode'><?php _e( 'Буквенный код ISO', 'usam' ); ?>:</label></td>
					<td><input id="currency_isocode" type="text" name="currency[code]" value="<?php echo $this->data['code']; ?>" /></td>				
				</tr>							
				<tr>				
					<td><label for='currency_code'><?php _e( 'Числовой код ISO', 'usam' ); ?>:</label></td>
					<td><input id="currency_code" type="text" name="currency[numerical]" value="<?php echo $this->data['numerical']; ?>" /></td>					
				</tr>									
			</tbody>
		</table>
      <?php
	}      
	
	public function display_settings( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>									
				<tr>				
					<td><label for='currency_symbol'><?php _e( 'Символ', 'usam' ); ?>:</label></td>
					<td><input id="currency_symbol" type="text" name="currency[symbol]" value="<?php echo $this->data['symbol']; ?>" /></td>					
				</tr>		
				<tr>				
					<td><label for='currency_symbol_html'><?php _e( 'HTML код', 'usam' ); ?>:</label></td>
					<td><input id="currency_symbol_html" type="text" name="currency[symbol_html]" value="<?php echo $this->data['symbol_html']; ?>" /></td>				
				</tr>
				<tr>				
					<td><?php _e( 'Отображение', 'usam' ); ?>:</td>
					<td><label><input type="radio" <?php checked( $this->data['display_currency'], 1 ); ?> name="currency[display_currency]" value="1" /> <?php _e( 'Да', 'usam' ); ?></label>&nbsp;&nbsp;
					<label><input type="radio" <?php checked( $this->data['display_currency'], 0 ); ?>name="currency[display_currency]" value="0" /> <?php _e( 'Нет', 'usam' ); ?></label>
					</td>				
				</tr>					
			</tbody>
		</table>
      <?php
	}      
		
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );	
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'settings' ) );	
		usam_add_box( 'usam_display_settings', __('Отображение','usam'), array( $this, 'display_settings' ) );
    }	
}
?>