<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_order_status extends USAM_Edit_Form
{	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{					
			$this->data = usam_get_order_status( $this->id );	
		}
		else
			$this->data = array( 'internalname' => '', 'name' => '', 'description' => '', 'short_name' => '', 'sort' => 100, 'visibility' => 1, 'close' => 0, 'color' => '' );					
	}		

	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить статус &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить статус заказа', 'usam');	
		return $title;
	}	
		
    public function settings( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>					
				<tr>				
					<td class="name"><?php _e('Короткое название','usam'); ?>:</td>
					<td>
						<input type="text" name="short_name" value="<?php echo $this->data['short_name']; ?>" autocomplete="off" />
					</td>
				</tr>	
				<tr>				
					<td class="name"><?php _e('Код','usam'); ?>:</td>
					<td>
						<input type="text" name="internalname" value="<?php echo $this->data['internalname']; ?>" autocomplete="off" />
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
					<td>
						<input type="text" name="sort" value="<?php echo $this->data['sort']; ?>" autocomplete="off" />
					</td>
				</tr>	
				<tr>				
					<td class="name"><?php _e( 'Отображение', 'usam' ); ?>:</td>
					<td><label><input type="radio" <?php checked($this->data['visibility'], 1); ?> name="visibility" value="1" /> <?php _e( 'Да', 'usam' ); ?></label>&nbsp;&nbsp;
					<label><input type="radio" <?php checked($this->data['visibility'], 0); ?>name="visibility" value="0" /> <?php _e( 'Нет', 'usam' ); ?></label>
					</td>				
				</tr>	
				<tr>				
					<td class="name"><?php _e( 'Закрыт', 'usam' ); ?>:</td>
					<td><label><input type="radio" <?php checked($this->data['close'], 1); ?> name="close" value="1" /> <?php _e( 'Да', 'usam' ); ?></label>&nbsp;&nbsp;
					<label><input type="radio" <?php checked($this->data['close'], 0); ?>name="close" value="0" /> <?php _e( 'Нет', 'usam' ); ?></label>
					</td>				
				</tr>
				<tr>				
					<td class="name"><?php _e('Цвет','usam'); ?>:</td>
					<td>
						<input type="text" name="color" class="usam_color background_color" size="7" maxlength="7" value="<?php echo $this->data['color']; ?>"/>
					</td>
				</tr>					
			</tbody>
		</table>
      <?php
	}      
	
	function display_left()
	{					
		$this->titlediv( $this->data['name'] );	
		$this->add_box_description( $this->data['description'] );	
		usam_add_box( 'usam_settings_status', __('Параметры','usam'), array( $this, 'settings' ) );		
    }	
}
?>