<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_location extends USAM_Edit_Form
{		
	protected $step = 1;
	protected $action = 'start_import';
	
	protected $formtype = true;
	private $file_path;
	private $delimiter = ';';
	private $encoding = '';	
		
	private $tables;
	
	protected function get_title_tab()
	{ 
		$title = __('Импорт местоположений', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		$this->data = get_transient('usam_start_import_location');		
	}
	
	public function screen_1()
	{			
		$this->display_toolbar();
		?>	
		<table>					
			<tr>			
				<td><?php _e( 'Источник местоположений', 'usam' ) ?></td>
				<td>
					<input id="server"  type="radio" value="server" checked name="source" />
					<label for="server"><?php _e("Удалённый сервер", 'usam'); ?></label>
						<br>
					<input id="file" type="radio" value="file" name="source" />	
					<label for="file"><?php _e("Из файла", 'usam'); ?></label>
				</td>
			</tr>
			<tr>			
				<td><label class ="name" for='delete_all_existing'><?php esc_html_e( 'Удалить все существующие местоположения' , 'usam' ); ?>: </label></td>
				<td><input type='checkbox' id ="delete_all_existing" name="delete_existing" value="1"/></td>
			</tr>			
		</table>		
		<?php
		submit_button( __('Продолжить &raquo;','usam'), 'primary', 'next', false, array( 'id' => 'submit' ) ); 		
	}		
	
	public function screen_2()
	{				
		$this->display_toolbar();
		
		if ( $this->data['source'] == 'file' ) 
		{
			?>					
			<table>			
				<tr>			
					<td><?php _e( 'Укажите файл', 'usam' ) ?></td>
					<td><input type="file" name="file_location" value=""/></td>
				</tr>			
				<tr>			
					<td><label class ="name" for='file_delimiter'><?php esc_html_e( 'Разделитель' , 'usam' ); ?>: </label></td>
					<td><input id="file_delimiter" type="text" size='10' maxlength="1" name="delimiter" value="<?php echo $this->delimiter; ?>"/></td>
				</tr>
				<tr>			
					<td><label class="name" for='file_encoding'><?php esc_html_e( 'Кодировка файла' , 'usam' ); ?>: </label></td>
					<td><select name='encoding' id="file_encoding">						
							<option value='utf-8' <?php selected($this->encoding, 'utf-8') ?>>utf-8</option>
							<option value='windows-1251' <?php selected($this->encoding, 'windows-1251') ?>>windows-1251</option>
						</select>
					</td>
				</tr>
			</table>
			<?php			
		}
		else			
		{
			$locations = array( 'RU' => __('Россия', 'usam'), 'BY' => __('Беларусь', 'usam'), 'KZ' => __('Казахстан', 'usam'), 'UA' => __('Украина', 'usam'),  );
			?>	
			<table>					
				<?php			
				foreach ( $locations as $key => $name )
				{		
					?>	
					<tr>			
						<td><label class ="name" for='locations'><?php echo $name; ?>: </label></td>
						<td><input type='checkbox' id ="locations" name="locations" value="<?php echo $key; ?>"/></td>
					</tr>		
				<?php			
				}	
				?>								
			</table>			
			<?php		
		}	
		submit_button( __('Продолжить &raquo;','usam'), 'primary', 'next', false, array( 'id' => 'submit' ) ); 			
	}	
	
	protected function screen_3()
	{		
		$this->display_toolbar();
		$file_path = get_transient( 'usam_file_location_file');	
		require_once( USAM_FILE_PATH . '/includes/data_exchange/import.class.php'         );	

		$import = new USAM_Import();
		$tables = $import->import_file_path( $file_path );
		$i = 0;	
		?>
		<table>
		<?php			
		foreach ( $tables as $key => $row )
		{		
			?>		
			<thead>
			<tr>		
				<?php	
				foreach ( $row as $key => $value )
				{
					?>
					<td>
						<select name="row[<?php echo $key; ?>]">
							<option value=''><?php _e( 'Не использовать', 'usam' ) ?></option>
							<option value='id'><?php _e( 'Код', 'usam' ) ?></option>
							<option value='name'><?php _e( 'Название', 'usam' ) ?></option>			
							<option value='parent'><?php _e( 'Родитель', 'usam' ) ?></option>									
							<option value='type'><?php _e( 'Тип местоположения', 'usam' ) ?></option>
						</select>
					</td>	
					<?php
				}			
				?>
			</tr>
			<thead>
			<tbody>
			<tr>
				<?php				
				foreach ( $row as $value )
				{
					?>
					<td><?php echo $value ?></td>	
					<?php
				}
				?>				
			</tr>
			<tbody>
			<?php	
			break;			
		}					
		?>
		</table>				
		<input type="submit" name="update_location" class="button button-primary" value="<?php _e( 'Продолжить', 'usam' ) ?>"/>	
		<?php		
	}		
}
?>