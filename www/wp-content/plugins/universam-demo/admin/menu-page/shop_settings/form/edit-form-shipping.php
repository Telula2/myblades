<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Shipping extends USAM_Edit_Form
{	
	private $shipping = array();

	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить службу доставки &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить службу доставки', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{			
		$default = array( 'name' => '', 'description' => '', 'active' => 0, 'handler' => '', 'img' => 0, 'sort' => 10, 'period_from' => 0, 'period_to' => 0, 'period_type' => 'D', 'price' => 0, 'setting' => array( 'locations' => array(),'stores' => array(), 'margin' => 0, 'margin_type' => 'P', 'handler_setting' => array() ) );	
		if ( $this->id != null )			
			$this->data = usam_get_delivery( $this->id );		
		else	
			$this->data = array();
		
		$this->data = array_merge ($default, $this->data );		
	}

	function display_right()
	{		
		$title = __( 'Миниатюра для доставки', 'usam' );		
		$this->display_imagediv( $this->data['img'], $title );
	}	
	
	function display_left()
	{	// Загрузить обработчики 
		$shipping_directory = USAM_FILE_PATH . '/shipping';		
		$files = usam_list_dir( $shipping_directory );			
		foreach ( $files as $file )
		{
			if ( stristr( $file, '.php' ) ) 		
			{
				$file_path = $shipping_directory .'/'. $file;			
				$data = implode( '', file( $file_path ) );
				if ( preg_match( '|Shipping Name:(.*)$|mi', $data, $name ) ) 
				{					
					$parts = explode( '.', $file );
					$this->shipping[$parts[0]] = array( 'name' => $name[1] );
				}
			}	
		}			
		$this->titlediv( $this->data['name'] );	
		$this->add_box_description( $this->data['description'] );
		$this->add_box_status_active( $this->data['active'] );			
		usam_add_box( 'usam_shipping_module_general_settings', __( 'Общие настройки', 'usam' ), array( $this, 'shipping_module_general_settings' ) );	
		usam_add_box( 'usam_shipping_module_handler_settings', __( 'Настройки обработчика', 'usam' ), array( $this, 'shipping_module_handler_settings' ) );			
		usam_add_box( 'usam_delivery_service_restrictions', __( 'Ограничения службы доставки', 'usam' ), array( $this, 'delivery_service_restrictions' ) );	
	}
	
	function shipping_module_handler_settings()
	{	
		$merchant_instance = usam_get_shipping_class( $this->id );	
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>					
				<tr>				
					<td class = 'name'><?php esc_html_e( 'Обработчик', 'usam' ); ?>:</td>
					<td>
						<select name='handler'>		
						<option value='0'><?php esc_html_e( 'По умолчанию', 'usam' ); ?></option>
						<?php							
						$output = '';
						foreach ( $this->shipping as $code => $shipping ) 
						{								
							$selected = '';
							if( $this->data['handler'] == $code )
								$selected = "selected='selected'";							
							echo "<option $selected value='$code'>".$shipping['name']."</option>";	
						}	
						?>
						</select>					
					</td>
				</tr>					
				<?php 
				echo $merchant_instance->get_form( ); 
				
				$setting = array_merge( array( 'include_in_cost' => 0, 'margin' => 0, 'margin_type' => 'P' ), $this->data['setting'] );		
				?>	
				<tr>							
					<td class ="name"><?php _e( 'Включать в стоимость заказа', 'usam' ); ?>:</td>				
					<td class ="row_option">
						<input type="radio" name="shipping_module[setting][include_in_cost]" value="0" <?php checked($setting['include_in_cost'], 0) ?> /><?php _e('Да', 'usam'); ?>&nbsp;
						<input type="radio" name="shipping_module[setting][include_in_cost]" value="1" <?php checked($setting['include_in_cost'], 1) ?> /><?php _e('Нет', 'usam'); ?>
					</td>
				</tr>	
				<?php 
				if ( !empty($this->data['handler']) ) 
				{ 				
				?>					
				<tr>							
					<td class ="name"><?php _e( 'Наценка', 'usam' ); ?>:</td>				
					<td class ="row_option"><input type="text" name="shipping_module[setting][margin]" maxlength = "8" size = "8" value="<?php echo (float)$setting['margin']; ?>"/></td>
				</tr>
				<tr>							
					<td class ="name"><?php _e( 'Тип наценки', 'usam' ); ?>:</td>				
					<td class ="row_option">					
						<select name="shipping_module[setting][margin_type]">
							<option value="P" <?php selected( $setting['margin_type'],'P' ); ?>>%</option>
							<option value="T" <?php selected( $setting['margin_type'],'T' ); ?>><?php echo esc_html( usam_get_currency_sign() ) ?></option>						
						</select>		
					</td>
				</tr>
				<?php } ?>				
			</tbody>
		</table>	
		<?php			
    }	
	
	function delivery_service_restrictions()
	{			
		$restrictions = !empty($this->data['setting']['restrictions'])?$this->data['setting']['restrictions']:array();
		
		$restrictions['price_from'] = !empty($restrictions['price_from'])?$restrictions['price_from']:0;
		$restrictions['price_to'] = !empty($restrictions['price_to'])?$restrictions['price_to']:0;
		$restrictions['weight_from'] = !empty($restrictions['weight_from'])?$restrictions['weight_from']:0;
		$restrictions['weight_to'] = !empty($restrictions['weight_to'])?$restrictions['weight_to']:0;
		$restrictions['locations'] = !empty($restrictions['locations'])?$restrictions['locations']:0;
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>	
			<tr>							
				<td class ="name"><?php _e( 'Вес (г)', 'usam' ); ?>:</td>
				<td class ="row_option">
					<?php _e( 'от', 'usam' ); ?> <input type="text" name="weight_from" maxlength = "8" size = "8" style='width:100px' value="<?php echo $restrictions['weight_from']; ?>"/> <?php _e( 'до', 'usam' ); ?> <input type="text" name="weight_to" maxlength = "8" size = "8" style='width:100px' value="<?php echo $restrictions['weight_to']; ?>"/>
				</td>
			</tr>
			<tr>							
				<td class ="name"><?php _e( 'Стоимость заказа', 'usam' ); ?>:</td>
				<td class ="row_option">
					<?php _e( 'от', 'usam' ); ?> <input type="text" name="price_from" maxlength = "12" size = "12" style='width:100px' value="<?php echo $restrictions['price_from']; ?>"/> <?php _e( 'до', 'usam' ); ?> <input type="text" maxlength = "12" size = "12" name="price_to" style='width:100px' value="<?php echo $restrictions['price_to']; ?>"/>														
				</td>
			</tr>			
			</tbody>
		</table>	
		<?php
		
		$this->selecting_locations( $restrictions['locations'] );
	
	}
	
	function shipping_module_general_settings()
	{				
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>		
				<tr>							
					<td class ="name"><?php _e( 'Сортировка', 'usam' ); ?>:</td>
					<td class ="row_option"><input type="text" name="sort" maxlength = "3" size = "3" value="<?php echo $this->data['sort']; ?>" autocomplete="off"/></td>
				</tr>
				<tr>							
					<td class ="name"><?php _e( 'Выбрать склады самовывоза', 'usam' ); ?>:</td>
					<td class ="row_option">
						<?php
						$checked = !empty($this->data['setting']['stores'])?'checked="checked"':'';
						?>
						<input type="checkbox" name="stores_show" <?php echo !empty($this->data['setting']['stores'])?'checked="checked"':''; ?> value="1"/>
					</td>
				</tr>
				<tr>							
					<td class ="name"><?php _e( 'Склады самовывоза', 'usam' ); ?>:</td>
					<td class ="row_option">
						<select name="stores[]" multiple="">
						<?php $stores = usam_get_stores(); 								
						foreach ( $stores as $store )			
						{
							$selected = in_array($store->id, $this->data['setting']['stores'])?'selected="selected"':'';
							?>	
							<option value="<?php echo $store->id; ?>" <?php echo $selected; ?>><?php echo $store->title; ?></option>
							<?php
						}									
						?>	
						</select>									
					</td>
				</tr>	
			</tbody>
		</table>	
		<?php
    }
}
?>