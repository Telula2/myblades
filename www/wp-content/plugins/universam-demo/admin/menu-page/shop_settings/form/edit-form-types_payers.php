<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_types_payers extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить тип &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить тип', 'usam');	
		return $title;
	}
		
	protected function get_data_tab()
	{		
		if ( $this->id != null )					
			$this->data = usam_get_payer( $this->id );	
		else
			$this->data = array( 'name' => '', 'active' => 1, 'type' => 'I', 'sort' => 10 );
	}
	
	function display_setting()
	{ 
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>			
			<tr>
				<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
				<td>
					<input type="text" style="width:300px" name="sort" value="<?php echo $this->data['sort']; ?>" autocomplete="off"/>
				</td>
			</tr>
			<tr>
				<td class="name"><?php _e('Тип платильщика','usam'); ?>:</td>
				<td>
					<select name="type">
						<option value="I" <?php selected($this->data['type'], 'I') ?> ><?php _e('Физическое лицо','usam'); ?></option>
						<option value="E" <?php selected($this->data['type'], 'E') ?> ><?php _e('Юридическое лицо','usam'); ?></option>
					</select>	
				</td>				
			</tr>			
			</tbody>
		</table>	
		<?php
	}
	
	function display_left()
	{				
		$this->titlediv( $this->data['name'] );		
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_setting', __( 'Общие настройки', 'usam' ), array( $this, 'display_setting' ) );			
    }
}
?>