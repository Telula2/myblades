<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_payment_gateway extends USAM_Edit_Form
{		
	private $gateways = array();
			
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить платежную систему &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить платежную систему', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{			
		if ( $this->id != null )
		{
			$this->data = usam_get_payment_gateway( $this->id );			
		}
		else		
			$this->data = array( 'name' => '', 'description' => '', 'currency' => get_option('usam_currency_type'), 'active' => 0, 'img' => '', 'sort' => 10, 'debug' => 0, 'type' => 'c', 'bank_account_id' => '', 'handler' => '', 'setting' => array( 'ipn' => 0, 'condition' => array( 'roles' => array(),'shipping' => array(), 'sales_area' => array()), 'gateway_option' => array() ) );	
	}
	
	function display_right()
	{		
		$this->display_imagediv( $this->data['img'], __( 'Миниатюра для способа оплаты', 'usam' ) );
	}

	function display_left()
	{				
		
		// Загрузить обработчики платежных шлюзов			
		$gateway_directory = USAM_FILE_PATH . '/merchants';		
		$files = usam_list_dir( $gateway_directory );			
		foreach ( $files as $file )
		{
			if ( stristr( $file, '.php' ) ) 		
			{
				$file_path = $gateway_directory .'/'. $file;			
				$data = implode( '', file( $file_path ) );
				if ( preg_match( '|Merchant Name:(.*)$|mi', $data, $name ) ) 
				{					
					$parts = explode( '.', $file );
					$this->gateways[$parts[0]] = array( 'name' => $name[1] );
				}
			}	
		}		
		$this->titlediv( $this->data['name'] );	
		$this->add_box_description( $this->data['description'] );	
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_payment_general', __( 'Общие настройки', 'usam' ), array( $this, 'payment_general' ) );		
		usam_add_box( 'usam_payment_document', __( 'Настройки документа оплаты', 'usam' ), array( $this, 'payment_document_settings' ) );
		
		if ( $this->data['handler'] != '' )
			usam_add_box( 'usam_payment_handler_settings', __( 'Настройки обработчика', 'usam' ), array( $this, 'payment_handler_settings' ) );		
		
		usam_add_box( 'usam_payment_handler_restrictions', __( 'Ограничения способов оплаты', 'usam' ), array( $this, 'payment_handler_restrictions' ) );		
    }

	function payment_document_settings()
	{					
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>		
				<tr>
					<td class = 'name'><?php _e( 'Расчетный счет', 'usam' ) ?>:</td>
					<td><?php usam_select_bank_accounts( $this->data['bank_account_id'], array('name' => 'payment_gateway[bank_account_id]') ) ?></td>					
				</tr>
			</tbody>
		</table>	
		<?php
    }				
	
	function payment_general()
	{			
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>							
				<tr>
					<td class = 'name'><?php esc_html_e( 'Отладка', 'usam' ); ?>:</td>
					<td>
						<input type="radio" name="payment_gateway[debug]" value="1" <?php checked($this->data['debug'], 1) ?> />
						<?php _e('Да', 'usam'); ?><br />
						<input type="radio" name="payment_gateway[debug]" value="0" <?php checked($this->data['debug'], 0); ?>/><?php _e('Нет', 'usam'); ?><br />
						<small><?php esc_html_e( 'Включить режим отладку', 'usam' ); ?></small>
					</td>
				</tr>					
				<tr>
					<td class = 'name'><?php _e( 'Касса', 'usam' ) ?>:</td>
					<td>
						<select name='payment_gateway[cashbox]'>		
							<option value=''><?php esc_html_e( 'нет', 'usam' ); ?></option>
							<?php 
							$option = get_option('usam_cashbox', array() );						
							$array = maybe_unserialize($option);	
							if ( empty($array) )
								$array = array();
							
							foreach ( $array as $value ) 
							{										
								echo "<option ".selected($this->data['cashbox'], $value['id'], false)." value='".$value['id']."'>".$value['name']."</option>";	
							}	
							?>
						</select>	
					</td>					
				</tr>				
				<tr>				
					<td class = 'name'><?php esc_html_e( 'Обработчик', 'usam' ); ?>:</td>
					<td>
						<select name='payment_gateway[handler]'>		
							<option value=''><?php esc_html_e( 'Выберите обработчик', 'usam' ); ?></option>
							<?php	
							foreach ( $this->gateways as $code => $gateway ) 
							{										
								echo "<option ".selected($this->data['handler'], $code, false)." value='$code'>".$gateway['name']."</option>";	
							}	
							?>
						</select>					
					</td>
				</tr>				
			</tbody>
		</table>	
		<?php
    }	
	
	function payment_handler_settings()
	{	
		$merchant_instance = usam_get_merchant_class( $this->data['handler'] );		
		$user_account_url = $merchant_instance->get_user_account_url();
		if ( $user_account_url )
		{
			?>
			<a href="<?php echo $user_account_url; ?>" target="_blank"><?php _e('Перейти в кабинет пользователя', 'usam'); ?></a>
			<?php
		}
		?>			
		<table class="subtab-detail-content-table usam_edit_table">	
			<tbody>					
				<?php
				if ( $merchant_instance->get_ipn() )
				{
				?>
				<tr>
					<td class = 'name'><?php _e('Уведомления платежей', 'usam'); ?>:</td>
					<td>
						<input for='ipn1' type='radio' value='1' name='payment_gateway[setting][ipn]' <?php checked($this->data['setting']['ipn'], 1) ?> /> <label for='ipn1'><?php _e('Да', 'usam') ?></label> &nbsp;
						<input for='ipn2' type='radio' value='0' name='payment_gateway[setting][ipn]' <?php checked( empty($this->data['setting']['ipn']) ) ?> /> <label for='ipn2'><?php _e('Нет', 'usam') ?></label>
					</td>
					<td><?php _e( "Система будет автоматически обновлять заказы, когда оплата завершена успешно. Если не включен, статус заказов автоматически меняться не будет.", 'usam' ) ?></td>
				</tr>
				<tr>
					<td class = 'name'><?php _e('Ссылка для уведомлений', 'usam'); ?>:</td>
					<td colspan='2'><?php echo usam_get_url_system_page('transaction-results').'/notification/'.$this->id; ?></td>
				</tr>				
				<?php				
				}
				?>
				<?php echo $merchant_instance->get_form( $this->id ); ?>
			</tbody>
		</table>	
		<input type='hidden' value='<?php echo $merchant_instance->get_type_operation(); ?>' name='payment_gateway[type]' />				
		<?php			
    }	
	
	function payment_handler_restrictions()
	{		
		$shipping = !empty($this->data['setting']['condition']['shipping'])?$this->data['setting']['condition']['shipping']:array();
		$roles = !empty($this->data['setting']['condition']['roles'])?$this->data['setting']['condition']['roles']:array();
		$sales_area = !empty($this->data['setting']['condition']['sales_area'])?$this->data['setting']['condition']['sales_area']:array();
		?>
		<div class="container_column">			
			<div class="column1" id="rule-descrip">  
				<p><?php _e('Выберете условия использования метода оплаты. Если ничего не выбрано, значит работает как выбрано всё.', 'usam'); ?></p>
			</div>
			<div id="all_taxonomy" class="all_taxonomy">						
				<?php $this->display_meta_box_group( 'selected_shipping', $shipping ); ?>
				<?php $this->display_meta_box_group( 'roles', $roles ); ?>
				<?php $this->display_meta_box_group( 'sales_area', $sales_area ); ?>				
			</div>	
		</div>
		<?php
    }
}
?>