<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_location extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 
		if ( $this->id != null )
			$title = sprintf( __('Редактировать местоположение &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить местоположение', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{
			$this->data = usam_get_location( $this->id );				
		}
		else	
		{			
			$this->data = array( 'name' => '', 'parent' => 0, 'sort' => 100 );			
		}				
	}
	
	public function back_to_list()
	{
		?>
		<a href="<?php echo admin_url("options-general.php?page=shop_settings&tab=location"); ?>" class="back_to_list"><?php esc_html_e( 'Вернуться в список', 'usam' ); ?></a>	
		<?php
	}
	
	function display_left()
	{				
		$this->titlediv( $this->data['name'] );
		usam_add_box( 'usam_location', __('Настройки местоположения','usam'), array( $this, 'location_setting' ) );			
    }
	
	function location_setting()
	{								
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>																
			<tr>							
				<td class ="name"><?php _e( 'Тип местоположения', 'usam' ); ?>:</td>
				<td class ="row_option">									
					<select name="id_type">				
					<?php $type_location = usam_get_types_location(); 								
					foreach ( $type_location as $type )			
					{
						$selected = ( $this->data['id_type'] == $type->id )?'selected="selected"':'';
						?>	
						<option value="<?php echo $type->id; ?>" <?php echo $selected; ?>><?php echo $type->name; ?></option>
						<?php
					}									
					?>	
					</select>						
				</td>
			</tr>	
			<tr>							
				<td class ="name"><?php _e( 'Родитель', 'usam' ); ?>:</td>
				<td class ="row_option">
					<?php 
					$t = new USAM_Autocomplete_Forms();		
					$t->get_form_position_location( $this->data['parent'], array( 'type' => 'all' ) );
					?>				
				</td>
			</tr>
			<tr>							
				<td class ="name"><?php _e( 'Сортировка', 'usam' ); ?>:</td>
				<td class ="row_option">									
					<input type="text" name="sort" size = "100" maxlength = "100" value="<?php echo $this->data['sort']; ?>" autocomplete="off" />
				</td>
			</tr>	
			<tr>							
				<td class ="name"><?php _e( 'Полное имя', 'usam' ); ?>:</td>
				<td class ="row_option">									
					<?php
						if ( $this->id != null )
						{
							echo usam_get_full_locations_name($this->id);	
						}
					?>
				</td>
			</tr>
			</tbody>
		</table>
		<?php		
		
    }
}
?>