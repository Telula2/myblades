<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_property extends USAM_Edit_Form
{	
	public $alt;

	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить свойство &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить свойство заказа', 'usam');	
		return $title;
	}
	
	protected function get_data_tab()
	{		
		if ( $this->id != null )
		{				
			$property = new USAM_Order_Property( $this->id );	
			$this->data = $property->get_data();			
		}
		else
			$this->data = array( 'name' => '','type' => '','group' => '','mandatory' => '','active' => 0,'fast_buy' => '','unique_name' => '','profile' => '','options' => array(), 'sort' => 100 );	
	}	
		
    public function settings( )
	{	
		$groups = usam_get_order_props_groups();
		$user_field_types = array(
			        'text'     => __( 'Текст', 'usam' ),
			        'textarea' => __( 'Текстовая область', 'usam' ),			     
			        'select'   => __( 'Выбор', 'usam' ),
					'radio'    => __( 'Radio Button', 'usam' ),
			        'checkbox' => __( 'Checkbox', 'usam' ),
		);	
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>										
				<tr>				
					<td class="name"><?php _e('Группа','usam'); ?>:</td>
					<td>
						<select name="property_order[group]">				
						<?php 					
						foreach ( $groups as $group )			
						{
							$selected = ( $this->data['group'] == $group->id )?'selected="selected"':'';
							?>	
							<option value="<?php echo $group->id; ?>" <?php echo $selected; ?>><?php echo $group->name; ?></option>
							<?php
						}									
						?>	
						</select>
					</td>
				</tr>
				<tr>				
					<td class="name"><?php _e('Код','usam'); ?>:</td>
					<td>
						<input type="text" name="property_order[unique_name]" value="<?php echo $this->data['unique_name']; ?>"/>
					</td>
				</tr>					
				<tr>				
					<td class="name"><?php _e('Обязательное','usam'); ?>:</td>
					<td>
						<input <?php checked( $this->data['mandatory'], 1 ); ?> type="checkbox" name="property_order[mandatory]" value="1" />
					</td>
				</tr>
				<tr>				
					<td class="name"><?php _e('Входит в профиль','usam'); ?>:</td>
					<td>
						<input <?php checked( $this->data['profile'], 1 ); ?> type="checkbox" name="property_order[profile]" value="1" />
					</td>
				</tr>			
				<tr>				
					<td class="name"><?php _e('Быстрый заказ','usam'); ?>:</td>
					<td>
						<input <?php checked( $this->data['fast_buy'], 1 ); ?> type="checkbox" name="property_order[fast_buy]" value="1" />
					</td>
				</tr>
				<tr>				
					<td class="name"><?php _e('Сортировка','usam'); ?>:</td>
					<td>
						<input type="text" name="property_order[sort]" value="<?php echo $this->data['sort']; ?>" autocomplete="off" />
					</td>
				</tr>				
			</tbody>
		</table>
      <?php
	}   

    public function data_type( )
	{			
		$groups = usam_get_order_props_groups();
		$user_field_types = array(
			        'email'    => __( 'Электронная почта', 'usam' ),
					'mobile_phone'    => __( 'Мобильный телефон', 'usam' ),
					'phone'    => __( 'Телефон', 'usam' ),
					'email'    => __( 'Электронная почта', 'usam' ),
					'text'     => __( 'Текст', 'usam' ),
			        'textarea' => __( 'Текстовая область', 'usam' ),			     
			        'select'   => __( 'Выбор', 'usam' ),
					'radio'    => __( 'Radio Button', 'usam' ),
			        'checkbox' => __( 'Checkbox', 'usam' ),
					'location' => __( 'Местоположение', 'usam' ),
					'location_type' => __( 'Текстовое местоположение', 'usam' ),					
		);	
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>				
					<td><?php _e('Тип','usam'); ?>:</td>
					<td>
						<select id ="property_order_type" name="property_order[type]">				
						<?php 					
						foreach ( $user_field_types as $key => $name )			
						{
							$selected = ( $this->data['type'] == $key )?'selected="selected"':'';
							?>	
							<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $name; ?></option>
							<?php
						}									
						?>	
						</select>
					</td>
				</tr>	
				<tr id ="table_property_order_options">				
					<td><?php _e('Установки значений','usam'); ?>:</td>
					<td>
						<table class = "usam_table_property_order_options table_rate">
							<thead>
								<tr>
									<th><?php _e('Название', 'usam' ); ?></th>
									<th><?php _e( 'Значение', 'usam' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<tr class="js-warning">
									<td colspan="2">
										<small><?php echo sprintf( __( '<a href="%s">Включите JavaScript</a>, для использования всех возможностей.', 'usam'), 'http://www.google.com/support/bin/answer.py?answer=23852' ); ?></small>
									</td>
								</tr>
								<?php if ( ! empty( $this->data['options'] ) ): ?>
									<?php
										foreach( $this->data['options'] as $value ){
											$this->output_row( $value );
										}
									?>
								<?php else: ?>
									<?php $this->output_row( array('name' => '', 'value' => '' ) ); ?>
								<?php endif ?>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
      <?php
	}   	
	
	private function output_row( $value = '' ) 
	{		
		$class = ( $this->alt ) ? ' class="alternate"' : '';
		$this->alt = ! $this->alt;		
		?>
			<tr>
				<td <?php echo $class; ?> >
					<div class="cell-wrapper electing_storage_name">					
						<input type="text" name="options[name][]" value="<?php echo $value['name']; ?>"/>
					</div>
				</td>
				<td <?php echo $class; ?> >
					<div class="cell-wrapper">						
						<input type="text" name="options[value][]" value="<?php echo $value['value']; ?>"/>
						<div class="actions">
							<a tabindex="-1" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a>
							<a tabindex="-1" title="<?php _e( 'Удалить уровень', 'usam' ); ?>" class="action delete" href="#"><?php _e( 'Удалить', 'usam' ); ?></a>
						</div>
					</div>
				</td>
			</tr>
		<?php
	} 
		
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );		
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_property_order', __('Параметры','usam'), array( $this, 'settings' ) );		
		usam_add_box( 'usam_data_type', __('Тип данных','usam'), array( $this, 'data_type' ) );			
    }	
}
?>