<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_location_type extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить тип местоположений','usam');
		else
			$title = __('Добавить тип местоположений', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{
			$this->data = usam_get_type_location( $this->id );						
		}
		else	
		{			
			$this->data = array( 'name' => '', 'code' => '', 'sort' => 100 );				
		}
	}
	
	function setting_display()
	{			
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>		
			<tr>							
				<td class ="name"><?php _e( 'Код', 'usam' ); ?>:</td>
				<td class ="row_option">									
					<input type="text" name="code" size = "100" maxlength = "100" value="<?php echo $this->data['code']; ?>" autocomplete="off" />
				</td>
			</tr>
			<tr>							
				<td class ="name"><?php _e( 'Сортировка', 'usam' ); ?>:</td>
				<td class ="row_option">									
					<input type="text" name="sort" size = "100" maxlength = "100" value="<?php echo $this->data['sort']; ?>" autocomplete="off" />
				</td>
			</tr>			
			</tbody>
		</table>	
		<?php
    }
	
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );
		usam_add_box( 'usam_location_type', __('Настройка типа местоположения','usam'), array( $this, 'setting_display' ) );			
    }	
}
?>