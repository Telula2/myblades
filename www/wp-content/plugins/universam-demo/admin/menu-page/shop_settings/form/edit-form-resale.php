<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_resale extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить сайт &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить сайт', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_settings_sites_suppliers');
		}
		else
		{
			$this->data = array( 'name' => '', 'domain' => '', 'store' => '', 'type_price' => '', 'currency_code' => '', 'setting' => array( 'sku' => '', 'price' => '', 'title' => '', 'content' => '', 'not_available' => '', ) );
		} 				
	}
	
	public function settings( )
	{		
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>			
				<tr>				
					<td><label for='link'><?php _e( 'Ссылка на сайт', 'usam' ); ?>:</label></td>
					<td><input id="link" type="text" name="link" value="<?php echo $this->data['domain']; ?>" /></td>
					<td><?php if ( empty($this->data['domain']) ) _e( 'Скопируйте ссылку подключаемого сайта', 'usam' ); ?></td>
				</tr>	
				<tr>				
					<td><label for='id_resale'><?php _e( 'Тип валюты на сайте поставщика', 'usam' ); ?>:</label></td>
					<td><?php usam_select_currencies( $this->data['currency_code'], array( "name" => "currency_code" ) ); ?></td>
					<td><?php _e( 'У кажите валюту, в которой указаны цены на сайте поставщика', 'usam' ); ?></td>
				</tr>
				<tr>				
					<td><label for='id_resale'><?php _e( 'Склад', 'usam' ); ?>:</label></td>
					<td>
						<select name='store'>
							<?php							
							$stores = usam_get_stores( array( 'fields' => 'id, title' ) );						
							foreach ( $stores as $store ) 
							{
								?>
								<option value='<?php echo $store->id; ?>' <?php selected( $store->id, $this->data['store'] ); ?>><?php esc_html_e( $store->title ); ?></option>
								<?php
							}											
							?>
						</select>
					</td>
					<td><?php _e( 'У кажите валюту, в которой указаны цены на сайте поставщика', 'usam' ); ?></td>
				</tr>
				<tr>				
					<td><label for='type_price'><?php _e('Цены','usam') ?> </label></td>
					<td><?php usam_get_select_prices( $this->data['type_price'] ); ?></td>
					<td><?php _e( 'У кажите цену Вашего сайта, в которую будет загружаться цена с сайта поставщика', 'usam' ); ?></td>
				</tr>											
			</tbody>
		</table>
      <?php
	}    

	function form_tags(  )
	{		
		$columns = array(			
				'title'      => __( 'Название', 'usam' ),
				'tag'        => __( 'Тег', 'usam' ),		
		);		
		$block = array( 'sku' => __('Артикул','usam'), 'not_available' => __('Не доступность товара','usam'), 'price' => __('Цена','usam'), 'title' => __('Название товара','usam'), 'content' => __('Описание товара','usam') );	
		register_column_headers( 'site-list', $columns );			
		?>		
		<table id="usam_site_setting" class="widefat page fixed"  cellspacing="0">
			<thead>
				<tr>
					<?php print_column_headers( 'site-list' ); ?>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<?php print_column_headers( 'site-list', false ); ?>
				</tr>
			</tfoot>
			<tbody>
			<?php
			foreach ( $block as $key => $value )
			{
				?>
				<tr data-field-id="<?php echo $key; ?>" id="site_id_<?php echo $id; ?>">			
					<td class="namecol"><?php echo $value; ?></td>				
					<td class="typecol"><input id="tag_<?php echo $key; ?>" type="text" name="site_setting_tag[<?php echo $key; ?>]" value="<?php echo $this->data['setting'][$key]; ?>" style="width:100%" /></td>
				</tr>
				<?php 
			}
			?>
			</tbody>
		</table>
		<?php 	
	}
		
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );		
		usam_add_box( 'usam_tags', __('Основные теги','usam'), array( $this, 'form_tags' ) );
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'settings' ) );	
    }	
}
?>