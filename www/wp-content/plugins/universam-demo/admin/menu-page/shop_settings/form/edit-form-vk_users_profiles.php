<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_vk_users_profiles extends USAM_Edit_Form
{	
	private $vk_api;	
	
	protected function get_title_tab()
	{ 	
		$this->vk_api = get_option('usam_vk_api', array('api_id' => ''));		
		if ( $this->id != null )
			$title = sprintf( __('Изменить анкету &laquo;%s&raquo;','usam'), $this->data['first_name'].' '.$this->data['last_name'] );
		else
			$title = __('Добавить анкету', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{					
		if ( $this->id != null )
		{				
			$this->data = usam_get_data( $this->id, 'usam_vk_profile' );
		}
		else
		{					
			$this->data = array( 'id' => '', 'page_id' => '', 'first_name' => '', 'last_name' => '', 'photo_50' => '', 'access_token' => '', 'friends_add' => 0 );
		}
	}	
	
    public function settings( )
	{  				
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>
					<td><?php _e('ID анкеты','usam'); ?>:</td>
					<td>
						<input type="text" name="profile_id" value="<?php echo $this->data['page_id']; ?>" size="60" />
					</td>					
				</tr>	
				<tr>
					<td><?php _e('Access Token','usam'); ?>:</td>
					<td>
						<input type="text" name="access_token" value="<?php echo $this->data['access_token']; ?>" size="60" />
						<?php $url = 'http://oauth.vk.com/authorize?client_id='.$this->vk_api['api_id'].'&scope=wall,photos,ads,offline,friends,notifications,market&display=page&response_type=token&redirect_uri=http://api.vk.com/blank.html'; ?>
						<p><?php _e('Чтобы получить Access Token','usam'); ?></p>
						<ol>
							<li><?php printf( __('перейдите по <a href="%s" target="_blank">ссылке</a>','usam'),$url); ?>,</li>
							<li><?php _e('подтвердите уровень доступа','usam'); ?>,</li>
							<li><?php _e('скопируйте access_token с открывшейся страницы','usam'); ?>.</li>
						</ol>
					</td>					
				</tr>						
			</tbody>
		</table>
      <?php
	}       
	
	function display_left()
	{			
		if ( !empty($this->vk_api['api_id']) )
		{
			?> 
			<div class="user_name-wrapper">
				<div class="user_name">		
					<img src="<?php echo $this->data['photo_50']; ?>"><h1><?php echo $this->data['first_name'].' '.$this->data['last_name']; ?></h1>
				</div>	
			</div>			
			<?php
			usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'settings' ) );	
		}
		else
		{
			_e('Не настроенно API','usam');
		}
    }
}
?>