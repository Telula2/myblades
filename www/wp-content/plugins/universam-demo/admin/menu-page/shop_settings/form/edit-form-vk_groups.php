<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_vk_groups extends USAM_Edit_Form
{	
	private $vk_api;	
	protected function get_data_tab(  )
	{					
		$this->vk_api = get_option('usam_vk_api', array('api_id' => ''));		
		if ( $this->id != null )
		{				
			$this->data = usam_get_data( $this->id, 'usam_vk_groups' );
		}
		else
		{					
			$this->data = array('name' => '', 'photo' => '', 'page_id' => '', 'access_token' => '', 'birthday' => 0, 'publish_reviews' => 1);
		}
	}	
	
    public function settings( )
	{  				
		?>	
		<table class="subtab-detail-content-table usam_edit_table" >
			<tbody>					
				<tr>				
					<td class="name"><label for='group_page_id'><?php esc_html_e( 'ID вашей группы', 'usam' ); ?>:</label></td>
					<td><input value = "<?php echo $this->data['page_id'] ?>" type='text' name='group_page_id' id = "group_page_id"/></td>				
				</tr>	
				<tr>
					<td class="name"><label for='vk_access_token'><?php _e('Access Token','usam'); ?>:</label></td>
					<td><input type="text" id="vk_access_token" name="group_access_token" value="<?php echo $this->data['access_token']; ?>" size="60" /></td>
				</tr>
				<tr>
					<td></td>
					<td>			
						<?php $url = 'http://oauth.vk.com/authorize?client_id='.$this->vk_api['api_id'].'&scope=wall,photos,ads,offline,friends,notifications,market&display=page&response_type=token&redirect_uri=http://api.vk.com/blank.html'; ?>
						<p><?php _e('Чтобы получить Access Token','usam'); ?></p>
						<ol>
							<li><?php printf( __('перейдите по <a href="%s" target="_blank">ссылке</a>','usam'),$url); ?>,</li>
							<li><?php _e('подтвердите уровень доступа','usam'); ?>,</li>
							<li><?php _e('скопируйте access_token с открывшейся страницы','usam'); ?>.</li>
						</ol>
					</td>	
				</tr>					
				<tr>				
					<td class="name"><label for='group_autopublish0'><?php esc_html_e( 'Поздравлять с ДР', 'usam' ); ?>:</label></td>
					<td>
						<input type="radio" value="1" name="group_birthday" id="group_birthday0" <?php checked($this->data['birthday'], 1 ) ?>>  
						<label for="group_autopublish0"><?php esc_html_e( 'Да', 'usam' ); ?></label> &nbsp;						
						<input type="radio" value="0" name="group_birthday" id="group_birthday1" <?php checked($this->data['birthday'], 0 ) ?>>
						<label for="group_autopublish1"><?php esc_html_e( 'Нет', 'usam' ); ?></label> &nbsp;						
					</td>	
					<td class="description"><?php esc_html_e( 'Публиковать поздравления с днем рождения участников группы.', 'usam' ); ?></td>				
				</tr>
				<tr>				
					<td class="name"><label for='publish_reviews0'><?php esc_html_e( 'Публиковать отзывы', 'usam' ); ?>:</label></td>
					<td>
						<input type="radio" value="1" name="publish_reviews" id="publish_reviews0" <?php checked($this->data['publish_reviews'], 1 ) ?>>  
						<label for="group_autopublish0"><?php esc_html_e( 'Да', 'usam' ); ?></label> &nbsp;						
						<input type="radio" value="0" name="publish_reviews" id="publish_reviews1" <?php checked($this->data['publish_reviews'], 0 ) ?>>
						<label for="group_autopublish1"><?php esc_html_e( 'Нет', 'usam' ); ?></label> &nbsp;						
					</td>	
					<td class="description"><?php esc_html_e( 'Публиковать отзывы вКонтакте, когда они будут опубликованы на вашем сайте.', 'usam' ); ?></td>				
				</tr>						
			</tbody>
		</table>
      <?php
	}       
	
	function display_left()
	{				
		if ( !empty($this->vk_api['api_id']) )
		{		
			if ( $this->data['name'] )
			{
				?> 
				<div class="user_name-wrapper">
					<div class="user_name">		
						<img src="<?php echo $this->data['photo']; ?>"><h1><?php echo $this->data['name']; ?></h1>
					</div>	
				</div>			
				<?php
			}
			usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'settings' ) );
		}		
		else
		{
			_e('Не настроено API','usam');
		}
    }
}
?>