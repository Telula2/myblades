<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_cashbox extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить кассу','usam');
		else
			$title = __('Добавить кассу', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{		
		if ( $this->id != null )							
			$this->data = usam_get_cashbox( $this->id );
		else
			$this->data = array( 'name' => '', 'handler' => '', 'active' => 0, 'number' => '', 'locations' => array(), 'roles' => array() );				
	}	
		
	function display_left()
	{			
		$this->titlediv( $this->data['name'] );	
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_payment_general', __( 'Общие настройки', 'usam' ), array( $this, 'show_settings' ) );				
		
		if ( $this->data['handler'] != '' )
			usam_add_box( 'usam_handler_settings', __( 'Настройки обработчика', 'usam' ), array( $this, 'handler_settings' ) );				
    }
	
	function show_settings()
	{			
		$gateway_directory = USAM_FILE_PATH . '/cashbox';		
		$files = usam_list_dir( $gateway_directory );	
		$cashbox = array();
		foreach ( $files as $file )
		{
			if ( stristr( $file, '.php' ) ) 		
			{
				$file_path = $gateway_directory .'/'. $file;			
				$data = implode( '', file( $file_path ) );
				if ( preg_match( '|Name:(.*)$|mi', $data, $name ) ) 
				{					
					$parts = explode( '.', $file );
					$cashbox[$parts[0]] = array( 'name' => $name[1] );
				}
			}	
		}	
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>				
				<tr>
					<td class = 'name'><?php _e('Заводской номер','usam'); ?>:</td>
					<td><input type="text" name="number" value="<?php echo $this->data['number']; ?>" size="45" /></td>
				</tr>	
				<tr>				
					<td class = 'name'><?php esc_html_e( 'Обработчик', 'usam' ); ?>:</td>
					<td>
						<select name='handler'>		
						<option value=''><?php esc_html_e( 'Выберите обработчик', 'usam' ); ?></option>
						<?php							
						$output = '';
						foreach ( $cashbox as $code => $gateway ) 
						{								
							$selected = '';
							if( $this->data['handler'] == $code )
								$selected = "selected='selected'";	
							
							echo "<option $selected value='$code'>".$gateway['name']."</option>";	
						}	
						?>
						</select>					
					</td>
				</tr>							
			</tbody>
		</table>	
		<?php
    }	
	
	function handler_settings()
	{			
		$instance = usam_get_cashbox_class( $this->id );
		if ( is_object($instance) )
			echo $instance->get_form( );
    }		
}
?>