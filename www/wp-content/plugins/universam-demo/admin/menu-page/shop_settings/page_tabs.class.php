<?php
/*
 * Настройки магазина 
 */ 
$default_tabs = array(
	array( 'id' => 'general',  'title' => __( 'Главные', 'usam' ) ),
	array( 'id' => 'presentation',  'title' => __( 'Внешний вид', 'usam' ),
					'level' => array( 
						array( 'id' => 'product_view',  'title' => __( 'Просмотр товара', 'usam' ) ),					
					//	array( 'id' => 'your_account',  'title' => __( 'Кабинет пользователя', 'usam' ) ),							
						array( 'id' => 'transaction_results',  'title' => __( 'Результаты транзакции', 'usam' ) ),		
						array( 'id' => 'search',  'title' => __( 'Поиск', 'usam' ) ),						
						array( 'id' => 'theme_file',  'title' => __( 'Файлы темы', 'usam' ) ),						
				   ),		
	),	
	array( 'id' => 'directories',  'title' => __( 'Справочники', 'usam' ),
		'level' => array( 		
			array( 'id' => 'cashbox',   'title' => __( 'Кассы', 'usam' ) ),	
			array( 'id' => 'crosssell',  'title' => __( 'Перекрёстные продажи', 'usam' ), ),	
			array( 'id' => 'prices',  'title' => __( 'Цены', 'usam' ) ),
			array( 'id' => 'location',  'title' => __( 'Местоположения', 'usam' ), ),
			array( 'id' => 'sales_area',  'title' => __( 'Зоны продаж', 'usam' ), ),
			array( 'id' => 'location_type',  'title' => __( 'Типы местоположений', 'usam' ) ),		
			array( 'id' => 'resale',  'title' => __( 'Перепродажа', 'usam' ) ),			
			array( 'id' => 'currency',  'title' => __( 'Валюты', 'usam' ) ),				
		 ),	
	),
	array( 'id' => 'CRM',  'title' => __( 'CRM', 'usam' ),	
		'level' => array( 
			array( 'id' => 'company_group',  'title' => __( 'Группа компаний', 'usam' ) ),	
			//array( 'id' => 'admin',  'title' => __( 'Админ', 'usam' ) ),	
		),
	),		
	array( 'id' => 'feedback',  'title' => __( 'Обратная связь', 'usam' ),	
					'level' => array( 					
						array( 'id' => 'mailing',  'title' => __( 'Рассылка', 'usam' ) ),
						array( 'id' => 'reviews',  'title' => __( 'Отзывы', 'usam' ) ),						
				   ),	
	),
	array( 'id' => 'messages',  'title' => __( 'Сообщения', 'usam' ),
				'level' => array( 					
						array( 'id' => 'notification',  'title' => __( 'Уведомления', 'usam' ) ),
						array( 'id' => 'mailboxes',  'title' => __( 'Почтовые ящики', 'usam' ) ),						
						array( 'id' => 'contactform',  'title' => __( 'Контактная форма', 'usam' ) ),					
						array( 'id' => 'template_messages',  'title' => __( 'Шаблоны сообщений', 'usam' ),
							'level' => array( 					
								array( 'id' => 'message_order_statuses',  'title' => __( 'Статус заказа', 'usam' ) ),													
								array( 'id' => 'message_coupon',  'title' => __( 'Сообщение о купоне', 'usam' ) ),
								array( 'id' => 'message_trackingid',  'title' => __( 'Трек-номер', 'usam' ) ),
								array( 'id' => 'message_discount_favorites',  'title' => __( 'Скидка на избранное', 'usam' ) ),
								array( 'id' => 'message_willingness_order',  'title' => __( 'Готовность заказа', 'usam' ) ),								
								array( 'id' => 'message_payment_invoice', 'title' => __( 'Счета на оплату', 'usam' ) ),		
							),
						),			
				   ),	
	),	
	array( 'id' => 'purchase',  'title' => __( 'Заказ', 'usam' ),
			'level' => array( 							
				array( 'id' => 'purchase_rules',  'title' => __( 'Правила покупки', 'usam' ) ),			
				array( 'id' => 'types_payers',  'title' => __( 'Типы плательщиков', 'usam' ) ),		
				array( 'id' => 'property_groups',  'title' => __( 'Группы свойств', 'usam' ) ),		
				array( 'id' => 'property',  'title' => __( 'Список свойств', 'usam' ) ),			
				array( 'id' => 'checkout_conditions',  'title' => __( 'Сроки и условия', 'usam' ) ),				
				array( 'id' => 'shipping',  'title' => __( 'Доставка', 'usam' ) ),
				array( 'id' => 'payment_gateway',  'title' => __( 'Оплата', 'usam' ) ),	
				array( 'id' => 'order_status',    'title' => __( 'Статусы заказа', 'usam' ) ),		
				array( 'id' => 'view_grouping',   'title' => __( 'Группировка просмотра', 'usam' ) ),						
			 ),
	),	
	array( 'id' => 'social_networks',  'title' => __( 'Социальные сети', 'usam' ), 'level' => 
		array( 
			array( 'id' => 'vk',  'title' => __( 'вКонтакте', 'usam' ), 'level' => 
				array( 	
					array( 'id' => 'vk_users_profiles',  'title' => __( 'Анкеты', 'usam' ) ),	
					array( 'id' => 'vk_groups',  'title' => __( 'Группы', 'usam' ) ),
					array( 'id' => 'vk_publishing_rules',  'title' => __( 'Правила публикации', 'usam' ) ),					
				),
			),
			array( 'id' => 'instagram',  'title' => 'Instagram' ),		
		),	
	),
//	array( 'id' => 'ai',  'title' => __( 'ИИ', 'usam' ) ),
//	array( 'id' => 'events',  'title' => __( 'События', 'usam' ) ),	
	array( 'id' => 'blanks',  'title' => __( 'Бланки', 'usam' ), ),		
	array( 'id' => 'seo',  'title' => __( 'SEO', 'usam' ),
		'level' => array( 
			array( 'id' => 'search_engines',  'title' => __( 'Поисковые системы', 'usam' ) ),	
			array( 'id' => 'search_engine_location',  'title' => __( 'Местоположение поисковиков', 'usam' ) ),			
			array( 'id' => 'search_engine_region',  'title' => __( 'Регионы поисковиков', 'usam' ) ),					
		),	
	),		
);


class USAM_Tab extends USAM_Page_Tab
{			
	protected  $display_save_button = true;
	protected  $name_field = 'usam_options';
		
	protected function get_default_radio() 
	{
		return  array('0' => __( 'Нет', 'usam' ), '1' => __( 'Да', 'usam' ) );
	}
	
	protected function display_table_row_option( $options ) 
	{
		?>		
		<table class='usam_setting_table usam_edit_table'>
			<?php $this->row_option( $options ); ?>	
		</table>
		<?php		
	}
	
	protected function row_option( $options ) 
	{		
		foreach ( $options as $key => $option )	
		{	
			$option_bd = '';
			if ( empty($option['description']) )
				$option['description'] = '';
			
			if ( empty($option['attribute']) )
				$option['attribute'] = array();
			
			if ( empty($option['default']) )
				$option['default'] = '';
						
			if ( isset($option['option']) )
			{
				$option_key = 'usam_'.$option['option'];	
				$name = "{$this->name_field}[{$option_key}]";				
				if ( !isset($option['attribute']['value']) )
				{
					$option_bd = get_option( $option_key, $option['default'] );					
					if ( !empty($option['key']) )
					{						
						$option_bd = isset($option_bd[$option['key']])?$option_bd[$option['key']]:'';						
						$name = "{$this->name_field}[{$option_key}][".$option['key']."]";
					}					
				}
				else
					$option_bd = $option['attribute']['value'];
			}		
			else
				$name = '';
			?>
			<tr id = "<?php echo !empty($option['option'])?$option['option']:'usam_row_'.$key; ?>">
			<?php
				switch ( $option['type'] )
				{
					case 'checkbox' :
						$default_attribute = array( 'id' => $option_key, 'name' => $name );	
						$option['attribute'] = array_merge( $default_attribute, $option['attribute'] );	
						$option['value'] = $option_bd;	
						
						$this->row_option_checkbox( $option );	
					break;
					case 'radio' :
						if ( empty($option['radio']) )
							$option['radio'] = $this->get_default_radio();					
					
						$default_attribute = array( 'id' => $option_key, 'value' => $option_bd, 'name' => $name );	
						$option['attribute'] = array_merge( $default_attribute, $option['attribute'] );		
						
						$this->row_option_radio( $option );	
					break;
					case 'input' :						
						$default_attribute = array( 'id' => $option_key, 'value' => $option_bd, 'name' => $name );	
						$option['attribute'] = array_merge( $default_attribute, $option['attribute'] );						
			
						$this->row_option_input( $option );	
					break;
					case 'select' : 
						$default_attribute = array( 'id' => $option_key, 'name' => $name );	
						$option['attribute'] = array_merge( $default_attribute, $option['attribute'] );	
						$option['value'] = $option_bd;					
							
						$this->row_option_select( $option );	
					break;
					case 'text' :
						$this->row_option_text( $option );	
					break;
					case 'textarea' :
						$default_attribute = array('id' => $option_key, 'name' => $name, 'style' => 'width:100%; height:200px;');	
						$option['attribute'] = array_merge( $default_attribute, $option['attribute'] );							
						$option['value'] = $option_bd;
						
						$this->row_option_textarea( $option );	
					break;
				}	
			?>
			</tr>
			<?php
		}
	}
	
	protected function row_option_textarea( $option ) 
	{			
		$attr = '';		
		foreach ( $option['attribute'] as $attribute_name => $attribute )		
			$attr .= " $attribute_name = '$attribute'";
			
		$option['value'] = str_replace('\n', chr(10), $option['value'] );
		?>
		<td class="name"><label for ="<?php echo $option['attribute']['id']; ?>"><?php echo $option['title']; ?>:</label></td>
		<td><textarea <?php echo $attr; ?>><?php echo esc_textarea( $option['value'] ); ?></textarea></td>
		<td class = "description" ><?php echo $option['description']; ?></td>
		<?php
	}
	
	protected function row_option_text( $option ) 
	{		
		?>
		<td class="name"><label><?php echo $option['title']; ?>:</label></td>
		<td><?php echo $option['html']; ?></td>
		<td class = "description" ><?php echo $option['description']; ?></td>
		<?php
	}
	
	protected function row_option_input( $option ) 
	{	
		?>	
		<td class="name"><label for ="<?php echo $option['attribute']['id']; ?>"><?php echo $option['title']; ?>:</label></td>
		<td>
			<?php 
			$attr = '';
			foreach ( $option['attribute'] as $attribute_name => $attribute )		
				$attr .= " $attribute_name = '$attribute'";					
			?>				
			<input <?php echo $attr; ?> type='text'/>
		</td>
		<td class = "description" ><?php echo $option['description']; ?></td>		
		<?php
	}
	
	protected function row_option_checkbox( $option ) 
	{				
		?>	
		<td class="name"><label><?php echo $option['title']; ?>:</label></td>
		<td>			
			<input type='hidden' value='0' name='<?php echo $option['attribute']['name']; ?>' />
			<input <?php checked($option['value'], 1); ?> type="checkbox" value="1"  name='<?php echo $option['attribute']['name']; ?>' id='<?php echo $option['attribute']['id']; ?>' />
		</td>
		<td class = "description" ><?php echo $option['description']; ?></td>		
		<?php
	}
	
	protected function row_option_radio( $option ) 
	{		
		?>
		<td class="name"><?php echo $option['title']; ?>:</td>
		<td>
		<?php										
			foreach( $option['radio'] as $value => $title )
			{				
				?>
				<input type='radio' value='<?php echo $value; ?>' name='<?php echo $option['attribute']['name']; ?>' id='<?php echo $option['attribute']['id'].'-'.$value; ?>' <?php checked($option['attribute']['value'], $value); ?> />  <label for='<?php echo $option['attribute']['id'].'-'.$value; ?>'><?php echo $title; ?></label> &nbsp;						
				<?php 
			} ?>
		</td>
		<td class = "description"><?php echo $option['description']; ?></td>
		<?php
	}
	
	protected function row_option_select( $option ) 
	{
		$attr = '';		
		foreach ( $option['attribute'] as $attribute_name => $attribute )		
			$attr .= " $attribute_name = '$attribute'";
		?>	
		<td class="name"><?php echo $option['title']; ?>:</td>
		<td>
			<select <?php echo $attr; ?>>
				<?php				
				foreach ( $option['options'] as $key => $value ) 
				{
					?>
					<option value='<?php echo $key; ?>' <?php selected( $key, $option['value'] ); ?>><?php echo $value; ?></option>
					<?php
				}															
				?>
			</select>
		</td>
		<td class = "description"><?php echo $option['description']; ?></td>		
		<?php
	}
	
	protected function callback_submit()
	{ 
		if ( isset( $_POST[$this->name_field] ) ) 
		{
			$options = stripslashes_deep( $_POST[$this->name_field] );	
			$updated = 0;			
			foreach ( $options as $key => $value ) 
			{ 
				$result = update_option( $key, $value );
				if ( $result )
					$updated++;					
			} 
			$this->sendback = add_query_arg( array( 'update' => 1 ), $this->sendback );			
			$this->redirect = true;			
		}	
	}
		
	protected function enqueue_scripts()
	{					
		wp_enqueue_script( 'usam-selecting_locations' );	
	}	
		
	protected function localize_script_tab()
	{		
		return array(			
			'navigate_settings_tab_nonce'         => usam_create_ajax_nonce( 'navigate_settings_tab' ),						
			'save_blank_nonce'                    => usam_create_ajax_nonce( 'save_blank' ),
			'test_mailbox_nonce'                  => usam_create_ajax_nonce( 'test_mailbox' ),	
			'delete_mailbox_user_nonce'           => usam_create_ajax_nonce( 'delete_mailbox_user' ),
			'add_mailbox_user_nonce'              => usam_create_ajax_nonce( 'add_mailbox_user' ),				
			'text_or'                             => __('ИЛИ', 'usam'),
			'text_and'                            => __('И', 'usam'),
		);
	}	
	
	protected function subject( $option ) 
	{		
		$option_key = 'usam_'.$option;
		?>  
		<tr class = "subject">
			<td class = "name"><strong><?php esc_html_e( 'Тема письма', 'usam' );?>:</strong></td>
			<td class = "td_subject_input"><input class = "width100 subject" name="<?php echo $this->name_field; ?>[<?php echo $option_key; ?>]" type='text' value='<?php echo esc_attr(get_option($option_key ) );?>' /></td>
		</tr>
		<?php    
	}	
	
	protected function message( $option ) 
	{		
		$option_key = 'usam_'.$option;
		?>  
		<tr>
			<td colspan="2">  
				<?php                  
				wp_editor(stripslashes(str_replace('\\&quot;','',get_option($option_key))),$option_key,array(
					'textarea_name' => $this->name_field.'['.$option_key.']',
					'media_buttons' => false,
					'textarea_rows' => 10,
					'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
					)	
				);
				?>     
			</td>	
		</tr>
		<?php    
	}
	
	protected function sms_message( $option ) 
	{		
		$option_key = 'usam_'.$option;
		$option_bd = get_option($option_key);
		?>  
		<tr>
			<td class = "name"><strong><?php esc_html_e( 'СМС сообщение', 'usam' );?>:</strong></td>
			<td>
				<div class="sms_message_wrapper">
					<textarea id="sms"  class="sms_message" rows="8" name="<?php echo $this->name_field; ?>[<?php echo $option_key; ?>]"><?php echo esc_textarea( $option_bd ); ?></textarea>
					<div id="characters" class="toolbar"><?php echo mb_strlen($option_bd); ?></div>
				</div>
			</td>
		</tr>
		<?php    
	}
	
	protected function order_labels() 
	{
		$labels = array(
			'%order_id%'                => esc_html__( 'Номер заказа', 'usam' ),
			'%status_name%'             => esc_html__( 'Код статуса заказа', 'usam' ),
			'%status_title%'            => esc_html__( 'Имя статуса заказа', 'usam' ),
			'%status_description%'      => esc_html__( 'Описание статуса заказа', 'usam' ),
			'%payment_status%'          => esc_html__( 'Имя статуса оплаты', 'usam' ),
			'%order_date%'              => esc_html__( 'Дата заказа', 'usam' ),
			'%product_list%'            => esc_html__( 'Купленные товары', 'usam' ),
			'%downloadable_links%'      => esc_html__( 'Файлы для загрузки', 'usam' ),
			'%current_date%'            => esc_html__( 'Текущая дата', 'usam' ),	
			'%order_basket%'            => esc_html__( 'Сумма корзины заказа', 'usam' ),
			'%order_basket_currency%'   => esc_html__( 'Сумма корзины заказа в валюте', 'usam' ),	
			'%order_final_basket%'    => esc_html__( 'Сумма корзины заказа с учетом скидок', 'usam' ),
			'%order_final_basket_currency%' => esc_html__( 'Сумма корзины заказа с учетом скидок в валюте', 'usam' ),			
			'%order_basket_discount%'    => esc_html__( 'Скидка на товары', 'usam' ),
			'%order_basket_discount_currency%' => esc_html__( 'Скидка на товары в валюте', 'usam' ),				
			'%total_tax%'               => esc_html__( 'Налог заказа', 'usam' ),
			'%total_tax_currency%'      => esc_html__( 'Налог заказа в валюте', 'usam' ),
			'%total_shipping%'          => esc_html__( 'Доставка', 'usam' ),
			'%total_shipping_currency%' => esc_html__( 'Доставка в валюте', 'usam' ),
			'%total_price%'             => esc_html__( 'Итог заказа', 'usam' ),
			'%total_price_currency%'    => esc_html__( 'Итог заказа в валюте', 'usam' ),
			'%shop_name%'               => esc_html__( 'Название магазина', 'usam' ),
			'%discount%'                => esc_html__( 'Скидка (с названием)', 'usam' ),
			'%coupon_code%'             => esc_html__( 'Код купона', 'usam' ),	
			'%coupon_discount%'         => esc_html__( 'Скидка по купону', 'usam' ),			
			'%shipping_method%'         => esc_html__( 'Метод доставки', 'usam' ),
			'%shipping_method_name%'    => esc_html__( 'Названия метода доставки', 'usam' ),
			'%storage_city%'            => esc_html__( 'Город склада самовывоза', 'usam' ),
			'%storage_address%'         => esc_html__( 'Адрес склада самовывоза', 'usam' ),
			'%storage_phone%'           => esc_html__( 'Телефон склада самовывоза', 'usam' ),			
			'%shipping_readiness_date%' => esc_html__( 'Предпологаемая дата готовности', 'usam' ),			
			'%storage_schedule%'        => esc_html__( 'График работы склада самовывоза', 'usam' ),
			'%gateway_name%' 	        => esc_html__( 'Название способа оплаты', 'usam' ),
			'%gateway%' 		        => esc_html__( 'Способ оплаты', 'usam' ),
			'%total_discount%'          => esc_html__( 'Общаяя скидка', 'usam' ),	
			'%bonus%'                   => esc_html__( 'Потраченные бонусы', 'usam' ),	
			'%date_paid%'               => esc_html__( 'Дата оплаты заказа', 'usam' ),	
			'%order_paid%'              => esc_html__( 'Код статуса оплаты', 'usam' ),				
			'%bonus%'                   => esc_html__( 'Потраченные бонусы', 'usam' ),	
			'%total_paid%'              => esc_html__( 'Всего оплачено', 'usam' ),	
			'%total_paid_currency%'     => esc_html__( 'Всего оплачено в валюте', 'usam' ),	
			'%shop_logo%'               => esc_html__( 'Логотип магазина', 'usam' ),	
		);		
		return $labels;
	}	
	
	protected function display_order_labels()
	{
		$order_labels = $this->order_labels();
		$str_labels = implode(', ',array_keys($order_labels));
		?>
		<p><?php esc_html_e( 'Метки могут быть использованы', 'usam' );?>: <?php echo $str_labels; ?></p>
		<div class = "detailed_description">
			<a id = "link_description" href=""><?php esc_html_e( 'Подробнее...', 'usam' );?></a> 
			<div class = "description_box order_labels">
				<?php 						
				foreach ( $order_labels as $label => $title )
				{						
					echo "<span>$label</span> - $title</br>";
				}	
				?>				
			</div>
		</div>
		<?php
	}
	
	protected function get_checklist ( $name, $array, $checked_list = NULL )
	{               
        $output = '<ul>';
		foreach ($array as $id => $title) 
		{       
            $output  .= '<li id='.$id.'>' ;
            $output  .= '<label class="selectit">' ;
            $output  .= '<input id="'.$id.'" ';
            $output  .= 'type="checkbox" name="'.$name.'[]" ';
            $output  .= 'value="'.$id.'" ';
            if ( $checked_list ) 
			{
                if (in_array($id, $checked_list))
                   $output  .= 'checked="checked"';                     
            }
            $output  .= '>';
            $output  .= '&nbsp;<span class="name">'.$title;
            $output  .= '</span></label>';            
            $output  .= '</li>';             
        }
		$output .= '</ul>';
		return $output;
    } 
} 