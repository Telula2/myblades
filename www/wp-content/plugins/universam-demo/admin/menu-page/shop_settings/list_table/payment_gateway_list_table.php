<?php
class USAM_List_Table_payment_gateway extends USAM_List_Table
{	
    private $type_location;
	protected $orderby = 'id';
	protected $order = 'DESC';
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item )
	{	
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );		
	}	
	
	function column_shipping( $item )
	{	
		$setting = maybe_unserialize( $item->setting );		
		if ( !empty($setting['shipping']) )			
		{
			$i = 0;
			foreach( $setting['shipping'] as $id )
			{			
				$delivery_name = usam_get_delivery_name( $id );		
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo $delivery_name." ($id)";				
				$i++;
			}
		}
	}				
		   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'type'       => array('type', false),			
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',					
			'name'             => __( 'Название', 'usam' ),
			'description'      => __( 'Описание', 'usam' ),
			'active'           => __( 'Активность', 'usam' ),		
			'sort'             => __( 'Сортировка', 'usam' ),			
			'shipping'         => __( 'Доставки', 'usam' ),		
			'id'               => __( 'ID', 'usam' ),						
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		$query = array( 
			'fields' => 'all',	
			'active' => 'all',
			'search' => $this->search, 
			'order' => $this->order, 
			'orderby' => $this->orderby, 
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
		);			
		if ( !empty($this->records) )
			$query['include'] = $this->records;		
		
		$payment_gateways = new USAM_Payment_Gateways_Query( $query );	
		$this->items = $payment_gateways->get_results();		
			
		$total_items = $payment_gateways->get_total();
		$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page ) );
	}
}
?>