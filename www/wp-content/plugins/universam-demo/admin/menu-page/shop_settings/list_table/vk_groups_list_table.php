<?php
class USAM_List_Table_vk_groups extends USAM_List_Table
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_title( $item ) 
    {
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );	
	}
	
	function column_image( $item ) 
    {
		?><img src="<?php echo $item['photo']; ?>"><?php
	}
		
	function column_birthday( $item )
	{
		if ( !empty($item['birthday']) )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}	
	
	function column_publish_reviews( $item )
	{
		if ( !empty($item['publish_reviews']) )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}	
	
	function get_sortable_columns()
	{
		$sortable = array(
			'title'     => array('title', false),		
			'code'    => array('code', false),		
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',	
			'image'      => '',			
			'title'      => __( 'Имя', 'usam' ),
			'page_id'    => __( 'ID группы', 'usam' ),			
			'birthday'   => __( 'Поздравлять с ДР', 'usam' ),		
			'publish_reviews'   => __( 'Публиковать отзывы', 'usam' ),				
        );		
        return $columns;
    }
	
	function prepare_items() 
	{		
		$option = get_option('usam_vk_groups', '' );
		$data = maybe_unserialize( $option );	
		if ( !empty($data) )			
			foreach( $data as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{				
					$this->items[] = $item;
				}
			}		
		$this->total_items = count($this->items);	
		$this->forming_tables();
	}
}
?>