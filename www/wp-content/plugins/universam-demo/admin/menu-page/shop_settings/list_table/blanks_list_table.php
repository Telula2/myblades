<?php
class USAM_List_Table_blanks extends USAM_List_Table
{	   
	function column_title( $item )
	{				
		$actions = array(
			'edit'      => $this->add_row_actions( $item['id'], 'edit', __( 'Изменить', 'usam' ) ),			
		);	
		$this->row_actions_table( $item['title'], $actions );
	}	
				   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'id'        => array('id', false),				
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           		
			'title'            => __( 'Название', 'usam' ),			
        );		
        return $columns;
    }	
		
	function prepare_items() 
	{		
		$files = usam_list_dir( USAM_PRINTING_FORM );	
		foreach ( $files as $file ) 
		{
			if ( stristr( $file, '.php' ) ) 		
			{
				$file_path = USAM_PRINTING_FORM .'/'. $file;			
				$blanks_data = implode( '', file( $file_path ) );
				if ( preg_match( '|Printing Forms:(.*)$|mi', $blanks_data, $name ) ) 
				{					
					$parts = explode( '.', $file );
					$this->items[] = array( 'id' => $parts[0], 'title' => $name[1] );
				}
			}		
		}				
		$this->total_items = count($this->items);
		//$this->forming_tables();	
	}
}
?>