<?php
class USAM_List_Table_shipping extends USAM_List_Table
{	
    private $type_location;
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item )
	{	
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );		
	}	
	
	function column_price( $item )
	{	
		if ( $item->handler )
			echo '-';
		else
			echo $item->price;	
	}
	
	function column_location( $item )
	{	
		$setting = maybe_unserialize( $item->setting );			
		$i = 0;
		if ( !empty($setting['restrictions']['locations']) )
		{
			foreach($setting['restrictions']['locations'] as $id )
			{			
				$title = usam_get_full_locations_name( $id );
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo $title." ($id)";			
				$i++;
			}
		}
	}	
			
		   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'type'       => array('type', false),			
			'sort'       => array('sort', false),
			'price'       => array('price', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',				
			'name'             => __( 'Название', 'usam' ),
			'description'      => __( 'Описание', 'usam' ),
			'active'           => __( 'Активность', 'usam' ),		
			'sort'             => __( 'Сортировка', 'usam' ),				
			'location'         => __( 'Местоположения', 'usam' ),	
			'price'            => __( 'Стоимость', 'usam' ),	
			'id'               => __( 'ID', 'usam' ),						
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort','price' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;	
	
		$this->get_standart_query_parent();
		
		if ( $this->search != '' )
		{			
			$this->where[] = "name='".$this->search."'";			
		}
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_DELIVERY_SERVICE." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);	
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );	
	}
}
?>