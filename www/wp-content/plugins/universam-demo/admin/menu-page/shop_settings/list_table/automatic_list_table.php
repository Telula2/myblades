<?php
require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );
class USAM_List_Table_automatic extends USAM_List_Table
{	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}		
		
	public function extra_tablenav( $which ) 
	{			
		echo '<div class="alignleft actions">';		
		if ( 'top' == $which ) 
		{		
			?>
			<a href="<?php echo admin_url("options-general.php?page=shop_settings&tab=location&location_type=add"); ?>" class="button primary"><?php esc_html_e( 'Добавить тип', 'usam' ); ?></a>		
			<?php					
		}
		echo '</div>';
	}	
   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'type'       => array('type', false),			
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',	
			'code'             => __( 'Код', 'usam' ),	
			'name'             => __( 'Название', 'usam' ),		
			'sort'             => __( 'Сортировка', 'usam' ),			
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;
		
		$this->get_standart_query_parent();
		if ( $this->search != '' )
		{			
		$this->where[] = "name='{$this->search}'";			
		}
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_LOCATION_TYPE." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);	
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		
		$this->forming_tables();		
	}
}
?>