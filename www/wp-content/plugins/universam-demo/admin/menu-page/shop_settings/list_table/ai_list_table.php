<?php
class USAM_List_Table_ai extends USAM_List_Table
{	   	
	protected $orderby = 'yach';
	protected $order = 'ASC';
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_yach( $item )
	{		
		$this->row_actions_table( $item->yach, $this->standart_row_actions( $item->yach ) );
	}
				   
	function get_sortable_columns()
	{
		$sortable = array(
			'yach'           => array('yach', false),		
			); 
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',	
			'yach'             => __( 'Ячейка', 'usam' ),		
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;
		
		$this->get_standart_query_parent();		
			
		if ( $this->search != '' )
		{			
			$this->where[] = "yach='".$this->search."'";			
		}
		$where = implode( ' AND ', $this->where );	

		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_AI_MEMORY." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";		
		$this->items = $wpdb->get_results($sql_query);	
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );				
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>