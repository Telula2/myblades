<?php
class USAM_List_Table_vk_users_profiles extends USAM_List_Table
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_title( $item ) 
    {
		$this->row_actions_table( $item['first_name'].' '.$item['last_name'], $this->standart_row_actions( $item['id'] ) );	
	}
	
	function column_image( $item ) 
    {
		?><img src="<?php echo $item['photo_50']; ?>"><?php
	}
	
	function get_sortable_columns()
	{
		$sortable = array(
			'title'     => array('title', false),		
			'code'    => array('code', false),		
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',	
			'image'      => '',			
			'title'      => __( 'Имя', 'usam' ),
			'page_id'    => __( 'ID анкеты', 'usam' ),					
		//	'add_friends'   => __( 'Добавить друзей', 'usam' ),			
        );		
        return $columns;
    }
	
	function prepare_items() 
	{		
		$option = get_option('usam_vk_profile', '' );
		$data = maybe_unserialize( $option );	
		if ( !empty($data) )			
			foreach( $data as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}		
		$this->total_items = count($this->items);	
		$this->forming_tables();
	}
}
?>