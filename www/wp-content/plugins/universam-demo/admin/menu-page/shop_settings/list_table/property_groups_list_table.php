<?php
class USAM_List_Table_property_groups extends USAM_List_Table
{	
	protected $orderby = 'sort';
	protected $order = 'ASC';
		
	function column_name( $item ) 
    {
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );	
	}	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}		
		
	public function extra_tablenav( $which ) 
	{			
		echo '<div class="alignleft actions">';		
		if ( 'top' == $which ) 
		{		
			?>
			<a href="<?php echo admin_url("options-general.php?page=shop_settings&tab=location&location_type=add"); ?>" class="button primary"><?php esc_html_e( 'Добавить тип', 'usam' ); ?></a>		
			<?php					
		}
		echo '</div>';
	}	
   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'type_payer' => array('type_payer', false),			
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',	
			'name'             => __( 'Название', 'usam' ),	
			'type_payer'       => __( 'Тип плательщика', 'usam' ),				
			'sort'             => __( 'Сортировка', 'usam' ),			
        );		
        return $columns;
    }	
	
	public function column_type_payer( $item )
    {    	
		echo usam_get_name_payer( $item->type_payer_id );
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;		
		
		$this->get_standart_query_parent( );
		if ( $this->search != '' )
		{			
			$this->where[] = "name='".$this->search."'";			
		}
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_ORDER_PROPS_GROUP." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);		
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );			
		
		$this->_column_headers = $this->get_column_info();	
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>