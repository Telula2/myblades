<?php
class USAM_List_Table_prices extends USAM_List_Table
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_title( $item ) 
    {
		$this->row_actions_table( $item['title'], $this->standart_row_actions( $item['id'] ) );	
	}

	function column_currency( $item ) 
    {	
		echo usam_get_currency_name( $item['currency'] );		
	}		
	
	function column_role( $item )
	{
		if ( !empty($item['roles']))
		{
			$roles = get_editable_roles();			
			$roles['notLoggedIn']['name'] = __('Не зарегистрированные','usam');
			$i = 0;
			foreach ( $item['roles'] as $value )
			{
				
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo translate_user_role( $roles[$value]['name'] );		
				$i++;				
			}
		}
	}
	
	function column_location( $item )
	{	
		$i = 0;
		if ( !empty($item['locations']) )
		{
			foreach( $item['locations'] as $id )
			{				
				$title = usam_get_full_locations_name( $id );
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';	
				echo $title." ($id)";			
				$i++;				
			}
		}
	}	

	function column_base_type( $item ) 
    {		
		if ( !empty($item['base_type']) )
			echo usam_get_name_price_by_code( $item['base_type'] );
	}	
	
	function column_underprice( $item ) 
    {		
		if ( !empty($item['underprice']) )
			echo $item['underprice']."%";
	}
	 
	function get_sortable_columns()
	{
		$sortable = array(
			'title'     => array('title', false),		
			'code'    => array('code', false),		
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',			
			'title'      => __( 'Название', 'usam' ),
			'code'       => __( 'Код', 'usam' ),			
			'currency'   => __( 'Тип валюты', 'usam' ),		
			'location'   => __( 'Местоположение', 'usam' ),			
			'base_type'  => __( 'Базовый тип цен', 'usam' ),		
			'underprice' => __( 'Наценка', 'usam' ),		
			'role'       => __( 'Роль покупателей', 'usam' ),	
			'rounding'   => __( 'Порядок округления', 'usam' ),				
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		$type_prices = usam_get_prices( );	
		if ( empty($type_prices) )
			$this->items = array();	
		else
			foreach( $type_prices as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}		
		$this->total_items = count($this->items);	
		$this->forming_tables();
	}
}
?>