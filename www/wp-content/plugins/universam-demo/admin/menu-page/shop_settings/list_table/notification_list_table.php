<?php
class USAM_List_Table_notification extends USAM_List_Table
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_name( $item )
	{		
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );
	}		
	
	function column_order( $item )
	{		
		if ( $item['events']['order']['email'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
		if ( $item['events']['order']['sms'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
	}
	
	function column_feedback( $item )
	{		
		if ( $item['events']['feedback']['email'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
		if ( $item['events']['feedback']['sms'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
	}
	
	function column_webproduct( $item )
	{		
		if ( $item['events']['webproduct']['email'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
		if ( $item['events']['webproduct']['sms'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
	}
	
	
	function column_low_stock( $item )
	{		
		if ( $item['events']['low_stock']['email'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
		if ( $item['events']['low_stock']['sms'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
	}
	
	function column_no_stock( $item )
	{		
		if ( $item['events']['no_stock']['email'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
		if ( $item['events']['no_stock']['sms'] )
			echo "<p>".__( 'На телефон', 'usam' )."</p>";
	}
		
	function get_sortable_columns()
	{
		$sortable = array(
			'name'     => array('name', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',			
			'name'       => __( 'Название', 'usam' ),
			'active'     => __( 'Активность', 'usam' ),
			'order'     => __( 'Новый заказ', 'usam' ),		
			'feedback'     => __( 'Новые сообщения', 'usam' ),	
			'webproduct'     => __( 'Нет у поставщика', 'usam' ),	
			'low_stock'     => __( 'Низкий запас', 'usam' ),	
			'no_stock'     => __( 'Нет в наличии', 'usam' ),	
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		$option = get_option('usam_notifications');
		$notifications = maybe_unserialize( $option );	

		if ( empty($notifications) )
			$this->items = array();	
		else
			foreach( $notifications as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}			
		$this->total_items = count($this->items);	
		$this->forming_tables();	
	}
}
?>