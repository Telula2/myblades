<?php
class USAM_List_Table_cashbox extends USAM_List_Table
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_name( $item )
	{		
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );
	}		
	
	
	function get_sortable_columns()
	{
		$sortable = array(
			'name'     => array('name', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'         => '<input type="checkbox" />',			
			'name'       => __( 'Название', 'usam' ),
			'active'     => __( 'Активность', 'usam' ),
			'number'     => __( 'Заводской номер', 'usam' ),								
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		$option = get_option('usam_cashbox');
		$cashbox = maybe_unserialize( $option );	

		if ( empty($cashbox) )
			$this->items = array();	
		else
			foreach( $cashbox as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}			
		$this->total_items = count($this->items);	
		$this->forming_tables();	
	}
}
?>