<?php
class USAM_List_Table_types_payers extends USAM_List_Table 
{	
    function __construct( $args = array() )
	{	
		parent::__construct( $args );	
    }
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item ) 
    {
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );	
	}
	
	function column_type( $item ) 
    {
		if ( $item['type'] == 'E' )
			_e( 'Юридическое лицо', 'usam' );
		else
			_e( 'Физическое лицо', 'usam' );	
	}
	
	function get_sortable_columns()
	{
		$sortable = array(
			'name'       => array('name', false),		
			'active'     => array('active', false),		
			'sort'       => array('sort', false),		
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(  
			'cb'             => '<input type="checkbox" />',
			'name'           => __( 'Название', 'usam' ),	
			'active' 	     => __( 'Активность', 'usam' ),		
			'type'            => __( 'Тип плательщика', 'usam' ),					
			'sort'           => __( 'Сортировка', 'usam' ),					
        ); 
        return $columns;
    }	
	
	function prepare_items() 
	{				
		$types_payers = usam_get_group_payers( false );
		
		$total_items = 0;
		$this->items = array();
		foreach ( $types_payers as $key => $item )
		{			
			if ( empty($this->records) || in_array($item['id'], $this->records) )
			{
				$this->items[$key] = $item;						
				$total_items++;
			}
		}		
		$this->total_items = count($total_items);			
		$this->forming_tables();		
	}
}