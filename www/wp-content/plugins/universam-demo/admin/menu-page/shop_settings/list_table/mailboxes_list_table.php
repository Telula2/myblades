<?php
class USAM_List_Table_mailboxes extends USAM_List_Table
{	
	protected $orderby = 'sort';
	protected $order   = 'ASC';
		
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_name( $item ) 
    {
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );	
	}	

	function get_sortable_columns()
	{
		$sortable = array(
			'title'     => array('title', false),		
			'code'    => array('code', false),		
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'        => '<input type="checkbox" />',		
			'name'      => __( 'Имя', 'usam' ),						
			'email'     => __( 'Почта', 'usam' ),
			'drag'     => '&nbsp;',			
        );		
        return $columns;
    }	
	
	function prepare_items() 
	{						
		$args = array( 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'search' => $this->search, 'orderby' => $this->orderby );		
		
		if ( !empty( $this->records ) )
			$args['include'] = $this->records;
			
		$this->items = usam_get_mailboxes( $args );		
		if ( $this->per_page )
		{
			global $wpdb;
			$this->total_items = $wpdb->get_var( 'SELECT FOUND_ROWS()' );			
			$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}
?>