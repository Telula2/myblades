<?php
class USAM_List_Table_property extends USAM_List_Table
{
	protected $orderby = 'sort';
	protected $order = 'ASC';
	private $group = array();
	
	function __construct( $args )
	{	
        parent::__construct( $args );
		$this->group = usam_get_order_props_groups();
	}

	public function single_row( $item ) 
	{
		echo '<tr data-field-id="'.$item->id.'" id="checkout_'.$item->id.'" class="checkout_form_field" >';
		$this->single_row_columns( $item );
		echo '</tr>';
	}	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}		
		
	public function extra_tablenav( $which ) 
	{			
		echo '<div class="alignleft actions">';		
		if ( 'top' == $which ) 
		{	?>
				
			<?php					
		}
		echo '</div>';
	}	
	
	function column_name( $item )
	{
		$this->row_actions_table( $item->name, $this->standart_row_actions( $item->id ) );
	}
		
	function column_group( $item )
	{		
		echo $this->group[$item->group]->name;
	}
	
	function column_mandatory( $item )
	{
		if ( $item->mandatory == 1 )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}
	
	function column_profile( $item )
	{
		if ( $item->profile == 1 )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}
	
	function column_fast_buy( $item )
	{
		if ( $item->fast_buy == 1 )
			_e('Да','usam');
		else
			_e('Нет','usam');
	}		
	/*
	 * Определить, является ли это поле по умолчанию или нет.
	 */
	private function is_field_default( $field ) 
	{		
		$default_fields = array(
				'billingfirstname',
				'billinglastname',
				'billingaddress',				
				'billingpostcode',
				'billingemail',
				'billingphone',				
				'billingmobilephone',
				'shippingfirstname',
				'shippinglastname',
				'shippingaddress',
				'shippingcity',
				'shippingregion',
				'shippingcountry',
				'shippingpostcode',
				'shippingemail',
				'shippinglocation',
				'shippingnotesclient',
				'company',
				'company_adr',
				'inn',
				'kpp',
				'contact_person',
				'fax',
				'company_shippingaddress',				
				'company_shippingcountry',			
				'company_shippingregion',			
				'company_shippingcity',			
				'company_shippingpostcode',			
				'company_shippinglocation',			
				'company_shippingnotesclient',	
			);
		if ( in_array( $field->unique_name, $default_fields ) )
			return true;
		return false;
	}
   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'            => array('name', false),			
			'type'            => array('type', false),			
			'active'          => array('active', false),		
			'unique_name'     => array('unique_name', false),		
			'sort'  => array('sort', false),				
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   			
			'name'           => __( 'Название', 'usam' ),
			'active'         => __( 'Активность', 'usam' ),
			'type'           => __( 'Тип', 'usam' ),
			'group'          => __( 'Группа', 'usam' ),
			'unique_name'    => __( 'Код', 'usam' ),			
			'mandatory'      => __( 'Обязательное', 'usam' ),
			'profile'        => __( 'Входит в профиль', 'usam' ),	
			'fast_buy'       => __( 'Быстрый заказ', 'usam' ),			
			'sort' => __( 'Сортировка', 'usam' ),			
			'drag'           => '&nbsp;',
        );		
        return $columns;
    }	
		
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;	

		$this->get_standart_query_parent( );		
	
		if ( $this->search != '' )
		{			
			$this->where[] = "name='".$this->search."'";			
		}
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_ORDER_PROPS." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );
		
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>