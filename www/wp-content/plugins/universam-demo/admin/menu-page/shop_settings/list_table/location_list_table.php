<?php
class USAM_List_Table_location extends USAM_List_Table
{	
    private $type_location;
	private $location_list = 0;
	private $location_id_parent;
	
	function __construct( $args = array() )
	{	
       parent::__construct( $args );
		
		$this->location_list = isset($_GET['location_list'])?$_GET['location_list']:$this->location_list;
		$this->location_id_parent = isset($_GET['id_parent']) && $_GET['id_parent'] != '' ?$_GET['id_parent']:0;	
		
		$this->type_location = usam_get_types_location();
    }	
	
	public function return_post()
	{
		return array('location_list');
	}	
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_name( $item )
	{			
		$url = add_query_arg( array( 'id_parent' => $item->id, 'location_list' => $this->location_list ), $_SERVER['REQUEST_URI'] );			
		$title = "<a href='".$url."'>$item->name</a>";		
		$this->row_actions_table( $title, $this->standart_row_actions( $item->id ) );
	}
	
	function column_type( $item ) 
	{	
		if ( !empty($item->id_type) )
			echo $this->type_location[$item->id_type]->name;		
	}
		
	function column_id( $item ) 
	{	
		if ( $item->id )
			echo $item->id;		
	}		
		
	public function extra_tablenav( $which ) 
	{			
		echo '<div class="alignleft actions">';		
		if ( 'top' == $which ) 
		{					
			?>				
			<div class = "usam_manage usam_manage-location_list">
				<input type='hidden' value='0' name='location_list' />
				<input id="location_list" type='checkbox' name='location_list' <?php checked( $this->location_list ); ?> value = '1' />
				<label for="location_list"><?php esc_html_e( 'Показать список', 'usam' ); ?></label>
			</div>	
			<?php 
			submit_button( __( 'Фильтр', 'usam' ), 'secondary', false, false, array( 'id' => 'location_submit' ) ); 
		}
		echo '</div>';
	}
	
	public function single_row( $item )
	{
		echo '<tr id = "item-'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'id'        => array('id', false),			
			'type'       => array('id_type', false),		
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',				
			'name'             => __( 'Название', 'usam' ),
			'id'               => __( 'Номер', 'usam' ),
			'type'             => __( 'Тип', 'usam' ),	
			'sort'             => __( 'Сортировка', 'usam' ),			
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		global $wpdb;
		
		$this->get_standart_query_parent( );				
						
		if ( $this->search != '' )
		{			
			$this->where[] = "name LIKE LOWER('".$this->search."%')";			
		}
		if ( !$this->location_list )
		{					
			$this->where[] = "parent='$this->location_id_parent'";				
		}	
		$where = implode( ' AND ', $this->where );	
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_LOCATION." WHERE $where ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query);
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		

		if ( !$this->location_list && $this->location_id_parent )
		{					
			$location = usam_get_location( $this->location_id_parent );
			$up->name = '...';
			$up->id = $location['parent'];
			$up->sort = '';
			$up->type = '';						
			array_unshift($this->items, $up );	
			$this->total_items++;
		}		
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );		
	}
}
?>