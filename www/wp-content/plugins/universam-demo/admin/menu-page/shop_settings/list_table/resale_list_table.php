<?php
class USAM_List_Table_resale extends USAM_List_Table 
{	
    function __construct( $args = array() )
	{	
		parent::__construct( $args );	
    }
	
	function column_name( $item ) 
    {
		$name = "<a href='".$item['domain']."' class='link'>".$item['name']."</a>";		
		$this->row_actions_table( $name, $this->standart_row_actions( $item['id'] ) );	
	}
	
	function get_sortable_columns()
	{
		$sortable = array(
			'name'       => array('name', false),		
			'currency_code'     => array('currency_code', false),	
		);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(  
			'cb'             => '<input type="checkbox" />',
			'name'           => __( 'Название сайта', 'usam' ),	
			'currency_code'  => __( 'Тип валюты', 'usam' ),					
        ); 
        return $columns;
    }	
	
	function prepare_items() 
	{	
		$option = get_option('usam_settings_sites_suppliers');
		$sites = maybe_unserialize( $option );	
		if ( !empty($sites) )
		{
			foreach ( $sites as $key => $item )
			{
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}		
			$this->total_items = count($this->total_items);			
		}
		$this->forming_tables();		
	}
}