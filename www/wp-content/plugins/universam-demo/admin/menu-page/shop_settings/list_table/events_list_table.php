<?php
class USAM_List_Table_events extends USAM_List_Table
{	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_event( $item )
	{		
		$this->row_actions_table( $item['event'], $this->standart_row_actions( $item['id'] ) );
	}
				   
	function get_sortable_columns()
	{
		$sortable = array(
			'name'      => array('name', false),			
			'id'        => array('id', false),		
			'sort'       => array('sort', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'               => '<input type="checkbox" />',				
			'event'            => __( 'Событие', 'usam' ),		
			'handling'         => __( 'Обработки', 'usam' ),				
        );		
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('sort' );
    }
	
	function prepare_items() 
	{	
		$option = get_option('usam_event_handling');
		$contact_statuses = maybe_unserialize( $option );	

		if ( empty($contact_statuses) )
			$this->items = array();	
		else
			foreach( $contact_statuses as $key => $item )
			{	
				if ( empty($this->records) || in_array($item['id'], $this->records) )
				{					
					$this->items[] = $item;
				}
			}					
		$this->total_items = count($this->items);	
		$this->forming_tables();	
	}
}
?>