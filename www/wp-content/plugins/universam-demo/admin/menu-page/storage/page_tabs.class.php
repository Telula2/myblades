<?php
/*
 * Отображение страницы Складской учет
 */
 
$default_tabs = array(
	array( 'id' => 'reserves',   'title' => __( 'Резервы', 'usam' ) ),
	array( 'id' => 'inventory',  'title' => __( 'Инвентаризация', 'usam' ) ),
	array( 'id' => 'storage',    'title' => __( 'Склады', 'usam' ) ), 
	array( 'id' => 'sold',       'title' => __( 'Проданные', 'usam' ) ),
	array( 'id' => 'suppliers',  'title' => __( 'Web поставщики', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{		
	protected function localize_script_tab()
	{		
		return array( );
	}
} 