<?php
class USAM_List_Table_storage extends USAM_List_Table
{	
	protected $orderby = 'id';
	protected $order   = 'asc'; 

	function no_items() 
	{
		_e( 'Нет складов' );
	}		
	
	function get_bulk_actions_display() 
	{
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}
	
	function column_title( $item )
	{	
		$this->row_actions_table( $item->title, $this->standart_row_actions( $item->id ) );
	}
	
	function column_author( $item )
	{	
		echo usam_get_manager_name( $item->author );
	}
	
	function column_location_id( $item )
	{	
		echo usam_get_full_locations_name( $item->location_id );	
	}
	
	function column_issuing( $item )
	{	
		$this->logical_column( $item->issuing );	
	}
	
	function column_shipping( $item )
	{	
		$this->logical_column( $item->issuing );
	}	
	  
	function get_sortable_columns() 
	{
		$sortable = array(
			'id'       => array('id', false),
			'code'     => array('code', false),
			'title'    => array('title', false),
			'sort'     => array('sort', false),
			'active'   => array('active', false),	
			'schedule' => array('schedule', false),	
			'date'     => array('date', false),	
			'author'   => array('author', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',
			'title'       => __( 'Название', 'usam' ),			
			'id'          => __( 'ID', 'usam' ),
			'code'        => __( 'Код', 'usam' ),	
			'active'      => __( 'Активность', 'usam' ),				
			'issuing'     => __( 'Выдача', 'usam' ),
			'shipping'    => __( 'Отгрузка', 'usam' ),
			'location_id' => __( 'Местоположение', 'usam' ),	
			'address'     => __( 'Адрес', 'usam' ),	
			'description' => __( 'Описание', 'usam' ),	
			'phone'       => __( 'Телефон', 'usam' ),	
			'schedule'    => __( 'График работы', 'usam' ),	
			'date_modified' => __( 'Дата изменения', 'usam' ),
			'author'      => __( 'Кем изменен', 'usam' )	
		//'site'  => __( 'Сайт', 'usam' ),	
        );
        return $columns;
    }
	
	public function get_hidden_columns()
    {
        return array('address', 'schedule');
    }
	
	public function get_number_columns_sql()
    {       
		return array('id', 'sort');
    }
	
	function prepare_items() 
	{			
		global $wpdb;
	
		$this->get_standart_query_parent( );

		$search_terms = empty( $this->search ) ? array() : explode( ' ', $this->search );
		$search_sql = array();			
		foreach ( $search_terms as $term )
		{
			$search_sql[$term][] = "title LIKE '%".esc_sql( $term )."%'";
			if ( is_numeric( $term ) )
				$search_sql[$term][] = 'id = ' . esc_sql( $term );
			$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
		}
		$search_sql = implode( ' AND ', array_values( $search_sql ) );
		if ( $search_sql )
		{
			$this->where[] = $search_sql;
		}		
		$where = implode( ' AND ', $this->where );			
		
		$sql_query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".USAM_TABLE_STORAGE_LIST." WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results($sql_query );	

		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );		
	
		$this->_column_headers = $this->get_column_info();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );	
	}
}