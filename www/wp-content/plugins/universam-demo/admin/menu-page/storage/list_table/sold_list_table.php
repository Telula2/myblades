<?php
class USAM_List_Table_sold extends USAM_List_Table 
{		    
	protected $prefix = 'c';	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );			
    }	

	public function column_order_date( $item ) 
	{
		echo date_i18n( __( get_option( 'date_format', 'Y/m/d' ) ), strtotime( $item['order_date'] ) );
	}
	
	public function column_order_id( $item ) 
	{
		echo usam_get_link_order( $item['order_id'] );	
	}
	
	//Фильтр по доставке
	public function shipping_dropdown() 
	{
		$shipping = isset( $_REQUEST['shipping'] ) ? $_REQUEST['shipping'] : 0;			
		$delivery_service = usam_get_delivery_services();
		?>
		<select name="shipping">
			<option <?php selected( 0, $shipping ); ?> value="0"><?php _e( 'Все варианты' ); ?></option>
			<?php
			foreach ( (array)$delivery_service as $method ) 
			{					
				?>
				<option <?php selected( $method->id, $shipping ); ?> value="<?php echo $method->id; ?>"><?php echo $method->name; ?></option>
				<?php
			}		
			?>				
		</select>		
		<?php
	}
	
	//Фильтр количества, сумме
	public function numerical_dropdown() 
	{
		$condition = isset( $_REQUEST['condition'] ) ? $_REQUEST['condition'] : '';		
		$compare = isset( $_REQUEST['compare'] ) ? $_REQUEST['compare'] : 0;
		$condition_v = isset( $_REQUEST['condition_v'] ) ? $_REQUEST['condition_v'] : '';		
		?>	
		<div class = "numerical">
			<select name="condition">
				<option <?php selected( 'product_price', $condition ); ?> value="product_price"><?php _e('Цена товаров', 'usam'); ?></option>				
				<option <?php selected( 'product_quantity', $condition ); ?> value="product_quantity"><?php _e('Количество товаров', 'usam'); ?></option>	
				<option <?php selected( 'order_price', $condition ); ?> value="order_price"><?php _e('Сумма заказа', 'usam'); ?></option>		
				<option <?php selected( 'order_quantity', $condition ); ?> value="order_quantity"><?php _e('Количество товаров в заказе', 'usam'); ?></option>					
			</select>
			<select name="compare">
				<option <?php selected( 0, $compare ); ?> value="0"><?php _e('Меньше', 'usam'); ?></option>
				<option <?php selected( 1, $compare ); ?> value="1"><?php _e('Равно', 'usam'); ?></option>	
				<option <?php selected( 2, $compare ); ?> value="2"><?php _e('Больше', 'usam'); ?></option>			
			</select>
			<input type="text" name="condition_v" value="<?php echo $condition_v; ?>" size = "7" />
		</div>
		<?php
	}
	
	public function user_dropdown() 
	{
		$user = isset( $_REQUEST['user'] ) ? $_REQUEST['user'] : '';		
		?>		
		<input type="text" name="user" value="<?php echo $user; ?>" size = "10" />
		<?php
	}	
	
	public function storage_dropdown() 
	{		
		global $wpdb;			
		$storage_selected = isset( $_REQUEST['storage'] ) ? $_REQUEST['storage'] : 0;		
		?>	
		<select name="storage">			
			<option <?php selected( 0, $storage_selected ); ?> value="0"><?php _e('Склады для выдачи', 'usam'); ?></option>
			<?php	
			$storages = $wpdb->get_results("SELECT id, title FROM ".USAM_TABLE_STORAGE_LIST." WHERE active = '1'");		
			foreach ( $storages as $storage )
			{
				?><option <?php selected( $storage->id, $storage_selected ); ?> value="<?php echo $storage->id; ?>"><?php echo $storage->title; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	public function status_order_dropdown() 
	{				
		$status_order_selected = isset( $_REQUEST['status_order'] ) ? $_REQUEST['status_order'] : 0;		
		$statuses = usam_get_order_statuses( );		
		?>	
		<select name="status_order">			
			<option <?php selected( 0, $status_order_selected ); ?> value="0"><?php _e('Все статусы', 'usam'); ?></option>
			<?php				
			foreach ( $statuses as $status )
			{
				?><option <?php selected( $status->internalname, $status_order_selected ); ?> value="<?php echo $status->internalname; ?>"><?php echo $status->name; ?></option><?php
			}
			?>				
		</select>
		<?php
	}	
	
	protected function get_filter_tablenav( ) 
	{
		//'months' => __( 'По датам', 'usam' ),
		return array( 'status_order' => array( 'title' => __( 'По статусам заказа', 'usam' )), 'shipping' => array( 'title' => __( 'По доставке', 'usam' )), 'storage' => array( 'title' => __( 'По складу', 'usam' )), 'user' => array( 'title' => __( 'По покупателям', 'usam' )), 'numerical' => array() );		
	}	
	   
	function get_sortable_columns() 
	{
		$sortable = array(
			'id'       => array('id', false),			
			'name'     => array('name', false),
			'sku'      => array('meta_value', false),	
			'order_id' => array('order_id', false),	
			'quantity' => array('quantity', false),	
			'price'    => array('price', false),	
			'order_date' => array('order_id', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(   
			'cb'          => '<input type="checkbox" />',			
			'product_id'      => __( 'ID', 'usam' ),
			'sku'         => __( 'Артикул', 'usam' ),				
			'name'        => __( 'Название', 'usam' ),		
			'price'       => __( 'Цена', 'usam' ),	
			'quantity'    => __( 'Количество', 'usam' ),				
			'order_id'    => __( 'Заказ', 'usam' ),			
			'order_date'  => __( 'Дата заказа', 'usam' ),					
		//'site'  => __( 'Сайт', 'usam' ),	
        );
        return $columns;
    }
	
	public function prepare_items() 
	{
		global $wpdb, $user_ID;

		if ( isset($_REQUEST['orderby']) )
			switch ( $_REQUEST['orderby'] ) 
			{
				case 'order_date' :
				case 'order_id' :
					$this->prefix = 'p';
				break;
				case 'meta_value' :
					$this->prefix = 'pm';
				break;				
			}
		$this->get_standart_query_parent( );
		
		$page = $this->get_pagenum();
		$offset = ( $page - 1 ) * $this->per_page;
		$joins = array();	

		if ( isset( $_REQUEST['post'] ) )
		{
			$post_ids = array_map('intval', $_REQUEST['post']);
			$this->where[] = 'c.id IN (' . implode( ', ', $post_ids ) . ')';
		}
		$i = 1;
		//название колонок, данные которых нужно получить
		$selects = array( 'c.id', 'c.product_id', 'c.name', 'c.order_id AS order_id', 'c.price', 'c.quantity', 'p.date_insert AS order_date', 'pm.meta_value AS sku', );
		$joins[] = "INNER JOIN ".USAM_TABLE_ORDERS." AS p ON c.order_id = p.id";	
		$joins[] = "INNER JOIN ".USAM_TABLE_SHIPPED_DOCUMENTS." AS sd ON c.order_id = sd.order_id";	
		$joins[] = "LEFT OUTER JOIN ".$wpdb->postmeta." AS pm ON c.product_id = pm.post_id AND pm.meta_key='_usam_sku'";		
	
		if ( $this->search != '' )
		{		
			$search_sql = array();				
			if ( is_numeric( $s ) )
				$search_sql[] = 'c.order_id='.esc_sql( $s );		
			
			$product_id = usam_get_product_id_by_sku( $s );
			if ( $product_id > 0 )
				$search_sql[] = "c.product_id=$product_id";				
			
			$search_sql[] = "c.name LIKE LOWER ('%" . esc_sql( $s ). "%')";			
			$search_terms = explode( ' ', $s );		
			foreach ( $search_terms as $term )
			{						
				$search_sql[] = "c.name LIKE LOWER ('%" . esc_sql( $term ). "%')";
			}
			$search_sql = implode( ' AND ', array_values( $search_sql ) );
			if ( $search_sql )
			{
				$this->where[] = $search_sql;
			}
		}			
		// фильтр статусов
		if ( ! empty( $_REQUEST['status_order'] ) && $_REQUEST['status_order'] != 'all' ) 
		{						
			$status = sanitize_title($_REQUEST['status_order']);
			$this->where[] = "p.status = '$status'";						
		}				
		// фильтр даты
		if ( ! empty( $_REQUEST['m'] ) )
		{
			$year = (int) substr( $_REQUEST['m'], 0, 4);
			$month = (int) substr( $_REQUEST['m'], -2 );
			$this->where[] = "YEAR(FROM_UNIXTIME(p.date)) = " . esc_sql( $year );
			$this->where[] = "MONTH(FROM_UNIXTIME(p.date)) = " . esc_sql( $month );
		}		
		if ( !empty( $_REQUEST['shipping'] ) && $_REQUEST['shipping'] != 'all' )
		{
			$shipping = array_map('intval', (array)$_REQUEST['shipping']);
			$method = implode( ',',  $shipping );		
			$this->where[] = "sd.method IN (".$method.")";		
		}
		if ( isset( $_REQUEST['user'] ) && $_REQUEST['user'] != '' )
		{
			if ( is_numeric( $_REQUEST['user'] ) )			
				$this->where[] = "p.user_ID = '".absint($_REQUEST['user'])."'";	
			else
			{				
				$login = sanitize_text_field($_REQUEST['user']);
				$user = get_user_by('login', $login);
				if ( is_numeric( $user->ID ) )
					$this->where[] = "p.user_ID = '".$user->ID."'";	
				else
					$this->where[] = "(p.user_ID = '' AND p.user_ID != '0')";	
			}
		}			
		if ( !empty($_REQUEST['condition_v']) && !empty($_REQUEST['condition']) )
		{
			$condition = sanitize_text_field($_REQUEST['condition']);			
			$compare = (int) $_REQUEST['compare'];
			$condition_v = sanitize_text_field($_REQUEST['condition_v']);
			
			$compare_where = "=";		
			switch ( $condition )
			{
				case 'product_price' :
					$condition_where = "c.price";					
				break;
				case 'product_quantity' :
					$condition_where = "c.quantity";					
				break;				
				case 'order_price' :
					$condition_where = "p.totalprice";				
				break;		
				case 'order_quantity' :
					$condition_where = "( SELECT COUNT(*) FROM ".USAM_TABLE_PRODUCTS_ORDER." AS c WHERE c.order_id = p.id )";				
				break;					
			}
			switch ( $compare ) 
			{
				case 0 :
					$compare_where = "<";					
				break;
				case 1 :
					$compare_where = "=";					
				break;	
				case 2 :
					$compare_where = ">";					
				break;				
			}
			$this->where[] = "( $condition_where $compare_where '$condition_v')";
		}
		if ( !empty( $_REQUEST['storage'] ) && $_REQUEST['storage'] != 'all' )
		{
			$storage = array_map('intval', (array)$_REQUEST['storage']);
			$in_storage = implode( ',',  $storage );		
			$this->where[] = "sd.storage IN (".$in_storage.")";		
		}		
		if ( !empty( $_REQUEST['user_role'] ) )
		{
			$this->where[] = "p.user_role = '".sanitize_title($_REQUEST['user_role'])."'";	
		}		
		$selects = implode( ', ', $selects );
		$joins = implode( ' ', $joins );
		$where = implode( ' AND ', $this->where );	
				
		$sql = "SELECT SQL_CALC_FOUND_ROWS {$selects} FROM ".USAM_TABLE_PRODUCTS_ORDER." AS c {$joins} WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
	
		$this->items = $wpdb->get_results( $sql, ARRAY_A );		
		if ( $this->per_page )
		{
			$total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );				
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}	
}