<?php
//Web поставщики товара
class USAM_List_Table_suppliers extends USAM_Product_List_Table 
{		
	function __construct( $args = array() )
	{	
		parent::__construct( $args );	
    }		
	
	function column_link( $item ) 
	{	
		$product_meta = usam_get_product_meta( $item->ID, 'product_metadata', true );		
		if (!empty($product_meta['webspy_link']))
		{
			$host = parse_url($product_meta['webspy_link'], PHP_URL_HOST);
			echo "<br/><a href='".$product_meta['webspy_link']."' target='_blank'>".$host."</a>";
		}
	}	
	
	function column_date_externalproduct( $item ) 
	{	
		$date = usam_get_product_meta( $item->ID, 'date_externalproduct', true );
		$date_format = get_option('date_format');	
		echo date_i18n( "$date_format H:i:s",  strtotime($date) );				
	}	  
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'product_title' => array('post_title', false),			
			'date_externalproduct' => array('date_externalproduct', false),					
			'stock'         => array('product_stock', false),
			'sku'           => array('product_sku', false),		
			);
		return $sortable;
	}
			
	function get_columns()
	{
        $columns = array(           
			'cb'    => '<input type="checkbox" />',			
			'image' => '',
			'product_title' => __( 'Название', 'usam' ),
			'sku'   => __( 'Артикул', 'usam' ),		
			'stock' => __( 'Запас', 'usam'),		
			'link'  => __( 'Ссылка', 'usam' ),	
			'date_externalproduct'  => __( 'Дата проверки', 'usam' ),				
        );		
        return $columns;
    }	
}