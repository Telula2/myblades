<?php
//Зарезервированные товары
require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );

class USAM_List_Table_reserves extends USAM_Product_List_Table 
{	
    function __construct( $args = array() )
	{	
		parent::__construct( $args );			
    }	
	
	public function get_views(){}
	
	function get_bulk_actions_display() 
	{			
		$actions = array( 'delete'    => __( 'Удалить из резерва', 'usam' ) );
		return $actions;
	}
	
	function query_vars( $query_vars ) 
	{	
		$query_vars['meta_query'] = array( array( 'key' => '_usam_reserve', 'value' => '0', 'compare' => '!=','type' => 'numeric' ));	
		return $query_vars;
	}

	protected function get_filter_tablenav( ) 
	{		
		return array( 'terms' => array() );		
	}
		
	function get_sortable_columns() 
	{
		$sortable = array(
			'product_title' => array('product_title', false),
			'price'     	=> array('price', false),			
			'stock'      	=> array('stock', false),
			'sku'        	=> array('sku', false),
			'reserve'       => array('reserve', false),
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'            => '<input type="checkbox" />',			
			'image'         => '',
			'product_title' => __('Имя', 'usam' ),			
			'sku'           => __('Артикул', 'usam' ),	
			'cats'          => __('Категория', 'usam' ),					
			'stock'         => __('Доступно', 'usam' ),	
			'reserve'       => __('Резерв', 'usam' ),	
        );				
        return $columns;
    }
}