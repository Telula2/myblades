<?php
require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );

class USAM_List_Table_inventory extends USAM_Product_List_Table 
{
	private $type_price = '';
	private $storages = array();
	
    function __construct( $args = array() )
	{	
		parent::__construct( $args );

		$this->storages = usam_get_stores( );					
    }	
		
	public function extra_tablenav( $which ) 
	{		
		if ( 'top' == $which )
		{
			submit_button( __( 'Сохранить', 'usam' ), 'primary', 'action', false, array( 'ID' => 'action-submit' ) );
			
			$filter_manage = new USAM_Product_Filter_Manage();			 			
			$filter_manage->filter_print('products_type_price');
			usam_terms_manage_products();
			submit_button( __( 'Фильтр', 'usam' ), 'secondary', false, false, array( 'ID' => 'filter-submit' ) );	
		}
		elseif ( 'bottom' == $which )
		{	
			submit_button( __( 'Удалить все', 'usam' ), 'primary', 'del', false, array( 'ID' => 'action-submit' ) );	
		}
	}	
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'product_title' => array('product_title', false),
			'price'     	=> array('price', false),			
			'stock'      	=> array('stock', false),
			'SKU'        	=> array('SKU', false),
			'views'     	 => array('views', false),
			'date'      	 => array('date', false)
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
		//	'cb'            => '<input type="checkbox" />',			
			'image'         => '',
			'product_title' => __('Имя', 'usam' ),
			'sku'           => __('Артикул', 'usam' ),
			'price'         => __('Цена', 'usam' ),				
			'stock'         => __('Запас', 'usam' ),	
			'reserve'       => __('Резерв', 'usam' ),	
			'cats'          => __('Категория', 'usam' ),	
        );
		foreach ( $this->storages as $storage )
		{				
			$columns[USAM_META_PREFIX.$storage->meta_key] = $storage->title;
		}			
        return $columns;
    }
}