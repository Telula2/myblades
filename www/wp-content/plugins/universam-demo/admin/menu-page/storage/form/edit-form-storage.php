<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Storage extends USAM_Edit_Form
{	
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить склад %s','usam'), $this->data['title'] );
		else
			$title = __('Добавить склад', 'usam');	
		return $title;
	}
	
	protected function get_data_tab()
	{		
		if ( $this->id != null )
		{				
			$this->data = usam_get_storage( $this->id );
		}
		else
			$this->data = array( 'active' => 1, 'issuing' => 1, 'location_id' => '', 'shipping' => 1, 'title' =>'', 'address' =>'', 'description' =>'', 'phone' =>'', 'schedule' => '', 'email' => '', 'code' => '', 'GPS_N' => 1, 'GPS_S' => 1, 'sort' => 100, 'site_id' => '', 'img' => '' );		
	}
	
	function display_right()
	{		
		$title = __( 'Миниатюра для склада', 'usam' );		
		$this->display_imagediv( $this->data['img'], $title );
	}
	
	function display_left()
	{	
		$this->titlediv( $this->data['title'] );	
		$this->add_box_description( $this->data['description'] );
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_storage_general_settings', __( 'Общие настройки', 'usam' ), array( $this, 'storage_general_settings' ) );	
	}	
		
	function storage_general_settings()
	{		
		?>
		<table class="subtab-detail-content-table usam_edit_table" id="storage">
			<tbody>				
				<tr>
					<?php
					if ( $this->data['issuing'] == 1 )
						$checked = 'checked';
					else
						$checked = '';
					?>
					<td class = "name"><?php _e('Пункт выдачи','usam'); ?>:</td>
					<td class = "option">
						<input type="hidden" name="storage[issuing]" value="0"/>
						<input type="checkbox" name="storage[issuing]" value="1" <?php echo $checked; ?>/>						
					</td>
				</tr>
				<tr>
					<?php
					if ( $this->data['shipping'] == 1 )
						$checked = 'checked';
					else
						$checked = '';
					?>
					<td class = "name"><?php _e('Для отгрузки','usam'); ?>:</td>
					<td class = "option">
						<input type="hidden" name="storage[shipping]" value="0"/>
						<input type="checkbox" name="storage[shipping]" value="1" <?php echo $checked; ?> />						
					</td>
				</tr>
				<?php
				if ( is_multisite() )
				{
			/*	?>
				<tr>
					<?php
					if ( $this->data['site_id'] == 1 )
						$checked = 'checked';
					else
						$checked = '';
					?>
					<td class = "name"><?php _e('Сайт','usam'); ?>:</td>
					<td>
						<select id="site_id" style="max-width: 300px; width: 300px;" name="storage[site_id]"  />
							<option  value="0">Выберите сайт</option>							
						</select>
					</td>
				</tr>
				<?php */
				}
				?>			
				<tr class="subtab-detail-required-field">
					<td class = "name"><?php _e('Город','usam'); ?>:</td>
					<td>
						<?php
						$autocomplete = new USAM_Autocomplete_Forms( );
						$autocomplete->get_form_position_location( $this->data['location_id'] );			
						?>	
					</td>
				</tr>	
				<tr class="subtab-detail-required-field">
					<td class = "name"><?php _e('Адрес','usam'); ?>:</td>
					<td>
						<textarea rows="3" class="typearea" name="storage[address]" wrap="virtual"><?php echo $this->data['address']; ?></textarea>
					</td>
				</tr>				
				<tr>
					<td class = "name"><?php _e('Телефон','usam'); ?>:</td>
					<td>
						<input type="text" name="storage[phone]" value="<?php echo $this->data['phone']; ?>" size="45" />
					</td>
				</tr>
				<tr>
					<td class = "name"><?php _e('График работы','usam'); ?>:</td>
					<td>
						<textarea rows="3" class="typearea" name="storage[schedule]"><?php echo $this->data['schedule']; ?></textarea>
					</td>
				</tr>
				<tr>
					<td class = "name"><?php _e('Email','usam'); ?>:</td>
					<td>
						<input type="text" name="storage[email]" value="<?php echo $this->data['email']; ?>"/>
					</td>
				</tr>
				<tr>
					<td class = "name"><?php _e('GPS широта','usam'); ?>:</td>
					<td><input type="text" name="storage[GPS_N]" value="<?php echo $this->data['GPS_N']; ?>" size="15" />
					</td>
				</tr>
				<tr>
					<td class = "name"><?php _e('GPS долгота','usam'); ?>:</td>
					<td><input type="text" name="storage[GPS_S]" value="<?php echo $this->data['GPS_S']; ?>" size="15" />
					</td>

				</tr>
				<tr>
					<td class = "name"><?php _e('Код склада','usam'); ?>:</td>
					<td><input type="text" name="storage[code]" value="<?php echo $this->data['code']; ?>" size="45" />
					</td>

				</tr>
				<tr>
					<td class = "name"><?php _e('Сортировка','usam'); ?>:</td>
					<td class = "option">
						<input type="text" name="storage[sort]" value="<?php echo $this->data['sort']; ?>" size="5" />
					</td>
				</tr>
			</tbody>
		</table>
		<?php
    }		
}
?>