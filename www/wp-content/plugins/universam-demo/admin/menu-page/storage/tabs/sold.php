<?php
class USAM_Tab_sold extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Проданные товары', 'usam'), 'description' => 'Здесь Вы можете посмотреть проданные товары.' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}