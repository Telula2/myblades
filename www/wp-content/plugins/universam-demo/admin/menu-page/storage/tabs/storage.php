<?php
class USAM_Tab_Storage extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Ваши склады и магазины', 'usam'), 'description' => __('Здесь Вы можете добавить, удалять и изменять склады и магазины.','usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
		
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		global $wpdb;	
		switch( $this->current_action )
		{		
			case 'delete':						
				$ids = array_map( 'intval', $this->records );
				$in = implode( "', '", $ids );
				
				$wpdb->query( "DELETE FROM " . USAM_TABLE_STORAGE_LIST . " WHERE id IN ('$in')" );
					
				usam_recalculate_stock_products();				
				
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ),$this->sendback );				
			break;	
			case 'save':	
				if( isset($_REQUEST['storage']) )
				{				
					$this->data = $_REQUEST['storage'];			
					$this->data['title'] = sanitize_text_field(stripcslashes($_POST['name']));	
					$this->data['active'] = !empty($_POST['active'])?1:0;
					$this->data['location_id'] = !empty($_POST['location'])?absint($_POST['location']):0;			
					$this->data['img'] = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
					$this->data['description'] = sanitize_textarea_field(stripcslashes($_POST['description']));						
					if ( $this->id != null )	
					{
						usam_update_storage( $this->id, $this->data );
					}
					else
					{		
						$this->id = usam_insert_storage( $this->data );
					}
				}
			break;			
		}		
	}
}