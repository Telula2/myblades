<?php
class USAM_Tab_Inventory extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Инвентаризация', 'usam'), 'description' => 'Здесь Вы можете работать с остатками товаров' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}		
}