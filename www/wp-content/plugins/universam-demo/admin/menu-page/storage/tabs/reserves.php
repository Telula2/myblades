<?php
class USAM_Tab_reserves extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Зарезервированные', 'usam'), 'description' => 'Здесь Вы можете просматривать зарезервированные товары.' );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{	
		switch( $this->current_action )
		{		
			case 'delete':						
				foreach ( $this->records as $product_id )
				{
					usam_update_product_meta($product_id, 'reserve', 0 );						
				}
				foreach ( $this->records as $product_id )
				{				
					usam_recalculate_stock_product( $product_id );
				}	
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ),$this->sendback );
			break;			
		}	
	}	
}