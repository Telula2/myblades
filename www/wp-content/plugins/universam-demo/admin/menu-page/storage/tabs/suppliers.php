<?php
class USAM_Tab_Suppliers extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Web поставщики товара', 'usam'), 'description' => 'Здесь Вы можете просматривать и редактировать ссылки на сайты поставщиков, если вы занимаетесь перепродажей товара.' );		
	}
	
	protected function load_tab()
	{
		$this->product_list_table();
	}	
}