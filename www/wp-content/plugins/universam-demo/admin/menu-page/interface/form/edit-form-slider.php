<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Slider extends USAM_Edit_Form
{	
	private $slides = array();
	public function __construct()
	{       
		parent::__construct( );			
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_style' ) );				
	}	
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить слайдер %s','usam'), $this->data['name'] );
		else
			$title = __('Добавить слайдер', 'usam');	
		return $title;
	}
	
	public function get_data_tab()
	{  		
		if ( $this->id )	
		{
			$_slider = new USAM_Slider( $this->id );
			$this->slides = $_slider->get_slides();
			$this->data = $_slider->get_data(  );	
			usort($this->slides, function($a, $b){  return ($a->sort - $b->sort); });			
		}
		else
			$this->data = array( 'name' => '', 'active' => 0, 'type' => 'i', 'template' => '', 'setting' => array( 'show' => '', 'condition' => array(), 'autospeed' => 6000, 'button' => array( 'show' => 1, 'fon' => 'ffffff','fon_active' => 'ffffff', 'border_color' => 'ffffff' ), 'roles' => array(), 'sales_area' => array() ) );				
	}
	
	public function message_error()
	{  		
		return array();
	}	
	
	function display_left()
	{					
		$this->titlediv( $this->data['name'] );
		$this->add_box_status_active( $this->data['active'] );
		usam_add_box( 'usam_settings', __( 'Общие настройки', 'usam' ), array( $this, 'slider_settings' ) );
		usam_add_box( 'usam_slider_template', __( 'Шаблоны слайдера', 'usam' ), array( $this, 'slider_template' ) );
		usam_add_box( 'usam_slider_button', __( 'Настройки кнопок', 'usam' ), array( $this, 'slider_button' ) );
		usam_add_box( 'usam_conditions', __('Условия','usam'), array( $this, 'display_conditions' ) );
		$title = '<a class="change_field" href="#" title="'.__( 'Изменить', 'usam' ).'"><img src="'.USAM_CORE_IMAGES_URL.'/edit.png" width="14"></a>';		
		usam_add_box( 'usam_slides', __( 'Слайды', 'usam' ).$title, array( $this, 'slides' ) );	
	}	
	
	function scripts_and_style()
	{
		wp_enqueue_media( );				
	}
	
	public function display_conditions( )
	{   			
		$roles = !empty($this->data['setting']['condition']['roles'])?$this->data['setting']['condition']['roles']:array();
		$sales_area = !empty($this->data['setting']['condition']['sales_area'])?$this->data['setting']['condition']['sales_area']:array();
		?>
		<div class="container_column">			
			<div class="column1" id="rule-descrip">  
				<p><?php _e('Выберете условия отображения слайдера. Если ничего не выбрано, значит работает как выбрано всё.', 'usam'); ?></p>
			</div>
			<div id="all_taxonomy" class="all_taxonomy">						
				<?php $this->display_meta_box_group( 'roles', $roles ); ?>
				<?php $this->display_meta_box_group( 'sales_area', $sales_area ); ?>
			</div>	
		</div>
	   <?php
	}	
	
	function slider_settings()
	{		
		?>
		<table class="subtab-detail-content-table usam_edit_table" id="slider">
			<tbody>								
				<tr>
					<td><?php _e('Тип слайдера','usam'); ?>:</td>
					<td>
						<select name = "slider[type]" id = "slider_type">						
							<option value="I" <?php echo ($this->data['type'] == 'I') ?'selected="selected"':''?> ><?php _e( 'Изображений', 'usam' ); ?></option>
							<option value="P" <?php echo ($this->data['type'] == 'P') ?'selected="selected"':''?> ><?php _e( 'Товаров', 'usam' ); ?></option>
						</select>	
					</td>
				</tr>									
				<tr>
					<td><?php _e('Задержка','usam'); ?>:</td>				
					<td class = "autospeed">						
						<input type="text" name="slider[setting][autospeed]" maxlength ="6" value="<?php echo (int)$this->data['setting']['autospeed']; ?>">
					</td>	
				</tr>	
				<tr>
					<td><?php _e('Показать','usam'); ?>:</td>
					<td>
						<select name = "slider[setting][show]" id = "slider_show">						
							<option value="" <?php selected($this->data['setting']['show'], '') ?> ><?php _e( 'Вручную при помощи кода', 'usam' ); ?></option>						
							<option value="catalog_head" <?php selected($this->data['setting']['show'], 'catalog_head') ?> ><?php _e( 'В верхней части категорий', 'usam' ); ?></option>
						</select>	
					</td>
				</tr>					
			</tbody>
		</table>
		<?php
    }	
			
	function slider_template()
	{				
		usam_get_templates_display( 'slider', $this->data['template'] );
		?><input type="hidden" id ="slider_template" name="slider[template]" value="<?php echo $this->data['template']; ?>"><?php
    }

	function slider_button()
	{		
		if ( !empty($this->data['setting']['button']['show']) )
			$button_show = $this->data['setting']['button']['show'];
		else
			$button_show = 0;
		
		if ( !empty($this->data['setting']['button']['fon']) )
			$button_fon = $this->data['setting']['button']['fon'];
		else
			$button_fon = '';
		
		if ( !empty($this->data['setting']['button']['fon_active']) )
			$button_fon_active = $this->data['setting']['button']['fon_active'];
		else
			$button_fon_active = '';
		
		if ( !empty($this->data['setting']['button']['border_color']) )
			$button_border_color = $this->data['setting']['button']['border_color'];
		else
			$button_border_color = '';	
		?>
		<table class="subtab-detail-content-table usam_edit_table" id="slider">
			<tbody>					
				<tr>
					<td><?php _e('Показывать кнопки','usam'); ?>:</td>
					<td>
						<select name = "slider[setting][button][show]" id = "show_button">						
							<option value="1" <?php echo ($button_show == 1) ?'selected="selected"':''?> ><?php _e( 'Да', 'usam' ); ?></option>
							<option value="0" <?php echo ($button_show == 0) ?'selected="selected"':''?> ><?php _e( 'Нет', 'usam' ); ?></option>
						</select>	
					</td>
				</tr>				
				<tr>
					<td><?php _e('Цвет фона','usam'); ?>:</td>
					<td class = "fon">						
						<input type="text" name="slider[setting][button][fon]" class="usam_color background_color" size="6" maxlength="6" value="<?php echo $button_fon; ?>" id="usam_fon_background_color">	
					</td>	
				</tr>
				<tr>
					<td><?php _e('Цвет фона активной фотографии','usam'); ?>:</td>
					<td class = "fon">	
						<input type="text" name="slider[setting][button][fon_active]" class="usam_color background_color" size="6" maxlength="6" value="<?php echo $button_fon_active; ?>" id="usam_fon_background_color">
					</td>	
				</tr>
				<tr>
					<td><?php _e('Цвет рамки','usam'); ?>:</td>
					<td class = "fon">						
						<input type="text" name="slider[setting][button][border_color]" class="usam_color background_color" size="6" maxlength="6" value="<?php echo $button_border_color; ?>" id="usam_fon_background_color">						
					</td>	
				</tr>				
			</tbody>
		</table>
		<?php
    }	
	
	function slides()
	{				
		if ( $this->id != null )			
		{	 
			switch ( $this->data['type'] ) 
			{
				case 'I' : //Изображений
					$this->table_slides_images();
					?>	
					<div id="add-slide-button-box">			
						<button id='add-slide' type="button" data-type = "<?php echo $this->data['type']; ?>" class="button"><?php _e( 'Добавить слайд', 'usam' ); ?></button>			
					</div>		
					<?php	
				break;
				case 'P' :
					$this->table_slides_products();
				break;
				case 'A' :
					$this->table_slides_discounts();
				break;					
			}				
		}
		else
			_e('Сначала добавьте слайдер, чтобы можно было добавлять слайды', 'usam');
    }	

	public function table_slides_products()
    {
		$columns = array(
			'n'           => __( '№', 'usam' ),
			'image'       => '',
			'title'       => __( 'Имя', 'usam' ),
			'description' => __( 'Описание', 'usam' ),		
			'interval'    => __( 'Интервал', 'usam' ),				
			'sku'         => __( 'Артикул', 'usam' ),		 	 
			'action'      => '',
		);			
		$this->display_products_table( $this->slides, $columns );
		$this->add_product_button();		
	}
	
	protected function display_table_products( $products, $columns )
	{				
		if ( empty($products) )
		{
			?>
				<tr class = "items_empty"><td colspan = '<?php echo count($columns); ?>'><?php _e( 'Нет товаров', 'usam' ); ?></td></tr>
			<?php
		}
		else
		{				
			$i = 0;
			$this->total = 0;			
			foreach ( $products as $key => &$product ) 
			{							
				$post = get_post( $product->object_id );						
				$i++;				
				?>
				<tr id ="product_<?php echo $post->ID; ?>" data-product_id="<?php echo $post->ID; ?>" >					
					<?php
					foreach ( $columns as $column => $name ) 
					{						
						$data = '';					
						switch ( $column ) 
						{
							case 'n' :
								$data = $i;
							break;							
							case 'image' :
								$data = usam_get_product_thumbnail( $post->ID );
							break;
							case 'title' :
								$url = admin_url('post.php?post='.$post->ID.'&action=edit');
								$data = "<a id = 'details_product' href='$url'>".$post->post_title."</a>";								
							break;
							case 'interval' :
								$interval_from = $interval_to = '';
								$date_format = get_option( 'date_format', 'Y/m/d' );
								if ( !empty($product->interval_from) )
									$interval_from = __('с','usam')." ".usam_local_date( $product->interval_from, $date_format );
								if ( !empty($product->interval_to) )
									$interval_to = __('по','usam')." ".usam_local_date( $product->interval_to, $date_format );
								
								$data = "<span class = 'usam_display_data'>$interval_from $interval_to</span>
								<span class = 'usam_edit_data'>".usam_display_date_interval( $product->interval_from, $product->interval_to, $product->id, false )."</span>";
							break;
							case 'sku' :
								$data = usam_get_product_meta( $post->ID, 'sku', true );
							break;
							case 'description' :
								$data = '<span class = "usam_display_data">'.$product->description.'</span>
								<textarea rows="5" disabled = "disabled" type="text" name="_slide_edit['.$key.'][description]" class = "usam_edit_data">'.$product->description.'</textarea>';
							break;											
							case 'action' :	
								$data = "<a class='button_delete'></a>";
							break;
						}
						echo "<td class = 'product_$column'>$data</td>";
					}					
					?>						
				</tr>
				<input type="hidden" name="_slide_edit[<?php echo $key; ?>][id]" value="<?php echo $product->id; ?>">								
				<?php										
			}
		}		
	}	

	
	
	public function table_slides_discounts()
    {
		
		
	}

	public function table_slides_images()
    {			
		?>
		<table id = "slides_table" class="widefat" cellspacing="0">
			<thead>
				<tr>							
					<th class ="image"><?php _e( 'Изображение', 'usam' ); ?></th>	
					<th class ="title"><?php _e( 'Название', 'usam' ); ?></th>							
					<th class ="description"><?php _e( 'Описание', 'usam' ); ?></th>										
					<th class ="interval"><?php _e( 'Интервал', 'usam' ); ?></th>
					<th class ="link"><?php _e( 'Ссылка', 'usam' ); ?></th>					
					<th class ="fon"><?php _e( 'Цвет фона', 'usam' ); ?></th>	
					<th class ="sort"><?php _e( 'Сортировка', 'usam' ); ?></th>					
					<th class ="action"></th>
				</tr>
			</thead>
			<tbody>
				<?php				
				$w = get_option('thumbnail_size_w');
				$h = get_option('thumbnail_size_h');
				if ( empty($this->slides) )
				{
					?>
						<tr id="items_empty"><td colspan = '7'><?php _e( 'Нет слайдов', 'usam' ); ?> </td></tr>
					<?php	
				}
				else
				{							
					foreach($this->slides as $key => $slide)
					{			
						$img = image_get_intermediate_size( $slide->object_id, 'thumbnail' );								
						?>						
						<tr id = "slide_<?php echo $slide->id; ?>">
							<td class ="image"><img src="<?php echo $img['url']; ?>" alt=""></td>
							<td class ="title"><span class = "usam_display_data"><?php echo $slide->title; ?></span><input disabled = "disabled" type="text" name="_slide_edit[<?php echo $key; ?>][title]" class = "usam_edit_data" maxlength ="60" value="<?php echo $slide->title; ?>"/></td>
							<td class = "description">
								<span class = "usam_display_data"><?php echo $slide->description; ?></span>
								<textarea rows="5" disabled = "disabled" type="text" name="_slide_edit[<?php echo $key; ?>][description]" class = "usam_edit_data"><?php echo $slide->description; ?></textarea>
							</td>	
							<td class = "interval">
								<span class = "usam_display_data">
								<?php 
								$interval_from = $interval_to = '';
								$date_format = get_option( 'date_format', 'Y/m/d' );
								if ( !empty($slide->interval_from) )
									$interval_from = __('с','usam')." ".usam_local_date( $slide->interval_from, $date_format ).' ';
								if ( !empty($slide->interval_to) )
									$interval_to = __('по','usam')." ".usam_local_date( $slide->interval_to, $date_format );
								echo $interval_from . $interval_to;
								?></span>
								<span class = "usam_edit_data"><?php usam_display_date_interval( $slide->interval_from, $slide->interval_to, $slide->id ); ?></span>
							</td>
							<td class = "link">
								<span class = "usam_display_data"><a href="<?php echo $slide->link; ?>" target="_blank"><?php echo $slide->link; ?></a></span>
								<input disabled = "disabled" type="text" name="_slide_edit[<?php echo $key; ?>][link]" class = "usam_edit_data" maxlength ="255" value="<?php echo $slide->link; ?>"/>
							</td>
							<td class = "fon">
								<span class = "usam_display_data"><?php echo $slide->fon; ?></span>
								<span class = "usam_edit_data">
									<input disabled = "disabled" type="text" name="_slide_edit[<?php echo $key; ?>][fon]" class="usam_color background_color" size="7" maxlength="7" value="<?php echo $slide->fon; ?>" id="usam_fon_background_color"/>
								</span>
							</td>								
							<td class = "sort">
								<span class = "usam_display_data"><?php echo $slide->sort; ?></span>
								<input disabled = "disabled" type="text" name="_slide_edit[<?php echo $key; ?>][sort]" class = "usam_edit_data" maxlength ="2" size = '3' value="<?php echo $slide->sort; ?>" autocomplete="off" />
							</td>							
							<td class = "action">
								<input type="hidden" name="_slide_edit[<?php echo $key; ?>][id]" value="<?php echo $slide->id; ?>"/>								
								<a class='button_delete'></a>					
							</td>							
						</tr>	
						<?php					
					} 
				}
				?>							
			</tbody>
			<tfoot>	
				<tr id = "new_slide">
					<td class ="image"><input disabled = "disabled" type="hidden" name="_slide_new[object_id][]" id ="slide_new_image_id" value=""><img width="<?php echo $w; ?>" height="<?php echo $h; ?>" src="" alt=""></td>
					<td class ="title"><input disabled = "disabled" type="text" name="_slide_new[title][]" class = "usam_edit_data" maxlength ="60" value=""/></td>
					<td class = "description"><textarea rows="5" type="text" name="_slide_new[description][]" class = "usam_edit_data" disabled = "disabled"></textarea></td>	
					<td class ="interval"></td>
					<td class ="link"><input disabled = "disabled" type="text" name="_slide_new[link][]" class = "usam_edit_data" maxlength ="255" value=""/></td>
					<td class ="fon"><input disabled = "disabled" type="text" name="_slide_new[fon][]" class = "usam_edit_data" maxlength ="7" value=""/></td>
					<td class ="sort"><input disabled = "disabled" type="text" name="_slide_new[sort][]" class = "usam_edit_data" maxlength ="2" size = '3' value="" autocomplete="off" /></td>
					<td class = "action">					
						<a class='button_delete'></a>
					</td>
				</tr>				
			</tfoot>
		</table>
		<?php		
	}
}
?>