<?php
/*
 * Отображение страницы работа с меню товаров
 */

$default_tabs = array(
	array( 'id' => 'menu',  'title' => __( 'Сортировка категорий', 'usam' ) ),
	array( 'id' => 'slider',  'title' => __( 'Слайдер', 'usam' ) ),
//	array( 'id' => 'dpgroups',  'title' => __( 'Группы товаров', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{		
	protected function localize_script_tab()
	{
		return array(			
			'id'              => isset($_GET['id'])?$_GET['id']:0,				
		);
	}
} 
?>