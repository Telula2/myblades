<?php
class USAM_Tab_Slider extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Слайдер', 'usam'), 'description' => __('Здесь вы можете настроить слайдер.', 'usam' ) );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}
	
	public function add_meta_options_help_center_tabs( ) 
	{
		$tabs[] = new USAM_Help_Center_Item('basic-help', __('Подключение', 'usam'), __('Подключение', 'usam'), array( 'content' => '<p>' .__( 'Добавьте в php файл код', 'usam' ) . ':<br /><code>&lt;?php usam_slider(1); ?&gt;</code></p>',) );
		return $tabs;
	}
	
	protected function load_tab()
	{
		$this->product_list_table();
	}
		
	protected function callback_submit()
	{		
		global $wpdb;
		switch( $this->current_action )
		{		
			case 'delete':		
				foreach ( $this->records as $key => $id )
				{
					$wpdb->query("DELETE FROM `".USAM_TABLE_SLIDER."` WHERE `id`={$id} LIMIT 1");								
				}
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;
			case 'save':
				if( isset($_REQUEST['slider']) )
				{
					$slider = $_REQUEST['slider'];	
					$slider['name'] = sanitize_text_field( stripslashes($_POST['name']));	
					$slider['active'] = !empty($_POST['active'])?1:0;			
					$slider['setting']['condition']['roles']	= isset($_REQUEST['input-roles'])?stripslashes_deep($_REQUEST['input-roles']):array();
					$slider['setting']['condition']['sales_area'] = isset($_REQUEST['input-sales_area'])?stripslashes_deep($_REQUEST['input-sales_area']):array();
					$slider['setting']['autospeed']	= !empty($slider['setting']['autospeed'])?(int)$slider['setting']['autospeed']:6000;	
					
					$newslides = isset($_REQUEST['_slide_edit'])?stripslashes_deep($_REQUEST['_slide_edit']):array();
										
					if ( empty($slider['template']) )
					{
						$list = usam_get_templates( 'slidertemplate' );	
						$slider['template'] = key($list);
					}
					if ( $this->id != null )	
					{		
						switch ( $slider['type'] ) 
						{
							case 'I' : //Изображений					
								if ( isset($newslides['new']) )
									unset($newslides['new']);			
							break;
							case 'P' :
								$products = isset($_REQUEST['new_products'])?stripslashes_deep($_REQUEST['new_products']):array();	
								$newslides = array_merge($newslides, $products);	
							break;				
							case 'A' :
								$newslides = isset($_REQUEST['new_discounts'])?stripslashes_deep($_REQUEST['new_discounts']):array();	
							break;					
						}	
						if ( !empty($newslides) )
						{
							foreach( $newslides as $key => $slide )
							{			
								if ( count($slide) > 3 )
								{
									$newslides[$key]['interval_from'] = usam_get_datepicker('from_'.$slide['id'] );
									$newslides[$key]['interval_to'] = usam_get_datepicker('to_'.$slide['id'] );	
								}
							}
						}						
						$_slider = new USAM_Slider( $this->id );	
						$slides = $_slider->get_slides();
						
						$_slider->set( $slider );			
						$_slider->save();			
						$_slider->save_slides( $newslides );			
						
						if ( !empty($newslides) )
						{ // Удалить слайды	
							$slides_bd = array();			
							foreach( $slides as $slide)
							{
								$slides_bd[] = $slide->id;
							}		
							$slides_id = array();
							foreach( $newslides as $slide )
							{
								$slides_id[] = $slide['id'];
							}					
							$result = array_diff($slides_bd, $slides_id);
							if ( !empty($result) )
							{
								foreach($result as $slide_id)
								{
									usam_delete_slides( $slide_id );
								}
							}
						}
						else
						{			
							foreach($slides as $key => $slide)
							{
								usam_delete_slides( $slide->id );
							}
						}
						
						if ( !empty($_REQUEST['_slide_new']) )
						{				
							foreach( $_REQUEST['_slide_new']['object_id'] as $key => $img_id )
							{ 			
								if ( empty($img_id) )
									continue;
								
								$slide = array( 'object_id' => absint($img_id) );
								
								$parents = array( 'title', 'description', 'link', 'fon', 'sort' );
								foreach( $parents as $parent )
								{
									if ( !empty($_REQUEST['_slide_new'][$parent][$key]) )
									{
										if ( $key == 'description')
											$slide[$parent] = sanitize_textarea_field($_REQUEST['_slide_new'][$parent][$key]);
										else
											$slide[$parent] = sanitize_text_field($_REQUEST['_slide_new'][$parent][$key]);
									}
								}										
								$id = $_slider->insert_slides( $slide );	
							}
						}
					}
					else
					{
						$slider = new USAM_Slider( $slider );				
						$slider->save();
						$slider->save_slides( $newslides );	
								
						$this->id = $slider->get( 'id' );	
					}
				}
			break;
		}	
	}	
}