<?php
class USAM_Tab_menu extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Сортировка категорий товаров', 'usam'), 'description' => 'Здесь вы можете сортировать категории товаров. Сортировка будет учитываться в меню категорий и витжета сайтбара.' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}