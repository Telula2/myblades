<?php
class USAM_List_Table_Menu extends USAM_List_Table 
{			
    function __construct( $args = array() )
	{	
		parent::__construct( $args );
	}	
	
	function column_name( $item ) 
	{		
		if ( !empty($item->term_id) )
		{
			$category_children = get_option('usam-category_children');
			$keys = array_keys($category_children);
			if ( !in_array($item->term_id, $keys) )
				echo $item->name;
			else
			{
				?>
				<a title="<?php esc_attr_e( 'Открыть подменю', 'usam' ); ?>" href = "<?php echo $this->get_link( $item->term_id ); ?>">
					<?php echo $item->name ?>
				</a>
				<?php
			}
		}
		else
			echo $item->name;
	}	

	private function get_link( $term_id )
	{		
		return esc_attr( '?page=interface&tab=menu&submenu='.$term_id );
	}

	function get_columns()
	{
        $columns = array(    
			'name'     => __( 'Имя', 'usam' ),
			'slug'     => __( 'Ярлык', 'usam' ),			
			'drag'     => '&nbsp;',
        );
        return $columns;
    }
	
	function column_drag( $item )
	{		
		if ( empty($item->term_id) )
			return;
		?>
		<div id ="dragging" class="ui-sortable-handle" data-id='<?php echo $item->term_id; ?>'>
			<a title="<?php esc_attr_e( 'Нажмите и перетащите, чтобы поменять местами', 'usam' ); ?>">
				<img src="<?php echo esc_url( USAM_CORE_IMAGES_URL . '/drag.png' ); ?>" />
			</a>		
		</div>
		<?php	
	}	
	
	function prepare_items() 
	{	
		$this->_column_headers = $this->get_column_info();
		$current_page = $this->get_pagenum();	
		
		$default_menu_category   = get_option( 'usam_default_menu_category' );
		if ( isset($_GET['submenu']))
		{
			$current_default = absint($_GET['submenu']);			
			$ancestors = get_ancestors( $current_default, 'usam-category' );	

			if ( !empty($ancestors) && in_array($default_menu_category, $ancestors) )
			{	
				$up->name = '<a title="'.esc_attr__( 'Вверх', 'usam' ).'" href = "'.$this->get_link( $ancestors[0] ).'">...</a>';			
				$this->items[] = $up;
			}
		}	
		else
			$current_default = $default_menu_category;
		
		$terms = get_terms( 'usam-category', "parent=$current_default&update_term_meta_cache=0&orderby=meta_value_num&meta_key=usam_sorting_menu_categories" );	
		if ( empty($terms)	)
			$terms = get_terms( 'usam-category', "parent=$current_default&update_term_meta_cache=0" );	
		
		
	/*	$terms1 = get_terms( 'usam-category', "update_term_meta_cache=0" );	
		foreach ( $terms1 as $term )
		{
			$sort = get_term_meta( $term->term_id, 'usam_sorting_menu_categories', true );		
			if ( $sort === '' )
				update_term_meta( $term->term_id, 'usam_sorting_menu_categories', 0 );
		}		*/
		if ( !empty($this->items) )
			foreach ( $terms as $term )
				$this->items[] = $term;
		else
			$this->items = $terms;
				
		$total_items = count( $this->items );
		
		$this->items = array_slice( $this->items, ($current_page - 1)*$this->per_page ,$this->per_page);
		$this->set_pagination_args( array(	'total_items' => $total_items, 'per_page'    => $this->per_page ) );		
	}
}