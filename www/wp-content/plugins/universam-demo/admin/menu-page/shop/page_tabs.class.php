<?php
/*
 * Отображение страницы "Инструменты магазина"
 */
$default_tabs = array(
	array( 'id' => 'general',  'title' => __( 'Общие состояние', 'usam' ) ),
	array( 'id' => 'update',  'title' => __( 'Обновление', 'usam' ) ),
	array( 'id' => 'debug',  'title' => __( 'Отладка', 'usam' ) ), 
	array( 'id' => 'nuke',  'title' => __( 'Удаление', 'usam' ) ),
	array( 'id' => 'tools',  'title' => __( 'Инструменты', 'usam' ) ),
//	array( 'id' => 'php',  'title' => __( 'PHP запрос', 'usam' ) ),
//	array( 'id' => 'sql',  'title' => __( 'Таблицы в БД', 'usam' ) ),
	array( 'id' => 'backup',  'title' => __( 'Резервирование', 'usam' ) ), 
	array( 'id' => 'log',  'title' => __( 'Логи', 'usam' ) ),
);


class USAM_Tab extends USAM_Page_Tab
{		
	
} 
