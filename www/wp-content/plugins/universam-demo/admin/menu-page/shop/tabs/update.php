<?php
class USAM_Tab_Update extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Обновление программного комплекса УНИВЕРСАМ', 'usam'), 'description' => __( 'Настоятельно рекомендуется создать резервную копию базы данных перед выполнением обновления.', 'usam' ) );
	}
	
	public function display() 
	{		
		if ( usam_needs_upgrade()  )
		{ 	
			if ( !empty($_REQUEST['start_update']) )
			{ 
				if ( $this->verify_nonce() )
				{
					ob_implicit_flush( true );
					
					echo esc_html__( 'Обновление...', 'usam' ) . '<br /><br />';	
					
					USAM_Update::start_update();

					echo esc_html__( 'УНИВЕРСАМ успешно обновлен!', 'usam' );			
					ob_implicit_flush( false );
				}
			}
			else
			{ 							
				?>
				<em><?php esc_html_e( 'Примечание: Если сервер прекратит работу или не хватает памяти, просто обновите эту страницу и сервер начнет работу там, где он остановился.', 'usam' ); ?></em>
				<br />
				<form action="" method="post" id="setup">
					<input type="hidden" name="start_update" value="1" />						
					<?php $this->nonce_field('start_update_nonce');  ?>								
					<p class="step">
						<input type="submit" class="button" value="<?php esc_attr_e( 'Обновление УНИВЕРСАМа', 'usam' ); ?>" name="submit">
					</p>
				</form>
			<?php
			}
		}
		else
		{
			?><h2><?php esc_html_e( 'Вы используете самую последнюю версию магазина.', 'usam' ); ?></h2><?php			
		}
			
	}
}