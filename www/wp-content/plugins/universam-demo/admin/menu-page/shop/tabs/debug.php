<?php
class USAM_Tab_Debug extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Отладка УНИВЕРСАМа', 'usam'), 'description' => 'Здесь Вы можете включать вывод разной служебной информации.' );	
	}
	
	protected function callback_submit()
    {	
		if (isset($_POST['debug']))
		{			
			update_option( 'usam_debug', $_POST['debug'] );
			update_option( 'usam_log', $_POST['log'] );			
			$this->sendback = add_query_arg( array( 'update' => '1' ), $this->sendback );			
			$this->redirect = true;
		}
		// Код для включения или отключения страницы отладок
		if ( !empty( $_POST['usam_activate_debug']) && ! $_SESSION['usam_activate_debug'] )
			$_SESSION['usam_activate_debug'] = true;
		else
			$_SESSION['usam_activate_debug'] = false;
		
		if ( isset( $_POST['usam_stand_service'] ) )
		{
			if ( get_option('usam_stand_service' ) )
				update_option('usam_stand_service', 0 );
			else
				update_option('usam_stand_service', 1 );	
		}
	}
	
	public function display_log() 
	{
		?>		
		<table class ="log_service_information">					
			<tr>
				<td><?php esc_html_e( 'Оплата заказов', 'usam' ); ?><td>
				<td>
					<input type="hidden" name="log[submit_checkout]" value="0">
					<input type='checkbox' value='1' name='log[submit_checkout]' <?php echo !empty($log['submit_checkout']) ?"checked='checked'":'' ?>/>
				<td>
				<td><?php esc_html_e( 'Записывать, когда пользователь завершает оформление заказа', 'usam' ); ?><td>
			</tr>					
		</table>
		<?php
	}
	
	public function display_service_information() 
	{
		$data_default = array( 'location' => 0, 'display' => array( 'sql' => 0, 'globals' => 0  ) );
		$debug = get_option( 'usam_debug', array() );
		$debug = array_merge($data_default, $debug);
		?>
		<form method='POST' action='' id='usam-tab_form'>
			<?php $this->nonce_field(); ?>
			<table class ="table_service_information">
				<tr>
					<td><?php _e( 'Отображение', 'usam' ); ?><td>
					<td>
						<input type="radio" name="debug[location]" value="all" <?php if ( $debug['location'] == 'all' ) { echo "checked='checked'"; } ?> /><?php _e('Везде', 'usam'); ?>&nbsp;
						<input type="radio" name="debug[location]" value="admin" <?php if ( $debug['location'] == 'admin' ) { echo "checked='checked'"; } ?> /><?php _e('В адинке', 'usam'); ?>&nbsp;		
						<input type="radio" name="debug[location]" value="site" <?php if ( $debug['location'] == 'site' ) { echo "checked='checked'"; } ?> /><?php _e('На сайте', 'usam'); ?>&nbsp;
						<input type="radio" name="debug[location]" value="0" <?php if ( $debug['location'] == '0' ) { echo "checked='checked'"; } ?> /><?php _e('Нигде', 'usam'); ?><br />	
					<td>
					<td><?php esc_html_e( 'Укажите где показывать служебную информацию.', 'usam' ); ?><td>
				</tr>
				<tr>
					<td><?php esc_html_e( 'SQL запросы', 'usam' ); ?><td>
					<td>
						<input type="hidden" name="debug[display][sql]" value="0">
						<input type='checkbox' value='1' name='debug[display][sql]' <?php echo $debug['display']['sql'] == 1 ?"checked='checked'":'' ?>/>
					<td>
					<td><?php esc_html_e( 'Вывести все SQL запросы', 'usam' ); ?><td>
				</tr>
				<tr>
					<td><?php esc_html_e( 'GLOBALS', 'usam' ); ?><td>
					<td>
						<input type="hidden" name="debug[display][globals]" value="0">
						<input type='checkbox' value='1' name='debug[display][globals]' <?php echo $debug['display']['globals'] == 1 ?"checked='checked'":'' ?>/>
					<td>
					<td><?php esc_html_e( 'Вывести содержимое GLOBALS', 'usam' ); ?><td>
				</tr>	
			</table>				
			<input type="submit" class="button" value="<?php esc_attr_e( 'Сохранить', 'usam' ); ?>" name="submit">
		</form>
		<?php
	}
	
	public function display() 
	{							
		?>	
		<form method='POST' action='' id='usam-tab_form'>
			<?php $this->nonce_field(); ?>	
			<table class ="stand_service">
				<tr>
					<td>										
						<?php if ( get_option('usam_stand_service', 0 ) == 0 ) { ?>							
							<input type="submit" class="button button-primary" value="<?php esc_attr_e( 'Встать на обслуживание', 'usam' ); ?>" name="usam_stand_service">
						<?php } else { ?>							
							<input type="submit" class="button" value="<?php esc_attr_e( 'Включить нормальную работу', 'usam' ); ?>" name="usam_stand_service">
						<?php } ?>
						<?php $this->nonce_field(); ?>
					</td>
					<td>
						<?php _e( 'Сайт будет закрыт для всех, кроме администраторов сайта.', 'usam' ); ?>
					</td>
				</tr>
				<tr>
					<td>			
						<?php if ( (isset($_SESSION['usam_activate_debug']) && !$_SESSION['usam_activate_debug']) || !isset($_SESSION['usam_activate_debug']) ) { ?>
							<input type="submit" class="button button-primary" value="<?php esc_attr_e( 'Включить отладку', 'usam' ); ?>" name="usam_activate_debug">
						<?php } else { ?>
							<input type="submit" class="button" value="<?php esc_attr_e( 'Отключить отладку', 'usam' ); ?>" name="usam_activate_debug">
						<?php } ?>
						<?php $this->nonce_field(); ?>
					</td>
					<td>
						<?php _e( 'Будет включена отладка сайта, и будет доступна техническая информация о текущей работе сайта.', 'usam' ); ?>
					</td>
				<tr>
			</table>			
		</form>
		<?php
		usam_add_box( 'usam_service_information', __('Вывод служебной информации','usam'), array( $this, 'display_service_information' ) );	
	//	usam_add_box( 'usam_log', __('Вести логи','usam'), array( $this, 'display_log' ) );
	}
}