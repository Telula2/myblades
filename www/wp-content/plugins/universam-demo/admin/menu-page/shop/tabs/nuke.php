<?php
class USAM_Tab_nuke extends USAM_Tab
{	
	public function __construct()
	{
		$this->header = array( 'title' => __('Удаление элементов магазина', 'usam'), 'description' => __( 'Выберите таблицу, которую вы хотите очистить и нажмите кнопку Удалить, чтобы окончательно удалить данные из базы данных.', 'usam' ) );
	}
	
	public function display_tables() 
	{
		$products = usam_return_details( 'products' );
		$variations = usam_return_details( 'variations' );
		$images = usam_return_details( 'images' );
		$files = usam_return_details( 'files' );		
		$categories = usam_return_details( 'categories' );
		
		
		
		$tags = usam_return_details( 'tags' );
		$orders = usam_return_details( 'orders' );
		$coupons = usam_return_details( 'coupons' );
		?>
		<p class="description"><?php _e( 'Навсегда удалить детали Универсама.', 'usam' ); ?></p>
		<p><a href="javascript:void(0)" id="usam-checkall"><?php _e( 'Выбрать все', 'usam' ); ?></a> | <a href="javascript:void(0)" id="usam-uncheckall"><?php _e( 'Снять отметку со всего', 'usam' ); ?></a></p>
		<table class="form-table">
			<tr>
				<th><label for="products"><?php _e( 'Товары', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="products" name="usam_products"<?php echo disabled( $products, 0 ); ?> /> (<?php echo $products; ?>)</td>
			</tr>
			<tr>
				<th><label for="product_variations"><?php _e( 'Вариации товаров', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="product_variations" name="usam_product_variations"<?php echo disabled( $variations, 0 ); ?> /> (<?php echo $variations; ?>)</td>
			</tr>
			<tr>
				<th><label for="product_images"><?php _e( 'Изображения товаров', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="product_images" name="usam_product_images"<?php echo disabled( $images, 0 ); ?> /> (<?php echo $images; ?>)</td>
			</tr>

			<tr>
				<th><label for="product_files"><?php _e( 'Файлы товаров', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="product_files" name="usam_product_files"<?php echo disabled( $files, 0 ); ?> /> (<?php echo $files; ?>)</td>
			</tr>

			<tr>
				<th><label for="product_tags"><?php _e( 'Теги товаров', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="product_tags" name="usam_product_tags"<?php echo disabled( $tags, 0 ); ?> /> (<?php echo $tags; ?>)</td>
			</tr>

			<tr>
				<th><label for="product_categories"><?php _e( 'Категории товаров', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="product_categories" name="usam_product_categories"<?php echo disabled( $categories, 0 ); ?> /> (<?php echo $categories; ?>)</td>
			</tr>
			<tr>
				<th><label for="sales_orders"><?php _e( 'Заказы', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="sales_orders" name="usam_sales_orders"<?php echo disabled( $orders, 0 ); ?> /> (<?php echo $orders; ?>)</td>
			</tr>
			<tr>
				<th><label for="coupons"><?php _e( 'Купоны', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="coupons" name="usam_coupons"<?php echo disabled( $coupons, 0 ); ?> /> (<?php echo $coupons; ?>)</td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" value="<?php _e( 'Удалить', 'usam' ); ?>" class="button-primary" />
		</p>
		<?php
	}
	
	
	public function display_remove_wordpress_data() 
	{
		$posts = usam_return_details( 'posts' );
		$post_categories = usam_return_details( 'post_categories' );
		$post_tags = usam_return_details( 'post_tags' );
		$links = usam_return_details( 'links' );
		$comments = usam_return_details( 'comments' );			
		?>
		<p class="description"><?php _e( 'Навсегда удалить данные WordPress.', 'usam' ); ?></p>
		<p><a href="javascript:void(0)" id="wordpress-checkall"><?php _e( 'Отметить всё', 'usam' ); ?></a> | <a href="javascript:void(0)" id="wordpress-uncheckall"><?php _e( 'Снять со всего', 'usam' ); ?></a></p>
		<table class="form-table">
			<tr>
				<th><label for="posts"><?php _e( 'Записи', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="posts" name="usam_posts"<?php echo disabled( $posts, 0 ); ?> /> (<?php echo $posts; ?>)</td>
			</tr>
			<tr>
				<th><label for="post_categories"><?php _e( 'Категории', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="post_categories" name="usam_post_categories"<?php echo disabled( $post_categories, 0 ); ?> /> (<?php echo $post_categories; ?>)</td>
			</tr>
			<tr>
				<th><label for="post_tags"><?php _e( 'Теги', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="post_tags" name="usam_post_tags"<?php echo disabled( $post_tags, 0 ); ?> /> (<?php echo $post_tags; ?>)</td>
			</tr>

			<tr>
				<th><label for="links"><?php _e( 'Ссылки', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="links" name="usam_links"<?php echo disabled( $links, 0 ); ?> /> (<?php echo $links; ?>)</td>
			</tr>
			<tr>
				<th><label for="comments"><?php _e( 'Комментарии', 'usam' ); ?></label></th>
				<td><input type="checkbox" id="links" name="usam_comments"<?php echo disabled( $comments, 0 ); ?> /> (<?php echo $comments; ?>)</td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" value="<?php _e( 'Удалить', 'usam' ); ?>" class="button-primary" />
		</p>
		<?php
	}
		
	public function display() 
	{			
		$preview_files = usam_return_details( 'preview-files' );
		?>			
		<form method="post" onsubmit="showProgress()">
			<?php 
			$this->nonce_field(); 
			usam_add_box( 'usam_tables', __('Очистить таблицы от данных Универсама','usam'), array( $this, 'display_tables' ) );	
			usam_add_box( 'usam_remove_wordpress_data', __('Очистить таблицы WordPress','usam'), array( $this, 'display_remove_wordpress_data' ) );				
			?>
			<input type="hidden" name="action" value="nuke" />
		</form>
		<?php		
	}
	
	protected function callback_submit( ) 
	{
		if( isset( $_POST['usam_products'] ) )
			$this->clear_dataset( 'products' );
		if( isset( $_POST['usam_product_variations'] ) )
			$this->clear_dataset( 'variations' );
		if( isset( $_POST['usam_product_tags'] ) )
			$this->clear_dataset( 'tags' );
		if( isset( $_POST['usam_the_cart_item'] ) ) {
			$categories = $_POST['usam_the_cart_item'];
			$this->clear_dataset( 'categories', $categories );
		} else if( isset( $_POST['usam_product_categories'] ) ) {
			$this->clear_dataset( 'categories' );
		}
		if( isset( $_POST['usam_product_images'] ) )
			$this->clear_dataset( 'images' );
		if( isset( $_POST['usam_product_files'] ) )
			$this->clear_dataset( 'files' );
		if( isset( $_POST['usam_sales_orders'] ) )
			$this->clear_dataset( 'orders' );
		if( isset( $_POST['usam_coupons'] ) )
			$this->clear_dataset( 'coupons' );		

		// WordPress
		if( isset( $_POST['usam_posts'] ) )
			$this->clear_dataset( 'posts' );
		if( isset( $_POST['usam_post_categories'] ) )
			$this->clear_dataset( 'post_categories' );
		if( isset( $_POST['usam_post_tags'] ) )
			$this->clear_dataset( 'post_tags' );
		if( isset( $_POST['usam_links'] ) )
			$this->clear_dataset( 'links' );
		if( isset( $_POST['usam_comments'] ) )
			$this->clear_dataset( 'comments' );				
	}	
	
	function clear_dataset( $dataset, $data = null ) 
	{
		global $wpdb;
		switch( $dataset )
		{	
			case 'products':
				$post_type = 'usam-product';
				$products = (array)get_posts( array(
					'post_type' => $post_type,		
					'numberposts' => -1
				) );
				if( $products ) {
					foreach( $products as $product ) {
						if( $product->ID )
							wp_delete_post( $product->ID, true );
					}
				}
			break;
			case 'variations':
				// Products
				$post_type = 'usam-product';
				$variations_sql = "SELECT `ID` FROM `" . $wpdb->posts . "` WHERE `post_type` = '" . $post_type . "' AND `post_parent` <> 0";
				$variations = $wpdb->get_results( $variations_sql );
				if( $variations ) {
					foreach( $variations as $variation ) {
						if( $variation->ID )
							wp_delete_post( $variation->ID, true );
					}
				}
				// Terms
				$term_taxonomy = 'usam-variation';
				$variations = get_terms( $term_taxonomy, array( 'hide_empty' => false ) );
				if( $variations ) {
					foreach( $variations as $variation ) {
						if( $variation->term_id ) {
							wp_delete_term( $variation->term_id, $term_taxonomy );
							$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->terms . "` WHERE `term_id` = %d", $variation->term_id ) );
						}
					}
				}
				delete_option( 'usam-variation_children' );
				break;
			case 'categories':
				$term_taxonomy = 'usam-category';
				if( $data )
				{
					foreach( $data as $single_category ) 
					{
						$post_type = 'usam-product';
						$args = array(
							'post_type' => $post_type,
							'tax_query' => array(
								array(
									'taxonomy' => $term_taxonomy,
									'field' => 'id',
									'terms' => $single_category
								)
							),
							'numberposts' => -1
						);
						$products = get_posts( $args );
						if( $products ) {
							foreach( $products as $product ) 
							{							
								if( $product->ID )
								{							
									$categories = wp_get_object_terms( $product->ID , 'usam-category', array( 'fields' => 'ids' ));
									foreach( $categories as $key => $value ) 
									{
										if ( $single_category == $value )
										{
											unset( $categories[$key] );
											break;
										}									
									}	
									wp_set_post_terms( $product->ID, $categories, 'usam-category', false );
									
								//	wp_delete_object_term_relationships ($product->ID, $term_taxonomy); 
									//wp_delete_post( $product->ID, true );
									
								}
							}
						}
					}
				} 
				else 
				{
					/*$categories = get_terms( $term_taxonomy, array( 'hide_empty' => false ) );
					if( $categories )
					{
						foreach( $categories as $category ) {
							if( $category->term_id ) {
								wp_delete_term( $category->term_id, $term_taxonomy );
								$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->terms . "` WHERE `term_id` = %d", $category->term_id ) );
							}
							if( $category->term_taxonomy_id )
								$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_relationships . "` WHERE `term_taxonomy_id` = %d", $category->term_taxonomy_id ) );
						}
					}*/				
					$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_taxonomy . "` WHERE `taxonomy` = '%s'", $term_taxonomy ) );
				}
				break;

			case 'tags':
				$term_taxonomy = 'product_tag';
				$tags = get_terms( $term_taxonomy, array( 'hide_empty' => false ) );
				if( $tags ) {
					foreach( $tags as $tag ) {
						if( $tag->term_id ) {
							wp_delete_term( $tag->term_id, $term_taxonomy );
							$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->terms . "` WHERE `term_id` = %d", $tag->term_id ) );
						}
					}
				}
				break;

			case 'images':
				$post_type = 'usam-product';
				$products = (array)get_posts( array(
					'post_type' => $post_type,					
					'numberposts' => -1
				) );
				if( $products ) {
					$upload_dir = wp_upload_dir();
					foreach( $products as $product ) {
						$args = array(
							'post_type' => 'attachment',
							'post_parent' => $product->ID,
							'post_status' => 'inherit',
							'post_mime_type' => 'image',
							'numberposts' => -1
						);
						$images = get_children( $args );
						if( $images ) {
							// $intermediate_sizes = usam_intermediate_image_sizes_advanced( $intermediate_sizes );
							foreach( $images as $image ) {
								wp_delete_attachment( $image->ID, true );
/*
								$image->filepath = dirname( $upload_dir['basedir'] . '/' . get_post_meta( $image->ID, '_wp_attached_file', true ) );
								chdir( $image->filepath );
								$image->filename = basename( get_post_meta( $image->ID, '_wp_attached_file', true ) );
								$image->extension = strrchr( $image->filename, '.' );
								$image->filebase = usam_remove_filename_extension( $image->filename );
								foreach( $intermediate_sizes as $intermediate_size ) {
									if( file_exists( $image->filebase . '-' . $intermediate_size['width'] . 'x' . $intermediate_size['height'] . $image->extension ) )
										@unlink( $image->filebase . '-' . $intermediate_size['width'] . 'x' . $intermediate_size['height'] . $image->extension );
								}
								if( file_exists( $image->filename ) )
									@unlink( basename( $image->filename ) );
								wp_delete_post( $image->ID );
*/
							}
							unset( $images, $image );
						}
					}
				}
				break;

			case 'files':
				$post_type = 'usam-product-file';
				$files = (array)get_posts( array(
					'post_type' => $post_type,					
					'numberposts' => -1
				) );
				if( $files ) {
					foreach( $files as $file ) {
						if( $file->ID )
							wp_delete_post( $file->ID, true );
					}
				}
				break;

			case 'orders':			
				$wpdb->query( "TRUNCATE TABLE `".USAM_TABLE_ORDER_PROPS_VALUE."`" );						
			break;
			case 'coupons':
				$wpdb->query( "TRUNCATE TABLE `" . USAM_TABLE_COUPON_CODES . "`" );
			break;
			case 'usam_pages':
				$system_page = get_option( 'usam_system_page', false );
				foreach( $system_page as $page ) 
				{
					wp_delete_post( $page['id'], true );
				}
			break;
			case 'usam_options':								
				// $wpdb->query( "DELETE FROM `" . $wpdb->prefix . "options` WHERE `option_name` LIKE 'usam_%'" );
				
			break;			
			case 'preview-files':
				$post_type = 'usam-preview-file';
				$preview_files = (array)get_posts( array( 
					'post_type' => $post_type,
					'post_status' => 'inherit',
					'numberposts' => -1
				) );
				if( $preview_files ) {
					foreach( $preview_files as $preview_file ) {
						if( isset( $preview_file->ID ) )
							wp_delete_post( $preview_file->ID, true );
					}
				}
			break;

			// WordPress

			case 'posts':
				$post_type = 'post';
				$posts = (array)get_posts( array( 
					'post_type' => $post_type,				
					'numberposts' => -1
				) );
				if( $posts ) {
					foreach( $posts as $post ) {
						if( isset( $post->ID ) )
							wp_delete_post( $post->ID, true );
					}
				}
				break;

			case 'post_categories':
				$term_taxonomy = 'category';
				$post_categories = get_terms( $term_taxonomy, array( 'hide_empty' => false ) );
				if( $post_categories ) {
					foreach( $post_categories as $post_category ) {
						if( $post_category->term_id ) {
							wp_delete_term( $post_category->term_id, $term_taxonomy );
							$wpdb->query( "DELETE FROM `" . $wpdb->terms . "` WHERE `term_id` = " . $post_category->term_id );
						}
						if( $post_category->term_taxonomy_id )
							$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_relationships . "` WHERE `term_taxonomy_id` = %d", $post_category->term_taxonomy_id ) );
					}
				}
				$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_taxonomy . "` WHERE `taxonomy` = '%s'", $term_taxonomy ) );
				break;

			case 'post_tags':
				$term_taxonomy = 'post_tag';
				$post_tags = get_terms( $term_taxonomy, array( 'hide_empty' => false ) );
				if( $post_tags ) {
					foreach( $post_tags as $post_tag ) {
						if( $post_tag->term_id ) {
							wp_delete_term( $post_tag->term_id, $term_taxonomy );
							$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->terms . "` WHERE `term_id` = %d", $post_tag->term_id ) );
						}
						if( $post_tag->term_taxonomy_id )
							$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_relationships . "` WHERE `term_taxonomy_id` = %d", $post_tag->term_taxonomy_id ) );
					}
				}
				$wpdb->query( $wpdb->prepare( "DELETE FROM `" . $wpdb->term_taxonomy . "` WHERE `taxonomy` = '%s'", $term_taxonomy ) );
				break;

			case 'links':
				$wpdb->query( "TRUNCATE TABLE `" . $wpdb->prefix . "links`" );
				break;

			case 'comments':
				$comments = get_comments();
				if( $comments ) {
					foreach( $comments as $comment ) {
						if( $comment->comment_ID )
							wp_delete_comment( $comment->comment_ID, true );
					}
				}
			break;			
		}
	}
}