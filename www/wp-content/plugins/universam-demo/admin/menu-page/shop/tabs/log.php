<?php
/*
Просмотр лог-файлов
*/
class USAM_Tab_Log extends USAM_Tab
{	
	private $_files = array();
    private $_currentFile = false;
    private $_settings = array( 'autorefresh' => 1, 'display' => 'fifo', 'refreshtime' => 15 );   	
	private $realfile = '';   
	
	public function __construct()
	{
		$this->header = array( 'title' => __('Логи магазина', 'usam'), 'description' => 'Здесь Вы можете просматривать логи разных служб вашего магазина.' );
						
		$files = $this->getFiles();     
		if (isset($_REQUEST['file']))
            $file = stripslashes($_REQUEST['file']);
        elseif ( isset($files[0]) )
            $file = $files[0];
		else
			$file = '';
		
        $this->_currentFile = validate_file_to_edit( $file, $this->getFiles() );
        $this->realfile     = $this->transformFilePath( $this->_currentFile );
	}
	
	public function get_message()
	{		
		$message = '';
		if( isset($_REQUEST['delete']) )
			$message = sprintf( _n( '%s файл удален.', '%s файлов удалено.', $_REQUEST['delete'], 'usam' ), $_REQUEST['delete'] );	
		
		if( isset($_REQUEST['empty']) )
		{
			if( $_REQUEST['empty'] == 0 )
				$message = sprintf( __( 'Не удалось открыть файл %s.', 'usam' ), $this->realfile );	
			elseif ( $_REQUEST['empty'] == 2 )
				$message = sprintf( __( 'Не удалось очистить пустой файл %s.', 'usam' ), $this->realfile );	
			elseif ( $_REQUEST['empty'] == 1 )
				$message = sprintf( __( 'Файл %s очищен успешно.', 'usam' ), $this->realfile );	
		}				
		return $message;
	} 
	
	public function get_message_error()
	{		
		$message = '';		
		if ( $this->realfile != '' && !is_file($this->realfile))		
			$message = sprintf( __( 'Не удалось загрузить файл %s.', 'usam' ), $this->realfile );	
		
		if( isset($_REQUEST['writeable']) )		
			$message = sprintf( __( 'Вы не можете редактировать файл %s. Не доступен для записи.', 'usam' ), $this->realfile );		
		
		return $message;
	} 
		
	
	protected function callback_submit()
    {		
		global $display, $autorefresh, $user_ID;   		
		wp_reset_vars( array('display', 'autorefresh') );
		
		if ( $autorefresh )
			$this->_settings['autorefresh'] = 1;
		else
			$this->_settings['autorefresh'] = 0;
		
		if ( $display == 'fifo' )
			$this->_settings['display'] = 'fifo';
		else
			$this->_settings['display'] = 'filo';
	
		update_user_meta( $user_ID, 'usam_page_tab_log', $this->_settings );		
		switch( $this->current_action )
		{			
			case 'delete':		
				$unlink = unlink($this->realfile);	
				$this->sendback = remove_query_arg( array( 'file' ), $this->sendback );		
				$this->sendback = add_query_arg( array( 'deleted' => '1' ), $this->sendback );				
				$this->redirect = true;
			break;
			case 'empty':
				$this->redirect = true;
				 
				$writeable = is_writeable($this->realfile);		
				if ( $writeable )
				{				
					$handle = fopen($this->realfile, 'w');
					if (!$handle)
					{
						$this->sendback = add_query_arg( array( 'empty' => 0 ), $this->sendback );	
					}
					else
					{
						$handle = fclose($handle);
						if (!$handle)
						   $this->sendback = add_query_arg( array( 'empty' => 2 ), $this->sendback );	
						else
							$this->sendback = add_query_arg( array( 'empty' => 1 ), $this->sendback );
					}	
				}
				else
					$this->sendback = add_query_arg( array( 'writeable' => 0 ), $this->sendback );
            break;           			
		}
		if (isset($_POST['file_checkbox']))
		{
			$i = 0;
			foreach ( $_POST['file_checkbox'] as $file )
			{
				$path = realpath(USAM_UPLOAD_DIR .'Log' . DIRECTORY_SEPARATOR . $file);	
				if (file_exists($path))
				{				
					$unlink = unlink($path);
					$i++;
				}
				else 
					$unlink = 0;
			}
			$this->sendback = remove_query_arg( array( 'file' ), $this->sendback );					
			$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );			
			$this->redirect = true;
		}	
	}
	
	
	public function display() 
	{			
		require 'helper.inc';	
		$this->onViewPage();	
	}

    public static function transformFilePath( $file )
    {
        $path = realpath(USAM_UPLOAD_DIR .'Log' . DIRECTORY_SEPARATOR . $file);
		if ( !is_file($path) )	
			$path = '';
        return $path;
    }

    public function getFiles()
    {
        if (empty($this->_files)) {
            $this->_updateFiles();
        }
        return $this->_files;
    }

    public function hasFiles()
    {
        $this->getFiles();
        if (empty($this->_files)) {
            return false;
        }
        return true;
    }

    private function _updateFiles()
    {
        $this->_files = array();

        $wp_c = realpath(USAM_UPLOAD_DIR .'Log');

        $str     = $wp_c . DIRECTORY_SEPARATOR . "*.txt";
        $f       = glob($str);
        $str_rep = $wp_c . DIRECTORY_SEPARATOR;

        foreach ($f as $file) {
            $this->_files[] = str_replace($str_rep, "", $file);
        }
    }	

    public function onViewPage()
    {
		global $user_ID;
		
		$settings = get_user_meta($user_ID, 'usam_page_tab_log', true );
	
		if ( !empty($settings) )
			$this->_settings = $settings;
		
        if ( $this->_settings["autorefresh"] === 1) 
		{
        ?>
            <script type="text/javascript">
                setTimeout("window.location.replace(document.URL);", <?php echo $this->_settings["refreshtime"] * 1000 ?>);
            </script>
        <?php
        }		        
        if (!$this->hasFiles()) 
		{
            ?><p><?php echo __('Нет фалов в папке','usam').': '.USAM_UPLOAD_DIR; ?></p><?php            
        }		
		else
		{
			$files = $this->getFiles();       

			$writeable = is_writeable($this->realfile);		     
			$url = remove_query_arg( array( 'autorefresh' ), $this->sendback );
			?>       
			<div class = "columns">
				<div class = "columns_log_right">
					<form method='POST' action='<?php echo $url; ?>' id='usam-page-tabs-form'>
					<div id="file_list" class = "file_list">						
						<h3><?php _e('Файлы логов','usam'); ?></h3>
						<ul>
							<?php 
							$url = admin_url( 'admin.php?page=shop&tab=log' );				
							$i = 0;
							foreach ($files as $file):
								if ($this->_currentFile === $file) 
								{
								?>
									<li class="active">
								<?php
								} else {                        
								?>
									<li>
								<?php
								}
								$i++;
								?>
								<input type="checkbox" name="file_checkbox[]" value = "<?php echo $file; ?>"/>
								<a href="<?php echo add_query_arg( array('file' => $file ), $url ); ?>" style = "display:inline-block;"><?php echo $file; ?></a>
								</li>
							<?php endforeach; ?>
						</ul>
						<?php 
						$this->nonce_field();	
						submit_button(__('Удалить','usam'), 'button', 'Apply', false); 
						?> 					
					</div>
				</div>
				<div class = "columns_log_left">
					<div class="header_menu">            
						<?php printf('%1$s <strong>%2$s</strong>', __('Текущий файл','usam'), $this->_currentFile); ?>  
						<form method='post' action='' id='usam-page-tabs-form'>
							<div class="tablenav top">		
								<?php 
								$this->nonce_field();	
								if ($writeable) 
								{ 
									?>
									<div class="alignleft">                      
										<input type="hidden" value="<?php echo $this->_currentFile; ?>" name="file"/>
										<input id="scrollto" type="hidden" value="0" name="scrollto">
										<select name="action">
											<option selected="selected" value="-1"><?php _e('Действия для файла'); ?></option>
											<option value="empty"><?php _e('Очистить','usam'); ?></option>
											<option value="break"><?php _e('Break'); ?></option>								
											<option value="delete"><?php _e('Удалить','usam'); ?></option>	
										</select>
										<input type="hidden" value="<?php echo $this->_currentFile; ?>" name="file2"/>
										<?php submit_button(__('Выполнить','usam'), 'button', 'Do', false); ?>             
									</div>
								<?php } ?>	
								<div class="header_menu">   
									<input type="hidden" value="<?php echo $this->_currentFile; ?>" name="file2"/>
									<input type="checkbox" value="1" <?php checked($this->_settings['autorefresh'], 1); ?>
										   name="autorefresh"/>
									<label for="autorefresh"><?php _e('Автообновление','usam'); ?></label>
									<select name="display">
										<option <?php selected('fifo' == $this->_settings['display']); ?> value="fifo">FIFO</option>
										<option <?php selected('filo' == $this->_settings['display']); ?> value="filo">FILO</option>
									</select>						
									<?php 
									$this->nonce_field();	
									submit_button(__('Применить','usam'), 'button', 'Apply', false); 
									?>     
								</div>	
							</div>
						</form>
					</div>
					<div>
						<?php if ( is_file($this->realfile) ) : ?>						
							<textarea id="newcontent" name="newcontent" rows="25" cols="70" readonly="readonly"><?php echo $this->_getCurrentFileContent(); ?></textarea>
						<?php endif; ?>
						<div>
							<h3><?php _e('Информация для текущего файла','usam'); ?></h3>
							<dl>
								<dt><?php _e('Путь к файлу:','usam'); ?></dt>
								<dd><?php echo $this->realfile; ?></dd>
								<dt><?php _e('Последнее обновление: ','usam'); ?></dt>
								<dd><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), filemtime($this->realfile)); ?></dd>
								<dt><?php _e('Размер файла: ','usam'); ?></dt>
								<dd><?php echo filesize($this->realfile)." ".__('байт ','usam'); ?></dd>
							</dl>
						</div>
					</div>
				</div>
			</div>
			<?php   
		}
    }

    private function _getCurrentFileContent()
    {
        if ($this->_settings["display"] == "filo") 
            $result = implode(array_reverse(file($this->transformFilePath($this->_currentFile))));
        else
            $result = file_get_contents($this->transformFilePath($this->_currentFile), false);        
        return $result;
    }	
}