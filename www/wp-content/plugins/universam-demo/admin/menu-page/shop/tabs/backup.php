<?php
class USAM_Tab_backup extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Резервирование Вашего сайта', 'usam'), 'description' => 'Здесь Вы можете посмотреть сделать резервную копию базы данных.' );
	}
	
	protected function callback_submit()
    {
		global $wpdb;		
		
		switch( $this->current_action )
		{
			case 'start_backup_bd':		
				require_once( USAM_FILE_PATH . '/includes/technical/mysql_backup.class.php' );
				
				header( "Pragma: public" );
				header( "Expires: 0" );
				header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
				header( "Content-Type: application/octet-stream; charset=". get_bloginfo( 'charset' ) );
				header( "Content-Disposition: attachment; filename=" . DB_NAME . ".sql;" );
				try 
				{				
					$sql_dump = new USAM_MySQL_Backup();
					foreach ( $sql_dump->tables_to_dump as $key => $table ) 
					{
						if ( $wpdb->prefix != substr( $table,0 , strlen( $wpdb->prefix ) ) )
							unset( $sql_dump->tables_to_dump[ $key ] );
					}
					$sql_dump->execute();
					unset( $sql_dump );
				} 
				catch ( Exception $e ) 
				{
					die( $e->getMessage() );
				}
				die();
			break;
		}
	}
	
	public function display() 
	{
		?>		
		<div class="button_box">
			<h3><?php _e( 'Резервная копия в один клик', 'usam' ); ?></h3>
			<form method='POST' action=''>				
				<?php $this->nonce_field(); ?>			
				<input type="hidden" name="action" value="start_backup_bd" />
				<input type="submit" name="title" class="button" value="<?php _e( 'Скачать резервную копию базы данных', 'usam' ); ?>">			
			</form>
		</div>		
		<?php
	}	
}