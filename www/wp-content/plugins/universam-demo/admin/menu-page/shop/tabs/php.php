<?php
class USAM_Tab_PHP extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Разрешения', 'usam'), 'description' => '' );
	}
	
	public function display() 
	{
		?>
		<h3><?php _e( 'Произвольный PHP-скрипт для выполнения на сервере', 'usam' ); ?></h3>
		<textarea id = "php_script">print_r('safdrseftrs');</textarea>
		<div class="usam_hls usam_light">
		<div class="usam_inner-hl">
			<div class="usam_inp-cont" style="top: 5px; left: 145px;">
				<textarea class="usam_inp" wrap="off" autocorrect="off" autocapitalize="off"></textarea>
			</div>
			 <div class="usam_scrollbar" style="display: none;">
				<div class="usam_scrollbar-inner"></div>
			</div>
			 <div class="usam_scroller" tabindex="-1" style="height: 330px;" draggable="true">
				<div class="usam_size-cont" tabindex="-1">
				  <div class="usam_inner-size-cont" style="top: 0px;">
					<div class="usam_hl-line-num" style="height: 330px;">
					  <div class="usam_hl-line-num-text">
						<pre>1</pre>
					  </div>
					</div>
					<div class="usam_highlight">
					  <div class="usam_lines-cnt" style="outline-color: currentColor; outline-width: medium; outline-style: none; margin-left: 52px; min-width: 90px;">            
					  
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			</div>
			
			<form method='POST' action='' id='usam-php-tab-form'>
				<div class="adm-detail-content-btns">
					<input type="button" accesskey="x" name="execute" value="Выполнить" id="run_php_script">
					<input type="reset" value="Очистить">
				</div>
			</form>
			<div class="usam_php_result">
			</div>
		<?php
	}	
}