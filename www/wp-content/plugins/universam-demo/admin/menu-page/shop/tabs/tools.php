<?php
class USAM_Tab_Tools extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Инструменты магазина', 'usam'), 'description' =>  __('На этой страницы представлены возможности отладки Вашего магазина, чтобы можно было исправить конкретные проблемы на некоторых сайтах.<br /> Неразумно использовать все, что представлено на этой странице, если вы не знаете точно, что они будут делать и зачем нужны!', 'usam') );
	}
	
	public function get_message()
	{		
		$message = '';		
		if( isset($_REQUEST['update']) )
			$message = sprintf( _n( '%s товаров обновлен.', '%s товаров обновлено.', $_REQUEST['update'], 'usam' ), $_REQUEST['update'] );
		if( isset($_REQUEST['delete_revision']) )
			$message = sprintf( _n( 'Удалена %s ревизия.', 'Удалено %s ревизий.', $_REQUEST['delete_revision'], 'usam' ), $_REQUEST['delete_revision'] );	
		if( isset($_REQUEST['vk_publish_birthday']) )
			$message = sprintf( _n( 'Поздравлен %s человек.', 'Поздравлено %s людей.', $_REQUEST['vk_publish_birthday'], 'usam' ), $_REQUEST['vk_publish_birthday'] );
		
		return $message;
	} 
	
	protected function callback_submit()
	{
		global $wpdb;
		
		switch( $this->current_action )
		{			
			case 'delete':
				$sql = "SELECT `post_id` FROM ".$wpdb->postmeta." WHERE `meta_key` = '_usam_stock'";
				$product_id = $wpdb->get_col($sql);
				foreach ( $product_id as $id )
				{					
					usam_update_product_meta( $id, 'stock', '' );
				}
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );			
				$this->redirect = true;
			break;				
		}	
		
	/*	if ( isset($_POST['calculate_color_filter_products']))
		{	//рассчитать фильтр цветов товаров				
			$args =  array( 'fields' => 'ids', 'cache_results' => false ,'update_post_meta_cache' => true, 'update_post_term_cache' => false );
			$products = usam_get_products( $args );							
			$i = 0;			
			foreach ( $products as $products_id )
			{		
				$attachment_ID = get_post_thumbnail_id( $products_id );
				$old_filepath = get_attached_file( $attachment_ID ); 
				
				if ( $old_filepath != '' )
				{
					// Узнаем цвет изображения
					require_once( USAM_FILE_PATH . '/includes/image/colors.inc.php' );	
					$pal = new GetMostCommonColors( $old_filepath );
					$colors = $pal->get_group_color( $colors_to_show );	
					
					$array = array_flip($colors); 
					unset ($array['white']); 
					$colors = array_flip($array); 
				
					update_product_metadata( $products_id, 'colors', $colors );
					$i++;
				}
			}
			$this->sendback = add_query_arg( array( 'calculate_color' => $i ), $this->sendback );
			$this->redirect = true;			
		}		*/
		if ( isset($_POST['calculate_link_products']))
		{
			$product_ids = usam_get_products( array('fields' => 'ids', 'update_post_meta_cache' => false) );
			foreach ( $product_ids as $id )
			{ 
				usam_guid_write( $id );
			}
		}		
		if ( isset($_POST['calculate_stock_products']))
		{
			usam_recalculate_stock_products( );					
		}		
		if ( isset($_POST['remove_duplicate_custom_fields']))
		{
			$sql = "SELECT * FROM ".$wpdb->postmeta." WHERE `meta_key` = '_usam_stock'";
			$product_id = $product_id2 = $wpdb->get_results($sql, ARRAY_A);	
			$i = 0;
			foreach ( $product_id as $key => $product )
			{
				unset($product_id2[$key]);
				foreach ( $product_id2 as $key => $product2 )
				{				
					if ( $product['post_id'] == $product2['post_id'] && $product['meta_key'] == $product2['meta_key'] && $product['meta_value'] == $product2['meta_value'] )
					{						
						$result = $wpdb->query( $wpdb->prepare( "DELETE FROM `".$wpdb->postmeta."` WHERE `meta_id` = %d", $product2['meta_id'] ) );	
						$i++;
					}
				}
			}			
			$this->sendback = add_query_arg( array( 'delete_meta' => $i ), $this->sendback );
			$this->redirect = true;			
		}
		if ( isset($_POST['zero_stock_products']))
		{
			$wpdb->query( "UPDATE `".$wpdb->postmeta."` SET `meta_value` = '0' WHERE `meta_value`<0 AND `meta_key` LIKE '_usam_storage_%'");
			usam_recalculate_stock_products( );	
		}
		if ( isset($_POST['clear_customer_meta']))
			USAM_Clear::clear_customer_meta();
	
		if ( isset($_POST['clear_stock_claims']))		
			USAM_Clear::clear_stock_claims();
		
		if ( isset($_POST['clear_cron']))
		{	
			wp_clear_scheduled_hook( 'usam_tracker_send_event' );	
			$cron = _get_cron_array();
			foreach ( $cron as $timestamp => $cronhooks ) { 
				foreach ( (array) $cronhooks as $hook => $events ) 
				{ 
					wp_clear_scheduled_hook($hook); // очистить очередь		
				}
			}						
		}				
		if ( isset($_POST['delete_revision']))
		{
			$wpdb->query("DELETE FROM $wpdb->postmeta WHERE post_id IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'revision')");
			$wpdb->query("DELETE FROM $wpdb->term_relationships WHERE object_id IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'revision')");
			$result = $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type = 'revision'");
			$this->sendback = add_query_arg( array( 'delete_revision' => $result ), $this->sendback );
			$this->redirect = true;
		}
		if ( isset($_POST['enable_reviews_for_all_messages']))
		{			
			$wpdb->query( "DELETE $wpdb->postmeta FROM $wpdb->postmeta LEFT JOIN $wpdb->posts ON $wpdb->posts.ID = $wpdb->postmeta.post_id WHERE $wpdb->posts.post_type = 'post' AND $wpdb->postmeta.meta_key = 'usam_reviews_enable'");
			$wpdb->query( "INSERT INTO $wpdb->postmeta SELECT 0,$wpdb->posts.ID,'usam_reviews_enable',1	FROM $wpdb->posts WHERE $wpdb->posts.post_type = 'post' " ); 
		}
		if ( isset($_POST['disable_reviews_for_all_messages']))
		{
			$wpdb->query( "DELETE $wpdb->postmeta FROM $wpdb->postmeta LEFT JOIN $wpdb->posts ON $wpdb->posts.ID = $wpdb->postmeta.post_id WHERE $wpdb->posts.post_type = 'post' AND $wpdb->postmeta.meta_key = 'usam_reviews_enable'" );
		}
		if ( isset($_POST['enable_reviews_for_all_pages']))
		{
			$wpdb->query( "DELETE $wpdb->postmeta FROM $wpdb->postmeta LEFT JOIN $wpdb->posts ON $wpdb->posts.ID = $wpdb->postmeta.post_id WHERE $wpdb->posts.post_type = 'page' AND $wpdb->postmeta.meta_key = 'usam_reviews_enable'" );
			$wpdb->query( "INSERT INTO $wpdb->postmeta SELECT 0,$wpdb->posts.ID,'usam_reviews_enable',1 FROM $wpdb->posts WHERE $wpdb->posts.post_type = 'page' " );                 
		}
		if ( isset($_POST['disable_reviews_for_all_pages']))
		{
			$wpdb->query( "DELETE $wpdb->postmeta FROM $wpdb->postmeta LEFT JOIN $wpdb->posts ON $wpdb->posts.ID = $wpdb->postmeta.post_id WHERE $wpdb->posts.post_type = 'page' AND $wpdb->postmeta.meta_key = 'usam_reviews_enable'" );
		}
		if ( isset($_POST['increase_sales_product']))
			usam_process_calculate_increase_sales_product();
	}
	
	public function display() 
	{	
		?>			
		<form method='post' action='' id='usam-page-tabs-form'>
			<?php $this->nonce_field(); ?>
			<div class = "container">
				<h3></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="check_product_availability" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Проверить наличие продукта на сайте поставщика', 'usam' ); ?>.</td>				
					</tr>				
					<tr>
						<td><input type="submit" name="update_the_cache_menu" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Пересоздать меню', 'usam' ); ?>.</td>				
					</tr>								
				</table>
			</div>
			<div class = "container">
				<h3><?php esc_html_e( 'Cron', 'usam' ); ?></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="clear_cron" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Очистить запланированные задания (cron)', 'usam' ); ?></td>				
					</tr>					
				</table>
			</div>
			<div class = "container">
				<h3><?php esc_html_e( 'Очистка базы', 'usam' ); ?></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="clear_customer_meta" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Очистить базу от временных записей клиентов', 'usam' ); ?></td>				
					</tr>
					<tr>
						<td><input type="submit" name="clear_stock_claims" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Очистить зарезервированные товары', 'usam' ); ?></td>				
					</tr>						
					<tr>
						<td><input type="submit" name="remove_duplicate_custom_fields" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Удалить дубликаты произвольны полей', 'usam' ); ?></td>				
					</tr>
					<tr>
						<td>
							<input type="submit" name="delete_revision" id="delete_revision" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/>
						</td>
						<td><?php esc_html_e( 'Удалить ревизии постов, записей и товаров', 'usam' ); ?></td>				
					</tr>								
				</table>
			</div>
			<div class = "container">
				<h3><?php esc_html_e( 'Товары', 'usam' ); ?></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="calculate_link_products" id="calculate_link_products" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Пересоздать кешированные ссылки', 'usam' ); ?></td>				
					</tr>	
					<tr>
						<td><input type="submit" name="calculate_color_filter_products" id="calculate_color_filter_products" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Рассчитать фильтр цветов товаров', 'usam' ); ?></td>				
					</tr>		
					<tr>
						<td><input type="submit" name="calculate_stock_products" id="calculate_stock_products" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Пересчитать остатки', 'usam' ); ?></td>				
					</tr>	
					<tr>
						<td><input type="submit" name="zero_stock_products" id="zero_stock_products" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Убрать нулевые', 'usam' ); ?></td>				
					</tr>	
					<tr>
						<td><input type="submit" name="increase_sales_product" id="increase_sales_product" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Расчет товаров для увеличения продаж', 'usam' ); ?></td>				
					</tr>	
				</table>
			</div>			
			<div class = "container">
				<h3><?php esc_html_e( 'Обмен данными', 'usam' ); ?></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="update_product_quantity" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Активировать загрузку остатков', 'usam' ); ?>.</td>				
					</tr>	
				</table>
			</div>
			<div class = "container">
				<h3><?php esc_html_e( 'Отзывы клиентов', 'usam' ); ?></h3>
				<table class = "usam_tools">				
					<tr>
						<td><input type="submit" name="enable_reviews_for_all_messages" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Включить для всех существующих сообщений', 'usam' ); ?>.</td>				
					</tr>	
					<tr>
						<td><input type="submit" name="disable_reviews_for_all_messages" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Выключить для всех существующих сообщений', 'usam' ); ?>.</td>				
					</tr>	
					<tr>
						<td><input type="submit" name="enable_reviews_for_all_pages" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Включить плагин для всех существующих страниц', 'usam' ); ?>.</td>				
					</tr>	
					<tr>
						<td><input type="submit" name="disable_reviews_for_all_pages" id="Upload_p" class="button button-primary" value="<?php _e( 'Активировать', 'usam' ) ?>"/></td>
						<td><?php esc_html_e( 'Выключить плагин для всех существующих страниц', 'usam' ); ?>.</td>				
					</tr>	
				</table>
			</div>
			<input type='hidden' value='true' name='tols' />
		</form>
		<?php		
	}
}