<?php
class USAM_Tab_General extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Общие состояние', 'usam'), 'description' => '' );
	}
	
	public function display() 
	{
		usam_add_box( 'usam_version', __('Версии','usam'), array( $this, 'box_version' ) );	
		usam_add_box( 'usam_server', __('Информация о сервере','usam'), array( $this, 'box_server' ) );	
		usam_add_box( 'usam_page', __('Страницы','usam'), array( $this, 'box_pages' ) );	
		usam_add_box( 'usam_showcase', __('Витрина','usam'), array( $this, 'box_showcase' ) );		
	}
	
	function box_version( )
	{
		if( get_bloginfo( 'version' ) < '3.4' )
			$theme_data = get_theme_data( get_stylesheet_directory() . '/style.css' );
		else
			$theme_data = wp_get_theme();
		$theme = array( 'name' => $theme_data['Name'], 'version' => $theme_data['Version'], 'author' => $theme_data['Author'], 'author_url' => $theme_data['AuthorURI'] );
		$plugins = get_plugins();
		$plugins_option = get_option( 'active_plugins', array() );
		$active_plugins = array();
		foreach( $plugins as $plugin_path => $plugin ) {
			if( !in_array( $plugin_path, $plugins_option ) )
				continue;
			$active_plugins[] = array( 'name' => $plugin['Name'], 'version' => $plugin['Version'], 'url' => $plugin['PluginURI'], 'author' => $plugin['Author'], 'author_url' => $plugin['PluginURI'], 'raw' => print_r( $plugin, true ) );
		}
		?>	
		<table class="widefat fixed">

			<tr>
				<th><?php _e( 'WordPress', 'usam' ); ?></th>
				<td><?php echo get_bloginfo( 'version' ); ?></td>
			</tr>

			<tr>
				<th><?php _e( 'УНИВЕРСАМ', 'usam' ); ?></th>
				<td><?php echo USAM_VERSION; ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Тема', 'usam' ); ?></th>
				<td><?php echo $theme['name']; ?> (<?php _e( 'Автор', 'usam' ); ?>: <a href="<?php echo $theme['author_url']; ?>" target="_blank"><?php echo $theme['author']; ?></a>) <?php _e( 'Версия', 'usam' ); ?>: <?php echo $theme['version']; ?></td>
			</tr>

			<tr>
				<th><?php _e( 'Активные плагины', 'usam' ); ?>:</th>
				<td>
					<?php if( $active_plugins ) { ?>
						<ul>
						<?php foreach( $active_plugins as $plugin ) 
						{ ?>
							<li><a href="<?php echo $plugin['url']; ?>"><?php echo $plugin['name']; ?></a> (<?php _e( 'Автор', 'usam' ); ?>: <a href="<?php echo $plugin['author_url']; ?>" target="_blank"><?php echo $plugin['author']; ?></a>) <?php _e( 'Версия', 'usam' ); ?>: <?php echo $plugin['version']; ?></li>
						<?php } ?>
					</ul>
					<?php } ?>
				</td>
			</tr>
		</table>
		<?php	
	}
	
	function box_server( )
	{		
		global $wpdb;
		$max_upload = round( wp_max_upload_size() / 1024 / 1024, 2 ) . 'MB';
		$max_post = (int)( ini_get( 'post_max_size' ) ) . 'MB';
		$memory_limit = (int)( ini_get( 'memory_limit' ) ) . 'MB';
		$max_execution_time = ini_get( 'max_execution_time' );
		?>
		<table class="widefat fixed">

			<tr>
				<th><?php _e( 'Версия PHP', 'usam' ); //print_r(ini_get_all()); ?></th>
				<td><?php echo PHP_VERSION; ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Разрадность PHP', 'usam' );  ?></th>
				<td><?php if (PHP_INT_SIZE === 4) { echo '32 bit'; } else { echo '64 bit'; }; ?></td>
			</tr>		
			<tr>
				<th><?php _e( 'Версия MySQL', 'usam' ); ?></th>
				<td><?php echo $wpdb->db_version(); ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Максимальный загружаемый файл', 'usam' ); ?></th>
				<td><?php echo $max_upload; ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Max. POST Size', 'usam' ); ?></th>
				<td><?php echo $max_post; ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Максимальное время выполнения', 'usam' ); ?></th>
				<td><?php echo $max_execution_time; ?></td>
			</tr>
			<tr>
				<th><?php _e( 'Распределение памяти', 'usam' ); ?></th>
				<td><?php echo $memory_limit; ?></td>
			</tr>
			<tr>
				<td><?php _e( 'Ограничение памяти WP','usam' ); ?>:</td>
				<td><?php
					$memory = usam_let_to_num( WP_MEMORY_LIMIT );
					if ( $memory < 67108864 ) {
						echo '<mark class="error">' . sprintf( __( '%s - Мы рекомендуем устанавливать память не менее 64. Посмотреть: <a href="%s">Увеличение памяти, выделенной PHP</a>', 'usam' ), size_format( $memory ), 'http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP' ) . '</mark>';
					} else {
						echo '<mark class="yes">' . size_format( $memory ) . '</mark>';
					}
				?></td>
			</tr>			
			<tr>
				<td><?php _e( 'Состояние мультисайта WordPress','usam' ); ?>:</td>
				<td><?php if ( is_multisite() ) echo __( 'Включен', 'usam' ); else echo __( 'Выключен', 'usam' ); ?></td>
			</tr>
			<tr>
				<td><?php _e( 'Информация Веб Сервера','usam' ); ?>:</td>
				<td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ); ?></td>
			</tr>			
			
			<tr>
				<td><?php _e( 'Режим отладки Wordpress', 'usam' ); ?>:</td>
				<td><?php if ( defined('WP_DEBUG') && WP_DEBUG ) echo '<mark class="yes">'.__( 'Да', 'usam' ).'</mark>'; else echo '<mark class="no">' . __( 'Нет', 'usam' ) . '</mark>'; ?></td>
			</tr>
			<tr>
				<td><?php _e( 'Язык Wordpress', 'usam' ); ?>:</td>
				<td><?php if ( defined( 'WPLANG' ) && WPLANG ) echo WPLANG; else  _e( 'Default', 'usam' ); ?></td>
			</tr>		
	<?php if ( function_exists( 'ini_get' ) ) : ?>			
			<tr>
				<td><?php _e( 'Максимальный входной PHP Vars','usam' ); ?>:</td>
				<td><?php echo ini_get('max_input_vars'); ?></td>
			</tr>
			<tr>
				<td><?php _e( 'SUHOSIN Installed','usam' ); ?>:</td>
				<td><?php echo extension_loaded( 'suhosin' ) ? __( 'Да', 'usam' ) : __( 'No', 'usam' ); ?></td>
			</tr>
	<?php endif; ?>
			<tr>
				<td><?php _e( 'Ведение логов магазина','usam' ); ?>:</td>
				<td><?php
					if ( true )
						echo '<mark class="yes">' . __( 'Логи ведутся.', 'usam' ) . '</mark>';
					else
						echo '<mark class="error">' . __( 'Логи магазина выключены', 'usam' ) . '</mark>';
				?></td>
			</tr>
			<tr>
				<td><?php _e( 'Часовой пояс по умолчанию','usam' ); ?>:</td>
				<td><?php
					$default_timezone = date_default_timezone_get();
					if ( 'UTC' !== $default_timezone ) {
						echo '<mark class="error">' . sprintf( __( 'Default timezone is %s - it should be UTC', 'usam' ), $default_timezone ) . '</mark>';
					} else {
						echo '<mark class="yes">' . sprintf( __( 'Default timezone is %s', 'usam' ), $default_timezone ) . '</mark>';
					} ?>
				</td>
			</tr>
		</table>
		<?php	
	}
	
	
	function box_pages( )
	{		
		$pages = usam_system_pages();
		if( $pages ) 
		{ 
			?>
			<table class="widefat fixed">
				<?php 
				foreach( $pages as $page )
				{ 
				$page_id = usam_get_system_page_id( $page['name'] );
				?>
				<tr id="<?php echo $page_id; ?>">
					<th><?php echo $page['title']; ?></th>
					<td>
						<code><?php echo usam_get_url_system_page( $page['name'] ); ?></code>					
						<?php if( $page_id ) 
						{ ?>
							<a href="<?php echo add_query_arg( array( 'post' => $page_id, 'action' => 'edit' ), 'post.php' ); ?>"><attr title="<?php _e( 'Правка страницы', 'usam' ); ?>"><?php _e( 'Правка', 'usam' ); ?></attr></a>
						<?php } ?>
						<?php if( $page['content'] ) { ?>
							<p class="description"><?php _e( 'Эта страница содержит правильный шорткод для страницы ' . $page['title'], 'usam' ); ?></p>
						<?php } else { ?>
							<p class="description"><?php _e( 'Эта страница не содержит ожидаемого шорткода для ' . $page['title'] . ' и может не работать.', 'usam' ); ?></p>
						<?php } ?>
						<!-- (#<?php echo $page_id; ?>) -->
					</td>
				</tr>
				<?php } ?>
			</table>
			<?php 
		} 
	}	
	
	function box_showcase( )
	{			
		?>				
		<table class="widefat fixed">
			<tr>
				<th><?php _e( 'Товары', 'usam' ); ?></th>
				<td><?php echo usam_return_details( 'products' ); ?></td>			
			</tr>
			<tr>
				<th><?php _e( 'Варриации товаров', 'usam' ); ?></th>
				<td><?php echo usam_return_details( 'variations' ); ?>	</td>
			</tr>		
			<tr>
				<th><?php _e( 'Изображения товаров', 'usam' ); ?></th>
				<td><?php echo usam_return_details( 'images' ); ?></td>			
			</tr>
			<tr>
				<th><?php _e( 'Файлы товаров', 'usam' ); ?></th>
				<td><?php echo usam_return_details( 'files' ); ?></td>
			</tr>
		</table>
		<?php 
	}	
}