<?php
class USAM_Tab_sql extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Таблицы базы данных', 'usam'), 'description' => 'Здесь Вы можете посмотреть все таблицы базы данных.' );
	}	
}