<?php
class Customer_Details_List_Table  extends USAM_List_Table 
{
	protected $orderby = 'user_id';
	protected $order   = 'asc'; 
	protected $user_list;   
	
	function __construct( $args )
	{	
       parent::__construct( $args );
	   $this->search_title = __('Поиск покупателя', 'usam');	
    }	
	
	function column_subtotal( $item ) 
    {
		$sum = 0;
		foreach ( $item->products as $product ) 
			$sum += usam_get_product_price( $product['product_id'] );
		
		echo usam_currency_display( $sum );		
    }
	
	function column_total_items( $item ) 
    {
		echo count( $item->products );		
    }
	
	function column_products( $item ) 
    {		
		foreach ( $item->products as $product ) 
		{
			$post = get_post( $product['product_id'] );
			echo '<a href="'.$post->guid.'">'.$post->post_title.'</a> ( '.usam_get_product_meta($product['product_id'], 'stock'). __('шт','usam').' ) - '.usam_local_date( $product['date'] ).'<br>';		
		}		
    }
	
	function column_customer( $item ) 
	{		
		echo $item->customer;		
	}
	
	function column_user( $item ) 
	{		
		echo "$item->display_name ({$item->user_login})";
	}
	 
   
	function get_sortable_columns()
	{
		$sortable = array(			
			'subtotal'      => array('subtotal', false),
			'total_items'   => array('total_items', false),				
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(  			
			'user'           => __( 'Пользователь', 'usam' ),	
			'customer'       => __( 'Покупатель', 'usam' ),	
			'subtotal'       => __( 'Стоимость', 'usam' ),
			'total_items'    => __( 'Количество', 'usam' ),
			'products'       => __( 'Товары', 'usam' ),					
        ); 
        return $columns;
    }
	
	public function get_number_columns_sql()
    {       
		return array('subtotal', 'total_items');
    }
	
	function prepare_items() 
	{			
			
	}
}