<?php
/*
 * Отображение страницы покупатели
 */
$default_tabs = array(
	array( 'id' => 'customers',  'title' => __( 'Покупатели', 'usam' ) ),	
	array( 'id' => 'cart',  'title' => __( 'Корзины', 'usam' ) ),
	array( 'id' => 'wish',  'title' => __( 'Список желаний', 'usam' ) ),
	array( 'id' => 'compare',  'title' => __( 'Список сравнений', 'usam' ) ),
	array( 'id' => 'views',  'title' => __( 'Просмотренные товары', 'usam' ) ),
	array( 'id' => 'bonuses',  'title' => __( 'Бонусы', 'usam' ) ),	
);

class USAM_Tab extends USAM_Page_Tab
{	
	public function localize_script_tab()
	{
		return array(			
			'save_bonus_nonce' => usam_create_ajax_nonce( 'save_bonus' ),
		);
	}	
} 	