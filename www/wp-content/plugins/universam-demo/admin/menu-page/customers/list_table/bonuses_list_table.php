<?php
class USAM_List_Table_Bonuses extends USAM_List_Table 
{	
	private $status_sum = array( );	
	protected $order = 'desc'; 
	private $bonus_status;
		
    function __construct( $args )
	{	
		parent::__construct( $args );

		$this->bonus_status = usam_get_statuses_bonus();	
    }	
	
	function get_bulk_actions_display() 
	{			
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}

	function get_views() 
	{	
		global $wpdb;	
		$sendback = remove_query_arg( array('status') );
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['status'])) ?  'class="current"' : '' ).">". __('Все записи','usam')." <span class='count'> ($this->total_items)</span></a>";			
		foreach( $this->bonus_status as $key => $value )
		{			
			if ( $this->status_sum[$value['number']] != 0 )
			{
				$sendback = add_query_arg( array( 'status' => $value['number'] ) );
				$views[$value['number']] = "<a href='$sendback' title ='".$value['title']."' ". (( isset($_GET['status']) && $_GET['status'] == $value['number'] ) ?  'class="current"' : '' ).">".$value['name']." <span class='count'> (".$this->status_sum[$value['number']].")</span></a>";
			}
		}	
		return $views;
	}		
	
	function no_items() 
	{
		_e( 'Бонусы не найдены', 'usam' );
	}		
		
	function column_customer( $item ) 
    {
		$this->row_actions_table( $item->user, $this->standart_row_actions( $item->id ) );	
    }
	
	function column_use_date( $item ) 
    {
		if ( !empty($item->use_date) )
		{
			$format = get_option( 'date_format', 'Y/m/d' )." H:i";		
			echo date_i18n( $format, strtotime($item->use_date) );
		}
    }
	
	function column_order( $item ) 
    {
		echo "<a href='".usam_get_url_order( $item->order_id )."'>$item->order_id</a>";		
    }	
	
	function column_status( $item )
    {
		?> 
		<select name="bonus_status" id="bonus_status" class = "width100">
			<?php
			foreach( $this->bonus_status as $value )
			{						
				?>               
					<option value="<?php echo $value['number']; ?>" <? echo ($item->status == $value['number']) ? 'selected="selected"':''?> ><?php echo $value['title']; ?></option>							
				<?php
			}
			?>  
		</select>  
		<?php			
    }	
	
	function column_type( $item ) 
    {	
		echo usam_get_bonus_type($item->type); 
    }	
	
	function column_action( $item ) 
    {	
		?> 
		<a href="#" id = "action_row_bonus_save" class="save button" data-id="<?php echo $item->id; ?>"><?php _e( 'Сохранить', 'usam' ) ?></a>
		<?php 
		usam_loader(); 
    }	
 
	function column_default( $item, $column_name ) 
    {
		echo $item->$column_name; 
    }   
	
	function get_sortable_columns()
	{
		$sortable = array(
			'customer'          => array('meta_value', false),
			'order'             => array('order_id', false),
			'bonus'             => array('bonus', false),
			'status'            => array('status', false),
			'type'              => array('type', false),
			'date'              => array('date', false),	
			'use_date'          => array('use_date', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(  
			'cb'                => '<input type="checkbox" />',				
			'customer'          => __( 'Покупатель', 'usam' ),
			'order'             => __( 'Номер заказа', 'usam' ),
			'bonus'             => __( 'Количество бонусов', 'usam' ),	
			'status'            => __( 'Статус', 'usam' ),	
			'type'              => __( 'Тип', 'usam' ),	
			'use_date'          => __( 'Дата использования', 'usam' ),
			'date'              => __( 'Дата создания', 'usam' ),	
			'date_update'       => __( 'Дата изменения', 'usam' ),	
			'action'            => __( 'Действие', 'usam' ),			
        ); 
        return $columns;
    }
	
	function prepare_items() 
	{		
		global $wpdb;
		
		$this->total_items = 0;		
		foreach ( $this->bonus_status as $key => $value )
		{
			$this->status_sum[$key] = $wpdb->get_var("SELECT COUNT(*) FROM ".USAM_TABLE_CUSTOMER_BONUSES." WHERE status = '$key'");
			$this->total_items = $this->total_items + $this->status_sum[$key];
		}	
		$this->get_standart_query_parent( );		
		$search_terms = $this->search != '' ? explode( ' ', $this->search ): array();
		
		$search_sql = array();
		foreach ( $search_terms as $term )
		{
			$search_sql[$term][] = "u.meta_value LIKE '%".esc_sql( $term )."%'";			
			$search_sql[$term] = '(' . implode( ' OR ', $search_sql[$term] ) . ')';
		}		
		if ( $search_sql )
		{
			$this->where[] = implode( ' AND ', array_values( $search_sql ) );
		}	
		
		if ( isset($_GET['status']) && $_GET['status'] != '-1' )		
			$this->where[] = 'status='.$_GET['status'];		
		$where = implode( ' AND ', $this->where );

		$joins = array();
		$joins[] = "INNER JOIN $wpdb->usermeta AS u ON (u.user_id=cb.user_id AND u.meta_key = 'nickname')";
		$joins = implode( ' ', $joins );
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS cb.*, u.meta_value AS user FROM `".USAM_TABLE_CUSTOMER_BONUSES."` AS cb {$joins}	WHERE {$where} ORDER BY {$this->orderby} {$this->order} {$this->limit}";
		$this->items = $wpdb->get_results( $sql );
		
		$this->total_items = $wpdb->get_var( "SELECT FOUND_ROWS()" );	
		
		$this->_column_headers = $this->get_column_info();		
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}