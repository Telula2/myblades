<?php
class USAM_List_Table_Cart extends USAM_List_Table 
{		
	protected $order   = 'desc';    
	protected $properties   = array(); 
	
	public function __construct( $args = array() ) 
	{		
		parent::__construct( $args );

		$this->search_title = __('Поиск покупателя', 'usam');	
		$this->properties = usam_get_order_properties( array('fields' => 'unique_name=>id') );			
	}
	
	function no_items() 
	{
		_e( 'Корзины не найдены', 'usam' );
	}

	function get_bulk_actions_display() 
	{			
		$actions = array(
			'delete'    => __( 'Очистить', 'usam' )
		);
		return $actions;
	}	
	
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			$this->print_button();
		}		
	}	
	
	protected function get_filter_tablenav( ) 
	{		
		return array( 'calendar' => array() );		
	}	
		
	function column_id( $item ) 
    {
		$actions = array(
			'order'      => $this->add_row_actions( $item->id, 'order', __( 'Создать заказ', 'usam' ) ),		
		);	
		if ( !$this->current_action() == 'delete' )
			$actions['delete'] = $this->add_row_actions( $item->id, 'delete', __( 'Удалить', 'usam' ) );
		
		$this->row_actions_table( $item->id, $actions );	
	}	
	
	function column_totalprice( $item ) 
	{
		return usam_currency_display( $item->totalprice );
    } 
	
	function column_customer( $item ) 
	{
		$customer = __('Гость','usam');
		if ( $item->user_id != 0 )
		{
			$profile = usam_get_all_customer_meta( $item->user_id );		
			if ( !empty($profile) && !empty($profile['checkout_details']) )
			{
				$checkout_details = $profile['checkout_details'];	
								
				$user = get_user_by('id', $item->user_id);
				$customer = isset($user->user_login)?$user->user_login."<br>":'';
				
				if ( !empty($checkout_details[$this->lproperties['billingfirstname']]) && !empty($checkout_details[$this->lproperties['billinglastname']]) && !empty($checkout_details[$this->lproperties['billingemail']]))					
					$customer .= $checkout_details[$this->lproperties['billingfirstname']].' '.$checkout_details[$this->lproperties['billinglastname']].'<br>'.$checkout_details[$this->lproperties['billingemail']];
			}
		}	
		echo $customer;
    } 
	
	function column_date_modified( $item ) 
	{
		echo usam_local_date( $item->date_modified );
    }
	
	function column_products( $item ) 
	{
		$products = usam_get_products_basket( $item->id );
		$cart_item = '';
		foreach ( $products as $product ) 
		{					
			$cart_item .= '<a href="'.usam_product_url( $product->product_id ).'">'.$product->name.'</a> ( '.$product->quantity. __('шт','usam').' ) - '. usam_currency_display($product->price).'<br>';
		}
		echo $cart_item;
    }
	  
	function get_sortable_columns()
	{
		$sortable = array(
			'date_modified' => array('date_modified', false),		
			'totalprice'      => array('totalprice', false),
			'quantity'       => array('quantity', false),	
			'customer'      => array('user_id', false),
			'total_items'   => array('total_items', false),
			'date'          => array('date_insert', false),
			);
		return $sortable;
	}
		
	function get_columns()
	{      		
		$columns = array(  
			'cb'             => '<input type="checkbox" />',		
			'id'             => __( 'ID корзины', 'usam' ),			
			'date_modified'  => __( 'Дата изменения', 'usam' ),
			'customer'       => __( 'Покупатель', 'usam' ),	
			'totalprice'     => __( 'Стоимость', 'usam' ),
			'quantity'       => __( 'Количество', 'usam' ),
			'products'       => __( 'Товар', 'usam' ),			
			'date'           => __( 'Дата добавления', 'usam' ),
        ); 
        return $columns;
    }
	
	public function get_number_columns_sql()
    {       
		return array('subtotal', 'total_items');
    }
	
	function prepare_items() 
	{			
		$args = array( 'cache_results' => false, 'cache_products' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby );
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{		
			$args = array_merge ($args, $this->get_date_for_query() );
		} 
		require_once( USAM_FILE_PATH . '/includes/basket/users_basket_query.class.php' );
		
		$query_orders = new USAM_Users_Basket_Query( $args );
		$this->items = $query_orders->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}
}