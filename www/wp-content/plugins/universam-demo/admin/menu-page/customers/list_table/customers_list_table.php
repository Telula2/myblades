<?php
class USAM_List_Table_Customers extends USAM_List_Table
{
    private $layers;
	function __construct( $args )
	{	
		parent::__construct( $args );		
    }	
		
	function no_items() {
		_e( 'Клиенты не найдены', 'usam' );
	}		
	
	function column_total_orders( $item ) 
    {	
		echo usam_currency_display( $item['total_orders'] ); 
    }
	
	function column_last_order( $item ) 
    {	
		echo usam_local_date( $item['last_order'] ); 
    }
	
	function column_bonus( $item ) 
    {	
		global $wpdb;		
		$user = get_user_by('email', $item['billingemail'] );	
		if ( $user->ID != NULL )
		{
			$bonus = $wpdb->get_var("SELECT SUM(bonus) FROM " .USAM_TABLE_CUSTOMER_BONUSES . " WHERE user_id = '$user->ID'");		
			echo $bonus;
		}			
    }  	
	
	public function extra_tablenav( $which ) 
	{		
		if ( 'top' == $which )
		{			
					
		}
	}
   
	function get_sortable_columns()
	{
		$sortable = array(
			'billingfirstname' => array('billingfirstname', true),
			'billinglastname'  => array('billinglastname', false),
			'billingemail'     => array('billingemail', false),
			'total_orders'     => array('total_orders', false),
			'count_orders'     => array('count_orders', false),
			'discount'         => array('discount', false),
			'last_order'       => array('last_order', false),
			'order_total'      => array('order_total', false),
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(  			
			'billingfirstname' => __( 'Имя', 'usam' ),
			'billinglastname'  => __( 'Фамилия', 'usam' ),	
			'billingemail'     => __( 'Электронная почта', 'usam' ),						
			'last_order'       => __( 'Последний заказ', 'usam' ),			
			'order_total'      => __( 'Последняя сумма', 'usam' ),	
			'count_orders'     => __( 'Заказов', 'usam' ),	
			'total_orders'     => __( 'Всего куплено', 'usam' ),			
			'bonus'            => __( 'Бонусы', 'usam' )				
        );
        return $columns;
    }	
	
	public function get_number_columns_sql()
    {       
		return array('order_total', 'count_orders', 'total_orders', 'bonus');
    }
	
	function prepare_items() 
	{		
		require_once( USAM_FILE_PATH . '/includes/customer/customer.class.php'    );
		
		$customer = new USAM_Customer();
		$this->items = $customer->get_data();
		$this->total_items = count( $this->items );
		
		$this->forming_tables();		
	}
}