<?php		
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_bonuses extends USAM_Edit_Form
{
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = __('Изменить бонус','usam');
		else
			$title = __('Добавить бонус', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{			
		if ( $this->id !== null )
		{
			$this->data = usam_get_bonus( $this->id );
		}
		else	
			$this->data = array( 'id' => 0,  'user_id' => 0, 'order_id' => '', 'payment_order_id' => 0, 'bonus' => '', 'status' => 0, 'type' => 0, 'use_date' => 0);
	}	
	
	function settings()
	{		
		?>
		<table class="subtab-detail-content-table usam_edit_table">
			<tbody>				
				<tr> 
				   <td><label for='user_id'><strong><?php esc_html_e( 'Клиент', 'usam' ); ?>:</strong></label></td>
				   <td>
						<?php 
						$args = array( 'fields' => array( 'id','user_nicename','user_email'), 'sorderby' => 'user_nicename' );
						$users = get_users( $args );						
						 ?>
						<select id = "order_field_userid" class="chzn-select" name = "data[user_id]">
							<option value='0' <?php echo ($this->data['user_id'] == 0 ? 'selected="selected"' :"") ?>><?php esc_html_e( 'Гость', 'usam' ); ?></option>
							<?php				
							foreach ( $users as $user ) 
							{					
								?><option value='<?php echo $user->id; ?>' <?php echo ($this->data['user_id'] == $user->id ? 'selected="selected"':""); ?>><?php echo $user->user_nicename; ?></option><?php
							}
							?>
						</select>				
					</td>
				</tr>						
				<tr>
					<td><label for='order_id'><strong><?php esc_html_e( 'Заказ', 'usam' ); ?>:</strong></label></td>				
					<td>
						 <input type="text" style="width:100px" name="data[order_id]" value="<?php echo $this->data['order_id']; ?>" />
					</td>
				</tr>
				<tr>
					<td><label for='order_id'><strong><?php esc_html_e( 'Количество бонусов', 'usam' ); ?>:</strong></label></td>				
					<td>
						 <input type="text" style="width:100px" name="data[bonus]" value="<?php echo $this->data['bonus']; ?>" />
					</td>
				</tr>
				<tr>
					<td><label for='order_id'><strong><?php esc_html_e( 'Статус', 'usam' ); ?>:</strong></label></td>				
					<td>
						<?php $statuses = usam_get_statuses_bonus( ); ?>
						<select id = "order_field_userid" name = "data[status]">							
							<?php				
							foreach ( $statuses as $key => $status ) 
							{					
								?><option value='<?php echo $status['internalname']; ?>' <?php echo ($this->data['status'] == $status['internalname'] ? 'selected="selected"':""); ?>><?php echo $status['title']; ?></option><?php
							}
							?>
						</select>	
					</td>
				</tr>	
				<tr>
					<td><label for='order_id'><strong><?php esc_html_e( 'Тип', 'usam' ); ?>:</strong></label></td>				
					<td>
						<?php $types = usam_get_bonus_types( ); ?>
						<select id = "order_field_userid" name = "data[type]">						
							<?php				
							foreach ( $types as $key => $title ) 
							{					
								?><option value='<?php echo $key; ?>' <?php echo ($this->data['type'] == $key ? 'selected="selected"':""); ?>><?php echo $title; ?></option><?php
							}
							?>
						</select>	
					</td>
				</tr>				
			</tbody>
		</table>		
		<?php			
	}

	function display_left()
	{			
		usam_add_box( 'usam_settings', __('Настройки','usam'), array( $this, 'settings' ) );	
    }
}
?>