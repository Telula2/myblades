<?php
class USAM_Tab_Cart extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Корзины Ваших посетителей', 'usam'), 'description' => 'Здесь Вы можете посмотреть, что добавили Ваши посетители себе в корзину.' );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':					
				$ids = array_map( 'intval', $this->records );				
				$result = usam_delete_cart( $this->records );					
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ),$this->sendback );
			break;	
			case 'order':					
				foreach ( $this->records as $basket_id ) 
				{	
					$cart = new USAM_CART( $basket_id );					
					$order_id = $cart->save_order();					
					if ( $order )
					{
						$order = new USAM_Order( $order_id );	
						$args =  array(	
							'status'          => 'received',	
							'shipping'        => 0,					
						);					
						$order->set( $args );	
						$order->save();	
						
						$payment['sum'] = $order->get( 'totalprice' );		
						$document_number = $order->add_payment_history( $payment );
				
						wp_redirect( usam_get_url_order( $order_id ) );
						exit;
					}
				}
			break;			
		}	
	}	
}
