<?php
class USAM_Tab_Bonuses extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Список бонусов Ваших посетителей', 'usam'), 'description' => 'Здесь Вы можете посмотреть когда и какие бонусы получили Ваши покупатели.' );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
		
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{		
			case 'delete':		
				$i = 0;
				$ids = array_map( 'intval', $this->records );
				$in = implode( ', ', $ids );
				$wpdb->query( "DELETE FROM ".USAM_TABLE_CUSTOMER_BONUSES." WHERE id IN ($in)" );		
				$this->sendback = add_query_arg( array( 'deleted' => count( $this->records ) ), $this->sendback );
			break;	
			case 'save':	
				if ( !empty($_POST['data']) )
				{
					$data = stripslashes_deep($_POST['data']);
					if ( $this->id != null )
					{
						$data['id'] = $this->id;
						usam_update_bonus( $data );	
					}
					else
					{
						$this->id = usam_insert_bonus( $data );	
					}			
				}
			break;
		}	
	}
}