<?php
class USAM_Tab_Views extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Список товаров, которые просмотрели посетители', 'usam'), 'description' => __('Здесь вы можете посмотреть какие товары смотрели Ваши посетители. Сохраняются последние 20 товаров.', 'usam') );		
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}
