<?php
class USAM_Tab_Customers extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Список покупателей', 'usam'), 'description' => 'Здесь Вы можете посмотреть список Ваших посетителей, совершивших заказ хотябы один раз.' );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}