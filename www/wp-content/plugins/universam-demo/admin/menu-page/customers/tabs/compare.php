<?php
class USAM_Tab_Compare extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Список сравнений Ваших посетителей', 'usam'), 'description' => 'Здесь вы можете посмотреть какие товары Ваши покупатели сравнивали.' );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}
