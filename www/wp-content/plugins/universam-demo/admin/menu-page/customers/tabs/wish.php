<?php
class USAM_Tab_Wish extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Список желаний Ваших посетителей', 'usam'), 'description' => 'Здесь вы можете посмотреть какие товары добавили Ваши покупатели в свой список желаний.' );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		
	}	
}
