<?php
$license_page = new USAM_license_page();
class USAM_license_page
{	
	function __construct()
	{	
      	add_action( 'admin_menu', array($this, 'menu_page') );
		add_action( 'init', array($this, 'save_license') );
    }
	
	function menu_page() 
	{
		$license_page = add_submenu_page( 'index.php', __('Лицензия Вашего магазина', 'usam'), __('UNIVERSAM', 'usam'), 'level_7', 'usam-license', array( $this, 'display_license_page' ) );	
	
	}
	
	function save_license() 
	{						
		if ( !empty($_POST['submit_free_license']) ) 
		{
			$api = new Universam_API();
			$api->set_free_license( );
		}
		elseif ( isset($_POST['license_delete']) ) 
		{ 
			update_option( 'usam_license', array( 'status' => '', 'type' => '', 'license' => '', 'name' => ''  ) );		
		}
		else
		{
			if ( isset($_POST['usam_activation_key']) ) 
			{				
				$name = sanitize_text_field($_POST['usam_activation_name']);
				$key = sanitize_title($_POST['usam_activation_key']);
				
				update_option( 'usam_license', array( 'license' => strtoupper($key), 'name' => $name ) );					
				if ( !empty($key) ) 
				{					
					$api = new Universam_API();
					$api->registration( );						
				}
			}
		}	
	}	
		
	function display_license_page() 
	{ 	
		$license = (array)get_option ( 'usam_license','' );	
		if ( !empty($license['status']) && $license['status'] )
			$disabled = 'disabled="disabled"';
		else
			$disabled = '';			
		?>
		<div class='wrap'>
			<div class='metabox-holder'>
				<form method='post' action=''>	
					<h2><?php _e( 'Активация UNIVERSAM', 'usam' ); ?></h2>
					<?php 
					if ( !usam_license() )
					{
						?>
						<p><?php printf( esc_html__( 'Для полноценного использования интернет-магазина введите имя и ключ, который вы получили при покупке. Купить лицензию можно на сайте %s', 'usam' ),'<a href="http://wp-universam.ru/buy" target="_blank">wp-universam.ru</a>'); ?></p> <br /><br />
						<?php 
					}
					?>					
					<table>
						<tr>
							<td><label for='activation_name'><?php esc_html_e( 'Статус лицензии:', 'usam' ); ?></label></td>
							<td>
								<strong><?php echo usam_get_name_type_license(); ?></strong>
							</td>
						</tr>		
						<?php if ( !empty($license['date']) ) { ?>
						<tr>
							<td><label for='activation_name'><?php esc_html_e( 'Срок лицензии:', 'usam' ); ?></label></td>
							<td>
								<strong><?php echo usam_get_name_type_license(); ?>
								<?php 
									$day = round((strtotime($license['date']) - time())/(60*60*24));	
									echo sprintf( __( 'Срок лицензии истикает через %s дней.', 'usam' ), $day);
								?>		
								</strong>
							</td>
						</tr>	
						<?php } ?>						
						<tr>
							<td><label for='activation_name'><?php esc_html_e( 'Ваше имя или название компании:', 'usam' ); ?></label></td>
							<td><input class='text' type='text' size='40' value='<?php echo $license['name']; ?>' <?php echo $disabled; ?> name='usam_activation_name' id='activation_name' /></td>
						</tr>
						<tr>
							<td><label for='activation_key'><?php esc_html_e( 'Ключ:', 'usam' ); ?></label></td>
							<td><input class='text' type='text' size='40' value='<?php echo $license['license']; ?>' <?php echo $disabled; ?> name='usam_activation_key' id='activation_key' /></td>
						</tr>
						<tr>
							<td><input type='hidden' value='license' name='usam_admin_action' /></td>
							<td>										
								<?php 
								if ( empty($license['license']) ) 
								{
									?>								
									<input type='submit' class='button' value='<?php esc_html_e( 'Запросить бесплатную лицензию', 'usam' ); ?>' name='submit_free_license' />
									<input type='submit' class='button-primary' value='<?php esc_html_e( 'Активировать', 'usam' ); ?>' name='submit' />
									<?php 
								}
								else
								{
									?>								
									<input type='submit' class='button' value='<?php esc_html_e( 'Сбросить', 'usam' ); ?>' name='license_delete' />
									<?php 
								}	
								?>
							</td>
						</tr>					
					</table>							
					<?php wp_nonce_field('license_nonce','_wpnonce'); ?>
				</form>
			</div>		
		</div>

	<?php
	}	
}
?>