<?php
class USAM_Tab_accumulative extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Накопительные скидки', 'usam'), 'description' => __('Здесь вы можете настроить накопительные скидки.','usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );		
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}

	protected function callback_submit()
    {
		switch( $this->current_action )
		{
			case 'delete':						
				usam_delete_data( $this->records, 'usam_accumulative_discount' );								
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':	
				if ( !empty($_POST['name']) )
				{						
					if ( ! isset( $_POST['sum'] ) || ! isset( $_POST['discounts'] ) )
						return false;
					
					$new_rule['name'] = sanitize_text_field( stripslashes($_POST['name']));		
					$new_rule['active'] = !empty($_POST['active'])?1:0;	
					$new_rule['sort'] = isset($_POST['sort'])?(int)$_POST['sort']:100;	
								
					$new_rule['method'] = isset($_POST['method']) && $_POST['method'] == 'bonus'?'bonus':'price';			
								
					$new_rule['type_prices'] = isset($_POST['input-type_prices'])?$_POST['input-type_prices']:array();	

					$new_rule['start_date'] = usam_get_datepicker('from_interval');
					$new_rule['end_date'] = usam_get_datepicker('to_interval');		

					$new_rule['period'] = isset($_POST['period'])?substr($_POST['period'], 0,1):'u';	
					$new_rule['period_from_type'] = isset($_POST['period_from_type'])?substr($_POST['period_from_type'], 0,1):'u';
					$new_rule['period_from'] = isset($_POST['period_from'])?(int)$_POST['period_from']:100;							
								
					$layers = (array) $_POST['sum'];
					$discounts = (array) $_POST['discounts'];		
					$new_layer = array();
					if ( !empty($discounts) ) 
					{					
						foreach ($discounts as $key => $discount)	
							if ( is_numeric($discount) )
								$new_layer[$layers[$key]] = $discount;
						ksort( $new_layer );
						$new_rule['layers'] =  $new_layer;
					}			
					if ( $this->id != null )	
					{
						usam_edit_data( $new_rule, $this->id, 'usam_accumulative_discount' );	
					}
					else			
					{				
						$new_rule['date_insert'] = date( "Y-m-d H:i:s" );	
						$this->id = usam_add_data( $new_rule, 'usam_accumulative_discount' );				
					}				
					if ( !$this->data['active'] != $new_rule['active'] && $new_rule['method'] != $this->data['method'] )
					{
						$args = array( 'fields' => array( 'ID' ) );
						$user_ids = get_users( $args );
						$page = 1;
					
						usam_create_system_process( __("Пересчет накопительной скидки", "usam" ), $page, 'calculation_accumulative_discount_customer', count($user_ids), 'calculation_accumulative_discount' );
					}
				}	
			break;	
		}				
	}	
}