<?php
class USAM_Tab_rules_coupons extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Правила создания автоматических купонов', 'usam'), 'description' => 'Здесь Вы можете добавлять правила создания автоматических купонов. Купоны создаются по завершению заказа и отправляются на почту покупателю.' );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}

	protected function callback_submit()
	{	
		global $wpdb;			
		switch( $this->current_action )
		{
			case 'delete':
				$i = 0;
				$option = get_option('usam_coupons_roles', array() );
				$coupons_roles = maybe_unserialize( $option );
				foreach($this->records as $id )
				{
					$result = $wpdb->query("DELETE FROM ".USAM_TABLE_COUPON_CODES." WHERE id = '".$coupons_roles[$id]['coupon_id']."'");
					unset($coupons_roles[$id]);
					$i++;
				}
				update_option('usam_coupons_roles', serialize( $coupons_roles ) );				
				$this->sendback = add_query_arg( array( 'deleted' => $i ), $this->sendback );				
				$this->redirect = true;
			break;
			case 'save':						
				if ( isset( $_POST['coupons_role'] ) ) 
				{					
					$new_rule = stripslashes_deep($_REQUEST['coupons_role']);	
					$new_rule['title'] = sanitize_text_field(stripslashes($_POST['name']));
					$new_rule['active'] = !empty($_POST['active'])?1:0;
					$new_rule['roles'] = isset($_REQUEST['input-roles'])?stripslashes_deep($_REQUEST['input-roles']):array();
					$new_rule['locations'] = isset($_POST['locations'])?array_map('intval', $_POST['locations']):array();
					
					$coupon = array();
					$coupon['use_once']      = (int)(bool)$_POST['use_once'];	
					$coupon['amount_bonuses_author'] = (int)$_POST['amount_bonuses_author'];		
					$coupon['condition'] = $this->get_rules_basket_conditions( );	
					
					$role = usam_get_data($this->id, 'usam_coupons_roles');
					if ( $this->id != null )	
					{
						$_coupon = new USAM_Coupon( $role['coupon_id'] );
						$_coupon->set( $coupon );
						$_coupon->save();	
		
						usam_edit_data( $new_rule, $this->id, 'usam_coupons_roles' );	
					}
					else			
					{			
						$coupon['coupon_code']   = '888';	
						$coupon['value']         = 0;
						$coupon['is_percentage'] = 0;
						$coupon['description']   = '';			
						$coupon['customer']      = 0;		
						$coupon['coupon_type']   = 'rule';

						$_coupon = new USAM_Coupon();
						$_coupon->set($coupon);
						$_coupon->save();						
		
						$new_rule['coupon_id'] = $_coupon->get('id');	
						$new_rule['date_insert'] = date( "Y-m-d H:i:s" );
						
						$this->id = usam_add_data( $new_rule, 'usam_coupons_roles' );							
					}						
				} 
			break;				
		}		
	}	
}