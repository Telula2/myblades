<?php
class USAM_Tab_discount extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Скидки на товар', 'usam'), 'description' => __('Здесь Вы можете добавлять правила скидок на товар.','usam') );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
    {			
		switch( $this->current_action )
		{
			case 'delete':						
				usam_delete_data( $this->records, 'usam_product_discount_rules' );								
				
				usam_recalculate_price_products();
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;				
			break;
			case 'save':
				if ( !empty($_POST['name']) )
				{						
					require_once( USAM_FILE_PATH . '/admin/includes/rules/product_discount_rules.class.php' );	
					
					$new_rule['name'] =  sanitize_text_field( stripslashes($_POST['name']));
					$new_rule['description'] = sanitize_textarea_field( stripslashes($_POST['description']));				
					$new_rule['active'] = !empty($_POST['active'])?1:0;
					$new_rule['priority'] = isset($_POST['priority'])?(int)$_POST['priority']:1;	
					$new_rule['end'] = !empty($_POST['end'])?1:0;
								
					$new_rule['type_prices'] = isset($_POST['input-type_prices'])?$_POST['input-type_prices']:array();	

					$new_rule['start_date'] = usam_get_datepicker('start');
					$new_rule['end_date'] = usam_get_datepicker('end');	
					
					$new_rule['discount'] = (int)$_POST['discount'];
					$new_rule['dtype'] = sanitize_title($_POST['dtype']);
						
					$product_discount_rules = new USAM_Product_Discount_Rules( );	
					$new_rule['conditions'] = $product_discount_rules->get_rules_basket_conditions(  );	
					$current_time = time();	
					
					if ( $this->id != null )	
					{
						if ( $this->data['active'] != $new_rule['active'] && (( empty($new_rule['start_date']) || strtotime($new_rule['start_date']) <= $current_time ) && ( empty($new_rule['end_date']) || strtotime($new_rule['end_date']) >= $current_time ) ))
							$recalculate = true;
						
						$recalculate = true;
						usam_edit_data( $new_rule, $this->id, 'usam_product_discount_rules' );					
					}
					else			
					{				
						$new_rule['process'] = 0;	
						if ( $new_rule['active'] && ( empty($new_rule['start_date']) || strtotime($new_rule['start_date']) <= $current_time ) && ( empty($new_rule['end_date']) || strtotime($new_rule['end_date']) >= $current_time ) )
							$recalculate = true;
						
						$new_rule['date_insert'] = date( "Y-m-d H:i:s" );						
						$this->id = usam_add_data( $new_rule, 'usam_product_discount_rules' );				
					}
					if ( $recalculate )
						usam_recalculate_price_products();
				}
			break;
			case 'activate':						
				$new_rule['active'] = 1;  
				foreach ( $this->records as $id )
				{				
					usam_edit_data( $new_rule, $id, 'usam_product_discount_rules' );							
				}
				usam_recalculate_price_products();
				$this->sendback = add_query_arg( array( 'update' => count($this->records) ), $this->sendback );				
				$this->redirect = true;		
			break;	
			case 'deactivate':				
				$new_rule['active'] = 0;		
				foreach ( $this->records as $id )
				{				
					usam_edit_data( $new_rule, $id, 'usam_product_discount_rules' );							
				}	
				usam_recalculate_price_products();
				$this->sendback = add_query_arg( array( 'update' => count($this->records) ), $this->sendback );				
				$this->redirect = true;						
			break;				
		}	
	}
}