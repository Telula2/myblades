<?php
class USAM_Tab_basket extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Правила скидок для корзин', 'usam'), 'description' => __('Здесь Вы можете добавлять правила скидок для корзин покупателей, например, скидку на количество товаров в корзине или общую стоимость. Доступно десятки параметров и тысячи комбинаций', 'usam') );		
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}
	
	public function add_meta_options_help_center_tabs( ) 
	{
		$tabs[] = new USAM_Help_Center_Item( 'basic-help', __( 'Создание скидок', 'usam' ), __( 'Создание скидок', 'usam' ),array( 'content' => '<p>' .__( '.', 'usam' ) . '</p>', ) );
		return $tabs;
	}
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{		
		switch( $this->current_action )
		{
			case 'delete':				
				usam_delete_data( $this->records, 'usam_rules_discounts_shopping_cart' );			
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;	
			case 'save':		
				$start_date = usam_get_datepicker('start');
				$end_date = usam_get_datepicker('end');
				$conditions = $this->get_rules_basket_conditions(  );
					
				$new = array(	
					'title'            => sanitize_text_field(stripslashes($_POST['name'])),		
					'description'      => sanitize_textarea_field(stripslashes($_POST['description'])),					
					'code'             => sanitize_title($_POST['code']),					
					'active'           => 0,	
					'end'              => isset($_POST['end'])?1:0,				
					'priority'         => (int)$_POST['priority'],
					'type'             => sanitize_title($_POST['type']),				
					'value'            => (int)$_POST['value'],
					'value_type'       => isset($_POST['value_type'])&& $_POST['value_type'] == 'f'?'f':'p',					
					'start_date'       => $start_date,
					'end_date'         => $end_date,							
					'condition'        => $conditions,						
					'date_update'      => date( 'd-m-Y H:i:s' ),					
				);
				
				if ( !empty($_POST['gift']) && $_POST['type'] == 'g' )
					$new['gift'] = $_POST['gift'];	
				else
					$new['gift'] = array();
							
				if ( $this->id != null )		
					usam_edit_data( $new, $this->id, 'usam_rules_discounts_shopping_cart' );	
				else			
				{
					$item['date_insert'] = date( 'Y-m-d H:i:s' );	
					$this->id = usam_add_data( $new, 'usam_rules_discounts_shopping_cart' );
				}		
			break;								
		}
	}		
}
?>