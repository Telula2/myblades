<?php
class USAM_Tab_product_day extends USAM_Tab
{		
	public function __construct()
	{
		$link = '<a target="_blank" href="'.usam_get_url_system_page('special-offer').'">'.__('здесь','usam').'</a>';
		$this->header = array( 'title' => __('Товар дня', 'usam'), 'description' =>  sprintf(__('Здесь вы можете выбирать ТОВАР ДНЯ. Посмотреть страницу с товаром дня можно %s. Для получения подробной информации воспользуйтесь кнопкой помощи.', 'usam'), $link ) );
		$this->buttons = array( 'add' => __('Добавить', 'usam') );
	}	
	
	protected function callback_submit()
	{				
		global $wpdb;
		switch( $this->current_action )
		{			
			case 'delete':			
				$ids = array_map( 'intval', $this->records );
				usam_delete_data( $ids, 'usam_product_day_rules' );			
				
				$products_ids = usam_get_products_day( array( 'rule_id' => $ids, 'status' =>  array( 1 ), 'fields' => 'product_id' ) );		
				if ( !empty($products_ids) )
				{
					usam_recalculate_price_products_ids( $products_ids );					
					wp_cache_delete( 'usam_active_products_day' );	
				}
				$result = $wpdb->query( "DELETE FROM ".USAM_TABLE_PRODUCT_DAY." WHERE rule_id IN (".implode(',',$ids).")" );					
		
				$this->sendback = add_query_arg( array( 'deleted' => count($ids) ), $this->sendback );				
				$this->redirect = true;				
			break;	
			case 'save':
				if ( !empty($_POST['name']) )
				{									
					
					
					$new_rule['name'] =  sanitize_text_field(stripslashes($_POST['name']));		
					$new_rule['active'] = !empty($_POST['active'])?1:0;		
					$new_rule['refill'] = !empty($_POST['refill'])?1:0;	

			//	$pday->refill_the_queue_product_day();				
					
					if (  isset($_POST['input-type_prices']) )
					{
						$new_rule['type_prices'] = $_POST['input-type_prices'];	
					}
					else
					{
						$prices = usam_get_prices( );	
						foreach ( $prices as $type_price ) 
						{
							$new_rule['type_prices'][] = $type_price['code'];
						}
					}
					$new_rule['start_date'] = usam_get_datepicker('start');
					$new_rule['end_date'] = usam_get_datepicker('end');	
								
					$new_rule['conditions']['pricemin'] = (float)$_POST['pricemin'];
					$new_rule['conditions']['pricemax'] = (float)$_POST['pricemax'];			
					$new_rule['conditions']['minstock'] = absint($_POST['minstock']);
					$new_rule['conditions']['value']    = absint($_POST['value']);		
					$new_rule['conditions']['c']        = absint($_POST['c']);
					$new_rule['conditions']['terms']['category'] = isset($_POST['tax_input']['usam-category'])?array_map('intval', $_POST['tax_input']['usam-category']):array();
					$new_rule['conditions']['terms']['brands'] = isset($_POST['tax_input']['usam-brands'])?array_map('intval', $_POST['tax_input']['usam-brands']):array();	
												
					if ( $this->id != null )	
					{						
						$update_product = array();
						if (  isset($_POST['products']) )
						{					
							foreach ( $_POST['products'] as $id => $discount ) 
							{							
								$update_product[$id] = $discount;									
							}		
						}				
						$products_day = usam_get_products_day( array( 'rule_id' => $this->id, 'status' => array( 0, 1 ) ) );
						if (  isset($_POST['ids']) )
						{ 					
							$ids = array_map('intval', $_POST['ids']);	
							$sort = 1;
							$products = array();
							foreach ( $products_day as $product ) 
							{
								if ( !in_array($product->id, $ids) )						
									usam_delete_product_day( $product->id );					
							}				
							foreach ( $ids as $id ) 
							{						
								$sort++;
								$update_product[$id]['sort'] = $sort;		
								usam_update_product_day( $id, $update_product[$id] );		
							}				
						}	
						else
						{
							foreach ( $products_day as $product ) 
								usam_delete_product_day( $product->id );
						}									
						usam_edit_data( $new_rule, $this->id, 'usam_product_day_rules' );					
					}
					else			
					{					
						$new_rule['date_insert'] = date( "Y-m-d H:i:s" );											
						$this->id = usam_add_data( $new_rule, 'usam_product_day_rules' );
					}
					if ( isset($_POST['new_products']) )
					{				
						foreach ( $_POST['new_products'] as $product_id => $discount ) 
						{						
							usam_insert_product_day( array( 'rule_id' => $this->id, 'product_id' => absint($product_id), 'value' => $discount['value'], 'dtype' => $discount['dtype'] ) ); 
						}				
					}			
					$pday = new USAM_Work_Product_Day();
					$pday->refill_the_queue_by_rule_id( $this->id );
					$pday->set_product_day_by_rule_id( $this->id );		
				}
			break;			
		}	
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
}