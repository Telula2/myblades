<?php
class USAM_Tab_Bonuses extends USAM_Tab
{
	public function __construct()
	{
		$this->header = array( 'title' => __('Правила начисления бонусов', 'usam'), 'description' => 'Здесь Вы можете добавлять правила начисления бонусов покупателям по завершению их заказа.' );	
		$this->buttons = array( 'add' => __('Добавить', 'usam') );	
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
	
	protected function callback_submit()
	{				
		switch( $this->current_action )
		{
			case 'delete':			
				usam_delete_data( $this->records, 'usam_bonuses_rules' );								
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;	
			break;
			case 'save':					
				$new_rule['name'] =  sanitize_text_field(stripslashes($_POST['name']));
				$new_rule['description'] = sanitize_textarea_field(stripslashes($_POST['description']));				
				$new_rule['active'] = !empty($_POST['active'])?1:0;
				$new_rule['start_date'] = usam_get_datepicker('from_interval');
				$new_rule['end_date'] = usam_get_datepicker('to_interval');		
				$new_rule['value'] = (int)$_POST['value'];		
				$new_rule['type'] = sanitize_title($_POST['type']);	
				
				if ( $this->id != null )	
				{					
					usam_edit_data( $new_rule, $this->id, 'usam_bonuses_rules' );					
				}
				else			
				{				
					$new_rule['date_insert'] = date( "Y-m-d H:i:s" );					
					$this->id = usam_add_data( $new_rule, 'usam_bonuses_rules' );				
				}	
			break;
		}		
	}	
}