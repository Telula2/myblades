<?php
class USAM_Tab_Coupons extends USAM_Tab
{	
	protected  $form_method = 'POST';
	
	public function __construct()
	{		
		$this->header = array( 'title' => __('Купоны', 'usam'), 'description' => 'Здесь Вы можете добавлять купоны' );	
		$this->buttons = array( 'add' => __('Добавить', 'usam'), 'generate' => __('Генерировать', 'usam') );
	}		
	
	protected function callback_submit()
	{				
		global $wpdb;			
		switch( $this->current_action )
		{
			case 'delete':			
				$result = $wpdb->query("DELETE FROM ".USAM_TABLE_COUPON_CODES." WHERE id IN ('".implode("','",$this->records)."')");						
				$this->sendback = add_query_arg( array( 'deleted' => count($this->records) ), $this->sendback );				
				$this->redirect = true;
			break;		
			case 'activate':							
				$result = $wpdb->query("UPDATE ".USAM_TABLE_COUPON_CODES." SET `active`='1' WHERE id IN ('".implode("','",$this->records)."')");			
				$this->sendback = add_query_arg( array( 'activate' => count($this->records) ), $this->sendback );			
				$this->redirect = true;
			break;			
			case 'deactivate':
				$result = $wpdb->query("UPDATE ".USAM_TABLE_COUPON_CODES." SET `active`='0' WHERE id IN ('".implode("','",$this->records)."')");			
				$this->sendback = add_query_arg( array( 'deactivate' => count($this->records) ), $this->sendback );			
				$this->redirect = true;
			break;
			case 'save':						
				if ( isset( $_POST['coupon'] ) ) 
				{					
					$insert = stripslashes_deep($_POST['coupon']);			
					$insert['description']   = sanitize_textarea_field($_POST['description']);	
					$insert['start']         = usam_get_datepicker('start');
					$insert['expiry']        = usam_get_datepicker('end');	
					$insert['condition']     = $this->get_rules_basket_conditions(  );
					$insert['active']        = !empty($_POST['active'])?1:0;						
					if ( $this->id != null )			
						$_coupon = new USAM_Coupon( $this->id );			
					else
						$_coupon = new USAM_Coupon();
					$_coupon->set( $insert );
					$_coupon->save();					
					$this->id = $_coupon->get('id');
				}
			break;		
			case 'generate':						
				if ( isset( $_POST['coupon'] ) ) 
				{					
					$insert = stripslashes_deep($_POST['coupon']);			
					$insert['description']   = sanitize_textarea_field($_POST['description']);	
					$insert['start']         = usam_get_datepicker('start');
					$insert['expiry']        = usam_get_datepicker('end');	
					$insert['condition']     = $this->get_rules_basket_conditions(  );
					$insert['active']        = !empty($_POST['active'])?1:0;	

					$quantity    = !empty($_POST['quantity'])?absint($_POST['quantity']):0;		
					$format    = !empty($_POST['format'])?sanitize_text_field($_POST['format']):'';		
					$type_format = !empty($_POST['type_format'])?sanitize_title($_POST['type_format']):'ln';							
						
					
					$ids = array();
					for ($i=0; $i < $quantity; $i++)
					{					
						$insert['coupon_code'] = usam_generate_coupon_code( $format, $type_format );						
						$_coupon = new USAM_Coupon( $insert );
						$_coupon->save();					
						$ids[] = $_coupon->get('id');
					}
					$this->sendback = add_query_arg( array( 'ready' => count($ids) ), $this->sendback );			
					$this->redirect = true;					
				}
			break;		
		}			
		if (isset($_POST['edit_coupon']) && isset($_POST['submit_coupon']))		
			foreach ( (array)$_POST['edit_coupon'] as $coupon_id => $coupon_data )
			{							
				$coupon_data['start']  = usam_get_datepicker( 'start_'.$coupon_id );
				$coupon_data['expiry'] = usam_get_datepicker( 'end_'.$coupon_id );				
			
				$_coupon = new USAM_Coupon( $coupon_id );
				$_coupon->set( $coupon_data );
				$_coupon->save();				
			}						
	}	
	
	protected function load_tab()
	{
		$this->list_table();
	}
}