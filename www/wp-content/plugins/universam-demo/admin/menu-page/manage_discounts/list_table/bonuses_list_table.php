<?php
class USAM_List_Table_Bonuses extends USAM_List_Table 
{
    function __construct( $args = array() )
	{	
		parent::__construct( $args );			
    }	
	
	function column_type($item)
	{		
		switch( $item['type'] )
		{
			case 1:
				_e( 'При регистрации', 'usam' );
			break;
			case 2:
				_e( 'Автору отзыва', 'usam' );
			break;
			case 3:
				_e( 'При покупки товара', 'usam' );
			break;			
		}
	}			
	
	function column_name( $item )
	{	
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );	
	}	
	
   	
	function get_sortable_columns() 
	{
		$sortable = array(
			'active'  => array('active', false),
			'start_date' => array('start_date', false),
			'end_date'   => array('end_date', false),			
			);
		return $sortable;
	}
		
	function get_columns(){
        $columns = array(           
			'cb'              => '<input type="checkbox" />',		
			'name'            => __('Название правила', 'usam' ),
			'active'          => __('Активность', 'usam' ),
			'type'            => __('Когда начислять бонусы', 'usam' ),
			'amount'          => __('Количество', 'usam' ),			
			'start_date'      => __('Начало', 'usam' ),
			'end_date'        => __('Окончание', 'usam' )
        );
        return $columns;
    }
	
	function prepare_items() 
	{			
		$option = get_option('usam_bonuses_rules', array() );
		$rules = maybe_unserialize( $option );		
		$this->items = array();
		if ( !empty($rules) )
			foreach( $rules as $rule )
			{			
				if ( empty($this->records) || !empty($this->records) && in_array($rule['id'], $this->records))
				{
					$this->items[] = $rule;	
				}	
			}		
		$this->total_items = count($this->items);
		$this->forming_tables();
	}
}