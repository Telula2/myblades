<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/manage_discounts/includes/coupons_list_table.php' );
class USAM_List_Table_coupons extends USAM_List_Table_Standart_Coupons
{	   
	protected $currency_sign;
	function __construct( $args = array() )
	{	
		$this->currency_sign = usam_get_currency_sign();
		parent::__construct( $args );
    }	
   	
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'        => array('name', false),
			'active'      => array('active', false),
			'discount'    => array('value', false),
			'is_used'     => array('is_used', false),
			'start'       => array('start', false),	
			'expiry'      => array('expiry', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',				
			'coupon_code'    => __( 'Код купона', 'usam' ),
			'active'         => __( 'Активный', 'usam' ),
			'discount'       => __( 'Скидка', 'usam' ),
			'start'          => __( 'Начало', 'usam' ),
			'expiry'         => __( 'Конец', 'usam' ),			
			'use_once'       => __( 'Использовать один раз', 'usam' ),	
			'is_used'        => __( 'Использован', 'usam' ),			
			'customer'       => __( 'Владелец', 'usam' ),		
			'description'    => __( 'Описание', 'usam' ),		
			'date_insert'    => __( 'Создан', 'usam' ),			
        );		
        return $columns;
    }	
	
	function column_customer( $item ) 
	{	
		echo usam_get_customer_name( $item->customer );		
	}

	protected function coupon_editor( $item ) 
	{	
		if ( $this->is_bulk_edit )
			$style = ' style="display:table-row;"';
		else
			$style = ' style="display:none;"';				
		$colspan = count( $this->get_columns() );			
		?>
		<tr class="usam-coupon-editor-row inline-edit-row"<?php echo $style; ?> id="usam-coupon-editor-row-<?php echo $item->id; ?>">			
			<td colspan="<?php echo $colspan; ?>" class="colspanchange">
				<h4><?php esc_html_e( 'Редактировать купон', 'usam' ); ?></h4>
				<?php $this->coupon_edit_form( $item ); ?>
			</td>
		</tr>
		<?php	
	}		
	
	protected function coupon_edit_form( $coupon )
	{		
		$conditions = maybe_unserialize($coupon->condition);	
		$id = $coupon->id;
		$output = '';	
		$output .= "<table class='edit_coupon'>\n\r";
		$output .= " <tr>\n\r";
		$output .= "   <th>".esc_html__('Код купона', 'usam')."</th>\n\r";
		$output .= "   <th>".esc_html__('Скидка', 'usam')."</th>\n\r";
		$output .= "   <th>".esc_html__('Начало', 'usam')."</th>\n\r";
		$output .= "   <th>".esc_html__('Истечение', 'usam')."</th>\n\r";
		$output .= "   <th>".esc_html__('Один раз', 'usam')."</th>\n\r";
		$output .= "   <th>".esc_html__('Активный', 'usam')."</th>\n\r";	
		$output .= "   <th>".esc_html__('Начислять бонусы', 'usam')."</th>\n\r";		
		$output .= "   <th></th>\n\r";
		$output .= " </tr>\n\r";
		$output .= " <tr>\n\r";
		$output .= "  <td>\n\r";
		$output .= "   <input type='hidden' value='true' name='is_edit_coupon' />\n\r";
		$output .= "   <input type='text' size='11' value='".$coupon->coupon_code."' name='edit_coupon[".$id."][coupon_code]' />\n\r";
		$output .= "  </td>\n\r";
		$output .= "  <td>\n\r";

		$output .= "   <input type='text' size='9' value='".$coupon->value."'  name=edit_coupon[".$id."][value]' style ='width:200px;'/>";
		$output .= "   <select name='edit_coupon[".$id."][is_percentage]' class='select_type_md'>";
		$output .= "     <option value='0' ".selected($coupon->is_percentage,0,false)." >".esc_html( $this->currency_sign )."</option>\n\r";
		$output .= "     <option value='1' ".selected($coupon->is_percentage,1,false)." >%</option>\n\r";
		$output .= "     <option value='2' ".selected($coupon->is_percentage,2,false)." >" . esc_html__( 'Бесплатная доставка', 'usam' ) . "</option>\n\r";
		$output .= "   </select>\n\r";
		$output .= "  </td>\n\r";
		$output .= "  <td>\n\r";
		$output .= usam_get_display_datetime_picker( 'start_'.$coupon->id, $coupon->start );
		$output .= "  </td>\n\r";		
		$output .= "  <td>\n\r";
		$output .= usam_get_display_datetime_picker( 'end_'.$coupon->id, $coupon->expiry );
		$output .= "  </td>\n\r";
		$output .= "  <td>\n\r";
		$output .= "   <input type='hidden' value='0' name='edit_coupon[".$id."][use_once]' />\n\r";
		$output .= "   <input type='checkbox' value='1' ".(($coupon->use_once == 1) ? "checked='checked'" : '')." name='edit_coupon[".$id."][use_once]' />\n\r";
		$output .= "  </td>\n\r";
		$output .= "  <td>\n\r";
		$output .= "   <input type='hidden' value='0' name='edit_coupon[".$id."][active]' />\n\r";
		$output .= "   <input type='checkbox' value='1' ".(($coupon->active == 1) ? "checked='checked'" : '')." name='edit_coupon[".$id."][active]' />\n\r";
		$output .= "  </td>\n\r";	
		$output .= "  <td>\n\r";
		$output .= "   <input type='hidden' value='".$id."' name='edit_coupon[".$id."][id]' />\n\r";
		$output .= "  </td>\n\r";
		$output .= " </tr>\n\r";
		$output .= "<tr>";		
		$output .= "<tr>";
		$output .= "<td colspan='10'>\n\r";	
		$output .= "<textarea class = 'coupon_description' name='edit_coupon[".$id."][description]' cols='5' rows='2'>".$coupon->description."</textarea>";
		$output .= "</td>\n\r";
		$output .= "</tr>";	
		$output .= '<tr><td colspan="10"><input type="submit" value="'.esc_attr__("Обновить купон", "usam").'" class="button-primary save alignright" name="submit_coupon" /></td></tr>';			
		$output .= "</table>\n\r";	
		echo $output;		
	}
}