<?php
require_once( USAM_FILE_PATH . '/admin/menu-page/manage_discounts/includes/coupons_list_table.php' );
class USAM_List_Table_Certificates extends USAM_List_Table_Standart_Coupons
{	  
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
		
		$this->coupon_type = 'certificate';
    }	

	function column_coupon_code( $item ) 
    {
		$actions = $this->standart_row_actions( $item->id );	
		$this->row_actions_table( $item->coupon_code, $actions );	
	}		
	
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'        => array('name', false),
			'active'      => array('active', false),
			'discount'    => array('value', false),
			'is_used'     => array('is_used', false),
			'start'       => array('start', false),	
			'expiry'      => array('expiry', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',				
			'coupon_code'    => __( 'Код сертификата', 'usam' ),
			'active'         => __( 'Активный', 'usam' ),
			'discount'       => __( 'Скидка', 'usam' ),
			'start'          => __( 'Начало', 'usam' ),
			'expiry'         => __( 'Конец', 'usam' ),			
			'is_used'        => __( 'Использован', 'usam' ),			
			'user_id'        => __( 'Владелец', 'usam' ),				
			'date_insert'     => __( 'Создан', 'usam' ),			
        );		
        return $columns;
    }
}