<?php
class USAM_List_Table_basket extends USAM_List_Table 
{	
	private $is_bulk_edit = false;
	protected $orderby = 'id';
	
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }	
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),
			'activate'  => __( 'Активировать', 'usam' ),
			'deactivate' => __( 'Отключить', 'usam' )
		);
		return $actions;
	}
	
	function get_views() 
	{	
		global $wpdb;		
				
		$option = get_option('usam_rules_discounts_shopping_cart', array() );
		$rules = maybe_unserialize( $option );		
		$status_sum = array( 'yes' => 0, 'no' => 0 );
		if ( !empty($rules) )
			foreach( $rules as $rule )
			{			
				if ( $rule['active'] )
					$status_sum['yes']++;
				else
					$status_sum['no']++;
			}
		$total_sum = count($rules);
		$sendback = remove_query_arg( array('active') );
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['active'])) ?  'class="current"' : '' ).">". __('Все правила','usam')." <span class='count'> ( $total_sum )</span></a>";	
		$sendback = add_query_arg( array( 'active' => 'yes' ) );
		$views['yes'] = "<a href='$sendback' ". (( isset ($_GET['active']) && $_GET['active'] == 'yes' ) ?  'class="current"' : '' ).">". __('Активные','usam')." <span class='count'> (". $status_sum['yes'].")</span></a>";
		$sendback = add_query_arg( array( 'active' => 'no' ) );
		$views['no'] = "<a href='$sendback' ". (( isset ($_GET['active']) && $_GET['active'] == 'no' ) ?  'class="current"' : '' ).">". __('Отключенные','usam')." <span class='count'> (". $status_sum['no'].")</span></a>";
		return $views;
	}		
			
	function column_title( $item ) 
    {
		$this->row_actions_table( $item['title'], $this->standart_row_actions( $item['id'] ) );	
	}	
	
	function column_discount( $item ) 
    {			
		switch ( $item['value_type'] ) 
		{
			case 'f':	
				echo usam_currency_display( $item['value'] );		
			break;
			case 'p':	
				echo $item['value'].'%';		
			break;				
		}	
	}
	
	function column_type( $item ) 
    {			
		switch ( $item['type'] ) 
		{
			case 'p':	
				esc_html_e( 'Изменить цену товара', 'usam' );
			break;
			case 'b':	
				esc_html_e( 'Добавить бонусы', 'usam' );
			break;	
			case 's':	
				esc_html_e( 'Изменить доставку', 'usam' );
			case 'g':	
				esc_html_e( 'Добавить подарок', 'usam' );				
			break;				
		}	
	}
		
	function column_logical( $item ) 
    {			
		switch ( $item['logical'] ) 
		{
			case 'and':	
				esc_html_e( 'Исключить', 'usam' );
			break;
			case 'or':	
				esc_html_e( 'Суммировать', 'usam' );
			break;				
		}	
	}
		
	
	function column_end_date( $item ) 
    {		
		if ( !empty($item['end_date']) )
		{
			$num_days = ceil((time() - strtotime($item['end_date']))/86400);
			$num_days_start = ceil((time() - strtotime($item['end_date']))/86400);
			$num = abs( $num_days );
			$message = '';
			if ( $num_days_start > 0 ) 
			{
				if ( $num_days < 0 ) 
					$message = sprintf( __('До конца действия %s','usam'), $num );
				elseif ( $num_days == 0 ) 
					$message = sprintf( __('Заканчивается сегодня','usam'), $num );
				else
					$message = sprintf( _n( 'Закончилась %s день назад', 'Закончилась %s дней назад', $num, 'usam' ), $num );
			}
			echo date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($item['end_date'], "Y-m-d H:i:s")));
			echo "<br><strong>{$message}</strong>";		
		}
		else
			_e('Бессрочная','usam');
	}
	
	function column_start_date( $item ) 
    {
		if ( !empty($item['end_date']) )
		{
			$num_days = ceil((time() - strtotime($item['start_date']))/86400);
			$num = abs( $num_days );	
		
			echo date_i18n("d.m.Y H:i", strtotime(get_date_from_gmt($item['start_date'], "Y-m-d H:i:s")));
			if ( $num_days < 0) 
			{
				$message = sprintf( _n( 'До начала действия остался %s день', 'До начала действия осталось %s дней', $num, 'usam' ), $num );		
				echo "<br><strong>{$message}</strong>";				
			}
			else
				echo "<br><strong>".__('Уже работает','usam')."</strong>";				
		}
		else
			_e('Уже работает','usam');
	} 
   	
	function get_sortable_columns() 
	{
		$sortable = array(
			'title'       => array('title', false),
			'active'      => array('active', false),
			'logical'     => array('logical', false),
			'discount'    => array('value', false),
			'start_date'  => array('start_date', false),	
			'end_date'    => array('end_date', false),	
			'date'        => array('date_insert', false),				
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',	
			'title'          => __( 'Название правила', 'usam' ),
			'active'         => __( 'Активный', 'usam' ),
			'priority'       => __( 'Приоритет', 'usam' ),
			'discount'       => __( 'Скидка', 'usam' ),
			'start_date'     => __( 'Начало', 'usam' ),
			'end_date'       => __( 'Конец', 'usam' ),
			'type'           => __( 'Выполнить действие', 'usam' ),	
			'date'     => __( 'Дата создания', 'usam' ),	
        );		
        return $columns;
    }
	
	public function get_number_columns_sql()
    {       
		return array('value', 'use_once');
    }
	
	function prepare_items() 
	{		
		$option = get_option('usam_rules_discounts_shopping_cart', array() );
		$rules = maybe_unserialize( $option );				
		
		if (empty($rules))
			$this->items = array();
		else
			foreach ($rules as $id => $rule ) 
			{		
				if ( empty($this->records) || in_array($rule['id'], $this->records))
				{
					if ( isset($_GET['active']) && ( $_GET['active'] == 'yes' && $rule['active'] == 0 || $_GET['active'] == 'no' && $rule['active'] == 1 ) )
					{
						continue;
					}
					$this->items[] = $rule;	
				}	
			}	
		$this->total_items = count($this->items);
		$this->forming_tables();	
	}	
}