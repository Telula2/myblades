<?php
class USAM_List_Table_accumulative extends USAM_List_Table 
{
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}	
	
	function column_name( $item ) 
    {		
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );	
	}	
	
	function column_start_date( $item )
    {		
		echo usam_local_date( $item['start_date'] );	
    }   
	
	function column_end_date( $item )
    {			
		echo usam_local_date( $item['end_date'] );	
    }  
			 
	function get_sortable_columns()
	{
		$sortable = array(
			'title'   => array('name', false),		
			'status'  => array('status', false),		
			'date'    => array('date_insert', false),		
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'          => '<input type="checkbox" />',
			'name'        => __( 'Название правила', 'usam' ),
			'active'      => __( 'Активность', 'usam' ),
			'type_prices' => __( 'Типы цен', 'usam' ),			
			'start_date'  => __( 'Время активации', 'usam' ),
			'end_date'    => __( 'Время деактивации', 'usam' ),
			'date'        => __( 'Дата создания', 'usam' ),		
        );		
        return $columns;
    }
	
	function prepare_items() 
	{			
		$option = get_option('usam_accumulative_discount', array() );
		$rules = maybe_unserialize( $option );		
		$this->items = array();
		if ( !empty($rules) )
			foreach( $rules as $role )
			{			
				if ( empty($this->records) || !empty($this->records) && in_array($role['id'], $this->records))
				{
					$this->items[] = $role;	
				}	
			}		
		$this->total_items = count($this->items);
		$this->forming_tables();
	}
}
?>