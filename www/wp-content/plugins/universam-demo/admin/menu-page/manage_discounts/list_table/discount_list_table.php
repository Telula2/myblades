<?php
class USAM_List_Table_discount extends USAM_List_Table 
{		
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),
			'activate'  => __( 'Активировать', 'usam' ),
			'deactivate' => __( 'Отключить', 'usam' )
		);
		return $actions;
	}	
	
	function get_views() 
	{	
		global $wpdb;		
				
		$option = get_option('usam_product_discount_rules', array() );
		$rules = maybe_unserialize( $option );		
		$status_sum = array( 'yes' => 0, 'no' => 0 );
		if ( !empty($rules) )
			foreach( $rules as $rule )
			{			
				if ( $rule['active'] )
					$status_sum['yes']++;
				else
					$status_sum['no']++;
			}
		$total_sum = count($rules);
		$sendback = remove_query_arg( array('active') );
		$views['all'] = "<a href='$sendback' ". (( !isset($_GET['active'])) ?  'class="current"' : '' ).">". __('Все правила','usam')." <span class='count'> ( $total_sum )</span></a>";	
		$sendback = add_query_arg( array( 'active' => 'yes' ) );
		$views['yes'] = "<a href='$sendback' ". (( isset ($_GET['active']) && $_GET['active'] == 'yes' ) ?  'class="current"' : '' ).">". __('Активные','usam')." <span class='count'> (". $status_sum['yes'].")</span></a>";
		$sendback = add_query_arg( array( 'active' => 'no' ) );
		$views['no'] = "<a href='$sendback' ". (( isset ($_GET['active']) && $_GET['active'] == 'no' ) ?  'class="current"' : '' ).">". __('Отключенные','usam')." <span class='count'> (". $status_sum['no'].")</span></a>";
		return $views;
	}
	
	function column_name( $item ) 
    {
		$this->row_actions_table( $item['name'], $this->standart_row_actions( $item['id'] ) );	
	}	
	
	function column_start_date( $item )
    {		
		echo usam_local_date( $item['start_date'] );	
    }   
	
	function column_end_date( $item )
    {			
		echo usam_local_date( $item['end_date'] );	
    } 
		 
	function get_sortable_columns()
	{
		$sortable = array(
			'title'   => array('name', false),		
			'active'  => array('active', false),		
			'date'    => array('date_insert', false),	
			);
		return $sortable;
	}
	
	function get_columns()
	{
        $columns = array(           
			'cb'          => '<input type="checkbox" />',
			'name'        => __( 'Название правила', 'usam' ),
			'active'      => __( 'Активность', 'usam' ),
			'type_prices' => __( 'Типы цен', 'usam' ),
			'discount'    => __( 'Скидка', 'usam' ),				
			'start_date'  => __( 'Время активации', 'usam' ),
			'end_date'    => __( 'Время деактивации', 'usam' ),
			'date'        => __( 'Дата создания', 'usam' ),		
        );		
        return $columns;
    }
	
	function prepare_items() 
	{			
		$option = get_option('usam_product_discount_rules', array() );
		$rules = maybe_unserialize( $option );		
		$this->items = array();
		if ( !empty($rules) )
			foreach( $rules as $rule )
			{			
				if ( empty($this->records) || in_array($rule['id'], $this->records))
				{
					if ( isset($_GET['active']) && ( $_GET['active'] == 'yes' && $rule['active'] == 0 || $_GET['active'] == 'no' && $rule['active'] == 1 ) )
					{
						continue;
					}
					$this->items[] = $rule;	
				}	
			}		
		$this->total_items = count($this->items);
		$this->forming_tables();
	}
}
?>