<?php
class USAM_List_Table_rules_coupons extends USAM_List_Table 
{	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' )
		);
		return $actions;
	}

	function column_title( $item ) 
	{
		$this->row_actions_table( $item['title'], $this->standart_row_actions( $item['id'] ) );	
	}
	
	function column_discount( $item ) 
    {	
		switch ( $item['discount_type'] ) 
		{
			case '0':	
				echo usam_currency_display( esc_attr($item['discount']) );		
			break;
			case '1':	
				echo $item['discount'].'%';		
			break;
			case '2':	
				echo $item['discount'].'% '.__( 'от заказа как фиксированная скидка в купоне', 'usam' );
			break;		
			case '3':	
				_e( 'Бесплатная доставка', 'usam' );
			break;				
		}	
	}
   
	function get_sortable_columns() 
	{
		$sortable = array(
			'discount'  => array('discount', false),	
			'active'    => array('active', false),			
			'day'       => array('day', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',				
			'title'          => __( 'Название правила', 'usam' ),
			'discount'       => __( 'Скидка', 'usam' ),
			'active'         => __( 'Активный', 'usam' ),		
			'day'            => __( 'Количество дней', 'usam' ),			
        );		
        return $columns;
    }
	
	public function get_number_columns_sql()
    {       
		return array('day');
    }
	
	function prepare_items() 
	{				
		$option = get_option('usam_coupons_roles', array() );
		$coupons_roles = maybe_unserialize( $option );		
		$this->items = array();
		if ( !empty($coupons_roles) )
			foreach( $coupons_roles as $id => $role )
			{
				$role['id'] = $id;
				if ( !empty($this->records) && in_array($id, $this->records))
				{					
					$this->items[] = $role;	
				}
				else				
					$this->items[] = $role;						
			}		
		$this->total_items = count($this->items);
		$this->forming_tables();	
	}
}