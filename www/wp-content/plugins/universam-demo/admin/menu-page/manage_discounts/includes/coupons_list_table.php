<?php
class USAM_List_Table_Standart_Coupons extends USAM_List_Table 
{	   
	protected $is_bulk_edit = false;
	protected $order = 'desc';
	protected $coupon_type = 'coupon';
	function __construct( $args = array() )
	{	
		parent::__construct( $args );
    }	
	
	function get_bulk_actions_display() 
	{	
		$actions = array(
			'delete'    => __( 'Удалить', 'usam' ),
			'activate'    => __( 'Активировань', 'usam' ),
			'deactivate'    => __( 'Отключить', 'usam' )
		);
		return $actions;
	}
				
	function column_coupon_code( $item ) 
    {
		$actions = $this->standart_row_actions( $item->id );
		$actions['editinline'] = $this->add_row_actions( $item->id, 'editinline', __( 'Свойство', 'usam' ) );		
		$this->row_actions_table( $item->coupon_code, $actions );	
	}	
	
	function column_discount( $item ) 
    {			
		switch ( $item->is_percentage ) 
		{
			case '0':					
				echo usam_currency_display( $item->value );		
			break;
			case '1':	
				echo $item->value.'%';		
			break;
			case '2':	
				_e( 'Бесплатная доставка', 'usam' );
			break;			
		}	
	}
	
	function column_expiry( $item ) 
    {		
		$num_days = ceil((time() - strtotime($item->expiry))/86400);
		$num_days_start = ceil((time() - strtotime($item->start))/86400);
		$num = abs( $num_days );
		$message = '';
		if ( $num_days_start >= 0 ) 
		{
			if ( $num_days < 0 ) 
				$message = sprintf( __('До конца действия %s','usam'), $num );
			elseif ( $num_days == 0 ) 
				$message = sprintf( __('Заканчивается сегодня','usam'), $num );
			else
				$message = sprintf( _n( 'Просрочен на %s день', 'Просрочен на %s дней', $num, 'usam' ), $num );
		}
		$date = usam_local_date( $item->expiry, get_option( 'date_format', 'Y/m/d' ).' H:i' );
		echo "{$date}<br><strong>{$message}</strong>";		
	}
	
	function column_start( $item ) 
    {
		$num_days = ceil((time() - strtotime($item->start))/86400);
		$num = abs( $num_days );			
		echo usam_local_date( $item->start, get_option( 'date_format', 'Y/m/d' ).' H:i' );		
		if ( $num_days < 0) 
		{
			$message = sprintf( _n( 'До начала действия остался %s день', 'До начала действия осталось %s дней', $num, 'usam' ), $num );		
			echo "<br><strong>{$message}</strong>";				
		}
	}
	 	
    function column_use_once( $item ) 
    {		
		$this->logical_column( $item->use_once );
    }
	
	function column_user_id( $item ) 
	{		
		$user = get_user_by('id', $item->customer );
		return isset($user->display_name)?"$user->display_name ({$user->user_login})":__('Любой','usam');
	}
   	
	function get_sortable_columns() 
	{
		$sortable = array(
			'name'        => array('name', false),
			'active'      => array('active', false),
			'discount'    => array('value', false),
			'is_used'     => array('is_used', false),
			'start'       => array('start', false),	
			'expiry'      => array('expiry', false),			
			);
		return $sortable;
	}
		
	function get_columns()
	{
        $columns = array(           
			'cb'             => '<input type="checkbox" />',				
			'coupon_code'    => __( 'Код купона', 'usam' ),
			'active'         => __( 'Активность', 'usam' ),
			'discount'       => __( 'Скидка', 'usam' ),
			'start'          => __( 'Начало', 'usam' ),
			'expiry'         => __( 'Конец', 'usam' ),			
			'use_once'       => __( 'Использовать один раз', 'usam' ),	
			'is_used'        => __( 'Использован', 'usam' ),			
			'user_id'        => __( 'Владелец', 'usam' ),		
			'description'    => __( 'Описание', 'usam' ),		
			'date'           => __( 'Создан', 'usam' ),			
        );		
        return $columns;
    }
	
	function prepare_items() 
	{			
		global $wpdb;			
				
		require_once( USAM_FILE_PATH . '/includes/basket/coupons_query.class.php' );
		
		$args = array( 'cache_results' => true, 'paged' => $this->get_pagenum(), 'number' => $this->per_page, 'order' => $this->order, 'orderby' => $this->orderby, 'coupon_type' => $this->coupon_type, 'active' => 'all' );	
		
		if ( $this->search != '' )
		{
			$args['search'] = $this->search;		
		}
		else
		{
			if ( !empty( $this->records ) )
				$args['include'] = $this->records;
		
			$args = array_merge ($args, $this->get_date_for_query() );					
			if ( !empty( $_REQUEST['condition_v'] ) )
			{				
				$args['condition'] = array( array( 'col' => $_REQUEST['selectc'], 'compare' => $_REQUEST['compare'], 'value' => $_REQUEST['condition_v'] ) );				
			}
		}				
		$query_orders = new USAM_Coupons_Query( $args );
		$this->items = $query_orders->get_results();	
		if ( $this->per_page )
		{
			$total_items = $query_orders->get_total();	
			$this->set_pagination_args( array( 'total_items' => $total_items, 'per_page' => $this->per_page, ) );
		}	
	}

	public function single_row( $item ) 
	{
		static $count = 0;
		$count ++;
		$item->index = $count;
		parent::single_row( $item );
		if ( !isset($_REQUEST['action'] ) )
			$this->coupon_editor( $item );	
	}

	protected function coupon_editor( $item ) 
	{	
		
	}	
}