<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_discount extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить правило &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить правило', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		require_once( USAM_FILE_PATH . '/admin/includes/rules/product_discount_rules.class.php' );	
		
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_product_discount_rules');
		}
		else
		{
			$this->data = array( 'name' => '', 'description' => '', 'active' => 0, 'priority' => 100, 'end' => 0, 'discount' => 0, 'dtype' => 'p', 'conditions' => array(), 'type_prices' => array(), 'start_date' => '', 'end_date' => '' );
		} 				
	}	
				  
    public function box_type_price_and_discount( )
	{        
        ?>		
		<div class="container_column">	
			<div class="column1 checklist_description" id="amtDescrip">
				<h4><?php _e('Цены и скидка', 'usam') ?></h4>
				<p><?php _e('Выберите цену и скидку.', 'usam')?>  </p>
			</div>
			<div id="amtRadio">
				<div class="column1 checklist_description" id="amtChoice">	
					<?php $this->display_meta_box_group( 'type_prices', $this->data['type_prices'] );  ?>
				</div> 
				<div class="column2" id="amtChoice">				
					<h3><?php _e('Сумма скидки', 'usam') ?></h3>					
					<span id="amtChoice-span">						
						<input id="rule_amt" type="text" name="discount" autocomplete="off" value="<?php echo $this->data['discount']; ?>">
					</span>
					<?php echo usam_get_select_type_md( $this->data['dtype'], array( 'name' => "dtype" ) ); ?>
				</div>                
			</div>			
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>
		</div>
       <?php
	}       
	
	function box_options( )
	{			
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start_date'], array( 'hour' ) ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['end_date'],  array( 'hour' ) ); ?></td>
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Приоритет', 'usam' );  ?>:</td>
				<td><input type="text" autocomplete="off" value="<?php echo $this->data['priority']; ?>" name="priority"/></td>				
			</tr>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Прекратить дальнейшее выполнение правил', 'usam' );  ?>:</td>				
				<td><input type="checkbox" value="1" <?php echo $this->data['end'] == 1?"checked='checked'":'' ?>name="end"/></td>				
			</tr>				
		</table>		
		<?php
	}		
	
	public function display_product_discount_rules( ) 
	{		
		wp_enqueue_script( 'usam-basket_edit_conditions', USAM_URL.'/admin/js/basket_edit_conditions.js', array('jquery'), '1.1', true );
		wp_localize_script( 'usam-basket_edit_conditions', 'USAM_Basket_Edit_Conditions',					
			array(
			'text_and'    => __('И','usam'),
			'text_or'     => __('ИЛИ','usam'),		
			'text_product' => __('Выбрать товары заказа, которые удовлетворяют условиям','usam'),
			'text_group' => __('Группа условий','usam'),
			'text_add_conditions' => __('Добавить условие','usam'),
			'images_url'  => USAM_CORE_IMAGES_URL,	
			)
		);		
		$product_discount_rules = new USAM_Product_Discount_Rules( );
		$product_discount_rules->load();
		$product_discount_rules->display( $this->data['conditions'] );	
	}
  
	
	function display_left()
	{		
		$help = array( array( 'title' => __('Не активно', 'usam'), 'description' => __('Скидки не будут установлены.', 'usam') ),
				array( 'title' => __('Активировать', 'usam'), 'description' => __('Скидки будут установлены при наступлении времени.', 'usam') ));
				
		$title = __('Статус правила', 'usam');	
		
		$this->titlediv( $this->data['name'] );		
		$this->add_box_description( $this->data['description'] );
		$this->add_box_status_active( $this->data['active'], $title, $help );		
		
		usam_add_box( 'usam_type_price_and_discount', __('Вариант применения правила','usam'), array( $this, 'box_type_price_and_discount' ) );		
		usam_add_box( 'usam_options', __('Настройки правила','usam'), array( $this, 'box_options' ) );
		usam_add_box( 'usam_condition', __('Условия выполнения правила','usam'), array( $this, 'display_product_discount_rules' ) );	
    }	
}
?>