<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_accumulative extends USAM_Edit_Form
{		
	private $alt = true;
	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить правило накопительных скидок &#171;%s&#187;','usam'), $this->data['name'] );
		else
			$title = __('Добавить правило накопительных скидок', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_accumulative_discount');
		}
		else
		{
			$this->data = array( 'name' => '', 'active' => 0, 'method' => 'price', 'end' => 0, 'sort' => 100, 'layers' => array(), 'type_prices' => array(), 'period' => 'u','period_from' => 1, 'period_from_type' => 'y','start_date' => '', 'end_date' => '' );
		} 				
	}	
	
	function box_options( )
	{			
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Тип скидки', 'usam' );  ?>:</td>
				<td>
					<select name='method'>
						<option value='price' <?php selected( $this->data['method'], 'price' ) ?>><?php _e('Скидка к покупке','usam') ?></option>
						<option value='bonus' <?php selected( $this->data['method'], 'bonus' ) ?>><?php _e('Начислить бонусы','usam') ?></option>						
					</select>
				</td>
			</tr>
			<tr>
				<td class ="name"><?php esc_html_e( 'Период для расчета скидок', 'usam' );  ?>:</td>
				<td>
					<select id="period" name='period'>
						<option value='u' <?php selected( $this->data['period'], 'u' ) ?>><?php _e('За все время','usam') ?></option>
						<option value='d' <?php selected( $this->data['period'], 'd' ) ?>><?php _e('За период','usam') ?></option>		
						<option value='p' <?php selected( $this->data['period'], 'p' ) ?>><?php _e('За последние','usam') ?></option>							
					</select>
				</td>
			</tr>
			<tr id = "interval">
				<td class ="name"><?php esc_html_e( 'Дата периода', 'usam' );  ?>:</td>
				<td><?php usam_display_date_interval( $this->data['start_date'], $this->data['end_date'] );  ?></td>
			</tr>	
			<tr id = "period_from">
				<td class ="name"><?php esc_html_e( 'Выбрать оплаченные заказы за последний(е)', 'usam' );  ?>:</td>
				<td>					
					<input type="text" name="period_from" value="<?php echo $this->data['period_from']; ?>" size="4" />
					<select name='period_from_type'>
						<option value='d' <?php selected( $this->data['period_from_type'], 'd' ) ?>><?php _e('День','usam') ?></option>
						<option value='m' <?php selected( $this->data['period_from_type'], 'm' ) ?>><?php _e('Месяц','usam') ?></option>	
						<option value='y' <?php selected( $this->data['period_from_type'], 'y' ) ?>><?php _e('Год','usam') ?></option>						
					</select>
				</td>
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Сортировка', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['sort']; ?>" name="sort" autocomplete="off" /></td>				
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Таблица скидок', 'usam' );  ?>:</td>
				<td>
					<table class = "table_rate">
						<thead>
							<tr>
								<th><?php _e('Сумма', 'usam' ); ?></th>
								<th><?php _e( 'Скидка', 'usam' ); ?></th>
							</tr>
						</thead>
						<tbody>						
							<?php 
							if ( ! empty( $this->data['layers'] ) )
							{
								foreach( $this->data['layers'] as $sum => $discount )
									$this->output_row( $sum, $discount );	
								$this->output_row(); 									
							}
							else 
							{
								$this->output_row(); 
								$this->output_row(); 
								$this->output_row(); 
							}
							?>
						</tbody>
					</table>
				</td>				
			</tr>				
		</table>		
		<?php		
	}		
	
	private function output_row( $sum = '', $discount = '' ) 
	{		
		$class = ( $this->alt ) ? ' class="alternate"' : '';
		$this->alt = ! $this->alt;		
		?>
			<tr>
				<td <?php echo $class; ?> >
					<div class="cell-wrapper sum">					
						<input type="text" name="sum[]" value="<?php echo esc_attr( $sum ); ?>" size="4" />						
					</div>
				</td>
				<td <?php echo $class; ?> >
					<div class="cell-wrapper">						
						<input type="text" name="discounts[]" value="<?php echo esc_attr( $discount ); ?>" size="4" />
						<div class="actions">
							<a tabindex="-1" title="<?php _e( 'Добавить уровень', 'usam' ); ?>" class="action add" href="#"><?php _e( 'Добавить', 'usam' ); ?></a>
							<a tabindex="-1" title="<?php _e( 'Удалить уровень', 'usam' ); ?>" class="action delete" href="#"><?php _e( 'Удалить', 'usam' ); ?></a>
						</div>
					</div>
				</td>
			</tr>
		<?php
	}
	
	function display_left()
	{		
		$help = array( array( 'title' => __('Не активно', 'usam'), 'description' => __('Скидки не будут установлены.', 'usam') ),
				array( 'title' => __('Активировать', 'usam'), 'description' => __('Скидки будут установлены при наступлении времени.', 'usam') ));
				
		$title = __('Статус правила', 'usam');	
		
		$this->titlediv( $this->data['name'] );			
		$this->add_box_status_active( $this->data['active'], $title, $help );		
		
		usam_add_box( 'usam_prices', __('Типы цен','usam'), array( $this, 'selecting_type_prices' ), $this->data['type_prices'] );		
		usam_add_box( 'usam_options', __('Настройки правила программы','usam'), array( $this, 'box_options' ) );
    }
}
?>