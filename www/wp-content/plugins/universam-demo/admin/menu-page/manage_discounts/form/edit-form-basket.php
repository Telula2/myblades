<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_basket extends USAM_Edit_Form
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить правило расчета скидок %s','usam'), $this->data['title'] );
		else
			$title = __('Добавить правило расчета скидок', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{			
			$this->data = usam_get_data( $this->id, 'usam_rules_discounts_shopping_cart' );	
		}
		else
			$this->data = array( 'title' => '', 'description' => '', 'priority' => 100, 'logical' => 'or', 'active' => 1,'value' => '', 'value_type' => 'f', 'type' => 'd', 'code' => '', 'start_date' => '', 'end_date' => '', 'condition' => array(), 'end' => 0 );			
	}	
	
	function box_options(  )
	{			
		?>		
		<table id='discount_cart' class='subtab-detail-content-table usam_edit_table'>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start_date'], array( 'hour' )  ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['end_date'], array( 'hour') ); ?></td>
			</tr>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Приоритет', 'usam' );  ?>:</td>
				<td><input type="text" value="<?php echo $this->data['priority']; ?>" autocomplete="off" name="priority"/></td>				
			</tr>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Прекратить дальнейшее выполнение правил', 'usam' );  ?>:</td>				
				<td><input type="checkbox" value="1" <?php echo $this->data['end'] == 1?"checked='checked'":'' ?>name="end"/></td>				
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Внешний код', 'usam' );  ?>:</td>				
				<td><input type="text" autocomplete="off" value="<?php echo $this->data['code']; ?>" name="code"/></td>			
			</tr>
		</table>		
		<?php
	}	
	
	function box_action( )
	{			
		$discount_cart_type = array( 'p' => esc_html__( 'Изменить цену товара', 'usam' ), 'b' => esc_html__( 'Бонусы', 'usam' ), 's' => esc_html__( 'Изменить доставку', 'usam' ), 'g' => esc_html__( 'Добавить подарок', 'usam' ) );
		?>		
		<table id='discount_action' class='subtab-detail-content-table usam_edit_table'>																		
			<tr>
				<td class ="name"><label for="discount_cart_type"><?php esc_html_e( 'Выполнить действие', 'usam' );  ?>:</label></td>
				<td>
					<select class="ruleprops" id="discount_cart_type" name="type">
						<?php 
						foreach ( $discount_cart_type as $key => $title ) 
						{
							?><option value="<?php echo $key; ?>" <?php echo (($this->data['type']==$key)?'selected="selected"':'')?>><?php echo $title; ?></option><?php 
						}
						?>
					</select>
				</td>				
			</tr>
			<tr id = "discount_cart-value">
				<td class ="name"><label for="discount_cart_value"><?php esc_html_e( 'Скидка', 'usam' );  ?></label>:</td>
				<td><span><input type="text" style="width:100px;" value="<?php echo $this->data['value']; ?>" name="value" id="discount_cart_value" autocomplete="off"/></span>
					<select class="ruleprops" name="value_type" style="width:300px;">
						<option value="f" <?php echo (($this->data['value_type']=='f')?'selected="selected"':'');?>><?php esc_html_e( 'Фиксированное', 'usam' ); ?></option>		
						<option value="p" <?php echo (($this->data['value_type']=='p')?'selected="selected"':'')?>><?php esc_html_e( 'Процент от покупки', 'usam' ); ?></option>										
					</select>
				</td>				
			</tr>								
		</table>
		<div id = 'discount_cart-gift'>
			<div class = 'gift'>
				<?php	
				$columns = array(
				 'n'         => __( '№', 'usam' ),
				 'title'     => __( 'Имя', 'usam' ),
				 'sku'       => __( 'Артикул', 'usam' ),		 
				 'quantity'  => __( 'Количество','usam' ),					
				 'remove'    => __( 'Действие', 'usam'),
				);			
				register_column_headers( 'order_item_details', $columns );								
				?>		
				<table class="widefat gift_table">
					<thead>
						<tr>
							<?php print_column_headers( 'order_item_details' ); ?>
						</tr>
					</thead>
					<tbody>
						<?php $this->table_gift(); ?>				
					</tbody>								
				</table>							
			</div>			
			<div id = "add_button_box" class ="usam_autocomplete">
				<div class="autocomplete_forms">						
					<?php
						$autocomplete = new USAM_Autocomplete_Forms( );
						$autocomplete->get_form_product( );
					?>								
				</div>	
				<button id='add_gift' type="button" class="button" data-id = "<?php echo $this->id; ?>"><?php _e( 'Добавить подарок', 'usam' ); ?></button>
				<?php usam_loader(); ?>					
			</div>		
		</div>					
		<?php		
	}	
	
	
	private function table_gift( )
	{		
		if ( empty($this->data['gift'] ) )
		{
			?>
				<tr id = 'items_empty'><td colspan = '7'><?php _e( 'Подарки не выбраны', 'usam' ); ?> </td></tr>
			<?php	
		}
		else
		{				
			$key = 0;
			foreach ( $this->data['gift'] as $product_id => $quantity ) 
			{
				$product = get_post( $product_id );					
				?>
				<tr id ="product_<?php echo $product_id; ?>" data-item-id="<?php echo $product_id; ?>" >
					<td><?php echo $key+1; ?></td>
					<td><a id = "details_product" title='<?php echo $product->post_title; ?>' href='<?php echo get_edit_post_link( $product_id ); ?>'><?php echo $product->post_title; ?></a></td>
					<td><?php echo usam_get_product_meta( $product_id, 'sku' ); ?></td>					
					<td><input size="4" type="text" name="gift[<?php echo $product_id; ?>]" value="<?php echo $quantity; ?>"></td>					
					<td class='delete'>						
						<a class = "button_delete" href=''></a>
						<?php usam_loader(); ?>						
					</td>	
				</tr>
			<?php			
			}
		}		
	}	
	
	function display_left()
	{		
		$this->titlediv( $this->data['title'] );		
		$this->add_box_description( $this->data['description'] );		
		$this->add_box_status_active( $this->data['active'] );	
	
		usam_add_box( 'usam_options', __('Основные настройки','usam'), array( $this, 'box_options' ) );		
		usam_add_box( 'usam_execute_actions', __('Выполнить действия','usam'), array( $this, 'box_action' ) );	
		usam_add_box( 'usam_condition', __('Условия выполнения правила','usam'), array( $this, 'display_rules_work_basket' ), $this->data['condition'] );	
    }
}
?>