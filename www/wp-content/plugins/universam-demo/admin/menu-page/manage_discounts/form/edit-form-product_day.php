<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_product_day extends USAM_Edit_Form
{			
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить программу &laquo;%s&raquo;','usam'), $this->data['name'] );
		else
			$title = __('Добавить программу', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{				
			$this->data = usam_get_data($this->id, 'usam_product_day_rules');
		}
		else
		{
			$this->data = array( 'name' => '', 'description' => '', 'active' => 0, 'refill' => 0, 'type_prices' => array(), 'start_date' => '', 'end_date' => '', 'conditions' => array( 'pricemin' => 0, 'pricemax' => 0, 'minstock' => 0, 'c' => 10, 'value' => 10, 'terms' => array( 'category' => array(), 'brands' => array() ) ) );
		} 				
	}	   
	
	function box_options( )
	{			
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start_date'],  array( 'hour' ) ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['end_date'],  array( 'hour' ) ); ?></td>
			</tr>			
			<tr>
				<td class ="name"><?php esc_html_e( 'Пополнять очередь', 'usam' );  ?></td>
				<td>
					<input type='radio' id = "refill_the_queue1" value='1' name='refill' <?php checked($this->data['refill'], 1) ?> /> <label for='refill_the_queue1'><?php _e( 'Да', 'usam' );  ?></label> &nbsp;
					<input type='radio' id = "refill_the_queue2" value='0' name='refill' <?php checked($this->data['refill'], 0) ?> /> <label for='refill_the_queue2'><?php _e( 'Нет', 'usam' );  ?></label>
				</td>
			</tr>						
		</table>		
		<?php
	}		
	
	public function table_products( )
	{		
		$columns = array(
		'n'         => __( '№', 'usam' ),
		'title'     => __( 'Имя', 'usam' ),
		'sku'       => __( 'Артикул', 'usam' ),				
		'discount'  => __( 'Скидка', 'usam' ).'<input size="4" type="text" name="_discount" id="product_discount" value="" class = "usam_edit_data">',		
		'status'     => __( 'Статус', 'usam' ),			 
		'action'    => __( 'Действие', 'usam'),
		'drag'      => '',		
		);	
		if ( !empty($this->id) )
			$products = usam_get_products_day( array( 'rule_id' => $this->id, 'status' => array( 0, 1 ) ) );
		else
			$products = array();
		$this->display_products_table( $products, $columns );
	}
	
	protected function display_table_products( $products, $columns )
	{		
		if ( empty($products) )
		{
			?>
				<tr class = "items_empty"><td colspan = '<?php echo count($columns); ?>'><?php _e( 'Нет товаров', 'usam' ); ?></td></tr>
			<?php
		}
		else
		{				
			$i = 0;
			$this->total = 0;			
			foreach ( $products as &$product ) 
			{							
				$post = get_post($product->product_id, ARRAY_A);						
				$i++;
				?>
				<tr id ="product_<?php echo $product->product_id; ?>" class="item_row sortable" >					
					<?php
					foreach ( $columns as $column => $name ) 
					{						
						$data = '';					
						switch ( $column ) 
						{
							case 'n' :
								$data = $i;
							break;
							case 'title' :
								$url = admin_url('post.php?post='.$product->product_id.'&action=edit');
								$data = "<a id = 'details_product' title='".$post['post_title']."' href='$url'>".$post['post_title']."</a>";								
							break;
							case 'sku' :
								$data = usam_get_product_meta( $product->product_id, 'sku', true );
							break;		
							case 'status' :
								switch ( $product->status ) 
								{
									case 0 :
										$data = __( 'Ожидание', 'usam');
									break;
									case 1 :
										$data = __( 'Установлен', 'usam');
									break;
									case 2 :
										$data = __( 'Снят', 'usam');
									break;
								}
							break;										
							case 'discount' :								
								$dtype = '';
								switch ( $product->dtype ) 
								{
									case 'p' :
										$dtype = '%';
									break;
									case 'f' :
										$dtype = usam_get_currency_sign();;
									break;
									case 't' :
										$dtype = esc_html__( 'Точная', 'usam');
									break;
								}
								
								$data = "<span class = 'usam_display_data'>".$product->value." $dtype</span>		
								<span class = 'usam_edit_data'>				
									<input size='4' type='text' name='products[$product->id][value]' value='".$product->value."'>
									".usam_get_select_type_md( $product->dtype, array('name'=> "products[$product->id][dtype]") )."
								</span>";
							break;
							case 'drag' :							
									if ( $product->status != 1 )
									{
										$data = "<div class='drag ui-sortable-handle'><img src='".USAM_CORE_IMAGES_URL . "/drag.png' /></div>";						
									}
							break;
							case 'action' :		
								$data .= "<a class='button_delete'></a><input type='hidden' name='ids[]' value='".$product->id."'>";						
							break;
						}
						echo "<td class = 'product_$column'>$data</td>";
					}
					?>	
				</tr>				
				<?php											
			}
		}	
		?>
		<tr class ="add_new">					
			<td class = 'product_n'></td>
			<td class = 'product_title'></td>
			<td class = 'product_sku'></td>
			<td class = 'product_discount'>	
				<input size='4' type='text' name='' value=''>
				<?php echo usam_get_select_type_md( ); ?>
			</td>
			<td class = 'product_status'><?php _e( 'Ожидание', 'usam'); ?></td>
			<td class = 'product_action'>						
				<a class='button_delete'></a>
			</td>
		</tr>
		<?php		
	}	
	
	public function section_products( )
	{				
		$this->table_products( );
		$this->add_product_button();
	}
		
	function auto_fill( )
	{					
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Диапазон цен', 'usam' );  ?>:</td>
				<td>
					<span><?php esc_html_e( 'От', 'usam' );  ?><span>
					<input type='text' class='text' size='10' name='pricemin' value='<?php echo number_format( $this->data['conditions']['pricemin'], 2, '.', '' );  ?>' />
					<span><?php esc_html_e( 'До', 'usam' );  ?><span>
					<input type='text' class='text' size='10' name='pricemax' value='<?php echo number_format( $this->data['conditions']['pricemax'], 2, '.', '' );  ?>' />
				</td>				
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Минимальный остаток', 'usam' );  ?>:</td>
				<td><input type='text' class='text' size='10' name='minstock' value='<?php echo $this->data['conditions']['minstock'];  ?>' /></td>				
			</tr>		
			<tr>
				<td class ="name"><?php esc_html_e( 'Количество в очереди', 'usam' );  ?>:</td>				
				<td><input type='text' class='text' size='10' name='c' value='<?php echo $this->data['conditions']['c'];  ?>' /></td>				
			</tr>	
			<tr>
				<td class ="name"><?php esc_html_e( 'Установить скидку', 'usam' );  ?>:</td>				
				<td><input type='text' class='text' size='10' name='value' value='<?php echo $this->data['conditions']['value'];  ?>' /></td>				
			</tr>						
		</table>
	
		<div class="container_column">		
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите, на какие термины', 'usam') ?></h4>
				<p><?php _e('Выберите, на какие термины установить скидку.', 'usam') ?></p>
			</div>
			
			<?php $this->checklist_meta_boxs( $this->data['conditions']['terms'] ); ?>	
			
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>	
		</div>			
	  <?php
	}
  
	
	function display_left()
	{		
		$help = array( array( 'title' => __('Не активно', 'usam'), 'description' => __('Скидки не будут установлены.', 'usam') ),
				array( 'title' => __('Активировать', 'usam'), 'description' => __('Скидки будут установлены при наступлении времени.', 'usam') ));
				
		$title = __('Статус правила', 'usam');	
		
		$this->titlediv( $this->data['name'] );				
		$this->add_box_status_active( $this->data['active'], $title, $help );				
	
		usam_add_box( 'usam_prices', __('Типы цен','usam'), array( $this, 'selecting_type_prices' ), $this->data['type_prices'] );		
		usam_add_box( 'usam_options', __('Настройки правила','usam'), array( $this, 'box_options' ) );
		usam_add_box( 'usam_products', __('Товары','usam'), array( $this, 'section_products' ), '', true  );
		usam_add_box( 'usam_auto_fill', __('Автоматическое заполнение','usam'), array( $this, 'auto_fill' )  );	
    }	
}
?>