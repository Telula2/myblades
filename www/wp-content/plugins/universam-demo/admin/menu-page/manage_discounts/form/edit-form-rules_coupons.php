<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_rules_coupons extends USAM_Edit_Form
{		
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить правило № %s','usam'), $this->data['title'] );
		else
			$title = __('Добавить правило', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{	
			$this->data = usam_get_data($this->id, 'usam_coupons_roles');
		}
		else
		{		
			$this->data = array( 'active' => 0, 'title' => __('Новое правило', 'usam'), 'discount' => '', 'discount_type' => 0, 'day' => 7, 'customer' => 0,'totalprice' => '', 'percentage_of_use' => '', 'roles' => array(), 'locations' => array() );			
		}				
	}	
	
	function box_options(  )
	{			
		?>	
		<table class="usam_edit_table subtab-detail-content-table">				
			<tr>
				<td class="name"><?php esc_html_e( 'Сумма заказа', 'usam' ); ?>:</td>
				<td><input type='text' size='10' value="<?php echo $this->data['totalprice']; ?>" name='coupons_role[totalprice]'/></td>
			</tr>										
		</table>	
		<?php	
	}	
	
	function coupon_data_settings( )
	{			
		if ( $this->id != null )
		{				
			$coupon = usam_get_coupon( $this->data['coupon_id'] );
		}
		else
		{				
			$coupon = array( 'use_once' => 1, 'condition' => array(), 'amount_bonuses_author' => '' );	
		}
		$currency = usam_get_currency_sign();
		?>		
		<table class="usam_edit_table subtab-detail-content-table">								
			<tr>							
				<td class="name"><?php esc_html_e( 'Скидка', 'usam' ); ?>:</td>
				<td>
					<input type='text' value='<?php echo $this->data['discount']; ?>' size='3' name='coupons_role[discount]' style ="width:300px;"/>
					<select name='coupons_role[discount_type]' class="select_type_md">
						<option value='0' <?php selected($this->data['discount_type'],0); ?>><?php echo esc_html( $currency ) ?></option>
						<option value='1' <?php selected($this->data['discount_type'],1); ?>>%</option>
						<option value='2' <?php selected($this->data['discount_type'],2); ?>><?php esc_html_e( '% от заказа как фиксированная скидка', 'usam' ); ?></option>
						<option value='3' <?php selected($this->data['discount_type'],3); ?>><?php esc_html_e( 'Бесплатная доставка', 'usam' ); ?></option>
					</select>
				</td>
				<td class='description'></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Использование', 'usam' ); ?>:</td>
				<td><input type='text' size='3' value="<?php echo esc_attr($this->data['percentage_of_use']); ?>" name='coupons_role[percentage_of_use]' /></td>
				<td class='description'><?php esc_html_e( 'Укажите процент от стоимости заказа, который активировал правило. Расчитанный процент в рублях будет добавлен к условиям использования купона. Больше суммы, полученной при расчете, нельзя использовать созданный купон.', 'usam' ) ?></td>
			</tr>						
			<tr>
				<td class="name"><?php esc_html_e( 'Количество дней', 'usam' ); ?>:</td>
				<td><input type='text' class='pickdate' size='4' value="<?php echo $this->data['day']; ?>" name='coupons_role[day]' /></td>
				<td class='description'></td>
			</tr>				
			<tr>
				<td class="name"><?php esc_html_e( 'Использовать один раз', 'usam' ); ?>:</td>
				<td>
					<input type='hidden' value='0' name='use_once' />
					<input type='checkbox' value='1' name='use_once' <?php echo $coupon['use_once'] == 1 ?"checked='checked'":'' ?>/>
				</td>
				<td class='description'><?php esc_html_e( 'Отключение купона после его использования.', 'usam' ) ?></td>
			</tr>	
			<tr>
				<td class="name"><?php _e( 'Владелец', 'usam' ); ?>:</td>
				<td>
					<?php						
					$users = get_users( );									
					?>
					<select name='coupons_role[customer]'>
						<option value='0' <?php echo $this->data['customer'] == 0 ?'selected="selected"':''; ?>><?php esc_html_e( 'Обезличенный', 'usam' ); ?></option>
						<option value='order' <?php echo $this->data['customer'] == 'order'?'selected="selected"':'';?> ><?php esc_html_e( 'Покупатель заказа', 'usam' ); ?></option>
						<?php												
						foreach ($users as $user)
						{
							?><option value='<?php echo $user->ID; ?>' <?php echo $this->data['customer'] == $user->ID ?'selected="selected"':''; ?>><?php echo $user->user_login; ?></option><?php
						}
						?>
					</select>
				</td>
				<td class='description'></td>
			</tr>					
			<tr>
				<td class="name"><?php esc_html_e( 'Начислить владелецу', 'usam' ); ?>:</td>
				<td><input type='text' size = "6" maxlength = "6" value='<?php echo $coupon['amount_bonuses_author']; ?>' name='amount_bonuses_author'/></td>
				<td class='description'><?php esc_html_e( 'Укажите количество бонусов, которые нужно начислить автору купона.', 'usam' ) ?></td>
			</tr>								
		</table>
		<?php	
		$this->display_rules_work_basket( $coupon['condition'] );
    }	
	
	public function box_roles( ) 
	{			
		?>  			
		<div class="container_column">	
			<div class="column1 checklist_description" id="inpop_descrip">
				<h4><?php _e('Выберите роли', 'usam') ?></h4>
				<p><?php _e('Выберите, при каких ролях покупателей создовать купон.', 'usam') ?></p>
			</div>						
			<div id="all_taxonomy" class="all_taxonomy">						
				<?php $this->display_meta_box_group( 'roles', $this->data['roles'] ); ?>
			</div>	
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>		
		</div>			
	   <?php   
	}	
	
	function display_left()
	{					
		$this->titlediv( $this->data['title'] );			
		$this->add_box_status_active( $this->data['active'] );	
		
		usam_add_box( 'usam_options', __('Активация правила','usam'), array( $this, 'box_options' ) );	
		usam_add_box( 'usam_roles', __('Роли покупателей','usam'), array( $this, 'box_roles' ) );		
		usam_add_box( 'usam_locations', __('Местоположение','usam'), array( $this, 'selecting_locations' ), $this->data['locations'] );
		usam_add_box( 'usam_coupon_data_settings', __('Условия использования купона','usam'), array( $this, 'coupon_data_settings' ), $this->data );		
    }		
}
?>