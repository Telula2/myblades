<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_certificates extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить сертификат № %s','usam'), $this->data['coupon_code'] );
		else
			$title = __('Добавить сертификат', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
		{
			$this->data = usam_get_coupon( $this->id );				
		}
		else	
			$this->data = array('coupon_code' => '', 'description' => '', 'use_once' => '', 'action' => '', 'is_used' => 0, 'max_is_used' => 0, 'value' => '', 'active' => 0, 'start' => date('Y-m-d H:i:s'), 'expiry' => date('Y-m-d H:i:s', time()+3600*24*360*3),  'is_percentage' => 0, 'customer' => 0, 'amount_bonuses_author' => 0, 'condition' => array() );					
	}
	
	function certificates_settings( )
	{	
		?>	
		<table class="usam_edit_table subtab-detail-content-table">		
			<tr>
				<td class="name"><?php _e( 'Код сертификата', 'usam' ); ?>:</td>
				<td><input type='text' size ="60" value='<?php echo $this->data['coupon_code']; ?>' name='coupon_code'/></td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['expiry'] ); ?></td>
			</tr>				
			<tr>
				<td class="name"><?php _e( 'Номинал сертификата', 'usam' ); ?>:</td>
				<td><input type='text' value='<?php echo $this->data['value']; ?>' size='10' name='value' /></td>
			</tr>			
			<tr>
				<td class="name"><?php _e( 'Владелец', 'usam' ); ?>:</td>
				<td>
					<?php $users = get_users(  ); ?>
					<select name='customer'>
						<option value='0' <?php selected($this->data['customer'],0); ?>><?php esc_html_e( 'Обезличенный', 'usam' ); ?></option>
						<?php												
						foreach ($users as $user)
						{
							?><option value='<?php echo $user->ID; ?>' <?php selected($this->data['customer'], $user->ID); ?>><?php echo $user->user_login; ?></option><?php
						}
						?>
					</select>
				</td>
			</tr>	
		</table>
		<?php 
	}
		
	function display_left()
	{				
		$this->add_box_status_active( $this->data['active'] );				
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'certificates_settings' ) );		
    }
}
?>