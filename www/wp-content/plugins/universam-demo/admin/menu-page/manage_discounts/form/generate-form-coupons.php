<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_coupons extends USAM_Edit_Form
{	
	protected $action = 'generate';	
	
	protected function get_title_tab()
	{ 	
		return __('Генерация купонов', 'usam');	
	}

	protected function get_data_tab(  )
	{			
		$this->data = array('format' => 'U*********', 'type_format' => 'n', 'quantity' => 5, 'description' => '', 'use_once' => '', 'action' => '', 'is_used' => 0, 'max_is_used' => 0, 'value' => '', 'active' => 0, 'start' => date('Y-m-d H:i:s'), 'expiry' => date('Y-m-d H:i:s', time()+3600*24*360),  'is_percentage' => 0, 'customer' => 0, 'amount_bonuses_author' => 0, 'condition' => array() );			
	}	

	function coupon_generate_settings( )
	{			
		?>	
		<table class="usam_edit_table subtab-detail-content-table">
			<tr class="coupon_code">
				<td class="name"><?php _e( 'Формат купона', 'usam' ); ?>:</td>
				<td><input type='text' size ="60" value='<?php echo $this->data['format']; ?>' name='format'/></td>
				<td class="description"><?php _e( 'Используйте цифры и буквы для фиксированной части кода купона и * для генерируемой', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php _e( 'Тип формата купона', 'usam' ); ?>:</td>
				<td><select name='type_format'>
						<option value='ln' <?php selected($this->data['type_format'],'ln'); ?> ><?php esc_html_e( 'Буквы и цифры', 'usam' ); ?></option>
						<option value='l' <?php selected($this->data['type_format'],'l'); ?> ><?php esc_html_e( 'Буквы', 'usam' ); ?></option>
						<option value='n' <?php selected($this->data['type_format'],'n'); ?>><?php esc_html_e( 'Цифры', 'usam' ); ?></option>							
					</select>										
				</td>
				<td class="description"><?php _e( 'Какие символы использовать в коде создаваемого купона', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php _e( 'Количество', 'usam' ); ?>:</td>
				<td><input type='text' size ="60" value='<?php echo $this->data['quantity']; ?>' name='quantity'/></td>
				<td class="description"><?php _e( 'Сколько купонов создать', 'usam' ); ?></td>
			</tr>
		</table>
		<?php 
	}	

	function coupon_data_settings( )
	{	
		$currency = usam_get_currency_sign();
		?>	
		<table class="usam_edit_table subtab-detail-content-table">			
			<tr>
				<td class="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['expiry'] ); ?></td>
			</tr>					
			<tr>
				<td class="name"><?php _e( 'Выполнить действие', 'usam' ); ?>:</td>
				<td><select name='coupon[action]'>
						<option value='b' <?php selected($this->data['action'],'b'); ?> ><?php esc_html_e( 'Изменить стоимость корзины', 'usam' ); ?></option>
						<option value='s' <?php selected($this->data['action'],'s'); ?>><?php esc_html_e( 'Изменить стоимость доставки', 'usam' ); ?></option>							
					</select>
				</td>
			</tr>
			<tr>
				<td class="name"><?php _e( 'Скидка', 'usam' ); ?>:</td>
				<td><input type='text' value='<?php echo $this->data['value']; ?>' size='10' name='coupon[value]' />
					<select name='coupon[is_percentage]' class="select_type_md">
						<option value='0' <?php selected($this->data['is_percentage'],0); ?> ><?php echo esc_html( $currency ) ?></option>
						<option value='1' <?php selected($this->data['is_percentage'],1); ?>>%</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="name"><?php esc_html_e( 'Использовать один раз', 'usam' ); ?>:</td>
				<td>
					<input type='hidden' value='0' name='coupon[use_once]' />
					<input type='checkbox' value='1' name='coupon[use_once]' <?php checked($this->data['use_once'],1) ?>/>
					<span class='description'><?php esc_html_e( 'Отключение купона после его использования.', 'usam' ) ?></span>
				</td>
			</tr>	
			<tr>
				<?php $max_is_used = empty($this->data['max_is_used'])?'':$this->data['max_is_used']; ?>
				<td class="name"><?php esc_html_e( 'Максимальное число использований', 'usam' ); ?>:</td>
				<td><input type='text' value='<?php echo $max_is_used; ?>' name='coupon[max_is_used]'/></td>
			</tr>				
			<tr>
				<td class="name"><?php _e( 'Владелец', 'usam' ); ?>:</td>
				<td>
					<?php
					$users = get_users(  );									
					?>
					<select name='coupon[customer]'>
						<option value='0' <?php selected( $this->data['customer'],0); ?>><?php esc_html_e( 'Обезличенный', 'usam' ); ?></option>
						<?php												
						foreach ($users as $user)
						{
							?><option value='<?php echo $user->ID; ?>' <?php selected($this->data['customer'],$user->ID); ?>><?php echo $user->user_login; ?></option><?php
						}
						?>
					</select>
				</td>
			</tr>								
			<tr>
				<td class="name"><?php esc_html_e( 'Начислить владельцу', 'usam' ); ?>:</td>
				<td><input type='text' value='<?php echo $this->data['amount_bonuses_author']; ?>' name='coupon[amount_bonuses_author]'/></td>
			</tr>
		</table>
		<?php 
	}
		
	function display_left()
	{				
		usam_add_box( 'usam_generate_settings', __('Параметры генерации купонов','usam'), array( $this, 'coupon_generate_settings' ) );
		
		$this->add_box_status_active( $this->data['active'] );
		$this->add_box_description( $this->data['description'] );	
				
		usam_add_box( 'usam_coupon_data_settings', __('Параметры купона','usam'), array( $this, 'coupon_data_settings' ) );
		usam_add_box( 'usam_condition', __('Условия использования','usam'), array( $this, 'display_rules_work_basket' ), $this->data['condition'] );		
    }
}
?>