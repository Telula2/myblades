<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_certificates extends USAM_Edit_Form
{	
	protected $action = 'generate';	
	
	protected function get_title_tab()
	{ 	
		return __('Генерация сертификатов', 'usam');	
	}

	protected function get_data_tab(  )
	{			
		$this->data = array('format' => 'U*********', 'type_format' => 'n', 'quantity' => 5, 'description' => '', 'use_once' => '', 'action' => '', 'is_used' => 0, 'max_is_used' => 0, 'value' => '', 'active' => 0, 'start' => date('Y-m-d H:i:s'), 'expiry' => date('Y-m-d H:i:s', time()+3600*24*360),  'is_percentage' => 0, 'customer' => 0, 'amount_bonuses_author' => 0, 'condition' => array() );			
	}	

	function coupon_generate_settings( )
	{			
		?>	
		<table class="usam_edit_table subtab-detail-content-table">
			<tr class="coupon_code">
				<td class="name"><?php _e( 'Формат сертификата', 'usam' ); ?>:</td>
				<td><input type='text' size ="60" value='<?php echo $this->data['format']; ?>' name='format'/></td>
				<td class="description"><?php _e( 'Используйте цифры и буквы для фиксированной части кода и * для генерируемой', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php _e( 'Тип формата', 'usam' ); ?>:</td>
				<td><select name='type_format'>
						<option value='ln' <?php selected($this->data['type_format'],'ln'); ?> ><?php esc_html_e( 'Буквы и цифры', 'usam' ); ?></option>
						<option value='l' <?php selected($this->data['type_format'],'l'); ?> ><?php esc_html_e( 'Буквы', 'usam' ); ?></option>
						<option value='n' <?php selected($this->data['type_format'],'n'); ?>><?php esc_html_e( 'Цифры', 'usam' ); ?></option>							
					</select>										
				</td>
				<td class="description"><?php _e( 'Какие символы использовать в коде создаваемого сертификата', 'usam' ); ?></td>
			</tr>
			<tr>
				<td class="name"><?php _e( 'Количество', 'usam' ); ?>:</td>
				<td><input type='text' size ="60" value='<?php echo $this->data['quantity']; ?>' name='quantity'/></td>
				<td class="description"><?php _e( 'Сколько сертификатов создать', 'usam' ); ?></td>
			</tr>
		</table>
		<?php 
	}	

	function coupon_data_settings( )
	{	
		$currency = usam_get_currency_sign();
		?>	
		<table class="usam_edit_table subtab-detail-content-table">			
			<tr>
				<td class="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_datetime_picker( 'start', $this->data['start'] ); ?> - <?php usam_display_datetime_picker( 'end', $this->data['expiry'] ); ?></td>
			</tr>				
			<tr>
				<td class="name"><?php _e( 'Номинал сертификата', 'usam' ); ?></td>
				<td><input type='text' value='<?php echo $this->data['value']; ?>' size='10' name='value' /></td>
			</tr>					
			<tr>
				<td class="name"><?php _e( 'Владелец', 'usam' ); ?>:</td>
				<td>
					<?php $users = get_users(  ); ?>
					<select name='customer'>
						<option value='0' <?php selected( $this->data['customer'],0); ?>><?php esc_html_e( 'Обезличенный', 'usam' ); ?></option>
						<?php												
						foreach ($users as $user)
						{
							?><option value='<?php echo $user->ID; ?>' <?php selected($this->data['customer'],$user->ID); ?>><?php echo $user->user_login; ?></option><?php
						}
						?>
					</select>
				</td>
			</tr>
		</table>
		<?php 
	}
		
	function display_left()
	{				
		usam_add_box( 'usam_generate_settings', __('Параметры генерации','usam'), array( $this, 'coupon_generate_settings' ) );
		
		$this->add_box_status_active( $this->data['active'] );
		$this->add_box_description( $this->data['description'] );	
				
		usam_add_box( 'usam_coupon_data_settings', __('Параметры сертификата','usam'), array( $this, 'coupon_data_settings' ) );
		usam_add_box( 'usam_condition', __('Условия использования','usam'), array( $this, 'display_rules_work_basket' ), $this->data['condition'] );		
    }
}
?>