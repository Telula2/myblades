<?php	
require_once( USAM_FILE_PATH .'/admin/includes/edit_form.class.php' );
class USAM_Form_Bonuses extends USAM_Edit_Form
{	
	protected function get_title_tab()
	{ 	
		if ( $this->id != null )
			$title = sprintf( __('Изменить правило начисления бонусов %s','usam'), $this->data['name'] );
		else
			$title = __('Добавить правило начисления бонусов', 'usam');	
		return $title;
	}
	
	protected function get_data_tab(  )
	{	
		if ( $this->id != null )
			$this->data = usam_get_data($this->id, 'usam_bonuses_rules');
		else
			$this->data = array('name' => '', 'description' => '', 'start_date' => '', 'end_date' => '', 'active' => 1, 'type' => 'registration', 'value' => '');	
	}	
		
	function display_left()
	{				
		$this->titlediv( $this->data['name'] );	
		$this->add_box_status_active( $this->data['active'] );
		$this->add_box_description( $this->data['description'] );
		usam_add_box( 'usam_settings', __('Параметры','usam'), array( $this, 'box_settings' ) );		
	}
	
	function box_settings( )
	{
		?>		
		<table class='subtab-detail-content-table usam_edit_table'>						
			<tr>
				<td class="name"><?php esc_html_e( 'Интервал', 'usam' );  ?>:</td>
				<td><?php usam_display_date_interval( $this->data['start_date'], $this->data['end_date'] );  ?>
			<tr>					
			<tr>
				<td class="name"><?php esc_html_e( 'Когда начислять бонусы', 'usam' );  ?>:</td>
				<td class = "type_rule" scope="row">
					<select name="type">
						<option value="register" <?php selected($this->data['type'],'register'); ?>><?php echo esc_html__( 'При регистрации', 'usam'); ?></option>
						<option value="review"<?php selected($this->data['type'],'review'); ?>><?php echo esc_html__( 'Автору отзыва', 'usam'); ?></option>
					</select>	
				</td>
			</tr>								
			<tr>
				<td class="name"><?php esc_html_e( 'Количество бонусов', 'usam' ); ?>:</td>
				<td><input type='text' value='<?php echo $this->data['value']; ?>' name='value' size='10' maxlength="10"/></td>
			</tr>		
		</table>	
		<?php
    }	
}
?>