<?php
/*
 * Отображение страницы "Управление скидками"
 */

$default_tabs = array(			
	array( 'id' => 'discount',  'title' => __( 'Скидки на товар', 'usam' ) ), 	
	array( 'id' => 'basket',  'title' => __( 'Корзины', 'usam' ) ),	
	array( 'id' => 'product_day',  'title' => __( 'Товар дня', 'usam' ) ),	
	array( 'id' => 'accumulative',  'title' => __( 'Накопительные', 'usam' ) ),
	array( 'id' => 'certificates',  'title' => __( 'Сертификаты', 'usam' ) ),
	array( 'id' => 'coupons',  'title' => __( 'Купоны', 'usam' ) ),	
	array( 'id' => 'rules_coupons',  'title' => __( 'Правила купонов', 'usam' ) ),
	array( 'id' => 'bonuses',  'title' => __( 'Бонусы', 'usam' ) ),						
);
		

class USAM_Tab extends USAM_Page_Tab
{		
	protected function localize_script_tab()
	{
		return array(				
			'empty_coupon'  => esc_html__( 'Пожалуйста, введите код купона.', 'usam' ),
		);
	}
	
	function help_tabs() 
	{	
		$help = array( 
			array(
				'title' => __( 'Общие', 'usam' ),				
				'content' => __( 'На этой страницы представлены все возможности по управлению скидками товаров','usam' ),
			),		
			array(
				'title' => __( 'Товар дня', 'usam' ),
				'content' => __('Если хотите добавить товар в очередь, укажите в колонке ТОВАР ДНЯ цену, по которой его нужно будет выставить, когда придет его очередь./nДля удаления товара из очереди выставите цены равную нулю./nДля сохранения настроек нажмите кнопку сохранить.','usam')	
				),
			);
		return $help;
	}
} 		