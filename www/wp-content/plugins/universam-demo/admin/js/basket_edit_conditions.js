(function($)
{		
	var logics_display = function() 
	{	
		var box = $(this).parents('tr').attr('id');
		
		if ( $("#"+box+' #logics .variability_logic .selected').length >0 )
		{		
			var property = $(this).data('property');
			var properties = [];
			
			$("#"+box+' #logics .variability_logic .selected').hide();
			$("#"+box+' #logics .variability_logic .selected').each(function()
			{
				var t = $(this);
				properties = t.data('properties').split(',');				
				for (index = 0; index < properties.length; ++index) 
				{						
					if ( properties[index] == property )
					{
						t.show();
						break;
					}
				}
			});
		}
	};
	
	var get_input = function( name, value ) 
	{		
		var attr_name = USAM_Basket_Edit_Conditions.attr_name+"["+USAM_Basket_Edit_Conditions.index+"]";
		input = '<input type="hidden" name="'+attr_name+'['+name+']" value="'+value+'"/>';
		return input;
	};
	
	// Получить блок условий
	var get_condition_blok = function( type, html ) 
	{						
		var $class = 'condition-'+type;		
		html = '<div id ="row-'+USAM_Basket_Edit_Conditions.index+'" class = "condition-block '+$class+'" data-id ="'+USAM_Basket_Edit_Conditions.index+'"><div class = "condition-wrapper">'+html+'</div><a class="button_delete" href="#"></a></div>';	
		return html;
	};
	
	// Получить оператор сравнения условий
	var get_logic_blok = function(  ) 
	{						
		var attr_name = 'c['+USAM_Basket_Edit_Conditions.index+']';	
		html = '<div id ="row-'+USAM_Basket_Edit_Conditions.index+'" class = "condition-block condition-logic condition_logic_and"><span>'+ USAM_Basket_Edit_Conditions.text_and+'</span>'+get_input( 'logic_operator', 'AND' )+'</div>';
		USAM_Basket_Edit_Conditions.index++;
		return html;
	};	
	
	var get_html_expression = function( property, logic, value, property_title, logics_title, value_title ) 
	{							
		var input = get_input( 'type', 'simple' )+get_input( 'property', property )+get_input( 'logic', logic )+get_input( 'value', value );
		html = input+'<div class = "expression-wrapper"><div class = "expression property_expression">'+property_title+'</div><div class = "expression logics_expression">'+logics_title+'</div><div class = "expression value_expression">'+value_title+'</div></div>';	
		USAM_Basket_Edit_Conditions.index++;
		return html;
	};
	
	// Вставка правил
	var insert_rules = function( html ) 
	{
		if ( html != '' )
			USAM_Basket_Edit_Conditions.div_conditions.append( html );			
	};
	
	var save_condition_cart = function() 
	{	
		var modal_id = $('#'+USAM_Basket_Edit_Conditions.modal_id);
		var type_properties = modal_id.find('#type_properties').val();
		var group = modal_id.find('.condition_group #'+type_properties);
		var property = group.find('#properties #select_property option:selected').val();
		var logic = group.find('#logics #select_logic option:selected').val();
		var value = group.find('#property_value').val();
		if ( logic || type_properties == 'products' || type_properties == 'group' || type_properties == 'group_product' || type_properties == 'user' )
		{					
			var property_title = '';		
			var logics_title = '';
			var id = '';
			var html = '';	
			var html_logic = '';			
			
			if ( USAM_Basket_Edit_Conditions.id_row == '' )
			{
				USAM_Basket_Edit_Conditions.attr_name = "c[0]";				
			}
			else
			{
				USAM_Basket_Edit_Conditions.attr_name = '';
				USAM_Basket_Edit_Conditions.wind_contaner.parents('.condition-block').each(function(index, elem) 
				{							
					USAM_Basket_Edit_Conditions.attr_name = "["+elem.id.replace(/[^0-9]/gim,'')+"]" + USAM_Basket_Edit_Conditions.attr_name;
				});		
				USAM_Basket_Edit_Conditions.attr_name = "c[0]"+USAM_Basket_Edit_Conditions.attr_name+"["+USAM_Basket_Edit_Conditions.id_row+"]";					
			}
			USAM_Basket_Edit_Conditions.index = USAM_Basket_Edit_Conditions.div_conditions.children('.condition-block').length;	
			if ( USAM_Basket_Edit_Conditions.index > 0 )
			{						
				html_logic = get_logic_blok( );					
			}	
			else
				USAM_Basket_Edit_Conditions.index = 0;					
			var tab = modal_id.find('#'+type_properties);

			if ( tab.find("#logics #select_logic option:selected").length >0 )
				logics_title = tab.find("#logics #select_logic option:selected").text();			

			switch ( type_properties ) 
			{
				case 'product_attributes':	
				case 'product':							
					if ( property && value )
					{							
						property_title = tab.find('#select_property option:selected').text();					
						
						var html_expression = get_html_expression( property, logic, value, property_title, logics_title, value );						
						html = html + get_condition_blok( 'simple', html_expression );
					}			
				break;				
				case 'group':							
					var add_conditions = '<div class = "title_group">'+USAM_Basket_Edit_Conditions.text_group+'<a href="#condition_cart_window" id = "usam_modal" class = "add_condition_cart_item" data-toggle="modal" data-type="condition_cart_window">'+USAM_Basket_Edit_Conditions.text_add_conditions+'</a></div><div class = "conditions"></div>';						
					html = html + get_condition_blok( 'group', add_conditions+get_input( 'type', 'group' ) );
				break;	
				case 'group_product':							
					var add_conditions = '<div class = "title_group">'+USAM_Basket_Edit_Conditions.text_group+'<a href="#condition_cart_item_window" id = "usam_modal" class = "add_condition_cart_item" data-toggle="modal" data-type="condition_cart_item_window">'+USAM_Basket_Edit_Conditions.text_add_conditions+'</a></div><div class = "conditions"></div>';						
					html = html + get_condition_blok( 'group_product', add_conditions+get_input( 'type', 'group_product' ) );
				break;									
				// Для всей корзины
				case 'products': // Добавить контейнер с условиями для элементов корзины								
					var add_conditions = '<div class = "title_group">'+USAM_Basket_Edit_Conditions.text_product+'<a href="#condition_cart_item_window" id = "usam_modal" class = "add_condition_cart_item" data-toggle="modal" data-type="condition_cart_item_window">'+USAM_Basket_Edit_Conditions.text_add_conditions+'</a></div><div class = "conditions"></div>';			
						
					html = html + get_condition_blok( 'basket_products', add_conditions+get_input( 'type', 'products' ) );		
				break;				
				case 'cart':					
				case 'order_property':									
					if ( property && value )
					{										
						property_title = tab.find('#select_property option:selected').text();	
						
						var html_expression = get_html_expression( property, logic, value, property_title, logics_title, value );						
						html = html + get_condition_blok( 'simple', html_expression );			
					}
				break;		
				case 'user':					
					property_title = tab.find('#select_property option:selected').text();	
						
					var html_expression = get_html_expression( property, logic, '', property_title, logics_title, '' );						
					html = html + get_condition_blok( 'simple', html_expression );	
				break;			
				case 'location':					
					property_title = tab.find('#search_location_1').val();	
					value = group.find('#select_location_1').val();
			
					var html_expression = get_html_expression( 'location', logic, value, property_title, logics_title, '' );						
					html = html + get_condition_blok( 'simple', html_expression );						
				break;					
				case 'users':
				case 'roles':
				case 'weekday':	
				case 'selected_shipping':
				case 'selected_gateway':
				case 'type_price':
				case 'type_payer':				
					property_title = modal_id.find('#type_properties option:selected').text();						
					var name_value_expression = '';	
					var html_logic_blok = '';	
					tab.find('.all_taxonomy input:checked').each(function()
					{	
						$(this).attr( "checked", false );						
						name_value_expression = $(this).parents('.selectit').find('span').html();	
						
						var html_expression = get_html_expression( type_properties, logic, $(this).val(), property_title, logics_title, name_value_expression );						
						html = html + html_logic_blok + get_condition_blok( 'simple', html_expression );	
						html_logic_blok = get_logic_blok( );							
					});		
				break;		
				case 'terms':				
				case 'cart_terms':										
					var name_value_expression = '';	
					var html_logic_blok = '';	
					property_title = tab.find('#select_property option:selected').text();			
					
					tab.find('.all_taxonomy input:checked').each(function()
					{	
						$(this).attr( "checked", false );		
						name_value_expression = $(this).parents('.selectit').text();	
						
						var html_expression = get_html_expression( property, logic, $(this).val(), property_title, logics_title, name_value_expression );						
						html = html + html_logic_blok + get_condition_blok( 'simple', html_expression );	
						html_logic_blok = get_logic_blok( );	
					});		
				break;					
			}		
			if ( html != '' )
				insert_rules( html_logic + html );
		}
	};
	
	var default_option = function() 
	{			
		var type = $(this).data('type');
		var id_windows = $('#'+type+' #type_properties').val();

		$('#properties input').attr('checked',false);
		$('#logics input').attr('checked',false);
		$('#property_value').val('');		
		
		$('#logics .variability_logic .selected').hide();	
		$('.condition_group table tr').hide();
		$('.condition_group table tr#'+id_windows).show();	
		
		$('#condition_cart_item_window table tr#'+id_windows).show();			
	};
	
	var show_help_box = function() 
	{					
		var box = $(this).parents('tr');
		var id = $(this).attr('id');
		box.find("#help_container .help").removeClass('show');
		box.find("#help_container .help").addClass('hidden');
		box.find("#help_container .help-"+id).removeClass('hidden');
		box.find("#help_container .help-"+id).addClass('show');			
	};
	
	var type_properties = function() 
	{				
		var id = $(this).val();			
		$('.condition_group table tr').hide();
		$('.condition_group table tr#'+id).show();
	};
	
	var change_condition_logic = function() 
	{				
		if ( $(this).hasClass("condition_logic_and") ) 
		{
			$(this).removeClass('condition_logic_and');
			$(this).addClass('condition_logic_or');	
			$(this).find('span').html(USAM_Basket_Edit_Conditions.text_or);
			$(this).find('input').val('OR');
		}
		else
		{
			$(this).removeClass('condition_logic_or');
			$(this).addClass('condition_logic_and');	
			$(this).find('span').html(USAM_Basket_Edit_Conditions.text_and);	
			$(this).find('input').val('AND');					
		}
	};
	
	// Удалить условие
	var delete_properties = function(event) 
	{					
		event.preventDefault();			
		var expression = $(this).closest('.condition-block');		
		var container = expression.parent('div');		
		var size = container.children().length;
		var index = expression.index();			
		if ( size > 1 && index != 0 )
		{
		
			expression.prev().remove();				
		}
		else if ( size > 1 )
		{
			expression.next().remove();	
		}
		expression.remove();
	};	
	
	
	var set_id_row = function() 
	{						
		USAM_Basket_Edit_Conditions.modal_id = $(this).data('type');
		
		if ( $(this).closest('.condition-block').length )
		{
			USAM_Basket_Edit_Conditions.wind_contaner = $(this).closest('.condition-block');			
			USAM_Basket_Edit_Conditions.id_row = $(this).closest('.condition-block').attr('id').replace(/[^0-9]/gim,'');
			USAM_Basket_Edit_Conditions.div_conditions = USAM_Basket_Edit_Conditions.wind_contaner.children('.condition-wrapper').children('.conditions');
		}
		else
		{
			USAM_Basket_Edit_Conditions.wind_contaner = $('.container_condition');
			USAM_Basket_Edit_Conditions.div_conditions = USAM_Basket_Edit_Conditions.wind_contaner;
			USAM_Basket_Edit_Conditions.id_row = '';
		}			
	};
	
	var change_terms = function() 
	{						
		var select = $(this).val();				
		var block = $(this).closest('td');		

		block.find('.taxonomy_box').hide();		
		block.find('#group-'+select).show()		
	};
	
	/**
	 * Показать доступную логику
	 */
	jQuery(document).ready(function()
	{	
		jQuery('.condition-logic').live('click', change_condition_logic);	
		jQuery('#usam_modal').live('click', default_option);	
		jQuery('#properties input').live('click', logics_display);	
		
		jQuery('#condition_cart_item_window #save_action').live('click', save_condition_cart);	
		jQuery('#condition_cart_window #save_action').live('click', save_condition_cart);

		jQuery('#usam_modal').live('click', set_id_row);	

		var select = jQuery('#cart_terms #select_property').val();
		jQuery('#cart_terms .taxonomy_box').hide();		
		jQuery('#cart_terms #group-'+select).show();
		
		select = jQuery('#terms #select_property').val();
		jQuery('#terms .taxonomy_box').hide();		
		jQuery('#terms #group-'+select).show();	
			
		jQuery('#cart_terms #select_property').live('change', change_terms);	
		jQuery('#terms #select_property').live('change', change_terms);	
		jQuery('#type_properties').live('change', type_properties);	
		
		jQuery('.container_containing_help .show_help').live('click', show_help_box);	
		jQuery("#help_container .help").addClass('hidden');	

		jQuery('#container_condition .button_delete').live('click', delete_properties);					
	});
})(jQuery);			
