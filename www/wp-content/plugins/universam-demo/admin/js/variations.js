
/**
 * Изменение размера IFRAME на высоту своего содержимого.
 * iframe is a pain in the ass. This is just some mild ointment to put on that pain.
 * In the next iteration (3.9), the iframe content will be pulled out of the iframe and AJAXified.
 * @since  3.8.9
 */ 
var usam_resize_iframe = function() 
{
	var jiframe = jQuery('#usam_variation_forms iframe');
	var iframe = jiframe[0];
	var i_document = iframe.contentDocument;
	var height_elements = [
		i_document,
		i_document.documentElement,
		i_document.body
	];

	// if iframe's parents are somehow hidden, need to briefly display them to get rendered height
	var invisible_parent = jiframe.parents(':not(:visible)');
	if (invisible_parent.length) {
		invisible_parent.show();
	}
	iframe.style.height = '';

	// getting true height of iframes in different browsers is a tricky business
	var content_height = 0;
	for (var i in height_elements) 
	{
		content_height = Math.max(
			content_height,
			height_elements[i].scrollHeight || 0,
			height_elements[i].offsetHeight || 0,
			height_elements[i].clientHeight || 0
		);
	}
	iframe.style.height = content_height + 'px';
	if (invisible_parent.length) {
		invisible_parent.css('display', '');
	}
};

var usam_display_thickbox = function(title, url) 
{ 		
	tb_show( USAM_Variations.thickbox_title.replace('%s', title), url);
};

var usam_display_media = function( attachment_id, post_id, product_title ) 
{ 
	var images_file_frame;
	
	images_file_frame = wp.media.frames.images_file_frame = wp.media( {
		title    : product_title,
		button   : { text : USAM_Variations.button_text },
		library  : { type: 'image' },
		multiple : false
	});			
	
	wp.media.frames.images_file_frame.on( 'open', function() 
	{
		var selection = wp.media.frames.images_file_frame.state().get( 'selection' );		
		if ( attachment_id > 0 ) 
		{
			attachment = wp.media.attachment( attachment_id );
			attachment.fetch();
			selection.add( attachment ? [ attachment ] : [] );
		}
	});
	
	images_file_frame.on( 'select', function() 
	{				
		attachment = images_file_frame.state().get( 'selection' ).first().toJSON();		
		if ( attachment.sizes.thumbnail )
			var src = attachment.sizes.thumbnail.url;
		else
			var src = attachment.sizes.full.url;
				
		var iframe = jQuery('#usam_variation_forms iframe');
		
		var post_data = {
				nonce      : USAM_Variations.set_variation_thumbnail_nonce,
				action     : 'set_variation_thumbnail',
				post_id    : post_id,
				attachment_id : attachment.id,
			};
		var ajax_callback = function(response)
		{	
			if (! response.is_successful)
			{
				alert(response.error.messages.join("\n"));
				return;
			}	
			iframe.contents().find('#thumb_product_' + post_id).attr('src', src);
		};
		jQuery.usam_post(post_data, ajax_callback);	
	});
	images_file_frame.open();
};