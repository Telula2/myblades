/**
 * Управление вариациями
 * @since 1.0
 */
(function($)
{
	$(function()
	{
		jQuery('#parent option:contains("   ")').remove();
		jQuery('#parent').mousedown(function(){
			jQuery('#parent option:contains("   ")').remove();
		});		
	});		
})(jQuery);