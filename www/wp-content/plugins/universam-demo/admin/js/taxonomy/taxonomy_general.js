/**
 * Управление фильтрами в категориях
 * @since 1.0
 */
(function($)
{	
	// Сортировка терминов
	function category_sort(e, ui)
	{
		var order = $(this).sortable('toArray');
		var post_values = {
			action             : 'set_taxonomy_order',		
			sort_order         : order,		
			nonce              : USAM_Taxonomy.set_taxonomy_order_nonce
		},
		response_handler = function(response) 
		{				
			if (! response.is_successful) 
			{
				alert(response.error.messages.join("\n"));
				return;
			}				
		};			
		$.usam_post( post_values, response_handler);
	}

	var submit_handlers = [];

	var disable_ajax_submit = function()
	{
		var t = $('#submit');
		if (t.data('events'))
			submit_handlers = t.data('events').click;
		t.unbind('click');
		t.bind('click', function() {
			var form = $(this).parents('form');
			if (! validateForm( form ) )
				return false;
		});
	};

	var restore_ajax_submit = function() {
		var t = $('#submit');
		t.unbind('click');
		$.each(submit_handlers, function(index, obj) {
			t.bind('click', obj.handler);
		});
	};

	$(function()
	{		
		var table = $('body.edit-tags-php .wp-list-table tbody');
		table.find('tbody tr').each(function(){
			var t = $(this),
				id = t.attr('id').replace(/[^0-9]+/g, '');
			t.data('level', USAM_Term_List_Levels[id]);
			t.data('id', id);
		});
		table.usam_sortable_table({
			stop : category_sort
		});

		$('.edit-tags-php form').attr('enctype', 'multipart/form-data').attr('encoding', 'multipart/form-data');

		$('[name="image"]').bind('change', function() 
		{
			var t = $(this);

			if (t.val())
				disable_ajax_submit();
			else
				restore_ajax_submit();
		});
	
		$('.usam_select_all').click(function(){
			$('input:checkbox', $(this).parent().siblings('.multiple-select') ).each(function(){ this.checked = true; });
			return false;
		});
		$('.usam_select_none').click(function(){
			$('input:checkbox', $(this).parent().siblings('.multiple-select') ).each(function(){ this.checked = false; });
			return false;
		});
	});	
})(jQuery);