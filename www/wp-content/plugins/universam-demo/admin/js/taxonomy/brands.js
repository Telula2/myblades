/**
 * Управление фильтрами в категориях
 * @since 1.0
 */
(function($)
{
	$(function()
	{
		$("#add_new_collection_box").hide();		
		$('a.add_new_collection').bind('click', new_collection_box_display);		
		
		$('#add_new_collection_box a').bind('click', add_collection);		
	});	
	
	/**
	 * Скрыть / показать добавление нового фильтра
	 * @since 1.0
	 */
	var new_collection_box_display = function(event) 
	{	
		event.preventDefault();
		var display = jQuery('#add_new_collection_box').css('display');
		if ( display== 'none')
			jQuery('#add_new_collection_box').fadeIn();
		else
			jQuery("#add_new_collection_box").hide();
	};
	
   	
	/**
	 * Добавление колекции
	 * @since 1.0
	 */
	var add_collection = function(e) 
	{				
		e.preventDefault();	
		var input = $('#new-collection-name');
		var li = "<li>"+input.val()+"</li>";	
		var post_values = {
				action             : 'add_new_collection',
				brand_id           : input.data("brand_id"),
				collection_name    : input.val(),      
				nonce              : USAM_Brands.add_new_collection_nonce
			},
			response_handler = function(response) 
			{				
				input.val('');
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
				jQuery("#collection_brand ul").append(li);	
			};			
		$.usam_post( post_values, response_handler);	
	};
	
	/**
	 * Удаление колекции
	 * @since 1.0
	 */
	var del_cat_filter = function(e) 
	{	
		var id_item = $(this).data('id_select');  		
		e.preventDefault();
		jQuery.confirm({
			'title'		: 'Подтверждение удаления',
			'message'	: 'Вы решили удалить пункт. <br />После удаления его нельзя будет восстановить! Продолжаем?',
			'buttons'	: {
					'Да'	: {
						'class'	: 'blue',
						'action': function()
						{	
							var post_values = {
								action             : 'delete_cat_filter',							
								id_item            : id_item,   
								nonce              : USAM_Brands.delete_cat_filter_nonce
							},
							response_handler = function(response) 
							{				
								if (! response.is_successful) 
								{
									alert(response.error.messages.join("\n"));
									return;
								}				
								jQuery("li#filter_select-" + id_item).remove();
							};			
							$.usam_post( post_values, response_handler);
						}
					},
				'Нет'	: {
					'class'	: 'gray',
					'action': function(){ }	// В данном случае ничего не делаем. Данную опцию можно просто опустить.
				}
			}
		});
	};
	
})(jQuery);