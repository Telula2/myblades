/**
 * Управление атрибутами товара
 * @since 1.0
 */
(function($)
{
	$(function()
	{	
		$('.box_category')
			.delegate('a#delete_category', 'click'   , delete_category)
			.delegate('a#add_category', 'click'   , add_category_box_display);
		
	//	$('a#add_category').bind('click', add_category_box_display);		
	//	$('a#delete_category').bind('click', delete_category);	
		$('#display_category_window #modal_action').bind('click', add_category);	
						
		$('#add_new_term').hide();		
		$('#parent').bind('change', settings_product_attribute);		

		jQuery('#parent option:contains("   ")').remove();
		jQuery('#parent').mousedown(function(){
			jQuery('#parent option:contains("   ")').remove();
		});	
		
		$('#type_product_attributes').bind('change', display_type_option);		
		
		$('#ready_options').delegate('a.add', 'click', add_component_row );		
		jQuery(document).ready(function() 
		{
			display_type_option( );				
		});		
	});	
	
	var settings_product_attribute = function() 
	{		
		var parent = $(this).val();	
		if ( parent == '-1' )
			$('#add_new_term').hide();
		else
			$('#add_new_term').show();	
	};	
	
	var is_numeric = function() 
	{				
		if (this.value.match(/[^0-9]/g)) 
		{
			this.value = this.value.replace(/[^0-9]/g, '');
		}		
	};		
	
	var display_type_option = function() 
	{		
		var select = $('#type_product_attributes').val();				
		if ( select == 'T' || select == 'O' || select == 'D' || select == 'C' || select == 'A' || select == 'U' )
			$('#ready_options').hide();
		else
			$('#ready_options').show();
	
		if ( select == 'N' || select == 'O' )
			$('#ready_options').delegate('input#value', 'change keyup input click', is_numeric );
		else
			$('#ready_options').undelegate('input#value', 'change keyup input click', is_numeric );			
	};	
	
	var add_component_row = function(e) 
	{					
		e.preventDefault();		
		var tr = $(this).closest('tr'),
			clone = tr.clone();

		clone.find('input').val('');		
		clone.insertAfter(tr);				
	};	
	
	/**
	 * Скрыть / показать добавление новой категории
	 * @since 1.0
	 */
	var add_category_box_display = function(e) 
	{	
		e.preventDefault();		
		$('#display_category_window').modal();	
		
		var category_ul = jQuery("#display_category_window .categorychecklist");	
		if ( category_ul.html() == '' )
		{
			var post_values = {
					action             : 'display_category_in_attributes_product',				
					nonce              : USAM_Product_Attributes.display_category_in_attributes_product_nonce,
					term_id            : USAM_Product_Attributes.term_id
				},
				response_handler = function(response) 
				{								
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}				
					category_ul.append(response.obj);	
				};			
			$.usam_post( post_values, response_handler);			
		}
	};
	
	
	var add_category = function() 
	{		
		var a = '<a href="" id="delete_category">'+USAM_Product_Attributes.text_delete+'</a>';		
		if ( $('.box_category #no_data').length )
			$('.box_category #no_data').remove();
		
		$('#display_category_window .categorychecklist input:checked').each(function()
		{				
			var val = $(this).val();
			
			if ( !$('.box_category #in-usam-category-'+val).length )
			{
				var input = $(this).attr('type','hidden').closest('.selectit').html();	
				$(".product_attributes_category .categories table").append( '<tr><td>'+input+a+'</td></tr>');
			}
		});	
		$('#display_category_window').modal('hide');	
	};
	
	var delete_category = function(e) 
	{		
		e.preventDefault();
		$(this).closest('tr').remove();	
	};	
})(jQuery);