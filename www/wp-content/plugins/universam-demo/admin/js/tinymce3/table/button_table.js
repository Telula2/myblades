// Docu : http://wiki.moxiecode.com/index.php/TinyMCE:Create_plugin/3.x#Creating_your_own_plugins
			
(function() 
{		
	tinymce.PluginManager.requireLangPack('product_table');	// Плагин загрузит определенный языковой пакет
	tinymce.create('tinymce.plugins.product_table', {
		/**
		 * Инициализацию плагина, это будет осуществляться после того, плагин был создан. Этот вызов делается когда экземпляр редактора закончил инициализацию, так * что используйте OnInit событие экземпляра редактора, чтобы перехватывать это событие.
		 */
		init : function(ed, url) 
		{				
			// Регистрация команду так, что он может быть вызван с помощью tinyMCE.activeEditor.execCommand('mceExample
			ed.addCommand('product_table', function() {
				ed.windowManager.open({
					url   : ed.documentBaseUrl+"admin-ajax.php?unprotected_query=tinymce_button_table&action=unprotected_query",
					width : 1000,
					height : 500,
					inline : 1
				}, {
					plugin_url : url // Plugin absolute URL
				});
			});

			// Регистрация кнопку для создания таблиц
			ed.addButton('product_table', {
				title : 'Создание таблицы описания товара',
				cmd : 'product_table',
				image : url + '/table-button.png'
			});

			// Add a node change handler, selects the button in the UI when a image is selected
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('product_table', n.nodeName == 'IMG');
			});
		},
		/**
		 * Создает экземпляры контроля, основанные на входящем имени. Этот метод обычно не требуется, так как метод AddButton класса tinymce.Editor является более * простой способ добавления кнопок, но иногда необходимо для создания более сложных элементов управления, как ListBoxes, сплит кнопок и т.д. 
		 * Этот метод можно использовать для создания таких задач.
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},
		/**
		 * Возвращает информацию о плагине в виде массива имя/значение. Нынешние ключи longname, author, authorurl, infourl и version.
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
					longname  : 'product_table',
					author 	  : 'Алексей',
					authorurl : '',
					infourl   : '',
					version   : "1.0"
			};
		}
	});
	// Регистрация плагина
	tinymce.PluginManager.add('product_table', tinymce.plugins.product_table);
	
	tinymce.create('tinymce.plugins.table_add_row', {
		init : function(ed, url) {			
			ed.onClick.add(function(ed, e) {
				e = e.target;

				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});			
		},

		getInfo : function() {
			return {
				longname : 'Добавить строку',
				author : 'Instinct Entertainment',
				authorurl : '',
				infourl : '',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});	
	tinymce.PluginManager.add('table_add_row', tinymce.plugins.table_add_row);	
})();