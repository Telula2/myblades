// Docu : http://wiki.moxiecode.com/index.php/TinyMCE:Create_plugin/3.x#Creating_your_own_plugins

(function() 
{		
 // добавить изображение для страница продукция вместо шорткода
 	tinymce.create('tinymce.plugins.productspage_image', {
		init : function(ed, url) {
			var pb = '<img style="border:1px dashed #888;padding:5% 25%;background-color:#F2F8FF;" src="' + url + '/productspage.jpg" class="productspage_image mceItemNoResize" title="Не удаляйте это изображение, если вы не знаете, что вы делаете." />', cls = 'productspage_image', sep = ed.getParam('productspage_image', '[productspage]'), pbRE;

			pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {return '\\' + a;}), 'g');
			
			// Register commands
			ed.addCommand('productspage_image', function() {
				ed.execCommand('mceInsertContent', 0, pb);
			});

			ed.onInit.add(function() {
				//ed.dom.loadCSS(url + "/css/content.css");
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG' && ed.dom.hasClass(o.node, cls))
							o.name = 'productspage_image';
					});
				}
			});

			ed.onClick.add(function(ed, e) {
				e = e.target;
				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('productspage_image', n.nodeName === 'IMG' && ed.dom.hasClass(n, cls));
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(pbRE, pb);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="productspage_image') !== -1)
							im = sep;
						return im;
					});
			});
		},

		getInfo : function() {
			return {
				longname : 'Вставить изображение в страницу товаров',
				author : 'Алексей',
				authorurl : 'http://wp-universam.ru',
				infourl : 'http://wp-universam.ru',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});
	
	
	 // добавить изображение для страница продукция вместо шорткода
 	tinymce.create('tinymce.plugins.usam_search_image', {
		init : function(ed, url) {
			var pb = '<img style="border:1px dashed #888;padding:5% 25%;background-color:#F2F8FF;" src="' + url + '/productspage.jpg" class="usam_search_image mceItemNoResize" title="Не удаляйте это изображение, если вы не знаете, что вы делаете." />', cls = 'usam_search_image', sep = ed.getParam('usam_search_image', '[search]'), pbRE;

			pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {return '\\' + a;}), 'g');
			
			// Register commands
			ed.addCommand('usam_search_image', function() {
				ed.execCommand('mceInsertContent', 0, pb);
			});

			ed.onInit.add(function() {
				//ed.dom.loadCSS(url + "/css/content.css");
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG' && ed.dom.hasClass(o.node, cls))
							o.name = 'usam_search_image';
					});
				}
			});

			ed.onClick.add(function(ed, e) {
				e = e.target;
				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('usam_search_image', n.nodeName === 'IMG' && ed.dom.hasClass(n, cls));
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(pbRE, pb);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="usam_search_image') !== -1)
							im = sep;
						return im;
					});
			});
		},

		getInfo : function() {
			return {
				longname : 'Вставить изображение в страницу товаров',
				author : 'Алексей',
				authorurl : 'http://wp-universam.ru',
				infourl : 'http://wp-universam.ru',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});
	
	tinymce.create('tinymce.plugins.transactionresultpage_image', {
		init : function(ed, url) {	
			var pb = '<img style="border:1px dashed #888;padding:5% 25%;background-color:#F2F8FF;" src="' + url + '/productspage.jpg" class="transactionresultpage_image mceItemNoResize" title="Не удаляйте это изображение, если вы не знаете, что вы делаете." />', cls = 'transactionresultpage_image', sep = ed.getParam('transactionresultpage_image', '[transactionresults]'), pbRE;
			
			pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {return '\\' + a;}), 'g');
			
			// Register commands
			ed.addCommand('transactionresultpage_image', function() {
				ed.execCommand('mceInsertContent', 0, pb);
			});


			ed.onInit.add(function() {
				//ed.dom.loadCSS(url + "/css/content.css");
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG' && ed.dom.hasClass(o.node, cls))
							o.name = 'transactionresultpage_image';
					});
				}
			});

			ed.onClick.add(function(ed, e) {
				e = e.target;
				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('transactionresultpage_image', n.nodeName === 'IMG' && ed.dom.hasClass(n, cls));
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(pbRE, pb);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="transactionresultpage_image') !== -1)
							im = sep;
						return im;
					});
			});
		},

		getInfo : function() {
			return {
				longname : 'Insert transactionpage Image',
				author : 'Алексей',
				authorurl : 'http://wp-universam.ru',
				infourl : 'http://wp-universam.ru',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});
	
	tinymce.create('tinymce.plugins.checkoutpage_image', {
		init : function(ed, url) {	
			var pb = '<img style="border:1px dashed #888;padding:5% 25%;background-color:#F2F8FF;" src="' + url + '/productspage.jpg" class="checkoutpage_image mceItemNoResize" title="Не удаляйте это изображение, если вы не знаете, что вы делаете." />', cls = 'checkoutpage_image', sep = ed.getParam('checkoutpage_image', '[shoppingcart]'), pbRE;

			pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {return '\\' + a;}), 'g');
			
			// Регистрируют команды
			ed.addCommand('checkoutpage_image', function() {
				ed.execCommand('mceInsertContent', 0, pb);
			});

			ed.onInit.add(function() {
				//ed.dom.loadCSS(url + "/css/content.css");
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG' && ed.dom.hasClass(o.node, cls))
							o.name = 'checkoutpage_image';
					});
				}
			});

			ed.onClick.add(function(ed, e) {
				e = e.target;

				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('checkoutpage_image', n.nodeName === 'IMG' && ed.dom.hasClass(n, cls));
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(pbRE, pb);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="checkoutpage_image') !== -1)
							im = sep;
						return im;
					});
			});
		},

		getInfo : function() {
			return {
				longname : 'Insert shoppingcart Image',
				author : 'Алексей',
				authorurl : 'http://wp-universam.ru',
				infourl : 'http://wp-universam.ru',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});
	
	tinymce.create('tinymce.plugins.userlogpage_image', 
	{
		init : function(ed, url) {			
			var pb = '<img style="border:1px dashed #888;padding:5% 25%;background-color:#F2F8FF;" src="' + url + '/productspage.jpg" class="userlogpage_image mceItemNoResize" title="Не удаляйте это изображение, если вы не знаете, что вы делаете." />', cls = 'userlogpage_image', sep = ed.getParam('userlogpage_image', '[userlog]'), pbRE;
			
			pbRE = new RegExp(sep.replace(/[\?\.\*\[\]\(\)\{\}\+\^\$\:]/g, function(a) {return '\\' + a;}), 'g');
			
			// Register commands
			ed.addCommand('userlogpage_image', function() {
				ed.execCommand('mceInsertContent', 0, pb);
			});

			ed.onInit.add(function() {
				//ed.dom.loadCSS(url + "/css/content.css");
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG' && ed.dom.hasClass(o.node, cls))
							o.name = 'userlogpage_image';
					});
				}
			});

			ed.onClick.add(function(ed, e) {
				e = e.target;

				if (e.nodeName === 'IMG' && ed.dom.hasClass(e, cls))
					ed.selection.select(e);
			});

			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('userlogpage_image', n.nodeName === 'IMG' && ed.dom.hasClass(n, cls));
			});

			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(pbRE, pb);
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="userlogpage_image') !== -1)
							im = sep;
						return im;
					});
			});
		},

		getInfo : function() {
			return {
				longname : 'Insert user log Image',
				author : 'Алексей',
				authorurl : 'http://wp-universam.ru',
				infourl : 'http://wp-universam.ru',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});	
	
	tinymce.PluginManager.add('productspage_image', tinymce.plugins.productspage_image);
	tinymce.PluginManager.add('usam_search_image', tinymce.plugins.usam_search_image);
	tinymce.PluginManager.add('transactionresultpage_image', tinymce.plugins.transactionresultpage_image);
	tinymce.PluginManager.add('checkoutpage_image', tinymce.plugins.checkoutpage_image);
	tinymce.PluginManager.add('userlogpage_image', tinymce.plugins.userlogpage_image);
})();