
//tinyMCEPopup.executeOnLoad('init();'); 
//document.body.style.display=''; 
//document.getElementById('category').focus();

function init() 
{
	tinyMCEPopup.resizeToInnerSize();
}

function getCheckedValue(radioObj) 
{
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function USAM_Insert_Link() 
{	
	var tab_id = jQuery('.header_tab').find('.current').attr('href').replace('#', '');
	var tagtext = '';		
	switch ( tab_id ) 
	{
		case 'add_product':
			var productid = jQuery('#select_product_1').val();				
			if (productid > 0) 
			{				
				var shortcode = jQuery('[name="usam_product_shortcode"]:checked').val();				
				switch ( shortcode ) 
				{
					case 'buy_now_button':
						tagtext = "[buy_now_button product_id='"+productid+"']";
					break;
					case 'buy_product':
						tagtext = "[buy_product product_id="+productid+"]";
					break;
					case 'usam_products':
						tagtext = "[usam_products product_id='"+productid+"']";
					break;
					case 'usam_product':
						var str = '';						
						if ( jQuery('#usam_product_title').prop( "checked" ) )
						{
							str = str + " title=1";
						}
						if ( jQuery('#usam_product_price').prop( "checked" ) )
						{
							str = str + " price=1";
						}	
						if ( jQuery('#usam_product_add_to_cart').prop( "checked" ) )
						{
							str = str + " add_to_cart=1";
						}							
						if ( jQuery('#usam_product_thumbnail').prop( "checked" ) )
						{
							str = str + " thumbnail=1";
							
							var width = jQuery('#usam_product_image_width').val();
							var height = jQuery('#usam_product_image_height').val();	
							
							str = str + " width="+width;
							str = str + " height="+height;
						}							
						tagtext = "[usam_product product_id='"+productid+"' "+str+"]";
					break;
				}
			} 		
		break;
		case 'product_list':		
			var items_per_page = jQuery('#usam_perpage').val();				
			var shortcode_id = jQuery('#usam_sale_shortcode').val();
			var category_id = jQuery('#usam_category').val();
			var per_page = '';	
		
			if ( category_id > 0 ) 
			{				
				if (items_per_page > 0)
					per_page = "number_per_page='"+items_per_page+"'";
					
				if ( shortcode_id == 1 )
					tagtext = "[usam_products category_id='"+category_id+"' price='sale' "+per_page+"]";
				else 
					tagtext = "[usam_products category_id='"+category_id+"' "+per_page+"]";
			} 
		break;
		case 'slider': 
			//  Слайдер товаров		
			if ( jQuery('#usam_product_slider').val() != '' ) 
			{	
				tagtext = "[usam_slider id='"+jQuery('#usam_product_slider').val()+"']";
			} 	
		break;
		case 'page':
	//  Что с моим заказом	
			if ( jQuery('#search_my_order').prop( "checked" ) ) 
			{	
				tagtext = "[usam_search_my_order]";
			} 		
			if ( jQuery('#shareonline').prop( "checked" ) ) 
			{	
				tagtext = "[shareonline]";
			} 	
			if ( jQuery('#reviews').prop( "checked" ) ) 
			{	
				tagtext = "[reviews]";
			} 		
			if ( jQuery('#contactform').prop( "checked" ) ) 
			{	
				tagtext = "[contactform]";
			} 		
			if ( jQuery('#point_receipt_products').prop( "checked" ) ) 
			{	
				tagtext = "[usam_point_receipt_products]";
			} 			
		break;
		case 'terms':
			//  Список категорий				
			var child_of = jQuery('#terms #usam_category').val( );			
			if ( child_of == '-' ) 
			{
				tagtext = "[usam_showcategories]";
			}	
			else if ( child_of != '' ) 
			{	
				tagtext = "[usam_showcategories child_of='"+child_of+"']";
			} 				
		break;
		case 'map':				
			var GPS_N = jQuery('#map #GPS_N').val( );		
			var GPS_S = jQuery('#map #GPS_S').val( );		
			var title = jQuery('#map #usam_title').val( );	
			var description = jQuery('#map #usam_description').val( );				
			if ( GPS_S != '' &&  GPS_N != '' ) 
			{
				tagtext = "[usam_map n="+GPS_N+" s="+GPS_S+" title='"+title+"' description='"+description+"']";
			}							
		break;	
	}
	if( window.tinyMCE && tagtext != '' ) 
	{		
		tinyMCE.execCommand('mceInsertContent',false, tagtext); 
		tinyMCEPopup.editor.execCommand('mceRepaint');	
		tinyMCEPopup.close();			
	}	
	USAM_submit_false();
	return false;
}

function USAM_submit_false() 
{
	jQuery('form').submit(function(e)
	{
		e.preventDefault();
	});
}