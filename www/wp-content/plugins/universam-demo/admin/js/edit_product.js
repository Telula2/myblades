(function($)
{
	$.extend(USAM_Edit_Product, {
		blur_timeout : null,
		reset_textbox_width : true,
		product_gallery_frame : null,
		image_gallery_ids : '#product_image_gallery',
		product_images : '#product_images_container',

		init : function() 
		{
			$(function(){
				$('.edit_address').attr('disabled', 'disabled');
				$('#product_link_webspy .header_tab').delegate('.tab', 'click', USAM_Edit_Product.external_product_edit);							
				$('#usam_stock_forms').delegate('#not_limited', 'click', USAM_Edit_Product.not_limited);
				$('#usam_stock_forms').delegate('#virtual_product', 'click', USAM_Edit_Product.virtual_product);
				$('#usam_link_webspy').delegate('#usam_webspy_loading_information', 'click', USAM_Edit_Product.webspy_loading_information);			
				$('#table_colors').delegate('td', 'click', USAM_Edit_Product.product_filter_colors);				
				$('#usam_product_images_forms')
					.delegate('.add_product_images a', 'click', USAM_Edit_Product.add_image)	
					.delegate('a.delete', 'click', USAM_Edit_Product.delete_image);	
				
				$('#usam_download_forms').delegate('.file_delete_button', 'click', USAM_Edit_Product.delete_product_file );			
				$(document).delegate('.upload_file_button', 'click', USAM_Edit_Product.product_upload_file );		

				$("#usam_attributes_forms #components").delegate('a.button_add', 'click', USAM_Edit_Product.add_component_row );	

				$(document).delegate('#publish', 'click', USAM_Edit_Product.check_required_fields);		
				
				var $product_images = $( USAM_Edit_Product.product_images ).find( 'ul.product_images' );			
				$product_images.sortable({
					items: 'li.image',
					cursor: 'move',
					scrollSensitivity: 40,
					forcePlaceholderSize: true,
					forceHelperSize: false,
					helper: 'clone',
					opacity: 0.65,
					placeholder: 'usam-metabox-sortable-placeholder',
					start: function( event, ui ) {
						ui.item.css( 'background-color', '#f6f6f6' );
					},
					stop: function( event, ui ) {
						ui.item.removeAttr( 'style' );
					},
					update: function() 
					{
						var attachment_ids = '';

						$( '#product_images_container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
							var attachment_id = $( this ).attr( 'data-attachment_id' );
							attachment_ids = attachment_ids + attachment_id + ',';
						});
						$( USAM_Edit_Product.image_gallery_ids ).val( attachment_ids );
					}
				});
			});			
		},		
		
		delete_image : function( event ) 		
		{			
			event.preventDefault();
			$( this ).closest( 'li.image' ).remove();
			var attachment_ids = '';

			$( '#product_images_container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
				var attachment_id = $( this ).attr( 'data-attachment_id' );
				attachment_ids = attachment_ids + attachment_id + ',';
			});

			$( USAM_Edit_Product.image_gallery_ids ).val( attachment_ids );

			// Remove any lingering tooltips.
			$( '#tiptip_holder' ).removeAttr( 'style' );
			$( '#tiptip_arrow' ).removeAttr( 'style' );

			return false;
		},
		
		add_image : function( event ) 		
		{ 
			event.preventDefault();
			var $el = $( this );	
			var $product_images = $( USAM_Edit_Product.product_images ).find( 'ul.product_images' );			
			if (  USAM_Edit_Product.product_gallery_frame ) {
				USAM_Edit_Product.product_gallery_frame.open();
				return;
			}
			// Create the media frame.
			USAM_Edit_Product.product_gallery_frame = wp.media.frames.product_gallery = wp.media({
				// Set the title of the modal.
				title: $el.data( 'choose' ),
				button: {
					text: $el.data( 'update' )
				},
				states: [
					new wp.media.controller.Library({
						title: $el.data( 'choose' ),
						filterable: 'all',
						multiple: true
					})
				]
			});

			// When an image is selected, run a callback.
			USAM_Edit_Product.product_gallery_frame.on( 'select', function() 
			{
				var selection = USAM_Edit_Product.product_gallery_frame.state().get( 'selection' );
				var attachment_ids = $( USAM_Edit_Product.image_gallery_ids ).val();

				selection.map( function( attachment ) 
				{
					attachment = attachment.toJSON();

					if ( attachment.id ) 
					{ 
						attachment_ids   = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
						var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
						$product_images.append( '<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" /><ul class="actions"><li><a href="#" class="delete dashicons" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></li></ul></li>' );
					}
				});
				$( USAM_Edit_Product.image_gallery_ids ).val( attachment_ids );
			});
			// Finally, open the modal.
			USAM_Edit_Product.product_gallery_frame.open();
		},
				
		add_component_row : function( e ) 		
		{					
			e.preventDefault();		
			var tr = $(this).closest('tr'),
			    clone = tr.clone();

			clone.find('input').val('');		
			clone.insertAfter(tr);			
			
			var i = 0;			
			tr.closest('table').find('tr').each(function(i,elem) 
			{
				i++;
				$(this).find('.n').html(i);			
			});			
		},
		
		delete_component_row : function( e ) 		
		{					
			e.preventDefault();
			var tr = $(this).closest('tr').remove();				
			var i = 0;			
			tr.closest('table').find('tr').each(function(i,elem) 
			{
				i++;
				$(this).find('.n').html(i);			
			});				
		},
		
		check_required_fields : function( ) 		
		{				
			var $submit = true;
			$('input.mandatory').each(function(i,elem) 
			{
				if ( $(this).val() == '' )
				{					
					$(this).css("border", "1px solid #a4286a");	
					$(this).closest("tr").find("#required_field span").show();
					$submit = false;
				}					
			});
			if ( $submit )
				return true;			
			
			var div = $("#usam_attributes_forms").offset().top;		
			$('body').animate({ scrollTop: div }, 1100);			
			$("#usam_attributes_forms #required_field").show();		
			$("#usam_attributes_forms #message_required_field").show();		
			return false;
		},
		
		// Загрузить файл для товар из имеющихся
		product_upload_file : function( ) 		
		{					
			var t = $(this);
			var post_data = {
				'select_product_file[]' : [],
				product_id : t.parent('form.product_upload_file').find('input#hidden_id').val(),
				nonce : t.data('nonce'),
				action : 'upload_product_file'
			};
			var products = jQuery(this).parent("form.product_upload_file").find('input:checkbox:checked').serializeArray();

			for (var index in products) {
				post_data['select_product_file[]'].push(products[index].value);
			}

			jQuery.usam_post(post_data, function(response)
			{				
				tb_remove();
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					return;
				}
				jQuery('#product_files').empty().append(response.obj.content);
			});
			event.preventDefault();
		},
				
		// Удалить загруженный файл
		delete_product_file : function( ) 		
		{
			var t = $(this),
				post_values = {
					action     : 'delete_file',
					file_id    : t.data('file_id'),				
					nonce      : t.data('nonce')
				},
				response_handler = function(response) 
				{
					if (! response.is_successful) {
						alert(response.error.messages.join("\n"));
						return;
					}

					t.closest('.usam_product_download_row').fadeOut('fast', function() {
						$('div.select_product_file p:even').removeClass('alt');
						$('div.select_product_file p:odd').addClass('alt');
						$(this).remove();
					});
				};

			$.usam_post( post_values, response_handler);

			return false;
		},
		
		webspy_loading_information : function( e ) 		
		{
			e.preventDefault();				
			var post_data = {
					'action'     : 'loading_information',	
					'url'        : jQuery('#product_link').val(),		
					'product_id' : USAM_Edit_Product.product_id,					
					'nonce'      : USAM_Edit_Product.loading_information_nonce
				},
				response_handler = function(response) 
				{
					if (! response.is_successful) {
						alert(response.error.messages.join("\n"));
						return;
					}										
					if ( response.obj.status == 1 ) 
					{
						alert(response.obj.error);
					}				
					//и выводим ответ php скрипта
					jQuery('#usam_sku').val(response.obj.sku);
					jQuery('#titlewrap #title').val(response.obj.title);	
					jQuery('#purchase_price').val(response.obj.price);	
					if ( jQuery('#content_ifr')[0] )
					{		
						var frameHTML = jQuery('#content_ifr')[0].contentWindow.document.documentElement;				
						jQuery('#tinymce', frameHTML).append(response.obj.content);
						//$('#tinymce', frameHTML).css('background', 'red');
					}
					else
					{
						jQuery('#post-body-content .wp-editor-area').val(response.obj.content);
					}	
				};
			jQuery.usam_post(post_data, response_handler);			
			return false;
		},
		
		virtual_product : function() 
		{
			jQuery('#usam_stock_forms .product_warehouse').toggleClass('virtual_yes');
		},
		
		not_limited : function() 
		{
			jQuery('#usam_stock_forms .storage_table').toggleClass('stock_is_not_limited');
		},	
		
		product_filter_colors : function() 
		{			
			if ( $(this).hasClass("current") )
			{				
				$(this).removeClass('current');
				$(this).find('input').prop("disabled", true);						
			}
			else
			{				
				$(this).addClass('current');
				$(this).find('input').prop("disabled", false);						
			}			
		},
		
		
		external_product_edit : function( e ) 
		{
			e.preventDefault();
			var id   = $(this).attr('id');
			var link = $(this).parents("#product_link_webspy").find('#webspy').val();	
			if ( link == '' )
				alert(USAM_Edit_Product.message_no_link);
			else
			{
				$('#external_product_type').val(id);
			}
		},				
	});
})(jQuery);
USAM_Edit_Product.init();


function usam_push_v2t(caller, target_slt)
{
	jQuery(target_slt).text(jQuery(caller).val());
}

jQuery(document).ready(function($)
{  
   jQuery( ".pattributes_date_picker" ).datepicker({
		dateFormat: "dd-mm-yy",
		numberOfMonths: 1,
		//showButtonPanel: true,
		hideIfNoPrevNext: true,			
	});		
    if( pagenow == 'edit-usam-product' ) 
	{
		jQuery('.inline-edit-password-input').closest('.inline-edit-group').css('display', 'none');
		var vcl = jQuery('.inline-edit-col input[name="tax_input[usam-variation][]"]').css('display', 'none');
		vcl.each(function(){
			jQuery(this).prev().css('display', 'none');
			jQuery(this).next().css('display', 'none');
			jQuery(this).css('display', 'none');
		});
		jQuery('#bulk-edit select[name=post_parent]').closest('fieldset').css('display', 'none');
		jQuery('.inline-edit-col select[name=post_parent]').parent().css('display', 'none');
		jQuery('.inline-edit-status').parent().css('display', 'none');
    }		
});