/**
* Основные функции универсама
*/
(function($)
{
   /*
	* Обертка для $.post. Помогает в выполнении 'usam_ajax_action' и 'action'.
	* @since  3.8.9
	*/
	$.usam_post = function(data, handler)
	{
		data['usam_ajax_action'] = data['action'];		
		data['action'] = 'usam_ajax';	
		$.post(ajaxurl, data, handler, 'json');
	};	

   /*
	* Обертка для $.get. Заботится о «usam_action» и «action» аргументах данных.
	* @since  3.8.9
	*/
	$.usam_get = function(data, handler) {
		data['usam_ajax_action'] = data['action'];
		data['action'] = 'usam_ajax';
		$.get(ajaxurl, data, handler, 'json');
	};		
})(jQuery);


function usam_set_url_attr( prmName, val )
{
	var res = '';
	var d = location.href.split("#")[0].split("?");
	var base = d[0];
	var query = d[1];
	if(query) {
		var params = query.split("&");
		for(var i = 0; i < params.length; i++) {
			var keyval = params[i].split("=");
			if(keyval[0] != prmName) {
				res += params[i] + '&';
			}
		}
	}
	res += prmName + '=' + val;
	history.replaceState( '' , '', base + '?' + res );
	return false;
};	

function usam_get_attachment_title( attachment_title ) 
{
	var $sumbol = 12,
		ext = attachment_title.replace(/^.*[\.]/ig, ''),
		filename = attachment_title.replace(/^.*[\/\\]/ig, '');	
	if ( filename.length > $sumbol )
	{
		filename = filename.substr(0, $sumbol)+"..."+ext;
	}				
	return filename;
};

// вспомогательная функция, которая форматирует размер файла
function formatFileSize(bytes) 
{
	if (typeof bytes !== 'number') {
		return '';
	}			 
	if (bytes >= 1000000000) {
		return (bytes / 1000000000).toFixed(2) + ' GB';
	} 
	if (bytes >= 1000000) {
		return (bytes / 1000000).toFixed(2) + ' MB';
	} 
	return (bytes / 1000).toFixed(2) + ' KB';
}	
				
	
/**
* Функции которые должны быть на всех страницах административного интерфейса
*/
jQuery(document).ready(function($)
{	
	if ( $('#email_attachments #file_upload').length > 0 )
	{		
		$('#email_attachments #drop a').click(function()
		{				
			$(this).parent().find('input').click();
		});
		
		$('#email_attachments').delegate('li .js_delete_action', 'click', function()
		{		
			var t = $(this).parent();
			var file_name = t.find('input').val();
			post_data   = {
				action     : delete_attachment_action,			    
				file_name  : file_name,
				nonce      : delete_attachment_nonce
			},
			ajax_callback = function(response)
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
			};				
			$.usam_post(post_data, ajax_callback);
			t.remove();						
		});
		
		$('#email_attachments #file_upload').fileupload({         
			url: fileupload_url,
			dataType: 'json',
		   
			dropZone: $('#drop'), 				
			add: function (e, data) 
			{						
				$('input[name="action"]').prop("disabled", true);	
				var tpl = '<input type="text" value="0" data-width="48" data-height="48" data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043"/>';
	 
				var $html = $("<li><div class='js_delete_action'></div><div class='attachment_icon'>"+tpl+"</div><div class='filename'>"+usam_get_attachment_title( data.files[0].name )+"</div><div class='filesize'>"+formatFileSize(data.files[0].size)+"</div></li>");							
	 
				var ul = $('#email_attachments ul'); 
				data.context = $html.appendTo(ul);			 
				$html.find('input').knob();
				$html.find('span').click(function()
				{			 
					if($html.hasClass('working')){
						jqXHR.abort();
					}			 
					$html.fadeOut(function(){
						$html.remove();
					});			 
				});
				var jqXHR = data.submit();
			},
						 
			progress: function(e, data)
			{								
				var progress = parseInt(data.loaded / data.total * 100, 10);								
				data.context.find('input').val(progress).change();		
				if(progress == 100){
					data.context.removeClass('working');
				}
			},
			
			done: function(e, data)
			{ 
				if ( data._response.result.status == 'success' )
				{
					$('input[name="action"]').prop("disabled", false);					
					var attachment = "<input type='hidden' name='fileupload[]' value='"+data._response.result.file_name+"'/>";
					data.context.append( attachment );
					data.context.find('.attachment_icon').html('').css('background-image', "url('"+data._response.result.icon+"')");						
				}
				else
				{
					data.context.addClass('error');	
					data.context.find('.attachment_icon').html(data._response.result.error_message);
				}
			},
	 
			fail:function(e, data)
			{						
				data.context.addClass('error');	
			}
	 
		});		
	}
		
	if ( $('#color_select').length > 0 )
	{
		$('#color_select').each(function(i,elem) 
		{
			var color = $(this).find('.select').data('color');		
			if ( color != 'undefined' )
				$(this).parent().append('<div class="color_view background_'+color+'"></div>');	
		});		
		
		$('.color_view').click(function()
		{				
			$(this).siblings("#color_select").show();
		});
		
		$('#color_select li').click(function()
		{							
			var parent = $(this).parent("#color_select");			
			var $color_view = parent.siblings(".color_view");	
			var color1 = parent.find('.select').removeClass( "select" ).data('color');			
			$(this).addClass( "select" );
			var color2 = $(this).data('color');			
			if ( $color_view.hasClass( 'background_'+color1 ) ) 
			{
				$color_view.removeClass( 'background_'+color1 );
			}
			$color_view.addClass( 'background_'+color2 );
			parent.siblings(".input_color").val(color2);	
			parent.hide();
		});
		
		$(document).click(function( event )
		{							 			
			if ( $(event.target).hasClass("color_view") ) 
				return false;	
			if ( $(event.target).parent().attr("id") == 'color_select' ) 
				return true;				
			
			$('#color_select').hide();	
		});	
	}	
	
	//было сделено изменение
	$('body').delegate('input.show_change, textarea.show_change, select.show_change', 'change', 
	function(){		
		jQuery(this).addClass( "change_made" );		
	});	
		
	$('.chzn-select').each(function(i,elem) {
		if ( $(this).is(":visible") )
		{
			$(this).chosen(); 
		}
	});		 	
	
	//jQuery(".chzn-select").chosen({ width: "300px" });	
	jQuery(".chzn-select-deselect").chosen({allow_single_deselect:true});
	jQuery(".chzn-select-nosearch").chosen({disable_search_threshold: 10});
	
	jQuery('.help_text_box').hover(function(){			
		jQuery(this).find(".help_text").fadeIn(); 		
	},function(){
		jQuery(this).find(".help_text").hide("slow");	 
	});	
	
	jQuery( 'body' ).on( 'click', '.usam_checked', function() 
	{
		if ( jQuery( this ).hasClass( 'checked' ) ) 
		{
			jQuery(this).find('input[type="checkbox"]').attr( 'checked', false );
			jQuery( this ).removeClass( 'checked' );
		} 
		else 
		{
			jQuery(this).find('input[type="checkbox"]').attr( 'checked', true );
			jQuery( this ).addClass( 'checked' );
		}
	} );

	//Добавляет текст в форму поиска
	var search_field_box = jQuery("[data-internal_text]");	
	var searchDefaultText = search_field_box.data("internal_text"); 
	var val = search_field_box.val();
	if ( val == "" || val == searchDefaultText )
	{			
		search_field_box.css("color", "#A9A9A9").val(searchDefaultText);		
	}
	else
	{
		search_field_box.css("color", "black");
	}
	
	search_field_box.focus(function()
	{ 		
		jQuery(this).css("color", "black");			
		if (jQuery(this).val() == searchDefaultText)
		{
			jQuery(this).val("");
		}	
	})
	.blur(function()
	{ 					// - установим обработчик потери фокуса		
		jQuery(this).css("color", "#A9A9A9");
		if ( jQuery(this).val() == ""){
			jQuery(this).val(searchDefaultText);
		}	
	});	
	/* ===========	*/
});


(function($)
{	
	$.extend(Universam_Admin, 
	{						 
		init : function() 
		{				
			$(function()
			{			
			//	Universam_Admin.event_edit_data();			
				$('body')
					.delegate('#review_later', 'click', Universam_Admin.select_remind)
					.delegate('#i_have_seen', 'click', Universam_Admin.close_task);						
			});
		},			
		
		close_task : function(e) 
		{	
			e.preventDefault();	
			
			var	t = $(this).closest('#task_reminder'),
				task_id = t.data('task_id'),
			post_data   = {
				action   : 'select_remind',			    
				task_id  : task_id,
				reminder : 0,
				nonce    : Universam_Admin.select_remind_nonce
			},
			ajax_callback = function(response)
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
			};		
			t.remove();			
			$.usam_post(post_data, ajax_callback);		
		},
		
		select_remind : function(e) 
		{	
			e.preventDefault();	
			
			var	t = $(this).closest('#task_reminder'),
			post_data   = {
				action  : 'select_remind',			    
				task_id : t.data('task_id'),
				remind  : t.find('#select_remind').val(),
				nonce   : Universam_Admin.select_remind_nonce
			},
			ajax_callback = function(response)
			{	
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
			};	
			t.remove();
			$.usam_post(post_data, ajax_callback);		
		},		
	});	
})(jQuery);	
Universam_Admin.init();