/**
* Объект и функции USAM_Calendar.  
*/
(function($)
{
	$.extend(USAM_Calendar, 
	{				
		cell: 0,	
		day_cell_width: 0,
		week_cell_width: 0,
		go_the_events_of_the_day: true,
		
		events : [],
		day_cells : [],
		
		month_number_events : [],	
		day_number_events : [],	
		week_number_events : [],	
		
		event_holder_class : 'usam_event_holder',
		event_class : 'usam_event',
		event_title_class : 'usam_event_title',	
	
		cel_width  : 0,
		cel_height : 0,		
		
		load_month : false,
		load_week : false,
		load_day : false,
		
		/**
		 * Обязательные события
		 */
		init : function() 
		{					
			$(function()
			{			
				$("#usam_page_tabs")
			        .delegate('.view_add_task_window', 'click', USAM_Calendar.show_add_new_task_window)
					.delegate('.usam_event', 'click', USAM_Calendar.get_view_event)
					.delegate('#calendars input', 'click', USAM_Calendar.event_select_calendar)
					.delegate('#tab-month .usam_day-title a', 'click', USAM_Calendar.go_the_events_of_the_day);					
											
				$('#show_details_event').delegate('#save_action', 'click', USAM_Calendar.event_save_action)
							   .delegate('#delete_action', 'click', USAM_Calendar.event_delete_task)
							   .delegate('#edit_action_view', 'click', USAM_Calendar.load_fleds_edit)	
							   .delegate('#edit_action', 'click', USAM_Calendar.edit_edit_task);			
						   
				
				$('body').delegate('.usam_event', 'hover', USAM_Calendar.event_hover)
						 .delegate('.usam_event', 'mouseleave', USAM_Calendar.event_not_hover)
						 .delegate('.calendar_tab .header_tab a', 'click', USAM_Calendar.tab_click);
											
					
				USAM_Calendar.calulate_task_tab_day();
				USAM_Calendar.calulate_task_tab_month();	
				
				USAM_Calendar.tab_load();			
				USAM_Calendar.get_events();							
			});
		},		
		
		tab_load : function() 
		{					
			if ( USAM_Calendar.tab == '' )
				USAM_Calendar.tab = 'tab-month';
		
			$('.calendar_tab .countent_tabs .tab').hide();			
			if ( USAM_Calendar.tab )
			{	
				$('.calendar_tab .header_tab #'+USAM_Calendar.tab).addClass('current');
				$('.calendar_tab .countent_tabs #'+USAM_Calendar.tab).show();
			}
			else
			{					
				$('.calendar_tab .header_tab .tab:first').addClass('current');
				$('.calendar_tab .countent_tabs .tab:first-child').show();		
			}	
		},
		
		tab_click : function(e) 
		{	
			e.preventDefault();	
			
			var li = jQuery(this).closest('li');	
			if ( li.hasClass("sizeable") ) 
				return false;
									
			var tab = li.attr('id');
			var subtab_boxs = li.closest('.calendar_tab');	
			var header_tab = subtab_boxs.find('.header_tab');	
			var countent_tabs = subtab_boxs.find('.countent_tabs');				
						
			header_tab.find('li').removeClass('current');	
			
			if (countent_tabs.find('.tab:visible').length > 0 ) 
			{				
				countent_tabs.children('.tab:visible:first').fadeOut( 100, function() {		
					countent_tabs.children('#'+tab).fadeIn('fast');
				});
			} 
			else 
			{
				countent_tabs.find( '#'+tab ).fadeIn('fast');
			}
			USAM_Calendar.tab = tab;
			
		//	location.hash = tab;	
			li.addClass('current');	
			var	post_data   = {
			    	action    : 'save_tab_calendar',	
					tab       : tab,			
				   	nonce     : USAM_Calendar.save_tab_calendar_nonce
			    },
				ajax_callback = function(response)
				{									
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}											
			    };		
			$.usam_post(post_data, ajax_callback);
			USAM_Calendar.get_events();		
		},		
				
		event_not_hover : function() 
		{			
			$('.usam_event').removeClass('usam_event_hover');				
		},	

		event_hover : function() 
		{ 
			var id = $(this).data('event_id');			
			$('[data-event_id="'+id+'"]').addClass("usam_event_hover");	
		},		
				
		// Получить события
		get_events : function() 
		{
			var tab = $("#calendar_tab .header_tab .current").attr('id');
			var	post_data   = {
			    	action    : 'get_data_calendar',	
					year      : USAM_Calendar.year,
					month     : USAM_Calendar.month,
					day       : USAM_Calendar.day,	
					tab       : tab,					
				   	nonce     : USAM_Calendar.get_data_calendar_nonce
			    },
				ajax_callback = function(response)
				{									
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	} console.log(response.obj);
					USAM_Calendar.events	= response.obj.events;	
					USAM_Calendar.day_cells	= response.obj.day_cells;			
					USAM_Calendar.append_table();				
			    };		
			$.usam_post(post_data, ajax_callback);	
		},
				
		append_table : function( ) 
		{							
			var tab = $("#calendar_tab .header_tab .current").attr('id');
			console.log(USAM_Calendar.day_cells); 	
			var current_day = '';			
			switch ( tab ) 
			{
				case 'tab-day':				
					if ( !USAM_Calendar.load_day )
					{
						USAM_Calendar.load_day = true;											
						var out = '<div id="day_grid" class="usam_days-grid-cont"><table class="usam_days-grid-table usam_table_day_calendar" id="usam_scel_table_day">';
						var hour;
						for(var i = 0; i <= 23; i++) 
						{
							USAM_Calendar.day_number_events[i] = 0;		
							if ( i < 10)
								hour = '0'+i;
							else
								hour = i;
							
							out = out + '<tr class="usam_days-tbl-grid"><td class="usam_hour"><div class="usam_tasks_cell">';
							out = out + '<div class="usam_time"><div class="usam_timeline_title">'+hour+':00</div></div>';
							out = out + '<div class="usam_tasks_box">';							
							out = out + '<div id="usam_ind-'+i+'_1" class="row_1 view_add_task_window"></div>';
							out = out + '<div id="usam_ind-'+i+'_2" class="row_2 view_add_task_window">';
							out = out + '</div></div></div></td></tr>';
						}
						out = out + '</table></div>';		
						$(out).appendTo(".countent_tabs #tab-day .usam_days-grid-td");

						USAM_Calendar.cel_width = $(".countent_tabs #tab-day .row_1").width();	
						USAM_Calendar.cel_height = $(".countent_tabs .usam_day ").height();							
						USAM_Calendar.display_events();		
					}
				break; 
				case 'tab-week':							
					if ( !USAM_Calendar.load_week )
					{
						USAM_Calendar.load_week = true;					
						var table = '<table class="usam_table_day_calendar" id="usam_scel_table_day">'
						for(var i = 0; i <= 23; i++) 
						{
							table = table +'<tr class="usam_days-tbl-grid">';
							table = table +'<td class="usam_hour"><div class="usam_tasks_cell">';								
							table = table +'<div id="usam_ind-'+i+'_1" class="row_1 view_add_task_window"></div><div id="usam_ind-'+i+'_2" class="row_2 view_add_task_window"></div>';							
							table = table +'</div></td></tr>';
						}
						table = table +'</table>';						
						
						var out = '<div class="usam_time">'
						var hour;
						for(var i = 0; i <= 23; i++) 
						{
							if ( i < 10)
								hour = '0'+i;
							else
								hour = i;
							out = out +'<div class="usam_timeline_title">'+hour+':00</div>';
						}
						out = out +'</div>';
						out = out+'<div id="days_grid" class="usam_days-grid-cont"><table class="usam_days-grid-table"><thead><tr>';							
						var html_cell = '';
						jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
						{
							var date_day_cell = new Date( day_cell );	
							var current_date = new Date( );									
							if ( date_day_cell.getDate() == current_date.getDate() && date_day_cell.getMonth() == current_date.getMonth() && date_day_cell.getFullYear() == current_date.getFullYear() )	
								current_day = 'usam_current_day';
							else
								current_day = '';							
							
							html_cell = '<div class="usam_day-title '+current_day+'"><a href="" class="usam_day-link" title="'+USAM_Calendar.message_view_event_day+'" id="usam_day-lnk-'+i+'">'+date_day_cell.getDate()+'</a></div>';
							out = out +'<td class="usam_day">'+html_cell + '</td>';
						});		
						out = out+'</tr></thead><tbody><tr>';	
						jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
						{
							USAM_Calendar.week_number_events[i] = 0;														
							out = out +'<td id = "usam_cell-'+i+'" class="usam_day"><div class="usam_day_cell">'+ table +'</div></td>';
						});		
						out = out+'</tr></tbody></table></div>';					
						$(out).appendTo(".countent_tabs #tab-week .usam_days-grid-td");		
						
						USAM_Calendar.cel_width = $(".countent_tabs .usam_days-tbl-grid").width();	
						USAM_Calendar.cel_height = $(".countent_tabs .usam_days-tbl-grid").height();							
						
						USAM_Calendar.display_events();	
					}
				break;
				case 'tab-month':	
					if ( !USAM_Calendar.load_month )
					{
						USAM_Calendar.load_month = true;
										
						var j = 0;
						var table = $('<div id="days_grid" class="usam_days-grid-cont"><table class="usam_days-grid-table"></table></div>');	
						table.appendTo(".countent_tabs #tab-month .usam_days-grid-td");	
						var out = '';						
						jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
						{
							if ( j == 0 )		
							{								
								out = out + '<tr>';
							}
							USAM_Calendar.month_number_events[i] = 0;	
														
							var date_day_cell = new Date( day_cell );	
							var current_date = new Date( );									
							if ( date_day_cell.getDate() == current_date.getDate() && date_day_cell.getMonth() == current_date.getMonth() && date_day_cell.getFullYear() == current_date.getFullYear() )	
								current_day = 'usam_current_day';
							else
								current_day = '';
							
							out = out+'<td id="usam_ind-'+i+'" class="usam_day view_add_task_window '+current_day+'"><div class="usam_day_cell" style="height: 123px;"><div class="usam_day-title"><a href="" class="usam_day-link" title="'+USAM_Calendar.message_view_event_day+'" id="usam_day-lnk-'+i+'">'+date_day_cell.getDate()+'</a></div></div></td>';							
							j++;
							if ( j == 7 )		
							{
								j = 0;
								out = out + '</tr>';
							}
							//css('min-width', width).css('top', top).css('left', left); 
	
						});							
						$(out).appendTo(".countent_tabs #tab-month .usam_days-grid-table");

						USAM_Calendar.cel_width = $(".countent_tabs #tab-month .usam_day").width();	
						USAM_Calendar.cel_height = $(".countent_tabs #tab-month .usam_day").height();
						
						USAM_Calendar.display_events();
					}
				break;				
			}			
		},
		
		display_events : function( ) 
		{
			if ( $(".countent_tabs #"+USAM_Calendar.tab+" .usam_days-grid-cont "+USAM_Calendar.event_holder_class) )				
				$('<div class ="'+USAM_Calendar.event_holder_class+'"></div>').appendTo(".countent_tabs #"+USAM_Calendar.tab+" .usam_days-grid-cont");	
			
			jQuery.each(USAM_Calendar.events, function(i, event) 
			{					
				switch ( USAM_Calendar.tab ) 
				{
					case 'tab-day':		
						USAM_Calendar.display_events_day( event ); 
					break;
					case 'tab-week':		
						USAM_Calendar.display_events_week( event ); 
					break;
					case 'tab-month':		
						USAM_Calendar.display_events_month( event ); 
					break;
				}						
				return;
			});
		},			
				
		display_events_month : function( event ) 
		{			
			var event_date_from = new Date( event.start );		
			var event_date_to = new Date( event.end );			
			var 
			event_left = 0,
			event_top = 0;			
				
			var event_start	= false,
				number_from,
				number_to,	
				y_offset = 0,
				start_x  = 0,
				start_y  = 0,				
				event_longer_calendar = false;
			// Найдем начальные точки и узнаем можно ли это событие вывести
			jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
			{
				var date_day_cell = new Date( day_cell );											
				if ( event_start == false  )
				{
					if ( date_day_cell.getDate() == event_date_from.getDate() && date_day_cell.getMonth() == event_date_from.getMonth() && date_day_cell.getFullYear() == event_date_from.getFullYear() )	
					{ 
						event_start	= true;
						number_from = i;
					}
					else if ( i == 0 && date_day_cell.getDate() >= event_date_from.getDate() && date_day_cell.getMonth() >= event_date_from.getMonth() && date_day_cell.getFullYear() >= event_date_from.getFullYear() )	
					{
						event_start	= true;
						number_from = i;
					}
				}					
				if ( event_start )
				{						
					if ( USAM_Calendar.month_number_events[i] > 3 )
					{ 		
						USAM_Calendar.month_number_events[i]++;	
						event_start	= false;	
						if ( USAM_Calendar.month_number_events[i] == 5 )
						{												
							var $html = $('<div class="usam_task_day_more" data-count_tasks="">'+USAM_Calendar.message_many_event+'</div>');	
							start_x = i%7;							
							start_y = i/7;
							start_y = Math.floor(start_y);									
							USAM_Calendar.place_block( $html, start_x, start_y, 4, start_x );						
						}
						return false;
					}	
					if ( date_day_cell.getDate() == event_date_to.getDate() && date_day_cell.getMonth() == event_date_to.getMonth() && date_day_cell.getFullYear() == event_date_to.getFullYear())	
					{									
						number_to = i;
						return false;
					}
					if ( i == 35 )
					{  //Задание длиньше, чем календарь
						number_to = i;
						event_longer_calendar = true;
					}
				}
			});				
			if ( !event_start )
				return false;
		
		//	console.log(USAM_Calendar.month_number_events);
			var event_start	= false,
			x = 0,
			y = 0,
			x_event = 1, 
			current_x = 0;
			jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
			{		
				y_offset = USAM_Calendar.month_number_events[i];				
				if ( number_from == i )
				{
					event_start	= true;
					start_x = x;
				}
				if ( event_start )
				{
					USAM_Calendar.month_number_events[i]++;				
					if ( number_to == i )
					{ 
						var $html = USAM_Calendar.event_append(event, event_longer_calendar );
						USAM_Calendar.place_block( $html, start_x, y, y_offset, x );
						return false;
					}						
				}
				if ( x == 6 )
				{					
					if ( event_start )
					{							
						var $html = USAM_Calendar.event_append(event, event_longer_calendar );
						USAM_Calendar.place_block( $html, start_x, y, y_offset, x );
					}					
					x = 0;
					start_x = x;
					y++;
				}
				else
					x++;
			});					
		},				
		
		event_append : function(event, event_longer_calendar ) 
		{			
			var $html = $('<div data-event_id ="'+event.id+'"><div class ="'+USAM_Calendar.event_title_class+'">'+event.title+" "+event.start+" "+event.end+'</div></div>');		
			$html.addClass(USAM_Calendar.event_class);
	
			if ( event_longer_calendar )
				$html.addClass('event_longer_calendar');
			
		//	event_class += ' event_color_'+event.color;							
			
			return $html;
		},		
		
		place_block : function( $html, start_x, start_y, y_offset, current_x ) 
		{		
			var
			height_day = $("#tab-month .usam_days-grid-table tbody").height(),
			width_table = $("#tab-month .usam_days-grid-table tbody").width(),
			width_k = (width_table-(USAM_Calendar.cel_width*7))/7,
			height_k = (height_day-(USAM_Calendar.cel_height*6))/6,
			height_header = $("#tab-month .usam_days-grid-table .usam_day-title").height();
		//	alert(USAM_Calendar.cel_width*7+' '+width_table);	
			var left = start_x*(USAM_Calendar.cel_width+width_k)-1;			
			var top = start_y*(USAM_Calendar.cel_height+height_k)+height_header*(y_offset+1)+y_offset;	
			var width = (current_x-start_x+1)*(USAM_Calendar.cel_width+width_k)-15;
			
			if ( left > 0 )
			{
				left = left + start_x;				
			}					
		//	$html.appendTo("#"+USAM_Calendar.tab+" ."+USAM_Calendar.event_holder_class).css('width', width).css('top', top).css('left', left);
			$html.appendTo(".usam_days-grid-cont").css('width', width).css('top', top).css('left', left);
		},	
		
		// Смена календаря
		event_select_calendar : function() 
		{
			var $select = 0;
			if ( $(this).attr("checked") == 'checked') 
			{ 
				$select = 1;
			}									
			var	post_data   = {
			    	action    : 'select_calendar',	
					calendar  : $(this).val(),			
					select    : $select,							
			    	nonce     : USAM_Calendar.select_calendar_nonce
			    },
				ajax_callback = function(response)
				{				
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}	
					$("#usam_page_tab").html(response.obj.content);
					Universam_Admin.tab_load();
					USAM_Calendar.calulate_task_tab_day();
					USAM_Calendar.calulate_task_tab_month();	
			    };		
			$.usam_post(post_data, ajax_callback);				
			USAM_Calendar.event_modal('hide', '');				
		},
		
		display_events_week : function( event ) 
		{				
			// Найдем начальные точки и узнаем можно ли это событие вывести
			jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
			{
				USAM_Calendar.day_time_processing( event, i, day_cell);	
			});						
		},		
		
		display_events_day : function( event ) 
		{			
			USAM_Calendar.day_time_processing( event, 0, USAM_Calendar.day_cells);
		},
		
		day_time_processing : function( event, i, day_cell ) 
		{			
			var event_date_from = new Date( event.start ),		
				event_date_to = new Date( event.end ),					
				date_day_cell = new Date( day_cell ),
				tab = $(".countent_tabs #"+USAM_Calendar.tab);				
				event_left = 0,
				event_top = 0,			
				min_shift1	= 0,	
				min_shift2	= 0,			
				number_to = 0,	
				height = 0,
				left = 0,
				event_top = 0,
				width = 0,
				event_longer_calendar = false,
				height_day = tab.find(".usam_table_day_calendar tbody").height(),
				width_table = tab.find(".usam_days-grid-table tbody").width(),
				width_k = (width_table-(USAM_Calendar.cel_width*7))/7;
				height_k = (height_day-(USAM_Calendar.cel_height*24))/24,				
				cel_height = USAM_Calendar.cel_height+height_k,
				min_height = cel_height/2,
				hours = 0,
				minutes_height_k = 0,
				width_header = 0,
				height_header = 0;
				if ( tab.find(".usam_days-grid-table thead").length > 0 )
					height_header = tab.find(".usam_days-grid-table thead").height()+3;
			
				if ( tab.find(".usam_days-grid-table .usam_time").length > 0 )
					width_header = tab.find(".usam_days-grid-table .usam_time").width();				
			
			//var date = new Date( date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0 );
			
			if ( date_day_cell.getDate() == event_date_from.getDate() && date_day_cell.getMonth() == event_date_from.getMonth() && date_day_cell.getFullYear() == event_date_from.getFullYear() )	
			{ 					
			//	USAM_Calendar.week_number_events[i][]++;
				USAM_Calendar.week_number_events[i] = 1;
				var $html = USAM_Calendar.event_append(event, event_longer_calendar );						
				if ( event_date_from.getMinutes() < 30 )
					min_shift1 = 0;
				else
					min_shift1 = 1; 	
				if ( event_date_to.getMinutes() == 0 )
					min_shift2 = 0;				
				else if ( event_date_to.getMinutes() < 30 )
					min_shift2 = 1;
				else
					min_shift2 = 1; 	

				minutes_height_k = min_height*min_shift1;			
				left = width_header+i*USAM_Calendar.cel_width+i*width_k+1;			
				event_top = cel_height*event_date_from.getHours()+height_header+minutes_height_k;					
				width = Math.floor((USAM_Calendar.cel_width-20)/USAM_Calendar.week_number_events[i]);
				if ( event_date_from.getDate() == event_date_to.getDate() )
					hours = event_date_to.getHours() - event_date_from.getHours();
				else
					hours = 24 - event_date_from.getHours();
				
				height = cel_height * hours -6 + min_height * min_shift2-minutes_height_k;
				
		console.log(event.id+' '+event.title+' height_header='+height_header);
		console.log(height_day);
	
				if ( left > 0 )
				{
			//		left = left + start_x;				
				}				
				$html.appendTo("#"+USAM_Calendar.tab+" ."+USAM_Calendar.event_holder_class).css('width', width).css('height', height).css('top', event_top).css('left', left);		
			}	
		},
		
	// Удалить задание
		event_delete_task : function() 
		{		
			USAM_Calendar.go_the_events_of_the_day = true;
			var event_id = $('#show_details_event').find('#event_id' ).val();				
			$('.usam_event').remove();
			$('.usam_task_day_more').remove();			
			jQuery.each(USAM_Calendar.events, function(i, event) 
			{					
				if ( event.id == event_id )
				{
					USAM_Calendar.events.splice(i,1);						
					return false;
				}
			});			
			USAM_Calendar.append_table();	
			
			var	post_data   = {
			    	action    : 'delete_event',	
					event_id  : event_id,				
			    	nonce     : USAM_Calendar.delete_event_nonce
			    },
				ajax_callback = function(response)
				{		
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}								
			    };		
			$.usam_post(post_data, ajax_callback);		
			USAM_Calendar.event_modal('hide', '');			
		},
		
		load_fleds_edit : function() 
		{
			USAM_Calendar.edit_event();			
			
			$('#edit_action').show();
			
			$('#edit_action_view').hide();
			$('#delete_action').hide();	
		},
		
		load_fleds_view : function() 
		{
			USAM_Calendar.view_event();			
			
			$('#edit_action').hide();
			
			$('#edit_action_view').show();
			$('#delete_action').show();	
		},
		
		edit_event : function() 
		{
			$('#show_details_event').find('.usam_display_data').hide();	
			$('#show_details_event').find('.usam_edit_data').show();			

			$('#show_details_event').find('select').prop("disabled", false);
			$('#show_details_event').find('textarea').prop("disabled", false);
			$('#show_details_event').find('input').prop("disabled", false);			
		},
		
		view_event : function() 
		{ 
			$('#show_details_event').find('.usam_display_data').show();	
			$('#show_details_event').find('.usam_edit_data').hide();
		},
		
		edit_edit_task : function() 
		{					
			var event_id = $('#show_details_event').find('#event_id' ).val();			
			var	post_data   = {
				event_id       : event_id,			
				action         : 'edit_event',						
				nonce          : USAM_Calendar.edit_event_nonce
			}; 
			USAM_Calendar.set_event( post_data );
				
		/*	var modal = $('#show_details_event');		
			
			var name = $('#show_details_event').find('#event_name').val();		
			$("[data-event_id ='"+event_id+"']").html( name );				
			USAM_Calendar.event_modal('hide', '' );	*/
		},
		
		get_view_event : function() 
		{
			event_title = $(this).find('.usam_event_title').html();			
			USAM_Calendar.event_modal('show', USAM_Calendar.message_edit_name+" "+event_title);				
			
			$('#event_add').hide();			
			$('#event_edit').show();	
			USAM_Calendar.load_fleds_view();						
			USAM_Calendar.go_the_events_of_the_day = false;
			var	post_data   = {
			    	action    : 'view_calendar_event',	
					event_id  : $(this).data('event_id'),				
			    	nonce     : USAM_Calendar.view_calendar_event_nonce
			    },
				ajax_callback = function(response)
				{		
					USAM_Calendar.go_the_events_of_the_day = true;
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}			
					$("#show_details_event #event_name").val( response.obj.title );		
					$("#show_details_event #event_name_display").html( response.obj.title );			
					
					$("#show_details_event #event_description").val( response.obj.description );		
					$("#show_details_event #event_description_display").html( response.obj.description );	
					
					var event_date_from = new Date( response.obj.start );
					var event_day = event_date_from.getDate();	
					if ( event_day < 10 )
						event_day = "0"+event_day;
					var event_month = event_date_from.getMonth()+1;	
					if ( event_month < 10 )
						event_month = "0"+event_month;						
					var date_from = event_day+'-'+event_month+'-'+event_date_from.getFullYear();
					
					var event_date_to = new Date( response.obj.end );						
					var event_day = event_date_to.getDate();	
					if ( event_day < 10 )
						event_day = "0"+event_day;
					var event_month = event_date_to.getMonth()+1;						
					if ( event_month < 10 )
						event_month = "0"+event_month;
					
					var event_hours_from = event_date_from.getHours();	
					if ( event_hours_from < 10 )
						event_hours_from = "0"+event_hours_from;
					
					var event_minutes_from = event_date_from.getMinutes();	
					if ( event_minutes_from < 10 )
						event_minutes_from = "0"+event_minutes_from;
					
					var event_hours_to = event_date_to.getHours();	
					if ( event_hours_to < 10 )
						event_hours_to = "0"+event_hours_to;
					
					var event_minutes_to = event_date_to.getMinutes();	
					if ( event_minutes_to < 10 )
						event_minutes_to = "0"+event_minutes_to;
					
					var date_to = event_day+'-'+event_month+'-'+event_date_to.getFullYear();
			
					$("#show_details_event #usam_date_picker-start").val( date_from );		
					$("#show_details_event #usam_date_hour-start").val( event_hours_from );	
					$("#show_details_event #usam_date_minute-start").val( event_minutes_from );	
					$("#show_details_event #event_date_from_display").html( response.obj.date_from_display );		

					$("#show_details_event #usam_date_picker-end").val( date_to );		
					$("#show_details_event #usam_date_hour-end").val( event_hours_to );	
					$("#show_details_event #usam_date_minute-end").val( event_minutes_to );	
					$("#show_details_event #event_date_to_display").html( response.obj.date_to_display );	
				
					if ( response.obj.reminder == 1 )
					{
						$("#show_details_event #usam_remind").attr('checked', 'checked');
						$("#show_details_event #date_remind").show();							
					}
					else
					{
						$("#show_details_event #usam_remind").attr('checked', false);
						$("#show_details_event #date_remind").hide();	
					}
					if ( response.obj.importance == 1 )
					{ 
						$("#show_details_event #event_importance").attr('checked', 'checked');
						$("#show_details_event #event_importance_display").html( USAM_Calendar.message_event_importance );	
					}
					else
					{
						$("#show_details_event #event_importance").attr('checked', false);
						$("#show_details_event #event_importance_display").html( USAM_Calendar.message_event_unimportant );	
					}						
					$("#show_details_event #event_calendar").val( response.obj.calendar );		
					$("#show_details_event #event_calendar_display").html( response.obj.calendar_name );						
			    };		
			$.usam_post(post_data, ajax_callback);				
			$('#show_details_event').find('#event_id' ).val( $(this).data('event_id') );	
		},
		
		go_the_events_of_the_day : function() 
		{
			USAM_Calendar.go_the_events_of_the_day = false;					
		},
		
		calulate_task_tab_day : function() 
		{
			var width_elem,	_width;				
			$('#tab-day .usam_table_day_calendar .usam_tasks_cell').each(function(i,elem) 
			{				
				width_elem = USAM_Calendar.day_cell_width/$(this).find('.usam_task').length-8;				
				$(this).find('.usam_task').each(function(j,elem_task) 
				{	
					$(this).css({width: width_elem}); 				
				});
			});					
		},
		
		calulate_task_tab_month : function() 
		{
			var top;			
			$('.usam_day_cell').each(function(i,elem) 
			{				
				$(this).find('.usam_task').each(function(j,elem_task) 
				{					
					top = j*20+20;			
					$(this).css({top: top}); 				
				});
			});					
		},
		
		calulate_task_tab_week : function() 
		{
			var width_elem,	_width;			
			$('#tab-week .usam_table_day_calendar .usam_tasks_cell').each(function(i,elem) 
			{				
				width_elem = USAM_Calendar.week_cell_width/$(this).find('.usam_task').length-8;				
				$(this).find('.usam_task').each(function(j,elem_task) 
				{	
					$(this).css({width: width_elem}); 				
				});
			});					
		},

		/**
		 * Событие при нажатие на ячейку времени в календаре
		 */
		show_add_new_task_window : function() 
		{	
			if ( !USAM_Calendar.go_the_events_of_the_day )
				return;		

			USAM_Calendar.load_fleds_edit();	
			$('#event_add').show();			
			$('#event_edit').hide();			
			
			USAM_Calendar.cell = $(this);	
			var args           = $(this).attr('id').split('-'),	
				id             = args[1],			
				current_date = '',
				date = '',
				hour_date_from = '00',
				min_hour_date_from = '00',
				hour_date_to = '00',
				min_hour_date_to = '00',
				jtask        = $('#show_details_event'),
				tab = $(this).closest('.tab').attr('id'),			
				today = new Date(),
				day  = USAM_Calendar.cell.attr('data-day'),
				month  = USAM_Calendar.cell.attr('data-month'),
				year  = USAM_Calendar.cell.attr('data-year');		
				
			switch ( tab ) 
			{
				case 'tab-day':				
					var parent = id.split('_');	
					var args = USAM_Calendar.day_cells.split('-');				
					date = args[2]+'-'+args[1]+"-"+args[0];						
					if ( parent[0] )
						hour_date_from = parseInt(parent[0]);	
					else
						hour_date_from = parseInt(parent[0]);	
					
					if ( USAM_Calendar.cell.hasClass('row_1') )
					{
						min_hour_date_from = '00';	
						min_hour_date_to = '30';
						hour_date_to = hour_date_from;
					}
					else
					{
						min_hour_date_from = '30';
						min_hour_date_to = '00';
						hour_date_to = hour_date_from+1;
					}
					USAM_Calendar.tab	= 'day';
				break;
				case 'tab-week':							
					var parent = id.split('_');
					var cell = $(this).closest('.usam_day').attr('id');
					var id = cell.split('-');
					jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
					{
						if ( id[1] == i)
						{
							current_date = day_cell;
							return false;
						}
					});
					var args = current_date.split('-');				
					date = args[2]+'-'+args[1]+"-"+args[0];						
					if ( parent[0] )
						hour_date_from = parseInt(parent[0]);	
					else
						hour_date_from = parseInt(parent[0]);	
					
					if ( USAM_Calendar.cell.hasClass('row_1') )
					{
						min_hour_date_from = '00';	
						min_hour_date_to = '30';
						hour_date_to = hour_date_from;
					}
					else
					{
						min_hour_date_from = '30';
						min_hour_date_to = '00';
						hour_date_to = hour_date_from+1;
					}				
					USAM_Calendar.tab	= 'week';
				break;
				case 'tab-month':					
					USAM_Calendar.tab	= 'month';
					jQuery.each(USAM_Calendar.day_cells, function(i, day_cell ) 
					{
						if ( id == i)
						{
							current_date = day_cell;
							return false;
						}
					});
					var args = current_date.split('-');
						
					date = args[2]+'-'+args[1]+"-"+args[0];
					hour_date_from = '00';
					min_hour_date_from  = '00';
					hour_date_to = '23';
					min_hour_date_to  = '59';
				break;
			}		
			if ( hour_date_to < 10 )
				hour_date_to = '0'+hour_date_to;	
			
			if ( hour_date_from < 10 )
				hour_date_from = '0'+hour_date_from;
					
			jtask.find('#event_description').val('');
			jtask.find('#event_name').val('');
			jtask.find('#usam_date_picker-start').val(date);
			jtask.find('#usam_date_hour-start').val(hour_date_from);
			jtask.find('#usam_date_minute-start').val(min_hour_date_from);
			jtask.find('#usam_date_picker-end').val(date);
			jtask.find('#usam_date_hour-end').val(hour_date_to);
			jtask.find('#usam_date_minute-end').val(min_hour_date_to);		
			jtask.find('#usam_remind').attr('checked',false);
			jtask.find('#event_importance').attr('checked',false);			
						
			USAM_Calendar.event_modal('show', USAM_Calendar.message_add_event);				
		},		
		
		event_modal : function( view, title ) 
		{				
			$('#show_details_event').modal( view );
			$("#show_details_event .header-title").html( title );
		},		
		
		set_event : function( post_data ) 
		{	
			var jtask        = $('#show_details_event'),
		     	jdescription = jtask.find('#event_description'),
				jname = jtask.find('#event_name'),				
				date_from = jtask.find('#usam_date_picker-start').val(),	
				date_to = jtask.find('#usam_date_picker-end').val(),
				reminder_date = jtask.find('#usam_date_picker-date_time').val();
			
				if ( date_from != '' )
					date_from = date_from + " "+jtask.find('#usam_date_hour-start').val() +":"+ jtask.find('#usam_date_minute-start').val();
				if ( date_to != '' )
					date_to = date_to + " "+jtask.find('#usam_date_hour-end').val() +":"+ jtask.find('#usam_date_minute-end').val();
				if ( reminder_date != '' )
					reminder_date = reminder_date + " "+jtask.find('#usam_date_hour-date_time').val() +":"+ jtask.find('#usam_date_minute-date_time').val();
										
			if ( jname.val() == '' )	
				return false;		
	
			USAM_Calendar.event_modal('hide','');			
			
			post_data.tab = USAM_Calendar.tab;		
			post_data.importance = jtask.find('#event_importance').prop('checked');
			post_data.date_from = date_from;
			post_data.date_to = date_to;
			post_data.reminder_date = reminder_date;
			post_data.name = jname.val();
			post_data.description = jdescription.val();
			post_data.calendar = jtask.find('#event_calendar').val();
			post_data.reminder = jtask.find('#usam_remind').prop('checked');
			
			var	ajax_callback = function(response)
			{								
				if (! response.is_successful ) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
				location.reload();		
			};
			$.usam_post(post_data, ajax_callback);			
		},
		
		/**
		 * Событие при нажатие на ячейку времени в календаре
		 */
		event_save_action : function() 
		{			
			var	post_data   = {
				action         : 'add_event',					
				nonce          : USAM_Calendar.add_event_nonce
			};
			USAM_Calendar.set_event( post_data );
		},		
	});	
})(jQuery);	
USAM_Calendar.init();