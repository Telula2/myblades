/**
 * Управление фильтрами в категориях
 * @since 1.0
 */
(function($)
{	
	var images_file_frame;
	
	$(function()
	{		
		$('.usam_thumbnail a').bind('click', set_thumbnail);	
		$('.usam_thumbnail_remove a').bind('click', delete_thumbnail);			
	});	
	
	/**
	 * Удилить миниатюру
	 */	
	var delete_thumbnail = function(event) 
	{
		event.preventDefault();
		$( '.usam_thumbnail img' ).attr( 'src', USAM_Thumbnail.no_image );
		$( '.usam_thumbnail a' ).data( 'attachment_id', 0 );
		$( '.usam_thumbnail #usam_thumbnail_id' ).val( 0 );
		$( '.usam_thumbnail_remove' ).hide();	
	};
		
	var set_thumbnail = function(event) 
	{
		event.preventDefault();
		button = $( this );	
		var attachment_id = $( this ).data( 'attachment_id' );
		// If the media frame already exists, reopen it.
		if ( images_file_frame ) 
		{	// Set the post ID to the term being edited and open
			images_file_frame.open();
			return;
		} 
		// Create the media frame.
		images_file_frame = wp.media.frames.images_file_frame = wp.media( {
			title    : button.data( 'title' ),
			button   : { text : button.data( 'button_text' ) },
			library  : { type: 'image' },
			multiple : false
		} );		
		// Pre-select selected attachment
		wp.media.frames.images_file_frame.on( 'open', function() {
			var selection = wp.media.frames.images_file_frame.state().get( 'selection' );
			
			if ( attachment_id > 0 ) 
			{
				attachment = wp.media.attachment( attachment_id );
				attachment.fetch();
				selection.add( attachment ? [ attachment ] : [] );
			}
		} );
		
		images_file_frame.on( 'select', function() 
		{				
			attachment = images_file_frame.state().get( 'selection' ).first().toJSON();			
			attachment_id = attachment.id;			
			if ( attachment.sizes.thumbnail )
				var url = attachment.sizes.thumbnail.url;
			else
				var url = attachment.sizes.full.url;
			
			$( '.usam_thumbnail img' ).attr( 'src', url );
			$( '.usam_thumbnail a' ).data( 'attachment_id', attachment_id );
			$( '.usam_thumbnail_remove' ).show();
			$( '.usam_thumbnail #usam_thumbnail_id' ).val( attachment_id );
		} );
		images_file_frame.open();
	};	
})(jQuery);