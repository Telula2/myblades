(function($){	
	
	$.extend( USAM_Sort, {
		init : function() {
			$(function()
			{	
				$(USAM_Sort).trigger('usam_loaded_sort');				
			});
		}
	});
	
	/**
	 * Функция сортировки полей
	 */	 
	USAM_Sort.sort = {
		new_field_count : 0,
		
		/**
		 * Связывание событий
		 */
		event_init : function() {
			var wrapper = jQuery('.wrap');	
		
			wrapper.find('.wp-list-table').
				sortable({
					items       : 'tr',
					axis        : 'y',
					containment : 'parent',
					placeholder : 'checkout-placeholder',
					handle      : '.drag',
					sort        : USAM_Sort.sort.event_sort,
					helper      : USAM_Sort.sort.fix_sortable_helper,					
					update      : USAM_Sort.sort.event_sort_update
				});				
		},
		
		/**
		 * Этот хук, чтобы убедиться, имеет ли строка 100% ширины		
		 */
		fix_sortable_helper : function(e, tr) {

			var row = tr.clone().width(tr.width());
			row.find('td').each(function(index){
				var td_class = jQuery(this).attr('class'), original = tr.find('.' + td_class), old_html = jQuery(this).html();
				jQuery(this).width(original.width());
			});
			return row;
		},
		
		/**
		 * Обновить данные после сортировки
		 */
		event_sort_update : function(e, ui) 
		{	
			var spinner = jQuery(this).find('#ajax-loading');	
			var data = [];	
			var i = 0;
			jQuery("tbody #dragging").each(function()
			{				
				data[i] = $(this).data('id');
				i++;
			});		
			var post_data = {
				action     : 'update_'+USAM_Sort.page+'_sort_fields',				
				nonce      : USAM_Sort.sort_fields_nonce,				
				sort_order : data,
			};			
			var ajax_callback = function(response) 
			{				
				spinner.toggleClass('ajax-loading-active');			
			};			
			spinner.toggleClass('ajax-loading-active');			
			$.usam_post(post_data, ajax_callback);	
		}
	};
	$(USAM_Sort).bind('usam_loaded_sort', USAM_Sort.sort.event_init);
	
})(jQuery);
USAM_Sort.init();