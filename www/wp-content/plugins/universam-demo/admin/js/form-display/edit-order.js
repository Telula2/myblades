// Редактирование заказа
(function($)
{
	$.extend(USAM_Edit_Order, 
	{	
		init : function() 
		{
			$(function()
			{							
				$('.order_details_header').																			
									delegate('.billing-same-as-shipping', 'click', USAM_Edit_Order.billing_same_as_shipping);
				$('#plan_delivery').
									delegate('a#plan_delivery_button', 'click', USAM_Edit_Order.add_plan_delivery_box).
									delegate('a#edit_shipping_date', 'click', USAM_Edit_Order.add_plan_delivery_box).
									delegate('a#delete_shipping_date', 'click', USAM_Edit_Order.delete_shipping_date).
									delegate('#add_plan_delivery_button', 'click', USAM_Edit_Order.add_plan_delivery);			
									
				$('#usam_order_customer').
										delegate('.load_customer_details', 'click', USAM_Edit_Order.load_customer_details).
										delegate('a.edit', 'click', USAM_Edit_Order.edit_customer_details);					
				$('#usam_suggest_customer').delegate('#add_product_to_order', 'click', USAM_Edit_Order.event_suggest_customer_add_product_order);
			
				$('#usam_order_products')
					.delegate('#add_order_item', 'click', USAM_Edit_Order.event_add_order_item)
					.delegate('#order_type_price', 'change', USAM_Edit_Order.event_recalculate_prices)
					.delegate('#_discount_cart', 'change', USAM_Edit_Order.event_discount_cart)	
					.delegate('#show_block_add_products', 'click', USAM_Edit_Order.show_block_add_products)	
					.delegate('#close_block_add_products', 'click', USAM_Edit_Order.close_block_add_products);	
												
				$("#usam_shipped_products")
					.delegate('#add_shipped_button', 'click', USAM_Edit_Order.add_document_shipped)
					.delegate('#send_tracking_email', 'click', USAM_Edit_Order.event_button_send_email_clicked);
										  
				$('#usam_contact_customer').delegate('.subject', 'click', USAM_Edit_Order.toggle_message);
				
				$('#send_mail').delegate('.insert_text li', 'click', USAM_Edit_Order.insert_text);	
				
				$("#usam_payment_history").delegate('#add_payment_button', 'click', USAM_Edit_Order.add_document_payment);
		
				$("#usam_payment_history #new_payment").hide();
				$("#usam_payment_history #cancel_button").hide();				
				$("#usam_payment_history #new_returns").hide();						
				USAM_Edit_Order.purchaselog_transactid();	

				var iframe = $('#usam_contact_customer iframe', parent.document.body);		
				iframe.height($(document.body).height());				
			});					
		},
				
		toggle_message : function(e) 
		{			
			$(this).toggleClass('open');			
			$(this).closest('td').find('.message_display').toggle();			
		},
		
		insert_text : function(e) 
		{				
			e.preventDefault();				
			var massage;
			switch ( $(this).attr('id') ) 
			{				
				case '_order_id': 
					massage = USAM_Edit_Order.order_id;						
				break;
				case '_readiness_date': 
					massage = $("#readiness_date .usam_display_data").html();						
				break;
				case '_date_allow_delivery': 
					massage = $("#date_allow_delivery .usam_display_data").html();						
				break;
				case '_storage_pickup': 
					massage = $("#storage_pickup .usam_display_data span.name").html();						
				break;		
				case '_storage_pickup_phone': 
					massage = $("#storage_pickup .usam_display_data span.phone").html();						
				break;
				case '_storage_pickup_address': 
					massage = $("#storage_pickup .usam_display_data span.address").html();						
				break;					
			}				
			if (jQuery("#wp-message_editor-wrap").hasClass("tmce-active"))
			{		
				tinyMCE.get('message_editor').insertContent( massage );	
			}		
		},
		
		event_discount_cart : function() 
		{				
			$('._cart_product_discount').val( $(this).val() );			
		},	
					
		edit_data_hide : function( t ) 
		{					
			$( t + ' .usam_display_data').show();
			$( t +' .usam_edit_data').hide();			
		},
						
		purchaselog_transactid : function() 
		{
			 USAM_Edit_Order.default_message_input ( USAM_Edit_Order.purchaselog_message_add_payment );
		},	

		default_message_input : function( message ) 
		{
			var field_box = $(".purchase-logs #purchaselog_data");	
			field_box.each( function()
			{			
				if ( $(this).val() == "" )
				{			
					$(this).css("color", "#A9A9A9").val( message );		
				}
				else
				{				
					$(this).css("color", "black");					
				}	
			});
			field_box.focus(function()
			{ 		
				jQuery(this).css("color", "black");			
				if (jQuery(this).val() == message )
				{
					jQuery(this).val("");
				}	
			})
			.blur(function(){ 					// - установим обработчик потери фокуса		
				jQuery(this).css("color", "#A9A9A9");
				if ( jQuery(this).val() == ""){
					jQuery(this).val( message );
				}	
			});				
		},		
		
		add_document_shipped : function(e)
		{
			e.preventDefault();			
			var customer_id =   $('#order_field_userid').val();			
						
			var post_data = {
				'action'     : 'add_document_shipped',				
				'id'         : USAM_Edit_Order.order_id,	
				'nonce'      : USAM_Edit_Order.add_document_shipped_nonce
			};				
			var ajax_callback = function(response) 
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));						
					return;
				}			
				var doc = $(response.obj);
				doc.appendTo('#usam_shipped_documents_box');	
				Universam_Tabs.container_section_edit( doc );					
							
			};			
			$.usam_post(post_data, ajax_callback);	
		},
		
		add_document_payment : function(e)
		{
			e.preventDefault();			
			var customer_id =   $('#order_field_userid').val();			
						
			var post_data = {
				'action'     : 'add_document_payment',				
				'id'         : USAM_Edit_Order.order_id,	
				'nonce'      : USAM_Edit_Order.add_document_payment_nonce
			};				
			var ajax_callback = function(response) 
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));						
					return;
				}		
				var doc = $(response.obj);
				doc.appendTo('#usam_payment_documents_box');	
				Universam_Tabs.container_section_edit( doc );				
			};			
			$.usam_post(post_data, ajax_callback);	
		},
		
	
		load_customer_details : function(e)
		{
			e.preventDefault();			
			var customer_id =   $('#order_field_userid').val();			
			if ( customer_id > 0 )
			{				
				var post_data = {
					'action'     : 'upload_customer_data_in_order',				
					'user_id'    : customer_id,
					'type_payer' : $('#order_field_type_payer').val(),		
					'nonce'      : USAM_Edit_Order.upload_customer_data_in_order_nonce
				};				
				var ajax_callback = function(response) 
				{					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));						
						return;
					}					
					jQuery.each(response.obj, function(i, val) 
					{						
						$('#checkout_item-'+i).val( val );				
					});				
				};			
				$.usam_post(post_data, ajax_callback);				
			}			
		},
		
		// Скопировать платежные данные в доставку
		billing_same_as_shipping : function(e) 
		{
			e.preventDefault();
			var post_data = {
				'action'  : 'billing_same_as_shipping',
				'order_id': $(this).data('order_id'),				
				'nonce'   : USAM_Edit_Order.billing_same_as_shipping_nonce
			};
			var ajax_callback = function(response) 
			{					
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));				
					return;
				}				
				jQuery.each(response.obj, function(index, val) 
				{				
					$('#usam_checkout_form_'+index).val( val );
				});
			};			
			$.usam_post(post_data, ajax_callback);
			return false;			
		},

		event_button_send_email_clicked : function() 
		{
			var t = $(this);
			var post_data = {
				'action'  : 'send_tracking_email',
				'order_id': $(this).data('order_id'),
				'item_id' : $(this).data('item-id'),
				'nonce'   : USAM_Edit_Order.send_tracking_email_nonce
			};
			var ajax_callback = function(response) 
			{				
				
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					t.show().siblings('em').remove();
					return;
				}
				t.siblings('em').addClass('sent').text(USAM_Edit_Order.sent_message);
				t.remove();
			};
			t.hide().after('<em>' + USAM_Edit_Order.sending_message + '</em>');
			$.usam_post(post_data, ajax_callback);
			return false;
		},
			
		// Показать	форму добавления даты доставки
		add_plan_delivery_box : function(e) 
		{	
			e.preventDefault();		
			jQuery(".shipping_date").show();	
			jQuery(".plan_delivery_button_box").hide();		
			jQuery("#shipping_date_display").hide();					
		},	
		
		// Удалить дату доставки
		delete_shipping_date : function(e) 
		{				
			e.preventDefault();		
			jQuery("#delivery_date").val('');
			jQuery("#delivery_date_hour").val('');
			jQuery("#delivery_date_minute").val('');
			jQuery("#shipping_date").remove();
			jQuery("#shipping_date_display").hide();
			jQuery(".plan_delivery_button_box").show();	
		},
		
		// Сохранить дату доставки
		add_plan_delivery : function() 
		{				
			jQuery(".shipping_date").hide();		
			jQuery("#shipping_date_display").show();
			var date = jQuery("#delivery_date").val()+' '+jQuery("#delivery_date_hour").val()+':'+jQuery("#delivery_date_minute").val();
			jQuery("#shipping_date_display b").html( date );
		},
		
		// Пересчитать цены
		event_recalculate_prices : function() 
		{				
			var post_data = {
					nonce               : USAM_Edit_Order.recalculate_prices_nonce,
					action              : 'recalculate_prices',						
					id                  : USAM_Edit_Order.order_id,
					type_price          : $(this).val(),						
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{			
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					return;
				}				
				jQuery.each(response.obj, function() {
				  
				  $("#product_"+this.id+" #_cart_product_price").val(this.price);				
				});
				
			/*	response.obj.each(function(i,elem) {
					
				});*/
				spinner.removeClass('ajax-loading-active');				
			};
			$.usam_post(post_data, ajax_callback);
		},
		
		//Добавить товар в заказ из товаров клиента
		event_suggest_customer_add_product_order : function(e) 
		{		
			e.preventDefault();			
			USAM_Edit_Order.add_product_order( $(this).data('product_id'), 1, 0, 0 );
		},
		
		//Добавить новый товар в заказ	
		add_product_order : function( product_id, quantity, document_shipped, documents_payment ) 
		{	
			var post_data = {
					nonce               : USAM_Edit_Order.add_order_item_nonce,
					action              : 'add_order_item',						
					document_shipped    : document_shipped, 	
					documents_payment   : documents_payment, 						
					id                  : USAM_Edit_Order.order_id,
					product_id          : product_id,					
					quantity            : quantity,						
				};						
			var ajax_callback = function(response) 
			{				
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					return;
				}					
				$('.ajax-loading-active').removeClass('ajax-loading-active');	
				if( response.obj.error == '' )
				{
					if ( document_shipped > 0 )
					{						
						jQuery('#usam_shipped_products_box').html(response.obj.document_shipped);
					}	
					if ( documents_payment > 0 )
					{						
						jQuery('#usam_payment_history_box').html(response.obj.payment_history);	
						jQuery("#usam_payment_history_box #new_payment").hide();
						jQuery("#usam_payment_history_box #new_returns").hide();
					}					
					$(".cart_order_box table").remove();	
					$('.cart_order_box').html(response.obj.cart_product);
					$('.usam_edit_data').hide();
					$('.usam_display_data').show();		
				}
				else
					alert(response.obj.error);	
			};
			$.usam_post(post_data, ajax_callback);
		},
		
//Добавить новый товар в заказ		
		event_add_order_item : function() 
		{					
			var quantity = 1;
			if( jQuery("#select_item_quantity").val() > 0 ) 
			{
				quantity = jQuery("#select_item_quantity").val();
			}
			var document_shipped = 0;			
			if( jQuery("#document_shipped").val() > 0 ) 
			{
				document_shipped = jQuery("#document_shipped").val();
			}	
			var documents_payment = 0;			
			if( jQuery("#documents_payment").val() > 0 ) 
			{
				documents_payment = jQuery("#documents_payment").val();
			}		
			var product_id = jQuery("#select_product_1").val();			

			$(this).siblings('#ajax-loading').addClass('ajax-loading-active');				
			USAM_Edit_Order.add_product_order( product_id, quantity, document_shipped, documents_payment );			
		},
//Скрыть блок добавления товара					
		close_block_add_products : function() 
		{		
			$("#show_block_add_products").show();
			$('.item_add_box').hide();
		},		
	
//Показать блок добавления товара
		show_block_add_products : function() 
		{		
			$(this).hide();
			$('.item_add_box').show();
		},
		
		// Отменить	форму для добавления оплаты заказа	
		сancel_payment : function() 
		{					
			jQuery("#order-add-payment").show();
			jQuery("#cancel_button").hide();			
			jQuery("#usam_payment_history #new_returns").hide();		
			jQuery("#usam_payment_history #new_payment").hide();			
		},		
		
		// Показать	формы для добавления оплаты заказа	
		add_payment_box : function() 
		{					
			jQuery("#cancel_button").show();
			jQuery("#order-add-payment").hide();			
			jQuery("#usam_payment_history #new_payment").show();
			jQuery(".chzn-select-nosearch-load").chosen();	
		},	
		
		// Показать	формы для возврата оплаты заказа	
		add_returns_box : function() 
		{			
			jQuery("#cancel_button").show();		
			jQuery("#order-add-payment").hide();			
			jQuery("#usam_payment_history #new_returns").show();
			jQuery(".chzn-select-nosearch-load").chosen();				
		},			
		
		// Отменить форму добавления документа отгрузки
		cancel_add_shipped_document : function() 
		{		
			jQuery("#add_shipped_document").show();	
			$("#usam_shipped_products tfoot tr").remove()			
		},
				
		edit_customer_details : function(event) 
		{					
			event.preventDefault();
			var box = $(this).closest('#usam_order_customer');
			var post_data = {
					nonce      : USAM_Edit_Order.get_form_edit_customer_details_nonce,
					action     : 'get_form_edit_customer_details',
					form       : 'customer_details',					
					id         : USAM_Edit_Order.order_id,								
				},
				spinner = $(this).siblings('#ajax-loading'),
				t = $(this);		
			spinner.addClass('ajax-loading-active');
			var ajax_callback = function(response)
			{			
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}	
				box.find('.inside').html(response.obj);					
				jQuery(".chzn-select").chosen();				
				jQuery(".chzn-select-nosearch-load").chosen({disable_search_threshold: 10});						
			};
			$.usam_post(post_data, ajax_callback);				
		},	
	});
})(jQuery);
USAM_Edit_Order.init();