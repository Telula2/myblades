/**
* Объект и функции Universam_Mail_Editor.  
* Следующие свойства Universam_Mail_Editor были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
* - Механизм: одноразовый номер используется для проверки запроса для загрузки содержимого вкладок с помощью AJAX
*/
(function($)
{
	$.extend(Universam_Mail_Editor, 
	{	
		block      : '', // Текущий блок, куда нужно вставить
		current_cell  : '', // Текущая ячейка, куда нужно вставить	
		current_column_cell	: '',
		tinymce_id : '',
		including_editing : false,
		images_file_frame : '', 	
		is_resizable : false,		
		
		init : function() 
		{			
			$(function()
			{						
				$('body').delegate('#usam_popup_title .close', 'click', Universam_Mail_Editor.close_iframe)	
						 .delegate('#usam_popup_overlay', 'click', Universam_Mail_Editor.close_iframe);				
				
				$('#usam_mailtemplate_editor').delegate('a.nav-tab', 'click' , Universam_Mail_Editor.hoverleave_edited_block)
				                   .delegate('.usam_block', 'hover', Universam_Mail_Editor.hover_edited_block)
								   .delegate('.usam_divider', 'click', Universam_Mail_Editor.edited_divider)
								   .delegate('.usam_editor_button', 'click', Universam_Mail_Editor.edited_button)
								   .delegate('.usam_text', 'hover', Universam_Mail_Editor.hover_edited_block_text)
								   .delegate('.usam_image', 'hover', Universam_Mail_Editor.hover_block_img)
								   .delegate('.usam_image', 'mouseleave', Universam_Mail_Editor.hoverleave_block_img)			   
								   .delegate('.usam_block', 'mouseleave', Universam_Mail_Editor.hoverleave_edited_block)							
								   .delegate('.usam_text', 'mouseleave', Universam_Mail_Editor.hoverleave_edited_block_text)
								   .delegate('.product_container', 'click', Universam_Mail_Editor.add_products)
								   .delegate('.usam_editable', 'click', Universam_Mail_Editor.change_block_content)
								   .delegate('.usam_controls .remove', 'click', Universam_Mail_Editor.delete_block)
								   .delegate('.usam_controls .background_container .usam_background_open', 'click', Universam_Mail_Editor.background_block)
								   .delegate('.usam_text .usam_tools .remove', 'click', Universam_Mail_Editor.delete_block_text)
								   .delegate('.usam_image .usam_tools .alignment-left', 'click', Universam_Mail_Editor.usam_image_alignment_left)
								   .delegate('.usam_image .usam_tools .alignment-center', 'click', Universam_Mail_Editor.usam_image_alignment_center)
								   .delegate('.usam_image .usam_tools .alignment-right', 'click', Universam_Mail_Editor.usam_image_alignment_right)
								   .delegate('.usam_image .usam_tools .add-link', 'click', Universam_Mail_Editor.usam_image_add_link)
								   .delegate('.usam_image .usam_tools .remove-link', 'click', Universam_Mail_Editor.usam_image_remove_link)
								   .delegate('.usam_image .usam_tools .remove', 'click', Universam_Mail_Editor.usam_image_remove)	
								   .delegate('.product_container a', 'click', Universam_Mail_Editor.close_link)
								   .delegate('a#image_link', 'click', Universam_Mail_Editor.close_link)
								   .delegate('.usam_editable a', 'click', Universam_Mail_Editor.close_link)								   
								   .delegate('#edit_mailtemplate', 'click', Universam_Mail_Editor.edit_mailtemplate)
								   .delegate('.image_placeholder', 'click', Universam_Mail_Editor.usam_image_add_link)
								   .delegate('.usam_background_controls .background_transparent', 'click', Universam_Mail_Editor.remove_background)
								   .delegate('.usam_background_controls .close', 'click', Universam_Mail_Editor.close_background_select);
								   
						    
				$('.widget_styles').delegate('select', 'change', Universam_Mail_Editor.set_tag_style)								 
								   .delegate('.wp-picker-open', 'click', Universam_Mail_Editor.set_color)
								   .delegate('#usam_a_font_underline', 'change', Universam_Mail_Editor.set_underline_a);	
								   
				$('.tab_sending_email').delegate('#save-submit', 'click', Universam_Mail_Editor.save_mailtemplate)
									   .delegate('#save-next', 'click', Universam_Mail_Editor.save_next)
									   .delegate('#select_mailtemplate', 'click', Universam_Mail_Editor.select_mailtemplate)
									   .delegate('#send_preview-submit' , 'click'   , Universam_Mail_Editor.send_preview);
									  
				$('#usam_toolbar_header').delegate('.usam-content', 'click' , Universam_Mail_Editor.show_tab);
						  
				Universam_Mail_Editor.install_draggable();
				Universam_Mail_Editor.load_tabs();
				Universam_Mail_Editor.add_tools();
				
				$(this).find('.usam_controls').hide();		
			//	$('.block_placeholder').addClass('active');	
				
				var myOptions =	
				{
				  collapsible: true,
				  heightStyle: "content",
				}
				$( "#usam_accordion" ).accordion( myOptions );	
				
				$('#usam_mailtemplate_editor').sortable({
					cursor: "move",
					items       : '.usam_block',
				//	axis        : 'y',
					containment : 'parent',
				//	placeholder : 'block_placeholder',
					handle      : '.handle_container .handle',
					appendTo    : '#usam_mailtemplate_content',	
				containment : 'parent',
					helper      : Universam_Mail_Editor.helper_sort, //Устанавливает вид элемента помощника
				//	grid        :[40,40],
					zIndex      : 9999,	
					cursorAt: { left: 80 },		
dropOnEmpty: false,					
					start		: Universam_Mail_Editor.start_sort,					
					stop		: Universam_Mail_Editor.stop_sort,	
					change		: Universam_Mail_Editor.receive_sort,	
					forceHelperSize: false,
					forcePlaceholderSize: false,
				});	
				
				var myOptions = {	
					change: Universam_Mail_Editor.set_color,
				};
				$('input.usam_color').wpColorPicker( myOptions );			
			
				$( ".usam_indentation").resizable({
					resize: Universam_Mail_Editor.start_resizable,				
					handles: "s",
					grid: [ 20, 10 ],	
					alsoResize: ".usam_indentation",
					stop		: Universam_Mail_Editor.stop_resizable,
				});				
				
			});
		},	
		
		stop_resizable: function(event, ui) 
		{
			Universam_Mail_Editor.is_resizable = false;			
		},
		
		start_resizable: function(event, ui) 
		{		
			Universam_Mail_Editor.is_resizable = true;		
			var field = ui.element.closest('.usam_indentation');
			field.find('.usam_resize_handle_text').html(ui.size.height+'px');
			return false;
		},
		
		receive_sort: function(event, ui) 
		{						
			ui.placeholder.addClass('hover');	
		//console.log(ui.placeholder.attr('id'));
		//$(this).addClass('hover');
		},    
		
		helper_sort : function(event, ui) 
		{ 
			var type = ui.data('type');				
			var widget_icon = $('.widget_content #'+type+' .usam_widget_icon_droppable').clone();
						
			widget_icon.addClass('usam_droppable_active');		
	       widget_icon.css('position', 'absolute');
	       widget_icon.css('top', 0);
	   //   widget_icon.css('left', '50%');
		   widget_icon.css('width', '70px');
		   widget_icon.css('height', '70px');
			
			widget_icon.appendTo('body');	    
			
			return $(".usam_droppable_active");
		},
		
		start_sort : function(event, ui) 
		{ 					
			$('.block_placeholder').addClass('active');		
		/*	var type = ui.item.data('type');			
		
			
			var widget_icon = $('.widget_content #'+type+' .usam_widget_icon_droppable').clone();
						
			widget_icon.addClass('usam_droppable_active');		
	        widget_icon.css('position', 'absolute');
	        widget_icon.css('top', 0);
	        widget_icon.css('left', 0);		
	        widget_icon.appendTo('body');	*/
		},
		
		stop_sort : function(event, ui) 
		{ 
			$('.block_placeholder').removeClass('active');	
		},
		
		// Отправить запрос
		send_ajax : function( post_data, ajax_callback ) 
		{				
			post_data.mail_id = Universam_Mail_Editor.mail_id;
			$.usam_post(post_data, ajax_callback);				
		},
				
		// Сохранить размер шрифта
		set_tag_style : function() 
		{ 
			var field = $(this).closest('.usam_form_field');
			var id = field.attr('id');			
			var tag = '';
					
			var font_family = field.find('.usam_font_family').val();	
			var font_size = field.find('.usam_font_size').val();	
			var line_height = field.find('.usam_line_height').val();		
			
			switch ( id ) 
			{
				case 'usam_tag_p':						
					tag = 'p';					
				break;
				case 'usam_tag_h1':						
					tag = 'h1';						
				break;
				case 'usam_tag_h2':						
					tag = 'h2';					
				break;
				case 'usam_tag_h3':						
					tag = 'h3';					
				break;
			}				
			if ( tag != '' )
			{
				$('#usam_mailtemplate_content').find(tag).css('font-size', font_size);	
				$('#usam_mailtemplate_content').find(tag).css('font-size', font_family);
				$('#usam_mailtemplate_content').find(tag).css('line-height', line_height);		
			}
		},
		
		set_color : function(event, ui)
		{
			var id = $(this).closest('.usam_form_field').attr('id');
			var val = ui.color.toString();	
			var tag = '';	
			
			var css = 'color';
			
			switch ( id ) 
			{
				case 'usam_tag_p':						
					tag = 'p';						
				break;
				case 'usam_tag_h1':						
					tag = 'h1';							
				break;
				case 'usam_tag_h2':						
					tag = 'h2';					
				break;
				case 'usam_tag_h3':						
					tag = 'h3';									
				break;
				case 'usam_tag_a':						
					tag = 'a';									
				break;
				case 'usam_newsletter':						
					tag = '.usam_newsletter_background';		
					css = 'background-color';
				break;
				case 'usam_fon':						
					tag = '#usam_newsletter_fon';
					css = 'background-color';						
				break;
			}		
			if ( tag != '' )
				$('#usam_mailtemplate_content').find(tag).css(css, val);	
		},	
		
		set_underline_a : function()
		{
			var underline = $(this).prop( "checked" );
			var text_decoration = 'none';
			if ( underline )
				text_decoration = 'underline';			
			
			$('#usam_mailtemplate_content a').css('text-decoration', text_decoration);	
		},
		
		send_preview : function(e) 
		{					
			e.preventDefault();				
			Universam_Mail_Editor.save_mailtemplate();
			var spinner     = jQuery(this).siblings('#ajax-loading'),			
				post_data   = {
					action          : 'send_preview_mail',	
					'from_email'    : jQuery('#from_email').val(),					
					'mail_id'       : jQuery('#mail_id').val(),	
					nonce           : Universam_Mail_Editor.send_preview_mail_nonce
				},
				ajax_callback = function(response)
				{			
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}			    	
					spinner.toggleClass('ajax-loading-active');	
				};					
			spinner.toggleClass('ajax-loading-active');
			$.usam_post(post_data, ajax_callback);			
			return false;
		},	
		
		// Выбор шаблона
		select_mailtemplate : function(e) 
		{
			e.preventDefault();	
			$('.themes .theme').removeClass('active');						
			var theme = $(this).closest('.theme');
			theme.addClass('active');
			$('#mailtemplate').val( theme.data("mailtemplate") );
		},
		
		usam_image_alignment_left : function() 
		{
			var t = $(this).parents('.usam_content');
			t.removeClass( t.attr('usam_align') );
			t.attr('usam_align','left');
			t.addClass('left');	
			var img_width = t.find('.usam_image img').attr('width');
			var img_height = t.find('.usam_image img').attr('height');			
			t.find('.usam_image').css({'width':img_width, 'height': img_height});
		},
		
		usam_image_alignment_center : function() 
		{
			var t = $(this).parents('.usam_content');
			t.removeClass( t.attr('usam_align') );
			t.attr('usam_align','center');
			t.addClass('center');
			var img_width = t.find('.usam_image img').attr('width');
			var img_height = t.find('.usam_image img').attr('height');			
			t.find('.usam_image').css({'width':img_width, 'height': img_height});
		},
		
		usam_image_alignment_right : function() 
		{
			var t = $(this).parents('.usam_content');
			t.removeClass( t.attr('usam_align') );
			t.attr('usam_align','right');
			t.addClass('right');	
			var img_width = t.find('.usam_image img').attr('width');
			var img_height = t.find('.usam_image img').attr('height');			
			t.find('.usam_image').css({'width':img_width, 'height': img_height});			
		},
		
		usam_image_add_link : function() 
		{			
			Universam_Mail_Editor.current_cell = $(this).closest('.usam_block');
			var block = $(this).closest('.usam_block');				
			Universam_Mail_Editor.position = block.data('position');	
			Universam_Mail_Editor.display_iframe( 'image_data' );		
		},
		
		usam_image_remove_link : function() 
		{				
			$(this).closest('.usam_block').find('.image_placeholder').attr('data-alt', '');
			$(this).closest('.usam_block').find('.image_placeholder').attr('data-url', '');
			$(this).closest('.usam_tools').find('.add-link').removeClass('active');
		},
		
		usam_image_remove : function() 
		{			
			var t = $(this).parents('.usam_content');
			var usam_image = t.find('.usam_image img');		
			t.find('.usam_image').addClass('empty');	
			t.find('.usam_text').addClass('alone');
			usam_image.attr('src','');	
			usam_image.attr('width','0px');	
			usam_image.attr('height','0px');				
			usam_image.addClass('empty');		
			t.find('.usam_image').css({'width':'auto', 'height': 'auto'});	
			t.find('.usam_tools').remove();			
		},
		
		add_tools : function() 
		{				 
			var remove_text = '<ul class="usam_tools"><li><a href="javascript:;" class="remove"><span></span></a></li></ul>';
			var usam_tools_html = '';	
			$('.usam_content').each(function(i,elem) 
			{				
				var left, center, right, add_link;
				switch ( $(this).attr('usam_align') ) 
				{
					case 'left':						
						left = 'active';
					break;
					case 'center':						
						center = 'active';
					break;
					case 'right':						
						right = 'active';
					break;
				}
				if ( $(this).find(".image_placeholder").attr('data-url') != '' )
					add_link = 'active';
				usam_tools_html = '<ul class="usam_tools" style="display: none;">'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_alignment_left+'" class="alignment-left '+left+'"><span></span></a></li>'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_alignment_center+'" class="alignment-center '+center+'"><span></span></a></li>'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_alignment_right+'" class="alignment-right '+right+'"><span></span></a></li>'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_add_link+'" class="add-link '+add_link+'"><span></span></a></li>'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_remove_link+'" class="remove-link"><span></span></a></li>'+
					'<li><a href="javascript:;" title="'+Universam_Mail_Editor.text_remove+'" class="remove"><span></span></a></li>'+
				'</ul>';
				$(this).find('.usam_image img').before( usam_tools_html );	
			});				
			$('.usam_text .usam_tools').css({'display':'none', 'right': '0px'});
		},		
		
		load_tabs : function() 
		{
			$('#usam_toolbar_header .usam-content:eq(0)').addClass('current');
			$('#usam_content_toolbar_tabs .tab:gt(0)').hide();					
		},
		

		show_tab : function(  ) 
		{		
			$('#usam_content_toolbar_tabs .tab').hide();
			$('#usam_content_toolbar_tabs .'+$(this).attr('id')).show();
			
			$('.usam_toolbar_tabs li').removeClass('current');
			$('.usam_toolbar_tabs #'+$(this).attr('id')).addClass('current');				
		},
		
		close_tinymce : function( event ) 
		{		
			if ( Universam_Mail_Editor.including_editing )
			{	
				if ( $(event.target).hasClass("usam_editable") ) 
					return;					
				if ( $(event.target).closest(".usam_editable").length ) 
					return;					
				if ( $(event.target).closest("#tinymce_block").length ) 
					return;
				if ( $(event.target).closest(".mce-panel").length ) 
					return;
				if ( $(event.target).closest(".mce-widget").length ) 
					return;		
				if ( $(event.target).closest(".mce-container").length ) 
					return;	
				if ( $(event.target).closest("#wp-link-wrap").length ) 
					return;									
				if ( $(event.target).closest("#short_codes_name").length ) 
				{ 
					tinyMCE.get('tinymce').insertContent( '%'+$(event.target).data('code')+'%' );	
					return;						
				}		
				if ( $(event.target).closest("#usam_accordion").length ) 
					return;						
				var	content = tinyMCE.get('tinymce').getContent();		
				tinyMCE.get('tinymce').remove();	
				
				Universam_Mail_Editor.tinymce_id.closest('.usam_content').find('.usam_image').show();
				Universam_Mail_Editor.tinymce_id.html( content );				
				Universam_Mail_Editor.including_editing = false;
			}			
		},
			
		// Если нажать на ссылку внутри контейнера с товаром
		close_link : function(e) 
		{	
			e.preventDefault();		
		},
		
		// Получить редактируемый блок текста
		change_block_content : function( e ) 
		{
			if ( !Universam_Mail_Editor.including_editing )
			{ 			
				var height = $(this).height();			
				var width = $(this).width();		
				$(this).closest('.usam_content').find('.usam_image').hide();
				
				$(document).click(Universam_Mail_Editor.close_tinymce);				
				e.preventDefault();					
				var textarea = '<textarea id="tinymce" class="tinymce_enabled required">'+$(this).html()+'</textarea>';
				$(this).html( textarea );			
				tinymce.init({ 					
					selector:'#tinymce',
					plugins: 'textcolor lists tabfocus paste wordpress link image',					
					table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol", 
					menubar: 'edit insert format table tools', 
					theme : "modern", 
					height : height,
					width : width,
					toolbar: 'bold italic | link image | fontsizeselect fontselect | backcolor forecolor | numlist bullist | alignleft aligncenter alignright alignjustify |' 
				});	
				
				Universam_Mail_Editor.including_editing = true;	
				Universam_Mail_Editor.tinymce_id = $(this);				
				return false;
			}
		},
		
		save_next : function() 
		{	
			Universam_Mail_Editor.save_mailtemplate();
			$( "#usam-table_item_form" ).submit();
		},			
		
		// Сохранить рассылку
		save_mailtemplate : function() 
		{			
			var styles = new Object();		
			$('.usam_form_field_tag').each(function(i,elem) 
			{			
				var id = $(this).attr('id');			
				var tag = '';
						
				var font_family = $(this).find('.usam_font_family').val();	
				var font_size = $(this).find('.usam_font_size').val();	
				var line_height = $(this).find('.usam_line_height').val();	
				var color = $(this).find('.usam_color').val();
					
				var mas = { font_family: font_family, font_size: font_size, line_height: line_height, color: color };	
				switch ( id ) 
				{
					case 'usam_tag_p':						
						tag = 'p';	
					break;
					case 'usam_tag_h1':						
						tag = 'h1';	
					break;
					case 'usam_tag_h2':						
						tag = 'h2';	
					break;
					case 'usam_tag_h3':						
						tag = 'h3';	
					break;					
				}	
				styles[tag] = mas;					
				
			});		
			var font_underline = $( '#usam_a_font_underline' ).prop( "checked" );
			var color = $( '#usam_a_font_color' ).val( );			
			styles['a'] = {font_underline: font_underline, color: color};					
			color = $( '#usam_table_background_color' ).val( );			
			styles['newsletter'] = { color: color};	
			color = $( '#usam_fon_background_color' ).val( );		
			styles['fon'] = { color: color};	
									
			blocks = Universam_Mail_Editor.block_cycle( $('.usam_mailtemplate').children('.usam_block') );	
			var post_data = {
					nonce               : Universam_Mail_Editor.save_mailtemplate_nonce,
					action              : 'save_mailtemplate',					
					data                : blocks,		
					styles              : styles,						
				},
				spinner = $(this).siblings('#ajax-loading');	
			spinner.addClass('ajax-loading-active');

			var ajax_callback = function(response) 
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
				spinner.removeClass('ajax-loading-active');
			}					
			Universam_Mail_Editor.send_ajax( post_data, ajax_callback );				
		},		
		
		block_cycle : function( div ) 
		{
			var blocks = {};			
			var	position = 0;
			div.each(function(i,elem) 
			{
				position++;			
				var type = $(this).attr('data-type');						
				switch ( type ) 
				{
					case 'content':						
						var text = new Object();
						var img = new Object();						
						var image = $(this).find('.image_placeholder');	
												
						img.src = image.attr('src');
						img.width = image.attr('width');
						img.height = image.attr('height');
						img.alt = image.attr('data-alt');
						img.url = image.attr('data-url');
						img.alignment = '';
						img.static = '';
						
						text.value = $(this).find('.usam_editable').html();			
						blocks[i] = {						
							'text'    : text,
							'image'    : img,	
							'alignment': $(this).find('.usam_content').attr('usam_align'),
							'static'   : '',						
							'type'     : type,	
						};		
					break;
					case 'indentation':
						var height = $(this).find('.usam_resize_handle_text');					
						blocks[i] = {
							'height'   : height.html(),							
							'type'     : type,	
						};	
					break;		
					case 'button':
						var $button = $(this).find('.usam_editor_button');		
						var button_css = {							
							'color'            : $button.css('color'),
							'font-family'      : $button.css('font-family'),
							'font-size'        : $button.css('font-size'),
							'font-weight'      : $button.css('font-weight'),
							'border-color'     : $button.css('border-color'),							
							'border-radius'    : $button.css('border-radius'),
							'border-width'     : $button.css('border-width'),
							'width'            : $button.css('width'),
							'height'           : $button.css('height'),
							'background-color' : $button.css('background-color'),							
						};							
						blocks[i] = {
							'text'    : $button.text(),							
							'type'    : type,	
							'css'     : button_css,	
							'url'     : $button.attr('href'),							
						};		
					break;						
					case 'divider':
						var img = $(this).find('.usam_divider img');					
						blocks[i] = {
							'src'      : img.attr('src'),
							'width'    : img.attr('width'),
							'height'   : img.attr('height'),	
							'type'     : type,	
						};	
					break;
					case 'column_product':
						var product = {};	
						var line = 0;	
						var column = 0;						
						$(this).find('.block_grid_products tr').each(function( k,elem ) 
						{							
							line++;
							column = 0;	
							$(this).find('td').each(function( k,elem ) 
							{	
								column++;
								var container = $(this).find('.product_container');								
								var product_block = container.find('#product_block');
								var product_id = 0;
								if( product_block.length ) 
								{
									product_id = product_block.data('product_id');
								}
								if ( !product[line] )
									product[line] = {};
								product[line][column] = product_id;
							});	
						});											
						blocks[i] = {
							'content'  : product,	
							'line'     : line,	
							'column'   : column,							
							'type'     : type,									
						};		
					break;
					case 'column':
						var content = {};	
						var line = 0;	
						var column = 0;							
						$(this).find('.block_grid tr').each(function( k,elem ) 
						{														
							line++;	
							if ( !content[line] )
								content[line] = {};
								
							column = 0;							
							$(this).find('td').each(function( k,elem ) 
							{	
								column++;
								var container = $(this).find('.cel_container');									
								var result = Universam_Mail_Editor.block_cycle( container.children('.usam_block') );								
								content[line][column] = result;		
							});															
						});	
						blocks[i] = {
							'content'  : content,	
							'line'     : line,	
							'column'   : column,							
							'type'     : type,									
						};									
					break;
				}
				blocks[i]['background'] = $(this).css('background-color');			
			});	
			return blocks;
		},		

		add_products : function(e) 
		{					
			e.preventDefault();					
			Universam_Mail_Editor.current_column_cell = $(this);		
			if ( jQuery('#usam_popup_iframe').contents().find('.popup_content').attr('id') != 'selection_products' )
			{					
				var post_data = {
						nonce               : Universam_Mail_Editor.get_selection_products_mail_editor_iframe_nonce,
						action              : 'get_selection_products_mail_editor_iframe',						
					};		
				var ajax_callback = function(response) 
				{					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}	
					Universam_Mail_Editor.add_iframe();	
					jQuery('#usam_popup_iframe').contents().find('body').html(response.obj);							
				}
				Universam_Mail_Editor.send_ajax( post_data, ajax_callback );
			}
			else
			{				
				Universam_Mail_Editor.add_iframe();					
			}
		},
		
		install_draggable : function() 
		{				
			$( ".usam_widget_icon_droppable" ).draggable({    
				revert: true, 
				revertDuration: 600,  
				zIndex: 100,
			});				
			$(".block_placeholder").droppable({
				accept: '.usam_widget_icon_droppable',
				activate:Universam_Mail_Editor.place_unit_block_create,	
				deactivate:Universam_Mail_Editor.place_unit_block_stop,
				over: function(event, ui) 
				{					
					$(this).addClass('hover');						
				},
				out: function(event, ui) 
				{						
					$('.block_placeholder').removeClass('hover');		
				},    
				drop: function( event, ui) 
				{ 						
					var element = ui.draggable.closest('.usam_widget').attr('id');				
					Universam_Mail_Editor.block = $(this);						
					switch ( element ) 
					{
						case 'posts':																			
							Universam_Mail_Editor.display_iframe( element );
						break;					
						case 'column':
						case 'column_product':
							var post_data = {
									nonce               : Universam_Mail_Editor.insert_blok_mail_editor_iframe_nonce,
									action              : 'insert_blok_mail_editor_iframe',										
									column              : ui.draggable.data('column'),		
									line                : ui.draggable.data('line'),											
									type                : element,												
								};								
							Universam_Mail_Editor.insert_block( post_data );	
						break;						
						case 'fotos':									
							var images_file_frame = Universam_Mail_Editor.images_file_frame;	
							// If the media frame already exists, reopen it.
							if ( images_file_frame ) 
							{	// Set the post ID to the term being edited and open
								images_file_frame.open();
								return;
							} 									
							images_file_frame = wp.media.frames.images_file_frame = wp.media( {
								title    : Universam_Mail_Editor.text_media_upload,						
								library  : { type: 'image' },
								multiple : false
							} );
							// Pre-select selected attachment
							wp.media.frames.images_file_frame.on( 'open', function() {
								var selection = wp.media.frames.images_file_frame.state().get( 'selection' );
							/*	var selected_id = USAM_Thumbnail.attachment_id;
								if ( selected_id > 0 ) 
								{
									attachment = wp.media.attachment( selected_id );
									attachment.fetch();
									selection.add( attachment ? [ attachment ] : [] );
								}*/
							} );
							
							images_file_frame.on( 'select', function() 
							{				
								attachment = images_file_frame.state().get( 'selection' ).first().toJSON();		
								var post_data = {
									nonce               : Universam_Mail_Editor.insert_blok_mail_editor_iframe_nonce,
									action              : 'insert_blok_mail_editor_iframe',																
									type                : 'image',
									imgurl              : attachment.url,	
									width               : attachment.width,
									height              : attachment.height,											
								};		
								Universam_Mail_Editor.insert_block( post_data );
							} );
							images_file_frame.open();
							
						break;						
						case 'content':		
						case 'button':	
						case 'indentation':	
						case 'divider':
							var post_data = {
									nonce               : Universam_Mail_Editor.insert_blok_mail_editor_iframe_nonce,
									action              : 'insert_blok_mail_editor_iframe',															
									type                : element,													
								};								
							Universam_Mail_Editor.insert_block( post_data );	
						break;						
					}
					$('.block_placeholder').removeClass('hover');											
				}
			});									
        },
		
		// Вставить блок в письмо
		insert_block : function( post_data ) 
		{ 
			Universam_Mail_Editor.close_iframe();
			var ajax_callback = function(response) 
			{						
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}						
				Universam_Mail_Editor.block.before( response.obj );									
				Universam_Mail_Editor.install_draggable();	
			}
			Universam_Mail_Editor.send_ajax( post_data, ajax_callback );
		},		
			
		delete_block_text : function() 
		{				
			var text_block = $(this).parents('.usam_text');	
			var width = text_block.width();
			var height = text_block.height();
			text_block.find('.usam_tools').remove();	
			text_block.find('.usam_editable').remove();	
			text_block.html('<div class="text_placeholder empty">'+Universam_Mail_Editor.text_insert_block_here+'</div>');	
			text_block.find('.text_placeholder').css({'position':"relative",'width':width,'height':height,'float':'right'});			
        },
		
		delete_block : function() 
		{					
			var block = $(this).closest('.usam_block');				
			block.next('.block_placeholder').remove();
			block.remove();					
        },
		
		background_block : function() 
		{						
			var block = $(this).closest('.usam_block');	
			block.find('.background_input').iris({
				hide: false,
				palettes: ['#125', '#459', '#78b', '#ab0', '#de3', '#f0f'],
				change: function(event, ui) {					
					block.find('.usam_background_open').css('background', ui.color.toString());
					block.css('background', ui.color.toString());					
				}
			});				
			block.find('.usam_background_controls').show();
			block.find('.background_input').iris('show');
        },
		
		remove_background : function() 
		{	
			var block = $(this).closest('.usam_block');	
			Universam_Mail_Editor.close_background( block );		
			block.css('background', 'transparent');
			block.find('.usam_background_open').css('background', 'transparent');
		},

		close_background_select : function( ) 
		{	
			var block = $(this).closest('.usam_block');	
			Universam_Mail_Editor.close_background( block );
		},		

		close_background : function( block ) 
		{				
			var picker = block.find('.iris-picker-inner');		
			if ( picker.length > 0 )
			{ 
				block.find('.usam_background_controls').hide();
				block.find('.background_input').iris('hide');
			}
		},			
		
		close_iframe : function() 
		{			
			$('#usam_popup_overlay').hide();
			$('#usam_popup').hide();			
		},
		
		// Событие конца перемещение блока в письмо
		place_unit_block_stop : function(event, ui) 
		{			
			$('.text_placeholder').removeClass('active');
			$('.block_placeholder').removeClass('active');
        },
			
		// Событие начала перемещение блока в письмо	
		place_unit_block_create : function(event, ui) 
		{
			$('.text_placeholder').addClass('active');
			$('.block_placeholder').addClass('active');							
        },
		
		// Добавить frame
		add_iframe : function()
		{				
			if ( $("#usam_popup").length == 0 )
			{
				var t='<div id="usam_popup_overlay"></div><div id="usam_popup"><div id="usam_popup_title"><h3></h3><button type="button" class="close">×</button></div><div id="usam_popup_content" class="clearfix"><iframe id="usam_popup_iframe" marginheight="0" marginwidth="0" frameborder="0"></iframe></div></div>';	
				$('body').append(t);									
			}	
			else
			{				
				$('#usam_popup_overlay').addClass('loading');
				$('#usam_popup_overlay').show();
				$('#usam_popup').show();						
			}
			var doc_w = jQuery(window).width();
			var doc_h = jQuery(window).height();				
			var top = (doc_h-jQuery('#usam_popup').height())/2 + $(window).scrollTop();
			var left = (doc_w-jQuery('#usam_popup').width())/2 + $(window).scrollLeft();	
			jQuery("#usam_popup").css({'top':top+"px",'left':left+"px"});
        },
		
		/**
		 * Изменить все письмо
		 */
		edit_mailtemplate : function(e)
		{		
			e.preventDefault();			
			Universam_Mail_Editor.save_mailtemplate();
			window.location.replace(location.href +'&edit_mailtemplate=1');			
        }, 
		
		// Изменить разделитель блоков в письме
		edited_divider : function(e)
		{		
			e.preventDefault();	
			Universam_Mail_Editor.current_cell = $(this).closest('.usam_block');
			Universam_Mail_Editor.display_iframe( 'dividers' );				
			$(this).addClass("hover");
			$(this).children('.usam_controls').show();		
        },	
		
		edited_button : function(e)
		{		
			e.preventDefault();	
			Universam_Mail_Editor.current_cell = $(this).closest('.usam_block');		
			Universam_Mail_Editor.display_iframe( 'button' );				
			$(this).addClass("hover");
			$(this).children('.usam_controls').show();		
        },
			
		/**
		 * При наведении на редактируемый блок
		 */
		hover_edited_block : function(e)
		{			
			var width = $(this).width();
			$(this).children('.usam_controls .size').html( width+'px' );
			$(this).addClass("hover");	
			$(this).children('.usam_controls').show();	
        },      
				
		// Удалить класс hover
		hoverleave_edited_block : function() 
		{ 				
			if ( !Universam_Mail_Editor.is_resizable )
			{
				$(this).removeClass("hover");
				$(this).children('.usam_controls').hide();	
				Universam_Mail_Editor.close_background( $(this) );			
			}
        },
		
		/**
		 * При наведении на редактируемый блок
		 */
		hover_edited_block_text : function() 
		{			
			if ( !$(this).hasClass("alone") )
				$(this).find('.usam_tools').show();		
        },
		
		hover_block_img : function() 
		{			
			$(this).find('.usam_tools').show();				
        },		
		
		hoverleave_block_img : function() 
		{
			$(this).find('.usam_tools').hide();				
        },
					
		hoverleave_edited_block_text : function() 
		{	
			$(this).find('.usam_tools').hide();		
        },	
		
		// Вывести iframe
		display_iframe : function( type ) 
		{					
			Universam_Mail_Editor.add_iframe();
			
			if ( $('#usam_popup_iframe').contents().find('#popup_'+type).length > 0 )
			{
				Universam_Mail_Editor_Iframe.content_load();
			}
			else
			{
				var post_data = {
						nonce               : Universam_Mail_Editor.get_form_mail_editor_iframe_nonce,
						action              : 'get_form_mail_editor_iframe',					
						type                : type,									
					};			
				var ajax_callback = function(response) 
				{					
					//alert(response.obj);
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}											
					$('#usam_popup_iframe').contents().find('body').html( response.obj.html );
					$('#usam_popup_title h3').html( response.obj.title );
				}
				Universam_Mail_Editor.send_ajax( post_data, ajax_callback );
			}			
		},
		
	});	
})(jQuery);	
Universam_Mail_Editor.init();