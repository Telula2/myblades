/**
* Объект и функции Universam_Mail_Editor_Iframe.  
* Следующие свойства Universam_Mail_Editor_Iframe были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
* - Механизм: одноразовый номер используется для проверки запроса для загрузки содержимого вкладок с помощью AJAX
*/ 
(function($)
{
	$.extend(Universam_Mail_Editor_Iframe, 
	{				
		iframe:'',
		init : function() 
		{			
			$(function()
			{							
				Universam_Mail_Editor_Iframe.iframe = jQuery('#usam_popup_iframe').contents();				
			
				var div_products = Universam_Mail_Editor_Iframe.iframe.find('#products');					
				var selection_products = Universam_Mail_Editor_Iframe.iframe.find('#popup_selection_products');	
				var dividers = Universam_Mail_Editor_Iframe.iframe.find('#popup_dividers');				
				Universam_Mail_Editor_Iframe.iframe.delegate('#post_type', 'change', Universam_Mail_Editor_Iframe.insert_record)					 
					  .delegate('#post_status', 'change', Universam_Mail_Editor_Iframe.insert_record)
					  .delegate('#articles-form', 'submit', Universam_Mail_Editor_Iframe.insert_post)	
					  .delegate('#search-submit', 'click', Universam_Mail_Editor_Iframe.insert_record);				
				selection_products
					.delegate('#articles-form', 'submit', Universam_Mail_Editor_Iframe.insert_product);
				dividers
					.delegate('.dividers a', 'click', Universam_Mail_Editor_Iframe.click_dividers)
					.delegate('#articles-form', 'submit', Universam_Mail_Editor_Iframe.save_dividers);
				Universam_Mail_Editor_Iframe.iframe.find('#popup_image_data')
					.delegate('#articles-form', 'submit', Universam_Mail_Editor_Iframe.save_image_data);	
				
				Universam_Mail_Editor_Iframe.iframe.find('#popup_button')
					.delegate('.usam_done_editing', 'click', Universam_Mail_Editor_Iframe.save_button_data);	
				
				Universam_Mail_Editor_Iframe.content_load();
				
				Universam_Mail_Editor_Iframe.iframe.find('.usam_color').wpColorPicker( );			
			});	
		},	
		
		// Вставить ссылку
		save_button_data : function() 
		{					
			var button = Universam_Mail_Editor.current_cell.find('.usam_editor_button');		
			
			var html = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_text').val( );
			var url = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_url').val( );			
			var font_color = Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_font_color').val( );	
			var background_color = Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_background_color').val( );	
			var font_family = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_font_family').val( );	
			var font_size = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_font_size').val( );	
			var font_weight = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_font_weight').val();
			var border_color = Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_border_color').val();	
			var border_width = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_border_width_input').val( );	
			var border_radius = Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_border_radius_input').val( );	
			var button_width = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_width_input').val( );	
			var line_height = Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_line_height_input').val( );				
			var margin = 'auto';
			switch ( Universam_Mail_Editor_Iframe.iframe.find('input:checked.usam_field_button_alignment').val() ) 
			{
				case 'left':							
					margin = '0 auto 0 0';
				break;	
				case 'right':							
					margin = '0 0 0 auto';
				break;
				case 'center':							
					margin = 'auto';
				break;
			}			
			button.attr('href', url);
			button.html(html);
			button.css('margin', margin);		
			button.css('color', font_color);
			button.css('font-family', font_family);
			button.css('font-size', font_size);
			button.css('font-weight', font_weight);
			button.css('border-color', border_color);			
			button.css('border-radius', border_radius+'px');
			button.css('border-width', border_width);			
			button.css('width', button_width);
			button.css('height', line_height);		
			button.css('background-color', background_color);
			
			Universam_Mail_Editor.close_iframe();		
			return false;			
		},
		
		content_load : function() 
		{ 
			if ( Universam_Mail_Editor_Iframe.iframe.find('#results').length )
			{
				$(document).ready
				{		
					Universam_Mail_Editor_Iframe.insert_record();
				}					
			}
			else if ( Universam_Mail_Editor_Iframe.iframe.find('#popup_image_data').length )
			{					
				var img = Universam_Mail_Editor.current_cell.find('.image_placeholder');	
			
				Universam_Mail_Editor_Iframe.iframe.find('.popup_content #url').val( img.data('url') );
				Universam_Mail_Editor_Iframe.iframe.find('.popup_content #alt').val( img.data('alt') );				
			} 	
			else if ( Universam_Mail_Editor_Iframe.iframe.find('#popup_button').length )
			{					
				var button = Universam_Mail_Editor.current_cell.find('.usam_editor_button');	
			
				Universam_Mail_Editor_Iframe.iframe.find('#button_name_link').val( button.html() );
				Universam_Mail_Editor_Iframe.iframe.find('#button_link').val( button.attr('href') );
				var margins = button.css('margin').split(' ');
				var background_color = button.css('background-color');
				var font_family_array = button.css('font-family').split(',');
				var font_family = $.trim(font_family_array[0]);
				var alignment = 'center';
				
				if ( margins.length > 2 )
				{					
					for(var i = 0; i < margins.length; i++) 
					{
						margin = parseInt(margins[i]);
						if ( i == 0 && margin == 0 )
						{
							alignment = 'right';							
						}
						else if ( i == 3 && margin == 0 )
						{
							alignment = 'left';
						}				
					}
				}				
				Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_alignment[value='+alignment+']').attr('checked', 'checked');				
				Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_width_input').val( parseInt(button.css('width')) );
				Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_line_height_input').val( parseInt(button.css('height')) );
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_background_color').val( background_color );
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_font_color').val( button.css('color') );
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_font_family').val( font_family );
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_font_size').val( button.css('font-size'));
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_font_weight').val( button.css('font-weight'));
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_border_color').val( button.css('border-color'));
				Universam_Mail_Editor_Iframe.iframe.find('.usam_field_button_border_width_input').val( parseInt(button.css('border-width')) );			
				Universam_Mail_Editor_Iframe.iframe.find('#usam_field_button_border_radius_input').val( parseInt(button.css('border-radius')) );
			}			
		},
				
		getBackgroundColor : function( $dom ) 
		{
			var bgColor = "";
			while ($dom[0].tagName.toLowerCase() != "html") 
			{
				bgColor = $dom.css("background-color");
				if (bgColor != "rgba(0, 0, 0, 0)" && bgColor != "transparent") {
					break;
				}
				$dom = $dom.parent();
			}
			return bgColor;
		},
		
		click_dividers : function(e) 
		{
			e.preventDefault();			
			
			var iframe = jQuery('#usam_popup_iframe').contents();
			iframe.find('#divider_src').val( $(this).find('img').attr('src') ); 
			iframe.find('#divider_width').val( $(this).find('img').attr('width') );
			iframe.find('#divider_height').val( $(this).find('img').attr('height') );			
		},
		
		// Вставить ссылку
		save_image_data : function() 
		{
			var iframe = jQuery('#usam_popup_iframe').contents();		
			var url = iframe.find('#url').val( );
			var alt = iframe.find('#alt').val( );
			var img = Universam_Mail_Editor.current_cell.find('.image_placeholder');	
	
			img.attr('data-alt', alt);
			img.attr('data-url', url);	
			
			Universam_Mail_Editor.close_iframe();	
			return false;			
		},
		
		save_dividers : function() 
		{
			var iframe = jQuery('#usam_popup_iframe').contents(),		
				src = iframe.find('#divider_src').val( ),
				width = iframe.find('#divider_width').val( ),
				height = iframe.find('#divider_height').val( );
			
			var divider = { src: src, width: width, height: height };
						
			var post_data = {
					nonce            : Universam_Mail_Editor.set_divider_mail_editor_nonce,
					action           : 'set_divider_mail_editor',	
					divider          : divider,							
					mail_id          : Universam_Mail_Editor.mail_id,											
				};					
			var ajax_callback = function(response) 
			{
				if (! response.is_successful) 				
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$('.usam_divider img').each(function(i,elem) 
				{					
					$(this).attr('src', src );
					$(this).attr('width', width );
					$(this).attr('height', height );				
				});						
				Universam_Mail_Editor.close_iframe();				
			}
			$.usam_post(post_data, ajax_callback);	
			return false;			
		},
		
		// Вставить товар в сетку товаров
		insert_product : function() 
		{		
			var iframe = jQuery('#usam_popup_iframe').contents();	
			Universam_Mail_Editor.close_iframe();				
			var post_data = {
					nonce            : Universam_Mail_Editor.insert_products_blok_mail_editor_iframe_nonce,
					action           : 'insert_products_blok_mail_editor_iframe',	
					products         : iframe.find("#results input:checkbox:checked").serializeArray(),
					mail_id          : Universam_Mail_Editor.mail_id,									
				};		
			var ajax_callback = function(response) 
			{	
				iframe.find("#results input:checkbox:checked").removeAttr("checked");
				if (! response.is_successful) 				
				{
					alert(response.error.messages.join("\n"));
					return;
				}					
				Universam_Mail_Editor.current_column_cell.html( response.obj );								
			}			
			$.usam_post(post_data, ajax_callback);			
			return false;					
		},		
		
		insert_post : function() 
		{							
			var iframe = jQuery('#usam_popup_iframe').contents();
			var post_data = {
				nonce            : Universam_Mail_Editor.insert_post_blok_mail_editor_iframe_nonce,
				action           : 'insert_post_blok_mail_editor_iframe',
				type_block       : 'post',			
				post_type        : iframe.find('#post_type').val(),
				post_status      : iframe.find('#post_status').val(),			
				post             : iframe.find("#results input:checkbox:checked").serializeArray(),
				mail_id          : Universam_Mail_Editor.mail_id,	
			};			
			Universam_Mail_Editor.insert_block( post_data );			
		},
		
		insert_record : function() 
		{				
			var iframe = jQuery('#usam_popup_iframe').contents();					
			var post_data = {
				'action'      : 'get_post_type',		
				'search'      : iframe.find('#search').val(),
				'post_type'   : iframe.find('#post_type').val(),
				'post_status' : iframe.find('#post_status').val(),					
				'nonce'       : Universam_Mail_Editor_Iframe.get_post_type_nonce
			};			
			var ajax_callback = function(response) 
			{				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));				
					return false;
				}				
				iframe.find('#results').html(response.obj);
			};							
			$.usam_post(post_data, ajax_callback);		
			return false;			
		},	
		
	});	
})(jQuery);	
Universam_Mail_Editor_Iframe.init();