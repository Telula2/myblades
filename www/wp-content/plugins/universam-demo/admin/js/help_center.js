(function($)
{
	$.extend(Universam_Help_Center, 
	{
		number : 5,
		paged  : 1,
		total  : false,
		/**
		 * Обязательные события для Universam_Help_Center
		 * @since 3.8.8
		 */
		init : function() 
		{				
			$(function()
			{	
				Universam_Help_Center.wrapper = $('#usam-help_center');													
				Universam_Help_Center.wrapper.delegate('#send-email-submit', 'click', Universam_Help_Center.event_send_support_message)
											 .delegate('.support-messages', 'click', Universam_Help_Center.event_get_support_messages)
											 .delegate('.usam-help_center_handle', 'click', Universam_Help_Center.event_click_item_menu);
											 
				$('.contextual-help-tabs-wrap').scroll(function ()
				{   
					if( $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 1 )
					{ //Прокручено до низа элемента
						Universam_Help_Center.get_support_messages(); 
					}				
				});					
			});
		},	
		
		event_click_item_menu : function( e ) 
		{
			var $container = $( e.delegateTarget );					
			var $slideout = $container.find( '.usam-tab-video-slideout' );
			if ( $slideout.is( ':hidden' ) ) 
			{					
				Universam_Help_Center.openVideoSlideout( $container );
			}
			else 
			{
				Universam_Help_Center.closeVideoSlideout();
			}
		},
		
		openVideoSlideout : function( $container ) 
		{
			$container.find( '.toggle__arrow' ).removeClass( 'dashicons-arrow-down' ).addClass( 'dashicons-arrow-up' );
			$container.find( '.usam-help_center_handle' ).attr( 'aria-expanded', 'true' );
			$container.find( '.usam-tab-video-slideout' ).css( 'display', 'flex' );

			var $activeTabLink = $container.find('.usam-help-center-item.active > a');

			$( '#wpcontent' ).addClass( 'usam-help-center-open' );

			if ( $activeTabLink.length > 0 ) {
				var activeTab = $activeTabLink.attr( 'aria-controls' );
				Universam_Help_Center.activateVideo( $( '#' + activeTab ) );

				$container.on( 'click', '.usam-help-center-item > a', function( e ) {
					var $link = $( this );
					var target = $link.attr( 'aria-controls' );

					$container.find( '.usam-help-center-item' ).removeClass( 'active' );
					$link.parent().addClass( 'active' );

					Universam_Help_Center.openHelpCenterTab( $container, $( '#' + target ) );

					e.preventDefault();
				} );
			}
			else {
				Universam_Help_Center.activateVideo( $container );
			}			
		},	
		
		openHelpCenterTab : function( $container, $tab ) 
		{
			$container.find('.help-tabs div').removeClass('active');
			$tab.addClass('active');

			Universam_Help_Center.stopVideos();
			Universam_Help_Center.activateVideo( $tab );
		},
		
		activateVideo : function( $tab ) 
		{		
			var $data = $tab.find( '.usam-tab-video__data' );
			if ( $data.length === 0 ) {
				return;
			}
			$data.append( '<iframe width="560" height="315" src="' + $data.data( 'url' ) + '" frameborder="0" allowfullscreen></iframe>' );
		},
		
		stopVideos : function() 
		{
			$( '#wpbody-content' ).find( '.usam-tab-video__data' ).children().remove();
		},				
		
		closeVideoSlideout : function() 
		{
			var $container = $( '#wpbody-content' ).find( '.usam-help_center' );
			$container.find( '.usam-tab-video-slideout' ).css( 'display', '' );

			Universam_Help_Center.stopVideos();

			$container.find( '.toggle__arrow' ).removeClass( 'dashicons-arrow-up' ).addClass( 'dashicons-arrow-down' );
			$container.find( '.usam-help_center_handle' ).attr( 'aria-expanded', 'false' );

			$( '#wpcontent' ).removeClass( 'usam-help-center-open' );
		},
		
		event_send_support_message : function() 
		{			
			var support     = jQuery(this).closest('.contextual-help-tabs-wrap-contact-support'),	
				message = support.find('#support_response').val();	
			if ( message != '' )
			{ 	
				var spinner = $(this).siblings('#ajax-loading'),
					post_data   = {
						action        : 'send_support_message',	
						nonce         : Universam_Help_Center.send_support_message_nonce,				
						'subject'     : support.find('#support_subject').val(),						
						'message'     : message,						
					},
					ajax_callback = function(response)
					{					
						if (! response.is_successful) 
						{
							alert(response.error.messages.join("\n"));
							return;
						}			    	
						spinner.toggleClass('ajax-loading-active');	
						support.find('#support_response').val('');
					};					
				spinner.toggleClass('ajax-loading-active');
				$.usam_post(post_data, ajax_callback);
			}
			return false;
		},
		
		// Получить сообщения
		event_get_support_messages : function() 
		{					
			 Universam_Help_Center.get_support_messages();
		},		
		
		get_support_messages : function() 
		{					
			if ( !Universam_Help_Center.total )
			{   
				Universam_Help_Center.total = true;
				var spinner = $('#support_messages_body').siblings('#ajax-loading'),
					post_data   = {
						action        : 'get_support_messages',	
						nonce         : Universam_Help_Center.get_support_messages_nonce,	
						number        : Universam_Help_Center.number,
						paged         : Universam_Help_Center.paged,					
					},
					ajax_callback = function(response)
					{							
						if (! response.is_successful) 
						{
							alert(response.error.messages.join("\n"));
							return;
						}			    	
						spinner.toggleClass('ajax-loading-active');					
						var row = '';
						var $class = '';	
						var $i = 0;							
						jQuery.each(response.obj, function(index, value)
						{
							$i++;					
							if ( value.outbox == 0 )
								$class = 'inbox'; // входящие
							else
								$class = 'outbox';													
						
							row = "<div class = 'message'><div class = 'message_content "+$class+"'><div class = 'subject'>"+value.subject+"</div><div class = 'message_text'>"+value.message+"</div><div class = 'date'>"+value.date+"</div></div></div>";
							$('#support_messages_body').append(row);
						});	
						if ( Universam_Help_Center.number < $i )
							Universam_Help_Center.total = true;		
						else
							Universam_Help_Center.total = false;	
						Universam_Help_Center.paged++;											
					};					
				spinner.toggleClass('ajax-loading-active');
				$.usam_post(post_data, ajax_callback);								
				return false;
			}
		},
	});	
})(jQuery);	
Universam_Help_Center.init();