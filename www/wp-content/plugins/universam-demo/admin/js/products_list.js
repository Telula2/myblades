jQuery(document).ready(function($)
{ 
	if( Universam_Admin.dragndrop && typenow == "usam-product" && adminpage == "edit-php" ) 
	{    // это делает таблицу списка продуктов сортируемым				
		jQuery('table.widefat:not(.tags)').sortable({
			update: function(event, ui) 
			{
				var category_id = jQuery('select#usam-category option:selected').val(),
					product_order = jQuery('table.widefat').sortable( 'toArray' ),
					post_data = {
						action : 'save_product_order',
						'category_id' : category_id,
						'post[]' : product_order,
						nonce : Universam_Admin.save_product_order_nonce
					};
				jQuery.usam_post(post_data, function(response) {
					if (! response.is_successful)
						alert(response.error.messages.join("\n"));
				});
			},
			items: 'tbody tr',
			axis: 'y',
			containment: 'table.widefat tbody',
			placeholder: 'product-placeholder',
			cursor: 'move',
			cancel: 'tr.inline-edit-usam-product'
        });			
	}			 
	jQuery(".product_rating").rating( {'selected': usam_save_product_rating } );	
	
	//Колонка "Избранный" в таблице продуктов админского интерфейса.	
	jQuery('.usam_featured_product_toggle').click(function(e)
	{			
		e.preventDefault();
		var t = $(this).find('span'),
			current_status = 0;
		if ( t.hasClass("not-featured") ) 
		{
			current_status = 1;
		}				
		var post_data = {
				nonce              : Universam_Admin.update_featured_products,	
				action             : 'update_featured_products',				
				product_id		   : $(this).data('product_id'),	
				status	           : current_status,						
			},
			spinner = $(this).siblings('#ajax-loading');					
		spinner.addClass('ajax-loading-active');
		var ajax_callback = function( response ) 
		{						
			if (! response.is_successful) 
			{
				alert(response.error.messages.join("\n"));
				return;
			}	
			if (! response.obj.result ) 
			{
				alert(response.obj.title);
				return;
			}
			t.html(response.obj.title);
			if ( current_status ) 
			{
				t.removeClass('not-featured');
			}
			else
			{
				t.addClass('not-featured');
			}
			spinner.removeClass('ajax-loading-active');				
		};		
		$.usam_post(post_data, ajax_callback);		
	});
	
	jQuery('#bulk-action-selector-top').change(function() 
	{				
		switch ( $(this).val() ) 
		{
			case 'category' :			
			case 'brand' :			
			case 'category_sale' :				
				$(".chzn-select").chosen({ width: "300px" });	
				var product_title = '';		
				var row = '';						
				$('.wp-list-table tbody .check-column input:checkbox:checked').each(function()
				{
					product_title = $(this).closest('tr').find('.row-title').html();
					row = row+ "<li>"+product_title+"</li>";
				});
				if ( row != '' )
				{
					$('#bulk_actions_terms .colum2 .products').html('<ul></ul>');
					$('#bulk_actions_terms .colum2 .products').append( row );		
				}
				$('#bulk_actions_terms').modal();									
			break;
			case 'product_attribute' :				
				$(".chzn-select").chosen({ width: "300px" });	
				var product_title = '';		
				var row = '';						
				$('.wp-list-table tbody .check-column input:checkbox:checked').each(function()
				{
					product_title = $(this).closest('tr').find('.row-title').html();
					row = row+ "<li>"+product_title+"</li>";
				});
				if ( row != '' )
				{
					$('#bulk_actions_product_attribute .colum2 .products').html('<ul></ul>');
					$('#bulk_actions_product_attribute .colum2 .products').append( row );		
				}
				$('#bulk_actions_product_attribute').modal();									
			break;
		}		
	});
	
	jQuery('#bulk_actions_product_attribute #modal_action').click(function() 
	{					
		var spinner = $('#all-ajax-loading');
		var products = [];
		var product_attribute = [];
		spinner.toggleClass('ajax-loading-active');	
		var i = 0;	
		$("#bulk_actions_product_attribute .product_attributes .change_made").each( function()
		{				
			product_attribute[i] = { "value" : $(this).val(), "id" : $(this).data('id') };
			$(this).removeClass("change_made");	
			i++;
		});			
		i = 0;	
		$('.wp-list-table tbody .check-column input:checkbox:checked').each(function()
		{
			products[i] = $(this).val();
		});		
		var	spinner = $('#ajax-loading'),
			post_data   = {
				action         : 'bulk_actions_product_attribute',
				'attributes'   : product_attribute,		
				'products'     : products,					
				nonce          : Universam_Admin.bulk_actions_product_attribute
			},
			ajax_callback = function(response)
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}						
				spinner.toggleClass('ajax-loading-active');					
				window.location.replace(location.href +'&update_product='+i);		
			};	
		spinner.toggleClass('ajax-loading-active');					
		$.usam_post(post_data, ajax_callback);			
		return false;	
	});			
	
	jQuery('#bulk_actions_terms #modal_action').click(function() 
	{	
		var products = [];	
		var i = 0;
		$('.wp-list-table tbody .check-column input:checkbox:checked').each(function(){
			products[i] = $(this).val();
			i++;
		});				
		var post_data = {
				nonce              : Universam_Admin.bulk_actions_terms,	
				action             : 'bulk_actions_terms',				
				operation		   : $('select#operation').val(),	
				category     	   : $('select#category').val(),	
				brands		       : $('select#brands').val(),	
				category_sale	   : $('select#category_sale').val(),	
				products           : products,						
			},
			spinner = $('#bulk_actions_terms').find('#ajax-loading');					
		spinner.addClass('ajax-loading-active');	
	//	$('#bulk_actions_terms .modal-body').hide();		
		var ajax_callback = function( response ) 
		{				
			$('#bulk_actions_terms').modal('hide');
			window.location.replace(location.href +'&updated='+i);				
			if (! response.is_successful) {
				alert(response.error.messages.join("\n"));
				return;
			}		
			spinner.removeClass('ajax-loading-active');				
		};		
		$.usam_post(post_data, ajax_callback);
	});		
	
	jQuery('td.hidden_alerts img').each(function(){
		var t = jQuery(this);
		t.appendTo(t.parents('tr').find('td.column-title strong'));
	});	
	
	setTimeout(bulkedit_edit_tags_hack, 1000);	
});


function usam_save_product_rating( rating, t) 
{ 
	var $product_rating = t.parent('.product_rating');
	var product_id = $product_rating.data('product_id');
	var post_data = {
		'action'     : 'product_rating',	
		 nonce       : Universam_Admin.product_rating_nonce,	
		'product_id' : product_id,	
		'rating'     : rating,
	},
	response_handler = function(response){ 
		jQuery('#vote_total_'+product_id).text( response.obj.rating_count );		
		var i = 0;
		$product_rating.find(".star").each( function()
		{			
			if ( i < response.obj.rating )
				jQuery(this).addClass('selected');
			else
				jQuery(this).removeClass('selected');
			i++;
		});		
	};
	jQuery.usam_post(post_data, response_handler);
}

// inline-edit-post.dev.js prepend tag edit textarea into the last fieldset. We need to undo that
function bulkedit_edit_tags_hack() {
	jQuery('<fieldset class="inline-edit-col-right"><div class="inline-edit-col"></div></fieldset>').insertBefore('#bulk-edit .usam-cols:first').find('.inline-edit-col').append(jQuery('#bulk-edit .inline-edit-tags'));
}