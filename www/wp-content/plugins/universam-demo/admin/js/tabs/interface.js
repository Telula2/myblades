/**
* Объект и функции USAM_Page_interface.  
*/
(function($)
{
	$.extend(USAM_Page_interface, 
	{		
		images_file_frame : '',
		/**
		 * Обязательные события
		 */
		init : function() 
		{					
			$(function()
			{			
				USAM_Page_interface.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_interface).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
			});
		},			
	});
	
		/**
	 * Вкладка "слайдер"	
	 */
	USAM_Page_interface.slider = 
	{		
		event_init : function() 
		{			
			USAM_Page_interface.wrapper
				.delegate('#add-slide', 'click', USAM_Page_interface.slider.event_click_add_slide)
				.delegate('#add_product', 'click', USAM_Page_interface.slider.event_click_add_product)						
				.delegate('.theme-actions a', 'click', USAM_Page_interface.slider.event_theme_actions);		
			$('input.usam_color').wpColorPicker( );	
		},		
		
		event_theme_actions : function( event ) 
		{
			event.preventDefault();
			
			$('.theme').removeClass('active');
			
			var template = $(this).parents('.theme').addClass('active').find('.theme-name').html();	
			$('#slider_template').val( template );
			
		},
		
		event_click_add_product : function() 
		{						
			var product_id = $('#product_box #select_product_1').val();	
			if ( product_id == '' )
				return false; 
		
			if ( $( '.table_products tr[data-product_id='+product_id+']' ).length>0 )
				return;				
			
			var table_products = $('.table_products');
			table_products.find('.items_empty').remove();	
			
			var sku = $('#product_box #select_product_1').data('product_sku');
			var product_title = $('#product_box #search_product_1').val();			
			var i = table_products.find('tbody tr').length+1;
			var del = '<a class="button_delete"></a>';						
		
			var html = '';
			html = "<td>"+i+"<input type='hidden' name='new_products[][object_id]' value='"+product_id+"'/></td>";
			html = html+"<td>"+product_title+"</td>";
			html = html+"<td>"+sku+"</td>";			
			html = html+"<td>"+del+"</td>";
			table_products.append('<tr data-product_id='+product_id+'>'+html+'</tr>');				
		},
							
		event_click_add_slide : function( event ) 
		{		
			event.preventDefault();
			button = $( this );
			USAM_Page_interface.ttID = $( this ).data( 'tt-id' );
			USAM_Page_interface.attachment_id = $( this ).data( 'attachment-id' );			

			// If the media frame already exists, reopen it.
			if ( USAM_Page_interface.images_file_frame ) 
			{	// Set the post ID to the term being edited and open
				images_file_frame = USAM_Page_interface.images_file_frame;				
				images_file_frame.open();
				return;
			} 
			else 
			{	// Set the wp.media post id so the uploader grabs the term ID being edited
				USAM_Page_interface.ttID = $( this ).data( 'tt-id' );
			}	
			// Create the media frame.
			images_file_frame = wp.media.frames.images_file_frame = wp.media( {
				title    : button.data( 'title' ),
				button   : { text : button.data( 'button_text' ) },
				library  : { type: 'image' },
				multiple : false
			} );
			// Pre-select selected attachment
			wp.media.frames.images_file_frame.on( 'open', function() {
				var selection = wp.media.frames.images_file_frame.state().get( 'selection' );
				var selected_id = USAM_Page_interface.attachment_id;
				if ( selected_id > 0 ) 
				{
					attachment = wp.media.attachment( selected_id );
					attachment.fetch();
					selection.add( attachment ? [ attachment ] : [] );
				}
			} );
			
			images_file_frame.on( 'select', function() 
			{				
				attachment = images_file_frame.state().get( 'selection' ).first().toJSON();				
				var attachment_id = attachment.id;
				
				var row = $("#slides_table tfoot #new_slide").clone().attr( 'id', 'slide_'+USAM_Page_interface.id );	
				row.find('#slide_new_image_id').val( attachment.id ).attr("disabled", false);
				row.find('img').attr( 'src', attachment.url );
				
				$('#slides_table #items_empty').remove();
				
				$("#slides_table tbody").append(row);
						
				$('.usam_edit_data').hide();
				$('.usam_display_data').show();	
			});
			images_file_frame.open();
		},
		
		delete_slide : function( ) 
		{						
			var slide_id = $(this).data('id');			
			var row = $(this).closest('tr');
			var post_data = {
					nonce      : USAM_Page_interface.delete_slide_nonce,
					action     : 'delete_slide',							
					slide_id   : slide_id,								
				},
				spinner = $(this).siblings('#ajax-loading'),
				t = $(this);		
			spinner.addClass('ajax-loading-active');
			var ajax_callback = function(response)
			{						
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}	
				row.remove();
			};
			$.usam_post(post_data, ajax_callback);				
		},	
	};
	$(USAM_Page_interface).bind('usam_tab_loaded_slider', USAM_Page_interface.slider.event_init);		
	
})(jQuery);	
USAM_Page_interface.init();