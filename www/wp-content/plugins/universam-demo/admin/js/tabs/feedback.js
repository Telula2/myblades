/**
* Объект и функции USAM_Page_feedback.  
* Следующие свойства USAM_Page_feedback были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/
(function($)
{
	$.extend(USAM_Page_feedback, 
	{	
		init : function() 
		{			
			$(function()
			{				
				USAM_Page_feedback.wrapper = $('#tab_'+Universam_Tabs.current_tab);
				$(USAM_Page_feedback).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);
				
				$('.page-feedback').delegate('.select_status_record', 'change' , USAM_Page_feedback.change_status);	
			});
		},
		
		change_status : function()
		{ 
			var id             = jQuery(this).data('id'),		
				status_record = jQuery(this).val(),
				spinner = $(this).siblings('#ajax-loading'),		
				post_data   = {
					action        : 'change_status_feedback',
					'tab'         : Universam_Tabs.current_tab,
					'status'      : status_record,
					'id'          : id,				
					 nonce        : USAM_Page_feedback.change_status_feedback_nonce
				},				
				ajax_callback = function(response)
				{	
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}			    	
					spinner.toggleClass('ajax-loading-active');	
				};					
			spinner.toggleClass('ajax-loading-active');
			$.usam_post(post_data, ajax_callback);			
			return false;
		},

		open_quick_response : function(e) 
		{  
			e.preventDefault();	
			var email = $(this).data('email');		
			var subject = $(this).data('subject');	
			
			$('#quick_response #to_email').val( email );
			$('#quick_response #subject').val( subject );
			$('#quick_response').modal();			
		},		
	});	
	
	USAM_Page_feedback.price = 
	{		
		event_init : function() 
		{	
			USAM_Page_feedback.wrapper.
				delegate('#open_quick_response', 'click', USAM_Page_feedback.open_quick_response); 
		},		
	};
	$(USAM_Page_feedback).bind('usam_tab_loaded_price', USAM_Page_feedback.price.event_init);	
	
	// Вкладка сообщения покупателей
	USAM_Page_feedback.questions = 
	{		
		event_init : function() 
		{	
			USAM_Page_feedback.wrapper.
				delegate('#open_quick_response', 'click', USAM_Page_feedback.open_quick_response); 
		},		
	};
	$(USAM_Page_feedback).bind('usam_tab_loaded_questions', USAM_Page_feedback.questions.event_init);

	// Вкладка отзывы покупателей
	USAM_Page_feedback.reviews = 
	{		
		event_init : function() 
		{				
			response_handler = function(response) 
			{
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					return;
				}

				t.closest('.usam_product_download_row').fadeOut('fast', function() {
					$('div.select_product_file p:even').removeClass('alt');
					$('div.select_product_file p:odd').addClass('alt');
					$(this).remove();
				});
			};			
		},		
		
		edit : function() 
		{
			
		}
	};
	$(USAM_Page_feedback).bind('usam_tab_loaded_reviews', USAM_Page_feedback.reviews.event_init);	
	
	// Вкладка письма
	USAM_Page_feedback.email = 
	{		
		mailbox_id: 0,
		folder_id: 0,
		id_email_input: 0,
		event_init : function() 
		{	
			$('.wp-list-table tbody').delegate('td', 'click' , USAM_Page_feedback.email.change_message)	
									 .delegate('.importance', 'click' , USAM_Page_feedback.email.change_importance);	
									 
			$('.list_folders')
				.delegate('#add_folder', 'click' , USAM_Page_feedback.email.display_add_folder)	
				.delegate('#add_folder', 'click' , USAM_Page_feedback.email.display_add_folder)	
				.delegate('#read_folder', 'click' , USAM_Page_feedback.email.read_folder)	
				.delegate('#open_clear_folder', 'click' , USAM_Page_feedback.email.open_clear_folder)				
				.delegate('#remove_folder', 'click' , USAM_Page_feedback.email.remove_folder);	
				
			$('#operation_confirm').delegate('#clear_folder', 'click' , USAM_Page_feedback.email.clear_folder);	
			
			$('#add_folder_window').delegate('#save_action', 'click' , USAM_Page_feedback.email.add_folder);			
			$('.tab_email')
				.delegate('#set_folder', 'change' , USAM_Page_feedback.email.change_email_folder)
				.delegate('.usam-delete-link', 'click' , USAM_Page_feedback.email.delete_email);	
				
			$('.mailing').delegate('#open_select_email', 'click' , USAM_Page_feedback.email.open_select_email);			
		
			var iframe = $('.email_body iframe', parent.document.body);		
			   iframe.height($(document.body).height());
			   
			if ( $(".wp-list-table tbody tr.message_current").length )
			{			
				$(document).ready(function()
				{
					USAM_Page_feedback.email.load_message( $(".wp-list-table tbody tr.message_current") ); 
				});
			}	
			$(document).ready(function() 
			{      // Вешаем слушатель события нажатие кнопок мыши для всего документа:  
				document.oncontextmenu = function( event )
				{			
					if ( $(event.target).hasClass("folder") || $(event.target).parent().hasClass('folder') )		
					{
						$('.menu_content').removeClass('select_menu');	
						$(event.target).closest(".folder").find('.menu_content').toggleClass('select_menu');	
						return false; 		
					}
				};	
			}); 
		},
		
		open_clear_folder : function(e) 
		{ 
			e.preventDefault();		
			var t = $(this).closest('li.folder');
			USAM_Page_feedback.email.mailbox_id = t.closest('.folders').data('mailbox_id');	
			USAM_Page_feedback.email.folder_id = t.data('folder');
			$('#operation_confirm').modal();			
		},	

		read_folder : function()
		{ 
			var t = $(this);		
			var	post_data   = {
					action        : 'read_email_folder',					
					'folder_id'   : t.closest('li.folder').data('folder'),		
					'mailbox_id'  : t.closest('.folders').data('mailbox_id'),							
					nonce         : USAM_Page_feedback.read_email_folder_nonce
				},
				ajax_callback = function(response)
				{		
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}	
					var $folder = t.closest(".folder");
					$folder.find('#new_email_numbers').remove();
					if ( $folder.hasClass('folder_current') )
					{						
						$('.usam_list_table_wrapper .message_unread').removeClass('message_unread');	
					}
				};					
			$.usam_post(post_data, ajax_callback);
		},		
		
		clear_folder : function()
		{ 								
			var	post_data   = {
					action        : 'clear_email_folder',					
					'folder_id'   : USAM_Page_feedback.email.folder_id,						
					'mailbox_id'  : USAM_Page_feedback.email.mailbox_id,		
					nonce         : USAM_Page_feedback.clear_email_folder_nonce
				},
				ajax_callback = function(response)
				{						
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}						
					var $folder = $('.folders [data-folder='+USAM_Page_feedback.email.folder_id+']').closest(".folder");
					$folder.find('#new_email_numbers').remove();
					if ( $folder.hasClass('folder_current') )
					{						
						$('.usam_list_table_wrapper tbody tr').remove();	
					}
					location.reload();
				};					
			$.usam_post(post_data, ajax_callback);
		},	
		
		remove_folder : function()
		{ 
			var t = $(this).closest('li.folder');		
			var	post_data   = {
					action        : 'remove_email_folder',					
					'folder_id'   : t.data('folder'),						
					nonce        : USAM_Page_feedback.remove_email_folder_nonce
				},
				ajax_callback = function(response)
				{						
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}	
					var $folder = t.closest(".folder");									
					if ( $folder.hasClass('folder_current') )
					{						
						$('.usam_list_table_wrapper tbody tr').remove();	
					}
					$folder.remove();	
					location.reload();
				};					
			$.usam_post(post_data, ajax_callback);
		},
				
		display_add_folder : function(e)
		{ 
			e.preventDefault();	
			USAM_Page_feedback.email.mailbox_id = $(this).closest('.folders').data('mailbox_id');		
			$('#add_folder_window #folder_name').val('');			
			$('#add_folder_window').modal();		
		},
		
		add_folder : function()
		{			
			var name = $('#add_folder_window #folder_name').val();	
			var	post_data   = {
					action        : 'add_email_folder',					
					'name'        : name,		
					'mailbox_id'  : USAM_Page_feedback.email.mailbox_id,						
					nonce         : USAM_Page_feedback.add_email_folder_nonce
				},
				ajax_callback = function(response)
				{						
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}	
					var html = '<li class="" data-folder="'+response.obj.id+'"><a href="'+Universam_Tabs.url+'&m='+USAM_Page_feedback.email.mailbox_id+'&f='+response.obj.slug+'">'+name+'</a></li>';
					$('#folders_mailbox_'+USAM_Page_feedback.email.mailbox_id+' ul').append( html );							
					$('#add_folder_window').modal('hide');	
				};				
			$.usam_post(post_data, ajax_callback);
		},
		
		change_importance : function()
		{ 
			var t = $(this);
			var id          = t.closest('tr').attr('data-id');	
			if ( id > 0 )
			{				
				var important = 1;
				if ( t.hasClass('important') )
					important = 0;
				
				var	post_data   = {
						action        : 'change_importance_email',					
						'id'          : id,		
						'importance'  : important,						
						nonce        : USAM_Page_feedback.change_importance_email_nonce
					},
					ajax_callback = function(response)
					{						
						if (! response.is_successful) 
						{
							alert(response.error.messages.join("\n"));
							return;
						}	
						t.toggleClass('important');	
						if ( important )
							t.removeClass('dashicons-star-empty').addClass('dashicons-star-filled');
						else
							t.removeClass('dashicons-star-filled').addClass('dashicons-star-empty');
					};					
				$.usam_post(post_data, ajax_callback);
			}
		},
		
		iframe_loaded_add_contact : function()
		{ 
			var list_iframe = $('#usam_select_list_iframe').contents();						
			list_iframe.delegate('.email', 'click' , USAM_Page_feedback.email.select_email);			
		},
				
		open_select_email : function(e)
		{ 
			e.preventDefault();	
			
			USAM_Page_feedback.email.id_email_input = $(this).parents('tr').find('.email_adress').attr('id');	
			$('#select_email').modal();	
		},
		
		select_email : function(e)
		{ 
			e.preventDefault();				
			var email_adress = $(this).text();		
			var name_customer = $(this).parents('tr').find('#customer_name').text();	
			if ( name_customer != '' )
				email_adress = name_customer+" <"+email_adress+">";
			
			var value = $('#'+USAM_Page_feedback.email.id_email_input).val();
			if ( value != '' )
			{
				email_adress = value + ", " + email_adress;
			}							
			$('#'+USAM_Page_feedback.email.id_email_input).val( email_adress );
		},	
		
		change_email_folder : function()
		{			
			var folder = $('#set_folder option:selected').val();				
			if ( folder == '' ) 
				return false;
			
			if ( $(".wp-list-table input:checkbox:checked").length  )
			{
				var id = $(".wp-list-table input:checkbox:checked").serializeArray();
				$(".wp-list-table input:checkbox:checked").parents('tr').remove();
			}
			else
			{
				var id = $('.wp-list-table tbody .message_current').data('id');		
				$('.wp-list-table tbody .message_current').remove();				
			}			
			$("#set_folder [value='']").attr("selected", "selected");
			 
			var	post_data   = {
					action        : 'change_email_folder',					
					'folder'      : folder,		
					'id'          : id,						
					nonce         : USAM_Page_feedback.change_email_folder_nonce
				},
				ajax_callback = function(response)
				{							
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}						
				};				
			$.usam_post(post_data, ajax_callback);
		},		
		
		change_message : function( event )
		{ 
			if ( $(event.target).closest(".row-actions").length || $(event.target).hasClass("object_link") || $(event.target).hasClass("importance") ) 
					return;		
	
			var t = $(this).closest('tr');
			USAM_Page_feedback.email.load_message( t );		
			
			if ( t.hasClass("message_unread") && $('.folder_current #new_email_numbers').length ) 
			{
			//	t.removeClass('message_unread');	
				var number = $('.folder_current .numbers').html();				
				if ( number > 1 )
				{	
					number--;			
					$('.folder_current .numbers').html( number );
				}
				else
					$('.folder_current #new_email_numbers').remove();
			}	
			$('.wp-list-table tbody tr').removeClass("message_current");
			t.addClass("message_current");						
			return false;
		},		
		
		delete_email : function(e)
		{			
			e.preventDefault();		
			var row = $('table.email tbody .message_current');
			var t = row.next('tr');				
			var id = row.data('id');
			row.remove();	
			t.addClass("message_current");			
			
			USAM_Page_feedback.email.load_message( t );	
			var	post_data   = {
					action  : 'delete_email',				
					'id'    : id,						
					nonce   : USAM_Page_feedback.delete_email_nonce
				},
				ajax_callback = function(response)
				{						
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}
				};				
			$.usam_post(post_data, ajax_callback);
		},
		
		load_message : function( t )
		{				
			var id          = t.attr('data-id');	
			if ( id > 0 )
			{
				var	post_data   = {
						action        : 'display_message',					
						'id'          : id,						
						 nonce        : USAM_Page_feedback.display_message_nonce
					},
					ajax_callback = function(response)
					{						
						if (! response.is_successful) 
						{
							alert(response.error.messages.join("\n"));
							return;
						}					
						$('.email_header').html(response.obj.header);							
						$('.display_email iframe').contents().find('body').html(response.obj.content);	
						
					//	$('.tab_email').delegate('.usam-delete-link', 'click' , USAM_Page_feedback.email.delete_email);	
					};			
				usam_set_url_attr( 'email_id', id );
				$.usam_post(post_data, ajax_callback);
			}
		}			
	};
	$(USAM_Page_feedback).bind('usam_tab_loaded_email', USAM_Page_feedback.email.event_init);	
	
	USAM_Page_feedback.sms = 
	{		
		event_init : function() 
		{	
			$('.wp-list-table tbody').delegate('td', 'click' , USAM_Page_feedback.sms.change_message);	
			var iframe = $('.email_body iframe', parent.document.body);		
			   iframe.height($(document.body).height());
			   
			if ( $(".wp-list-table tbody tr.message_current").length )
			{			
				USAM_Page_feedback.sms.load_message( $(".wp-list-table tbody tr.message_current") );  
			}			  
		},	
		
		change_message : function( event )
		{						
			if ( $(event.target).closest(".row-actions").length ) 
				return;		
		
			var t = $(this).closest('tr')
			USAM_Page_feedback.sms.load_message( t );
			
			t.removeClass('message_unread');	
			
			var number = $('.message_unread').length;			
			if ( number > 0 )
				$('.folders .numbers').html( "("+number+")" );
			else
				$('.folders .numbers').remove();
			
			$('.wp-list-table tbody tr').removeClass("message_current");	
			t.addClass("message_current");							
			return false;
		},	

		load_message : function( t )
		{						
			var id          = t.attr('data-id'),					
				post_data   = {
					action        : 'display_sms',					
					'id'          : id,						
					 nonce        : USAM_Page_feedback.display_sms_nonce
				},
				ajax_callback = function(response)
				{						
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}					
					$('.email_header .message_header_text').html(response.obj.phone);					
					var iframe = jQuery('.display_email .message').html(response.obj.message);	
				};	
			usam_set_url_attr( 'email_id', id );				
			$.usam_post(post_data, ajax_callback);		
		}			
	};
	$(USAM_Page_feedback).bind('usam_tab_loaded_sms', USAM_Page_feedback.sms.event_init);	
	
})(jQuery);	
USAM_Page_feedback.init();

function iframeLoaded() 
{
    USAM_Page_feedback.email.iframe_loaded_add_contact();
}