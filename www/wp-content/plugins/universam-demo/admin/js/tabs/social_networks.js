/**
* Объект и функции USAM_Page_social_networks.  
* Следующие свойства USAM_Page_social_networks были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_social_networks, 
	{		
		/**
		 * Обязательные события
		 */
		init : function() 
		{					
			$(function()
			{						
				USAM_Page_social_networks.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_social_networks).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
			});
		},		
	});
	
	/**
	 * Вкладка "Управление публикацией вКонтакте"	
	 * @since 3.8.8
	 */
	USAM_Page_social_networks.vk_products = 
	{		
		event_init : function() 
		{						
			USAM_Page_social_networks.wrapper
				.delegate('#bulk-action-selector-top', 'change', USAM_Page_social_networks.vk_products.add_modal);
				
			$('#add_products_vk').delegate('#modal_action', 'click', USAM_Page_social_networks.vk_products.publish_product);					
		},
		
		add_modal : function() 
		{						
			switch ( $(this).val() ) 
			{
				case 'add_product' :					
					$(".chzn-select").chosen({ width: "300px" });	
					var product_title = '';		
					var row = '';						
					$('.wp-list-table tbody .check-column input:checkbox:checked').each(function()
					{
						product_title = $(this).closest('tr').find('.column-product_title a').html();
						row = row+ "<li>"+product_title+"</li>";
					});
					if ( row != '' )
					{
						$('#add_products_vk .colum2 .products').html('<ul></ul>');
						$('#add_products_vk .colum2 .products').append( row );		
					}
					$('#add_products_vk').modal();					
				break;
			}				
		},	
		
		publish_product : function() 
		{	
			var products = [];	
			var category_id = $('#add_products_vk select#category').val();
			var i = 0;
			$('.wp-list-table tbody .check-column input:checkbox:checked').each(function(){
				products[i] = $(this).val();
				i++;
			});				
			if ( i == 0 )
			{
				$('#add_products_vk #products').css('color', '#DC143C');
				return false;
			}			
			if ( category_id == '' )
			{
				$('.colum-category label').css('color', '#DC143C');
				return false;
			}
			var post_data = {
					nonce              : USAM_Page_social_networks.add_products_vk_nonce,	
					action             : 'add_products_vk',		
					profile_id         : $('select#profile_id').val(),	
					album_id           : $('#add_products_vk select#market_album').val(),					
					group 		       : $('#add_products_vk select#group').val(),	
					category     	   : category_id,	
					products           : products,						
				},
				spinner = $('#add_products_vk').find('#ajax-loading');					
			spinner.addClass('ajax-loading-active');		
			var ajax_callback = function( response ) 
			{							
				$('#add_products_vk').modal('hide');
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}												
				var url = location.href;
				if ( response.obj.error ) 
				{
					url = url +'&error='+response.obj.error;								
				}
				else
				{
					url = url +'&update_product='+response.obj.update;	
				}										
				window.location.replace( url );						
				spinner.removeClass('ajax-loading-active');				
			};			
			$.usam_post(post_data, ajax_callback);
			
			$('#add_products_vk .popButton').remove();
		},	
					
	};
	$(USAM_Page_social_networks).bind('usam_tab_loaded_vk_products', USAM_Page_social_networks.vk_products.event_init);
	
})(jQuery);	
USAM_Page_social_networks.init();