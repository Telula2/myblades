/**
* Объект и функции USAM_Page_storage.  
* Следующие свойства USAM_Page_storage были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
* - Механизм: одноразовый номер используется для проверки запроса для загрузки содержимого вкладок с помощью AJAX
*/
(function($)
{	
	$.extend(USAM_Page_storage, 
	{					
		
		init : function() 
		{							
			$(function()
			{	
				USAM_Page_storage.wrapper = $('.tab_'+Universam_Tabs.current_tab);				
				$(USAM_Page_storage).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);				
			}); 			
		}				
	});

	/**
	 * Вкладка "Склады"	
	 * @since 3.8.8
	 */
	USAM_Page_storage.storage = 
	{		
		event_init : function() 
		{	
		
		},		
	};
	$(USAM_Page_storage).bind('usam_tab_loaded_storage', USAM_Page_storage.storage.event_init);	
	
})(jQuery);	
USAM_Page_storage.init();