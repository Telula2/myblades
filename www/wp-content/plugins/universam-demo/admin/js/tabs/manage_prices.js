/**
* Объект и функции USAM_Page_manage_prices.  
* Следующие свойства USAM_Page_manage_prices были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_manage_prices, 
	{			
		init : function() 
		{					
			$(function()
			{					
				USAM_Page_manage_prices.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_manage_prices).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);				
			});
		},							
	});
			
	/**
	 * Вкладка "Управление ценой"	
	 * @since 3.8.8
	 */
	USAM_Page_manage_prices.products = 
	{		
		event_init : function() 
		{	
			USAM_Page_manage_prices.wrapper			
				.delegate('#save-submit' , 'click'   , USAM_Page_manage_prices.products.save_all)
				.delegate('#apply', 'click', USAM_Page_manage_prices.products.bulk_actions);
		},
				
		/**
		 * Массовые действия
		 */
		bulk_actions : function( e ) 
		{				
			e.preventDefault();				
			var operation = $('#price_global_operation').val();			
			var type_operation = $('#price_global_type_operation').val();	
			var value = $('#price_global_value').val();						
			if ( value != '' )
			{				
				value = parseFloat(value);	
				var price = 0;
				var margin = 0;
				$(".wp-list-table .column-price input:text").each( function()
				{						
					price = $(this).val();	
					price = parseFloat(price);					
					switch ( type_operation ) 
					{
						case 'p':	
							margin = price*value/100;						
							margin = parseFloat(margin.toFixed(2));	
						break;	
						case 'f':	
							margin = value;	
						break;								
						default:	
							price = value;
							margin = 0;
						break;	
					}		
					if ( operation == '+' )
						price = price + margin;
					else
						price = price - margin;							
					$(this).addClass("change_made").val(price);
				});	
			}	
		},				
		
		save_all : function(e) 
		{				
			e.preventDefault();
			var spinner = $('#all-ajax-loading');
			var products = [];
			spinner.toggleClass('ajax-loading-active');	
			var i = 0;			
			$(".column-price .change_made").each( function()
			{					
				products[i] = { "price" : $(this).val(), "product_id" : $(this).data('product_id') };
				i++;
			});					
			var	spinner = $('#ajax-loading'),
				post_data   = {
			    	action         : 'change_product_price',
			    	'products'     : products,			
					'code_price'   : $('#type_price').val(),						
			    	nonce          : USAM_Page_manage_prices.change_product_price_nonce
			    },
				ajax_callback = function(response)
				{							
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}						
			    	spinner.toggleClass('ajax-loading-active');					
					USAM_Page_manage_prices.wrapper.find(".change_made").removeClass("change_made");	
					window.location.replace(location.href +'&update_product='+response.obj);		
			    };	
			spinner.toggleClass('ajax-loading-active');					
			$.usam_post(post_data, ajax_callback);			
			return false;			
		},			
	};
	$(USAM_Page_manage_prices).bind('usam_tab_loaded_products', USAM_Page_manage_prices.products.event_init);	
})(jQuery);	
USAM_Page_manage_prices.init();