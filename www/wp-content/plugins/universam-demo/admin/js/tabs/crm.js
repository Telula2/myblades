/**
* Объект и функции USAM_Page_crm.  
* Следующие свойства USAM_Page_crm были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/
(function($)
{
	$.extend(USAM_Page_crm, 
	{		
		/**
		 * Обязательные события
		 */
		comment_id: 0,
		
		init : function() 
		{					
			$(function()
			{
				USAM_Page_crm.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_crm).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);		
				
				$('.usam_event_edit').delegate('#save_action', 'click', USAM_Page_crm.save_event);	
				
				USAM_Page_crm.wrapper
					.delegate('#usam_products #add_product', 'click', USAM_Page_crm.add_product)						
					.delegate('#customer_type', 'change', USAM_Page_crm.select_customer_type)	
					.delegate('#add_contact', 'click', USAM_Page_crm.open_add_contact)		
					.delegate('.select_status_record', 'change' , USAM_Page_crm.change_status_event)					
					.delegate('.usam_cell_line_importance .dashicons ', 'click', USAM_Page_crm.change_event_importance);	
					
				
				if ( $('.tab_'+Universam_Tabs.current_tab+' input.usam_color').length > 0 )
					$('input.usam_color').wpColorPicker( );			
				
				$('.event_object_container')
					.delegate('#delete', 'click', USAM_Page_crm.container_section_delete);						
				
				$('#usam_action_list')
					.delegate('#add_event_action', 'click', USAM_Page_crm.add_event_action)					
					.delegate('#save_name_action', 'click', USAM_Page_crm.edit_action_list)
					.delegate('.status_action', 'click', USAM_Page_crm.edit_status_action)					
					.delegate('.js_delete_action', 'click', USAM_Page_crm.open_confirm_action_list);						
					
				$('#operation_confirm')					
					.delegate('#delete_action', 'click', USAM_Page_crm.delete_action_list);		
				
				$('#usam_participants')
					.delegate('#add_participant', 'click', USAM_Page_crm.add_participant)
					.delegate('.js_delete_action', 'click', USAM_Page_crm.delete_participant); 
			});						
		},		

		change_event_importance : function()
		{ 
			var $importance = $(this).siblings('input[name="importance"]');
			var importance = $importance.val();
			if ( importance == 1)
			{
				$importance.val(0);
				$(this).removeClass('dashicons-star-filled').addClass('dashicons-star-empty');
			}
			else
			{
				$importance.val(1);
				$(this).removeClass('dashicons-star-empty').addClass('dashicons-star-filled');
			}			
		},
			
		add_participant : function()
		{ 		
			var $t = $(this).siblings('#users');	
			var user_id = $t.find(':selected').val();		
			var user_name = $t.find(':selected').html();				
			if ( user_id == 0 )
				return; 			
			
			if ( $('.participants [data-user_id='+user_id+']').length )				
				return;
			
			var post_data = {
					nonce            : USAM_Page_crm.add_participant_nonce,
					action           : 'add_participant',					
					event_id         : USAM_Page_crm.id,
					user_id          : user_id,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');		
			var ajax_callback = function(response) 
			{				
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}							
				if ( response.obj > 0 ) 
				{					
					var html = "<div class='user-box' data-user_id='"+user_id+"'><div class='user_wrapper'><div class='user_name'>"+user_name+"</div><a class='js_delete_action' href='#'></a></div></div>";				
					$('.participants').append(html);
				}	
			};		
			$.usam_post(post_data, ajax_callback);
		},	

		delete_participant : function(e)
		{ 			
			e.preventDefault();	
			var $t = $(this).parents('.user-box');				
			var post_data = {
					nonce               : USAM_Page_crm.delete_event_participant_nonce,
					action              : 'delete_event_participant',					
					event_id            : USAM_Page_crm.id,
					user_id             : $t.data('user_id'),			
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$t.remove();
			};		
			$.usam_post(post_data, ajax_callback);
		},		
		
		open_confirm_action_list : function(e) 
		{ 
			e.preventDefault();		
			USAM_Page_crm.action_list_id = $(this).parents('.action-box').data('id');	
			$('#operation_confirm .action_confirm').attr("id",'delete_action');	
			$('#operation_confirm').modal();			
		},		
		
		delete_action_list : function()
		{ 
			$('#operation_confirm .action_confirm').attr("id",'');	
			var $t = $('.action_lists [data-id='+USAM_Page_crm.action_list_id+']');				
			var post_data = {
					nonce               : USAM_Page_crm.delete_event_action_nonce,
					action              : 'delete_event_action',					
					id                  : USAM_Page_crm.action_list_id,	
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$t.remove();
			};		
			$.usam_post(post_data, ajax_callback);
		},
		
		edit_status_action : function()
		{ 
			var $t = $(this).parents('.action-box');				
			if ( $t.hasClass('made_action') )
			{
				$t.removeClass('made_action');
				var made = 0;
			}
			else
			{
				$t.addClass('made_action');
				var made = 1; 
			}			
			var post_data = {
					nonce            : USAM_Page_crm.edit_event_action_nonce,
					action           : 'edit_event_action',					
					id               : $t.data('id'),
					made             : made,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
			};		
			$.usam_post(post_data, ajax_callback);
		},
				
		edit_action_list : function()
		{ 
			var $action_name = $(this).parents('.event_action_name');	
			var $t = $(this).parents('.action-box');				
			
			var name = $t.find('.event_input_text').val();			
			if ( name == '' )
				return; 
			
			$action_name.find('.usam_display_data').html(name).show();
			$action_name.find('.usam_edit_data').hide().find('textarea').prop("disabled", true);	
			var post_data = {
					nonce            : USAM_Page_crm.edit_event_action_nonce,
					action           : 'edit_event_action',					
					id               : $t.data('id'),
					name             : name,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
			};		
			$.usam_post(post_data, ajax_callback);
		},
		
		add_event_action : function()
		{ 
			var $t = $(this).siblings('.js_new_event_action');	
			var name = $t.val();			
			if ( name == '' )
				return; 
			$t.val('');			
			var post_data = {
					nonce            : USAM_Page_crm.add_event_action_nonce,
					action           : 'add_event_action',					
					id               : USAM_Page_crm.id,
					name             : name,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');		
			var ajax_callback = function(response) 
			{				
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
				if ( response.obj != false ) 
				{					
					var html = "<div class='action-box' data-id='"+response.obj.id+"'><div class='action_wrapper'><div class='status_action'></div><div class='event_action_name'><div id = 'edit_name_action' class='usam_display_data'>"+name+"</div><div class='usam_edit_data'><textarea class='event_input_text' tabindex='1' dir='auto'>"+name+"</textarea><button id='save_name_action' type='button' class='button'>"+USAM_Page_crm.add_button_text+"</button></div></div><a class='js_delete_action' href='#'></a></div></div>";				
					$('.action_lists').append(html);
				}	
			};		
			$.usam_post(post_data, ajax_callback);
		},				
		
		change_status_event : function()
		{ 
			var id            = jQuery(this).data('id'),		
				status_record = jQuery(this).val(),
				spinner = $(this).siblings('#ajax-loading'),		
				post_data   = {
					action        : 'edit_event',					
					'status'      : status_record,
					'event_id'    : id,				
					 nonce        : USAM_Page_crm.edit_event_nonce
				},				
				ajax_callback = function(response)
				{	
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}			    	
					spinner.toggleClass('ajax-loading-active');	
				};					
			spinner.toggleClass('ajax-loading-active');
			$.usam_post(post_data, ajax_callback);		
			return false;
		},
		
		iframe_loaded_add_contact : function()
		{ 
			var list_iframe = $('#usam_select_list_iframe').contents();	
			list_iframe.delegate('.wp-list-table .column-select a', 'click', USAM_Page_crm.add_contact);	
		},
		
		open_add_contact : function(e)
		{ 
			e.preventDefault();	
			
			var list_iframe = $('#usam_select_list_iframe').contents();				
			var company_id = $('#select_company_1').val();	
			if ( company_id == 0 )						
				list_iframe.find('#contact option[value="company"]').hide();			
			else
				list_iframe.find('#contact option[value="company"]').show();
			list_iframe.find('#company').val( company_id );
			
			$('#select_contacts').modal();
		},	

		add_contact : function(e) 
		{		
			e.preventDefault();				
			var tr = $(this).parents('tr');
			var customer_id = tr.data('customer_id');			
			$('.table_contacts table .items_empty').hide();
			if ( ! $('.table_contacts table #contact_'+customer_id).length > 0 ) 
			{
				var contact_name = tr.find('#customer_name').text();	
				var row = "<td>"+contact_name+"<a class = 'button_delete' href=''></a><input type='hidden' name='contacts_ids[]' value='"+customer_id+"'/></td>";
				$(".table_contacts table tbody").append("<tr id='contact_"+customer_id+"'>"+row+"</tr>");
			}			
		},

		container_section_delete : function(e) 
		{			
			$(this).closest('.event_object_container').remove();	
		},		
		
		save_event : function() 
		{		
			var main_div = $(this).closest('.usam_event_edit');
			var date = main_div.find('#usam_date_picker-start').val();
			var hour = main_div.find('#usam_date_hour-start').val();
			var minute = main_div.find('#usam_date_minute-start').val();
			var	mas_date = date.split('-');	
			
			var	year = mas_date[2];	
			var	month = mas_date[1];	
			var	day = mas_date[0];	
			month = month-1;
						
			var d = new Date(year, month, day, hour, minute );
		
			var event_name = main_div.find('#event_name');
			var name = event_name.val();
			event_name.val('');
			
			var event_description = main_div.find('#event_description');
			var description = event_description.val();
			event_description.val('');	
			
			var importance = 0;
			if ( main_div.find('#event_importance').prop('checked') )
				importance = 1;
			
			var calendar = main_div.find('#event_calendar').val();
			var reminder_date_format = '';
			var object_id = main_div.find('#event_object_id').val();
			var object_type = main_div.find('#event_object_type').val();	
			var event_type = main_div.find('#event_type').val();				
			
			if ( main_div.find('#usam_remind').prop('checked') )
			{
				var date = main_div.find('#usam_date_picker-date_time').val();
				var hour = main_div.find('#usam_date_hour-date_time').val();
				var minute = main_div.find('#usam_date_minute-date_time').val();
				var	mas_date = date.split('-');	
				
				var	year = mas_date[2];	
				var	month = mas_date[1];	
				var	day = mas_date[0];	
				month = month-1;
							
				var reminder_date = new Date(year, month, day, hour, minute );				
				month = reminder_date.getUTCMonth()+1;
				reminder_date_format = reminder_date.getUTCFullYear()+'-'+month+'-'+reminder_date.getUTCDate()+' '+reminder_date.getUTCHours()+':'+reminder_date.getUTCMinutes()+":00";	
			}
			month = d.getUTCMonth()+1;
			date_format = d.getUTCFullYear()+'-'+month+'-'+d.getUTCDate()+' '+d.getUTCHours()+':'+d.getUTCMinutes()+":00";		

			var post_data = {
					nonce      : USAM_Page_crm.add_event_nonce,
					action     : 'add_event',
					date_from  : date_format,
					name       : name,
					description: description,
					importance : importance,
					calendar   : calendar,	
					reminder_date  : reminder_date_format,		
					type       : event_type,			
					object_id  : object_id,		
					object_type: object_type,					
				},
				spinner = $(this).siblings('#ajax-loading');			
			spinner.addClass('ajax-loading-active');
			$('#event_windows').modal('hide');			
			var ajax_callback = function( response )
			{	
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}		
				if ( $('usam_view_form_tabs').length > 0  )
					USAM_Page_crm.view_form_tab();	
				else
				{				
					USAM_Page_crm.add_customer_case( object_id, object_type );
				}
			};			
			$.usam_post(post_data, ajax_callback);
		},	
		
		add_customer_case : function( object_id, object_type ) 
		{
			var post_data = {
				nonce   : USAM_Page_crm.form_customer_case_nonce,
				action  : 'form_customer_case',				
				customer_type : object_type,		
				id            : object_id,						
			},
			spinner = $('tr#'+object_type+'-'+object_id+' #customer_case').find('#ajax-loading'+object_id);			
			spinner.addClass('ajax-loading-active');
			var ajax_callback = function( response )
			{	
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}	
				$('tr#'+object_type+'-'+object_id+' #customer_case').html( response.obj );							
			};
			$.usam_post(post_data, ajax_callback);	
		},
		
		view_form_tab : function( ) 
		{
			var tab = $('.usam_view_form_tabs .header_tab .current').data('tab');
			var post_data = {
				nonce   : USAM_Page_crm.view_form_tab_nonce,
				action  : 'view_form_tab',	
				page    : 'crm',
				tab     : Universam_Tabs.current_tab,		
				id      : USAM_Page_crm.id,							
				view_form_tab     : tab,							
			},
			spinner = $(this).siblings('#ajax-loading');			
			spinner.addClass('ajax-loading-active');
			var ajax_callback = function( response )
			{	
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}	
				$('.usam_view_form_tabs .countent_tabs #view_form-'+tab).html( response.obj );							
			};
			$.usam_post(post_data, ajax_callback);			
		},
		
		select_customer_type : function(  ) 
		{	
			$("#customer_type_company").toggle();
			$("#customer_type_contact").toggle();
		},	

		open_message_send : function(e) 
		{ 
			e.preventDefault();	
			var email = '';			
			if ( $('#content_form_view').length > 0 )
			{				
				var customer_id = $('#content_form_view').data('id');
				email = $('.communication-email').text();
			}	
			else
			{				
				var row = $(this).closest('tr');	
				var customer_id = row.data('company');					
				row.find(".email p").each( function()
				{						
					email = $(this).text();					
				});					
			}		
			$('#quick_response #to_email').val( email );						
			$('.mailing #object_id').val( customer_id );
			if ( Universam_Tabs.current_tab == 'contacts' )
				$('.mailing #object_type').val( 'contact' );	
			else
				$('.mailing #object_type').val( 'company' );	
			$('#quick_response').modal();			
		},		
		
		open_task_windows : function(e) 
		{ 
			e.preventDefault();	
			
			var item_class = $(this).attr('class');
			var event_type = 'event';
			if ( item_class == 'usam-add_meeting-link' )
				event_type = 'meeting';
			else if ( item_class == 'usam-add_phone-link' )
				event_type = 'call';
			
			
			if ( $('#content_form_view').length > 0 )
			{				
				var company_id = $('#content_form_view').data('id');
				var company_name = $('.tab-content #customer_name').text();	
			}	
			else
			{
				var row = $(this).closest('tr');
				var company_id = row.data('customer_id');			
				var company_name = row.find('#customer_name').text();	
			}			
			$('#event_windows .usam_event_edit .usam_txt').html( company_name );
			$('#event_windows .usam_event_edit #event_object_id').val( company_id );	
			
			$('#event_windows .usam_event_edit #event_type').val( event_type );		
			$('#event_windows').modal();			
		},	
				
		// Добавить товар
		add_product : function() 
		{		
			var product_id = $('#product_box #select_product_1').val();	
			if ( product_id == '' )
				return; 
		
			var post_data = {
					nonce               : USAM_Page_crm.add_product_to_document_crm_nonce,
					action              : 'add_product_to_document_crm',
					document            : Universam_Tabs.current_tab,
					id                  : USAM_Page_crm.id,
					product_id          : product_id,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');	
			var ajax_callback = function(response) 
			{					
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}			
				if ( response.obj.error != '' ) 
				{
					alert(response.obj.error); 
					return;
				}				
				spinner.removeClass('ajax-loading-active');	
				$("table.table_products").remove();	
				$('.table_products_box').html(response.obj.content);			
			};		
			$.usam_post(post_data, ajax_callback);				
		},			
		

		delete_row_means_communication : function( e ) 
		{				
			e.preventDefault();
			var row = $(this).closest('tr').remove();			
		},		
				
		add_row_means_communication : function( e ) 
		{				
			e.preventDefault();
			var row = $(this).closest('tr').prev();
			var html = row.html();			
			row.before('<tr>'+html+'</tr>');
		},		
	});
	
	/**
	 * Вкладка "Задания"	
	 */
	USAM_Page_crm.tasks = 
	{				
		event_init : function() 
		{							
			$('#usam_event_file_upload')
				.delegate('.js_delete_action', 'click', USAM_Page_crm.tasks.open_confirm_delete_file);
				
			$('#operation_confirm')					
					.delegate('#delete_event_file', 'click', USAM_Page_crm.tasks.delete_event_file);	
		},		
		
		open_confirm_delete_file : function(e) 
		{ 
			e.preventDefault();		
			var id = $(this).parent('li').data('id');	
			$('#operation_confirm .action_confirm').attr("id",'delete_event_file').attr('data-id', id);	
			$('#operation_confirm').modal();		
		},		
		
		delete_event_file : function()
		{ 
			$('#operation_confirm .action_confirm').attr("id",'');	
			delete_attachment.file_id = $('#operation_confirm .action_confirm').data("id");
			
			var $t = $('#usam_event_file_upload [data-id='+delete_attachment.file_id+']');				
			var spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$t.remove();
			};		
			$.usam_post(delete_attachment, ajax_callback);
		},
	};
	$(USAM_Page_crm).bind('usam_tab_loaded_tasks', USAM_Page_crm.tasks.event_init);	
		
	
	/**
	 * Вкладка "контакты"	
	 */
	USAM_Page_crm.contacts = 
	{		
		not_send_form : true,
		
		event_init : function() 
		{					
			USAM_Page_crm.wrapper
				.delegate('#usam_means_communication table#contact_table #add_row' , 'click'   , USAM_Page_crm.add_row_means_communication)
				.delegate('#usam_means_communication table#contact_table tr .button_delete' , 'click'   , USAM_Page_crm.delete_row_means_communication)
				.delegate('.usam-send_message-link', 'click', USAM_Page_crm.open_message_send)
				.delegate('.usam-add_meeting-link', 'click', USAM_Page_crm.open_task_windows)
				.delegate('.usam-add_phone-link', 'click', USAM_Page_crm.open_task_windows)
				.delegate('.usam-add_reminder-link', 'click', USAM_Page_crm.open_task_windows)				
				.delegate('.validate' , 'click'   , USAM_Page_crm.contacts.event_select_upload);			
		},
				
		event_select_upload : function(  ) 
		{	
			$('.csvmode').hide();
			$( '.'+$(this).attr('id') ).show();		
		},		
	};	
	$(USAM_Page_crm).bind('usam_tab_loaded_contacts', USAM_Page_crm.contacts.event_init);	
	
	/**
	 * Вкладка "Компании"	
	 */
	USAM_Page_crm.company = 
	{		
		not_send_form : true,
		
		event_init : function() 
		{					
			USAM_Page_crm.wrapper
				.delegate('#usam_means_communication table #add_row' , 'click'   , USAM_Page_crm.add_row_means_communication)
				.delegate('#usam_means_communication table tr a.button_delete' , 'click'   , USAM_Page_crm.delete_row_means_communication)
				.delegate('.validate' , 'click'   , USAM_Page_crm.company.event_select_upload)	
				.delegate('#hide_add' , 'click'   , USAM_Page_crm.company.hide_bank_accounts)	
				.delegate('#show_add' , 'click'   , USAM_Page_crm.company.add_bank_accounts)	
				.delegate('.usam-send_message-link', 'click', USAM_Page_crm.open_message_send)
				.delegate('.usam-add_meeting-link', 'click', USAM_Page_crm.open_task_windows)
				.delegate('.usam-add_phone-link', 'click', USAM_Page_crm.open_task_windows)
				.delegate('.usam-add_reminder-link', 'click', USAM_Page_crm.open_task_windows)				
				.delegate('#usam_company_acc_number .button_delete' , 'click'   , USAM_Page_crm.company.delete_acc_number);					
		},
		
		delete_acc_number : function( e ) 
		{	
			e.preventDefault();		
			$(this).closest('tr').remove();	
		},
		
		event_select_upload : function(  ) 
		{	
			$('.csvmode').hide();
			$( '.'+$(this).attr('id') ).show();		
		},	
		
		add_bank_accounts : function(  ) 
		{	
			$(this).hide();
			$("#hide_add").show();				
			$('#acc_number-0 .usam_edit_data').prop("disabled", false);	
			$("#acc_number-0").show();
			
			$("#acc_number-0 .usam_display_data").hide();	
			$("#acc_number-0 .usam_edit_data").show();			
		},
		
		hide_bank_accounts : function(  ) 
		{	
			$(this).hide();
			$("#show_add").show();		
			$('#acc_number-0 .usam_edit_data').prop("disabled", true);	
			$("#acc_number-0").hide();	
		},
	};
	$(USAM_Page_crm).bind('usam_tab_loaded_company', USAM_Page_crm.company.event_init);	
	
	
	/**
	 * Вкладка "счета"	
	 */
	USAM_Page_crm.invoice = 
	{				
		event_init : function() 
		{							
			
		},		
	};
	$(USAM_Page_crm).bind('usam_tab_loaded_invoice', USAM_Page_crm.invoice.event_init);	
	
	
	/**
	 * Вкладка "Коммерческое предложение"	 	
	 */
	USAM_Page_crm.suggestions = {
		
		event_init : function() 
		{				
			
		},		
	};
	$(USAM_Page_crm).bind('usam_tab_loaded_suggestions', USAM_Page_crm.suggestions.event_init);
})(jQuery);	
USAM_Page_crm.init();

function iframeLoaded() 
{
    USAM_Page_crm.iframe_loaded_add_contact();
}