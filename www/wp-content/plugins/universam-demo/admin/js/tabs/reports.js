/**
* Объект и функции USAM_Page_reports.  
* Следующие свойства USAM_Page_reports были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/
(function($)
{
	$.extend(USAM_Page_reports, 
	{		
		/**
		 * Обязательные события	
		 */
		init : function() 
		{					
			$(function()
			{					
				USAM_Page_reports.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_reports).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	

				USAM_Page_reports.wrapper.delegate('.select__control', 'change', USAM_Page_reports.select_period_group)
										 .delegate('.date_interval', 'change', USAM_Page_reports.select_period_group)
										 .delegate('.wp-list-table .manage-column', 'click', USAM_Page_reports.select_colum_graph);
				if ( typeof usam_data_graph !== "undefined" )
				{			
					USAM_Page_reports.graph( usam_data_graph, usam_name_graph );
				}
			});
		},				
		
		select_colum_graph : function( e ) 
		{
			if ( $(this).hasClass("column-date") )
				return false;
			
			if ( $(".wp-list-table tbody tr.no-items").length != 0 )
				return false;
			
			var usam_name_graph = $(this).text();
			var usam_data_graph = [];	
			
			var id = $(this).attr('id');	
			var i = $(".wp-list-table tbody .column-"+id).length-1;
			$(".wp-list-table tbody .column-"+id).each(function(index, elem) 
			{							
				number = parseFloat($(this).text().replace(".","").replace(",","."));
				usam_data_graph[i] = {						
					'date'     : $(this).closest('tr').find(".column-date").text(),
					'x_data'    : number,
					'label'    : usam_name_graph+": "+$(this).text()
				};	
				i--;
			});					
			$("#graph").html('');			
			USAM_Page_reports.graph( usam_data_graph, usam_name_graph );				
		},		
		
		graph : function( usam_data_graph, usam_name_graph ) 
		{	
			if ( usam_data_graph.length != 0 )
			{		
				USAM_Page_reports.display_graph( usam_data_graph, usam_name_graph );				
				$("#graph").show();
			}
			else
				$("#graph").hide();
		},
		
		display_graph : function( data, name_graph ) 
		{						
			var width_graph = jQuery('.usam_tab_table').width();
				
			var margin = {top: 60, right: 10, bottom: 40, left: 70},
				width = width_graph - margin.left - margin.right,
				height = 500 - margin.top - margin.bottom;

			var x = d3.scale.ordinal()
				.rangeRoundBands([0, width], .1, .3);

			var y = d3.scale.linear()
				.range([height, 0]);

			var xAxis = d3.svg.axis()
				.scale(x)
				.orient("bottom"); 
				
		/*	var x_text = d3.time.scale()
				.domain([new Date(2017, 0, 2), new Date(2018, 3, 1)])
				.range([0, width-500]);				

			var xAxis = d3.svg.axis()
				.scale(x)
				.orient("bottom")
				.tickSize(8, 0)
				.tickFormat(d3.time.format("%d.%m.%Y"));			
			*/
			var yAxis = d3.svg.axis()
				.scale(y)
				.orient("left")
				.ticks(8, "");

			var svg = d3.select("#graph")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)					
			  .append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");			

				/*функция масштабирования значений по оси X*/
				var xScale = x.domain(data.map(function(d) { return d.date; }));
				/*функция масштабирования значений по оси Y*/
				var yScale = y.domain([0, d3.max(data, function(d) { return d.x_data; })]);

			  svg.append("text")
				  .attr("class", "title")
				  .attr("x", -70)
				  .attr("y", -26)				  
				  .text( name_graph );

			  svg.append("g")
				  .attr("class", "x axis")			  
				  .attr("transform", "translate(0," + height + ")")
				  .call(xAxis)
				.selectAll(".tick text")
				  .call( USAM_Page_reports.wrap, x.rangeBand());
// вывод значений y
			  svg.append("g")
				  .attr("class", "y axis")				  
				  .call(yAxis);

				  // создаем набор вертикальных линий для сетки   
d3.selectAll("g.x g.tick")
    .append("line") // добавляем линию
    .classed("grid-line", true) // добавляем класс	
//	.attr("style", 'stroke: #cccccc')
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", 0)
    .attr("y2", - (height));
     
// рисуем горизонтальные линии 
d3.selectAll("g.y g.tick")
    .append("line")   
	.classed("grid-line", true)
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", width)
    .attr("y2", 0);
	
			var labelsContainers = svg.selectAll(".bar")		
				.data(data)				 
				.enter().append("rect")
				.attr("class", "bar")
				.attr("x", function(d) { return x(d.date); })
				.attr("width", x.rangeBand())
				.attr("y", function(d) { return y(d.x_data); })
				.attr("height", function(d) { return height - y(d.x_data); })
				.text(function (d) { return "Расходы: " + d.x_data; })
				.on("mouseenter", function (d, i) {
					svg.select("#label" + i).style("display", "block");
				})
				.on("mouseleave", function (d, i) { 
					svg.select("#label" + i).style("display", "none"); 
				});
		
			
			var xpScale = d3.scale.ordinal()
				.rangeRoundBands([0, width], .2)
				.domain(data.map(function (d) { return d.date; }));
				
			/*Ниже код для добавления всплывающей подсказки*/
			var labelsContainers = svg.selectAll("g.label")
				.data(data)
				.enter()
				.append("g")
				.attr("class", "label")
				.attr("transform", function (d) {
					var lInitialX = xScale(d.date);
					var lX = lInitialX + xScale.rangeBand() / 2;
					var lY = yScale(d.x_data);
					return "translate(" + lX + ", " + lY + ")";
				})
				.attr("id", function (d, i) { return "label" + i; })
				.style("display", "none");
			labelsContainers.append("polygon")
				.attr("points", "0,0 -5,-10 -70,-10 -70,-50 70,-50 70,-10 5,-10");
			labelsContainers.append("text")
				.attr("id", function (d, i) { return "date" + i; })
				.attr("x", "0")
				.attr("y", function (d) {
					return -35;
				})
				.style("text-anchor", "middle")
				.text(function (d) { return "Дата: " + d.date; });
			labelsContainers.append("text")
				.attr("id", function (d, i) { return "value" + i; })
				.attr("x", "0")
				.attr("y", function (d) {
					return -15;
				})
				.style("text-anchor", "middle")
				.text(function (d) { return d.label; });
		},
		
		wrap : function( text, width ) 
		{ 
			text.each(function() 
			{ 
				var text = d3.select(this),
					words = text.text().split(/\s+/).reverse(),
					word,
					line = [],
					lineNumber = 0,
					lineHeight = 1.1, // ems
					y = text.attr("y"),
					dy = parseFloat(text.attr("dy")),
					tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
				while (word = words.pop()) 
				{
					line.push(word);
					tspan.text(line.join(" "));
					if (tspan.node().getComputedTextLength() > width) 
					{
						line.pop();
						tspan.text(line.join(" "));
						line = [word];
						tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
				  }
				}
			});
		},
		
		select_period_group : function(  ) 
		{
			$("#usam-tab_form").submit();
		},
	});	
})(jQuery);	
USAM_Page_reports.init();