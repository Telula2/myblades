/**
* Объект и функции USAM_Page_newsletter.  
* Следующие свойства USAM_Page_newsletter были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_newsletter, 
	{		
		/**
		 * Обязательные события		 
		 */
		init : function() 
		{					
			$(function()
			{	
				USAM_Page_newsletter.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_newsletter).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);								
			});
		},		
	});
	
	/**
	 * Вкладка "Email-рассылка"	
	 */
	USAM_Page_newsletter.sending_email = 
	{		
		event_init : function() 
		{			
			USAM_Page_newsletter.wrapper						
				.delegate('form#usam-table_item_form' , 'submit', USAM_Page_newsletter.sending_email.event_next_step)			
				.delegate('form#usam-table_item_form', 'submit', USAM_Page_newsletter.sending_email.event_submit_screen_end)
				.delegate('#event_start', 'change', USAM_Page_newsletter.sending_email.trigger_condition)
				.delegate('.column-action .action', 'click', USAM_Page_newsletter.sending_email.event_email_action);
				
				
			USAM_Page_newsletter.sending_email.trigger_condition();
		},
		
		
		event_email_action : function( ) 
		{			
			var action_status;
			var t = $(this);
			if ( t.attr('id') == 'status-5' )
				action_status = 0;
			else
				action_status = 1;
		
			var spinner     = jQuery('#ajax-loading'),			
				post_data   = {
					action          : 'sending_control',						
					'action_status' : action_status,	
					'mail_id'       : $(this).data('mail_id'),	
					nonce           : USAM_Page_newsletter.sending_control_nonce
				},
				ajax_callback = function(response)
				{				
					if ( action_status )
						t.attr('id', 'status-5');
					else
						t.attr('id', 'status-4');
					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}			    	
					spinner.toggleClass('ajax-loading-active');	
				};					
			spinner.toggleClass('ajax-loading-active');
			$.usam_post(post_data, ajax_callback);			
			return false;
		},	
		
		trigger_condition : function(e) 
		{			
			if ( $('#trigger_condition').length != 0 )
			{				
				$('#trigger_condition table').hide();
				var event_start = $('#event_start').val();				
				
				$('#trigger_condition table').find('select').prop("disabled", true);
				$('#trigger_condition table').find('input').prop("disabled", true);				
				
				$('#trigger_condition #'+event_start).find('select').prop("disabled", false);
				$('#trigger_condition #'+event_start).find('input').prop("disabled", false);				
				
				$('#trigger_condition #'+event_start).show();		
			}
		},
		
		event_next_step : function(e) 
		{				
			if ( $('.titlebox').length != 0 && $('.titlebox').val() == '' )
			{			
				$('.titlebox').css("border", "1px solid #a4286a");			
				e.preventDefault();		
			}					
		},
				
		event_submit_screen_end : function(e) 
		{			
			if ( $('#list_of_subscribers input').length != 0 )
			{
				var checked = false;
				$('#list_of_subscribers input').each(function()
				{
					if ( $(this).is(':checked') )
					{
						checked = true;
						return false;
					}
				});			
				if ( !checked )
				{
					$('#list_of_subscribers input').css("border", "1px solid #a4286a");		
					e.preventDefault();		
				}
			}			
		},
	};
	$(USAM_Page_newsletter).bind('usam_tab_loaded_sending_email', USAM_Page_newsletter.sending_email.event_init);	
	
	/**
	 * Вкладка "Email-рассылка"	
	 */
	USAM_Page_newsletter.subscriber = 
	{		
		event_init : function() 
		{			
			USAM_Page_newsletter.wrapper		
				.delegate('.validate' , 'click'   , USAM_Page_newsletter.subscriber.preview);	
			USAM_Page_newsletter.subscriber.preview();
		},
							
		preview : function() 
		{		
			if ( $("#copy-paste").prop("checked") )
			{
				$(".tab_subscriber .copy").show();
				$('.tab_subscriber .upload').hide();
				
			}
			else
			{
				$(".tab_subscriber .upload").show();
				$('.tab_subscriber .copy').hide();
			}
		},	
	};
	$(USAM_Page_newsletter).bind('usam_tab_loaded_subscriber', USAM_Page_newsletter.subscriber.event_init);	
})(jQuery);	
USAM_Page_newsletter.init();