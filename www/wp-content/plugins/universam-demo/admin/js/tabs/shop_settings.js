/**
* Объект и функции USAM_Page_shop_settings.  
*/
(function($)
{
	$.extend(USAM_Page_shop_settings, 
	{		
		/**
		 * Обязательные события
		 */
		init : function() 
		{					
			$(function()
			{			
				USAM_Page_shop_settings.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_shop_settings).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
				
				USAM_Page_shop_settings.wrapper.
				                   delegate('input, textarea, select', 'change', USAM_Page_shop_settings.event_settings_changed).						
				                   delegate('#usam-settings-form'    , 'submit', USAM_Page_shop_settings.event_settings_form_submitted).
								   delegate('a#link_description'    , 'click', USAM_Page_shop_settings.show_description).
								   delegate('#sms', 'keypress', USAM_Page_shop_settings.event_change_sms).
								   delegate('#sms', 'keyup', USAM_Page_shop_settings.event_change_sms);
								   
				USAM_Page_shop_settings.hide_description();	
				if ( $('.tab_'+Universam_Tabs.current_tab+' input.usam_color').length > 0 )
					$('input.usam_color').wpColorPicker( );					
			});
		},	

		show_description : function(event) 
		{				
			var  description = $(this).siblings(".description_box");
			if ( description.is(":visible") ) {				
				description.hide();
			} else {				
				description.show();
			}					
			event.preventDefault();
		},
		
		hide_description : function() 
		{				
			jQuery('.detailed_description').find('.description_box').hide();			
		},
						
		/**
		 * Это предотвращает диалог подтверждения вызвана event_before_unload не будет отображаться.
		 * @since 3.8.8
		 */
		event_settings_form_submitted : function() {
			USAM_Page_shop_settings.unsaved_settings = false;
		},

		/**
		 * Mark the page as "unsaved" when a field is modified
		 * @since 3.8.8
		 */
		event_settings_changed : function() {
			USAM_Page_shop_settings.unsaved_settings = true;
		},		
		
		event_change_sms : function() 
		{ 
			var length = $(this).val().length;
			$(this).parent().find("#characters").html(length);
		},	
	});		
	
	USAM_Page_shop_settings.product_view = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper			    
					.delegate('.template_active', 'change', USAM_Page_shop_settings.product_view.template_active)
					.delegate('.up-carousel', 'click', USAM_Page_shop_settings.product_view.up_carousel)
					.delegate('.down-carousel', 'click', USAM_Page_shop_settings.product_view.down_carousel);		
			
			$('#products_for_buyers').sortable({
					cursor: "move",
					items       : 'tr',
					containment : 'parent',	
					containment : 'parent',		
					zIndex      : 9999,	
					cursorAt: { left: 80 },		
					dropOnEmpty: false,					
					forceHelperSize: false,
					forcePlaceholderSize: false,
				});	
		},
		
		change_template : function( item, carousel ) 
		{			
			var name_template = item.data('name_template');
			carousel.closest('tr').find('.template_input').val(name_template);
		},
	
		up_carousel : function(e) 
		{ 
			e.preventDefault();	
			var carousel = $(this).siblings('#jcarousel');
			var item = carousel.find('ul .active');
			if ( item.length == 0)
			{
				item = carousel.find('li').first().addClass('active');
				USAM_Page_shop_settings.product_view.change_template( item, carousel);
			}
			else
			{			
				var next_li = item.prev();
				if ( next_li.length != 0 )
				{
					$(item).removeClass('active');
					$(next_li).addClass('active');
					USAM_Page_shop_settings.product_view.change_template( next_li, carousel);
				}
			}
		},	
		
		down_carousel : function(e) 
		{			
			e.preventDefault();	
			var carousel = $(this).siblings('#jcarousel');
			var item = carousel.find('ul .active');
			if ( item.length == 0)
			{
				item = carousel.find('li').first().addClass('active');
				USAM_Page_shop_settings.product_view.change_template( item, carousel);
			}
			else
			{			
				var next_li = item.next();
				if ( next_li.length != 0 )
				{
					$(item).removeClass('active');
					$(next_li).addClass('active');
					USAM_Page_shop_settings.product_view.change_template( next_li, carousel);
				}
			}
		},		
		
		template_active : function() 
		{
			$(this).closest('.postbox').toggleClass('active');
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_product_view', USAM_Page_shop_settings.product_view.event_init);
	
	
	/**
	 * Бланки
	 */
	USAM_Page_shop_settings.blanks = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper
			      //  .delegate('#usam-table_item_form', 'submit', USAM_Page_shop_settings.blanks.save_form_blanks);
					.delegate('.button_save_close', 'click', USAM_Page_shop_settings.blanks.save_form_blanks)
					.delegate('.button_save', 'click', USAM_Page_shop_settings.blanks.save_form_blanks);
		},
			
		save_form_blanks : function() 
		{
			var $close = false;
			if ( $(this).attr('id') == "edit-submit-save-close" )
				$close = true;
			
			var iframe = jQuery('#edit_form_blanks').contents();					
			var i = 0;			
			var input = {};
			var textarea = {};
			var table = {};		
			iframe.find(".option_form_input").each( function()
			{					
				input[i] = { "value" : $(this).val(), "name" : $(this).attr('name') };				
				++i;
			});		
			iframe.find(".option_form_textarea").each( function()
			{					
				textarea[i] = { "value" : $(this).val(), "name" : $(this).attr('name') };				
				++i;
			});				
			
			i = 0;	
			iframe.find(".table_colum_edit").each( function()
			{								
				table[i] = { "value" : $(this).val(), "name" : $(this).attr('name'), 'show': $(this).siblings(".table_colum_edit_show").prop('checked') };				
				++i;
			});							
			var post_data   = {
					action        : 'save_blank',
					'input'       : input,		
					'textarea'    : textarea,						
					'table'       : table,						
					'blank'       : jQuery('#edit_form_blanks').data('id'),						
					nonce         : USAM_Page_shop_settings.save_blank_nonce
				},			
				ajax_callback = function(response)
				{					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}						
					if ( $close )
					{
						$('#usam-table_item_form').submit();
						return true;
					}
					else
						window.location.replace(location.href +'&update=1');						
				};				
			$.usam_post(post_data, ajax_callback);	
			return false;			
			
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_blanks', USAM_Page_shop_settings.blanks.event_init);
	
	
	/**
	 * Почтовые ящики
	 */
	USAM_Page_shop_settings.mailboxes = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper			     
					.delegate('#test', 'click', USAM_Page_shop_settings.mailboxes.open_windows_test);
					
			$('#usam_users_mailbox')
					.delegate('#add_participant', 'click', USAM_Page_shop_settings.mailboxes.add_participant)
					.delegate('.js_delete_action', 'click', USAM_Page_shop_settings.mailboxes.delete_participant);
		},
		
		add_participant : function()
		{ 		
			var $t = $(this).siblings('#users');	
			var user_id = $t.find(':selected').val();		
			var user_name = $t.find(':selected').html();				
			if ( user_id == 0 )
				return; 
			
			if ( $('.participants [data-user_id='+user_id+']').length )				
				return;
			
			var post_data = {
					nonce            : USAM_Page_shop_settings.add_mailbox_user_nonce,
					action           : 'add_mailbox_user',					
					id               : Universam_Tabs.id,
					user_id          : user_id,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');		
			var ajax_callback = function(response) 
			{				
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}							
				if ( response.obj > 0 ) 
				{					
					var html = "<div class='user-box' data-user_id='"+user_id+"'><div class='user_wrapper'><div class='user_name'>"+user_name+"</div><a class='js_delete_action' href='#'></a></div></div>";				
					$('.participants').append(html);
				}	
			};		
			$.usam_post(post_data, ajax_callback);
		},	

		delete_participant : function(e)
		{ 			
			e.preventDefault();	
			var $t = $(this).parents('.user-box');				
			var post_data = {
					nonce               : USAM_Page_shop_settings.delete_mailbox_user_nonce,
					action              : 'delete_mailbox_user',					
					id                  : Universam_Tabs.id,
					user_id             : $t.data('user_id'),			
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$t.remove();
			};		
			$.usam_post(post_data, ajax_callback);
		},
			
		open_windows_test : function() 
		{			
			$('#check_connection').modal();					
			var spinner = $('#check_connection #ajax-loading');			
			spinner.addClass('ajax-loading-active');		
			
			$('#check_connection .status_connection').html('');
			var post_data   = {
					action        : 'test_mailbox',
					'id'          : USAM_Page_shop_settings.id,						
					nonce         : USAM_Page_shop_settings.test_mailbox_nonce
				},			
				ajax_callback = function(response)
				{					
					if (! response.is_successful) 
					{
						alert(response.error.messages.join("\n"));
						return;
					}		
					spinner.removeClass('ajax-loading-active');				
					$('#check_connection .status_connection').html( response.obj );
				};				
			$.usam_post(post_data, ajax_callback);	
			return false;			
			
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_mailboxes', USAM_Page_shop_settings.mailboxes.event_init);
	
	/**
	 * Шаблоны сообщений
	 */
	USAM_Page_shop_settings.template_messages = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper.
			        delegate('.theme .theme-screenshot', 'click', USAM_Page_shop_settings.template_messages.event_select_template_messages);
		},
	
		event_select_template_messages : function() 
		{
			//$('.themes .theme').removeClass('active');
		//	$(this).addClass('active');			
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_template_messages', USAM_Page_shop_settings.template_messages.event_init);	
	
	/**
	 * Перекрёстные продажи
	 */
	USAM_Page_shop_settings.crosssell = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper
			        .delegate('#check_type', 'change', USAM_Page_shop_settings.crosssell.event_select_check_type)
					.delegate('#usam_conditions .condition-logic', 'click', USAM_Page_shop_settings.crosssell.change_condition_logic);
		},
	
		change_condition_logic : function() 
		{			
			if ( $(this).hasClass("condition_logic_and") ) 
			{
				$(this).removeClass('condition_logic_and');
				$(this).addClass('condition_logic_or');	
				$(this).find('span').html(USAM_Page_shop_settings.text_or);
				$(this).find('input').val('OR');
			}
			else
			{
				$(this).removeClass('condition_logic_or');
				$(this).addClass('condition_logic_and');	
				$(this).find('span').html(USAM_Page_shop_settings.text_and);	
				$(this).find('input').val('AND');					
			}
		},
		
		event_select_check_type : function() 
		{
			var type = $(this).val();			
			var parent = $(this).closest('tr');
			
			parent.find(".check_blok").addClass('hidden').find('.condition_value').attr('disabled', true);
			parent.find("#check_"+type).removeClass('hidden').addClass('show').find('.condition_value').attr('disabled', false);			
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_crosssell', USAM_Page_shop_settings.crosssell.event_init);
	
	/**
	 * Уведомления о событиях
	 */
	USAM_Page_shop_settings.notification = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper
			        .delegate('#check_type', 'change', USAM_Page_shop_settings.notification.event_select_check_type)
					.delegate('#usam_conditions .condition-logic', 'click', USAM_Page_shop_settings.notification.change_condition_logic);
		},
	
		change_condition_logic : function() 
		{			
			if ( $(this).hasClass("condition_logic_and") ) 
			{
				$(this).removeClass('condition_logic_and');
				$(this).addClass('condition_logic_or');	
				$(this).find('span').html(USAM_Page_shop_settings.text_or);
				$(this).find('input').val('OR');
			}
			else
			{
				$(this).removeClass('condition_logic_or');
				$(this).addClass('condition_logic_and');	
				$(this).find('span').html(USAM_Page_shop_settings.text_and);	
				$(this).find('input').val('AND');					
			}
		},
		
		event_select_check_type : function() 
		{
			var type = $(this).val();			
			var parent = $(this).closest('tr');
			
			parent.find(".check_blok").addClass('hidden').find('.condition_value').attr('disabled', true);
			parent.find("#check_"+type).removeClass('hidden').addClass('show').find('.condition_value').attr('disabled', false);			
		},		
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_notification', USAM_Page_shop_settings.notification.event_init);
	
	/**
	 * Вкладка "Правила покупки"
	 * @since 3.8.8
	 */
	USAM_Page_shop_settings.purchase_rules = 
	{		
		event_init : function() 
		{				
			USAM_Page_shop_settings.purchase_rules.show_all_taxonomy_box();
			USAM_Page_shop_settings.wrapper.
				delegate('.inside'    , 'click'   , USAM_Page_shop_settings.purchase_rules.show_all_taxonomy_box);
		},		
		
		//Показать таксомании
		show_all_taxonomy_box : function() 
		{				
			var id = $("#type_search input:radio:checked").attr('id');	
			if ( id == 'type_search_group' )
			{	
				$("#all_taxonomy").removeClass('hidden').addClass('show');								
			}
			else
			{					
				$("#all_taxonomy").addClass('hidden').removeClass('show');			
			}		
		},
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_purchase_rules', USAM_Page_shop_settings.purchase_rules.event_init);
	
	/**
	 * Вкладка Главные
	 */
	USAM_Page_shop_settings.General = 
	{		
		event_init : function() 
		{			
			USAM_Page_shop_settings.wrapper.
			        delegate('.usam-select-all', 'click', USAM_Page_shop_settings.General.event_select_all).
			        delegate('.usam-select-none', 'click', USAM_Page_shop_settings.General.event_select_none);
		},

		/**
		 * Select all countries for Target Markets		
		 */
		event_select_all : function() {
			$('#usam-target-markets input:checkbox').each(function(){ this.checked = true; });
			return false;
		},

		/**
		 * Deselect all countries for Target Markets
		 * @since 3.8.8
		 */
		event_select_none : function() {
			$('#usam-target-markets input:checkbox').each(function(){ this.checked = false; });
			return false;
		},
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_general', USAM_Page_shop_settings.General.event_init);

	/**
	 * Вкладка презентация	
	 */
	USAM_Page_shop_settings.Presentation = {
		
		grid_view_boxes : ['usam-display-variations', 'usam-display-description', 'usam-display-add-to-cart', 'usam-display-more-details'],
		
		event_init : function() 
		{
			var checkbox_selector = '#' + USAM_Page_shop_settings.Presentation.grid_view_boxes.join(',#');
			USAM_Page_shop_settings.wrapper.delegate('#usam-show-images-only', 'click', USAM_Page_shop_settings.Presentation.event_show_images_only_clicked);
			USAM_Page_shop_settings.wrapper.delegate(checkbox_selector       , 'click', USAM_Page_shop_settings.Presentation.event_grid_view_boxes_clicked);
			USAM_Page_shop_settings.wrapper.delegate('#comment_system input', 'click', USAM_Page_shop_settings.Presentation.intensedebate_display);
			
			USAM_Page_shop_settings.Presentation.intensedebate_display();
		},
		
		intensedebate_display : function() 
		{	
			if ($("#usam_comment_system-integrated").is(':checked')) 
				$('#intensedebate_account_id').hide();
			else
				$('#intensedebate_account_id').show();
		},
		
		/**
		 * Deselect "Show Images Only" checkbox when any other Grid View checkboxes are selected
		 * @since 3.8.8
		 */
		event_grid_view_boxes_clicked : function() {
			document.getElementById('usam-show-images-only').checked = false;
		},

		/**
		 * Deselect all other Grid View checkboxes when "Show Images Only" is selected
		 * @since 3.8.8
		 */
		event_show_images_only_clicked : function() {
			var i;
			if ($(this).is(':checked')) 
			{
				for (i in USAM_Page_shop_settings.Presentation.grid_view_boxes) {
					document.getElementById(USAM_Page_shop_settings.Presentation.grid_view_boxes[i]).checked = false;
				}
			}
		}
	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_presentation', USAM_Page_shop_settings.Presentation.event_init);
	
	
	/**
	 * Список свойств заказа
	 */
	USAM_Page_shop_settings.property = 
	{
		event_init : function() 
		{
			USAM_Page_shop_settings.wrapper.
				                   delegate('#property_order_type', 'change', USAM_Page_shop_settings.property.event_property_order_type_changed);
			
			USAM_Page_shop_settings.property.property_order_type_changed( $("#property_order_type").val() );
		},	

		event_property_order_type_changed : function () 
		{
			USAM_Page_shop_settings.property.property_order_type_changed( $(this).val() );	
		},
		
		property_order_type_changed : function ( val ) 
		{			
			if ( val == 'select' || val == 'radio' || val == 'checkbox' )
			{
				$("#table_property_order_options").show();
			}		
			else
				$("#table_property_order_options").hide();			
		}

	};
	$(USAM_Page_shop_settings).bind('usam_tab_loaded_property', USAM_Page_shop_settings.property.event_init);	
	
})(jQuery);	
USAM_Page_shop_settings.init();



function hideelement1(id, item_value) {
	//alert(value);
	if(item_value == 5) {
		jQuery(document.getElementById(id)).css('display', 'block');
	} else {
		jQuery(document.getElementById(id)).css('display', 'none');
	}
}