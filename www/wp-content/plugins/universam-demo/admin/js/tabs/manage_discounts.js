/**
* Объект и функции для страницы "Управление скидками".  
*/
(function($)
{
	$.extend(USAM_Page_manage_discounts, 
	{		
		terms   : '',
		/**
		 * Обязательные события
		 * @since 3.8.8
		 */
		init : function() 
		{			
			$(function()
			{				
				USAM_Page_manage_discounts.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_manage_discounts).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);			
				$('.settings-error').insertAfter('.nav-tab-wrapper');				
			});
		},	
	});
	
	/**
	 * Вкладка "Корзина"	 	
	 */
	USAM_Page_manage_discounts.basket = {
		
		event_init : function() 
		{				
			USAM_Page_manage_discounts.wrapper.delegate('#discount_cart_type', 'change', USAM_Page_manage_discounts.basket.display_action_cart_discont)
											  .delegate('#add_gift', 'click', USAM_Page_manage_discounts.basket.add_gift)	
											  .delegate('#delete', 'click', USAM_Page_manage_discounts.basket.delete_gift);	
			
			
			USAM_Page_manage_discounts.basket.display_action_cart_discont();
		},	
		
		display_action_cart_discont : function() 
		{			
			var type = $('#discount_cart_type').val();		
			if ( type == 'g' )
			{
				$('#discount_cart-value').hide();
				$('#discount_cart-gift').show();
			}
			else
			{
				$('#discount_cart-value').show();
				$('#discount_cart-gift').hide();
			}							
		},	
		
		add_gift : function() 
		{		
			var product_id = $('#product_box #select_product_1').val();	
			if ( product_id == '' )
				return; 
			
			var sku = $('#product_box #select_product_1').data('product_sku');
			var product_title = $('#product_box #search_product_1').val();			
			var i = $('.gift_table td').length+1;
			var del = '<a class="button_delete" data-id="'+product_id+'"></a>';
				
			$('#items_empty').remove();	
			var html = '';
			html = "<td>"+i+"</td>";
			html = html+"<td>"+product_title+"</td>";
			html = html+"<td>"+sku+"</td>";
			html = html+"<td><input type='text' value='1' name='gift["+product_id+"]'></td>";
			html = html+"<td>"+del+"</td>";
			$('.gift_table').append('<tr>'+html+'<tr>');	
				
		},		
		
		delete_gift : function(e) 
		{			
			e.preventDefault();
			var id = $(this).data('id');	
			if ( id == '' )
				return; 
			
			var item_id = $(this).data('item-id');	
			if ( item_id == '' )
				return; 
			
			$(this).closest('tr').remove();					
		},		
	};
	$(USAM_Page_manage_discounts).bind('usam_tab_loaded_basket', USAM_Page_manage_discounts.basket.event_init);
		
		
	/**
	 * Вкладка "Товар дня"	 
	 * @since 3.8.8
	 */
	USAM_Page_manage_discounts.product_day = {
		
		event_init : function() 
		{			
			USAM_Page_manage_discounts.wrapper			
				.delegate('#add_product' , 'click'   , USAM_Page_manage_discounts.product_day.event_click_add_product)
				.delegate('input[name="refill"]' , 'change'   , USAM_Page_manage_discounts.product_day.event_click_auto_fill);
				
			if ( $('#refill_the_queue2').prop('checked') )
				$('#usam_auto_fill').hide();
			
			$('.table_products').sortable({
				stop : USAM_Page_manage_discounts.product_day.sortable_table,
				handle  : '.drag',
				axis    : 'y',
				items   : 'tr',
			});	
		},	
		
		sortable_table : function() 
		{				
				
        },		

		event_click_auto_fill : function() 
		{			
			$('#usam_auto_fill').toggle();
		},
		
		/**
		 * Добавить товар дня
		 * @since 3.8.8
		 */
		event_click_add_product : function() 
		{		
			var product_id = $('#product_box #select_product_1').val();	
			if ( product_id == '' )
				return false; 
					
			var table_products = $('.table_products');
			table_products.find('.items_empty').hide();	
			
			var new_product = table_products.find('.add_new').clone();
			var product_sku = $('#product_box #select_product_1').data('product_sku');
			var product_title = $('#product_box #search_product_1').val();			
			var i = table_products.find('tbody tr.item_row').length+1;
		
			new_product.attr('id','product_'+product_id);
			new_product.find('.product_n').html(i);
			new_product.find('.product_title').html(product_title);
			new_product.find('.product_sku').html(product_sku);	
			new_product.find('.product_discount input').attr('name','new_products['+product_id+'][value]');	
			new_product.find('.product_discount select').attr('name','new_products['+product_id+'][dtype]');
					
			new_product.appendTo(table_products).removeClass('add_new').addClass('item_row');		
		},		
	};
	$(USAM_Page_manage_discounts).bind('usam_tab_loaded_product_day', USAM_Page_manage_discounts.product_day.event_init);
	
	/**
	 * Вкладка "Купоны"	 
	 * @since 3.8.8
	 */
	USAM_Page_manage_discounts.coupons = {
		
		event_init : function() 
		{			
			USAM_Page_manage_discounts.wrapper.				
				delegate('.usam-editinline-link' , 'click'   , USAM_Page_manage_discounts.coupons.editor_copon).
				delegate('.button_delete',         'click'      , USAM_Page_manage_discounts.coupons.delete_condition).
				delegate('form#coupon_form'       , 'submit'  , USAM_Page_manage_discounts.coupons.add_coupon_submit);
		},
		
		add_coupon_submit : function() 
		{			
			var title = jQuery("#coupon_form input[name='coupon[add_coupon_code]']").val();
			if ( title == '') 
			{
				jQuery('<div id="notice" class="error"><p>' + USAM_Page_manage_discounts.empty_coupon + '</p></div>').insertAfter('div.wrap > h2').delay(2500).hide(350);
				return false;
			}		
		},	
		
		/**
		 * Показать быстрое редактирование товара
		 * @since 3.8.8
		 */
		editor_copon : function() 
		{		
			var parent = $(this).closest('tr'),
				target_row = parent.nextAll('.usam-coupon-editor-row').eq(0);
			target_row.show();
			parent.addClass('active');		
			return false;
		},		
		
		delete_condition : function(event) 
		{
			event.preventDefault();			
			jQuery(this).closest(".condition_coupon").remove();				
		},		
	};
	$(USAM_Page_manage_discounts).bind('usam_tab_loaded_coupons', USAM_Page_manage_discounts.coupons.event_init);
	
	
	USAM_Page_manage_discounts.accumulative = {
		
		event_init : function() 
		{			
			USAM_Page_manage_discounts.wrapper.delegate('#period', 'change', USAM_Page_manage_discounts.accumulative.change_period);				
			USAM_Page_manage_discounts.accumulative.change_period();
		},		
		
		change_period : function() 
		{			
			var type = $('#period').val();		
			if ( type == 'd' )
			{
				$('#period_from').hide();
				$('#interval').show();
			}
			else if( type == 'p' )
			{
				$('#period_from').show();
				$('#interval').hide();
			}	
			else
			{
				$('#period_from').hide();
				$('#interval').hide();
			}			
		},		
	};
	$(USAM_Page_manage_discounts).bind('usam_tab_loaded_accumulative', USAM_Page_manage_discounts.accumulative.event_init);
	
})(jQuery);	
USAM_Page_manage_discounts.init();