/**
* Объект и функции USAM_Page_customers.  
* Следующие свойства USAM_Page_customers были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
* - Механизм: одноразовый номер используется для проверки запроса для загрузки содержимого вкладок с помощью AJAX
*/
(function($)
{	
	$.extend(USAM_Page_customers, 
	{		
		unsaved_settings : false,

		/**
		 * Обязательные события для USAM_Page_customers
		 * @since 3.8.8
		 */
		init : function() 
		{					
			$(function()
			{	
				USAM_Page_customers.wrapper = $('.tab_'+Universam_Tabs.current_tab);			
				$(USAM_Page_customers).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);			
				$('.settings-error').insertAfter('.nav-tab-wrapper');
			}); 			
		}				
	});

	/**
	 * Вкладка "Накопительные скидки"	
	 * @since 3.8.8
	 */
	USAM_Page_customers.bonuses = 
	{		
		event_init : function() 
		{	
			//USAM_Page_customers.bonuses.wrapper = $('.tab_bonuses');			
			USAM_Page_customers.wrapper.			
				delegate('#action_row_bonus_save', 'click', USAM_Page_customers.bonuses.save_bonus);		
		},	

		/**
		 * Удалить уровень
		 * @since 3.8.8
		 */
		save_bonus : function() 
		{			
			var tr          = $(this).closest("tr"),
				spinner     = $(this).siblings('#ajax-loading');
			var post_data   = {
			    	action        : 'save_bonus',
			    	'id'          : $(this).attr('data-id'),	
					'status'      : tr.find("#bonus_status").val(),				
			    	 nonce        : USAM_Page_customers.save_bonus_nonce
			    },
			    ajax_callback = function(response)
				{
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}			    	
			    	spinner.toggleClass('ajax-loading-active');					
			//    	$('.column-discount #discount_terms-'+taxonomy_id).text(response.obj.content);
			    };				
			spinner.toggleClass('ajax-loading-active');
			$.usam_post(post_data, ajax_callback);			
			return false;		
		}			
	};
	$(USAM_Page_customers).bind('usam_tab_loaded_bonuses', USAM_Page_customers.bonuses.event_init);	
	
})(jQuery);	
USAM_Page_customers.init();