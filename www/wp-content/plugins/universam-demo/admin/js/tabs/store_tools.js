/**
* Объект и функции USAM_Page_store_tools.  
* Следующие свойства USAM_Page_store_tools были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_store_tools, 
	{		
		/**
		 * Обязательные события	
		 */
		init : function() 
		{					
			$(function()
			{					
				USAM_Page_store_tools.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_store_tools).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);			
			});
		},		
	});
	
	/**
	 * Вкладка "Редактирование название товара"	
	 * @since 3.8.8
	 */
	USAM_Page_store_tools.php = 
	{		
		event_init : function() 
		{		
			USAM_Page_store_tools.wrapper.
				delegate('#run_php_script' , 'click'   , USAM_Page_store_tools.php.run_php_script);
		},

		run_php_script : function() 
		{	
			var php_script    = $('#php_script').val(),					
				post_data   = {
			    	action         : 'run_php_script',
			    	'php_script'   : php_script,								
			    	 nonce         : USAM_Page_store_tools.run_php_script_nonce
			    },
			    ajax_callback = function(response)
				{							
					if (! response.is_successful) 
					{
			    		alert(response.error.messages.join("\n"));
			    		return;
			    	}				
					jQuery('.usam_php_result').html(response.obj);		
			    };					
			$.usam_post(post_data, ajax_callback);					
		},	
	};
	$(USAM_Page_store_tools).bind('usam_tab_loaded_php', USAM_Page_store_tools.php.event_init);	
})(jQuery);	
USAM_Page_store_tools.init();