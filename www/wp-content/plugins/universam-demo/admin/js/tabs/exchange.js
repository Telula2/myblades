/**
* Объект и функции USAM_Page_exchange.  
* Следующие свойства USAM_Page_exchange были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_exchange, 
	{			
		init : function() 
		{					
			$(function()
			{					
				USAM_Page_exchange.wrapper = $('.tab_'+Universam_Tabs.current_tab);				
				$(USAM_Page_exchange).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
			});
		},		
	});
	
	/**
	 * Вкладка "FTP"		
	 */
	USAM_Page_exchange.ftp = 
	{		
		event_init : function() 
		{				
			USAM_Page_exchange.ftp.load();
		},		
		
		load : function() 
		{			
			if ( $("#file_purchase_prices").val() == '' )
			{
				$("#upload_purchase_price").attr('disabled','disabled');			
			}
			if ( $("#upload_transactions").val() == '' )
			{
				$("#upload_transactions_submit").attr('disabled','disabled');			
			}
		}
	};
	$(USAM_Page_exchange).bind('usam_tab_loaded_ftp', USAM_Page_exchange.ftp.event_init);	
	
})(jQuery);	
USAM_Page_exchange.init();