/**
* Объект и функции Universam_Tabs.  
* Следующие свойства Universam_Tabs были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
* - Механизм: одноразовый номер используется для проверки запроса для загрузки содержимого вкладок с помощью AJAX
*/
(function($)
{
	$.extend(Universam_Tabs, 
	{
		unsaved_settings : false,	
		/**
		 * Обязательные события для Universam_Tabs
		 * @since 5.0
		 */
		init : function() 
		{	
			$.event.props.push('state');				
			if (history.replaceState) {
				(function(){
					history.replaceState({url : location.search + location.hash}, '', location.search + location.hash);
				})();
			}			
			$(window).bind('popstate', Universam_Tabs.event_pop_state);
			$(function()
			{				
				Universam_Tabs.wrapper = $('#tab_'+Universam_Tabs.current_tab);				
				// отключил не правильно работают запросы форм	
				$('#usam_page_tabs1').delegate('a.nav-tab'              , 'click' , Universam_Tabs.event_tab_button_clicked).
				                     delegate('input, textarea, select', 'change', Universam_Tabs.event_changed).									 
				                     delegate('#usam-manage_discounts-form'    , 'submit', Universam_Tabs.event_form_submitted);	

				Universam_Tabs.wrapper.						
					delegate('.table_rate .add'              , 'click'   , Universam_Tabs.event_add_table_rate_layer).
					delegate('.table_rate .delete'           , 'click'   , Universam_Tabs.event_delete_table_rate_layer).			
					delegate('.table_rate input[type="text"]', 'keypress', Universam_Tabs.event_enter_key_pressed).

					delegate('table tr td .button_delete'    , 'click'   , Universam_Tabs.event_delete_table_row).
					delegate('table tr td #delete'           , 'click'   , Universam_Tabs.event_delete_table_row).
					delegate('.button_radio', 'click', Universam_Tabs.select_date).
					delegate('#open_send_email', 'click', Universam_Tabs.send_email).
					delegate('.usam_edits .usam_display_data', 'click', Universam_Tabs.open_edit)						
					
				$('.table_products')
					.delegate('#product_discount', 'change', Universam_Tabs.event_change_discount);					
									 
									 
				$('#usam_page_tabs .nav-tab-wrapper ul').delegate('li', 'mouseenter' , Universam_Tabs.show_subtabs_menu)
													    .delegate('li', 'mouseleave' , Universam_Tabs.hide_subtabs_menu);
		
		
				$('#usam_page_tab')	
					.delegate('#add_button', 'click', Universam_Tabs.add_item_event)	
					.delegate('.change_field', 'click', Universam_Tabs.field_change_event); // Изменить поле				
								
				$('.usam_box').delegate('.handlediv', 'click', Universam_Tabs.toggle_usam_box); // Скрыть показать
				
				$('.usam_box').delegate('#button_toggle', 'click', Universam_Tabs.container_section_toggle)// Скрыть показать
						      .delegate('#button_edit', 'click', Universam_Tabs.container_section_edit_click)
							  .delegate('#bitton_delete', 'click', Universam_Tabs.container_section_delete);	
				
				$('#usam_table_filter_form').delegate('#filter_date_from_calendar_period', 'change', Universam_Tabs.calendar_date_filter_select);	
		
				Universam_Tabs.calendar_date_filter_select();													
												
				$('.usam_box .hndle').delegate('a.edit', 'click', Universam_Tabs.click_edit_data);						
				
				$(window).bind('beforeunload', Universam_Tabs.event_before_unload);
				$(Universam_Tabs).trigger('usam_tab_loaded');					
				$('.settings-error').insertAfter('.nav-tab-wrapper');
				
				$('.inside').delegate('.show_help' , 'click' , Universam_Tabs.show_help_setting_box);	
							
				Universam_Tabs.hide_edit_data();									
				Universam_Tabs.loading_help_setting();			
				
				$('.menu_fixed_right').hcSticky({						
					stickTo: 'document',
					top    : 80,
				});		
				$('.toolbar').hcSticky({						
					stickTo: 'document',
					top    : 20,
				});		
				$('body').delegate('#usam_remind', 'change', Universam_Tabs.event_task_remind_clicked);		

				$('.tab-content').delegate('form', 'submit', Universam_Tabs.form_submit);		

				$('.usam_menu').delegate('.menu_name', 'click', Universam_Tabs.select_name_menu);
				$(document).click(Universam_Tabs.close_menu);
				
				$('.taxonomy_box h3').delegate('input', 'change', Universam_Tabs.checked_all);
				
				$('#usam_comments')
					.delegate('#add_comment', 'click', Universam_Tabs.add_comment)					
					.delegate('#edit_comment', 'click', Universam_Tabs.edit_comment)
					.delegate('.js_delete_action', 'click', Universam_Tabs.open_confirm_comment);	
				
				$('#operation_confirm')
					.delegate('#delete_comment', 'click', Universam_Tabs.delete_comment);					
			});
		},			
		
		open_confirm_comment : function(e) 
		{ 			
			e.preventDefault();		
			var comment_id = $(this).parents('.comment-box').data('id');	
			$('#operation_confirm .action_confirm').attr("id",'delete_comment').attr("data-id",comment_id);			
			$('#operation_confirm').modal();				
		},		
		
		delete_comment : function()
		{ 			
			$(this).attr("id",'');							
			var	comment_id = $(this).data("id");				
			var $comment = $('.comments [data-id='+comment_id+']');				
			var post_data = {
					nonce               : Universam_Tabs.delete_comment_nonce,
					action              : 'delete_comment',					
					id                  : comment_id,	
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
				$comment.remove();
			};		
			$.usam_post(post_data, ajax_callback);
		},
		
		edit_comment : function()
		{ 
			var $comment = $(this).parents('.comment-box');	
			var message = $comment.find('.event_comment_input').val();			
			if ( message == '' )
				return; 
			$comment.find('.comment_message').html(message);
			$comment.find('.usam_display_data').show();
			$comment.find('.usam_edit_data').hide().find('textarea').prop("disabled", true);			
			
			$comment.find('.js_hide_button').show();	
			var post_data = {
					nonce               : Universam_Tabs.edit_comment_nonce,
					action              : 'edit_comment',					
					id                  : $comment.data('id'),
					message             : message,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
			};		
			$.usam_post(post_data, ajax_callback);
		},
		
		add_comment : function()
		{ 
			var $comment = $(this).siblings('.comment_message_box').find('textarea');	
			var message = $comment.val();	
			if ( message == '' )
				return; 
			
			var type='';
			switch ( Universam_Tabs.current_page ) 
			{				
				case 'orders':							
					type = 'order';  
				break;	
				case 'crm':							
					type = 'event';  
				break;	
				default:				
					return;
			}		
			$comment.val('');			
			var post_data = {
					nonce               : Universam_Tabs.add_comment_nonce,
					action              : 'add_comment',					
					id                  : Universam_Tabs.id,
					type                : type,
					message             : message,					
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');		
			var ajax_callback = function(response) 
			{					
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}				
				if ( response.obj != false ) 
				{					
					var comment_message_html = $('.new_comment .comment_message_box').html();				
					var comment_html = "<div class='comment-box'><div class='comment_header'><span class='comment_author'>"+response.obj.author+"</span><span class='comment_date'>"+response.obj.date+"</span><a class='js_delete_action'></a></div><div class='comment_message usam_display_data'>"+response.obj.message+"</div><div class='usam_edit_data'><div class='comment_message_box'>"+comment_message_html+"</div><button id='edit_comment' type='button' class='button'>"+Universam_Tabs.edit_button_text+"</button></div></div>";
					
					var $comment = $(comment_html).data('id', response.obj.id);	
					$comment.find('.event_comment_input').val(response.obj.message);						
					$comment.appendTo(".comments");
				}	
			};		
			$.usam_post(post_data, ajax_callback);
		},
		
		send_email : function(e) 
		{ 
			e.preventDefault();				
			var send_mail_modal = $('#send_mail');
			send_mail_modal.modal();	
			
			send_mail_modal.on('shown.bs.modal', function (event)
			{ 
				var window = send_mail_modal.find('.modal-body');			
				var body_height = $("body").height();
				var window_top = document.getElementById("send_mail").offsetTop+20;
				var window_height = window_top + window.height();
				if ( window_height > body_height )
				{
					var limit = body_height-window_top*2;
					window.css({"max-height":limit, "overflow-y":"auto"}); 
				}
			});
		},
												
		checked_all : function( e ) 
		{					
			var checked = $(this).is(':checked');				
			$(this).parents('.taxonomy_box').find('.categorydiv input[type="checkbox"]').attr('checked',checked);
		},
		
		select_date : function( e ) 
		{		
			e.preventDefault();
			$('.date_interval_toolbar .button_radio_control').attr('checked',false);
			
			var input = $(this).find('.button_radio_control');			
			input.attr('checked',true);				
			
			$(".button_radio").removeClass('button_radio_checked_yes');
			$(this).addClass('button_radio_checked_yes');			
			
			msInDay = 86400000;  // миллисекунд в сутках
			var date_end = currentDate = new Date; 
			
		//	var date_start;
	//		var date_end; 
		
			switch ( input.val() ) 
			{				
				case 'today':							
					date_start = currentDate;  // текущая дата		
				break;		
				case 'yesterday':							
					date_start = date_end = new Date(currentDate - msInDay); 
				break;		
				case 'week':							
					date_start = new Date(currentDate - (7 * msInDay)); 
					
				/*	var prevWeekDate = new Date(currentDate - (7 * msInDay)); // дата на прошлой неделе							
					Date.prototype.getWeekDay = function() {
  var day = this.getDay();
  if(day==0) return 7;
  else return day;  }

// Returns current week start date
Date.prototype.getWeekStartDate = function() {
  var date = new Date(this.getTime());      
  date.setDate(this.getDate()-(this.getWeekDay()-1));
  return date;     
}
// Returns current week end date
Date.prototype.getWeekEndDate = function() {
  var date = new Date(this.getTime());      
  date.setDate(this.getDate()+(7-this.getWeekDay()));
  return date;     
}										
					date_start = prevWeekDate.getWeekStartDate();  // дата понедельника прошлой недели
					date_end = prevWeekDate.getWeekEndDate();  // дата воскресенья прошлой недели		
*/					
				break;	
				case 'month':							
					date_start = new Date(currentDate - (30 * msInDay)); 					
				break;			
				case 'year':							
					date_start = new Date(currentDate - (365 * msInDay)); 
				break;	
			}			
			var date_start_day = date_start.getDate();
			if ( date_start_day < 10 )
				date_start_day = "0"+date_start_day;
			
			var date_start_month = date_start.getMonth()+1;
			if ( date_start_month < 10 )
				date_start_month = "0"+date_start_month;
			
			var date_end_month = date_end.getMonth()+1;
			if ( date_end_month < 10 )
				date_end_month = "0"+date_end_month;
		
			var date_end_day = date_end.getDate();
			if ( date_end_day < 10 )
				date_end_day = "0"+date_end_day;
			
			var date_from = date_start_day+"-"+date_start_month+"-"+date_start.getFullYear();
			var date_to = date_end_day+"-"+date_end_month+"-"+date_end.getFullYear();
		
			$('#date_from_interval').val(date_from);
			$('#date_to_interval').val(date_to);	
						
			$(".date_interval_toolbar").parents('form').submit();
		},	
		
		select_name_menu : function() 
		{					
			$('.menu_content').removeClass('select_menu');	
			$(this).siblings('.menu_content').toggleClass('select_menu');			
		},
		
		close_menu : function( event ) 
		{				
			if ( $(event.target).hasClass("menu_content") ) 
				return;	
			if ( $(event.target).hasClass("menu_name") ) 
				return;
			$('.menu_content').removeClass('select_menu');	
		},
		
		form_submit : function() 
		{				
			var j = 0;
			$('.required').each(function(i,elem) 
			{
				if ( $(this).val() == '' )
				{								
					j++;					
					$(this).addClass('highlight');					
				}
				else
					$(this).removeClass('highlight');				
			});				
			if ( j > 0 )
			{
				$('html, body').animate({ scrollTop: $(".highlight").offset().top }, 500); 
				return false;
			}			
			return true;
		},		
		
		event_change_discount : function() 
		{				
			$('.table_products .product_discount input').val( $(this).val() );			
		},	
				
		event_task_remind_clicked : function() 
		{					
			var date_remind = $(this).siblings('#date_remind');
			if ( $(this).prop('checked') )
			{
				date_remind.removeClass('hide');
			}
			else
			{
				date_remind.addClass('hide');
			}
		},	
				
		// фильтр по дате
		calendar_date_filter_select : function() 
		{						
			var calendar_period = $("#filter_date_from_calendar_period").val();		
			var direction = $('.usam_filter-box-sizing').find('span.usam_calendar-direction');		
			var weekday = $('.usam_filter-box-sizing').find('span.usam_calendar-weekday');		
			var hour = $('.usam_filter-box-sizing').find('span.usam_calendar-hour');	
			var first = $('.usam_filter-box-sizing').find('.usam_calendar-first');	
			var separate = $('.usam_filter-box-sizing').find('span.usam_calendar-separate');				
			var second = $('.usam_filter-box-sizing').find('.usam_calendar-second');	
		
			switch ( calendar_period ) 
			{
				case 'day':	
				case 'week':
				case 'month':
				case 'quarter':
				case 'year':
					direction.show();
					first.hide();
					weekday.hide();
					separate.hide();
					second.hide();	
					hour.hide();					
				break;
				case 'hour':
					hour.show();
					direction.hide();
					first.hide();
					weekday.hide();
					separate.hide();
					second.hide();
				break;
				case 'exact':	
				case 'before':
				case 'after':
					direction.hide();
					first.show();
					separate.hide();
					second.hide();
					weekday.hide();
					hour.hide();	
				break;
				case 'interval':				
					direction.hide();
					first.show();
					separate.show();
					second.show();
					weekday.hide();
					hour.hide();	
				break;
				case 'weekday':				
					direction.hide();
					first.hide();
					separate.hide();
					second.hide();
					weekday.show();					
					hour.hide();	
				break;
				default:				
					direction.hide();
					first.hide();
					separate.hide();
					second.hide();
					weekday.hide();
					hour.hide();	
				break;
			}
		},		
		
		container_section_edit_click : function(e) 
		{			
			e.preventDefault();
			var p = $(this).closest(".usam_document_container");	
			Universam_Tabs.container_section_edit( p );
		},			
		
		close_edit : function(e)
		{ 						 				
			e.preventDefault();	
			if ( $(event.target).hasClass("usam_display_data") ) 
				return;	
		
			if ( $(event.target).hasClass("usam_edit_data") ) 
				return;	

			if ( $(event.target).parents().hasClass("usam_edit_data") ) 
				return;				
			
			$('.usam_edits .usam_edit_data').hide();	
			$('.usam_edits .usam_display_data').show();	
			$(document).off('click', Universam_Tabs.close_edit);
		},	

		open_edit : function(e)
		{ 
			$(document).click(Universam_Tabs.close_edit);
			
			e.preventDefault();	
			var $edits = $(this).parents('.usam_edits');
			$edits.find('.usam_edit_data').hide();
			$edits.find('.usam_display_data').show();
			$(this).hide();		
			$(this).siblings('.usam_edit_data').show().find('textarea').prop("disabled", false);			
		},	
		
		container_section_edit : function( p ) 
		{									
			p.find('.usam_display_data').hide();
			p.find('.usam_edit_data').show();			
			
			p.find('select').removeAttr("disabled");
			p.find('textarea').prop("disabled", false);
			p.find('input').prop("disabled", false);
			
			p.find(".chzn-select").chosen();	
			p.find(".chzn-select-nosearch-load").chosen({disable_search_threshold: 10});
		},

		container_section_delete : function(e) 
		{			
			$(this).closest('.usam_document_container').remove();	
		},		
		
		container_section_toggle : function() 
		{			
			var p = $(this).closest('.usam_document_container');		
			p.find('.usam_document_container-content').toggle();
		},
		
		toggle_usam_box : function() 
		{				
			var t = $(this).parents('.usam_box');	
			if ( t.hasClass('closed') ) 
			{			
				hidden = 0;
			}
			else
			{
				hidden = 1;
			}			
			t.toggleClass("closed");	
			var post_data = {
					nonce               : Universam_Tabs.save_nav_menu_metaboxes_nonce,
					action              : 'save_nav_menu_metaboxes',					
					id                  : t.attr('id'),	
					hidden              : hidden,
				},
				spinner = $(this).siblings('#ajax-loading');					
			spinner.addClass('ajax-loading-active');			
			var ajax_callback = function(response) 
			{			
				spinner.removeClass('ajax-loading-active');				
				if (! response.is_successful) 
				{
					alert(response.error.messages.join("\n"));
					return;
				}
			};		
			$.usam_post(post_data, ajax_callback);			
		},
		
		click_edit_data : function(e) 
		{			
			e.preventDefault();		
			
			var p = $(this).closest('.postbox');		

			if( ! p.hasClass('js_action_edit_data') )
			{			
				p.addClass('js_action_edit_data');		
				
				p.find('.usam_display_data').hide();
				p.find('.usam_edit_data').show();			
						
				p.find('select').prop("disabled", false);
				p.find('input').prop("disabled", false);
				p.find('textarea').prop("disabled", false);
			
				p.find(".chzn-select").chosen();				
//chosen-container				
				p.find(".chzn-select-nosearch-load").chosen({disable_search_threshold: 10});
			}
		},
				
		/**
		 * Обновить зебру
		 */
		refresh_alt_row : function( this_table ) 
		{			
			this_table.find('.alternate').removeClass('alternate');
			this_table.find('tr:odd').addClass('alternate');
		},
		
		/**
		 * Обработка нажатия кнопки ENTER			
		 */
		event_enter_key_pressed : function(e) 
		{
			var code = e.keyCode ? e.keyCode : e.which;
			if (code == 13) {
				var add_button = $(this).siblings('.actions').find('.add');
				if (add_button.length > 0) {
					add_button.trigger('click', [true]);
				} else {
					$(this).closest('td').siblings('td').find('input').focus();
				}
				e.preventDefault();
			}
		},

		/**
		 * Добавить уровень
		 */
		event_add_table_rate_layer : function(e, focus_on_new_row) 
		{ 
			if (typeof focus_on_new_row === 'undefined') {
				focus_on_new_row = false;
			}
			var this_row = $(this).closest('tr'),
			    clone = this_row.clone();
			var this_table = this_row.closest('table');	

			clone.find('input').val('');
			clone.find('.cell-wrapper').hide();
			clone.insertAfter(this_row).find('.cell-wrapper').slideDown(150, function() {
				if (focus_on_new_row) {
					clone.find('input').eq(0).focus();
				}
			});
			Universam_Tabs.refresh_alt_row( this_table );
			return false;
		},

		/**
		 * Удалить уровень
		 */
		event_delete_table_rate_layer : function( e ) 
		{			
			e.preventDefault();				
			var this_row = $(this).closest('tr');	
			var this_table = this_row.closest('table');			
			if ( this_table.find('tr:not(.js-warning)').length == 1) 
			{
				this_row.find('input').val('');
				this_row.fadeOut(150, function(){ $(this).fadeIn(150); } );
			} 
			else if ( this_table.find('tr').length > 2)
			{			
				this_row.find('.cell-wrapper').slideUp(150, function()
				{
					this_row.remove();
					Universam_Tabs.refresh_alt_row( this_table );
				});
			}
			return false;
		},	

		/**
		 * Удалить строку таблицы
		 */
		event_delete_table_row : function( e ) 
		{			
			e.preventDefault();				
			var this_row = $(this).closest('tr');	
			this_row.remove();
		},		
	
		
		show_subtabs_menu : function() 
		{			
			$(this).children('ul').show();			
			$(this).children('dl').show();		
		},
		
		hide_subtabs_menu : function() 
		{				
			$(this).children('ul').hide();	
			$(this).children('dl').hide();		
		},
		
		hide_edit_data : function() 
		{				
			$('.usam_edit_data').hide();
			$('.usam_display_data').show();			
					
			$('.usam_edit_data').find('select').prop("disabled", true);
			$('.usam_edit_data').find('input').prop("disabled", true);
			$('.usam_edit_data').find('textarea').prop("disabled", true);	

			$('.usam_edit_data').prop("disabled", true);
			$('.usam_edit_data').prop("disabled", true);
			$('.usam_edit_data').prop("disabled", true);			
		},
		
		field_change_event : function(e) 
		{		
			e.preventDefault();
			var p = $(this).closest(".usam_box");			
			Universam_Tabs.field_change( p );
		},
						
		field_change : function( p ) 
		{					
			p.find('.usam_display_data').hide();
			p.find('.usam_edit_data').show();			
			
			p.find('select').prop("disabled", false);
			p.find('textarea').prop("disabled", false);
			p.find('input').prop("disabled", false);
		},
		
		add_item_event : function( ) 
		{					
			var div = $(this).closest('.inside');	
			var set_row_element = div.find('.set_row_element');	
			var clone_row = div.find(".add_new").clone();
			clone_row.appendTo(set_row_element).removeClass('add_new');
			div.find(".set_row_element .items_empty ").hide();
			
			Universam_Tabs.field_change( clone_row );
		},	
		
		//Загрузить опции помощи
		loading_help_setting : function() 
		{					
			var id = '';
			$(".box_help_setting").addClass('hidden');
			$('.show_help').each(function(i,elem) 
			{
				if ( $(this).prop("checked") )
				{
					id = $(this).attr('id');					
					$(".box_help_setting#"+id).removeClass('hidden');
					$(".box_help_setting#"+id).addClass('show');
				}
				
			});
		},
		
		//Показать описание опции
		show_help_setting_box : function() 
		{					
			var box = $(this).parents('.inside');
			var id = $(this).attr('id');	
			
			box.find(".box_help_setting").addClass('hidden');
			box.find(".box_help_setting#"+id).removeClass('hidden');
			box.find(".box_help_setting#"+id).addClass('show');
		},	
		
		/**
		 * This prevents the confirm dialog triggered by event_before_unload from being displayed.
		 */
		event_form_submitted : function() {
			Universam_Tabs.unsaved_settings = false;
		},

		/**
		 * Mark the page as "unsaved" when a field is modified
		 */
		event_changed : function() 
		{
			Universam_Tabs.unsaved_settings = true;
		},

		/**
		 * Display a confirm dialog when the user is trying to navigate away with unsaved settings
		 */
		event_before_unload : function() 
		{
			if (Universam_Tabs.unsaved_settings) {
				return Universam_Tabs.before_unload_dialog;
			}
		},

		/**
		 * Загрузите вкладку настроек, когда кнопка вкладки нажата
		 */
		event_tab_button_clicked : function()
		{		
			var href = $(this).attr('href');
			Universam_Tabs.load_tab(href);
			return false;
		},

		/**
		 * Когда нажата кнопка вперед / назад в браузере загрузить нужную вкладку	
		 */
		event_pop_state : function(e) 
		{			
			if (e.state) {
				Universam_Tabs.load_tab(e.state.url, false);
			}
		},

		/**
		 * Display a small spinning wheel when loading a tab via AJAX	
		 */
		toggle_ajax_state : function(tab_id) 
		{
			var tab_button = $('a[data-tab-id="' + tab_id + '"]');
			tab_button.toggleClass('nav-tab-loading');
		},

		/**
		 * Используя AJAX загрузить вкладку на страницу настроек. Если имеются не сохраненные настройки в текущей вкладке, Диалог подтверждения будет отображаться.		 
		 */
		load_tab : function(url, push_state) 
		{
			if (Universam_Tabs.unsaved_settings && ! confirm(Universam_Tabs.ajax_navigate_confirm_dialog)) {
				return;
			}
			if (typeof push_state == 'undefined') {
				push_state = true;
			}
			var query = $.query.load(url);
			var tab_id = query.get('tab');
			var post_data = $.extend({}, query.get(), {
				'action'      : 'navigate_tab',
				'nonce'       : Universam_Tabs.navigate_tab_nonce,
				'current_url' : location.href,
				'page'        : Universam_Tabs.current_page,				
				'tab'         : tab_id
			});		
			var spinner = $('#usam-page-tabs-title #ajax-loading');

			spinner.addClass('ajax-loading-active');
			Universam_Tabs.toggle_ajax_state(tab_id);

			// PushState, чтобы сохранить эту загрузку страницы в историю, и изменить поле адреса браузера
			if (push_state && history.pushState) {
				history.pushState({'url' : url}, '', url);
			}
			/**
			 * Замените содержимое вкладки на вариант с ответом AJAX, а также изменить URL в форме и переключаться активную вкладку.
			 */
			var ajax_callback = function(response) 
			{
				if ( response.obj.error != '' ) {
					alert(response.obj.error.join("\n"));
					return;
				}
				if (! response.is_successful) {
					alert(response.error.messages.join("\n"));
					return;
				}					
				var t = Universam_Tabs;				
				t.unsaved_settings = false;
				t.toggle_ajax_state(tab_id);
				$('#tab_' + Universam_Tabs.current_tab).replaceWith(response.obj.content);				
				Universam_Tabs.current_tab = tab_id;
				$('.settings-error').remove();
				$('.nav-tab-active').removeClass('nav-tab-active');
				$('[data-tab-id="' + tab_id + '"]').addClass('nav-tab-active');
				$('#usam_tab_page form').attr('action', url);
				$(t).trigger('usam_tab_loaded');
				$(t).trigger('usam_tab_loaded_' + tab_id);
				spinner.removeClass('ajax-loading-active');
			};		
			$.usam_post(post_data, ajax_callback);		
		}
	});	
})(jQuery);	
Universam_Tabs.init();