/**
* Объект и функции USAM_Page_orders.  
* Следующие свойства USAM_Page_orders были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_orders, 
	{		
		/**
		 * Обязательные события		 
		 */
		init : function() 
		{					
			$(function()
			{					
				USAM_Page_orders.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_orders).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
				
				USAM_Page_orders.wrapper.delegate('#document_edit', 'click', USAM_Page_orders.document_edit);							
			});
		},	
		
		document_edit : function( e ) 
		{			
			e.preventDefault();
			var p = $(this).parents('.usam_document_container');			
			p.find('.usam_display_data').hide();
			p.find('.usam_edit_data').show();			
					
			p.find('select').prop("disabled", false);
			p.find('textarea').prop("disabled", false);
			p.find('input').prop("disabled", false);
			p.find('.usam_document_container-content').show();
		},		
	});	
	
	/**
	 * Вкладка "Заказы"	
	 */
	USAM_Page_orders.orders = 
	{		
		event_init : function() 
		{					
			USAM_Page_orders.wrapper.delegate('.usam-order-status', 'change', USAM_Page_orders.orders.event_order_status_change);	
		},
							
		event_order_status_change : function() 
		{
			var post_data = {
					nonce      : USAM_Page_orders.change_purchase_log_status_nonce,
					action     : 'change_purchase_log_status',
					order_id   : $(this).data('log-id'),
					new_status : $(this).val(),				
					status     : USAM_Page_orders.current_view,
					paged      : USAM_Page_orders.current_page,
					_wp_http_referer : window.location.href
				},
				spinner = $(this).siblings('#ajax-loading'),
				t = $(this);			
			spinner.addClass('ajax-loading-active');
			var ajax_callback = function(response)
			{	
				if (! response.is_successful)
				{
					alert(response.error.messages.join("\n"));
					return;
				}	
				spinner.removeClass('ajax-loading-active');
				$('ul.subsubsub').replaceWith(response.obj.views);
				$('.tablenav.top').replaceWith(response.obj.tablenav_top);
				$('.tablenav.bottom').replaceWith(response.obj.tablenav_bottom);
			};
			$.usam_post(post_data, ajax_callback);
		},
	};
	$(USAM_Page_orders).bind('usam_tab_loaded_orders', USAM_Page_orders.orders.event_init);	
	
	/**
	 * Вкладка "Оплата"	
	 */
	USAM_Page_orders.payment =
	{		
		reset_textbox_width : true,
		blur_timeout : null,
		event_init : function() 
		{					
			USAM_Page_orders.wrapper.delegate('#external_document', 'focus', USAM_Page_orders.payment.event_tracking_id_focused).
				                     delegate('#external_document', 'blur', USAM_Page_orders.payment.event_tracking_id_blurred).
				                     delegate('.column-external_document a.save', 'click', USAM_Page_orders.payment.save_external_document).
				                     delegate('.column-external_document a.save', 'mousedown', USAM_Page_orders.payment.event_disable_textbox_resize).
				                     delegate('.column-external_document a.save', 'focus', USAM_Page_orders.payment.event_disable_textbox_resize);
									
			USAM_Page_orders.payment.default_message_input ( USAM_Page_orders.message_payment );			
		},		
				
		default_message_input : function( message ) 
		{		
			var field_box = $("table #external_document");		
			field_box.each( function()
			{	
				if ( $(this).val() == "" )
				{			
					$(this).css("color", "#A9A9A9").val( message );		
				}
				else
				{				
					$(this).css("color", "black");					
				}	
			});
			field_box.focus(function()
			{ 			
				jQuery(this).css("color", "black");			
				if (jQuery(this).val() == message )
				{
					jQuery(this).val("");
				}	
			})
			.blur(function(){ 					// - установим обработчик потери фокуса		
				jQuery(this).css("color", "#A9A9A9");
				if ( jQuery(this).val() == ""){
					jQuery(this).val( message );
				}	
			});				
		},			
		
		// Сохранение номера документа
		save_external_document : function( e ) 
		{			
			var t = $(this), 
			textbox = t.siblings('#external_document'), 
			spinner = t.siblings('#ajax-loading'),
			document_number = textbox.val(),
			$div = t.parent();
			if ( document_number != USAM_Page_orders.message_payment )
			{ 
				var post_data = {
					'action'     : 'payment_save_external_document',
					'document_number' : document_number,				
					'payment_id' : $div.data('payment-id'),
					'nonce'      : USAM_Page_orders.payment_save_external_document_nonce
				};			
				var ajax_callback = function(response) 
				{					
					spinner.toggleClass('ajax-loading-active');
					textbox.blur();
					if (! response.is_successful) {
						alert(response.error.messages.join("\n"));
						return;
					}				
					$div.text( document_number );
				};
				t.hide();
				spinner.toggleClass('ajax-loading-active');
				$.usam_post(post_data, ajax_callback);
			}
			return false;
		},		

		event_disable_textbox_resize : function() {
			USAM_Page_orders.payment.reset_textbox_width = false;
		},
		
		reset_tracking_id_width : function(t) 
		{
			var reset_width = function() 
			{
				if (USAM_Page_orders.payment.reset_textbox_width) 
				{
					t.siblings('a.save').hide();
					t.width('');
					if (t.val() === '') {
						t.siblings('.add').show();
					}
				}
				USAM_Page_orders.payment.reset_textbox_width = true;
			};
			USAM_Page_orders.payment.blur_timeout = setTimeout(reset_width, 100);
		},

		event_tracking_id_blurred : function() 
		{			
			var t = $(this);
			USAM_Page_orders.payment.reset_tracking_id_width(t);
		},

		event_tracking_id_focused : function() 
		{				
			$(this).siblings('a.save').show();
			$(this).siblings('a.add').hide();			
		},		
	};
	$(USAM_Page_orders).bind('usam_tab_loaded_payment', USAM_Page_orders.payment.event_init);	
})(jQuery);	
USAM_Page_orders.init();