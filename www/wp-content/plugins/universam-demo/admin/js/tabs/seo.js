/**
* Объект и функции USAM_Page_seo.  
* Следующие свойства USAM_Page_seo были установлены wp_localize_script ():   
* - Current_tab: Идентификатор активной вкладки
*/

(function($)
{
	$.extend(USAM_Page_seo, 
	{		
		/**
		 * Обязательные события
		 */
		init : function() 
		{					
			$(function()
			{						
				USAM_Page_seo.wrapper = $('.tab_'+Universam_Tabs.current_tab);						
				$(USAM_Page_seo).trigger('usam_tab_loaded_' + Universam_Tabs.current_tab);	
			});
		},		
		
		display_graph : function( data, name_graph ) 
		{				
			var width_graph = jQuery('.usam_tab_table').width();
					
			var margin = {top: 40, right: 10, bottom: 40, left: 70},
				width = width_graph - margin.left - margin.right,
				height = 500 - margin.top - margin.bottom;

			var x = d3.scale.ordinal()
				.rangeRoundBands([0, width], .1, .3);		
				
			var y = d3.scale.linear()
				.range([height, 0]);

			var xAxis = d3.svg.axis()
				.scale(x)
				.orient("bottom")
				.ticks(5);
		
			var yAxis = d3.svg.axis()
				.scale(y)
				.orient("left")
				.ticks(5, "");

			var svg = d3.select("#graph")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)					
			    .append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");			

			var xScale = x.domain(data.map(function(d) { return d.date; }));
			var yScale = y.domain([0, d3.max(data, function(d) { return d.x_data; })]);

			svg.append("text")
				  .attr("class", "title")
				  .attr("x", -70)
				  .attr("y", -26)				  
				  .text( name_graph );

			svg.append("g")
				.attr("class", "x axis")			  
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis)
				.selectAll(".tick text");
	// вывод значений y
			svg.append("g")
				.attr("class", "y axis")				  
				.call(yAxis);

				  // создаем набор вертикальных линий для сетки   
			d3.selectAll("g.x g.tick")
				.append("line") // добавляем линию
				.classed("grid-line", true) // добавляем класс	
				//	.attr("style", 'stroke: #cccccc')
				.attr("x1", 0)
				.attr("y1", 0)
				.attr("x2", 0)
				.attr("y2", - (height));
			 
			// рисуем горизонтальные линии 
			d3.selectAll("g.y g.tick")
				.append("line")   
				.classed("grid-line", true)
				.attr("x1", 0)
				.attr("y1", 0)
				.attr("x2", width)
				.attr("y2", 0);			
			// функция, создающая по массиву точек линии
			var line = d3.svg.line()
				.x(function(d){return xScale(d.date) + xScale.rangeBand() / 2;})
				.y(function(d){return yScale(d.x_data);});
			// добавляем путь
			svg.append("g").append("path")
				.attr("d", line(data))
				.style("stroke", "steelblue")
				.style("stroke-width", 2);
		},		
	});
	
	/**
	 * Вкладка "Редактирование название товара"	
	 * @since 3.8.8
	 */
	USAM_Page_seo.title = 
	{		
		not_send_form : true,
		
		event_init : function() 
		{					
			USAM_Page_seo.wrapper
				.delegate('#button_replaced' , 'click'   , USAM_Page_seo.title.text_replacement)
				.delegate('#save-seo-submit' , 'click'   , USAM_Page_seo.title.save_seo)
				.delegate('#post-query-submit' , 'click'   , USAM_Page_seo.title.filter)
				.delegate('#search-submit'   , 'click'   , USAM_Page_seo.title.search)
				.delegate('#usam-tab_form'   , 'submit'   , USAM_Page_seo.title.send_form);				
		},
		
		search : function() 
		{				
			USAM_Page_seo.title.not_send_form = false;			
		},
		
		send_form : function() 
		{					
			if ( USAM_Page_seo.title.not_send_form )
			{				
				return false;	
			}
			else
				USAM_Page_seo.title.not_send_form = true;
		},
		
		filter : function() 
		{						
			USAM_Page_seo.title.not_send_form = false;
			$( "form#usam-tab_form" ).submit();		
		},
			
		save_seo : function() 
		{							
			var spinner = $('#all-ajax-loading');
			var products = {};
			var send = false;
			spinner.toggleClass('ajax-loading-active');		
			$(".product_title input:text.change_made").each( function()
			{						
				var product_id = $(this).data('product_id'),
				product_title = $('#product_title_'+product_id),
				product_excerpt = $('#product_excerpt_'+product_id);	
				products[product_id] = {
					"product_title": product_title.val(),
				//	"product_excerpt": product_excerpt.val()
				};
				if ( product_excerpt.hasClass('change_made'))
					products[product_id]["product_excerpt"] = product_excerpt.val();	
				
				product_title.removeClass("change_made");	
				product_excerpt.removeClass("change_made");		
				send = true;				
			});				
			$(".post_excerpt textarea.change_made").each( function()
			{						
				var product_id = $(this).data('product_id'),
				product_title = $('#product_title_'+product_id),
				product_excerpt = $('#product_excerpt_'+product_id);	
				products[product_id] = {
				//	"product_title": product_title.val(),
					"product_excerpt": product_excerpt.val()
				};
				if ( product_title.hasClass('change_made'))
					products[product_id]["product_title"] = product_title.val();	
				product_title.removeClass("change_made");	
				product_excerpt.removeClass("change_made");	
				send = true;						
			});	
			$(".post_content textarea.change_made").each( function()
			{						
				var product_id = $(this).data('product_id'),			
				post_content = $('#product_content_'+product_id);	
				products[product_id] = {				
					"product_content": post_content.val()
				};				
				post_content.removeClass("change_made");	
				send = true;						
			});		
			if ( send )
			{
				var post_data   = {
						action         : 'seo_title_product_save',
						'products'     : products,								
						nonce          : USAM_Page_seo.seo_title_product_save_nonce
					},
					ajax_callback = function(response)
					{				
						if (! response.is_successful) 
						{
							alert(response.error.messages.join("\n"));
							return;
						}			
						var url = location.href +'&updated='+response.obj;						
						if ( $("#usam-category").length > 0 && $("#usam-category").val() != '' ) 
							url= url+'&usam-category='+$("#usam-category").val();
						if ( $("#usam-brands").length > 0 && $("#usam-brands").val() != '' ) 
							url= url+'&usam-brands='+$("#usam-brands").val();					
						window.location.replace( url );							
						spinner.toggleClass('ajax-loading-active');				    	
					};			
				$.usam_post(post_data, ajax_callback);			
			}
			else
				spinner.toggleClass('ajax-loading-active');
			return false;	
		},
		
		text_replacement : function() 
		{					
			var where  = $('#where_replace').val();
			var what   = $('#what_replaced').val();	
			var how_to = $('#how_to_replace').val();
			if ( what != '' )
			{		
				switch ( where ) 
				{
					case 'title':
						var box = $(".product_title input:text");
					break;
					case 'charact':
						var box = $(".post_content textarea");
					break;
					case 'desc':
						var box = $(".post_excerpt textarea");
					break;
				}				
				box.each( function()
				{	
					var text = $(this).val();					
					var new_text = text.replace(what, how_to);
					if ( new_text != text )
						$(this).addClass( "change_made" );
					$(this).val(new_text);				
				});	
			}			
		},	
	};
	$(USAM_Page_seo).bind('usam_tab_loaded_title', USAM_Page_seo.title.event_init);	
	
	
	USAM_Page_seo.positions = 
	{				
		event_init : function() 
		{				
			if ( typeof usam_data_graph !== "undefined" )
			{ 
				USAM_Page_seo.display_graph( usam_data_graph, usam_name_graph );				
				$("#graph").show();
			}
		},
	};
	$(USAM_Page_seo).bind('usam_tab_loaded_positions', USAM_Page_seo.positions.event_init);	
	
})(jQuery);	
USAM_Page_seo.init();