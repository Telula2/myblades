<?php
new USAM_Category_Sale_Forms_Admin();
class USAM_Category_Sale_Forms_Admin
{
	function __construct( ) 
	{		
		add_action( 'created_usam-category_sale', array( $this, 'save' ), 10 , 2 ); //После создания
		add_action( 'edited_usam-category_sale', array( $this, 'save' ), 10 , 2 ); //После сохранения
		add_action( 'usam-category_sale_add_form_fields', array( $this, 'add_forms') ); // форма добавления
		add_action( 'usam-category_sale_edit_form_fields', array( $this, 'edit_forms'), 10, 2 ); // форма редактирования
		
		add_filter( 'manage_edit-usam-category_sale_columns', array( $this, 'custom_category_columns') );
		add_filter( 'manage_usam-category_sale_custom_column', array( $this, 'custom_category_column_data'), 10, 3);	
	}	
	
	/**
	 * Добавляет столбец изображения в категории колонке.
	 */
	function custom_category_columns( $columns ) 
	{
		unset( $columns["cb"] );
		$custom_array = array( 'cb' => '<input type="checkbox" />',	'image' => __( 'Изображение', 'usam' ) );
		$columns = array_merge( $custom_array, $columns );
		$columns['active'] = __( 'Активность', 'usam' );
		$columns['interval'] = __( 'Интервал', 'usam' );
		$columns['sale_area'] = __( 'Зона', 'usam' );
		return $columns;
	}
	
	/*
	 * Добавляет изображения в колонке на странице категорий
	 */
	function custom_category_column_data( $string, $column_name, $taxonomy_id )
	{				
		switch ( $column_name ) 
		{
			case 'image':
				$name = get_term_by( 'id', $taxonomy_id, 'usam-category_sale' );
				$name = $name->name;		
				$image = usam_taxonomy_image( $taxonomy_id, array(30, 30) );
				if( !empty( $image ) )
					echo "<img src=\"".$image . "\" title='".esc_attr( $name )."' alt='".esc_attr( $name )."' width='30' height='30' />";
				else
					echo "<img src='".USAM_CORE_IMAGES_URL."/no-image-uploaded.gif' title='".esc_attr( $name )."' alt='".esc_attr( $name )."' width='30' height='30' />";
			break;
			case 'active':				
				$status = get_term_meta($taxonomy_id, 'usam_status_stock', true);	
				if( $status )
					_e('Активно','usam');
				else
					_e('Не активно','usam');
			break;
			case 'sale_area':				
				$area = get_term_meta($taxonomy_id, 'usam_sale_area', true);	
				echo usam_get_name_sales_area( $area );
			break;
			case 'interval':
				$start_date = get_term_meta($taxonomy_id, 'usam_start_date_stock', true);	
				$end_date = get_term_meta($taxonomy_id, 'usam_end_date_stock', true);	
				$message = '';
				if ( !empty($start_date) )
					$message .= sprintf(__('c %s','usam'), usam_local_date( $start_date ) ).' ';
				
				if ( !empty($end_date) )
				{
					$message .=  sprintf(__('до %s','usam'), usam_local_date( $end_date ) );
				}
				echo $message;
			break;
		}
	}
	
	/**
	 * печатает левую часть страницы добавления новой категории
	 */
	function add_forms( ) 
	{	
		$settings = array( 'status' => 1, 'start_date' => '', 'end_date' => '', 'area' => '' );
		?>
		<div id="add_new_term" class="postbox">
			<h3 class="hndle"><?php esc_html_e('Дополнительные настройки', 'usam'); ?></h3>
			<div class="inside">
				<table class ="usam_table">					
					<?php					
						$this->settings_product_attribute( $settings );		
					?>
				</table>
			</div>
		</div>		
	  <?php
	}
	
	
	function settings_product_attribute( $settings ) 
	{	
		?>						
		<tr>
			<td colspan="2"><h3><?php esc_html_e( 'Настройка акции', 'usam' ); ?></h3></td>
		</tr>
		<tr>
			<th scope="row" valign="top"><label for="display_type"><?php esc_html_e( 'Статус акции', 'usam' ); ?></label></th>
			<td>		
				<input type='radio' value='1' name='status' id='status1' <?php checked( $settings['status'], 1 ); ?>/> 		
				<label for='status1'><?php _e( 'Активно', 'usam' ); ?></label> &nbsp;
				<input type='radio' value='0' name='status' id='status2' <?php checked( $settings['status'], 0 ); ?>/>
				<label for='status2'><?php _e( 'Не активно', 'usam' ); ?></label>	
			</td>			
		</tr>
		<tr>
			<th scope="row" valign="top"><label for="display_type"><?php esc_html_e( 'Срок акции', 'usam' ); ?></label></th>
			<td><?php usam_display_datetime_picker( 'start', $settings['start_date'] ); ?> - <?php usam_display_datetime_picker( 'end', $settings['end_date'] ); ?></td>
		<tr>
		<tr>
			<th scope="row" valign="top"><label for="display_type"><?php esc_html_e( 'Зона продаж', 'usam' ); ?></label></th>
			<td><?php usam_select_sales_area( $settings['area'], array( 'name' => 'sale_area' ) ); ?></td>
		<tr>		
		<?php
	}	

	function edit_forms( $term, $taxonomy) 
	{				
		$status = get_term_meta($term->term_id, 'usam_status_stock', true);	
		$start_date = get_term_meta($term->term_id, 'usam_start_date_stock', true);	
		$end_date = get_term_meta($term->term_id, 'usam_end_date_stock', true);	
		$area = get_term_meta($term->term_id, 'usam_sale_area', true);			
		
		$settings = array( 'status' => $status, 'start_date' => $start_date, 'end_date' => $end_date, 'area' => $area );		
		$this->settings_product_attribute( $settings );
	}
	
	/**
	 * Сохраняет данные категории
	 */
	function save( $category_id, $tt_id )
	{				
		if( !empty( $_POST )  ) 
		{					
			$status = !empty( $_POST['status'] ) ? 1:0;	
			update_term_meta($category_id, 'usam_status_stock', $status );
			
			$start_date = usam_get_datepicker('start');
			$end_date = usam_get_datepicker('end');
			
			update_term_meta($category_id, 'usam_start_date_stock',$start_date );
			update_term_meta($category_id, 'usam_end_date_stock',$end_date );
			
			
			$sale_area = !empty( $_POST['sale_area'] ) ? (int)$_POST['sale_area']:0;
			update_term_meta($category_id, 'usam_sale_area', $sale_area );
		}
		add_term_meta( $category_id, 'usam_sorting_menu_categories', 999 , true );
		
		$thumbnail_id = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
		update_term_meta( $category_id, 'thumbnail', $thumbnail_id );
	}
}
?>