<?php
/* Этот файл используется для добавления полей в страницу редактирование категорий продуктов и правильно сохранения
 * @since 3.8
 */
new USAM_Category_Forms_Admin();
class USAM_Category_Forms_Admin
{
	function __construct( ) 
	{		
		add_action( 'created_usam-category', array( $this, 'save' ), 10 , 2 ); //После создания
		add_action( 'edited_usam-category', array( $this, 'save' ), 10 , 2 ); //После сохранения
		add_action( 'usam-category_add_form_fields', array( $this, 'add_forms') ); // форма добавления категорий
		add_action( 'usam-category_edit_form_fields', array( $this, 'edit_forms'),10, 2 ); // форма редактирования категорий
		
		add_filter( 'manage_edit-usam-category_columns', array( $this, 'custom_category_columns'), 10 );
		add_filter( 'manage_usam-category_custom_column', array( $this, 'custom_category_column_data'), 10, 3);	
	}	
	
	/**
	 * Добавляет столбец изображения в категории колонке.
	 */
	function custom_category_columns( $columns ) 
	{
		unset( $columns["cb"] );
		$custom_array = array( 'cb' => '<input type="checkbox" />',	'image' => '' );
		$columns = array_merge( $custom_array, $columns );
		return $columns;
	}
	
	/*
	 * Добавляет изображения в колонке на странице категорий
	 */
	function custom_category_column_data( $string, $column_name, $taxonomy_id )
	{	//	echo get_term_meta($term_id, 'usam_sort_order', true);
		$name = get_term_by( 'id', $taxonomy_id, 'usam-category' );
		$name = $name->name;		
		$image = usam_taxonomy_image( $taxonomy_id, array(30, 30) );
		if( !empty( $image ) )
			$image = "<img src=\"".$image . "\" title='".esc_attr( $name )."' alt='".esc_attr( $name )."' width='30' height='30' />";
		else
			$image = "<img src='".USAM_CORE_IMAGES_URL."/no-image-uploaded.gif' title='".esc_attr( $name )."' alt='".esc_attr( $name )."' width='30' height='30' />";
		return $image;
	}
	
	/**
	 * печатает левую часть страницы добавления новой категории
	 */		
	function add_forms( ) 
	{	
		$settings = array( 'display_type' => '', 'order_props_group' => '' );
		?>
		<div id="add_new_term" class="postbox">
			<h3 class="hndle"><?php esc_html_e('Дополнительные настройки', 'usam'); ?></h3>
			<div class="inside">
				<table class ="usam_table">					
					<?php $this->settings_product_attribute( $settings ); ?>
				</table>
			</div>
		</div>		
	  <?php
	}
	
	
	function settings_product_attribute( $settings ) 
	{	
		?>						
		<tr class="form-field">
			<th scope="row" valign="top"><label for="display_type"><?php esc_html_e( 'Просмотр каталога', 'usam' ); ?></label></th>
			<td>		
				<select name='display_type'>
					<option value='default' <?php selected( $settings['display_type'], 'default' ); ?>><?php esc_html_e( 'По умолчанию', 'usam' ); ?></option>
					<option value='list' <?php selected( $settings['display_type'], 'list' ); ?>><?php esc_html_e('Списком', 'usam'); ?></option>
					<option value='grid' <?php selected( $settings['display_type'], 'grid' ); ?>><?php esc_html_e( 'Сеткой', 'usam' ); ?></option>
				</select>
			</td>			
		</tr>	
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="image"><?php esc_html_e( 'Дополнительные поля формы расчета', 'usam' ); ?></label>
			</th>
			<td>
				<select name='order_props_group'>
					<option value=''><?php esc_html_e( 'Не одной', 'usam' ); ?></option>
					<?php								
						$props_groups = usam_get_order_props_groups( );					
						foreach( $props_groups as $key => $group ) 
						{
							?>
							<option <?php selected( $settings['order_props_group'], $group->id); ?> value='<?php echo $group->id; ?>'><?php echo esc_html( $group->name ); ?></option>
							<?php
						}
					?>
				</select>
			  </td>
		</tr>	
		<?php
	}

	function edit_forms( $tag, $taxonomy) 
	{				
		$display_type = get_term_meta($tag->term_id, 'usam_display_type', true);		
		$order_props_group = maybe_unserialize( get_term_meta($tag->term_id, 'usam_order_props_group', true) );
		?>		
		<tr>
			<td colspan="2"><h3><?php esc_html_e( 'Дополнительные настройки', 'usam' ); ?></h3></td>
		</tr>
		<?php $this->settings_product_attribute( array( 'display_type' => $display_type, 'order_props_group' => $order_props_group ) );	?>				
		<tr>
			<td colspan="2"><h3><?php esc_html_e( 'Шорткоды и тэги шаблона', 'usam' ); ?></h3></td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="image"><?php esc_html_e( 'Показать шорткоды категории', 'usam' ); ?>:</label>
			</th>
			<td>
				<span>[usam_products category_url_name='<?php echo $tag->slug; ?>']</span><br />
				<span class="description"><?php esc_html_e( 'Shortcodes используются для отображения определенной категории или группы на любой странице WordPress или по почте.', 'usam' ); ?></span>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="image"><?php esc_html_e( 'Показать шаблонные теги категории', 'usam' ); ?>:</label>
			</th>
			<td>
				<span>&lt;?php echo usam_display_products_page( array( 'category_url_name'=>'<?php echo $tag->slug; ?>' ) ); ?&gt;</span><br />
				<span class="description"><?php esc_html_e( 'Шаблонные теги используются для отображения определенной категории или группы в рамках вашей темы / шаблона.', 'usam' ); ?></span>
			</td>
		</tr>		
	  <?php
	}
	
	/**
	 * Сохраняет данные категории
	 */
	function save( $category_id, $tt_id )
	{			
		if( !empty( $_POST )  ) 
		{					
			if ( isset( $_POST['display_type'] ) )
				update_term_meta($category_id, 'usam_display_type', esc_sql(stripslashes($_POST['display_type'])));
			
			$order_props_group = !empty($_POST['order_props_group'])? absint($_POST['order_props_group']):	$order_props_group = 0;			
			update_term_meta($category_id, 'usam_order_props_group', $order_props_group);
			
			$thumbnail_id = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
			update_term_meta( $category_id, 'thumbnail', $thumbnail_id );
		}		
		add_term_meta( $category_id, 'usam_sorting_menu_categories', 999 , true );	
	}
}
?>