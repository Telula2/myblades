<?php
new USAM_Product_Attributes_Forms_Admin();
class USAM_Product_Attributes_Forms_Admin
{
	function __construct( ) 
	{		
		add_action( 'created_usam-product_attributes', array( $this, 'save' ), 10 , 2 ); //После создания
		add_action( 'edited_usam-product_attributes', array( $this, 'save' ), 10 , 2 ); //После сохранения		
		
		add_action( 'usam-product_attributes_edit_form_fields', array( $this, 'edit_forms'), 10 , 2  ); // форма редактирования		
		add_action( 'usam-product_attributes_add_form_fields', array( $this, 'add_forms') ); // форма добавления
		
		if ( isset($_GET['taxonomy']) && $_GET['taxonomy'] == 'usam-product_attributes' && isset($_GET['tag_ID']) )
			add_action('admin_footer', array(&$this, 'admin_footer'));		
		
		add_filter( 'manage_edit-usam-product_attributes_columns', array( $this, 'add_columns' ), 1 );
		add_filter( 'manage_usam-product_attributes_custom_column', array( $this, 'parse_column' ), 10, 3 );
	}		
	
	public function add_columns( array $columns )
	{ 	
		$columns['category'] = __( 'Категории', 'usam' );
		return $columns;
	}
	
	public function parse_column( $content, $column_name, $term_id ) 
	{ 
		switch ( $column_name ) 
		{
			case 'category':
				
				$term = get_term_by( 'id', $term_id, 'usam-product_attributes' );
				if ( $term->parent )
				{
					$category_ids = usam_get_taxonomy_relationships( $term_id, 'usam-category', '1' );						
					if ( !empty( $category_ids ) )
					{
						$args = array( 'hide_empty' => 0, 'orderby' => 'meta_value_num', 'meta_key' => 'usam_sort_order', 'include' => $category_ids );
						$category = get_terms('usam-category', $args);	
						
						$out = array();
						foreach ( $category as $term )
							$out[] = "<a href='".admin_url("edit.php?post_type=usam-product&amp;usam-category={$term->slug}")."'> ".$term->name."</a>";
						echo join( ', ', $out );
					} 
					else
						_e("Во всех категориях","usam");
				}
			break;
		}
	}
	
	function add_forms( ) 
	{	
		$settings = array( 'mandatory' => 0, 'do_not_show_in_features' => 0, 'filter' => 0, 'search' => 0, 'type' => 'T' );	
		?>		
		<div id="add_new_term" class="postbox">
			<h3 class="hndle"><?php esc_html_e('Дополнительные настройки', 'usam'); ?></h3>
			<div class="inside">
				<table class ="usam_table">				
					<?php $this->settings_product_attribute( $settings ); ?>
				</table>
				<p><?php esc_html_e('Остальные настройки Вы можете сделать, открыв свойство.', 'usam'); ?></p>
			</div>
		</div>	
	  <?php
	}
	
	function settings_product_attribute( $settings ) 
	{	
		?>
		<tr>
			<th scope="row" valign="top" class="name"><label for="type_field"><?php esc_html_e( "Обязательное", "usam" ); ?>:</label></th>
			<td>
				<input type='radio' value='1' name='mandatory' id='mandatory1' <?php checked( $settings['mandatory'], 1 ); ?>/> 		
				<label for='mandatory1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
				<input type='radio' value='0' name='mandatory' id='mandatory2' <?php checked( $settings['mandatory'], 0 ); ?>/>
				<label for='mandatory2'><?php _e( 'Нет', 'usam' ); ?></label>						
			</td>
		</tr>			
		<tr>
			<th scope="row" valign="top"><label for="type_field"><?php esc_html_e( "Скрыть в характеристиках для покупателей", "usam" ); ?>:</label></th>
			<td>
				<input type='radio' value='1' name='do_not_show_in_features' id='do_not_show_in_features1' <?php checked( $settings['do_not_show_in_features'], 1 ); ?>/> 		
				<label for='do_not_show_in_features1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
				<input type='radio' value='0' name='do_not_show_in_features' id='do_not_show_in_features2' <?php checked( $settings['do_not_show_in_features'], 0 ); ?>/>
				<label for='do_not_show_in_features2'><?php _e( 'Нет', 'usam' ); ?></label>						
			</td>
		</tr>
		<tr>
			<th scope="row" valign="top"><label for="type_field"><?php esc_html_e( "Фильтр по этому свойству", "usam" ); ?>:</label></th>
			<td>
				<input type='radio' value='1' name='filter' id='filter1' <?php checked( $settings['filter'], 1 ); ?>/> 		
				<label for='filter1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
				<input type='radio' value='0' name='filter' id='filter2' <?php checked( $settings['filter'], 0 ); ?>/>
				<label for='filter2'><?php _e( 'Нет', 'usam' ); ?></label>						
			</td>
		</tr>
		<tr>
			<th scope="row" valign="top"><label for="type_field"><?php esc_html_e( "Поиск по этому свойству", "usam" ); ?>:</label></th>
			<td>
				<input type='radio' value='1' name='search' id='search1' <?php checked( $settings['search'], 1 ); ?>/> 		
				<label for='search1'><?php _e( 'Да', 'usam' ); ?></label> &nbsp;
				<input type='radio' value='0' name='search' id='search2' <?php checked( $settings['search'], 0 ); ?>/>
				<label for='search2'><?php _e( 'Нет', 'usam' ); ?></label>						
			</td>
		</tr>
		<tr>
			<th scope="row" valign="top"><label for="type_field"><?php esc_html_e( "Тип", "usam" ); ?>:</label></th>
			<td>
				<select id="type_product_attributes" name="type_product_attributes">
					<optgroup label="<?php esc_html_e( "Базовый тип", "usam" ); ?>">							
						<option value="T"<?php echo ($settings['type'] == 'T'?'selected':''); ?>><?php _e( 'Текст', 'usam'); ?></option>
						<option value="O"<?php echo ($settings['type'] == 'O'?'selected':''); ?>><?php _e( 'Число', 'usam'); ?></option>
					</optgroup>				
					<optgroup label="<?php esc_html_e( "Флажок", "usam" ); ?>">							
						<option value="C"<?php echo ($settings['type'] == 'C'?'selected':''); ?>><?php _e( 'Один', 'usam'); ?></option>
						<option value="M"<?php echo ($settings['type'] == 'M'?'selected':''); ?>><?php _e( 'Несколько', 'usam'); ?></option>
					</optgroup>		
					<optgroup label="<?php esc_html_e( "Выбор из вариантов", "usam" ); ?>">							
						<option value="S"<?php echo ($settings['type'] == 'S'?'selected':''); ?>><?php _e( 'Текст', 'usam'); ?></option>
						<option value="N"<?php echo ($settings['type'] == 'N'?'selected':''); ?>><?php _e( 'Число', 'usam'); ?></option>
					</optgroup>	
					<optgroup label="<?php esc_html_e( "Пользователи", "usam" ); ?>">						
						<option value="U"<?php echo ($settings['type'] == 'U'?'selected':''); ?>><?php _e( 'Менеджеры', 'usam'); ?></option>
						<option value="A"<?php echo ($settings['type'] == 'A'?'selected':''); ?>><?php _e( 'Удаленные агенты', 'usam'); ?></option>
					</optgroup>	
					<optgroup label="<?php esc_html_e( "Другое", "usam" ); ?>">	
						<option value="D"<?php echo ($settings['type'] == 'D'?'selected':''); ?>><?php _e( 'Дата', 'usam'); ?></option>						
					</optgroup>							
				</select>
			</td>
		</tr>
		<?php	
	}
	
	function admin_footer( ) 
	{		
		$html = '<div class="categorydiv"><ul id="usam-categorychecklist" class="categorychecklist form-no-clear"></ul></div>';
		$html .= "<div class='popButton'>
			<button id = 'modal_action' type='button' class='button-primary button'>".__( 'Отправить', 'usam' )."</button>
			<button type='button' class='button' data-dismiss='modal' aria-hidden='true'>".__( 'Отменить', 'usam' )."</button>
		</div>";
		echo usam_get_modal_window( __('Добавить категории','usam'), 'display_category_window', $html );						
	}

	function edit_forms( $tag, $taxonomy) 
	{
		if ( $tag->parent != 0 )
		{
			$mandatory = get_term_meta($tag->term_id, 'usam_mandatory', true);	
			$filter = get_term_meta($tag->term_id, 'usam_filter', true);
			$search = get_term_meta($tag->term_id, 'usam_search', true);
			$do_not_show_in_features = get_term_meta($tag->term_id, 'usam_do_not_show_in_features', true);			
			
			$settings['type'] = get_term_meta($tag->term_id, 'usam_type_attribute', true);	
			$ready_options = usam_get_product_attribute_values( array( 'attribute_id' => $tag->term_id ) );
			$category_ids = usam_get_taxonomy_relationships( $tag->term_id, 'usam-category', $colum = '1' );		
			
			$settings['mandatory'] = empty($mandatory)?0:1;	
			$settings['filter'] = empty($filter)?0:1;
			$settings['search'] = empty($search)?0:1;	
			$settings['do_not_show_in_features'] = empty($do_not_show_in_features)?0:1;				
			?>
			<?php $this->settings_product_attribute( $settings ); ?>
			<tr id = "ready_options">
				<th scope="row" valign="top"><label for="type_field"><?php esc_html_e( "Варианты", "usam" ); ?>:</label></th>
				<td>
					<table class="widefat fixed striped ready_options">
						<thead>
							<tr>
								<td class="number"></td>	
								<td class="code"><?php _e( 'Код', 'usam' ); ?></td>
								<td class="value"><?php _e( 'Значение', 'usam' ); ?></td>	
								<td class="sort"><?php _e( 'Сортировка', 'usam' ); ?></td>								
								<td class="action"></td>								
							</tr>
						</thead>
						<tbody>								
							<?php					
							if ( !empty($ready_options) )
							{
								foreach( $ready_options as $key => $option )
								{
									?>
									<tr>								
										<td><?php echo $key+1; ?></td>
										<td><input type="text" id="code" name="ready_options[<?php echo $option->id; ?>][code]" value="<?php echo $option->code; ?>" /></td>	
										<td><input type="text" id="value" name="ready_options[<?php echo $option->id; ?>][value]" value="<?php echo $option->value; ?>" /></td>
										<td><input type="text" id="sort" name="ready_options[<?php echo $option->id; ?>][sort]" value="<?php echo $option->sort; ?>" /></td>
										<td><a href="" class="add"><span class="dashicons dashicons-plus"></span></a> | <a href="" class="delete"><span class="dashicons dashicons-minus"></span></a></td>
									</tr>
									<?php 
								}
							} ?>							
							<tr>								
								<td></td>	
								<td><input type="text" id="code" name="ready_options_new[code][]" value="" /></td>	
								<td><input type="text" id="value" name="ready_options_new[value][]" value="" /></td>
								<td><input type="text" id="sort" name="ready_options_new[sort][]" value="" /></td>
								<td><a href="" class="add"><span class="dashicons dashicons-plus"></span></a> | <a href="" class="delete"><span class="dashicons dashicons-minus"></span></a></td>
							</tr>								
						</tbody>
					</table>
				</td>
			</tr>					
			<tr class="product_attributes_category">
				<th scope="row" valign="top">
					<label for="type_field"><?php esc_html_e( "Прикрепить к категориям", "usam" ); ?>:</label>
				</th>
				<td>
					<div class ="box_category">
						<div class="categories_header">	
							<a href="" id="add_category"><?php esc_html_e( '+ Добавить категорию', 'usam' ) ?></a>
							<h2><?php esc_html_e( "Прикрепленные категории", "usam" ); ?></h2>
						</div>
						<div class="categories">				
							<table>
							<?php	
							if ( !empty($category_ids) )
							{
								$categories = get_terms('usam-category', array( 'hide_empty' => 0, 'orderby' => 'name', 'include' => $category_ids ) );	
								foreach( $categories as $category )
								{												
									echo "<tr><td><input value='$category->term_id' type='hidden' name='tax_input[usam-category][]' id='in-usam-category-$category->term_id'>".$category->name."<a href='' id='delete_category'>Удалить</a></td></tr>";
								}
							}
							else
								echo "<tr id='no_data'><td>".__( "Во всех категориях", "usam" )."</td></tr>";
							?>					
							</table>
						</div>		
					</div>						
				</td>
			</tr>
		  <?php
		}
		else
		{
			
		}
	}
	
	/**
	 * Сохраняет данные
	 */
	function save( $term_id, $tt_id )
	{				
		if( !empty( $_POST )  ) 
		{								
		//	$thumbnail_id = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
		//	update_term_meta( $term_id, 'thumbnail', $thumbnail_id );
			
			$term = get_term( $term_id, 'usam-product_attributes' );			
			if ( $term->parent != 0 )
			{							
				if ( empty($_POST['tax_input']) )
				{				
					usam_delete_taxonomy_relationships( array( 'term_id1' => $term_id, 'taxonomy' => 'usam-category' ) );
				}
				else		
				{				
					$terms = usam_get_taxonomy_relationships( $term_id, 'usam-category', '1' );
					$category   = stripslashes_deep($_POST['tax_input']['usam-category']);	
					$result = array_diff($terms, $category);
					
					foreach( $result as $id )
						usam_delete_taxonomy_relationships( array( 'term_id1' => $term_id, 'term_id2' => $id, 'taxonomy' => 'usam-category' ) );
						
					foreach( $category as $category_id )
					{							
						usam_set_taxonomy_relationships($term_id, $category_id, 'usam-category' );	
					}	
				}			
				$filter = isset($_POST['filter'])?(bool)$_POST['filter']:0;
				if ( !$filter )
				{
					usam_delete_products_meta( "filter_$term_id" );				
				}		
				update_term_meta($term_id, 'usam_filter', $filter );	
				if ( isset($_POST['search']) )
				{
					$search = (bool)$_POST['search'];
					update_term_meta($term_id, 'usam_search', $search );	
				}
				if ( isset($_POST['do_not_show_in_features']) )
				{
					$do_not_show_in_features = (bool)$_POST['do_not_show_in_features'];
					update_term_meta($term_id, 'usam_do_not_show_in_features', $do_not_show_in_features );	
				}			
				if ( isset($_POST['mandatory']) )
				{
					$mandatory = (bool)$_POST['mandatory'];
					update_term_meta($term_id, 'usam_mandatory', $mandatory );	
				}				
				if ( isset($_POST['type_product_attributes']) )
				{ 
					$type = strtoupper(sanitize_title($_POST['type_product_attributes']));
					
					$old_type = get_term_meta($term_id, 'usam_type_attribute', true);	
					if ( !empty($old_type) && $old_type != $type )
					{
						$args['meta_query'] = array( array('key' => "_usam_product_attributes_$term_id", 'compare' => 'EXISTS') );
						$total_products = usam_get_total_products( $args );
						usam_create_system_process( __("Обновление атрибутов товаров","usam" ), array('attribute_id' => $term_id, 'old_type' => $old_type, 'type' => $type), 'change_attribute_type', $total_products, "change_attribute_type_$term_id" );
					}
					update_term_meta($term_id, 'usam_type_attribute', $type );	
					if ( $type == 'M' || $type == 'S' || $type == 'N')
					{ 					
						if ( isset($_POST['ready_options']) )
						{	
							if ( $filter )
							{
								$product_attribute_values = usam_get_product_attribute_values( array( 'attribute_id' => $term_id ) );
								$current_ready_options = array();
								foreach( $product_attribute_values as $option )	
								{
									$current_ready_options[$option->id] = array( 'value' => $option->value, 'attribute_id' => $term_id );
								}
							}								
							$ready_options = stripslashes_deep($_POST['ready_options']);
							foreach( $ready_options as $id => $option )	
							{
								if ( $type == 'N' )
									$option['value'] = preg_replace('~[^0-9]+~','',$option['value']);
								
								if ( !empty($option['value']) )
								{
									$update = array('value' => $option['value'], 'code' => $option['code'], 'sort' => $option['sort'] );
									usam_update_product_attribute( $id, $update );								
								}
								else
									usam_delete_product_attribute( $id );
								
								if ( $filter && !empty($current_ready_options) )
								{									
									usam_update_product_filter( $current_ready_options[$id], array( 'value' => $option['value'] ) );
								}
							}
						}
						if ( isset($_POST['ready_options_new']) )
						{	
							$ready_options = stripslashes_deep($_POST['ready_options_new']);						
							foreach( $ready_options['code'] as $key => $code )
							{
								if ( !empty($ready_options['value'][$key]) )
									$value = $ready_options['value'][$key];
								else
									continue;
								
								if ( $type == 'N' )
									$value =  preg_replace('~[^0-9]+~','',$value);
								
								if ( isset($ready_options['sort'][$key]) )
									$sort = $ready_options['sort'][$key];
								else
									$sort = '';							
								$insert_product_attribute = array( 'code' => $code, 'value' => $value, 'sort' => $sort, 'attribute_id' => $term_id );	

								usam_insert_product_attribute( $insert_product_attribute );							
							}					
						}					
					}
					else
						usam_delete_product_attribute( $term_id, 'attribute_id' );
				}	
			}
		}		
	}
}
?>