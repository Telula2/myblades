<?php
/* Этот файл используется для добавления полей в страницу редактирование таксоманий
 */
 
$category_class = new USAM_Taxonomy_Forms_Admin();
class USAM_Taxonomy_Forms_Admin
{
	private $taxonomies = array( "usam-brands", "usam-category", "usam-category_sale", 'usam-variation' );	
	function __construct( ) 
	{		
		if ( isset($_REQUEST['tag_ID']) )
		{			// Если категория
			
			foreach ( $this->taxonomies as $taxonomy ) 
			{
			//	add_filter( 'manage_' . $taxonomy . '_custom_column', 'taxonomy_image_plugin_taxonomy_rows', 15, 3 );
			//	add_filter( 'manage_edit-' . $taxonomy . '_columns',  'taxonomy_image_plugin_taxonomy_columns' );
				add_action( $taxonomy.'_edit_form_fields', array( $this, 'edit_forms'), 8, 2 );// форма редактирования категорий	
			}			
		}		
		else
		{ // Если все категории
			add_filter( 'admin_footer', array($this, 'print_term_list_levels_script') );
			add_filter( 'term_name', array($this, 'term_list_levels'), 10, 2 );	
			add_filter( 'get_terms_defaults', array($this, 'sort_order_taxonomy'), 10, 2 );
		}	
	}	
	
	function sort_order_taxonomy( $query_var_defaults, $taxonomies ) 
	{		
		if ( empty( $_REQUEST['orderby'] ) )
		{
			foreach ( $this->taxonomies as $taxonomy ) 
			{
				if ( in_array($taxonomy, $taxonomies) )
				{ 
					$query_var_defaults['orderby'] = 'meta_value_num';
					$query_var_defaults['order'] = 'ASC';
					$query_var_defaults['meta_key'] = 'usam_sort_order';	
					break;
				}
			}
		}
		return $query_var_defaults;
	}	
	
	function term_list_levels( $term_name, $term ) 
	{
		global $wp_list_table, $usam_term_list_levels;		
		
		if ( !isset($term->term_id) )
			return $term_name;
		if ( ! isset( $usam_term_list_levels ) )
			$usam_term_list_levels = array();
		$usam_term_list_levels[$term->term_id] = $wp_list_table->level;
		return $term_name;
	}
	/**
	При выполнении изменения продукта и перетаскивании категории, мы хотим ограничить перетаскивание тем же уровнем (дети категория не могут быть удалены). Чтобы сделать это, мы должны быть определить уровень глубины термина. Мы можем сделать это с WP хуками. Эта функция добавляется в меню "term_name" фильтр. Его задача заключается в записи глубины уровня каждого терминов в глобальной переменной. Эта глобальная переменная позже будет вывод на JS 
	 */
	function print_term_list_levels_script()
	{		
		global $usam_term_list_levels;		
		?>
		<script type="text/javascript">
		//<![CDATA[
		var USAM_Term_List_Levels = <?php echo json_encode( $usam_term_list_levels ); ?>;
		//]]>
		</script>
		<?php
	}	
	
	function edit_forms( $term, $taxonomy ) 
	{		
		$attachment_id = (int)get_term_meta($term->term_id, 'thumbnail', true);	
		
		$image_attributes = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
		$thumbnail = $image_attributes[0];		
		
		if ( empty($thumbnail) )
		{
			$thumbnail = USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif';	
			$class_thumbnail_remove = 'hide';
		}
		else
			$class_thumbnail_remove = '';
		?>		
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="image"><?php esc_html_e( 'Установите миниатюру', 'usam' ); ?></label>				
			</th>
			<td>				
				<div class="usam_thumbnail">
					<a data-attachment_id="<?php echo $attachment_id; ?>" data-title="<?php  _e( 'Миниатюра для термина', 'usam' ); ?>" data-button_text="<?php  _e( 'Задать миниатюру', 'usam' ); ?>" href="<?php echo esc_url( admin_url( 'media-upload.php?tab=gallery&TB_iframe=true&width=640&height=566' ) ) ?>">
						<img src="<?php echo esc_url( $thumbnail ); ?>" alt="">
					</a>
					<input type='hidden' id='usam_thumbnail_id' name ="thumbnail" value='<?php echo $attachment_id; ?>' />		
				</div>		
				<div class="usam_thumbnail_remove <?php echo $class_thumbnail_remove; ?>">				
					<a class="control remove" href="#" id="remove-<?php echo $term->term_id; ?>" title="<?php esc_html_e( 'Удалить миниатюру', 'usam' ); ?>"><?php esc_html_e( 'Удалить миниатюру', 'usam' ); ?></a>
				</div>		
				<span class="description"><?php _e( 'Вы можете установить миниатюру здесь.', 'usam' ); ?></span>
			</td>
		</tr>  
		<?php
	}	
}
?>