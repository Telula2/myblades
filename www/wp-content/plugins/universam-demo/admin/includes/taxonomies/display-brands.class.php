<?php
/* Этот файл используется для добавления полей в страницу редактирования таксономии бренда продуктов и правильно сохранения
  */  
$brand_class = new USAM_Brands_Forms_Admin();
class USAM_Brands_Forms_Admin
{
	function __construct( ) 
	{		
		add_filter( 'manage_edit-usam-brands_columns', array( &$this, 'image_column_display') );
		add_filter( 'manage_usam-brands_custom_column', array( &$this, 'image_column'), 10, 3); // Добавляет изображения в колонке на странице категорий
		add_action( 'usam-brands_add_form_fields', array( &$this, 'forms_add') ); // форма добавления категорий
		add_action( 'usam-brands_edit_form_fields', array( &$this, 'forms_edit'),10, 2 ); // форма редактирования категорий
		add_action( 'create_usam-brands', array( &$this, 'save'), 10 , 2 ); //Выполнить после создания бренда
		add_action( 'edited_usam-brands', array( &$this, 'save'), 10 , 2 ); //Выполнить после сохранения бренда

	//	add_action( 'delete_usam-brands',  );
	}
	
	/**
	 * Добавляет столбец изображения в категории колонке.
	 */
	function image_column_display( $columns ) 
	{  
		unset( $columns["cb"] );
		$custom_array = array(
			'cb' => '<input type="checkbox" />',
			'image' => ''
		);
		$columns = array_merge( $custom_array, $columns );
		return $columns;
	}
	
	/*
	 * Добавляет изображения в колонке на странице категорий
	 */
	function image_column( $string, $column_name, $term_id )
	{
		$term = get_term_by( 'id', $term_id, 'usam-brands' );	
		$image = usam_taxonomy_image( $term_id, array(30, 30) );
		if( !empty( $image ) )
			$image = "<img src=\"".$image . "\" title='".esc_attr( $term->name )."' alt='".esc_attr( $term->name )."' width='30' height='30' />";
		else
			$image = "<img src='".USAM_CORE_IMAGES_URL."/no-image-uploaded.gif' title='".esc_attr( $term->name )."' alt='".esc_attr( $term->name )."' width='30' height='30' />";
		return $image;
	}

	/**
	 * 
	 */
	function forms_add()
	{	
		$display_type = '';
		?>		
		<div id="poststuff" class="postbox">
			<h3 class="hndle"><?php esc_html_e('Дополнительные настройки', 'usam'); ?></h3>
			<div class="inside">				
				<table class ="usam_table">		
					<tr>
						<td>
							<?php esc_html_e( 'Просмотр каталога', 'usam' ); ?>
						</td>
						<td>
							<select name='display_type'>
								<option value='default'<?php checked( $display_type, 'default' ); ?>><?php esc_html_e('По умолчанию', 'usam'); ?></option>
								<option value='list'<?php checked( $display_type, 'list' ); ?>><?php esc_html_e('Списком', 'usam'); ?></option>
								<option value='grid'<?php checked( $display_type, 'grid' ); ?>><?php esc_html_e('Сеткой', 'usam'); ?></option>
							</select>
						</td>
					</tr>
				</table>				
			</div>
		</div>
	  <?php
	}	

	function forms_edit( $tag, $taxonomy) 
	{	   		
		$display_type = get_term_meta($tag->term_id, 'usam_display_type', true);		
		$link = get_term_meta($tag->term_id, 'link', true);		
		?>
		<tr>
			<td colspan="2">
				<h3><?php esc_html_e( 'Дополнительные настройки', 'usam' ); ?></h3>
			</td>
		</tr>				
		<tr class="form-field">
			<th scope="row" valign="top"><label for="display_type"><?php esc_html_e( 'Просмотр каталога', 'usam' ); ?></label></th>
			<td>			
				<select name='display_type'>
					<option value='default' <?php checked( $display_type, 'default' ); ?>><?php esc_html_e( 'По умолчанию', 'usam' ); ?></option>
					<option value='list' <?php checked( $display_type, 'list' ); ?>><?php esc_html_e('Списком', 'usam'); ?></option>
					<option value='grid' <?php checked( $display_type, 'grid' ); ?>><?php esc_html_e( 'Сеткой', 'usam' ); ?></option>
				</select>
			</td>
		</tr>	
		<tr class="form-field">
			<th scope="row" valign="top"><label for="usam_cat_link"><?php esc_html_e( 'Ссылка на сайт бренда', 'usam' ); ?></label></th>
			<td><input type='text' name='link' id = "usam_cat_link" class="usam_cat_link" value='<?php echo $link; ?>' /></td>
		</tr>	
	  <?php
	}
	
	/**
	 * Сохраняет данные Бренда
	 */
	function save( $brands_id, $tt_id )
	{	
		if( !empty( $_POST ) )
		{	
			if ( isset( $_POST['display_type'] ) )
				update_term_meta($brands_id, 'usam_display_type',sanitize_title($_POST['display_type']));			
			if ( isset( $_POST['link'] ) )
				update_term_meta( $brands_id, 'link', sanitize_text_field($_POST['link']) );
			
			$thumbnail_id = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
			update_term_meta( $brands_id, 'thumbnail', $thumbnail_id );
		}	
	}
}
?>