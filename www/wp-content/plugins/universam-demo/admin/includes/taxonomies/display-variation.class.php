<?php
new USAM_Product_Variation_Forms_Admin();
class USAM_Product_Variation_Forms_Admin
{
	function __construct( ) 
	{		
		add_action( 'usam-variation_edit_form_fields', array($this, 'variation_set_field') );
		add_action( 'usam-variation_add_form_fields', array($this, 'variation_set_field') );
		add_action( 'edited_usam-variation', array( $this, 'save' ), 10 , 2 ); //После сохранения
	}	
	
	/*
	Используйте Jquery для перемещения набора вариации полей в начало и добавить описание
	*/
	function variation_set_field()
	{
	?>
		<script>		
			jQuery("#parent option[value='-1']").text("Новый набор вариантов");		
			jQuery("#tag-name").parent().before( jQuery("#parent").parent().append('<p>Выберите "Новый набор вариантов" если вы хотите создать новый набор вариантов. Ели хотите добавить вариант в уже имеющий набор, выберете его.</p>') );		
			( jQuery("div#ajax-response").after('<p>Вариации позволяют раширить возможности для вашей продукции, например, если вы продаете футболки, то они будут иметь опцию размера, можно создать это как вариацию товара. Размер будет имя Варианта набора. После этого создаются варианты (маленький, средний, большой), которые будут иметь "Набор варианта" размера. После того как вы сделали свой набор, можно управлять ими (изменять и удалять).</p>') );
		</script>
	<?php
	}
	
	function save( $term_id, $tt_id )
	{			
		$thumbnail_id = !empty($_POST['thumbnail'])?absint($_POST['thumbnail']):0;
		update_term_meta( $term_id, 'thumbnail', $thumbnail_id );
	}
}
?>