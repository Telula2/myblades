<?php
require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
class USAM_List_Table extends WP_List_Table 
{		
	protected $total_items;
	protected $bulk_actions = true;
	protected $per_page = 20;		
	
	protected $select = array();
	protected $where = array();
	protected $joins = array();	
	protected $search = '';
	protected $orderby = '';
	protected $order = 'desc';
	protected $limit = '';
	protected $prefix = '';	
	protected $pimary_id = 'id'; // Основной столбец с номером
	
	protected $footer_bar = true;
	protected $search_box = true;	
	protected $filter_box = true;	
	protected $sortable = true;
	protected $views = true;	
	protected $standart_button = true;	
	protected $records = array();
	protected $url = array();	
	
	protected $page = '';	
	protected $tab = '';	
	protected $search_title = '';	
	
	protected $id;
	public    $items = array();
	
	protected $start_date_interval;
	protected $end_date_interval;
	
	protected $period = 'year';
	protected $time_calculation_interval_start;
	protected $time_calculation_interval_end;
	protected $groupby_date = 'month';	
	
	function __construct( $args = array() )
	{		
		$this->search_title = __('Поиск', 'usam');	
		$this->get_orderby();
		
		if ( !empty( $_REQUEST['s'] ) )
		{
			global $wpdb;
			$this->search =  esc_sql( $wpdb->esc_like( trim($_REQUEST['s'])));
		}
		if ( !empty( $_REQUEST['cb'] ) )
		{
			if ( is_array($_REQUEST['cb'])) 	
				$records = array_values( $_REQUEST['cb'] );			
			else 	
				$records = explode( ',', $_REQUEST['cb'] );	
			foreach ( $records as $record ) 
			{            
			   $this->records[] = sanitize_title( $record );
			}	
		}
		elseif ( isset($_REQUEST['id']) )				
			$this->records = array( sanitize_title($_REQUEST['id']) );	
			
		if ( isset($_REQUEST['id']) )				
			$this->id = sanitize_title($_REQUEST['id']);				
					
		if ( !empty($_GET['page']) )
			$this->page = sanitize_title($_GET['page']);
		if ( !empty($_GET['tab']) )			
			$this->tab = sanitize_title($_GET['tab']);
		
		$this->url = admin_url( 'admin.php' );		
		
		if ( $this->page )
			$this->url  = add_query_arg( array('page' => $this->page ), $this->url  );	
		if ( $this->tab )
			$this->url  = add_query_arg( array('tab' => $this->tab ), $this->url  );
		
		if ( !empty($_GET['table']) )
			$this->url  = add_query_arg( array('table' => $_GET['table'] ), $this->url  );
		
		parent::__construct( $args );		
    }	
	
	protected function set_date_period() 
	{
		$this->period = !empty($_REQUEST['period'])?sanitize_title($_REQUEST['period']):$this->period;
		if ( isset($_REQUEST['groupby_date']) )
			$this->groupby_date = sanitize_title($_REQUEST['groupby_date']);
		
		switch ( $this->period ) 
		{					
			case 'today':
			case 'yesterday':
			case 'week':	
				if ( $this->groupby_date != 'day'  )
					$this->groupby_date = 'day';
			break;				
		}				
		if ( ! empty( $_REQUEST['date_from_interval'] ) )
		{
			$array = explode('-', $_REQUEST['date_from_interval']);
			$date_from = mktime(0, 0, 0, $array[1] , $array[0],$array[2]);			
		}
		else 
		{
			switch ( $this->period ) 
			{					
				case 'today':
					$date_from = mktime(0, 0, 0, date("m") , date("d"), date("Y"));
				break;
				case 'yesterday':
					$date_from = mktime(0, 0, 0, date("m") , date("d")-1, date("Y"));
				break;
				case 'week':
					$date_from = mktime(0, 0, 0, date("m") , date("d")-7, date("Y"));
				break;
				case 'month':
					$date_from = mktime(0, 0, 0, date("m")-1, date("d"), date("Y"));				
				break;				
				case 'year':
				default:				
					$date_from = mktime(0, 0, 0, date("m") , date("d"), date("Y")-1);
				break;
			}			
		}					
		if ( ! empty( $_REQUEST['date_to_interval'] ) )
		{
			$array = explode('-', $_REQUEST['date_to_interval']);
			$date_to = mktime(23, 59, 59, $array[1] , $array[0],$array[2]);	
		}
		else 
		{		
			$date_to = mktime(23, 59, 59, date('m'),date('d'),date('Y'));				
		}		
		$this->start_date_interval = date('Y-m-d H:i:s', $date_from);					
		$this->end_date_interval = date("Y-m-d H:i:s", $date_to);	
		
		switch ( $this->groupby_date ) 
		{					
			case 'day':	
				$this->time_calculation_interval_start = $date_to;	
				$this->time_calculation_interval_end = $date_from;	
			break;
			case 'week':	
				$w = date("w", $date_to);	
				switch ( $w ) 
				{					
					case 0:
						$this->time_calculation_interval_start = $date_to - 6*86400;							
					break;	
					case 1:		
						$this->time_calculation_interval_start = $date_to - 604800;										
					break;				
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
						$this->time_calculation_interval_start = $date_to - ($w-1)*86400;												
					break;
				}
				$w = date("w", $date_from);	
				switch ( $w ) 
				{					
					case 0:					
						$this->time_calculation_interval_end = $date_from - 6*86400;						
					break;	
					case 1:								
						$this->time_calculation_interval_end = $date_from - 604800;							
					break;				
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:						
						$this->time_calculation_interval_end = $date_from - ($w-1)*86400;									
					break;
				}						
			break;
			case 'month':						
				$this->time_calculation_interval_start = mktime(0,0,0,date('m',$date_to),1, date('Y',$date_to));	
				$this->time_calculation_interval_end = mktime(0,0,0,date('m',$date_from),1,date('Y',$date_from));				
			break;
			case 'year':				
				$this->time_calculation_interval_start = mktime(0,0,0,1,1, date('Y',$date_to));
				$this->time_calculation_interval_end = mktime(0,0,0,1,1,date('Y',$date_from));		
			break;				
		}	
	}
	
	protected function get_orderby() 
	{		
		if ( isset($_REQUEST['orderby']) )
		{
			$sortable_columns = $this->get_sortable_columns();		
			foreach( (array)$sortable_columns as $key => $columns )
			{
				if ( is_array($columns) && $_REQUEST['orderby'] == $columns[0] || $_REQUEST['orderby'] == $columns )
				{
					$this->orderby = sanitize_title($_REQUEST['orderby']);				
					break;
				}
			}
		}
		elseif ( $this->orderby == '' )
			$this->orderby = $this->pimary_id;
			
		if ( $this->prefix != '' )
			$this->orderby = $this->prefix.'.'.$this->orderby;
		
		if ( isset($_REQUEST['order']) )
			$this->order = usam_product_order( $_REQUEST['order'] );			
	}
	
	protected function get_standart_query_parent( )
	{	
		$offset = ($this->get_pagenum() - 1) * $this->per_page;		
		$this->limit = ( $this->per_page !== 0 ) ? "LIMIT {$offset}, {$this->per_page}" : '';		
			
		$prefix = $this->prefix != '' ? $this->prefix.'.':'';
		
		if ( in_array($this->orderby,$this->get_number_columns_sql()))
			$this->orderby = "CAST({$prefix}{$this->orderby} AS SIGNED)";
	
		if ( !empty( $this->records ) )
			$this->where[] = $prefix.$this->pimary_id." IN ('" . implode( "','", $this->records ) . "')";			
		else
			$this->where = array("1 = '1'");			
	}
	
	protected function get_filter_tablenav( ) 
	{
		return array();
	}	
	
	// Получить html всех фильтров
	function get_html_filter_tablenav()
	{
		$filters = $this->get_filter_tablenav();
		$html = '';
		foreach ( $filters as $filter => $value )
		{
			$method = $filter.'_dropdown';
			
			if( method_exists($this, $method) )
			{ 
				$data = isset($value['data'])?$value['data']:null;
				$title = isset($value['title'])?$value['title']:'';
				
				ob_start();
				$this->$method( $data );
				$return = ob_get_contents();
				ob_end_clean();					
				
				$html .= $this->filter_box( $return, $title );
			}
		}				
		return $html;
	}	
	
	protected function terms_dropdown( ) 
	{
		$filter_manage = new USAM_Product_Filter_Manage();			 
		$filter_manage->terms_products_search( array( 'usam-category', 'usam-brands', 'usam-category_sale' ) );
	}
	
	public function filter_box( $html, $title = '' ) 
	{
		$title = $title!=''?'<span class="title">'.$title.'</span>':'';
		return '<div class="usam_filter">'.$title.$html.'</div>';
	}
	
	public function filter_table(  ) 
	{		
		$html = $this->get_html_filter_tablenav();			
		if ( $this->filter_box && $html )
		{ 
			echo '<div class="usam_filter_table">';
			echo '<div class="header_filter"><h2>';		
				echo __('Фильтр','usam');	
				echo '<div class="control_buttons">';					
					submit_button( __( 'Фильтр', 'usam' ), 'secondary control_button', false, false, array( 'id' => 'usam-query-submit' ) );	
					echo '<a id="cancel" class="button control_button" href="'.$this->url.'">'.__( 'Отменить', 'usam' ).'</a>';	
				echo '</div>';
			echo '</h2></div>';
			echo '<div class="content_filter">';
			echo '<div class="content_box">'.$html.'</div>';				
			echo '</div></div>';	
		}
	}
	
	// Формирование таблицы
	protected function forming_tables( )
	{	
		$this->get_order();
		$this->_column_headers = $this->get_column_info();
		
		if ( empty($this->items) )
			return;
	
		$current = current($this->items);
		if ( is_object($current) )
			usort( $this->items, array( $this, 'usort_reorder' ) );	
		else
			usort( $this->items, array( $this, 'usort_reorder_array' ) );			
	
		$this->items = array_slice( $this->items, ($this->get_pagenum() - 1)*$this->per_page ,$this->per_page);
		$this->set_pagination_args( array(	'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}	
	
	public function usort_reorder( $a, $b ) 
	{			
		$orderby = $this->orderby;		
		if ( in_array($this->orderby, $this->get_number_columns_sql()))
		{			
			if ( $this->order === 'ASC')		
			{				
				if ( $a->$orderby > $b->$orderby )			
					return 1;				
			}
			else 				
			{				
				if ( $a->$orderby < $b->$orderby )			
					return 1;
			}									
			return 0;			
		}
		else
		{		
			$result = strcmp( $a->$orderby, $b->$orderby );	
			return ( $this->order === 'ASC' ) ? $result : -$result;
		}		
	}
	
	public function usort_reorder_array( $a, $b ) 
	{			
		$orderby = $this->orderby;		
		if ( in_array($this->orderby, $this->get_number_columns_sql()))
		{			
			if ( $this->order === 'ASC')		
			{				
				if ( $a[$orderby] > $b[$orderby] )			
					return 1;				
			}
			else 				
			{				
				if ( $a[$orderby] < $b[$orderby] )			
					return 1;
			}									
			return 0;			
		}
		else
		{		
			$result = strcmp( $a[$orderby], $b[$orderby] );	
			return ( $this->order === 'ASC' ) ? $result : -$result;
		}		
	}
	
	public function get_number_columns_sql()
    {       
		return array();
    }
	
	function logical_column( $logical ) 
	{		
		$logical == 1?_e('Да','usam'):_e('Нет','usam');
	}
		
	function no_items() 
	{
		_e( 'Ничего не найдено', 'usam' );
	}
		
	function column_active( $item ) 
	{		
		if ( is_object($item) )
			$logical = $item->active;	
		else
			$logical = $item['active'];
		$this->logical_column( $logical );
	}
	
	function column_color( $item )
	{		
		if ( !empty($item->color) )
		{
			$style = $item->color != ''?'style="background:'.$item->color.'"':'';
			echo "<span class ='usam_color_view' $style></span>";
		}
	}
	
	function column_type_prices( $item ) 
    {		
		$prices = usam_get_prices( );	
		$i = 0;
		foreach ( $prices as $price )
		{ 
			if ( in_array($price['code'], $item['type_prices']) )
			{			
				if ( $i > 0 )
					echo '<hr size="1" width="90%">';
				echo $price['title'].' ( '.$price['code'].' )';				
				$i++;
			}
		}
	}
	
	function column_manager( $item ) 
	{		
		if ( is_object($item) )
		{
			$id = $item->manager_id;			
		}
		else
		{
			$id = $item['manager_id'];			
		}
		if ( $id )
		{
			$user = get_user_by('id', $id );			
			$url = add_query_arg( array( 'manager' => $id, 'page' => $this->page, 'tab' => $this->tab ), wp_get_referer() );	
			?>
			<a href="<?php echo $url; ?>"><?php echo $user->display_name; ?></a>		
			<?php	
		}
	}
	
	function column_customer( $item ) 
	{		
		if ( is_object($item) )		
			echo usam_get_customer_name( $item->user_id );
		else
			echo usam_get_customer_name( $item['user_id'] );
	}
		
	function column_date( $item )
    {			
		if ( is_object($item) )
			$date = $item->date_insert;
		elseif ( is_array($item) )
			$date = $item['date_insert'];			
		if ( !empty($date) )
		{			
			$timestamp = strtotime( $date );	
			$full_time = usam_local_date( $date );
			$time_diff = time() - $timestamp;
			if ( $time_diff > 0 && $time_diff < 86400 ) // 24 * 60 * 60
				$h_time = sprintf( __( '%s назад' ), human_time_diff( $timestamp, time() ) );
			else
				$h_time = $full_time;
			
			echo '<abbr title="'.$full_time.'">' . $h_time . '</abbr>';
		}
	}
	
	function column_date_update( $item )
    {		
		if ( is_object($item) )
			echo usam_local_date( $item->date_update );
		elseif ( is_array($item) )
			echo usam_local_date( $item['date_update'] );
	}
	
	protected function column_default( $item, $column_name ) 
	{		
		if ( is_object($item) && isset($item->$column_name) )
			$output = $item->$column_name;
		elseif ( is_array($item) && isset($item[$column_name]) )
			$output = $item[$column_name];
		else
			$output = '';
		return $output;
	}
	
	public function extra_tablenav_display( $which ) 
	{	
	
	}
	
	public function extra_tablenav( $which ) 
	{		
		if ( $this->bulk_actions )
			$this->extra_tablenav_display( $which );
	}	
	// Массовые действия
	function get_bulk_actions_display() 
	{
		return array();
	}
	// Массовые действия
	function get_bulk_actions() 
	{
		if ( $this->bulk_actions )
			$bulk_actions = $this->get_bulk_actions_display();
		else
			$bulk_actions = array();	
		return $bulk_actions;
	}
	
	protected function format_description( $description ) 
	{		
		return "<span class='item_description'>".esc_html( usam_limit_words($description))."</span>";
	}
		
	protected function column_cb( $item ) 
	{				
		$column = $this->pimary_id;
		if ( is_object($item) )
		{
			$checked = in_array($item->$column, $this->records )?"checked='checked'":"";
			echo "<input id='checkbox-".$item->$column."' type='checkbox' name='cb[]' value='".$item->$column."' ".$checked."/>";	
		}
		else
		{ 
			$checked = in_array($item[$column], $this->records )?"checked='checked'":"";
			echo "<input id='checkbox-".$item[$column]."' type='checkbox' name='cb[]' value='".$item[$column]."' ".$checked."/>";				
		}
    }	
	
	protected function column_description( $item ) 
	{		
		if ( is_object($item) )
		{
			$description = $item->description;
		}
		else
		{ 
			$description = $item['description'];			
		}
		echo $this->format_description( $description );
	}	
	
	function column_drag( $item )
	{		
		if ( is_object($item) )
		{
			$id = $item->id;
		}
		else
		{ 
			$id = $item['id'];			
		}
		?>
		<div id ="dragging" class="ui-sortable-handle" data-id='<?php echo $id; ?>'>
			<a title="<?php esc_attr_e( 'Нажмите и перетащите, чтобы поменять местами', 'usam' ); ?>">
				<img src="<?php echo esc_url( USAM_CORE_IMAGES_URL . '/drag.png' ); ?>" />
			</a>				
		</div>
		<?php	
	}			
	
	protected function item_view( $id, $text )
	{			
		return '<a class="row-title" href="'.esc_url( add_query_arg( array('action' => 'view', 'id' => $id), $this->url ) ).'">'.$text.'</a>';
	}
	
	function standart_row_actions( $id ) 
	{					
		if ( $this->current_action() != 'delete' )
		{ 
			$actions = array(
				'edit'      => $this->add_row_actions( $id, 'edit', __( 'Изменить', 'usam' ) ),
				'delete'    => $this->add_row_actions( $id, 'delete', __( 'Удалить', 'usam' ) ),
			);
		}
		else
			$actions = array(); 
		return $actions;
	}	
	
	function row_actions_table( $value, $actions ) 
	{	
		if ( $this->bulk_actions )
			echo sprintf('%1$s %2$s', $value, $this->row_actions( $actions ) );
		else
			echo $value;		
	}
	
	protected function delete_url( $id ) 
	{
		$url = $this->add_row_actions( $id, 'delete', __( 'Удалить', 'usam' ) );
		return $url;
	}
	
	protected function get_nonce_url( $url )
	{
		global $current_screen;		
		return wp_nonce_url( $url, 'usam-'.$current_screen->id);		
	}
	
	protected function item_url( $id ) 
	{		
		$url = add_query_arg( array('id' => $id ), $this->url );		
		$url = $this->get_nonce_url($url);
		return $url;
	}
	
	function add_row_actions( $id, $action, $title_action, $args = array() )
    {		
		$args['action'] = $action;
		$url = add_query_arg( $args, $this->item_url( $id ) );	
		return '<a class="usam-'.$action.'-link" href="'.$url.'">'.$title_action.'</a>';		
    }

	public function disable_sortable() {
		$this->sortable = false;
	}

	public function disable_search_box() {
		$this->search_box = false;
	}
	
	public function disable_standart_button() {
		$this->standart_button = false;
	}
	
	public function disable_filter_box() {
		$this->filter_box = false;
	}	

	public function disable_bulk_actions() {
		$this->bulk_actions = false;
	}

	public function disable_views() {
		$this->views = false;
	}

	public function set_per_page( $per_page )
	{		
		$this->per_page = (int) $per_page;		
	}
	
	public function is_pagination_enabled() {
		return $this->per_page !== 0;
	}

	public function is_sortable() {
		return $this->sortable;
	}

	public function is_views_enabled() {
		return $this->views;
	}

	public function is_search_box_enabled() 
	{
		return $this->search_box;
	}	
	
	public function disable_footer_bar() {
		$this->footer_bar = false;
	}
	
	public function get_footer_bar() {
		return $this->footer_bar;
	}
	
	public function get_per_page(  )
	{				
		return $this->per_page;
	}
		
	public function search_box( $text, $input_id ) 
	{
		if ( ! $this->search_box )
			return '';

		parent::search_box( $text, $input_id );
	}
	
	public function standart_button(  ) 
	{
		if ( $this->standart_button )
		{
			$this->print_button();	
			$this->excel_button();		
		}
	}
	
	public function get_date_for_query() 
	{
		$args = array();
		if ( ! empty( $_REQUEST['calendar_period'] ) )	
		{				
			if ( ! empty( $_REQUEST['date_direction'] ) )				
				switch ( $_REQUEST['date_direction'] ) 
				{						
					case 'current' :
						$direction = 'current';
					break;	
					case 'previous' :						
					default :
						$direction = 'previous';
					break;					
				}					
			$date_from = strtotime(usam_get_datepicker('from'));			
			$date_to = strtotime(usam_get_datepicker('to'));
			switch ( $_REQUEST['calendar_period'] ) 
			{
				case 'day' :
					$day = date('d');
					$month = date('m');
					$year = date('Y');
					if ( $direction == 'previous' )
					{
						if ( $day-1 == 0 )
						{
							$month = $month-1;
							if ( $month == 0 )
							{
								$month = 12;
								$year = $year - 1;
							}
						}
						else	
							$day = $day-1;
					}									
					$args['date_query'] =  array( 'year'  => $year, 'month'  => $month, 'day'  => $day );				
				break;
				case 'week' :					
					if ( $direction == 'previous' )
						$week = date('W')-1;
					else
						$week = date('W');					
					$args['date_query'] =  array( 'year'  => date('Y'), 'week'  => $week );		
				break;
				case 'hour' :					
					if ( !empty( $_REQUEST['hour'] ) )	
					{						
						$hour = array_map( 'intval', $_REQUEST['hour'] );						
						$args['date_query'] = array( 'hour' => $hour, 'compare' => 'IN' );		
					}
				break;
				case 'weekday' :							
					if ( !empty( $_REQUEST['weekday'] ) )	
						$weekday = array_map( 'intval', $_REQUEST['weekday'] );
					else
						$weekday = array( );
					$args['date_query'] = array( 'dayofweek' => $weekday, 'compare' => 'IN' );						
				break;
				case 'month' :
					$month = date('m');
					$year = date('Y');
					if ( $direction == 'previous' )
					{
						if ( $month -1 == 0 )
						{
							$month = 12;
							$year = $year - 1;
						}
						else	
							$month = $month-1;
					}									
					$args['date_query'] =  array( 'year'  => $year, 'month'  => $month );
				break;
				case 'quarter' :						
					$y = date('Y' );
					if ( $direction == 'previous' )
					{
						$month = date('m')-3;
						if ( $month < 0 )
						{
							$month = 10;
							$y = date('Y' )-1;
						}
					}
					else
						$month = date('m');					
			
					$quarter = usam_get_beginning_quarter( $month, $y );									
					$end_quarter = mktime( 0,0,0,date('m',$quarter)+2,1, $y);	
					
					$args['date_query'] = array(
						'after'     => array( 'year'  => date('Y',$quarter), 'month' => date('m',$quarter), 'day' => 1 ),
						'before'    => array( 'year'  => date('Y',$end_quarter), 'month' => date('m',$end_quarter), 'day' => date('t', $end_quarter) ),
						'inclusive' => true,
					);					
				break;					
				case 'year' :
					if ( $direction == 'previous' )
						$year = date('Y')-1;
					else
						$year = date('Y');
					$args['date_query'] =  array( 'year'  => $year );
				break;					
				case 'exact' :
					$args['date_query'] =  array( 'year'  => date('Y',$date_from), 'month' => date('m',$date_from), 'day' => date('d',$date_from) );
				break;
				case 'before' :
					$args['date_query'] = array(						
						'before'    => array( 'year'  => date('Y',$date_from), 'month' => date('m',$date_from), 'day' => date('d',$date_from) ),
						'inclusive' => true,
					);
				break;
				case 'after' :
					$args['date_query'] = array(
						'after'     => array( 'year'  => date('Y',$date_from), 'month' => date('m',$date_from), 'day' => date('d',$date_from) ),							
						'inclusive' => true,
					);
				break;
				case 'interval' :
					$args['date_query'] = array(
						'after'     => array( 'year'  => date('Y',$date_from), 'month' => date('m',$date_from), 'day' => date('d',$date_from) ),
						'before'    => array( 'year'  => date('Y',$date_to), 'month' => date('m',$date_to), 'day' => date('d',$date_to) ),
						'inclusive' => true,
					);
				break;
			}								
		}		
		return $args;
	}
	
	public function print_button(  ) 
	{	
		$url = add_query_arg( array('action' => 'print'), $_SERVER['REQUEST_URI'] );			
		$url = $this->get_nonce_url( $url );
		?>		
		<a target='blank' href="<?php echo $url; ?>" class = "button button-export"><?php _e( 'Распечатать', 'usam' ); ?></a>
		<?php 
	}
	
	public function excel_button(  ) 
	{	
		$url = add_query_arg( array('action' => 'excel'), $_SERVER['REQUEST_URI'] );			
		$url = $this->get_nonce_url( $url );
		?>		
		<a target='blank' href="<?php echo $url; ?>" class = "button button-export" title="<?php esc_attr_e( 'Импортировать в эксель', 'usam' ) ?>"><?php _e( 'Excel', 'usam' ); ?></a>
		<?php 
	}
	
	public function download_csv_button(  ) 
	{	
		$url = add_query_arg( array('action' => 'download_csv'), $_SERVER['REQUEST_URI'] );			
		$url = $this->get_nonce_url( $url );
		?>		
		<a target='blank' href="<?php echo $url; ?>" class = "button button-export" title="<?php esc_attr_e( 'Импортировать в csv', 'usam' ) ?>"><?php _e( 'CSV', 'usam' ); ?></a>
		<?php 
	}
	
	public function interval_dropdown( $default ) 
	{	
		$interval_from = usam_get_datepicker('from_interval');
		if ( $interval_from == '' )
		{
			$interval_from = !empty($default['from'])?$default['from']:$this->start_date_interval;
		}		
		$interval_to = usam_get_datepicker('to_interval');	
		if ( $interval_to == '' )
		{
			$interval_to = !empty($default['to'])?$default['to']:$this->end_date_interval;
		}			
		usam_display_date_interval( $interval_from, $interval_to );
	}
	
	public function interval_group_dropdown( $default ) 
	{	
		$this->date_interval_toolbar();
	}
	
	public function groups_by_date_dropdown( $default ) 
	{		
		$groupby_date = empty($default)?$this->groupby_date:$default;
		$groupby_date = isset( $_REQUEST['groupby_date'] ) ? sanitize_text_field($_REQUEST['groupby_date']):$groupby_date;
		?>
		<select name="groupby_date">
			<option <?php selected($groupby_date, 'day'); ?> value='day'><?php _e( 'По дням', 'usam' ); ?></option>
			<option <?php selected($groupby_date, 'week'); ?> value='week'><?php _e( 'По неделям', 'usam' ); ?></option>
			<option <?php selected($groupby_date, 'month');  ?> value='month'><?php _e( 'По месяцам', 'usam' ); ?></option>	
			<option <?php selected($groupby_date, 'year');  ?> value='year'><?php _e( 'По годам', 'usam' ); ?></option>			
		</select>
		<?php
	}
	
	public function calendar_dropdown() 
	{	
		$calendar_from = usam_get_datepicker('from');		
		$calendar_to   = usam_get_datepicker('to');			
	
		$date_direction   = isset( $_REQUEST['date_direction'] ) ? sanitize_text_field($_REQUEST['date_direction']) : '';		
		$calendar_period = isset( $_REQUEST['calendar_period'] ) ? sanitize_text_field($_REQUEST['calendar_period']) : '';		
		$select_weekday = isset( $_REQUEST['weekday'] ) ? stripslashes_deep($_REQUEST['weekday']) : '';	
		$select_hour = isset( $_REQUEST['hour'] ) ? stripslashes_deep($_REQUEST['hour']) : '';
		?>
		<div class="usam_filter-box-sizing">
			<span class="usam_select-wrap usam_calendar-period">
				<select class="usam_calendar-period" id="filter_date_from_calendar_period" name="calendar_period" title="<?php _e( 'Выбор периода', 'usam' ); ?>">
					<option <?php echo ($calendar_period == ''?'selected':''); ?> value=""><?php _e( '(нет)', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'hour'?'selected':''); ?> value="hour"><?php _e( 'Час', 'usam' ); ?></option>	
					<option <?php echo ($calendar_period == 'day'?'selected':''); ?> value="day"><?php _e( 'День', 'usam' ); ?></option>					
					<option <?php echo ($calendar_period == 'week'?'selected':''); ?> value="week"><?php _e( 'Неделя', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'weekday'?'selected':''); ?> value="weekday"><?php _e( 'День недели', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'month'?'selected':''); ?> value="month"><?php _e( 'Месяц', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'quarter'?'selected':''); ?> value="quarter"><?php _e( 'Квартал', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'year'?'selected':''); ?> value="year"><?php _e( 'Год', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'exact'?'selected':''); ?> value="exact"><?php _e( 'Точно', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'before'?'selected':''); ?> value="before"><?php _e( 'Раньше', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'after'?'selected':''); ?> value="after"><?php _e( 'Позже', 'usam' ); ?></option>
					<option <?php echo ($calendar_period == 'interval'?'selected':''); ?> value="interval"><?php _e( 'Интервал', 'usam' ); ?></option>
				</select>
			</span>
			<?php $class = $calendar_period == 'day' || $calendar_period == 'week' || $calendar_period == 'month' || $calendar_period == 'quarter' || $calendar_period == 'year' ?'':'hide'; ?>
			<span class="usam_select-wrap usam_calendar-direction <?php echo $class; ?>">
				<select class="usam_select usam_calendar-direction" id="date_direction" name="date_direction" title="<?php _e( 'Уточнение периода', 'usam' ); ?>">
					<option <?php echo ($date_direction == 'previous'?'selected':''); ?> value="previous"><?php _e( 'Предыдущий', 'usam' ); ?></option>
					<option <?php echo ($date_direction == 'current'?'selected':''); ?> value="current"><?php _e( 'Текущий', 'usam' ); ?></option>					
				</select>
			</span>
			<?php $class = $calendar_period != 'weekday'?'hide':''; ?>
			<span class="usam_select-wrap usam_calendar-weekday <?php echo $class; ?>">				
				<select multiple class="usam_select usam_calendar-direction" id="weekday" name="weekday[]" title="<?php _e( 'Уточнение периода', 'usam' ); ?>">
					<?php 				
					$weekday = array( '2' => __('Понедельник','usam'), '3' => __('Вторник','usam'), '4' => __('Среда','usam'), '5' => __('Четверг','usam'), '6' => __('Пятница','usam'), '7' => __('Суббота','usam'), '1' => __('Воскресение','usam') );
					foreach( $weekday as $key => $name )
					{
						if ( is_array($select_weekday) && in_array( $key, $select_weekday ) || $key == $select_weekday )
							$select = 'selected';
						else
							$select = '';
						echo "<option $select value='$key'>$name</option>";
					}
					?>		
				</select>
			</span>
			<?php $class = $calendar_period != 'hour'?'hide':''; ?>
			<span class="usam_select-wrap usam_calendar-hour <?php echo $class; ?>">				
				<select multiple class="usam_select usam_calendar-direction" id="hour" name="hour[]" title="<?php _e( 'Уточнение периода', 'usam' ); ?>">
					<?php 
					for ($i = 0; $i<24; $i++)
					{					
						if ( is_array($select_hour) && in_array( $i, $select_hour ) || $i == $select_hour)
							$select = 'selected';
						else
							$select = '';
						if ( $i < 10 )
							$time = "0$i";
						else
							$time = $i;
						echo "<option $select value='$i'>$time</option>";
					}
					?>		
				</select>
			</span>
			<?php $class = $calendar_period == 'exact' || $calendar_period == 'before' || $calendar_period == 'after' || $calendar_period == 'interval' ?'':'hide'; ?>			
			<div class="usam_input-wrap usam_calendar-first <?php echo $class; ?>">
				<?php usam_display_date_picker( 'from', $calendar_from ); ?>
			</div>
			<?php $class = $calendar_period == 'interval' ?'':'hide'; ?>		
			<span class="usam_calendar-separate <?php echo $class; ?>"></span>
			<?php $class = $calendar_period == 'interval' ?'':'hide'; ?>		
			<div class="usam_input-wrap usam_calendar-second <?php echo $class; ?>">
				<?php usam_display_date_picker( 'to', $calendar_to ); ?>
			</div>
		</div>
		<?php
	}
	
	public function date_interval_toolbar(  ) 
	{		
		?>		
		<div class="date_interval_toolbar">
			<div class="date-selector date-selector_report_comparable">
				<div class="period-selector date-selector__period i-bem period-selector_js_inited">
					<span class="buttons_radio radio-button_size_s button_radio_theme radio-button_view_classic i-bem radio-button_js_inited">
						<label class="button_radio button_radio_side_left <?php echo $this->period=='today'?'button_radio_checked_yes':''; ?>" for="_period_today">
							<input class="button_radio_control" value="today" <?php checked($this->period, 'today'); ?> autocomplete="off" id="_period_today" type="radio" name="period">
							<span class="radio-button__text"><?php _e('Сегодня', 'usam') ?></span>
						</label>
						<label class="button_radio <?php echo $this->period=='yesterday'?'button_radio_checked_yes':''; ?>" for="_period_yesterday">
							<input class="button_radio_control" value="yesterday" <?php checked($this->period, 'yesterday'); ?> autocomplete="off" id="_period_yesterday" type="radio" name="period">
							<span class="radio-button__text"><?php _e('Вчера', 'usam') ?></span>
						</label>
						<label class="button_radio <?php echo $this->period=='week'?'button_radio_checked_yes':''; ?>" for="_period_week">
							<input class="button_radio_control" value="week" <?php checked($this->period, 'week'); ?> autocomplete="off" id="_period_week" type="radio" name="period">
							<span class="radio-button__text"><?php _e('Неделя', 'usam') ?></span>
						</label>
						<label class="button_radio <?php echo $this->period=='month'?'button_radio_checked_yes':''; ?>" for="_period_month">
							<input class="button_radio_control" value="month" <?php checked($this->period, 'month'); ?> autocomplete="off" id="_period_month" type="radio" name="period"><span class="radio-button__text"><?php _e('Месяц', 'usam') ?></span>
						</label>					
						<label class="button_radio radio-button__radio_side_right <?php echo $this->period=='year'?'button_radio_checked_yes':''; ?>" for="_period_year">
							<input class="button_radio_control" value="year" <?php checked($this->period, 'year'); ?> autocomplete="off" id="_period_year" type="radio" name="period"><span class="radio-button__text"><?php _e('Год', 'usam') ?></span>
						</label>
					</span>
				</div>
				<div class="date-selector__period">
					<span class="date-range-selector date-range-selector_report_comparable i-bem date-range-selector_js_inited">
						<?php usam_display_date_interval( $this->start_date_interval, $this->end_date_interval, 'interval', true, false ); ?>
					</span>
				</div>			
				<div class="period-group-selector date-selector__period i-bem period-group-selector_js_inited">
					<select class="chzn-select-nosearch select__control chosen" id="_groupby_date" name="groupby_date" tabindex="-1" aria-hidden="true" autocomplete="off">
						<option class="select__option" <?php selected($this->groupby_date, 'day'); ?> value="day"><?php _e('по дням', 'usam') ?></option>
						<option class="select__option" <?php selected($this->groupby_date, 'week'); ?> value="week"><?php _e('по неделям', 'usam') ?></option>
						<option class="select__option" <?php selected($this->groupby_date, 'month'); ?> value="month"><?php _e('по месяцам', 'usam') ?></option>
					</select>				
				</div>
			</div>	
		</div>
		<?php		
	}
	
	public function return_post()
	{
		return array( );
	}	
	
	public function display_table()
    {
		$default_parents = array( 'status', 'n', 'table' );
		
		if ( $this->search )
			printf( '<h3 class="search_title">' . __( 'Результаты поиска &#8220;%s&#8221;' ) . '</h3>', esc_html( $this->search ) );
			
		$this->prepare_items();	
		$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );
		?>				
		<div class='usam_tab_table'>
			<form method='GET' action='' id='usam_table_filter_form'>
				<input type='hidden' value='<?php echo $this->page; ?>' name='page' />
				<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />					
				<?php
				$args_get = $this->return_post();
				$args_get = array_merge ($default_parents, $args_get);	
				foreach( $args_get as $get)
				{					
					if ( isset($_GET[$get]) )
					{	
					?>		
						<input type='hidden' value='<?php echo sanitize_title($_GET[$get]); ?>' name='<?php echo $get; ?>' />				
					<?php
					}
				}		
				if ( $this->is_views_enabled() )
					$this->views(); 
				
				if ( $this->is_search_box_enabled() )						
					$this->search_box( $this->search_title, 'search_id' );				
				
				$this->filter_table( );
				?>
			</form>
			<form method='<?php echo $this->form_method; ?>' action='' id='usam-tab_form'>
				<input type='hidden' value='<?php echo $this->page; ?>' name='page' />
				<input type='hidden' value='<?php echo $this->tab; ?>' name='tab' />				
				<?php				
				$args_get = $this->return_post();
				$args_get = array_merge ($default_parents, $args_get);	
				foreach( $args_get as $get)
				{					
					if ( isset($_REQUEST[$get]) )
					{	
					?>		
						<input type='hidden' value='<?php echo sanitize_title($_REQUEST[$get]); ?>' name='<?php echo $get; ?>' />				
					<?php
					}
				}					
				do_action( 'usam_list_table_before' );	
				?>
				<div class='usam_list_table_wrapper'>
					<?php $this->display(); ?>
				</div>
				<?php do_action( 'usam_list_table_after' );	?>
			</form>
		</div>
		<?php
	}	
}
?>