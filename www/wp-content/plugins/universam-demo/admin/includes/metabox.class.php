<?php
/**
 * Метабокс 
 */
class USAM_Meta
{	
	public function __construct() 
	{
		add_action( 'add_meta_boxes', array( $this, 'create_custombox') ); 
		add_action( 'save_post', array( $this,'save_meta' ) );
	}
	
	public static function create_custombox()
	{		
		add_meta_box( 'usam_search_metabox', __('Ключевые слова поиска', 'usam'), array('USAM_Meta','data_metabox'), 'post', 'normal', 'low' );
		add_meta_box( 'usam_search_metabox', __('Ключевые слова поиска', 'usam'), array('USAM_Meta','data_metabox'), 'page', 'normal', 'low' );
		add_meta_box( 'usam_search_metabox', __('Ключевые слова поиска', 'usam'), array('USAM_Meta','data_metabox'), 'usam-product', 'normal', 'low' );
		
		$meta_box = array( 
						array( 'box' => array(
										'id' => 'customer-reviews',
										'title' => '<img src="'.USAM_CORE_IMAGES_URL.'/star_reviews.png" />&nbsp;'.__("Отзывы клиентов","usam"),							
										'context' => 'normal',
										'priority' => 'high',
										'function' => array('USAM_Meta', 'reviews_meta_box'),
									),
							'type_page' => array( 'page', 'post' ),
						),											
					);
					
		foreach($meta_box as $box) 
			foreach($box['type_page'] as $type_page)
			{				
				add_meta_box($box['box']['id'], $box['box']['title'], $box['box']['function'], $type_page, $box['box']['context'], $box['box']['priority']);				
			}
	}
	
	public static function data_metabox()
	{
		global $post;	
		$hide = usam_get_product_meta($post->ID, 'search_hide' );	
		$search = usam_get_product_search( $post->ID );	
		$str = implode(', ',$search);
		?>    	
    	<table class="form-table" cellspacing="0">
        	<tr valign="top">
				<th scope="rpw" class="titledesc"><label for="usam_search_focuskw"><?php _e('Ключевые слова', 'usam'); echo usam_help_tip(__('Введите слова через запятую','usam')); ?></label></th>
				<td class="forminp">
					<div class="wide_div">
						<input type="text" value="<?php echo $str; ?>" id="usam_search_focuskw" name="usam_search_focuskw" style="width:98%;" />
					</div>					
				</td>
				<td class="forminp" width ="100px">
					<?php echo '<label><input type="checkbox" '.checked( $hide ).' value="1" name="_usam_search_exclude_item" /> '.__('Скрыть', 'usam').'</label>'.usam_help_tip(__('Скрыть от результатов поиска','usam')); ?>
				</td>
			</tr>
        </table>       
		<?php		
	}
	
	public static function save_meta( $post_id )
	{
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) 
		{
			return $post_id;
		}
		if (!current_user_can('edit_post', $post_id)) 
			return $post_id;
		
		$post_status = get_post_status($post_id);		
		if( $post_status != false  && $post_status != 'inherit') 
		{								
			if ( empty($_REQUEST['_usam_search_exclude_item']) )			
				usam_delete_product_meta($post_id, 'search_hide' );				
			else
				usam_update_product_meta($post_id, 'search_hide', 1 );	
			
			if ( empty($_REQUEST['usam_search_focuskw']) )
			{
				usam_delete_product_meta($post_id, 'search' );			
			}
			else
			{				
				$product_search = usam_get_product_search( $post_id );		
				
				$search_words = explode(",",$_REQUEST['usam_search_focuskw']);				
				foreach ( $search_words as $search )
				{
					$search = trim($search);					
					if ( !in_array($search, $product_search) )
					{						
						if ( !empty($search) )
							usam_add_product_meta($post_id, 'search', $search, false);					
					}
					else
					{
						$key = array_search($search, $product_search);				
						unset($product_search[$key]);	
					}
				}
				foreach ( $product_search as $search )
				{
					usam_delete_product_meta($post_id, 'search', $search );	
				}
			}			
			if ( isset($_REQUEST['usam_reviews_enable']) && $_REQUEST['usam_reviews_enable'] == 1 )		
				usam_update_product_meta($post_id, 'reviews_enable', 1 );
			else
				usam_delete_product_meta($post_id, 'reviews_enable', 1 );				
		}
		return $post_id;
	}
	
	public static function reviews_meta_box()
	{
		global $meta_box, $post;
		
		$meta = usam_get_product_meta($post->ID, "reviews_enable", true); 	
		?>    	
    	<table class="form-table" cellspacing="0">
        	<tr valign="top">
				<th scope="rpw" class="titledesc">
					<label for="customer_reviews_enable"><?php _e('Включить отзывы', 'usam'); ?></label>
				</th>
				<td class="forminp">
					<div class="wide_div">
						<input value="1" type="checkbox" name="usam_reviews_enable" <?php echo $meta == 1?'checked="checked"':''; ?> id="customer_reviews_enable">
					</div>					
				</td>
			</tr>			
        </table>       
		<?php	
	}	  	
}
$meta = new USAM_Meta();
?>