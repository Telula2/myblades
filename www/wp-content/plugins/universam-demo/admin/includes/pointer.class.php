<?php
/**
 * Указатели
 * @version     2.1.0
 */

//new USAM_Pointer();
class USAM_Pointer 
{	
	private $version = '';
	public function __construct() 
	{			
		add_action( 'admin_print_footer_scripts', array( $this, 'init' ), 30 );	
	}
	
	/**
	 * Загрузка скриптов
	 */
	public function init( ) 
	{		
		global $current_screen, $user_ID;				
		if ( isset($current_screen->id) )
		{
			$pointer_contents = false;
			switch ( $current_screen->id ) 
			{	
				case 'usam-product':			
					$pointer_contents = array( 
						array( 'name' => 'usam_attributes_forms h2 span', 'position' => 'left', 'header' => __('Создание описание товара','usam'),'content' => __('Добавьте данные о товаре, такие как характеристики, комплектация, фотографии','usam') ),
						array( 'name' => 'usam_prices_forms h2 span', 'position' => 'left', 'header' => __('Цена товара','usam'),'content' => __('Укажите цену, если цена связанная, то она ра считается, как процент от основной указанный в настройках.','usam') ),			
					);
				break;			
				case 'edit-usam-product':
					
				break;		
			}			
			if ( $pointer_contents != false )
			{ 
				$user_pointer = get_user_option( $user_ID, 'usam_pointer' );	
				?>
				<script type='text/javascript'>
				jQuery(document).ready(function()
				{	
					// <! [CDATA [ 
					<?php
						foreach ( $pointer_contents as $pointer ) 
						{
							if ( empty($user_pointer[$current_screen->id][$pointer['name']]) )
							{
								?>								
									$('#<?php echo $pointer['name']; ?>').pointer({
										content: '<div class="usam_pointer"><h3><?php echo $pointer['header']; ?></h3><p><?php echo $pointer['content']; ?></p></div>',
										position: '<?php echo $pointer['position']; ?>',
										close: function() 
										{											
											post_data   = {
												action  : 'pointer_close',			    
												screen  : '<?php echo $current_screen->id; ?>',
												name    : '<?php echo $pointer['name']; ?>',
												nonce   : '<?php echo usam_create_ajax_nonce( 'pointer_close' ); ?>'
											},
											ajax_callback = function(response)
											{					
												console.log(response);
												if (! response.is_successful) 
												{
													alert(response.error.messages.join("\n"));
													return;
												}					
											};	
											$.usam_post(post_data, ajax_callback);													
										}
									}).pointer('open');
								<?php					
							}
						}
					?>
					//]]> 
				});
				</script>
				<?php
			}
		}
	}
}
?>