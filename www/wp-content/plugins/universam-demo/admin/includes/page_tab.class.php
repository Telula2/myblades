<?php
abstract class USAM_Page_Tab
{		
	protected $page_name = '';
	protected $tab;
	protected $list_table = null;		
	protected $table = null;
	protected $sendback = array();	
 
	protected  $current_action = '';
	protected  $redirect   = false;
	protected  $search_box = true;
	protected  $views      = true;
	protected  $item_table;
	protected  $per_page = 60;	
	protected  $message_error = '';
	protected  $header = array();	
	protected  $buttons = array();	
	protected  $records = array();
	protected  $id = 0;		
	protected  $display_save_button = false;	
	
	protected  $form_method = 'GET';	
		
	public function __construct( ) { }		
	protected function load_tab(){ }	
	protected function callback_submit(){ }	
	public function display( ) { }		
	
	public function add_meta_options_help_center_tabs() 
	{		
		return array();
	}		
	
	public function load( $page, $tab ) 
	{				
		$this->page_name = $page;
		$this->tab       = $tab;
		$this->submit_url( );				
				
		$per_page_option = strtolower("{$this->page_name}_{$this->tab}");			
		set_current_screen($per_page_option);	
		
		$this->load_tab();		
		$this->process_bulk_action(); // менять с load_tab нельзя	

		do_action( 'usam_page_load', $this->page_name, $this->tab );
		
		add_action( 'admin_enqueue_scripts', array($this, 'include_js'), 4 );				
		add_action('admin_notices',   array($this, 'custom_bulk_admin_notices') );		
	}	
	
	public function get_video_url() 
	{
	//	return $this->get_argument( 'video_url' );
	}
	
	// Подготовка массива данных перед сохранением
	public function array_data_preparation( $data ) 
	{	
		$new_data = array();
		foreach ( $data as $key => $value )
		{			
			$new_data[$key] = htmlspecialchars( stripslashes(trim($value)), ENT_QUOTES);
		}		
		return $new_data;
	}
	
	protected function nonce_field()
	{
		global $current_screen;	
		wp_nonce_field('usam-'.$current_screen->id,'usam_name_nonce_field');		
	}	
	
	public function verify_nonce()
    {				
		global $current_screen;	
			
		if ( empty($_REQUEST['usam_name_nonce_field']) || wp_verify_nonce( $_REQUEST['usam_name_nonce_field'], 'usam-'.$current_screen->id ) )
			return true;
		else
			return false;
	}
	
	protected function get_nonce_url( $url )
	{
		global $current_screen;		
		return wp_nonce_url( $url, 'usam-'.$current_screen->id);		
	}
		
	// Вывести центр помощи
	public function display_help_center() 
	{		
		require_once( USAM_FILE_PATH . '/admin/includes/help_center/help_center.class.php' );
		$help_center = new USAM_Help_Center( $this->page_name, $this );
		$help_center->output_help_center();
	}
	
	public function display_title() 
	{		
		$title_tab = $this->get_title_tab( );				
		if ( isset($title_tab['title']) && $title_tab['title'] != '' )
		{			
			?><h2><?php
				echo $title_tab['title'];	
				foreach ( $this->buttons as $action => $title )
				{									
					$url = $this->get_nonce_url( add_query_arg( array('action' => $action, 'page' => $this->page_name, 'tab' => $this->tab ), admin_url( 'admin.php' )) );
					if ( $this->table )
						$url = add_query_arg( array( 'table' => $this->table ), $url );	
					?><a href="<?php echo $url; ?>" id="buttom-<?php echo $action; ?>" class="add-new-h2"><?php echo $title; ?></a><?php 
				}
			?></h2><?php 				
			if ( $title_tab['description'] != '' ) 
			{ 
				?><p class = "description"><?php echo $title_tab['description']; ?></p><?php 
			}			
		}
	}
	
	// Отображение текущей вкладки
	public function display_tab() 
	{		
		?>
		<div id="tab_<?php echo esc_attr( $this->tab ); ?>" class="tab-content tab_<?php echo esc_attr( $this->tab ); ?>">
			<?php $this->tab_structure();	?>			
		</div>
		<?php
	}
		
	public function tab_structure() 
	{			
		if ( is_object($this->item_table) )
		{
			$this->item_table->display_form();
		}
		elseif ( !empty($this->list_table) )
		{
			$this->display_title();	
			$this->display_help_center();	
			$this->display_tables_select();		
			$this->list_table->display_table();		
		}
		else
		{
			$this->display_title();	
			$this->display_help_center();	
			$this->output_tab();	
		}
	}	
	
	protected function output_tab()
	{	
		if ( $this->display_save_button )
		{
			?>
			<form method='POST' action='' id='usam-tab_form'>
				<?php 
				$this->nonce_field();
				$this->display(); 
				?>
				<div class="tab_buttons">
					<?php submit_button( __( 'Сохранить', 'usam' ), 'primary', 'submit', false, array( 'id' => 'action-submit' ) );	?>
					<input type="hidden" name="form" value="submit_button" />
					<input type="hidden" name="update" value="1" />										
				</div>
			</form>
			<?php
		}
		else
			$this->display();
	}
	
	public function display_tables_select() 
	{
		$tables = $this->get_tables();

		$tables = apply_filters( "usam_tables_select_{$this->table}", $tables );

		if ( empty( $tables ) )
			return;
		
		echo "<div class = 'display_panel'>";
		echo "<ul>\n";
		foreach ( $tables as $key => $table ) 
		{
			$all_class = ( $this->table == $key ) ? 'class="current"' : '';			
			$view = sprintf('<a href="%s" %s>%s</a>', esc_url( admin_url("admin.php?page={$this->page_name}&tab={$this->tab}&table={$key}") ), $all_class, $table['title'] );			
			$tables[ $key ] = "\t<li class='$key'>$view";
		}
		echo implode( " |</li>\n", $tables ) . "</li>\n";
		echo "</ul>";
		echo "</div>";		
	}
	
	protected function localize_script_tab()
	{	
		return array();
	}		

	private function table_item(  )
    {		
		if ( isset($_REQUEST['action']) )
		{ 
			$action = sanitize_title($_REQUEST['action']);
			if ( $action == 'add' )
				$action = 'edit';			
						
			$screen = isset($_REQUEST['screen'])?sanitize_title($_REQUEST['screen']):$this->table;				
			$this->item_table = usam_get_class_form_display( $this->page_name, $screen, $action );
			if ( is_object($this->item_table) )
			{	
				unset($_REQUEST['action']);						
				return true;	
			}
		}
		return false;
	}
	
	protected function get_file_name_table( )
    {
		$this->table = $this->tab;
		$filename = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/list_table/'.$this->table.'_list_table.php';
		if ( !empty($_GET['table']) )
		{ 
			$this->table = sanitize_title($_GET['table']);
			$filename = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/list_table/'.$this->table.'_list_table.php';	
		}			
		return $filename;
	}
	
	public function list_table( )
    {				
		global $wp_list_table;
		
		$filename = $this->get_file_name_table();		
		if ( !file_exists($filename) ) 
			return false;	
		
		$this->display_save_button = false;
		
		if ( !$this->table_item( ) )
		{		
			require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );		
			require_once( $filename );				
		
			$option = 'per_page';	
			$per_page_option = strtolower("{$this->page_name}_{$this->tab}");		
			$args = array( 'label' => __('Записей','usam'), 'default' => $this->per_page, 'option' => $per_page_option );
						
			add_screen_option( $option, $args );
			
			$args = array(
				'singular'  => $this->page_name,    
				'plural'    => $this->table,  
				'ajax'      => true,
				'screen'    => $per_page_option,
			);				
			$name_class_table = 'USAM_List_Table_'.$this->table;
			$this->list_table = $wp_list_table = new $name_class_table( $args );
			
			$per_page = $this->list_table->get_items_per_page($per_page_option, $this->per_page);			
			$per_page = apply_filters( $per_page_option, $per_page );			
			$this->list_table->set_per_page( $per_page );						
					
			if ( !empty( $_REQUEST['confirm'] ) )
			{ 
				add_action( 'usam_list_table_before', array( $this, 'bulk_delete_confirm' ) );
				$this->list_table->disable_bulk_actions();		
				$this->list_table->disable_search_box();	
				$this->list_table->disable_standart_button(); 				
				$this->list_table->disable_views();		
				$this->list_table->disable_filter_box();	
			}		
		}			
	}
	
	public function get_list_table(  )
    {
		return $this->list_table;
	}
	
	public function product_list_table()
    {			
		require_once( USAM_FILE_PATH .'/admin/includes/product_list_table.php' );		
		$this->list_table();	
	}	
	
	protected function button_dropdown( ) 
	{	
		?>
		<div class = "panel_buttons">	
			<input type="submit" class="button button-primary button-large" value="<?php esc_attr_e('Распечатать','usam'); ?> " name = "print">
			<input type="submit" class="button button-primary button-large" value="<?php esc_attr_e('Импортировать','usam'); ?> " name = "export">
		</div>
		<?php
	}
	
	public function bulk_delete_confirm( ) 
	{	
		?>
			<h3><?php esc_html_e( 'Вы уверены, что хотите удалить эти позиции?', 'usam' ); ?><br /></h3>
			<div>
				<a href="<?php echo esc_url( wp_get_referer() ); ?>" class="button"><?php esc_html_e( 'Назад', 'usam' ); ?></a>
				<input class="button-primary" type="submit" value="<?php esc_attr_e( 'Удалить', 'usam' ); ?>" />
				<input type="hidden" name="confirm" value="1" />
				<input type="hidden" name="action" value="delete" />
			</div>
		<?php
	}
	
	public function get_tables( ) { return array(); }	
	
	public function set_user_screen_error( $message_error ) 
	{		
		global $current_screen;
		if ( $message_error != '' )
			usam_set_user_screen_error( $message_error, $current_screen->id );
	}
	
	public function set_user_screen_message( $message ) 
	{		
		global $current_screen;
		if ( $message != '' )
			usam_set_user_screen_message( $message, $current_screen->id );
	}
	
	private function display_message( $messages, $type = 'message' ) 
	{
		global $current_screen;	
		
		if ( $type == 'message' )		
			$screen_message = usam_get_user_screen_message( $current_screen->id );
		else		
			$screen_message = usam_get_user_screen_error( $current_screen->id );	
		
		if ( !empty($screen_message) )
		{
			if ( is_array( $screen_message ))
				$messages = array_merge($messages, $screen_message);		
			else
				$messages[] = $screen_message;
		}	
		if ( !empty($messages) )		
		{
			foreach ( $messages as $message )
			{
				if ( $type == 'message' )	
					echo "<div class=\"updated\"><p>{$message}</p></div>";
				else
					echo "<div class=\"error\"><p>{$message}</p></div>";
			}
		}		
	}	

	public function custom_bulk_admin_notices() 
	{		
		$messages = array();
		$errors = array();
		
		$message = $this->get_message();	
		if( $message != '' )	
		{
			if ( is_array( $message ))
				$messages = array_merge($messages, $message);		
			else
				$messages[] = $message;			
		}		
		$error = $this->get_message_error();	
		if( $error != '' )	
		{
			if ( is_array( $error ))
				$errors = array_merge($errors, $error);		
			else
				$errors[] = $error;
		}	
		if( isset($_REQUEST['send_email']) && $_REQUEST['send_email'] == 1 )		
			$messages[] = __( 'Сообщение отправлено', 'usam' );	
		
		if( isset($_REQUEST['send_sms']) && $_REQUEST['send_sms'] == 1 )		
			$messages[] = __( 'Сообщение отправлено', 'usam' );	
		
		if( isset($_REQUEST['update']) )		
			$messages[] = __( 'Настройки сохранены', 'usam' );	
		if( isset($_REQUEST['deleted']) )
		{
			$deleted = absint( $_REQUEST['deleted'] );	
			$messages[] = sprintf( _n( 'Удалена %s запись.', 'Удалено %s записей.', $deleted, 'usam' ), $deleted );		
		}
		if( isset($_REQUEST['updated']) )
		{
			$updated = absint( $_REQUEST['updated'] );	
			$messages[] = sprintf( _n( 'Изменена %s запись.', 'Изменена %s записей.', $updated, 'usam' ), $updated );			
		}		
		if( isset($_REQUEST['update_product']) )
		{
			$updated = absint( $_REQUEST['update_product'] );	
			$messages[] = sprintf( _n( 'Обновлен %s товар.', 'Обновлено %s товаров.', $updated, 'usam' ), $updated );		
		}
		if( isset($_REQUEST['add_product']) )
		{
			$updated = absint( $_REQUEST['add_product'] );	
			$messages[] = sprintf( _n( 'Добавлен %s товар.', 'Добавлено %s товаров.', $updated, 'usam' ), $updated );		
		}		
		if( isset($_REQUEST['ready']) )
		{
			$messages[] = __( 'Готово!', 'usam' );		
		}	
// Стандартные сообщения об ошибках		
		if( isset($_REQUEST['send_email']) && $_REQUEST['send_email'] == 0 )		
			$errors[] = __( 'Сообщение не отправлено. Пожалуйста, убедитесь, что ваш сервер может отправлять сообщения электронной почты.', 'usam' );
		
		if( isset($_REQUEST['send_sms']) && $_REQUEST['send_sms'] == 0 )		
			$errors[] = __( 'Сообщение не отправлено. Пожалуйста, убедитесь, что вы правильно настроили смс шлюз.', 'usam' );				
	
		$this->display_message( $messages, 'message' );
		$this->display_message( $errors, 'error' );		
	
	}
	
	protected function get_export_table()
	{			
		$class_name_table = 'USAM_Export_List_Table_'.$this->table;
		$name_file_table = $this->table .'_export_list_table';		
		require_once( USAM_FILE_PATH .'/admin/includes/usam_export_list_table.class.php' );	
		
		$export_list_table_file = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/export_list_table/'.$name_file_table.'.php';	
		if ( !file_exists($export_list_table_file) )		
		{
			$class_name_table = 'USAM_Export_List_Table_'.$this->page_name;
			$name_file_table = $this->page_name .'_export_list_table';			
			$export_list_table_file = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/export_list_table/'.$name_file_table.'.php';	
			if ( file_exists($export_list_table_file) )		
				require_once( $export_list_table_file );
			else
				$class_name_table = 'USAM_Export_List_Table';					
		}	
		else
		{					
			require_once( $export_list_table_file );	
		}
		$args = array( 'class_table' => $this->list_table );	
		$print_list_table = new $class_name_table( $args );	
		
		return $print_list_table;		
	}
	
	protected function get_message_error()
	{	
		return $this->message_error;
	}

	protected function get_message()
	{	
		return '';
	}
		
	function help_tabs() 
	{	
		$help = array( 	);
		return $help;
	}
	
	function set_help_tabs() 
	{	
		$help_tabs = $this->help_tabs();
		
		$help = new USAM_Help_Tab();
		$help->set_help_tabs( $help_tabs );
	}
	
	public function get_tab() 
	{			
		return $this->tab;
	}
	
	// Получить название и описание вкладки
	function get_title_tab() 
	{			
		return $this->header;
	}
		
	public function return_post()
	{
		return array( );
	}	
	
	protected function submit_url( )
	{
		//$this->sendback = admin_url( 'admin.php' );
		//$this->sendback = $_SERVER['REQUEST_URI'];	
		
		$this->sendback = add_query_arg( array('page' => $this->page_name, 'tab' => $this->tab ) );
		$this->sendback = remove_query_arg( array('update', 'deleted', 'updated' ), $this->sendback  );			
		$this->sendback = remove_query_arg( array('_wpnonce', '_wp_http_referer', 'usam_name_nonce_field', 'action', 'action2' ), $this->sendback  );	
		$this->post_in_get();
	}	
	
	public function post_in_get()
	{		
		$posts = $this->return_post();			
		$default = array( 'status', 'n', 'table', 's',  'usam-category', 'usam-brands', 'usam-category_sale');
		
		$posts = array_merge ($default, $posts);					
		$mas = array();
		foreach( $posts as $post)
		{
			if ( isset($_POST[$post]) && $_POST[$post] != '' )
			{
				$mas[$post] = sanitize_text_field($_POST[$post]);
			}						
		}			
		if ( !empty($mas) )
		{					
			$this->sendback = $_SERVER['REQUEST_URI'] = add_query_arg( $mas , $this->sendback );				
		}					
	}
	
	public function process_bulk_action()
    {	
		if( isset($_REQUEST['usam_name_nonce_field']) )
			$nonce = $_REQUEST['usam_name_nonce_field'];
		elseif( isset($_REQUEST['_wpnonce']) )
			$nonce = $_REQUEST['_wpnonce'];
		else
		{					
			return;
		}	
		global $current_screen;	
//check_admin_referer( 'update-options', 'usam-update-options' );					
		if ( !wp_verify_nonce( $nonce, 'usam-'.$current_screen->id ) )
		{
		//	echo 'Проверка не прошла';		
		//	wp_redirect( remove_query_arg( array('_wp_http_referer', '_wpnonce', 'action', 'action2'), stripslashes($_SERVER['REQUEST_URI']) ));
		//	exit;			  
		}		
		if (isset($this->list_table))					
			$this->current_action = $this->list_table->current_action();	
		elseif ( !empty($_REQUEST['action']) )
			$this->current_action = sanitize_title( $_REQUEST['action'] );
			
		if ( isset($_REQUEST['cb']) ) 		
		{
			if ( is_array($_REQUEST['cb'])) 	
				$records = $_REQUEST['cb'];			
			else 	
				$records = explode( ',', $_REQUEST['cb'] );	
			
			foreach ( $records as $record ) 
			{            
			   $this->records[] = sanitize_title( $record );
			}	
		}
		elseif ( isset($_REQUEST['id']) ) 
			$this->records[] = $this->id = sanitize_title($_REQUEST['id']);		
	
		if ( !empty($this->current_action) )
			$this->redirect = true;	// Если нет действия, например, нажать фильтр		

		switch( $this->current_action )
		{				
			case 'delete':					
				if ( empty($this->records) )
					return;		
				
				$action_delete = empty( $_REQUEST['confirm'] )?false:true;
				
				if ( apply_filters( 'usam_action_delete_items_table', $action_delete  ) ) 
				{								
					$this->callback_submit();					
					$this->sendback = remove_query_arg( array('cb', 'id', 'confirm' ), $this->sendback  ); 
				}
				else
				{												
					$this->sendback = remove_query_arg( array('s' ), $this->sendback  );
					$this->sendback = add_query_arg( array( 'action' => 'delete', 'confirm' => 1 ), $this->sendback );
					if ( $this->form_method == 'POST' )
						$this->sendback = add_query_arg( array( 'cb' => join(',', $this->records) ), $this->sendback );
				}
				$this->redirect	= true;
			break;			
			case 'save':					
				$this->callback_submit();	
				if ( $this->id != null && empty($_REQUEST['save-close']) )
				{
					$this->sendback = add_query_arg( array( 'action' => 'edit', 'id' => $this->id ), $this->sendback );		
					if ( $this->table )
						$this->sendback = add_query_arg( array( 'table' => $this->table ), $this->sendback );	
				}					
				else
					$this->sendback = remove_query_arg( array( 'action', 'id' ), $this->sendback );												
			break;
			case 'copy':
				$this->callback_submit();	
				if ( $this->id != null )
					$this->sendback = add_query_arg( array( 'action' => 'edit', 'id' => $this->id ), $this->sendback );						
			break;
			case 'print':		
				$print_list_table = $this->get_export_table();		
		
				$title_tab = $this->get_title_tab( );
				$title_page = $title_tab['title'];			
		
				require_once( USAM_FILE_PATH .'/admin/includes/form/printing_table_form.php' );			
				exit;
			break;			
			case 'excel':								
				$this->list_table->set_per_page(0);	
				$this->list_table->prepare_items();	

				$excel_list_table = $this->get_export_table();					
			
				list( $columns, $hidden, $sortable, $primary ) = $this->list_table->get_column_info();	
				if ( isset($columns['cb']) )
					unset($columns['cb']);
		
				$excel_items = array();				
				$items = $this->list_table->items;	
				foreach ( $items as $item )
				{				
					$excel_items[] = $excel_list_table->get_row_columns( $item );				
				}
		
				$title_tab = $this->get_title_tab( );
				$title_page = $title_tab['title'];			

				require_once( USAM_FILE_PATH . '/admin/includes/exported.class.php' );
				$exported = new USAM_Exported_Table();	
				$exported->excel( $excel_items, $columns );	
			break;
			case 'screen':	
				$this->callback_submit();
				$this->sendback = add_query_arg( array( 'action' => $this->current_action ), $this->sendback );	
			break;			
			default:
				if ( !empty($this->buttons[$this->current_action]) )
					$this->sendback = add_query_arg( array( 'action' => $this->current_action ), $this->sendback );		
				else
					$this->sendback = remove_query_arg( array('cb', 'action', 'record', 'id' ), $this->sendback  );				
				
				$this->callback_submit();	
			break;
		}	
		if ( !empty($_REQUEST['next']) )
		{
			$step = !empty($_REQUEST['step'])?$_REQUEST['step']:1;
			$step++;
			$this->sendback = add_query_arg( array( 'step' => $step ), $this->sendback );
		}		
		if ( $this->redirect )
		{ 
			wp_redirect( $this->sendback );
			exit;
		}				
	}
	
	// Вернуть правила условий корзины
	public function get_rules_basket_conditions( ) 
	{				
		require_once( USAM_FILE_PATH . '/admin/includes/rules/basket_discount_rules.class.php' );	
			
		$rules_work_basket = new USAM_Basket_Discount_Rules( );	
		return $rules_work_basket->get_rules_basket_conditions(  );	
	}
	
	protected function enqueue_scripts()
    {
		
	}
	
	public function include_js()
    {	
		if ( defined('WP_DEBUG') && WP_DEBUG  )
			$version = time();		
		else
			$version = USAM_VERSION;	
		
		wp_print_scripts( 'jquery' );	

		wp_enqueue_script( 'hc-sticky' );		
		
		wp_enqueue_script( 'usam-admin-tab', USAM_URL . '/admin/js/tabs/tabs.js', array( 'jquery-query' ), $version );
		wp_localize_script( 'usam-admin-tab', 'Universam_Tabs', array(			
			'navigate_tab_nonce'                  => usam_create_ajax_nonce( 'navigate_tab' ),	
			'save_nav_menu_metaboxes_nonce'       => usam_create_ajax_nonce( 'save_nav_menu_metaboxes' ),	
			'add_comment_nonce'                   => usam_create_ajax_nonce( 'add_comment' ),
			'edit_comment_nonce'                  => usam_create_ajax_nonce( 'edit_comment' ),
			'delete_comment_nonce'                => usam_create_ajax_nonce( 'delete_comment' ),	
			'id'                                  => isset($_GET['id'])?$_GET['id']:0,		
			'current_tab'                         => $this->tab,				
			'current_page'                        => $this->page_name,						
			'url'                                 => add_query_arg( array( 'page' => $this->page_name, 'tab' => $this->tab ), admin_url('admin.php') ),	
			'edit_button_text'                    => __( 'Изменить', 'usam' ),
			'before_unload_dialog'                => __( 'Внесенные изменения будут потеряны, если вы уйдете с этой страницы.', 'usam' ),
			'ajax_navigate_confirm_dialog'        => __( 'Внесенные изменения будут потеряны, если вы уйдете с этой страницы.', 'usam' )."\n\n".__( 'Нажмите OK, чтобы отменить изменения, или Отмена, чтобы оставаться на этой странице.' )
		) );	
		
		$this->enqueue_scripts();
		
		$file_js = '/admin/js/tabs/'.$this->page_name.'.js';
		if ( file_exists(USAM_FILE_PATH.$file_js) ) 
		{ 			
			$js_class = 'USAM_Page_'.str_replace("-", "_", $this->page_name);
			wp_enqueue_script( 'usam-admin-tab-'.$this->page_name, USAM_URL.$file_js, array( 'jquery-query' ), $version );		
			
			$data = $this->localize_script_tab();			
			wp_localize_script( 'usam-admin-tab-'.$this->page_name, $js_class, $data );			
		}				
		$file_css = '/admin/css/tabs/'.$this->page_name.'.css';
		if ( file_exists(USAM_FILE_PATH.$file_css) ) 
			wp_enqueue_style( 'usam-help-center-admin'.$this->page_name, USAM_URL.$file_css, false, $version, 'all' );	
		
		wp_enqueue_script( 'usam-admin-help_center', USAM_URL . '/admin/js/help_center.js', array( ), $version );
		wp_localize_script( 'usam-admin-help_center', 'Universam_Help_Center', array(			
			'send_support_message_nonce'          => usam_create_ajax_nonce( 'send_support_message' ),	
			'get_support_messages_nonce'          => usam_create_ajax_nonce( 'get_support_messages' ),	
		) );		
		wp_enqueue_style( 'usam-help-center-admin', USAM_URL .'/admin/css/help-center.css', false, $version, 'all' );				
		wp_enqueue_style( 'usam-tabs-admin', USAM_URL .'/admin/css/tabs.css', false, $version, 'all' );			
	} 	
}
?>