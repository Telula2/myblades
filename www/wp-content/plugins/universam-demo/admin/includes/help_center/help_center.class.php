<?php
class USAM_Help_Center
{
	private $help_center_items = array();
	private $group_name;
	private $tab;

	public function __construct( $group_name, $tab ) 
	{
		require_once( USAM_FILE_PATH . '/admin/includes/help_center/help_center_item.class.php' );
		
		$this->group_name = $group_name;
		$this->tab        = $tab;

		$this->add_video_tutorial_item();
	//	$this->add_hc_search_item();
		$this->add_contact_support_item();
		$this->add_support_message_item();		
		$this->add_about_item();				
	}
	
	private function hc_search_html() 
	{	
		$html = '<div class="usam-kb-search">
		<div data-reactroot="" class="usam-kb-search-container">
			<div>
				<div class="usam-kb-search-search-bar">
					<h2>'.__('Поиск по базе знаний','usam').'</h2>
					<form method="POST" action="">
						<input type="text"><button type="submit" class="button usam-kb-search-search-button">'.__('Поиск','usam').'</button>
					</form>
				</div>
			</div>
		</div>
		</div>';
		return $html;
	}

	private function add_hc_search_item() 
	{
		$kb_help_center_item = new USAM_Help_Center_Item(
			'knowledge-base',
			__( 'База знаний', 'usam' ),
			array(
				'content'        => $this->hc_search_html(),
				'view_arguments' => array( 'identifier' => $this->tab->get_title_tab() ),
			),
			'dashicons-search'
		);
		array_push( $this->help_center_items, $kb_help_center_item );
	}
	
	/**
	 * Контакт центр.
	 */
	private function contact_support_html() 
	{	
		$types = usam_get_subject_support_message();
		$subject = '';	
		$html = '		
		<table>
			<tr>
				<td>'.__( 'Тип сообщения','usam' ).':</td>
				<td>
				<select id="support_subject" name="type">';
					foreach( $types as $key => $title )
					{						
						$html .= "<option value='$key'>$title</option>";
					}
				$html .= '</select></td>
			</tr>			
			<td>'.__( 'Сообщение','usam' ).':</td>		
			<td><textarea id="support_response" name="response" rows="6" id="response"/></textarea></td>
			</tr>
			<tr>
		<td colspan="2">'
		.usam_get_loader( ).get_submit_button( __( 'Отправить','usam' ), 'primary','action_send_message', false, array( 'id' => 'send-email-submit' ) )
		.'<td></tr></table>';		
		return $html;
	}
	
	private function support_message_html() 
	{				
		$html = '<div id = "support_messages_body" class = "support_messages_body"></div>'.usam_get_loader( );		
		return $html;
	}
	
	private function support_about_html() 
	{				
		$html = '<table>
			<tr>
				<td>'.__( 'Сайт','usam' ).':</td>
				<td><a target="_blank" href="' . esc_url( 'wp-universam.ru' ) . '">wp-universam.ru</a><td>
			</tr>
			<tr>
				<td>'.__( 'Электронная почта компании','usam' ).':</td>
				<td><a href="mailto:office@wp-universam.ru">office@wp-universam.ru</a><td>
			</tr>
		</table>';
		return $html;
	}

	/**
	 * Контакт центр.
	 */
	private function add_contact_support_item() 
	{	
		$item = new USAM_Help_Center_Item('contact-support', __( 'Поддержка', 'usam' ), __( 'Отправить сообщение разработчикам', 'usam' ), array( 'content' => $this->contact_support_html() ), 'dashicons-email-alt');
		array_push( $this->help_center_items, $item );
	}
	
	private function add_about_item() 
	{	
		$item = new USAM_Help_Center_Item('about', __( 'Разработчики', 'usam' ), __( 'Разработчики', 'usam' ), array( 'content' => $this->support_about_html() ), 'dashicons-about');
		array_push( $this->help_center_items, $item );
	}
	
	private function add_support_message_item() 
	{	
		$item = new USAM_Help_Center_Item( 'support-messages', __( 'Сообщения', 'usam' ), __( 'Сообщения', 'usam' ), array( 'content' => $this->support_message_html() ), 'dashicons-email-alt');
		array_push( $this->help_center_items, $item );
	}		

	private function add_video_tutorial_item() {
		array_push( $this->help_center_items, $this->get_video_help_center_item() );
	}

	private function get_video_help_center_item() 
	{
		$url = $this->tab->get_video_url();
		if ( empty( $url ) ) {
			return null;
		}
		return new USAM_Help_Center_Item( 'video', __( 'Видео урок', 'usam' ), '',	array(	'view' => 'partial-help-center-video', 'view_arguments' => array( 'tab_video_url' => $url ),	), 'dashicons-video-alt3' );
	}

	/**
	 * Вывод помощи
	 */
	public function output_help_center() 
	{
		require_once( USAM_FILE_PATH . '/includes/technical/support_message_query.class.php'   );				
		$support_messages = usam_get_support_message_document( array('fields' => 'id','read' => 0, 'outbox' => 1) );	
		$support_messages_count = count($support_messages);		
		
		$tab_help_center_items = $this->tab->add_meta_options_help_center_tabs();
		
		$this->help_center_items = array_merge ($this->help_center_items, $tab_help_center_items);
		$help_center_items = apply_filters( 'wpseo_help_center_items', $this->help_center_items );	
		$help_center_items = array_filter( $help_center_items, array( $this, 'is_a_help_center_item' ) );			
			
		if ( empty( $help_center_items ) ) {
			return;
		}		
		$id = sprintf( 'tab-help-center-%s-%s', $this->group_name, $this->tab->get_tab() );
		?>
		<div id="usam-help_center" class="usam-help_center">
			<button type="button" class="usam-help_center_handle" aria-controls="<?php echo $id ?>"
			        aria-expanded="false">
					<?php
					if ( $support_messages_count)
					{
						?>
						<span class="dashicon-button support_messages"><?php echo __( 'Не прочитанные', 'usam' )." ($support_messages_count)"; ?></span>
					<?php } ?>
					<span class="dashicons-before dashicons-editor-help"><?php _e( 'Центр поддержки', 'usam' ) ?></span>											
				<span class="dashicons dashicons-arrow-down toggle__arrow"></span>
			</button>
			<div id="<?php echo $id ?>" class="usam-tab-video-slideout">
				<div class="usam-help-center-tabs">
					<ul>
						<?php
						$class = 'active usam-help-center-item';

						foreach ( $help_center_items as $help_center_item ) 
						{
							$id = $help_center_item->get_identifier();
							$dashicon = $help_center_item->get_dashicon();
							if ( ! empty( $dashicon ) ) {
								$dashicon = 'dashicons-before ' . $dashicon;
							}
							$link_id  = "tab-link-{$this->group_name}_{$this->tab->get_tab()}__{$id}";
							$panel_id = "tab-panel-{$this->group_name}_{$this->tab->get_tab()}__{$id}";
							?>

							<li id="<?php echo esc_attr( $link_id ); ?>" class="<?php echo $class; ?>">
								<a href="<?php echo esc_url( "#$panel_id" ); ?>"
								   class="<?php echo $id . ' ' . $dashicon?>"
								   aria-controls="<?php echo esc_attr( $panel_id ); ?>"><?php echo esc_html( $help_center_item->get_label() ); ?></a>
							</li>
							<?php
							$class = 'usam-help-center-item';
						}
						?>
					</ul>
				</div>
				<div class="help-tabs">
					<?php
					$classes = 'help-tab-content active';
					foreach ( $help_center_items as $help_center_item ) 
					{
						$id = $help_center_item->get_identifier();

						$panel_id = "tab-panel-{$this->group_name}_{$this->tab->get_tab()}__{$id}";
						?>
						<div id="<?php echo esc_attr( $panel_id ); ?>" class="<?php echo $classes; ?>">
							<h2><?php echo $help_center_item->get_name(); ?></h2>
							<div class="contextual-help-tabs-wrap contextual-help-tabs-wrap-<?php echo $id; ?>">
								<?php echo $help_center_item->get_content(); ?>
							</div>
						</div>
						<?php
						$classes = 'help-tab-content';
					}
					?>
				</div>
			</div>
		</div>
		<?php
	}

	private function is_a_help_center_item( $item ) 
	{
		return is_a( $item, 'USAM_Help_Center_Item' );
	}
}
?>