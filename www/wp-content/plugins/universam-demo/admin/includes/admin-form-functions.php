<?php
// Выбор статуса
function usam_get_status_dropdown( $statuses, $current_status, $args ) 
{
	$out = sprintf(
		'<select name="%1$s" id="%2$s" class="%3$s" %4$s>',
		/* %1$s */ esc_attr( $args['name'] ),
		/* %2$s */ esc_attr( $args['id'] ),
		/* %3$s */ esc_attr( $args['class'] ),
		/* %4$s */ $args['additional_attributes']
	);	
	foreach ( $statuses as $key => $status ) 
	{	
		$selected = $key == $current_status ? 'selected="selected"' :'';
		$out .= '<option value="'.$key.'" '.$selected.'>'.esc_html( $status ).'</option>';
	}
	$out .= '</select>';	
	return $out;
}

function usam_select_variation_file( $file_id, $variation_ids, $variation_combination_id = null )
{
	global $wpdb;
	$file_list = usam_uploaded_files();
	$unique_id_component = ((int)$variation_combination_id) . "_" . str_replace( ",", "_", $variation_ids );

	$output = "<div class='variation_settings_contents'>\r\n";
	$output .= "<span class='admin_product_notes select_product_note '>" . __( 'Выберите загружаемый файл для этого варианта', 'usam' ) . "</span>\r\n";
	$output .= "<div class='select_variation_file'>\r\n";

	$num = 0;
	$output .= "  <p>\r\n";
	$output .= "    <input type='radio' name='variation_priceandstock[{$variation_ids}][file]' value='0' id='select_variation_file{$unique_id_component}_{$num}' " . ((!is_numeric( $file_id ) || ($file_id < 1)) ? "checked='checked'" : "") . " />\r\n";
	$output .= "    <label for='select_variation_file{$unique_id_component}_{$num}'>" . __( 'Нет товаров', 'usam' ) . "</label>\r\n";
	$output .= "  </p>\r\n";

	foreach ( (array)$file_list as $file ) 
	{
		$num++;
		$output .= "  <p>\r\n";
		$output .= "    <input type='radio' name='variation_priceandstock[{$variation_ids}][file]' value='" . $file['file_id'] . "' id='select_variation_file{$unique_id_component}_{$num}' " . ((is_numeric( $file_id ) && ($file_id == $file['file_id'])) ? "checked='checked'" : "") . " />\r\n";
		$output .= "    <label for='select_variation_file{$unique_id_component}_{$num}'>" . $file['display_filename'] . "</label>\r\n";
		$output .= "  </p>\r\n";
	}
	$output .= "</div>\r\n";
	$output .= "</div>\r\n";

	return $output;
}

function usam_get_payment_gateway_dropdown( $selected = '', $args = array() ) 
{	
	$name = !empty($args['name'])?$args['name']:'payment';
	$class = !empty($args['class'])?'class="'.$args['class'].'"':'';
	$not_selected_text = !empty($args['not_selected_text'])?$args['not_selected_text']:__('Не выбрано', 'usam');
	?>			
	<select <?php echo $class; ?> name = "<?php echo $name; ?>">
		<option <?php selected( '', $selected ); ?> value=""><?php echo $not_selected_text; ?></option>
		<?php
		$gateways = usam_get_payment_gateways( );
		foreach ($gateways as $gateway )
		{
			?>
			<option value="<?php echo esc_attr( $gateway->id ); ?>" <?php selected( $gateway->id, $selected ); ?>><?php echo $gateway->name; ?></option>
			<?php
		}
		?>															
	</select>	
	<?php
}	

function usam_get_storage_dropdown( $selected = '', $args = array() ) 
{	
	$name = !empty($args['name'])?$args['name']:'storage';
	$class = !empty($args['class'])?'class="'.$args['class'].'"':'';
	$not_selected_text = !empty($args['not_selected_text'])?$args['not_selected_text']:__('Не выбрано', 'usam');
	?>			
	<select <?php echo $class; ?> name = "<?php echo $name; ?>">
		<option <?php selected( '', $selected ); ?> value=""><?php echo $not_selected_text; ?></option>
		<?php
		$stores = usam_get_stores( ); 
		foreach ($stores as $storage )
		{
			?>
			<option value="<?php echo esc_attr( $storage->id ); ?>" <?php selected( $storage->id, $selected ); ?>><?php echo $storage->title; ?></option>
			<?php
		}
		?>															
	</select>	
	<?php
}

function usam_get_delivery_service_dropdown( $selected = '', $args = array() ) 
{	
	$name = !empty($args['name'])?$args['name']:'shipping';
	$class = !empty($args['class'])?'class="'.$args['class'].'"':'';
	$not_selected_text = !empty($args['not_selected_text'])?$args['not_selected_text']:__('Не выбрано', 'usam');
	?>			
	<select <?php echo $class; ?> name = "<?php echo $name; ?>">
		<option <?php selected( '', $selected ); ?> value=""><?php echo $not_selected_text; ?></option>
		<?php
		$delivery_service = usam_get_delivery_services( array( 'active' => 'all' ) );
		foreach ($delivery_service as $service )
		{
			?>
			<option value="<?php echo esc_attr( $service->id ); ?>" <?php selected( $service->id, $selected ); ?>><?php echo $service->name; ?></option>
			<?php
		}
		?>															
	</select>	
	<?php
}

function usam_select_bank_accounts( $selected = '', $attr = array() ) 
{
	$companies = usam_get_companies( array('type' => 'own') );
	if ( empty($companies) )
		return '';
	
	$ids = array();
	$selected_companies = array();
	foreach ( $companies as $company )
	{
		$ids[] = $company->id;
		$selected_companies[$company->id] = $company;
	}
	$args = array( 'company_include' => $ids );
	$bank_accounts = usam_get_bank_accounts_company( $args );	
	if ( empty($bank_accounts) )
		return '';
	
	if ( !empty($attr['class']) )
		$attr['class'] .= " chzn-select";
	else
		$attr['class'] = "chzn-select";
	?>		
	<select 
			<?php
			foreach ( $attr as $name => $value )
				echo " $name='$value' ";
			?>>		
		<?php				
		foreach( $bank_accounts as $acc )
		{						
			$currency = usam_get_currency_name( $acc->currency );
			?>             
			<option value="<?php echo $acc->id; ?>" <?php selected($selected, $acc->id); ?> ><?php echo $selected_companies[$acc->company_id]->name." - $acc->name $currency"; ?></option>
			<?php
		}
		?>
	</select>	
	<?php	
}

function usam_select_contracts( $selected = '', $attr = array(), $args = array() ) 
{
	require_once( USAM_FILE_PATH . '/includes/crm/documents_query.class.php' );	
	$args['type'] = 'contract'; 
	$contracts = usam_get_documents( $args );	

	if ( !empty($attr['class']) )
		$attr['class'] .= " chzn-select";
	else
		$attr['class'] = "chzn-select";		
	?>		
	<select 
			<?php
			foreach ( $attr as $name => $value )
				echo " $name='$value' ";
			?>>		
		<?php			
		foreach( $contracts as $contract )
		{					
			?>             
			<option value="<?php echo $contract->id; ?>" <?php selected($selected, $contract->id); ?> ><?php printf( __('%s №%s от %s','usam'), $contract->name, $contract->number,usam_local_date( $contract->date_insert, "d.m.Y" )); ?></option>
			<?php
		}
		?>
	</select>	
	<?php
}

function usam_select_currencies( $selected = '', $attr = array() ) 
{
	?>		
	<select 
			<?php
			foreach ( $attr as $name => $value )
				echo " $name='$value' ";
			?>>		
		<?php		
		$currencies = usam_get_currencies( );
		foreach( $currencies as $currency )
		{						
			?>               
			<option value="<?php echo $currency->code; ?>" <?php selected($selected, $currency->code); ?> ><?php echo $currency->code." ($currency->name)"; ?></option>
			<?php
		}
		?>
	</select>	
	<?php
}

function usam_select_sales_area( $selected = '', $attr = array() ) 
{
	?>		
	<select 
			<?php
			foreach ( $attr as $name => $value )
				echo " $name='$value' ";
			?>>	
		<option value="" <?php selected($selected, ''); ?> ><?php _e('Все зоны', 'usam'); ?></option>			
		<?php		
		$option = get_option('usam_sales_area');
		$grouping = maybe_unserialize( $option );
		foreach( $grouping as $group )
		{						
			?>               
			<option value="<?php echo $group['id']; ?>" <?php selected($selected, $group['id']); ?> ><?php echo $group['name']; ?></option>
			<?php
		}
		?>
	</select>	
	<?php
}

function usam_select_manager( $selected = '', $attr = array(), $args = array() ) 
{	
	$class = 'chzn-select';
	if ( isset( $attr['class'] ) )
		$attr['class'] = $attr['class'].' '.$class;
	else
		$attr['class'] = $class;
	?>		
	<select 
			<?php
			foreach ( $attr as $name => $value )
				echo " $name='$value' ";
			?>>
		<option value="0" <?php echo ($selected == 0) ?'selected="selected"':''?> ><?php _e('Нет','usam'); ?></option>
		<?php	
		if ( !isset($args['orderby']) )
			$args['orderby'] = 'nicename';
		
		if ( !isset($args['role__in']) )
			$args['role__in'] = array('editor','shop_manager','administrator', 'shop_crm');
				
		$args['fields'] = array( 'ID','display_name');
		$users = get_users( $args );
		foreach( $users as $user )
		{						
			?>               
			<option value="<?php echo $user->ID; ?>" <?php echo ($selected == $user->ID) ?'selected="selected"':''?> ><?php echo $user->display_name; ?></option>
			<?php
		}
		?>
	</select>	
	<?php
}


function usam_get_select_type_md( $selected = '', $attr = array() ) 
{
	$currency = usam_get_currency_sign();
	$class = 'select_type_md';
	if ( isset( $attr['class'] ) )
		$attr['class'] = $attr['class'].' '.$class;
	else
		$attr['class'] = $class;	
	$out = "<select ";			
			foreach ( $attr as $name => $value )
				$out .= " $name='$value' ";
	$out .= ">";
	$out .= "	<option value='p' ".selected('p', $selected, false).">%</option>";
	$out .= "	<option value='f' ".selected('f', $selected, false).">$currency</option>";
	$out .= "	<option value='t' ".selected('t', $selected, false).">".esc_html__( 'Точная', 'usam')."</option>";
	$out .= "</select>";
	return $out;
}

function usam_get_select_prices( $selected = '', $attr = array(), $not = false, $args = array() ) 
{			
	if ( empty($attr['name']) )
		$attr['name'] = 'type_price';
	?>	
	<div style ="width-min: 100px; display: inline-block;">
	<select id="type_price"
		<?php	
		foreach ( $attr as $name => $value )
			echo " $name='$value' ";
		?>
	>
		<?php		
		if ( $not )
		{
			?>			
			<option value=""<?php echo ($selected == ''?'selected="selected"':''); ?>><?php echo esc_html__( 'Цены', 'usam'); ?></option>		
			<?php
		}
		$prices = usam_get_prices( $args );						
		foreach ( $prices as $value )
		{					
			?>			
			<option value="<?php echo $value['code']; ?>"<?php echo ($selected === $value['code']?'selected="selected"':''); ?>><?php echo $value['title']; ?></option>		
			<?php
		}				
		?>
	</select>
	</div>
	<?php
}

function usam_get_password_input( $password = '', $attr = array(), $echo = true ) 
{
	$password = !empty($password)?"***":'';
	$output = "<input";			
			foreach ( $attr as $name => $value )
				$output .= " $name='$value' ";				
	$output .= "value='$password' type='text'/>";
	
	if ( $echo )
		echo $output;
	else
		return $output;
}

function usam_help_tip( $text_help )
{	
	$out = "<span class = 'help_text_box'>
				<img class='help_tip' src='".USAM_CORE_IMAGES_URL."/help.png' height='16' width='16'>
				<span class = 'help_text'>$text_help</span>
			</span>";
	return $out;
}

function usam_get_form_send_message( $args = array(), $attachments = array() )
{	
	global $user_ID;
	$signature_email = usam_get_manager_signature_email();	
	$default = array( 'form_url' => usam_url_admin_action( 'send_email', admin_url('admin.php') ), 'object_id' => '', 'object_type' => '', 'customer_type' => '', 'customer_id' => '', 'to_email' => array(), 'to_select' => '', 'subject' => '', 'message' => $signature_email, 'insert_text' => array() );
	$args = array_merge( $default, $args );	
	
	$primary_mailbox = usam_get_customer_primary_mailbox_id( );
	
	$args['to_email'] = is_string($args['to_email'])?array($args['to_email']):$args['to_email'];
			
	$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name','email'), 'user_id' => $user_ID ) );	
	
	add_editor_style( USAM_URL . '/admin/css/email-editor-style.css' );		
	
	ob_start();	
	wp_editor( $args['message'], 'message_editor', array(
							'textarea_name' => 'message',
							'media_buttons'=> false,
							'teeny'=> true,						
							'textarea_rows' => 20,							
							'tinymce' => array( 'theme_advanced_buttons3' => 'invoicefields,checkoutformfields', 'remove_linebreaks' => false )
							)
				);
	$wp_editor = ob_get_contents();
	ob_end_clean();		
	
	$out  = "<div class='mailing'>";	
	$out .= '<form method="post" action="'.$args['form_url'].'">';		
	$out .= "<input type='hidden' id='object_id' name='object_id' value='".$args['object_id']."' />";
	$out .= "<input type='hidden' id='object_type' name='object_type' value='".$args['object_type']."' />";
	$out .= "<input type='hidden' id='customer_type' name='customer_type' value='".$args['customer_type']."' />";
	$out .= "<input type='hidden' id='customer_id' name='customer_id' value='".$args['customer_id']."' />";
	$out .= "<div class='mailing-mailing_wrapper'>";
	$out .= "<table class ='table_email_form widefat'>";
	$out .= "		<tr>
				<td class ='name'>".__('От кого', 'usam').":</td>
				<td><select name='from_email' id='from_email'>";				
				foreach ( $mailboxes as $mailbox )
				{		
					$out .= "<option value='$mailbox->id' ".selected($mailbox->id, $primary_mailbox, false ).">$mailbox->name ($mailbox->email)</option>";
				}					
			$out .= "</select></td>";
			$out .= "</tr>			
			<tr>
				<td class ='name'>".__('Кому', 'usam').":</td>
				<td>";
					if ( !empty($args['to_email']) ) 
					{
						$out .= "<select name='to' id='to_email'>";	
							foreach ( $args['to_email'] as $email )
							{		
								$out .= "<option value='$email' ".selected($email, $args['to_select'], false ).">$email</option>";
							}				
						$out .= '</select>';
					}		
					else
						$out .= "<input id='to_email' type='text' name='to' value=''/>
				</td>
			</tr>";		
			$out .= "<tr>
				<td class ='name'>".__('Тема', 'usam').":</td>
				<td><input id='subject' type='text' name='subject' value='".$args['subject']."'/></td>
			</tr>";
			if ( !empty($args['insert_text']) )
			{ 
				$out .= "<tr><td colspan='2'><div class = 'insert_text'><ul>";
				foreach ( $args['insert_text'] as $id => $title ) 
				{ 
					$out .= "<li id = '$id'>".$title."</li>";
				}
				$out .= "</ul></div></td></tr>";
			}
			$out .= "<tr><td colspan='2'>$wp_editor</td></tr>";			
			$out .= "<tr><td colspan='2'>
				<p>".__('Прикрепленные файлы', 'usam').":</p>".
				usam_get_form_attachments( $attachments )."</td></tr>";		
	$out .= "</table>";		
	$out .= "</div>";
	$out .= '<div class="popButton">'.get_submit_button( __( 'Отправить','usam' ), 'primary','action_send_email', false, array( 'id' => 'send-email-submit' ) ).'</div>';	
	$out .= '</form>';
	$out .= "</div>"; 
	return $out;
}

function usam_get_form_attachments( $attachments, $file_upload = true )
{			
	$out = "
	<div id = 'email_attachments' class = 'attachments'>
		<div class ='attachments-content'>
			<ul>";		
			if ( $file_upload )
			{ 			
				$out .= "<li>
					<div id='drop'>
						<a>
							".__('Прикрепить', 'usam')."<br />
							".__('файл', 'usam')."
						</a>			
						<input id='file_upload' type='file' name='upl' multiple />
					</div> 
				</li>";
			}
			if ( !empty($attachments) )
			{ 
				foreach ( $attachments as $attachment ) 
				{ 								
					if ( file_exists($attachment['path']) )
						$size = size_format( filesize($attachment['path']) );
					else
						$size = '';
							
					$out .= "<li>							
							<a href='".$attachment['url']."' title ='".$attachment['title']."' target='_blank'><div class='attachment_icon' style='background-image:url(".usam_get_icon_by_file_extension( $attachment['path'] ).");'></div></a>
							<div class='filename'>".usam_get_attachment_title( $attachment['title'], $attachment['path'] )."</div>
							<div class='filesize'><a download href='".$attachment['url']."' title ='".__('Сохранить этот файл себе на компьютер','usam')."' target='_blank'>".__('Скачать','usam')."</a>".$size."</div>
							<input type='hidden' name='attachments[]' value='".$attachment['path']."'/></li>
						</li>";
				}
			}				
	$out .= "</ul></div></div>";
	if ( $file_upload )
	{
		$delete_attachment = array( 'action' => 'delete_email_attachment', 'nonce' => usam_create_ajax_nonce('delete_email_attachment') );
		$out .= "<script>
		var fileupload_url ='".add_query_arg( array('usam_ajax_action' => 'email_fileupload', 'action' => 'usam_ajax', 'nonce' => usam_create_ajax_nonce('email_fileupload') ), admin_url( 'admin-ajax.php' ) )."';		
		var delete_attachment=". json_encode( $delete_attachment ).";		
		</script>";
	}
	return $out;
}

function usam_get_form_send_sms( )
{	
	global $user_ID;			
	
	$out  = "<div class='mailing'>";	
	$out .= '<form method="post" action="'.usam_url_admin_action( 'send_sms', admin_url('admin.php') ).'">';		
	$out .= "<div class='mailing-mailing_wrapper'>";
	$out .= "<table class ='widefat'>";
	$out .= "<tr>
				<td>".__('Кому', 'usam')."</td>
				<td><input id='phone' type='text' name='phone' value=''/></td>
			</tr>			
			<tr>
				<tr>
					<td colspan='2'><textarea rows='10' autocomplete='off' cols='40' name='message' id='message_editor' ></textarea></td>						
				</tr>				
			</tr>
		</table>";		
	$out .= "</div>";
	$out .= '<div class="popButton">'.get_submit_button( __( 'Отправить','usam' ), 'primary','action_send_sms', false, array( 'id' => 'send-sms-submit' ) ).'</div>';	
	$out .= '</form>';
	$out .= "</div>"; 
	return $out;
}

function usam_get_notification_message( $message )
{	
	return '<div class="notification_message"><p><strong>'.__( 'Уведомление', 'usam' ).': </strong>'.$message.'</p></div>';
}

function usam_notification_message( $message )
{	
	echo usam_get_notification_message( $message );
}