<?php
new USAM_Query_Admin();
final class USAM_Query_Admin
{	
	public function __construct() 
	{		
		add_filter( 'posts_search', array($this, 'posts_search'), 8, 2 );
		add_filter( 'pre_get_posts', array($this, 'product_meta_filter'), 8 );	
	}

	/* 
	Описание: сортировки
	*/
	function product_meta_filter( $query )
	{
		global $wpdb, $type_price;
		
		remove_filter( 'pre_get_posts', array( $this, 'product_meta_filter'), 8 );
		
		if ( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'usam-product' ) 
		{					
			if ( !isset($query->query_vars['meta_query']) )
				$query->query_vars['meta_query'] = array();		
				
			if (isset($_REQUEST['av']) && $_REQUEST['av'] != '' && isset($_REQUEST['a']) && $_REQUEST['a'] != '' )
			{			
				//	$query->query_vars['p'] = $_REQUEST['seach_id'];// поиск по id								
				$type = 'CHAR';	
				switch ( $_REQUEST['a'] ) 
				{
					case 'price':
						$meta_key = "price_".$type_price;
						$type = 'NUMERIC';								
					break;	
					case 'oldprice':
						$meta_key = "old_price_".$type_price;
						$type = 'NUMERIC';								
					break;				
					case 'stock':
						$meta_key = "stock";	
						$type = 'NUMERIC';								
					break;	
					case 'storage':
						$meta_key = "storage";	
						$type = 'NUMERIC';							
					break;					
					case 'sku':
						$meta_key = "sku";						
					break;	
					case 'vkid':
						$meta_key = "vk_market_id";						
					break;			
					case 'barcode':
						$meta_key = "barcode";	
						$type = 'NUMERIC';						
					break;		
					case 'code':
						$meta_key = "code";						
					break;	
					case 'weight':
						$meta_key = "weight";	
						$type = 'NUMERIC';						
					break;	
					case 'views':
						$meta_key = "product_views";	
						$type = 'NUMERIC';						
					break;	
					case 'rating':
						$meta_key = "rating";	
						$type = 'NUMERIC';						
					break;					
					default:
						$meta_key = "";
					break;	
				}		
				if ( $meta_key != '' )
					$meta_key = USAM_META_PREFIX.$meta_key;
				
				switch ( $_REQUEST['ac'] ) 
				{
					case 'b':
						$compare = ">";										
					break;	
					case 'm':
						$compare = "<";				
					break;	
					case 'notr':
						$compare = "!=";				
					break;				
					case 'not_contain':
						$compare = "NOT LIKE";				
					break;					
					case 'contains':
						$compare = "LIKE";				
					break;
					case 'NOT_EXISTS':
						$compare = "NOT EXISTS";				
					break;					
					default:
						$compare = "=";
					break;	
				}	
				$query->query_vars['meta_query'][] = array( 'key' => $meta_key, 'type' => $type, 'value' => $_REQUEST['av'], 'compare' => $compare, 'relation' => 'AND');
			}
			if ( !empty($_REQUEST['d']) )
			{					
				switch ( $_REQUEST['d'] ) 
				{
					case 'image_yes':
						$query->query_vars['meta_query'][] = array( 'key' => '_thumbnail_id', 'value' => '', 'compare' => '!=', 'relation' => 'AND');
					break;					
					case 'image_no':
						$sql_query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_thumbnail_id'";
						$product = $wpdb->get_col( $sql_query );			
						$query->query_vars['post__not_in'] = $product;	
					break;				
					case 'webspy_yes':
						$sql_query = "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_usam_product_metadata'";
						$product = $wpdb->get_results( $sql_query, ARRAY_A );
						$id = array();
						foreach ($product as $key => $value) 
						{
							$product_meta = maybe_unserialize($value['meta_value']);
							if ( !empty($product_meta['webspy_link']) )
								$id[] = $value['post_id'];
						}									
						if (!empty($id) ) $query->query_vars['post__in'] = $id;
					break;	
					case 'webspy_no':					
						$sql_query = "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_usam_product_metadata'";
						$product = $wpdb->get_results( $sql_query );
						$id = array();
						foreach ($product as $key => $value) 
						{
							$product_meta = maybe_unserialize($value->meta_value);
							if ( empty($product_meta['webspy_link']) )
								$id[] = $value->post_id;
						}									
						if (!empty($id) ) $query->query_vars['post__in'] = $id;	
					break;
					case 'variant_prod':	
						$sql_query = "SELECT DISTINCT post_parent FROM $wpdb->posts WHERE post_parent != '0' AND post_type = 'usam-product'";					
						$product = $wpdb->get_col( $sql_query );
						$query->query_vars['post__in'] = $product;						
					break;								
				}
			}			
			if ( !empty($_REQUEST['price_from']) )	
			{
				$price_from = absint($_REQUEST['price_from']);
				$query->query_vars['meta_query'][] = array( 'key' => '_usam_price_'.$type_price, 'type' => 'NUMERIC', 'value' => $price_from, 'compare' => '>=', 'relation' => 'AND');	
			}
			if ( !empty($_REQUEST['price_to']) )	
			{
				$price_to = absint($_REQUEST['price_to']);
				$query->query_vars['meta_query'][] = array( 'key' => '_usam_price_'.$type_price, 'type' => 'NUMERIC', 'value' => $price_to, 'compare' => '<=', 'relation' => 'AND');	
			}			
			if ( isset($_REQUEST['orderby']) )		
				switch ( $_REQUEST['orderby'] ) 
				{
					case 'title' :				
						$query->query_vars["orderby"] = 'post_title';
					break;
					case 'name' :
						$query->query_vars["orderby"] = 'title';
					break;
					case 'price' :
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_price_'.$type_price, 'orderby' => 'meta_value_num' ) );					
					break;	
					case 'oldprice' :
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_old_price_'.$type_price, 'orderby' => 'meta_value_num' ));
					break;				
					case 'stock' :
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_stock', 'orderby' => 'meta_value_num' ));
					break;	
					case 'reserve' :
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_reserve', 'orderby' => 'meta_value_num' ));
					break;	
					case 'storage' :
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_storage', 'orderby' => 'meta_value_num' ));
					break;					
					case 'sku' :
						$query->query_vars = array_merge( $query->query_vars,	array( 'meta_key' => '_usam_sku', 'orderby' => 'meta_value'	));
					break;				
					case 'date' :
						$query->query_vars["orderby"] = 'date';
					break;				
					case "views":				
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_product_views', 'orderby'  => 'meta_value_num' ));				
					break;
					case "rating":				
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_rating', 'orderby'  => 'meta_value_num' ));				
					break;
					case "weight":				
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_weight', 'orderby'  => 'meta_value_num' ));				
					break;					
					case "date_externalproduct":			
						$query->query_vars = array_merge( $query->query_vars, array( 'meta_key' => '_usam_date_externalproduct', 'orderby'  => 'meta_value' ));				
					break;				
				}		
			if (isset( $_REQUEST['order']))		
				$query->query_vars['order'] = usam_product_order();	
			if (!empty($_REQUEST['s']))			
				$query->query_vars["s"] = esc_sql( $wpdb->esc_like( trim($_REQUEST['s'])));			
			if (!empty($_REQUEST['usam-category']))			
				$query->query_vars["usam-category"] = sanitize_title($_REQUEST['usam-category']);			
			if (!empty($_REQUEST['usam-brands']))			
				$query->query_vars["usam-brands"] = sanitize_title($_REQUEST['usam-brands']);	
			if (!empty($_REQUEST['usam-category_sale']))			
				$query->query_vars["usam-category_sale"] = sanitize_title($_REQUEST['usam-category_sale']);
		}
	}
	
	function posts_search( $search, $wp_query )
	{
		global $wpdb;		
		if ( isset($wp_query->query_vars['post_type']) && $wp_query->query_vars['post_type'] == 'usam-product' ) 
		{		
			if ( empty( $search) || empty($wp_query->query_vars['s']) )
				return $search; 		
				
			$search = '';	
			$s = trim($wp_query->query_vars['s']);		
			
			if ( is_numeric($s) )
			{							
				$wp_query->query_vars['post_status'] = 'all';		 
				$search .= "$wpdb->posts.ID='$s' OR ";		
			}		
			$search .= "$wpdb->posts.post_title LIKE LOWER ('%{$s}%') ";		
			$search .= "OR $wpdb->posts.post_content LIKE LOWER ('%{$s}%') ";		
			if (strripos($s, ' ') === false) 
			{
				$search .= " OR $wpdb->posts.id IN (SELECT ID FROM $wpdb->posts AS p, $wpdb->postmeta AS m WHERE p.ID = m.post_id AND m.meta_value='$s' AND m.meta_key = '_usam_sku')";		
			}			
			$search = " AND ($search)";
		}
		return $search;
	}	
}
?>