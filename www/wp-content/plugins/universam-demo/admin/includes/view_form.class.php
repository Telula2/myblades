<?php
// Класс форм таблиц
abstract class USAM_View_Form
{	
	protected $id = null;
	protected $page_name = null;	
	protected $list_table = null;	
	
	protected $data = array();
	
	public function __construct() 
	{ 
		if ( isset($_REQUEST['id']) )
			$this->id = sanitize_title($_REQUEST['id']);	
	
		if ( isset($_REQUEST['page']) )
			$this->page_name = sanitize_title($_REQUEST['page']);	
		
		$this->get_data_tab();
		add_action( 'admin_enqueue_scripts', array( $this, 'js' ) );
	}
	
	protected function title_tab_none( ) 
	{ 
		_e('Запись не найдена','usam');
	}	
	
	protected function get_title_tab( ) { }	
	
	protected function main_content_cell_1( ) { }	
	protected function main_content_cell_2( ) { }	
	protected function main_content_cell_3( ) { }	
	
	protected function header_view()
	{	
		?>		
		<div class = "header_main">
			<div id='header_main_content' class = "header_main_content">
				<div class = "header_main_content_wrap">
					<div class = "main_content_cell main_content_cell_border">
						<div class = "main_content_cell_wrap">						
							<?php $this->main_content_cell_1(); ?>									
						</div>	
					</div>
					<div class = "main_content_cell main_content_cell_border">
						<div class = "main_content_cell_wrap">						
							<?php $this->main_content_cell_2(); ?>									
						</div>	
					</div>
					<div class = "main_content_cell">
						<div class = "main_content_cell_wrap">						
							<?php $this->main_content_cell_3(); ?>									
						</div>	
					</div>	
				</div>						
			</div>	
			<?php
			if ( !empty($this->header_content) ) 
			{
				?>
				<h4 class="header_content_name"><?php echo $this->header_title; ?></h4>
				<div class = "main_content_footer">						
					<?php echo $this->header_content; ?>									
				</div>	
			<?php } ?>
		</div>	
		<?php	
	}		
	
	public function js() 
	{			
		wp_enqueue_script( 'postbox' );
		wp_enqueue_style( 'usam-view-form' );
	}
	
	protected function toolbar_buttons( ) 
	{
		if ( $this->id != null )
		{
			global $current_screen;					
			?>
			<ul>				
				<?php $sendback = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'delete' )), 'usam-'.$current_screen->id); ?>
				<li><a href="<?php echo $sendback; ?>" class="button delete"><?php _e('Удалить','usam'); ?></a></li>
				<?php $sendback = wp_nonce_url( add_query_arg( array('id' => $this->id, 'action' => 'edit' )), 'usam-'.$current_screen->id); ?>
				<li><a href="<?php echo $sendback; ?>" class="button"><?php _e('Изменить','usam'); ?></a></li>
			</ul>
			<?php		
		}
	}
	
	protected function get_url_go_back( ) 
	{
		return remove_query_arg( array( 'id', 'action', 'n', 'status' )  );		
	}
	
	protected function display_toolbar( ) 
	{				
		?>			
		<div class="toolbar">			
			<div class="window-fastnav">				
				<a href="<?php echo $this->get_url_go_back(); ?>"  class="go_back"><span class="back_text">&#171; <?php _e('вернуться назад','usam'); ?></span></a>
				<div class="fastnav">	
					<div class="fastnav-container">						
						<div class="button_action">
							<?php 
							if ( !empty($this->data) )
								$this->toolbar_buttons();						
							?>															
						</div>	
					</div>						
				</div>					
			</div>
		</div>
      <?php
	}		
	
	// Отображение элемента таблицы	
	public function display_form()
    {										
		if ( empty($this->data) )
		{
			?><h2><?php $this->title_tab_none( ); ?></h2><?php 	
			$this->display_toolbar();		
		}
		else
		{
			?>	
			<h2><?php echo $this->get_title_tab( ); ?></h2>		
			<?php $this->display_toolbar(); ?>			
			<div id="content_form_view" data-id="<?php echo $this->id; ?>">								
				<?php				
				$this->header_view();							
				$this->view_tabs();			
				?>			
			</div>	
			<?php	
		}
	}
	
	private function view_tabs() 
	{	
		echo '<div class = "usam_tabs usam_view_form_tabs">';
		$this->display_header_tabs();
		$this->display_content_tabs();	
		echo '</div>';	
	}
		
	public function display_header_tabs() 
	{		
		?>	
		<div class="header_tab">			
			<ul>
			<?php		
			foreach ( $this->tabs as $menu )
			{
				?>
					<li id="view_form-<?php echo $menu['slug']; ?>" class="tab usam_menu-<?php echo $menu['slug']; ?>">
						<a href='#view_form-<?php echo $menu['slug']; ?>' data-tab="<?php echo $menu['slug']; ?>"><?php echo $menu['title']; ?></a>
					</li>
				<?php
			}
			?>
			</ul>
		</div>		
		<?php
	}	
	
	public function display_content_tabs() 
	{		
		echo '<div class = "countent_tabs">';			
		foreach ( $this->tabs as $menu )
		{
			?>
			<div id = "view_form-<?php echo $menu['slug']; ?>" class = "tab">				
				<?php 
				$method = 'display_tab_'.$menu['slug'];				
				if ( method_exists($this, $method) )
					$this->$method();
				?>
			</div>
			<?php 
		}		
		echo '</div>';
	}	
	
	public function buttons()
	{    
		if ( $this->id != null )
		{			
			submit_button( __('Сохранить и закрыть', 'usam'), 'primary button_save_close', 'save-close', false, array( 'id' => 'submit-save-close' ) ); 
			submit_button( __('Сохранить', 'usam'), 'secondary button_save', 'save', false, array( 'id' => 'submit-save' ) ); 
		}
		else
		{
			submit_button( __('Добавить', 'usam').' &#10010;', 'primary button_save_close', 'add', false, array( 'id' => 'submit-add' ) ); 
		}
	}

	public function list_table( $table )
    {				
		require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );		
		$filename = USAM_FILE_PATH .'/admin/menu-page/'.$this->page_name.'/form/view_form_table/'.$table.'_list_table.php';
		if ( file_exists($filename) ) 
		{
			require_once( $filename );							
			$args = array(
				'singular'  => $this->page_name.'_'.$table,    
				'plural'    => $table,  
				'ajax'      => true,	
				'screen'    => $this->page_name.'_'.$table,				
			);				
			$name_class_table = 'USAM_List_Table_'.$table;
			$this->list_table = new $name_class_table( $args );
			$this->list_table->display_table(); 
		}
	//	$per_page = $this->list_table->get_items_per_page($this->per_page_option, $this->per_page);			
	//	$per_page = apply_filters( $this->per_page_option, $per_page );			
	//	$this->list_table->set_per_page( $per_page );
	}	
}
?>