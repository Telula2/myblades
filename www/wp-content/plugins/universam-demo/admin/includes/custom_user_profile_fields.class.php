<?php
new USAM_Admin_User_Profile();
class USAM_Admin_User_Profile 
{	
	public function __construct() 
	{			
		add_action('show_user_profile', array($this, 'user_profile_fields'));
		add_action('edit_user_profile', array($this, 'user_profile_fields'));
		add_action('personal_options_update', array($this, 'save_user_profile_fields'));
		add_action('edit_user_profile_update', array($this, 'save_user_profile_fields'));
	}
			
	function user_profile_fields( $user ) 
	{
		global $user_ID;
		if ( usam_check_current_user_role( 'administrator' ) || usam_check_current_user_role('editor') || usam_check_current_user_role('shop_manager') )
		{
			$signature_for_email = get_the_author_meta('usam_signature_email', $user->ID);
			?>
			<h3><?php _e('Данные пользователя', 'usam'); ?></h3>
			<table class="form-table">	
				<tr>
					<th><label for="sex"><?php _e( 'Пол', 'usam' ) ?></label></th>
					<td>
						<?php $selected = get_user_meta( $user->ID, 'usam_sex', true ); ?>
						<select class="select_sex" id="sex" name="sex">
							<option value="" <?php echo ($selected == ''?'selected':''); ?>><?php _e( 'Выберите', 'usam' ) ?>...</option>
							<option value="w" <?php echo ($selected == 'w'?'selected':''); ?>><?php _e( 'Мужской', 'usam' ) ?></option>
							<option value="m"<?php echo ($selected == 'm'?'selected':''); ?>><?php _e( 'Женский', 'usam' ) ?></option>					
						</select>
					</td>
				</tr>
				<tr>
					<?php $birthday = get_user_meta( $user->ID, 'usam_birthday', true ); ?>
					<th><label for="birthday"><?php _e( 'День рождения', 'usam' ) ?></label></th>
					<td><?php usam_display_date_picker( 'birthday', $birthday ); ?></td>
				</tr>
			</table>		
			<h3><?php _e('Настройки электронной почты', 'usam'); ?></h3>
			<?php 	
			$mailboxes = usam_get_mailboxes( array( 'fields' => array( 'id','name'), 'user_id' => $user_ID ) );	
			$email_default = esc_attr(get_the_author_meta('usam_email_default', $user->ID));
			?>		
			<table class="form-table">			
				<tr>				
					<th><?php esc_html_e( 'Почта по умолчанию', 'usam' ); ?></th>
					<td>
						<select name="usam_email_default">
							<?php						
							foreach( $mailboxes as $mailbox )
							{						
								?><option value="<?php echo $mailbox->id; ?>" <?php selected($mailbox->id, $email_default) ?>><?php echo $mailbox->name; ?></option><?php
							}		
							?>				
						</select>
					</td>		
				</tr>	
				<tr>
					<th>
						<label for="user_country"><?php _e('Подпись', 'usam'); ?></label>
					</th>
					<td>
						<?php 
							wp_editor( $signature_for_email, 'signature_for_email', array(
								'textarea_name' => 'signature_for_email',
								'media_buttons'=>false,
								'textarea_rows' => 10,	
								'wpautop' => 0,							
								'tinymce' => array(
									'theme_advanced_buttons3' => 'invoicefields,checkoutformfields',
									)
								)
							 ); 
						?>
					</td>		
				</tr>  	
			</table>	
		<?php
		}
		if ( usam_check_current_user_role('administrator' ) )
		{
			?>	
			<h3><?php _e('Управление персоналом', 'usam'); ?></h3>
			<table class="form-table">
				<tr>
					<th>
						<label for="user_country"><?php _e('Начальник', 'usam'); ?></label>
					</th>
					<td>
						<select name="chief">
							<?php				
							$select = esc_attr(get_the_author_meta('usam_chief', $user->ID));
							$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator'), 'fields' => array( 'ID','display_name'), 'exclude' => array($user->ID)  );
							$users = get_users( $args );				
							?><option value="" <?php selected('' , $select ) ?>><?php _e('Не выбран', 'usam'); ?></option><?php
							foreach( $users as $user_data )
							{						
								?><option value="<?php echo $user_data->ID; ?>" <?php selected($user_data->ID, $select) ?>><?php echo $user_data->display_name; ?></option><?php
							}		
							?>				
						</select>
					</td>		
				</tr>  	
			</table>	
			<h3><?php _e('Просмотры заказов', 'usam'); ?></h3>
			<?php 	
			$option = get_option('usam_order_view_grouping');
			$grouping = maybe_unserialize( $option );	
			$select = esc_attr(get_the_author_meta('usam_order_view_grouping', $user->ID));
			?>		
			<table class="form-table">			
				<tr>				
					<th><?php esc_html_e( 'Группы просмотра заказов', 'usam' ); ?></th>
					<td>
						<select name="usam_order_view_grouping">
							<option value="0" <?php selected(0, $select) ?>><?php esc_html_e( 'Все заказы', 'usam' ); ?></option>
							<?php						
							foreach( $grouping as $value )
							{						
								?><option value="<?php echo $value['id']; ?>" <?php selected($value['id'], $select) ?>><?php echo $value['name']; ?></option><?php
							}		
							?>				
						</select>
					</td>		
				</tr>				
			</table>		
			<?php 	
		}
	}

	function save_user_profile_fields( $user_id ) 
	{	
		if (!current_user_can('edit_user', $user_id))
			return FALSE; 
		
		if ( usam_check_current_user_role('administrator' ) || usam_check_current_user_role('editor') || usam_check_current_user_role('shop_manager') )
		{		
			if ( isset($_REQUEST['sex']) )
			{
				$sex = sanitize_title($_REQUEST['sex']);
				update_user_meta($user_id, 'usam_sex', $sex ); 		
			}
			$birthday = usam_get_datepicker('birthday');
			update_user_meta($user_id, 'usam_birthday', $birthday ); 
			
			if ( isset($_POST['signature_for_email']) )
			{		
				$signature_for_email = $_REQUEST['signature_for_email'];				
				update_user_meta( $user_id, 'usam_signature_email', $signature_for_email ); 
			}				
			if ( isset($_POST['usam_email_default']) )
			{		
				$email_default = absint($_REQUEST['usam_email_default']);
				update_user_meta( $user_id, 'usam_email_default', $email_default ); 
			}				
		}
		if ( usam_check_current_user_role('administrator' ) )
		{
			if ( isset($_POST['chief']) )
			{		
				$chief = absint($_REQUEST['chief']);
				update_user_meta( $user_id, 'usam_chief', $chief ); 
			}	
			if ( isset($_POST['usam_order_view_grouping']) )
			{		
				$order_view_grouping = absint($_REQUEST['usam_order_view_grouping']);
				update_user_meta( $user_id, 'usam_order_view_grouping', $order_view_grouping ); 
			}	
		}
	}
}