<?php
// Класс страницы вариантов товара. Здесь сохранения вариантов товара
 
class USAM_Product_Variations_Page
{
	private $list_table;
	private $parent_id;
	private $current_tab = 'manage';
	private $post;

	public function __construct() 
	{
		require_once( USAM_FILE_PATH . '/admin/includes/product/product-variation-list-table.class.php' );
		$GLOBALS['hook_suffix'] = 'usam-product-variations-iframe';	
		$this->parent_id = absint( $_REQUEST['product_id'] );
		set_current_screen();

		if ( ! empty( $_REQUEST['tab'] ) )
			$this->current_tab = sanitize_title($_REQUEST['tab']);
		
		add_action( 'admin_init', array( $this, 'update_variations'), 50 );
	}
	
	function update_variations() 
	{ 
		if ( !empty($_POST["product_id"]) )
		{
			$product_id = absint( $_POST["product_id"] );			
			
			$post_data = array( );
			$post_data['variations'] = isset( $_POST['edit_var_val'] ) ? $_POST["edit_var_val"] : array();	
			usam_edit_product_variations( $product_id, $post_data );
		}
	}

	private function merge_meta_deep( $original, $updated ) 
	{		
		if (!empty($original))
		{
			$keys = array_merge( array_keys( $original ), array_keys( $updated ) );
		}
		else
			$keys = array_keys( $updated );
		foreach ( $keys as $key ) 
		{
			if ( ! isset( $updated[$key] ) )
				continue;
			if ( is_array( $original[$key] ) )
				$original[$key] = $this->merge_meta_deep( $original[$key]	, $updated[$key] );
			else 
			{
				$original[$key] = $updated[$key];
				if ( in_array( $key, array( 'weight', 'height', 'width', 'length' ) ) )
					$original[$key] = (float) $original[$key];
			}
		}
		return $original;
	}

	// По нажатию кнопки
	private function save_variation_meta( $product_id, $data )
	{
		$product_id = absint( $product_id );	
		
		$product_data = array();
		$product_data['meta'] = $data;		
		$product = new USAM_Product( $product_id );	
		$product->set( $product_data );
		$product->save_product_meta( );
	}

	private function save_variations_meta()
	{
		if ( empty( $_REQUEST['usam_variations'] ) )
			return;

		check_admin_referer( 'usam_save_variations_meta', '_usam_save_meta_nonce' );
		$post_type_object = get_post_type_object( 'usam-product' );
		if ( ! current_user_can( $post_type_object->cap->edit_posts ) )
			wp_die( __( 'Cheatin&#8217; uh?' ) );		
		
		foreach ( $_REQUEST['usam_variations'] as $id => $data ) 
		{
			if ( !empty($data) )			
				$this->save_variation_meta( $id, $data );				
		}
	}

	public function display() 
	{
		global $title, $hook_suffix, $current_screen, $wp_locale, $wp_version, $is_iphone, $current_site, $update_title, $total_update_count, $parent_file;

		$current_screen = get_current_screen();
		$admin_body_class = $hook_suffix;
		$post_type_object = get_post_type_object( 'usam-product' );

		wp_enqueue_style( 'global' );
		wp_enqueue_style( 'wp-admin' );
		wp_enqueue_style( 'buttons' );
		wp_enqueue_style( 'colors' );
		wp_enqueue_style( 'ie'     );
		wp_enqueue_script( 'common'       );
		wp_enqueue_script( 'jquery-color' );
		wp_enqueue_script( 'utils'        );
		wp_enqueue_script( 'jquery-query' );

		$callback = "callback_tab_{$this->current_tab}";
		if ( ! is_callable( array( $this, "callback_tab_{$this->current_tab}" ) ) )
			$callback = "callback_tab_manage";

		$this->$callback();

		@header('Content-Type: ' . get_option('html_type') . '; charset=' . get_option('blog_charset'));
		require_once( USAM_FILE_PATH . "/admin/includes/product/product-variations.page.php" );
	}

	private function display_tabs() 
	{
		$tabs = array(
			'manage'   => _x( 'Управление', 'manage product variations', 'usam' ),
			'setup'    => __( 'Установки', 'usam' ),
		);
		echo '<ul class="usam-product-variations-tabs">';
		foreach ( $tabs as $tab => $title ) {
			$class = ( $tab == $this->current_tab ) ? ' class="active"' : '';
			$item = '<li' . $class . '>';
			$item .= '<a href="' . add_query_arg( 'tab', $tab ) . '">' . esc_html( $title ) . '</a></li>';
			echo $item;
		}
		echo '</ul>';
	}

	private function callback_tab_manage()
	{	
		$this->list_table = new USAM_Product_Variation_List_Table( $this->parent_id );
		$this->save_variations_meta();
		$this->process_bulk_action();
		$this->list_table->prepare_items();
	}

	private function callback_tab_setup()
	{
		global $post;
		require_once( 'walker-variation-checklist.php' );
		$this->generate_variations();
	}
// Генерация вариаций
	private function generate_variations() 
	{
		if ( ! isset( $_REQUEST['action2'] ) || $_REQUEST['action2'] != 'generate' )
			return;

		check_admin_referer( 'usam_generate_product_variations', '_usam_generate_product_variations_nonce' );
		$this->update_variations();
		$sendback = remove_query_arg( array( '_wp_http_referer', 'updated',	) );
		wp_redirect( add_query_arg( 'tab', 'manage', $sendback ) );
		exit;
	}

	public function display_current_tab()
	{
		require_once( USAM_FILE_PATH . "/admin/includes/product/product-variations-{$this->current_tab}.page.php" );
	}

	public function process_bulk_action_trash( $post_ids ) 
	{
		$post_type_object = get_post_type_object( 'usam-product' );
		$trashed = 0;
		foreach( (array) $post_ids as $post_id ) 
		{
			if ( !current_user_can( $post_type_object->cap->delete_post, $post_id ) )
				wp_die( __( 'You are not allowed to move this item to the Trash.' ) );
			if ( !wp_trash_post( $post_id ) )
				wp_die( __( 'Error in moving to Trash.' ) );
			$trashed++;
		}
		return add_query_arg( array( 'trashed' => $trashed, 'ids' => join( ',', $post_ids ) ) );
	}

	public function process_bulk_action_untrash( $post_ids ) 
	{
		$post_type_object = get_post_type_object( 'usam-product' );
		$untrashed = 0;
		foreach( (array) $post_ids as $post_id )
		{
			if ( ! current_user_can( $post_type_object->cap->delete_post, $post_id ) )
				wp_die( __( 'You are not allowed to restore this item from the Trash.' ) );

			if ( !wp_untrash_post( $post_id ) )
				wp_die( __( 'Error in restoring from Trash.' ) );
			$untrashed++;
		}
		return add_query_arg( 'untrashed', $untrashed );
	}

	public function process_bulk_action_delete( $post_ids ) 
	{
		$deleted = 0;
		$post_type_object = get_post_type_object( 'usam-product' );		
		foreach( (array) $post_ids as $post_id ) 
		{
			$post_del = get_post( $post_id );
			if ( ! current_user_can( $post_type_object->cap->delete_post, $post_id ) )
				wp_die( __( 'Вы не можете удалить этот элемент.', 'usam' ) );
			if ( $post_del->post_type == 'attachment' ) 
			{
				if ( ! wp_delete_attachment( $post_id ) )
					wp_die( __( 'Error in deleting...' ) );
			} 
			else 
			{
				if ( ! wp_delete_post( $post_id ) )
					wp_die( __( 'Error in deleting...' ) );
			}
			$deleted++;
		}
		return add_query_arg( 'deleted', $deleted );
	}

	public function process_bulk_action_hide( $post_ids ) 
	{
		$updated = 0;
		foreach( $post_ids as $id ) 
		{
			wp_update_post( array( 'ID' => $id,	'post_status' => 'draft', ) );
			$updated ++;
		}
		return add_query_arg( 'updated', $updated );
	}

	public function process_bulk_action_show( $post_ids )
	{
		$updated = 0;
		foreach ( $post_ids as $id ) 
		{
			wp_update_post( array(	'ID' => $id, 'post_status' => 'publish', ) );
			$updated ++;
		}
		return add_query_arg( 'updated', $updated );
	}
	
	public function process_bulk_action_edit( $post_ids ) 
	{
		$this->list_table->set_bulk_edited_items( $post_ids );
	}	

	public function process_bulk_action() 
	{		
		global $wpdb;
		if ( isset($_REQUEST['delete_all']) )
		{			
			$post_ids = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_status='trash' AND post_type='usam-product' AND post_parent = '%d'", $this->parent_id ) );
			$this->process_bulk_action_delete( $post_ids );
		}		
		$current_action = $this->list_table->current_action();
		if ( empty( $current_action ) )
			return;

		check_admin_referer( 'usam_product_variations_bulk_action', 'bulk_action_nonce' );
		$sendback = $_SERVER['REQUEST_URI'];
		$callback = 'process_bulk_action_' . $current_action;

		$post_ids = isset( $_REQUEST['post'] ) ? $_REQUEST['post'] : array();
		if ( ! is_array( $post_ids ) )
			$post_ids = explode( ',', $post_ids );
		$post_ids = array_map('intval', $post_ids);
		if ( ! empty( $post_ids ) && is_callable( array( $this, $callback ) ) )
			$sendback = $this->$callback( $post_ids );

		$sendback = remove_query_arg( array( '_wp_http_referer', 'bulk_action', 'bulk_action2', 'bulk_action_nonce', 'confirm',	'post',	'last_paged' ), $sendback );		
		if ( $current_action != 'edit' ) 
		{
			wp_redirect( $sendback );
			exit;
		}
	}
}