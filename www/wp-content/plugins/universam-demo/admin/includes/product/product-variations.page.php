<?php $admin_html_class = ( is_admin_bar_showing() ) ? 'wp-toolbar' : ''; ?>
<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8 <?php echo $admin_html_class; ?>" <?php do_action('admin_xml_ns'); ?> <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" class="<?php echo $admin_html_class; ?>" <?php do_action('admin_xml_ns'); ?> <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php echo get_option('blog_charset'); ?>" />

<title><?php esc_html_e( 'Управление вариациями товара', 'usam' ); ?></title>
<script type="text/javascript">
addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
var userSettings = {
		'url': '<?php echo SITECOOKIEPATH; ?>',
		'uid': '<?php if ( ! isset($current_user) ) $current_user = wp_get_current_user(); echo $current_user->ID; ?>',
		'time':'<?php echo time() ?>'
	},
	ajaxurl = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>',
	pagenow = '<?php echo $current_screen->id; ?>',
	typenow = '<?php echo $current_screen->post_type; ?>',
	adminpage = '<?php echo $admin_body_class; ?>',
	thousandsSeparator = '<?php echo addslashes( $wp_locale->number_format['thousands_sep'] ); ?>',
	decimalPoint = '<?php echo addslashes( $wp_locale->number_format['decimal_point'] ); ?>',
	isRtl = <?php echo (int) is_rtl(); ?>;
</script>
<?php
	do_action('admin_enqueue_scripts', $hook_suffix);
	do_action("admin_print_styles-$hook_suffix");
	do_action('admin_print_styles');
	do_action("admin_print_scripts-$hook_suffix");
	do_action('admin_print_scripts');
	do_action("admin_head-$hook_suffix");
	do_action('admin_head');
?>
<style type="text/css">
	html { background-color:transparent; }
</style>
</head>
<body class="no-js wp-admin wp-core-ui usam-product-variation-iframe">
	<script type="text/javascript">document.body.className = document.body.className.replace('no-js','js');</script>
	<div id="usam-product-variations-wrapper">
		<?php $this->display_tabs(); ?>
		<div class="usam-product-variations-tab-content">
			<?php $this->display_current_tab(); ?>
		</div>		
	</div>	
	<?php
	do_action('admin_print_footer_scripts');
	do_action("admin_footer-" . $GLOBALS['hook_suffix']);
	?>
	<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>
</body>
</html>