<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo esc_html__( 'Печать товара', 'usam' ); ?></title>
	<style type="text/css">
		body {font-family:"Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;}
		h1 span {font-size:0.75em;}
		h2 {color: #333;}
		#wrapper {margin:0 auto; width:95%;}
		#header {	}
		#customer {	overflow:hidden;}
		#customer .shipping, #customer .billing {float: left; width: 50%;}
		table {border:1px solid #000; border-collapse:collapse;	margin-top:1em; width:100%;	}
		th {background-color:#efefef; text-align:center;}
		th, td { padding:5px;}
		td {text-align:center;}
		#print-items td.amount {	text-align:right; }
		td, tbody th { border-top:1px solid #ccc; }
		th.column-total { width:90px;}
		th.column-shipping { width:120px;}
		th.column-price { width:100px;}
		tfoot{background-color:#efefef;}
	</style>
</head>
<body onload="window.print()">
	<div id="wrapper">
		<div id="header">
			<h1>
				<?php echo get_bloginfo('name'); ?> <br />
				<span><?php esc_html_e( 'Печать товара', 'usam' ); ?></span>
			</h1>
		</div>		
		<table id="product">
			<thead>
				<tr>
					<th><?php echo esc_html_x( 'Номер', 'printing products', 'usam' ); ?></th>
					<th><?php echo esc_html_x( 'Артикул', 'printing products', 'usam' ); ?></th>
					<th><?php echo esc_html_x( 'Имя', 'printing products', 'usam' ); ?></th>
					<th><?php echo esc_html_x( 'Цена', 'printing products', 'usam' ); ?></th>					
					<th><?php echo esc_html_x( 'Остаток', 'printing products', 'usam' ); ?></th>
					<th><?php echo esc_html_x( 'Категория', 'printing products', 'usam' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 0;
				foreach( $product as $post_id => $value )
				{
					$i++;
				?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $value['sku']; ?></td>
						<td><?php echo $value['title']; ?></td>
						<td><?php echo $value['price']; ?></td>						
						<td><?php echo $value['stock']; ?></td>
						<td><?php echo $value['cat']; ?></td>
					</tr>
				<?php
				}
				?>
			</tbody>
			<tfoot>	
				<tr class="usam_purchaselog_start_totals">
					<td colspan="4"></td>	
					<td>						
						<?php esc_html_e( 'Количество товаров', 'usam' ); ?>:					
					</td>				
					<td class="amount"><?php echo $product_count ?></td>
				</tr>
			</tfoot>
		</table>		
	</div>
</body>
</html>