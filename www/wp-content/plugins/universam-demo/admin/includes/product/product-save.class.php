<?php
/*
 *  Сохранение данных продукта в админском интерфейсе (сохранение цены, описания ...)
 */
class USAM_Save_Product
{	
	public $message_code = 0;
	
	public function __construct( ) 
	{			
		add_filter( 'wp_insert_post_data',array($this, 'pre_update'), 99, 2 );	
		add_action( 'save_post', array($this, 'save_product'), 10, 2 );		
		add_action( 'admin_notices', array($this, 'submit_notices') );			
		add_filter( 'post_updated_messages', array($this, 'updated_messages') );	
		add_filter( 'redirect_post_location', array($this, 'redirect_product_location') , 99, 2);	
		add_action( 'delete_post', array($this, 'delete_variations') );	
	}
	
	function delete_variations( $postid ) 
	{
		$post = get_post( $postid );
		if ( $post->post_type != 'usam-product' || $post->post_parent != 0 )
			return;
		$variations = get_posts( array(
			'post_type' => 'usam-product',
			'post_parent' => $postid,
			'post_status' => 'any',
			'numberposts' => -1,
		) );

		if ( ! empty( $variations ) )
			foreach ( $variations as $variation ) {
				wp_delete_post( $variation->ID, true );
			}
	}	
	
	function redirect_product_location( $location, $post_id ) 
	{
		if( $this->message_code )
			$location = add_query_arg( 'message', $this->message_code, $location );
		return $location;
	}
	
	/**
	 * Сообщения при обновлении продукта
	 */
	function updated_messages( $messages ) 
	{
		global $post, $post_ID;		
		
		$url = esc_url( usam_product_url() );
		$sku = usam_get_product_meta( $post_ID, 'sku' );
		$messages['usam-product'] = array(
			0  => '', 
			1  => sprintf( __( 'Товар обновлен. <a href="%s">Посмотреть</a>', 'usam' ), $url ),
			2  => __( 'Пользовательские поля обновлены.', 'usam' ),
			3  => __( 'Пользовательские поля удалены.', 'usam' ),
			4  => __( 'Товар обнавлен.', 'usam' ),			
			5  => isset( $_GET['revision'] ) ? sprintf( __('Продукт восстановлен до ревизии от %s', 'usam' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => sprintf( __( 'Товар опубликован. <a href="%s">Посмотреть товар</a>', 'usam' ), $url ),
			7  => __( 'Товар сохранен.', 'usam' ),
			8  => sprintf( __( 'Представлены продуктов. <a target="_blank" href="%s">Просмотр товара</a>', 'usam' ), esc_url( add_query_arg('preview', 'true', $url)) ),
			9  => sprintf( __( 'Публикация товара запланирована на: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотреть товар</a>', 'usam' ),
				date_i18n( __( 'M j, Y @ G:i', 'usam' ), strtotime($post->post_date) ), $url ),
			10 => sprintf( __( 'Черновик товара обновлен. <a target="_blank" href="%s">Просмотр товара</a>', 'usam' ), esc_url( add_query_arg( 'preview', 'true', $url ) ) ),
			11 => __( 'Артикул не может быть пустым. Публикация товара в этом случае не возможна. Укажите артикул!', 'usam' ),
			12 => sprintf( __( 'Артикул "%s" уже существует. Публикация товара в этом случае не возможна. Укажите другой артикул!', 'usam' ), $sku ),
		);
		return $messages;
	}	
		
	public function save_product( $product_id, $post )
	{		
		remove_action( 'save_post', array($this, 'save_product'), 10, 2 );// выполняется много раз;!!!!!!!!!!!!!		
		if ( ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || $post->post_type != 'usam-product' )
			return $product_id;	
	
		$post_data = $_POST;
		if ( isset($post_data['product_meta']) )
		{
			$post_data['meta'] = $post_data['product_meta'][$product_id];
			unset($post_data['product_meta']);	
		}
		
		//	  Сохраняет вход для различных мета в быстрых полях редактирования
		$custom_fields = array( 'weight', 'sku' );
		foreach ( $custom_fields as $meta_key ) 
		{			
			if ( isset($_REQUEST[$meta_key]) )
				switch ( $meta_key ) 
				{
					case 'weight':
						$post_data['meta']['weight'] = absint($_REQUEST[$meta_key]);
					break;			
					case 'sku':					
						$post_data['meta']['sku'] = sanitize_text_field($_REQUEST[$meta_key]);
					break;				
				}
		}			
		
		if ( $post->post_parent != 0 )	
			$product_type = 'variation';  // это вариация
		else
		{	
			global $wpdb;
			$product_variation = $wpdb->get_var("SELECT COUNT(ID) FROM `{$wpdb->posts}` WHERE `post_parent` = '$product_id' AND `post_type` = 'usam-product'");
			if ( $product_variation != 0 )									
				$product_type = 'variable'; // это главный товар вариации				
			else
				$product_type =  'simple'; // простой товар	
		}		
		$post_data['product_type'] = $product_type;							
		if ( empty( $post_data ) )
			$post_data = $post;		
		
		if ( !empty($_POST['components']) )
		{
			foreach( $_POST['components']['title'] as $key => $component)
			{								
				if ( trim($component) != '' )
				{
					$c = !empty($_POST['components']['c'][$key])?$_POST['components']['c'][$key]:1;
					$post_data['meta']['product_metadata']['components'][] = array( 'c' => $c, 'title' => $component );					
				}
			}
		}			
		$attachment_images = !empty($post_data['attachment_images'])?explode( ',', $post_data['attachment_images'] ):array();	
		$post_data['image_gallery'] = !empty($post_data['image_gallery'])?explode( ',', $post_data['image_gallery'] ):array();	
		$attachments = (array)get_posts( array( 'fields' => 'ids', 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $product_id, 'orderby' => 'menu_order', 'order' => 'ASC' ) );
		$result = array_diff($attachments, $attachment_images);
		$post_data['image_gallery'] = array_merge( $result, $post_data['image_gallery'] );		
		if ( !empty($post_data['not_limited']) )
		{		
			$storages = usam_get_stores( array( 'fields' => 'id, shipping, meta_key' ) );
			foreach ( $storages as $storage )
			{
				$post_data['meta'][$storage->meta_key] = USAM_UNLIMITED_STOCK;
			}	
		}		
		if ( !empty($post_data['meta']['code']) )
		{
			$exchange = new USAM_Exchange_FTP();
			$out = $exchange->return_product_price( $post_data['meta']['code'] );
			usam_set_user_screen_error( $exchange->get_error( ), 'product' );		
			if ( !empty($out) )
				$post_data['meta'] = array_merge( $post_data['meta'], $out );				
		}
		if ( !empty($_FILES) )
		{
			add_filter('upload_dir', array($this, 'modify_upload_directory'));
			$overrides = array('test_form' => false);

			$time = current_time('mysql');
			if ( $post = get_post($product_id) ) {
				if ( substr( $post->post_date, 0, 4 ) > 0 )
					$time = $post->post_date;
			}	
			if ( !empty($_FILES['file']) )
			{
				$file = wp_handle_upload($_FILES['file'], $overrides, $time);
				if ( !isset($file['error']) )
				{
					$post_data['file'] = $file;
				}	
			}				
			if ( !empty($_FILES['preview_file']) )
			{
				$file = wp_handle_upload($_FILES['file'], $overrides, $time);
				if ( !isset($file['error']) )
				{
					$post_data['preview_file'] = $file;
				}	
			}		
		}	
		if ( isset($post_data['meta']['product_attributes']) )
		{
			$product_attributes = $post_data['meta']['product_attributes'];
			unset($post_data['meta']['product_attributes']);
		}
		else
			$product_attributes = array();
		
		$product = new USAM_Product( $product_id );	
		$product->set( $post_data );
		$product->calculate_product_attributes( $product_attributes );
		$product->save_product_meta( );	
		
		do_action('usam_edit_product', $product_id );
				
		return $product_id;
	}
	
	public function modify_upload_directory( $input )
	{
		$previous_subdir = $input['subdir'];
		$download_subdir = str_replace($input['basedir'], '', USAM_FILE_DIR);
		$input['path'] = substr_replace(str_replace($previous_subdir, $download_subdir, $input['path']),'',-1);
		$input['url'] = substr_replace(str_replace($previous_subdir, $download_subdir, $input['url']),'',-1);
		$input['subdir'] = substr_replace(str_replace($previous_subdir, $download_subdir, $input['subdir']),'',-1);
		return $input;
	}
	
	function pre_update( $data , $postarr ) 
	{
		global $wpdb;
		
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || $postarr["post_type"] != 'usam-product' )
			return $data;
		
		if( !empty( $postarr["parent_post"] ) )
			$data["post_parent"] = $postarr["parent_post"];
		
		if( $data["post_title"] != '' && $data["post_status"] == 'draft' && $data["post_name"] == '' )
			$data["post_name"] = sanitize_title( $data["post_title"] );		
		
		if ( !empty( $postarr['product_meta'][$postarr['ID']] ) && ( empty($postarr['product_meta'][$postarr['ID']]['product_metadata']['comment']) ) )
			$data["comment_status"] = "closed";
		else
			$data["comment_status"] = "open";	
		// обработка вариаций
		if ( $data['post_parent'] )		
		{	// статус вариаций не должен быть pending		
			if ( $data['post_status'] == 'pending' )
				$data['post_status'] = 'draft';
			elseif ( $data['post_status'] == 'publish' ) 
				$data['post_status'] = 'publish';
		}
		else
		{	// обработка простого товара или главного товара вариации
			if ( $data['post_status'] == 'publish' && !empty( $_POST['product_meta']) )
			{				
				// Проверить остаток
				$product_meta = stripslashes_deep($_POST['product_meta'][$postarr['ID']]);
				if ( empty( $product_meta['sku']) )
				{
					$sku = $postarr['ID'].usam_rand_string( 4, "abcdefghijklmnopqrstuvwxyz1234567890");
					$_POST['product_meta'][$postarr['ID']]['sku'] = $sku;
					
				//	$data['post_status'] = 'draft';					
				//	$this->message_code = 11;				
				}
				else
				{					
					$sku_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(meta_value) FROM `{$wpdb->postmeta}` WHERE meta_key = '_usam_sku' AND meta_value = %s AND post_id !='".$postarr['ID']."'", $product_meta['sku'] ) );		
					if ( $sku_count > 1 )
					{	
						$data['post_status'] = 'draft';				
						$this->message_code = 12;
					}
				}					
			}
		}
		return $data;
	}
	
	// Вывод ошибок при публикации товара
	function submit_notices()
	{		
		global $post, $current_screen;			
		if( isset($current_screen) && $current_screen->id == 'usam-product' )
		{
			//<div id="message" class="updated notice notice-success is-dismissible">
			if( isset($_SESSION['p_error_messages'][$post->ID]) )
			{
				foreach ( $_SESSION['p_error_messages'][$post->ID] as $error )
					echo "<div id=\"message\" class=\"updated below-h2\"><p><span>".__('Ошибка:','usam')." </span>".$error."</p></div>";
				unset( $_SESSION['p_error_messages'] );
			}
			if( isset($_SESSION['p_messages'][$post->ID]) )
			{
				foreach ( $_SESSION['p_messages'][$post->ID] as $message )
					echo "<div id=\"message\" class=\"updated below-h2\"><p>".$message."</p></div>";				
				unset( $_SESSION['p_messages'] );
			}
		}		
	}	
}	
new USAM_Save_Product();