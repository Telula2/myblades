<?php
require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
require_once( ABSPATH . 'wp-admin/includes/class-wp-posts-list-table.php' );

class USAM_Product_Variation_List_Table extends WP_List_Table
{
	private $product_id;
	private $object_terms_cache = array();
	private $args = array();
	private $is_trash             = false;
	private $is_draft             = false;
	private $is_pending           = false; // на утверждении
	private $is_publish           = false;
	private $is_all               = true;
	private $is_bulk_edit         = false;
	private $bulk_edited_items    = array();
	private $bulk_edited_item_ids = array();
	private $post_status = 'publish, inherit, draft';
	
	public function __construct( $product_id ) 
	{
		global $wpdb;
		WP_List_Table::__construct( array(	'plural' => 'variations', ) );

		$this->product_id = $product_id;
		if ( isset( $_REQUEST['post_status'] ) ) 
		{
			$this->is_trash = $_REQUEST['post_status'] == 'trash';
			$this->is_draft = $_REQUEST['post_status'] == 'draft';
			$this->is_pending = $_REQUEST['post_status'] == 'pending';
			$this->is_publish = $_REQUEST['post_status'] == 'publish';
			$this->is_all = $_REQUEST['post_status'] == 'all';
			if ( in_array($_REQUEST['post_status'], array( 'trash', 'draft', 'pending', 'publish' ) ) )
				$this->post_status = $_REQUEST['post_status'];			
		} 
		else 			
			$this->is_all = true;	
	}

	public function prepare_items() 
	{
		if ( ! empty( $this->items ) )
			return;
		
		$per_page = $this->get_items_per_page( 'edit_usam-product-variations_per_page' );
		$per_page = apply_filters( 'edit_usam_product_variations_per_page', $per_page );

		$this->args = array(
			'post_type'      => 'usam-product',
			'orderby'        => 'menu_order post_title',
			'post_parent'    => $this->product_id,
			'post_status'    => $this->post_status,
			'numberposts'    => -1,
			'order'          => "ASC",
			'posts_per_page' => $per_page,
		);
		if ( isset( $_REQUEST['s'] ) )
			$this->args['s'] =  esc_sql( $wpdb->esc_like( trim($_REQUEST['s'])));

		if ( isset( $_REQUEST['paged'] ) )
			$this->args['paged'] = $_REQUEST['paged'];

		$query = new WP_Query( $this->args );
		$this->items = $query->posts;
		
		$total_items = $query->found_posts;
		$total_pages = $query->max_num_pages;

		$this->set_pagination_args( array( 'total_items' => $total_items, 'total_pages' => $total_pages, 'per_page' => $per_page ) );

		if ( empty( $this->items ) )
			return;

		$ids = wp_list_pluck( $this->items, 'ID' );
		$object_terms = wp_get_object_terms( $ids, 'usam-variation', array( 'fields' => 'all_with_object_id' ) );

		foreach ( $object_terms as $term ) 
		{
			if ( ! array_key_exists( $term->object_id, $this->object_terms_cache ) )
				$this->object_terms_cache[$term->object_id] = array();

			$this->object_terms_cache[$term->object_id][$term->parent] = $term->name;
		}
	}

	public function get_hidden_columns() 
	{
		return array();
	}

	public function get_columns() 
	{
		$columns = array(
			'cb'             => '<input type="checkbox" />',
			'image'          => '',
			'title'          => __( 'Название', 'usam' ),
			'sku'            => __( 'Артикул', 'usam' ),				
			'stock'          => __( 'Запас', 'usam' ),			
		);		
		return apply_filters( 'usam_variation_column_headers', $columns );
	}

	public function get_sortable_columns() {
		return array();
	}

	public function column_cb( $item ) 
	{
		$checked = isset( $_REQUEST['variations'] ) ? checked( in_array( $item->ID, $_REQUEST['variations'] ), true, false ) : '';
		return sprintf( '<input type="checkbox" %1$s name="%2$s[]" value="%3$s" />', /*$1%s*/ $checked, /*$2%s*/ 'post', /*$3%s*/ $item->ID	);
	}
	
// Действия в таблицы вариаций
	private function get_row_actions( $item ) 
	{
		$post_type_object = get_post_type_object( 'usam-product' );
		$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $item->ID );

		$actions = array();
		if ( apply_filters( 'usam_show_product_variations_edit_action', true, $item ) && $can_edit_post && 'trash' != $item->post_status )
			$actions['edit'] = '<a target="_blank" href="'.get_edit_post_link( $item->ID, true ).'" title="'.esc_attr( __( 'Редактировать вариацию' ), 'usam' ).'">'.__( 'Изменить' ).'</a>';
		$actions['stock hide-if-no-js'] = '<a class="usam-variation-editor-link" data-key="stock" href="#" title="'.__( 'Показать редактор запасов', 'usam' ).'">'.__( 'Запас', 'usam' ).'</a>';
		$actions['price hide-if-no-js'] = '<a class="usam-variation-editor-link" data-key="price" href="#" title="'.__( 'Показать редактор цен', 'usam' ).'">'.__( 'Цены', 'usam' ).'</a>';		
		$actions['vendor hide-if-no-js'] = '<a class="usam-variation-editor-link" data-key="vendor" href="#" title="'.__( 'Показать редактор поставщика товара', 'usam' ).'">'.__( 'Поставщик', 'usam' ).'</a>';

		if ( $item->post_status == 'draft' ) 
		{
			$url = add_query_arg( array( 'bulk_action' => 'show', 'post' => $item->ID, 'bulk_action_nonce' => wp_create_nonce( 'usam_product_variations_bulk_action' ),) );
			$actions['show'] = '<a class="usam-variation-show-link" href="'.$url.'" title="'.__( 'Сделать доступным этот вариант для покупателей', 'usam' ).'">'.__( 'Публиковать', 'usam' ).'</a>';
		} 
		elseif ( in_array( $item->post_status, array( 'publish', 'inherit' ) ) ) 
		{
			$url = add_query_arg( array( 'bulk_action' => 'hide', 'post' => $item->ID, 'bulk_action_nonce' => wp_create_nonce( 'usam_product_variations_bulk_action' ),) );
			$actions['hide'] = '<a class="usam-variation-hide-link" href="'.$url.'" title="'. __( 'Скрыть этот вариант от покупателей', 'usam' ).'">'.__( 'Черновик', 'usam' ).'</a>';
		}
		if ( current_user_can( $post_type_object->cap->delete_post, $item->ID ) )
		{
			$force_delete = 'trash' == $item->post_status || ! EMPTY_TRASH_DAYS;		
			if ( 'trash' == $item->post_status ) 
			{
				$url = add_query_arg( array( 'bulk_action' => 'untrash', 'post' => $item->ID, 'bulk_action_nonce' => wp_create_nonce( 'usam_product_variations_bulk_action' ),) );
				$actions['untrash'] = "<a title='".esc_attr( __( 'Восстановить этот вариант из корзины','usam' ) )."' href='".$url."'>".__( 'Восстановить' ,'usam')."</a>";
			} 
			elseif ( EMPTY_TRASH_DAYS )
			{
				$url = add_query_arg( array( 'bulk_action' => 'trash', 'post' => $item->ID, 'bulk_action_nonce' => wp_create_nonce( 'usam_product_variations_bulk_action' ),) );
				$actions['trash'] = "<a class='submitdelete' title='".esc_attr( __('Переместить этот элемент в корзину', 'usam') )."' href='".$url."'>".__('В корзину','usam'). "</a>";			
			}
			if ( $force_delete )
			{
				$url = add_query_arg( array( 'bulk_action' => 'delete', 'post' => $item->ID, 'bulk_action_nonce' => wp_create_nonce( 'usam_product_variations_bulk_action' ),) );
				$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently' ) ) . "' href='" . $url . "'>" . __( 'Delete Permanently' ) . "</a>";
			}
		}
		return $actions;
	}
	
	public function column_image( $item ) 
	{	
		$title = implode( ', ', $this->object_terms_cache[$item->ID] ).($item->post_status == 'draft'?' ('.__('Черновик', 'usam').')':'');
		$thumbnail_id = usam_the_product_thumbnail_id( $item->ID );
		if ( empty($thumbnail_id) )
			$thumbnail_id = usam_the_product_thumbnail_id($this->product_id);		
		?>
		<div class="usam-product-variation-thumbnail">
			<a id ="set-product-thumbnail" data-attachment_id="<?php echo $thumbnail_id; ?>" data-title="<?php echo esc_attr( $title ); ?>" data-product_title="<?php echo esc_attr( $item->post_title ); ?>" data-post_id="<?php echo $item->ID; ?>" href="<?php echo esc_url( admin_url( 'media-upload.php?post_id='.$item->ID. '&TB_iframe=1&width=640&height=566' ) ) ?>">
				<?php echo usam_get_product_thumbnail( $item->ID, 'manage-products' ); ?>
			</a>
		</div>
		<?php	
	}

	public function column_title( $item ) 
	{				
		$title = implode( ', ', $this->object_terms_cache[$item->ID] ).($item->post_status == 'draft'?' ('.__('Черновик', 'usam').')':'');
		$show_edit_link = apply_filters( 'usam_show_product_variations_edit_action', true, $item );		
		?>	
		<strong class="row-title">
			<?php if ( $show_edit_link ): ?>
				<a target="_blank" href="<?php echo esc_url( get_edit_post_link( $item->ID, true ) ); ?>" title="<?php esc_attr_e( __( 'Изменить элемент' ), 'usam' ); ?>">
			<?php endif; ?>
			<?php echo esc_html( $title ); ?>
			<?php if ( $show_edit_link ): ?>
				</a>
			<?php endif; ?>
		</strong>
		<?php 
		echo $this->row_actions( $this->get_row_actions( $item ) ); 
	}

	public function column_stock( $item )
	{
		$stock = usam_get_product_meta( $item->ID, 'stock' );
		?>
			<input type="text" name="usam_variations[<?php echo $item->ID; ?>][stock]" disabled="disabled" value="<?php echo esc_attr( $stock ); ?>" />
		<?php
	}
		
	public function column_sku( $item )
	{
		$sku = usam_get_product_meta( $item->ID, 'sku' );
		?>
			<input type="text" name="usam_variations[<?php echo $item->ID; ?>][sku]" value="<?php echo esc_attr( $sku ); ?>" />
		<?php
	}
	
	public function column_default( $item, $column_name ) 
	{
		$output = apply_filters( 'usam_manage_product_variations_custom_column', '', $column_name, $item );
		return $output;
	}
	
	private function vendor_editor( $item = false ) 
	{
		static $alternate = '';

		if ( ! $item )
			$alternate = '';
		else
			$alternate = ( $alternate == '' ) ? ' alternate' : '';
		$row_class = $alternate;
		$style = '';
		if ( ! $item )
		{
			$item = get_post( $this->product_id );				
			if ( $this->is_bulk_edit )
				$style = ' style="display:table-row;"';
			else
				$style = ' style="display:none;"';			
		}		
		$colspan = count( $this->get_columns() ) - 1;
		$meta = usam_get_product_meta( $item->ID, 'product_metadata' );
		$date_external = usam_get_product_meta( $item->ID, 'date_externalproduct' );		
		?>
		<tr class="usam-vendor-editor-row inline-edit-row<?php echo $row_class; ?> usam-editor-row"<?php echo $style; ?> id="usam-vendor-editor-row-<?php echo $item->ID; ?>">
			<td></td>
			<td colspan="<?php echo $colspan; ?>" class="colspanchange">
				<?php
					$_product_meta = new USAM_Product_Meta_Box($item->ID); 
					$_product_meta->product_link_webspy_form();
				?>	
			</td>
		</tr>
		<?php
	}	
	
	private function stock_editor( $item = false ) 
	{
		static $alternate = '';		

		if ( ! $item )
			$alternate = '';
		else
			$alternate = ( $alternate == '' ) ? ' alternate' : '';
		$row_class = $alternate;
		$style = '';
		if ( ! $item )
		{
			$field_name = "usam_bulk_edit[storage]";			
			if ( $this->is_bulk_edit )
				$style = ' style="display:table-row;"';
			else
				$style = ' style="display:none;"';			
		} 	
		else
			$field_name = "usam_variations[{$item->ID}]";	
		$colspan = count( $this->get_columns() ) - 1;		
		?>
		<tr class="usam-stock-editor-row inline-edit-row<?php echo $row_class; ?> usam-editor-row"<?php echo $style; ?> id="usam-stock-editor-row-<?php echo $item->ID; ?>">
			<td></td>
			<td colspan="<?php echo $colspan; ?>" class="colspanchange">
				<h4><?php esc_html_e( 'Управление товаром на складах', 'usam' ); ?></h4>
				<table class = "storage_table">
					<thead>
						<tr>
							<td><?php esc_html_e( 'Название', 'usam' ); ?></td>
							<td><?php esc_html_e( 'Остаток', 'usam' ); ?></td>	
						</tr>
					</thead>
					<tbody>
					<?php						
					$storages = usam_get_stores();						
					foreach ( $storages as $storage )
					{
						$stock = usam_get_product_meta( $item->ID, $storage->meta_key, true );
						$pmeta_key = $storage->meta_key;
						?>
						<tr>
							<td><label for="storage_<?php echo $storage->id; ?>"><?php echo $storage->title; ?></label></td>
							<td><input type='text' id="storage_<?php echo $storage->id; ?>" name='<?php echo $field_name; ?>[<?php echo $pmeta_key; ?>]' size='3' value='<?php echo $stock; ?>' class='stock_limit_quantity' <?php echo (($storage->shipping==0)?'disabled':''); ?>/>
							</td>							
						</tr>
						<?php			
					}
					?>
					</tbody>
				</table>			
			</td>
		</tr>
		<?php
	}	
	
	private function price_editor( $item = false ) 
	{
		static $alternate = '';	

		if ( ! $item )
			$alternate = '';
		else
			$alternate = ( $alternate == '' ) ? ' alternate' : '';
		$row_class = $alternate;
		$style = '';
		if ( ! $item )
		{
			$field_name = "usam_bulk_edit";			
			if ( $this->is_bulk_edit )
				$style = ' style="display:table-row;"';
			else
				$style = ' style="display:none;"';			
		} 	
		else
			$field_name = "usam_variations";	
		$colspan = count( $this->get_columns() ) - 1;		
		?>
		<tr class="usam-price-editor-row inline-edit-row<?php echo $row_class; ?> usam-editor-row"<?php echo $style; ?> id="usam-price-editor-row-<?php echo $item->ID; ?>">
			<td></td>
			<td colspan="<?php echo $colspan; ?>" class="colspanchange">
				<div id = "usam_prices_forms">
					<?php
					$_product_meta = new USAM_Product_Meta_Box( $item->ID, $field_name ); 
					$_product_meta->price_control_forms();
					?>
				</div>
			</td>
		</tr>
		<?php
	}	

	public function single_row( $item ) 
	{		
		static $count = 0;
		$count ++;
		$item->index = $count;
		echo '<tr class = "row_product_variation" >';
		$this->single_row_columns( $item );
		echo '</tr>';	
		$this->stock_editor( $item );
		$this->price_editor( $item );		
		$this->vendor_editor( $item );		
	}

	/**
	 * Создание навигационной таблице выше или ниже таблицы
	 * @since 3.1.0
	 */
	public function display_tablenav( $which ) 
	{
		?>
		<div class="tablenav <?php echo esc_attr( $which ); ?>">
			<div class="alignleft actions">
				<?php $this->bulk_actions( $which ); ?>
			</div>
			<?php
			$this->extra_tablenav( $which );
			$this->pagination( $which );
			?>
			<br class="clear" />
		</div>
		<?php
	}

	public function display_rows()
	{		
		//$this->display_bulk_edit_row();
		if ( ! $this->is_bulk_edit )
			parent::display_rows();
	}

	public function display_messages()
	{
		if ( isset($_REQUEST['locked']) || isset($_REQUEST['updated']) || isset($_REQUEST['deleted']) || isset($_REQUEST['trashed']) || isset($_REQUEST['untrashed']) )
			$messages = array();
		else
			return;

		if ( !empty($_REQUEST['updated']) ) 
		{
			$updated = absint($_REQUEST['updated']);
			$messages[] = sprintf( _n( '%s post updated.', '%s posts updated.', $updated ), number_format_i18n( $updated ) );
			unset($_REQUEST['updated']);
		}
		if ( isset($_REQUEST['locked']) ) 
		{
			$locked = absint($_REQUEST['locked']);
			$messages[] = sprintf( _n( '%s item not updated, somebody is editing it.', '%s items not updated, somebody is editing them.', $locked ), number_format_i18n( $locked ) );
			unset($_REQUEST['locked']);
		}
		if ( isset($_REQUEST['deleted']) ) 
		{
			$deleted = absint($_REQUEST['deleted']);
			$messages[] = sprintf( _n( 'Вариант удален навсегда.', '%s вариантов удалены навсегда.', $deleted ), number_format_i18n( $deleted ) );
			unset($_REQUEST['deleted']);
		}
		if ( isset($_REQUEST['trashed']) ) 
		{
			$trashed = absint($_REQUEST['trashed']);
			$messages[] = sprintf( _n( 'Товар перемещен в корзину.', '%s товары перемещены в корзину.', $trashed ), number_format_i18n( $trashed ) );
			$ids = isset($_REQUEST['ids']) ? $_REQUEST['ids'] : 0;
			$undo_url = wp_nonce_url( add_query_arg( array( 'doaction' => 'undo', 'action' => 'untrash', 'ids' => $ids ) ), 'bulk-posts' );
			$messages[] = '<a href="' . esc_url( $undo_url ) . '">' . __('Undo') . '</a>';
			unset($_REQUEST['trashed']);
		}
		if ( isset($_REQUEST['untrashed']) ) 
		{
			$untrashed = absint($_REQUEST['untrashed']);
			$messages[] = sprintf( _n( 'Элемент восстановлен из корзины.', '%s элементов востановленно из корзины.', $untrashed, 'usam' ), number_format_i18n( $untrashed ) );
			unset($_REQUEST['undeleted']);
		}
		?>
		<div id="message" class="updated"><p>
		<?php
		echo join( ' ', $messages ); unset( $messages );
		$_SERVER['REQUEST_URI'] = remove_query_arg( array('locked', 'updated', 'deleted', 'trashed', 'untrashed'), $_SERVER['REQUEST_URI'] );
		echo '</p></div>';
	}

	public function get_bulk_actions()
	{
		$actions = array();
		if ( $this->is_trash )
			$actions['untrash'] = __( 'Восстановить' );

		if ( $this->is_draft )
			$actions['show'] = __( 'Опубликовать', 'usam' );			
		elseif ( $this->is_all || $this->is_publish )
			$actions['hide'] = __( 'Черновик', 'usam' );			
		if ( $this->is_pending )
			$actions['show'] = __( 'Опубликовать', 'usam' );	
		if ( $this->is_trash || !EMPTY_TRASH_DAYS )
			$actions['delete'] = __( 'Удалить изображение' );
		else
			$actions['trash'] = __( 'Переместить в корзину' );
		return $actions;
	}

	public function bulk_actions( $which = '' )
	{
		$screen = get_current_screen();

		if ( is_null( $this->_actions ) )
		{
			$no_new_actions = $this->_actions = $this->get_bulk_actions();		
			$this->_actions = apply_filters( 'bulk_actions-' . $screen->id, $this->_actions );
			$this->_actions = array_intersect_assoc( $this->_actions, $no_new_actions );
			$two = '';
		} 
		else
			$two = '2';		
		if ( empty( $this->_actions ) )
			return;

		echo '<input type="hidden" name="bulk_action_nonce" value="' . wp_create_nonce( 'usam_product_variations_bulk_action' ) .'" />';
		echo "<select name='bulk_action$two'>\n";
		echo "<option value='-1' selected='selected'>" . __( 'Действия' ) . "</option>\n";

		foreach ( $this->_actions as $name => $title ) {
			$class = 'edit' == $name ? ' class="hide-if-no-js"' : '';
			echo "\t<option value='$name'$class>$title</option>\n";
		}
		echo "</select>\n";
		submit_button( __( 'Применить' ), 'action', false, false, array( 'id' => "doaction$two" ) );
		echo "\n";
	}

	public function current_action()
	{
		if ( isset( $_REQUEST['bulk_action'] ) && -1 != $_REQUEST['bulk_action'] )
			return $_REQUEST['bulk_action'];

		if ( isset( $_REQUEST['bulk_action2'] ) && -1 != $_REQUEST['bulk_action2'] )
			return $_REQUEST['bulk_action2'];

		return false;
	}

	private function count_variations()
	{
		global $wpdb;
		$query = $wpdb->prepare( "SELECT post_status, COUNT( * ) AS num_posts FROM {$wpdb->posts} WHERE post_type = 'usam-product' AND post_parent = %d GROUP BY post_status", $this->product_id );
		$results = $wpdb->get_results( $query );
	
		$return = array();
		foreach ( $results as $row ) {
			$return[$row->post_status] = $row->num_posts;
		}
		return (object) $return;
	}

	public function get_views()
	{
		global $locked_post_status;
		$parent = get_post( $this->product_id );
		$avail_post_stati = get_available_post_statuses( 'usam-product' );
		$post_type_object = get_post_type_object( 'usam-product' );
		$post_type = $post_type_object->name;
		$url_base = usam_url_admin_action('product_variations_table', '', array( 'product_id' => $this->product_id, 'post_status' => 'all' ));
			
		if ( !empty($locked_post_status) )
			return array();

		$status_links = array();		
		
		$num_posts = $this->count_variations();		
		$class = '';
		$current_user_id = get_current_user_id();		
		$total_posts = array_sum( (array) $num_posts );
	
		foreach ( get_post_stati( array('show_in_admin_all_list' => false) ) as $state ) 
		{
			if ( isset( $num_posts->$state ) )
				$total_posts -= $num_posts->$state;
		}
		$class = empty( $class ) && ( empty($_REQUEST['post_status']) || $_REQUEST['post_status'] == 'all' )&& empty( $_REQUEST['show_sticky'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='{$url_base}' $class>".sprintf( _nx( 'Все <span class="count">(%s)</span>', 'Все <span class="count">(%s)</span>', $total_posts, 'usam' ), number_format_i18n( $total_posts ) ) . '</a>';
		foreach ( get_post_stati(array(), 'objects') as $status ) 
		{ 
			$class = '';
			$status_name = $status->name;	
			if ( !in_array( $status_name, $avail_post_stati ) )
				continue;
			if ( empty( $num_posts->$status_name ) ) 
			{ 
				if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
					$num_posts->$status_name = 0;
				else
					continue;
			}
			if ( isset($_REQUEST['post_status']) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';

			$status_links[$status_name] = "<a href='" . esc_url( add_query_arg( 'post_status', $status_name, $url_base ) ) ."'$class>" . sprintf( translate_nooped_plural( $status->label_count, $num_posts->$status_name ), number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}
		return $status_links;
	}

	public function set_bulk_edited_items( $item_ids )
	{
		$this->prepare_items();
		$this->is_bulk_edit = true;
		foreach ( $this->items as $key => $item )
		{
			if ( in_array( $item->ID, $item_ids ) )
			{
				$this->bulk_edited_items[] = $item;
				unset( $this->items[$key] );
			}
		}
		$this->bulk_edited_item_ids = $item_ids;
	}

	private function display_bulk_edit_row()
	{
		$style = $this->is_bulk_edit ? '' : " style='display:none;'";
		$classes = 'usam-bulk-edit';
		if ( $this->is_bulk_edit )
			$classes .= ' active';
		echo "<tr {$style} class='{$classes}'>";
		list( $columns, $hidden ) = $this->get_column_info();
		foreach ( $columns as $column_name => $column_display_name )
		{
			$class = "class='$column_name column-$column_name inline-edit-row'";
			$style = '';
			if ( in_array( $column_name, $hidden ) )
				$style = ' style="display:none;"';
			$attributes = "$class $style";
			if ( $column_name == 'cb' )
				echo '<td></td>';
			elseif ( method_exists( $this, 'bulk_edit_column_' . $column_name ) )
			{
				echo "<td $attributes>";
				echo call_user_func( array( &$this, 'bulk_edit_column_' . $column_name ) );
				echo "</td>";
			}
		}
		echo '</tr>';	
		$this->stock_editor( );
		$this->price_editor( );		
		$this->vendor_editor();
	}
	
	public function extra_tablenav( $which )
	{
		$post_type_object = get_post_type_object( 'usam-product' );
		?><div class="alignleft actions"><?php
		if ( $this->is_trash && current_user_can( $post_type_object->cap->edit_others_posts ) ) 
		{
			submit_button( __( 'Очистить корзину', 'usam' ), 'button-secondary apply', 'delete_all', false );
		}
		?></div><?php
	}
}