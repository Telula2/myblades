<?php
/**
 * Главные функции продукта в админском интерфейсе. Карточка товара.
 * @since 3.7
 */
 
function usam_redirect_variation_update( $location, $post_id )
{
	global $post;
	if ( !empty($post->post_parent) && 'usam-product' == $post->post_type )
		wp_redirect( admin_url( 'post.php?post='.$post->post_parent.'&action=edit' ) );
	else
		return $location;
}
//add_filter( 'redirect_post_location', 'usam_redirect_variation_update', 10, 2 );

function usam_meta_boxes()
{
	global $post;		
	
	$pagename = 'usam-product';
	remove_meta_box( 'usam-variationdiv', 'usam-product', 'side' );	
	remove_meta_box( 'usam-product_attributesdiv', 'usam-product', 'side' );	
	remove_meta_box( 'commentstatusdiv', 'usam-product', 'normal' );	
	remove_meta_box( 'commentsdiv', 'usam-product', 'normal' );	
	
	$meta_box = new USAM_Product_Meta_Box( $post->ID );
	
	add_action( 'admin_notices', array($meta_box, 'notices') );			
	
	// если страница вариации то не показывать эти метабоксы
	if ( is_object( $post ) && $post->post_parent == 0 ) 
	{		
		add_meta_box( 'usam_attributes_forms', __('Торговое предложение', 'usam'), array($meta_box, 'product_attributes_forms'), $pagename, 'normal', 'high' );
		add_meta_box( 'usam_variation_forms', __( 'Вариации', 'usam' ), array($meta_box, 'product_variation_forms'), $pagename, 'normal', 'high' );	
		//	add_meta_box( 'usam_product_filter', __('Фильтры товаров', 'usam'), array($meta_box, 'product_filter'), $pagename, 'normal', 'high' );
	}
	elseif( is_object( $post ) && $post->post_status == "inherit" ) 
	{
		remove_meta_box( 'tagsdiv-product_tag'             , 'usam-product', 'core' );	
		remove_meta_box( 'usam_product_categorydiv'        , 'usam-product', 'core' );
		remove_meta_box( 'usam-brandsdiv',                   'usam-product', 'side' );
	}	
	add_meta_box( 'usam_prices_forms', __('Управление ценой', 'usam'), array($meta_box, 'price_control_forms'), $pagename, 'normal', 'high' );		
	add_meta_box( 'usam_stock_forms', __('Управление запасом', 'usam'), array($meta_box, 'stock_control_forms'), $pagename, 'side', 'high' );	
	add_meta_box( 'usam_download_forms', __('Загружаемые файлы', 'usam'), array($meta_box, 'product_download_forms'), $pagename, 'normal', 'low' );
	
	$product_type = usam_get_product_type( $post->ID );		
	if ( ! empty( $post->ID ) && ( $product_type == 'simple' || $product_type == 'variable' ) )
	{		// Простой товар или вариация
		add_meta_box( 'usam_related_forms', __('Сопутствующая продукция','usam'), array($meta_box,'related_products_forms'), $pagename, 'normal', 'low' );	
		add_meta_box( 'usam_link_webspy', __('Поставщик товара', 'usam'), array($meta_box, 'product_link_webspy_form'), $pagename, 'normal', 'low' );
	}
	add_meta_box( 'usam_additional_settings_forms', __('Дополнительные настройки', 'usam'), array($meta_box, 'product_advanced_forms'), $pagename, 'side', 'low' );	
	add_meta_box( 'usam_product_images_forms', __('Галерея товара', 'usam'), array($meta_box, 'product_images_forms'), $pagename, 'side', 'low' );	
//	add_action('usam_type_product', array($meta_box, 'type_product'), 5 );

	if ( empty($post->post_title) )
		$work = array( 'title' => __('Создание нового товара','usam') );
	else
		$work = array( 'title' => sprintf(__('Работа c товаром %s','usam'), $post->post_title ) );
	usam_work_on_task( $work, array( 'object_type' => 'product', 'object_id' => $post->ID ) );
}	

/**
 * Удалить мета бокс категорий и брендов в редакторе вариации.
 */
function usam_variation_remove_metaboxes()
{
	global $post;
	if ( ! $post->post_parent )
		return;
	remove_meta_box( 'usam-categorydiv', 'usam-product', 'side' );
	remove_meta_box( 'usam-brandsdiv', 'usam-product', 'side' );
}
add_action( 'add_meta_boxes_usam-product', 'usam_variation_remove_metaboxes', 99 );

class USAM_Product_Meta_Box
{
	private $p_data = array();	
	private $product_id;
	private $product_type;
	private $field_prefix;
	
	public function __construct( $product_id, $field_prefix = '' )
	{					
		$this->product_id = $product_id;
		$product_data = new USAM_Product( $product_id );			
		$this->p_data = $product_data->get_product( true );
		
		$this->product_type = usam_get_product_type( $this->product_id );		
		$field_prefix = $field_prefix != ''?$field_prefix:'product_meta';
		$this->field_prefix = "{$field_prefix}[$this->product_id]";
	}
	
	public function notices(  ) 
	{
		$categories = get_the_terms( $this->product_id , 'usam-category' );				
		if ( empty( $categories ))
		{ 
			?><div class="error"><p><?php _e( 'У товара не указана категория', 'usam' ); ?>.</p></div><?php
		}
	}	
	
	private function edit_box( $title ) 
	{
		return '<a class="edit_box" href="#" title="'.$title.'"><img src="'.USAM_CORE_IMAGES_URL.'/edit.png" width="14"></a>';
	}
	
	function type_product() 
	{		
		?>
		<span class="type_box"> — 
			<label for="product-type">
				<select id="product-type" name="<?php echo $this->field_prefix; ?>[product-type]">
					<optgroup label="<?php _e('Тип продукта', 'usam'); ?>">
						<option value="simple" selected="selected"><?php _e('Простой продукт', 'usam'); ?></option>
						<option value="grouped"><?php _e('Группировка продуктов', 'usam'); ?></option>
						<option value="external"><?php _e('Внешний/Партнерский продукт', 'usam'); ?></option>
						<option value="variable"><?php _e('Вариативный товар', 'usam'); ?></option>
					</optgroup>
				</select>	
			</label>
			<label for="_virtual" class="show_if_simple tips" style="display: inline;"><?php _e('Виртуальный', 'usam'); ?>: <input type="checkbox" name="_virtual" id="_virtual"></label>
			<label for="_downloadable" class="show_if_simple tips" style="display: inline;"><?php _e('Загружаемый', 'usam'); ?>: <input type="checkbox" name="_downloadable" id="_downloadable"></label>
		</span>
		<?php		
	}
	
	function product_attributes_forms() 
	{		
		$product_attributes = usam_get_attributes( $this->product_id );		
		$total = 0;
		$html_table = '';
		
		$ids = array();				
		foreach( $product_attributes as $term )
		{						
			$ids[] = $term->term_id;					
		}	
		$ready_options_attributes = usam_get_product_attribute_values( array( 'attribute_id__in' => $ids ) );	
		$ready_options = array();
		foreach( $ready_options_attributes as $option )
		{
			$ready_options[$option->attribute_id][$option->id] = $option->value;
		}	
		$n = 0;			
		$html_table = '<table class="t_characteristics">';
		foreach( $product_attributes as $term )
		{							
			if ( $term->parent == 0 )
			{						
				$out = '';
				foreach( $product_attributes as $attr )
				{
					if ( $term->term_id == $attr->parent )
					{
						$id = $attr->term_id;
						$disabled = '';			
						$mandatory = get_term_meta($id, 'usam_mandatory', true);
						$type_attribute = get_term_meta($id, 'usam_type_attribute', true);
						if ( $mandatory )
							$class = "class='mandatory'";
						else
							$class = '';
						$meta_key = 'product_attributes_'.$id;			
						if ( $attr->slug == 'brand' )
						{
							$disabled = 'disabled="disabled"';
							$brand = usam_product_brand( $this->product_id );									
							$value = !empty($brand)?$brand->name:'';
						}
						elseif ( isset($this->p_data['meta'][$meta_key]) )
						{							
							$value = $this->p_data['meta'][$meta_key];
						}
						else
							$value = '';
						$total++;
						if ( $value != '' )								
							$n++;
						
						$fled_name = "name='$this->field_prefix[product_attributes][$id]'";
						$fled_id = "id='product_attribute_$id'";
						$out .= "<tr class ='attribute'>
								<td class='name'><label for='product_attribute_$id'>".$attr->name.":</label></td>
								<td class='value'>";
									switch ( $type_attribute ) 
									{
										case 'C' ://Один																									
											$out .= "<input type='hidden' value='0' $disabled $fled_name>";
											$out .= "<input ".checked( $value, true, false )." type='checkbox' value='1' $disabled $class $fled_name $fled_id>";
										break;
										case 'M' ://Несколько																					
											if ( !empty($ready_options[$attr->term_id]) )
											{
												$fled_name = "name='$this->field_prefix[product_attributes][$id][]'";										
												$value = is_array($value)?$value:array($value);														
												$out .= "<select class='chzn-select1' multiple='multiple' $fled_name $fled_id>";
												foreach ( $ready_options[$attr->term_id] as $option_id => $option_name )
												{					
													$selected = in_array($option_id, $value); 
													$out .= "<option value='$option_id' ".selected(true, $selected, false).">$option_name</option>";
												}	
												$out .= "</select>";	
											}
										break;
										case 'N' ://Число
										case 'S' ://Текст											
											if ( !empty($ready_options[$attr->term_id]) )
											{
												$out .= "<select class='chzn-select9' $fled_name $fled_id>";														
												foreach ( $ready_options[$attr->term_id] as $option_id => $option_name )
												{					
													$out .= "<option value='$option_id' ".selected($option_id, $value, false).">$option_name</option>";
												}				
												$out .= "</select>";
											}
										break;																	
										case 'D' ://Дата
											if ( $value != '' )																								
												$date = date_i18n("d-m-Y",  strtotime($value) );
											else											
												$date = '';
											$out .= "<input type='text' class='pattributes_date_picker' $fled_name $fled_id maxlength='10' style='width:90px' value='$date' pattern='(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}' />";
										break;
										case 'O' ://Число
											$out .= "<input $disabled $class value='$value' type='text' $fled_name $fled_id>";
										break;
										case 'U' ://Менеджер	
											$args = array( 'orderby' => 'nicename', 'role__in' => array('editor','shop_manager','administrator','author','contributor'), 'fields' => array( 'ID','display_name') );
											$users = get_users( $args );
											if ( !empty($users) )
											{
												$out .= "<select class='chzn-select9' $fled_name $fled_id>";														
												foreach ( $users as $user )
												{					
													$out .= "<option value='$user->ID' ".selected($user->ID, $value, false).">$user->display_name</option>";
												}				
												$out .= "</select>";
											}
										break;		
										case 'A' ://Агенты	
											$args = array( 'orderby' => 'nicename', 'role__in' => array('remote_agents'), 'fields' => array( 'ID','display_name') );
											$users = get_users( $args );
											if ( !empty($users) )
											{
												$out .= "<select class='chzn-select9' $fled_name $fled_id>";														
												foreach ( $users as $user )
												{					
													$out .= "<option value='$user->ID' ".selected($user->ID, $value, false).">$user->display_name</option>";
												}				
												$out .= "</select>";
											}
										break;											
										case 'T' ://Текст
										default:															
											$value = esc_html__($value);
											$out .=  "<input $disabled $class value='$value' type='text' $fled_name $fled_id>";
										break;
									}									
								$out .= "</td><td class='message' id='required_field'><span>".__('обязательное','usam')."</span></td>
							</tr>";
					}
				}
				if ( $out != '' )
					$html_table .= "<tr><td colspan='2' class='group_name'><strong>".$term->name."</strong></td></tr>".$out;
			}
		}
		$html_table .= "<tfoot><tr><td><a href='".admin_url('edit-tags.php?taxonomy=usam-product_attributes&post_type=usam-product')."' target='_blank'>".__( 'Добавить характеристику', 'usam' )."</a></td><td></td></tr></tfoot>";
		$html_table .= "</table>";	
		if ( $total != 0 )
			$result = round($n/$total*100, 0);
		else 
			$result = 0;
		if ( $result < 30 )
			$color = "#FF0000";
		elseif ( $result >= 30 && $result < 50 )
			$color = "#FFA500";
		elseif ( $result >= 50 && $result < 90 )
			$color = "#BDB76B";
		elseif ( $result <= 90)
			$color = "#32CD32";
		?>	
		<div class ="colums">			
			<div class ="colum1 colums_style">		
			<h4><?php _e('Характеристики товара', 'usam'); ?> <span>(<?php _e('заполнено на ', 'usam'); ?><strong style ="color:<?php echo $color; ?>"><?php echo $result; ?>%</strong>)</span></h4>			
			<div id="message_required_field"><p><?php _e('Заполните обязательные характеристики товара, чтобы сохранить товар', 'usam'); ?>.</p></div>
				<?php echo $html_table; ?>			
			</div>
			<div id = "components" class ="colum2 colums_style">
				<h4><?php _e('Комплектация', 'usam'); ?></h4>
				<table>
					<thead>
						<tr>
							<td></td>
							<td><?php _e( 'Количество', 'usam' ); ?></td>
							<td><?php _e( 'Наименование', 'usam' ); ?></td>	
							<td></td>
						</tr>
					</thead>
					<tbody>
					<?php										
					if ( !empty($this->p_data['meta']['product_metadata']['components']) )
					{
						$n = 0;
						foreach( $this->p_data['meta']['product_metadata']['components'] as $id => $component )
						{							
							$n++;
							echo "<tr class ='component'>
								<td class='n'>$n</td>
								<td class='c'><input value='".$component['c']."' type='text' autocomplete='off' size='5' maxlength='5' name='components[c][]'></td>
								<td><input value='".$component['title']."' type='text' autocomplete='off' name='components[title][]'></td>							
								<td class='add'><a class ='button_add'></a> | <a class ='button_delete'></a></td></tr>";
						}
					}				
					echo "<tr class ='component'>
							<td class='n'>1</td>
							<td class='c'><input value='1' type='text' name='components[c][]'></td>
							<td><input value='' type='text' name='components[title][]'></td>
							<td class='add'><a class ='button_add'></a> | <a class ='button_delete'></a></td>
						</tr>";	
					?>		
					</tbody>					
				</table>
			</div>
		</div>	
		<div class ="product_excerpt">
			<h4><?php _e( 'Дополнительное описание', 'usam' ); ?></h4>
			<?php	
			global $post;
			$settings = array(
				'textarea_name' => 'post_excerpt',
				'quicktags'     => array( 'buttons' => 'em,strong,link' ),
				'tinymce'       => array(
					'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
					'theme_advanced_buttons2' => '',
				),
				'media_buttons' => 0,
				'editor_css'    => '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>',
			);
			wp_editor( htmlspecialchars_decode( $post->post_excerpt ), 'excerpt', apply_filters( 'usam_product_short_description_editor_settings', $settings ) );
			?>		
		</div>	
		<?php
	}
		
	function product_images_forms()
	{
		?>
		<div id="product_images_container">
			<ul class="product_images">
				<?php			
				$attachments = (array)get_posts( array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $this->product_id, 'orderby' => 'menu_order', 'order' => 'ASC' ) );				
				$attachment_ids = array();
				if ( ! empty( $attachments ) ) 
				{
					foreach ( $attachments as $attachment ) 
					{	
						$image_attributes = wp_get_attachment_image_src($attachment->ID, 'admin-product-thumbnails');				
						echo '<li class="image" data-attachment_id="' . esc_attr( $attachment->ID ) . '"><img src="'.$image_attributes[0].'" alt="'.$attachment->post_title.'">
							<ul class="actions">
								<li><a href="#" class="delete dashicons" data-tip="' . esc_attr__( 'Удалить изображение', 'usam' ) . '">' . __( 'Удалить', 'usam' ) . '</a></li>
							</ul>
						</li>';
						$attachment_ids[] = $attachment->ID;
					}			
				}	
				$image_gallery = implode( ',', $attachment_ids );				
				?>				
			</ul>
			<input type="hidden" name="attachment_images" value="<?php echo esc_attr( $image_gallery ); ?>" />
			<input type="hidden" id="product_image_gallery" name="image_gallery" value="<?php echo esc_attr( $image_gallery ); ?>" />
		</div>
		<p class="add_product_images hide-if-no-js">
			<a href="#" data-choose="<?php esc_attr_e( 'Добавить изображание в галерею товара', 'usam' ); ?>" data-update="<?php esc_attr_e( 'Добавить в галлерею', 'usam' ); ?>" data-delete="<?php esc_attr_e( 'Удалить изображение', 'usam' ); ?>" data-text="<?php esc_attr_e( 'Удалить', 'usam' ); ?>"><?php _e( 'Добавить в галлерею', 'usam' ); ?></a>
		</p>
		<?php		
	}	
	
// Цена товара
	function price_control_forms() 
	{
		global $post;
			
		$p_data = $this->p_data['meta'];
		$p_meta = $this->p_data['meta']['product_metadata'];
		$field_prefix = $this->field_prefix."[product_metadata]";	
		
		$discounts = usam_get_product_discount( $this->product_id );
		$discounts_rule = usam_get_product_discount_rules( array( 'fields' => 'id=>data') );

		$option = get_option('usam_underprice_rules',array());
		$underprice_rules = maybe_unserialize( $option );			
		if ( $this->product_type == 'variable' ) 
		{
			?>			
			<p><?php echo sprintf( __( 'Чтобы изменить цены, используйте &laquo;<a href="%s">Управление вариациями</a>&raquo;.', 'usam'), '#usam_variation_forms' ); ?></p>
			<?php		
		}
		else
		{
			?>			
			<h4><?php _e( 'Выберите цену для редактирования', 'usam' ); ?>:</h4>
			<?php			
		}
		$prices = usam_get_prices( array( 'type' => 'all' ) );	
		?>			
		<div id='price_tab' class = "usam_tabs usam_tabs_style1 price_tab">
			<div class='header_tab'>
				<ul>
				<?php 			
				foreach ( $prices as $type_price )
				{		
					echo "<li class='tab'><a href='#tab-".$type_price['id']."'>".$type_price['title']."</a></li>";	
				}				
				?>
				</ul>
			</div>
			<div class='countent_tabs'>
				<?php 						
				foreach ( $prices as $type_price )
				{						
					$price_key = "price_".$type_price['code'];
					$old_price_key = "old_price_".$type_price['code'];
					$underprice_key = "underprice_".$type_price['code'];
					?>
<div id='tab-<?php echo $type_price['id']; ?>' class='tab'>
	<?php 
	if ( $this->product_type == 'variable' ) 
	{
		?>
		<span><?php echo _e( 'Цена от', 'usam' )." ".number_format( (float)$p_data[$price_key], 2, '.', '' ); ?></span>
		<?php
	}
	else
	{
	?>	
	<div class='block_price'>
		<table>
		<?php 
		if ( $type_price['type'] == 'P' ) 
		{
			?>
			<tr>
				<th><label><?php _e( 'Цена', 'usam' ); ?>:</label></th>
				<td><input type='text' size='10' name='<?php echo $this->field_prefix; ?>[<?php echo $price_key; ?>]' <?php disabled( $p_data[$old_price_key] != 0 ); ?> value='<?php echo number_format( (float)$p_data[$price_key], 2, '.', '' ); ?>' /></td>
			</tr>		
			<?php
		}
		else
		{
			if ( $type_price['base_type'] ) 			
			{
				$discont = $p_data[$old_price_key] == 0 ? 0 : round(100 - $p_data[$price_key]*100/$p_data[$old_price_key], 1);		
				?>
				<tr>
					<th><label><?php _e( 'Цена', 'usam' ); ?>:</label></th>
					<td><?php echo number_format( (float)$p_data[$price_key], 2, '.', ' ' ); ?></td>
				</tr>
				<tr>
					<th><label for='add_form_special'><?php _e( 'Старая цена', 'usam' ); ?>:</label></th>
					<td><?php echo number_format( (float)$p_data[$old_price_key], 2, '.', ' ' );  echo $discont>0?' ('.$discont.'%)':'' ?></td>
				</tr>		
				<tr>
					<th><label for='add_form_special'><?php _e( 'Наценка от базовой', 'usam' ); ?>:</label></th>
					<td>
					<?php 
					$underprice = !empty($p_data[$underprice_key])?$p_data[$underprice_key]:0; 				
					?>
					<select name="<?php echo $this->field_prefix; ?>[<?php echo $underprice_key; ?>]">
						<option value="0" <?php selected( 0, $underprice ); ?>><?php _e( 'Не установлено', 'usam' ); ?></option>
					<?php 							
					foreach ( $underprice_rules as $rule )
					{
						?><option value="<?php echo $rule['id']; ?>" <?php selected( $rule['id'], $underprice ); ?>><?php echo $rule['title']." (".$rule['value']."%)"; ?></option><?php											
					}
					?>
					</select>
					</td>
				</tr>
				<?php 
			}
			else
			{  
				$discont = empty($p_data[$old_price_key]) || empty($p_data[$price_key])? 0 : round(100 - $p_data[$price_key]*100/$p_data[$old_price_key], 1);
				?>
				<tr>
					<th><label><?php _e( 'Цена', 'usam' ); ?>:</label></th>
					<td><input type='text' size='10' name='<?php echo $this->field_prefix; ?>[<?php echo $price_key; ?>]' <?php disabled( $p_data[$old_price_key] != 0 ); ?> value='<?php echo number_format( (float)$p_data[$price_key], 2, '.', '' ); ?>' /></td>
				</tr>
				<tr>
					<th><label for='add_form_special'><?php _e( 'Старая цена', 'usam' ); ?>:</label></th>
					<?php 
					if ( $p_data[$old_price_key] == 0 ) 
					{
					?>
					<td><input type='text' size='10' value='<?php echo number_format( (float)$p_data[$old_price_key], 2, '.', '' ); ?>' disabled="disabled"/></td>
					<input type="hidden" name="<?php echo $this->field_prefix; ?>[<?php echo $old_price_key; ?>]" value="<?php echo number_format( (float)$p_data[$old_price_key], 2, '.', '' ); ?>" />
					<?php 
					}
					else
					{
					?>			
					<td><input type='text' size='10' name='<?php echo $this->field_prefix; ?>[<?php echo $old_price_key; ?>]' value='<?php echo number_format( (float)$p_data[$old_price_key], 2, '.', '' ); ?>'/></td>
					<?php 
					}
					?>
				</tr>	
				<tr>
					<th><label for='add_form_special'><?php _e( 'Скидка', 'usam' ); ?>:</label></th>
					<td><?php echo $discont; ?>%</td>
				</tr>	
				<?php
			}
			
		}
		?>
			<tr>
				<th><label for='add_form_special'><?php _e( 'Валюта', 'usam' ); ?>:</label></th>
				<td><?php echo usam_get_currency_name( $type_price['currency']); ?></td>
			</tr>	
		</table>
		<?php 
		if ( $type_price['type'] == 'R' ) 
		{
			?>
		<div class='product_discounts'>
			<?php 	
			$product = usam_get_active_products_day_by_codeprice( $type_price['code'], $this->product_id );			
			if ( !empty($product) )
			{
				$rule = usam_get_data($product->rule_id, 'usam_product_day_rules');
				?>
				<strong><?php _e( 'Товар дня', 'usam' ); ?></strong>
				<table class ="wp-list-table widefat fixed">
					<thead>
						<tr>										
							<td><?php _e( 'Активность', 'usam' ); ?></td>
							<td><?php _e( 'Название', 'usam' ); ?></td>
							<td><?php _e( 'Величина', 'usam' ); ?></td>
							<td><?php _e( 'Действия', 'usam' ); ?></td>				
						</tr>
					</thead>
					<tbody>
					<?php 							
					switch ( $product->dtype ) 
					{
						case 'p' :																									
							$value = $product->value."%";
						break;
						case 'f' :
							$value = usam_currency_display($product->value, array('currency' => $type_price['currency']) );
						break;
						case 't' :
							$value = __('Точная цена:').' '. usam_currency_display($product->value, array('currency' => $type_price['currency']) );
						break;
					}
					?>	
					<tr>							
						<td style="text-align: center;"><?php echo $rule['active']?__('Да','usam'):__('Нет','usam'); ?></td>
						<td style="text-align: left;"><?php echo $rule['name']; ?></td>
						<td style="text-align: right;"><?php echo $value; ?></td>
						<td style="text-align: center;">
							<a href="<?php echo add_query_arg( array('id' => $rule['id'], 'page' => 'manage_discounts', 'tab' => 'product_day', 'action' => 'edit'),admin_url( 'admin.php' ) ) ?>" target="_blank">Изменить</a>
						</td>
					</tr>
					</tbody>
				</table>
				<?php 
			}
			elseif ( !empty($discounts[$type_price['code']]) ) 
			{ ?>
			<strong><?php _e( 'Установленные скидки', 'usam' ); ?></strong>
			<table class ="wp-list-table widefat fixed">
				<thead>
					<tr>										
						<td><?php _e( 'Активность', 'usam' ); ?></td>
						<td><?php _e( 'Название', 'usam' ); ?></td>
						<td><?php _e( 'Величина', 'usam' ); ?></td>
						<td><?php _e( 'Действия', 'usam' ); ?></td>				
					</tr>
				</thead>
				<tbody>
				<?php 								
				foreach ( $discounts[$type_price['code']] as $discount_id )
				{
					$discount = $discounts_rule[$discount_id];
					switch ( $discount['dtype'] ) 
					{
						case 'p' :																									
							$value = $discount['discount']."%";
						break;
						case 'f' :
							$value = usam_currency_display( $discount['discount'], array('currency' => $type_price['currency']) );
						break;
						case 't' :
							$value = __('Точная цена:').' '. usam_currency_display( $discount['discount'], array('currency' => $type_price['currency']) );
						break;
					}
					?>	
					<tr>							
						<td style="text-align: center;"><?php echo $discount['active']?__('Да','usam'):__('Нет','usam'); ?></td>
						<td style="text-align: left;"><?php echo $discount['name']; ?></td>
						<td style="text-align: right;"><?php echo $value; ?></td>
						<td style="text-align: center;">
							<a href="<?php echo add_query_arg( array('id' => $discount['id'], 'page' => 'manage_discounts', 'tab' => 'discount', 'action' => 'edit'),admin_url( 'admin.php' ) ) ?>" target="_blank">Изменить</a>
						</td>
					</tr>
					<?php 												
				}			
				?>	
				</tbody>
			</table>
			<?php 
			} 
			else
			{
				?><strong><?php _e( 'Нет скидок для этого товара', 'usam' ); ?></strong><?php 
			}
			?>			
		</div>	
		<?php 
		}
		?>		
	</div>
	<?php 
	if ( !empty($p_data["price_date"]) ) 
	{	
		?>	
		<div class='date_price'>
			<span><?php _e( 'Последние изменение цены', 'usam' ); ?>: </span><?php echo usam_local_date( $p_data["price_date"] ); ?>	
		</div>
		<?php 
	} ?>
	<?php } ?>
</div>
		<?php 			
		}	
		?>	
			</div>	
		</div>
		<?php 
		if ( $this->product_type !== 'variable' ) 
		{		
		?>	
		<div class='bonuses_div'>
<table>
	<tr>
		<td><label for='add_form_bonuses'><?php _e( 'Бонусы', 'usam' ); ?>:</label></td>
		<td><input id='add_form_bonuses' name="<?php echo $field_prefix; ?>[bonuses][value]" size="5" type="text" value="<?php echo $p_meta['bonuses']['value']; ?>"/></td>
		<td>
		<select class="select_bonuses" name="<?php echo $field_prefix; ?>[bonuses][type]">
			<option value="p" <?php echo ($p_meta['bonuses']['type'] == 'p'?'selected':''); ?>><?php echo esc_html__( 'В процентах', 'usam'); ?></option>
			<option value="f"<?php echo ($p_meta['bonuses']['type'] == 'f'?'selected':''); ?>><?php echo esc_html__( 'Фиксированные', 'usam'); ?></option>
		</select>
		</td>	
	</tr>	
</table>
		</div>				
		<?php
		} 
	}	
	
//Управление товарным запасом
	function stock_control_forms()
	{
		$p_data = $this->p_data['meta'];
		$p_meta = $this->p_data['meta']['product_metadata'];
		$dimension_units = usam_get_dimension_units();	
		$dimension_unit = get_option('usam_dimension_unit'); 
		$measurement_fields = array(
			array(
				'name'   => 'weight',
				'prefix' => $this->field_prefix,
				'label'  => __( 'Вес', 'usam' ),
				'value'  => !empty($p_data['weight'])?$p_data['weight']:0,
				'units'  => usam_get_name_weight_units(),
			),			
			array(
				'name'   => 'length',
				'prefix' => $this->field_prefix.'[product_metadata][dimensions]',
				'label'  => __( 'Длина', 'usam' ),
				'value'  => !empty($p_meta['dimensions']['length'])?$p_meta['dimensions']['length']:0,
				'units'  => $dimension_units[$dimension_unit],
			),
			array(
				'name'   => 'width',
				'prefix' => $this->field_prefix.'[product_metadata][dimensions]',
				'label'  => __( 'Ширина', 'usam' ),
				'value'  => !empty($p_meta['dimensions']['length'])?$p_meta['dimensions']['width']:0,
				'units'  => $dimension_units[$dimension_unit],
			),		
			array(
				'name'   => 'height',
				'prefix' => $this->field_prefix.'[product_metadata][dimensions]',
				'label'  => __( 'Высота', 'usam' ),
				'value'  => !empty($p_meta['dimensions']['length'])?$p_meta['dimensions']['height']:0,
				'units'  => $dimension_units[$dimension_unit],
			),
		);				
		$units = usam_get_list_product_units();
		?>
		<label for="usam_sku"><abbr title="<?php esc_attr_e( 'Артикул товара', 'usam' ); ?>"><?php _e( 'Артикул:', 'usam' ); ?></abbr></label>
		<input size='32' type='text' class='text' id="usam_sku" name='<?php echo $this->field_prefix; ?>[sku]' value='<?php echo esc_html( $p_data['sku'] ); ?>' />
		<label for="code"><abbr title="<?php esc_attr_e( 'Код товара', 'usam' ); ?>"><?php _e( 'Внешний код:', 'usam' ); ?></abbr></label>
		<input size='32' type='text' class='text' id="code" name='<?php echo $this->field_prefix; ?>[code]' value='<?php echo esc_html( $p_data['code'] ); ?>' />
		<br style="clear:both" />
		<label for="barcode"><abbr title="<?php esc_attr_e( 'Штрих-код', 'usam' ); ?>"><?php _e( 'Штрих-код', 'usam' ); ?>:</abbr></label>
		<input size='32' type='text' class='text' id="barcode" name='<?php echo $this->field_prefix; ?>[barcode]' value='<?php echo esc_html($p_data['barcode'] ); ?>' />
		<br style="clear:both" />
		<?php	
		if ( $this->product_type == 'variable' ) : ?>						
			<p><?php echo sprintf( __( 'Чтобы изменить запас используйте &laquo;<a href="%s">Управление вариациями</a>&raquo;' , 'usam' ), '#usam_variation_forms' ); ?>.</p>
			<p><?php printf( _n( "%s вариант товара на складе.", "%s вариантов товара на складе.", $p_data['stock'], 'usam' ), $p_data['stock'] ); ?></p>
		<?php 
		else: 			
			$class_not_limited = $p_data['stock'] == USAM_UNLIMITED_STOCK?'stock_is_not_limited':''; 
			$class = $p_data['virtual']?'virtual_yes':''; ?>
			<div class='product_property'>		
				<input type='checkbox' id="virtual_product" name='<?php echo $this->field_prefix; ?>[virtual]' <?php checked( $p_data['virtual'], 1 ); ?> value="1"/>
				<label for="virtual_product"><?php esc_attr_e( 'Виртуальный', 'usam' ); ?></label>
				<input type='checkbox' id="not_limited" name='not_limited' value="1" <?php checked( $p_data['stock'], USAM_UNLIMITED_STOCK ); ?> />
				<label for="not_limited"><?php esc_attr_e( 'Запас не ограничен', 'usam' ); ?></label>	
			</div>	
			<div class='storage_list'>						
				<table class = "storage_table <?php echo $class_not_limited; ?>">
				<thead>
					<tr>
						<td><?php _e( 'Название склада', 'usam' ); ?></td>
						<td><?php _e( 'Остаток', 'usam' ); ?></td>	
					</tr>
				</thead>
				<tbody>
				<?php						
				$storages = usam_get_stores();					
				foreach ( $storages as $storage )
				{						
					$pmeta_key = $storage->meta_key;
					if ( empty($p_data[$pmeta_key]) )
						$stock = 0;
					elseif ( $p_data[$pmeta_key] >= USAM_UNLIMITED_STOCK )
						$stock = '';
					else
						$stock = $p_data[$pmeta_key];
					?>
					<tr>
						<td><label for="storage_<?php echo $storage->id; ?>">
							<?php 								
							if ( $storage->shipping == 0 )
								echo "<span style = 'color:red'>!</span>";
							echo $storage->title; 
							?>								
						</label></td>
						<td><input type='text' id="storage_<?php echo $storage->id; ?>" name='<?php echo $this->field_prefix; ?>[<?php echo $pmeta_key; ?>]' size='3' value='<?php echo $stock; ?>' class='stock_quantity'/></td>
					</tr>
					<?php			
				}
				?>
				</tbody>
				<tfoot>
					<tr>
						<td><label><?php _e( 'Сумма по всем складам', 'usam' ); ?></label></td>
						<td><span><?php echo $p_data['storage'] == USAM_UNLIMITED_STOCK ? '' : $p_data['storage']; ?></span></td>
					</tr>						
					<tr>
						<td><label><?php _e( 'Резерв', 'usam' ); ?></label></td>
						<td><span><?php echo $p_data['reserve']; ?></span></td>
					</tr>
					<?php if ( $p_data['storage'] != USAM_UNLIMITED_STOCK ) { ?>						
					<tr>
						<td><label><?php _e( 'Доступно', 'usam' ); ?></label></td>
						<td><input type='text' id="stock_limit_quantity" name='<?php echo $this->field_prefix; ?>[stock]' size='3' value='<?php echo $p_data['stock']; ?>' class='stock_quantity' /></td>
					</tr>
					<?php } ?>	
				</tfoot>
				</table>				
			</div>
			<div class='product_warehouse <?php echo $class; ?>'>				
				<div class='unit_measure'>	
					<label for="unit"><abbr title="<?php esc_attr_e( 'Единица измерения', 'usam' ); ?>"><?php _e( 'Коэффициент единицы измерения', 'usam' ); ?>:</abbr></label>
					<input size='8' type='text' id="unit" name='<?php echo $this->field_prefix; ?>[product_metadata][unit]' value='<?php echo esc_html($p_meta['unit'] ); ?>' />
					<select name="<?php echo $this->field_prefix; ?>[product_metadata][unit_measure]">
						<?php			
						foreach ( $units as $key => $title )
						{					
							?><option value="<?php echo $key; ?>" <?php selected( $key, $p_meta['unit_measure'] ); ?>>
								<?php echo $title; ?>
							</option><?php
						}				
						?>
					</select>	
				</div>		
				<div class="usam-stock-editor usam-bulk-edit">			
					<div class="usam-product-shipping-section usam-product-shipping-weight-dimensions">
						<p><strong><?php _e( 'Вес и размеры коробки', 'usam' ); ?></strong></p>
						<table class = "measurement_fields">
						<?php
						foreach ( $measurement_fields as $field ):
						?>
							<tr>
								<td><label for="usam-product-shipping-<?php echo $field['name']; ?>"><?php echo esc_html( $field['label'] ); ?></label></td>
								<td><input type="text" id="usam-product-shipping-<?php echo $field['name']; ?>" name="<?php echo $field['prefix'] .'['.$field['name'].']'; ?>" value="<?php echo esc_attr( $field['value'] ); ?>" /></td>
								<td><?php echo $field['units']; ?></td>
							</tr>			
						<?php
						endforeach;
						?>
						</table>	
					</div>
				</div>
			</div>
			<?php
		endif; 
	}
	
	//Дополнительные настройки
	function product_advanced_forms()
	{
		$p_meta = $this->p_data['meta']['product_metadata'];			
		$field_prefix = $this->field_prefix."[product_metadata]";
		?>
		<label for="usam-product-comments"><?php _e( 'Включить комментарии', 'usam' ); ?>:</label>
		<select id ="usam-product-comments" name='<?php echo $field_prefix; ?>[comment]'>
			<option value='2' <?php echo ( ($p_meta['comment'] == '2') ? 'selected' : '' ); ?> ><?php _e('Использовать по умолчанию', 'usam'); ?></option>
			<option value='1' <?php echo ( ($p_meta['comment'] == '1') ? 'selected' : '' ); ?> ><?php _e( 'Да', 'usam' ); ?></option>
			<option value='0' <?php echo ( ($p_meta['comment'] == '0') ? 'selected' : '' ); ?> ><?php _e( 'Нет', 'usam' ); ?></option>
		</select>
		<?php
		do_action( 'usam_add_advanced_options', $this->product_id );
	}
	
	//Описание: фильтры товаров
	function product_filter()
	{
		$p_meta = $this->p_data['meta']['product_metadata'];		
		
		require_once( USAM_FILE_PATH . '/includes/image/colors.inc.php' );	
		require_once( USAM_FILE_PATH . '/includes/image/group_rgb.php' );	
//Описание: фильтры цветов товаров
		$p_colors = array();
		if ( !empty($p_meta['colors']) )
			$p_colors = array_flip($p_meta['colors']); 
		
		$out = "<div class = 'usam_product_filter_colors'><tr>";
		$out .= "<h4>".__('Фильтры цветов','usam')."</h4>";
		$out .= "<table id = 'table_colors'><tr>";
		foreach ( $group_rgb as $key => $value ) 	
		{			
			$out .= "<td data-color='$key' class = '".(isset($p_colors[$key])?"current":"")."'>
			<div class ='color' style ='background-color: rgb(".$value['red'].", ".$value['green'].", ".$value['blue'].");'></div>			
			<input type='hidden' value='$key' id='bgcolor_$key' name='$this->field_prefix[product_metadata][colors][]' ".(isset($p_colors[$key])?"":"disabled")."></td>";
		}	
		$out .= "</tr></table></div>";
		echo $out;
	}

	//Описание: Поле для хранения ссылки для паука
	function product_link_webspy_form()
	{			
		$p_meta = $this->p_data['meta']['product_metadata'];	
		$field_prefix = $this->field_prefix."[product_metadata]";				
		?> 
		<input type="hidden" value="<?php echo $p_meta['external']['type']; ?>" id="external_product_type" name="<?php echo $field_prefix; ?>[external][type]"/>
		<div id="product_link_webspy">
		<table class="webspy_link_box">
			<tr class="form-field">
				<th valign="top" scope="row"><label for="external_link"><?php _e( 'Внешняя ссылка', 'usam' ); ?>:</label></th>
				<td><input type="text" value="<?php echo $p_meta['webspy_link']; ?>" id="product_link" name="<?php echo $field_prefix; ?>[webspy_link]"/></td>			
			</tr>
			<?php	
			if ( $p_meta['webspy_link'] != '' )
			{						
				$host = parse_url($p_meta['webspy_link'], PHP_URL_HOST);	
				?>
				<tr class="form-field">
					<th valign="top" scope="row"><?php _e( 'Ваш поставщик товара', 'usam' ); ?>:</th>
					<td><a href='<?php echo $p_meta['webspy_link']; ?>' target='_blank'><?php echo $host; ?></a></td>					
				</tr>
				<tr class="form-field">
					<th valign="top" scope="row"><?php _e( 'Последняя проверка', 'usam' ); ?>:</th>
					<td><?php echo date( 'd.m.Y', strtotime( $this->p_data['meta']['date_externalproduct'])); ?></td>						
				</tr>
			<?php
			}
			?>
		</table>				
		<div id = "external" data-tab_start = "<?php echo !empty($p_meta['external']['type'])?$p_meta['external']['type']:'resale'; ?>" class = "usam_tabs usam_tabs_style1" >
			<div class = "header_tab">
				<ul>
					<li class = "title"><?php _e('Выберите вариант внешнего товара','usam'); ?>:</li>
					<li class = "tab <?php echo $p_meta['external']['type'] == 'resale'?'current':''; ?>"><a href='#resale'><?php _e('Перепродажа','usam'); ?></a></li>
					<li class = "tab <?php echo $p_meta['external']['type'] == 'redirect'?'current':''; ?>"><a href='#redirect'><?php _e('Пересылка','usam'); ?></a></li>
				</ul>
			</div>	
			<div class = "countent_tabs">	
				<div id = "resale" class = "tab <?php echo $p_meta['external']['type'] == 'resale'?'current':''; ?>">			
					<p class = "description"><?php _e('Оставьте &laquo;перепродажа&raquo; если Вы продаете товар с другого сайта.','usam'); ?></p>
					<?php	
					if ( $p_meta['webspy_link'] != '' )
					{		
					?>					
					<div class = "webspy_bottom_box">		
						<div class = "webspy_bottom">
							<input type="button" value="<?php _e('Загрузить информацию', 'usam'); ?>" id="usam_webspy_loading_information" class="button button-secondary"/>
							<input type="submit" value="<?php _e( 'Проверить остатки и цену', 'usam' ); ?>" id="check_product_availability_item" class = "button button-secondary" data-id="<?php echo $this->product_id; ?>" name = "check_product_availability_item"/>
						</div>
					</div>
					<?php	
					}		
					?>	
				</div>
				<div id = "redirect" class = "tab <?php echo $p_meta['external']['type'] == 'redirect'?'current':''; ?>">
					<p class = "description"><?php _e('Оставьте &laquo;Пересылка&raquo; если Вы переправляете покупателя на другой веб-сайт. Например, если ваш продукт MP3 файлы для продажи на ITunes.', 'usam' ); ?></p>
					<table class="form-table" style="width: 100%;" cellspacing="2" cellpadding="5">
						<tbody>						
							<tr class="form-field">
								<th valign="top" scope="row"><label for="external_link_text"><?php _e( 'Текст внешней ссылки', 'usam' ); ?></label></th>
								<td><input type="text" name="<?php echo $field_prefix; ?>[external][title]" id="external_link_text" value="<?php esc_attr_e( $p_meta['external']['title']); ?>" size="50" style="width: 95%"></td>
							</tr>
							<tr class="form-field">
								 <th valign="top" scope="row"><label for="external_link_target"><?php _e( 'Внешняя ссылка', 'usam' ); ?></label></th>
								<td>
									<select id="external_link_target" name="<?php echo $field_prefix; ?>[external][target]">
										<option value=""><?php _ex( 'По умолчанию (устанавливается по теме)', 'usam' ); ?></option>
										<option value="_self" <?php echo (($p_meta['external']['target'] == '_self')?'selected':''); ?>><?php _e( 'Открыть ссылку в том же окне', 'usam' ); ?></option>
										<option value="_blank" <?php echo (($p_meta['external']['target'] == '_blank')?'selected':''); ?>><?php _e( 'Открытие ссылки в новом окне', 'usam' ); ?></option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
		</div>	
		</div>	
	<?php
	}
	
	//Описание: Сопутствующая продукция
	function related_products_forms()
	{		
		global $wpdb;		
		$product_ids = usam_get_associated_products( $this->product_id, 'crosssell' );
		if ( !empty( $product_ids ))
		{				
			$in_product = "'".implode("','", $product_ids )."'";	
			$crosssell = $wpdb->get_results("SELECT p.post_title, p.ID FROM ".$wpdb->posts." AS p WHERE p.ID IN ($in_product) AND p.post_type='usam-product' AND p.post_parent='0'");			
		}		
		$field_prefix = $this->field_prefix."[product_metadata]";
		?>				
		<?php 
		if ( !empty($this->p_data['increase_sales_time']) ) 
		{  ?>
			<div class="auto_box">					
				<p><?php printf( __( 'Последнее обновление %s', 'usam' ), usam_local_date( date("Y-m-d H:i:s", $this->p_data['increase_sales_time'] ) ) ); ?></p>							
			</div>	
		<?php } ?>	
		<div class = "related_products_forms">			
			<div id="cross-sells" class="form-field">					
				<strong><?php echo __('Перекрестные продажи (Cross-Sells) в корзине','usam' )."<a target='_blank' href='".admin_url("options-general.php?page=shop_settings&tab=crosssell")."' title='".__('Редактировать правила перекрестных продаж)','usam')."'><img src='".USAM_CORE_IMAGES_URL."/edit.png' width='14'></a>".usam_help_tip(__('Перекрёстные продажи – это также мотивация покупателя потратить больше денег, но уже через продажу товаров из других категорий, нежели изначально выбранная пользователем, т.е. в первую очередь продажа сопутствующих товаров','usam') ); ?></strong><br>	
				<div class="selection_form">
					<?php 
					if ( !empty($crosssell) ) 
					{
						foreach ($crosssell as $product) 
							echo $product->post_title.'<br>';
					}
					else 
					{
						?><span class ="highlight_text"><?php _e('Вы еще не создали правила, подходящие для этого товара. Нажмите редактировать выше.','usam'); ?></span><?php
					}
					?>	
				</div>						
			</div>			
		</div>	
		<?php		
	}
	
	function product_download_forms() 
	{		
		$p_meta = $this->p_data['meta']['product_metadata'];		
		$field_prefix = $this->field_prefix."[product_metadata]";
			/*	<div class="license_enabled_box">
			<input type='checkbox' id = "license_enabled" name='<?php echo $field_prefix; ?>[license][enabled]' value="1"/>
			<label for="license_enabled"><?php _e( 'Использовать лицензию', 'usam' ); ?></label>
		</div>		
		*/
		?>
		<div id ="product_files">		
			<?php echo usam_select_product_file( $this->product_id ); ?>
		</div>
		<div class = "upload_product_files_button">	
			<a href="<?php echo esc_url(usam_url_admin_action('product_files_existing', admin_url( 'admin.php' ), array( 'product_id' => $this->product_id ))); ?>" class="thickbox button button-small" title="<?php echo esc_attr( sprintf( __( 'Выберите все загружаемые файлы для %s', 'usam' ), $this->p_data['post_title'] ) ); ?>"><?php _e( 'Выберите из существующих', 'usam' ); ?></a>
			<div id ="upload_product_files" class = "upload_product_files button button-small">	
				<span><?php _e( 'Загрузить файл', 'usam' ); ?></span>
				<input type='file' name='file' value='' onchange="usam_push_v2t(this, '#usam_fileupload_path')"/>			
			</div>		
			<em id="usam_fileupload_path"></em>
		</div>
		<?php
	//	if ( function_exists( "make_mp3_preview" ) || function_exists( "usam_media_player" ) ) 
		?>
		<br />
		<h4><?php _e( 'Выберите MP3 файл для загрузки в качестве превью', 'usam' ) ?></h4>
		<input type='file' name='preview_file' value='' />		
		<?php
		$args = array( 'post_type'   => 'usam-preview-file', 'post_parent' => $this->product_id, 'numberposts' => -1, 'post_status' => 'all' );
		$preview_files = (array)get_posts( $args );
		if ( !empty($preview_files) )
		{
			?>
			<h4><?php _e( 'Ваш превью для данного продукта', 'usam' ) ?>:</h4>
			<?php		
			foreach ($preview_files as $preview)
				echo $preview->post_title . '<br />';			
		}
		?>
		<span><?php _e( 'Выберите свой файл, затем обновите этот продукт, чтобы закончить загрузку. Максимальный размер загружаемого файла', 'usam' ); ?>: <span><?php echo usam_get_max_upload_size(); ?></span><?php
	}		
	
// Вариации	
	function product_variation_forms()
	{
		$field_prefix = $this->field_prefix."[product_metadata]";
		$id = get_the_ID();
		?>
		<div class="product_variation_iframe">
			<iframe src="<?php echo usam_url_admin_action('product_variations_table', '', array( 'product_id' => $id )); ?>"></iframe>
		</div>
		<div class="product_variation_image_swap">
			<label for="sp_image_swap">
				<?php _e( "Отключить переключение фотографий в вариациях:", 'usam' ); ?>
				<input type="checkbox" value="1" name="<?php echo $field_prefix; ?>[image_swap]" id="sp_image_swap" <?php echo ( $this->p_data['meta']['product_metadata']['image_swap']?' checked="checked"' : '') ?>/>
			</label>
		</div>
		<?php
	}
}
//=====================================================================================================================