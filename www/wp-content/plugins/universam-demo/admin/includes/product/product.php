<?php
/*
* При добавлении в каталог WordPress не имеет права доступа, проблема в этой функции.
*/
function usam_uploaded_files() 
{
	global $wpdb, $usam_uploaded_file_cache;

	$dir = @opendir( USAM_FILE_DIR );
	$num = 0;
	$dirlist = array( );

	if ( count( $usam_uploaded_file_cache ) > 0 )
		$dirlist = $usam_uploaded_file_cache;	
	elseif ( $dir ) 
	{
		while ( ($file = @readdir( $dir )) !== false ) 
		{
			//filter out the dots, macintosh hidden files and any backup files
			if ( ($file != "..") && ($file != ".") && ($file != "product_files") && ($file != "preview_clips") && !stristr( $file, "~" ) && !( strpos( $file, "." ) === 0 ) && !strpos( $file, ".old" ) ) 
			{
				$file_data = null;
				$args = array(
					'post_type' => 'usam-product-file',
					'post_name' => $file,
					'numberposts' => 1,
					'post_status' => 'all'
				);
				//не выбирает по post_name, нужно петли на WordPress API исправить.
				//$file_data = (array)get_posts($args);
				if ( $file_data[0] != null ) 
				{
					$dirlist[$num]['display_filename'] = $file_data[0]->post_title;
					$dirlist[$num]['file_id'] = $file_data[0]->ID;
				} 
				else 
				{
					$dirlist[$num]['display_filename'] = $file;
					$dirlist[$num]['file_id'] = null;
				}
				$dirlist[$num]['real_filename'] = $file;
				$num++;
			}
		}
		if ( count( $dirlist ) > 0 )
			$usam_uploaded_file_cache = $dirlist;		
	}
	$dirlist = apply_filters( 'usam_downloadable_file_list', $dirlist );
	return $dirlist;
}

/*
 * Этот фильтр заменяет строку с контекстным переводом
 */
function usam_filter_gettex_with_context( $translation, $text, $context, $domain ) 
{
	if ( 'Taxonomy Parent' == $context && 'Parent' == $text && isset($_GET['taxonomy']) && 'usam-variation' == $_GET['taxonomy'] ) 
	{
		$translations = &get_translations_for_domain( $domain );
		return $translations->translate( 'Набор вариации', 'usam' );
	}
	return $translation;
}
add_filter( 'gettext_with_context', 'usam_filter_gettex_with_context', 12, 4);

/*
function usam_filter_delete_text( $translation, $text, $domain ) 
{ 
	if ( 'Delete' == $text && isset( $_REQUEST['post_id'] ) && isset( $_REQUEST['parent_page'] ) ) {
		$translations = &get_translations_for_domain( $domain );
		return $translations->translate( 'Удалить', 'usam' ) ;
	}
	return $translation;
}
add_filter( 'gettext', 'usam_filter_delete_text', 12 , 3 );
*/

function usam_form_multipart_encoding() {
	echo ' enctype="multipart/form-data"';
}
add_action( 'post_edit_form_tag', 'usam_form_multipart_encoding' );

function usam_get_max_upload_size()
{
	return size_format( wp_max_upload_size(), 0 );
}


/**
 * Выбор загружаемого файла товара
 */
function usam_select_product_file( $product_id ) 
{
	global $wpdb;
	$product_id = absint( $product_id );
	$file_list = usam_uploaded_files();

	$args = array(
		'post_type' => 'usam-product-file',
		'post_parent' => $product_id,
		'numberposts' => -1,
		'post_status' => 'all'
	);
	$attached_files = (array)get_posts( $args );

	$output = '<table class="wp-list-table widefat fixed posts select_product_file">';
		$output .= '<thead>';
			$output .= '<tr>';
				$output .= '<th>' . _x( 'Название', 'Интерфейс загрузки', 'usam' ) . '</th>';
				$output .= '<th>' . _x( 'Размер', 'Интерфейс загрузки', 'usam' ) . '</th>';
				$output .= '<th>' . _x( 'Тип файла', 'Интерфейс загрузки', 'usam' ) . '</th>';
				$output .= '<th>' . _x( 'Действия', 'Интерфейс загрузки', 'usam' ) . '</th>';		
			$output .= '</tr>';
		$output .= '</thead>';	
		$output .= '<tbody>';
	$num = 0;	
	$delete_nonce = usam_create_ajax_nonce( 'delete_file' );
	if( !empty( $attached_files ) )
		foreach ( (array)$attached_files as $file ) 
		{
			$file_dir = USAM_FILE_DIR . $file->post_title;
			$file_size = ( 'http://s3file' == $file->guid ) ? __( 'Размеры удаленного файла не может быть рассчитана', 'usam' ) : size_format( filesize( $file_dir ) );

			$file_url = usam_url_admin_action('admin_download_file', admin_url( 'admin.php' ), array( 'file_id' => $file->ID ));
			$deletion_url = usam_url_admin_action('delete_file', admin_url( 'admin.php' ), array( 'file_id' => $file->ID ));

			$num = absint( $num );
			$class = ( ! ($num & 1) ) ? 'alternate' : '';

			$output .= '<tr class="usam_product_download_row ' . $class . '">';
			$output .= '<td style="padding-right: 30px;">' . $file->post_title . '</td>';
			$output .= '<td>' . $file_size .'</td>';
			$output .= '<td>.' . usam_get_extension( $file->post_title ) . '</td>';
			$output .= "<td><a href='$file_url'>" . __( 'Загрузить', 'usam' ) . "</a>&#160;|&#160;<a data-file-name='".esc_attr( $file->post_title )."' data-product-id='$product_id' data-file_id='$file->ID' data-nonce='".esc_attr( $delete_nonce ) . "' class='file_delete_button' href='{$deletion_url}' >" . __( 'Удалить', 'usam' ) . "</a></td>";
			$output .= '</tr>';
			$num++;
		}
	else
	{
		$output .= "<tr><td class='no-item' colspan='4'>" . __( 'Нет файлов, прикрепленных к вашему товару. Загрузить новый файл или выберите файлы из других товаров.', 'usam' ) . "</td></tr>";
	}
	$output .= '</tbody>';
	$output .= '</table>';	
	$output .= "<div class='" . ( ( is_numeric( $product_id ) ) ? 'edit_' : '') . "select_product_handle'></div>";
	$output .= "<script type='text/javascript'>\r\n";
	$output .= "var select_min_height = " . ( 25 * 3 ) . ";\r\n";
	$output .= "var select_max_height = " . ( 25 * ( $num + 1 ) ) . ";\r\n";
	$output .= "</script>";

	return $output;
}