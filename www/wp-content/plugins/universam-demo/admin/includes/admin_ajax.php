<?php
// Класс работает ajax в админском интерфейсе
new USAM_Admin_Ajax();
class USAM_Admin_Ajax extends USAM_Callback
{				
	protected $ajax_parameter = 'usam_ajax_action';
	public function controller_display_category_in_attributes_product()
	{		
		if ( !empty($_POST['term_id']) )
		{
			$term_id = absint($_POST['term_id']);
			$selected = usam_get_taxonomy_relationships( $term_id, 'usam-category', $colum = '1' );				
		}
		else
			$selected = array();
		$content = wp_terms_checklist( 0, array( 'taxonomy' => 'usam-category', 'selected_cats' => $selected, 'checked_ontop' => false, 'echo' => false ) );			
		return $content;
	}	
	
	public function controller_change_product_price()
	{
		if ( empty($_POST['products']) )
			return 0;
		
		$products = $_POST['products'];		
		$i = 0;
		$code_price = sanitize_text_field($_POST['code_price']);
		foreach ( $products as $product ) 
		{
			$product_id = absint($product['product_id']);
			$price = (float)$product['price'];		
		
			$prices['price_'.$code_price] = $price;	
			usam_edit_product_prices( $product_id, $prices );					
			$i++;
		}
		return $i;
	}
	
	public function controller_add_products_vk()
	{		
		if ( empty($_POST['products']) || !is_array($_POST['products']) )
			return array( 'error' => 1 );
		
		$products_ids = array_map('intval', $_POST['products']);		
		$group_id = sanitize_title($_POST['group']);	
		$category_id  = absint($_POST['category']);
		$album_id  = absint($_POST['album_id']);		
		$profile_id  = $_POST['profile_id'];
		$i = 0;	
		
		require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
		$vkontakte = new USAM_VKontakte_API();	
		
		$profiles = $vkontakte->user_and_group( 'group' );		
		foreach ( $profiles as $profile ) 
		{											
			if ( $group_id != 'all' && $profile['page_id'] != $group_id )
					continue;
				
			foreach ( $products_ids as $product_id )
			{	
				if( $vkontakte->publish_product( $product_id, $profile, $category_id, $album_id ) ) 
					$i++;
			}
		}
		$vkontakte->set_log_file();
		return array( 'error' => 0, 'update' => $i );
	}
	
	public function controller_pointer_close()
	{		
		if ( !empty($_POST['screen']) && !empty($_POST['name']) )
		{ 
			$user_pointer = get_user_option( $user_ID, 'usam_pointer' );
			$user_pointer = maybe_unserialize( $user_pointer );	
			$user_pointer = empty($user_pointer)?array():$user_pointer;
			
			$screen = sanitize_title($_POST['screen']);
			$name = sanitize_title($_POST['name']);			
			$user_pointer[$screen][$name] = 1;		
			$user_pointer = unserialize( $user_pointer );			
			return update_user_option( $user_ID, 'usam_pointer', $user_pointer );	
		}
		return false;
	}	
	
	public function controller_confirm_deactivation()
	{		
		if ( !empty($_POST['reason']) && !empty($_POST['message']) )
		{
			$reason = sanitize_textarea_field(stripslashes($_POST['reason']));
			$message = sanitize_textarea_field(stripslashes($_POST['message']));
			
			$api = new Universam_API();
			$result = $api->confirm_deactivation( $reason, $message );	
			
			return $result;
		}
		return false;
	}		
	
	public function controller_product_rating()
	{
		global $wpdb;	
		$nowtime = time();
		$product_id = absint( $_REQUEST['product_id'] );
		$rating = absint( $_REQUEST['rating'] );		
		
		$rating_count = (int)usam_get_product_meta( $product_id, 'rating_count' );	
		$p_rating = (int)usam_get_product_meta( $product_id, 'rating' );
		
		$rating_sum = $p_rating*$rating_count;		
			
		++$rating_count;	
		
		$rating_sum += $rating;
		$rating_new = $rating_sum / $rating_count;	
	
		usam_update_product_meta( $product_id, 'rating_count', $rating_count );	
		usam_update_product_meta( $product_id, 'rating', $rating_new );	
		return array( 'rating_count' => $rating_count, 'rating' => $rating_new );
	}		
	
	public function controller_bulk_actions_product_attribute()
	{
		if ( empty($_POST['products']) || !is_array($_POST['products']) )
			return false;
		
		$attributes = $_POST['attributes'];	
		$products_ids = array_map('intval', $_POST['products']);	
		$product_attributes = array();				
		foreach( $attributes as $attribute )
		{			
			$id = absint($attribute['id']);	
			if ( is_array($attribute['value']) )
				$value = stripslashes_deep($attribute['value']);	
			else
				$value = absint($attribute['value']);	
			$product_attributes[$id] = $value;
		}						
		foreach( $products_ids as $product_id )
		{				
			$product_meta = usam_get_product_meta( $product_id );			
			foreach( $product_meta as $meta_key => $meta )
			{	
				foreach( $meta as $meta_value )
				{
					$explode = explode('_usam_product_attributes_',$meta_key);				
					if ( !empty($explode[1]) )
					{							
						if ( !isset($product_attributes[$explode[1]]) )
						{
							if ( count($meta) > 1 )
								$product_attributes[$explode[1]][$meta_value] = $meta_value;
							else
								$product_attributes[$explode[1]] = $meta_value;
						}
					}
				}		
			}	
			$product = new USAM_Product( $product_id );	
			$product->calculate_product_attributes( $product_attributes );
			$product->save_product_meta( );		
		}
	}	
	
	public function controller_bulk_actions_terms()
	{		
		if ( empty($_POST['products']) || !is_array($_POST['products']) )
			return false;
						
		$products_ids = array_map('intval', $_POST['products']);		
		$operation = sanitize_title($_POST['operation']);	
		$category  = absint($_POST['category']);	
		$brands    = absint($_POST['brands']);	
		$category_sale = absint($_POST['category_sale']);				
		
		foreach( $products_ids as $product_id )
		{			
			$product_id = absint($product_id);	
			if ( $operation == 'del' )			
			{
				if ( !empty($category) )
					wp_remove_object_terms( $product_id, $category, 'usam-category' );			
				if ( !empty($brands) )
					wp_remove_object_terms( $product_id, $brands, 'usam-brands' );		
				if ( !empty($category_sale) )				
					wp_remove_object_terms( $product_id, $category_sale, 'usam-category_sale' );	
			}
			else
			{				
				if ( !empty($category) )
					wp_set_object_terms( $product_id, $category, 'usam-category', $operation );			
				if ( !empty($brands) )
					wp_set_object_terms( $product_id, $brands, 'usam-brands' );		
				if ( !empty($category_sale) )
					wp_set_object_terms( $product_id, $category_sale, 'usam-category_sale', $operation );	
			}	
		}	
	}
	
	public function controller_update_mailboxes_sort_fields()
	{		
		if ( empty($_POST['sort_order']) || !is_array($_POST['sort_order']) )
			return false;
		
		$taxonomy_order = (array)$_POST['sort_order'];		
		$i = 0;	
		foreach( $taxonomy_order as $id )
		{			
			$id = preg_replace("/[^0-9]/", '', $id);			
			usam_update_mailbox( $id, array( 'sort' => $i ) );
			$i++;
		}
	}	
	
	//Сортировка терминов
	public function controller_set_taxonomy_order()
	{		
		if ( empty($_POST['sort_order']) || !is_array($_POST['sort_order']) )
			return false;
		
		$taxonomy_order = (array)$_POST['sort_order'];		
		$result = true;			
		$i = 0;		
		foreach( $taxonomy_order as $id )
		{			
			$term_id = preg_replace("/[^0-9]/", '', $id);			
			update_term_meta( $term_id, 'usam_sort_order', $i );				
			$i++;
		}		
		return $result;
	}
	
	//Сортировка терминов
	public function controller_set_taxonomy_sets_orderp()
	{		
		if ( empty($_POST['sort_order']) || !is_array($_POST['sort_order']) )
			return false;
		
		$taxonomy_order = (array)$_POST['sort_order'];		
		$result = true;			
		$i = 0;		
		foreach( $taxonomy_order as $id )
		{			
			$term_id = preg_replace("/[^0-9]/", '', $id);			
			update_term_meta( $term_id, 'usam_sort_sets_products', $i );				
			$i++;
		}		
		return $result;
	}
	
	//Удалить товар из заказа
	public function controller_review_edit() 
	{				
		global $wpdb;		
		$id    = absint($_REQUEST['id']);
		$colum = sanitize_text_field($_REQUEST['col']);
		$value = sanitize_text_field($_REQUEST['value']);		
		
		if ( $colum == 'custom_fields' )	
		{   //обновить произвольное поле	
			$field_name = substr($_REQUEST['attribute'],7); 		
			
			$review = usam_get_review( $id );
			if ( isset($review['custom_fields'][$field_name]) )
				$review['custom_fields'][$field_name] = $value;
			else
				return false;
		}
		else 			
			$review = array( $colum => $value );	
		
		usam_update_review( $id, $review );
		
		return $value;
	}
	
	function controller_set_variation_thumbnail() 
	{		
		$post_id = absint( $_REQUEST['post_id'] );	
		$attachment_id = absint( $_REQUEST['attachment_id'] );	
		return set_post_thumbnail( $post_id, $attachment_id );
	}
		
	/**
	 * Обновление избранных товаров, когда нажимают на звездочку.
	 */
	function controller_update_featured_products() 
	{		
		$product_id = absint( $_REQUEST['product_id'] );	
		$new_status = absint( $_REQUEST['status'] );	
		if ( !current_user_can('edit_post', $product_id) )
			return array( 'title' => __( 'У вас не прав на изменения', 'usam' ), 'result' => 0);
		
		usam_update_product_meta( $product_id, 'sticky_products', $new_status );	
		if ( $new_status )
			$title = __( 'Убрать из Избранного', 'usam' );
		else
			$title = __( 'Добавить в Избранное', 'usam' );
		
		$out = array( 'title' => $title, 'result' => 1);		
		return $out;
	}
	
	function controller_recalculate_prices() 
	{
		if ( isset($_POST['id']) && is_numeric( $_POST['id'] ))
			$order_id = absint($_POST['id']);
		else
			return;
		
		if ( isset($_POST['type_price']) )
			$type_price = sanitize_title($_POST['type_price']);
		else
			return;
	
		$order = new USAM_Order( $order_id );
		$products = $order->get_order_products();	
		
		$result = array();
		foreach( $products as $product )
		{
			$price = usam_get_product_meta( $product->product_id, 'price_'.$type_price );	
			$result[] = array( 'price' => $price, 'id' => $product->id, );	
		}
		return $result;
	}	
			
	function controller_add_comment() 
	{		
		global $user_ID;
		if ( isset($_POST['id']) && isset($_POST['message'])  )
		{
			$object_id = absint($_POST['id']);	
			$object_type = sanitize_title($_POST['type']);	
			$message = sanitize_textarea_field($_POST['message']);	
			
			$id = usam_insert_comment( array( 'message' => $message, 'object_id' => $object_id, 'object_type' => $object_type ) );
			
			$date =  usam_local_formatted_date( date( "Y-m-d H:i:s" ) );			
			return array( 'id' => $id, 'date' => $date, 'message' => $message, 'author' => usam_get_manager_name( $user_ID ) );
		}
		else
			return  false;
	}	

	function controller_delete_comment() 
	{		
		if ( isset($_POST['id']) )
		{
			$id = absint($_POST['id']);				
			return usam_delete_comment( $id );
		}	
		return false;
	}		
	
	function controller_edit_comment() 
	{		
		if ( !empty($_POST['id']) && isset($_POST['message'])  )
		{
			$id = absint($_POST['id']);	
			$message = sanitize_textarea_field($_POST['message']);	
			
			return usam_update_comment( $id, array( 'message' => $message ) );
		}	
		return  false;
	}	

	function controller_add_participant() 
	{			
		if ( !empty($_POST['event_id']) && !empty($_POST['user_id'])  )
		{
			$event_id = absint($_POST['event_id']);	
			$user_id = absint($_POST['user_id']);			
			return usam_set_event_user( $event_id, $user_id );
		}
		else
			return false;
	}	
	
	function controller_delete_event_participant() 
	{			
		if ( isset($_POST['event_id']) && isset($_POST['user_id'])  )
		{
			$event_id = absint($_POST['event_id']);	
			$user_id = absint($_POST['user_id']);			
			return usam_delete_event_user( $event_id, $user_id );
		}
		else
			return  false;
	}		
	
	function controller_add_event_action() 
	{		
		global $user_ID;
		if ( isset($_POST['id']) && isset($_POST['name'])  )
		{
			$event_id = absint($_POST['id']);	
			$name = sanitize_textarea_field($_POST['name']);	
			
			$id = usam_insert_event_action( array( 'name' => $name, 'event_id' => $event_id ) );
					
			return array( 'id' => $id, 'name' => $name );
		}
		else
			return  false;
	}	

	function controller_delete_event_action() 
	{		
		if ( isset($_POST['id']) )
		{
			$id = absint($_POST['id']);				
			return usam_delete_event_action( $id );
		}	
		return  false;
	}		
	
	function controller_edit_event_action() 
	{		
		if ( isset($_POST['id']) )
		{
			$id = absint($_POST['id']);	
			$update = array();
			if ( isset($_POST['name'])  )
				$update['name'] = sanitize_textarea_field($_POST['name']);	
			
			if ( isset($_POST['made'])  )
				$update['made'] = sanitize_textarea_field($_POST['made']);	
			
			return usam_update_event_action( $id, $update );
		}	
		return  false;
	}
			
	//Добавить товар в коммерческое предложение
	function controller_add_product_to_document() 
	{	
		$return = array('error' => '' );	
		if ( isset($_POST['id']) && is_numeric( $_POST['id'] ))
			$id = absint($_POST['id']);
		else
			return $return['error'] = __('Не удалось добавить товар. Не получен номер документа. Попробуйте еще раз.','usam');	
		
		if ( isset($_POST['product_id']) && is_numeric( $_POST['product_id'] ))
			$product_id = absint($_POST['product_id']);
		else
			return $return['error'] = __('Не удалось добавить товар. Не получен номер товара. Попробуйте еще раз.','usam');

		if ( isset($_POST['document']) )
			$document = sanitize_title($_REQUEST['document']);	
		else
			return $return['error'] = __('Не удалось добавить товар. Не указан документ. Попробуйте еще раз.','usam');
			
		$product = array( 'product_id' => $product_id, 'quantity' => 1 );
		
		$crm = new USAM_Document( $id );
		$result = $crm->add_product( $product );
		if ( $result )
		{				
			ob_start();				
			$item_table = usam_get_class_form_display( 'crm', $document );
			$item_table->table_products();			
			$return['content'] = ob_get_clean();
		}	
		else
			$return['error'] = __('Не удалось добавить товар. Попробуйте еще раз.','usam');
		return $return;
	}	

	// Добавление товара в корзину
	function controller_add_order_item() 
	{		
		if ( isset($_POST['id']) && is_numeric( $_POST['id'] ))
			$id = absint($_POST['id']);
		else
			return;		
		
		if ( isset($_POST['product_id']) && is_numeric( $_POST['product_id'] ))
			$product_id = absint($_POST['product_id']);
		else
			return;				
		if ( isset($_POST['quantity']) &&  is_numeric( $_POST['quantity'] ) )
			$quantity = $_POST['quantity'];	
		else 
			$quantity = 1;			
	
		$return = array('error' => '', 'cart_product' => '', 'document_shipped' => '');	
		
		if ( !empty($_POST['document_shipped']) )		
			$cart_parameters['document_shipped'] = $_POST['document_shipped'];				
		
		if ( !empty($_POST['documents_payment']) )		
			$cart_parameters['documents_payment'] = $_POST['documents_payment'];							
					
		$result = usam_add_product_to_order( $id, $product_id, $quantity, $cart_parameters );				
		if ( $result['result'] )
		{					
			$form = usam_get_class_form_display( 'orders' );
			if ( is_object($form) )
			{
				ob_start();					
				$form->get_form( 'order_products' );			
				$return['cart_product'] = ob_get_contents();
				ob_end_clean();
				
				ob_start();					
				$form->get_form( 'shipped_documents' );
				$return['document_shipped'] = ob_get_contents();
				ob_end_clean();	

				ob_start();					
				$form->get_form( 'payment_history' );
				$return['payment_history'] = ob_get_contents();
				ob_end_clean();			
			}	
			else
				$return['error'] = __('Не нашел форму заказа','usam');
		}	
		else
			$return['error'] = $result['error'];
		return $return;
	}
	
	function controller_add_document_payment() 
	{	
		ob_start();	
		$form = usam_get_class_form_display( 'orders' );
		if ( is_object($form) )
			$form->get_form( 'payment_document' );
		
		$out = ob_get_contents();
		ob_end_clean();
		
		return $out;
	}	
	
	
	function controller_add_document_shipped() 
	{			
		ob_start();	
		$form = usam_get_class_form_display( 'orders' );
		if ( is_object($form) )
			$form->get_form( 'document_shipped' );
		
		$out = ob_get_contents();
		ob_end_clean();
		
		return $out;
	}	
		
	function controller_upload_customer_data_in_order() 
	{	
		$user_id    = absint( $_REQUEST['user_id'] );		
		$checkout_details = array();
		if ( $user_id > 0 )
		{
			$checkout_details = usam_get_customer_meta( 'checkout_details', $user_id );			
		}
		if ( empty($checkout_details) )
		{
			$checkout_details = array();
			$list_properties = usam_get_order_properties( array('fields' => 'id=>unique_name') );	
			foreach ( $list_properties as $key => $item )
				$checkout_details[$key] = '';
		}
		return $checkout_details;
	}	
	
	function controller_billing_same_as_shipping() 
	{
		$order_id = absint( $_REQUEST['order_id'] );		
		
		$purchase_log = new USAM_Order( $order_id );
		$customer_data = $purchase_log->get_customer_data( );
		$result = array();			
		foreach ( $customer_data as $key => $item )
		{
			if ( strpos( $key, 'billing' ) !== false )
			{
				$k = str_replace('billing', 'shipping', $key );
				$id = $customer_data[$k]['id'];
				$result[$id] = $item['value'];
			}			
		}		
		return $result;	
	}
	
	/**
	 * Отправить номер отслеживания(tracking) по электронной почте
	 * @since 3.8.9
	 */
	function controller_send_tracking_email() 
	{
		$order_id = absint( $_POST['order_id'] );
		$shipping_id  = absint( $_POST['item_id'] );
		
		$notification = new USAM_Сustomer_Notification_Tracking( $order_id, $shipping_id );
		$email_sent = $notification->send_mail();

		if ( ! $email_sent )
			return new WP_Error( 'usam_cannot_send_tracking_email', __( "Не удалось отправить письмо с номером отслеживания почтового отправления. Пожалуйста, попробуйте еще раз.", 'usam' ) );			
		return $email_sent;
	}	
		
	//Загрузить адрес доставки или платежный адрес
	function controller_purchase_log_load_customer_address() 
	{	
		$customer_id = absint($_POST['customer_id']);	
		$type = $_POST['type'];	
		$return = array();
		$list_properties = usam_get_order_properties( array('fields' => 'id=>unique_name') );	
		$profile = usam_get_all_customer_meta( $customer_id );
		foreach ( $profile['checkout_details'] as $key => $detail )
		{		
			if ( stripos($list_properties[$key], $type ) !== false ) 
				$return[$list_properties[$key]] = $detail;
		}
		return $return;
	}
		
	//Добавить бокс с добавлением товара в заказ
	function controller_add_product_box() 
	{	
		ob_start();	
		$form = usam_get_class_form_display( 'orders' );
		if ( is_object($form) )
			$form->get_form( 'add_product_box' );
		
		$out = ob_get_contents();
		ob_end_clean();
		
		return $out;
	}
	
	//Обновление полей формы "Оформить заказ"
	function controller_update_order_property_sort_fields() 
	{		
		if ( !empty($_REQUEST['sort_order']) )
		{
			$fields = stripslashes_deep($_REQUEST['sort_order']);	
			foreach($fields as $key => $id) 
			{ 	
				$id = absint( $id );			
				$args = array( 'sort' => absint($key) );
				usam_update_order_property( $id, $args );								
			}			
		}			
	}	

	//Сортировка пунктов меню
	function controller_update_menu_sort_fields() 
	{
		if ( !empty($_REQUEST['sort_order']) )
		{
			$fields = stripslashes_deep($_REQUEST['sort_order']);	
			foreach($fields as $key => $id) 
			{ 	
				$id = absint( $id );	
				update_term_meta( $id, 'usam_sorting_menu_categories', absint($key) );
			}			
		}			
	}

	/**
	 * Добавить новый вариант вариации. Если имя вариации такое же, как существующий набор вариаций, дети для варианта термина будут добавлены внутри этого существующего набора.
	 */
	function controller_add_variation_set() 
	{
		$new_variation_set = stripslashes_deep($_POST['variation_set']);
		$variants = preg_split( '/\s*,\s*/', $_POST['variants'] );

		$return = array();

		$parent_term_exists = term_exists( $new_variation_set, 'usam-variation' );
		// только использовать существующий родительский ID, если термин не термин ребенок
		if ( $parent_term_exists ) {
			$parent_term = get_term( $parent_term_exists['term_id'], 'usam-variation' ); echo '1+';
			if ( $parent_term->parent == '0' )
				$variation_set_id = $parent_term_exists['term_id'];
		}

		if ( empty( $variation_set_id ) ) {
			$results = wp_insert_term( $new_variation_set, 'usam-variation' );
			if ( is_wp_error( $results ) )
				return $results;
			$variation_set_id = $results['term_id'];
		}
		if ( empty( $variation_set_id ) )
			return new WP_Error( 'usam_invalid_variation_id', __( 'Не удается получить вариацию набора, чтобы продолжить.', 'usam' ) );

		foreach ( $variants as $variant ) 
		{
			$results = wp_insert_term( $variant, 'usam-variation', array( 'parent' => $variation_set_id ) );
			if ( is_wp_error( $results ) )
				return $results;
			$inserted_variants[] = $results['term_id'];
		}
		require_once( USAM_FILE_PATH . '/admin/includes/product/walker-variation-checklist.php' );

		/* Следующие 3 строки будут удалены детьми термина кэша для вариации. Без этого, новые вариации дети не будет отображаться на странице "Вариации", 
		и также не будет отображаться в wp_terms_checklist().
		*/
		clean_term_cache( $variation_set_id, 'usam-variation' );
		delete_option('usam-variation_children');
		wp_cache_set( 'last_changed', 1, 'terms' );
		_get_term_hierarchy('usam-variation');

		ob_start();
		wp_terms_checklist( (int) $_POST['post_id'], array(
			'taxonomy'      => 'usam-variation',
			'descendants_and_self' => $variation_set_id,
			'walker'        => new USAM_Walker_Variation_Checklist( $inserted_variants ),
			'checked_ontop' => false,
		) );
		$content = ob_get_clean();

		$return = array(
			'variation_set_id'  => $variation_set_id,
			'inserted_variants' => $inserted_variants,
			'content'           => $content,
		);
		return $return;
	}
	
	function controller_get_support_messages()
	{
		require_once( USAM_FILE_PATH . '/includes/technical/support_message_query.class.php'   );
		
		$paged = !empty( $_POST['paged'])? $_POST['paged']:1;
		$number = !empty( $_POST['number'])? $_POST['number']:20;
		
		$support_messages = usam_get_support_message_document( array('order' => 'DESC', 'paged' => $paged, 'number' => $number) );	
		if ( !empty($support_messages) )
		{
			foreach( $support_messages as $key => $value) 
			{
				if ( $value->read == 0 )
					usam_update_support_message( $value->id, array( 'read' => 1 ) );
				
				if ( date("Y", strtotime($value->date_insert) ) == date("Y") )
					$format = "d F H:m";
				else
					$format = "d F Y H:m";
				
				$subject = usam_get_subject_support_message();			
				$support_messages[$key]->subject = isset($subject[$value->subject])?$subject[$value->subject]:__('Ответ службы поддержки','usam');
				$support_messages[$key]->date = usam_local_date( $value->date_insert, $format );			
			}
		}		
		return $support_messages;
	}
	
	function controller_send_support_message()
	{					
		$result = false;
		if ( !empty($_POST['message']) && isset($_POST['subject']) ) 
		{
			$insert = array( 'message' => $_POST['message'], 'subject' => $_POST['subject']  );			
			
			$api = new Universam_API();
			$result = $api->sent_support_message( $insert );
			
			$result = usam_insert_support_message( $insert );
		}
		return $result;
	}	

	//Загрузка вкладки страницы
	function controller_navigate_tab()
	{				
		$return = array( 'error' => '' );			
		if (class_exists($page_class)) 
		{
			ob_start();	
			$page = new USAM_Page_Tabs( $_POST['tab'] );
			$page->display_current_tab();			
			$return['content'] = ob_get_clean();
		}
		else
			$return['error'] = sprintf( __('Класс %s не объявлен','usam'), $page_class );
		return $return;
	}

	function controller_payment_save_external_document()
	{	
		$id = absint($_POST['payment_id']);
		$payment['external_document'] = sanitize_text_field($_POST['document_number']);			
		$update = usam_update_payment_document( $id, $payment );
		
		if ( ! $update )
			return new WP_Error( 'usam_cannot_save_external_document', __( "Не удалось сохранить номер документа. Пожалуйста, попробуйте еще раз.", 'usam' ) );
	}

	/**
	 * Удалить прикрепленный загружаемый файл через AJAX.
	 * @since 3.8.9	
	 */
	function controller_delete_file() 
	{	
		$file_id = absint( $_REQUEST['file_id'] );
		$result = wp_delete_post( $file_id, true );
		
		if ( ! $result )
			return new WP_Error( 'usam_cannot_delete_file', __( "Не удалось удалить файл. Пожалуйста, попробуйте еще раз.", 'usam' ) );
	}
	
	/**
	 * Удалить мета продукта через AJAX
	 * @since 3.8.9	
	 */
	function controller_remove_product_meta() 
	{
		$meta_id = (int) $_POST['meta_id'];
		if ( ! delete_meta( $meta_id ) )
			return new WP_Error( 'usam_cannot_delete_product_meta', __( "Не удалось удалить данные товара. Пожалуйста, попробуйте еще раз.", 'usam' ) );

		return array( 'meta_id' => $meta_id );
	}
	
	// Получить форму изменения данных клиента
	function controller_get_form_edit_customer_details()
	{	
		$form_name = sanitize_title($_POST['form']);		
		ob_start();		
		$form = usam_get_class_form_display( 'orders' );
		if ( is_object($form) )
			$form->get_form( $form_name );			
		$return = ob_get_contents();
		ob_end_clean();
			
		return $return;
	}		
	
	function controller_get_selection_products_mail_editor_iframe()
	{
		ob_start();	
				
		require_once( USAM_FILE_PATH . '/admin/includes/mail/back.class.php' );
		$control_back = new USAM_Mail_Editor_Iframe();	
		wp_iframe( array( $control_back, 'selection_products' ) );
		
		$return = ob_get_contents();
		ob_end_clean();
			
		return $return;
	}
	
	// Сохранить выбранный разделитель блоков в письме
	function controller_set_divider_mail_editor()
	{
		if ( !empty($_POST['divider']) && !empty($_POST['mail_id']) )
		{		
			$divider  = sanitize_text_field($_POST['divider']);		
			$mail_id = absint($_POST['mail_id']);	
			$mailing = usam_get_newsletter( $mail_id );
	
			$update['data']['divider'] = $divider;		
			usam_update_newsletter( $mail_id, $update );	
		}
	}	
	
	function controller_insert_products_blok_mail_editor_iframe()
	{			
		$mail_id = absint($_POST['mail_id']);
		$products = array();
		foreach( (array)$_POST['products'] as $key => $value) 
		{
			$products[] = (int)$value['value'];
		}	
		$query_vars = array(				
			'post__in' => $products,
			'post_type' => "usam-product",
			'post_status' => 'publish',			
		);			
		$wp_query = new WP_Query( $query_vars );
		
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );
		$edit_mail = new USAM_Edit_Newsletter( $mail_id );
		
		$product_mailtemplate = array();
		if (!empty($wp_query)) 
		{
			global $post;
			while ($wp_query->have_posts())
			{ 
				$wp_query->the_post();				
				$product_mailtemplate[] = $edit_mail->get_product_template( );	
			}
		}			
		return $product_mailtemplate[0];
	}
	
	function controller_insert_post_blok_mail_editor_iframe()
	{			
		$mail_id = absint($_POST['mail_id']);		
		$post__in = array();
		foreach( (array)$_POST['post'] as $key => $value) 
		{
			$post__in[] = (int)$value['value'];
		}			
		$query_vars = array(				
			'post__in' => $post__in,
			'post_status' => $_POST['post_status'],	
			'post_type' => $_POST['post_type'],				
		);			
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );		
		$edit_mail = new USAM_Edit_Newsletter( $mail_id );		
			
		$query = new WP_Query( $query_vars );
		$product_mailtemplate = array();
		if (!empty($query)) 
		{
			global $post;
			while ($query->have_posts())
			{ 
				$query->the_post();				
				$product_mailtemplate[] = $edit_mail->get_post_block( $post );
			}
		}			
		return $product_mailtemplate;
	}
	
	// Сохранить шаблон рассылки
	function controller_save_mailtemplate()
	{
		$data    = stripslashes_deep($_POST['data']);		
		$styles  = stripslashes_deep($_POST['styles']);		
		$mail_id = absint($_POST['mail_id']);	
	
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );		
		
		$update = usam_get_newsletter( $mail_id );		
			
		$mail = new USAM_Edit_Newsletter( $mail_id );
		$update['body'] = $mail->get_mail( ); 			
		$update['data']['body'] = $data;
		$update['data']['styles'] = $styles;
		usam_update_newsletter( $mail_id, $update );		
	}
	
	
	// Получить форму iframe для рассылки
	function controller_get_form_mail_editor_iframe()
	{
		ob_start();	
		
		$action = sanitize_title($_POST['type']);			
		$mail_id  = absint($_POST['mail_id']);		
		
		require_once( USAM_FILE_PATH . '/admin/includes/mail/back.class.php' );		
			
		$control_back = new USAM_Mail_Editor_Iframe( $mail_id );		
		wp_iframe( array($control_back, 'handler'), $action );	
		
		$html = ob_get_contents();
		ob_end_clean();
		
		$title = $control_back->get_title( $action );
			
		return array( 'title' => $title, 'html' => $html );
	}	
	
	function controller_install_mailtemplate()
	{
		if( !isset($_POST['theme']) )
			return false;
		$theme = sanitize_title($_REQUEST['theme']);
		update_option('usam_mailtemplate', $theme );
	}
		
	// Получить html блока для вставки
	function controller_insert_blok_mail_editor_iframe()
	{
		$type = sanitize_title($_POST['type']);	
		$mail_id = absint($_POST['mail_id']);		
		
		switch ( $type ) 
		{		
			case 'image' :	
				$imgurl = !empty($_POST['imgurl'])?sanitize_text_field($_POST['imgurl']):'';
				$height = !empty($_POST['height'])?absint($_POST['height']):'';
				$width  = !empty($_POST['width']) && $_POST['width'] < 640 ?absint($_POST['width']):640;				
				$image = array( 'src' => $imgurl, 'width' => $width, 'height' => $height, 'alignment' => '', 'static' => '' );				
				$block = array( 'image' => $image, 'type' => 'content' );	
			break;		
			case 'column':
			case 'column_product':	
				$line = !empty($_POST['line'])?absint($_POST['line']):1;		
				$column = !empty($_POST['column'])?absint($_POST['column']):2;									
				$block = array( 'type' => $type, 'line' => $line, 'column' => $column, 'content' => array() );
			break;	
			case 'content':	
				$image = array( 'src' => '', 'width' => '', 'height' => '', 'alignment' => '', 'static' => '', 'url' => '', 'alt' => '' );
				$block = array( 'text' => array( 'value' => '<p>'.__('Нажмите сюда, чтобы добавить заголовок или текст.','usam').'</p>' ), 'image' => $image, 'alignment' => 'left', 'static' => '', 'type' => 'content' );
			break;			
			default:		
				$block = array( 'type' => $type );	
			break;
		}
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );		
	
		$edit_mail = new USAM_Edit_Newsletter( $mail_id );			
		$return = $edit_mail->get_block_edit( $block );

		return $return;
	}
	
	// Получить форму iframe для рассылки
	function controller_get_post_type()
	{
		$vars = array();		
		$vars['s'] = sanitize_text_field($_POST['search']);	
		$vars['post_type'] = sanitize_text_field($_POST['post_type']);	
		$vars['post_status'] = sanitize_text_field($_POST['post_status']);		
	
		require_once( USAM_FILE_PATH . '/admin/includes/mail/back.class.php' );		
		$control_back = new USAM_Mail_Editor_Iframe();			
		$return = $control_back->get_post_type( $vars );
			
		return $return;
	}	
	
	
	/**
	 * Изменить статус заказа в Журнале продаж
	 */
	function controller_change_purchase_log_status()
	{
		$order_id = absint($_POST['order_id']);
		$new_status = sanitize_title($_POST['new_status']);
		$result = usam_update_order( $order_id, array('status' => $new_status) );
		
		if ( ! $result )
			return new WP_Error( 'usam_cannot_edit_order_status', __( "Не удалось изменить статус покупки заказа. Пожалуйста, попробуйте еще раз.", 'usam' ) );

		$page = new USAM_Page_Tabs( 'orders', 'orders' );
		$current_tab = $page->get_current_tab();
		$current_tab->list_table();
		$purchaselog_table = $current_tab->get_list_table();			
		$purchaselog_table->prepare_items();
				
		ob_start();
		$purchaselog_table->views();
		$views = ob_get_clean();

		ob_start();
		$purchaselog_table->display_tablenav( 'top' );
		$tablenav_top = ob_get_clean();

		ob_start();
		$purchaselog_table->display_tablenav( 'bottom' );
		$tablenav_bottom = ob_get_clean();

		$return = array(
			'id'              => $order_id,
			'new_status'      => $_POST['new_status'],
			'views'           => $views,
			'tablenav_top'    => $tablenav_top,
			'tablenav_bottom' => $tablenav_bottom,
		);
		return $return;
	}

	/**
	 * Изменить статус заказа в самом заказе	
	 */
	function controller_change_purchase_log_item_status()
	{
		$order_id = absint($_POST['id']);
		$new_status = sanitize_title($_POST['new_status']);
		$result = usam_update_order( $order_id, array('status' => $new_status) );
		if ( ! $result )
			return new WP_Error( 'usam_cannot_edit_purchase_log_status', __( "Не удалось изменить статус покупки заказа. Пожалуйста, попробуйте еще раз.", 'usam' ) );
	}

	/**
	 * Сохранить порядок продукта после drag-and-drop сортировки
	 * @since 3.8.9
	 */
	function controller_save_product_order() 
	{
		$products = array( );
		foreach ( $_POST['post'] as $product ) {
			$products[] = (int) str_replace( 'post-', '', $product );
		}
		$failed = array();
		foreach ( $products as $order => $product_id ) 
		{
			$result = wp_update_post( array( 'ID' => $product_id, 'menu_order' => $order, ) );
			if ( ! $result )
				$failed[] = $product_id;
		}
		if ( ! empty( $failed ) ) 
		{
			$error_data = array( 'failed_ids' => $failed, );
			return new WP_Error( 'usam_cannot_save_product_sort_order', __( "Не удалось сохранить порядок сортировки продукции. Пожалуйста, попробуйте еще раз.", 'usam' ), $error_data );
		}
		return true;
	}

	/**
	 * Сохранить загружаемый файл в продукте
	 */
	function controller_upload_product_file() 
	{
		$product_id = absint( $_POST["product_id"] );		
		$output = '';
		
		if ( empty($_POST["select_product_file"]) )
			return array( 'content' => $output );	
		
		$post_ids = array_map('intval', $_POST["select_product_file"]);		
		$args = array(			
			'post__in' => $post_ids,
		);
		$selected_file = usam_get_product_files( $args );
		
		$args = array(			
			'post_parent' => $product_id,			
		);
		$attached_files = usam_get_product_files( $args );
		
		$attached_files_by_file = array();
		foreach ( $attached_files as $key => $attached_file ) {
			$attached_files_by_file[$attached_file->ID] = & $attached_files[$key];
		}		
		foreach ( $selected_file as $file ) 
		{			
			if ( !isset( $attached_files_by_file[$file->ID] ) ) 
			{
				$attachment = array(
					'post_mime_type' => $file->post_mime_type,
					'post_parent' => $product_id,
					'post_title' => $file->post_title,
					'post_content' => '',
					'post_type' => "usam-product-file",
					'post_status' => 'inherit'
				);
				$id = wp_insert_post( $attachment );			
			}
		}
		$output = usam_select_product_file( $product_id ); 
		return array( 'content' => $output, );
	}
	

	/**
	 * Создать вариации
	 * @since 3.8.9
	 * @return array|WP_Error Response args if successful, WP_Error if otherwise
	 */
	function controller_update_variations() 
	{
		$product_id = absint( $_REQUEST["product_id"] );	
		
		$post_data = array( );
		$post_data['variations'] = isset( $_POST['edit_var_val'] ) ? $_POST["edit_var_val"] : '';	
		usam_edit_product_variations( $product_id, $post_data );

		ob_start();
		usam_admin_product_listing( $product_id );
		$content = ob_get_clean();
		return array( 'content' => $content );
	}
			
	function controller_sending_control()
	{				
		$mail_id     = absint($_POST['mail_id']);		
		
		if ( $_POST['action_status'] )		
			$update['status'] = 5;
		else
			$update['status'] = 4;
		
		usam_update_newsletter( $mail_id, $update );
	}
		
	function controller_send_preview_mail()
	{				
		$mail_id     = absint($_POST['mail_id']);	
		$from_email  = sanitize_email($_POST['from_email']);	
		
		require_once( USAM_FILE_PATH . '/includes/mailings/send_newsletter.class.php' );
		$newsletter = new USAM_Send_Newsletter();
		return $newsletter->send_mail_preview( $mail_id, $from_email );		
	}
	
	//Обновление статусов на странице обратной связи в админ панели
	function controller_change_status_feedback()
	{		
		global $user_ID;
		
		$status = absint($_POST['status']);
		$id     = absint($_POST['id']);	
		
		$data   = array( 'status' => $status, 'manager_id' => $user_ID, );		
		$tab    = sanitize_title($_POST['tab']);
		if ( $tab == 'price' )			
			$result_update = usam_update_price_comparison( $id, $data );			
		else
			$result_update = usam_update_feedback( $id, $data );
		
		if ( $result_update >= 1 )
		{
			$out['error'] = __('Ошибка обновления записи','usam');	
			return $out;
		}
	}

	function controller_change_importance_email()
	{			
		$id = absint( $_REQUEST['id'] );	
		$importance = absint( $_REQUEST['importance'] );
		
		usam_update_email( $id, array('importance' => $importance) );		
	}
	
	function controller_change_email_folder()
	{			
		if ( !empty($_REQUEST['id']) && !empty($_REQUEST['folder'] ) )
		{		
			$folder = sanitize_title( $_REQUEST['folder'] );
			if ( is_array($_REQUEST['id']) )
			{			
				foreach( $_REQUEST['id'] as $cb )
				{
					$id = absint( $cb['value'] );
					usam_update_email( $id, array('folder' => $folder) );	
				}
			}
			else
			{
				$id = absint( $_REQUEST['id'] );					
				usam_update_email( $id, array('folder' => $folder) );	
			}
		}
		return $id;
	}
	
	function controller_delete_email()
	{
		if ( !empty($_REQUEST['id']) )
		{
			$id = absint( $_REQUEST['id'] );	
			$email = usam_get_email( $id );		
			if ( $email['folder'] == 'deleted' )
				usam_delete_email( $id );
			else
				usam_update_email( $id, array('folder' => 'deleted' ) );
		}
	}	
	
	function controller_read_email_folder()
	{			
		if ( !empty($_REQUEST['mailbox_id']) && !empty($_REQUEST['folder_id']))
		{					
			global $wpdb;
			$mailbox_id = absint( $_REQUEST['mailbox_id'] );	
			$folder_id = absint( $_REQUEST['folder_id'] );
			
			$folder = usam_get_email_folder( $folder_id );
			
			$result = $wpdb->update( USAM_TABLE_EMAIL, array('read' => 1), array('mailbox_id' => $mailbox_id, 'folder' => $folder['slug']), array('%d'), array('%d','%s') );
			usam_update_email_folder( $folder_id, array( 'not_read' => 0 ) );
		}		
	}

	function controller_clear_email_folder()
	{		
		if ( !empty($_REQUEST['mailbox_id']) && !empty($_REQUEST['folder_id']))
		{					
			global $wpdb;
			$mailbox_id = absint( $_REQUEST['mailbox_id'] );	
			$folder_id = absint( $_REQUEST['folder_id'] );
			
			$folder = usam_get_email_folder( $folder_id );
			$result = $wpdb->delete( USAM_TABLE_EMAIL, array('mailbox_id' => $mailbox_id, 'folder' => $folder['slug']), array('%d','%s') );
			usam_update_email_folder( $folder_id, array( 'count' => 0, 'not_read' => 0 ) );
		}	
	}

	function controller_remove_email_folder()
	{		
		if ( !empty($_REQUEST['folder_id']))
		{	
			$folder_id = absint( $_REQUEST['folder_id'] );			
			usam_delete_email_folder( $folder_id );
		}
	}	
	
	function controller_add_email_folder()
	{		
		$id = array( 'slug' => '', 'id' => 0 );
		if ( !empty($_REQUEST['mailbox_id']) && !empty($_REQUEST['name']) )
		{		
			$name = sanitize_text_field( $_REQUEST['name'] );	
			$mailbox_id = absint( $_REQUEST['mailbox_id'] );	
			
			$id = usam_insert_email_folder( array('name' => $name, 'mailbox_id' => $mailbox_id) );	
			$slug = sanitize_title($name);			
		}
		return array( 'slug' => $slug, 'id' => $id );
	}
		
	function controller_email_fileupload()
	{ 		
		$results = array( 'status' => 'error', 'error_message' => __('Неизвестная ошибка','usam') );
		if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0)
		{
			global $user_ID;
			$directory = USAM_FILE_DIR.$user_ID;
			$results = usam_fileupload( $_FILES['upl'], $directory, 20000000 );
		}		
		echo json_encode( $results );
		exit;	
	}
	
	function controller_delete_email_attachment()
	{
		$file_name = stripcslashes( $_REQUEST['file_name'] );		
		if ( file_exists($file_name) )
			unlink($file_name);		
	}	
		
	function controller_display_message()
	{		
		$id = absint( $_REQUEST['id'] );			
		$email = usam_get_email( $id );		
		
		$body = $email['body'];											
		$return['content'] =  preg_replace_callback("/\n>+/u", 'usam_email_replace_body', $body );	
			
		$page = new USAM_Page_Tabs( 'feedback', 'email' );
		$tab = $page->get_current_tab(  );			
		
		ob_start();
		usam_work_on_task( array('title' => __('Просмотр письма','usam')), array('object_type' => 'email', 'object_id' => $id) );
		
		$tab->get_email_html( $id );		
		$return['header'] = ob_get_clean();
		
		return $return;  
	}	

	function controller_display_sms()
	{		
		$id = absint( $_REQUEST['id'] );	
		
		usam_work_on_task( array( 'title' => __('Просмотр СМС сообщения','usam'), 'status' => 3 ), array( 'object_type' => 'sms', 'object_id' => $id ) );	
		
		$sms = usam_get_sms( $id );		
		return $sms;  
	}	

	function controller_seo_title_product_save()
	{			
		$i = 0;		
		if ( isset($_POST['products']) )
		{	
			$products = $_POST['products'];		
			foreach( $products as $product_id => $product )
			{
				$product_data = array();
				$product_data['ID'] = (int)esc_sql(stripslashes($product_id));	
				if ( isset($product['product_title']) )
					$product_data['post_title'] = sanitize_text_field($product['product_title']);	
				if ( isset($product['product_content']) )
					$product_data['post_content'] = sanitize_textarea_field($product['product_content']);	
				if ( isset($product['product_excerpt']) )
					$product_data['post_excerpt'] = sanitize_textarea_field($product['product_excerpt']);				
				wp_update_post( $product_data );	
				$i++;
			}
		}
		return $i;
	}	

	function controller_json_search_products() 
	{
		global $wpdb;
		$products = $wpdb->get_results( "SELECT post_title FROM $wpdb->posts WHERE post_parent = '0' AND post_type = 'usam-product'" );	
		return $products;
	}	
	
	function controller_loading_information()
	{	
		$product_id  = absint($_POST['product_id']);
		$url = sanitize_text_field($_POST['url']);	
		$webspy = new USAM_WebSpy();
		return $webspy->loading_information( $url );
	}	
	
	function controller_save_bonus()
	{	
		$id  = absint($_POST['id']);
		$status = absint($_POST['status']);	
		usam_update_bonus( array( 'status' => $status, 'id' => $id ) );
	}	
	
	function controller_send_client_mail_by_order()
	{
		$order_id = absint( $_POST['order_id'] );	
		
		$email  = trim($_POST['email']);
		$name	= !empty($_POST['name'])?$_POST['name']:'';
		$subject  = trim(sanitize_text_field(stripslashes($_POST['subject'])));	
		$message  = sanitize_textarea_field(stripslashes($_POST['message']));
		$message = nl2br( $message );
		
		$result = usam_send_mail_by_id( $email, $name, $message, $subject);		
		if ( $result )
		{
			global $user_ID;
			
			$purchase_log = new USAM_Order( $order_id );
			$purchase_log->set( 'contact_status', 2 );		
			$purchase_log->save( );	
			
			$purchase_log->set_order_feedback_history( $user_ID, $message  );
			
			return __('Сообщение успешно отправлено','usam');
		}
		else
			return __('Сообщение не отправлено. Попробуйте еще раз','usam');
	}	
	
	// Получить форму iframe фильтров товаров
	function controller_get_form_product_filter_iframe()
	{
		ob_start();	
		
	//	require_once( USAM_FILE_PATH . '/admin/includes/mail/back.class.php' );
			
		$filter_manage = new USAM_Product_Filter_Manage();	
	//	$filter_manage->filter_print( 'meta' );	
		
		wp_iframe( array($filter_manage, 'filter_print' ), 'meta' );
		
		$return = ob_get_contents();
		ob_end_clean();			
		return $return;
	}

	function controller_view_form_tab()
	{			
		$id = sanitize_title($_POST['id']);
		$view_form_tab = sanitize_title($_POST['view_form_tab']);
		$tab = sanitize_title($_POST['tab']);		
		$page = sanitize_title($_POST['page']);		
		$table = isset($_POST['table'])?sanitize_title($_POST['table']):$tab;
			
		ob_start();		
		require_once( USAM_FILE_PATH .'/admin/includes/view_form.class.php' );	
		$form_file = USAM_FILE_PATH .'/admin/menu-page/'.$page.'/form/view-form-'.$table.'.php';	
		if ( file_exists($form_file) )
		{ 
			require_once( $form_file );	
			$class_display_item = "USAM_View_Form_{$table}";	
			if ( class_exists( $class_display_item ))
			{	
				$item_table = new $class_display_item();
				$method = 'display_tab_'.$view_form_tab;				
				if ( method_exists($item_table, $method) )
				{						
					$item_table->$method();				
				}
			}
		}		
		$return = ob_get_contents();
		ob_end_clean();				
		return $return;
	}
	
	function controller_event_file_upload()
	{ 	
		$results = array( 'status' => 'error', 'error_message' => __('Неизвестная ошибка','usam') );
		if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0)
		{	
			$id = absint($_POST['id']);	
			$dir_path = USAM_UPLOAD_DIR.'events/'.$id;
			$results = usam_fileupload( $_FILES['upl'], $dir_path, 20000000 );
			if( $results['status'] == 'success' )
			{					
				$attachments = usam_get_event_metadata( $id, 'attachments' );
				if ( empty($attachments) )
					$attachments = array();
				$attachments[] = array( 'title' => $results['file_title'], 'name' => $results['file_name'] );
				usam_update_event_metadata($id, 'attachments', $attachments );		
			}
			else
				$results['error_message'] = __('Не удалось скопировать','usam');			
		}		
		echo json_encode( $results );
		exit;	
	}
	
	function controller_delete_event_file()
	{
		$id = absint($_POST['id']);	
		$file_id = absint($_POST['file_id']);	
		$attachments = usam_get_event_metadata( $id, 'attachments' );
		if ( empty($attachments[$file_id]) )
			return false;
		
		$file_path = USAM_UPLOAD_DIR.'events/'.$id.'/'.$attachments[$file_id]['name'];
		unlink($file_path);		
		unset($attachments[$file_id]);
		usam_update_event_metadata($id, 'attachments', $attachments );		
	}
	
	// Добавить задание
	function controller_add_event()
	{
		if ( empty($_POST['name']) || empty($_POST['calendar']))
			return false;
		
		$task['title'] = sanitize_text_field($_POST['name']);
		$task['description'] = sanitize_textarea_field($_POST['description']);
		$task['type'] = sanitize_title($_POST['type']);		
		$task['status'] = !empty($_POST['status'])?absint($_POST['status']):1;	
		$task['calendar'] = !empty($_POST['calendar'])?absint($_POST['calendar']):1;	
		$task['reminder'] = !empty($_POST['date_time'])&&!empty($_POST['reminder'])?1:0;			
					
		if ( !empty($_POST['importance']) )
			$task['importance'] = (bool)$_POST['importance'];
		
		if ( $task['reminder'] && !empty($_POST['reminder_date']) )
			$task['date_time'] = get_gmt_from_date(date( "Y-m-d H:i:s", strtotime($_POST['reminder_date']) ));	
		
		if ( !empty($_POST['date_from']) )
			$task['start'] = get_gmt_from_date(date( "Y-m-d H:i:s", strtotime($_POST['date_from'])) );	
		
		if ( !empty($_POST['date_to']) )
			$task['end'] = get_gmt_from_date( date( "Y-m-d H:i:s", strtotime($_POST['date_to'])) );	
		
		$event_id = usam_insert_event( $task );	
		
		$objects = !empty($_POST['objects'])?stripslashes_deep($_POST['object_id']):array();		
		if ( !empty($_POST['object_type']) && !empty($_POST['object_id']))
		{
			$object_type = !empty($_POST['object_type'])?sanitize_title($_POST['object_type']):'';
			$object_id = !empty($_POST['object_id'])?absint($_POST['object_id']):0;
			
			$objects[] = array( 'object_type' => $object_type, 'object_id' => $object_id );
		}
		if ( !empty($objects) )
		{
			foreach ( $objects as $object ) 
			{
				$object['event_id'] = $event_id;
				usam_set_event_object( $object ); 
			}
		}		
		return $event_id;
	}
	
	function controller_form_customer_case()
	{
		$id = absint($_POST['id']);	
		$customer_type = sanitize_text_field($_POST['customer_type']);		
		return usam_get_form_customer_case( $id, $customer_type );
	}	
		
	function controller_view_calendar_event()
	{
		$event_id = absint($_POST['event_id']);	
		$data = usam_get_event( $event_id );
		
		$data['date_time'] = usam_local_date($data['date_time'], 'd F');
		$data['date_from_display'] = usam_local_date( $data['start'] );
		$data['date_to_display'] = usam_local_date( $data['end'] );		
		$data['calendar_name'] = usam_get_calendar_name_by_id( $data['calendar'] );	
		$data['start'] = get_date_from_gmt($data['start']);
		$data['end'] = get_date_from_gmt($data['end']);		
		return $data;
	}
	
	function controller_edit_event()
	{		
		$event_id = absint($_POST['event_id']);
		
		if ( isset($_POST['name']) )
			$task['title'] = sanitize_text_field($_POST['name']);
		
		if ( isset($_POST['description']) )
			$task['description'] = sanitize_textarea_field($_POST['description']);
		
		if ( isset($_POST['type']) )		
			$task['type'] = sanitize_title($_POST['type']);	
		
		if ( isset($_POST['status']) )
			$task['status'] = !empty($_POST['status'])?absint($_POST['status']):1;	
		
		if ( isset($_POST['calendar']) )
			$task['calendar'] = !empty($_POST['calendar'])?absint($_POST['calendar']):1;	
		
		if ( isset($_POST['reminder']) )
			$task['reminder'] = !empty($_POST['date_time'])&&!empty($_POST['reminder'])?1:0;			
					
		if ( !empty($_POST['importance']) )
			$task['importance'] = 1;
		
		if ( !empty($task['reminder']) && !empty($_POST['reminder_date']) )
			$task['date_time'] = get_gmt_from_date(date( "Y-m-d H:i:s", strtotime($_POST['reminder_date']) ));	
		
		if ( !empty($_POST['date_from']) )
			$task['start'] = get_gmt_from_date(date( "Y-m-d H:i:s", strtotime($_POST['date_from'])) );	
		
		if ( !empty($_POST['date_to']) )
			$task['end'] = get_gmt_from_date( date( "Y-m-d H:i:s", strtotime($_POST['date_to'])) );	
		
		if ( !empty($task) )
			usam_update_event($event_id, $task);
		
		$objects = !empty($_POST['objects'])?stripslashes_deep($_POST['object_id']):array();		
		if ( !empty($_POST['object_type']) && !empty($_POST['object_id']))
		{
			$object_type = !empty($_POST['object_type'])?sanitize_title($_POST['object_type']):'';
			$object_id = !empty($_POST['object_id'])?absint($_POST['object_id']):0;
			
			$objects[] = array( 'object_type' => $object_type, 'object_id' => $object_id );
		}
		if ( !empty($objects) )
		{
			foreach ( $objects as $object ) 
			{
				$object['event_id'] = $event_id;
				usam_set_event_object( $object ); 
			}
		}
		return $task;
	}	
	
	function controller_save_nav_menu_metaboxes() 
	{
		global $user_ID; 
		
		$hidden_meta_boxes = get_user_option( 'usam_metaboxhidden_nav_menus' );
		if ( empty($hidden_meta_boxes) )
			$hidden_meta_boxes = array();	
		
		$metabox = sanitize_title($_POST['id']);	 		
		$hidden = absint($_POST['hidden']);	 
		
		if ( $hidden == 0 && isset($hidden_meta_boxes[$metabox]) )
			unset($hidden_meta_boxes[$metabox]);
		else
			$hidden_meta_boxes[$metabox] = $hidden;
		
		update_user_option( $user_ID, 'usam_metaboxhidden_nav_menus', $hidden_meta_boxes, true );
	}
	
	function controller_save_tab_calendar()
	{
		global $user_ID; 
		
		$tab = sanitize_title($_POST['tab']);	
		update_user_meta($user_ID, 'usam_tab_calendar', $tab);
	}	
		
	function controller_get_data_calendar()
	{					
		global $user_ID; 
				
		$month = isset($_POST['month']) && (int)$_POST['month']<=12?(int)$_POST['month']:date('n');
		$day = isset($_POST['day']) && (int)$_POST['day']<=31?(int)$_POST['day']:date('d');
		$year = isset($_POST['year'])?(int)$_POST['year']:date('Y');	
		$tab = isset($_POST['tab'])?$_POST['tab']:'';	

		if ( $month == 1 )
		{
			$previous_month = 12;
			$previous_year = $year -1;
		}
		else
		{
			$previous_month = $month-1;
			$previous_year = $year;
		}			
		$day_cells = array();
		switch ( $tab ) 
		{		
			case 'tab-day' :	
				$day_cells = date("Y-m-d",mktime( 0,0,0, $month, $day, $year ));
				$day_from = $day;
				$day_to = $day;
				$previous_year = $year;
				$previous_month = $month;
			break;
			case 'tab-week' :			
				$w = date('w', mktime( 0,0,0, $month, $day, $year));
				if ( $w == 0 )
					$day_from = $day - 6;	
				else
					$day_from = $day - $w+1;	

				$day_to = $day_from+7;
				
			/*	if ( $day_from < 0 )		
				{
					$day_week_from = $this->days_in_previous_month + $day_from;				
				}
				else
				{			
					$day_week_from = $day_from;
					$month--;
				}			
				*/
				for ($i=$day_from;$i<$day_to;$i++)
				{
					$day_cells[] = date("Y-m-d",mktime( 0,0,0, $month, $i, $year ));
				}			
			break;
			case 'tab-month' :				
				$days_in_month = cal_days_in_month( CAL_GREGORIAN ,$month, $year );
				$days_in_previous_month = cal_days_in_month( CAL_GREGORIAN , $previous_month, $previous_year );		
				
				$number_cells = 42;
				$number_day_week = date('N',mktime( 0,0,0, $month, 1, $year ));			
				$day_from = $days_in_previous_month - ($number_day_week - 2);	
				$end_day = $number_cells - $days_in_month - ($number_day_week - 2) - 1;	
				
				$day_to = $days_in_month;
				
				$day_cells = array();
				if ( $number_day_week == 1)
				{
					$month_cell = $month;
					$year_cell = $year;
				}
				else
				{
					$month_cell = $previous_month;
					$year_cell = $previous_year;
				}
				for ($i=$day_from;$i<$day_from+$number_cells;$i++)
				{
					$day_cells[] = date("Y-m-d",mktime( 0,0,0, $month_cell, $i, $year_cell ));
				}	
			break;
		}
	
		$calendar = usam_get_user_select_calendars();
	
		$args = array( 'date_query' => array(
						array(
							'column'    => 'end', 
							'after'     => array(
								'year'  => $previous_year,
								'month' => $previous_month,
								'day'   => $day_from,
							),	
							'inclusive' => true,
						),	
						array(						
							'column'    => 'start', 
							'before'     => array(
								'year'  => $year,
								'month' => $month,
								'day'   => $day_to,
							),
							'inclusive' => true,
						),						
					),
		'user_id' => $user_ID,
	//	'calendar__in' => !empty($calendar)?$calendar:0,
		'fields' => array( 'id', 'title', 'description', 'date_time', 'start', 'end','reminder', 'importance', 'color' ),
		'orderby' => 'length_event',
		'order'   => 'DESC',
		);			
		$events = usam_get_tasks( $args );
		
		foreach( $events as &$event )
		{
			$event->start = get_date_from_gmt($event->start);
			$event->end = get_date_from_gmt($event->end);
		}	
		return array( 'day_cells' => $day_cells, 'events' => $events, 'args' => $args );
	}
	
	function controller_delete_event()
	{			
		$event_id = absint($_POST['event_id']);		
		usam_delete_event( $event_id );	
	}	
	
	function controller_select_calendar()
	{		
		global $user_ID;
		
		$calendars = usam_get_user_select_calendars();
		
		$calendar = absint($_POST['calendar']);	
		if ( empty($_POST['select']) )
		{
			foreach( $calendars as $key => $id )
			{
				if ( $id == $calendar )
				{
					unset($calendars[$key]);
					break;
				}			
			}
		}
		elseif ( !in_array($calendar, $calendars))
			$calendars[] = $calendar;
		
		update_user_meta($user_ID, 'usam_calendars', $calendars ); 		
		
		ob_start();
		
		$page = new USAM_Page_Tabs( 'crm', 'calendar' );
		$page->display_current_tab();			
		$return['content'] = ob_get_clean();
		
		return $return;	
	}

/*
function controller_tunable_module_shipping_settings() 
	{
		require_once( USAM_FILE_PATH . '/admin/settings-page.php' );	
		require_once( USAM_FILE_PATH . '/admin/settings-page/tabs/shipping.php' );	

		$return = array();
		ob_start();
		$tab = new USAM_Settings_Tab_Shipping();
		$tab->display_tunable_module_shipping_settings_form();
		$return['content'] = ob_get_clean();

		return $return;
	}

	//Загрузка вкладки страницы
	function controller_navigate_tab()
	{				
		$return = array( 'error' => '' );			
		if (class_exists($page_class)) 
		{
			ob_start();	
			$page = new USAM_Page_Tabs( $_POST['tab'] );
			$page->display_current_tab();			
			$return['content'] = ob_get_clean();
		}
		else
			$return['error'] = sprintf( __('Класс %s не объявлен','usam'), $page_class );
		return $return;
	}		
	*/	
	
	function controller_select_remind()
	{				
		$task_id = absint( $_POST['task_id'] );		
		
		$data = array();
		if ( isset($_POST['reminder']) )
			$data['reminder'] = $_POST['reminder']==1?1:0;		
		
		if ( !empty($_POST['remind']) )
		{
			$remind = absint( $_POST['remind'] );	
			$data['date_time'] = date("Y-m-d H:i:s",current_time('timestamp')+$remind*60);
		}
		usam_update_event($task_id, $data);
	}	

	function controller_save_blank()
	{	
		$input = stripslashes_deep($_POST['input']);		
		$textarea = stripslashes_deep($_POST['textarea']);
		$blank = sanitize_title($_POST['blank']);		
		$printing_form_options = get_option( 'usam_printing_form', array() );	
		$printing_form_options[$blank] = array();	
		foreach( $input as $args )
		{
			$key = sanitize_title($args['name']);
			$printing_form_options[$blank]['data'][$key] = sanitize_text_field($args['value']);	
		}
		foreach( $textarea as $args )
		{
			$key = sanitize_title($args['name']);
			$printing_form_options[$blank]['data'][$key] = sanitize_textarea_field($args['value']);	
		}		
		if ( isset($_POST['table']) )
		{ 
			$table = array();
			foreach( $_POST['table'] as $args )
			{							
				if ( $args['show'] == 'true' )			
					$show = true;
				else
					$show = false;			
					
				$key = sanitize_title($args['name']);
				$table[$key] = array( 'c' => $show, 'title' => sanitize_text_field($args['value']) );					
			}				
			$printing_form_options[$blank]['table'] = $table;
		}	
		update_option( 'usam_printing_form', $printing_form_options );			
	}
	
	function controller_add_mailbox_user() 
	{			
		if ( !empty($_POST['id']) && !empty($_POST['user_id'])  )
		{
			$id = absint($_POST['id']);	
			$user_id = absint($_POST['user_id']);			
			
			global $wpdb;	
			$sql = "INSERT INTO `".USAM_TABLE_MAILBOX_USERS."` (`id`,`user_id`) VALUES ('%d','%d') ON DUPLICATE KEY UPDATE `user_id`='%d'";	
			return $wpdb->query( $wpdb->prepare($sql, $id, $user_id, $user_id ));	
		}
		else
			return false;
	}	
	
	function controller_delete_delete_mailbox_user() 
	{			
		if ( !empty($_POST['id']) && !empty($_POST['user_id'])  )
		{
			$id = absint($_POST['id']);	
			$user_id = absint($_POST['user_id']);			
			
			global $wpdb;	
			return $wpdb->delete( USAM_TABLE_MAILBOX_USERS, array( 'id' => $id, 'user_id' => $user_id ), array( '%d', '%d' ) );	
		}
		else
			return  false;
	}		
	
	function controller_test_mailbox()
	{
		$mailbox_id = absint( $_POST['id'] );
		$mailboxes = new USAM_POP3( $mailbox_id );
		$errors = $mailboxes->get_message_errors();
		if ( !empty($errors) )
		{
			$html = '';
			foreach( $errors as $error )
				$html .= $error."</br>";
		}
		else
			$html = __('Соединение установлено','usam');
		return $html;
	}
}