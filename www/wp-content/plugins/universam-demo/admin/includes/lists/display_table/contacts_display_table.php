<?php
if ( $list_table->search )
	printf( '<h3 class="search_title">' . __( 'Результаты поиска &#8220;%s&#8221;' ) . '</h3>', esc_html( $list_table->search ) );
	
$list_table->prepare_items();	
$contact = !empty($_GET['contact']) && $_GET['contact'] == 'company' ? 'company' : 'all';
$company = !empty($_GET['company']) ? $_GET['company'] : '';
?>
<script type='text/javascript'>		
window.onload = function() {
    parent.iframeLoaded();
}		
</script>
<div class='usam_tab_table'>			
	<form method='GET' action='<?php echo admin_url('admin.php'); ?>' id='usam-tab_form'>
		<input type='hidden' value='display_contacts_list' name='usam_admin_action' />	
		<div class="usam_select_address_book">			
			<span class="usam_field-label"><label for="contact"><?php _e('Контакты', 'usam'); ?>:</label></span>
			<span class="usam_field-val usam_field-title-inner">
				<select name="contact" id="contact" onChange="this.form.submit()">							
					<option value='company' <?php selected('company', $contact); ?>><?php _e('Сотрудники компании', 'usam'); ?></option>		
					<option value='all' <?php selected('all', $contact); ?>><?php _e('Все контакты', 'usam'); ?></option>								
				</select>
				<input type='hidden' id='company' value='<?php echo $company; ?>' name='company' />					
			</span>		
		</div>
		<?php wp_nonce_field('display_contacts_list_nonce', 'nonce' );	?>	
		<?php
		if ( $list_table->is_search_box_enabled() )						
			$list_table->search_box( $list_table->search_title, 'search_id' );
		?>					
		<div class='usam_list_table_wrapper'>
			<?php $list_table->display(); ?>
		</div>				
	</form>
</div>