<?php
require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );
class USAM_Contacts_Table extends USAM_List_Table
{	
	protected $order = 'DESC';	
		
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			
		}			
	}	

	protected function get_table_classes() {
		return array( 'widefat', 'striped', $this->_args['plural'] );
	}	
	
	function column_contact( $item ) 
    {
		$customer_name = $item->lastname;
		if ( $item->firstname != '' )
			$customer_name .= ' '.$item->firstname;
		
		echo '<span id="customer_name">'.$customer_name.'</span>';
	}
	
	function column_contact_source( $item ) 
    {		
		echo usam_get_name_contact_source( $item->contact_source );
	}
	
	function column_select( $item ) 
    {		
		echo "<a href=''>".__('добавить', 'usam')."</a>";
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "contact-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
			
	function get_columns()
	{		
        $columns = array(           					
			'contact'        => __( 'Контакт', 'usam' ),					
			'contact_source' => __( 'Источник', 'usam' ),	
			'select'         => __( 'Выбрать', 'usam' ),			
        );		
        return $columns;
    }
		
	function prepare_items() 
	{		
		global $user_ID;
		require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );		
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 		
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'search' => $this->search,		
			'name_is_known' => true,			
		);										
		if ( $this->search == '' )
		{		
			if ( !empty($this->records) )
				$query['include'] = $this->records;			
		}
		if ( !empty($_GET['contact']) && !empty($_GET['company']) && $_GET['contact'] == 'company' )
		{
			$query['company_id'] = absint($_GET['company']);			
		}	
		$_contacts = new USAM_Contacts_Query( $query );
		$this->items = $_contacts->get_results();			
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>