<?php
// Формы для страниц магазина Universam
function usam_add_box( $id, $title, $function, $parameters = null, $edit = false ) 
{	
	if ( $edit )
		$title .= '<a class="edit" href="#" title="'.__('Изменить','usam').'"><img src="'.USAM_CORE_IMAGES_URL.'/edit.png" width="14"></a>';
		
	$metabox = get_user_option( 'usam_metaboxhidden_nav_menus' );	
	$closed = isset($metabox[$id]) && $metabox[$id] ? 'closed' : '';	
	?>
	<a id ="nav-<?php echo $id; ?>"></a>
	<div id="<?php echo $id; ?>" class="postbox usam_box <?php echo $closed; ?>">
		<div class="handlediv" title="Нажмите, чтобы переключить"><br></div>
		<h3 class="hndle ui-sortable-handle"><span><?php echo $title; ?></span></h3>
		<div class="inside"><?php call_user_func( $function, $parameters ); ?></div>
	</div>
	<?php
}	

// Редактирование данных с помощью ajax
function usam_get_fast_data_editing( $text, $id, $col, $action, $data_type = 'input', $callback = null, $collection = '', $custom = '') 
{
	/*$attr = '';
	foreach( $args as $name => $value )
	{
		$attr .= "$name='$value' ";
	}*/
	if ( $custom != '' )
		$custom = "data-attribute='custom_$custom'";
	$out = "
	<span class='fast_editing'>
		<span class='best_in_place usam_edit_$col' data-id='$id' data-nonce='".usam_create_ajax_nonce( $action )."' data-action='$action' data-col='$col' data-callback='$callback' data-type='$data_type' data-collection='$collection' $custom>$text</span>
		<span class='pencil'></span>
	</span>";	
	return $out;
}

//usam_toggle( 'usam_stand_service_box', 'false' );		

// Формы для страниц магазина Universam
function usam_toggle( $id, $on, $title_1 = '', $title_2 = '' ) 
{	
	if ( $title_1 == '' ) 
		$title_1 = __('Вкл','usam');
	if ( $title_2 == '' ) 
		$title_2 = __('Выкл','usam');
	if ( $on == 'off' ) 
		$on_class = 'off';		
	?>
	<div id="<?php echo $id; ?>" class="usam_toggle">
		<div class="toggle <?php echo $on_class; ?>" data-ontext="<?php echo $title_1; ?>" data-offtext="<?php echo $title_2; ?>"></div>
	</div>
	<?php
}	
?>