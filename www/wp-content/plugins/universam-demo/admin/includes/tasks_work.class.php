<?php
class USAM_Tasks_Work
{
	public function __construct(  )
	{	
		add_action('admin_notices',   array($this, 'cron_tasks') );
		add_action('init',   array($this, 'init') );	
	} 
	
	public function cron_tasks()
	{	
		global $user_ID, $wpdb;			
		
		$data = date( "Y-m-d H:i:s", current_time('timestamp', 1) );	
		$tasks = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_EVENTS." WHERE user_id='{$user_ID}' AND date_time <= '{$data}' AND reminder = '1'");	
		if ( empty($tasks) )
			return false;
		$remind = "<span class='remind'>".__('Напомнить через','usam').": <select id='select_remind'>
			<option value='5'>".__( '5 минут', 'usam' )."</option>
			<option value='5'>".__( '5 минут', 'usam' )."</option>
			<option value='30'>".__( '30 минут', 'usam' )."</option>	
			<option value='60'>".__( '1 час', 'usam' )."</option>	
			<option value='120'>".__( '2 часа', 'usam' )."</option>	
			<option value='240'>".__( '4 часа', 'usam' )."</option>		
			<option value='480'>".__( '8 часа', 'usam' )."</option>	
			<option value='1440'>".__( '1 день', 'usam' )."</option>		
			<option value='7200'>".__( '5 деней', 'usam' )."</option>	
			<option value='10080'>".__( '1 неделю', 'usam' )."</option>			
		</select></span>";
			
		echo "<div class ='tasks_reminder'>";				
		foreach ( $tasks as $task )
		{			
			$view_object = '';
			if ( !empty($task->object_type) && !empty($task->object_id) )
			{				
				switch ( $task->object_type ) 
				{
					case 'order' :
						$url = usam_get_url_order( $task->object_id );
						$view_object = __("Посмотрите заказ №%s","usam");
					break;
					case 'suggestions' :
						$url = admin_url("admin.php?page=crm&tab=suggestions&id=".$task->object_id."&action=edit");		
						$view_object = __("Посмотрите коммерческое предложение №%s","usam");
					break;
					case 'invoice' :
						$url = admin_url("admin.php?page=crm&tab=invoice&id=".$task->object_id."&action=edit");		
						$view_object = __("Посмотрите счет №%s","usam");
					break;
					case 'shipped_document' :
						$url = admin_url("admin.php?page=orders&tab=shipped&id=".$task->object_id."&action=edit");		
						$view_object = __("Посмотрите отгрузку №%s","usam");
					break;	
					case 'questions' :
						$url = admin_url("admin.php?page=feedback&tab=questions&id=".$task->object_id."&action=edit");		
						$view_object = __("Посмотрите сообщение клиента №%s","usam");
					break;						
				}
				if ( $view_object != '' )
					$view_object = sprintf("<p><a href='$url' target='_blank'>$view_object</a></p>",$task->object_id);
			}			
			$url = add_query_arg( array( 'task_reminder' => $task->id ));
			echo "<div id ='task_reminder' data-task_id='$task->id' class='".($task->importance?'importance':'')."'>					
					<div class='task_body'>
						<div class='colums-2'>
							<div class='colum1'>
								<h5><a href='".admin_url( "admin.php?page=crm&tab=tasks&id=".$task->id."&action=edit")."'>".$task->title.".</a>";
								if ( !empty($task->start) )
									echo "<span>".esc_html__( 'Начать', 'usam' ).":</span><strong>".usam_local_date( $task->start, get_option( 'date_format', 'Y/m/d' ).' H:i' )."</strong>";
								if ( !empty($task->end) )									
									echo "<span>".esc_html__( 'Завершить', 'usam' ).":</span><strong>".usam_local_date( $task->end, get_option( 'date_format', 'Y/m/d' ).' H:i' )."</strong>";
								echo "</h5><p>$task->description</p>$view_object										
							</div>
							<div class='colum2' >
								<div class='remind_box row'>$remind<a href='' id='review_later' class='button'>".esc_html__( 'Отложить', 'usam' )."</a></div>
								<div class='i_have_seen_box row'><a href='".$url."' id='i_have_seen' class='button'>".esc_html__( 'Я видел', 'usam' )."</a></div>
							</div>
						</div>
					</div>
				</div>"; 
		}
		echo "</div>";		
	}
	
	public function init()
	{	
		if ( !empty($_GET['task_reminder']) )
		{
			$task_id = (int)$_GET['task_reminder'];			
			
			$_event = new USAM_Event( $task_id );	
			$data['reminder'] = 0;
			$_event->set( $data );
			$result = $_event->save();			
		}
	}
}
new USAM_Tasks_Work();
?>