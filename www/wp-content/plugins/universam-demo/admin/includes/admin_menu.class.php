<?php
/**
 * Меню
 * @since 3.7   
 */ 
class USAM_Admin_Menu
{	
	protected static $_instance = null;
	function __construct( )
	{			
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) 
		{
			add_action( 'admin_init', array($this, 'product_list_ajax'), 2 );
		}
		add_action( 'admin_menu', array($this, 'admin_menu_pages') );
		add_filter('set-screen-option', array($this, 'set_screen_option'), 98, 3);	
	}
		
	/**
	 * установка опции экрана в диапазоне от 1 до 999
	 * @since 3.8
	 */
	function set_screen_option($status, $option, $value)
	{	
		if( $value <= 999 )
		{	
			if ( "edit_usam_variation_per_page" == $option )
			{
				global $user_ID;			
				update_user_option($user_ID,'edit_usam-variation_per_page',$value);
			}
			return $value;
		}
	}	
	
	function product_list_ajax()
	{	
		new USAM_Display_Product_Page(); 	
	}
	
	function product_list()
	{
		global $current_screen;	
		if( $current_screen->post_type == 'usam-product' )		
			new USAM_Display_Product_Page(); 	
	}
	
	// Инициализация класса страницы вкладок
	function init_page_tabs( )
	{			
		USAM_Page_Tabs::init();
		self::$_instance = USAM_Page_Tabs::get_instance();			
	}	

	// Отображение страницы вкладок
	function display_page_tabs( )
	{			
		if ( !is_null( self::$_instance ) )
		{
			self::$_instance->display();
		}			
	}	
	
	// Добавляет меню
	function add_menu_page_tabs( )
	{		
		do_action( 'usam_add_submenu-before' );
		$function = array($this,'display_page_tabs');
		$admin_menu = $this->get_admin_menu();
		foreach ( $admin_menu as $menu_slug => $menu )
		{
			if ( isset($menu['toplevel']) )
				$menu_page_hook = add_menu_page( $menu['toplevel']['page_title'], $menu['toplevel']['menu_title'], $menu['toplevel']['capability'], $menu_slug, $function, $menu['toplevel']['icon'], $menu['toplevel']['position']);			
			foreach ( $menu['submenu'] as $submenu )
			{							
				$cap = apply_filters( 'usam_submenu_page_cap', $submenu['capability'] );
				if ( current_user_can( $cap ) )		
				{ 			
					$page_hook = add_submenu_page( $menu_slug, $submenu['page_title'], $submenu['menu_title'], $cap, $submenu['menu_slug'], $function );	
					add_action( 'load-'.$page_hook, array( $this, 'init_page_tabs') );			
				}
			}			
		}		
		do_action( 'usam_add_submenu-after' ); 
	}	
	
	function get_admin_menu() 
	{
		global $usam_events;	
		
		if ( $usam_events['feedback'] > 0 )
			$events = usam_get_style_number_message( $usam_events['feedback'] );
		else 
			$events = '';	
		
		if ( usam_license() )
		{
			$menu['orders']['toplevel'] = array( 'page_title' => __('Магазин','usam'), 'menu_title' => __('Магазин','usam'), 'icon' => "dashicons-cart", 'position' => "27", 'capability' => 'store_section' );		
			$menu['orders']['submenu'] = array( 
				array( 'page_title' => __('Журнал заказов', 'usam'), 'menu_title' => __('Заказы', 'usam'), 'capability' => 'view_orders',  'menu_slug' => 'orders' ),
				array( 'page_title' => __('Обратная связь', 'usam'), 'menu_title' => __('Обратная связь', 'usam').$events, 'capability' => 'view_feedback', 'menu_slug' => 'feedback'),	
				array( 'page_title' => __('Система управления взаимоотношениями с клиентами', 'usam'), 'menu_title' => __('CRM', 'usam'), 'capability' => 'view_crm','menu_slug' => 'crm' ),	
				array( 'page_title' => __('Управление скидками', 'usam'), 'menu_title' => __('Скидки', 'usam'), 'capability' => 'manage_prices',  'menu_slug' => 'manage_discounts'),		
				array( 'page_title' => __('Управление ценами', 'usam'), 'menu_title' => __('Управление ценами', 'usam'), 'capability' => 'manage_prices', 'menu_slug' => 'manage_prices'),	
				array( 'page_title' => __('Складской учет', 'usam'), 'menu_title' => __('Складской учет', 'usam'), 'capability' => 'view_storage', 'menu_slug' => 'storage'),	
				array( 'page_title' => __('Покупатели', 'usam'), 'menu_title' => __('Покупатели', 'usam'), 'capability' => 'view_customers',  'menu_slug' => 'customers'),			
				array( 'page_title' => __('Настройка отображения элементов магазина', 'usam'), 'menu_title' => __('Интерфейс', 'usam'), 'capability' => 'view_interface', 'menu_slug' => 'interface'),
			//	array( 'page_title' => __('Управление персоналом', 'usam'), 'menu_title' => __('Персонал', 'usam'), 'capability' => 'level_7', 'menu_slug' => 'manage_personnel'),					
				array( 'page_title' => __('Обмен данными между сайтом и другими программами', 'usam'), 'menu_title' => __('Обмен', 'usam'), 'capability' => 'view_exchange', 'menu_slug' => 'exchange'),	
				array( 'page_title' => __('Инструменты', 'usam'), 'menu_title' => __('Инструменты', 'usam'), 'capability' => 'shop_tools',  'menu_slug' => 'shop' ),			
				array( 'page_title' => __('Отчеты и аналитика', 'usam'), 'menu_title' => __('Отчеты', 'usam'), 'capability' => 'view_reports', 'menu_slug' => 'reports'),						
			);
			$menu['marketing']['toplevel'] = array( 'page_title' => __('Кабинет маркетолога','usam'), 'menu_title' => __('Маркетинг','usam'), 'icon' => "dashicons-chart-bar", 'position' => "28", 'capability' => 'marketing_section' );				
			$menu['marketing']['submenu'] = array( 						
				array( 'page_title' => __('Маркетинг', 'usam'), 'menu_title' => __('Маркетинг', 'usam'), 'capability' => 'view_marketing',  'menu_slug' => 'marketing'),
				array( 'page_title' => __('Управление социальными сетями', 'usam'), 'menu_title' => __('Социальные сети', 'usam'), 'capability' => 'view_social_networks', 'menu_slug' => 'social_networks'),			
				array( 'page_title' => __('Управление рассылками', 'usam'), 'menu_title' => __('Рассылка', 'usam'), 'capability' => 'view_newsletter',  'menu_slug' => 'newsletter'),
		//		array( 'page_title' => __('Управление рекламой', 'usam'), 'menu_title' => __('Реклама', 'usam'), 'capability' => 'level_7','menu_slug' => 'manage_advertising' ),
			);		
			$menu['seo']['toplevel'] = array( 'page_title' => __('Кабинет SEO-специалиста','usam'), 'menu_title' => __('SEO','usam'), 'icon' => "dashicons-analytics", 'position' => "29", 'capability' => 'view_seo' );				
			$menu['seo']['submenu'] = array( 						
				array( 'page_title' => __('Поисковая оптимизация', 'usam'), 'menu_title' => __('SEO', 'usam'), 'capability' => 'view_seo', 'menu_slug' => 'seo'),						
			);		
		}		
		$menu['options-general.php']['submenu'] = array( 
			array( 'page_title' => __('Настройки магазина', 'usam'), 'menu_title' => __('Магазин', 'usam'), 'capability' => 'level_7',  'menu_slug' => 'shop_settings'),
		);							
		return $menu;
	}
		
	/**
	 * Добавление в меню
	 */
	function admin_menu_pages() 
	{
		$this->add_menu_page_tabs( );

		add_action( 'load-edit.php', array($this, 'product_list') ); // Загрузка списка товаров		
	}
}
$admin_menu = new USAM_Admin_Menu();
?>