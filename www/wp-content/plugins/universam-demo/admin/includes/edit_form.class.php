<?php
// Класс форм таблиц
class USAM_Edit_Form
{	
	protected $formtype = false;
	protected $sendback;
	protected $id = null;
	protected $data = array();
	protected $title_action;
	protected $redirect = false;
	protected $screen = '';
	protected $action = 'save';	
	
	protected $table = '';
	protected $step = '';
	protected $not_exist = false;	
	
	public function __construct( $id = null ) 
	{ 
		if ( $id !== null )
			$this->id = sanitize_title($id);	
		elseif ( isset($_REQUEST['id']) )
			$this->id = sanitize_title($_REQUEST['id']);	

		$this->step = empty( $_REQUEST['step'] ) ? $this->step : sanitize_title( $_REQUEST['step'] );	
		$this->screen = empty( $_REQUEST['action'] ) ? $this->screen : sanitize_title( $_REQUEST['action'] );
		
		$this->title_action = __( 'Доступные действия', 'usam' );	
		$this->get_data_tab();			
		add_action( 'admin_enqueue_scripts', array( $this, 'js' ) );
	}
	
	protected function get_title_tab( ) { }		
		
	
	// Получить данные после сохранения элемента
	protected function get_data_tab() { }
	
	protected function localize_script_tab() { }	
	
	public function js() 
	{			
		wp_enqueue_script( 'postbox' );
		wp_enqueue_style( 'usam-edit-form' );			
	}
	
	public function get_meta_group_save( $group )
	{
		$results = array();
		if ( !empty($_POST['tax_input'][$group]) )
			foreach ( $_POST['tax_input'][$group] as $value )
				$results[] = sanitize_title($value);
		return $results;
	}
	
	public function message_error()
	{       
		if ( isset($_GET['item_error']) )
			$message_error = __('Не заполнены поля','usam');
		else
			$message_error = '';
		return $message_error;
	}
		
	
	// Отображение элемента таблицы	
	public function display_form()
    {		
		$title = $this->get_title_tab( );	
		
		if ( $this->formtype )
			$enctype = "enctype='multipart/form-data'";
		else
			$enctype = '';			
		?>	
		<h2><?php echo $title; ?></h2>	
		<?php
		if ( $this->not_exist )
			return false;
		?>		
		<form method='POST' action='' id='usam-table_item_form' <?php echo $enctype; ?>>			
			<div class="subtab_detail-content">								
				<?php										
				$display = 'display';	
				if ( !empty($this->step) )
				{ 
					$method = "screen_".$this->step;		
					if ( method_exists( $this, $method ) )
					{ 
						?><input type='hidden' value='<?php echo $this->step; ?>' name='step' /><?php 
						$display = $method;					
					}
				}					
				$this->$display();
				
				global $current_screen;	
				wp_nonce_field('usam-'.$current_screen->id,'usam_name_nonce_field');
				?>
				<input type='hidden' value='<?php echo $this->action; ?>' name='action' />
				<input type='hidden' value='<?php echo $this->screen; ?>' name='screen' />		
				
				<?php if ( $this->id != null ) { ?>	
					<input type='hidden' value='<?php echo $this->id; ?>' name='id' />	
				<?php } ?>				
			</div>			
		</form> 
		<?php	
	}	
	
	protected function get_fastnav( ) 
	{
		return array();
	}
	
	protected function toolbar_buttons( ) 
	{
		if ( $this->id != null )
		{
			global $current_screen;		
			$sendback = add_query_arg( array('id' => $this->id, 'action' => 'delete' ));
			$sendback = wp_nonce_url( $sendback, 'usam-'.$current_screen->id);
			?>
			<ul>
				<li><a href="<?php echo $sendback; ?>" class="button delete"><?php _e('Удалить','usam'); ?></a></li>
			</ul>
			<?php		
		}
	}
	
	protected function display_toolbar( ) 
	{		
		$sendback = remove_query_arg( array( 'id', 'action', 'n', 'status' )  );
		?>			
		<div class="toolbar">			
			<div class="window-fastnav">				
				<a href="<?php echo $sendback; ?>"  class="go_back"><span class="back_text">&#171; <?php _e('вернуться назад','usam'); ?></span></a>
				<div class="fastnav">	
					<div class="fastnav-container">				
						<div class="fastnav_table_container">			
							<table class="fastnav_table">					
								<tr>							
									<td class="window-fastnav-title"><?php _e( 'Навигация', 'usam' ); ?>:</td>
									<td>
										<div class="navlist">	
											<ul class="window-fastnav-navlist">
												<?php 
												$nav = $this->get_fastnav();
												foreach ( $nav as $id => $name )
												{
													?>
													<li><a href="#nav-<?php echo $id; ?>" id="fastnav_<?php echo $id; ?>"><?php echo $name; ?></a></li>
													<?php 
												}
												?>
												<li><a href="#usam-page-tabs-title" id="usam-page-tabs-title"><?php _e( 'В начало', 'usam' ); ?>&nbsp;&uarr;</a></li>
											</ul>
										</div>											
									</td>
								</tr>
							</table>						
						</div>	
						<div class="button_action">
							<?php $this->toolbar_buttons();	?>															
						</div>	
					</div>						
				</div>					
			</div>
		</div>
      <?php
	}		
	
	public function display( ) 
	{		
		$this->display_toolbar();	
		?>
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id = 'post-body-content'>				
					<?php $this->display_left(); ?>
				</div>	
				<div id ="postbox-container-1" class = 'postbox-container'>				
					<div class = 'menu_fixed_right'>
						<?php					
						$this->display_actions();				
						$this->display_right(); 
						?>
					</div>	
				</div>	
			</div>	
		</div>
      <?php
	}		
	
	function display_right() {	}		
	function display_left() {	}
	
	public function buttons()
	{    
		if ( $this->id != null )
		{			
			submit_button( __('Сохранить и закрыть', 'usam'), 'primary button_save_close', 'save-close', false, array( 'id' => 'submit-save-close' ) ); 
			submit_button( __('Сохранить', 'usam'), 'secondary button_save', 'save', false, array( 'id' => 'submit-save' ) ); 
		}
		else
		{
			submit_button( __('Добавить', 'usam').' &#10010;', 'primary button_save_close', '', false, array( 'id' => 'submit-add' ) ); 
		}
	}
	
	function display_actions() 
	{			
		?>		
		<div id = 'tab-actions' class = "postbox">						
			<h3 class="hndle"><?php echo $this->title_action; ?></h3>			
			<div id="major-publishing-actions">
				<div class="div-actions">
				<?php $this->buttons(); ?>
				</div>		
			</div>							
		</div>	
		<?php
	}
	
	function save() {	}		
	function edit() {	}	  


	//Вывести боксы
	public function checklist_meta_boxs( $boxs )
	{        	
		?>			
		<div id="all_taxonomy" class="all_taxonomy">
			<?php
			foreach ($boxs as $key => $checklist)
			{					
				$this->display_meta_box_group( $key, $checklist ); 
			}
			?>				
		</div>	
      <?php
	}	
	
	//Вывести бокс
	public function checklist_meta_box( $meta_box, $checked_list )
	{        	
		?>
		<div id="taxonomy-<?php echo $meta_box; ?>" class="categorydiv">
			<div id="<?php echo $meta_box; ?>-all" class="tabs-panel">				
				<ul id="<?php echo $meta_box; ?>checklist" class="categorychecklist form-no-clear">
				<?php   
					$c = 'controller_get_condition_'.$meta_box;
					if ( method_exists( $this, $c ) )
					{
						$data = $this->$c( );
						echo $this->get_checklist( $meta_box, $data, $checked_list );						
					}
					else						
					{								
						switch( $meta_box ) 
						{												
							case 'products': 
								$this->products_checklist( $checked_list );
							break;	
							case 'category': 	
								echo $this->build_checkbox_contents ('usam-category', $checked_list);                             
							break;	
							case 'brands': 						
								echo $this->build_checkbox_contents ('usam-brands', $checked_list);                             
							break;	
							case 'category_sale': 						
								echo $this->build_checkbox_contents ('usam-category_sale', $checked_list);                             
							break;										
							default:  //Категории товаров или категории правил покупки
								echo $this->build_checkbox_contents ($meta_box, $checked_list);                             
							break;
						}
					}
					?>  
              </ul>
          </div>
		</div>
      <?php
	}	
	
	public function display_select_type_discont( $name, $checked )
	{  
		$currency = usam_get_currency_sign();
		?>
		<select class="select_type_discont" name="<?php echo $name; ?>">
			<option value="f" <?php echo (($checked=='f')?'selected="selected"':'');?>><?php echo $currency; ?></option>
			<option value="p" <?php echo (($checked=='p')?'selected="selected"':'')?>>%</option>
		</select>
		<?php
	}
	
	public function display_meta_box_group( $group, $checked_list = array(), $title = '' )
	{        	
		if ( $title == '' )
		{
			switch( $group ) 		
			{		
				case 'roles': 
					$title = __('Роли посетителей','usam');
				break;    
				case 'users': 
					$title = __('Посетители','usam');
				break;	
				case 'products': 
					$title = __('Товары','usam');
				break;	
				case 'weekday': 
					$title = __('Дни недели','usam');
				break;		
				case 'type_prices': 
					$title = __('Цены','usam');
				break;	
				case 'selected_gateway': 
					$title = __('Метод оплаты','usam');
				break;
				case 'selected_shipping': 
					$title = __('Метод доставки','usam');
				break;
				case 'sales_area': 
					$title = __('Зоны продаж','usam');
				break;
				case 'types_payers': 
					$title = __('Типы плательщиков','usam');
				break;			
				case 'category': 
				case 'brands': 		
				case 'category_sale': 				
					$taxonomy = get_taxonomy( 'usam-'.$group );				
					$title = $taxonomy->labels->name; 
				break;				
				default:  				
					$title = '';                      
				break;
			}
		}		
		?>
		<div id="group-<?php echo $group; ?>" class="taxonomy_box">
			<h3><label><input id="checked_all-<?php echo $group; ?>" type="checkbox"/>&nbsp;<?php echo $title; ?></label></h3>
			<?php  $this->checklist_meta_box( $group, $checked_list ); ?>
		</div>
      <?php
	}
	
	protected function get_checklist ( $name, $array, $checked_list = NULL )
	{               
        $output = '';
		foreach ($array as $id => $value) 
		{          
            $output  .= '<li id='.$id.'>' ;
            $output  .= '<label class="selectit">' ;
            $output  .= '<input id="'.$id.'" ';
            $output  .= 'type="checkbox" name="input-'. $name . '[]" ';
            $output  .= 'value="'.$id.'" ';
            if ($checked_list) 
			{
                if (in_array($id, $checked_list)) {  
                   $output  .= 'checked="checked"';
                }               
            }
            $output  .= '>';
            $output  .= '&nbsp;<span class="name">'.$value;
            $output  .= '</span></label>';            
            $output  .= '</li>';             
        }
		return $output;
    } 
	
	// Условие корзины - роли пользователя
	protected function controller_get_condition_roles()
	{ 
        $roles = get_editable_roles();	
        $result['notLoggedIn'] = __('Не вошел в систему','usam');
		foreach ($roles as $role => $info) 
		{
            $result[$role] = translate_user_role( $info['name'] );
        }	
		return $result;
    }  
	
	// Условие зоны продаж
	protected function controller_get_condition_sales_area( )
	{     
		$option = get_option('usam_sales_area');
		$grouping = maybe_unserialize( $option );		
		$results = array();
		if ( !empty($grouping) )
		{
			foreach ( $grouping as $value )
			{
				$results[$value['id']] = $value['name'];
			}
		}
		return $results;
    } 
	
	protected function controller_get_condition_types_payers( )
	{     
		$option = get_option('usam_types_payers');
		$grouping = maybe_unserialize( $option );
	
		$results = array();
		foreach ( $grouping as $value )
		{
			$results[$value['id']] = $value['name'];
		}
		return $results;
    } 

	// Условие корзины - типы цен
	protected function controller_get_condition_type_prices( )
	{     
		$prices = usam_get_prices( array('type' => 'R') );		
		$results = array();
		foreach ( $prices as $value )
		{
			$results[$value['code']] = $value['title'];
		}
		return $results;
    }   	
		
	// Условие корзины - метод оплаты
	public function controller_get_condition_selected_gateway(  )
	{     
		$gateways = usam_get_payment_gateways( array( 'fields' => array( 'name', 'id' ) ) );		
		$results = array();
		foreach ( $gateways as $gateway )
			$results[$gateway->id] = $gateway->name;	
			
		return $results;
    }   
	
	// Условие корзины - метод доставки
	public function controller_get_condition_selected_shipping(  )
	{     
		$delivery_service = usam_get_delivery_services();
		
		$shipping = array();
		foreach ($delivery_service as $value)
			$shipping[$value->id] = $value->name;	
		
		return $shipping;
    }   
	
	// Условие корзины - дни недели 
	protected function controller_get_condition_weekday( )
	{     
		$weekday = array( '1' => __('Понедельник','usam'), '2' => __('Вторник','usam'), '3' => __('Среда','usam'), '4' => __('Четверг','usam'), '5' => __('Пятница','usam'), '6' => __('Суббота','usam'), '0' => __('Воскресение','usam') );		
		return $weekday;
    }   
	
	// Условие корзины - пользователи
	protected function controller_get_condition_users ( )
	{ 
        $args = array( 'orderby' => 'nicename', 'fields' => array( 'ID','user_nicename','display_name') );
		$users = get_users( $args );    
		$result = array();
		foreach ( $users as $user ) 
		{            
           $result[$user->ID] = "$user->display_name ($user->user_nicename)";
        }	
		return $result;
    }   	
	
	protected function build_checkbox_contents ( $taxonomy, $checked_list = NULL ) 
	{      
		$args = array(
					'descendants_and_self' => 0,
					'selected_cats'        => $checked_list,
					'popular_cats'         => false,				
					'taxonomy'             => $taxonomy,
					'checked_ontop'        => false, 
					'echo'                 => false,
				);
		return wp_terms_checklist( 0, $args );
    } 	
	
	public function titlediv ( $value = '' ) 
	{
		?> 
		<div id="titlediv">
			<div id="titlewrap">
				<input type="text" name="name" value="<?php echo htmlspecialchars($value); ?>" data-internal_text = "<?php _e('Ввведите название', 'usam') ?>" class="titlebox show_internal_text"/>
			</div>
		</div>			
		<?php
	}
	
	public function selecting_locations ( $selecting = array() ) 
	{		
		$args = array(				
			'selected' => $selecting,			
		);
		?>  	
		<div class="usam_container">
			<?php usam_locations_checklist( $args ); ?>
		</div>
		<div class="back-to-top">
			<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
		</div>				
	   <?php   
	}
	
	public function selecting_type_prices( $prices ) 
	{		
		?>
		<div class="container_column">	
			<div class="column1 checklist_description" id="amtDescrip">
				<h4><?php _e('Цены', 'usam') ?></h4>
				<p><?php _e('Выберите цену.', 'usam')?>  </p>
			</div>
			<div class="column2">				
				<?php $this->display_meta_box_group( 'type_prices', $prices );  ?>
			</div>   
			<div class="back-to-top">
				<a title="<?php _e('Вернуться в начало', 'usam')?>" href="#wpbody"><?php _e('Ввверх', 'usam')?><span class="back-to-top-arrow">&nbsp;&uarr;</span></a>
			</div>
		</div>
		<?php
	}
	
	public function box_status( $args )
	{   
		if ( isset($args['checked']))
			$checked = $args['checked'];
		else
			$checked = 0;		
		
		if ( !empty($args['title']))
			$title = $args['title'];
		else
			$title = __('Можно сразу активировать или активировать потом.', 'usam');
		
		if ( !empty($args['help']))
			$help = $args['help'];
		else
			$help = array( 	array( 'title' => __('Не активно', 'usam'), 'description' => __('Не активировано и не действует сейчас. Его можно активировать в удобное для Вас время.', 'usam') ),
					array( 'title' => __('Активно', 'usam'), 'description' => __('Активировано и действует уже сейчас.', 'usam') )
		);
		
		?>		
	<div class="container_column">  
		<div class="column1" id="rule-descrip">  
			<p><?php echo $title; ?></p>
		</div>
		<div class="column2" id="rule-choice">
			<h3><?php _e('Статус', 'usam')?></h3>
			<div id="rule-radio">
				<span id="rule-selected">			
					<input type="radio" id = "status_rule_0" class = "show_help" name="active" value="0" <?php checked($checked, 0) ?> /><?php _e('Не активно', 'usam'); ?><br />
					<input type="radio" id = "status_rule_1" class = "show_help" name="active" value="1" <?php checked($checked, 1) ?> /><?php _e('Активно', 'usam'); ?><br />			
				</span>
			</div>                
		</div>
		<div class="column3">
			<?php 		
			$this->box_help_setting( $help[0], 'status_rule_0' );
			$this->box_help_setting( $help[1], 'status_rule_1' );
			?>
		</div>
	</div>
	   <?php
	}
	
	public function add_box_status_active( $checked = 0, $title = '', $help = array() )
	{	
		$args['checked'] = $checked;
		$args['title'] = $title;
		$args['help'] = $help;
		usam_add_box( 'usam_status', __('Активировать','usam'), array( $this, 'box_status' ), $args );	
	}
	
	public function display_rules_work_basket( $conditions ) 
	{
		require_once( USAM_FILE_PATH . '/admin/includes/rules/basket_discount_rules.class.php' );	
			
		wp_enqueue_script( 'usam-basket_edit_conditions', USAM_URL.'/admin/js/basket_edit_conditions.js', array('jquery'), '1.1', true );
		wp_localize_script( 'usam-basket_edit_conditions', 'USAM_Basket_Edit_Conditions',					
			array(
			'text_and'    => __('И','usam'),
			'text_or'     => __('ИЛИ','usam'),		
			'text_product' => __('Выбрать товары заказа, которые удовлетворяют условиям','usam'),
			'text_group' => __('Группа условий','usam'),
			'text_add_conditions' => __('Добавить условие','usam'),
			'images_url'  => USAM_CORE_IMAGES_URL,	
			)
		);		
		$rules_work_basket = new USAM_Basket_Discount_Rules( );
		$rules_work_basket->load();
		$rules_work_basket->display( $conditions );	
	}	
	
	public function add_box_description( $description, $name = 'description', $title = ''  )
	{	
		$args['name'] = $name;		
		$args['description'] = $description;	
		if ( $title == '' )
			$title = __('Описание','usam');
		usam_add_box( 'usam_description', $title, array( $this, 'box_description' ), $args );	
	}
	
	public function box_description( $args ) 
	{		
		?>
		<div class ="descriptionbox">
			<textarea name='<?php echo $args['name']; ?>'><?php echo htmlspecialchars($args['description']); ?></textarea>
		</div>
		<?php
	}
	
	public function box_help_setting( $args, $id, $hidden = 0 ) 
	{		
		if ( is_array($args) )
		{
			$title = $args['title'];
			$description = $args['description'];
		}
		else
			return;		
		?>		
		<div class="box_help_setting <?php if ( $hidden == 0 ) { echo "hidden"; } ?>" id ="<?php echo $id; ?>">
			<h4><?php echo $title; ?><span> - <?php _e('объяснение', 'usam')?></span></h4>
			<p><?php echo $description; ?>        
			</p>
		</div>		
		<?php		
	}	
	
	public function display_imagediv( $thumbnail_id, $title = '', $button_text = ''  )
	{	
		if ( $button_text == '' )				
			$button_text = __( 'Задать миниатюру', 'usam' );
		$args['thumbnail_id'] = $thumbnail_id;		
		$args['title'] = $title;
		$args['button_text'] = $button_text;	
			
		usam_add_box( 'usam_description', __('Миниатюра','usam'), array( $this, 'imagediv' ), $args );	
	}
		
	public function imagediv( $args ) 
	{					
		$image_attributes = wp_get_attachment_image_src( $args['thumbnail_id'], 'thumbnail' );
		$thumbnail = $image_attributes[0];		
		$hide = '';
		if ( empty($thumbnail) )
		{
			$thumbnail = USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif';	
			$hide = 'hide';
		}							
		?>
		<div class="usam_thumbnail">
			<a id="select_thumbnail" data-attachment_id="<?php echo $args['thumbnail_id']; ?>" data-title="<?php echo $args['title']; ?>" title="<?php _e( 'Нажмите, чтобы установить миниатюру', 'usam' ); ?>" data-button_text="<?php echo $args['button_text']; ?>" href="<?php echo esc_url( admin_url( 'media-upload.php?tab=gallery&TB_iframe=true&width=640&height=566' ) ) ?>">
				<img src="<?php echo esc_url( $thumbnail ); ?>" alt="">
			</a>
			<input type='hidden' id='usam_thumbnail_id' name ="thumbnail" value='<?php echo $args['thumbnail_id']; ?>' />		
		</div>												
		<div class="usam_thumbnail_remove <?php echo $hide; ?>">				
			<a class="control remove" href="#" id="thumbnail_remove" title="<?php esc_html_e( 'Удалить миниатюру', 'usam' ); ?>"><?php esc_html_e( 'Удалить миниатюру', 'usam' ); ?></a>
		</div>			
		<?php		
	}	
	
	protected function total_table_product( $products, $columns )
	{
		
	}
	
	public function display_products_table( $products, $columns )
	{		
		register_column_headers( 'product_column', $columns );		
		?>	
		<div class = 'table_products_box'>				
			<table class="widefat table_products">
				<thead>
					<tr><?php print_column_headers( 'product_column' ); ?></tr>
				</thead>
				<tbody>
					<?php $this->display_table_products( $products, $columns ); ?>				
				</tbody>
				<tfoot>
					<?php $this->total_table_product( $products, $columns  ); ?>	
				</tfoot>			
			</table>							
		</div>		
		<?php
	}	
	
	public function add_product_button( )
	{
		?>						
		<div id = "add_product_button_box" class ="usam_autocomplete usam_edit_data">				
			<div class="autocomplete_forms">						
				<?php
					$autocomplete = new USAM_Autocomplete_Forms( );
					$autocomplete->get_form_product( );
				?>								
			</div>	
			<button id='add_product' type="button" class="button"><?php _e( 'Добавить товар', 'usam' ); ?></button>
			<?php usam_loader(); ?>									
		</div>			
		<?php		
	}
	
		
	function display_comment( $message = '' )
	{
		?>		
		<div class='comment_message_box'>
			<textarea class="event_comment_input event_input_text" placeholder="<?php _e('Напишите комментарий…','usam'); ?>" tabindex="1" dir="auto"><?php echo $message; ?></textarea>
		</div>			
		<?php
	}  
	
	public function display_comments( $object_type )
	{			
		require_once( USAM_FILE_PATH . '/includes/crm/comments_query.class.php' );		
		?>				
		<div class='comments usam_edits'>
			<?php
			$comments = usam_get_comments( array( 'object_id' => $this->id, 'object_type' => $object_type ) );
			foreach ( $comments as $comment )
			{
				?>		
				<div class='comment-box' data-id="<?php echo $comment->id; ?>">			
					<div class='comment_header'>
						<span class='comment_author'><?php echo usam_get_manager_name($comment->user_id); ?></span>
						<span class='comment_date'><?php echo usam_local_formatted_date( $comment->date_insert ); ?></span>
						<a class="js_delete_action" href="#"></a>
					</div>	
					<div class='comment_message usam_display_data'><?php echo $comment->message; ?></div>					
					<div class='usam_edit_data'>
						<?php $this->display_comment( $comment->message ); ?>
						<button id='edit_comment' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>	
					</div>						
				</div>	
				<?php
			}
			?>	
		</div>	
		<div class='new_comment'>
			<?php $this->display_comment( ); ?>			
			<button id='add_comment' type="button" class="button"><?php _e( 'Добавить', 'usam' ); ?></button>
		</div>			
		<?php	
	}
}
?>