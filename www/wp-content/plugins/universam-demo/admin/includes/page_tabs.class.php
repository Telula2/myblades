<?php
require_once( USAM_FILE_PATH . '/admin/includes/page_tab.class.php' );

final class USAM_Page_Tabs
{	
	private static $instance; 
	private static $default_tabs = array(); 
	private static $page_name;		

	private $group_tab_id;
	private $current_tab_id; 
	private $current_tab_class = null; 
	private $tabs; 
	
	
	public function __construct( $page = null, $tab_id = null )
	{	
		$this->set_current_page( $page );			
		if ( !empty(self::$default_tabs) )
		{	
			$this->init();
			
			do_action( 'usam_register_tabs', $this, self::$default_tabs );			
	
			$this->tabs = apply_filters( 'usam_tabs', $this->tabs );					
			$this->set_current_tab( $tab_id );					
		}
	}
	
	public static function init(  )
	{	
		add_action( 'usam_load_tab_class', array( 'USAM_Page_Tabs', 'load_default_tab_class' ), 1 );
		add_action( 'usam_register_tabs' , array( 'USAM_Page_Tabs', 'register_default_tabs'  ), 2, 2 );		
	}
	
	public static function get_instance() 
	{
		if ( ! self::$instance ) 
			self::$instance = new USAM_Page_Tabs();		
		return self::$instance;
	}

	public static function load_default_tab_class( $page_instance )
	{		
		$current_tab_id = $page_instance->get_current_tab_id();		
		$file_tab =  USAM_FILE_PATH . '/admin/menu-page/'.self::$page_name.'/tabs/'.$current_tab_id.'.php';	
		if ( file_exists( $file_tab ) ) 
		{	
			require_once( $file_tab );
		}
	}
	
	public static function register_default_tabs( $page_instance, $tabs )
	{		
		foreach ( (array)$tabs as $id => $value )
		{		
			$page_instance->register_tab( $value['id'], $value['title'] );	
			if ( isset($value['level']) )
			{
				$page_instance->register_default_tabs($page_instance, $value['level']);
			}		
		}		
	}	
	
	public function get_current_tab()
	{			
		if ( ! $this->current_tab_class ) 
		{
			do_action( 'usam_load_tab_class', $this );
			$class_name = ucwords( $this->current_tab_id );			
			$class_name = 'USAM_Tab_' . $class_name;	
			if ( class_exists( $class_name ) ) 
			{		
				$reflection = new ReflectionClass( $class_name );			
				$this->current_tab_class = $reflection->newInstance();
				$this->current_tab_class->load( self::$page_name, $this->current_tab_id );						
			}
		}		
		return $this->current_tab_class;
	}
	
	public function get_current_tab_id()
	{
		return $this->current_tab_id;
	}
	
	public function tab_user_can( $tab_id )
	{		
		$current_user = wp_get_current_user();

		if ( empty( $current_user ) )
			return false;
		
		$capability = "usam_tab_".$tab_id;

		if ( !isset($current_user->allcaps[$capability]) )
			return true;
		else
			return $current_user->allcaps[$capability];
	}	
	
	public function set_current_page( $page = null )
	{		
		if ( $page != null )
			self::$page_name = $page;
		else
			self::$page_name = isset($_REQUEST['page'])?$_REQUEST['page']:'';	

		require( USAM_FILE_PATH . '/admin/menu-page/'.self::$page_name.'/page_tabs.class.php' );
		if ( !empty($default_tabs) )
		{						
			foreach ( $default_tabs as $value )
			{			
				if ( $this->tab_user_can( $value['id'] ) || usam_check_current_user_role('administrator') )	
				{					
					self::$default_tabs[] = $value; //Вкладки				
				}
			}		
		}
	}
	
	public function go_through_levels_tab( $tabs )
	{
		foreach ( $tabs as $tab )
		{
			if ( $tab['id'] == $this->current_tab_id )
				return true;
			elseif ( isset($tab['level']) && $this->go_through_levels_tab( $tab['level'] ))
			{
				return true;
			}
		}
		return false;
	}
	
	
	public function set_group_tab_id( )
	{
		foreach ( self::$default_tabs as $tab )
		{
			if ( $tab['id'] == $this->current_tab_id )
			{
				$this->group_tab_id == $this->current_tab_id;
				break;
			}
			elseif ( isset($tab['level']) && $this->go_through_levels_tab( $tab['level'] ) )
			{
				$this->group_tab_id = $tab['id'];
				break;
			}
		}
	}		
		
	public function set_current_tab( $tab_id = null )
	{		
		if ( empty($this->tabs) )
			return;
	
		if ( ! $tab_id ) 
		{					
			if ( isset( $_REQUEST['tab'] ) && array_key_exists( $_REQUEST['tab'], $this->tabs ) )
				$this->current_tab_id = $_REQUEST['tab'];
			else
			{
				$key = array_keys( $this->tabs );
				$this->current_tab_id = array_shift( $key );
			}
		} 
		else 
			$this->current_tab_id = $tab_id;
		
		$this->set_group_tab_id();

		add_filter( 'admin_title', array( $this, 'set_page_title' ), 15, 2 );	
		$this->current_tab_class = $this->get_current_tab();	
	}	

	public function set_page_title( $admin_title, $_title ) 
	{		
		global $title;		
		return $title.' - '.$this->tabs[$this->current_tab_id];
	}
		
	public function register_tab( $id, $title ) 
	{
		$this->tabs[$id] = $title;
	}

	public function get_tabs() 
	{
		return $this->tabs;
	}

	private function tab_class( $id ) 
	{		
		$class = 'nav-tab';
		if ( $id == $this->current_tab_id || $id == $this->group_tab_id )
			$class .= ' nav-tab-active';
		if ( $id == $this->group_tab_id )
			$class .= ' nav-group-tab-active';
		return $class;
	}
	
	private function submit_url() 
	{
		$url = add_query_arg( 'tab', $this->current_tab_id );	
		return $url;
	}
	
	public function get_breadcrumbs( $tabs )
	{	
		$breadcrumbs = array();	
		foreach ( $tabs as $tab )
		{	
			$result = null;
			if ( isset($tab['level']) )				
				$result = $this->get_breadcrumbs( $tab['level'] );
			
			if ( $this->current_tab_id == $tab['id'] || !empty($result) )
			{					
				$breadcrumbs[] = array( 'id' => $tab['id'], 'title' => $tab['title'], 'url' => '' );
				if ( !empty($result) )
					$breadcrumbs = array_merge($breadcrumbs, $result);	
			}			
		}			
		return $breadcrumbs;
	}
	
	public function display_breadcrumbs(  )
	{	
		global $title;	
		
		$breadcrumbs = $this->get_breadcrumbs( self::$default_tabs );		
		?>		
		<div class="breadcrumbs-tab-wrapper">												
		<ul class="breadcrumb_tab">
			<li>
				<a class="breadcrumb-0" href="<?php echo esc_attr( '?page='.self::$page_name ); ?>"><?php echo $title; ?></a>&nbsp;&raquo;&nbsp;
			</li>
			<?php 			
			foreach ( $breadcrumbs as $breadcrumb )
			{				
				if ( $this->current_tab_id == $breadcrumb['id'] )
				{
					?>
					<li>
						<span class="breadcrumb-<?php echo $breadcrumb['id']; ?>"><?php echo esc_html( $breadcrumb['title'] ); ?></span>
					</li>
					<?php 
				}	
				else 
				{
				?>
				<li>
					<a class="breadcrumb-<?php echo $breadcrumb['id']; ?>" href="<?php echo esc_attr( '?page='.self::$page_name.'&tab='.$breadcrumb['id'] ); ?>"><?php echo esc_html( $breadcrumb['title'] ); ?></a>&nbsp;&raquo;&nbsp;
				</li>
				<?php 
				}
			}
			?>
		</ul>
		</div>
		<?php
	}	

	public function output_tabs( $tabs )
	{
		static $recursion = 0;		
		$recursion++;
		
		$count = 0;
		
		switch ( $recursion )
		{
			case 1:	
				$type_list_tag = 'ul';
				$type_item_tag = 'li';
				$class = 'main-menu';
			break;
			case 2:	
				$type_list_tag = 'ul';
				$type_item_tag = 'li';
				$class = 'subtab-menu';
			break;
			case 3:			
				$type_list_tag = 'dl';
				$type_item_tag = 'dt';
				$class = 'subtab-menu';
				
				$count = ceil( count( $tabs ) / 2 );
			break;
			default:
				$type_list_tag = 'dl';
				$type_item_tag = 'dd';
				$class = 'subtab-menu';				
			break;					
		}		
		$i = 0;
		?>
		<<?php echo $type_list_tag; ?> class="menu_tabs_level-<?php echo $recursion; ?> <?php echo $class; ?>">
			<?php 
			foreach ( $tabs as $tab )
			{				
				$i++;
				?>
				<<?php echo $type_item_tag; ?>>
					<a class="<?php echo $this->tab_class( $tab['id'] ); ?>" data-tab-id="<?php echo esc_attr( $tab['id'] ); ?>" href="<?php echo esc_attr( '?page='.self::$page_name.'&tab='.$tab['id'] ); ?>"><?php echo esc_html( $tab['title'] ); ?></a>
				<?php 
				if ( isset($tab['level']) )
				{
					$this->output_tabs( $tab['level'] );
					$recursion --;
				}
				?>
				</<?php echo $type_item_tag; ?>>
				<?php 
				if ( $recursion == 2 && $count == $i )			
				{				
					$str .= '</dl><dl>';				
				}
			}
			?>
		</<?php echo $type_list_tag; ?>>
		<?php
	}	
	
	public function display_current_tab()
	{
		if ( $this->current_tab_class == null )
			_e('Не доступа к вкладке','usam');
		elseif ( method_exists( $this->current_tab_class, 'display_tab' ) ) 
			$this->current_tab_class->display_tab();
		else
			_e('Файл вкладки не существует','usam');
	}
	
	public function display()
	{		
		global $title;		
		
		if ( filter_input( INPUT_GET, 'about' ) || get_transient( 'usam_process_complete') ) 
		{			
			delete_transient('usam_process_complete');
			require_once( USAM_FILE_PATH . '/admin/includes/about.php' );
			return;
		}		
		if ( $this->current_tab_class != null )
		{
		?>
			<div id="usam_page_tabs" class="usam_page_tabs wrap">
				<div class="usam_page_tabs_wrap">
					<div id="icon_card" class="icon32"></div>
					<h1 id="usam-page-tabs-title">
						<?php echo $title; ?><?php usam_loader(); ?>
					</h1>
				<?php	$this->display_paid();	?>
					<div class="nav-tab-wrapper">					
						<?php $this->output_tabs( self::$default_tabs ); ?>													
					</div>								
					<div id='usam_page_tab' class = 'usam_page page-<?php echo self::$page_name; ?>'>
						<?php $this->display_breadcrumbs( ); ?>					
						<?php $this->display_current_tab();	?>
					</div>				
				</div>
			</div>
		<?php
		}
		else
		{
			?>
			<div id="usam_page_tabs" class="usam_page_tabs wrap">
				<div class="usam_page_tabs_wrap">
					<div id="icon_card" class="icon32"></div>
					<h1 id="usam-page-tabs-title"><?php printf( __('У вас нет доступа к странице &laquo;%s&raquo;','usam'),$title); ?></h1>
				</div>
			</div>
		<?php
		}
	}		
	
	
	
	protected function buy_section() 
	{		
		$section = self::$page_name.'_'.$this->tab;
		?>
		<div class="buy_section">
			<h2 class="message"><?php _e( 'Подключите этот раздел чтобы иметь больше возможностей для управления', 'usam' ); ?></h2>
			<a target="_blank" href="<?php echo add_query_arg( array('action' => 'buy_section', 'section' => $section ),'http://wp-universam/buy/' ); ?>" id="buy_section-<?php echo $section; ?>" class="button-primary button"><?php _e( 'Подключить сейчас', 'usam' ); ?></a>
		</div>
		<?php
	}	
	
	public function display_paid_version() 
	{
		?>		
		<div style="margin-bottom:10px; width:100%;color:#fff;background-color: #a4286a;box-shadow: inset 0 10px 10px -5px rgba(123, 30, 80, 0.5), inset 0 -10px 10px -5px rgba(123, 30, 80, 0.5);">
			<div style="padding:5px;text-align:center;">
				Эта функция не доступна в вашей версии.<br><br>
				<a target="_blank" href="http://computer.wp-universam.ru" style="color:#fff;font-weight: 600;text-align:center; text-transform: uppercase;border-bottom: 1px solid #fff;">Посмотреть пример магазина</a> / <a target="_blank" href="http://wp-universam.ru/buy/" style="color:#fff;font-weight: 600; text-transform: uppercase;border-bottom: 1px solid #fff;">Купить</a><br><br>
					Перейти на расширенную версию! Увеличте ваши возможности!
			</div>	
		</div>
		<?php
	}
	
	public function check_the_version() 
	{
		$page_free = array( 
			'newsletter' => array( 'sending_email'), 
			'feedback' => array( 'chat', 'sms', 'email'), 
			'crm' => array( 'suggestions', 'invoice', 'tasks', 'affairs', 'events', 'calendar', 'work'),
			'manage_discounts' => array( 'basket', 'accumulative' ), 
			'customers' => array( 'cart', 'wish', 'compare', 'views'), 
			'exchange' => array( 'ftp' ), 
		//	'reports' => 'all',
			'social_networks' => 'all',
			'seo' => array( 'positions' ),			
		);
		$page_lite = array( 
			'newsletter' => array( 'sending_email'), 
			'feedback' => array( 'chat', 'sms'), 
			'crm' => array( 'suggestions', 'invoice', 'tasks', 'affairs', 'events', 'calendar'),
			'manage_discounts' => array( 'basket', 'accumulative' ), 		
			'exchange' => array( 'ftp' ), 
		//	'social_networks' => 'all',
			'seo' => array( 'positions' ),			
		);		
		if ( usam_is_license_type('FREE') )
			$page = $page_free;
		elseif ( usam_is_license_type('LITE') )
			$page = $page_lite;
		else
			return false;
		
		if ( isset($page[self::$page_name]) )
		{ 
			if ( $page[self::$page_name] == 'all' )
				return true;	
			elseif ( in_array($this->current_tab_id, $page[self::$page_name]) )
				return true;	
		}	
		return false;			
	}
	
	public function display_paid() 
	{	
		if ( $this->check_the_version() && !usam_is_license_type('PRO') )
			$this->display_paid_version();
	}		
}
?>