<?php 
if ( !current_user_can('edit_pages') || !current_user_can('edit_posts') ) 
	wp_die( __( 'Вы должны иметь разрешение, чтобы сделать это!', 'usam' ) );

$categorylist = get_terms('usam-category',array('hide_empty'=> 0));
$brands_list = get_terms('usam-brands',array('hide_empty'=> 0));

//global $current_screen;
//if ( empty( $current_screen ) )
//	set_current_screen();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>УНИВЕРСАМ</title>
		<?php		
	
		do_action('admin_enqueue_scripts');		
		do_action('admin_print_styles');	
		do_action('admin_print_scripts');	
		do_action('admin_head');

		wp_enqueue_script( 'jquery-ui-autocomplete' );	
		wp_enqueue_script( 'usam-tinymce' );	
		
		wp_enqueue_style( 'buttons' );	
		wp_enqueue_style( 'forms' );			
		?>			
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/tiny_mce_popup.js"></script>
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/utils/mctabs.js"></script>
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/utils/form_utils.js"></script>		
		<base target="_self" />
		<style type='text/css'>
			body{font-family: "Times New Roman" !important;}
			.usam_tabs_style1 .header_tab .tab {width: 90px!important;}	
			.header_tab ul{margin:0; padding:0}
			label{width: 100px}
			table{width: 100%}	
			table td{font-size: 16px !important; padding: 10px}			
			div.current{overflow-y: auto !important; height: 390px !important;}					
			.description{color:grey	!important;	font-style: italic !important; font-size: 12px !important;}	
			.countent_tabs #slider a{color: blue	!important;}			
			.shortcode{border: 1px solid #919B9C; padding: 10px}
			
			.mceActionPanel{margin-top:20px}			
		</style>
	</head>
<body id="link" >
	<form name="usam_scortcode" action="#">
		<div class = "wp-core-ui" >
			<div id = "usam_scortcode" class = "usam_tabs usam_tabs_style1" >
				<div class = "header_tab">
					<ul>
						<li class="current tab"><a href="#add_product"><?php _e("Товар", 'usam'); ?></a></li>				
						<li class = "tab"><a href="#product_list"><?php _e("Товары", 'usam'); ?></a></li>				
						<li class = "tab"><a href="#slider"><?php _e("Слайдер", 'usam'); ?></a></li>
						<li class = "tab"><a href="#page"><?php _e("Страницы", 'usam'); ?></a></li>		
						<li class = "tab"><a href="#terms"><?php _e("Списки", 'usam'); ?></a></li>	
						<li class = "tab"><a href="#map"><?php _e("Карта", 'usam'); ?></a></li>						
					</ul>
				</div>	
				<div class = "countent_tabs">	
					<div id = "add_product" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">
							<tr valign="top">					
								<td>
									<?php
									$autocomplete = new USAM_Autocomplete_Forms( );
									$autocomplete->get_form_product( );
									?>						
									<span class="description"><?php _e('Выберите товар, для которого вы хотели бы создать шорткод.', 'usam') ?></span>	
								</td>
							</tr>				
							<tr>	
								<td class ="shortcode">
									<input type="radio" id="add_product_buynow" name="usam_product_shortcode" value="buy_now_button">
									<label for="add_product_buynow"><?php _e("Добавить кнопку Купить сейчас", 'usam'); ?></label>
									<br /><span class="description"><?php _e('Это добавляет кнопку "Купить сейчас" для выбранного товара. Нажатие отправит Вашего клиента прямо в PayPal.', 'usam') ?></span>
								</td>
							<tr>						
								<td class ="shortcode">
									<input type="radio" id="usam_buy_product" name="usam_product_shortcode" value="buy_product"><?php _e('Добавить кнопку "Купить"', 'usam');?>
									<br /><span class="description"><?php _e('Это добавляет кнопку "Купить" для выбранного товара.' , 'usam') ?></span>
								</td>
							</tr>				
							<tr>						
								<td class ="shortcode">
									<input type="radio" id="usam_products" name="usam_product_shortcode" value="usam_products"><?php _e('Добавить товар', 'usam'); ?>
									<br /><span class="description"><?php _e('Это добавит выбранный товар на вашу страницу.' , 'usam') ?></span>
								</td>
							</tr>
							<tr>						
								<td class ="shortcode">
									<input type="radio" id="usam_product" name="usam_product_shortcode" value="usam_product"><?php _e('Добавить мини товар', 'usam'); ?>
									<br /><span class="description"><?php _e('Этот выбор добавить фото товара и ссылку на него.' , 'usam') ?></span>
									<br />
									<input type="checkbox" value="1" name="" id="usam_product_title" />		
									<label for="usam_product_title"><?php _e("Показать название", 'usam'); ?></label>
									<br />
									<input type="checkbox" value="1" name="" id="usam_product_price" />		
									<label for="usam_product_price"><?php _e("Показать цену", 'usam'); ?></label>	
									<br />
									<input type="checkbox" value="1" name="" id="usam_product_add_to_cart" />		
									<label for="usam_product_add_to_cart"><?php _e("Показать кнопку добавить в корзину", 'usam'); ?></label>	
									<br />
									<input type="checkbox" value="1" name="" id="usam_product_thumbnail" />		
									<label for="usam_product_thumbnail"><?php _e("Показать миниатюру", 'usam'); ?></label>	
									<br />									
									<?php $product_image = get_option( 'usam_product_image' ); ?>
									<label for="usam_product_price"><?php _e("Размер фотографии", 'usam'); ?></label>		
									<input type="text" value="<?php echo $product_image['width']; ?>" size = '3' maxlength='3' name="" id="usam_product_image_width" /> X 		
									<input type="text" value="<?php echo $product_image['height']; ?>" size = '3' maxlength='3' name="" id="usam_product_image_height" />	
								</td>						
							</tr>					
						</table>
					</div>					
					<div id = "product_list" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">					
							<tr valign="top">
								<td><strong><label for="usam_category"><?php _e("Выберите категорию: ", 'usam'); ?></label></strong></td>
								<td>
									<select id="usam_category" name="usam_category" style="width: 150px">
										<option value="0"><?php _e("Нет категории", 'usam'); ?></option>
										<?php 						
											foreach($categorylist as $category) 
												echo "<option value=".$category->term_id." >".$category->name."</option>"."\n";											
										?>
									</select><br />
									<span class="description"><?php _e('Выберите категорию, которую вы хотели бы опубликовать.', 'usam') ?></span>
								</td>
							</tr>					
							<tr valign="top">
								<td><strong><label for="usam_perpage"><?php _e("Количество товаров на странице: ", 'usam'); ?></label></strong></td>
								<td>
									<input name="number_per_page" id="usam_perpage" type="text" value="" style="width: 80px" /><br />
									<span class="description"><?php _e('Укажите количество товаров, которые вы хотели бы показать на странице, если необходимо', 'usam') ?></span>
								</td>
							</tr>				
							<tr>
								<td></td>	
								<td>
									<input type="radio" id="usam_sale_shortcode" checked="checked" name="usam_sale_shortcode" value="2"><?php _e('Показать все товары', 'usam');?>
									<br /><span class="description"><?php _e('Показать все товары в выбранной категории' , 'usam') ?></span>
								</td>
							</tr>
							<tr>
								<td></td>	
								<td>
									<input type="radio" id="usam_sale_shortcode" name="usam_sale_shortcode" value="1"><?php _e('Показать акционные товары', 'usam');?>
									<br /><span class="description"><?php _e('Показать только акционные товары на странице' , 'usam') ?></span>
								</td>
							</tr>
						</table>
					</div>	
					<div id = "slider" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">	
							<tr valign="top">
								<?php 
								global $wpdb;					
								$sliders = $wpdb->get_results( "SELECT * FROM ".USAM_TABLE_SLIDER." ORDER BY id" );							
								 ?>
								<td><strong><label for="usam_product_slider"><?php _e("Выберете слайдер", 'usam'); ?></label></strong></td>
								<td>
									<select id="usam_product_slider" style="width: 200px">
										<option value="0"><?php _e("не выбран", 'usam'); ?></option>	
										<?php
											foreach($sliders as $slider) 
												echo "<option value=".$slider->id." >".$slider->name."</option>"."\n";
										?>
									</select><br />
									<span class="description"><?php _e('Выберите слайдер, для которого вы хотели бы создать шорткод.', 'usam') ?></span>
								</td>
							</tr>							
						</table>		
					</div>	
					<div id = "page" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">	
							<tr valign="top">									
								<td>
									<input type="checkbox" value="1" name="" id="search_my_order" />		
									<label for="search_my_order"><?php _e("Что с моим заказом", 'usam'); ?></label><br/>
									<span class="description"><?php _e('Добавить шорт-код формы "Что с моим заказом".', 'usam') ?></span>
								</td>
							</tr>		
							<tr valign="top">									
								<td>
									<input type="checkbox" value="1" name="" id="shareonline" />		
									<label for="shareonline"><?php _e("Акции магазина", 'usam'); ?></label><br/>
									<span class="description"><?php _e('Добавить шорт-код страницы с акциями магазина.', 'usam') ?></span>
								</td>
							</tr>			
							<tr valign="top">									
								<td>
									<input type="checkbox" value="1" name="" id="reviews" />		
									<label for="reviews"><?php _e("Отзывы", 'usam'); ?></label><br/>
									<span class="description"><?php _e('Добавить шорт-код страницы с отзывами магазина.', 'usam') ?></span>
								</td>
							</tr>	
							<tr valign="top">									
								<td>
									<input type="checkbox" value="1" name="" id="contactform" />		
									<label for="contactform"><?php _e("Контактная форма", 'usam'); ?></label><br/>
									<span class="description"><?php _e('Добавить шорт-код контактной формы.', 'usam') ?></span>
								</td>
							</tr>	
							<tr valign="top">									
								<td>
									<input type="checkbox" value="1" name="" id="point_receipt_products" />		
									<label for="point_receipt_products"><?php _e("Точки самовывоза", 'usam'); ?></label><br/>
									<span class="description"><?php _e('Добавить шорт-код карт точек самовывоза.', 'usam') ?></span>
								</td>
							</tr>								
						</table>				
					</div>		
					<div id = "terms" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">								
							<tr valign="top">
								<td><strong><label for="usam_category"><?php _e("Выберите категорию: ", 'usam'); ?></label></strong></td>
								<td>
									<select id="usam_category" style="width: 150px">
										<option value=""><?php _e("Не выбрано", 'usam'); ?></option>
										<option value="-"><?php _e("Все категории", 'usam'); ?></option>
										<?php 												
											foreach($categorylist as $category) 
												echo "<option value=".$category->term_id." >".$category->name."</option>"."\n";
											unset($categorylist);																
										?>
									</select><br />
									<span class="description"><?php _e('Выберите категорию, подкатегории которой будут опубликованы.', 'usam') ?></span>
								</td>
							</tr>														
						</table>				
					</div>		
					<div id = "map" class = "tab">						
						<table border="0" cellpadding="4" cellspacing="0">								
							<tr valign="top">
								<td><strong><label for="usam_coordinates"><?php _e("Координаты: ", 'usam'); ?></label></strong></td>
								<td><label for="GPS_N">N</label><input type="text" value="" size = '10' maxlength='10' id="GPS_N"/> <label for="GPS_S">S</label><input type="text" value="" size = '10' maxlength='10' id="GPS_S" /></td>
							</tr>	
							<tr valign="top">	
								<td><strong><label for="usam_coordinates"><?php _e("Название: ", 'usam'); ?></label></strong></td>
								<td><input type="text" value="" id="usam_title"/></td>
							</tr>	
							<tr valign="top">
								<td><strong><label for="usam_description"><?php _e("Описание: ", 'usam'); ?></label></strong></td>
								<td><input type="text" value="" id="usam_description"/></td>
							</tr>														
						</table>				
					</div>					
				</div>	
			</div>		
		<div class="mceActionPanel">
			<div style="float: left">
				<input type="button" id="cancel_button" class="secondary button" value="<?php _e("Отмена", 'usam'); ?>" onclick="tinyMCEPopup.close();" />
			</div>			
			<div style="float: right">
				<input type="submit" id="insert_button" class="button-primary button" value="<?php _e("Вставить", 'usam'); ?>" onclick="USAM_Insert_Link();" />
			</div>
		</div>
		</form>	
		<?php	
		do_action('admin_print_footer_scripts');	
		?>
		</div>
	</body>	
</html>