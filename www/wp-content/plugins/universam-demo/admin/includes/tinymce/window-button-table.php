<?php
$table = array( 1 => array ('name'=>'','value' => ''),
				2 => array ('name'=>'','value' => ''),
				3 => array ('name'=>'','value' => ''),
				4 => array ('name'=>'','value' => ''),
				5 => array ('name'=>'','value' => ''));
//Проверка доступа
if ( !current_user_can('edit_pages') && !current_user_can('edit_posts') ) 
	wp_die( __( 'Вы должны иметь разрешение, чтобы сделать это!', 'usam' ) );

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8" />
		<title>Добавление таблицы с характеристиками</title>			
		<base target="_self" />
		<?php			
		do_action('admin_enqueue_scripts');		
		do_action('admin_print_styles');	
		do_action('admin_print_scripts');	
		do_action('admin_head');
		
		wp_enqueue_script( 'jquery-ui-autocomplete' );	
		wp_enqueue_style( 'buttons' );	
		?>		
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/tiny_mce_popup.js"></script>
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/utils/mctabs.js"></script>
		<script language="javascript" type="text/javascript" src="<?php echo includes_url(); ?>js/tinymce/utils/form_utils.js"></script>
		<script language="javascript" type="text/javascript" src="<?php echo USAM_URL; ?>/admin/js/tinymce3/table/tinymce_table.js"></script>
		<style type='text/css'>
			 body{font-family: "Georgia" !important; background: #F1F1F1}
			#message_panel{padding: 10px 0}
			.panel_wrapper{height: auto !important;}
			.panel_wrapper{padding: 10px;}
			.table_variant{margin: 10px 0; padding: 10px; background: #E1E1E1;}
			.table_variant ul{margin: 0px; display: inline-block; padding: 5px 0 5px 5px;}
			.table_variant li{float: left; display: block; list-style: none; padding-right: 10px}
			.table_variant li a {text-decoration: none; color:#000;}
			.table_variant li a:hover {border-bottom: 1px solid #000;}
			.table_variant li img:hover {cursor: pointer;}
			.panel_wrapper h3{padding: 10px 0; font-size: 18px; font-style: italic; color: #222222; font-weight: 600;}
			.panel_wrapper table{width: 100%; font-size: 16px; line-height: 18px;}
			.panel_wrapper table thead, .panel_wrapper table tfoot{font-size: 14px; text-transform: uppercase; text-align: center; border: 1px solid #E1E1E1; background: #E1E1E1; }
			.panel_wrapper table tbody{background: #F1F1F1}
			table .table_add_row{width: 60px;}
			.panel_wrapper input{font-size: 16px !important; line-height: 18px !important; width: 100%;}
			div.current{height: auto !important; width: auto !important;}			
			.description{color:grey	!important;	font-style: italic !important;}			
			#product_slider_panel a{color: blue	!important;}	
			.table_add_row a{background-image: url("<?php echo USAM_URL; ?>/admin/css/plus-minus.png");background-repeat: no-repeat;display: inline-block;height: 20px;margin-left: 3px;
			text-indent: -9999em;width: 20px;}
			.table_add_row a.link_del_row {background-position: 0px -20px;}
			.save_table{ background: #F1F1F1; padding: 10px;}
			.save_table #name_table{width: 40%;}
			.mceActionPanel{width: 100%; display: inline-block; margin: 20px 0}
		</style>
		 <script type="text/javascript">
	
	var get_table = function(  ) 	
	{		
		 alert(tinyMCE.activeEditor.getContent({format : 'html'}));
	}
	
	var read_table = function( number_table ) 	
	{		
		jQuery.ajax({
			type: "POST",
			url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
			data: { read_table: 'action', name_table: number_table},			
			success: function(response)
			{				
				response = JSON.parse(response);					
				var i = 0;
				var bol = true;
				var id_table_tbody = '#current_table_characteristics tbody';
				var id_table = 'current_table_characteristics';
				var id_row_new;					
				jQuery(id_table_tbody+ ' .name input').attr("value", '');
				jQuery(id_table_tbody+' tr').each(function () 
				{		
					if (i in response)
					{
						jQuery(id_table_tbody + " tr#" + this.id + " .name input").attr("value", response[i]);
						id_row_new = this.id;
					}
					else 					
						bol = false;
					i++;
					return bol;
				});				
				if (i in response)
				{				
					var id_row, new_row, new_row_id;					
					for (var k = i; k < response.length; k++) 
					{				
						id_row_new = k+1;
						new_row = jQuery(id_table_tbody + " tr").eq(0).clone().attr("id","table_row-" + id_row_new);
						new_row_id = id_table_tbody + " tr#table_row-" + id_row_new;
						id_row = id_row_new-2;					
						jQuery(id_table_tbody + ' tr').eq(id_row).after( new_row );							
						jQuery(new_row_id + ' .table_add_row a.link_add_row').attr("onclick","table_add_row("+id_row_new+",'" + id_table + "')");
						jQuery(new_row_id + ' .table_add_row a.link_del_row').attr("onclick","table_del_row("+id_row_new+",'" + id_table + "')");		
						jQuery(new_row_id + ' .name input').attr("name"," ");	
						jQuery(new_row_id + " .name input").attr("value", response[k]);
					}
				}
				jQuery('#message_panel').html('Вставлено!');
			}
		});
	}
	
	
	var table_add_row = function(id_row_new, id_table) 
	{	
		id_row_new++;		
		var new_row_id = '#' + id_table + " tbody tr#table_row-" + id_row_new;
		var array_id_row = new Array();
		var id = new Array();		
		
		jQuery('#' + id_table + " tbody tr").each(function (i) {
			array_id_row.push(this.id);
		});
		if (array_id_row.length > 1)		
			for (var i = array_id_row.length-1; i >= id_row_new-1; i--) 
			{	
				id = array_id_row[i].split('-');
				id_row = parseInt(id[1]);		
				id_row_n_new = id_row + 1;
				var row  = jQuery('#' + id_table + " tbody tr#table_row-" + id_row);	
				jQuery('#' + id_table + " tbody tr#table_row-" + id_row + " .table_add_row a.link_add_row").attr("onclick","table_add_row("+id_row_n_new+",'" + id_table + "')");	
				jQuery('#' + id_table + " tbody tr#table_row-" + id_row + " .table_add_row a.link_del_row").attr("onclick","table_del_row("+id_row_n_new+",'" + id_table + "')");
				row.attr("id","table_row-" + id_row_n_new);			
			}		
		var new_row = jQuery('#' + id_table + " tbody tr").eq(0).clone().attr("id","table_row-" + id_row_new);
		
		id_row = id_row_new-2;		
		jQuery('#' + id_table + ' tbody tr').eq(id_row).after( new_row );	
		jQuery(new_row_id + ' .table_add_row a.link_add_row').attr("onclick","table_add_row("+id_row_new+",'" + id_table + "')");
		jQuery(new_row_id + ' .table_add_row a.link_del_row').attr("onclick","table_del_row("+id_row_new+",'" + id_table + "')");		
		jQuery(new_row_id + ' .name input').attr("value"," ");		
		jQuery(new_row_id + ' .value_option input').attr("value"," ");
	}
	
	var table_del_row = function(id_row, id_table) 	
	{		
		var array_id_row = new Array();
		jQuery('#' + id_table + " tbody tr#table_row-" + id_row).remove();
		
		jQuery('#' + id_table + " tbody tr").each(function (i) {
			array_id_row.push(this.id);
		});
		if (array_id_row.length > 1)		
			for (var i = 0; i <= array_id_row.length; i++) 
			{	
				id = array_id_row[i].split('-');
				id_row = parseInt(id[1]);		
				id_row_n_new = i + 1;
				var row  = jQuery('#' + id_table + " tbody tr#table_row-" + id_row);	
				jQuery('#' + id_table + " tbody tr#table_row-" + id_row + " .table_add_row a.link_add_row").attr("onclick","table_add_row("+id_row_n_new+",'" + id_table + "')");	
				jQuery('#' + id_table + " tbody tr#table_row-" + id_row + " .table_add_row a.link_del_row").attr("onclick","table_del_row("+id_row_n_new+",'" + id_table + "')");
				row.attr("id","table_row-" + id_row_n_new);			
			}		
	}
	
	
	var insert_table = function() 	
	{		
		var inset_text = '', table = '';
		var i = 1;
		var str = '';
		var start = false;
		
		jQuery('#current_table_characteristics tbody tr input').each(function () 
		{			
			if ( this.name.search('name') != -1)
				str = "<td class = 'name'>" + this.value + "</td>";	
			else
				str = str + "<td class = 'value_option'>" + this.value + "</td>";
			
			if ( this.name.search('characteristics') != -1)
			{		
				if ( i % 2 == 0 )
				{					
					if ( this.value != '' )
					{						
						table = table + "<tr>" + str +  "</tr>";
						start = true;
					}
					str = '';
				}		
				i++; 				
			}			 
		});	
		//window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false, table);
		tinyMCE.execCommand('mceInsertContent',false, table); 
		tinyMCEPopup.editor.execCommand('mceRepaint');
		tinyMCEPopup.close();
	}
	
	var clean_option = function()
	{		
		jQuery('#current_table_characteristics tbody .name input').attr("value", '');
		jQuery('#current_table_characteristics tbody .value_option input').attr("value", '');
	}	
	
		</script>
	</head>		
<body id="link" onload="document.getElementById('current_table').focus();">
	<div id = "message_panel"></div>
	<form name="product_table" action="#">
		<div class="tabs">
			<ul>
				<li id="current_table" class="current"><span><a href="javascript:mcTabs.displayTab('current_table','usam_table_tab');" onmousedown="return false;"><?php _e("Добавление таблицы", 'usam'); ?></a></span></li>					
			</ul>
		</div>		
		<div class="panel_wrapper">
			<div id="usam_table_tab" class="panel current">				
				<h3><?php _e('Таблица','usam'); ?></h3>
				<table id = "current_table_characteristics" border="0" cellpadding="4" cellspacing="0">				
					<thead>
						<tr>
							<td><?php _e("Название", 'usam'); ?></td>
							<td><?php _e("Значение", 'usam'); ?></td>
							<td></td>
						</tr>
					</thead>				
					<tbody>
					<?php 						
					foreach($table as $key => $row_table) 
					{
					?>
						<tr id = "table_row-<?php echo $key; ?>">
							<td class = "name">
								<strong><input type='text' name='characteristics[name][<?php echo $key; ?>]' value='<?php echo $row_table['name']; ?>' /></strong>
							</td>
							<td class = "value_option">
								<input  type='text' name='characteristics[value][<?php echo $key; ?>]' value='<?php echo $row_table['value']; ?>' />
							</td>
							<td class = "table_add_row">
								<a onclick="table_add_row(<?php echo $key; ?>,'current_table_characteristics');" title="<?php _e('Добавить','usam'); ?>" class = "link_add_row" href="#"><?php _e('Добавить','usam'); ?></a>
								<a onclick="table_del_row(<?php echo $key; ?>,'current_table_characteristics');" title="<?php _e('Удалить','usam'); ?>" class = "link_del_row" href="#"><?php _e('Удалить','usam'); ?></a>
							</td>
						</tr>
					<?php } ?>
					</tbody>					
				</table>						
			</div>			
			<div id="usam_add_product_tab" class="panel">
			
			</div>			
		</div>
		
		<div class="mceActionPanel">
			<div style="float: left; margin-right: 10px">
				<input type="button" id="cancel" class="secondary button" name="cancel" value="<?php _e("Отменить", 'usam'); ?>" onclick="tinyMCEPopup.close();" />
			</div>
			<div style="float: left">
				<input type="button" id="cancel" class="secondary button" name="clean" value="<?php _e("Очистить", 'usam'); ?>" onclick="clean_option();" />
			</div>
			<div style="float: right">
				<input type="submit" id="insert" class="button-primary button" name="insert" value="<?php _e("Вставить", 'usam'); ?>" onclick ="insert_table();" />
			</div>			
		</div>
	</form>
</body>
</html>