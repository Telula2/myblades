<?php
class USAM_Exported_Table
{
	private $filename = 'exported';
	
    public function __construct()
	{	
	
    }	
	
	// Выгрузка в csv-файл			
	function csv( $items, $header )
	{		
		if ( current_user_can( 'manage_options' ) )
		{						
			$output = '';

			foreach($header as $key => $value)
			{		
				$output .= "\"" . $value . "\","; 	
			}
			$output .= "\n"; // Конец строки
			foreach($items as $item)
			{				
				foreach($header as $key => $val)
				{
					if ( isset($item->$key) )
						$output .= $item->$key; 
					$output .= ",";
				}
				$output .= "\n"; // Конец строки
			}
			header( 'Content-Type: text/csv' );
			header( 'Content-Disposition: inline; filename="'.$this->filename.'.csv"' );
			echo $headers . "\n". $output;
			exit;
		}
	}
	
	function excel( $items, $header )
	{		
		if ( current_user_can( 'manage_options' ) )
		{			 	
			require_once( USAM_FILE_PATH . '/resources/PHPExcel/PHPExcel.php');	// подключаем фреймворк
			$pExcel = new PHPExcel(); //создаем рабочий объект
			$pExcel->setActiveSheetIndex(0); // устанавливаем номер рабочего документа
			$aSheet = $pExcel->getActiveSheet(); // получаем объект рабочего документа
			
			$writer_i = 1;			
			$j = 0;			
			foreach($header as $key => $value)
			{
				$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$aSheet->setCellValueByColumnAndRow($j,$writer_i,$value); // записываем данные массива в ячейку
				$j++;			
			}			
			$writer_i++;			
			foreach($items as $ar)
			{
				$j = 0;					
				$aSheet->getStyle($writer_i)->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); // выравнивание
				foreach($header as $key => $val)
				{					
					if ( isset($ar->$key) )
					{						
						$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
						$aSheet->setCellValueByColumnAndRow($j,$writer_i,$ar->$key); // записываем данные массива в ячейку				
					}
					elseif ( isset($ar[$key]) )
					{						
						$aSheet->getStyleByColumnAndRow($j, $writer_i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
						$aSheet->setCellValueByColumnAndRow($j,$writer_i,$ar[$key]); // записываем данные массива в ячейку				
					}					
					$j++;
				}			
				$writer_i++;
			}			
			// определение максимальных размеров документа
			$max_col = $aSheet->getHighestColumn();
			/* автоширина */
			for ($col = 'A'; $col <= $max_col; $col++) {
			   $aSheet->getColumnDimension($col)->setAutoSize(true);
			}			
			$objWriter = new PHPExcel_Writer_Excel5( $pExcel ); // создаем объект для записи excel в файл
			header('Content-Type: application/vnd.ms-excel'); // посылаем браузеру нужные заголовки для сохранения файла
			header('Content-Disposition: attachment;filename="'.$this->filename.'.xls"');
			header('Cache-Control: max-age=0');	
			$objWriter->save('php://output'); // выводим данные в excel формате
			exit;
		}
	}
}