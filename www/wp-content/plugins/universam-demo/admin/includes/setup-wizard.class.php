<?php
/**
 *  Мастер установки
 */
class USAM_Admin_Setup_Wizard 
{
	private $step   = '';
	private $steps  = array();
	
	public function __construct()
	{
		if ( apply_filters( 'usam_enable_setup_wizard', true ) && current_user_can( 'edit_shop_settings' ) ) 
		{
			add_action( 'admin_menu', array( $this, 'admin_menus' ) );
			add_action( 'admin_init', array( $this, 'setup_wizard' ) );
		}
	}

	/**
	 * Add admin menus/screens.
	 */
	public function admin_menus() {
		add_dashboard_page( '', '', 'manage_options', 'usam-setup', '' );
	}
	
	public function update_option( $key, $value) 
	{
		update_option( 'usam_'.$key, $value );
	}
	
	public function setup_wizard()
	{
		if ( empty( $_GET['page'] ) || 'usam-setup' !== $_GET['page'] ) {
			return;
		}
		set_current_screen( 'usam_setup_wizard' );
		
		$default_steps = array(
			'introduction' => array(
				'name'    => __( 'Начальная страница', 'usam' ),
				'view'    => 'introduction',
			),			
			'information' => array(
				'name'    => __( 'Информация о магазине', 'usam' ),
				'view'    => 'information',
			),
			'types_payers' => array(
				'name'    => __( 'Типы плательщиков', 'usam' ),
				'view'    => 'types_payers',			
			),
			'shipping_payments' => array(
				'name'    => __( 'Доставка и оплата', 'usam' ),
				'view'    => 'shipping_payments',
			),	
			'template_messages' => array(
				'name'    => __( 'Стиль сообщений', 'usam' ),
				'view'    => 'template_messages',
			),
			'seo' => array(
				'name'    => __( 'SEO', 'usam' ),
				'view'    => 'seo',
			),		
			'crm' => array(
				'name'    => __( 'CRM', 'usam' ),
				'view'    => 'crm',
			),						
			'theme' => array(
				'name'    => __( 'Тема', 'usam' ),
				'view'    => 'theme',
			),
			'next_steps' => array(
				'name'    => __( 'Готово', 'usam' ),
				'view'    => 'ready',
			),
		);
		if ( ! current_user_can( 'install_themes' ) || ! current_user_can( 'switch_themes' ) || is_multisite() ) {
			unset( $default_steps['theme'] );
		}

		$this->steps = apply_filters( 'usam_setup_wizard_steps', $default_steps );
		$this->step = isset( $_GET['step'] ) ? sanitize_key( $_GET['step'] ) : current( array_keys( $this->steps ) );	
	
		wp_enqueue_style( 'usam-setup', USAM_URL . '/admin/css/setup-wizard.css', array( 'dashicons', 'install' ), USAM_VERSION );		
		wp_enqueue_style('themes');	
		
		$this->callback_submit();

		ob_start();
		$this->setup_wizard_header();
		$this->setup_wizard_steps();
		$this->setup_wizard_content();
		$this->setup_wizard_footer();
		exit;		
	}
	
	public function callback_submit()
	{		
		if ( ! empty( $_POST['save_step'] ) && isset( $this->steps[ $this->step ]['view'] ) )
		{			
			$method = 'controller_'.$this->steps[ $this->step ]['view'].'_save';			
			if ( method_exists($this, $method) )
			{ 
				check_admin_referer( 'usam_setup' );
				$this->$method();		
			}
			wp_redirect( esc_url_raw( $this->get_next_step_link() ) );
			exit;
		}	
	}

	public function get_next_step_link( $step = '' ) 
	{
		if ( ! $step ) {
			$step = $this->step;
		}

		$keys = array_keys( $this->steps );
		if ( end( $keys ) === $step ) {
			return admin_url();
		}

		$step_index = array_search( $step, $keys );
		if ( false === $step_index ) {
			return '';
		}

		return add_query_arg( 'step', $keys[ $step_index + 1 ] );
	}

	public function setup_wizard_header() 
	{
		?>
		<!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		<head>
			<meta name="viewport" content="width=device-width" />
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title><?php esc_html_e( 'УНИВЕРСАМ &rsaquo; Мастер установки', 'usam' ); ?></title>			
			<?php wp_print_scripts( 'usam_setup' ); ?>
			<?php do_action( 'admin_enqueue_scripts', 'usam_setup_wizard' ); ?>	
			<?php do_action( 'admin_print_styles' ); ?>
			<?php do_action( 'admin_print_scripts' ); ?>
			<?php do_action( 'admin_head' ); ?>	
		</head>
		<body class="usam_setup wp-core-ui">
			<h1 id="usam_logo"><a href="https://wp-universam.ru"><img src="<?php echo USAM_CORE_IMAGES_URL; ?>/universam.png" alt="Универсам" /></a></h1>
			<form method="post">
		<?php
	}

	
	public function setup_wizard_footer() 
	{				
			if ( 'next_steps' === $this->step ) : ?>
				<a class="usam_return-to-dashboard" href="<?php echo esc_url( admin_url() ); ?>"><?php esc_html_e( 'Вернуться в консоль WordPress', 'usam' ); ?></a>
			<?php endif; ?>
			<?php do_action("admin_footer", 'usam_setup_wizard'); ?>
			<?php do_action('admin_print_footer_scripts'); ?>		
			</body>
			</form>
		</html>
		<?php
	}

	
	public function setup_wizard_steps() 
	{
		$ouput_steps = $this->steps;
		array_shift( $ouput_steps );
		?>
		<ol class="usam_setup-steps">
			<?php foreach ( $ouput_steps as $step_key => $step ) : ?>
				<li class="<?php
					if ( $step_key === $this->step ) {
						echo 'active';
					} elseif ( array_search( $this->step, array_keys( $this->steps ) ) > array_search( $step_key, array_keys( $this->steps ) ) ) {
						echo 'done';
					}
				?>"><?php echo esc_html( $step['name'] ); ?></li>
			<?php endforeach; ?>
		</ol>
		<?php
	}

	public function setup_wizard_content() 
	{
		echo '<div class="usam_setup-content">';		
		$method = 'setup_'.$this->steps[ $this->step ]['view'];	
		if ( method_exists($this, $method) )
		{			
			$this->$method();		
		}	
		echo '</div>';
		wp_nonce_field( 'usam_setup' ); 
	}

	
	public function setup_introduction()
	{
		?>
		<h1><?php esc_html_e( 'Добро пожаловать!', 'usam' ); ?></h1>
		<p><?php _e( 'Спасибо за то, что выбрали &laquo;Универсам&raquo;, самую большую платформу для <strong>бизнеса и интернет-магазина</strong>, помогающую управлять любыми каналами продаж. Этот помощник установки поможет произвести базовые настройки. Это необязательная процедура и она не должна занять много времени.', 'usam' ); ?></p>
		<p><?php _e( 'Если вы что-то продаете то, <strong>&laquo;Универсам&raquo; поможит увеличить продажи и сократить человеко-часы</strong> на облуживания вашего бизнеса.', 'usam' ); ?></p>		
		<p><?php esc_html_e( 'Вы можете настроить &laquo;Универсам&raquo; позже с помощью этого помощника или с помощью страницы настроек магазина. Он поможет найти ваших конкурентов на рынке.', 'usam' ); ?></p>		
		<p class="usam_setup-actions step">
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button-primary button button-large button-next"><?php esc_html_e( "Начать настройку", 'usam' ); ?></a>
			<a href="<?php echo esc_url( admin_url() ); ?>" class="button button-large"><?php esc_html_e( 'Не сейчас', 'usam' ); ?></a>
		</p>
		<?php
	}
		
	public function setup_information()
	{				
		$currency_default = get_option( 'usam_currency_type' );
		$currency_pos   = get_option( 'usam_currency_sign_location' );
		$decimal_sep    = get_option( 'usam_decimal_separator' );	
		$thousand_sep   = get_option( 'usam_thousands_separator' );
		
		$location = get_option( 'usam_shop_location' );
		$currencies = usam_get_currencies( );		
		$prices = usam_get_prices( );	
		
		$dimension_unit = get_option( 'usam_dimension_unit', false );
		$weight_unit    = get_option( 'usam_weight_unit', false );
		$shop_company = get_option( 'usam_shop_company' ); 		
		$return_email = get_option( 'usam_return_email' ); 		
		
		$mailboxes = usam_get_mailboxes( );	
		?>
		<h1><?php esc_html_e( 'Информация о магазине', 'usam' ); ?></h1>	
		<table class="form-table">
			<tr>
				<th scope="row"><label for="decimal_sep"><?php esc_html_e( 'Ваша компания', 'usam' ); ?></label></th>
				<td>
					<?php usam_select_bank_accounts( $shop_company, array('name' => "shop_company") );
					if ( empty($shop_company) )	
					{
						?><p><?php _e( 'Добавьте вашу фирму в разделе и банковский счет', 'usam' ); ?> &laquo;<a target="_blank" href="<?php echo admin_url("admin.php?page=crm&tab=company"); ?>"><?php _e( 'Компании', 'usam' ); ?></a>&raquo; <?php _e( 'и укажите тип компании &laquo;своя&raquo;. После добавления обновите эту страницу.', 'usam' ); ?></p><?php
					}
					?>							
				</td>
			</tr>					
			<tr>
				<th scope="row"><label for="store_location"><?php esc_html_e( 'Местоположение магазина', 'usam' ); ?></label></th>
				<td>					
				<?php
				$autocomplete = new USAM_Autocomplete_Forms( );
				$autocomplete->get_form_position_location( $location );
				?>	
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="decimal_sep"><?php esc_html_e( 'Email магазина', 'usam' ); ?></label></th>
				<td>
					<?php 
					if ( !empty($mailboxes) )										
					{ 
						?>
						<select id="return_email" name="return_email" style="width:100%;" data-placeholder="<?php esc_attr_e( 'Выберите…', 'usam' ); ?>" class="chzn-select">							
							<?php
							foreach ( $mailboxes as $mailbox )
							{
								echo '<option value="' . esc_attr( $mailbox->id ) . '" ' . selected( $return_email, $mailbox->id, false ) . '>' . $mailbox->name.' ('.$mailbox->email.')</option>';
							}
							?>
						</select>
						<?php
					}
					else
					{
						?><span class="description"><?php printf( __( 'Добавьте ваш почтовый адрес можно <a href="%s" target="_blank">добавить её здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=mailboxes') ); ?></span><?php
					}
					?>							
				</td>
			</tr>	
			<tr>
				<th scope="row"><label for="currency_type"><?php esc_html_e( 'Валюта магазина', 'usam' ); ?></label></th>
				<td>
					<select id="currency_type" name="currency_type" style="width:100%;" data-placeholder="<?php esc_attr_e( 'Выберите валюту…', 'usam' ); ?>" class="chzn-select">
						<option value=""><?php esc_html_e( 'Choose a currency&hellip;', 'usam' ); ?></option>
						<?php
						foreach ( $currencies as $currency ) {
							echo '<option value="' . esc_attr( $currency->code ) . '" ' . selected( $currency_default, $currency->code, false ) . '>' . $currency->code." ($currency->name)" . '</option>';
						}							
						?>
					</select>
					<span class="description"><?php printf( __( 'Если нужная валюта отсутствует в списке, можно <a href="%s" target="_blank">добавить её здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=currency') ); ?></span>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="currency_sign_location"><?php esc_html_e( 'Позиция валюты', 'usam' ); ?></label></th>
				<td>
					<select id="currency_sign_location" name="currency_sign_location" class="chzn-select-nosearch">
						<option value="3" <?php selected( $currency_pos, '3' ); ?>><?php esc_html_e( 'Слева', 'usam' ); ?></option>
						<option value="1" <?php selected( $currency_pos, '1' ); ?>><?php esc_html_e( 'Справа', 'usam' ); ?></option>
						<option value="4" <?php selected( $currency_pos, '4' ); ?>><?php esc_html_e( 'Слева с пробелом', 'usam' ); ?></option>
						<option value="2" <?php selected( $currency_pos, '2' ); ?>><?php esc_html_e( 'Справа с пробелом', 'usam' ); ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="thousands_separator"><?php esc_html_e( 'Разделитель тысяч', 'usam' ); ?></label></th>
				<td>
					<input type="text" id="thousands_separator" name="thousands_separator" size="2" value="<?php echo esc_attr( $thousand_sep ); ?>" />
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="decimal_separator"><?php esc_html_e( 'Десятичный разделитель', 'usam' ); ?></label></th>
				<td>
					<input type="text" id="decimal_separator" name="decimal_separator" size="2" value="<?php echo esc_attr( $decimal_sep ); ?>" />
				</td>
			</tr>	
			<tr>
				<th scope="row"><label for="prices"><?php esc_html_e( 'Цены', 'usam' ); ?></label></th>
				<td>
					<ul>
					<?php
						foreach ( $prices as $price ) 
						{
							echo "<li>".$price['title']."</li>";
						}
					?>
					</ul>
					<span class="description"><?php printf( __( 'Вы можете <a href="%s" target="_blank">добавить цены здесь</a>. Например, цены для оптовых покупателей.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=prices') ); ?></span>
				</td>
			</tr>	
			<tr>
				<th scope="row"><label for="weight_unit"><?php esc_html_e( 'Единица измерения веса', 'usam' ); ?></label></th>
				<td>
					<select id="weight_unit" name="weight_unit" class="chzn-select-nosearch">
						<option value="kg" <?php selected( $weight_unit, 'kg' ); ?>><?php esc_html_e( 'кг', 'usam' ); ?></option>
						<option value="g" <?php selected( $weight_unit, 'g' ); ?>><?php esc_html_e( 'грамм', 'usam' ); ?></option>
						<option value="lbs" <?php selected( $weight_unit, 'lbs' ); ?>><?php esc_html_e( 'фунт', 'usam' ); ?></option>
						<option value="oz" <?php selected( $weight_unit, 'oz' ); ?>><?php esc_html_e( 'унция', 'usam' ); ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="dimension_unit"><?php esc_html_e( 'Единица измерения', 'usam' ); ?></label></th>
				<td>
					<select id="dimension_unit" name="dimension_unit" class="chzn-select-nosearch">
						<option value="m" <?php selected( $dimension_unit, 'm' ); ?>><?php esc_html_e( 'м', 'usam' ); ?></option>
						<option value="cm" <?php selected( $dimension_unit, 'cm' ); ?>><?php esc_html_e( 'см', 'usam' ); ?></option>
						<option value="mm" <?php selected( $dimension_unit, 'mm' ); ?>><?php esc_html_e( 'мм', 'usam' ); ?></option>
						<option value="in" <?php selected( $dimension_unit, 'in' ); ?>><?php esc_html_e( 'дюйм', 'usam' ); ?></option>
						<option value="yd" <?php selected( $dimension_unit, 'yd' ); ?>><?php esc_html_e( 'ярд', 'usam' ); ?></option>
					</select>
				</td>
			</tr>				
		</table>
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
		</p>
		<?php
	}
		
	public function controller_information_save() 
	{		
		$shop_company = !empty($_POST['shop_company'])?absint( $_POST['shop_company'] ):0;
		$shop_location  = absint( $_POST['location'] );
		$return_email   = !empty($_POST['return_email'])?absint( $_POST['return_email'] ):0;
		$currency_type    = sanitize_text_field( $_POST['currency_type'] );
		$currency_sign_location   = sanitize_text_field( $_POST['currency_sign_location'] );
		$thousands_separator   = sanitize_text_field( $_POST['thousands_separator'] );
		$decimal_separator   = sanitize_text_field( $_POST['decimal_separator'] );
		$weight_unit   = sanitize_text_field( $_POST['weight_unit'] );
		$dimension_unit   = sanitize_text_field( $_POST['dimension_unit'] );

		$this->update_option( 'shop_company', $shop_company );
		$this->update_option( 'shop_location', $shop_location );
		$this->update_option( 'return_email', $return_email );
		$this->update_option( 'currency_type', $currency_type );
		$this->update_option( 'currency_sign_location', $currency_sign_location );
		$this->update_option( 'thousands_separator', $thousands_separator );
		$this->update_option( 'decimal_separator', $decimal_separator );
		$this->update_option( 'weight_unit', $weight_unit );
		$this->update_option( 'dimension_unit', $dimension_unit );
	}

	public function setup_shipping_payments() 
	{
		$delivery_service = usam_get_delivery_services();
		$gateways = usam_get_payment_gateways( );		
		?>
		<h1><?php esc_html_e( 'Службы доставки и варианты оплаты ', 'usam' ); ?></h1>	
		<table class="form-table">
			<tr>
				<th scope="row"><label for="delivery_service"><?php esc_html_e( 'Службы доставки', 'usam' ); ?></label></th>
				<td>
					<ul>
					<?php
						foreach ( $delivery_service as $shipping ) 
						{
							echo "<li>".$shipping->name."</li>";
						}
					?>
					</ul>
					<span class="description"><?php printf( __( 'Изменить и добавить службу доставки можно <a href="%s" target="_blank">здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=shipping') ); ?></span>
				</td>
			</tr>	
			<tr>
				<th scope="row"><label for="gateways"><?php esc_html_e( 'Варианты оплаты', 'usam' ); ?></label></th>
				<td>
					<ul>
					<?php
						foreach ( $gateways as $gateway ) 
						{
							echo "<li>".$gateway->name."</li>";
						}
					?>
					</ul>
					<span class="description"><?php printf( __( 'Изменить и добавить вариант оплаты можно <a href="%s" target="_blank">здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=payment_gateway') ); ?></span>
				</td>
			</tr>	
		</table>
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<input type="submit" class="button button-large button-next" value="<?php esc_attr_e( 'Пропустить этот шаг', 'usam' ); ?>" name="save_step" />
		</p>
		<?php
	}

	public function controller_shipping_payments_save() 
	{
	
	}	
	
	public function setup_types_payers() 
	{
		$types_payers = usam_get_group_payers( );			
		?>
		<h1><?php esc_html_e( 'Типы плательщиков', 'usam' ); ?></h1>	
		<p><?php _e( 'Выберите типы плательщиков, которым вы будете продавать на сайте. Для разных типов плательщиков возможны различные способы оплаты и доставки, а также различный набор свойств заказа.','usam' ); ?></p>
		<ul class="usam_checked_wrapper">
			<?php foreach ( $types_payers as $types_payer ) : ?>
				<li class="usam_checked usam_checked-<?php echo esc_attr( $types_payer['id'] ); ?> <?php echo $types_payer['active']==1?'checked':''; ?>">
					<div class="usam_checked_enable">
						<input type="checkbox" name="types_payers[]" class="input-checkbox" value="<?php echo esc_attr( $types_payer['id'] ); ?>" <?php checked( $types_payer['active'] ); ?>/>
						<label><?php echo esc_html( $types_payer['name'] ); ?></label>
					</div>										
				</li>
			<?php endforeach; ?>
		</ul>
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
		</p>
		<?php
	}
	
	public function controller_types_payers_save()
	{		
		$types_payers_ids = !empty($_POST['types_payers'])?array_map('intval', $_POST['types_payers']):array(); 
		$types_payers = usam_get_group_payers( );
		foreach ( $types_payers as $types_payer )
		{				
			if ( in_array($types_payer['id'], $types_payers_ids ) )
				$types_payer['active'] = 1;		
			else
				$types_payer['active'] = 0;	
			usam_edit_data( $types_payer, $types_payer['id'], 'usam_types_payers' );	
		}	
	}
	
	public function controller_seo_save()
	{		
		$check_position_site = !empty($_POST['check_position_site'])?1:0;
		update_option( 'usam_check_position_site', $check_position_site );	
		if ( !empty($_POST['keywords']) )
		{			
			$keywords = explode("\r\n", $_POST['keywords']);
			usam_insert_keywords( $keywords );
		}
	}
	
	private function setup_seo() 
	{
		$check_position_site = get_option('usam_check_position_site');
		?>		
		<h4><?php _e( 'Добавить ключевые слова для анализа позиции в поиске', 'usam' ); ?></h4>
		<textarea rows="30" cols="100" id="keywords" name="keywords"></textarea>
		<ul class="usam_checked_wrapper">
			<li class="usam_checked usam_checked-check_position_site; ?> <?php echo $check_position_site==1?'checked':''; ?>">
				<div class="usam_checked_enable">
					<input type="checkbox" name="check_position_site" class="input-checkbox" value="1" <?php checked( $check_position_site ); ?>/>
					<label><?php _e( 'Включить проверку позиции сайта', 'usam' ); ?></label>
				</div>									
			</li>
		</ul>
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
		</p>
		<?php
	}
	
	function controller_crm_save()
	{	
		$record_affairs_managers = !empty($_POST['record_affairs_managers'])?array_map('intval', $_POST['record_affairs_managers']):array();
		update_option( 'usam_record_affairs_managers', $record_affairs_managers );	
	}	

	private function setup_crm() 
	{
		$record = get_option('usam_record_affairs_managers', array());
		$args = array( 'orderby' => 'nicename', 'role__in' => array('shop_manager','administrator'), 'fields' => array( 'ID','display_name') );
		$users = get_users( $args );			
		?>		
		<h4><?php _e( 'Вы можете контролировать ваших менеджеров, записывая их работу', 'usam' ); ?></h4>
		<ul class="usam_checked_wrapper">
			<?php foreach ( $users as $user ) : ?>
				<li class="usam_checked usam_checked-<?php echo esc_attr( $user->ID ); ?> <?php echo in_array($user->ID, $record)?'checked':''; ?>">
					<div class="usam_checked_enable">
						<input type="checkbox" name="record_affairs_managers[]" class="input-checkbox" value="<?php echo esc_attr( $user->ID ); ?>" <?php checked( in_array($user->ID, $record), 1 ); ?>/>
						<label><?php echo esc_html( $user->display_name ); ?></label>
					</div>										
				</li>
			<?php endforeach; ?>
		</ul>
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
		</p>
		<?php
	}	
	
	private function setup_template_messages()
	{		
		$mailtemplate_list = usam_get_templates( 'mailtemplate' );	
		$active = get_option('usam_mailtemplate');	
		?>
		<h1><?php esc_html_e( 'Глобальный шаблон сообщений электронной почты', 'usam' ); ?></h1>
		<div class ="theme-browser" >
			<div class ="themes wp-clearfix" >
			<?php		
			$url = admin_url('options-general.php?page=shop_settings&tab=template_messages');
			foreach ($mailtemplate_list as $template => $data ) 
			{
				$class = $template == $active?'active':'';			
				?>				
				<div class="theme <?php echo $class; ?>" tabindex="0" aria-describedby="<?php echo $template; ?>-action <?php echo $template; ?>-name">
		
					<div class="theme-screenshot">
						<img src="<?php echo $data['screenshot']; ?>" alt="">
					</div>				
					<span class="more-details"><?php echo __('Автор', 'usam').": ".$data['author']; ?></span>				
					<h3 class="theme-name"><?php echo $template; ?></h3>

					<div class="theme-actions">
						<a class="button button-primary" href="<?php echo usam_url_admin_action('install_mailtemplate', $url, array( 'theme' => $template ) ); ?>
						"><?php _e('Установить', 'usam'); ?></a>						
					</div>
				</div>
				<?php
			}
			?>
			</div>
		</div>		
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Продолжить', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
		</p>
		<?php
	}
		
	/**
	 * Установка темы
	 */
	private function setup_theme() 
	{		
		?>
		<h2><?php esc_html_e( 'Использование тему Store Leader', 'usam' ); ?></h2>
		<p class="usam_wizard_theme_intro">
			<?php echo wp_kses_post( __( '<strong>Leader</strong> - это бесплатная тема для WordPress, которая разработана и поддерживается создателями Универсам.', 'usam' ) ); ?>
			<img src="http://wp-universam.ru/wp-content/uploads/sl/downloadables/wp-theme/StoreLeader/screenshot.jpg" alt="Store Leader" />
		</p>

		<ul class="usam_wizard_theme_capabilities">
			<li class="usam_wizard_theme first"><?php echo wp_kses_post( __( '<strong>Безотказная интеграция:</strong> вы можете не сомневаться в надежности интеграции между Универсам, расширениями Универсам и Store Leader.', 'usam' ) ); ?></li>		
			<li class="usam_wizard_theme last"><?php echo wp_kses_post( __( '<strong>Оптимизировано для поиска:</strong> Актуальная семантическая разметка для улучшенной поисковой оптимизации.', 'usam' ) ); ?></li>
		</ul>
		<h2><?php esc_html_e( 'Использование своей темы', 'usam' ); ?></h2>
		<strong><?php esc_html_e( 'Используйте следующие функции для интеграции', 'usam' ); ?>:</strong>
		<ul class="usam_wizard_theme_capabilities">
			<li class="usam_wizard_theme first"><?php _e('Добавить поиск', 'usam');?> - <code>&lt;?php usam_search_widget(); ?&gt;</code> <?php _e('Обычно добавляют в header.php.', 'usam'); ?></li>
			<li class="usam_wizard_theme first"><?php _e('Получить ссылку на основные страницы магазина', 'usam');?> - <code>&lt;?php usam_url_system_page( 'basket' ); ?&gt;</code> <?php _e('Обычно добавляют в ссылки.','usam'); ?></li>
			<li class="usam_wizard_theme first"><?php _e('Получить промежуточную сумму корзины', 'usam');?> - <code>&lt;?php usam_get_basket_subtotal(); ?&gt;</code> <?php _e('Обычно добавляют в header.php.', 'usam');?></li>			
			<li class="usam_wizard_theme first"><?php _e('Получить количество товаров в корзине', 'usam');?> - <code>&lt;?php usam_get_basket_number_items(); ?&gt;</code> <?php _e('Обычно добавляют в header.php.', 'usam');?></li>		
		</ul>
		<strong><?php esc_html_e( 'Используйте следующие файлы для изменения', 'usam' ); ?>:</strong>
		<ul class="usam_wizard_theme_capabilities">
			<li class="usam_wizard_theme first"><?php _e('Шаблон товара', 'usam');?> - single-product.php</li>
			<li class="usam_wizard_theme first"><?php _e('Универсальный шаблон списков товаров', 'usam'); ?> - page-products.php <?php _e('Если удалить, например, page-sale.php, то страница home.ru/page-sale будет отображаться по этому шаблону.', 'usam'); ?></li>
			<li class="usam_wizard_theme first"><?php _e('Шаблон страницы со скидочными товарами', 'usam');?> - page-sale.php</li>
			<li class="usam_wizard_theme first"><?php _e('Шаблон страницы товаров', 'usam');?> - page-products_page.php</li>
			<li class="usam_wizard_theme first"><?php _e('Универсальный шаблон', 'usam');?> - page.php</li>
		</ul>
		<p><?php esc_html_e( 'Многие шаблоны разделены на несколько файлов. Например, шаблон страницы с корзиной использует универсальный шаблон (page.php) и шаблон content-page-basket.php, который подключается через функцию usam_get_content().', 'usam' ); ?></p>				
		<p><?php esc_html_e( 'Для изменения выше описаных файлов, скопируйте их в вашу тему.', 'usam' ); ?></p>		
		<p class="usam_setup-actions step">
			<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e( 'Установить и активировать', 'usam' ); ?>" name="save_step" />
			<a href="<?php echo esc_url( $this->get_next_step_link() ); ?>" class="button button-large button-next"><?php esc_html_e( 'Пропустить этот шаг', 'usam' ); ?></a>
			<?php wp_nonce_field( 'usam_setup' ); ?>
		</p>		
		<?php

	}
	
	private function controller_theme_save()
	{		
		wp_schedule_single_event( time() + 1, 'usam_theme_installer', array( 'StoreLeader' ) );	
	}
	
	public function setup_ready() 
	{		
		?>
		<h1><?php esc_html_e( 'Ваш магазин готов!', 'usam' ); ?></h1>
		<p >
			<?php printf( __( 'Вы можете продолжить настройку в разделе <a href="%s" target="_blank">настройки магазина</a> в любой момент. Вы можете в любой момент задать нам вопрос. Для этого нажмите на &laquo;Центр подержки&raquo; на любой странице.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=shipping') ); ?>
		</p>
		<h2><?php esc_html_e( 'Рекомендуем настроить', 'usam' ); ?>:</h2>
		<ul class="usam_wizard_theme_capabilities">
			<li class="usam_wizard_theme first"><?php printf( __( 'Подключить группу или анкеты Вконтакте можно <a href="%s" target="_blank">здесь</a>. Появится возможность ручного или автоматического постинга, автоматическое поздравление с днем рождения и многое другое', 'usam' ), admin_url('admin.php?page=shop_settings&tab=social_networks') ); ?></li>		
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте скидки на товар <a href="%s" target="_blank">здесь</a>. Например, выбрать товар дня или добавить правило создания автоматических купонов, установить скидку на товар или создать скидку на корзину товаров и многое другое.', 'usam' ), admin_url('admin.php?page=manage_discounts') ); ?></li>
			<li class="usam_wizard_theme last"><?php printf( __( 'Подключить сервисы Яндекс или Google можно <a href="%s" target="_blank">здесь</a>. Появится возможность передача данных в Яндекс метрика, подключить Яндекс XML(необходим для определения позиции в яндексе) и многое другое', 'usam' ), admin_url('admin.php?page=shop_settings&tab=search_engines') ); ?></li>
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте смс шлюз <a href="%s" target="_blank">здесь</a>. Будет возможность отправлять клиентам смс оповещения.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=messages') ); ?></li>			
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте бланки <a href="%s" target="_blank">здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=blanks') ); ?></li>
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте SEO <a href="%s" target="_blank">здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=seo') ); ?></li>
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте автоматический подбор перекрестных продаж <a href="%s" target="_blank">здесь</a>.', 'usam' ), admin_url('admin.php?page=shop_settings&tab=crosssell') ); ?></li>	
			<li class="usam_wizard_theme last"><?php printf( __( 'Настройте сайты ваших партнеров, если вы занимаетесь перепродажей <a href="%s" target="_blank">здесь</a>. Цены и доступность товаров будет автоматически проверяться', 'usam' ), admin_url('admin.php?page=shop_settings&tab=resale') ); ?></li>			
		</ul>		
		<div class="usam_setup-next-steps">
			<div class="usam_setup-next-steps-first">
				<h2><?php esc_html_e( 'Следующие шаги', 'usam' ); ?></h2>
				<ul>
				<?php 
				if ( !usam_license() )
				{
				?>
					<li class="setup-first"><a class="button button-primary button-large" href="<?php echo esc_url( admin_url( 'index.php?page=usam-license' ) ); ?>" target="_blank"><?php esc_html_e( 'Активируйте лицензию!', 'usam' ); ?></a></li>
				<?php 
				}
				?>
					<li class="setup-first"><a class="button button-large" href="<?php echo esc_url( admin_url( 'post-new.php?post_type=usam-product' ) ); ?>" target="_blank"><?php esc_html_e( 'Создайте свой первый товар!', 'usam' ); ?></a></li>
					<li class="setup-first"><a class="button button-large" href="<?php echo esc_url( admin_url( 'admin.php?page=exchange&tab=product_importer' ) ); ?>" target="_blank"><?php esc_html_e( 'Импорт товаров из CSV файла', 'usam' ); ?></a></li>
					<li class="setup-first"><a class="button button-large" href="<?php echo esc_url( admin_url( 'admin.php?page=shop_settings&tab=cashbox' ) ); ?>" target="_blank"><?php esc_html_e( 'Подключите кассу в соответствие с 54-ФЗ', 'usam' ); ?></a></li>
				</ul>
			</div>
			<div class="usam_setup-next-steps-last">
				<h2><?php _e( 'Узнать больше', 'usam' ); ?></h2>
				<ul>					
					<li class="learn-more"><a target='blank' href="http://docs.wp-universam.ru/document/category/universam"><?php esc_html_e( 'Узнать больше о том как начать', 'usam' ); ?></a></li>
					<li class="learn-more"><a target='blank' href="http://wp-universam.ru/posmotret/"><?php esc_html_e( 'Возможности', 'usam' ); ?></a></li>
					<li class="newsletter"><a target='blank' href="http://wp-universam.ru/kontakty"><?php esc_html_e( 'Задать вопрос', 'usam' ); ?></a></li>					
				</ul>
			</div>
		</div>
		<?php
	}
}
new USAM_Admin_Setup_Wizard();
?>