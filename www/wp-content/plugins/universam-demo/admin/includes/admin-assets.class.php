<?php
/**
 * Загрузка стилей и скриптов.
 * @version     2.1.0
 */

class USAM_Admin_Assets 
{	
	private $version = '';
	public function __construct() 
	{			
		add_action( 'admin_enqueue_scripts', array( $this, 'init' ), 3 );	
		add_action( 'admin_head-media-upload-popup', array( $this, 'address_book' ), 3 );		
	}
	
	public function address_book( $pagehook ) 
	{
		wp_enqueue_style( 'usam-address_book' );		
	}
		
	public function init( $pagehook ) 
	{				
		if ( defined('WP_DEBUG') && WP_DEBUG  )
			$this->version = time();		
		else
			$this->version = USAM_VERSION;		
				
		$this->register( $pagehook );		
		$this->scripts_and_style( $pagehook);				
	}
	
	private function register( $pagehook ) 
	{					
		$styles = array( 
			'usam-admin-taxonomies' => array( 'file_name' => 'taxonomies.css', 'deps' => array(), 'media' => 'all' ),		
			'usam-edit-form' => array( 'file_name' => 'edit_form.css', 'deps' => array(), 'media' => 'all' ), // Редактирование элементов таблицы
			'usam-view-form' => array( 'file_name' => 'view_form.css', 'deps' => array(), 'media' => 'all' ), // Редактирование элементов таблицы
			'usam-order-admin' => array( 'file_name' => 'order.css', 'deps' => array(), 'media' => 'all' ),
			'usam-admin' => array( 'file_name' => 'admin.css', 'deps' => array(), 'media' => 'all' ),	
			'usam-address_book' => array( 'file_name' => 'address_book.css', 'deps' => array(), 'media' => 'all' ),	
		);
		foreach( $styles as $name => $style )
		{
			wp_register_style( $name, USAM_URL . '/admin/css/'.$style['file_name'], $style['deps'], $this->version, $style['media'] );	
		}		
		$scripts = array( 
			'usam-sort_fields' => array( 'file_name' => 'sort_fields.js', 'deps' => array( 'jquery-query' ), 'in_footer' => false ),// Сортировка полей		
			'usam-sortable-table' => array( 'file_name' => 'sortable-table.js', 'deps' => array('jquery-query'), 'in_footer' => false ),          // Сортировка полей
			'usam-set_thumbnails' => array( 'file_name' => 'set_thumbnails.js', 'deps' => array('jquery', 'usam-admin'), 'in_footer' => false ),   //Установка миниатюр     
			'usam-admin' => array( 'file_name' => 'admin.js', 'deps' => array('jquery', 'jquery-ui-core', 'jquery-ui-sortable'), 'in_footer' => false ),   	
			'usam-mail-editor' => array( 'file_name' => 'mail-editor/mail-editor.js', 'deps' => array('jquery', 'jquery-ui-sortable', 'jquery-ui-droppable'), 'in_footer' => false ),   
			'usam-selecting_locations' => array( 'file_name' => 'selecting_locations.js', 'deps' => array('jquery', 'usam-admin'), 'in_footer' => false ), 		
			'usam-taxonomy_general' => array( 'file_name' => 'taxonomy/taxonomy_general.js', 'deps' => array('jquery', 'usam-admin'), 'in_footer' => false ), 
			'edit-data-admin' => array( 'file_name' => 'edit_data.js', 'deps' => array('jquery'), 'in_footer' => false ),			
			'usam-tinymce' => array( 'file_name' => 'tinymce3/scortcode/tinymce.js', 'deps' => array('jquery'), 'in_footer' => false ),			
		);		
		foreach( $scripts as $name => $script )
		{
			wp_register_script( $name, USAM_URL . '/admin/js/'.$script['file_name'], $script['deps'], $this->version, $script['in_footer'] );	
		}
	}	
		
	/**
	 * Загрузка скриптов
	 */
	public function scripts_and_style( $pagehook ) 
	{		
		global $post_type, $current_screen, $post;
		
		if ( get_option('usam_pointer', false) )
		{
			wp_enqueue_style ('wp-pointer');
			wp_enqueue_script ('wp-pointer');
		}				
		wp_enqueue_style( 'usam-form' );		
		wp_enqueue_script( 'chosen' );			
		wp_enqueue_script( 'usam-tab' );	
		
		wp_enqueue_script( 'jquery-ui-datepicker' );
		
		wp_localize_script( 'usam-taxonomy_general', 'USAM_Taxonomy', array(				
			'set_taxonomy_order_nonce' => usam_create_ajax_nonce( 'set_taxonomy_order' ),	
		));		
		wp_enqueue_script( 'usam-admin' );	
		wp_localize_script( 'usam-admin', 'Universam_Admin', array(			
			'dragndrop'                => isset($_GET['orderby']) && strpos($_GET['orderby'], 'menu_order')!== false?true:false,			
			'save_product_order_nonce' => usam_create_ajax_nonce( 'save_product_order' ), // сортировка товара	
			'product_rating_nonce' => usam_create_ajax_nonce( 'product_rating' ), //рейтинг товара	
			'select_remind_nonce'      => usam_create_ajax_nonce( 'select_remind' ), // Отложить задание			
			'bulk_actions_terms'       => usam_create_ajax_nonce( 'bulk_actions_terms' ), // Перенести термин		
			'bulk_actions_product_attribute' => usam_create_ajax_nonce( 'bulk_actions_product_attribute' ), 
			'update_featured_products' => usam_create_ajax_nonce( 'update_featured_products' ), 
			'bulk_edit_no_vars'        => esc_html__( 'Быстрый параметры ограничены при редактировании товаров, которые имеют варианты.', 'usam' ),
			'current_page'             => empty($_REQUEST['page']) ? '' : sanitize_title($_REQUEST['page']),
		) );					
		wp_enqueue_style( 'usam-admin');
		wp_enqueue_style( 'chosen-style' );
		wp_enqueue_style( 'datetimepicker' );		
		
		wp_enqueue_script( 'bootstrap' );			
		if ( isset($current_screen->id) )
		switch ( $current_screen->id ) 
		{						
			case 'dashboard':				
				wp_enqueue_style( 'usam-dashboard', USAM_URL .'/admin/css/dashboard.css', false, $this->version, 'all' );	
			break;	
			case 'usam-product':					
				wp_enqueue_style( 'usam-product-metabox', USAM_URL . '/admin/css/product-metabox.css', false, $this->version, 'all' );	
				wp_enqueue_script( 'usam-edit-product', USAM_URL . '/admin/js/edit_product.js', array( 'jquery' ), $this->version );						
				wp_localize_script( 'usam-edit-product', 'USAM_Edit_Product', array(
					'product_id' => isset($_GET['post'])?$_GET['post']:0,						
					'loading_information_nonce' => usam_create_ajax_nonce( 'loading_information' ),
					'save_variant_set_table_product_nonce' => usam_create_ajax_nonce( 'save_variant_set_table_product' ),							
					'message_no_link' => __( 'Ссылка не указана', 'usam' ),									
				) );
				wp_enqueue_script( 'usam-variations', USAM_URL .'/admin/js/variations.js', array( 'jquery', 'usam-sortable-table' ), $this->version, true );
				wp_localize_script( 'usam-variations', 'USAM_Variations', array(				
					'button_text' => __( 'Установить миниатюру', 'usam' ), 
					'set_variation_thumbnail_nonce' => usam_create_ajax_nonce( 'set_variation_thumbnail' ),
					)
				);					
			break;			
			case 'edit-usam-product':
				wp_enqueue_script( 'usam-rating' );
				wp_enqueue_script( 'usam-products_list', USAM_URL . '/admin/js/products_list.js', array( 'jquery' ), $this->version );	
			break;	
			case 'manage_discounts_product_day':	
				wp_enqueue_script( 'usam-sortable-table' );
			break;					
			case 'manage_discounts_rules_coupons':			
				wp_enqueue_script( 'usam-selecting_locations' );
			break;
			case 'shop_settings_order_status':	
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );
				
				$this->sort_fields( 'slider' );
			break;
			case 'interface_slider':	
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );
				
				$this->sort_fields( 'slider' );
			break;
			case 'crm_affairs':	
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );
			break;
			case 'crm_tasks':	
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );
			break;
			case 'crm_calendar':					
				global $user_ID; 
				
				$month = isset($_GET['month']) && (int)$_GET['month']<=12?(int)$_GET['month']:date('n');
				$day = isset($_GET['day']) && (int)$_GET['day']<=31?(int)$_GET['day']:date('d');
				$year = isset($_GET['year'])?(int)$_GET['year']:date('Y');	
		
				wp_enqueue_script( 'usam-calendar', USAM_URL . '/admin/js/calendar.js', array( 'jquery' ), $this->version );						
				wp_localize_script( 'usam-calendar', 'USAM_Calendar', array(
					'add_event_nonce' => usam_create_ajax_nonce( 'add_event' ),
					'message_edit_name' => __( 'Редактирование задания', 'usam' ).':',
					'message_view_event_day' => __( 'Посмотреть события в этот день', 'usam' ),			
					'message_add_event' => __( 'Добавить задание', 'usam' ),
					'message_event_importance' => __( 'Важное', 'usam' ),
					'message_event_unimportant' => __( 'Обычное', 'usam' ),		
					'message_many_event' => __( 'Есть еще задание', 'usam' ),
					'view_calendar_event_nonce' => usam_create_ajax_nonce( 'view_calendar_event' ),
					'delete_event_nonce' => usam_create_ajax_nonce( 'delete_event' ),
					'edit_event_nonce' => usam_create_ajax_nonce( 'edit_event' ),
					'select_calendar_nonce' => usam_create_ajax_nonce( 'select_calendar' ),
					'save_tab_calendar_nonce' => usam_create_ajax_nonce( 'save_tab_calendar' ),							
					'get_data_calendar_nonce' => usam_create_ajax_nonce( 'get_data_calendar' ),
					'month' => $month,			
					'day' => $day,	
					'year' => $year,			
					'tab' => get_user_meta($user_ID, 'usam_tab_calendar', true ),									
				) );
			break;			
			case 'feedback_reviews':				
				wp_enqueue_script('edit-data-admin');
			break;					
			case 'interface_menu':	
				$this->sort_fields( 'menu' );
			break;		
			case 'shop_settings_property':	
				$this->sort_fields( 'order_property' );
			break;								
			case 'dashboard_page_documents':	
				wp_enqueue_style('usam-order-admin');	
			break;	
			case 'documents_shipped':				
			case 'documents_cancellation':
			case 'documents_payment':
				wp_enqueue_style( 'usam-admin');				
			break;					
			case 'purchase_rules_rules':			
				wp_enqueue_script( 'usam-selecting_locations' );
			break;		
			case 'manage_prices_taxes':			
				wp_enqueue_script( 'usam-selecting_locations' );
			break;			
			case 'manage_prices_type_price':			
				wp_enqueue_script( 'usam-selecting_locations' );
			break;		
			case 'storage_storage':			
				$this->set_thumbnails( );
			break;			
			case 'shop_settings_shipping':			
				$this->set_thumbnails( );
			break;			
			case 'crm_contacts':			
				$this->set_thumbnails( );
			break;
			case 'crm_company':			
				$this->set_thumbnails( );
			break;	
			case 'shop_settings_payment_gateway':			
				$this->set_thumbnails( );
			break;		
			case 'edit-usam-category':		
				wp_enqueue_script( 'usam-sortable-table' );	
				wp_enqueue_style( 'usam-admin-taxonomies');			
			
				$this->set_thumbnails(  );	
			
				wp_enqueue_script( 'usam-taxonomy_general' );								
			break;
			case 'edit-usam-product_attributes':			
				wp_enqueue_script( 'usam-sortable-table' );	
				wp_enqueue_style( 'usam-admin-taxonomies');		
				wp_enqueue_script( 'usam-product_attributes', USAM_URL . '/admin/js/taxonomy/product_attributes.js', array( 'jquery' ), $this->version  );
				wp_localize_script( 'usam-product_attributes', 'USAM_Product_Attributes',					
					array(
					'term_id'  => !empty($_GET['tag_ID'])?$_GET['tag_ID']:0,
					'display_category_in_attributes_product_nonce'  => usam_create_ajax_nonce( 'display_category_in_attributes_product' ),
					'add_category_in_attributes_product_nonce'  => usam_create_ajax_nonce( 'add_category_in_attributes_product' ),
					'delete_category_in_attributes_product_nonce'  => usam_create_ajax_nonce( 'delete_category_in_attributes_product' ),				
					'text_delete' => __( 'Удалить', 'usam' ),								
					)
				);		
				wp_enqueue_script( 'usam-taxonomy_general' );					
			break;			
			case 'edit-usam-variation':						
				wp_enqueue_script( 'usam-product_variation', USAM_URL . '/admin/js/taxonomy/product_variation.js', array( 'jquery' ), $this->version  );							
				wp_enqueue_script( 'usam-sortable-table' );				
				wp_enqueue_style( 'usam-admin-taxonomies');
				
				$this->set_thumbnails( );	

				wp_enqueue_script( 'usam-taxonomy_general' );					
			break;	
			case 'edit-usam-brands':
				wp_enqueue_script( 'usam-sortable-table' );				
				wp_enqueue_style( 'usam-admin-taxonomies');
				
				$this->set_thumbnails( );	
		
				wp_enqueue_script( 'usam-taxonomy_general' );	
				wp_enqueue_script( 'usam-brands', USAM_URL . '/admin/js/taxonomy/brands.js', array( 'jquery' ), $this->version  );
				wp_localize_script( 'usam-brands', 'USAM_Brands',					
					array(					
					'add_new_collection_nonce'  => usam_create_ajax_nonce( 'add_new_collection' ),// добавить колекцию		
					'delete_collection_nonce'  => usam_create_ajax_nonce( 'delete_collection' ),// Удалить фильтр
					'images_url'     => USAM_CORE_IMAGES_URL,						
					)
				);				
			break;			
			case 'edit-usam-category_sale':
				wp_enqueue_style( 'usam-admin-taxonomies');
				wp_enqueue_script( 'usam-selecting_locations' );
				$this->set_thumbnails( );	
			break;	
			case 'dashboard_page_usam-sales-logs':
				// jQuery
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script( 'jquery-ui-draggable' );
				wp_enqueue_script( 'jquery-ui-droppable' );
				wp_enqueue_script( 'jquery-ui-sortable' );
				// Metaboxes
				wp_enqueue_script( 'common' );
				wp_enqueue_script( 'wp-lists' );
				wp_enqueue_script( 'postbox' );
			break;
			case 'usam-product-variations-iframe':			// Фрейм вариаций на странице редактирования товара
				wp_enqueue_media();
				
				$product_id = !empty($_REQUEST['product_id'])?absint($_REQUEST['product_id']):0;
				wp_enqueue_style( 'usam-product-metabox', USAM_URL . '/admin/css/product-metabox.css', false, $this->version, 'all' );	
				wp_enqueue_script( 'usam-product-variations', USAM_URL . '/admin/js/product-variations.js', array( 'jquery', 'media-views' ), $this->version, true );
				wp_localize_script( 'usam-product-variations', 'USAM_Product_Variations', array(
					'product_id'              => $product_id,
					'add_variation_set_nonce' => usam_create_ajax_nonce( 'add_variation_set' ),
				) );					
				wp_enqueue_script( 'usam-edit-product', USAM_URL . '/admin/js/edit_product.js', array( 'jquery' ), $this->version );
				wp_localize_script( 'usam-edit-product', 'USAM_Edit_Product', array(
					'product_id' => $product_id,
					'save_variant_set_table_product_nonce' => usam_create_ajax_nonce( 'save_variant_set_table_product' ),							
					'message_no_link' => __( 'Ссылка не указана', 'usam' ),						
				) );				
			break;													
			case 'dashboard_page_usam-upgrades':	
			case 'dashboard_page_usam-update':	
				wp_enqueue_style( 'usam-admin');
			break;			
			case 'partners_sets':					
				$this->sort_fields( 'partners_sets' );
			break;			
			case 'media-upload-popup':					
				if ( !empty($_REQUEST['post_id']) )
				{
					$post = get_post( absint($_REQUEST['post_id']) );
					if ( $post->post_type == 'usam-product' && $post->post_parent ) 
					{ // Установка фотографий для вариаций 
						wp_enqueue_script( 'set-post-thumbnail' );							
					}
				}
			break;					
			case 'shop_settings_mailboxes':			
				$this->sort_fields( 'mailboxes' );
			break;	
			case 'seo_positions':			
				wp_enqueue_script( 'd3' );	
			break;			
		}			
	}	
	
	// Установка миниатюры
	private function set_thumbnails( ) 
	{			
		wp_enqueue_script( 'usam-set_thumbnails');					
		wp_enqueue_media();
		
		wp_localize_script( 'usam-set_thumbnails', 'USAM_Thumbnail',					
			array(				
			'no_image'  => USAM_CORE_IMAGES_URL . '/no-image-uploaded.gif'		
			)
		);			
	}
	
	private function sort_fields( $action ) 
	{		
		wp_enqueue_script( 'usam-sortable-table' );	
		
		wp_enqueue_script( 'jquery-ui-draggable' );	
		wp_enqueue_script( 'jquery-ui-droppable' );	
		wp_enqueue_script( 'usam-sort_fields' );
		wp_localize_script( 'usam-sort_fields', 'USAM_Sort', array(		
			'sort_fields_nonce'                 => usam_create_ajax_nonce( "update_{$action}_sort_fields" ),
			'page'                              => $action,
			'ajax_navigate_confirm_dialog'      => __( 'Внесенные изменения будут потеряны, если вы уйдете с этой страницы.', 'usam' ) . "\n\n" . __( 'Нажмите OK, чтобы отменить изменения, или Отмена, чтобы оставаться на этой странице.', 'usam' )
		) );
	}
}
new USAM_Admin_Assets();
?>