<?php
if ( $this->search )
	printf( '<h3 class="search_title">' . __( 'Результаты поиска &#8220;%s&#8221;' ) . '</h3>', esc_html( $this->search ) );
	
$this->prepare_items();	
$address_book = !empty($_GET['address_book']) ? sanitize_title($_GET['address_book']) : 'contacts';
?>			
<script type='text/javascript'>		
window.onload = function() {
    parent.iframeLoaded();
}		
</script>	
<div class='usam_tab_table'>			
	<form method='GET' action='<?php echo admin_url('admin.php'); ?>' id='usam-tab_form'>
		<input type='hidden' value='display_address_book' name='usam_admin_action' />	
		<div class="usam_select_address_book">			
			<span class="usam_field-label"><label for="address_book"><?php _e('Адресная книга', 'usam'); ?>:</label></span>
			<span class="usam_field-val usam_field-title-inner">
				<select name="address_book" id="address_book" onChange="this.form.submit()">				
					<option value='email' <?php selected('email', $address_book); ?>><?php _e('Контакты из почты', 'usam'); ?></option>	
					<option value='contacts' <?php selected('contacts', $address_book); ?>><?php _e('Контакты', 'usam'); ?></option>			
					<option value='companies' <?php selected('companies', $address_book); ?>><?php _e('Компании', 'usam'); ?></option>						
				</select>	
			</span>		
		</div>
		<?php wp_nonce_field('display_address_book_nonce', 'nonce' );	?>	
		<?php
		if ( $this->is_search_box_enabled() )						
			$this->search_box( $this->search_title, 'search_id' );
		?>					
		<div class='usam_list_table_wrapper'>
			<?php $this->display(); ?>
		</div>				
	</form>
</div>