<?php
require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );
class USAM_Companies_Table extends USAM_List_Table
{	
	private $company_groups = array();
	
	protected $order = 'DESC';
	function __construct( $args = array() )
	{			
		parent::__construct( $args );
		
		$option = get_option('usam_crm_company_group');
		$company_groups = maybe_unserialize( $option );	
		if (!empty($company_groups) )
		{
			foreach( $company_groups as $item )
				$this->company_groups[$item['id']] = $item['name'];
		}		
    }	
	
	protected function get_table_classes() {
		return array( 'widefat', 'striped', $this->_args['plural'] );
	}
	
	public function display_table()
    {
		require_once( USAM_FILE_PATH .'/admin/includes/address_book/display_table.php' );
	}	
			
	function column_company( $item ) 
    {
		echo '<span id="customer_name">'.$item->name.'</span>';
	}	

	function column_group( $item ) 
    {
		if ( isset($this->company_groups[$item->group]) )
			echo $this->company_groups[$item->group];	
	}	
	
	function column_type( $item ) 
    {
		$types = array( 
			'customer' => __('Клиент','usam'), 
			'prospect' => __('Будующий клиент','usam'), 
			'partner' => __('Партнер','usam'),
			'reseller' => __('Реселлер','usam'),
			'competitor' => __('Конкурент','usam'),
			'investor' => __('Инвестор','usam'),
			'integrator' => __('Интегратор','usam'),
			'press' => __('СМИ','usam'),
			'own' => __('Своя','usam'),			
			'other' => __('Другое','usam'),		
		);
		echo $types[$item->type];		
	}
	
	function column_communication( $item ) 
	{
		$communication = usam_get_company_means_communication( $item->id );			
		if ( isset($communication['email']) )
		{			
			foreach( $communication['email'] as $data)
				echo "<div class = 'email'>".$data['value']."</div>";
		}
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "company-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
	 
	function get_sortable_columns()
	{
		$sortable = array(
			'company' => array('name', false),			
			'status'  => array('status', false),					
			'date'  => array('date_insert', false),	
			'type'  => array('type', false),		
			'group'  => array('group', false),			
			);
		return $sortable;
	}
	
	function get_columns()
	{		
        $columns = array(           		
			'company'        => __( 'Компания', 'usam' ),	
			'communication'  => __( 'Адрес электронной почты', 'usam' ),			
			'type'           => __( 'Тип компании', 'usam' ),			
			'group'          => __( 'Группа', 'usam' ),				
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		global $user_ID;			
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'cache_case' => true,		
		);	
		$query['open'] = 1;			
		if ( $this->search != '' )
		{
			$query['search'] = $this->search;					
		}
		else
		{		
			$query = array_merge ($query, $this->get_date_for_query() );		
			
			if ( !empty($this->records) )
				$query['include'] = $this->records;
			
			if ( !empty($_REQUEST['industry']) )
				$query['industry'] = sanitize_title($_REQUEST['industry']);	
			
			if ( !empty($_REQUEST['type']) )
				$query['type'] = sanitize_title($_REQUEST['type']);	
			
			if ( !empty($_REQUEST['group']) )
				$query['group'] = sanitize_title($_REQUEST['group']);	
		} 
		$_contacts = new USAM_Companies_Query( $query );
		$this->items = $_contacts->get_results();		
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>