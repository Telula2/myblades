<?php
require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );
class USAM_Contacts_Table extends USAM_List_Table
{	
	protected $order = 'DESC';	
		
	public function extra_tablenav( $which ) 
	{
		if ( 'top' == $which && $this->filter_box ) 
		{
			
		}			
	}	

	protected function get_table_classes() {
		return array( 'widefat', 'striped', $this->_args['plural'] );
	}	
	
	public function display_table()
    {
		require_once( USAM_FILE_PATH .'/admin/includes/address_book/display_table.php' );
	}	
	
	function column_contact( $item ) 
    {
		$customer_name = $item->lastname;
		if ( $item->firstname != '' )
			$customer_name .= ' '.$item->firstname;
		
		echo '<span id="customer_name">'.$customer_name.'</span>';
	}
	
	function column_contact_source( $item ) 
    {		
		echo usam_get_name_contact_source( $item->contact_source );
	}
	
	public function single_row( $item ) 
	{		
		echo '<tr id = "contact-'.$item->id.'" data-customer_id = "'.$item->id.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
		
	function column_communication( $item ) 
	{
		$communication = usam_get_contact_means_communication( $item->id );		
		
		if ( isset($communication['email']) )
		{			
			foreach( $communication['email'] as $data)
				echo "<div class = 'email'>".$data['value']."</div>";
		}
	}	
	
	function get_columns()
	{		
        $columns = array(           					
			'contact'        => __( 'Контакт', 'usam' ),		
			'communication'  => __( 'Адрес электронной почты', 'usam' ),				
			'contact_source' => __( 'Источник', 'usam' ),						
        );		
        return $columns;
    }
	
	
	function prepare_items() 
	{		
		global $user_ID;
		require_once( USAM_FILE_PATH . '/includes/crm/contacts_query.class.php' );		
		
		$query = array( 
			'fields' => 'all',	
			'order' => $this->order, 
			'orderby' => $this->orderby, 		
			'paged' => $this->get_pagenum(),	
			'number' => $this->per_page,	
			'search' => $this->search,
			'cache_case' => true,			
		);								
		$query['manager_id'] = array( 0, $user_ID);			
		if ( $this->search == '' )
		{		
			if ( !empty($this->records) )
				$query['include'] = $this->records;			
		}
		$_contacts = new USAM_Contacts_Query( $query );
		$this->items = $_contacts->get_results();			
		$this->total_items = $_contacts->get_total();
		$this->set_pagination_args( array( 'total_items' => $this->total_items, 'per_page' => $this->per_page ) );
	}
}
?>