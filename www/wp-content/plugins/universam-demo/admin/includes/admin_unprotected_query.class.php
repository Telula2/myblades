<?php
/**
 *  ========================== Обработка запросов ================================================
 */ 	
class USAM_Admin_Unprotected_Query extends USAM_Callback
{				
	protected $init_parameter = 'unprotected_query';
	protected $verify_nonce = false;
	
	public function __construct() 
	{		
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) 
			add_action( 'wp_ajax_unprotected_query', array($this, 'handler') );	
		else
			add_action( 'init', array($this, 'handler') );		
	}
	
	function controller_tinymce_button_table() 
	{	
		require_once( USAM_FILE_PATH . '/admin/includes/tinymce/window-button-table.php' );			
		exit;
	}
	
	function controller_tinymce_scortcode() 
	{	
		require_once( USAM_FILE_PATH . '/admin/includes/tinymce/window-scortcode.php' );			
		exit;
	}
	
	function controller_vk_token() 
	{	
		$id = '';
		if ( !empty($_REQUEST['state']) )
		{
			$id = absint($_REQUEST['state']);			
		}				
		else
		{
			$url = admin_url('admin.php?page=shop_settings&tab=vk_users_profile');	
			wp_redirect( $url );
			exit;
		}		
		if ( !empty($_REQUEST['code']) )
		{ 			
			$code = sanitize_text_field($_REQUEST['code']);			
			
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();
			$result = $vkontakte->get_access_token2( $code, admin_url('admin.php?unprotected_query=vk_token') );	
			if ( !empty($result['access_token']) )
			{
				$data = usam_get_data( $id, 'usam_vk_profile' );
				$data['access_token'] = $result['access_token'];					
				usam_edit_data( $data, $id, 'usam_vk_profile' );
			}
		}	
		$url = admin_url('admin.php?page=shop_settings&tab=vk_users_profiles&action=edit&id='.$id);	
		wp_redirect( $url );
		exit;
	}
	
	function controller_vk_token_group() 
	{	
		$id = '';
		if ( !empty($_REQUEST['state']) )
		{
			$id = absint($_REQUEST['state']);			
		}				
		if ( !empty($_REQUEST['code']) )
		{ 			
			$code = sanitize_text_field($_REQUEST['code']);			
			
			require_once( USAM_FILE_PATH . '/includes/social_networks/vkontakte_api.class.php' );
			$vkontakte = new USAM_VKontakte_API();
			$result = $vkontakte->get_access_token( $code, admin_url('admin.php?unprotected_query=vk_token_group') );	
			if ( !empty($result['access_token']) )
			{
				
				
			}
		}	
		$url = admin_url('admin.php?page=shop_settings&tab=social_networks');	
		wp_redirect( $url );
		exit;
	}
	
	function controller_instagram_token() 
	{			
		$url = admin_url('admin.php?service_api=instagram');		
		if ( !empty($_REQUEST['code']) )
		{ 			
			$code = sanitize_text_field($_REQUEST['code']);			
			require_once( USAM_FILE_PATH . '/includes/social_networks/instagram_api.class.php' );
			$vkontakte = new USAM_Instagram_API();			
			$token = $vkontakte->get_access_token( $code );	
			
			$instagram_api = get_option('usam_instagram_api' );	
			$instagram_api['token'] = $token;				
			update_option('usam_instagram_api', $instagram_api );
		}				
		wp_redirect( $url );
		exit;
	}	
}
new USAM_Admin_Unprotected_Query();
?>