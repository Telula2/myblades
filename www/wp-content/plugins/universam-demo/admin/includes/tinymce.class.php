<?php
class USAM_TinyMCE
{
	public function __construct( )
	{			
		add_action('admin_init', array($this, 'add_buttons') ); //Процесс инициализации кнопок
		add_filter('tiny_mce_version', array($this, 'change_tinymce_version') );
		//add_action( 'media_buttons', 'usam_media_buttons' );		
	}	
	
	//кнопки в редакторе
	function media_buttons($editor_id = 'content')
	{
		$post = get_post();
		if ( ! $post && ! empty( $GLOBALS['post_ID'] ) )
			$post = $GLOBALS['post_ID'];

		wp_enqueue_media( array('post' => $post) );

		$img = '<span class="wp-media-buttons-icon"></span> ';
		echo '<a href="#" id="insert-media-button" class="button insert-media add_media" data-editor="'.esc_attr( $editor_id ).'" title="'.esc_attr__( 'Add Media' ).'">'.$img.__( 'Add Media' ).'</a>';
		$legacy_filter = apply_filters('media_buttons_context', ''); // устарела
		if ( $legacy_filter ) 
		{
			if ( 0 === stripos( trim( $legacy_filter ), '</a>' ) )
				$legacy_filter .= '</a>';
			echo $legacy_filter;
		}
	}	
	
	/**
	 * Кнопки TinyMCE V3
	 */
	function add_buttons() 
	{
		// Добавить только в режиме визуального редактора			
		if ( user_can_richedit() ) 
		{
			add_filter("mce_external_plugins", array( $this, "add_tinymce_plugin"), 5);
			add_filter('mce_buttons', array( $this,'register_button'), 5);	
		}
	}
	
	// used to insert button in wordpress 2.5x editor
	function register_button($buttons) 
	{
		array_push($buttons, "separator", "usam_scortcode");
		array_push($buttons, "separator", "product_table");//регистрация кнопки для создания таблицы	
		return $buttons;
	}

	// Load the TinyMCE plugin
	function add_tinymce_plugin( $plugin_array ) 
	{
		$plugin_array['usam_scortcode'] =              USAM_URL . '/admin/js/tinymce3/scortcode/button.js';
		$plugin_array['productspage_image'] =          USAM_URL . '/admin/js/tinymce3/scortcode/scortcode.js';
		$plugin_array['transactionresultpage_image'] = USAM_URL . '/admin/js/tinymce3/scortcode/scortcode.js';
		$plugin_array['checkoutpage_image'] =          USAM_URL . '/admin/js/tinymce3/scortcode/scortcode.js';
		$plugin_array['userlogpage_image'] =           USAM_URL . '/admin/js/tinymce3/scortcode/scortcode.js';
		$plugin_array['usam_search_image'] =           USAM_URL . '/admin/js/tinymce3/scortcode/scortcode.js';
	// 	$plugin_array['product_table']     =           USAM_URL . '/admin/js/tinymce3/table/button_table.js';	//добавление кнопки для создания таблицы
		return $plugin_array;
	}
		
	// Изменить версию, когда TinyMCE плагины изменилось.
	function change_tinymce_version($version) 
	{
		return ++$version;
	}
}
$button = new USAM_TinyMCE();
?>