<div class="inline-svg full-width">
	<h2><?php printf(	__( 'CRM 2.0 %s','usam' ),'&#x1F389'	); ?></h2>
	<p><?php _e( 'Переработанный интерфейс и добавленны новые возможности в CRM. Новые документы и улучшенный интерфейс почтового клиента','usam' ); ?></p>					
</div>	
<div class="floating-header-section">
	<div class="section-header">
		<h2><?php _e( 'CRM 2.0' ); ?></h2>
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Интерфейс' ); ?></h3>
			<p><?php _e( 'Переработан и улучшен интерфейс форм относящихся к CRM. Теперь все типы событий в одном окне.', 'usam' ); ?></p>
		</div>									
	</div>	
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Документы' ); ?></h3>
			<p><?php _e( 'Появилися новый документ - договор. Можно отправлять договор на согласование и вести реестр договоров.', 'usam' ); ?></p>
		</div>									
	</div>
</div>
<div class="floating-header-section">
	<div class="section-header">
		<h2><?php _e( 'Социальные сети' ); ?></h2>
	</div>	
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Инстаграмм' ); ?></h3>
			<p><?php _e( 'Подключен Инстаграмм, функционал будет наращиваться в следующих версиях.', 'usam' ); ?></p>
		</div>									
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Правила публикации вКонтакте' ); ?></h3>
			<p><?php _e( 'Появились правила автоматических публикации товаров вКонтакте. Множество параметров, по которым можно точно задать, какие товары выбирать и когда публиковать.', 'usam' ); ?></p>
		</div>									
	</div>	
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Интерфейс публикации вКонтакте' ); ?></h3>
			<p><?php _e( 'Улучшен интерфейс для публикации товаров вКонтакте. Возможность выбирать категории и подборки вовремя настройки публикации.', 'usam' ); ?></p>
		</div>									
	</div>	
</div>
<div class="floating-header-section">
	<div class="section-header">
		<h2><?php _e( 'Электронная почта' ); ?></h2>
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Файлы' ); ?></h3>
			<p><?php _e( 'Теперь можно прикреплять файлы.', 'usam' ); ?></p>
		</div>									
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Папки' ); ?></h3>
			<p><?php _e( 'Теперь можно добавлять папки для писем, как в обычной почтовой программе.', 'usam' ); ?></p>
		</div>									
	</div>
		<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Фильтры писем' ); ?></h3>
			<p><?php _e( 'Теперь можно создавать фильтры писем, как в ручном режими так и по шаблону.', 'usam' ); ?></p>
		</div>									
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Адресная книга' ); ?></h3>
			<p><?php _e( 'Добавлена возможность получить электронный адрес из адресной книги.', 'usam' ); ?></p>
		</div>									
	</div>			
</div>
<div class="floating-header-section">
	<div class="section-header">
		<h2><?php _e( 'Товары' ); ?></h2>
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Массовое обновление' ); ?></h3>
			<p><?php _e( 'Мы сделали инструмент, с помощью которого легко можно обновить свойства, характеристики, параметры поиска и фильтры у сотни товаров. Это сэкономит массу времени!', 'usam' ); ?></p>
		</div>									
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Управление ценами' ); ?></h3>
			<p><?php _e( 'Добавлена возможность массово обновлять цены.', 'usam' ); ?></p>
		</div>									
	</div>	
</div>
<div class="floating-header-section">
	<div class="section-header">
		<h2><?php _e( 'Рассылка' ); ?></h2>
	</div>	
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Персонализация писем' ); ?></h3>
			<p><?php _e( 'Теперь можно персонализировать каждое письмо.', 'usam' ); ?></p>
		</div>									
	</div>
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Триггеры' ); ?></h3>
			<p><?php _e( 'Доступен новый триггер "При добавлении в рассылку". Когда вы добавляете клиента в рассылку он получает письмо, например, вы хотите ему отправить один раз коммерческое предложение.', 'usam' ); ?></p>
		</div>									
	</div>	
	<div class="section-content">
		<div class="section-item">							
			<h3><?php _e( 'Шаблон' ); ?></h3>
			<p><?php _e( 'Добавлено простой шаблон, из которого можно сделать свой и копировать его, для создания новой рассылки.', 'usam' ); ?></p>
		</div>									
	</div>		
</div>
<div class="inline-svg full-width">
	<h2><?php _e( 'Что дальше?','usam' ); ?></h2>
	<p><?php _e( 'Мы ведем множество разработок, которые будут включены в следующие версии. Мы планируем внедрить в следующих версиях:','usam' ); ?></p>	
	<ul>
		<li><?php _e( 'что-то вроде искусственного интеллекта для разбора почты и общения с покупателями в чате.','usam' ); ?></li>
		<li><?php _e( 'определение посетителя сайта.','usam' ); ?></li>						
	</ul>
	<h2><?php _e( 'А что нужно имено Вам?','usam' ); ?></h2>
	<p><?php _e( 'Напишите нам что вы хотите увидеть в следующий версиях и мы постараемся это сделать.','usam' ); ?></p>	
</div>	