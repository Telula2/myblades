<?php
require_once( USAM_FILE_PATH .'/admin/includes/usam_list_table.class.php' );		

class USAM_Product_List_Table extends USAM_List_Table 
{	
	protected $orderby = 'ID';
	protected $order   = 'desc'; 
	public  $post_status = array('private', 'draft', 'pending', 'publish', 'future' );

    function __construct( $args )
	{	
        parent::__construct( $args );		
		$columns_products = new USAM_Manage_Columns_Products( );		
    }	
		
	public function get_views() 
	{	
		global $wp_post_statuses;
		
		$status = implode( "','", $this->post_status );
		$current_status = isset($_GET['post_status'])?$_GET['post_status']:'all';
		
		$num_posts = (array)wp_count_posts( 'usam-product', 'readable' );		
		$total_count = 0;

		if ( ! empty( $num_posts ) )
		{		
			$total_count = array_sum( $num_posts );
			// Исключить системные статусы
			foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state ) 	
			{
				$total_count -= $num_posts[$state];		
				unset($num_posts[$state]);						
			}
		}
		$all_text = sprintf(__( 'Всего <span class="count">(%s)</span>', 'usam' ),	number_format_i18n( $total_count ) );
		$all_href = remove_query_arg( array('post_status', 'paged', 'action', 'action2', 'm', 'deleted',	'updated', 'paged',	's', 'orderby','order') );
		$all_class = ( $current_status == 'all' && empty( $_REQUEST['m'] ) && empty( $_REQUEST['s'] ) ) ? 'class="current"' : '';
		$views = array(	'all' => sprintf('<a href="%s" %s>%s</a>', esc_url( $all_href ), $all_class, $all_text ), );

		foreach ( $num_posts as $status => $count )
		{
			if ( ! isset($wp_post_statuses[$status]) || $count == 0 )
				continue;			
			$text = $wp_post_statuses[$status]->label." <span class='count'>($count)</span>";
			$href = add_query_arg( 'post_status', $status );
			$href = remove_query_arg( array( 'deleted',	'updated', 'action', 'action2',	'm', 'paged', 's','orderby','order'), $href );
			$class = ( $current_status == $status ) ? 'class="current"' : '';
			$views[$status] = sprintf( '<a href="%s" %s>%s</a>', esc_url( $href ), $class, $text );
		}
		return $views;
	}
	
	function column_date( $item ) 
	{
	   echo date( "d.m.Y H:i:s", strtotime($item->post_date) );	
	}
	
   function column_default( $item, $column_name ) 
   {
		switch( $column_name ) 
		{
			case 'product_title':
				$edit_post_link = get_edit_post_link( $item->ID );
				$title = "<a href='".$edit_post_link."'>".$item->post_title."</a>";
				$actions = array(			
					'edit'    => '<a href="'.$edit_post_link.'">'.__( 'Изменить', 'usam' ).'</a>',
					'view'    => '<a href="'.$item->guid.'">'.__( 'Просмотреть', 'usam' ).'</a>',
				);
				return sprintf('%1$s %2$s', $title, $this->row_actions($actions) );	
			break;
			case 'image': 
			case 'stock':
			case 'reserve':
			case 'price':	
			case 'old_price':	
			case 'views':			
			case 'sku':
			case 'cats':		
				return do_action( "manage_usam-product_posts_custom_column", $column_name, $item->ID );
			break;			
			case 'post_content':
			case 'post_excerpt':
				echo "<div class = 'overflow_y'>".$item->$column_name."</div>";
			break;
			default:
				echo $item->$column_name;
			break;
		}
    }   
		
	public function has_items() 
	{
		return have_posts();
	}
		
	public function display_rows( $posts = array(), $level = 0 ) 
	{
		global $wp_query;

		if ( empty( $posts ) )
			$posts = $wp_query->posts;	
		if ( $this->hierarchical_display ) 
			$this->_display_rows_hierarchical( $posts, $this->get_pagenum(), $this->per_page );
		else 
			$this->_display_rows( $posts, $level );
	}


	private function _display_rows( $posts, $level = 0 ) 
	{
		$post_ids = array();

		foreach ( $posts as $a_post )
			$post_ids[] = $a_post->ID;
		
		foreach ( $posts as $post )
			$this->single_row( $post, $level );
	}
	
	protected function column_cb( $item ) 
	{				
		$checked = in_array($item->ID, $this->records )?"checked='checked'":"";		
		echo "<input id='checkbox-".$item->ID."' type='checkbox' name='cb[]' value='".$item->ID."' ".$checked."/>";
    }		
	
	function query_vars( $query_vars )
	{
		return $query_vars;
	}
	
	function prepare_items() 
	{			
		global $wp_query, $per_page;				
	
		$offset = ($this->get_pagenum() - 1) * $this->per_page;
		$query_vars = array(
			'post_status'    => 'publish',
			'post_type'      => 'usam-product',			
			'posts_per_page' => $this->per_page,
			'offset'         => $offset,		
			'post_parent'    => 0				
		);					
		if ( !empty($this->records) )
			$query_vars['post__in'] = $this->records;

		$query_vars = $this->query_vars( $query_vars );
		$wp_query = new WP_Query( $query_vars );
		
		$this->_column_headers = $this->get_column_info();

		$total_items = $this->hierarchical_display ? $wp_query->post_count : $wp_query->found_posts;	

		if ( $this->hierarchical_display )
			$total_pages = ceil( $total_items / $this->per_page );
		else
			$total_pages = $wp_query->max_num_pages;			

		$this->is_trash = isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] == 'trash';
	
		$this->set_pagination_args( array('total_items' => $total_items, 'total_pages' => $total_pages,	'per_page' => $this->per_page) );
	}	
}