<?php
function usam_get_manager_name( $user_id )
{
	$user = get_user_by('id', $user_id );
	return isset($user->display_name)?"$user->display_name":''; 
}

function usam_terms_manage_products() 
{
	$filter_manage = new USAM_Product_Filter_Manage();			 
	$filter_manage->terms_products_search( array( 'usam-category', 'usam-brands', 'usam-category_sale' ) );
}


function usam_return_details( $dataset ) 
{
	global $wpdb;

	$count_sql = null;
	
	$post_statuses = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash' );	
	switch( $dataset ) 
	{
		case 'products':
			$post_type = 'usam-product';
			$count = wp_count_posts( $post_type );
		break;
		case 'variations':
			$post_type = 'usam-variation';
			$count = wp_count_posts( $post_type );
		break;
		case 'images':				
			$status = implode("','", $post_statuses );				
			$count = $wpdb->get_var("SELECT COUNT(*) FROM `$wpdb->posts` AS p 
			INNER JOIN `$wpdb->posts` AS p2 ON ( p2.post_parent = p.ID ) 
			WHERE p.post_status IN ('$status') AND p.post_type = 'usam-product' AND p2.post_type = 'attachment' AND p2.post_status = 'inherit'");	
		break;
		case 'files':
			$post_type = 'usam-product-file';
			$count = wp_count_posts( $post_type );
		break;
		case 'categories':
			$term_taxonomy = 'usam-category';
			$count = wp_count_terms( $term_taxonomy );
		break;
		case 'tags':
			$term_taxonomy = 'product_tag';
			$count = wp_count_terms( $term_taxonomy );
		break;
		case 'orders':
			$query = array( 'fields' => array('count') );	
			$orders = new USAM_Orders_Query( $query );	
			$results = $orders->get_results();	
			$count = $results[0]->count;
		break;
		case 'coupons':
			$count_sql = "SELECT COUNT(`id`) FROM `" . USAM_TABLE_COUPON_CODES . "`";
		break;		
		case 'preview-files':
			$post_type = 'usam-preview-file';
			$count = wp_count_posts( $post_type );
		break;
		
		// WordPress

		case 'posts':
			$post_type = 'post';
			$count = wp_count_posts( $post_type );
		break;
		case 'post_categories':
			$term_taxonomy = 'category';
			$count = wp_count_terms( $term_taxonomy );
		break;
		case 'post_tags':
			$term_taxonomy = 'post_tag';
			$count = wp_count_terms( $term_taxonomy );
		break;
		case 'links':
			$count_sql = "SELECT COUNT(`link_id`) FROM `" . $wpdb->prefix . "links`";
		break;
		case 'comments':
			$count = wp_count_comments();
		break;
	}
	if( isset( $count ) || $count_sql ) 
	{
		if( isset( $count ) ) 
		{
			if( is_object( $count ) ) 
			{
				$count_object = $count;
				$count = 0;
				foreach( $count_object as $key => $item )
					$count = $item + $count;
			}
			return $count;
		} 
		else 
		{
			$count = $wpdb->get_var( $count_sql );
		}
		return $count;
	} 
	else 
		return 0;		
}

function usam_get_class_form_display( $page, $screen = '', $action = 'edit' ) 
{
	if ( $screen == '' )
		$screen = $page;	
	
	$form_file = USAM_FILE_PATH ."/admin/menu-page/{$page}/form/{$action}-form-{$screen}.php";	
	if ( file_exists($form_file) )
	{			
		$name_class_form = "USAM_Form_{$screen}";		
		require_once( $form_file );				
		if ( class_exists( $name_class_form ))
		{				
			$class_form = new $name_class_form();
			return $class_form;		
		}
	}
	return false;
}

function usam_set_data_graph( $data_graph, $name_graph = '' ) 
{
	?>
	<script type='text/javascript'>	
	var usam_data_graph = <?php echo json_encode( $data_graph ); ?>;
	var usam_name_graph = '<?php echo $name_graph; ?>';	 
	</script>
	<?php
}

function usam_get_attachment_title( $attachment_title, $attachment_name ) 
{
	$sumbol = 12;								
	$ext = usam_get_extension( $attachment_name );
	$filename = basename( $attachment_title, ".".$ext );
	if ( iconv_strlen ($filename) > $sumbol )
	{
		$name = substr($filename, 0, $sumbol)."...".$ext;
	}
	else
		$name = $filename.".".$ext;
	
	return $name;
}

function usam_get_form_customer_case( $customer_id, $customer_type ) 
{	
	if ( $customer_type == 'company' )
		$affairs = usam_get_customer_case( $customer_id, 'company' );
	else
		$affairs = usam_get_customer_case( $customer_id, 'contact' );
	
	$text = '';
	if ( !empty($affairs) )
	{
		$i = 0;				
		foreach ( $affairs as $affair )
		{
			if ( $affair->status == 3 )
				continue;
			
			$i++;
			if ( $i > 1 )
				$text .= '<hr size="1" width="90%">';
			$text .= "<a href='".add_query_arg( array('page' => 'crm', 'tab' => 'events', 'action' => 'edit', 'id' => $affair->id), admin_url('admin.php') )."'>".usam_local_date($affair->start)." - $affair->title</a>";
		}			
	}
	else
		$text = __('Нет дел','usam');
	
	return '<div id="customer_case">'.$text.usam_get_loader( $customer_id ).'</div>';
}

function usam_get_display_table_lists( $list, $display_table )
{		
	$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );	
	
	$args = array( 'label' => __('Список','usam'), 'default' => 20, 'option' => $list.'_lists_table' );			
	add_screen_option( 'per_page', $args );		
	$args = array(
		'singular'  => $list.'_lists',    
		'plural'    => $list.'_lists', 
		'ajax'      => true,
		'screen'    => $list.'_lists_table',
	);					
	$filename = USAM_FILE_PATH ."/admin/includes/lists/{$list}_list_table.php"; 
	if ( file_exists($filename) ) 	
	{ 
		require_once( $filename );
		$table = "USAM_{$list}_Table";
		$list_table = new $table( $args );	
		$filename = USAM_FILE_PATH ."/admin/includes/lists/display_table/{$display_table}_display_table.php";
		if ( file_exists($filename) ) 	
		{ 
			ob_start();	
			require_once( $filename );	
			$out = ob_get_contents();
			ob_end_clean();	
			echo $out;
		}
	}
}
?>