<?php
/**
 *  ========================== Обработка запросов ================================================
 */ 	
new USAM_Admin_Init_Nonce();
class USAM_Admin_Init_Nonce extends USAM_Callback
{
	protected $init_parameter = 'usam_admin_action';
	public function __construct() 
	{	
		if ( isset($_REQUEST[$this->init_parameter]) )
			add_action( 'admin_init', array($this, 'handler') );		
	}	
		
	function controller_printed_form()
	{				
		if ( isset($_GET['form']) )
		{			
			$form = sanitize_title($_GET['form']);
			$form_html = usam_get_printing_forms( $form );
			if ( !empty($form_html) )
			{
				echo $form_html;
				exit;
			}
		}
	}
	
	function controller_printed_form_to_pdf()
	{				
		if ( isset($_GET['form']) )
		{			
			$id = absint($_REQUEST['id']);	
			$printed_form = sanitize_title($_GET['form']);			
			
			$html = usam_get_printing_forms_to_pdf( $printed_form, $id );
			
			header('Content-Type: application/pdf');
			echo $html;
			exit;
		}	
	}
	
	function controller_send_payment_invoice_email() 
	{		
		$id  = absint( $_REQUEST['item_id'] );		
		$notification = new USAM_Сustomer_Notification_Invoice( $id );
		$email_sent = $notification->send_mail();
		if ( ! $email_sent )
			return new WP_Error( 'usam_cannot_send_payment_invoice_email', __( "Не удалось отправить письмо со счетом. Пожалуйста, попробуйте еще раз.", 'usam' ) );			
		return $email_sent;
	}
	
	/**
	 * Получить бланк
	 */
	function controller_edit_blank()
	{		
		if ( isset($_GET['blank']))
		{			
			$blank = sanitize_title($_REQUEST['blank']);			
			usam_get_edit_printing_forms( $blank );
		}
	}	
	
	function controller_display_contacts_list( )
	{			
		set_current_screen();		
		$list = !empty($_GET['list'])?sanitize_title($_REQUEST['list']):'contacts';
		wp_iframe( 'usam_get_display_table_lists', $list, "contacts" );
		exit;
	}	
	
	function controller_display_address_book( )
	{	
		$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );
		set_current_screen();
		$args = array( 'label' => __('Записи','usam'), 'default' => 20, 'option' => 'address_book_list_table' );					
		add_screen_option( 'per_page', $args );		
		$args = array(
			'singular'  => 'address_book',    
			'plural'    => 'address_book', 
			'ajax'      => true,
			'screen'    => 'address_book_list_table',
		);			
		$screen = get_current_screen();	
		
		$list = !empty($_GET['address_book'])?sanitize_title($_REQUEST['address_book']):'contacts';				
		$filename = USAM_FILE_PATH ."/admin/includes/address_book/{$list}_list_table.php";
		if (file_exists($filename)) 
		{	
			require_once( $filename );
			$table = "USAM_{$list}_Table";
			$list_table = new $table( $args );	
			
			wp_iframe( array($list_table, 'display_table' ) );
		}
		exit;
	}
	
	function controller_display_mail_body()
	{	
		if ( !empty($_REQUEST['id'] ) )
		{					
			$id = absint( $_REQUEST['id'] );				
			$email = usam_get_email( $id );	

			usam_work_on_task( array( 'title' => __('Просмотр письма','usam')), array( 'object_type' => 'email', 'object_id' => $id ) );			
			
			echo '
			<head>
				<link rel="stylesheet" href="'. USAM_URL . '/admin/css/email-editor-style.css'.'" type="text/css">
			</head>';			
			$body = $email['body'];						
			$body = preg_replace_callback("/\n>+/u", 'usam_email_replace_body', $body );				
			echo $body;
		}
		else
		{
			echo "<span class='dashicons no_messages'></span>";
		}
		exit;
	}
	
	function controller_display_sms_body()
	{	
		if ( !empty($_REQUEST['id'] ) )
		{ 
			$id = absint( $_REQUEST['id'] );				
			$sms = usam_get_sms( $id );		
			usam_work_on_task( array( 'title' => __('Просмотр СМС сообщения','usam')), array( 'object_type' => 'sms', 'object_id' => $id ) );	
			
			echo $sms['message'];
		}
		else
		{
			echo "<span class='dashicons no_messages'></span>";
		}
		exit;
	}
	
	function controller_send_sms()
	{	
		if ( !empty($_REQUEST['message']) && !empty($_REQUEST['phone']) )
		{			
			$message = nl2br( sanitize_textarea_field($_REQUEST['message']) );
			$phone = $_REQUEST['phone'];	
			$object_type = '';
			$object_id = 0;
			if ( !empty($_REQUEST['object_type']) && !empty($_REQUEST['object_id']) )
			{
				$object_type = sanitize_title($_REQUEST['object_type']);
				$object_id = absint($_REQUEST['object_id']);				
			}						
			$result = usam_send_sms( $phone, $message, $object_id, $object_type );
			if ( $result )				
				$this->sendback = add_query_arg( 'send_sms', 1, $this->sendback );	
			else
				$this->sendback = add_query_arg( 'send_sms', 0, $this->sendback ); 
		}	
	}	
	
	function controller_send_email()
	{		
		global $user_ID;
		if ( !empty($_REQUEST['to']) && !empty($_REQUEST['from_email']) && is_numeric($_REQUEST['from_email']) )
		{			
			$message = !empty($_REQUEST['message'])?stripcslashes($_REQUEST['message']):'';//nl2br
			$subject = !empty($_REQUEST['subject'])?sanitize_text_field($_REQUEST['subject']):get_bloginfo('name');				
			$to_email = sanitize_email($_REQUEST['to']);
			$from_email = absint($_REQUEST['from_email']);		
			
			$message = str_replace( array( "\n\r" ), '<br>', $message );
			
			$attachments = array();
			if ( !empty($_REQUEST['fileupload']) )	
			{ // Загруженные пользователем файлы				
				$directory = USAM_FILE_DIR.$user_ID.'/';
				foreach ($_REQUEST['fileupload'] as $attachment ) 	
				{
					$attachments[] = array( 'file_path' => $directory.stripcslashes($attachment), 'delete' => 1 );						
				}
			} 		
			if ( !empty($_REQUEST['attachments']) )	
			{   // Документы
				foreach ($_REQUEST['attachments'] as $attachment ) 				
					$attachments[] = array( 'file_path' => stripcslashes($attachment), 'delete' => 0 );
			}			
			if ( $message != '' )
			{
				$style = new USAM_Mail_Styling();
				$message = $style->process_plaintext_args( $message );
			} 
			$insert_email = array( 'body' => $message, 'subject' => $subject, 'to_email' => $to_email, 'mailbox_id' => $from_email );		
			
			if ( !empty($_REQUEST['object_type']) && !empty($_REQUEST['object_id']) )
			{
				$insert_email['object_type'] = sanitize_title($_REQUEST['object_type']);
				$insert_email['object_id'] = absint($_REQUEST['object_id']);
			}	
			$_email = new USAM_Email( $insert_email );
			$_email->save();	
			$_email->set_attachments( $attachments );
			$result = $_email->send_mail();					
	
			if ( $result )
			{				
				$affair = array( 'title' => __('Отправлено письмо','usam'), 'type' => 'email' );						
				$objects = array( array('object_type' => 'email', 'object_id' => $_email->get('id') ) );
				
				if ( !empty($_REQUEST['object_type']) && !empty($_REQUEST['object_id']) )
					$objects[] = array('object_type' => sanitize_title($_REQUEST['object_type']), 'object_id' => absint($_REQUEST['object_id']) ); 
				
				if ( !empty($_REQUEST['customer_type']) && !empty($_REQUEST['customer_id']) )
					$objects[] = array('object_type' => sanitize_title($_REQUEST['customer_type']), 'object_id' => absint($_REQUEST['customer_id']) ); 
				
				$event_id = usam_set_affair( $affair, $objects );
				
				if ( !empty($_REQUEST['do_action_send_email']) )
				{
					$do_action_send_email = sanitize_title($_REQUEST['do_action_send_email']);			
					do_action( 'usam_send_email_'.$do_action_send_email, $event_id );
				}
				$this->sendback = add_query_arg( 'send_email', 1, $this->sendback );	
			}
			else
				$this->sendback = add_query_arg( 'send_email', 0, $this->sendback ); 			
		}
	}	
/**
 *  ========================== Товар ================================================
*/ 
	protected function controller_delete_variation_set() 
	{		
		if ( is_numeric( $_GET['deleteid'] ) ) 
		{
			$variation_id = absint( $_GET['deleteid'] );
			$variation_set = get_term( $variation_id, 'usam-variation', ARRAY_A );
			$variations = get_terms( 'usam-variation', array( 'hide_empty' => 0, 'parent' => $variation_id ) );

			foreach ( (array)$variations as $variation ) 
				$return_value = wp_delete_term( $variation->term_id, 'usam-variation' );
			
			if ( !empty( $variation_set ) )
				$return_value = wp_delete_term( $variation_set['term_id'], 'usam-variation' );		
			$deleted = 1;
		}
		if ( isset( $deleted ) )
			$this->sendback = add_query_arg( 'deleted', $deleted, $this->sendback );	
		$this->sendback = remove_query_arg( array( 'deleteid', 'variation_id' ), $this->sendback );		
	}
	
	/**
	* Перенести товар в архив
	*/
	protected function controller_set_product_status_archive( ) 
	{		
		if ( isset($_GET['id']) )
		{
			$product['ID'] = absint($_GET['id']);
			$product['post_status'] = 'archive';

			wp_update_post( $product );
		}
	}	

	protected function controller_product_files_existing() 
	{
		$product_id = absint( $_GET["product_id"] );	
		
		$file_list = usam_get_product_files( );

		$args = array( 'post_parent' => $product_id );
		$attached_files = usam_get_product_files( $args );

		$attached_files_by_file = array();
		foreach ( $attached_files as $key => $attached_file ) {
			$attached_files_by_file[$attached_file->ID] = & $attached_files[$key];
		}
		$output = "<span class='admin_product_notes select_product_note '>" . esc_html__( 'Выберите загружаемый файл для этого товара:', 'usam' ) . "</span><br>";
		$output .= "<form method='post' class='product_upload_file' id='product_upload_file'>";
		$output .= "<div class='ui-widget-content multiple-select select_product_file'>";
		$num = 0;		
		foreach ( (array)$file_list as $file ) 
		{
			$num++;
			$checked_curr_file = "";
			if ( isset( $attached_files_by_file[$file->ID] ) ) 
			{
				$checked_curr_file = "checked='checked'";
			}
			$output .= "<p " . ((($num % 2) > 0) ? '' : "class='alt'") . " id='select_product_file_row_$num'>\n";
			$output .= "  <input type='checkbox' name='select_product_file[]' value='".$file->ID."' id='select_product_file_$num' " . $checked_curr_file . " />\n";
			$output .= "  <label for='select_product_file_$num'>".$file->post_title. "</label>\n";
			$output .= "</p>\n";
		}
		$output .= "</div>";
		$output .= "<input type='hidden' id='hidden_id' value='$product_id' />";
		$output .= "<input data-nonce='".usam_create_ajax_nonce( 'upload_product_file' )."' type='submit' name='save' name='product_files_submit' class='button-primary upload_file_button' value='".esc_html__('Сохранить файлы товара', 'usam')."' />";
		$output .= "</form>";
		$output .= "<div class='" . ((is_numeric( $product_id )) ? "edit_" : "") . "select_product_handle'><div></div></div>";
		$output .= "<script type='text/javascript'>\n\r";
		$output .= "var select_min_height = " . (25 * 3) . ";\n\r";
		$output .= "var select_max_height = " . (25 * ($num + 1)) . ";\n\r";
		$output .= "</script>";
		echo $output;
		exit;
	}
	
	/**
	* Функции и действия для дублирования товаров
	*/
	function controller_duplicate_product() 
	{
		$id = absint( $_GET['id'] );
		$post = get_post( $id );
		if ( isset( $post ) && $post != null ) 
		{			
			$new_id = usam_duplicate_product_process( $post );
			$this->sendback = add_query_arg( 'duplicated', 1, $this->sendback );
		} 
		else 
			wp_die( __('К сожалению, мы не можем дублировать этот Товар, поскольку он не найден в базе данных, проверьте ID этого товара:', 'usam').' '.$id );	
	}
		
/**
 *  ========================== Журнал продаж. ================================================
 */ 
 
	protected function controller_downloadable_file_clear_locks() 
	{		
		if ( isset($_GET['id']) && is_numeric( $_GET['id'] ) ) 
		{
			global $wpdb;
			
			$order_id = absint($_GET['id']);
			$downloadable_items = $wpdb->get_results( "SELECT * FROM `" . USAM_TABLE_DOWNLOAD_STATUS . "` WHERE `order_id` IN ('$order_id')", ARRAY_A );
			
			$clear_locks_sql = "UPDATE`" . USAM_TABLE_DOWNLOAD_STATUS . "` SET `ip_number` = '' WHERE `order_id` IN ('$order_id')";
			$wpdb->query( $clear_locks_sql );
			
			$download_links = array();
			$email = usam_get_buyers_email( $order_id );	
			foreach ( (array)$downloadable_items as $downloadable_item ) 
			{
				$download_links .= add_query_arg( array('downloadid' => $downloadid, 'usam_action' => 'download_file') , home_url() )  . "\n";
			}
			usam_mail( $email, __( 'Администратор разблокировал ваш файл', 'usam' ), str_replace( "%download_links%", $download_links, __( 'Уважаемый клиент, мы рады вам сообщить, что ваш заказ был обновлен и ваши загрузки в настоящее время активны. Пожалуйста, загрузите покупку, используя ссылки, представленные ниже. %download_links% Спасибо за ваш выбор.', 'usam' ) ) );
			$this->sendback = add_query_arg( 'cleared', 1, $this->sendback );			
		}
	}	
 
 // Пересчитать заказ
	protected function controller_recalculate() 
	{
		$order_id = absint($_REQUEST['id']);
		
		$order = new USAM_Order( $order_id );
		$status = $order->get('status');
		if ( usam_check_order_is_completed( $status ) )
			return false;
			
		$cart = new USAM_CART();
		$cart->set_order( $order_id );		
	}
	
	// Выгрузить резервы на FTP
	protected function controller_export_reserve_export() 
	{
		$document_id = absint($_REQUEST['item_id']);
			
		$exchange_FTP = new USAM_Exchange_FTP(  );		
		$result = $exchange_FTP->submit_reserve_export( $document_id );			
		usam_set_user_screen_error( $exchange_FTP->get_error( ), 'order' );				
		$this->sendback = add_query_arg( array( 'export_reserve' => $result ), $this->sendback );
			
		return $result;		
	}	
	
	protected function controller_recalculate_document_shipped() 
	{
		$document_id = absint($_REQUEST['item_id']);
		$order_id = absint($_REQUEST['id']);
		
		usam_calculate_amount_shipped_document( $order_id, $document_id );	
		
		$this->sendback = add_query_arg( array( 'recalculate_document_shipped' => 1 ), $this->sendback );
	}
	
	
	// Выгрузить чеки на FTP
	protected function controller_export_order_ftp() 
	{		
		$result = false;
		if ( !empty( $_REQUEST['id'] ) )
		{		
			$order_id = absint($_REQUEST['id']);
			$exchange_FTP = new USAM_Exchange_FTP( );
			$result = $exchange_FTP->export_order( $order_id );
			usam_set_user_screen_error( $exchange_FTP->get_error( ), 'order' );		
			$this->sendback = add_query_arg( array( 'export_receipt' => $result ), $this->sendback );
		}	
		return $result;
	}
		
	protected function controller_order_copy() 
	{			
		$result = true;
		if ( !empty( $_REQUEST['id'] ) )
		{		
			$order_id = absint($_REQUEST['id']);
			$new_order_id = usam_order_copy( $order_id );			
			$this->sendback = usam_get_url_order( $new_order_id );				
		}
		else
			$this->sendback = add_query_arg( array( 'error' => 1 ), $this->sendback );
		return $result;
	
	}		
	
	function controller_motify_order_status_mail() 
	{
		$order_id = absint($_REQUEST['id']);
		
		$order = new USAM_Order( $order_id );
		
		$notification = new USAM_Сustomer_Notification_Change_Order_Status( $order );			
		$email_sent = $notification->send_mail();
	}
	
	function controller_motify_order_status_sms() 
	{
		$order_id = absint($_REQUEST['id']);
		
		$order = new USAM_Order( $order_id );		
		$notification = new USAM_Сustomer_Notification_Change_Order_Status( $order );	
		$sms_sent = $notification->send_sms();	
	}
 
	// отправка писем 
	protected function controller_order_resend_email() 
	{				
		$result = false;
		if ( !empty( $_REQUEST['id'] ) )
		{
			$order_id = absint($_REQUEST['id']);
			$order = new USAM_Order( $order_id );
			$status = $order->get('status');
			if ( usam_check_order_is_completed( $status ) )
				return false;
			$notification = new USAM_Сustomer_Notification_Order( $order );
			$email_sent = $notification->send_mail();
			$this->sendback = add_query_arg( array( 'send_email' => 1 ), $this->sendback );
		}
		else
			$this->sendback = add_query_arg( array( 'send_email' => 0 ), $this->sendback );
		return $result;
	}
	
	//Удалить заказ из журнала продаж. Работает по нажатию кнопки в самом заказе.
	protected function controller_delete_order( ) 
	{
		$order_id = absint( $_REQUEST['id'] );	
		
		$result = usam_delete_order( $order_id );		
		
		$this->sendback = add_query_arg( array( 'deleted' => 1 ), $this->sendback );
		$this->sendback = remove_query_arg( array( 'id', 'action' ), $this->sendback );	
		
		return $result;
	}
	
	// Удалить товар в заказе
	protected function controller_delete_order_item()
	{			
		$order_id = absint( $_REQUEST['id'] );	
		$item_id = absint( $_REQUEST['item_id'] );	
		
		$purchase_log = new USAM_Order( $order_id );
		$result_deleted = $purchase_log->delete_order_product( $item_id );
		
		$result = true;		
		$this->sendback = add_query_arg( array( 'deleted' => 1 ), $this->sendback );
		
		return $result;
	}
	
	//Удалить оплату из заказа
	protected function controller_delete_payment_item() 
	{	
		$order_id = absint($_REQUEST['id']);
		$item_id  = absint($_REQUEST['item_id']);	

		usam_delete_payment_document( $item_id );
	
		$this->sendback = add_query_arg( array( 'delete_payment' => 1 ), $this->sendback );
		return $result;
	}	
		
	protected function controller_delete_file() 
	{
		$file_id = absint( $_REQUEST['file_id'] );
		wp_delete_post( $file_id, true );		
	}	
	
	protected function controller_admin_download_file() 
	{
		$file_id = absint($_REQUEST['file_id']);	
		usam_force_download_file( $file_id );
	}
	
	/**
	 * Получает постоянные ссылки для продуктов страниц и сохраняет их в опциях для быстрого ознакомления
	 */
	protected function controller_update_page_urls( $auto = false ) 
	{
		global $wpdb;

		usam_update_permalink_slugs();
		$this->sendback = add_query_arg( array( 'updated' => 1 ), $this->sendback );
	}
	
	function controller_install_mailtemplate()
	{
		if( !isset($_REQUEST['theme']) )
			return false;
		$theme = sanitize_title($_REQUEST['theme']);		
		update_option('usam_mailtemplate', $theme );
	}
	
	function controller_get_mail_edit()
	{
		require_once( USAM_FILE_PATH . '/admin/includes/mail/usam_edit_mail.class.php' );
		$id = absint($_REQUEST['id']);
		
		$mail = new USAM_Edit_Newsletter( $id );	
		echo $mail->get_mail_edit(  ); 
		exit;		
	}
	
	/**
	 * Обновление избранных товаров, когда нажимают на звездочку.
	 */
	function controller_update_featured_product() 
	{	
		$product_id = absint( $_GET['product_id'] );		
		if ( !current_user_can('edit_post', $product_id) )
			exit();

		$status = usam_get_product_meta( $product_id, 'sticky_products' );	
		if ( empty($status) )	
			$new_status = 1;	
		else
			$new_status = 0;		
		usam_update_product_meta( $product_id, 'sticky_products', $new_status );	
	}
	
	function controller_product_variations_table() 
	{		
		require_once( USAM_FILE_PATH . '/admin/includes/product/product-variations-page.class.php' );
		$page = new USAM_Product_Variations_Page();
		$page->display();
		exit;
	}
}
?>