<?php
class USAM_Mail_Editor_Iframe
{
	private $mail_id;	
	
	public function __construct( $mail_id = 0  )
	{			
		$this->mail_id = $mail_id;		
		add_action( 'admin_print_styles', array( $this, 'scripts_and_style' ) );	
		set_current_screen( 'page_mail-editor-iframe' );
	}	
	
	public function get_title( $action ) 
	{
		switch ( $action ) 
		{
			case 'dividers' :
				$title = __('Разделитель','usam');
			break;
			case 'image_data' :
				$title = __('Данные картинки','usam');
			break;
			case 'button' :
				$title = __('Кнопка','usam');
			break;			
			case 'posts' :			
				$title = __('Записи','usam');
			break;
			default:
				$title = '';
			break;
		}
		return $title;		
	}
	
	function handler( $action ) 
	{
		$method = 'controller_'.$action;				
		if ( method_exists($this, $method) )
		{
			$html = $this->$method();			
			$this->windows( $html, $action );
		}
		else
			echo sprintf(__( 'Не верный параметр для вызова функции: %s', 'usam'), $action);	
	}
	
	private function windows( $html, $id ) 
	{		
		?>	
		<div id = "popup_<?php echo $id; ?>" class="popup_content popup_<?php echo $id; ?>">
			<form enctype="multipart/form-data" method="post" action="" id="articles-form">
				<?php echo $this->print_messages( ); ?>	
				<?php echo $html; ?>
			</form>
		</div>
		<?php
	}
	
	function print_messages( ) 
	{
	
	}
	
	function scripts_and_style( ) 
	{		
		wp_enqueue_script('usam-mail-editor-iframe', USAM_URL . '/admin/js/mail-editor/mail-editor-iframe.js', array('jquery'), USAM_VERSION);	
		wp_localize_script( 'usam-mail-editor-iframe', 'Universam_Mail_Editor_Iframe', array(				
				'get_post_type_nonce'     => usam_create_ajax_nonce( 'get_post_type' ),				
			) );	
	
		wp_enqueue_script('iris');
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_style( 'buttons' );
		

		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');		
		
		wp_enqueue_style( 'usam-mail-editor-iframe', USAM_URL . '/admin/css/mail/mail-editor-iframe.css', false, USAM_VERSION );	
?>
<style id="iris-css">.iris-picker{display:block;position:relative}.iris-picker,.iris-picker *{-moz-box-sizing:content-box;-webkit-box-sizing:content-box;box-sizing:content-box}input+.iris-picker{margin-top:4px}.iris-error{background-color:#ffafaf}.iris-border{border-radius:3px;border:1px solid #aaa;width:200px;background-color:#fff}.iris-picker-inner{position:absolute;top:0;right:0;left:0;bottom:0}.iris-border .iris-picker-inner{top:10px;right:10px;left:10px;bottom:10px}.iris-picker .iris-square-inner{position:absolute;left:0;right:0;top:0;bottom:0}.iris-picker .iris-square,.iris-picker .iris-slider,.iris-picker .iris-square-inner,.iris-picker .iris-palette{border-radius:3px;box-shadow:inset 0 0 5px rgba(0,0,0,.4);height:100%;width:12.5%;float:left;margin-right:5%}.iris-only-strip .iris-slider{width:100%}.iris-picker .iris-square{width:76%;margin-right:10%;position:relative}.iris-only-strip .iris-square{display:none}.iris-picker .iris-square-inner{width:auto;margin:0}.iris-ie-9 .iris-square,.iris-ie-9 .iris-slider,.iris-ie-9 .iris-square-inner,.iris-ie-9 .iris-palette{box-shadow:none;border-radius:0}.iris-ie-9 .iris-square,.iris-ie-9 .iris-slider,.iris-ie-9 .iris-palette{outline:1px solid rgba(0,0,0,.1)}.iris-ie-lt9 .iris-square,.iris-ie-lt9 .iris-slider,.iris-ie-lt9 .iris-square-inner,.iris-ie-lt9 .iris-palette{outline:1px solid #aaa}.iris-ie-lt9 .iris-square .ui-slider-handle{outline:1px solid #aaa;background-color:#fff;-ms-filter:"alpha(Opacity=30)"}.iris-ie-lt9 .iris-square .iris-square-handle{background:0 0;border:3px solid #fff;-ms-filter:"alpha(Opacity=50)"}.iris-picker .iris-strip{margin-right:0;position:relative}.iris-picker .iris-strip .ui-slider-handle{position:absolute;background:0 0;margin:0;right:-3px;left:-3px;border:4px solid #aaa;border-width:4px 3px;width:auto;height:6px;border-radius:4px;box-shadow:0 1px 2px rgba(0,0,0,.2);opacity:.9;z-index:5;cursor:ns-resize}.iris-strip-horiz .iris-strip .ui-slider-handle{right:auto;left:auto;bottom:-3px;top:-3px;height:auto;width:6px;cursor:ew-resize}.iris-strip .ui-slider-handle:before{content:" ";position:absolute;left:-2px;right:-2px;top:-3px;bottom:-3px;border:2px solid #fff;border-radius:3px}.iris-picker .iris-slider-offset{position:absolute;top:11px;left:0;right:0;bottom:-3px;width:auto;height:auto;background:transparent;border:0;border-radius:0}.iris-strip-horiz .iris-slider-offset{top:0;bottom:0;right:11px;left:-3px}.iris-picker .iris-square-handle{background:transparent;border:5px solid #aaa;border-radius:50%;border-color:rgba(128,128,128,.5);box-shadow:none;width:12px;height:12px;position:absolute;left:-10px;top:-10px;cursor:move;opacity:1;z-index:10}.iris-picker .ui-state-focus .iris-square-handle{opacity:.8}.iris-picker .iris-square-handle:hover{border-color:#999}.iris-picker .iris-square-value:focus .iris-square-handle{box-shadow:0 0 2px rgba(0,0,0,.75);opacity:.8}.iris-picker .iris-square-handle:hover::after{border-color:#fff}.iris-picker .iris-square-handle::after{position:absolute;bottom:-4px;right:-4px;left:-4px;top:-4px;border:3px solid #f9f9f9;border-color:rgba(255,255,255,.8);border-radius:50%;content:" "}.iris-picker .iris-square-value{width:8px;height:8px;position:absolute}.iris-ie-lt9 .iris-square-value,.iris-mozilla .iris-square-value{width:1px;height:1px}.iris-palette-container{position:absolute;bottom:0;left:0;margin:0;padding:0}.iris-border .iris-palette-container{left:10px;bottom:10px}.iris-picker .iris-palette{margin:0;cursor:pointer}.iris-square-handle,.ui-slider-handle{border:0;outline:0}</style>	
<?php	
	}
	
	function selection_products( ) 
	{				
		$html = '<div id="basic">					
					<div class="clearfix">
						<div class="filters-box">'.$this->field_select_terms().'</div>
						<div class="search-box">
							<input type="text" id="search" name="search" autocomplete="off" />
							<input type="submit" id="search-submit" name="submit" value="'.__('Поиск', 'usam').'" />
							<input type="hidden" id="post_type" name="post_type" value="usam-product" />	
							<input type="hidden" id="post_status" name="post_status" value="publish" />							
						</div>
					</div>					
					<div id="results" data-load="false"></div>
				</div>	
				<div class="footer_panel">		
					<div class="submit-box">
						<div id="loading-icon"></div>
						<div id="loading-message"></div>					
						<input id="insert-selection" class="button-primary" type="submit" name="insert" value="'.__('Вставить товар', 'usam').'" />
					</div>
				</div>';
		$this->windows( $html, 'selection_products' );
	}
	
	
	function get_post_type( $vars ) 
	{		
		$per_page  = 20;
		$page = 1;
		$offset = ($page - 1) * $per_page;
		$query_vars = array(				
			'posts_per_page' => $per_page,
			'offset'         => $offset,		
			'post_parent'    => 0				
		);	
		$query_vars = array_merge( $query_vars, (array)$vars );	
		$wp_query = new WP_Query( $query_vars );		
		$html = '';
		if (!empty($wp_query)) 
		{
			global $post;
			$html .= '<table class ="wp-list-table1 widefat fixed striped pages">
				<thead>
					<tr>
						<td class="column-checkbox"></td>
						<td class="column-thumbnail"></td>
						<td class="column-title">'.__('Название','usam').'</td>';
						if ( isset($query_vars['post_type']) && $query_vars['post_type'] == 'usam-product' ) 
						{
							$html .= '<td class="column-sku">'.__('Артикул','usam').'</td>';
						}
					$html .= '</tr>
				</thead>
				<tbody>';
			while ($wp_query->have_posts())
			{ 
				$wp_query->the_post();			
				$html .= '<tr id="post-'.$post->ID.'">
				<td class="column-checkbox"><input id="post-checkbox-'.$post->ID.'" data-post-id="'.$post->ID.'" type="checkbox" class="checkbox" value="'.$post->ID.'" name="post_selection"></td>
				<td class="column-thumbnail">'.get_the_post_thumbnail($post->ID, array(60, 60)).'</td>
				<td class="column-title"><label class="with_thumbnail" for="post-checkbox-'.$post->ID.'">'.get_the_title().'</label></td>';
				if ( $post->post_type == 'usam-product' ) 
				{
					$sku = usam_get_product_meta( $post->ID, 'sku' );
					$html .= '<td class="column-sku">'.$sku.'</td>';
				}
			$html .= '</tr>';
			}
			$html .= '</tbody></table>';
		}
		return $html;
	}	
	
	function controller_dividers( ) 
	{
		$mail = usam_get_newsletter( $this->mail_id );				
			
		$dividers = array();
		$list = usam_list_dir( USAM_CORE_IMAGES_PATH. '/mailtemplate/dividers' );		
		foreach ( $list as $file )
		{
			if ( stristr( $file, '.png' ) || stristr( $file, '.jpg' ))		
				$dividers[] = array( 'src' => USAM_CORE_IMAGES_URL.'/mailtemplate/dividers/'.$file, 'width' => 563, 'height' => 11 );			
		}	
		
		$html = '<ul class="dividers">';		
		foreach( $dividers as $divider ) 
		{
			$selected = ($divider['src'] === $mail['data']['divider']['src']) ? ' class="selected"' : '';
			$html .= '<li class="clearfix"><a href="javascript:;"'.$selected.'><img src="'.$divider['src'].'" alt="" width="'.$divider['width'].'" height="'.$divider['height'].'" /></a></li>';
		}	
		$html .= '
		</ul>
		<input type="hidden" name="email_id" value="'.$this->mail_id.'" id="email_id" />
		<input type="hidden" name="divider_src" value="'.$mail['data']['divider']['src'].'" id="divider_src" />
		<input type="hidden" name="divider_width" value="'.$mail['data']['divider']['width'].'" id="divider_width" />
		<input type="hidden" name="divider_height" value="'.$mail['data']['divider']['height'].'" id="divider_height" />
		<div class="footer_panel">		
			<div class="submit-box">
				<input type="submit" id="dividers-submit" class="button-primary" name="submit" value="'.__('Готово', 'usam').'" />
			</div>
		</div>';
		return $html;
	}
	
	function controller_image_data( ) 
	{		
		$html = '
		<p>
			<label for="url">'.__('Адрес:', 'usam').'</label><br/>
			<input type="text" name="url" value="" id="url" style="width: 100%;" />
		</p>
		<p>
			<label for="alt">'.__('Альтернативный текст:', 'usam').'</label><br/>
			<input type="text" name="alt" value="" id="alt" style="width: 100%;"/>
		</p>		
		<div class="footer_panel">		
			<div class="submit-box">
				<input id="image-data-submit" class="button-primary" type="submit" name="submit" value="'.__('Сохранить', 'usam').'" />
			</div>
		</div>';		
		return $html;
	}
	
	function controller_button( ) 
	{		
		$fonts_family = array( 'Arial', 'Comic Sans MS', 'Courier New', 'Georgia', 'Lucida', 'Tahoma', 'Times New Roman', 'Trebuchet MS', 'Verdana'  );				
		$html = '
			<table class ="table_fields">
				<tr>
					<td class="usam_field_title"><label for="button_name_link">'.__( 'Название ссылки', 'usam').'</label></td>
					<td class="usam_field_option"><input id="button_name_link" type="text" name="text" class="usam_input usam_field_button_text" value=""></td>
				</tr>
				<tr>
					<td class="usam_field_title"><label for="button_link">'.__( 'Ссылка', 'usam').'</label></td>
					<td class="usam_field_option"><input id="button_link" type="text" name="url" class="usam_input usam_field_button_url" value="" placeholder="http://"></td>
				</tr>
				<tr>
					<td class="usam_field_title"><label for="button_alignment">'.__( 'Выравнивание', 'usam').'</label></td>
					<td class="usam_field_option">
						<div class="usam_form_field_radio_option">
							<label><input type="radio" name="alignment" class="usam_field_button_alignment" value="left">'.__( 'Слева', 'usam').'</label>
						</div>
						<div class="usam_form_field_radio_option">
							<label><input type="radio" name="alignment" class="usam_field_button_alignment" value="center" checked="">'.__( 'По центру', 'usam').'</label>
						</div>
						<div class="usam_form_field_radio_option">
							<label><input type="radio" name="alignment" class="usam_field_button_alignment" value="right">'.__( 'Справа', 'usam').'</label>
						</div>
					</td>
				</tr>				
				<tr>
					<td class="usam_field_title"><label for="usam_field_button_background_color">'.__( 'Фон кнопки', 'usam').'</label></td>
					<td class="usam_field_option"><input type="text" name="font-color" id="usam_field_button_background_color" class="usam_color" value="#ffffff"></td>
				</tr>
				<tr>
					<td class="usam_field_title"><label for="usam_field_button_width">'.__( 'Ширина', 'usam').'</label></td>
					<td class="usam_field_option">
						<input id ="usam_field_button_width" type="number" name="width-input" class="usam_input usam_input_small usam_field_button_width_input" value="180" min="50" max="288" step="1"> px
					</td>
				</tr>
				<tr>
					<td class="usam_field_title"><label for="usam_field_button_height">'.__( 'Высота', 'usam').'</label></td>
					<td class="usam_field_option">
						<input id ="usam_field_button_height"  type="number" name="line-height-input" class="usam_input usam_input_small usam_field_button_line_height_input" value="40" min="20" max="50" step="1"> px
					</td>
				</tr>			
			</table> 
			<table class ="table_fields_column2">
				<tr>
					<td><h4>'.__( 'Текст', 'usam').'</h4></td>
					<td><h4>'.__( 'Рамка', 'usam').'</h4></td>
				</tr>	
			   <tr>
					<td>									
					<div class="usam_form_field_input_option">
						<input type="text" name="background-color" id ="usam_field_button_font_color" class="usam_color" value="#2ea1cd" style="display: none;">
					</div>	
					<div class="usam_form_field_title usam_form_field_title_inline">'.__( 'Шрифт', 'usam').'</div>
					<div class="usam_form_field">
						<div class="usam_form_field_input_option">
							 <select id="usam_field_button_font_family" name="font-family" class="usam_select usam_select_medium usam_field_button_font_family usam_font_family">
								<option value="Arial">Arial</option>
								<option value="Comic Sans MS">Comic Sans MS</option>
								<option value="Courier New">Courier New</option>
								<option value="Georgia">Georgia</option>
								<option value="Lucida">Lucida</option>
								<option value="Tahoma">Tahoma</option>
								<option value="Times New Roman">Times New Roman</option>
								<option value="Trebuchet MS">Trebuchet MS</option>
								<option value="Verdana" selected="">Verdana</option>
							</select> 
						</div>					
					</div>
					<div class="usam_form_field_title usam_form_field_title_inline">'.__( 'Размер шрифта', 'usam').'</div>
					<div class="usam_form_field">
						<div class="usam_form_field_input_option">
							<select id="usam_field_button_font_size" name="font-size" class="usam_select usam_select_small usam_field_button_font_size usam_font_size">
								<option value="10px">10px</option>
								<option value="12px">12px</option>
								<option value="14px">14px</option>
								<option value="16px">16px</option>
								<option value="18px" selected="">18px</option>
								<option value="20px">20px</option>
								<option value="22px">22px</option>
								<option value="24px">24px</option>
								<option value="26px">26px</option>
								<option value="30px">30px</option>
								<option value="36px">36px</option>
								<option value="40px">40px</option>
							</select>
						</div>					
					</div>
					<div class="usam_form_field_title usam_form_field_title_inline">'.__( 'Начертание', 'usam').'</div>
					<div class="usam_form_field">
						<div class="usam_form_field_input_option">
							<select id="usam_field_button_font_weight" name="font_weight" class="usam_select usam_select_small usam_field_button_font_weight usam_font_weight">';
							for ($i=100; $i<=800;)
							{				           
								$i += 100;
								$html .= "<option value='$i'>$i</option>";				
							}			
							$html .= '</select>  
						</div>					
					</div>								
				</td>
				<td>
					<div class="usam_form_field">
						<div class="usam_form_field_input_option">
							<input type="text" name="border-color" id ="usam_field_button_border_color" class="usam_color" value="#0074a2" style="display: none;">
						</div>
					</div>
					<div class="usam_form_field">
						<label>
							<div class="usam_form_field_title">'.__( 'Ширина', 'usam').'</div>	
							<div class="usam_form_field_input_option">
								<input type="number" name="border-width-input" class="usam_input usam_input_small usam_field_button_border_width_input" value="0" min="0" max="10" step="1"> px
							</div>
						</label>
					</div>
					<div class="usam_form_field">
						<label>
							<div class="usam_form_field_title">'.__( 'Скругленные углы', 'usam').'</div>
							<div class="usam_form_field_input_option">
								<input type="number" name="border-radius-input" id ="usam_field_button_border_radius_input" class="usam_input usam_input_small" value="5" min="0" max="40" step="1"> px
							</div>
						</label>
					</div>					
				</td>
			</tr>
		</table>
		<div class="footer_panel">		
			<div class="submit-box">
				<input type="button" name="replace-all-button-styles" class="button button-secondary usam_button_full usam_button_replace_all_styles" value="'.__( 'Применить ко всем кнопкам', 'usam').'">
				<input type="button" class="button button-primary usam_done_editing" value="'.__( 'Готово', 'usam').'">
			</div>
		</div>';		
	return $html;
	}
		
	function controller_posts() 
	{
		$html = '
		<div id="basic">
			<div class="clearfix">
				<div class="filters-box">
					'.$this->field_select_post_type().$this->field_select_terms().$this->field_select_status().'
				</div>
				<div class="search-box">
					<input type="text" id="search" name="search" autocomplete="off" />
					<input type="submit" id="search-submit" name="submit" value="'.__('Поиск', 'usam').'" />
				</div>
			</div>
			<div id="results" data-load="true"></div>
		</div>

		<div id="advanced">'.$this->_post_display_options( $data ).'</div>

		<div class="footer_panel">		
			<div class="submit-box">
				<div id="loading-icon"></div>
				<div id="loading-message"></div>
				<a id="toggle-advanced" href="javascript:;">'.__('Отобразить и вставить параметры', 'usam').'</a>
				<input id="insert-selection" class="button button-primary" type="submit" name="insert" value="'.__('Вставить выбранные', 'usam').'" />
				<input id="back-selection" class="button button-secondary" type="button" value="'.__('Назад к выбору', 'usam').'" />
			</div>
		</div>';
		return $html;
	}
	
	function get_post_types($return_type = 'objects') 
	{
		$args=array(
		  'public'   => true,
		  '_builtin' => false,
		  'show_in_menu'=>true,
		  'show_ui'=>true,
		);
		return get_post_types($args, $return_type);
	}
	
	function field_select_post_type( $value = 'post' ) 
	{		
		$post_types = $this->get_post_types();

		// build post type selection
		$output = '<select name="post_type" data-placeholder="' . esc_attr__( 'Filter by type', 'usam' ) . '" class="mailpoet-field-select2-simple" data-minimum-results-for-search="-1" style="margin-right: 5px;" width="150" id="post_type">';		
		$output .= '<option value="post"' . ( ( $value === 'post' ) ? ' selected="selected"' : '' ) . '>' . __( 'Статьи', 'usam' ) . '</option>';
		$output .= '<option value="page"' . ( ( $value === 'page' ) ? ' selected="selected"' : '' ) . '>' . __( 'Страницы', 'usam' ) . '</option>';

		foreach ( $post_types as $key => $object_post_type ) 
		{
			$selected = ($value === $key) ? ' selected="selected"' : '';
			$output .= '<option value="'.$key.'"'.$selected.'>'.$object_post_type->labels->name.'</option>';
		}
		$output .= '</select>';
		return $output;
	}

	function field_select_terms() 
	{
		return '<input name="post_category" data-placeholder="' . esc_attr__( 'Categories and tags', 'usam' ) . '" class="mailpoet-field-select2-terms post_category" style="margin-right: 5px; width: 180px"  width="180" value="" data-multilple="false" type="hidden">';
	}


	private function field_select_status( $current_status = 'publish' ) 
	{
		$output = '';		
		$statuses = get_post_statuses();

		$output .= '<select data-placeholder="' . esc_attr__( 'Filter by status', 'usam' ) . '" class="mailpoet-field-select2-simple post_status" data-minimum-results-for-search="-1" id="post_status" name="post_status" width="150">';
		$output .= '<option></option>';
		foreach ( $statuses as $key => $label ) 
		{
			$is_selected = ( $current_status === $key ) ? 'selected="selected"' : '';
			$output .= '<option value="' . $key . '" ' . $is_selected . '>' . $label . '</option>';
		}
		$output .= '</select>';
		return $output;
	}
	
	private function _post_display_options( $data ) 
	{
		$output = '';
		$output .= '<div class="block clearfix">';
		$output .= '    <label>'.__('Display...', 'usam');
		$output .= '    </label>';
		$output .= '    <label class="radio"><input type="radio" name="post_content" value="excerpt" '.(($data['params']['post_content'] === 'excerpt') ? 'checked="checked"' : '').' />'.__('excerpt', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="post_content" value="full" '.(($data['params']['post_content'] === 'full') ? 'checked="checked"' : '').' />'.__('full post', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="post_content" value="title" '.(($data['params']['post_content'] === 'title') ? 'checked="checked"' : '').' />'.__('title only', 'usam').'</label>';
		$output .= '</div>';

		// title format
		$output .= '<div class="block clearfix alternate">';
		$output .= '    <label>'.__('Title format', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_tag" value="h1" '.(($data['params']['title_tag'] === 'h1') ? 'checked="checked"' : '').' />'.__('heading 1', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_tag" value="h2" '.(($data['params']['title_tag'] === 'h2') ? 'checked="checked"' : '').' />'.__('heading 2', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_tag" value="h3" '.(($data['params']['title_tag'] === 'h3') ? 'checked="checked"' : '').' />'.__('heading 3', 'usam').'</label>';
		$output .= '    <label id="title_tag_list" class="radio"><input type="radio" name="title_tag" value="list" '.(($data['params']['title_tag'] === 'list') ? 'checked="checked"' : '').' />'.__('show as list', 'usam').'</label>';
		$output .= '</div>';

		// title position
		$output .= '<div id="title_position_block" class="block clearfix">';
		$output .= '    <label>'.__('Title position', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_position" value="inside" '.(($data['params']['title_position'] === 'inside') ? 'checked="checked"' : '').' />'.__('in text block', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_position" value="outside" '.(($data['params']['title_position'] === 'outside') ? 'checked="checked"' : '').' />'.__('above text block and image', 'usam').'</label>';
		$output .= '</div>';

		// title alignment
		$output .= '<div class="block clearfix alternate">';
		$output .= '    <label>'.__('Title alignment', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_alignment" value="left" '.(($data['params']['title_alignment'] === 'left') ? 'checked="checked"' : '').' />'.__('left', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_alignment" value="center" '.(($data['params']['title_alignment'] === 'center') ? 'checked="checked"' : '').' />'.__('center', 'usam').'</label>';
		$output .= '    <label class="radio"><input type="radio" name="title_alignment" value="right" '.(($data['params']['title_alignment'] === 'right') ? 'checked="checked"' : '').' />'.__('right', 'usam').'</label>';
		$output .= '</div>';

		// image alignment
		$output .= '<div id="image_block">';
		$output .= '    <div class="block clearfix">';
		$output .= '        <label>'.__('Image alignment', 'usam').'</label>';

		$output .= '        <div class="group clearfix">';

		// display alternate only when multiple posts are allowed
		if($data['autopost_type'] !== 'single') {
			$output .= '    <label class="radio"><input type="radio" name="image_alignment" value="alternate" '.(($data['params']['image_alignment'] === 'alternate') ? 'checked="checked"' : '').' />'.__('alternate left & right', 'usam').'</label>';
		}
		$output .= '        <label class="radio"><input type="radio" name="image_alignment" value="left" '.(($data['params']['image_alignment'] === 'left') ? 'checked="checked"' : '').' />'.__('left', 'usam').'</label>';
		$output .= '        <label class="radio"><input type="radio" name="image_alignment" value="center" '.(($data['params']['image_alignment'] === 'center') ? 'checked="checked"' : '').' />'.__('center', 'usam').'</label>';
		$output .= '        <label class="radio"><input type="radio" name="image_alignment" value="right" '.(($data['params']['image_alignment'] === 'right') ? 'checked="checked"' : '').' />'.__('right', 'usam').'</label>';
		$output .= '        <label class="radio"><input type="radio" name="image_alignment" value="none" '.(($data['params']['image_alignment'] === 'none') ? 'checked="checked"' : '').' />'.__('no image', 'usam').'</label>';
		$output .= '    </div>';

		$output .= '    </div>';

		// image width
		$output .= '    <div id="image_width_block" class="block clearfix">';
		$output .= '        <input id="image_width" type="hidden" name="image_width" value="'.(int)$data['params']['image_width'].'" />';
		$output .= '        <label>'.__('Image width', 'usam').'</label>';
		$output .= '        <span id="image_width_slider">';
		$output .= '            <span id="slider_handle"></span>';
		$output .= '        </span>';
		$output .= '        <span id="slider_info"><span>'.(int)$data['params']['image_width'].'</span> px</span>';
		$output .= '    </div>';
		$output .= '</div>';

		// author options
		$output .= '<div id="author-block" class="block clearfix alternate">';
		$output .= '    <label>'.__('Include author', 'usam').'</label>';
		$output .= '    <div class="group">';
		$output .= '        <p class="clearfix">';
		$output .= '            <label class="radio"><input type="radio" name="author_show" value="no" '.(($data['params']['author_show'] === 'no') ? 'checked="checked"' : '').' />'.__('no', 'usam').'</label>';
		$output .= '            <label class="radio"><input type="radio" name="author_show" value="above" '.(($data['params']['author_show'] === 'above') ? 'checked="checked"' : '').' />'.__('above content', 'usam').'</label>';
		$output .= '            <label class="radio"><input type="radio" name="author_show" value="below" '.(($data['params']['author_show'] === 'below') ? 'checked="checked"' : '').' />'.__('below content', 'usam').'</label>';
		$output .= '        </p>';
		$output .= '        <p class="clearfix">';
		$output .= '            <label>'.__('Preceded by:', 'usam').'&nbsp;<input type="text" name="author_label" value="'.stripslashes($data['params']['author_label']).'" /></label>';
		$output .= '        </p>';
		$output .= '    </div>';
		$output .= '</div>';

		// categories options
		$output .= '<div id="category-block" class="block clearfix">';
		$output .= '    <label>'.__('Include categories', 'usam').'</label>';
		$output .= '    <div class="group">';
		$output .= '        <p class="clearfix">';
		$output .= '            <label class="radio"><input type="radio" name="category_show" value="no" '.(($data['params']['category_show'] === 'no') ? 'checked="checked"' : '').' />'.__('no', 'usam').'</label>';
		$output .= '            <label class="radio"><input type="radio" name="category_show" value="above" '.(($data['params']['category_show'] === 'above') ? 'checked="checked"' : '').' />'.__('above content', 'usam').'</label>';
		$output .= '            <label class="radio"><input type="radio" name="category_show" value="below" '.(($data['params']['category_show'] === 'below') ? 'checked="checked"' : '').' />'.__('below content', 'usam').'</label>';
		$output .= '        </p>';
		$output .= '        <p class="clearfix">';
		$output .= '            <label>'.__('Preceded by:', 'usam').'&nbsp;<input type="text" name="category_label" value="'.stripslashes($data['params']['category_label']).'" /></label>';
		$output .= '        </p>';
		$output .= '    </div>';
		$output .= '</div>';

		// read more
		$output .= '<div id="readmore-block" class="block clearfix">';
		$output .= '    <label for="readmore">'.__('"Read more" text', 'usam').'</label>';
		$output .= '    <input type="text" name="readmore" value="'.stripslashes($data['params']['readmore']).'" id="readmore" />';
		$output .= '</div>';

		// check if we allow mutiple posts within the ALC
		if($data['autopost_type'] === 'single') {
			// background color
			$output .= '<div id="bgcolor-block" class="block clearfix">';
			$output .= '    <label>'.__('Background color', 'usam').'</label>';
			$output .= '    <input class="color" type="text" name="bgcolor1" value="'.(isset($data['params']['bgcolor1']) ? $data['params']['bgcolor1'] : '').'" />';
			$output .= '</div>';
		} else {
			// batch insert options
			$output .= '<div class="block clearfix">';
			$output .= '    <h2>'.__('Batch insert options', 'usam').'</h2>';
			$output .= '</div>';

			// sort by
			$output .= '<div id="sort-block" class="block clearfix">';
			$output .= '    <label>'.__('Sort by', 'usam').'</label>';
			$output .= '    <label class="radio"><input type="radio" name="sort_by" value="newest" '.(($data['params']['sort_by'] === 'newest') ? 'checked="checked"' : '').' />'.__('newest', 'usam').'</label>';
			$output .= '    <label class="radio"><input type="radio" name="sort_by" value="oldest" '.(($data['params']['sort_by'] === 'oldest') ? 'checked="checked"' : '').' />'.__('oldest', 'usam').'</label>';
			$output .= '</div>';

			// show dividers
			$output .= '<div id="divider-block" class="block clearfix">';
			$output .= '    <label>'.__('Show divider between posts', 'usam').'</label>';
			$output .= '    <label class="radio"><input type="radio" name="show_divider" value="yes" '.(($data['params']['show_divider'] === 'yes') ? 'checked="checked"' : '').' />'.__('yes', 'usam').'</label>';
			$output .= '    <label class="radio"><input type="radio" name="show_divider" value="no" '.(($data['params']['show_divider'] === 'no') ? 'checked="checked"' : '').' />'.__('no', 'usam').'</label>';
			$output .= '</div>';

			// background colors
			$output .= '<div id="bgcolor-block" class="block clearfix">';
			$output .= '    <label>'.__('Background color with alternate', 'usam').'</label>';
			$output .= '    <input class="color" type="text" name="bgcolor1" value="'.(isset($data['params']['bgcolor1']) ? $data['params']['bgcolor1'] : '').'" />';
			$output .= '    <input class="color" type="text" name="bgcolor2" value="'.(isset($data['params']['bgcolor2']) ? $data['params']['bgcolor2'] : '').'" />';
			$output .= '</div>';
		}

		return $output;
	}
	
	function controller_delimiter() 
	{
		
		
	}
	
	function controller_content() 
	{
		
		
	}
}
?>