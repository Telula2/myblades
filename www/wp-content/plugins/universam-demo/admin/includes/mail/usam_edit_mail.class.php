<?php
// Класс рассылки
class USAM_Edit_Newsletter
{	
	protected $data = array();	
	protected $edit = true;	
	
	function __construct( $id )
	{				
		$this->data = usam_get_newsletter( $id );
		if ( empty($this->data['data']['body']) )
			$this->data['data']['body'] = array();
	}
	
	public function get_message( $content )
	{					
		$template = $this->data['template'];
		$mailtemplate = usam_get_email_template( 'index', $template );		
	
		return str_replace('%mailcontent%',$content, $mailtemplate);
	}
	
	public function get_mail_edit( )
	{			
		$content = $this->get_mail_content_edit( );
		$message = $this->get_message( $content );
		return $message;
	}
	
	public function get_mail( )
	{			
		$content = $this->get_mail_content( );
		$message = $this->get_message( $content );
		
		return $message;
	}
	
	// Получить css
	public function get_css( $css )
	{
		$properties = array();		
		foreach( $css as $key => $value) 
		{
			$properties[] = "$key:$value";
		}
		return implode(';', $properties);
	}
	
	public function get_block_placeholder( $id, $parent )
	{	
		if ( $this->edit )
		{
			ob_start();	
			?>
			<div class="block_placeholder enabled" id="anonymous_element-<?php echo $id; ?>" style="position: relative;"><?php _e('Вставьте блок здесь', 'usam') ?></div>
			<?php
			$return = ob_get_contents();
			ob_end_clean();
		}
		else
			$return = '';
		return $return;
	}
	
	public function get_content_block_column( $id, $block )
	{			
		ob_start();		
		?>
		<table class="block_grid" id="anonymous_element-<?php echo $id; ?>" style="position: relative;width:100%;table-layout: fixed;">
		<?php
		for ($i = 1; $i<=$block['line']; $i++)
		{
			?>
			<tr>
			<?php
			for ($j = 1; $j<=$block['column']; $j++)
			{
				?>
				<td>
					<div class="cel_container" id="block-<?php echo $id; ?>">				
						<?php 
						if ( $this->edit )
						{
							if ( !empty($block['content'][$i][$j]) )
								echo $this->blocks_edit_cycle( $block['content'][$i][$j] ); 
							else
								echo $this->get_block_placeholder( $i, $i );		
						}
						else
						{
							if ( !empty($block['content'][$i][$j]) )
								echo $this->blocks_cycle( $block['content'][$i][$j] ); 
						}
						?>
					</div>
				</td>
				<?php
			}
			?>
			</tr>
			<?php
		}
		?>
		</table>
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}
	
	public function get_product_template( )
	{	
		global $post;		
		$product_html = '<div id="product_block" data-product_id="'.$post->ID.'">'.usam_get_email_template( 'product' ).'</div>';
		return $product_html;
	}	
	
	public function get_content_block_column_product( $id, $block )
	{	
		global $post;
		ob_start();		
		?>
		<table class="block_grid_products" id="anonymous_element-<?php echo $id; ?>" style="position: relative;width:100%;table-layout: fixed;">
		<?php
		for ($i=1; $i<=$block['line']; $i++)
		{
			?>
			<tr>
			<?php
			for ($j=1; $j<=$block['column']; $j++)
			{
				?>
				<td>
					<div class="cel_container product_container" id="products-<?php echo $id; ?>">					
						<?php 									
						if ( !empty($block['content'][$i][$j]) )
						{									
							$post = get_post( $block['content'][$i][$j] );		
							if ( !empty($post) )
								echo $this->get_product_template();
							else
							{
								if ( $this->edit )
									_e('Нажми здесь', 'usam');
							}							
						}
						else
						{
							if ( $this->edit )
								_e('Нажми здесь', 'usam');
						}
						?>
					</div>
				</td>
				<?php
			}
			?>
			</tr>
			<?php
		}
		?>
		</table>
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}


	private function style( $style )
	{
		if ( $style != '' )
			$style = "style = '$style'";
		else
			$style = "";
		return $style;
	}
	
	public function get_content_block_content( $id, $block )
	{		
		$style_image = "";
		if ( !isset($block['alignment']) )
			$block['alignment'] = '';
		if ( !$this->edit )
		{
			switch ( $block['alignment'] ) 
			{
				case 'left' :
					$style_image = "float: left;  margin: 0 15px 5px 0;";
				break;
				case 'center' :
					$style_image = "margin: 0 auto 15px auto;";
				break;
				case 'right' :
					$style_image = "float:right; margin:0 0 5px 15px;";
				break;
			}			
		}
		else
		{
			$style_content = '';			
		}
		$image_default = array( 'src' => '', 'width' => '', 'height' => '', 'alignment' => '', 'static' => '', 'url' => '', 'alt' => '' );
		if ( !empty($block['image']) )
			$image = array_merge( $image_default, $block['image'] );	
		else
			$image = $image_default;
		
		$style_image .= "width: ".$image['width']."px;height:".$image['height']."px";
		
		$style_image = $this->style( $style_image );
		
		if ( $image['src'] !== '' )
		{
			$class_image = '';
			$class_text = '';
		}
		else
		{
			$class_image = 'empty';	
			$class_text = 'alone';
		}
		if ( !isset($block['text']['value']) )
			$block['text']['value'] = '';
		
		$background = !empty($block['background'])?$block['background']:'transparent';
		ob_start();			
		?> 
		<div class="usam_content <?php echo $block['alignment']; ?>" usam_align="<?php echo $block['alignment']; ?>" style="width: 100%; display:inline-block; background-color: <?php echo $background; ?>;">
			<div usam_ratio="1" class="usam_image <?php echo $class_image; ?>" <?php echo $style_image; ?>>				
				<?php 
				if ( $image['url'] != '' ) 
				{ 					
					echo "<a id='image_link' alt='".$image['alt']."' style='display:inline-block;' href='".$image['url']."'>";  
				}
				?>				
				<img height="<?php echo $image['height']; ?>" width="<?php echo $image['width']; ?>" class="image_placeholder <?php echo $class_image; ?>" src="<?php echo $image['src']; ?>" data-alt="<?php echo $image['alt']; ?>" data-url="<?php echo $image['url']; ?>" alt="" id="anonymous_element-<?php echo $id; ?>" style="position: relative; visibility: visible;">
				<?php if ( $image['url'] != '' ) {  echo "</a>"; } ?>								
			</div>			
			<div class="usam_text <?php echo $class_text; ?>" style="width: 100%; height: auto; float: none;">				
				<div class="usam_editable"><?php echo $block['text']['value']; ?></div>							
			</div>
		</div>
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}
	
	public function get_content_block_button( $id, $block )
	{				
		$default = array( 'text' => __('Кнопка','usam'), 'alignment' => '', 'url' => '', 'css' => array( 'width' => '180px', 'height' => '40px', 'background-color' => '#2ea1cd', 'color' => '#ffffff', 'font-family' => 'Verdana, Geneva, sans-serif;', 'font-size' => '18px', 'font-weight' => 'normal', 'line-height' => '36px', 'border' => '1px solid #0074a2', 'border-radius' => '5px', 'text-decoration' => 'none') );
		$block = usam_array_merge_recursive( $default, $block );	
	
		$background = !empty($block['background'])?$block['background']:'transparent';
		
		$css = $this->get_css( $block['css'] );
		ob_start();			
		?> 
		<div class="usam_content <?php echo $block['alignment']; ?>" style="background-color: <?php echo $block['background']; ?>;">
			<a href="<?php echo $block['url']; ?>" class="usam_editor_button" style="margin: auto; display: block; text-align: center;<?php echo $css; ?>" onclick="return false;"><?php echo $block['text']; ?></a>
		</div>
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}	
		
	public function get_content_block_gallery( $id, $block )
	{
	
	}
	
	public function get_default_divider( )
	{
		$divider = array();
		$list = usam_list_dir( USAM_CORE_IMAGES_PATH. '/mailtemplate/dividers' );		
		foreach ( $list as $file )
		{
			if ( stristr( $file, '.png' ) || stristr( $file, '.jpg' ))		
			{
				$divider = array( 'src' => USAM_CORE_IMAGES_URL.'/mailtemplate/dividers/'.$file, 'width' => 563, 'height' => 11 );			
				break;
			}
		}	
		return $divider;
	}
	
	public function get_content_block_divider( $id, $block )
	{			
		if ( empty($block['src']) )
		{
			if ( !empty($this->data['data']['divider']) )
				$block = $this->data['data']['divider'];
			else
				$block = $this->get_default_divider();
		}	
		$background = !empty($block['background'])?$block['background']:'transparent';
		
		ob_start();			
		?>
		<div class="usam_divider" style="background-color: <?php echo $block['background']; ?>;">
			<img src="<?php echo $block['src']; ?>" height="<?php echo $block['height']; ?>" width="<?php echo $block['width']; ?>" style="visibility: visible; margin:0 auto;">
		</div>		
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}	
	
	public function get_content_block_indentation( $id, $block )
	{	
		ob_start();	
		$default = array( 'height' => '30px', 'background' => 'transparent' );	
		$block = array_merge( $default, $block );	
	
		?>
		<div class="usam_indentation" style="height: <?php echo $block['height']; ?>;background-color: <?php echo $block['background']; ?>;">
			<?php if ( $this->edit ) { ?>
			<div class="usam_resize_handle_container ui-resizable-handle ui-resizable-s">
				<div class="usam_resize_handle">
					<span class="usam_resize_handle_text"><?php echo $block['height']; ?></span>
					<span class="usam_resize_handle_icon"><span class="usam_plus"></span><span class="usam_minus"></span></span>
				</div>
			</div>
			<?php } ?>
		</div>	
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}	
	
	private function stripShortcodes( $content ) 
	{
		if(strlen(trim($content)) === 0) {
			return '';
		}		
		$content = preg_replace("/\[caption.*?\](.*<\/a>)(.*?)\[\/caption\]/", '$1', $content);// remove captions		
		$content = preg_replace('/\[[^\[\]]*\]/', '', $content);// remove other shortcodes

		return $content;
	}
	
	private function convertEmbeddedContent($content = '') 
	{
		// remove embedded video and replace with links
		$content = preg_replace('#<iframe.*?src=\"(.+?)\".*><\/iframe>#', '<a href="$1">'.__('Нажмите здеь чтобы открыть', 'usam' ).'</a>', $content);
		// replace youtube links
		$content = preg_replace('#http://www.youtube.com/embed/([a-zA-Z0-9_-]*)#Ui', 'http://www.youtube.com/watch?v=$1', $content);

		return $content;
	}
	
	private function post_content( $post, $params )
	{
		$post_meta_above = '';
		if( !empty($post->post_excerpt) ) 		
			$content = $post->post_excerpt;
		else 
			$content = $post->post_content;
		
		// convert new lines into <p>
		$content = wpautop( $content );

		// remove images
		$content = preg_replace('/<img[^>]+./','', $content);

		// strip shortcodes
		$content = $this->stripShortcodes($content);

		// remove wysija nl shortcode
		$content= preg_replace('/\<div class="wysija-register">(.*?)\<\/div>/','',$content);

		// convert embedded content if necessary
		$content = $this->convertEmbeddedContent($content);

		// convert h4 h5 h6 to h3
		$content = preg_replace('/<([\/])?h[456](.*?)>/', '<$1h3$2>', $content);

		// convert ol to ul
		$content = preg_replace('/<([\/])?ol(.*?)>/', '<$1ul$2>', $content);

		// convert currency signs
		$content = str_replace(array('$', '€', '£', '¥'), array('&#36;', '&euro;', '&pound;', '&#165;'), $content);

		// strip useless tags
		// TODO should we add table, tr, td and th into that list it could create issues in some cases
		$tags_not_being_stripped = array('<p>','<em>','<span>','<b>','<strong>','<i>','<h1>','<h2>','<h3>','<a>','<ul>','<ol>','<li>','<br>');

		// filter to modify that list
		$tags_not_being_stripped = apply_filters('mpoet_strip_tags_ignored',$tags_not_being_stripped);

		$content = strip_tags($content, implode('',$tags_not_being_stripped));
		
		if( $params['title_position'] === 'inside' ) 
			$content = $this->getPostTitle($post, $params).$post_meta_above.$content;
		else 
			$content = $post_meta_above.$content;	
			
		
		return $content;
	}
	
	public function getPostTitle( $post = array(), $params = array() )
	{
		$content = '';

		if(strlen(trim($post->post_title)) > 0) 
		{			
			$post_title = trim(str_replace(array('$', '€', '£', '¥'), array('&#36;', '&euro;', '&pound;', '&#165;'), strip_tags($post->post_title)));

			$content .= '<'.$params['title_tag'].' class="align-'.$params['title_alignment'].'">';				
				$content .= '<a href="'.get_permalink($post->ID).'" target="_blank">';
					$content .= $post_title;			
				$content .= '</a>';			
			$content .= '</'.$params['title_tag'].'>';
		}

		return $content;
	}
	
	public function get_post_block( $post, $params = array() )
	{
		$default = array(
			'title_tag' => 'h1',
			'title_alignment' => 'left',
			'title_position' => 'inside',
			'image_alignment' => 'left',			
			'image_width' => 325,
			'readmore' => __('Читать онлайн', 'usam'),
			'post_content' => 'full',
			'author_show' => 'no',
			'author_label' => '',
			'category_show' => 'no',
			'category_label' => ''
		);	
		$params = array_merge($default, $params);	
		
		$width  = 325;
		$height = 325;	
		$size = array( $width, $height );
			
		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		$image_link_attributes = wp_get_attachment_image_src($post_thumbnail_id, $size);	
		$image = array( 'src' => $image_link_attributes[0], 'width' => $width, 'height' => $height, 'alignment' => '', 'static' => '', 'url' => '', 'alt' => '' );
		
		$block = array( 'text' => array( 'value' => apply_filters('the_content', $this->post_content($post, $params) ) ), 'image' => $image, 'alignment' => 'left', 'static' => '', 'type' => 'content' );
		
		return $this->get_block_edit( $block );
	}			
	
	public function get_block( $block, $i )
	{					
		$method = 'get_content_block_'.$block['type'];	
		if ( method_exists($this, $method) )
		{
			$return = $this->$method( $i+1, $block );
		}		
		else
			$return = '';
		return $return;
	}
	
	private function blocks_cycle( $blocks )
	{
		$i = 0;	
		$return = '';		
		if ( !empty($blocks) )
		{			
			foreach ( (array)$blocks as $key => $block ) 		
			{			
				$i++;			
				$return .= $this->get_block( $block, $i );			
			}			
		}				
		return $return;
	}
	
	public function get_mail_content(  )
	{	
		$this->edit = false;
		return $this->blocks_cycle( $this->data['data']['body'] );
	}
	
	
	public function get_block_edit( $block, $id = 0)
	{		
		ob_start();	
		echo $this->get_block_placeholder( $id+1, $id );
		$background = !empty($block['background'])?$block['background']:'transparent';
		?>
		<div data-type="<?php echo $block['type']; ?>" class="usam_block" id="anonymous_element-<?php echo $id; ?>" usam_bg_color="" style="top: 0px; left: 0px; background-color: <?php echo $background; ?>;">
			<ul class="usam_controls clearfix">
				<li class="handle_container"><a class="handle" href="javascript:;"><span></span></a></li>
				<li class="background_container">						
					<ul class="usam_background_controls">
						<li><a class="background_transparent" href="javascript:;"><span></span></a></li>
						<li><a class="close" href="javascript:;"><span></span></a></li>
					</ul>
					<input class="background_input" id="background_input_<?php echo $id; ?>" type="hidden">
					<span class="usam_background_open" style="background-color:<?php echo $background; ?>"></span>
				</li>
				<?php if ( $block['type'] == 'image' ) { ?>
					<li><a class="link" href="javascript:;"><span></span></a></li>
				<?php } ?>
				<li><a class="remove" href="javascript:;"><span></span></a></li>
				<li class="size"></li>
			</ul>
			<?php echo $this->get_block( $block, $id ); ?>			
		</div>		
		<?php
		$return = ob_get_contents();
		ob_end_clean();
		return $return;
	}
	
	// Цикл по блокам
	private function blocks_edit_cycle( $blocks )
	{
		$i = 0;	
		$return = '';		
		if ( !empty($blocks) )
		{			
			foreach ( (array)$blocks as $key => $block ) 		
			{			
				$i++;			
				$return .= $this->get_block_edit( $block, $i );			
			}
			$return .= $this->get_block_placeholder( $i+2, $i+2 );
		}
		else
		{			
			$return .= $this->get_block_placeholder( 1, 1 );		
		}					
		return $return;
	}
	
	public function get_mail_content_edit(  )
	{				
		$return = '<div class="usam_mailtemplate">';		
		$return .= $this->blocks_edit_cycle( $this->data['data']['body'] );		
		$return .= '</div>';
		return $return;
	}  
}
?>