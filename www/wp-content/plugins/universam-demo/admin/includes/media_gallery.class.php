<?php
/*
* =====================  Изменения в Media Gallery ================================================
*/
class USAM_Media_Gallery
{
	function __construct( ) 
	{
		add_filter( 'gettext', array($this, 'filter_feature_image_text'), 12, 3 ); //заменяет слова строки, когда пользователь выбирает продукт миниатюры
		add_filter( 'attachment_fields_to_edit', array($this,'attachment_fields'), 11, 2 );
		add_filter( 'attachment_fields_to_save', array($this,'save_attachment_fields'), 9, 2 );
		
		if ( ( isset( $_REQUEST['parent_page'] ) && ( $_REQUEST['parent_page'] == 'usam-edit-products' ) ) ) 
		{
			add_filter( 'media_upload_tabs', array($this,'media_upload_tab_gallery'), 12 );
			add_filter( 'media_upload_form_url', array($this,'media_upload_url'), 9, 1 );
			add_action( 'admin_head', array($this,'gallery_css_mods') );
		}
		add_filter( 'attachment_fields_to_save', array($this,'attachment_fields_to_save'), 1, 2 ); // Переименовать загружаемые картинки. Имя сделать название артикула товара
		add_filter( 'wp_update_attachment_metadata', array($this,'rename_attacment'), 1,2 ); //Переименовать изображение при первой загрузке
	}
	
	/*
	 * Заменяет слова строки "использования в качестве показа картинки" на слова 'Использование в качестве продукта миниатюры ", когда пользователь выбирает продукт миниатюры.
	 */
	function filter_feature_image_text( $translation, $text, $domain ) 
	{
		if ( 'Use as featured image' == $text && !empty( $_REQUEST['post_id'] ) ) 
		{
			$post = get_post( absint($_REQUEST['post_id']) );
			if ( empty($post->post_type) || $post->post_type != 'usam-product' ) 
				return $translation;
			$translations = get_translations_for_domain( $domain );
			return $translations->translate( 'Использовать как миниатюру для товара', 'usam' );
		}
		return $translation;
	}
	
	function attachment_fields( $form_fields, $post ) 
	{
		$out = '';
		if( !empty( $_REQUEST["post_id"] ) )
			$parent_post = get_post( absint($_REQUEST["post_id"]) );
		else
			$parent_post = get_post( $post->post_parent );		
		if ( isset($parent_post) && $parent_post->post_type == "usam-product" ) 
		{
			ob_start();		
			echo '
	<script type="text/javascript">
		jQuery(function()
		{
			jQuery("a.wp-post-thumbnail").each(function(){
				var product_image = jQuery(this).text();
				if (product_image == "' . esc_js( __( 'Use as featured image' ) ) . '") {
					jQuery(this).text("' . esc_js( __('Использовать как миниатюру для товара', 'usam') ) . '");
				}
			});
			var trash = jQuery("#media-upload a.del-link").text();
			if (trash == "' . esc_js( __( 'Delete' ) ) . '") {
				jQuery("#media-upload a.del-link").text("' . esc_js( __( 'Удалить','usam' ) ) . '");
			}
		});
	</script>';
			$out .= ob_get_clean();
		}
		return $form_fields;

	}
	
	// Сохраняет настройки фотографий товаров
	function save_attachment_fields( $post, $attachment )
	{		
		return $post;
	}
	
	function media_upload_tab_gallery( $tabs ) 
	{
		unset( $tabs['gallery'] );
		$tabs['gallery'] = __( 'Галерея фотографий товара', 'usam' );
		return $tabs;
	}
	
	function media_upload_url( $form_action_url ) 
	{
		$form_action_url = esc_url( add_query_arg( array( 'parent_page'=>'usam-edit-products' ) ) );
		return $form_action_url;
	}

	function gallery_css_mods() 
	{
		print '
		<style type="text/css">
				#gallery-settings *{ display:none; }
				a.wp-post-thumbnail { color:green; }
				#media-upload a.del-link { color:red; }
				#media-upload a.wp-post-thumbnail {	margin-left:0px; }
				td.savesend input.button { display:none; }
		</style>';
		print '
		<script type="text/javascript">
		jQuery(function(){
			jQuery("td.A1B1").each(function()
			{
				var target = jQuery(this).next();
					jQuery("p > input.button", this).appendTo(target);
			});
			jQuery("a.wp-post-thumbnail").each(function()
			{
				var product_image = jQuery(this).text();
				if (product_image == "' . __( 'Use as featured image' ) . '") {
					jQuery(this).text("' . __( 'Использование в качестве миниатюры товара', 'usam' ) . '");
				}
			});
		});
		</script>';
	}
	
	// Переименовать загружаемые картинки. Имя сделать название артикула товара
	function attachment_fields_to_save( $post_attacment, $attachment )
	{				
		if ( empty($post_attacment['ancestors'][0]) )
			return $post_attacment;  
		
		$post = get_post( $post_attacment['ancestors'][0] );	
		if ( $post->post_type != 'usam-product' )	
		{
			return $post_attacment;  
			//$title = $post->post_title;
			//$format = $post->post_name;	  
		}
		else
		{
			$sku = usam_get_product_meta( $post_attacment['ancestors'][0], 'sku' );
			if ( empty( $sku ) )	
			{
				$title = $post->post_title;
				$format = $post->post_name;
			}
			else
			{
				$sku = preg_replace("/[^A-Za-z0-9-_]/", "", $sku);					
				$title = $post->post_title.' ['.$sku.']';
				$format = $post->post_name;						
			}
		}	
		if ( empty($format) )
			return $post_attacment;  
		
		$old_filepath = get_attached_file( $post_attacment['ID'] ); 
		if ( !file_exists($old_filepath) )  	
			return $post_attacment;
		$path_parts = pathinfo( $old_filepath );
		$directory = $path_parts['dirname']; 
		$old_filename = $path_parts['basename']; 
		$ext = str_replace( 'jpeg', 'jpg', $path_parts['extension'] ); 	
		$noext_old_filename = str_replace( '.'.$ext, '', $old_filename );      
		// Проверим, имя файла на соответствие желаемому стандарту
		$pos = strpos($noext_old_filename, $format);
		if ( $pos !== false )
			return $post_attacment;		
		$i = 1;
		do 
		{		
			$sanitized_media_title = $format."_$i";
			$new_filepath = $directory . '/' . $sanitized_media_title .'.'.$ext;
			if ( !file_exists( $new_filepath ) )
				break;						
			$i++;
		} 
		while ( true );  
		if ( !file_exists($new_filepath) )  			
			rename( $old_filepath, $new_filepath );
		$meta = wp_get_attachment_metadata( $post_attacment['ID'] );
		$meta['file'] = str_replace( $noext_old_filename, $sanitized_media_title, $meta['file'] );	 
		// Переименовать названия файла разных размеров
		foreach ( $meta['sizes'] as $size => $meta_size ) 
		{
			$meta_old_filename = $meta['sizes'][$size]['file'];
			$meta_old_filepath = $directory . '/' . $meta_old_filename;
			$meta_new_filename = str_replace( $noext_old_filename, $sanitized_media_title, $meta_old_filename );
			$meta_new_filepath = $directory . '/' . $meta_new_filename;
			$meta['sizes'][$size]['file'] = $meta_new_filename;
			if ( file_exists($meta_old_filepath) && ((!file_exists($meta_new_filepath)) || is_writable($meta_new_filepath)) )
			{		
			   rename($meta_old_filepath, $meta_new_filepath);           
			}   
		}   
		wp_update_attachment_metadata( $post_attacment['ID'], $meta );	
		update_attached_file( $post_attacment['ID'], $new_filepath );
		$post_attacment['post_title'] = $title;
		$post_attacment['post_name'] = $sanitized_media_title;
		$post_attacment['guid'] = str_replace( $noext_old_filename, $sanitized_media_title, $post_attacment['guid'] );
		wp_update_post( $post_attacment ); 
		global $wpdb;	// исправление ошибки обновление ссылки. wp_update_post меняет guid
		$wpdb->query("UPDATE $wpdb->posts SET guid='".$post_attacment['guid']."' WHERE ID = '".$post_attacment['ID']."' LIMIT 1");	
		if ( isset( $_REQUEST['_wp_original_http_referer'] ) && strpos( $_REQUEST['_wp_original_http_referer'], '/wp-admin/' ) === false )	
			$_REQUEST['_wp_original_http_referer'] = get_permalink( $post_attacment['ID'] );    
		return $post_attacment;
	}
	
	//Переименовать изображение при первой загрузке
	function rename_attacment( $data, $attachment_ID )
	{   		
		$post_attacment = get_post( $attachment_ID );
		
		if ( $post_attacment->post_parent == 0 )
			return $data;  
			
		$old_filepath = get_attached_file( $attachment_ID ); 
		$post = get_post($post_attacment->post_parent);		
		if ( isset($post->post_type) && $post->post_type != 'usam-product' )	
		{
			$title = $post->post_title;
			$format = $post->post_name;	
		} 
		else
		{			
			$sku = usam_get_product_meta( $post->ID, 'sku' );
			if ( empty( $sku ) )	
				return $data;  
			
			$sku = preg_replace("/[^A-Za-z0-9-_]/", "", $sku);
			$title = $post->post_title.' ['.$sku.']';
			$format = $post->post_name;		
			
			// Узнаем цвет изображения
		/*	$colors_to_show = 25;
			require_once( USAM_FILE_PATH . '/includes/image/colors.inc.php' );	
			$pal = new GetMostCommonColors( $old_filepath );
			$colors = $pal->get_group_color( $colors_to_show );	
			
			$array = array_flip($colors); 
			unset ($array['white']); 
			$colors = array_flip($array); 
			
			update_product_metadata( $post->ID, 'colors', $colors ); */
		}			
		$path_parts = pathinfo( $old_filepath );
		$directory = $path_parts['dirname']; 
		$old_filename = $path_parts['basename']; 
		$ext = str_replace( 'jpeg', 'jpg', $path_parts['extension'] ); 	
		$noext_old_filename = str_replace( '.'.$ext, '', $old_filename );      
		// Проверим, имя файла на соответствие желаемому стандарту		
		if ( !empty($format) )
		{	
			$pos = strpos($data['file'], $format);
			if ( $pos !== false )
				return $data;		
		}
		else
			return $data;  
		$i = 1;
		do 
		{		
			$sanitized_media_title = $format."_$i";
			$new_filepath = $directory . '/' . $sanitized_media_title .'.'.$ext;
			if ( !file_exists( $new_filepath ) )
				break;						
			$i++;
		} 
		while ( true );
		if ( !file_exists($new_filepath) )  
			rename( $old_filepath, $new_filepath );	
		$data['file'] = str_replace( $noext_old_filename, $sanitized_media_title, $data['file'] );	
		// Переименовать названия файла разных размеров
		foreach ( $data['sizes'] as $size => $meta_size ) 
		{
			$meta_old_filename = $meta_size['file'];
			$meta_old_filepath = $directory . '/' . $meta_old_filename;
			$meta_new_filename = str_replace( $noext_old_filename, $sanitized_media_title, $meta_old_filename );
			$meta_new_filepath = $directory . '/' . $meta_new_filename;
			$data['sizes'][$size]['file'] = $meta_new_filename;
			if ( file_exists($meta_old_filepath) && ((!file_exists($meta_new_filepath)) || is_writable($meta_new_filepath)) ) 	
			   rename($meta_old_filepath, $meta_new_filepath); 
		} 	
		update_attached_file( $attachment_ID, $new_filepath );
		$guid = str_replace( $noext_old_filename, $sanitized_media_title, $post_attacment->guid );
		$attachment['ID'] = $attachment_ID;
		$attachment['post_title'] = $title;
		$attachment['post_name'] = $sanitized_media_title;
		$attachment['guid'] = $guid;
		wp_update_post( $attachment ); 
		global $wpdb;	// исправление ошибки обновление ссылки. wp_update_post меняет guid
		$wpdb->query("UPDATE $wpdb->posts SET guid='".$guid."' WHERE ID = '$attachment_ID' LIMIT 1");	
		return $data;
	}
}
$media_gallery = new USAM_Media_Gallery();
?>