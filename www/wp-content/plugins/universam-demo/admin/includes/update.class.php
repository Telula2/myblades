<?php
/**
 * Обновление магазина
 */ 	
new USAM_Update();
final class USAM_Update
{			
	public function __construct( ) 
	{
		add_action( 'upgrader_process_complete', array( $this, 'process_complete' ), 20 );				
	}
	
	public static function process_complete( $upgrader ) 
	{
		if ( !empty($upgrader->skin->plugin_info ) && !empty($upgrader->skin->plugin_info['Name']) )
		{			
			$name = mb_strtolower($upgrader->skin->plugin_info['Name']);
			if ( $name == 'universam' || $name == 'universam-demo' )
			{	
				set_transient( 'usam_process_complete', true, DAY_IN_SECONDS );	
				$this->start_update();		

				$api = new Universam_API();
				$result = $api->check_license( );				
			}
		}					
	}
	
	private static function maybe_upgrade() 
	{ 
		$current_db_ver = (int)get_option( 'usam_db_version', 0 );
		if ( $current_db_ver == USAM_DB_VERSION )
			return false;
		
		$file_path = USAM_FILE_PATH . '/admin/db/db-upgrades/';
		$dir = opendir( $file_path );	
		while ( ($file = readdir( $dir )) !== false ) 
		{	
			if ( ($file != "..") && ($file != ".") && !stristr( $file, "~" ) )
			{
				$ver_update = basename( $file , '.php' );			
				if ( $current_db_ver < $ver_update  )
				{ 	
					require_once( $file_path . $file );	
					update_option( 'usam_db_version', $ver_update );				
				}
			}
		}	
		update_option( 'usam_db_version', USAM_DB_VERSION );			
	}	
	
	public static function start_update() 
	{	
		if ( ! current_user_can( 'update_plugins' ) )
			return false;
		
		ini_set('max_execution_time', 1900); 			
		
		usam_installing();		
		self::maybe_upgrade();
				
		usam_installing( true );		
	}
}


function usam_needs_upgrade() 
{
	if ( ! current_user_can( 'update_plugins' ) )
		return false;

	$current_db_ver = get_option( 'usam_db_version', 0 );
	if ( USAM_DB_VERSION <= $current_db_ver )
		return false;	
	return true;
}

function usam_installing( $is_installing = null )
{
	static $installing = null;

	if ( is_null( $installing ) )
	{
		$installing = defined( 'USAM_INSTALLING' ) && USAM_INSTALLING;
	}
	if ( ! is_null( $is_installing ) )
	{
		$old_installing = $installing;
		$installing = $is_installing;
		return (bool) $old_installing;
	}
	return (bool) $installing;
}