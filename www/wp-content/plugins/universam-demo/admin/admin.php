<?php
/**
 * Главные функции админского интерфейса
 * @since 3.7   
 */ 
 
//require_once( USAM_FILE_PATH . '/includes/packing-products.class.php' );

require_once( USAM_FILE_PATH . '/admin/menu-page/display-items.page.php' );
require_once( USAM_FILE_PATH . '/admin/includes/product/product-save.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/product/product-metabox.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/product/product.php' );
require_once( USAM_FILE_PATH . '/admin/includes/update.class.php' );
require_once( USAM_FILE_PATH . '/admin/menu-page/display-variations-page.php' );
require_once( USAM_FILE_PATH . '/includes/taxonomy/class-walker-category-select.php' );

require_once( USAM_FILE_PATH . '/admin/includes/admin-assets.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/admin-form-functions.php' );
require_once( USAM_FILE_PATH . '/admin/includes/widget_admin.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/admin_functions.php' ); // Разные функции
require_once( USAM_FILE_PATH . '/admin/includes/code_name.function.php' ); 
require_once( USAM_FILE_PATH . '/admin/includes/metabox.class.php' ); 
require_once( USAM_FILE_PATH . '/admin/includes/help_center/help_tab.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/tinymce.class.php' ); // Редактор
require_once( USAM_FILE_PATH . '/admin/includes/template.php'); 
require_once( USAM_FILE_PATH . '/admin/includes/media_gallery.class.php' );

require_once( USAM_FILE_PATH . '/admin/includes/page_tabs.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/tasks_work.class.php' );	
require_once( USAM_FILE_PATH. '/admin/includes/query_admin.class.php' );

require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-brands.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-category.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-product_attributes.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-category_sale.class.php' );
require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-variation.class.php' );
require_once( USAM_FILE_PATH. '/admin/menu-page/display-license.page.php' );

require_once( USAM_FILE_PATH . '/includes/crm/comment_document.class.php'          );


//if ( get_option('usam_pointer', false) )
//	require_once( USAM_FILE_PATH . '/admin/includes/pointer.class.php' );
			
$load_admin = new USAM_Load_Admin();	
class USAM_Load_Admin
{
	function __construct( ) 
	{				
		require_once( USAM_FILE_PATH. '/admin/includes/admin_menu.class.php' );		
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX == true ) 
			require_once( USAM_FILE_PATH . '/admin/includes/admin_ajax.php' );
		
		if ( isset($_REQUEST['usam_admin_action']) )
			require_once( USAM_FILE_PATH . '/admin/includes/admin_init.class.php' );
				
		if ( isset($_REQUEST['unprotected_query']) )
			require_once( USAM_FILE_PATH . '/admin/includes/admin_unprotected_query.class.php' );
		
		if ( ! empty($_GET['page']) && $_GET['page'] == 'usam-setup' ) 
			require_once( USAM_FILE_PATH . '/admin/includes/setup-wizard.class.php' );	
		
		add_action( 'permalink_structure_changed', array($this, 'action_permalink_structure_changed') );			
		add_filter( 'screen_layout_columns', array($this, 'screen_two_columns'), 10, 2 );
		//add_action( 'init', array($this, 'in_plugin_update_message') );			 // для обновления через сервер wordpress
		add_action('admin_init', array($this, 'theme_admin_notices') );
		if ( version_compare( PHP_VERSION, '5.4.0', '<' ) ) 
			add_action( 'admin_notices', array($this, 'display_php_version_notice') );	
		add_action( 'save_post', array($this, 'refresh_page_urls'), 10, 2 );
		
		add_filter('admin_footer_text', array( $this, 'remove_footer_admin') );
		add_action('admin_init', array( $this, 'select_code_price'), 1 );
		add_action('admin_init', array( $this, 'admin_redirects') );		
		
		add_action( 'current_screen', array( &$this, 'conditional_includes' ) );

		add_action( 'admin_head', array( $this, 'add_favicon') ); 	
		add_filter( 'plugin_action_links_' . USAM_PLUGINSLUG, array( $this, 'add_action_link' ), 10, 2 );	

		$this->updating_service();
		
		if ( isset($_REQUEST['service_api']) )
			$this->display_service_api();
	}
	
	function display_service_api() 
	{	
		wp_die( __('Готово. Можете закрыть окно...', 'usam') );
	}
	
	public function add_action_link( $links, $file ) 
	{ 
		if ( current_user_can( 'edit_shop_settings' ) )
		{
			$settings_link = '<a href="' . esc_url( admin_url( 'admin.php?page=shop_settings' ) ) . '">' . __( 'Настройки', 'usam' ) . '</a>';
			array_unshift( $links, $settings_link );
		}
		if ( usam_is_license_type('FREE') )		
		{
			$premium_link = '<a href="http://wp-universam.ru/buy/">' . __( 'Премиум', 'usam' ) . '</a>';
			array_unshift( $links, $premium_link );
			
			$premium_link = '<a href="http://wp-universam.ru/kontakty/">' . __( 'Поддержка', 'usam' ) . '</a>';
			array_unshift( $links, $premium_link );
			
			$faq_link = '<a href="http://wp-universam.ru/posmotret/">' . __( 'Документация', 'usam' ) . '</a>';
		//	array_unshift( $links, $faq_link );
		}
		return $links;
	}
	
	public function updating_service()
	{		
		if ( !usam_is_license_type('FREE') )
		{
			$license = get_option ( 'usam_license' );
			require_once( USAM_FILE_PATH . '/includes/upgrader.class.php');
			$upgrader = new USAM_Upgrader('universam', array(
				'token' => $license['license'],
				'version' => USAM_VERSION,
				'slug' => 'universam',
				'pluginSlug' => USAM_PLUGINSLUG,
				'name' => 'UNIVERSAM'
			));
		}
	}	
	
	function admin_redirects() 
	{	 
		if ( get_transient( '_usam_activation_redirect' ) ) 
		{
			delete_transient( '_usam_activation_redirect' );
			if ( !is_network_admin() && apply_filters( 'usam_prevent_automatic_wizard_redirect', true ) ) 				
			{
				wp_safe_redirect( admin_url( 'index.php?page=usam-setup' ) );
				exit;
			}
		}		
	}
	
	function add_favicon() 
	{	  
	   echo '<link rel="shortcut icon" href="'.USAM_URL.'/admin/images/favicon.jpg"/>';
	}
	
	public function conditional_includes() 
	{		
		$screen = get_current_screen();
		// Таксомании
		if ( isset($_GET['taxonomy']) )
		{			
			require_once( USAM_FILE_PATH . '/admin/includes/taxonomies/display-taxonomies.class.php' );
		}
		switch ( $screen->id ) 
		{
			case 'edit-usam-brands' :
				
			break;
			case 'edit-usam-category' :
	
			break;
			case 'options-permalink' :
				require_once( USAM_FILE_PATH . '/admin/includes/wp-options/class-permalink-settings.php' ); 
			break;			
			case 'users' :
			case 'user' :
			case 'profile' :
			case 'user-edit' :
				require_once( USAM_FILE_PATH . '/admin/includes/custom_user_profile_fields.class.php' ); 
			break;
		}
	}		
			
	function select_code_price()
	{
		global $type_price, $user_ID;		
		if ( isset($_REQUEST['type_price']) )
		{
			$code_price = sanitize_title($_REQUEST['type_price']);	
			$prices = usam_get_prices( );			
			foreach ( $prices as $id => $value )
			{	
				if ( $code_price == $value['code'] )
				{
					$type_price = $value['code'];
					break;				
				}
			}		
			unset($_REQUEST['type_price']);
			update_user_meta($user_ID, 'manager_type_price', $type_price ); 			
		}
		else
		{
			$type_price = get_the_author_meta( 'manager_type_price', $user_ID ); 
			if ( !empty($type_price) )
				$price = usam_get_name_price_by_code( $type_price );
			
			if ( empty($price) )
			{
				$type_prices = usam_get_prices( );			
				foreach ( $type_prices as $id => $value )
				{	
					$type_price = $value['code'];
					break;				
				}					
				update_user_meta($user_ID, 'manager_type_price', $type_price ); 		
			}			
		}
	}	
	
	//Изменяем сообщение в футере админки
	function remove_footer_admin ()
	{
		echo '<span id="footer-thankyou"><a href="http://www.wp-universam.ru/">УНИВЕРСАМ:</a> управление интернет-магазином '.USAM_VERSION.' © УНИВЕРСАМ, 2012 - 2018</span>';
	}
	
	/**
	 * Обновить URL-адрес страницы, когда обновляется страница
	 */
	function refresh_page_urls( $post_id, $post )
	{
		if ( ! current_user_can( 'manage_options' ) )
			return;
		if ( 'page' != $post->post_type )
			return;
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( ! in_array( $post->post_status, array( 'publish', 'private' ) ) )
			return;
		usam_update_permalink_slugs();
		return $post_id;
	}
	
	function display_php_version_notice() 
	{
	?>
		<div id='usam-warning' class='error'><p><?php printf( __( 'Вы используете PHP %s. УНИВЕРСАМ %s требует PHP 5.4 или выше. Пожалуйста, свяжитесь с вашим хостинг-провайдером для получения дополнительной помощи.', 'usam' ), PHP_VERSION, USAM_VERSION ); ?></p></div>
	<?php
	}

	//Модификация для обеспечения макета несколько колонок
	function screen_two_columns( $columns, $screen ) 
	{
		if ( $screen == 'toplevel_page_usam-edit-products' )
			$columns['toplevel_page_usam-edit-products'] = 2;
		return $columns;
	}	
	
	/**
	 * Отображает уведомление обновление базы
	 */
	function database_update_notice() 
	{ 
		?>
		<div class="error">
			<p><?php printf( __( '<strong>Платформа для интернет-магазина УНИВЕРСАМ нуждаются в обновлении</strong>.<br>Вы должны <a href="%1s">обновить базу данных</a> вашего магазина, чтобы продолжить работу.', 'usam' ), admin_url( 'admin.php?page=shop&tab=update' ) ) ?></p>
		</div>

	<?php
	}
	
	// вывести сообщение об обновлении
	function theme_admin_notices() 
	{	
		if ( usam_needs_upgrade() ) 	
			add_action ( 'admin_notices', array($this, 'database_update_notice') );	
	}
	
	/**
	 * Уведомление об обновлении
	 */
	function update_notice() 
	{
		$info_title = __( 'Пожалуйста, сделайте резервное копирования вашего сайта перед обновлением!', 'usam' );
		$info_text =  __( 'Перед обновлением создайте резервную копию базы данных и файлы в случае что-то пойдет не так.', 'usam' );
		echo '<div style="border-top:1px solid #CCC; margin-top:3px; padding-top:3px; font-weight:normal;"><strong style="color:#CC0000">' . strip_tags( $info_title ) . '</strong> ' . strip_tags( $info_text, '<br><a><strong><em><span>' ) . '</div>';
	}
	
	function in_plugin_update_message() 
	{
		add_action( 'in_plugin_update_message-' . USAM_DIR_NAME . '/universam.php', array($this, 'update_notice') );
	}	
	
	/**
	 * Обновить URL страницы продуктов при изменении настройки схемы постоянных ссылок.
	 */
	function action_permalink_structure_changed() 
	{
		
	}
}

function usam_license() 
{
	$license = get_option ( 'usam_license', array() );			
	if ( !empty($license['status']) && $license['status'] == 1 && ( empty($license['date']) || strtotime($license['date']) < time() ) )	
		return true;
	else
		return false;
}
?>