<?php
/*
 * Plugin Name: UNIVERSAM
 * Plugin URI: http://wp-universam.ru
 * Description: Платформа для создания мощного интернет-магазина. Обеспечивающая учет и продажу товаров, а также аналитику и статистику. Великолепное отображение товаров, красивые письма для клиентов.
 * Version: 5.30.1
 * Author: universam
 * Author URI: http://wp-universam.ru
 * Text Domain: usam
 * Domain Path: /languages/

 * Copyright 2012 - 2018
*/

final class UNIVERSAM 
{	
	protected static $_instance = null;
	private          $version = '5.30.1';
	
	public function __construct()
	{	
		register_activation_hook( __FILE__, array( 'USAM_Install', 'install' ) );
		register_deactivation_hook( __FILE__, array( 'USAM_Install', 'deactivate' ) );		
		
		$this->constants();				
		$this->includes();		
		
		add_action( 'plugins_loaded', array( $this, 'init' ) );	
	}
	
	public static function instance() 
	{
		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self();
		}
		return self::$_instance;
	}	
		
	/**
	 * Настройка основные константы
	 */
	function constants() 
	{
		do_action( 'usam_started' );        // Конец загрузки		
		
		define( 'USAM_FILE_PATH', dirname( __FILE__ ) );    // Задать путь к файлам плагина		
		define( 'USAM_URL',       plugins_url( '', __FILE__ ) );   // Определяет URL к папке плагина			
	
		define( 'USAM_VERSION', $this->version );		
		define( 'USAM_DB_VERSION', 380 );				
		
		require_once( USAM_FILE_PATH . '/includes/constants.php' );					
		do_action( 'usam_constants' );           // Любые дополнительные константы можно подключить здесь
	}

	/**
	 * Инициализация класса
	 */
	function init() 
	{	
		do_action( 'usam_pre_init' ); // До инициализации			
		
		$this->maintenance_mode();		
		$this->load();		
		if ( is_admin() )		
			include_once( USAM_FILE_PATH . '/admin/admin.php' );	
		else
			include_once( USAM_FILE_PATH . '/includes/load_site.class.php' );		
				//require_once( USAM_FILE_PATH . '/includes/viber.class.php'        );
		do_action( 'usam_init' );     // После инициализации				
	}
	
	function languages() 
	{	
		//load_plugin_textdomain( 'usam', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}	
	
		//отключить сайт для всех, кроме администратора сайта
	function maintenance_mode() 
	{
		if ( ( defined( 'USAM_INSTALLING' ) && USAM_INSTALLING || get_option('usam_stand_service', 0 ) == 1) && (!current_user_can( 'administrator' ) || !is_user_logged_in()) ) 
		{
			wp_die( __('На сайте ведутся работы по техническому обслуживанию. Пожалуйста подождите...', 'usam') );
		}
	}	
	
	/**
	 * Подключение файлов
	 */
	function includes()
	{			
		require_once( USAM_FILE_PATH . '/includes/includes.php' );	
		do_action( 'usam_includes' );               // Любые дополнительные файлы включаются здесь
	}	
	
	/**
	Описание: Загрузка сессии
	 */
	function load_session() 
	{		
		if ( ! isset( $_SESSION ) )
			$_SESSION = null;
		else
			return;
	
		if ( ( empty($_SESSION) ) )
		{ 
			session_start();
		}
	}		
	
	/* Создать идентификатор клиента после 'plugins_loaded'
	 * @since  3.8.9
	 */
	function action_create_customer_id() 
	{
		usam_get_current_customer_id( 'create' );			
	}		

	// если наши правила еще не включены
	function flush_rules_universam()
	{
		$rules = get_option( 'rules' );
		if ( !isset($rules['(your-account)/(\d*)$']) ) 
		{
			global $wp_rewrite;
			$wp_rewrite->flush_rules();
		}
	}

	/**
	 * Настройка ядра
	 */
	function load() 
	{		
		do_action( 'usam_pre_load' ); // Крюк перед установкой	
	
		$this->languages();			
	
		$this->load_session();           			  // Запустить сессии		
		$this->action_create_customer_id( );     // Настройка клиент ID на всякий случай, чтобы убедиться, что он правильно установлен				

		//add_action( 'wp_loaded', array('flush_rules_universam') );						
		add_action('sanitize_title', 'usam_sanitize_title_with_translit', 0); //Русские в английские
				
		do_action( 'usam_loaded' );  // крюк когда UNIVERSAM полностью загружен				
	}		
}
UNIVERSAM::instance();
?>