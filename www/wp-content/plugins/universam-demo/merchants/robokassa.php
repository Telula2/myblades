<?php 
/*
  Merchant Name: Модуль оплаты Robokassa
 */
class USAM_Merchant_robokassa extends USAM_Merchant 
{	
	protected $api_version = '2.0';
	protected $type_operation = 'a';
	protected $ipn = true;
	protected $payment_link = 'https://auth.robokassa.ru/Merchant/Index.aspx';	
	
	/**
	* передает полученные данные в платежный шлюз
	*/
	function get_vars( $aggregate = false ) 
	{
		$args = array(
			'MrchLogin' => $this->gateway_option['login'],
			'OutSum' => $this->payment['sum'],		
			'InvDesc' => sprintf( __("Оплата заказа №%s","usam"), $this->purchase_id),				
			'InvId' => $this->purchase_id,			
			'Culture' => $this->get_gateway_currency_code(),
			'shp_payment_number' => $this->payment_number,
		);		
		if ( $this->is_debug() )
		{			
			$args['IsTest'] = 1;		
			$args['SignatureValue'] = md5($this->gateway_option['login'].':'.$this->payment['sum'].':'.$this->purchase_id.':'.$this->gateway_option['key1_test'].':shp_payment_number='.$this->payment_number );
		}	
		else
		{			
			$args['SignatureValue'] = md5($this->gateway_option['login'].':'.$this->payment['sum'].':'.$this->purchase_id.':'.$this->gateway_option['key1'].':shp_payment_number='.$this->payment_number );		
		}
		return $args;
	}		
	
	/**
	 * Получить номер платежного документа
	 */
	protected function get_payment_number( ) 
	{ 
		if (isset($this->gateway_data['shp_payment_number']))
			return $this->gateway_data['shp_payment_number'];
		else
			return false;
	}
	
	/**
	 * Процесс шлюза уведомления, проверяет и решает, что делать с данными шлюза, выданных в торговых файлы
	*/
	protected function parse_gateway_notification() 
	{ 	
		$inv_id = absint($this->gateway_data['InvId']);
		$this->payment_number = $this->gateway_data['shp_payment_number'];	
		$out_summ = (float)$this->payment['sum'];		
		
		if ( $this->is_debug() )
			$key = $this->gateway_option['key1_test'];			
		else
			$key = $this->gateway_option['key2'];		
		
		if ( $this->gateway_data['SignatureValue'] == md5($out_summ.':'.$inv_id.':'.$key.':shp_payment_number='.$this->payment_number) )
		{				
			$this->update_payment_document( array( 'status' => 3 ) );	
			echo 'OK'.$inv_id;			
			return true;
		}
		else
			return false;		
	}
	
	protected function form()
	{
		$default = array( 'login' => '', 'key1' => '', 'key2' => '', 'key1_test' => '', 'key2_test' => '');
				
		$this->gateway_option = array_merge ($default, $this->gateway_option);		
		$output = "
			<tr>
				<td class ='name'>" . __('Логин', 'usam') . "</td>
				<td><input type='text' size='40' value='".$this->gateway_option['login']."' name='gateway_handler[login]' /></td>
			</tr>
			<tr>
				<td class ='name'>" . __('Пароль #1', 'usam') . "</td>
				<td><input type='text' size='40' value='".$this->gateway_option['key1']."' name='gateway_handler[key1]' /></td>
			</tr>
			<tr>
				<td class ='name'>" . __('Пароль #2', 'usam') . "</td>
				<td><input type='text' size='40' value='".$this->gateway_option['key2']."' name='gateway_handler[key2]' /></td>
			</tr>
			<tr>
				<td class ='name'>" . __('Пароль #1 тестового режима', 'usam') . "</td>
				<td><input type='text' size='40' value='".$this->gateway_option['key1_test']."' name='gateway_handler[key1_test]' /></td>
			</tr>
			<tr>
				<td class ='name'>" . __('Пароль #2 тестового режима', 'usam') . "</td>
				<td><input type='text' size='40' value='".$this->gateway_option['key2_test']."' name='gateway_handler[key2_test]' /></td>
			</tr>";
		return $output;
	}
}
?>