<?php
/**
 * Merchant Name: Квитанция Сбербанка
 */
class USAM_Merchant_receipt_sberbank extends USAM_Merchant 
{
	protected $api_version = '2.0';
	protected $type_operation = 'c';
	
	function display_the_page_transaction() 
	{	
		$printed_form_url = usam_url_action( 'printed_form', usam_get_url_system_page( 'transaction-results' ), array( 'form' => 'receipt_sberbank', 'type' => 'payment', 'id' => $this->payment['id'] ) );
		$printed_form_to_pdf_url = usam_url_action( 'printed_form_to_pdf', usam_get_url_system_page( 'transaction-results' ), array( 'form' => 'receipt_sberbank', 'id' => $this->payment['id'] ) );
		$output = "<div class='button_pay_order'>
		<ul>
			<li><a href='$printed_form_url' target='_blank'>".__( 'Распечатать квитанцию', 'usam' )."</a></li>
			<li><a href='$printed_form_to_pdf_url' target='_blank'>".__( 'Скачать квитанцию', 'usam' )."</a></li>
		</ul></div>";
		return $output;
	}
}