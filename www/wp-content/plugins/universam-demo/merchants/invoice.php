<?php
/**
 * Merchant Name: Оплата по счету 
 */
class USAM_Merchant_invoice extends USAM_Merchant 
{
	protected $api_version = '2.0';
	protected $type_operation = 'c';	
	function __construct(  ) 
	{		
		$this->currency_list = array('AUD', 'BRL', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'MYR', 'NOK', 'NZD', 'PHP', 'PLN', 'SEK', 'SGD', 'THB', 'TWD', 'USD', "RUB");	
		parent::__construct( );
	}
	
	function display_the_page_transaction() 
	{	
		$printed_form_url = usam_url_action( 'printed_form', usam_get_url_system_page( 'transaction-results' ), array( 'form' => 'payment_invoice', 'type' => 'payment', 'id' => $this->payment['id'] ) );
		$printed_form_to_pdf_url = usam_url_action( 'printed_form_to_pdf', usam_get_url_system_page( 'transaction-results' ), array( 'form' => 'payment_invoice', 'id' => $this->payment['id'] ) );
		$output = "
		<div class='button_pay_order'>
		<ul>
			<li><a href='$printed_form_url' target='_blank'>".__( 'Распечатать счет', 'usam' )."</a></li>
			<li><a href='$printed_form_to_pdf_url' target='_blank'>".__( 'Скачать счет', 'usam' )."</a></li>
		</ul>
		</div>";
		return $output;
	}
	
	/**
	* передает полученные данные в платежный шлюз
	*/
	function submit() 
	{ 		
		$notification = new USAM_Сustomer_Notification_Invoice( $this->payment['id'] );
		$email_sent = $notification->send_mail();
	
		$this->set_purchase_processed_by_purchid('waiting_payment');
		$this->go_to_transaction_results();		
		exit();
	}
		
	protected function form() 
	{
		$output = '';
		return $output;
	}
}