<?php
/**
 * Merchant Name: Оплата через Сбербанк
 */
class USAM_Merchant_sberbank extends USAM_Merchant 
{
	protected $api_version = '2.0';
	protected $type_operation = 'a';
	protected $ipn = true;		
	protected $user_account_url = 'https://securepayments.sberbank.ru/mportal3/login';
	
	function __construct( ) 
	{	
		$this->currency_list = array("RUB", "USD");	
		parent::__construct( );
	}
			
	protected function get_url_sberbank( $function )
	{		
		if ( !empty($this->gateway_option['url']) )
			$url = "https://securepayments.sberbank.ru/payment/rest";				
		else
			$url = "https://3dsec.sberbank.ru/payment/rest";
		
		$url = "$url/$function.do";
		return $url;
	}	
	
	protected function get_payment_status( )
	{				
		$currency_code = $this->get_gateway_currency_code();	
		$currency = usam_get_currency( $currency_code );	
		
		$headers["Content-Type"] = 'application/x-www-form-urlencoded';
		
		$language = mb_strtolower($this->get_country_code());
		if ( !empty($this->payment['transactid']) )
		{			
			$params = array(			
				'orderId'  => $this->payment['transactid'],		
				'language' => $language, // язык меню	
				'password' => $this->gateway_option['pass'],
				'userName' => $this->gateway_option['login'],						
			);		
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.1',
				'blocking' => true,
		//		'headers' => $headers,
				'body' => $params,
				'cookies' => array(),
				'sslverify' => false
			);				
			$result = $this->send_request( $this->get_url_sberbank('getOrderStatus'), $args );	
			return $result;		
		}
		return false;
	}
	
	protected function get_url( )
	{				
		$currency_code = $this->get_gateway_currency_code();	
		$currency = usam_get_currency( $currency_code );	
		
		$headers["Content-Type"] = 'application/x-www-form-urlencoded';
		
		$language = mb_strtolower($this->get_country_code());
		if ( !empty($this->payment['transactid']) )
		{	
			$result = $this->get_payment_status();
			if ( isset($result['orderStatus']) )
			{
				return false;
			}				
		}
		$amount = $this->payment['sum']*100; 
		$params = array(			
			'amount' => $amount,
			'currency' => $currency['numerical'],
			'language' => $language, // язык меню
			'orderNumber' => $this->payment_number,
			'password' => $this->gateway_option['pass'],
			'userName' => $this->gateway_option['login'],				
			'returnUrl' => $this->url_return, // при удачном платеже
			'failUrl' => $this->url_cancel_return,// URL-адрес, к которому PayPal перенаправляет браузер покупателя, если они отменить проверку до завершения их выплаты.
			'pageView' => 'DESKTOP', //DESKTOP – для отображения на экранах ПК  MOBILE – для отображения на экранах мобильных устройств		
			'description' => sprintf( __("Оплата заказа %s"), $this->order['id']),				
		);		
		$args = array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.1',
			'blocking' => true,
	//		'headers' => $headers,
			'body' => $params,
			'cookies' => array(),
			'sslverify' => false
		);					
		$result = $this->send_request( $this->get_url_sberbank('register'), $args );				
		if ( isset($result['errorCode']) )
		{
			$this->set_error( $result['errorMessage'] );
		}		
		if ( is_array($result) && !empty($result['formUrl']) )
		{			
			$payment['transactid'] = $result['orderId'];
			$this->update_payment_document( $payment );				
			return $result['formUrl'];
		}
		else
			return $this->url_cancel_return;		
	}	
	
	/**
	 * Получить номер платежного документа
	 */
	protected function get_payment_number( ) 
	{  
		if (isset($this->gateway_data['orderNumber']))
			return $this->gateway_data['orderNumber'];
		else
			return false;
	}
	
	/**
	 * Процесс уведомления из торгового шлюза. Проверяет данные и возвращает ответ.
	 
approved - операция удержания (холдирования) суммы;
deposited - операция завершения;
reversed - операция отмены;
refunded - операция возврата.

checksum

Возвращает
[gateway] => sberbank
[orderNumber] => PH0000002675
[mdOrder] => 34f1998c-e7df-7718-34f1-998c00148666
[operation] => deposited
[status] => 1
*/
	protected function parse_gateway_notification() 
	{			
		if ( isset($this->gateway_data['status']) && isset($this->gateway_data['operation']) )
		{			
	//		$checksum = $this->gateway_data['checksum'];
		//	unset($this->gateway_data['checksum']);
			ksort($this->gateway_data);
			
			$transact_id = $this->gateway_data['mdOrder'];		
			$payment = usam_get_payment_document( $this->payment_number, 'document_number');	
			if ( $transact_id != $payment['transactid'] )
			{		
				$this->set_error( "Не равен $transact_id != ".$payment['transactid'] );
				return false;				
			}		
			$str = '';
			foreach ( $this->gateway_data as $key => $value)
				$str .= $key.';'.$value;
				
		/*	$hmac =  hash_hmac('sha256', $str, $this->gateway_option['pass'] );		
			if ( strtoupper($hmac) == $checksum )
			{
				$this->set_error( __('Контрольная сумма не верна','usam') );
				return false;
			}
		*/ 
			if ( $this->gateway_data['operation'] == 'deposited' ) 
			{
				if ( $this->gateway_data['status'] == 1 ) 
					$status = 3; //Оплачено
				else
					$status = 2; //Отклонено
			
				$payment['status'] = $status;			
				$this->update_payment_document( $payment );					
			}
			header("HTTP/1.1 200 OK");
		}	
    }
	
	protected function form() 
	{				
		$default = array( 'login' => '', 'pass' => '', 'url' => 0 );
		$this->gateway_option = array_merge ($default, $this->gateway_option);		
		$output = "
		<tr>
		  <td class = 'name'>" . __( 'Имя пользователя:', 'usam' ) . " </td>
		  <td> <input type='text' size='40' value='".$this->gateway_option['login']."' name='gateway_handler[login]' /></td>
		  <td></td>
		</tr>	
		<tr>
		   <td class = 'name'>" . __( 'Пароль:', 'usam' ) . " </td>
		   <td> <input type='text' size='40' value='".$this->gateway_option['pass']."' name='gateway_handler[pass]' /></td>
		   <td></td>
		</tr>
		<tr>
		  <td class = 'name'>" . __( 'Тип среды:', 'usam' )."</td>
		  <td>
			<select name='gateway_handler[url]'>
				<option value='0' ". selected( $this->gateway_option['url'], 0, false ) .">" . __( 'Тестовая', 'usam' ) . "</option>
				<option value='1' ". selected( $this->gateway_option['url'], 1, false ) .">" . __( 'Реальный счет', 'usam' ) . "</option>
			</select>
		   </td>
		   <td>".__( 'Если вы хотите принимать деньги используйте "Реальный счет".', 'usam' )."</td>
		</tr>	
		";		
		return $output;
	}	
}