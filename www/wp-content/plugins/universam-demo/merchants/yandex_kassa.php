<?php 
/*
  Merchant Name: Модуль оплаты Яндекс Касса
 */
class USAM_Merchant_yandex_kassa extends USAM_Merchant 
{	
	protected $api_version = '2.0';
	protected $type_operation = 'a';
	protected $ipn = true;
	protected $payment_link = 'https://payment.yandex.net/api/v3/payments';

	/**
	* передает полученные данные в платежный шлюз
	payment_method_data:
	
bank_card — банковская карта;
yandex_money — Яндекс.Деньги;
sberbank — Сбербанк Онлайн;
qiwi — QIWI Wallet;
webmoney — Webmoney,
alfabank — Альфа-Клик;
cash — оплата наличными в терминале.
	*/
	function get_url( ) 
	{	
		if ( !$this->gateway_option['login'] || !$this->gateway_option['pass'] )
		{
			return false;
		}			
		$params = array(
			'amount' => array( 'value' => $this->payment['sum'], 'currency' => $this->get_gateway_currency_code() ),	
			'payment_method_data' => array( 'type' => 'bank_card'),
			'confirmation' => array( 'type' => 'redirect', 'return_url' => $this->url_return ),	
		//	'description' => sprintf( __("Оплата заказа %s"), $this->order['id']),			
		//	'metadata' => array( '' => )
		);				
		$headers['Authorization'] = 'Basic '.base64_encode($this->gateway_option['login'].':'.$this->gateway_option['pass']);
		$headers["Idempotence-Key"] = $this->payment_number;
		$headers["Content-Type"] = 'application/json';
		$headers["Accept"] = 'application/json';
		
		$args = array(
			'method' => 'POST',
			'timeout' => 45,
			'user-agent' => 'UNIVERSAM',
			'redirection' => 5,
			'blocking' => true,
			'headers' => $headers,
			'body' => json_encode($params),
			'sslverify' => true,
		);
		$result = $this->send_request( $this->payment_link, $args);		
		if ( isset($result['type']) && $result['type'] == 'error' )
		{
			$this->set_error( $result['description'] );
		}				
		if ( is_array($result) && !empty($result['confirmation']['confirmation_url']) )
		{	
			$this->update_payment_document( array( 'transactid' => $result['id'] ) );				
			return $result['confirmation']['confirmation_url'];
		}
		else
			return $this->url_cancel_return;		
	}	
		
	/**
	 * Получить номер платежного документа
	 */
	protected function get_payment_number( ) 
	{ 
		if ( !isset($_REQUEST['object']) )
			return false;		
		
		$object = $_REQUEST['object'];			
		$transactid = sanitize_text_field($object['id']);
		
		$payments = usam_get_payments( array('transactid' => $transactid ) );
		if ( empty($payments))
			return false;					
	
		return $payments[0]->document_number;
	}

	/**
	 * Процесс шлюза уведомления, проверяет и решает, что делать с данными шлюза, выданных в торговых файлы
 {
  "type": "notification",
  "event": "payment.waiting_for_capture",
  "object": {
    "id": "21740069-000f-50be-b000-0486ffbf45b0",
    "status": "waiting_for_capture",
    "paid": true,
    "amount": {
      "value": "2.00",
      "currency": "RUB"
    },
    "created_at": "2017-10-14T10:53:29.072Z",
    "metadata": {},
    "payment_method": {
      "type": "yandex_money",
      "id": "731714f2-c6eb-4ae0-aeb6-8162e89c1065",
      "saved": false,
      "account_number": "410011066000000",
      "title": "Yandex.Money wallet 410011066000000"
    }
  }
}

//vat_code
Код	Ставка НДС
1	Без НДС
2	НДС по ставке 0%
3	НДС по ставке 10%
4	НДС чека по ставке 18%
5	НДС чека по расчетной ставке 10/110
6	НДС чека по расчетной ставке 18/118
	*/
	protected function parse_gateway_notification() 
	{ 	
		if ( !isset($_REQUEST['object']) )
			return false;		
		
		$object = $_REQUEST['object'];	
		
		if ( $object['status'] != 'waiting_for_capture' )
			return false;	
		
		$transactid = sanitize_text_field($object['id']);				
		
		$email = usam_get_buyers_email( $this->purchase_id );
		if ( !$email )
			$phone = usam_get_buyers_phone( $this->purchase_id );
		else
			$phone = '';
		
		$items = array();
		$products = $this->purchase_log->get_order_products();			
		foreach ( $products as $product ) 
		{
			$items[] = array( 'description' => $product->name, 'quantity' => $product->quantity, 'amount' => array( 'amount' => $product->price, 'currency' => $this->get_gateway_currency_code() ), 'vat_code' => 0 );
		}		
		$params = array(
			'amount' => array( 'value' => $this->payment['sum'], 'currency' => $this->get_gateway_currency_code() ),	
			'receipt' => array( 'items' => $items, 'phone' => $phone, 'email' => $email ),	
		);	
		$headers["<".$this->gateway_option['login'].">"] = $this->gateway_option['pass'];
		$headers["Idempotence-Key"] = $this->gateway_option['idempotence_key'];
		$headers["Content-Type"] = 'application/json';
		$args = array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.1',
			'blocking' => true,
			'headers' => $headers,
			'body' => $params,
			'cookies' => array(),
			'sslverify' => false
		);			
		$result = $this->send_request( $this->payment_link.'/'.$transactid."/capture", $args);				
		if ( isset($result['type']) && $result['type'] == 'error' )
		{
			$this->set_error( $result['description'] );
		}
		if ( isset($result['status']) )
		{
			$status = 0;
			switch ( $result['status'] ) 
			{
				case 'pending ' : //платеж создан, но не завершен
					$status = 1;
				break;
				case 'waiting_for_capture' : //платеж завершен и ожидает ваших действий.
					$status = 6;
				break;
				case 'succeeded ' : // платеж успешно завершен,
					$status = 3;
				break;			
				case 'canceled ' :		//  платеж отменен
					$status = 2;
				break;
			}			
			if ( $status )
			{
				$this->update_payment_document( array( 'status' => $status ) );	
				
				//header("HTTP/1.1 200 OK");
			//	header("Status: 200 OK");
			}
		}			
	}
	
	protected function form()
	{
		$default = array( 'login' => '', 'pass' => '', 'idempotence_key' => '');
				
		$this->gateway_option = array_merge ($default, $this->gateway_option);		
		$output = "
		<tr>
			<td class ='name'>" . __('Логин', 'usam') . ":</td>
			<td><input type='text' size='40' value='".$this->gateway_option['login']."' name='gateway_handler[login]' /></td>
		</tr>
		<tr>
			<td class ='name'>" . __('Пароль', 'usam') . ":</td>
			<td><input type='text' size='40' value='".$this->gateway_option['pass']."' name='gateway_handler[pass]' /></td>
		</tr>";
		return $output;
	}
}
?>